namespace WoodPlan5
{
    partial class frm_OM_Job_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Job_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule2 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue2 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling13 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling14 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling15 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling16 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule3 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue3 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule4 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue4 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling17 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling18 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling19 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling20 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling21 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling22 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling23 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling24 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling25 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling26 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling27 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.gridColumn78 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn74 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bbiLinkedDocuments = new DevExpress.XtraBars.BarButtonItem();
            this.bbiViewOnMap = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataSet_HR_Core = new WoodPlan5.DataSet_HR_Core();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.SuspendedRemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.sp06174OMJobEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Job = new WoodPlan5.DataSet_OM_Job();
            this.SuspendedReasonIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06432OMJobSuspensionReasonsWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn79 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn80 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MandatoryCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.DisplayOrderSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ManuallyCompletedCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.VisitSellCalculationLevelTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.VisitCostCalculationLevelTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CalculateSellButton = new DevExpress.XtraEditors.SimpleButton();
            this.CalculateCostButton = new DevExpress.XtraEditors.SimpleButton();
            this.ClientPONumberButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.NoWorkRequiredCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.CancelledReasonIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06268OMCancelledReasonsWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn76 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn77 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ReworkOriginalJobIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ReworkCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.BillingCentreCodeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TenderJobIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TenderIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.gridControlMaterials = new DevExpress.XtraGrid.GridControl();
            this.sp06035OMJobManagerLinkedMaterialsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewMaterials = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn40 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn45 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn46 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn47 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn48 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn49 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn50 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn51 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn52 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn53 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn54 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn55 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn56 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn57 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn58 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn59 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn60 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn61 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn62 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn63 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn64 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn65 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn66 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn67 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit10 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn68 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit11 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn69 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn70 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn71 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn72 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn73 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn75 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControlEquipment = new DevExpress.XtraGrid.GridControl();
            this.sp06034OMJobManagerLinkedEquipmentBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewEquipment = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn41 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn42 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn43 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn44 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LocationYTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LocationXTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.DaysLeewaySpineEdit = new DevExpress.XtraEditors.SpinEdit();
            this.MaximumDaysFromLastVisitSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.MinimumDaysFromLastVisitSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.FinishLongitudeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.FinishLatitudeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.StartLongitudeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.StartLatitudeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RouteOrderSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ClientPOIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AccessPermitIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.DoNotInvoiceClientCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.DoNotPayContractorCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.SelfBillingInvoiceAmountPaidSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SelfBillingInvoicePaidDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.SelfBillingInvoiceReceivedDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.SelfBillingInvoiceIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.DateClientInvoicedDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.SellTotalCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SellTotalCostVATSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SellTotalCostExVATSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CostTotalCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CostTotalCostVATSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CostTotalCostExVATSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SellTotalLabourCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SellTotalLabourVATSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SellTotalLabourVATRateSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SellTotalLabourExVATSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CostTotalLabourCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CostTotalLabourVATSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CostTotalLabourVATRateSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CostTotalLabourExVATSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SellTotalEquipmentCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SellTotalEquipmentVATSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SellTotalEquipmentVATRateSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SellTotalEquipmentExVATSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CostTotalEquipmentCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CostTotalEquipmentVATSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CostTotalEquipmentVATRateSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CostTotalEquipmentExVATSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SellTotalMaterialCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SellTotalMaterialVATSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SellTotalMaterialVATRateSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SellTotalMaterialExVATSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CostTotalMaterialVATRateSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CostTotalMaterialVATSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CostTotalMaterialCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CostTotalMaterialExVATSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ActualEndDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.ActualDurationUnitsDescriptionIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06177OMJobDurationDescriptorsWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ActualDurationUnitsSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ActualStartDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.ExpectedDurationUnitsDescriptorIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ExpectedDurationUnitsSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.FinanceSystemPONumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RequiresAccessPermitCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.JobNoLongerRequiredCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ReactiveCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.JobTypeDescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.JobSubTypeDescriptionButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.SiteNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientNameContractDescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.JobStatusIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06208OMJObStatusesListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.VisitIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.JobTypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.JobIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ExpectedEndDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.ClientInvoiceIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControlLabour = new DevExpress.XtraGrid.GridControl();
            this.sp06033OMJobManagerLinkedlabourBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewLabour = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLabourUsedID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostUnitsUsed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCostUnitDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellUnitsUsed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellUnitDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostPerUnitExVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCostPerUnitVatRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSellPerUnitExVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellPerUnitVatRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostValueExVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostValueVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostValueTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellValueExVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellValueVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellValueTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubTypeDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedStartDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colVisitNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditInteger = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colFullDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelected1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostUnitDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellUnitDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLabourType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCISPercentage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCISValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPurchaseOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEndDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.JobSubTypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CreatedByStaffIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.VisitNumberButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.ExpectedStartDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ItemForJobID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForVisitID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobSubTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCreatedByStaffID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAccessPermitID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTenderID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTenderJobID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForBillingCentreCodeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForReworkOriginalJobID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup27 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForClientPOID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup29 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForClientNameContractDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobTypeDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForVisitNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobSubTypeDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDisplayOrder = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRouteOrder = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobStatusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCancelledReasonID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem27 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem28 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.simpleSeparator9 = new DevExpress.XtraLayout.SimpleSeparator();
            this.ItemForReactive = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem22 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.simpleSeparator10 = new DevExpress.XtraLayout.SimpleSeparator();
            this.ItemForRework = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem25 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForJobNoLongerRequired = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem26 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup21 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForMinimumDaysFromLastVisit = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMaximumDaysFromLastVisit = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDaysLeeway = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForExpectedStartDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForExpectedDurationUnits = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForExpectedEndDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForExpectedDurationUnitsDescriptorID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForActualStartDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForActualDurationUnits = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForActualEndDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForActualDurationUnitsDescriptionID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup23 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForLocationX = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLocationY = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem20 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup20 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForStartLatitude = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStartLongitude = new DevExpress.XtraLayout.LayoutControlItem();
            this.loca = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForFinishLatitude = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFinishLongitude = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.splitterItem2 = new DevExpress.XtraLayout.SplitterItem();
            this.emptySpaceItem15 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layGrpRemarks = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForMandatory = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNoWorkRequired = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup18 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRequiresAccessPermit = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForManuallyCompleted = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem19 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem21 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem24 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForSuspendedReasonID = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup25 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForLinkedLabourGrid = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup24 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForEquipmentGrid = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup26 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForMaterialGrid = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForSuspendedRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForCostTotalMaterialExVAT = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCostTotalMaterialVATRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCostTotalMaterialVAT = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCostTotalMaterialCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator1 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlGroup15 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForCostTotalCostExVAT = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCostTotalCostVAT = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCostTotalCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator7 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlGroup13 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForCostTotalLabourExVAT = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCostTotalLabourVATRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCostTotalLabourVAT = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCostTotalLabourCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator5 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForVisitCostCalculationLevel = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup11 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForCostTotalEquipmentExVAT = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCostTotalEquipmentVATRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCostTotalEquipmentVAT = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCostTotalEquipmentCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator3 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup10 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSellTotalMaterialExVAT = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSellTotalMaterialVATRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSellTotalMaterialVAT = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSellTotalMaterialCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator2 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlGroup16 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSellTotalCostExVAT = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSellTotalCostVAT = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSellTotalCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator8 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlGroup14 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSellTotalLabourExVAT = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSellTotalLabourVATRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSellTotalLabourVAT = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSellTotalLabourCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator6 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForVisitSellCalculationLevel = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup12 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSellTotalEquipmentExVAT = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSellTotalEquipmentVATRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSellTotalEquipmentVAT = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSellTotalEquipmentCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator4 = new DevExpress.XtraLayout.SimpleSeparator();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForClientPONumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFinanceSystemPONumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup19 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSelfBillingInvoicePaidDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSelfBillingInvoiceReceivedDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSelfBillingInvoiceID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSelfBillingInvoiceAmountPaid = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup17 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForDateClientInvoiced = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientInvoiceID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem16 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem17 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForDoNotPayContractor = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDoNotInvoiceClient = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem29 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp06174_OM_Job_EditTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06174_OM_Job_EditTableAdapter();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp06208_OM_JOb_Statuses_ListTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06208_OM_JOb_Statuses_ListTableAdapter();
            this.sp06033_OM_Job_Manager_Linked_labourTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06033_OM_Job_Manager_Linked_labourTableAdapter();
            this.sp06177_OM_Job_Duration_Descriptors_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06177_OM_Job_Duration_Descriptors_With_BlankTableAdapter();
            this.sp06034_OM_Job_Manager_Linked_EquipmentTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06034_OM_Job_Manager_Linked_EquipmentTableAdapter();
            this.sp06035_OM_Job_Manager_Linked_MaterialsTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06035_OM_Job_Manager_Linked_MaterialsTableAdapter();
            this.sp06268_OM_Cancelled_Reasons_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06268_OM_Cancelled_Reasons_With_BlankTableAdapter();
            this.sp06432_OM_Job_Suspension_Reasons_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06432_OM_Job_Suspension_Reasons_With_BlankTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SuspendedRemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06174OMJobEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Job)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SuspendedReasonIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06432OMJobSuspensionReasonsWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MandatoryCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DisplayOrderSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ManuallyCompletedCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitSellCalculationLevelTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitCostCalculationLevelTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPONumberButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoWorkRequiredCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CancelledReasonIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06268OMCancelledReasonsWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReworkOriginalJobIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReworkCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BillingCentreCodeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TenderJobIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TenderIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlMaterials)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06035OMJobManagerLinkedMaterialsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMaterials)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlEquipment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06034OMJobManagerLinkedEquipmentBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEquipment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LocationYTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LocationXTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DaysLeewaySpineEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaximumDaysFromLastVisitSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinimumDaysFromLastVisitSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FinishLongitudeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FinishLatitudeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartLongitudeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartLatitudeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RouteOrderSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPOIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccessPermitIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DoNotInvoiceClientCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DoNotPayContractorCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelfBillingInvoiceAmountPaidSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelfBillingInvoicePaidDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelfBillingInvoicePaidDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelfBillingInvoiceReceivedDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelfBillingInvoiceReceivedDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelfBillingInvoiceIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateClientInvoicedDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateClientInvoicedDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellTotalCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellTotalCostVATSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellTotalCostExVATSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostTotalCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostTotalCostVATSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostTotalCostExVATSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellTotalLabourCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellTotalLabourVATSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellTotalLabourVATRateSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellTotalLabourExVATSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostTotalLabourCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostTotalLabourVATSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostTotalLabourVATRateSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostTotalLabourExVATSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellTotalEquipmentCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellTotalEquipmentVATSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellTotalEquipmentVATRateSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellTotalEquipmentExVATSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostTotalEquipmentCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostTotalEquipmentVATSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostTotalEquipmentVATRateSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostTotalEquipmentExVATSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellTotalMaterialCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellTotalMaterialVATSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellTotalMaterialVATRateSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellTotalMaterialExVATSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostTotalMaterialVATRateSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostTotalMaterialVATSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostTotalMaterialCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostTotalMaterialExVATSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualEndDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualEndDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualDurationUnitsDescriptionIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06177OMJobDurationDescriptorsWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualDurationUnitsSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualStartDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualStartDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpectedDurationUnitsDescriptorIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpectedDurationUnitsSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FinanceSystemPONumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RequiresAccessPermitCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobNoLongerRequiredCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReactiveCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobTypeDescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobSubTypeDescriptionButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameContractDescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobStatusIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06208OMJObStatusesListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobTypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpectedEndDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpectedEndDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientInvoiceIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlLabour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06033OMJobManagerLinkedlabourBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLabour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobSubTypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByStaffIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitNumberButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpectedStartDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpectedStartDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobSubTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByStaffID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccessPermitID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTenderID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTenderJobID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBillingCentreCodeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReworkOriginalJobID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPOID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientNameContractDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobTypeDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobSubTypeDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDisplayOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRouteOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCancelledReasonID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReactive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRework)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobNoLongerRequired)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMinimumDaysFromLastVisit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMaximumDaysFromLastVisit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDaysLeeway)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpectedStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpectedDurationUnits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpectedEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpectedDurationUnitsDescriptorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualDurationUnits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualDurationUnitsDescriptionID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLocationX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLocationY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartLatitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartLongitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loca)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFinishLatitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFinishLongitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMandatory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNoWorkRequired)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRequiresAccessPermit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForManuallyCompleted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSuspendedReasonID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedLabourGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipmentGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMaterialGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSuspendedRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostTotalMaterialExVAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostTotalMaterialVATRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostTotalMaterialVAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostTotalMaterialCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostTotalCostExVAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostTotalCostVAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostTotalCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostTotalLabourExVAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostTotalLabourVATRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostTotalLabourVAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostTotalLabourCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitCostCalculationLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostTotalEquipmentExVAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostTotalEquipmentVATRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostTotalEquipmentVAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostTotalEquipmentCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellTotalMaterialExVAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellTotalMaterialVATRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellTotalMaterialVAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellTotalMaterialCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellTotalCostExVAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellTotalCostVAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellTotalCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellTotalLabourExVAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellTotalLabourVATRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellTotalLabourVAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellTotalLabourCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitSellCalculationLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellTotalEquipmentExVAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellTotalEquipmentVATRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellTotalEquipmentVAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellTotalEquipmentCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPONumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFinanceSystemPONumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSelfBillingInvoicePaidDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSelfBillingInvoiceReceivedDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSelfBillingInvoiceID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSelfBillingInvoiceAmountPaid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateClientInvoiced)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientInvoiceID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDoNotPayContractor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDoNotInvoiceClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(1415, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 732);
            this.barDockControlBottom.Size = new System.Drawing.Size(1415, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 706);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1415, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 706);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Images = this.imageCollection1;
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // gridColumn78
            // 
            this.gridColumn78.Caption = "ID";
            this.gridColumn78.FieldName = "ID";
            this.gridColumn78.Name = "gridColumn78";
            this.gridColumn78.OptionsColumn.AllowEdit = false;
            this.gridColumn78.OptionsColumn.AllowFocus = false;
            this.gridColumn78.OptionsColumn.ReadOnly = true;
            this.gridColumn78.Width = 53;
            // 
            // gridColumn74
            // 
            this.gridColumn74.Caption = "ID";
            this.gridColumn74.FieldName = "ID";
            this.gridColumn74.Name = "gridColumn74";
            this.gridColumn74.OptionsColumn.AllowEdit = false;
            this.gridColumn74.OptionsColumn.AllowFocus = false;
            this.gridColumn74.OptionsColumn.ReadOnly = true;
            this.gridColumn74.Width = 53;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "ID";
            this.gridColumn4.FieldName = "ID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Width = 53;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "ID";
            this.gridColumn1.FieldName = "ID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Width = 53;
            // 
            // colID1
            // 
            this.colID1.Caption = "ID";
            this.colID1.FieldName = "ID";
            this.colID1.Name = "colID1";
            this.colID1.OptionsColumn.AllowEdit = false;
            this.colID1.OptionsColumn.AllowFocus = false;
            this.colID1.OptionsColumn.ReadOnly = true;
            this.colID1.Width = 53;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation,
            this.bbiLinkedDocuments,
            this.bbiViewOnMap});
            this.barManager2.MaxItemId = 17;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiLinkedDocuments, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiViewOnMap, true)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.ImageOptions.Image")));
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.ImageOptions.Image")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bbiLinkedDocuments
            // 
            this.bbiLinkedDocuments.Caption = "Linked Documents";
            this.bbiLinkedDocuments.Id = 15;
            this.bbiLinkedDocuments.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiLinkedDocuments.ImageOptions.Image")));
            this.bbiLinkedDocuments.Name = "bbiLinkedDocuments";
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image4")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image5")));
            toolTipTitleItem3.Text = "Linked Documents - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to open the Linked Documents Manager, filtered on the current data entry" +
    " record.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiLinkedDocuments.SuperTip = superToolTip3;
            this.bbiLinkedDocuments.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLinkedDocuments_ItemClick);
            // 
            // bbiViewOnMap
            // 
            this.bbiViewOnMap.Caption = "View On Map";
            this.bbiViewOnMap.Id = 16;
            this.bbiViewOnMap.ImageOptions.ImageIndex = 10;
            this.bbiViewOnMap.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiViewOnMap.ImageOptions.LargeImage")));
            this.bbiViewOnMap.Name = "bbiViewOnMap";
            this.bbiViewOnMap.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image6")));
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image7")));
            toolTipTitleItem4.Text = "View On Map - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to View the Site Location and the Job Start and End Position on the Map." +
    "";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.bbiViewOnMap.SuperTip = superToolTip4;
            this.bbiViewOnMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiViewOnMap_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageOptions.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip5.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem5.Text = "Form Mode - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.barStaticItemFormMode.SuperTip = superToolTip5;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageOptions.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(1415, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 732);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(1415, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 706);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(1415, 26);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 706);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Preview_16x16, "Preview_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 5);
            this.imageCollection1.Images.SetKeyName(5, "Delete_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.BlockAdd_16x16, "BlockAdd_16x16", typeof(global::WoodPlan5.Properties.Resources), 6);
            this.imageCollection1.Images.SetKeyName(6, "BlockAdd_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.refresh_16x16, "refresh_16x16", typeof(global::WoodPlan5.Properties.Resources), 7);
            this.imageCollection1.Images.SetKeyName(7, "refresh_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.linked_documents_16_16, "linked_documents_16_16", typeof(global::WoodPlan5.Properties.Resources), 8);
            this.imageCollection1.Images.SetKeyName(8, "linked_documents_16_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Calculate_16x16, "Calculate_16x16", typeof(global::WoodPlan5.Properties.Resources), 9);
            this.imageCollection1.Images.SetKeyName(9, "Calculate_16x16");
            this.imageCollection1.InsertGalleryImage("geopoint_16x16.png", "images/maps/geopoint_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/maps/geopoint_16x16.png"), 10);
            this.imageCollection1.Images.SetKeyName(10, "geopoint_16x16.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataSet_HR_Core
            // 
            this.dataSet_HR_Core.DataSetName = "DataSet_HR_Core";
            this.dataSet_HR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.SuspendedRemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.SuspendedReasonIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.MandatoryCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.DisplayOrderSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ManuallyCompletedCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.VisitSellCalculationLevelTextEdit);
            this.dataLayoutControl1.Controls.Add(this.VisitCostCalculationLevelTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CalculateSellButton);
            this.dataLayoutControl1.Controls.Add(this.CalculateCostButton);
            this.dataLayoutControl1.Controls.Add(this.ClientPONumberButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.NoWorkRequiredCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.CancelledReasonIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ReworkOriginalJobIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ReworkCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.BillingCentreCodeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TenderJobIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TenderIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.gridControlMaterials);
            this.dataLayoutControl1.Controls.Add(this.gridControlEquipment);
            this.dataLayoutControl1.Controls.Add(this.LocationYTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LocationXTextEdit);
            this.dataLayoutControl1.Controls.Add(this.DaysLeewaySpineEdit);
            this.dataLayoutControl1.Controls.Add(this.MaximumDaysFromLastVisitSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.MinimumDaysFromLastVisitSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.FinishLongitudeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.FinishLatitudeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.StartLongitudeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.StartLatitudeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RouteOrderSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientPOIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AccessPermitIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.DoNotInvoiceClientCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.DoNotPayContractorCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.SelfBillingInvoiceAmountPaidSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SelfBillingInvoicePaidDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.SelfBillingInvoiceReceivedDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.SelfBillingInvoiceIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.DateClientInvoicedDateEdit);
            this.dataLayoutControl1.Controls.Add(this.SellTotalCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SellTotalCostVATSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SellTotalCostExVATSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.CostTotalCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.CostTotalCostVATSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.CostTotalCostExVATSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SellTotalLabourCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SellTotalLabourVATSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SellTotalLabourVATRateSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SellTotalLabourExVATSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.CostTotalLabourCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.CostTotalLabourVATSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.CostTotalLabourVATRateSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.CostTotalLabourExVATSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SellTotalEquipmentCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SellTotalEquipmentVATSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SellTotalEquipmentVATRateSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SellTotalEquipmentExVATSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.CostTotalEquipmentCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.CostTotalEquipmentVATSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.CostTotalEquipmentVATRateSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.CostTotalEquipmentExVATSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SellTotalMaterialCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SellTotalMaterialVATSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SellTotalMaterialVATRateSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SellTotalMaterialExVATSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.CostTotalMaterialVATRateSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.CostTotalMaterialVATSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.CostTotalMaterialCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.CostTotalMaterialExVATSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ActualEndDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.ActualDurationUnitsDescriptionIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ActualDurationUnitsSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ActualStartDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.ExpectedDurationUnitsDescriptorIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ExpectedDurationUnitsSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.FinanceSystemPONumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RequiresAccessPermitCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.JobNoLongerRequiredCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ReactiveCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.JobTypeDescriptionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.JobSubTypeDescriptionButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientNameContractDescriptionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.JobStatusIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.VisitIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.JobTypeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.JobIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ExpectedEndDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientInvoiceIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.btnSave);
            this.dataLayoutControl1.Controls.Add(this.gridSplitContainer1);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.JobSubTypeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CreatedByStaffIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.VisitNumberButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.ExpectedStartDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.DataSource = this.sp06174OMJobEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForJobID,
            this.ItemForVisitID,
            this.ItemForJobSubTypeID,
            this.ItemForJobTypeID,
            this.ItemForCreatedByStaffID,
            this.ItemForAccessPermitID,
            this.ItemForTenderID,
            this.ItemForTenderJobID,
            this.ItemForBillingCentreCodeID,
            this.ItemForReworkOriginalJobID,
            this.layoutControlGroup27,
            this.ItemForClientPOID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1247, 282, 549, 569);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1415, 706);
            this.dataLayoutControl1.TabIndex = 9;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // SuspendedRemarksMemoEdit
            // 
            this.SuspendedRemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "SuspendedRemarks", true));
            this.SuspendedRemarksMemoEdit.Location = new System.Drawing.Point(150, 303);
            this.SuspendedRemarksMemoEdit.MenuManager = this.barManager1;
            this.SuspendedRemarksMemoEdit.Name = "SuspendedRemarksMemoEdit";
            this.SuspendedRemarksMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SuspendedRemarksMemoEdit, true);
            this.SuspendedRemarksMemoEdit.Size = new System.Drawing.Size(541, 42);
            this.scSpellChecker.SetSpellCheckerOptions(this.SuspendedRemarksMemoEdit, optionsSpelling1);
            this.SuspendedRemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.SuspendedRemarksMemoEdit.TabIndex = 14;
            // 
            // sp06174OMJobEditBindingSource
            // 
            this.sp06174OMJobEditBindingSource.DataMember = "sp06174_OM_Job_Edit";
            this.sp06174OMJobEditBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // dataSet_OM_Job
            // 
            this.dataSet_OM_Job.DataSetName = "DataSet_OM_Job";
            this.dataSet_OM_Job.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // SuspendedReasonIDGridLookUpEdit
            // 
            this.SuspendedReasonIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "SuspendedReasonID", true));
            this.SuspendedReasonIDGridLookUpEdit.Location = new System.Drawing.Point(454, 279);
            this.SuspendedReasonIDGridLookUpEdit.MenuManager = this.barManager1;
            this.SuspendedReasonIDGridLookUpEdit.Name = "SuspendedReasonIDGridLookUpEdit";
            this.SuspendedReasonIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SuspendedReasonIDGridLookUpEdit.Properties.DataSource = this.sp06432OMJobSuspensionReasonsWithBlankBindingSource;
            this.SuspendedReasonIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.SuspendedReasonIDGridLookUpEdit.Properties.NullText = "";
            this.SuspendedReasonIDGridLookUpEdit.Properties.PopupView = this.gridView1;
            this.SuspendedReasonIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.SuspendedReasonIDGridLookUpEdit.Size = new System.Drawing.Size(237, 20);
            this.SuspendedReasonIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.SuspendedReasonIDGridLookUpEdit.TabIndex = 15;
            // 
            // sp06432OMJobSuspensionReasonsWithBlankBindingSource
            // 
            this.sp06432OMJobSuspensionReasonsWithBlankBindingSource.DataMember = "sp06432_OM_Job_Suspension_Reasons_With_Blank";
            this.sp06432OMJobSuspensionReasonsWithBlankBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn78,
            this.gridColumn79,
            this.gridColumn80});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.gridColumn78;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView1.FormatRules.Add(gridFormatRule1);
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn80, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn79
            // 
            this.gridColumn79.Caption = "Suspension Reason";
            this.gridColumn79.FieldName = "Description";
            this.gridColumn79.Name = "gridColumn79";
            this.gridColumn79.OptionsColumn.AllowEdit = false;
            this.gridColumn79.OptionsColumn.AllowFocus = false;
            this.gridColumn79.OptionsColumn.ReadOnly = true;
            this.gridColumn79.Visible = true;
            this.gridColumn79.VisibleIndex = 0;
            this.gridColumn79.Width = 220;
            // 
            // gridColumn80
            // 
            this.gridColumn80.Caption = "Order";
            this.gridColumn80.FieldName = "RecordOrder";
            this.gridColumn80.Name = "gridColumn80";
            this.gridColumn80.OptionsColumn.AllowEdit = false;
            this.gridColumn80.OptionsColumn.AllowFocus = false;
            this.gridColumn80.OptionsColumn.ReadOnly = true;
            // 
            // MandatoryCheckEdit
            // 
            this.MandatoryCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "Mandatory", true));
            this.MandatoryCheckEdit.Location = new System.Drawing.Point(368, 187);
            this.MandatoryCheckEdit.MenuManager = this.barManager1;
            this.MandatoryCheckEdit.Name = "MandatoryCheckEdit";
            this.MandatoryCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.MandatoryCheckEdit.Properties.ValueChecked = 1;
            this.MandatoryCheckEdit.Properties.ValueUnchecked = 0;
            this.MandatoryCheckEdit.Size = new System.Drawing.Size(76, 19);
            this.MandatoryCheckEdit.StyleController = this.dataLayoutControl1;
            this.MandatoryCheckEdit.TabIndex = 14;
            // 
            // DisplayOrderSpinEdit
            // 
            this.DisplayOrderSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "DisplayOrder", true));
            this.DisplayOrderSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.DisplayOrderSpinEdit.Location = new System.Drawing.Point(150, 141);
            this.DisplayOrderSpinEdit.MenuManager = this.barManager1;
            this.DisplayOrderSpinEdit.Name = "DisplayOrderSpinEdit";
            this.DisplayOrderSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DisplayOrderSpinEdit.Properties.IsFloatValue = false;
            this.DisplayOrderSpinEdit.Properties.Mask.EditMask = "N0";
            this.DisplayOrderSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DisplayOrderSpinEdit.Properties.MaxValue = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.DisplayOrderSpinEdit.Size = new System.Drawing.Size(174, 20);
            this.DisplayOrderSpinEdit.StyleController = this.dataLayoutControl1;
            this.DisplayOrderSpinEdit.TabIndex = 14;
            // 
            // ManuallyCompletedCheckEdit
            // 
            this.ManuallyCompletedCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "ManuallyCompleted", true));
            this.ManuallyCompletedCheckEdit.Location = new System.Drawing.Point(602, 210);
            this.ManuallyCompletedCheckEdit.MenuManager = this.barManager1;
            this.ManuallyCompletedCheckEdit.Name = "ManuallyCompletedCheckEdit";
            this.ManuallyCompletedCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.ManuallyCompletedCheckEdit.Properties.ValueChecked = 1;
            this.ManuallyCompletedCheckEdit.Properties.ValueUnchecked = 0;
            this.ManuallyCompletedCheckEdit.Size = new System.Drawing.Size(76, 19);
            this.ManuallyCompletedCheckEdit.StyleController = this.dataLayoutControl1;
            this.ManuallyCompletedCheckEdit.TabIndex = 14;
            // 
            // VisitSellCalculationLevelTextEdit
            // 
            this.VisitSellCalculationLevelTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "VisitSellCalculationLevel", true));
            this.VisitSellCalculationLevelTextEdit.Location = new System.Drawing.Point(1141, 348);
            this.VisitSellCalculationLevelTextEdit.MenuManager = this.barManager1;
            this.VisitSellCalculationLevelTextEdit.Name = "VisitSellCalculationLevelTextEdit";
            this.VisitSellCalculationLevelTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.VisitSellCalculationLevelTextEdit, true);
            this.VisitSellCalculationLevelTextEdit.Size = new System.Drawing.Size(209, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.VisitSellCalculationLevelTextEdit, optionsSpelling2);
            this.VisitSellCalculationLevelTextEdit.StyleController = this.dataLayoutControl1;
            this.VisitSellCalculationLevelTextEdit.TabIndex = 144;
            // 
            // VisitCostCalculationLevelTextEdit
            // 
            this.VisitCostCalculationLevelTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "VisitCostCalculationLevel", true));
            this.VisitCostCalculationLevelTextEdit.Location = new System.Drawing.Point(821, 348);
            this.VisitCostCalculationLevelTextEdit.MenuManager = this.barManager1;
            this.VisitCostCalculationLevelTextEdit.Name = "VisitCostCalculationLevelTextEdit";
            this.VisitCostCalculationLevelTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.VisitCostCalculationLevelTextEdit, true);
            this.VisitCostCalculationLevelTextEdit.Size = new System.Drawing.Size(200, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.VisitCostCalculationLevelTextEdit, optionsSpelling3);
            this.VisitCostCalculationLevelTextEdit.StyleController = this.dataLayoutControl1;
            this.VisitCostCalculationLevelTextEdit.TabIndex = 143;
            // 
            // CalculateSellButton
            // 
            this.CalculateSellButton.ImageOptions.ImageIndex = 9;
            this.CalculateSellButton.ImageOptions.ImageList = this.imageCollection1;
            this.CalculateSellButton.Location = new System.Drawing.Point(1324, 372);
            this.CalculateSellButton.Name = "CalculateSellButton";
            this.CalculateSellButton.Size = new System.Drawing.Size(26, 22);
            this.CalculateSellButton.StyleController = this.dataLayoutControl1;
            toolTipTitleItem6.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image8")));
            toolTipTitleItem6.Appearance.Options.UseImage = true;
            toolTipTitleItem6.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image9")));
            toolTipTitleItem6.Text = "Re-Calculate Sell Price - Information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Click me to re-calculate the Job Sell price based on the method selected in the S" +
    "ell Method box.\r\n\r\nNote: I am only enabled when the Sell Method is not Manual.";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this.CalculateSellButton.SuperTip = superToolTip6;
            this.CalculateSellButton.TabIndex = 62;
            this.CalculateSellButton.Click += new System.EventHandler(this.CalculateSellButton_Click);
            // 
            // CalculateCostButton
            // 
            this.CalculateCostButton.ImageOptions.ImageIndex = 9;
            this.CalculateCostButton.ImageOptions.ImageList = this.imageCollection1;
            this.CalculateCostButton.Location = new System.Drawing.Point(995, 372);
            this.CalculateCostButton.Name = "CalculateCostButton";
            this.CalculateCostButton.Size = new System.Drawing.Size(26, 22);
            this.CalculateCostButton.StyleController = this.dataLayoutControl1;
            toolTipTitleItem7.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image10")));
            toolTipTitleItem7.Appearance.Options.UseImage = true;
            toolTipTitleItem7.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image11")));
            toolTipTitleItem7.Text = "Re-Calculate Cost - Information";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Click me to re-calculate the Job Cost based on the method selected in the Cost Me" +
    "thod box.\r\n\r\nNote: I am only enabled when the Cost Method is not Manual.";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            this.CalculateCostButton.SuperTip = superToolTip7;
            this.CalculateCostButton.TabIndex = 61;
            this.CalculateCostButton.Click += new System.EventHandler(this.CalculateCostButton_Click);
            // 
            // ClientPONumberButtonEdit
            // 
            this.ClientPONumberButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "ClientPONumber", true));
            this.ClientPONumberButtonEdit.EditValue = "";
            this.ClientPONumberButtonEdit.Location = new System.Drawing.Point(855, 71);
            this.ClientPONumberButtonEdit.MenuManager = this.barManager1;
            this.ClientPONumberButtonEdit.Name = "ClientPONumberButtonEdit";
            this.ClientPONumberButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click me to Open Select Client Purchase Order screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Close, "Clear", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Click me to Clear the selected Client PO", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ClientPONumberButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ClientPONumberButtonEdit.Size = new System.Drawing.Size(190, 20);
            this.ClientPONumberButtonEdit.StyleController = this.dataLayoutControl1;
            this.ClientPONumberButtonEdit.TabIndex = 14;
            this.ClientPONumberButtonEdit.TabStop = false;
            this.ClientPONumberButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ClientPONumberButtonEdit_ButtonClick);
            // 
            // NoWorkRequiredCheckEdit
            // 
            this.NoWorkRequiredCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "NoWorkRequired", true));
            this.NoWorkRequiredCheckEdit.Location = new System.Drawing.Point(368, 210);
            this.NoWorkRequiredCheckEdit.MenuManager = this.barManager1;
            this.NoWorkRequiredCheckEdit.Name = "NoWorkRequiredCheckEdit";
            this.NoWorkRequiredCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.NoWorkRequiredCheckEdit.Properties.ValueChecked = 1;
            this.NoWorkRequiredCheckEdit.Properties.ValueUnchecked = 0;
            this.NoWorkRequiredCheckEdit.Size = new System.Drawing.Size(76, 19);
            this.NoWorkRequiredCheckEdit.StyleController = this.dataLayoutControl1;
            this.NoWorkRequiredCheckEdit.TabIndex = 14;
            // 
            // CancelledReasonIDGridLookUpEdit
            // 
            this.CancelledReasonIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "CancelledReasonID", true));
            this.CancelledReasonIDGridLookUpEdit.Location = new System.Drawing.Point(454, 255);
            this.CancelledReasonIDGridLookUpEdit.MenuManager = this.barManager1;
            this.CancelledReasonIDGridLookUpEdit.Name = "CancelledReasonIDGridLookUpEdit";
            this.CancelledReasonIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CancelledReasonIDGridLookUpEdit.Properties.DataSource = this.sp06268OMCancelledReasonsWithBlankBindingSource;
            this.CancelledReasonIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.CancelledReasonIDGridLookUpEdit.Properties.NullText = "";
            this.CancelledReasonIDGridLookUpEdit.Properties.PopupView = this.gridView3;
            this.CancelledReasonIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.CancelledReasonIDGridLookUpEdit.Size = new System.Drawing.Size(237, 20);
            this.CancelledReasonIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.CancelledReasonIDGridLookUpEdit.TabIndex = 14;
            // 
            // sp06268OMCancelledReasonsWithBlankBindingSource
            // 
            this.sp06268OMCancelledReasonsWithBlankBindingSource.DataMember = "sp06268_OM_Cancelled_Reasons_With_Blank";
            this.sp06268OMCancelledReasonsWithBlankBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn74,
            this.gridColumn76,
            this.gridColumn77});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule2.ApplyToRow = true;
            gridFormatRule2.Column = this.gridColumn74;
            gridFormatRule2.Name = "Format0";
            formatConditionRuleValue2.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue2.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue2.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue2.Value1 = 0;
            gridFormatRule2.Rule = formatConditionRuleValue2;
            this.gridView3.FormatRules.Add(gridFormatRule2);
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn77, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn76
            // 
            this.gridColumn76.Caption = "Cancelled Reason";
            this.gridColumn76.FieldName = "Description";
            this.gridColumn76.Name = "gridColumn76";
            this.gridColumn76.OptionsColumn.AllowEdit = false;
            this.gridColumn76.OptionsColumn.AllowFocus = false;
            this.gridColumn76.OptionsColumn.ReadOnly = true;
            this.gridColumn76.Visible = true;
            this.gridColumn76.VisibleIndex = 0;
            this.gridColumn76.Width = 220;
            // 
            // gridColumn77
            // 
            this.gridColumn77.Caption = "Order";
            this.gridColumn77.FieldName = "RecordOrder";
            this.gridColumn77.Name = "gridColumn77";
            this.gridColumn77.OptionsColumn.AllowEdit = false;
            this.gridColumn77.OptionsColumn.AllowFocus = false;
            this.gridColumn77.OptionsColumn.ReadOnly = true;
            // 
            // ReworkOriginalJobIDTextEdit
            // 
            this.ReworkOriginalJobIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "ReworkOriginalJobID", true));
            this.ReworkOriginalJobIDTextEdit.Location = new System.Drawing.Point(388, 180);
            this.ReworkOriginalJobIDTextEdit.MenuManager = this.barManager1;
            this.ReworkOriginalJobIDTextEdit.Name = "ReworkOriginalJobIDTextEdit";
            this.ReworkOriginalJobIDTextEdit.Properties.Mask.EditMask = "######0";
            this.ReworkOriginalJobIDTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.ReworkOriginalJobIDTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ReworkOriginalJobIDTextEdit, true);
            this.ReworkOriginalJobIDTextEdit.Size = new System.Drawing.Size(204, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ReworkOriginalJobIDTextEdit, optionsSpelling4);
            this.ReworkOriginalJobIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ReworkOriginalJobIDTextEdit.TabIndex = 142;
            // 
            // ReworkCheckEdit
            // 
            this.ReworkCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "Rework", true));
            this.ReworkCheckEdit.Location = new System.Drawing.Point(150, 279);
            this.ReworkCheckEdit.MenuManager = this.barManager1;
            this.ReworkCheckEdit.Name = "ReworkCheckEdit";
            this.ReworkCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.ReworkCheckEdit.Properties.ValueChecked = 1;
            this.ReworkCheckEdit.Properties.ValueUnchecked = 0;
            this.ReworkCheckEdit.Size = new System.Drawing.Size(174, 19);
            this.ReworkCheckEdit.StyleController = this.dataLayoutControl1;
            this.ReworkCheckEdit.TabIndex = 141;
            // 
            // BillingCentreCodeIDTextEdit
            // 
            this.BillingCentreCodeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "BillingCentreCodeID", true));
            this.BillingCentreCodeIDTextEdit.Location = new System.Drawing.Point(162, 391);
            this.BillingCentreCodeIDTextEdit.MenuManager = this.barManager1;
            this.BillingCentreCodeIDTextEdit.Name = "BillingCentreCodeIDTextEdit";
            this.BillingCentreCodeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.BillingCentreCodeIDTextEdit, true);
            this.BillingCentreCodeIDTextEdit.Size = new System.Drawing.Size(646, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.BillingCentreCodeIDTextEdit, optionsSpelling5);
            this.BillingCentreCodeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.BillingCentreCodeIDTextEdit.TabIndex = 140;
            // 
            // TenderJobIDTextEdit
            // 
            this.TenderJobIDTextEdit.Location = new System.Drawing.Point(170, 443);
            this.TenderJobIDTextEdit.MenuManager = this.barManager1;
            this.TenderJobIDTextEdit.Name = "TenderJobIDTextEdit";
            this.TenderJobIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.TenderJobIDTextEdit, true);
            this.TenderJobIDTextEdit.Size = new System.Drawing.Size(626, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.TenderJobIDTextEdit, optionsSpelling6);
            this.TenderJobIDTextEdit.StyleController = this.dataLayoutControl1;
            this.TenderJobIDTextEdit.TabIndex = 136;
            // 
            // TenderIDTextEdit
            // 
            this.TenderIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "TenderID", true));
            this.TenderIDTextEdit.Location = new System.Drawing.Point(158, 465);
            this.TenderIDTextEdit.MenuManager = this.barManager1;
            this.TenderIDTextEdit.Name = "TenderIDTextEdit";
            this.TenderIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.TenderIDTextEdit, true);
            this.TenderIDTextEdit.Size = new System.Drawing.Size(650, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.TenderIDTextEdit, optionsSpelling7);
            this.TenderIDTextEdit.StyleController = this.dataLayoutControl1;
            this.TenderIDTextEdit.TabIndex = 135;
            // 
            // gridControlMaterials
            // 
            this.gridControlMaterials.DataSource = this.sp06035OMJobManagerLinkedMaterialsBindingSource;
            this.gridControlMaterials.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlMaterials.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlMaterials.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlMaterials.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlMaterials.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlMaterials.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlMaterials.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlMaterials.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlMaterials.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlMaterials.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlMaterials.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlMaterials.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "View Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Reload Data", "reload"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 8, true, true, "Linked Documents", "linked_document")});
            this.gridControlMaterials.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlMaterials_EmbeddedNavigator_ButtonClick);
            this.gridControlMaterials.Location = new System.Drawing.Point(36, 832);
            this.gridControlMaterials.MainView = this.gridViewMaterials;
            this.gridControlMaterials.MenuManager = this.barManager1;
            this.gridControlMaterials.Name = "gridControlMaterials";
            this.gridControlMaterials.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3,
            this.repositoryItemTextEdit10,
            this.repositoryItemTextEdit8,
            this.repositoryItemTextEdit7,
            this.repositoryItemTextEdit9,
            this.repositoryItemTextEdit11});
            this.gridControlMaterials.Size = new System.Drawing.Size(643, 126);
            this.gridControlMaterials.TabIndex = 48;
            this.gridControlMaterials.UseEmbeddedNavigator = true;
            this.gridControlMaterials.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewMaterials});
            // 
            // sp06035OMJobManagerLinkedMaterialsBindingSource
            // 
            this.sp06035OMJobManagerLinkedMaterialsBindingSource.DataMember = "sp06035_OM_Job_Manager_Linked_Materials";
            this.sp06035OMJobManagerLinkedMaterialsBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridViewMaterials
            // 
            this.gridViewMaterials.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn39,
            this.gridColumn40,
            this.gridColumn45,
            this.gridColumn46,
            this.gridColumn47,
            this.gridColumn48,
            this.gridColumn49,
            this.gridColumn50,
            this.gridColumn51,
            this.gridColumn52,
            this.gridColumn53,
            this.gridColumn54,
            this.gridColumn55,
            this.gridColumn56,
            this.gridColumn57,
            this.gridColumn58,
            this.gridColumn59,
            this.gridColumn60,
            this.gridColumn61,
            this.gridColumn62,
            this.gridColumn63,
            this.gridColumn64,
            this.gridColumn65,
            this.gridColumn66,
            this.gridColumn67,
            this.gridColumn68,
            this.gridColumn69,
            this.gridColumn70,
            this.gridColumn71,
            this.gridColumn72,
            this.gridColumn73,
            this.gridColumn75,
            this.colVisitID});
            this.gridViewMaterials.GridControl = this.gridControlMaterials;
            this.gridViewMaterials.Name = "gridViewMaterials";
            this.gridViewMaterials.OptionsCustomization.AllowFilter = false;
            this.gridViewMaterials.OptionsFilter.AllowFilterEditor = false;
            this.gridViewMaterials.OptionsFilter.AllowMRUFilterList = false;
            this.gridViewMaterials.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewMaterials.OptionsLayout.StoreAppearance = true;
            this.gridViewMaterials.OptionsLayout.StoreFormatRules = true;
            this.gridViewMaterials.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewMaterials.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewMaterials.OptionsSelection.MultiSelect = true;
            this.gridViewMaterials.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewMaterials.OptionsView.ColumnAutoWidth = false;
            this.gridViewMaterials.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewMaterials.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewMaterials.OptionsView.ShowGroupPanel = false;
            this.gridViewMaterials.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn70, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewMaterials.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewMaterials.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewMaterials.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewMaterials.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewMaterials.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewMaterials.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewMaterials_MouseUp);
            this.gridViewMaterials.DoubleClick += new System.EventHandler(this.gridViewMaterials_DoubleClick);
            this.gridViewMaterials.GotFocus += new System.EventHandler(this.gridViewMaterials_GotFocus);
            // 
            // gridColumn39
            // 
            this.gridColumn39.Caption = "Material Used ID";
            this.gridColumn39.FieldName = "MaterialUsedID";
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.OptionsColumn.AllowEdit = false;
            this.gridColumn39.OptionsColumn.AllowFocus = false;
            this.gridColumn39.OptionsColumn.ReadOnly = true;
            this.gridColumn39.Width = 108;
            // 
            // gridColumn40
            // 
            this.gridColumn40.Caption = "Material ID";
            this.gridColumn40.FieldName = "MaterialID";
            this.gridColumn40.Name = "gridColumn40";
            this.gridColumn40.OptionsColumn.AllowEdit = false;
            this.gridColumn40.OptionsColumn.AllowFocus = false;
            this.gridColumn40.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn45
            // 
            this.gridColumn45.Caption = "Job ID";
            this.gridColumn45.FieldName = "JobID";
            this.gridColumn45.Name = "gridColumn45";
            this.gridColumn45.OptionsColumn.AllowEdit = false;
            this.gridColumn45.OptionsColumn.AllowFocus = false;
            this.gridColumn45.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn46
            // 
            this.gridColumn46.Caption = "Cost Units Used";
            this.gridColumn46.ColumnEdit = this.repositoryItemTextEdit7;
            this.gridColumn46.FieldName = "CostUnitsUsed";
            this.gridColumn46.Name = "gridColumn46";
            this.gridColumn46.OptionsColumn.AllowEdit = false;
            this.gridColumn46.OptionsColumn.AllowFocus = false;
            this.gridColumn46.OptionsColumn.ReadOnly = true;
            this.gridColumn46.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostUnitsUsed", "{0:n2}")});
            this.gridColumn46.Visible = true;
            this.gridColumn46.VisibleIndex = 2;
            this.gridColumn46.Width = 97;
            // 
            // repositoryItemTextEdit7
            // 
            this.repositoryItemTextEdit7.AutoHeight = false;
            this.repositoryItemTextEdit7.Mask.EditMask = "n2";
            this.repositoryItemTextEdit7.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit7.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit7.Name = "repositoryItemTextEdit7";
            // 
            // gridColumn47
            // 
            this.gridColumn47.Caption = "Cost Unit Descriptor ID";
            this.gridColumn47.FieldName = "CostUnitDescriptorID";
            this.gridColumn47.Name = "gridColumn47";
            this.gridColumn47.OptionsColumn.AllowEdit = false;
            this.gridColumn47.OptionsColumn.AllowFocus = false;
            this.gridColumn47.OptionsColumn.ReadOnly = true;
            this.gridColumn47.Width = 131;
            // 
            // gridColumn48
            // 
            this.gridColumn48.Caption = "Sell Units Used";
            this.gridColumn48.ColumnEdit = this.repositoryItemTextEdit7;
            this.gridColumn48.FieldName = "SellUnitsUsed";
            this.gridColumn48.Name = "gridColumn48";
            this.gridColumn48.OptionsColumn.AllowEdit = false;
            this.gridColumn48.OptionsColumn.AllowFocus = false;
            this.gridColumn48.OptionsColumn.ReadOnly = true;
            this.gridColumn48.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellUnitsUsed", "{0:n2}")});
            this.gridColumn48.Visible = true;
            this.gridColumn48.VisibleIndex = 8;
            this.gridColumn48.Width = 91;
            // 
            // gridColumn49
            // 
            this.gridColumn49.Caption = "Sell Unit Descriptor ID";
            this.gridColumn49.FieldName = "SellUnitDescriptorID";
            this.gridColumn49.Name = "gridColumn49";
            this.gridColumn49.OptionsColumn.AllowEdit = false;
            this.gridColumn49.OptionsColumn.AllowFocus = false;
            this.gridColumn49.OptionsColumn.ReadOnly = true;
            this.gridColumn49.Width = 125;
            // 
            // gridColumn50
            // 
            this.gridColumn50.Caption = "Cost Per Unit Ex VAT";
            this.gridColumn50.ColumnEdit = this.repositoryItemTextEdit8;
            this.gridColumn50.FieldName = "CostPerUnitExVat";
            this.gridColumn50.Name = "gridColumn50";
            this.gridColumn50.OptionsColumn.AllowEdit = false;
            this.gridColumn50.OptionsColumn.AllowFocus = false;
            this.gridColumn50.OptionsColumn.ReadOnly = true;
            this.gridColumn50.Visible = true;
            this.gridColumn50.VisibleIndex = 3;
            this.gridColumn50.Width = 121;
            // 
            // repositoryItemTextEdit8
            // 
            this.repositoryItemTextEdit8.AutoHeight = false;
            this.repositoryItemTextEdit8.Mask.EditMask = "c";
            this.repositoryItemTextEdit8.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit8.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit8.Name = "repositoryItemTextEdit8";
            // 
            // gridColumn51
            // 
            this.gridColumn51.Caption = "Cost VAT Rate";
            this.gridColumn51.ColumnEdit = this.repositoryItemTextEdit9;
            this.gridColumn51.FieldName = "CostPerUnitVatRate";
            this.gridColumn51.Name = "gridColumn51";
            this.gridColumn51.OptionsColumn.AllowEdit = false;
            this.gridColumn51.OptionsColumn.AllowFocus = false;
            this.gridColumn51.OptionsColumn.ReadOnly = true;
            this.gridColumn51.Visible = true;
            this.gridColumn51.VisibleIndex = 4;
            this.gridColumn51.Width = 91;
            // 
            // repositoryItemTextEdit9
            // 
            this.repositoryItemTextEdit9.AutoHeight = false;
            this.repositoryItemTextEdit9.Mask.EditMask = "P";
            this.repositoryItemTextEdit9.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit9.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit9.Name = "repositoryItemTextEdit9";
            // 
            // gridColumn52
            // 
            this.gridColumn52.Caption = "Sell Per Unit Ex VAT";
            this.gridColumn52.ColumnEdit = this.repositoryItemTextEdit8;
            this.gridColumn52.FieldName = "SellPerUnitExVat";
            this.gridColumn52.Name = "gridColumn52";
            this.gridColumn52.OptionsColumn.AllowEdit = false;
            this.gridColumn52.OptionsColumn.AllowFocus = false;
            this.gridColumn52.OptionsColumn.ReadOnly = true;
            this.gridColumn52.Visible = true;
            this.gridColumn52.VisibleIndex = 9;
            this.gridColumn52.Width = 115;
            // 
            // gridColumn53
            // 
            this.gridColumn53.Caption = "Sell VAT Rate";
            this.gridColumn53.ColumnEdit = this.repositoryItemTextEdit9;
            this.gridColumn53.FieldName = "SellPerUnitVatRate";
            this.gridColumn53.Name = "gridColumn53";
            this.gridColumn53.OptionsColumn.AllowEdit = false;
            this.gridColumn53.OptionsColumn.AllowFocus = false;
            this.gridColumn53.OptionsColumn.ReadOnly = true;
            this.gridColumn53.Visible = true;
            this.gridColumn53.VisibleIndex = 10;
            this.gridColumn53.Width = 85;
            // 
            // gridColumn54
            // 
            this.gridColumn54.Caption = "Cost Ex VAT";
            this.gridColumn54.ColumnEdit = this.repositoryItemTextEdit8;
            this.gridColumn54.FieldName = "CostValueExVat";
            this.gridColumn54.Name = "gridColumn54";
            this.gridColumn54.OptionsColumn.AllowEdit = false;
            this.gridColumn54.OptionsColumn.AllowFocus = false;
            this.gridColumn54.OptionsColumn.ReadOnly = true;
            this.gridColumn54.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostValueExVat", "{0:c}")});
            this.gridColumn54.Visible = true;
            this.gridColumn54.VisibleIndex = 5;
            this.gridColumn54.Width = 80;
            // 
            // gridColumn55
            // 
            this.gridColumn55.Caption = "Cost VAT";
            this.gridColumn55.ColumnEdit = this.repositoryItemTextEdit8;
            this.gridColumn55.FieldName = "CostValueVat";
            this.gridColumn55.Name = "gridColumn55";
            this.gridColumn55.OptionsColumn.AllowEdit = false;
            this.gridColumn55.OptionsColumn.AllowFocus = false;
            this.gridColumn55.OptionsColumn.ReadOnly = true;
            this.gridColumn55.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostValueVat", "{0:c}")});
            this.gridColumn55.Visible = true;
            this.gridColumn55.VisibleIndex = 6;
            // 
            // gridColumn56
            // 
            this.gridColumn56.Caption = "Cost Total";
            this.gridColumn56.ColumnEdit = this.repositoryItemTextEdit8;
            this.gridColumn56.FieldName = "CostValueTotal";
            this.gridColumn56.Name = "gridColumn56";
            this.gridColumn56.OptionsColumn.AllowEdit = false;
            this.gridColumn56.OptionsColumn.AllowFocus = false;
            this.gridColumn56.OptionsColumn.ReadOnly = true;
            this.gridColumn56.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostValueTotal", "{0:c}")});
            this.gridColumn56.Visible = true;
            this.gridColumn56.VisibleIndex = 7;
            // 
            // gridColumn57
            // 
            this.gridColumn57.Caption = "Sell Ex VAT";
            this.gridColumn57.ColumnEdit = this.repositoryItemTextEdit8;
            this.gridColumn57.FieldName = "SellValueExVat";
            this.gridColumn57.Name = "gridColumn57";
            this.gridColumn57.OptionsColumn.AllowEdit = false;
            this.gridColumn57.OptionsColumn.AllowFocus = false;
            this.gridColumn57.OptionsColumn.ReadOnly = true;
            this.gridColumn57.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellValueExVat", "{0:c}")});
            this.gridColumn57.Visible = true;
            this.gridColumn57.VisibleIndex = 11;
            // 
            // gridColumn58
            // 
            this.gridColumn58.Caption = "Sell VAT";
            this.gridColumn58.ColumnEdit = this.repositoryItemTextEdit8;
            this.gridColumn58.FieldName = "SellValueVat";
            this.gridColumn58.Name = "gridColumn58";
            this.gridColumn58.OptionsColumn.AllowEdit = false;
            this.gridColumn58.OptionsColumn.AllowFocus = false;
            this.gridColumn58.OptionsColumn.ReadOnly = true;
            this.gridColumn58.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellValueVat", "{0:c}")});
            this.gridColumn58.Visible = true;
            this.gridColumn58.VisibleIndex = 12;
            // 
            // gridColumn59
            // 
            this.gridColumn59.Caption = "Sell Total";
            this.gridColumn59.ColumnEdit = this.repositoryItemTextEdit8;
            this.gridColumn59.FieldName = "SellValueTotal";
            this.gridColumn59.Name = "gridColumn59";
            this.gridColumn59.OptionsColumn.AllowEdit = false;
            this.gridColumn59.OptionsColumn.AllowFocus = false;
            this.gridColumn59.OptionsColumn.ReadOnly = true;
            this.gridColumn59.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellValueTotal", "{0:c}")});
            this.gridColumn59.Visible = true;
            this.gridColumn59.VisibleIndex = 13;
            // 
            // gridColumn60
            // 
            this.gridColumn60.Caption = "Remarks";
            this.gridColumn60.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.gridColumn60.FieldName = "Remarks";
            this.gridColumn60.Name = "gridColumn60";
            this.gridColumn60.OptionsColumn.ReadOnly = true;
            this.gridColumn60.Visible = true;
            this.gridColumn60.VisibleIndex = 14;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // gridColumn61
            // 
            this.gridColumn61.Caption = "Client ID";
            this.gridColumn61.FieldName = "ClientID";
            this.gridColumn61.Name = "gridColumn61";
            this.gridColumn61.OptionsColumn.AllowEdit = false;
            this.gridColumn61.OptionsColumn.AllowFocus = false;
            this.gridColumn61.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn62
            // 
            this.gridColumn62.Caption = "Site ID";
            this.gridColumn62.FieldName = "SiteID";
            this.gridColumn62.Name = "gridColumn62";
            this.gridColumn62.OptionsColumn.AllowEdit = false;
            this.gridColumn62.OptionsColumn.AllowFocus = false;
            this.gridColumn62.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn63
            // 
            this.gridColumn63.Caption = "Site Name";
            this.gridColumn63.FieldName = "SiteName";
            this.gridColumn63.Name = "gridColumn63";
            this.gridColumn63.OptionsColumn.AllowEdit = false;
            this.gridColumn63.OptionsColumn.AllowFocus = false;
            this.gridColumn63.OptionsColumn.ReadOnly = true;
            this.gridColumn63.Width = 133;
            // 
            // gridColumn64
            // 
            this.gridColumn64.Caption = "Client Name";
            this.gridColumn64.FieldName = "ClientName";
            this.gridColumn64.Name = "gridColumn64";
            this.gridColumn64.OptionsColumn.AllowEdit = false;
            this.gridColumn64.OptionsColumn.AllowFocus = false;
            this.gridColumn64.OptionsColumn.ReadOnly = true;
            this.gridColumn64.Width = 139;
            // 
            // gridColumn65
            // 
            this.gridColumn65.Caption = "Job Sub-Type";
            this.gridColumn65.FieldName = "JobSubTypeDescription";
            this.gridColumn65.Name = "gridColumn65";
            this.gridColumn65.OptionsColumn.AllowEdit = false;
            this.gridColumn65.OptionsColumn.AllowFocus = false;
            this.gridColumn65.OptionsColumn.ReadOnly = true;
            this.gridColumn65.Width = 150;
            // 
            // gridColumn66
            // 
            this.gridColumn66.Caption = "Job Type";
            this.gridColumn66.FieldName = "JobTypeDescription";
            this.gridColumn66.Name = "gridColumn66";
            this.gridColumn66.OptionsColumn.AllowEdit = false;
            this.gridColumn66.OptionsColumn.AllowFocus = false;
            this.gridColumn66.OptionsColumn.ReadOnly = true;
            this.gridColumn66.Width = 169;
            // 
            // gridColumn67
            // 
            this.gridColumn67.Caption = "Expected Start Date";
            this.gridColumn67.ColumnEdit = this.repositoryItemTextEdit10;
            this.gridColumn67.FieldName = "ExpectedStartDate";
            this.gridColumn67.Name = "gridColumn67";
            this.gridColumn67.OptionsColumn.AllowEdit = false;
            this.gridColumn67.OptionsColumn.AllowFocus = false;
            this.gridColumn67.OptionsColumn.ReadOnly = true;
            this.gridColumn67.Visible = true;
            this.gridColumn67.VisibleIndex = 1;
            this.gridColumn67.Width = 119;
            // 
            // repositoryItemTextEdit10
            // 
            this.repositoryItemTextEdit10.AutoHeight = false;
            this.repositoryItemTextEdit10.Mask.EditMask = "d";
            this.repositoryItemTextEdit10.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit10.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit10.Name = "repositoryItemTextEdit10";
            // 
            // gridColumn68
            // 
            this.gridColumn68.Caption = "Visit #";
            this.gridColumn68.ColumnEdit = this.repositoryItemTextEdit11;
            this.gridColumn68.FieldName = "VisitNumber";
            this.gridColumn68.Name = "gridColumn68";
            this.gridColumn68.OptionsColumn.AllowEdit = false;
            this.gridColumn68.OptionsColumn.AllowFocus = false;
            this.gridColumn68.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemTextEdit11
            // 
            this.repositoryItemTextEdit11.AutoHeight = false;
            this.repositoryItemTextEdit11.Mask.EditMask = "n0";
            this.repositoryItemTextEdit11.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit11.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit11.Name = "repositoryItemTextEdit11";
            // 
            // gridColumn69
            // 
            this.gridColumn69.Caption = "Linked To Job";
            this.gridColumn69.FieldName = "FullDescription";
            this.gridColumn69.Name = "gridColumn69";
            this.gridColumn69.OptionsColumn.AllowEdit = false;
            this.gridColumn69.OptionsColumn.AllowFocus = false;
            this.gridColumn69.OptionsColumn.ReadOnly = true;
            this.gridColumn69.Width = 415;
            // 
            // gridColumn70
            // 
            this.gridColumn70.Caption = "Material Type";
            this.gridColumn70.FieldName = "MaterialName";
            this.gridColumn70.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn70.Name = "gridColumn70";
            this.gridColumn70.OptionsColumn.AllowEdit = false;
            this.gridColumn70.OptionsColumn.AllowFocus = false;
            this.gridColumn70.OptionsColumn.ReadOnly = true;
            this.gridColumn70.Visible = true;
            this.gridColumn70.VisibleIndex = 0;
            this.gridColumn70.Width = 181;
            // 
            // gridColumn71
            // 
            this.gridColumn71.Caption = "Selected";
            this.gridColumn71.FieldName = "Selected";
            this.gridColumn71.Name = "gridColumn71";
            this.gridColumn71.OptionsColumn.AllowEdit = false;
            this.gridColumn71.OptionsColumn.AllowFocus = false;
            this.gridColumn71.OptionsColumn.ReadOnly = true;
            this.gridColumn71.Width = 62;
            // 
            // gridColumn72
            // 
            this.gridColumn72.Caption = "Cost Unit Descriptor";
            this.gridColumn72.FieldName = "CostUnitDescriptor";
            this.gridColumn72.Name = "gridColumn72";
            this.gridColumn72.OptionsColumn.AllowEdit = false;
            this.gridColumn72.OptionsColumn.AllowFocus = false;
            this.gridColumn72.OptionsColumn.ReadOnly = true;
            this.gridColumn72.Width = 117;
            // 
            // gridColumn73
            // 
            this.gridColumn73.Caption = "Sell Unit Descriptor";
            this.gridColumn73.FieldName = "SellUnitDescriptor";
            this.gridColumn73.Name = "gridColumn73";
            this.gridColumn73.OptionsColumn.AllowEdit = false;
            this.gridColumn73.OptionsColumn.AllowFocus = false;
            this.gridColumn73.OptionsColumn.ReadOnly = true;
            this.gridColumn73.Width = 111;
            // 
            // gridColumn75
            // 
            this.gridColumn75.Caption = "PO ID";
            this.gridColumn75.FieldName = "PurchaseOrderID";
            this.gridColumn75.Name = "gridColumn75";
            this.gridColumn75.OptionsColumn.AllowEdit = false;
            this.gridColumn75.OptionsColumn.AllowFocus = false;
            this.gridColumn75.OptionsColumn.ReadOnly = true;
            this.gridColumn75.Width = 49;
            // 
            // colVisitID
            // 
            this.colVisitID.Caption = "Visit ID";
            this.colVisitID.FieldName = "VisitID";
            this.colVisitID.Name = "colVisitID";
            this.colVisitID.OptionsColumn.AllowEdit = false;
            this.colVisitID.OptionsColumn.AllowFocus = false;
            this.colVisitID.OptionsColumn.ReadOnly = true;
            // 
            // gridControlEquipment
            // 
            this.gridControlEquipment.DataSource = this.sp06034OMJobManagerLinkedEquipmentBindingSource;
            this.gridControlEquipment.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlEquipment.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlEquipment.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlEquipment.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlEquipment.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlEquipment.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlEquipment.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlEquipment.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlEquipment.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlEquipment.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlEquipment.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlEquipment.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "View Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Reload Data", "reload"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 8, true, true, "Linked Documents", "linked_document")});
            this.gridControlEquipment.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlEquipment_EmbeddedNavigator_ButtonClick);
            this.gridControlEquipment.Location = new System.Drawing.Point(36, 832);
            this.gridControlEquipment.MainView = this.gridViewEquipment;
            this.gridControlEquipment.MenuManager = this.barManager1;
            this.gridControlEquipment.Name = "gridControlEquipment";
            this.gridControlEquipment.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2,
            this.repositoryItemTextEdit5,
            this.repositoryItemTextEdit2,
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit4,
            this.repositoryItemTextEdit6});
            this.gridControlEquipment.Size = new System.Drawing.Size(643, 126);
            this.gridControlEquipment.TabIndex = 47;
            this.gridControlEquipment.UseEmbeddedNavigator = true;
            this.gridControlEquipment.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewEquipment});
            // 
            // sp06034OMJobManagerLinkedEquipmentBindingSource
            // 
            this.sp06034OMJobManagerLinkedEquipmentBindingSource.DataMember = "sp06034_OM_Job_Manager_Linked_Equipment";
            this.sp06034OMJobManagerLinkedEquipmentBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridViewEquipment
            // 
            this.gridViewEquipment.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27,
            this.gridColumn28,
            this.gridColumn29,
            this.gridColumn30,
            this.gridColumn31,
            this.gridColumn32,
            this.gridColumn33,
            this.gridColumn34,
            this.gridColumn35,
            this.gridColumn36,
            this.gridColumn37,
            this.gridColumn38,
            this.gridColumn41,
            this.gridColumn42,
            this.gridColumn43,
            this.gridColumn44,
            this.colOwnerName});
            this.gridViewEquipment.GridControl = this.gridControlEquipment;
            this.gridViewEquipment.Name = "gridViewEquipment";
            this.gridViewEquipment.OptionsCustomization.AllowFilter = false;
            this.gridViewEquipment.OptionsFilter.AllowFilterEditor = false;
            this.gridViewEquipment.OptionsFilter.AllowMRUFilterList = false;
            this.gridViewEquipment.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewEquipment.OptionsLayout.StoreAppearance = true;
            this.gridViewEquipment.OptionsLayout.StoreFormatRules = true;
            this.gridViewEquipment.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewEquipment.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewEquipment.OptionsSelection.MultiSelect = true;
            this.gridViewEquipment.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewEquipment.OptionsView.ColumnAutoWidth = false;
            this.gridViewEquipment.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewEquipment.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewEquipment.OptionsView.ShowGroupPanel = false;
            this.gridViewEquipment.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn34, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewEquipment.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewEquipment.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewEquipment.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewEquipment.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewEquipment.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewEquipment.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewEquipment_MouseUp);
            this.gridViewEquipment.DoubleClick += new System.EventHandler(this.gridViewEquipment_DoubleClick);
            this.gridViewEquipment.GotFocus += new System.EventHandler(this.gridViewEquipment_GotFocus);
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Equipment Used ID";
            this.gridColumn7.FieldName = "EquipmentUsedID";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Width = 108;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Equipment ID";
            this.gridColumn8.FieldName = "EquipmentID";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Job ID";
            this.gridColumn9.FieldName = "JobID";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Cost Units Used";
            this.gridColumn10.ColumnEdit = this.repositoryItemTextEdit1;
            this.gridColumn10.FieldName = "CostUnitsUsed";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostUnitsUsed", "{0:n2}")});
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 6;
            this.gridColumn10.Width = 97;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "n2";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Cost Unit Descriptor ID";
            this.gridColumn11.FieldName = "CostUnitDescriptorID";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Width = 131;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Sell Units Used";
            this.gridColumn12.ColumnEdit = this.repositoryItemTextEdit1;
            this.gridColumn12.FieldName = "SellUnitsUsed";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellUnitsUsed", "{0:n2}")});
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 12;
            this.gridColumn12.Width = 91;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Sell Unit Descriptor ID";
            this.gridColumn13.FieldName = "SellUnitDescriptorID";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Width = 125;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Cost Per Unit Ex VAT";
            this.gridColumn14.ColumnEdit = this.repositoryItemTextEdit2;
            this.gridColumn14.FieldName = "CostPerUnitExVat";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 7;
            this.gridColumn14.Width = 121;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Mask.EditMask = "c";
            this.repositoryItemTextEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Cost VAT Rate";
            this.gridColumn15.ColumnEdit = this.repositoryItemTextEdit4;
            this.gridColumn15.FieldName = "CostPerUnitVatRate";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 8;
            this.gridColumn15.Width = 91;
            // 
            // repositoryItemTextEdit4
            // 
            this.repositoryItemTextEdit4.AutoHeight = false;
            this.repositoryItemTextEdit4.Mask.EditMask = "P";
            this.repositoryItemTextEdit4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit4.Name = "repositoryItemTextEdit4";
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Sell Per Unit Ex VAT";
            this.gridColumn16.ColumnEdit = this.repositoryItemTextEdit2;
            this.gridColumn16.FieldName = "SellPerUnitExVat";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowFocus = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 13;
            this.gridColumn16.Width = 115;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Sell VAT Rate";
            this.gridColumn17.ColumnEdit = this.repositoryItemTextEdit4;
            this.gridColumn17.FieldName = "SellPerUnitVatRate";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.AllowFocus = false;
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 14;
            this.gridColumn17.Width = 85;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Cost Ex VAT";
            this.gridColumn18.ColumnEdit = this.repositoryItemTextEdit2;
            this.gridColumn18.FieldName = "CostValueExVat";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.AllowFocus = false;
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            this.gridColumn18.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostValueExVat", "{0:c}")});
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 9;
            this.gridColumn18.Width = 80;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Cost VAT";
            this.gridColumn19.ColumnEdit = this.repositoryItemTextEdit2;
            this.gridColumn19.FieldName = "CostValueVat";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.OptionsColumn.AllowFocus = false;
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            this.gridColumn19.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostValueVat", "{0:c}")});
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 10;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Cost Total";
            this.gridColumn20.ColumnEdit = this.repositoryItemTextEdit2;
            this.gridColumn20.FieldName = "CostValueTotal";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.AllowEdit = false;
            this.gridColumn20.OptionsColumn.AllowFocus = false;
            this.gridColumn20.OptionsColumn.ReadOnly = true;
            this.gridColumn20.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostValueTotal", "{0:c}")});
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 11;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Sell Ex VAT";
            this.gridColumn21.ColumnEdit = this.repositoryItemTextEdit2;
            this.gridColumn21.FieldName = "SellValueExVat";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.AllowEdit = false;
            this.gridColumn21.OptionsColumn.AllowFocus = false;
            this.gridColumn21.OptionsColumn.ReadOnly = true;
            this.gridColumn21.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellValueExVat", "{0:c}")});
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 15;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Sell VAT";
            this.gridColumn22.ColumnEdit = this.repositoryItemTextEdit2;
            this.gridColumn22.FieldName = "SellValueVat";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.AllowEdit = false;
            this.gridColumn22.OptionsColumn.AllowFocus = false;
            this.gridColumn22.OptionsColumn.ReadOnly = true;
            this.gridColumn22.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellValueVat", "{0:c}")});
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 16;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Sell Total";
            this.gridColumn23.ColumnEdit = this.repositoryItemTextEdit2;
            this.gridColumn23.FieldName = "SellValueTotal";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.AllowEdit = false;
            this.gridColumn23.OptionsColumn.AllowFocus = false;
            this.gridColumn23.OptionsColumn.ReadOnly = true;
            this.gridColumn23.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellValueTotal", "{0:c}")});
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 17;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Remarks";
            this.gridColumn24.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.gridColumn24.FieldName = "Remarks";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.ReadOnly = true;
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 18;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Client ID";
            this.gridColumn25.FieldName = "ClientID";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.OptionsColumn.AllowEdit = false;
            this.gridColumn25.OptionsColumn.AllowFocus = false;
            this.gridColumn25.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Site ID";
            this.gridColumn26.FieldName = "SiteID";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.OptionsColumn.AllowEdit = false;
            this.gridColumn26.OptionsColumn.AllowFocus = false;
            this.gridColumn26.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Site Name";
            this.gridColumn27.FieldName = "SiteName";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.OptionsColumn.AllowEdit = false;
            this.gridColumn27.OptionsColumn.AllowFocus = false;
            this.gridColumn27.OptionsColumn.ReadOnly = true;
            this.gridColumn27.Width = 133;
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "Client Name";
            this.gridColumn28.FieldName = "ClientName";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.OptionsColumn.AllowEdit = false;
            this.gridColumn28.OptionsColumn.AllowFocus = false;
            this.gridColumn28.OptionsColumn.ReadOnly = true;
            this.gridColumn28.Width = 139;
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "Job Sub-Type";
            this.gridColumn29.FieldName = "JobSubTypeDescription";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.OptionsColumn.AllowEdit = false;
            this.gridColumn29.OptionsColumn.AllowFocus = false;
            this.gridColumn29.OptionsColumn.ReadOnly = true;
            this.gridColumn29.Width = 150;
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "Job Type";
            this.gridColumn30.FieldName = "JobTypeDescription";
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.OptionsColumn.AllowEdit = false;
            this.gridColumn30.OptionsColumn.AllowFocus = false;
            this.gridColumn30.OptionsColumn.ReadOnly = true;
            this.gridColumn30.Width = 169;
            // 
            // gridColumn31
            // 
            this.gridColumn31.Caption = "Expected Start Date";
            this.gridColumn31.ColumnEdit = this.repositoryItemTextEdit5;
            this.gridColumn31.FieldName = "ExpectedStartDate";
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.OptionsColumn.AllowEdit = false;
            this.gridColumn31.OptionsColumn.AllowFocus = false;
            this.gridColumn31.OptionsColumn.ReadOnly = true;
            this.gridColumn31.Visible = true;
            this.gridColumn31.VisibleIndex = 3;
            this.gridColumn31.Width = 119;
            // 
            // repositoryItemTextEdit5
            // 
            this.repositoryItemTextEdit5.AutoHeight = false;
            this.repositoryItemTextEdit5.Mask.EditMask = "d";
            this.repositoryItemTextEdit5.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit5.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit5.Name = "repositoryItemTextEdit5";
            // 
            // gridColumn32
            // 
            this.gridColumn32.Caption = "Visit #";
            this.gridColumn32.ColumnEdit = this.repositoryItemTextEdit6;
            this.gridColumn32.FieldName = "VisitNumber";
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.OptionsColumn.AllowEdit = false;
            this.gridColumn32.OptionsColumn.AllowFocus = false;
            this.gridColumn32.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemTextEdit6
            // 
            this.repositoryItemTextEdit6.AutoHeight = false;
            this.repositoryItemTextEdit6.Mask.EditMask = "n0";
            this.repositoryItemTextEdit6.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit6.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit6.Name = "repositoryItemTextEdit6";
            // 
            // gridColumn33
            // 
            this.gridColumn33.Caption = "Linked To Job";
            this.gridColumn33.FieldName = "FullDescription";
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.OptionsColumn.AllowEdit = false;
            this.gridColumn33.OptionsColumn.AllowFocus = false;
            this.gridColumn33.OptionsColumn.ReadOnly = true;
            this.gridColumn33.Width = 415;
            // 
            // gridColumn34
            // 
            this.gridColumn34.Caption = "Equipment Type";
            this.gridColumn34.FieldName = "EquipmentType";
            this.gridColumn34.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn34.Name = "gridColumn34";
            this.gridColumn34.OptionsColumn.AllowEdit = false;
            this.gridColumn34.OptionsColumn.AllowFocus = false;
            this.gridColumn34.OptionsColumn.ReadOnly = true;
            this.gridColumn34.Visible = true;
            this.gridColumn34.VisibleIndex = 0;
            this.gridColumn34.Width = 181;
            // 
            // gridColumn35
            // 
            this.gridColumn35.Caption = "Selected";
            this.gridColumn35.FieldName = "Selected";
            this.gridColumn35.Name = "gridColumn35";
            this.gridColumn35.OptionsColumn.AllowEdit = false;
            this.gridColumn35.OptionsColumn.AllowFocus = false;
            this.gridColumn35.OptionsColumn.ReadOnly = true;
            this.gridColumn35.Width = 62;
            // 
            // gridColumn36
            // 
            this.gridColumn36.Caption = "Cost Unit Descriptor";
            this.gridColumn36.FieldName = "CostUnitDescriptor";
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.OptionsColumn.AllowEdit = false;
            this.gridColumn36.OptionsColumn.AllowFocus = false;
            this.gridColumn36.OptionsColumn.ReadOnly = true;
            this.gridColumn36.Width = 117;
            // 
            // gridColumn37
            // 
            this.gridColumn37.Caption = "Sell Unit Descriptor";
            this.gridColumn37.FieldName = "SellUnitDescriptor";
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.OptionsColumn.AllowEdit = false;
            this.gridColumn37.OptionsColumn.AllowFocus = false;
            this.gridColumn37.OptionsColumn.ReadOnly = true;
            this.gridColumn37.Width = 111;
            // 
            // gridColumn38
            // 
            this.gridColumn38.Caption = "Owner Type";
            this.gridColumn38.FieldName = "OwnerType";
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.OptionsColumn.AllowEdit = false;
            this.gridColumn38.OptionsColumn.AllowFocus = false;
            this.gridColumn38.OptionsColumn.ReadOnly = true;
            this.gridColumn38.Visible = true;
            this.gridColumn38.VisibleIndex = 2;
            this.gridColumn38.Width = 109;
            // 
            // gridColumn41
            // 
            this.gridColumn41.Caption = "PO ID";
            this.gridColumn41.FieldName = "PurchaseOrderID";
            this.gridColumn41.Name = "gridColumn41";
            this.gridColumn41.OptionsColumn.AllowEdit = false;
            this.gridColumn41.OptionsColumn.AllowFocus = false;
            this.gridColumn41.OptionsColumn.ReadOnly = true;
            this.gridColumn41.Width = 49;
            // 
            // gridColumn42
            // 
            this.gridColumn42.Caption = "Start Date\\Time";
            this.gridColumn42.ColumnEdit = this.repositoryItemTextEdit5;
            this.gridColumn42.FieldName = "StartDateTime";
            this.gridColumn42.Name = "gridColumn42";
            this.gridColumn42.OptionsColumn.AllowEdit = false;
            this.gridColumn42.OptionsColumn.AllowFocus = false;
            this.gridColumn42.OptionsColumn.ReadOnly = true;
            this.gridColumn42.Visible = true;
            this.gridColumn42.VisibleIndex = 4;
            this.gridColumn42.Width = 97;
            // 
            // gridColumn43
            // 
            this.gridColumn43.Caption = "End Date\\Time";
            this.gridColumn43.ColumnEdit = this.repositoryItemTextEdit5;
            this.gridColumn43.FieldName = "EndDateTime";
            this.gridColumn43.Name = "gridColumn43";
            this.gridColumn43.OptionsColumn.AllowEdit = false;
            this.gridColumn43.OptionsColumn.AllowFocus = false;
            this.gridColumn43.OptionsColumn.ReadOnly = true;
            this.gridColumn43.Visible = true;
            this.gridColumn43.VisibleIndex = 5;
            this.gridColumn43.Width = 91;
            // 
            // gridColumn44
            // 
            this.gridColumn44.Caption = "Visit ID";
            this.gridColumn44.FieldName = "VisitID";
            this.gridColumn44.Name = "gridColumn44";
            this.gridColumn44.OptionsColumn.AllowEdit = false;
            this.gridColumn44.OptionsColumn.AllowFocus = false;
            this.gridColumn44.OptionsColumn.ReadOnly = true;
            this.gridColumn44.Width = 54;
            // 
            // colOwnerName
            // 
            this.colOwnerName.Caption = "Owner Name";
            this.colOwnerName.FieldName = "OwnerName";
            this.colOwnerName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colOwnerName.Name = "colOwnerName";
            this.colOwnerName.OptionsColumn.AllowEdit = false;
            this.colOwnerName.OptionsColumn.AllowFocus = false;
            this.colOwnerName.OptionsColumn.ReadOnly = true;
            this.colOwnerName.Visible = true;
            this.colOwnerName.VisibleIndex = 1;
            this.colOwnerName.Width = 124;
            // 
            // LocationYTextEdit
            // 
            this.LocationYTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "LocationY", true));
            this.LocationYTextEdit.Location = new System.Drawing.Point(111, 545);
            this.LocationYTextEdit.MenuManager = this.barManager1;
            this.LocationYTextEdit.Name = "LocationYTextEdit";
            this.LocationYTextEdit.Properties.Mask.EditMask = "n8";
            this.LocationYTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.LocationYTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.LocationYTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LocationYTextEdit, true);
            this.LocationYTextEdit.Size = new System.Drawing.Size(101, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LocationYTextEdit, optionsSpelling8);
            this.LocationYTextEdit.StyleController = this.dataLayoutControl1;
            this.LocationYTextEdit.TabIndex = 16;
            this.LocationYTextEdit.TabStop = false;
            // 
            // LocationXTextEdit
            // 
            this.LocationXTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "LocationX", true));
            this.LocationXTextEdit.Location = new System.Drawing.Point(111, 521);
            this.LocationXTextEdit.MenuManager = this.barManager1;
            this.LocationXTextEdit.Name = "LocationXTextEdit";
            this.LocationXTextEdit.Properties.Mask.EditMask = "n8";
            this.LocationXTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.LocationXTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.LocationXTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LocationXTextEdit, true);
            this.LocationXTextEdit.Size = new System.Drawing.Size(101, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LocationXTextEdit, optionsSpelling9);
            this.LocationXTextEdit.StyleController = this.dataLayoutControl1;
            this.LocationXTextEdit.TabIndex = 15;
            this.LocationXTextEdit.TabStop = false;
            // 
            // DaysLeewaySpineEdit
            // 
            this.DaysLeewaySpineEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "DaysLeeway", true));
            this.DaysLeewaySpineEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.DaysLeewaySpineEdit.Location = new System.Drawing.Point(162, 441);
            this.DaysLeewaySpineEdit.MenuManager = this.barManager1;
            this.DaysLeewaySpineEdit.Name = "DaysLeewaySpineEdit";
            this.DaysLeewaySpineEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DaysLeewaySpineEdit.Properties.IsFloatValue = false;
            this.DaysLeewaySpineEdit.Properties.Mask.EditMask = "N00";
            this.DaysLeewaySpineEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DaysLeewaySpineEdit.Properties.MaxValue = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.DaysLeewaySpineEdit.Size = new System.Drawing.Size(50, 20);
            this.DaysLeewaySpineEdit.StyleController = this.dataLayoutControl1;
            this.DaysLeewaySpineEdit.TabIndex = 11;
            // 
            // MaximumDaysFromLastVisitSpinEdit
            // 
            this.MaximumDaysFromLastVisitSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "MaximumDaysFromLastVisit", true));
            this.MaximumDaysFromLastVisitSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.MaximumDaysFromLastVisitSpinEdit.Location = new System.Drawing.Point(162, 417);
            this.MaximumDaysFromLastVisitSpinEdit.MenuManager = this.barManager1;
            this.MaximumDaysFromLastVisitSpinEdit.Name = "MaximumDaysFromLastVisitSpinEdit";
            this.MaximumDaysFromLastVisitSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MaximumDaysFromLastVisitSpinEdit.Properties.IsFloatValue = false;
            this.MaximumDaysFromLastVisitSpinEdit.Properties.Mask.EditMask = "N00";
            this.MaximumDaysFromLastVisitSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.MaximumDaysFromLastVisitSpinEdit.Properties.MaxValue = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.MaximumDaysFromLastVisitSpinEdit.Size = new System.Drawing.Size(50, 20);
            this.MaximumDaysFromLastVisitSpinEdit.StyleController = this.dataLayoutControl1;
            this.MaximumDaysFromLastVisitSpinEdit.TabIndex = 10;
            // 
            // MinimumDaysFromLastVisitSpinEdit
            // 
            this.MinimumDaysFromLastVisitSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "MinimumDaysFromLastVisit", true));
            this.MinimumDaysFromLastVisitSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.MinimumDaysFromLastVisitSpinEdit.Location = new System.Drawing.Point(162, 393);
            this.MinimumDaysFromLastVisitSpinEdit.MenuManager = this.barManager1;
            this.MinimumDaysFromLastVisitSpinEdit.Name = "MinimumDaysFromLastVisitSpinEdit";
            this.MinimumDaysFromLastVisitSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MinimumDaysFromLastVisitSpinEdit.Properties.IsFloatValue = false;
            this.MinimumDaysFromLastVisitSpinEdit.Properties.Mask.EditMask = "N00";
            this.MinimumDaysFromLastVisitSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.MinimumDaysFromLastVisitSpinEdit.Properties.MaxValue = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.MinimumDaysFromLastVisitSpinEdit.Size = new System.Drawing.Size(50, 20);
            this.MinimumDaysFromLastVisitSpinEdit.StyleController = this.dataLayoutControl1;
            this.MinimumDaysFromLastVisitSpinEdit.TabIndex = 9;
            // 
            // FinishLongitudeTextEdit
            // 
            this.FinishLongitudeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "FinishLongitude", true));
            this.FinishLongitudeTextEdit.Location = new System.Drawing.Point(561, 545);
            this.FinishLongitudeTextEdit.MenuManager = this.barManager1;
            this.FinishLongitudeTextEdit.Name = "FinishLongitudeTextEdit";
            this.FinishLongitudeTextEdit.Properties.Mask.EditMask = "n8";
            this.FinishLongitudeTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.FinishLongitudeTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.FinishLongitudeTextEdit, true);
            this.FinishLongitudeTextEdit.Size = new System.Drawing.Size(118, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.FinishLongitudeTextEdit, optionsSpelling10);
            this.FinishLongitudeTextEdit.StyleController = this.dataLayoutControl1;
            this.FinishLongitudeTextEdit.TabIndex = 23;
            // 
            // FinishLatitudeTextEdit
            // 
            this.FinishLatitudeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "FinishLatitude", true));
            this.FinishLatitudeTextEdit.Location = new System.Drawing.Point(561, 521);
            this.FinishLatitudeTextEdit.MenuManager = this.barManager1;
            this.FinishLatitudeTextEdit.Name = "FinishLatitudeTextEdit";
            this.FinishLatitudeTextEdit.Properties.Mask.EditMask = "n8";
            this.FinishLatitudeTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.FinishLatitudeTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.FinishLatitudeTextEdit, true);
            this.FinishLatitudeTextEdit.Size = new System.Drawing.Size(118, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.FinishLatitudeTextEdit, optionsSpelling11);
            this.FinishLatitudeTextEdit.StyleController = this.dataLayoutControl1;
            this.FinishLatitudeTextEdit.TabIndex = 22;
            // 
            // StartLongitudeTextEdit
            // 
            this.StartLongitudeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "StartLongitude", true));
            this.StartLongitudeTextEdit.Location = new System.Drawing.Point(327, 545);
            this.StartLongitudeTextEdit.MenuManager = this.barManager1;
            this.StartLongitudeTextEdit.Name = "StartLongitudeTextEdit";
            this.StartLongitudeTextEdit.Properties.Mask.EditMask = "n8";
            this.StartLongitudeTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.StartLongitudeTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.StartLongitudeTextEdit, true);
            this.StartLongitudeTextEdit.Size = new System.Drawing.Size(116, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.StartLongitudeTextEdit, optionsSpelling12);
            this.StartLongitudeTextEdit.StyleController = this.dataLayoutControl1;
            this.StartLongitudeTextEdit.TabIndex = 21;
            // 
            // StartLatitudeTextEdit
            // 
            this.StartLatitudeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "StartLatitude", true));
            this.StartLatitudeTextEdit.Location = new System.Drawing.Point(327, 521);
            this.StartLatitudeTextEdit.MenuManager = this.barManager1;
            this.StartLatitudeTextEdit.Name = "StartLatitudeTextEdit";
            this.StartLatitudeTextEdit.Properties.Mask.EditMask = "n8";
            this.StartLatitudeTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.StartLatitudeTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.StartLatitudeTextEdit, true);
            this.StartLatitudeTextEdit.Size = new System.Drawing.Size(116, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.StartLatitudeTextEdit, optionsSpelling13);
            this.StartLatitudeTextEdit.StyleController = this.dataLayoutControl1;
            this.StartLatitudeTextEdit.TabIndex = 20;
            // 
            // RouteOrderSpinEdit
            // 
            this.RouteOrderSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "RouteOrder", true));
            this.RouteOrderSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.RouteOrderSpinEdit.Location = new System.Drawing.Point(454, 141);
            this.RouteOrderSpinEdit.MenuManager = this.barManager1;
            this.RouteOrderSpinEdit.Name = "RouteOrderSpinEdit";
            this.RouteOrderSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RouteOrderSpinEdit.Properties.IsFloatValue = false;
            this.RouteOrderSpinEdit.Properties.Mask.EditMask = "N0";
            this.RouteOrderSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.RouteOrderSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.RouteOrderSpinEdit.Size = new System.Drawing.Size(237, 20);
            this.RouteOrderSpinEdit.StyleController = this.dataLayoutControl1;
            this.RouteOrderSpinEdit.TabIndex = 3;
            // 
            // ClientPOIDTextEdit
            // 
            this.ClientPOIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "ClientPOID", true));
            this.ClientPOIDTextEdit.Location = new System.Drawing.Point(162, 371);
            this.ClientPOIDTextEdit.MenuManager = this.barManager1;
            this.ClientPOIDTextEdit.Name = "ClientPOIDTextEdit";
            this.ClientPOIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientPOIDTextEdit, true);
            this.ClientPOIDTextEdit.Size = new System.Drawing.Size(646, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientPOIDTextEdit, optionsSpelling14);
            this.ClientPOIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientPOIDTextEdit.TabIndex = 109;
            // 
            // AccessPermitIDTextEdit
            // 
            this.AccessPermitIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "AccessPermitID", true));
            this.AccessPermitIDTextEdit.Location = new System.Drawing.Point(154, 202);
            this.AccessPermitIDTextEdit.MenuManager = this.barManager1;
            this.AccessPermitIDTextEdit.Name = "AccessPermitIDTextEdit";
            this.AccessPermitIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AccessPermitIDTextEdit, true);
            this.AccessPermitIDTextEdit.Size = new System.Drawing.Size(678, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AccessPermitIDTextEdit, optionsSpelling15);
            this.AccessPermitIDTextEdit.StyleController = this.dataLayoutControl1;
            this.AccessPermitIDTextEdit.TabIndex = 107;
            // 
            // DoNotInvoiceClientCheckEdit
            // 
            this.DoNotInvoiceClientCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "DoNotInvoiceClient", true));
            this.DoNotInvoiceClientCheckEdit.Location = new System.Drawing.Point(1175, 95);
            this.DoNotInvoiceClientCheckEdit.MenuManager = this.barManager1;
            this.DoNotInvoiceClientCheckEdit.Name = "DoNotInvoiceClientCheckEdit";
            this.DoNotInvoiceClientCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.DoNotInvoiceClientCheckEdit.Properties.ValueChecked = 1;
            this.DoNotInvoiceClientCheckEdit.Properties.ValueUnchecked = 0;
            this.DoNotInvoiceClientCheckEdit.Size = new System.Drawing.Size(199, 19);
            this.DoNotInvoiceClientCheckEdit.StyleController = this.dataLayoutControl1;
            this.DoNotInvoiceClientCheckEdit.TabIndex = 8;
            // 
            // DoNotPayContractorCheckEdit
            // 
            this.DoNotPayContractorCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "DoNotPayContractor", true));
            this.DoNotPayContractorCheckEdit.Location = new System.Drawing.Point(855, 95);
            this.DoNotPayContractorCheckEdit.MenuManager = this.barManager1;
            this.DoNotPayContractorCheckEdit.Name = "DoNotPayContractorCheckEdit";
            this.DoNotPayContractorCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.DoNotPayContractorCheckEdit.Properties.ValueChecked = 1;
            this.DoNotPayContractorCheckEdit.Properties.ValueUnchecked = 0;
            this.DoNotPayContractorCheckEdit.Size = new System.Drawing.Size(190, 19);
            this.DoNotPayContractorCheckEdit.StyleController = this.dataLayoutControl1;
            this.DoNotPayContractorCheckEdit.TabIndex = 7;
            // 
            // SelfBillingInvoiceAmountPaidSpinEdit
            // 
            this.SelfBillingInvoiceAmountPaidSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "SelfBillingInvoiceAmountPaid", true));
            this.SelfBillingInvoiceAmountPaidSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SelfBillingInvoiceAmountPaidSpinEdit.Location = new System.Drawing.Point(818, 234);
            this.SelfBillingInvoiceAmountPaidSpinEdit.MenuManager = this.barManager1;
            this.SelfBillingInvoiceAmountPaidSpinEdit.Name = "SelfBillingInvoiceAmountPaidSpinEdit";
            this.SelfBillingInvoiceAmountPaidSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SelfBillingInvoiceAmountPaidSpinEdit.Properties.Mask.EditMask = "c";
            this.SelfBillingInvoiceAmountPaidSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SelfBillingInvoiceAmountPaidSpinEdit.Size = new System.Drawing.Size(215, 20);
            this.SelfBillingInvoiceAmountPaidSpinEdit.StyleController = this.dataLayoutControl1;
            this.SelfBillingInvoiceAmountPaidSpinEdit.TabIndex = 1;
            // 
            // SelfBillingInvoicePaidDateDateEdit
            // 
            this.SelfBillingInvoicePaidDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "SelfBillingInvoicePaidDate", true));
            this.SelfBillingInvoicePaidDateDateEdit.EditValue = null;
            this.SelfBillingInvoicePaidDateDateEdit.Location = new System.Drawing.Point(818, 210);
            this.SelfBillingInvoicePaidDateDateEdit.MenuManager = this.barManager1;
            this.SelfBillingInvoicePaidDateDateEdit.Name = "SelfBillingInvoicePaidDateDateEdit";
            this.SelfBillingInvoicePaidDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SelfBillingInvoicePaidDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SelfBillingInvoicePaidDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.SelfBillingInvoicePaidDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.SelfBillingInvoicePaidDateDateEdit.Size = new System.Drawing.Size(215, 20);
            this.SelfBillingInvoicePaidDateDateEdit.StyleController = this.dataLayoutControl1;
            this.SelfBillingInvoicePaidDateDateEdit.TabIndex = 0;
            // 
            // SelfBillingInvoiceReceivedDateDateEdit
            // 
            this.SelfBillingInvoiceReceivedDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "SelfBillingInvoiceReceivedDate", true));
            this.SelfBillingInvoiceReceivedDateDateEdit.EditValue = null;
            this.SelfBillingInvoiceReceivedDateDateEdit.Location = new System.Drawing.Point(818, 162);
            this.SelfBillingInvoiceReceivedDateDateEdit.MenuManager = this.barManager1;
            this.SelfBillingInvoiceReceivedDateDateEdit.Name = "SelfBillingInvoiceReceivedDateDateEdit";
            this.SelfBillingInvoiceReceivedDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SelfBillingInvoiceReceivedDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SelfBillingInvoiceReceivedDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.SelfBillingInvoiceReceivedDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.SelfBillingInvoiceReceivedDateDateEdit.Size = new System.Drawing.Size(215, 20);
            this.SelfBillingInvoiceReceivedDateDateEdit.StyleController = this.dataLayoutControl1;
            this.SelfBillingInvoiceReceivedDateDateEdit.TabIndex = 38;
            // 
            // SelfBillingInvoiceIDTextEdit
            // 
            this.SelfBillingInvoiceIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "SelfBillingInvoiceID", true));
            this.SelfBillingInvoiceIDTextEdit.Location = new System.Drawing.Point(818, 186);
            this.SelfBillingInvoiceIDTextEdit.MenuManager = this.barManager1;
            this.SelfBillingInvoiceIDTextEdit.Name = "SelfBillingInvoiceIDTextEdit";
            this.SelfBillingInvoiceIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SelfBillingInvoiceIDTextEdit, true);
            this.SelfBillingInvoiceIDTextEdit.Size = new System.Drawing.Size(215, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SelfBillingInvoiceIDTextEdit, optionsSpelling16);
            this.SelfBillingInvoiceIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SelfBillingInvoiceIDTextEdit.TabIndex = 130;
            // 
            // DateClientInvoicedDateEdit
            // 
            this.DateClientInvoicedDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "DateClientInvoiced", true));
            this.DateClientInvoicedDateEdit.EditValue = null;
            this.DateClientInvoicedDateEdit.Location = new System.Drawing.Point(1165, 162);
            this.DateClientInvoicedDateEdit.MenuManager = this.barManager1;
            this.DateClientInvoicedDateEdit.Name = "DateClientInvoicedDateEdit";
            this.DateClientInvoicedDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateClientInvoicedDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateClientInvoicedDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.DateClientInvoicedDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.DateClientInvoicedDateEdit.Size = new System.Drawing.Size(197, 20);
            this.DateClientInvoicedDateEdit.StyleController = this.dataLayoutControl1;
            this.DateClientInvoicedDateEdit.TabIndex = 2;
            // 
            // SellTotalCostSpinEdit
            // 
            this.SellTotalCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "SellTotalCost", true));
            this.SellTotalCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SellTotalCostSpinEdit.Location = new System.Drawing.Point(1141, 856);
            this.SellTotalCostSpinEdit.MenuManager = this.barManager1;
            this.SellTotalCostSpinEdit.Name = "SellTotalCostSpinEdit";
            this.SellTotalCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SellTotalCostSpinEdit.Properties.Mask.EditMask = "c";
            this.SellTotalCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SellTotalCostSpinEdit.Properties.ReadOnly = true;
            this.SellTotalCostSpinEdit.Size = new System.Drawing.Size(209, 20);
            this.SellTotalCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.SellTotalCostSpinEdit.TabIndex = 129;
            this.SellTotalCostSpinEdit.TabStop = false;
            // 
            // SellTotalCostVATSpinEdit
            // 
            this.SellTotalCostVATSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "SellTotalCostVAT", true));
            this.SellTotalCostVATSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SellTotalCostVATSpinEdit.Location = new System.Drawing.Point(1141, 830);
            this.SellTotalCostVATSpinEdit.MenuManager = this.barManager1;
            this.SellTotalCostVATSpinEdit.Name = "SellTotalCostVATSpinEdit";
            this.SellTotalCostVATSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SellTotalCostVATSpinEdit.Properties.Mask.EditMask = "c";
            this.SellTotalCostVATSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SellTotalCostVATSpinEdit.Properties.ReadOnly = true;
            this.SellTotalCostVATSpinEdit.Size = new System.Drawing.Size(209, 20);
            this.SellTotalCostVATSpinEdit.StyleController = this.dataLayoutControl1;
            this.SellTotalCostVATSpinEdit.TabIndex = 128;
            this.SellTotalCostVATSpinEdit.TabStop = false;
            // 
            // SellTotalCostExVATSpinEdit
            // 
            this.SellTotalCostExVATSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "SellTotalCostExVAT", true));
            this.SellTotalCostExVATSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SellTotalCostExVATSpinEdit.Location = new System.Drawing.Point(1141, 806);
            this.SellTotalCostExVATSpinEdit.MenuManager = this.barManager1;
            this.SellTotalCostExVATSpinEdit.Name = "SellTotalCostExVATSpinEdit";
            this.SellTotalCostExVATSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SellTotalCostExVATSpinEdit.Properties.Mask.EditMask = "c";
            this.SellTotalCostExVATSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SellTotalCostExVATSpinEdit.Properties.ReadOnly = true;
            this.SellTotalCostExVATSpinEdit.Size = new System.Drawing.Size(209, 20);
            this.SellTotalCostExVATSpinEdit.StyleController = this.dataLayoutControl1;
            this.SellTotalCostExVATSpinEdit.TabIndex = 127;
            this.SellTotalCostExVATSpinEdit.TabStop = false;
            // 
            // CostTotalCostSpinEdit
            // 
            this.CostTotalCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "CostTotalCost", true));
            this.CostTotalCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CostTotalCostSpinEdit.Location = new System.Drawing.Point(821, 856);
            this.CostTotalCostSpinEdit.MenuManager = this.barManager1;
            this.CostTotalCostSpinEdit.Name = "CostTotalCostSpinEdit";
            this.CostTotalCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CostTotalCostSpinEdit.Properties.Mask.EditMask = "c";
            this.CostTotalCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CostTotalCostSpinEdit.Properties.ReadOnly = true;
            this.CostTotalCostSpinEdit.Size = new System.Drawing.Size(200, 20);
            this.CostTotalCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.CostTotalCostSpinEdit.TabIndex = 44;
            this.CostTotalCostSpinEdit.TabStop = false;
            // 
            // CostTotalCostVATSpinEdit
            // 
            this.CostTotalCostVATSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "CostTotalCostVAT", true));
            this.CostTotalCostVATSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CostTotalCostVATSpinEdit.Location = new System.Drawing.Point(821, 830);
            this.CostTotalCostVATSpinEdit.MenuManager = this.barManager1;
            this.CostTotalCostVATSpinEdit.Name = "CostTotalCostVATSpinEdit";
            this.CostTotalCostVATSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CostTotalCostVATSpinEdit.Properties.Mask.EditMask = "c";
            this.CostTotalCostVATSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CostTotalCostVATSpinEdit.Properties.ReadOnly = true;
            this.CostTotalCostVATSpinEdit.Size = new System.Drawing.Size(200, 20);
            this.CostTotalCostVATSpinEdit.StyleController = this.dataLayoutControl1;
            this.CostTotalCostVATSpinEdit.TabIndex = 43;
            this.CostTotalCostVATSpinEdit.TabStop = false;
            // 
            // CostTotalCostExVATSpinEdit
            // 
            this.CostTotalCostExVATSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "CostTotalCostExVAT", true));
            this.CostTotalCostExVATSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CostTotalCostExVATSpinEdit.Location = new System.Drawing.Point(821, 806);
            this.CostTotalCostExVATSpinEdit.MenuManager = this.barManager1;
            this.CostTotalCostExVATSpinEdit.Name = "CostTotalCostExVATSpinEdit";
            this.CostTotalCostExVATSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CostTotalCostExVATSpinEdit.Properties.Mask.EditMask = "c";
            this.CostTotalCostExVATSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CostTotalCostExVATSpinEdit.Properties.ReadOnly = true;
            this.CostTotalCostExVATSpinEdit.Size = new System.Drawing.Size(200, 20);
            this.CostTotalCostExVATSpinEdit.StyleController = this.dataLayoutControl1;
            this.CostTotalCostExVATSpinEdit.TabIndex = 42;
            this.CostTotalCostExVATSpinEdit.TabStop = false;
            // 
            // SellTotalLabourCostSpinEdit
            // 
            this.SellTotalLabourCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "SellTotalLabourCost", true));
            this.SellTotalLabourCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SellTotalLabourCostSpinEdit.Location = new System.Drawing.Point(1141, 448);
            this.SellTotalLabourCostSpinEdit.MenuManager = this.barManager1;
            this.SellTotalLabourCostSpinEdit.Name = "SellTotalLabourCostSpinEdit";
            this.SellTotalLabourCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SellTotalLabourCostSpinEdit.Properties.Mask.EditMask = "c";
            this.SellTotalLabourCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SellTotalLabourCostSpinEdit.Properties.ReadOnly = true;
            this.SellTotalLabourCostSpinEdit.Size = new System.Drawing.Size(209, 20);
            this.SellTotalLabourCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.SellTotalLabourCostSpinEdit.TabIndex = 124;
            this.SellTotalLabourCostSpinEdit.TabStop = false;
            // 
            // SellTotalLabourVATSpinEdit
            // 
            this.SellTotalLabourVATSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "SellTotalLabourVAT", true));
            this.SellTotalLabourVATSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SellTotalLabourVATSpinEdit.Location = new System.Drawing.Point(1141, 422);
            this.SellTotalLabourVATSpinEdit.MenuManager = this.barManager1;
            this.SellTotalLabourVATSpinEdit.Name = "SellTotalLabourVATSpinEdit";
            this.SellTotalLabourVATSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SellTotalLabourVATSpinEdit.Properties.Mask.EditMask = "c";
            this.SellTotalLabourVATSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SellTotalLabourVATSpinEdit.Properties.ReadOnly = true;
            this.SellTotalLabourVATSpinEdit.Size = new System.Drawing.Size(209, 20);
            this.SellTotalLabourVATSpinEdit.StyleController = this.dataLayoutControl1;
            this.SellTotalLabourVATSpinEdit.TabIndex = 124;
            this.SellTotalLabourVATSpinEdit.TabStop = false;
            // 
            // SellTotalLabourVATRateSpinEdit
            // 
            this.SellTotalLabourVATRateSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "SellTotalLabourVATRate", true));
            this.SellTotalLabourVATRateSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SellTotalLabourVATRateSpinEdit.Location = new System.Drawing.Point(1141, 398);
            this.SellTotalLabourVATRateSpinEdit.MenuManager = this.barManager1;
            this.SellTotalLabourVATRateSpinEdit.Name = "SellTotalLabourVATRateSpinEdit";
            this.SellTotalLabourVATRateSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SellTotalLabourVATRateSpinEdit.Properties.Mask.EditMask = "P";
            this.SellTotalLabourVATRateSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SellTotalLabourVATRateSpinEdit.Size = new System.Drawing.Size(209, 20);
            this.SellTotalLabourVATRateSpinEdit.StyleController = this.dataLayoutControl1;
            this.SellTotalLabourVATRateSpinEdit.TabIndex = 33;
            this.SellTotalLabourVATRateSpinEdit.EditValueChanged += new System.EventHandler(this.SellTotalLabourVATRateSpinEdit_EditValueChanged);
            this.SellTotalLabourVATRateSpinEdit.Validated += new System.EventHandler(this.SellTotalLabourVATRateSpinEdit_Validated);
            // 
            // SellTotalLabourExVATSpinEdit
            // 
            this.SellTotalLabourExVATSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "SellTotalLabourExVAT", true));
            this.SellTotalLabourExVATSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SellTotalLabourExVATSpinEdit.Location = new System.Drawing.Point(1141, 372);
            this.SellTotalLabourExVATSpinEdit.MenuManager = this.barManager1;
            this.SellTotalLabourExVATSpinEdit.Name = "SellTotalLabourExVATSpinEdit";
            this.SellTotalLabourExVATSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SellTotalLabourExVATSpinEdit.Properties.Mask.EditMask = "c";
            this.SellTotalLabourExVATSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SellTotalLabourExVATSpinEdit.Size = new System.Drawing.Size(179, 20);
            this.SellTotalLabourExVATSpinEdit.StyleController = this.dataLayoutControl1;
            this.SellTotalLabourExVATSpinEdit.TabIndex = 32;
            this.SellTotalLabourExVATSpinEdit.EditValueChanged += new System.EventHandler(this.SellTotalLabourExVATSpinEdit_EditValueChanged);
            this.SellTotalLabourExVATSpinEdit.Validated += new System.EventHandler(this.SellTotalLabourExVATSpinEdit_Validated);
            // 
            // CostTotalLabourCostSpinEdit
            // 
            this.CostTotalLabourCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "CostTotalLabourCost", true));
            this.CostTotalLabourCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CostTotalLabourCostSpinEdit.Location = new System.Drawing.Point(821, 448);
            this.CostTotalLabourCostSpinEdit.MenuManager = this.barManager1;
            this.CostTotalLabourCostSpinEdit.Name = "CostTotalLabourCostSpinEdit";
            this.CostTotalLabourCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CostTotalLabourCostSpinEdit.Properties.Mask.EditMask = "c";
            this.CostTotalLabourCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CostTotalLabourCostSpinEdit.Properties.ReadOnly = true;
            this.CostTotalLabourCostSpinEdit.Size = new System.Drawing.Size(200, 20);
            this.CostTotalLabourCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.CostTotalLabourCostSpinEdit.TabIndex = 41;
            this.CostTotalLabourCostSpinEdit.TabStop = false;
            // 
            // CostTotalLabourVATSpinEdit
            // 
            this.CostTotalLabourVATSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "CostTotalLabourVAT", true));
            this.CostTotalLabourVATSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CostTotalLabourVATSpinEdit.Location = new System.Drawing.Point(821, 422);
            this.CostTotalLabourVATSpinEdit.MenuManager = this.barManager1;
            this.CostTotalLabourVATSpinEdit.Name = "CostTotalLabourVATSpinEdit";
            this.CostTotalLabourVATSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CostTotalLabourVATSpinEdit.Properties.Mask.EditMask = "c";
            this.CostTotalLabourVATSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CostTotalLabourVATSpinEdit.Properties.ReadOnly = true;
            this.CostTotalLabourVATSpinEdit.Size = new System.Drawing.Size(200, 20);
            this.CostTotalLabourVATSpinEdit.StyleController = this.dataLayoutControl1;
            this.CostTotalLabourVATSpinEdit.TabIndex = 40;
            this.CostTotalLabourVATSpinEdit.TabStop = false;
            // 
            // CostTotalLabourVATRateSpinEdit
            // 
            this.CostTotalLabourVATRateSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "CostTotalLabourVATRate", true));
            this.CostTotalLabourVATRateSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CostTotalLabourVATRateSpinEdit.Location = new System.Drawing.Point(821, 398);
            this.CostTotalLabourVATRateSpinEdit.MenuManager = this.barManager1;
            this.CostTotalLabourVATRateSpinEdit.Name = "CostTotalLabourVATRateSpinEdit";
            this.CostTotalLabourVATRateSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CostTotalLabourVATRateSpinEdit.Properties.Mask.EditMask = "P";
            this.CostTotalLabourVATRateSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CostTotalLabourVATRateSpinEdit.Size = new System.Drawing.Size(200, 20);
            this.CostTotalLabourVATRateSpinEdit.StyleController = this.dataLayoutControl1;
            this.CostTotalLabourVATRateSpinEdit.TabIndex = 27;
            this.CostTotalLabourVATRateSpinEdit.EditValueChanged += new System.EventHandler(this.CostTotalLabourVATRateSpinEdit_EditValueChanged);
            this.CostTotalLabourVATRateSpinEdit.Validated += new System.EventHandler(this.CostTotalLabourVATRateSpinEdit_Validated);
            // 
            // CostTotalLabourExVATSpinEdit
            // 
            this.CostTotalLabourExVATSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "CostTotalLabourExVAT", true));
            this.CostTotalLabourExVATSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CostTotalLabourExVATSpinEdit.Location = new System.Drawing.Point(821, 372);
            this.CostTotalLabourExVATSpinEdit.MenuManager = this.barManager1;
            this.CostTotalLabourExVATSpinEdit.Name = "CostTotalLabourExVATSpinEdit";
            this.CostTotalLabourExVATSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CostTotalLabourExVATSpinEdit.Properties.Mask.EditMask = "c";
            this.CostTotalLabourExVATSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CostTotalLabourExVATSpinEdit.Size = new System.Drawing.Size(170, 20);
            this.CostTotalLabourExVATSpinEdit.StyleController = this.dataLayoutControl1;
            this.CostTotalLabourExVATSpinEdit.TabIndex = 26;
            this.CostTotalLabourExVATSpinEdit.EditValueChanged += new System.EventHandler(this.CostTotalLabourExVATSpinEdit_EditValueChanged);
            this.CostTotalLabourExVATSpinEdit.Validated += new System.EventHandler(this.CostTotalLabourExVATSpinEdit_Validated);
            // 
            // SellTotalEquipmentCostSpinEdit
            // 
            this.SellTotalEquipmentCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "SellTotalEquipmentCost", true));
            this.SellTotalEquipmentCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SellTotalEquipmentCostSpinEdit.Location = new System.Drawing.Point(1141, 592);
            this.SellTotalEquipmentCostSpinEdit.MenuManager = this.barManager1;
            this.SellTotalEquipmentCostSpinEdit.Name = "SellTotalEquipmentCostSpinEdit";
            this.SellTotalEquipmentCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SellTotalEquipmentCostSpinEdit.Properties.Mask.EditMask = "c";
            this.SellTotalEquipmentCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SellTotalEquipmentCostSpinEdit.Properties.ReadOnly = true;
            this.SellTotalEquipmentCostSpinEdit.Size = new System.Drawing.Size(209, 20);
            this.SellTotalEquipmentCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.SellTotalEquipmentCostSpinEdit.TabIndex = 6;
            this.SellTotalEquipmentCostSpinEdit.TabStop = false;
            // 
            // SellTotalEquipmentVATSpinEdit
            // 
            this.SellTotalEquipmentVATSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "SellTotalEquipmentVAT", true));
            this.SellTotalEquipmentVATSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SellTotalEquipmentVATSpinEdit.Location = new System.Drawing.Point(1141, 566);
            this.SellTotalEquipmentVATSpinEdit.MenuManager = this.barManager1;
            this.SellTotalEquipmentVATSpinEdit.Name = "SellTotalEquipmentVATSpinEdit";
            this.SellTotalEquipmentVATSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SellTotalEquipmentVATSpinEdit.Properties.Mask.EditMask = "c";
            this.SellTotalEquipmentVATSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SellTotalEquipmentVATSpinEdit.Properties.ReadOnly = true;
            this.SellTotalEquipmentVATSpinEdit.Size = new System.Drawing.Size(209, 20);
            this.SellTotalEquipmentVATSpinEdit.StyleController = this.dataLayoutControl1;
            this.SellTotalEquipmentVATSpinEdit.TabIndex = 5;
            this.SellTotalEquipmentVATSpinEdit.TabStop = false;
            // 
            // SellTotalEquipmentVATRateSpinEdit
            // 
            this.SellTotalEquipmentVATRateSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "SellTotalEquipmentVATRate", true));
            this.SellTotalEquipmentVATRateSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SellTotalEquipmentVATRateSpinEdit.Location = new System.Drawing.Point(1141, 542);
            this.SellTotalEquipmentVATRateSpinEdit.MenuManager = this.barManager1;
            this.SellTotalEquipmentVATRateSpinEdit.Name = "SellTotalEquipmentVATRateSpinEdit";
            this.SellTotalEquipmentVATRateSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SellTotalEquipmentVATRateSpinEdit.Properties.Mask.EditMask = "P";
            this.SellTotalEquipmentVATRateSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SellTotalEquipmentVATRateSpinEdit.Size = new System.Drawing.Size(209, 20);
            this.SellTotalEquipmentVATRateSpinEdit.StyleController = this.dataLayoutControl1;
            this.SellTotalEquipmentVATRateSpinEdit.TabIndex = 35;
            this.SellTotalEquipmentVATRateSpinEdit.EditValueChanged += new System.EventHandler(this.SellTotalEquipmentVATRateSpinEdit_EditValueChanged);
            this.SellTotalEquipmentVATRateSpinEdit.Validated += new System.EventHandler(this.SellTotalEquipmentVATRateSpinEdit_Validated);
            // 
            // SellTotalEquipmentExVATSpinEdit
            // 
            this.SellTotalEquipmentExVATSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "SellTotalEquipmentExVAT", true));
            this.SellTotalEquipmentExVATSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SellTotalEquipmentExVATSpinEdit.Location = new System.Drawing.Point(1141, 518);
            this.SellTotalEquipmentExVATSpinEdit.MenuManager = this.barManager1;
            this.SellTotalEquipmentExVATSpinEdit.Name = "SellTotalEquipmentExVATSpinEdit";
            this.SellTotalEquipmentExVATSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SellTotalEquipmentExVATSpinEdit.Properties.Mask.EditMask = "c";
            this.SellTotalEquipmentExVATSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SellTotalEquipmentExVATSpinEdit.Size = new System.Drawing.Size(209, 20);
            this.SellTotalEquipmentExVATSpinEdit.StyleController = this.dataLayoutControl1;
            this.SellTotalEquipmentExVATSpinEdit.TabIndex = 34;
            this.SellTotalEquipmentExVATSpinEdit.EditValueChanged += new System.EventHandler(this.SellTotalEquipmentExVATSpinEdit_EditValueChanged);
            this.SellTotalEquipmentExVATSpinEdit.Validated += new System.EventHandler(this.SellTotalEquipmentExVATSpinEdit_Validated);
            // 
            // CostTotalEquipmentCostSpinEdit
            // 
            this.CostTotalEquipmentCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "CostTotalEquipmentCost", true));
            this.CostTotalEquipmentCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CostTotalEquipmentCostSpinEdit.Location = new System.Drawing.Point(821, 592);
            this.CostTotalEquipmentCostSpinEdit.MenuManager = this.barManager1;
            this.CostTotalEquipmentCostSpinEdit.Name = "CostTotalEquipmentCostSpinEdit";
            this.CostTotalEquipmentCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CostTotalEquipmentCostSpinEdit.Properties.Mask.EditMask = "c";
            this.CostTotalEquipmentCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CostTotalEquipmentCostSpinEdit.Properties.ReadOnly = true;
            this.CostTotalEquipmentCostSpinEdit.Size = new System.Drawing.Size(200, 20);
            this.CostTotalEquipmentCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.CostTotalEquipmentCostSpinEdit.TabIndex = 37;
            this.CostTotalEquipmentCostSpinEdit.TabStop = false;
            // 
            // CostTotalEquipmentVATSpinEdit
            // 
            this.CostTotalEquipmentVATSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "CostTotalEquipmentVAT", true));
            this.CostTotalEquipmentVATSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CostTotalEquipmentVATSpinEdit.Location = new System.Drawing.Point(821, 566);
            this.CostTotalEquipmentVATSpinEdit.MenuManager = this.barManager1;
            this.CostTotalEquipmentVATSpinEdit.Name = "CostTotalEquipmentVATSpinEdit";
            this.CostTotalEquipmentVATSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CostTotalEquipmentVATSpinEdit.Properties.Mask.EditMask = "c";
            this.CostTotalEquipmentVATSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CostTotalEquipmentVATSpinEdit.Properties.ReadOnly = true;
            this.CostTotalEquipmentVATSpinEdit.Size = new System.Drawing.Size(200, 20);
            this.CostTotalEquipmentVATSpinEdit.StyleController = this.dataLayoutControl1;
            this.CostTotalEquipmentVATSpinEdit.TabIndex = 36;
            this.CostTotalEquipmentVATSpinEdit.TabStop = false;
            // 
            // CostTotalEquipmentVATRateSpinEdit
            // 
            this.CostTotalEquipmentVATRateSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "CostTotalEquipmentVATRate", true));
            this.CostTotalEquipmentVATRateSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CostTotalEquipmentVATRateSpinEdit.Location = new System.Drawing.Point(821, 542);
            this.CostTotalEquipmentVATRateSpinEdit.MenuManager = this.barManager1;
            this.CostTotalEquipmentVATRateSpinEdit.Name = "CostTotalEquipmentVATRateSpinEdit";
            this.CostTotalEquipmentVATRateSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CostTotalEquipmentVATRateSpinEdit.Properties.Mask.EditMask = "P";
            this.CostTotalEquipmentVATRateSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CostTotalEquipmentVATRateSpinEdit.Size = new System.Drawing.Size(200, 20);
            this.CostTotalEquipmentVATRateSpinEdit.StyleController = this.dataLayoutControl1;
            this.CostTotalEquipmentVATRateSpinEdit.TabIndex = 29;
            this.CostTotalEquipmentVATRateSpinEdit.EditValueChanged += new System.EventHandler(this.CostTotalEquipmentVATRateSpinEdit_EditValueChanged);
            this.CostTotalEquipmentVATRateSpinEdit.Validated += new System.EventHandler(this.CostTotalEquipmentVATRateSpinEdit_Validated);
            // 
            // CostTotalEquipmentExVATSpinEdit
            // 
            this.CostTotalEquipmentExVATSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "CostTotalEquipmentExVAT", true));
            this.CostTotalEquipmentExVATSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CostTotalEquipmentExVATSpinEdit.Location = new System.Drawing.Point(821, 518);
            this.CostTotalEquipmentExVATSpinEdit.MenuManager = this.barManager1;
            this.CostTotalEquipmentExVATSpinEdit.Name = "CostTotalEquipmentExVATSpinEdit";
            this.CostTotalEquipmentExVATSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CostTotalEquipmentExVATSpinEdit.Properties.Mask.EditMask = "c";
            this.CostTotalEquipmentExVATSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CostTotalEquipmentExVATSpinEdit.Size = new System.Drawing.Size(200, 20);
            this.CostTotalEquipmentExVATSpinEdit.StyleController = this.dataLayoutControl1;
            this.CostTotalEquipmentExVATSpinEdit.TabIndex = 28;
            this.CostTotalEquipmentExVATSpinEdit.EditValueChanged += new System.EventHandler(this.CostTotalEquipmentExVATSpinEdit_EditValueChanged);
            this.CostTotalEquipmentExVATSpinEdit.Validated += new System.EventHandler(this.CostTotalEquipmentExVATSpinEdit_Validated);
            // 
            // SellTotalMaterialCostSpinEdit
            // 
            this.SellTotalMaterialCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "SellTotalMaterialCost", true));
            this.SellTotalMaterialCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SellTotalMaterialCostSpinEdit.Location = new System.Drawing.Point(1141, 736);
            this.SellTotalMaterialCostSpinEdit.MenuManager = this.barManager1;
            this.SellTotalMaterialCostSpinEdit.Name = "SellTotalMaterialCostSpinEdit";
            this.SellTotalMaterialCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SellTotalMaterialCostSpinEdit.Properties.Mask.EditMask = "c";
            this.SellTotalMaterialCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SellTotalMaterialCostSpinEdit.Properties.ReadOnly = true;
            this.SellTotalMaterialCostSpinEdit.Size = new System.Drawing.Size(209, 20);
            this.SellTotalMaterialCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.SellTotalMaterialCostSpinEdit.TabIndex = 2;
            this.SellTotalMaterialCostSpinEdit.TabStop = false;
            // 
            // SellTotalMaterialVATSpinEdit
            // 
            this.SellTotalMaterialVATSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "SellTotalMaterialVAT", true));
            this.SellTotalMaterialVATSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SellTotalMaterialVATSpinEdit.Location = new System.Drawing.Point(1141, 710);
            this.SellTotalMaterialVATSpinEdit.MenuManager = this.barManager1;
            this.SellTotalMaterialVATSpinEdit.Name = "SellTotalMaterialVATSpinEdit";
            this.SellTotalMaterialVATSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SellTotalMaterialVATSpinEdit.Properties.Mask.EditMask = "c";
            this.SellTotalMaterialVATSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SellTotalMaterialVATSpinEdit.Properties.ReadOnly = true;
            this.SellTotalMaterialVATSpinEdit.Size = new System.Drawing.Size(209, 20);
            this.SellTotalMaterialVATSpinEdit.StyleController = this.dataLayoutControl1;
            this.SellTotalMaterialVATSpinEdit.TabIndex = 1;
            this.SellTotalMaterialVATSpinEdit.TabStop = false;
            // 
            // SellTotalMaterialVATRateSpinEdit
            // 
            this.SellTotalMaterialVATRateSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "SellTotalMaterialVATRate", true));
            this.SellTotalMaterialVATRateSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SellTotalMaterialVATRateSpinEdit.Location = new System.Drawing.Point(1141, 686);
            this.SellTotalMaterialVATRateSpinEdit.MenuManager = this.barManager1;
            this.SellTotalMaterialVATRateSpinEdit.Name = "SellTotalMaterialVATRateSpinEdit";
            this.SellTotalMaterialVATRateSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SellTotalMaterialVATRateSpinEdit.Properties.Mask.EditMask = "P";
            this.SellTotalMaterialVATRateSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SellTotalMaterialVATRateSpinEdit.Size = new System.Drawing.Size(209, 20);
            this.SellTotalMaterialVATRateSpinEdit.StyleController = this.dataLayoutControl1;
            this.SellTotalMaterialVATRateSpinEdit.TabIndex = 37;
            this.SellTotalMaterialVATRateSpinEdit.EditValueChanged += new System.EventHandler(this.SellTotalMaterialVATRateSpinEdit_EditValueChanged);
            this.SellTotalMaterialVATRateSpinEdit.Validated += new System.EventHandler(this.SellTotalMaterialVATRateSpinEdit_Validated);
            // 
            // SellTotalMaterialExVATSpinEdit
            // 
            this.SellTotalMaterialExVATSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "SellTotalMaterialExVAT", true));
            this.SellTotalMaterialExVATSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SellTotalMaterialExVATSpinEdit.Location = new System.Drawing.Point(1141, 662);
            this.SellTotalMaterialExVATSpinEdit.MenuManager = this.barManager1;
            this.SellTotalMaterialExVATSpinEdit.Name = "SellTotalMaterialExVATSpinEdit";
            this.SellTotalMaterialExVATSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SellTotalMaterialExVATSpinEdit.Properties.Mask.EditMask = "c";
            this.SellTotalMaterialExVATSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SellTotalMaterialExVATSpinEdit.Size = new System.Drawing.Size(209, 20);
            this.SellTotalMaterialExVATSpinEdit.StyleController = this.dataLayoutControl1;
            this.SellTotalMaterialExVATSpinEdit.TabIndex = 36;
            this.SellTotalMaterialExVATSpinEdit.EditValueChanged += new System.EventHandler(this.SellTotalMaterialExVATSpinEdit_EditValueChanged);
            this.SellTotalMaterialExVATSpinEdit.Validated += new System.EventHandler(this.SellTotalMaterialExVATSpinEdit_Validated);
            // 
            // CostTotalMaterialVATRateSpinEdit
            // 
            this.CostTotalMaterialVATRateSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "CostTotalMaterialVATRate", true));
            this.CostTotalMaterialVATRateSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CostTotalMaterialVATRateSpinEdit.Location = new System.Drawing.Point(821, 686);
            this.CostTotalMaterialVATRateSpinEdit.MenuManager = this.barManager1;
            this.CostTotalMaterialVATRateSpinEdit.Name = "CostTotalMaterialVATRateSpinEdit";
            this.CostTotalMaterialVATRateSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CostTotalMaterialVATRateSpinEdit.Properties.Mask.EditMask = "P";
            this.CostTotalMaterialVATRateSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CostTotalMaterialVATRateSpinEdit.Size = new System.Drawing.Size(200, 20);
            this.CostTotalMaterialVATRateSpinEdit.StyleController = this.dataLayoutControl1;
            this.CostTotalMaterialVATRateSpinEdit.TabIndex = 31;
            this.CostTotalMaterialVATRateSpinEdit.EditValueChanged += new System.EventHandler(this.CostTotalMaterialVATRateSpinEdit_EditValueChanged);
            this.CostTotalMaterialVATRateSpinEdit.Validated += new System.EventHandler(this.CostTotalMaterialVATRateSpinEdit_Validated);
            // 
            // CostTotalMaterialVATSpinEdit
            // 
            this.CostTotalMaterialVATSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "CostTotalMaterialVAT", true));
            this.CostTotalMaterialVATSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CostTotalMaterialVATSpinEdit.Location = new System.Drawing.Point(821, 710);
            this.CostTotalMaterialVATSpinEdit.MenuManager = this.barManager1;
            this.CostTotalMaterialVATSpinEdit.Name = "CostTotalMaterialVATSpinEdit";
            this.CostTotalMaterialVATSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CostTotalMaterialVATSpinEdit.Properties.Mask.EditMask = "c";
            this.CostTotalMaterialVATSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CostTotalMaterialVATSpinEdit.Properties.ReadOnly = true;
            this.CostTotalMaterialVATSpinEdit.Size = new System.Drawing.Size(200, 20);
            this.CostTotalMaterialVATSpinEdit.StyleController = this.dataLayoutControl1;
            this.CostTotalMaterialVATSpinEdit.TabIndex = 32;
            this.CostTotalMaterialVATSpinEdit.TabStop = false;
            // 
            // CostTotalMaterialCostSpinEdit
            // 
            this.CostTotalMaterialCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "CostTotalMaterialCost", true));
            this.CostTotalMaterialCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CostTotalMaterialCostSpinEdit.Location = new System.Drawing.Point(821, 736);
            this.CostTotalMaterialCostSpinEdit.MenuManager = this.barManager1;
            this.CostTotalMaterialCostSpinEdit.Name = "CostTotalMaterialCostSpinEdit";
            this.CostTotalMaterialCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CostTotalMaterialCostSpinEdit.Properties.Mask.EditMask = "c";
            this.CostTotalMaterialCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CostTotalMaterialCostSpinEdit.Properties.ReadOnly = true;
            this.CostTotalMaterialCostSpinEdit.Size = new System.Drawing.Size(200, 20);
            this.CostTotalMaterialCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.CostTotalMaterialCostSpinEdit.TabIndex = 33;
            this.CostTotalMaterialCostSpinEdit.TabStop = false;
            // 
            // CostTotalMaterialExVATSpinEdit
            // 
            this.CostTotalMaterialExVATSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "CostTotalMaterialExVAT", true));
            this.CostTotalMaterialExVATSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CostTotalMaterialExVATSpinEdit.Location = new System.Drawing.Point(821, 662);
            this.CostTotalMaterialExVATSpinEdit.MenuManager = this.barManager1;
            this.CostTotalMaterialExVATSpinEdit.Name = "CostTotalMaterialExVATSpinEdit";
            this.CostTotalMaterialExVATSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CostTotalMaterialExVATSpinEdit.Properties.Mask.EditMask = "c";
            this.CostTotalMaterialExVATSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CostTotalMaterialExVATSpinEdit.Size = new System.Drawing.Size(200, 20);
            this.CostTotalMaterialExVATSpinEdit.StyleController = this.dataLayoutControl1;
            this.CostTotalMaterialExVATSpinEdit.TabIndex = 30;
            this.CostTotalMaterialExVATSpinEdit.EditValueChanged += new System.EventHandler(this.CostTotalMaterialExVATSpinEdit_EditValueChanged);
            this.CostTotalMaterialExVATSpinEdit.Validated += new System.EventHandler(this.CostTotalMaterialExVATSpinEdit_Validated);
            // 
            // ActualEndDateDateEdit
            // 
            this.ActualEndDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "ActualEndDate", true));
            this.ActualEndDateDateEdit.EditValue = null;
            this.ActualEndDateDateEdit.Location = new System.Drawing.Point(561, 441);
            this.ActualEndDateDateEdit.MenuManager = this.barManager1;
            this.ActualEndDateDateEdit.Name = "ActualEndDateDateEdit";
            this.ActualEndDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ActualEndDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ActualEndDateDateEdit.Properties.Mask.EditMask = "G";
            this.ActualEndDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ActualEndDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.ActualEndDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.ActualEndDateDateEdit.Size = new System.Drawing.Size(118, 20);
            this.ActualEndDateDateEdit.StyleController = this.dataLayoutControl1;
            this.ActualEndDateDateEdit.TabIndex = 19;
            this.ActualEndDateDateEdit.EditValueChanged += new System.EventHandler(this.ActualEndDateDateEdit_EditValueChanged);
            this.ActualEndDateDateEdit.Validated += new System.EventHandler(this.ActualEndDateDateEdit_Validated);
            // 
            // ActualDurationUnitsDescriptionIDGridLookUpEdit
            // 
            this.ActualDurationUnitsDescriptionIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "ActualDurationUnitsDescriptionID", true));
            this.ActualDurationUnitsDescriptionIDGridLookUpEdit.Location = new System.Drawing.Point(629, 417);
            this.ActualDurationUnitsDescriptionIDGridLookUpEdit.MenuManager = this.barManager1;
            this.ActualDurationUnitsDescriptionIDGridLookUpEdit.Name = "ActualDurationUnitsDescriptionIDGridLookUpEdit";
            this.ActualDurationUnitsDescriptionIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ActualDurationUnitsDescriptionIDGridLookUpEdit.Properties.DataSource = this.sp06177OMJobDurationDescriptorsWithBlankBindingSource;
            this.ActualDurationUnitsDescriptionIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.ActualDurationUnitsDescriptionIDGridLookUpEdit.Properties.NullText = "";
            this.ActualDurationUnitsDescriptionIDGridLookUpEdit.Properties.PopupView = this.gridView7;
            this.ActualDurationUnitsDescriptionIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.ActualDurationUnitsDescriptionIDGridLookUpEdit.Size = new System.Drawing.Size(50, 20);
            this.ActualDurationUnitsDescriptionIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.ActualDurationUnitsDescriptionIDGridLookUpEdit.TabIndex = 18;
            this.ActualDurationUnitsDescriptionIDGridLookUpEdit.EditValueChanged += new System.EventHandler(this.ActualDurationUnitsDescriptionIDGridLookUpEdit_EditValueChanged);
            this.ActualDurationUnitsDescriptionIDGridLookUpEdit.Validated += new System.EventHandler(this.ActualDurationUnitsDescriptionIDGridLookUpEdit_Validated);
            // 
            // sp06177OMJobDurationDescriptorsWithBlankBindingSource
            // 
            this.sp06177OMJobDurationDescriptorsWithBlankBindingSource.DataMember = "sp06177_OM_Job_Duration_Descriptors_With_Blank";
            this.sp06177OMJobDurationDescriptorsWithBlankBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.gridView7.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule3.ApplyToRow = true;
            gridFormatRule3.Column = this.gridColumn4;
            gridFormatRule3.Name = "Format0";
            formatConditionRuleValue3.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue3.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue3.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue3.Value1 = 0;
            gridFormatRule3.Rule = formatConditionRuleValue3;
            this.gridView7.FormatRules.Add(gridFormatRule3);
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView7.OptionsLayout.StoreAppearance = true;
            this.gridView7.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView7.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView7.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView7.OptionsView.ColumnAutoWidth = false;
            this.gridView7.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            this.gridView7.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView7.OptionsView.ShowIndicator = false;
            this.gridView7.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn6, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Unit Descriptor";
            this.gridColumn5.FieldName = "Description";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 220;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Order";
            this.gridColumn6.FieldName = "RecordOrder";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            // 
            // ActualDurationUnitsSpinEdit
            // 
            this.ActualDurationUnitsSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "ActualDurationUnits", true));
            this.ActualDurationUnitsSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ActualDurationUnitsSpinEdit.Location = new System.Drawing.Point(561, 417);
            this.ActualDurationUnitsSpinEdit.MenuManager = this.barManager1;
            this.ActualDurationUnitsSpinEdit.Name = "ActualDurationUnitsSpinEdit";
            this.ActualDurationUnitsSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ActualDurationUnitsSpinEdit.Properties.Mask.EditMask = "n2";
            this.ActualDurationUnitsSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ActualDurationUnitsSpinEdit.Properties.MaxValue = new decimal(new int[] {
            99999,
            0,
            0,
            131072});
            this.ActualDurationUnitsSpinEdit.Size = new System.Drawing.Size(64, 20);
            this.ActualDurationUnitsSpinEdit.StyleController = this.dataLayoutControl1;
            this.ActualDurationUnitsSpinEdit.TabIndex = 17;
            this.ActualDurationUnitsSpinEdit.EditValueChanged += new System.EventHandler(this.ActualDurationUnitsSpinEdit_EditValueChanged);
            this.ActualDurationUnitsSpinEdit.Validated += new System.EventHandler(this.ActualDurationUnitsSpinEdit_Validated);
            // 
            // ActualStartDateDateEdit
            // 
            this.ActualStartDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "ActualStartDate", true));
            this.ActualStartDateDateEdit.EditValue = null;
            this.ActualStartDateDateEdit.Location = new System.Drawing.Point(561, 393);
            this.ActualStartDateDateEdit.MenuManager = this.barManager1;
            this.ActualStartDateDateEdit.Name = "ActualStartDateDateEdit";
            this.ActualStartDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ActualStartDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ActualStartDateDateEdit.Properties.Mask.EditMask = "G";
            this.ActualStartDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ActualStartDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.ActualStartDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.ActualStartDateDateEdit.Size = new System.Drawing.Size(118, 20);
            this.ActualStartDateDateEdit.StyleController = this.dataLayoutControl1;
            this.ActualStartDateDateEdit.TabIndex = 16;
            this.ActualStartDateDateEdit.EditValueChanged += new System.EventHandler(this.ActualStartDateDateEdit_EditValueChanged);
            this.ActualStartDateDateEdit.Validated += new System.EventHandler(this.ActualStartDateDateEdit_Validated);
            // 
            // ExpectedDurationUnitsDescriptorIDGridLookUpEdit
            // 
            this.ExpectedDurationUnitsDescriptorIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "ExpectedDurationUnitsDescriptorID", true));
            this.ExpectedDurationUnitsDescriptorIDGridLookUpEdit.Location = new System.Drawing.Point(390, 417);
            this.ExpectedDurationUnitsDescriptorIDGridLookUpEdit.MenuManager = this.barManager1;
            this.ExpectedDurationUnitsDescriptorIDGridLookUpEdit.Name = "ExpectedDurationUnitsDescriptorIDGridLookUpEdit";
            this.ExpectedDurationUnitsDescriptorIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ExpectedDurationUnitsDescriptorIDGridLookUpEdit.Properties.DataSource = this.sp06177OMJobDurationDescriptorsWithBlankBindingSource;
            this.ExpectedDurationUnitsDescriptorIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.ExpectedDurationUnitsDescriptorIDGridLookUpEdit.Properties.NullText = "";
            this.ExpectedDurationUnitsDescriptorIDGridLookUpEdit.Properties.PopupView = this.gridView6;
            this.ExpectedDurationUnitsDescriptorIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.ExpectedDurationUnitsDescriptorIDGridLookUpEdit.Size = new System.Drawing.Size(53, 20);
            this.ExpectedDurationUnitsDescriptorIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.ExpectedDurationUnitsDescriptorIDGridLookUpEdit.TabIndex = 14;
            this.ExpectedDurationUnitsDescriptorIDGridLookUpEdit.EditValueChanged += new System.EventHandler(this.ExpectedDurationUnitsDescriptorIDGridLookUpEdit_EditValueChanged);
            this.ExpectedDurationUnitsDescriptorIDGridLookUpEdit.Validated += new System.EventHandler(this.ExpectedDurationUnitsDescriptorIDGridLookUpEdit_Validated);
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView6.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule4.ApplyToRow = true;
            gridFormatRule4.Column = this.gridColumn1;
            gridFormatRule4.Name = "Format0";
            formatConditionRuleValue4.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue4.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue4.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue4.Value1 = 0;
            gridFormatRule4.Rule = formatConditionRuleValue4;
            this.gridView6.FormatRules.Add(gridFormatRule4);
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView6.OptionsLayout.StoreAppearance = true;
            this.gridView6.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView6.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView6.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView6.OptionsView.ShowIndicator = false;
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Unit Descriptor";
            this.gridColumn2.FieldName = "Description";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 220;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Order";
            this.gridColumn3.FieldName = "RecordOrder";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            // 
            // ExpectedDurationUnitsSpinEdit
            // 
            this.ExpectedDurationUnitsSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "ExpectedDurationUnits", true));
            this.ExpectedDurationUnitsSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ExpectedDurationUnitsSpinEdit.Location = new System.Drawing.Point(321, 417);
            this.ExpectedDurationUnitsSpinEdit.MenuManager = this.barManager1;
            this.ExpectedDurationUnitsSpinEdit.Name = "ExpectedDurationUnitsSpinEdit";
            this.ExpectedDurationUnitsSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ExpectedDurationUnitsSpinEdit.Properties.Mask.EditMask = "n2";
            this.ExpectedDurationUnitsSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ExpectedDurationUnitsSpinEdit.Properties.MaxValue = new decimal(new int[] {
            99999,
            0,
            0,
            131072});
            this.ExpectedDurationUnitsSpinEdit.Size = new System.Drawing.Size(65, 20);
            this.ExpectedDurationUnitsSpinEdit.StyleController = this.dataLayoutControl1;
            this.ExpectedDurationUnitsSpinEdit.TabIndex = 13;
            this.ExpectedDurationUnitsSpinEdit.EditValueChanged += new System.EventHandler(this.ExpectedDurationUnitsSpinEdit_EditValueChanged);
            this.ExpectedDurationUnitsSpinEdit.Validated += new System.EventHandler(this.ExpectedDurationUnitsSpinEdit_Validated);
            // 
            // FinanceSystemPONumberTextEdit
            // 
            this.FinanceSystemPONumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "FinanceSystemPONumber", true));
            this.FinanceSystemPONumberTextEdit.Location = new System.Drawing.Point(1175, 71);
            this.FinanceSystemPONumberTextEdit.MenuManager = this.barManager1;
            this.FinanceSystemPONumberTextEdit.Name = "FinanceSystemPONumberTextEdit";
            this.FinanceSystemPONumberTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.FinanceSystemPONumberTextEdit, true);
            this.FinanceSystemPONumberTextEdit.Size = new System.Drawing.Size(199, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.FinanceSystemPONumberTextEdit, optionsSpelling17);
            this.FinanceSystemPONumberTextEdit.StyleController = this.dataLayoutControl1;
            this.FinanceSystemPONumberTextEdit.TabIndex = 25;
            // 
            // RequiresAccessPermitCheckEdit
            // 
            this.RequiresAccessPermitCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "RequiresAccessPermit", true));
            this.RequiresAccessPermitCheckEdit.Location = new System.Drawing.Point(602, 187);
            this.RequiresAccessPermitCheckEdit.MenuManager = this.barManager1;
            this.RequiresAccessPermitCheckEdit.Name = "RequiresAccessPermitCheckEdit";
            this.RequiresAccessPermitCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.RequiresAccessPermitCheckEdit.Properties.ValueChecked = 1;
            this.RequiresAccessPermitCheckEdit.Properties.ValueUnchecked = 0;
            this.RequiresAccessPermitCheckEdit.Size = new System.Drawing.Size(76, 19);
            this.RequiresAccessPermitCheckEdit.StyleController = this.dataLayoutControl1;
            this.RequiresAccessPermitCheckEdit.TabIndex = 5;
            // 
            // JobNoLongerRequiredCheckEdit
            // 
            this.JobNoLongerRequiredCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "JobNoLongerRequired", true));
            this.JobNoLongerRequiredCheckEdit.Location = new System.Drawing.Point(150, 210);
            this.JobNoLongerRequiredCheckEdit.MenuManager = this.barManager1;
            this.JobNoLongerRequiredCheckEdit.Name = "JobNoLongerRequiredCheckEdit";
            this.JobNoLongerRequiredCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.JobNoLongerRequiredCheckEdit.Properties.ValueChecked = 1;
            this.JobNoLongerRequiredCheckEdit.Properties.ValueUnchecked = 0;
            this.JobNoLongerRequiredCheckEdit.Size = new System.Drawing.Size(83, 19);
            this.JobNoLongerRequiredCheckEdit.StyleController = this.dataLayoutControl1;
            this.JobNoLongerRequiredCheckEdit.TabIndex = 6;
            // 
            // ReactiveCheckEdit
            // 
            this.ReactiveCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "Reactive", true));
            this.ReactiveCheckEdit.Location = new System.Drawing.Point(150, 187);
            this.ReactiveCheckEdit.MenuManager = this.barManager1;
            this.ReactiveCheckEdit.Name = "ReactiveCheckEdit";
            this.ReactiveCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.ReactiveCheckEdit.Properties.ValueChecked = 1;
            this.ReactiveCheckEdit.Properties.ValueUnchecked = 0;
            this.ReactiveCheckEdit.Size = new System.Drawing.Size(83, 19);
            this.ReactiveCheckEdit.StyleController = this.dataLayoutControl1;
            this.ReactiveCheckEdit.TabIndex = 4;
            // 
            // JobTypeDescriptionTextEdit
            // 
            this.JobTypeDescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "JobTypeDescription", true));
            this.JobTypeDescriptionTextEdit.Location = new System.Drawing.Point(150, 117);
            this.JobTypeDescriptionTextEdit.MenuManager = this.barManager1;
            this.JobTypeDescriptionTextEdit.Name = "JobTypeDescriptionTextEdit";
            this.JobTypeDescriptionTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.JobTypeDescriptionTextEdit, true);
            this.JobTypeDescriptionTextEdit.Size = new System.Drawing.Size(174, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.JobTypeDescriptionTextEdit, optionsSpelling18);
            this.JobTypeDescriptionTextEdit.StyleController = this.dataLayoutControl1;
            this.JobTypeDescriptionTextEdit.TabIndex = 3;
            this.JobTypeDescriptionTextEdit.TabStop = false;
            // 
            // JobSubTypeDescriptionButtonEdit
            // 
            this.JobSubTypeDescriptionButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "JobSubTypeDescription", true));
            this.JobSubTypeDescriptionButtonEdit.Location = new System.Drawing.Point(454, 117);
            this.JobSubTypeDescriptionButtonEdit.MenuManager = this.barManager1;
            this.JobSubTypeDescriptionButtonEdit.Name = "JobSubTypeDescriptionButtonEdit";
            this.JobSubTypeDescriptionButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Click to Open the Select Job Type screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.JobSubTypeDescriptionButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.JobSubTypeDescriptionButtonEdit.Size = new System.Drawing.Size(237, 20);
            this.JobSubTypeDescriptionButtonEdit.StyleController = this.dataLayoutControl1;
            this.JobSubTypeDescriptionButtonEdit.TabIndex = 1;
            this.JobSubTypeDescriptionButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.JobSubTypeDescriptionButtonEdit_ButtonClick);
            this.JobSubTypeDescriptionButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.JobSubTypeDescriptionButtonEdit_Validating);
            // 
            // SiteNameTextEdit
            // 
            this.SiteNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "SiteName", true));
            this.SiteNameTextEdit.Location = new System.Drawing.Point(150, 93);
            this.SiteNameTextEdit.MenuManager = this.barManager1;
            this.SiteNameTextEdit.Name = "SiteNameTextEdit";
            this.SiteNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteNameTextEdit, true);
            this.SiteNameTextEdit.Size = new System.Drawing.Size(174, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteNameTextEdit, optionsSpelling19);
            this.SiteNameTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteNameTextEdit.TabIndex = 1;
            this.SiteNameTextEdit.TabStop = false;
            // 
            // ClientNameContractDescriptionTextEdit
            // 
            this.ClientNameContractDescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "ClientNameContractDescription", true));
            this.ClientNameContractDescriptionTextEdit.Location = new System.Drawing.Point(150, 69);
            this.ClientNameContractDescriptionTextEdit.MenuManager = this.barManager1;
            this.ClientNameContractDescriptionTextEdit.Name = "ClientNameContractDescriptionTextEdit";
            this.ClientNameContractDescriptionTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientNameContractDescriptionTextEdit, true);
            this.ClientNameContractDescriptionTextEdit.Size = new System.Drawing.Size(541, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientNameContractDescriptionTextEdit, optionsSpelling20);
            this.ClientNameContractDescriptionTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientNameContractDescriptionTextEdit.TabIndex = 0;
            this.ClientNameContractDescriptionTextEdit.TabStop = false;
            // 
            // JobStatusIDGridLookUpEdit
            // 
            this.JobStatusIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "JobStatusID", true));
            this.JobStatusIDGridLookUpEdit.Location = new System.Drawing.Point(150, 255);
            this.JobStatusIDGridLookUpEdit.MenuManager = this.barManager1;
            this.JobStatusIDGridLookUpEdit.Name = "JobStatusIDGridLookUpEdit";
            this.JobStatusIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.JobStatusIDGridLookUpEdit.Properties.DataSource = this.sp06208OMJObStatusesListBindingSource;
            this.JobStatusIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.JobStatusIDGridLookUpEdit.Properties.NullText = "";
            this.JobStatusIDGridLookUpEdit.Properties.PopupView = this.gridView5;
            this.JobStatusIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.JobStatusIDGridLookUpEdit.Size = new System.Drawing.Size(174, 20);
            this.JobStatusIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.JobStatusIDGridLookUpEdit.TabIndex = 2;
            this.JobStatusIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.JobStatusIDGridLookUpEdit_Validating);
            // 
            // sp06208OMJObStatusesListBindingSource
            // 
            this.sp06208OMJObStatusesListBindingSource.DataMember = "sp06208_OM_JOb_Statuses_List";
            this.sp06208OMJObStatusesListBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID1,
            this.colDescription,
            this.colOrder});
            this.gridView5.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colID1;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView5.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Status";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 220;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "RecordOrder";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            // 
            // VisitIDTextEdit
            // 
            this.VisitIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "VisitID", true));
            this.VisitIDTextEdit.Location = new System.Drawing.Point(154, 83);
            this.VisitIDTextEdit.MenuManager = this.barManager1;
            this.VisitIDTextEdit.Name = "VisitIDTextEdit";
            this.VisitIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.VisitIDTextEdit, true);
            this.VisitIDTextEdit.Size = new System.Drawing.Size(678, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.VisitIDTextEdit, optionsSpelling21);
            this.VisitIDTextEdit.StyleController = this.dataLayoutControl1;
            this.VisitIDTextEdit.TabIndex = 78;
            // 
            // JobTypeIDTextEdit
            // 
            this.JobTypeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "JobTypeID", true));
            this.JobTypeIDTextEdit.Location = new System.Drawing.Point(154, 59);
            this.JobTypeIDTextEdit.MenuManager = this.barManager1;
            this.JobTypeIDTextEdit.Name = "JobTypeIDTextEdit";
            this.JobTypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.JobTypeIDTextEdit, true);
            this.JobTypeIDTextEdit.Size = new System.Drawing.Size(678, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.JobTypeIDTextEdit, optionsSpelling22);
            this.JobTypeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.JobTypeIDTextEdit.TabIndex = 71;
            // 
            // JobIDTextEdit
            // 
            this.JobIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "JobID", true));
            this.JobIDTextEdit.Location = new System.Drawing.Point(154, 155);
            this.JobIDTextEdit.MenuManager = this.barManager1;
            this.JobIDTextEdit.Name = "JobIDTextEdit";
            this.JobIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.JobIDTextEdit, true);
            this.JobIDTextEdit.Size = new System.Drawing.Size(678, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.JobIDTextEdit, optionsSpelling23);
            this.JobIDTextEdit.StyleController = this.dataLayoutControl1;
            this.JobIDTextEdit.TabIndex = 70;
            // 
            // ExpectedEndDateDateEdit
            // 
            this.ExpectedEndDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "ExpectedEndDate", true));
            this.ExpectedEndDateDateEdit.EditValue = null;
            this.ExpectedEndDateDateEdit.Location = new System.Drawing.Point(321, 441);
            this.ExpectedEndDateDateEdit.MenuManager = this.barManager1;
            this.ExpectedEndDateDateEdit.Name = "ExpectedEndDateDateEdit";
            this.ExpectedEndDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ExpectedEndDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ExpectedEndDateDateEdit.Properties.Mask.EditMask = "G";
            this.ExpectedEndDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ExpectedEndDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.ExpectedEndDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.ExpectedEndDateDateEdit.Size = new System.Drawing.Size(122, 20);
            this.ExpectedEndDateDateEdit.StyleController = this.dataLayoutControl1;
            this.ExpectedEndDateDateEdit.TabIndex = 15;
            this.ExpectedEndDateDateEdit.EditValueChanged += new System.EventHandler(this.ExpectedEndDateDateEdit_EditValueChanged);
            this.ExpectedEndDateDateEdit.Validated += new System.EventHandler(this.ExpectedEndDateDateEdit_Validated);
            // 
            // ClientInvoiceIDTextEdit
            // 
            this.ClientInvoiceIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "ClientInvoiceID", true));
            this.ClientInvoiceIDTextEdit.Location = new System.Drawing.Point(1165, 186);
            this.ClientInvoiceIDTextEdit.MenuManager = this.barManager1;
            this.ClientInvoiceIDTextEdit.Name = "ClientInvoiceIDTextEdit";
            this.ClientInvoiceIDTextEdit.Properties.MaxLength = 50;
            this.ClientInvoiceIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientInvoiceIDTextEdit, true);
            this.ClientInvoiceIDTextEdit.Size = new System.Drawing.Size(197, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientInvoiceIDTextEdit, optionsSpelling24);
            this.ClientInvoiceIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientInvoiceIDTextEdit.TabIndex = 93;
            // 
            // btnSave
            // 
            this.btnSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.ImageOptions.Image")));
            this.btnSave.Location = new System.Drawing.Point(24, 763);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(87, 22);
            this.btnSave.StyleController = this.dataLayoutControl1;
            this.btnSave.TabIndex = 11;
            this.btnSave.Text = "Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Grid = this.gridControlLabour;
            this.gridSplitContainer1.Location = new System.Drawing.Point(36, 832);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControlLabour);
            this.gridSplitContainer1.Size = new System.Drawing.Size(643, 126);
            this.gridSplitContainer1.TabIndex = 79;
            // 
            // gridControlLabour
            // 
            this.gridControlLabour.DataSource = this.sp06033OMJobManagerLinkedlabourBindingSource;
            this.gridControlLabour.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlLabour.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlLabour.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlLabour.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "View Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Reload Data", "reload"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 8, true, true, "Linked Documents", "linked_document")});
            this.gridControlLabour.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlLabour_EmbeddedNavigator_ButtonClick);
            this.gridControlLabour.Location = new System.Drawing.Point(0, 0);
            this.gridControlLabour.MainView = this.gridViewLabour;
            this.gridControlLabour.MenuManager = this.barManager1;
            this.gridControlLabour.Name = "gridControlLabour";
            this.gridControlLabour.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemTextEditCurrency,
            this.repositoryItemTextEdit2DP,
            this.repositoryItemTextEditPercentage,
            this.repositoryItemTextEditInteger});
            this.gridControlLabour.Size = new System.Drawing.Size(643, 126);
            this.gridControlLabour.TabIndex = 46;
            this.gridControlLabour.UseEmbeddedNavigator = true;
            this.gridControlLabour.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewLabour});
            // 
            // sp06033OMJobManagerLinkedlabourBindingSource
            // 
            this.sp06033OMJobManagerLinkedlabourBindingSource.DataMember = "sp06033_OM_Job_Manager_Linked_labour";
            this.sp06033OMJobManagerLinkedlabourBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridViewLabour
            // 
            this.gridViewLabour.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLabourUsedID,
            this.colContractorID,
            this.colJobID1,
            this.colCostUnitsUsed,
            this.colCostUnitDescriptorID,
            this.colSellUnitsUsed,
            this.colSellUnitDescriptorID,
            this.colCostPerUnitExVat,
            this.colCostPerUnitVatRate,
            this.colSellPerUnitExVat,
            this.colSellPerUnitVatRate,
            this.colCostValueExVat,
            this.colCostValueVat,
            this.colCostValueTotal,
            this.colSellValueExVat,
            this.colSellValueVat,
            this.colSellValueTotal,
            this.colRemarks,
            this.colClientID,
            this.colSiteID,
            this.colSiteName1,
            this.colClientName1,
            this.colJobSubTypeDescription1,
            this.colJobTypeDescription2,
            this.colExpectedStartDate1,
            this.colVisitNumber1,
            this.colFullDescription,
            this.colContractorName,
            this.colSelected1,
            this.colCostUnitDescriptor,
            this.colSellUnitDescriptor,
            this.colLabourType,
            this.colCISPercentage,
            this.colCISValue,
            this.colPurchaseOrderID,
            this.colStartDateTime,
            this.colEndDateTime,
            this.colVisitID1});
            this.gridViewLabour.GridControl = this.gridControlLabour;
            this.gridViewLabour.Name = "gridViewLabour";
            this.gridViewLabour.OptionsCustomization.AllowFilter = false;
            this.gridViewLabour.OptionsFilter.AllowFilterEditor = false;
            this.gridViewLabour.OptionsFilter.AllowMRUFilterList = false;
            this.gridViewLabour.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewLabour.OptionsLayout.StoreAppearance = true;
            this.gridViewLabour.OptionsLayout.StoreFormatRules = true;
            this.gridViewLabour.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewLabour.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewLabour.OptionsSelection.MultiSelect = true;
            this.gridViewLabour.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewLabour.OptionsView.ColumnAutoWidth = false;
            this.gridViewLabour.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewLabour.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewLabour.OptionsView.ShowGroupPanel = false;
            this.gridViewLabour.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colContractorName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewLabour.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewLabour.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewLabour.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewLabour.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewLabour.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewLabour.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewLabour_MouseUp);
            this.gridViewLabour.DoubleClick += new System.EventHandler(this.gridViewLabour_DoubleClick);
            this.gridViewLabour.GotFocus += new System.EventHandler(this.gridViewLabour_GotFocus);
            // 
            // colLabourUsedID
            // 
            this.colLabourUsedID.Caption = "Labour Used ID";
            this.colLabourUsedID.FieldName = "LabourUsedID";
            this.colLabourUsedID.Name = "colLabourUsedID";
            this.colLabourUsedID.OptionsColumn.AllowEdit = false;
            this.colLabourUsedID.OptionsColumn.AllowFocus = false;
            this.colLabourUsedID.OptionsColumn.ReadOnly = true;
            this.colLabourUsedID.Width = 108;
            // 
            // colContractorID
            // 
            this.colContractorID.Caption = "Contractor ID";
            this.colContractorID.FieldName = "ContractorID";
            this.colContractorID.Name = "colContractorID";
            this.colContractorID.OptionsColumn.AllowEdit = false;
            this.colContractorID.OptionsColumn.AllowFocus = false;
            this.colContractorID.OptionsColumn.ReadOnly = true;
            // 
            // colJobID1
            // 
            this.colJobID1.Caption = "Job ID";
            this.colJobID1.FieldName = "JobID";
            this.colJobID1.Name = "colJobID1";
            this.colJobID1.OptionsColumn.AllowEdit = false;
            this.colJobID1.OptionsColumn.AllowFocus = false;
            this.colJobID1.OptionsColumn.ReadOnly = true;
            // 
            // colCostUnitsUsed
            // 
            this.colCostUnitsUsed.Caption = "Cost Units Used";
            this.colCostUnitsUsed.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colCostUnitsUsed.FieldName = "CostUnitsUsed";
            this.colCostUnitsUsed.Name = "colCostUnitsUsed";
            this.colCostUnitsUsed.OptionsColumn.AllowEdit = false;
            this.colCostUnitsUsed.OptionsColumn.AllowFocus = false;
            this.colCostUnitsUsed.OptionsColumn.ReadOnly = true;
            this.colCostUnitsUsed.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostUnitsUsed", "{0:n2}")});
            this.colCostUnitsUsed.Visible = true;
            this.colCostUnitsUsed.VisibleIndex = 5;
            this.colCostUnitsUsed.Width = 97;
            // 
            // repositoryItemTextEdit2DP
            // 
            this.repositoryItemTextEdit2DP.AutoHeight = false;
            this.repositoryItemTextEdit2DP.Mask.EditMask = "n2";
            this.repositoryItemTextEdit2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP.Name = "repositoryItemTextEdit2DP";
            // 
            // colCostUnitDescriptorID
            // 
            this.colCostUnitDescriptorID.Caption = "Cost Unit Descriptor ID";
            this.colCostUnitDescriptorID.FieldName = "CostUnitDescriptorID";
            this.colCostUnitDescriptorID.Name = "colCostUnitDescriptorID";
            this.colCostUnitDescriptorID.OptionsColumn.AllowEdit = false;
            this.colCostUnitDescriptorID.OptionsColumn.AllowFocus = false;
            this.colCostUnitDescriptorID.OptionsColumn.ReadOnly = true;
            this.colCostUnitDescriptorID.Width = 131;
            // 
            // colSellUnitsUsed
            // 
            this.colSellUnitsUsed.Caption = "Sell Units Used";
            this.colSellUnitsUsed.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colSellUnitsUsed.FieldName = "SellUnitsUsed";
            this.colSellUnitsUsed.Name = "colSellUnitsUsed";
            this.colSellUnitsUsed.OptionsColumn.AllowEdit = false;
            this.colSellUnitsUsed.OptionsColumn.AllowFocus = false;
            this.colSellUnitsUsed.OptionsColumn.ReadOnly = true;
            this.colSellUnitsUsed.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellUnitsUsed", "{0:n2}")});
            this.colSellUnitsUsed.Visible = true;
            this.colSellUnitsUsed.VisibleIndex = 11;
            this.colSellUnitsUsed.Width = 91;
            // 
            // colSellUnitDescriptorID
            // 
            this.colSellUnitDescriptorID.Caption = "Sell Unit Descriptor ID";
            this.colSellUnitDescriptorID.FieldName = "SellUnitDescriptorID";
            this.colSellUnitDescriptorID.Name = "colSellUnitDescriptorID";
            this.colSellUnitDescriptorID.OptionsColumn.AllowEdit = false;
            this.colSellUnitDescriptorID.OptionsColumn.AllowFocus = false;
            this.colSellUnitDescriptorID.OptionsColumn.ReadOnly = true;
            this.colSellUnitDescriptorID.Width = 125;
            // 
            // colCostPerUnitExVat
            // 
            this.colCostPerUnitExVat.Caption = "Cost Per Unit Ex VAT";
            this.colCostPerUnitExVat.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCostPerUnitExVat.FieldName = "CostPerUnitExVat";
            this.colCostPerUnitExVat.Name = "colCostPerUnitExVat";
            this.colCostPerUnitExVat.OptionsColumn.AllowEdit = false;
            this.colCostPerUnitExVat.OptionsColumn.AllowFocus = false;
            this.colCostPerUnitExVat.OptionsColumn.ReadOnly = true;
            this.colCostPerUnitExVat.Visible = true;
            this.colCostPerUnitExVat.VisibleIndex = 6;
            this.colCostPerUnitExVat.Width = 121;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // colCostPerUnitVatRate
            // 
            this.colCostPerUnitVatRate.Caption = "Cost VAT Rate";
            this.colCostPerUnitVatRate.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colCostPerUnitVatRate.FieldName = "CostPerUnitVatRate";
            this.colCostPerUnitVatRate.Name = "colCostPerUnitVatRate";
            this.colCostPerUnitVatRate.OptionsColumn.AllowEdit = false;
            this.colCostPerUnitVatRate.OptionsColumn.AllowFocus = false;
            this.colCostPerUnitVatRate.OptionsColumn.ReadOnly = true;
            this.colCostPerUnitVatRate.Visible = true;
            this.colCostPerUnitVatRate.VisibleIndex = 7;
            this.colCostPerUnitVatRate.Width = 91;
            // 
            // repositoryItemTextEditPercentage
            // 
            this.repositoryItemTextEditPercentage.AutoHeight = false;
            this.repositoryItemTextEditPercentage.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage.Name = "repositoryItemTextEditPercentage";
            // 
            // colSellPerUnitExVat
            // 
            this.colSellPerUnitExVat.Caption = "Sell Per Unit Ex VAT";
            this.colSellPerUnitExVat.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSellPerUnitExVat.FieldName = "SellPerUnitExVat";
            this.colSellPerUnitExVat.Name = "colSellPerUnitExVat";
            this.colSellPerUnitExVat.OptionsColumn.AllowEdit = false;
            this.colSellPerUnitExVat.OptionsColumn.AllowFocus = false;
            this.colSellPerUnitExVat.OptionsColumn.ReadOnly = true;
            this.colSellPerUnitExVat.Visible = true;
            this.colSellPerUnitExVat.VisibleIndex = 12;
            this.colSellPerUnitExVat.Width = 115;
            // 
            // colSellPerUnitVatRate
            // 
            this.colSellPerUnitVatRate.Caption = "Sell VAT Rate";
            this.colSellPerUnitVatRate.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colSellPerUnitVatRate.FieldName = "SellPerUnitVatRate";
            this.colSellPerUnitVatRate.Name = "colSellPerUnitVatRate";
            this.colSellPerUnitVatRate.OptionsColumn.AllowEdit = false;
            this.colSellPerUnitVatRate.OptionsColumn.AllowFocus = false;
            this.colSellPerUnitVatRate.OptionsColumn.ReadOnly = true;
            this.colSellPerUnitVatRate.Visible = true;
            this.colSellPerUnitVatRate.VisibleIndex = 13;
            this.colSellPerUnitVatRate.Width = 85;
            // 
            // colCostValueExVat
            // 
            this.colCostValueExVat.Caption = "Cost Ex VAT";
            this.colCostValueExVat.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCostValueExVat.FieldName = "CostValueExVat";
            this.colCostValueExVat.Name = "colCostValueExVat";
            this.colCostValueExVat.OptionsColumn.AllowEdit = false;
            this.colCostValueExVat.OptionsColumn.AllowFocus = false;
            this.colCostValueExVat.OptionsColumn.ReadOnly = true;
            this.colCostValueExVat.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostValueExVat", "{0:c}")});
            this.colCostValueExVat.Visible = true;
            this.colCostValueExVat.VisibleIndex = 8;
            this.colCostValueExVat.Width = 80;
            // 
            // colCostValueVat
            // 
            this.colCostValueVat.Caption = "Cost VAT";
            this.colCostValueVat.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCostValueVat.FieldName = "CostValueVat";
            this.colCostValueVat.Name = "colCostValueVat";
            this.colCostValueVat.OptionsColumn.AllowEdit = false;
            this.colCostValueVat.OptionsColumn.AllowFocus = false;
            this.colCostValueVat.OptionsColumn.ReadOnly = true;
            this.colCostValueVat.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostValueVat", "{0:c}")});
            this.colCostValueVat.Visible = true;
            this.colCostValueVat.VisibleIndex = 9;
            // 
            // colCostValueTotal
            // 
            this.colCostValueTotal.Caption = "Cost Total";
            this.colCostValueTotal.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCostValueTotal.FieldName = "CostValueTotal";
            this.colCostValueTotal.Name = "colCostValueTotal";
            this.colCostValueTotal.OptionsColumn.AllowEdit = false;
            this.colCostValueTotal.OptionsColumn.AllowFocus = false;
            this.colCostValueTotal.OptionsColumn.ReadOnly = true;
            this.colCostValueTotal.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostValueTotal", "{0:c}")});
            this.colCostValueTotal.Visible = true;
            this.colCostValueTotal.VisibleIndex = 10;
            // 
            // colSellValueExVat
            // 
            this.colSellValueExVat.Caption = "Sell Ex VAT";
            this.colSellValueExVat.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSellValueExVat.FieldName = "SellValueExVat";
            this.colSellValueExVat.Name = "colSellValueExVat";
            this.colSellValueExVat.OptionsColumn.AllowEdit = false;
            this.colSellValueExVat.OptionsColumn.AllowFocus = false;
            this.colSellValueExVat.OptionsColumn.ReadOnly = true;
            this.colSellValueExVat.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellValueExVat", "{0:c}")});
            this.colSellValueExVat.Visible = true;
            this.colSellValueExVat.VisibleIndex = 14;
            // 
            // colSellValueVat
            // 
            this.colSellValueVat.Caption = "Sell VAT";
            this.colSellValueVat.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSellValueVat.FieldName = "SellValueVat";
            this.colSellValueVat.Name = "colSellValueVat";
            this.colSellValueVat.OptionsColumn.AllowEdit = false;
            this.colSellValueVat.OptionsColumn.AllowFocus = false;
            this.colSellValueVat.OptionsColumn.ReadOnly = true;
            this.colSellValueVat.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellValueVat", "{0:c}")});
            this.colSellValueVat.Visible = true;
            this.colSellValueVat.VisibleIndex = 15;
            // 
            // colSellValueTotal
            // 
            this.colSellValueTotal.Caption = "Sell Total";
            this.colSellValueTotal.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSellValueTotal.FieldName = "SellValueTotal";
            this.colSellValueTotal.Name = "colSellValueTotal";
            this.colSellValueTotal.OptionsColumn.AllowEdit = false;
            this.colSellValueTotal.OptionsColumn.AllowFocus = false;
            this.colSellValueTotal.OptionsColumn.ReadOnly = true;
            this.colSellValueTotal.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellValueTotal", "{0:c}")});
            this.colSellValueTotal.Visible = true;
            this.colSellValueTotal.VisibleIndex = 16;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 19;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // colSiteID
            // 
            this.colSiteID.Caption = "Site ID";
            this.colSiteID.FieldName = "SiteID";
            this.colSiteID.Name = "colSiteID";
            this.colSiteID.OptionsColumn.AllowEdit = false;
            this.colSiteID.OptionsColumn.AllowFocus = false;
            this.colSiteID.OptionsColumn.ReadOnly = true;
            // 
            // colSiteName1
            // 
            this.colSiteName1.Caption = "Site Name";
            this.colSiteName1.FieldName = "SiteName";
            this.colSiteName1.Name = "colSiteName1";
            this.colSiteName1.OptionsColumn.AllowEdit = false;
            this.colSiteName1.OptionsColumn.AllowFocus = false;
            this.colSiteName1.OptionsColumn.ReadOnly = true;
            this.colSiteName1.Width = 133;
            // 
            // colClientName1
            // 
            this.colClientName1.Caption = "Client Name";
            this.colClientName1.FieldName = "ClientName";
            this.colClientName1.Name = "colClientName1";
            this.colClientName1.OptionsColumn.AllowEdit = false;
            this.colClientName1.OptionsColumn.AllowFocus = false;
            this.colClientName1.OptionsColumn.ReadOnly = true;
            this.colClientName1.Width = 139;
            // 
            // colJobSubTypeDescription1
            // 
            this.colJobSubTypeDescription1.Caption = "Job Sub-Type";
            this.colJobSubTypeDescription1.FieldName = "JobSubTypeDescription";
            this.colJobSubTypeDescription1.Name = "colJobSubTypeDescription1";
            this.colJobSubTypeDescription1.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeDescription1.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeDescription1.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeDescription1.Width = 150;
            // 
            // colJobTypeDescription2
            // 
            this.colJobTypeDescription2.Caption = "Job Type";
            this.colJobTypeDescription2.FieldName = "JobTypeDescription";
            this.colJobTypeDescription2.Name = "colJobTypeDescription2";
            this.colJobTypeDescription2.OptionsColumn.AllowEdit = false;
            this.colJobTypeDescription2.OptionsColumn.AllowFocus = false;
            this.colJobTypeDescription2.OptionsColumn.ReadOnly = true;
            this.colJobTypeDescription2.Width = 169;
            // 
            // colExpectedStartDate1
            // 
            this.colExpectedStartDate1.Caption = "Expected Start Date";
            this.colExpectedStartDate1.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colExpectedStartDate1.FieldName = "ExpectedStartDate";
            this.colExpectedStartDate1.Name = "colExpectedStartDate1";
            this.colExpectedStartDate1.OptionsColumn.AllowEdit = false;
            this.colExpectedStartDate1.OptionsColumn.AllowFocus = false;
            this.colExpectedStartDate1.OptionsColumn.ReadOnly = true;
            this.colExpectedStartDate1.Visible = true;
            this.colExpectedStartDate1.VisibleIndex = 2;
            this.colExpectedStartDate1.Width = 119;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "d";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colVisitNumber1
            // 
            this.colVisitNumber1.Caption = "Visit #";
            this.colVisitNumber1.ColumnEdit = this.repositoryItemTextEditInteger;
            this.colVisitNumber1.FieldName = "VisitNumber";
            this.colVisitNumber1.Name = "colVisitNumber1";
            this.colVisitNumber1.OptionsColumn.AllowEdit = false;
            this.colVisitNumber1.OptionsColumn.AllowFocus = false;
            this.colVisitNumber1.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemTextEditInteger
            // 
            this.repositoryItemTextEditInteger.AutoHeight = false;
            this.repositoryItemTextEditInteger.Mask.EditMask = "n0";
            this.repositoryItemTextEditInteger.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditInteger.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditInteger.Name = "repositoryItemTextEditInteger";
            // 
            // colFullDescription
            // 
            this.colFullDescription.Caption = "Linked To Job";
            this.colFullDescription.FieldName = "FullDescription";
            this.colFullDescription.Name = "colFullDescription";
            this.colFullDescription.OptionsColumn.AllowEdit = false;
            this.colFullDescription.OptionsColumn.AllowFocus = false;
            this.colFullDescription.OptionsColumn.ReadOnly = true;
            this.colFullDescription.Width = 415;
            // 
            // colContractorName
            // 
            this.colContractorName.Caption = "Contractor Name";
            this.colContractorName.FieldName = "ContractorName";
            this.colContractorName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colContractorName.Name = "colContractorName";
            this.colContractorName.OptionsColumn.AllowEdit = false;
            this.colContractorName.OptionsColumn.AllowFocus = false;
            this.colContractorName.OptionsColumn.ReadOnly = true;
            this.colContractorName.Visible = true;
            this.colContractorName.VisibleIndex = 0;
            this.colContractorName.Width = 181;
            // 
            // colSelected1
            // 
            this.colSelected1.Caption = "Selected";
            this.colSelected1.FieldName = "Selected";
            this.colSelected1.Name = "colSelected1";
            this.colSelected1.OptionsColumn.AllowEdit = false;
            this.colSelected1.OptionsColumn.AllowFocus = false;
            this.colSelected1.OptionsColumn.ReadOnly = true;
            this.colSelected1.Width = 62;
            // 
            // colCostUnitDescriptor
            // 
            this.colCostUnitDescriptor.Caption = "Cost Unit Descriptor";
            this.colCostUnitDescriptor.FieldName = "CostUnitDescriptor";
            this.colCostUnitDescriptor.Name = "colCostUnitDescriptor";
            this.colCostUnitDescriptor.OptionsColumn.AllowEdit = false;
            this.colCostUnitDescriptor.OptionsColumn.AllowFocus = false;
            this.colCostUnitDescriptor.OptionsColumn.ReadOnly = true;
            this.colCostUnitDescriptor.Width = 117;
            // 
            // colSellUnitDescriptor
            // 
            this.colSellUnitDescriptor.Caption = "Sell Unit Descriptor";
            this.colSellUnitDescriptor.FieldName = "SellUnitDescriptor";
            this.colSellUnitDescriptor.Name = "colSellUnitDescriptor";
            this.colSellUnitDescriptor.OptionsColumn.AllowEdit = false;
            this.colSellUnitDescriptor.OptionsColumn.AllowFocus = false;
            this.colSellUnitDescriptor.OptionsColumn.ReadOnly = true;
            this.colSellUnitDescriptor.Width = 111;
            // 
            // colLabourType
            // 
            this.colLabourType.Caption = "Labour Type";
            this.colLabourType.FieldName = "LabourType";
            this.colLabourType.Name = "colLabourType";
            this.colLabourType.OptionsColumn.AllowEdit = false;
            this.colLabourType.OptionsColumn.AllowFocus = false;
            this.colLabourType.OptionsColumn.ReadOnly = true;
            this.colLabourType.Visible = true;
            this.colLabourType.VisibleIndex = 1;
            this.colLabourType.Width = 114;
            // 
            // colCISPercentage
            // 
            this.colCISPercentage.Caption = "CIS %";
            this.colCISPercentage.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colCISPercentage.FieldName = "CISPercentage";
            this.colCISPercentage.Name = "colCISPercentage";
            this.colCISPercentage.OptionsColumn.AllowEdit = false;
            this.colCISPercentage.OptionsColumn.AllowFocus = false;
            this.colCISPercentage.OptionsColumn.ReadOnly = true;
            this.colCISPercentage.Visible = true;
            this.colCISPercentage.VisibleIndex = 17;
            // 
            // colCISValue
            // 
            this.colCISValue.Caption = "CIS Value";
            this.colCISValue.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCISValue.FieldName = "CISValue";
            this.colCISValue.Name = "colCISValue";
            this.colCISValue.OptionsColumn.AllowEdit = false;
            this.colCISValue.OptionsColumn.AllowFocus = false;
            this.colCISValue.OptionsColumn.ReadOnly = true;
            this.colCISValue.Visible = true;
            this.colCISValue.VisibleIndex = 18;
            // 
            // colPurchaseOrderID
            // 
            this.colPurchaseOrderID.Caption = "PO ID";
            this.colPurchaseOrderID.FieldName = "PurchaseOrderID";
            this.colPurchaseOrderID.Name = "colPurchaseOrderID";
            this.colPurchaseOrderID.OptionsColumn.AllowEdit = false;
            this.colPurchaseOrderID.OptionsColumn.AllowFocus = false;
            this.colPurchaseOrderID.OptionsColumn.ReadOnly = true;
            this.colPurchaseOrderID.Width = 49;
            // 
            // colStartDateTime
            // 
            this.colStartDateTime.Caption = "Start Date\\Time";
            this.colStartDateTime.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colStartDateTime.FieldName = "StartDateTime";
            this.colStartDateTime.Name = "colStartDateTime";
            this.colStartDateTime.OptionsColumn.AllowEdit = false;
            this.colStartDateTime.OptionsColumn.AllowFocus = false;
            this.colStartDateTime.OptionsColumn.ReadOnly = true;
            this.colStartDateTime.Visible = true;
            this.colStartDateTime.VisibleIndex = 3;
            this.colStartDateTime.Width = 97;
            // 
            // colEndDateTime
            // 
            this.colEndDateTime.Caption = "End Date\\Time";
            this.colEndDateTime.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colEndDateTime.FieldName = "EndDateTime";
            this.colEndDateTime.Name = "colEndDateTime";
            this.colEndDateTime.OptionsColumn.AllowEdit = false;
            this.colEndDateTime.OptionsColumn.AllowFocus = false;
            this.colEndDateTime.OptionsColumn.ReadOnly = true;
            this.colEndDateTime.Visible = true;
            this.colEndDateTime.VisibleIndex = 4;
            this.colEndDateTime.Width = 91;
            // 
            // colVisitID1
            // 
            this.colVisitID1.Caption = "Visit ID";
            this.colVisitID1.FieldName = "VisitID";
            this.colVisitID1.Name = "colVisitID1";
            this.colVisitID1.OptionsColumn.AllowEdit = false;
            this.colVisitID1.OptionsColumn.AllowFocus = false;
            this.colVisitID1.OptionsColumn.ReadOnly = true;
            this.colVisitID1.Width = 54;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp06174OMJobEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(12, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(189, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 24;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // JobSubTypeIDTextEdit
            // 
            this.JobSubTypeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "JobSubTypeID", true));
            this.JobSubTypeIDTextEdit.Location = new System.Drawing.Point(154, 59);
            this.JobSubTypeIDTextEdit.MenuManager = this.barManager1;
            this.JobSubTypeIDTextEdit.Name = "JobSubTypeIDTextEdit";
            this.JobSubTypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.JobSubTypeIDTextEdit, true);
            this.JobSubTypeIDTextEdit.Size = new System.Drawing.Size(678, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.JobSubTypeIDTextEdit, optionsSpelling25);
            this.JobSubTypeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.JobSubTypeIDTextEdit.TabIndex = 4;
            // 
            // CreatedByStaffIDTextEdit
            // 
            this.CreatedByStaffIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "CreatedByStaffID", true));
            this.CreatedByStaffIDTextEdit.Location = new System.Drawing.Point(154, 83);
            this.CreatedByStaffIDTextEdit.MenuManager = this.barManager1;
            this.CreatedByStaffIDTextEdit.Name = "CreatedByStaffIDTextEdit";
            this.CreatedByStaffIDTextEdit.Properties.MaxLength = 50;
            this.CreatedByStaffIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CreatedByStaffIDTextEdit, true);
            this.CreatedByStaffIDTextEdit.Size = new System.Drawing.Size(678, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CreatedByStaffIDTextEdit, optionsSpelling26);
            this.CreatedByStaffIDTextEdit.StyleController = this.dataLayoutControl1;
            this.CreatedByStaffIDTextEdit.TabIndex = 6;
            // 
            // VisitNumberButtonEdit
            // 
            this.VisitNumberButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "VisitNumber", true));
            this.VisitNumberButtonEdit.Location = new System.Drawing.Point(454, 93);
            this.VisitNumberButtonEdit.MenuManager = this.barManager1;
            this.VisitNumberButtonEdit.Name = "VisitNumberButtonEdit";
            this.VisitNumberButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Click to Open the Select Visit screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.VisitNumberButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.VisitNumberButtonEdit.Size = new System.Drawing.Size(237, 20);
            this.VisitNumberButtonEdit.StyleController = this.dataLayoutControl1;
            this.VisitNumberButtonEdit.TabIndex = 0;
            this.VisitNumberButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.VisitNumberButtonEdit_ButtonClick);
            this.VisitNumberButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.VisitNumberButtonEdit_Validating);
            // 
            // ExpectedStartDateDateEdit
            // 
            this.ExpectedStartDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "ExpectedStartDate", true));
            this.ExpectedStartDateDateEdit.EditValue = null;
            this.ExpectedStartDateDateEdit.Location = new System.Drawing.Point(321, 393);
            this.ExpectedStartDateDateEdit.MenuManager = this.barManager1;
            this.ExpectedStartDateDateEdit.Name = "ExpectedStartDateDateEdit";
            this.ExpectedStartDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ExpectedStartDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ExpectedStartDateDateEdit.Properties.Mask.EditMask = "G";
            this.ExpectedStartDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ExpectedStartDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.ExpectedStartDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.ExpectedStartDateDateEdit.Size = new System.Drawing.Size(122, 20);
            this.ExpectedStartDateDateEdit.StyleController = this.dataLayoutControl1;
            this.ExpectedStartDateDateEdit.TabIndex = 12;
            this.ExpectedStartDateDateEdit.EditValueChanged += new System.EventHandler(this.ExpectedStartDateDateEdit_EditValueChanged);
            this.ExpectedStartDateDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ExpectedStartDateDateEdit_Validating);
            this.ExpectedStartDateDateEdit.Validated += new System.EventHandler(this.ExpectedStartDateDateEdit_Validated);
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06174OMJobEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, 637);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.RemarksMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(643, 110);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling27);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 3;
            // 
            // ItemForJobID
            // 
            this.ItemForJobID.Control = this.JobIDTextEdit;
            this.ItemForJobID.CustomizationFormText = "Job ID:";
            this.ItemForJobID.Location = new System.Drawing.Point(0, 143);
            this.ItemForJobID.Name = "ItemForJobID";
            this.ItemForJobID.Size = new System.Drawing.Size(824, 24);
            this.ItemForJobID.Text = "Job ID:";
            this.ItemForJobID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForVisitID
            // 
            this.ItemForVisitID.Control = this.VisitIDTextEdit;
            this.ItemForVisitID.CustomizationFormText = "Visit ID:";
            this.ItemForVisitID.Location = new System.Drawing.Point(0, 71);
            this.ItemForVisitID.Name = "ItemForVisitID";
            this.ItemForVisitID.Size = new System.Drawing.Size(824, 24);
            this.ItemForVisitID.Text = "Visit ID:";
            this.ItemForVisitID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForJobSubTypeID
            // 
            this.ItemForJobSubTypeID.Control = this.JobSubTypeIDTextEdit;
            this.ItemForJobSubTypeID.CustomizationFormText = "Job Sub-Type ID:";
            this.ItemForJobSubTypeID.Location = new System.Drawing.Point(0, 47);
            this.ItemForJobSubTypeID.Name = "ItemForJobSubTypeID";
            this.ItemForJobSubTypeID.Size = new System.Drawing.Size(824, 24);
            this.ItemForJobSubTypeID.Text = "Job Sub-Type ID:";
            this.ItemForJobSubTypeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForJobTypeID
            // 
            this.ItemForJobTypeID.Control = this.JobTypeIDTextEdit;
            this.ItemForJobTypeID.CustomizationFormText = "Job Type ID:";
            this.ItemForJobTypeID.Location = new System.Drawing.Point(0, 47);
            this.ItemForJobTypeID.Name = "ItemForJobTypeID";
            this.ItemForJobTypeID.Size = new System.Drawing.Size(824, 24);
            this.ItemForJobTypeID.Text = "Job Type ID:";
            this.ItemForJobTypeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForCreatedByStaffID
            // 
            this.ItemForCreatedByStaffID.Control = this.CreatedByStaffIDTextEdit;
            this.ItemForCreatedByStaffID.CustomizationFormText = "Created By Staff ID:";
            this.ItemForCreatedByStaffID.Location = new System.Drawing.Point(0, 71);
            this.ItemForCreatedByStaffID.Name = "ItemForCreatedByStaffID";
            this.ItemForCreatedByStaffID.Size = new System.Drawing.Size(824, 24);
            this.ItemForCreatedByStaffID.Text = "Created By Staff ID:";
            this.ItemForCreatedByStaffID.TextSize = new System.Drawing.Size(83, 13);
            // 
            // ItemForAccessPermitID
            // 
            this.ItemForAccessPermitID.Control = this.AccessPermitIDTextEdit;
            this.ItemForAccessPermitID.CustomizationFormText = "Access Permit ID:";
            this.ItemForAccessPermitID.Location = new System.Drawing.Point(0, 190);
            this.ItemForAccessPermitID.Name = "ItemForAccessPermitID";
            this.ItemForAccessPermitID.Size = new System.Drawing.Size(824, 24);
            this.ItemForAccessPermitID.Text = "Access Permit ID:";
            this.ItemForAccessPermitID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForTenderID
            // 
            this.ItemForTenderID.Control = this.TenderIDTextEdit;
            this.ItemForTenderID.CustomizationFormText = "Tender ID:";
            this.ItemForTenderID.Location = new System.Drawing.Point(0, 264);
            this.ItemForTenderID.Name = "ItemForTenderID";
            this.ItemForTenderID.Size = new System.Drawing.Size(776, 24);
            this.ItemForTenderID.Text = "Tender ID:";
            this.ItemForTenderID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForTenderJobID
            // 
            this.ItemForTenderJobID.Control = this.TenderJobIDTextEdit;
            this.ItemForTenderJobID.CustomizationFormText = "Tender Job ID:";
            this.ItemForTenderJobID.Location = new System.Drawing.Point(0, 48);
            this.ItemForTenderJobID.Name = "ItemForTenderJobID";
            this.ItemForTenderJobID.Size = new System.Drawing.Size(752, 24);
            this.ItemForTenderJobID.Text = "Tender Job ID:";
            this.ItemForTenderJobID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForBillingCentreCodeID
            // 
            this.ItemForBillingCentreCodeID.Control = this.BillingCentreCodeIDTextEdit;
            this.ItemForBillingCentreCodeID.CustomizationFormText = "Billing Centre Code ID:";
            this.ItemForBillingCentreCodeID.Location = new System.Drawing.Point(0, 326);
            this.ItemForBillingCentreCodeID.Name = "ItemForBillingCentreCodeID";
            this.ItemForBillingCentreCodeID.Size = new System.Drawing.Size(776, 24);
            this.ItemForBillingCentreCodeID.Text = "Billing Centre Code ID:";
            this.ItemForBillingCentreCodeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForReworkOriginalJobID
            // 
            this.ItemForReworkOriginalJobID.Control = this.ReworkOriginalJobIDTextEdit;
            this.ItemForReworkOriginalJobID.CustomizationFormText = "Rework Original Job ID:";
            this.ItemForReworkOriginalJobID.Location = new System.Drawing.Point(250, 168);
            this.ItemForReworkOriginalJobID.Name = "ItemForReworkOriginalJobID";
            this.ItemForReworkOriginalJobID.Size = new System.Drawing.Size(334, 24);
            this.ItemForReworkOriginalJobID.Text = "Rework Original Job ID:";
            this.ItemForReworkOriginalJobID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup27
            // 
            this.layoutControlGroup27.CustomizationFormText = "layoutControlGroup27";
            this.layoutControlGroup27.ExpandButtonVisible = true;
            this.layoutControlGroup27.Expanded = false;
            this.layoutControlGroup27.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup27.Location = new System.Drawing.Point(528, 0);
            this.layoutControlGroup27.Name = "layoutControlGroup27";
            this.layoutControlGroup27.Size = new System.Drawing.Size(56, 217);
            // 
            // ItemForClientPOID
            // 
            this.ItemForClientPOID.Control = this.ClientPOIDTextEdit;
            this.ItemForClientPOID.CustomizationFormText = "Client PO ID:";
            this.ItemForClientPOID.Location = new System.Drawing.Point(0, 48);
            this.ItemForClientPOID.Name = "ItemForClientPOID";
            this.ItemForClientPOID.Size = new System.Drawing.Size(776, 24);
            this.ItemForClientPOID.Text = "Client PO ID:";
            this.ItemForClientPOID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1398, 994);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlGroup29,
            this.emptySpaceItem2,
            this.layoutControlGroup3,
            this.emptySpaceItem29});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1378, 974);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(193, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroup29
            // 
            this.layoutControlGroup29.ExpandButtonVisible = true;
            this.layoutControlGroup29.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup29.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForClientNameContractDescription,
            this.ItemForSiteName,
            this.ItemForJobTypeDescription,
            this.ItemForVisitNumber,
            this.ItemForJobSubTypeDescription,
            this.ItemForDisplayOrder,
            this.ItemForRouteOrder,
            this.ItemForJobStatusID,
            this.ItemForCancelledReasonID,
            this.emptySpaceItem1,
            this.emptySpaceItem27,
            this.emptySpaceItem28,
            this.simpleSeparator9,
            this.ItemForReactive,
            this.emptySpaceItem22,
            this.simpleSeparator10,
            this.ItemForRework,
            this.emptySpaceItem25,
            this.ItemForJobNoLongerRequired,
            this.emptySpaceItem26,
            this.layoutControlGroup21,
            this.layoutControlGroup4,
            this.layoutControlGroup5,
            this.layoutControlGroup23,
            this.layoutControlGroup20,
            this.loca,
            this.splitterItem1,
            this.splitterItem2,
            this.emptySpaceItem15,
            this.layGrpRemarks,
            this.emptySpaceItem4,
            this.layoutControlGroup6,
            this.layoutControlGroup18,
            this.emptySpaceItem5,
            this.emptySpaceItem19,
            this.emptySpaceItem21,
            this.emptySpaceItem24,
            this.ItemForSuspendedReasonID,
            this.tabbedControlGroup1,
            this.layoutControlItem3,
            this.emptySpaceItem3,
            this.ItemForSuspendedRemarks});
            this.layoutControlGroup29.Location = new System.Drawing.Point(0, 23);
            this.layoutControlGroup29.Name = "layoutControlGroup29";
            this.layoutControlGroup29.Size = new System.Drawing.Size(695, 951);
            this.layoutControlGroup29.Text = "Job Details";
            // 
            // ItemForClientNameContractDescription
            // 
            this.ItemForClientNameContractDescription.Control = this.ClientNameContractDescriptionTextEdit;
            this.ItemForClientNameContractDescription.CustomizationFormText = "Client Name \\ Contract:";
            this.ItemForClientNameContractDescription.Location = new System.Drawing.Point(0, 0);
            this.ItemForClientNameContractDescription.Name = "ItemForClientNameContractDescription";
            this.ItemForClientNameContractDescription.Size = new System.Drawing.Size(671, 24);
            this.ItemForClientNameContractDescription.Text = "Client Name \\ Contract:";
            this.ItemForClientNameContractDescription.TextSize = new System.Drawing.Size(123, 13);
            // 
            // ItemForSiteName
            // 
            this.ItemForSiteName.Control = this.SiteNameTextEdit;
            this.ItemForSiteName.CustomizationFormText = "Site Name:";
            this.ItemForSiteName.Location = new System.Drawing.Point(0, 24);
            this.ItemForSiteName.Name = "ItemForSiteName";
            this.ItemForSiteName.Size = new System.Drawing.Size(304, 24);
            this.ItemForSiteName.Text = "Site Name:";
            this.ItemForSiteName.TextSize = new System.Drawing.Size(123, 13);
            // 
            // ItemForJobTypeDescription
            // 
            this.ItemForJobTypeDescription.Control = this.JobTypeDescriptionTextEdit;
            this.ItemForJobTypeDescription.CustomizationFormText = "Job Type:";
            this.ItemForJobTypeDescription.Location = new System.Drawing.Point(0, 48);
            this.ItemForJobTypeDescription.Name = "ItemForJobTypeDescription";
            this.ItemForJobTypeDescription.Size = new System.Drawing.Size(304, 24);
            this.ItemForJobTypeDescription.Text = "Job Type:";
            this.ItemForJobTypeDescription.TextSize = new System.Drawing.Size(123, 13);
            // 
            // ItemForVisitNumber
            // 
            this.ItemForVisitNumber.AllowHide = false;
            this.ItemForVisitNumber.Control = this.VisitNumberButtonEdit;
            this.ItemForVisitNumber.CustomizationFormText = "Visit Number:";
            this.ItemForVisitNumber.Location = new System.Drawing.Point(304, 24);
            this.ItemForVisitNumber.Name = "ItemForVisitNumber";
            this.ItemForVisitNumber.Size = new System.Drawing.Size(367, 24);
            this.ItemForVisitNumber.Text = "Visit Number:";
            this.ItemForVisitNumber.TextSize = new System.Drawing.Size(123, 13);
            // 
            // ItemForJobSubTypeDescription
            // 
            this.ItemForJobSubTypeDescription.Control = this.JobSubTypeDescriptionButtonEdit;
            this.ItemForJobSubTypeDescription.CustomizationFormText = "Job Sub-Type:";
            this.ItemForJobSubTypeDescription.Location = new System.Drawing.Point(304, 48);
            this.ItemForJobSubTypeDescription.Name = "ItemForJobSubTypeDescription";
            this.ItemForJobSubTypeDescription.Size = new System.Drawing.Size(367, 24);
            this.ItemForJobSubTypeDescription.Text = "Job Sub-Type:";
            this.ItemForJobSubTypeDescription.TextSize = new System.Drawing.Size(123, 13);
            // 
            // ItemForDisplayOrder
            // 
            this.ItemForDisplayOrder.Control = this.DisplayOrderSpinEdit;
            this.ItemForDisplayOrder.Location = new System.Drawing.Point(0, 72);
            this.ItemForDisplayOrder.Name = "ItemForDisplayOrder";
            this.ItemForDisplayOrder.Size = new System.Drawing.Size(304, 24);
            this.ItemForDisplayOrder.Text = "App Display Order:";
            this.ItemForDisplayOrder.TextSize = new System.Drawing.Size(123, 13);
            // 
            // ItemForRouteOrder
            // 
            this.ItemForRouteOrder.Control = this.RouteOrderSpinEdit;
            this.ItemForRouteOrder.CustomizationFormText = "Route Order:";
            this.ItemForRouteOrder.Location = new System.Drawing.Point(304, 72);
            this.ItemForRouteOrder.Name = "ItemForRouteOrder";
            this.ItemForRouteOrder.Size = new System.Drawing.Size(367, 24);
            this.ItemForRouteOrder.Text = "Route Order:";
            this.ItemForRouteOrder.TextSize = new System.Drawing.Size(123, 13);
            // 
            // ItemForJobStatusID
            // 
            this.ItemForJobStatusID.Control = this.JobStatusIDGridLookUpEdit;
            this.ItemForJobStatusID.CustomizationFormText = "Job Status:";
            this.ItemForJobStatusID.Location = new System.Drawing.Point(0, 186);
            this.ItemForJobStatusID.Name = "ItemForJobStatusID";
            this.ItemForJobStatusID.Size = new System.Drawing.Size(304, 24);
            this.ItemForJobStatusID.Text = "Job Status:";
            this.ItemForJobStatusID.TextSize = new System.Drawing.Size(123, 13);
            // 
            // ItemForCancelledReasonID
            // 
            this.ItemForCancelledReasonID.Control = this.CancelledReasonIDGridLookUpEdit;
            this.ItemForCancelledReasonID.CustomizationFormText = "Cancelled Reason:";
            this.ItemForCancelledReasonID.Location = new System.Drawing.Point(304, 186);
            this.ItemForCancelledReasonID.Name = "ItemForCancelledReasonID";
            this.ItemForCancelledReasonID.Size = new System.Drawing.Size(367, 24);
            this.ItemForCancelledReasonID.Text = "Cancelled Reason:";
            this.ItemForCancelledReasonID.TextSize = new System.Drawing.Size(123, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 164);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(671, 10);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem27
            // 
            this.emptySpaceItem27.AllowHotTrack = false;
            this.emptySpaceItem27.Location = new System.Drawing.Point(0, 96);
            this.emptySpaceItem27.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem27.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem27.Name = "emptySpaceItem27";
            this.emptySpaceItem27.Size = new System.Drawing.Size(671, 10);
            this.emptySpaceItem27.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem27.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem28
            // 
            this.emptySpaceItem28.AllowHotTrack = false;
            this.emptySpaceItem28.Location = new System.Drawing.Point(0, 108);
            this.emptySpaceItem28.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem28.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem28.Name = "emptySpaceItem28";
            this.emptySpaceItem28.Size = new System.Drawing.Size(671, 10);
            this.emptySpaceItem28.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem28.TextSize = new System.Drawing.Size(0, 0);
            // 
            // simpleSeparator9
            // 
            this.simpleSeparator9.AllowHotTrack = false;
            this.simpleSeparator9.Location = new System.Drawing.Point(0, 106);
            this.simpleSeparator9.Name = "simpleSeparator9";
            this.simpleSeparator9.Size = new System.Drawing.Size(671, 2);
            // 
            // ItemForReactive
            // 
            this.ItemForReactive.Control = this.ReactiveCheckEdit;
            this.ItemForReactive.CustomizationFormText = "Reactive:";
            this.ItemForReactive.Location = new System.Drawing.Point(0, 118);
            this.ItemForReactive.MaxSize = new System.Drawing.Size(213, 23);
            this.ItemForReactive.MinSize = new System.Drawing.Size(213, 23);
            this.ItemForReactive.Name = "ItemForReactive";
            this.ItemForReactive.Size = new System.Drawing.Size(213, 23);
            this.ItemForReactive.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForReactive.Text = "Reactive:";
            this.ItemForReactive.TextSize = new System.Drawing.Size(123, 13);
            // 
            // emptySpaceItem22
            // 
            this.emptySpaceItem22.AllowHotTrack = false;
            this.emptySpaceItem22.Location = new System.Drawing.Point(0, 176);
            this.emptySpaceItem22.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem22.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem22.Name = "emptySpaceItem22";
            this.emptySpaceItem22.Size = new System.Drawing.Size(671, 10);
            this.emptySpaceItem22.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem22.TextSize = new System.Drawing.Size(0, 0);
            // 
            // simpleSeparator10
            // 
            this.simpleSeparator10.AllowHotTrack = false;
            this.simpleSeparator10.Location = new System.Drawing.Point(0, 174);
            this.simpleSeparator10.Name = "simpleSeparator10";
            this.simpleSeparator10.Size = new System.Drawing.Size(671, 2);
            // 
            // ItemForRework
            // 
            this.ItemForRework.Control = this.ReworkCheckEdit;
            this.ItemForRework.CustomizationFormText = "Rework:";
            this.ItemForRework.Location = new System.Drawing.Point(0, 210);
            this.ItemForRework.Name = "ItemForRework";
            this.ItemForRework.Size = new System.Drawing.Size(304, 24);
            this.ItemForRework.Text = "Rework:";
            this.ItemForRework.TextSize = new System.Drawing.Size(123, 13);
            // 
            // emptySpaceItem25
            // 
            this.emptySpaceItem25.AllowHotTrack = false;
            this.emptySpaceItem25.Location = new System.Drawing.Point(213, 118);
            this.emptySpaceItem25.MaxSize = new System.Drawing.Size(37, 0);
            this.emptySpaceItem25.MinSize = new System.Drawing.Size(37, 10);
            this.emptySpaceItem25.Name = "emptySpaceItem25";
            this.emptySpaceItem25.Size = new System.Drawing.Size(37, 46);
            this.emptySpaceItem25.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem25.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForJobNoLongerRequired
            // 
            this.ItemForJobNoLongerRequired.Control = this.JobNoLongerRequiredCheckEdit;
            this.ItemForJobNoLongerRequired.CustomizationFormText = "Job No Longer Required:";
            this.ItemForJobNoLongerRequired.Location = new System.Drawing.Point(0, 141);
            this.ItemForJobNoLongerRequired.Name = "ItemForJobNoLongerRequired";
            this.ItemForJobNoLongerRequired.Size = new System.Drawing.Size(213, 23);
            this.ItemForJobNoLongerRequired.Text = "Job No Longer Required:";
            this.ItemForJobNoLongerRequired.TextSize = new System.Drawing.Size(123, 13);
            // 
            // emptySpaceItem26
            // 
            this.emptySpaceItem26.AllowHotTrack = false;
            this.emptySpaceItem26.Location = new System.Drawing.Point(424, 118);
            this.emptySpaceItem26.MaxSize = new System.Drawing.Size(36, 0);
            this.emptySpaceItem26.MinSize = new System.Drawing.Size(36, 10);
            this.emptySpaceItem26.Name = "emptySpaceItem26";
            this.emptySpaceItem26.Size = new System.Drawing.Size(36, 46);
            this.emptySpaceItem26.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem26.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup21
            // 
            this.layoutControlGroup21.CustomizationFormText = "Visit Constraints";
            this.layoutControlGroup21.ExpandButtonVisible = true;
            this.layoutControlGroup21.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup21.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForMinimumDaysFromLastVisit,
            this.ItemForMaximumDaysFromLastVisit,
            this.ItemForDaysLeeway});
            this.layoutControlGroup21.Location = new System.Drawing.Point(0, 290);
            this.layoutControlGroup21.Name = "layoutControlGroup21";
            this.layoutControlGroup21.Size = new System.Drawing.Size(204, 118);
            this.layoutControlGroup21.Text = "Visit Constraints";
            // 
            // ItemForMinimumDaysFromLastVisit
            // 
            this.ItemForMinimumDaysFromLastVisit.Control = this.MinimumDaysFromLastVisitSpinEdit;
            this.ItemForMinimumDaysFromLastVisit.CustomizationFormText = "Min Days From Last Visit:";
            this.ItemForMinimumDaysFromLastVisit.Location = new System.Drawing.Point(0, 0);
            this.ItemForMinimumDaysFromLastVisit.Name = "ItemForMinimumDaysFromLastVisit";
            this.ItemForMinimumDaysFromLastVisit.Size = new System.Drawing.Size(180, 24);
            this.ItemForMinimumDaysFromLastVisit.Text = "Min Days From Last Visit:";
            this.ItemForMinimumDaysFromLastVisit.TextSize = new System.Drawing.Size(123, 13);
            // 
            // ItemForMaximumDaysFromLastVisit
            // 
            this.ItemForMaximumDaysFromLastVisit.Control = this.MaximumDaysFromLastVisitSpinEdit;
            this.ItemForMaximumDaysFromLastVisit.CustomizationFormText = "Max Days From Last Visit:";
            this.ItemForMaximumDaysFromLastVisit.Location = new System.Drawing.Point(0, 24);
            this.ItemForMaximumDaysFromLastVisit.Name = "ItemForMaximumDaysFromLastVisit";
            this.ItemForMaximumDaysFromLastVisit.Size = new System.Drawing.Size(180, 24);
            this.ItemForMaximumDaysFromLastVisit.Text = "Max Days From Last Visit:";
            this.ItemForMaximumDaysFromLastVisit.TextSize = new System.Drawing.Size(123, 13);
            // 
            // ItemForDaysLeeway
            // 
            this.ItemForDaysLeeway.Control = this.DaysLeewaySpineEdit;
            this.ItemForDaysLeeway.CustomizationFormText = "Days Leeway:";
            this.ItemForDaysLeeway.Location = new System.Drawing.Point(0, 48);
            this.ItemForDaysLeeway.Name = "ItemForDaysLeeway";
            this.ItemForDaysLeeway.Size = new System.Drawing.Size(180, 24);
            this.ItemForDaysLeeway.Text = "Days Leeway:";
            this.ItemForDaysLeeway.TextSize = new System.Drawing.Size(123, 13);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Expected Duration";
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForExpectedStartDate,
            this.ItemForExpectedDurationUnits,
            this.ItemForExpectedEndDate,
            this.ItemForExpectedDurationUnitsDescriptorID});
            this.layoutControlGroup4.Location = new System.Drawing.Point(210, 290);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AlignLocal;
            this.layoutControlGroup4.Size = new System.Drawing.Size(225, 118);
            this.layoutControlGroup4.Text = "Expected Duration";
            // 
            // ItemForExpectedStartDate
            // 
            this.ItemForExpectedStartDate.AllowHide = false;
            this.ItemForExpectedStartDate.Control = this.ExpectedStartDateDateEdit;
            this.ItemForExpectedStartDate.CustomizationFormText = "Expected Start Date:";
            this.ItemForExpectedStartDate.Location = new System.Drawing.Point(0, 0);
            this.ItemForExpectedStartDate.Name = "ItemForExpectedStartDate";
            this.ItemForExpectedStartDate.Size = new System.Drawing.Size(201, 24);
            this.ItemForExpectedStartDate.Text = "Start Date:";
            this.ItemForExpectedStartDate.TextSize = new System.Drawing.Size(72, 13);
            // 
            // ItemForExpectedDurationUnits
            // 
            this.ItemForExpectedDurationUnits.Control = this.ExpectedDurationUnitsSpinEdit;
            this.ItemForExpectedDurationUnits.CustomizationFormText = "Expected Duration Units:";
            this.ItemForExpectedDurationUnits.Location = new System.Drawing.Point(0, 24);
            this.ItemForExpectedDurationUnits.MaxSize = new System.Drawing.Size(144, 0);
            this.ItemForExpectedDurationUnits.MinSize = new System.Drawing.Size(144, 24);
            this.ItemForExpectedDurationUnits.Name = "ItemForExpectedDurationUnits";
            this.ItemForExpectedDurationUnits.Size = new System.Drawing.Size(144, 24);
            this.ItemForExpectedDurationUnits.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForExpectedDurationUnits.Text = "Duration Units:";
            this.ItemForExpectedDurationUnits.TextSize = new System.Drawing.Size(72, 13);
            // 
            // ItemForExpectedEndDate
            // 
            this.ItemForExpectedEndDate.Control = this.ExpectedEndDateDateEdit;
            this.ItemForExpectedEndDate.CustomizationFormText = "Expected End Date:";
            this.ItemForExpectedEndDate.Location = new System.Drawing.Point(0, 48);
            this.ItemForExpectedEndDate.Name = "ItemForExpectedEndDate";
            this.ItemForExpectedEndDate.Size = new System.Drawing.Size(201, 24);
            this.ItemForExpectedEndDate.Text = "End Date:";
            this.ItemForExpectedEndDate.TextSize = new System.Drawing.Size(72, 13);
            // 
            // ItemForExpectedDurationUnitsDescriptorID
            // 
            this.ItemForExpectedDurationUnitsDescriptorID.Control = this.ExpectedDurationUnitsDescriptorIDGridLookUpEdit;
            this.ItemForExpectedDurationUnitsDescriptorID.CustomizationFormText = "Expected Duration Unit Descriptor:";
            this.ItemForExpectedDurationUnitsDescriptorID.Location = new System.Drawing.Point(144, 24);
            this.ItemForExpectedDurationUnitsDescriptorID.Name = "ItemForExpectedDurationUnitsDescriptorID";
            this.ItemForExpectedDurationUnitsDescriptorID.Size = new System.Drawing.Size(57, 24);
            this.ItemForExpectedDurationUnitsDescriptorID.Text = "Unit Descriptor:";
            this.ItemForExpectedDurationUnitsDescriptorID.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForExpectedDurationUnitsDescriptorID.TextVisible = false;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Actual Duration";
            this.layoutControlGroup5.ExpandButtonVisible = true;
            this.layoutControlGroup5.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForActualStartDate,
            this.ItemForActualDurationUnits,
            this.ItemForActualEndDate,
            this.ItemForActualDurationUnitsDescriptionID});
            this.layoutControlGroup5.Location = new System.Drawing.Point(441, 290);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AlignLocal;
            this.layoutControlGroup5.Size = new System.Drawing.Size(230, 118);
            this.layoutControlGroup5.Text = "Actual Duration";
            // 
            // ItemForActualStartDate
            // 
            this.ItemForActualStartDate.Control = this.ActualStartDateDateEdit;
            this.ItemForActualStartDate.CustomizationFormText = "Actual Start Date:";
            this.ItemForActualStartDate.Location = new System.Drawing.Point(0, 0);
            this.ItemForActualStartDate.Name = "ItemForActualStartDate";
            this.ItemForActualStartDate.Size = new System.Drawing.Size(206, 24);
            this.ItemForActualStartDate.Text = "Start Date:";
            this.ItemForActualStartDate.TextSize = new System.Drawing.Size(81, 13);
            // 
            // ItemForActualDurationUnits
            // 
            this.ItemForActualDurationUnits.Control = this.ActualDurationUnitsSpinEdit;
            this.ItemForActualDurationUnits.CustomizationFormText = "Duration Units:";
            this.ItemForActualDurationUnits.Location = new System.Drawing.Point(0, 24);
            this.ItemForActualDurationUnits.MaxSize = new System.Drawing.Size(152, 24);
            this.ItemForActualDurationUnits.MinSize = new System.Drawing.Size(152, 24);
            this.ItemForActualDurationUnits.Name = "ItemForActualDurationUnits";
            this.ItemForActualDurationUnits.Size = new System.Drawing.Size(152, 24);
            this.ItemForActualDurationUnits.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForActualDurationUnits.Text = "Duration Units:";
            this.ItemForActualDurationUnits.TextSize = new System.Drawing.Size(81, 13);
            // 
            // ItemForActualEndDate
            // 
            this.ItemForActualEndDate.Control = this.ActualEndDateDateEdit;
            this.ItemForActualEndDate.CustomizationFormText = "End Date:";
            this.ItemForActualEndDate.Location = new System.Drawing.Point(0, 48);
            this.ItemForActualEndDate.Name = "ItemForActualEndDate";
            this.ItemForActualEndDate.Size = new System.Drawing.Size(206, 24);
            this.ItemForActualEndDate.Text = "Actual End Date:";
            this.ItemForActualEndDate.TextSize = new System.Drawing.Size(81, 13);
            // 
            // ItemForActualDurationUnitsDescriptionID
            // 
            this.ItemForActualDurationUnitsDescriptionID.Control = this.ActualDurationUnitsDescriptionIDGridLookUpEdit;
            this.ItemForActualDurationUnitsDescriptionID.CustomizationFormText = "Unit Descriptor:";
            this.ItemForActualDurationUnitsDescriptionID.Location = new System.Drawing.Point(152, 24);
            this.ItemForActualDurationUnitsDescriptionID.Name = "ItemForActualDurationUnitsDescriptionID";
            this.ItemForActualDurationUnitsDescriptionID.Size = new System.Drawing.Size(54, 24);
            this.ItemForActualDurationUnitsDescriptionID.Text = "Unit Descriptor:";
            this.ItemForActualDurationUnitsDescriptionID.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForActualDurationUnitsDescriptionID.TextVisible = false;
            // 
            // layoutControlGroup23
            // 
            this.layoutControlGroup23.CustomizationFormText = "Location - Site";
            this.layoutControlGroup23.ExpandButtonVisible = true;
            this.layoutControlGroup23.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup23.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForLocationX,
            this.ItemForLocationY,
            this.emptySpaceItem20});
            this.layoutControlGroup23.Location = new System.Drawing.Point(0, 418);
            this.layoutControlGroup23.Name = "layoutControlGroup23";
            this.layoutControlGroup23.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AlignLocal;
            this.layoutControlGroup23.Size = new System.Drawing.Size(204, 104);
            this.layoutControlGroup23.Text = "Location - Site";
            // 
            // ItemForLocationX
            // 
            this.ItemForLocationX.Control = this.LocationXTextEdit;
            this.ItemForLocationX.CustomizationFormText = "Site Latitude:";
            this.ItemForLocationX.Location = new System.Drawing.Point(0, 0);
            this.ItemForLocationX.Name = "ItemForLocationX";
            this.ItemForLocationX.Size = new System.Drawing.Size(180, 24);
            this.ItemForLocationX.Text = "Site Latitude:";
            this.ItemForLocationX.TextSize = new System.Drawing.Size(72, 13);
            // 
            // ItemForLocationY
            // 
            this.ItemForLocationY.Control = this.LocationYTextEdit;
            this.ItemForLocationY.CustomizationFormText = "Site Longitude:";
            this.ItemForLocationY.Location = new System.Drawing.Point(0, 24);
            this.ItemForLocationY.Name = "ItemForLocationY";
            this.ItemForLocationY.Size = new System.Drawing.Size(180, 24);
            this.ItemForLocationY.Text = "Site Longitude:";
            this.ItemForLocationY.TextSize = new System.Drawing.Size(72, 13);
            // 
            // emptySpaceItem20
            // 
            this.emptySpaceItem20.AllowHotTrack = false;
            this.emptySpaceItem20.Location = new System.Drawing.Point(0, 48);
            this.emptySpaceItem20.Name = "emptySpaceItem20";
            this.emptySpaceItem20.Size = new System.Drawing.Size(180, 10);
            this.emptySpaceItem20.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup20
            // 
            this.layoutControlGroup20.CustomizationFormText = "Location - Start";
            this.layoutControlGroup20.ExpandButtonVisible = true;
            this.layoutControlGroup20.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup20.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForStartLatitude,
            this.ItemForStartLongitude});
            this.layoutControlGroup20.Location = new System.Drawing.Point(210, 418);
            this.layoutControlGroup20.Name = "layoutControlGroup20";
            this.layoutControlGroup20.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AlignLocal;
            this.layoutControlGroup20.Size = new System.Drawing.Size(225, 104);
            this.layoutControlGroup20.Text = "Location - Start";
            // 
            // ItemForStartLatitude
            // 
            this.ItemForStartLatitude.Control = this.StartLatitudeTextEdit;
            this.ItemForStartLatitude.CustomizationFormText = "Start Latitude:";
            this.ItemForStartLatitude.Location = new System.Drawing.Point(0, 0);
            this.ItemForStartLatitude.Name = "ItemForStartLatitude";
            this.ItemForStartLatitude.Size = new System.Drawing.Size(201, 24);
            this.ItemForStartLatitude.Text = "Start Latitude:";
            this.ItemForStartLatitude.TextSize = new System.Drawing.Size(78, 13);
            // 
            // ItemForStartLongitude
            // 
            this.ItemForStartLongitude.Control = this.StartLongitudeTextEdit;
            this.ItemForStartLongitude.CustomizationFormText = "Start Longitude:";
            this.ItemForStartLongitude.Location = new System.Drawing.Point(0, 24);
            this.ItemForStartLongitude.Name = "ItemForStartLongitude";
            this.ItemForStartLongitude.Size = new System.Drawing.Size(201, 34);
            this.ItemForStartLongitude.Text = "Start Longitude:";
            this.ItemForStartLongitude.TextSize = new System.Drawing.Size(78, 13);
            // 
            // loca
            // 
            this.loca.CustomizationFormText = "Location - Finish";
            this.loca.ExpandButtonVisible = true;
            this.loca.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.loca.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForFinishLatitude,
            this.ItemForFinishLongitude});
            this.loca.Location = new System.Drawing.Point(441, 418);
            this.loca.Name = "loca";
            this.loca.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AlignLocal;
            this.loca.Size = new System.Drawing.Size(230, 104);
            this.loca.Text = "Location - Finish";
            // 
            // ItemForFinishLatitude
            // 
            this.ItemForFinishLatitude.Control = this.FinishLatitudeTextEdit;
            this.ItemForFinishLatitude.CustomizationFormText = "Finish Latitude:";
            this.ItemForFinishLatitude.Location = new System.Drawing.Point(0, 0);
            this.ItemForFinishLatitude.Name = "ItemForFinishLatitude";
            this.ItemForFinishLatitude.Size = new System.Drawing.Size(206, 24);
            this.ItemForFinishLatitude.Text = "Finish Latitude:";
            this.ItemForFinishLatitude.TextSize = new System.Drawing.Size(81, 13);
            // 
            // ItemForFinishLongitude
            // 
            this.ItemForFinishLongitude.Control = this.FinishLongitudeTextEdit;
            this.ItemForFinishLongitude.CustomizationFormText = "Finish Longitude:";
            this.ItemForFinishLongitude.Location = new System.Drawing.Point(0, 24);
            this.ItemForFinishLongitude.Name = "ItemForFinishLongitude";
            this.ItemForFinishLongitude.Size = new System.Drawing.Size(206, 34);
            this.ItemForFinishLongitude.Text = "Finish Longitude:";
            this.ItemForFinishLongitude.TextSize = new System.Drawing.Size(81, 13);
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.Location = new System.Drawing.Point(435, 290);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(6, 232);
            // 
            // splitterItem2
            // 
            this.splitterItem2.AllowHotTrack = true;
            this.splitterItem2.Location = new System.Drawing.Point(204, 290);
            this.splitterItem2.Name = "splitterItem2";
            this.splitterItem2.Size = new System.Drawing.Size(6, 232);
            // 
            // emptySpaceItem15
            // 
            this.emptySpaceItem15.AllowHotTrack = false;
            this.emptySpaceItem15.Location = new System.Drawing.Point(0, 280);
            this.emptySpaceItem15.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem15.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem15.Name = "emptySpaceItem15";
            this.emptySpaceItem15.Size = new System.Drawing.Size(671, 10);
            this.emptySpaceItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem15.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layGrpRemarks
            // 
            this.layGrpRemarks.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("layGrpRemarks.CaptionImageOptions.Image")));
            this.layGrpRemarks.CustomizationFormText = "Remarks";
            this.layGrpRemarks.ExpandButtonVisible = true;
            this.layGrpRemarks.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layGrpRemarks.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layGrpRemarks.Location = new System.Drawing.Point(0, 532);
            this.layGrpRemarks.Name = "layGrpRemarks";
            this.layGrpRemarks.Size = new System.Drawing.Size(671, 162);
            this.layGrpRemarks.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.MaxSize = new System.Drawing.Size(0, 114);
            this.ItemForRemarks.MinSize = new System.Drawing.Size(14, 114);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(647, 114);
            this.ItemForRemarks.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextLocation = DevExpress.Utils.Locations.Top;
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 720);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(671, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.GroupBordersVisible = false;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForMandatory,
            this.ItemForNoWorkRequired});
            this.layoutControlGroup6.Location = new System.Drawing.Point(250, 118);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AlignLocal;
            this.layoutControlGroup6.Size = new System.Drawing.Size(174, 46);
            // 
            // ItemForMandatory
            // 
            this.ItemForMandatory.Control = this.MandatoryCheckEdit;
            this.ItemForMandatory.Location = new System.Drawing.Point(0, 0);
            this.ItemForMandatory.MaxSize = new System.Drawing.Size(174, 23);
            this.ItemForMandatory.MinSize = new System.Drawing.Size(174, 23);
            this.ItemForMandatory.Name = "ItemForMandatory";
            this.ItemForMandatory.Size = new System.Drawing.Size(174, 23);
            this.ItemForMandatory.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForMandatory.Text = "Mandatory:";
            this.ItemForMandatory.TextSize = new System.Drawing.Size(91, 13);
            // 
            // ItemForNoWorkRequired
            // 
            this.ItemForNoWorkRequired.Control = this.NoWorkRequiredCheckEdit;
            this.ItemForNoWorkRequired.Location = new System.Drawing.Point(0, 23);
            this.ItemForNoWorkRequired.Name = "ItemForNoWorkRequired";
            this.ItemForNoWorkRequired.Size = new System.Drawing.Size(174, 23);
            this.ItemForNoWorkRequired.Text = "No Work Required:";
            this.ItemForNoWorkRequired.TextSize = new System.Drawing.Size(91, 13);
            // 
            // layoutControlGroup18
            // 
            this.layoutControlGroup18.ExpandButtonVisible = true;
            this.layoutControlGroup18.GroupBordersVisible = false;
            this.layoutControlGroup18.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup18.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRequiresAccessPermit,
            this.ItemForManuallyCompleted,
            this.emptySpaceItem8});
            this.layoutControlGroup18.Location = new System.Drawing.Point(460, 118);
            this.layoutControlGroup18.Name = "layoutControlGroup18";
            this.layoutControlGroup18.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AlignLocal;
            this.layoutControlGroup18.Size = new System.Drawing.Size(211, 46);
            // 
            // ItemForRequiresAccessPermit
            // 
            this.ItemForRequiresAccessPermit.Control = this.RequiresAccessPermitCheckEdit;
            this.ItemForRequiresAccessPermit.CustomizationFormText = "Requires Access Permit:";
            this.ItemForRequiresAccessPermit.Location = new System.Drawing.Point(0, 0);
            this.ItemForRequiresAccessPermit.MaxSize = new System.Drawing.Size(198, 23);
            this.ItemForRequiresAccessPermit.MinSize = new System.Drawing.Size(198, 23);
            this.ItemForRequiresAccessPermit.Name = "ItemForRequiresAccessPermit";
            this.ItemForRequiresAccessPermit.Size = new System.Drawing.Size(198, 23);
            this.ItemForRequiresAccessPermit.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForRequiresAccessPermit.Text = "Requires Access Permit:";
            this.ItemForRequiresAccessPermit.TextSize = new System.Drawing.Size(115, 13);
            // 
            // ItemForManuallyCompleted
            // 
            this.ItemForManuallyCompleted.Control = this.ManuallyCompletedCheckEdit;
            this.ItemForManuallyCompleted.Location = new System.Drawing.Point(0, 23);
            this.ItemForManuallyCompleted.Name = "ItemForManuallyCompleted";
            this.ItemForManuallyCompleted.Size = new System.Drawing.Size(198, 23);
            this.ItemForManuallyCompleted.Text = "Manually Completed:";
            this.ItemForManuallyCompleted.TextSize = new System.Drawing.Size(115, 13);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.Location = new System.Drawing.Point(198, 0);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(13, 46);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 408);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(204, 10);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem19
            // 
            this.emptySpaceItem19.AllowHotTrack = false;
            this.emptySpaceItem19.Location = new System.Drawing.Point(210, 408);
            this.emptySpaceItem19.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem19.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem19.Name = "emptySpaceItem19";
            this.emptySpaceItem19.Size = new System.Drawing.Size(225, 10);
            this.emptySpaceItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem19.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem21
            // 
            this.emptySpaceItem21.AllowHotTrack = false;
            this.emptySpaceItem21.Location = new System.Drawing.Point(441, 408);
            this.emptySpaceItem21.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem21.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem21.Name = "emptySpaceItem21";
            this.emptySpaceItem21.Size = new System.Drawing.Size(230, 10);
            this.emptySpaceItem21.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem21.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem24
            // 
            this.emptySpaceItem24.AllowHotTrack = false;
            this.emptySpaceItem24.Location = new System.Drawing.Point(0, 522);
            this.emptySpaceItem24.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem24.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem24.Name = "emptySpaceItem24";
            this.emptySpaceItem24.Size = new System.Drawing.Size(671, 10);
            this.emptySpaceItem24.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem24.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForSuspendedReasonID
            // 
            this.ItemForSuspendedReasonID.Control = this.SuspendedReasonIDGridLookUpEdit;
            this.ItemForSuspendedReasonID.Location = new System.Drawing.Point(304, 210);
            this.ItemForSuspendedReasonID.Name = "ItemForSuspendedReasonID";
            this.ItemForSuspendedReasonID.Size = new System.Drawing.Size(367, 24);
            this.ItemForSuspendedReasonID.Text = "Suspended Reason ID:";
            this.ItemForSuspendedReasonID.TextSize = new System.Drawing.Size(123, 13);
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 730);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup25;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(671, 175);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup25,
            this.layoutControlGroup24,
            this.layoutControlGroup26});
            // 
            // layoutControlGroup25
            // 
            this.layoutControlGroup25.CustomizationFormText = "Linked Labour";
            this.layoutControlGroup25.ExpandButtonVisible = true;
            this.layoutControlGroup25.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup25.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForLinkedLabourGrid});
            this.layoutControlGroup25.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup25.Name = "layoutControlGroup25";
            this.layoutControlGroup25.Size = new System.Drawing.Size(647, 130);
            this.layoutControlGroup25.Text = "Linked Labour";
            // 
            // ItemForLinkedLabourGrid
            // 
            this.ItemForLinkedLabourGrid.Control = this.gridSplitContainer1;
            this.ItemForLinkedLabourGrid.CustomizationFormText = "Linked Labour Grid:";
            this.ItemForLinkedLabourGrid.Location = new System.Drawing.Point(0, 0);
            this.ItemForLinkedLabourGrid.MaxSize = new System.Drawing.Size(0, 157);
            this.ItemForLinkedLabourGrid.MinSize = new System.Drawing.Size(5, 130);
            this.ItemForLinkedLabourGrid.Name = "ItemForLinkedLabourGrid";
            this.ItemForLinkedLabourGrid.Size = new System.Drawing.Size(647, 130);
            this.ItemForLinkedLabourGrid.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForLinkedLabourGrid.Text = "Linked Labour Grid:";
            this.ItemForLinkedLabourGrid.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForLinkedLabourGrid.TextVisible = false;
            // 
            // layoutControlGroup24
            // 
            this.layoutControlGroup24.CustomizationFormText = "Linked Equipment";
            this.layoutControlGroup24.ExpandButtonVisible = true;
            this.layoutControlGroup24.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup24.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForEquipmentGrid});
            this.layoutControlGroup24.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup24.Name = "layoutControlGroup24";
            this.layoutControlGroup24.Size = new System.Drawing.Size(647, 130);
            this.layoutControlGroup24.Text = "Linked Equipment";
            // 
            // ItemForEquipmentGrid
            // 
            this.ItemForEquipmentGrid.Control = this.gridControlEquipment;
            this.ItemForEquipmentGrid.CustomizationFormText = "Equipment Grid:";
            this.ItemForEquipmentGrid.Location = new System.Drawing.Point(0, 0);
            this.ItemForEquipmentGrid.Name = "ItemForEquipmentGrid";
            this.ItemForEquipmentGrid.Size = new System.Drawing.Size(647, 130);
            this.ItemForEquipmentGrid.Text = "Equipment Grid:";
            this.ItemForEquipmentGrid.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForEquipmentGrid.TextVisible = false;
            // 
            // layoutControlGroup26
            // 
            this.layoutControlGroup26.CustomizationFormText = "Linked Materials";
            this.layoutControlGroup26.ExpandButtonVisible = true;
            this.layoutControlGroup26.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup26.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForMaterialGrid});
            this.layoutControlGroup26.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup26.Name = "layoutControlGroup26";
            this.layoutControlGroup26.Size = new System.Drawing.Size(647, 130);
            this.layoutControlGroup26.Text = "Linked Materials";
            // 
            // ItemForMaterialGrid
            // 
            this.ItemForMaterialGrid.Control = this.gridControlMaterials;
            this.ItemForMaterialGrid.CustomizationFormText = "Material Grid:";
            this.ItemForMaterialGrid.Location = new System.Drawing.Point(0, 0);
            this.ItemForMaterialGrid.Name = "ItemForMaterialGrid";
            this.ItemForMaterialGrid.Size = new System.Drawing.Size(647, 130);
            this.ItemForMaterialGrid.Text = "Material Grid:";
            this.ItemForMaterialGrid.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForMaterialGrid.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnSave;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 694);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(91, 26);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(91, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(91, 26);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(91, 694);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(0, 26);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(10, 26);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(580, 26);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForSuspendedRemarks
            // 
            this.ItemForSuspendedRemarks.Control = this.SuspendedRemarksMemoEdit;
            this.ItemForSuspendedRemarks.Location = new System.Drawing.Point(0, 234);
            this.ItemForSuspendedRemarks.MaxSize = new System.Drawing.Size(0, 46);
            this.ItemForSuspendedRemarks.MinSize = new System.Drawing.Size(140, 46);
            this.ItemForSuspendedRemarks.Name = "ItemForSuspendedRemarks";
            this.ItemForSuspendedRemarks.Size = new System.Drawing.Size(671, 46);
            this.ItemForSuspendedRemarks.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForSuspendedRemarks.Text = "Suspended Remarks:";
            this.ItemForSuspendedRemarks.TextLocation = DevExpress.Utils.Locations.Left;
            this.ItemForSuspendedRemarks.TextSize = new System.Drawing.Size(123, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(193, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(1185, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup3.CaptionImageOptions.Image")));
            this.layoutControlGroup3.CustomizationFormText = "Financial";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup7,
            this.layoutControlGroup8,
            this.emptySpaceItem7,
            this.emptySpaceItem6,
            this.ItemForClientPONumber,
            this.ItemForFinanceSystemPONumber,
            this.layoutControlGroup19,
            this.layoutControlGroup17,
            this.emptySpaceItem17,
            this.ItemForDoNotPayContractor,
            this.ItemForDoNotInvoiceClient});
            this.layoutControlGroup3.Location = new System.Drawing.Point(705, 23);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(673, 951);
            this.layoutControlGroup3.Text = "Financial";
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "Cost";
            this.layoutControlGroup7.ExpandButtonVisible = true;
            this.layoutControlGroup7.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup9,
            this.layoutControlGroup15,
            this.layoutControlGroup13,
            this.layoutControlGroup11});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 209);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AlignLocal;
            this.layoutControlGroup7.Size = new System.Drawing.Size(320, 624);
            this.layoutControlGroup7.Text = "Cost";
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.CustomizationFormText = "Materials";
            this.layoutControlGroup9.ExpandButtonVisible = true;
            this.layoutControlGroup9.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForCostTotalMaterialExVAT,
            this.ItemForCostTotalMaterialVATRate,
            this.ItemForCostTotalMaterialVAT,
            this.ItemForCostTotalMaterialCost,
            this.simpleSeparator1});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 314);
            this.layoutControlGroup9.Name = "layoutControlGroup9";
            this.layoutControlGroup9.Size = new System.Drawing.Size(296, 144);
            this.layoutControlGroup9.Text = "Materials";
            // 
            // ItemForCostTotalMaterialExVAT
            // 
            this.ItemForCostTotalMaterialExVAT.Control = this.CostTotalMaterialExVATSpinEdit;
            this.ItemForCostTotalMaterialExVAT.CustomizationFormText = "Cost Total Material Ex VAT:";
            this.ItemForCostTotalMaterialExVAT.Location = new System.Drawing.Point(0, 0);
            this.ItemForCostTotalMaterialExVAT.Name = "ItemForCostTotalMaterialExVAT";
            this.ItemForCostTotalMaterialExVAT.Size = new System.Drawing.Size(272, 24);
            this.ItemForCostTotalMaterialExVAT.Text = "Total Ex VAT:";
            this.ItemForCostTotalMaterialExVAT.TextSize = new System.Drawing.Size(65, 13);
            // 
            // ItemForCostTotalMaterialVATRate
            // 
            this.ItemForCostTotalMaterialVATRate.Control = this.CostTotalMaterialVATRateSpinEdit;
            this.ItemForCostTotalMaterialVATRate.CustomizationFormText = "Cost Total Material VAT Rate:";
            this.ItemForCostTotalMaterialVATRate.Location = new System.Drawing.Point(0, 24);
            this.ItemForCostTotalMaterialVATRate.Name = "ItemForCostTotalMaterialVATRate";
            this.ItemForCostTotalMaterialVATRate.Size = new System.Drawing.Size(272, 24);
            this.ItemForCostTotalMaterialVATRate.Text = "VAT Rate:";
            this.ItemForCostTotalMaterialVATRate.TextSize = new System.Drawing.Size(65, 13);
            // 
            // ItemForCostTotalMaterialVAT
            // 
            this.ItemForCostTotalMaterialVAT.Control = this.CostTotalMaterialVATSpinEdit;
            this.ItemForCostTotalMaterialVAT.CustomizationFormText = "Cost Total Material VAT:";
            this.ItemForCostTotalMaterialVAT.Location = new System.Drawing.Point(0, 48);
            this.ItemForCostTotalMaterialVAT.Name = "ItemForCostTotalMaterialVAT";
            this.ItemForCostTotalMaterialVAT.Size = new System.Drawing.Size(272, 24);
            this.ItemForCostTotalMaterialVAT.Text = "Total VAT:";
            this.ItemForCostTotalMaterialVAT.TextSize = new System.Drawing.Size(65, 13);
            // 
            // ItemForCostTotalMaterialCost
            // 
            this.ItemForCostTotalMaterialCost.Control = this.CostTotalMaterialCostSpinEdit;
            this.ItemForCostTotalMaterialCost.CustomizationFormText = "Cost Total Material Cost:";
            this.ItemForCostTotalMaterialCost.Location = new System.Drawing.Point(0, 74);
            this.ItemForCostTotalMaterialCost.Name = "ItemForCostTotalMaterialCost";
            this.ItemForCostTotalMaterialCost.Size = new System.Drawing.Size(272, 24);
            this.ItemForCostTotalMaterialCost.Text = "Total Cost:";
            this.ItemForCostTotalMaterialCost.TextSize = new System.Drawing.Size(65, 13);
            // 
            // simpleSeparator1
            // 
            this.simpleSeparator1.AllowHotTrack = false;
            this.simpleSeparator1.CustomizationFormText = "simpleSeparator1";
            this.simpleSeparator1.Location = new System.Drawing.Point(0, 72);
            this.simpleSeparator1.Name = "simpleSeparator1";
            this.simpleSeparator1.Size = new System.Drawing.Size(272, 2);
            // 
            // layoutControlGroup15
            // 
            this.layoutControlGroup15.CustomizationFormText = "Total";
            this.layoutControlGroup15.ExpandButtonVisible = true;
            this.layoutControlGroup15.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup15.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForCostTotalCostExVAT,
            this.ItemForCostTotalCostVAT,
            this.ItemForCostTotalCost,
            this.simpleSeparator7});
            this.layoutControlGroup15.Location = new System.Drawing.Point(0, 458);
            this.layoutControlGroup15.Name = "layoutControlGroup15";
            this.layoutControlGroup15.Size = new System.Drawing.Size(296, 120);
            this.layoutControlGroup15.Text = "Total";
            // 
            // ItemForCostTotalCostExVAT
            // 
            this.ItemForCostTotalCostExVAT.Control = this.CostTotalCostExVATSpinEdit;
            this.ItemForCostTotalCostExVAT.CustomizationFormText = "Cost Total Cost Ex VAT:";
            this.ItemForCostTotalCostExVAT.Location = new System.Drawing.Point(0, 0);
            this.ItemForCostTotalCostExVAT.Name = "ItemForCostTotalCostExVAT";
            this.ItemForCostTotalCostExVAT.Size = new System.Drawing.Size(272, 24);
            this.ItemForCostTotalCostExVAT.Text = "Total Ex VAT:";
            this.ItemForCostTotalCostExVAT.TextSize = new System.Drawing.Size(65, 13);
            // 
            // ItemForCostTotalCostVAT
            // 
            this.ItemForCostTotalCostVAT.Control = this.CostTotalCostVATSpinEdit;
            this.ItemForCostTotalCostVAT.CustomizationFormText = "Cost Total Cost VAT:";
            this.ItemForCostTotalCostVAT.Location = new System.Drawing.Point(0, 24);
            this.ItemForCostTotalCostVAT.Name = "ItemForCostTotalCostVAT";
            this.ItemForCostTotalCostVAT.Size = new System.Drawing.Size(272, 24);
            this.ItemForCostTotalCostVAT.Text = "Total VAT:";
            this.ItemForCostTotalCostVAT.TextSize = new System.Drawing.Size(65, 13);
            // 
            // ItemForCostTotalCost
            // 
            this.ItemForCostTotalCost.Control = this.CostTotalCostSpinEdit;
            this.ItemForCostTotalCost.CustomizationFormText = "Cost Total Cost:";
            this.ItemForCostTotalCost.Location = new System.Drawing.Point(0, 50);
            this.ItemForCostTotalCost.Name = "ItemForCostTotalCost";
            this.ItemForCostTotalCost.Size = new System.Drawing.Size(272, 24);
            this.ItemForCostTotalCost.Text = "Total Cost:";
            this.ItemForCostTotalCost.TextSize = new System.Drawing.Size(65, 13);
            // 
            // simpleSeparator7
            // 
            this.simpleSeparator7.AllowHotTrack = false;
            this.simpleSeparator7.CustomizationFormText = "simpleSeparator7";
            this.simpleSeparator7.Location = new System.Drawing.Point(0, 48);
            this.simpleSeparator7.Name = "simpleSeparator7";
            this.simpleSeparator7.Size = new System.Drawing.Size(272, 2);
            // 
            // layoutControlGroup13
            // 
            this.layoutControlGroup13.CustomizationFormText = "Labour";
            this.layoutControlGroup13.ExpandButtonVisible = true;
            this.layoutControlGroup13.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup13.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForCostTotalLabourExVAT,
            this.ItemForCostTotalLabourVATRate,
            this.ItemForCostTotalLabourVAT,
            this.ItemForCostTotalLabourCost,
            this.simpleSeparator5,
            this.layoutControlItem2,
            this.ItemForVisitCostCalculationLevel});
            this.layoutControlGroup13.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup13.Name = "layoutControlGroup13";
            this.layoutControlGroup13.Size = new System.Drawing.Size(296, 170);
            this.layoutControlGroup13.Text = "Labour";
            // 
            // ItemForCostTotalLabourExVAT
            // 
            this.ItemForCostTotalLabourExVAT.Control = this.CostTotalLabourExVATSpinEdit;
            this.ItemForCostTotalLabourExVAT.CustomizationFormText = "Cost Total Labour Ex VAT:";
            this.ItemForCostTotalLabourExVAT.Location = new System.Drawing.Point(0, 24);
            this.ItemForCostTotalLabourExVAT.Name = "ItemForCostTotalLabourExVAT";
            this.ItemForCostTotalLabourExVAT.Size = new System.Drawing.Size(242, 26);
            this.ItemForCostTotalLabourExVAT.Text = "Total Ex VAT:";
            this.ItemForCostTotalLabourExVAT.TextSize = new System.Drawing.Size(65, 13);
            // 
            // ItemForCostTotalLabourVATRate
            // 
            this.ItemForCostTotalLabourVATRate.Control = this.CostTotalLabourVATRateSpinEdit;
            this.ItemForCostTotalLabourVATRate.CustomizationFormText = "Cost Total Labour VAT Rate:";
            this.ItemForCostTotalLabourVATRate.Location = new System.Drawing.Point(0, 50);
            this.ItemForCostTotalLabourVATRate.Name = "ItemForCostTotalLabourVATRate";
            this.ItemForCostTotalLabourVATRate.Size = new System.Drawing.Size(272, 24);
            this.ItemForCostTotalLabourVATRate.Text = "VAT Rate:";
            this.ItemForCostTotalLabourVATRate.TextSize = new System.Drawing.Size(65, 13);
            // 
            // ItemForCostTotalLabourVAT
            // 
            this.ItemForCostTotalLabourVAT.Control = this.CostTotalLabourVATSpinEdit;
            this.ItemForCostTotalLabourVAT.CustomizationFormText = "Cost Total Labour VAT:";
            this.ItemForCostTotalLabourVAT.Location = new System.Drawing.Point(0, 74);
            this.ItemForCostTotalLabourVAT.Name = "ItemForCostTotalLabourVAT";
            this.ItemForCostTotalLabourVAT.Size = new System.Drawing.Size(272, 24);
            this.ItemForCostTotalLabourVAT.Text = "Total VAT:";
            this.ItemForCostTotalLabourVAT.TextSize = new System.Drawing.Size(65, 13);
            // 
            // ItemForCostTotalLabourCost
            // 
            this.ItemForCostTotalLabourCost.Control = this.CostTotalLabourCostSpinEdit;
            this.ItemForCostTotalLabourCost.CustomizationFormText = "Cost Total Labour Cost:";
            this.ItemForCostTotalLabourCost.Location = new System.Drawing.Point(0, 100);
            this.ItemForCostTotalLabourCost.Name = "ItemForCostTotalLabourCost";
            this.ItemForCostTotalLabourCost.Size = new System.Drawing.Size(272, 24);
            this.ItemForCostTotalLabourCost.Text = "Total Cost:";
            this.ItemForCostTotalLabourCost.TextSize = new System.Drawing.Size(65, 13);
            // 
            // simpleSeparator5
            // 
            this.simpleSeparator5.AllowHotTrack = false;
            this.simpleSeparator5.CustomizationFormText = "simpleSeparator5";
            this.simpleSeparator5.Location = new System.Drawing.Point(0, 98);
            this.simpleSeparator5.Name = "simpleSeparator5";
            this.simpleSeparator5.Size = new System.Drawing.Size(272, 2);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.CalculateCostButton;
            this.layoutControlItem2.Location = new System.Drawing.Point(242, 24);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(30, 26);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(30, 26);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(30, 26);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "Re-calculate Labour Cost";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // ItemForVisitCostCalculationLevel
            // 
            this.ItemForVisitCostCalculationLevel.Control = this.VisitCostCalculationLevelTextEdit;
            this.ItemForVisitCostCalculationLevel.CustomizationFormText = "Visit Cost Method:";
            this.ItemForVisitCostCalculationLevel.Location = new System.Drawing.Point(0, 0);
            this.ItemForVisitCostCalculationLevel.Name = "ItemForVisitCostCalculationLevel";
            this.ItemForVisitCostCalculationLevel.Size = new System.Drawing.Size(272, 24);
            this.ItemForVisitCostCalculationLevel.Text = "Cost Method:";
            this.ItemForVisitCostCalculationLevel.TextSize = new System.Drawing.Size(65, 13);
            // 
            // layoutControlGroup11
            // 
            this.layoutControlGroup11.CustomizationFormText = "Equipment";
            this.layoutControlGroup11.ExpandButtonVisible = true;
            this.layoutControlGroup11.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup11.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForCostTotalEquipmentExVAT,
            this.ItemForCostTotalEquipmentVATRate,
            this.ItemForCostTotalEquipmentVAT,
            this.ItemForCostTotalEquipmentCost,
            this.simpleSeparator3});
            this.layoutControlGroup11.Location = new System.Drawing.Point(0, 170);
            this.layoutControlGroup11.Name = "layoutControlGroup11";
            this.layoutControlGroup11.Size = new System.Drawing.Size(296, 144);
            this.layoutControlGroup11.Text = "Equipment";
            // 
            // ItemForCostTotalEquipmentExVAT
            // 
            this.ItemForCostTotalEquipmentExVAT.Control = this.CostTotalEquipmentExVATSpinEdit;
            this.ItemForCostTotalEquipmentExVAT.CustomizationFormText = "Cost Total Equipment Ex VAT:";
            this.ItemForCostTotalEquipmentExVAT.Location = new System.Drawing.Point(0, 0);
            this.ItemForCostTotalEquipmentExVAT.Name = "ItemForCostTotalEquipmentExVAT";
            this.ItemForCostTotalEquipmentExVAT.Size = new System.Drawing.Size(272, 24);
            this.ItemForCostTotalEquipmentExVAT.Text = "Total Ex VAT:";
            this.ItemForCostTotalEquipmentExVAT.TextSize = new System.Drawing.Size(65, 13);
            // 
            // ItemForCostTotalEquipmentVATRate
            // 
            this.ItemForCostTotalEquipmentVATRate.Control = this.CostTotalEquipmentVATRateSpinEdit;
            this.ItemForCostTotalEquipmentVATRate.CustomizationFormText = "Cost Total Equipment VAT Rate:";
            this.ItemForCostTotalEquipmentVATRate.Location = new System.Drawing.Point(0, 24);
            this.ItemForCostTotalEquipmentVATRate.Name = "ItemForCostTotalEquipmentVATRate";
            this.ItemForCostTotalEquipmentVATRate.Size = new System.Drawing.Size(272, 24);
            this.ItemForCostTotalEquipmentVATRate.Text = "VAT Rate:";
            this.ItemForCostTotalEquipmentVATRate.TextSize = new System.Drawing.Size(65, 13);
            // 
            // ItemForCostTotalEquipmentVAT
            // 
            this.ItemForCostTotalEquipmentVAT.Control = this.CostTotalEquipmentVATSpinEdit;
            this.ItemForCostTotalEquipmentVAT.CustomizationFormText = "Cost Total Equipment VAT:";
            this.ItemForCostTotalEquipmentVAT.Location = new System.Drawing.Point(0, 48);
            this.ItemForCostTotalEquipmentVAT.Name = "ItemForCostTotalEquipmentVAT";
            this.ItemForCostTotalEquipmentVAT.Size = new System.Drawing.Size(272, 24);
            this.ItemForCostTotalEquipmentVAT.Text = "Total VAT:";
            this.ItemForCostTotalEquipmentVAT.TextSize = new System.Drawing.Size(65, 13);
            // 
            // ItemForCostTotalEquipmentCost
            // 
            this.ItemForCostTotalEquipmentCost.Control = this.CostTotalEquipmentCostSpinEdit;
            this.ItemForCostTotalEquipmentCost.CustomizationFormText = "Cost Total Equipment Cost:";
            this.ItemForCostTotalEquipmentCost.Location = new System.Drawing.Point(0, 74);
            this.ItemForCostTotalEquipmentCost.Name = "ItemForCostTotalEquipmentCost";
            this.ItemForCostTotalEquipmentCost.Size = new System.Drawing.Size(272, 24);
            this.ItemForCostTotalEquipmentCost.Text = "Total Cost:";
            this.ItemForCostTotalEquipmentCost.TextSize = new System.Drawing.Size(65, 13);
            // 
            // simpleSeparator3
            // 
            this.simpleSeparator3.AllowHotTrack = false;
            this.simpleSeparator3.CustomizationFormText = "simpleSeparator3";
            this.simpleSeparator3.Location = new System.Drawing.Point(0, 72);
            this.simpleSeparator3.Name = "simpleSeparator3";
            this.simpleSeparator3.Size = new System.Drawing.Size(272, 2);
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.CustomizationFormText = "Sell";
            this.layoutControlGroup8.ExpandButtonVisible = true;
            this.layoutControlGroup8.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup10,
            this.layoutControlGroup16,
            this.layoutControlGroup14,
            this.layoutControlGroup12});
            this.layoutControlGroup8.Location = new System.Drawing.Point(320, 209);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AlignLocal;
            this.layoutControlGroup8.Size = new System.Drawing.Size(329, 624);
            this.layoutControlGroup8.Text = "Sell";
            // 
            // layoutControlGroup10
            // 
            this.layoutControlGroup10.CustomizationFormText = "Materials";
            this.layoutControlGroup10.ExpandButtonVisible = true;
            this.layoutControlGroup10.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup10.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSellTotalMaterialExVAT,
            this.ItemForSellTotalMaterialVATRate,
            this.ItemForSellTotalMaterialVAT,
            this.ItemForSellTotalMaterialCost,
            this.simpleSeparator2});
            this.layoutControlGroup10.Location = new System.Drawing.Point(0, 314);
            this.layoutControlGroup10.Name = "layoutControlGroup10";
            this.layoutControlGroup10.Size = new System.Drawing.Size(305, 144);
            this.layoutControlGroup10.Text = "Materials";
            // 
            // ItemForSellTotalMaterialExVAT
            // 
            this.ItemForSellTotalMaterialExVAT.Control = this.SellTotalMaterialExVATSpinEdit;
            this.ItemForSellTotalMaterialExVAT.CustomizationFormText = "Sell Total Material Ex VAT:";
            this.ItemForSellTotalMaterialExVAT.Location = new System.Drawing.Point(0, 0);
            this.ItemForSellTotalMaterialExVAT.Name = "ItemForSellTotalMaterialExVAT";
            this.ItemForSellTotalMaterialExVAT.Size = new System.Drawing.Size(281, 24);
            this.ItemForSellTotalMaterialExVAT.Text = "Total Ex VAT:";
            this.ItemForSellTotalMaterialExVAT.TextSize = new System.Drawing.Size(65, 13);
            // 
            // ItemForSellTotalMaterialVATRate
            // 
            this.ItemForSellTotalMaterialVATRate.Control = this.SellTotalMaterialVATRateSpinEdit;
            this.ItemForSellTotalMaterialVATRate.CustomizationFormText = "Sell Total Material VAT Rate:";
            this.ItemForSellTotalMaterialVATRate.Location = new System.Drawing.Point(0, 24);
            this.ItemForSellTotalMaterialVATRate.Name = "ItemForSellTotalMaterialVATRate";
            this.ItemForSellTotalMaterialVATRate.Size = new System.Drawing.Size(281, 24);
            this.ItemForSellTotalMaterialVATRate.Text = "VAT Rate:";
            this.ItemForSellTotalMaterialVATRate.TextSize = new System.Drawing.Size(65, 13);
            // 
            // ItemForSellTotalMaterialVAT
            // 
            this.ItemForSellTotalMaterialVAT.Control = this.SellTotalMaterialVATSpinEdit;
            this.ItemForSellTotalMaterialVAT.CustomizationFormText = "Sell Total Material VAT:";
            this.ItemForSellTotalMaterialVAT.Location = new System.Drawing.Point(0, 48);
            this.ItemForSellTotalMaterialVAT.Name = "ItemForSellTotalMaterialVAT";
            this.ItemForSellTotalMaterialVAT.Size = new System.Drawing.Size(281, 24);
            this.ItemForSellTotalMaterialVAT.Text = "Total VAT:";
            this.ItemForSellTotalMaterialVAT.TextSize = new System.Drawing.Size(65, 13);
            // 
            // ItemForSellTotalMaterialCost
            // 
            this.ItemForSellTotalMaterialCost.Control = this.SellTotalMaterialCostSpinEdit;
            this.ItemForSellTotalMaterialCost.CustomizationFormText = "Sell Total Material Cost:";
            this.ItemForSellTotalMaterialCost.Location = new System.Drawing.Point(0, 74);
            this.ItemForSellTotalMaterialCost.Name = "ItemForSellTotalMaterialCost";
            this.ItemForSellTotalMaterialCost.Size = new System.Drawing.Size(281, 24);
            this.ItemForSellTotalMaterialCost.Text = "Total Sell:";
            this.ItemForSellTotalMaterialCost.TextSize = new System.Drawing.Size(65, 13);
            // 
            // simpleSeparator2
            // 
            this.simpleSeparator2.AllowHotTrack = false;
            this.simpleSeparator2.CustomizationFormText = "simpleSeparator2";
            this.simpleSeparator2.Location = new System.Drawing.Point(0, 72);
            this.simpleSeparator2.Name = "simpleSeparator2";
            this.simpleSeparator2.Size = new System.Drawing.Size(281, 2);
            // 
            // layoutControlGroup16
            // 
            this.layoutControlGroup16.CustomizationFormText = "Total";
            this.layoutControlGroup16.ExpandButtonVisible = true;
            this.layoutControlGroup16.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup16.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSellTotalCostExVAT,
            this.ItemForSellTotalCostVAT,
            this.ItemForSellTotalCost,
            this.simpleSeparator8});
            this.layoutControlGroup16.Location = new System.Drawing.Point(0, 458);
            this.layoutControlGroup16.Name = "layoutControlGroup16";
            this.layoutControlGroup16.Size = new System.Drawing.Size(305, 120);
            this.layoutControlGroup16.Text = "Total";
            // 
            // ItemForSellTotalCostExVAT
            // 
            this.ItemForSellTotalCostExVAT.Control = this.SellTotalCostExVATSpinEdit;
            this.ItemForSellTotalCostExVAT.CustomizationFormText = "Sell Total Cost Ex VAT:";
            this.ItemForSellTotalCostExVAT.Location = new System.Drawing.Point(0, 0);
            this.ItemForSellTotalCostExVAT.Name = "ItemForSellTotalCostExVAT";
            this.ItemForSellTotalCostExVAT.Size = new System.Drawing.Size(281, 24);
            this.ItemForSellTotalCostExVAT.Text = "Total Ex VAT:";
            this.ItemForSellTotalCostExVAT.TextSize = new System.Drawing.Size(65, 13);
            // 
            // ItemForSellTotalCostVAT
            // 
            this.ItemForSellTotalCostVAT.Control = this.SellTotalCostVATSpinEdit;
            this.ItemForSellTotalCostVAT.CustomizationFormText = "Sell Total Cost VAT:";
            this.ItemForSellTotalCostVAT.Location = new System.Drawing.Point(0, 24);
            this.ItemForSellTotalCostVAT.Name = "ItemForSellTotalCostVAT";
            this.ItemForSellTotalCostVAT.Size = new System.Drawing.Size(281, 24);
            this.ItemForSellTotalCostVAT.Text = "Total VAT:";
            this.ItemForSellTotalCostVAT.TextSize = new System.Drawing.Size(65, 13);
            // 
            // ItemForSellTotalCost
            // 
            this.ItemForSellTotalCost.Control = this.SellTotalCostSpinEdit;
            this.ItemForSellTotalCost.CustomizationFormText = "Sell Total Cost:";
            this.ItemForSellTotalCost.Location = new System.Drawing.Point(0, 50);
            this.ItemForSellTotalCost.Name = "ItemForSellTotalCost";
            this.ItemForSellTotalCost.Size = new System.Drawing.Size(281, 24);
            this.ItemForSellTotalCost.Text = "Total Sell:";
            this.ItemForSellTotalCost.TextSize = new System.Drawing.Size(65, 13);
            // 
            // simpleSeparator8
            // 
            this.simpleSeparator8.AllowHotTrack = false;
            this.simpleSeparator8.CustomizationFormText = "simpleSeparator8";
            this.simpleSeparator8.Location = new System.Drawing.Point(0, 48);
            this.simpleSeparator8.Name = "simpleSeparator8";
            this.simpleSeparator8.Size = new System.Drawing.Size(281, 2);
            // 
            // layoutControlGroup14
            // 
            this.layoutControlGroup14.CustomizationFormText = "Labour";
            this.layoutControlGroup14.ExpandButtonVisible = true;
            this.layoutControlGroup14.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup14.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSellTotalLabourExVAT,
            this.ItemForSellTotalLabourVATRate,
            this.ItemForSellTotalLabourVAT,
            this.ItemForSellTotalLabourCost,
            this.simpleSeparator6,
            this.layoutControlItem5,
            this.ItemForVisitSellCalculationLevel});
            this.layoutControlGroup14.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup14.Name = "layoutControlGroup14";
            this.layoutControlGroup14.Size = new System.Drawing.Size(305, 170);
            this.layoutControlGroup14.Text = "Labour";
            // 
            // ItemForSellTotalLabourExVAT
            // 
            this.ItemForSellTotalLabourExVAT.Control = this.SellTotalLabourExVATSpinEdit;
            this.ItemForSellTotalLabourExVAT.CustomizationFormText = "Sell Total Labour Ex VAT:";
            this.ItemForSellTotalLabourExVAT.Location = new System.Drawing.Point(0, 24);
            this.ItemForSellTotalLabourExVAT.Name = "ItemForSellTotalLabourExVAT";
            this.ItemForSellTotalLabourExVAT.Size = new System.Drawing.Size(251, 26);
            this.ItemForSellTotalLabourExVAT.Text = "Total Ex VAT:";
            this.ItemForSellTotalLabourExVAT.TextSize = new System.Drawing.Size(65, 13);
            // 
            // ItemForSellTotalLabourVATRate
            // 
            this.ItemForSellTotalLabourVATRate.Control = this.SellTotalLabourVATRateSpinEdit;
            this.ItemForSellTotalLabourVATRate.CustomizationFormText = "Sell Total Labour VAT Rate:";
            this.ItemForSellTotalLabourVATRate.Location = new System.Drawing.Point(0, 50);
            this.ItemForSellTotalLabourVATRate.Name = "ItemForSellTotalLabourVATRate";
            this.ItemForSellTotalLabourVATRate.Size = new System.Drawing.Size(281, 24);
            this.ItemForSellTotalLabourVATRate.Text = "VAT Rate:";
            this.ItemForSellTotalLabourVATRate.TextSize = new System.Drawing.Size(65, 13);
            // 
            // ItemForSellTotalLabourVAT
            // 
            this.ItemForSellTotalLabourVAT.Control = this.SellTotalLabourVATSpinEdit;
            this.ItemForSellTotalLabourVAT.CustomizationFormText = "Sell Total Labour VAT:";
            this.ItemForSellTotalLabourVAT.Location = new System.Drawing.Point(0, 74);
            this.ItemForSellTotalLabourVAT.Name = "ItemForSellTotalLabourVAT";
            this.ItemForSellTotalLabourVAT.Size = new System.Drawing.Size(281, 24);
            this.ItemForSellTotalLabourVAT.Text = "Total VAT:";
            this.ItemForSellTotalLabourVAT.TextSize = new System.Drawing.Size(65, 13);
            // 
            // ItemForSellTotalLabourCost
            // 
            this.ItemForSellTotalLabourCost.Control = this.SellTotalLabourCostSpinEdit;
            this.ItemForSellTotalLabourCost.CustomizationFormText = "Sell Total Labour Cost:";
            this.ItemForSellTotalLabourCost.Location = new System.Drawing.Point(0, 100);
            this.ItemForSellTotalLabourCost.Name = "ItemForSellTotalLabourCost";
            this.ItemForSellTotalLabourCost.Size = new System.Drawing.Size(281, 24);
            this.ItemForSellTotalLabourCost.Text = "Total Sell:";
            this.ItemForSellTotalLabourCost.TextSize = new System.Drawing.Size(65, 13);
            // 
            // simpleSeparator6
            // 
            this.simpleSeparator6.AllowHotTrack = false;
            this.simpleSeparator6.CustomizationFormText = "simpleSeparator6";
            this.simpleSeparator6.Location = new System.Drawing.Point(0, 98);
            this.simpleSeparator6.Name = "simpleSeparator6";
            this.simpleSeparator6.Size = new System.Drawing.Size(281, 2);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.CalculateSellButton;
            this.layoutControlItem5.Location = new System.Drawing.Point(251, 24);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(30, 26);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(30, 26);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(30, 26);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "Re-calculate Labour Sell";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // ItemForVisitSellCalculationLevel
            // 
            this.ItemForVisitSellCalculationLevel.Control = this.VisitSellCalculationLevelTextEdit;
            this.ItemForVisitSellCalculationLevel.CustomizationFormText = "Visit Sell Method:";
            this.ItemForVisitSellCalculationLevel.Location = new System.Drawing.Point(0, 0);
            this.ItemForVisitSellCalculationLevel.Name = "ItemForVisitSellCalculationLevel";
            this.ItemForVisitSellCalculationLevel.Size = new System.Drawing.Size(281, 24);
            this.ItemForVisitSellCalculationLevel.Text = "Sell Method:";
            this.ItemForVisitSellCalculationLevel.TextSize = new System.Drawing.Size(65, 13);
            // 
            // layoutControlGroup12
            // 
            this.layoutControlGroup12.CustomizationFormText = "Equipment";
            this.layoutControlGroup12.ExpandButtonVisible = true;
            this.layoutControlGroup12.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup12.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSellTotalEquipmentExVAT,
            this.ItemForSellTotalEquipmentVATRate,
            this.ItemForSellTotalEquipmentVAT,
            this.ItemForSellTotalEquipmentCost,
            this.simpleSeparator4});
            this.layoutControlGroup12.Location = new System.Drawing.Point(0, 170);
            this.layoutControlGroup12.Name = "layoutControlGroup12";
            this.layoutControlGroup12.Size = new System.Drawing.Size(305, 144);
            this.layoutControlGroup12.Text = "Equipment";
            // 
            // ItemForSellTotalEquipmentExVAT
            // 
            this.ItemForSellTotalEquipmentExVAT.Control = this.SellTotalEquipmentExVATSpinEdit;
            this.ItemForSellTotalEquipmentExVAT.CustomizationFormText = "Sell Total Equipment Ex VAT:";
            this.ItemForSellTotalEquipmentExVAT.Location = new System.Drawing.Point(0, 0);
            this.ItemForSellTotalEquipmentExVAT.Name = "ItemForSellTotalEquipmentExVAT";
            this.ItemForSellTotalEquipmentExVAT.Size = new System.Drawing.Size(281, 24);
            this.ItemForSellTotalEquipmentExVAT.Text = "Total Ex VAT:";
            this.ItemForSellTotalEquipmentExVAT.TextSize = new System.Drawing.Size(65, 13);
            // 
            // ItemForSellTotalEquipmentVATRate
            // 
            this.ItemForSellTotalEquipmentVATRate.Control = this.SellTotalEquipmentVATRateSpinEdit;
            this.ItemForSellTotalEquipmentVATRate.CustomizationFormText = "Sell Total Equipment VAT Rate:";
            this.ItemForSellTotalEquipmentVATRate.Location = new System.Drawing.Point(0, 24);
            this.ItemForSellTotalEquipmentVATRate.Name = "ItemForSellTotalEquipmentVATRate";
            this.ItemForSellTotalEquipmentVATRate.Size = new System.Drawing.Size(281, 24);
            this.ItemForSellTotalEquipmentVATRate.Text = "VAT Rate:";
            this.ItemForSellTotalEquipmentVATRate.TextSize = new System.Drawing.Size(65, 13);
            // 
            // ItemForSellTotalEquipmentVAT
            // 
            this.ItemForSellTotalEquipmentVAT.Control = this.SellTotalEquipmentVATSpinEdit;
            this.ItemForSellTotalEquipmentVAT.CustomizationFormText = "Sell Total Equipment VAT:";
            this.ItemForSellTotalEquipmentVAT.Location = new System.Drawing.Point(0, 48);
            this.ItemForSellTotalEquipmentVAT.Name = "ItemForSellTotalEquipmentVAT";
            this.ItemForSellTotalEquipmentVAT.Size = new System.Drawing.Size(281, 24);
            this.ItemForSellTotalEquipmentVAT.Text = "Total VAT:";
            this.ItemForSellTotalEquipmentVAT.TextSize = new System.Drawing.Size(65, 13);
            // 
            // ItemForSellTotalEquipmentCost
            // 
            this.ItemForSellTotalEquipmentCost.Control = this.SellTotalEquipmentCostSpinEdit;
            this.ItemForSellTotalEquipmentCost.CustomizationFormText = "Sell Total Equipment Cost:";
            this.ItemForSellTotalEquipmentCost.Location = new System.Drawing.Point(0, 74);
            this.ItemForSellTotalEquipmentCost.Name = "ItemForSellTotalEquipmentCost";
            this.ItemForSellTotalEquipmentCost.Size = new System.Drawing.Size(281, 24);
            this.ItemForSellTotalEquipmentCost.Text = "Total Sell:";
            this.ItemForSellTotalEquipmentCost.TextSize = new System.Drawing.Size(65, 13);
            // 
            // simpleSeparator4
            // 
            this.simpleSeparator4.AllowHotTrack = false;
            this.simpleSeparator4.CustomizationFormText = "simpleSeparator4";
            this.simpleSeparator4.Location = new System.Drawing.Point(0, 72);
            this.simpleSeparator4.Name = "simpleSeparator4";
            this.simpleSeparator4.Size = new System.Drawing.Size(281, 2);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 833);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(649, 70);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 47);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(649, 10);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForClientPONumber
            // 
            this.ItemForClientPONumber.Control = this.ClientPONumberButtonEdit;
            this.ItemForClientPONumber.Location = new System.Drawing.Point(0, 0);
            this.ItemForClientPONumber.MaxSize = new System.Drawing.Size(320, 24);
            this.ItemForClientPONumber.MinSize = new System.Drawing.Size(320, 24);
            this.ItemForClientPONumber.Name = "ItemForClientPONumber";
            this.ItemForClientPONumber.Size = new System.Drawing.Size(320, 24);
            this.ItemForClientPONumber.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForClientPONumber.Text = "Client PO #:";
            this.ItemForClientPONumber.TextSize = new System.Drawing.Size(123, 13);
            // 
            // ItemForFinanceSystemPONumber
            // 
            this.ItemForFinanceSystemPONumber.Control = this.FinanceSystemPONumberTextEdit;
            this.ItemForFinanceSystemPONumber.CustomizationFormText = "Finance System PO #:";
            this.ItemForFinanceSystemPONumber.Location = new System.Drawing.Point(320, 0);
            this.ItemForFinanceSystemPONumber.MaxSize = new System.Drawing.Size(329, 24);
            this.ItemForFinanceSystemPONumber.MinSize = new System.Drawing.Size(329, 24);
            this.ItemForFinanceSystemPONumber.Name = "ItemForFinanceSystemPONumber";
            this.ItemForFinanceSystemPONumber.Size = new System.Drawing.Size(329, 24);
            this.ItemForFinanceSystemPONumber.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForFinanceSystemPONumber.Text = "Finance System PO #:";
            this.ItemForFinanceSystemPONumber.TextSize = new System.Drawing.Size(123, 13);
            // 
            // layoutControlGroup19
            // 
            this.layoutControlGroup19.CustomizationFormText = "Self-Billing Invoice";
            this.layoutControlGroup19.ExpandButtonVisible = true;
            this.layoutControlGroup19.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup19.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSelfBillingInvoicePaidDate,
            this.ItemForSelfBillingInvoiceReceivedDate,
            this.ItemForSelfBillingInvoiceID,
            this.ItemForSelfBillingInvoiceAmountPaid});
            this.layoutControlGroup19.Location = new System.Drawing.Point(0, 57);
            this.layoutControlGroup19.Name = "layoutControlGroup19";
            this.layoutControlGroup19.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AlignLocal;
            this.layoutControlGroup19.Size = new System.Drawing.Size(320, 142);
            this.layoutControlGroup19.Text = "Self-Billing Invoice";
            // 
            // ItemForSelfBillingInvoicePaidDate
            // 
            this.ItemForSelfBillingInvoicePaidDate.Control = this.SelfBillingInvoicePaidDateDateEdit;
            this.ItemForSelfBillingInvoicePaidDate.CustomizationFormText = "Self-Billing Invoice Paid Date:";
            this.ItemForSelfBillingInvoicePaidDate.Location = new System.Drawing.Point(0, 48);
            this.ItemForSelfBillingInvoicePaidDate.Name = "ItemForSelfBillingInvoicePaidDate";
            this.ItemForSelfBillingInvoicePaidDate.Size = new System.Drawing.Size(296, 24);
            this.ItemForSelfBillingInvoicePaidDate.Text = "Paid Date:";
            this.ItemForSelfBillingInvoicePaidDate.TextSize = new System.Drawing.Size(74, 13);
            // 
            // ItemForSelfBillingInvoiceReceivedDate
            // 
            this.ItemForSelfBillingInvoiceReceivedDate.Control = this.SelfBillingInvoiceReceivedDateDateEdit;
            this.ItemForSelfBillingInvoiceReceivedDate.CustomizationFormText = "Self-Billing Invoice Received Date:";
            this.ItemForSelfBillingInvoiceReceivedDate.Location = new System.Drawing.Point(0, 0);
            this.ItemForSelfBillingInvoiceReceivedDate.Name = "ItemForSelfBillingInvoiceReceivedDate";
            this.ItemForSelfBillingInvoiceReceivedDate.Size = new System.Drawing.Size(296, 24);
            this.ItemForSelfBillingInvoiceReceivedDate.Text = "Received Date:";
            this.ItemForSelfBillingInvoiceReceivedDate.TextSize = new System.Drawing.Size(74, 13);
            // 
            // ItemForSelfBillingInvoiceID
            // 
            this.ItemForSelfBillingInvoiceID.Control = this.SelfBillingInvoiceIDTextEdit;
            this.ItemForSelfBillingInvoiceID.CustomizationFormText = "Self-Billing Invoice ID:";
            this.ItemForSelfBillingInvoiceID.Location = new System.Drawing.Point(0, 24);
            this.ItemForSelfBillingInvoiceID.Name = "ItemForSelfBillingInvoiceID";
            this.ItemForSelfBillingInvoiceID.Size = new System.Drawing.Size(296, 24);
            this.ItemForSelfBillingInvoiceID.Text = "Invoice ID:";
            this.ItemForSelfBillingInvoiceID.TextSize = new System.Drawing.Size(74, 13);
            // 
            // ItemForSelfBillingInvoiceAmountPaid
            // 
            this.ItemForSelfBillingInvoiceAmountPaid.Control = this.SelfBillingInvoiceAmountPaidSpinEdit;
            this.ItemForSelfBillingInvoiceAmountPaid.CustomizationFormText = "Self-Billing Invoice Amount Paid:";
            this.ItemForSelfBillingInvoiceAmountPaid.Location = new System.Drawing.Point(0, 72);
            this.ItemForSelfBillingInvoiceAmountPaid.Name = "ItemForSelfBillingInvoiceAmountPaid";
            this.ItemForSelfBillingInvoiceAmountPaid.Size = new System.Drawing.Size(296, 24);
            this.ItemForSelfBillingInvoiceAmountPaid.Text = "Amount Paid:";
            this.ItemForSelfBillingInvoiceAmountPaid.TextSize = new System.Drawing.Size(74, 13);
            // 
            // layoutControlGroup17
            // 
            this.layoutControlGroup17.CustomizationFormText = "Client Invoice";
            this.layoutControlGroup17.ExpandButtonVisible = true;
            this.layoutControlGroup17.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup17.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForDateClientInvoiced,
            this.ItemForClientInvoiceID,
            this.emptySpaceItem16});
            this.layoutControlGroup17.Location = new System.Drawing.Point(320, 57);
            this.layoutControlGroup17.Name = "layoutControlGroup17";
            this.layoutControlGroup17.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AlignLocal;
            this.layoutControlGroup17.Size = new System.Drawing.Size(329, 142);
            this.layoutControlGroup17.Text = "Client Invoice";
            // 
            // ItemForDateClientInvoiced
            // 
            this.ItemForDateClientInvoiced.Control = this.DateClientInvoicedDateEdit;
            this.ItemForDateClientInvoiced.CustomizationFormText = "Client Invoiced Date:";
            this.ItemForDateClientInvoiced.Location = new System.Drawing.Point(0, 0);
            this.ItemForDateClientInvoiced.Name = "ItemForDateClientInvoiced";
            this.ItemForDateClientInvoiced.Size = new System.Drawing.Size(305, 24);
            this.ItemForDateClientInvoiced.Text = "Client Invoiced Date:";
            this.ItemForDateClientInvoiced.TextSize = new System.Drawing.Size(101, 13);
            // 
            // ItemForClientInvoiceID
            // 
            this.ItemForClientInvoiceID.Control = this.ClientInvoiceIDTextEdit;
            this.ItemForClientInvoiceID.CustomizationFormText = "Client Invoice ID:";
            this.ItemForClientInvoiceID.Location = new System.Drawing.Point(0, 24);
            this.ItemForClientInvoiceID.Name = "ItemForClientInvoiceID";
            this.ItemForClientInvoiceID.Size = new System.Drawing.Size(305, 24);
            this.ItemForClientInvoiceID.Text = "Invoice ID:";
            this.ItemForClientInvoiceID.TextSize = new System.Drawing.Size(101, 13);
            // 
            // emptySpaceItem16
            // 
            this.emptySpaceItem16.AllowHotTrack = false;
            this.emptySpaceItem16.Location = new System.Drawing.Point(0, 48);
            this.emptySpaceItem16.Name = "emptySpaceItem16";
            this.emptySpaceItem16.Size = new System.Drawing.Size(305, 48);
            this.emptySpaceItem16.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem17
            // 
            this.emptySpaceItem17.AllowHotTrack = false;
            this.emptySpaceItem17.CustomizationFormText = "emptySpaceItem17";
            this.emptySpaceItem17.Location = new System.Drawing.Point(0, 199);
            this.emptySpaceItem17.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem17.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem17.Name = "emptySpaceItem17";
            this.emptySpaceItem17.Size = new System.Drawing.Size(649, 10);
            this.emptySpaceItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem17.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForDoNotPayContractor
            // 
            this.ItemForDoNotPayContractor.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.ItemForDoNotPayContractor.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForDoNotPayContractor.Control = this.DoNotPayContractorCheckEdit;
            this.ItemForDoNotPayContractor.CustomizationFormText = "Do Not Pay Team:";
            this.ItemForDoNotPayContractor.Location = new System.Drawing.Point(0, 24);
            this.ItemForDoNotPayContractor.Name = "ItemForDoNotPayContractor";
            this.ItemForDoNotPayContractor.Size = new System.Drawing.Size(320, 23);
            this.ItemForDoNotPayContractor.Text = "Do Not Pay Team:";
            this.ItemForDoNotPayContractor.TextSize = new System.Drawing.Size(123, 13);
            // 
            // ItemForDoNotInvoiceClient
            // 
            this.ItemForDoNotInvoiceClient.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.ItemForDoNotInvoiceClient.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForDoNotInvoiceClient.Control = this.DoNotInvoiceClientCheckEdit;
            this.ItemForDoNotInvoiceClient.CustomizationFormText = "Do Not Invoice Client:";
            this.ItemForDoNotInvoiceClient.Location = new System.Drawing.Point(320, 24);
            this.ItemForDoNotInvoiceClient.Name = "ItemForDoNotInvoiceClient";
            this.ItemForDoNotInvoiceClient.Size = new System.Drawing.Size(329, 23);
            this.ItemForDoNotInvoiceClient.Text = "Do Not Invoice Client:";
            this.ItemForDoNotInvoiceClient.TextSize = new System.Drawing.Size(123, 13);
            // 
            // emptySpaceItem29
            // 
            this.emptySpaceItem29.AllowHotTrack = false;
            this.emptySpaceItem29.Location = new System.Drawing.Point(695, 23);
            this.emptySpaceItem29.MaxSize = new System.Drawing.Size(10, 0);
            this.emptySpaceItem29.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem29.Name = "emptySpaceItem29";
            this.emptySpaceItem29.Size = new System.Drawing.Size(10, 951);
            this.emptySpaceItem29.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem29.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp06174_OM_Job_EditTableAdapter
            // 
            this.sp06174_OM_Job_EditTableAdapter.ClearBeforeFill = true;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AllowHide = false;
            this.layoutControlItem4.Control = this.ExpectedStartDateDateEdit;
            this.layoutControlItem4.CustomizationFormText = "Expected Start Date:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "ItemForExpectedStartDate";
            this.layoutControlItem4.Size = new System.Drawing.Size(752, 24);
            this.layoutControlItem4.Text = "Start Date:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(139, 13);
            // 
            // sp06208_OM_JOb_Statuses_ListTableAdapter
            // 
            this.sp06208_OM_JOb_Statuses_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp06033_OM_Job_Manager_Linked_labourTableAdapter
            // 
            this.sp06033_OM_Job_Manager_Linked_labourTableAdapter.ClearBeforeFill = true;
            // 
            // sp06177_OM_Job_Duration_Descriptors_With_BlankTableAdapter
            // 
            this.sp06177_OM_Job_Duration_Descriptors_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp06034_OM_Job_Manager_Linked_EquipmentTableAdapter
            // 
            this.sp06034_OM_Job_Manager_Linked_EquipmentTableAdapter.ClearBeforeFill = true;
            // 
            // sp06035_OM_Job_Manager_Linked_MaterialsTableAdapter
            // 
            this.sp06035_OM_Job_Manager_Linked_MaterialsTableAdapter.ClearBeforeFill = true;
            // 
            // sp06268_OM_Cancelled_Reasons_With_BlankTableAdapter
            // 
            this.sp06268_OM_Cancelled_Reasons_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp06432_OM_Job_Suspension_Reasons_With_BlankTableAdapter
            // 
            this.sp06432_OM_Job_Suspension_Reasons_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // frm_OM_Job_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1415, 762);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_OM_Job_Edit";
            this.Text = "Edit Job - Operations";
            this.Activated += new System.EventHandler(this.frm_OM_Job_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_OM_Job_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_OM_Job_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SuspendedRemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06174OMJobEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Job)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SuspendedReasonIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06432OMJobSuspensionReasonsWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MandatoryCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DisplayOrderSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ManuallyCompletedCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitSellCalculationLevelTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitCostCalculationLevelTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPONumberButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoWorkRequiredCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CancelledReasonIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06268OMCancelledReasonsWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReworkOriginalJobIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReworkCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BillingCentreCodeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TenderJobIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TenderIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlMaterials)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06035OMJobManagerLinkedMaterialsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMaterials)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlEquipment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06034OMJobManagerLinkedEquipmentBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEquipment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LocationYTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LocationXTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DaysLeewaySpineEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaximumDaysFromLastVisitSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinimumDaysFromLastVisitSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FinishLongitudeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FinishLatitudeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartLongitudeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartLatitudeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RouteOrderSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPOIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccessPermitIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DoNotInvoiceClientCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DoNotPayContractorCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelfBillingInvoiceAmountPaidSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelfBillingInvoicePaidDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelfBillingInvoicePaidDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelfBillingInvoiceReceivedDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelfBillingInvoiceReceivedDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelfBillingInvoiceIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateClientInvoicedDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateClientInvoicedDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellTotalCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellTotalCostVATSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellTotalCostExVATSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostTotalCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostTotalCostVATSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostTotalCostExVATSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellTotalLabourCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellTotalLabourVATSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellTotalLabourVATRateSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellTotalLabourExVATSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostTotalLabourCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostTotalLabourVATSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostTotalLabourVATRateSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostTotalLabourExVATSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellTotalEquipmentCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellTotalEquipmentVATSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellTotalEquipmentVATRateSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellTotalEquipmentExVATSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostTotalEquipmentCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostTotalEquipmentVATSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostTotalEquipmentVATRateSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostTotalEquipmentExVATSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellTotalMaterialCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellTotalMaterialVATSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellTotalMaterialVATRateSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellTotalMaterialExVATSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostTotalMaterialVATRateSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostTotalMaterialVATSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostTotalMaterialCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostTotalMaterialExVATSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualEndDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualEndDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualDurationUnitsDescriptionIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06177OMJobDurationDescriptorsWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualDurationUnitsSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualStartDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualStartDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpectedDurationUnitsDescriptorIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpectedDurationUnitsSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FinanceSystemPONumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RequiresAccessPermitCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobNoLongerRequiredCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReactiveCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobTypeDescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobSubTypeDescriptionButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameContractDescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobStatusIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06208OMJObStatusesListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobTypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpectedEndDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpectedEndDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientInvoiceIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlLabour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06033OMJobManagerLinkedlabourBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLabour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobSubTypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByStaffIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitNumberButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpectedStartDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpectedStartDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobSubTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByStaffID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccessPermitID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTenderID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTenderJobID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBillingCentreCodeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReworkOriginalJobID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPOID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientNameContractDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobTypeDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobSubTypeDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDisplayOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRouteOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCancelledReasonID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReactive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRework)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobNoLongerRequired)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMinimumDaysFromLastVisit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMaximumDaysFromLastVisit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDaysLeeway)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpectedStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpectedDurationUnits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpectedEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpectedDurationUnitsDescriptorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualDurationUnits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualDurationUnitsDescriptionID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLocationX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLocationY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartLatitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartLongitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loca)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFinishLatitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFinishLongitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMandatory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNoWorkRequired)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRequiresAccessPermit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForManuallyCompleted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSuspendedReasonID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedLabourGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipmentGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMaterialGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSuspendedRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostTotalMaterialExVAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostTotalMaterialVATRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostTotalMaterialVAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostTotalMaterialCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostTotalCostExVAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostTotalCostVAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostTotalCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostTotalLabourExVAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostTotalLabourVATRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostTotalLabourVAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostTotalLabourCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitCostCalculationLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostTotalEquipmentExVAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostTotalEquipmentVATRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostTotalEquipmentVAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostTotalEquipmentCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellTotalMaterialExVAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellTotalMaterialVATRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellTotalMaterialVAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellTotalMaterialCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellTotalCostExVAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellTotalCostVAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellTotalCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellTotalLabourExVAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellTotalLabourVATRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellTotalLabourVAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellTotalLabourCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitSellCalculationLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellTotalEquipmentExVAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellTotalEquipmentVATRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellTotalEquipmentVAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellTotalEquipmentCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPONumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFinanceSystemPONumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSelfBillingInvoicePaidDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSelfBillingInvoiceReceivedDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSelfBillingInvoiceID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSelfBillingInvoiceAmountPaid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateClientInvoiced)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientInvoiceID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDoNotPayContractor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDoNotInvoiceClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DataSet_AT dataSet_AT;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00134_Vetting_Issuers_With_BlankTableAdapter sp_HR_00134_Vetting_Issuers_With_BlankTableAdapter;
        private WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00136_Employee_Vetting_Restriction_ListTableAdapter sp_HR_00136_Employee_Vetting_Restriction_ListTableAdapter;
        private WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00147_Master_Vetting_Types_With_BlankTableAdapter sp_HR_00147_Master_Vetting_Types_With_BlankTableAdapter;
        private WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00150_Master_Vetting_SubTypes_With_BlankTableAdapter sp_HR_00150_Master_Vetting_SubTypes_With_BlankTableAdapter;
        private DataSet_HR_Core dataSet_HR_Core;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.DateEdit ExpectedEndDateDateEdit;
        private DevExpress.XtraEditors.TextEdit ClientInvoiceIDTextEdit;
        private DevExpress.XtraEditors.TextEdit VisitIDTextEdit;
        private DevExpress.XtraEditors.TextEdit JobTypeIDTextEdit;
        private DevExpress.XtraEditors.TextEdit JobIDTextEdit;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraGrid.GridControl gridControlLabour;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewLabour;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraEditors.TextEdit JobSubTypeIDTextEdit;
        private DevExpress.XtraEditors.TextEdit CreatedByStaffIDTextEdit;
        private DevExpress.XtraEditors.ButtonEdit VisitNumberButtonEdit;
        private DevExpress.XtraEditors.DateEdit ExpectedStartDateDateEdit;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCreatedByStaffID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobSubTypeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobTypeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitID;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitNumber;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExpectedStartDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExpectedEndDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraBars.BarButtonItem bbiLinkedDocuments;
        private System.Windows.Forms.BindingSource sp06174OMJobEditBindingSource;
        private DataSet_OM_Job dataSet_OM_Job;
        private DataSet_OM_JobTableAdapters.sp06174_OM_Job_EditTableAdapter sp06174_OM_Job_EditTableAdapter;
        private DevExpress.XtraEditors.GridLookUpEdit JobStatusIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobStatusID;
        private DevExpress.XtraEditors.TextEdit ClientNameContractDescriptionTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientNameContractDescription;
        private DevExpress.XtraEditors.TextEdit SiteNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteName;
        private DevExpress.XtraEditors.ButtonEdit JobSubTypeDescriptionButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobSubTypeDescription;
        private DevExpress.XtraEditors.TextEdit JobTypeDescriptionTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobTypeDescription;
        private DevExpress.XtraEditors.CheckEdit ReactiveCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReactive;
        private DevExpress.XtraEditors.CheckEdit RequiresAccessPermitCheckEdit;
        private DevExpress.XtraEditors.CheckEdit JobNoLongerRequiredCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRequiresAccessPermit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobNoLongerRequired;
        private DevExpress.XtraEditors.TextEdit AccessPermitIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAccessPermitID;
        private DevExpress.XtraEditors.TextEdit ClientPOIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientPOID;
        private DevExpress.XtraEditors.TextEdit FinanceSystemPONumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraEditors.SpinEdit ExpectedDurationUnitsSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExpectedDurationUnits;
        private DevExpress.XtraEditors.GridLookUpEdit ExpectedDurationUnitsDescriptorIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExpectedDurationUnitsDescriptorID;
        private DevExpress.XtraEditors.DateEdit ActualStartDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActualStartDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraEditors.SpinEdit ActualDurationUnitsSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActualDurationUnits;
        private DevExpress.XtraEditors.GridLookUpEdit ActualDurationUnitsDescriptionIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActualDurationUnitsDescriptionID;
        private DevExpress.XtraEditors.DateEdit ActualEndDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActualEndDate;
        private DevExpress.XtraEditors.SpinEdit CostTotalMaterialVATRateSpinEdit;
        private DevExpress.XtraEditors.SpinEdit CostTotalMaterialVATSpinEdit;
        private DevExpress.XtraEditors.SpinEdit CostTotalMaterialCostSpinEdit;
        private DevExpress.XtraEditors.SpinEdit CostTotalMaterialExVATSpinEdit;
        private DevExpress.XtraEditors.SpinEdit SellTotalMaterialCostSpinEdit;
        private DevExpress.XtraEditors.SpinEdit SellTotalMaterialVATSpinEdit;
        private DevExpress.XtraEditors.SpinEdit SellTotalMaterialVATRateSpinEdit;
        private DevExpress.XtraEditors.SpinEdit SellTotalMaterialExVATSpinEdit;
        private DevExpress.XtraEditors.SpinEdit CostTotalEquipmentVATSpinEdit;
        private DevExpress.XtraEditors.SpinEdit CostTotalEquipmentVATRateSpinEdit;
        private DevExpress.XtraEditors.SpinEdit CostTotalEquipmentExVATSpinEdit;
        private DevExpress.XtraEditors.SpinEdit CostTotalEquipmentCostSpinEdit;
        private DevExpress.XtraEditors.SpinEdit SellTotalEquipmentExVATSpinEdit;
        private DevExpress.XtraEditors.SpinEdit SellTotalEquipmentVATRateSpinEdit;
        private DevExpress.XtraEditors.SpinEdit SellTotalEquipmentCostSpinEdit;
        private DevExpress.XtraEditors.SpinEdit SellTotalEquipmentVATSpinEdit;
        private DevExpress.XtraEditors.SpinEdit CostTotalLabourCostSpinEdit;
        private DevExpress.XtraEditors.SpinEdit CostTotalLabourVATSpinEdit;
        private DevExpress.XtraEditors.SpinEdit CostTotalLabourVATRateSpinEdit;
        private DevExpress.XtraEditors.SpinEdit CostTotalLabourExVATSpinEdit;
        private DevExpress.XtraEditors.SpinEdit SellTotalLabourExVATSpinEdit;
        private DevExpress.XtraEditors.SpinEdit SellTotalLabourCostSpinEdit;
        private DevExpress.XtraEditors.SpinEdit SellTotalLabourVATSpinEdit;
        private DevExpress.XtraEditors.SpinEdit SellTotalLabourVATRateSpinEdit;
        private DevExpress.XtraEditors.SpinEdit CostTotalCostExVATSpinEdit;
        private DevExpress.XtraEditors.SpinEdit CostTotalCostSpinEdit;
        private DevExpress.XtraEditors.SpinEdit CostTotalCostVATSpinEdit;
        private DevExpress.XtraEditors.SpinEdit SellTotalCostExVATSpinEdit;
        private DevExpress.XtraEditors.SpinEdit SellTotalCostSpinEdit;
        private DevExpress.XtraEditors.SpinEdit SellTotalCostVATSpinEdit;
        private DevExpress.XtraEditors.DateEdit DateClientInvoicedDateEdit;
        private DevExpress.XtraEditors.DateEdit SelfBillingInvoiceReceivedDateDateEdit;
        private DevExpress.XtraEditors.TextEdit SelfBillingInvoiceIDTextEdit;
        private DevExpress.XtraEditors.DateEdit SelfBillingInvoicePaidDateDateEdit;
        private DevExpress.XtraEditors.SpinEdit SelfBillingInvoiceAmountPaidSpinEdit;
        private DevExpress.XtraEditors.CheckEdit DoNotPayContractorCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDoNotPayContractor;
        private DevExpress.XtraEditors.CheckEdit DoNotInvoiceClientCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDoNotInvoiceClient;
        private DevExpress.XtraEditors.SpinEdit RouteOrderSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRouteOrder;
        private DevExpress.XtraEditors.TextEdit StartLatitudeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStartLatitude;
        private DevExpress.XtraEditors.TextEdit FinishLongitudeTextEdit;
        private DevExpress.XtraEditors.TextEdit FinishLatitudeTextEdit;
        private DevExpress.XtraEditors.TextEdit StartLongitudeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStartLongitude;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFinishLatitude;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFinishLongitude;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup20;
        private DevExpress.XtraEditors.TextEdit TenderJobIDTextEdit;
        private DevExpress.XtraEditors.TextEdit TenderIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTenderID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTenderJobID;
        private DevExpress.XtraEditors.SpinEdit MaximumDaysFromLastVisitSpinEdit;
        private DevExpress.XtraEditors.SpinEdit MinimumDaysFromLastVisitSpinEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup21;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMinimumDaysFromLastVisit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMaximumDaysFromLastVisit;
        private DevExpress.XtraEditors.SpinEdit DaysLeewaySpineEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDaysLeeway;
        private DevExpress.XtraEditors.TextEdit BillingCentreCodeIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBillingCentreCodeID;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup25;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedLabourGrid;
        private DevExpress.XtraLayout.LayoutControlGroup loca;
        private DevExpress.XtraBars.BarButtonItem bbiViewOnMap;
        private DevExpress.XtraEditors.TextEdit LocationYTextEdit;
        private DevExpress.XtraEditors.TextEdit LocationXTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup23;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLocationX;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLocationY;
        private System.Windows.Forms.BindingSource sp06208OMJObStatusesListBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DataSet_OM_JobTableAdapters.sp06208_OM_JOb_Statuses_ListTableAdapter sp06208_OM_JOb_Statuses_ListTableAdapter;
        private System.Windows.Forms.BindingSource sp06033OMJobManagerLinkedlabourBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourUsedID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobID1;
        private DevExpress.XtraGrid.Columns.GridColumn colCostUnitsUsed;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP;
        private DevExpress.XtraGrid.Columns.GridColumn colCostUnitDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colSellUnitsUsed;
        private DevExpress.XtraGrid.Columns.GridColumn colSellUnitDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colCostPerUnitExVat;
        private DevExpress.XtraGrid.Columns.GridColumn colCostPerUnitVatRate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage;
        private DevExpress.XtraGrid.Columns.GridColumn colSellPerUnitExVat;
        private DevExpress.XtraGrid.Columns.GridColumn colSellPerUnitVatRate;
        private DevExpress.XtraGrid.Columns.GridColumn colCostValueExVat;
        private DevExpress.XtraGrid.Columns.GridColumn colCostValueVat;
        private DevExpress.XtraGrid.Columns.GridColumn colCostValueTotal;
        private DevExpress.XtraGrid.Columns.GridColumn colSellValueExVat;
        private DevExpress.XtraGrid.Columns.GridColumn colSellValueVat;
        private DevExpress.XtraGrid.Columns.GridColumn colSellValueTotal;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedStartDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitNumber1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditInteger;
        private DevExpress.XtraGrid.Columns.GridColumn colFullDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colSelected1;
        private DevExpress.XtraGrid.Columns.GridColumn colCostUnitDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colSellUnitDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourType;
        private DevExpress.XtraGrid.Columns.GridColumn colCISPercentage;
        private DevExpress.XtraGrid.Columns.GridColumn colCISValue;
        private DevExpress.XtraGrid.Columns.GridColumn colPurchaseOrderID;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitID1;
        private DataSet_OM_JobTableAdapters.sp06033_OM_Job_Manager_Linked_labourTableAdapter sp06033_OM_Job_Manager_Linked_labourTableAdapter;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup24;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup26;
        private System.Windows.Forms.BindingSource sp06177OMJobDurationDescriptorsWithBlankBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DataSet_OM_JobTableAdapters.sp06177_OM_Job_Duration_Descriptors_With_BlankTableAdapter sp06177_OM_Job_Duration_Descriptors_With_BlankTableAdapter;
        private DevExpress.XtraGrid.GridControl gridControlEquipment;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewEquipment;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn37;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn41;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn42;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn43;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn44;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEquipmentGrid;
        private System.Windows.Forms.BindingSource sp06034OMJobManagerLinkedEquipmentBindingSource;
        private DataSet_OM_JobTableAdapters.sp06034_OM_Job_Manager_Linked_EquipmentTableAdapter sp06034_OM_Job_Manager_Linked_EquipmentTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerName;
        private DevExpress.XtraGrid.GridControl gridControlMaterials;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewMaterials;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn39;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn40;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn45;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn46;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn47;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn48;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn49;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn50;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn51;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn52;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn53;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn54;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn55;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn56;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn57;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn58;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn59;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn60;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn61;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn62;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn63;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn64;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn65;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn66;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn67;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn68;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn69;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn70;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn71;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn72;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn73;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn75;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMaterialGrid;
        private System.Windows.Forms.BindingSource sp06035OMJobManagerLinkedMaterialsBindingSource;
        private DataSet_OM_JobTableAdapters.sp06035_OM_Job_Manager_Linked_MaterialsTableAdapter sp06035_OM_Job_Manager_Linked_MaterialsTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitID;
        private DevExpress.XtraEditors.CheckEdit ReworkCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRework;
        private DevExpress.XtraEditors.TextEdit ReworkOriginalJobIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReworkOriginalJobID;
        private DevExpress.XtraEditors.GridLookUpEdit CancelledReasonIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn74;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn76;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn77;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCancelledReasonID;
        private System.Windows.Forms.BindingSource sp06268OMCancelledReasonsWithBlankBindingSource;
        private DataSet_OM_JobTableAdapters.sp06268_OM_Cancelled_Reasons_With_BlankTableAdapter sp06268_OM_Cancelled_Reasons_With_BlankTableAdapter;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup27;
        private DevExpress.XtraEditors.CheckEdit NoWorkRequiredCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNoWorkRequired;
        private DevExpress.XtraEditors.ButtonEdit ClientPONumberButtonEdit;
        private DevExpress.XtraEditors.SimpleButton CalculateCostButton;
        private DevExpress.XtraEditors.SimpleButton CalculateSellButton;
        private DevExpress.XtraEditors.TextEdit VisitSellCalculationLevelTextEdit;
        private DevExpress.XtraEditors.TextEdit VisitCostCalculationLevelTextEdit;
        private DevExpress.XtraEditors.CheckEdit ManuallyCompletedCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForManuallyCompleted;
        private DevExpress.XtraEditors.SpinEdit DisplayOrderSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDisplayOrder;
        private DevExpress.XtraEditors.CheckEdit MandatoryCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMandatory;
        private DevExpress.XtraEditors.MemoEdit SuspendedRemarksMemoEdit;
        private DevExpress.XtraEditors.GridLookUpEdit SuspendedReasonIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn78;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn79;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn80;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSuspendedRemarks;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSuspendedReasonID;
        private System.Windows.Forms.BindingSource sp06432OMJobSuspensionReasonsWithBlankBindingSource;
        private DataSet_OM_JobTableAdapters.sp06432_OM_Job_Suspension_Reasons_With_BlankTableAdapter sp06432_OM_Job_Suspension_Reasons_With_BlankTableAdapter;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup29;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem27;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem28;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem22;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem25;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem26;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostTotalMaterialExVAT;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostTotalMaterialVATRate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostTotalMaterialVAT;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostTotalMaterialCost;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup15;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostTotalCostExVAT;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostTotalCostVAT;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostTotalCost;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator7;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup13;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostTotalLabourExVAT;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostTotalLabourVATRate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostTotalLabourVAT;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostTotalLabourCost;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitCostCalculationLevel;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup11;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostTotalEquipmentExVAT;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostTotalEquipmentVATRate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostTotalEquipmentVAT;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostTotalEquipmentCost;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup10;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSellTotalMaterialExVAT;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSellTotalMaterialVATRate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSellTotalMaterialVAT;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSellTotalMaterialCost;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup16;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSellTotalCostExVAT;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSellTotalCostVAT;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSellTotalCost;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator8;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup14;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSellTotalLabourExVAT;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSellTotalLabourVATRate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSellTotalLabourVAT;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSellTotalLabourCost;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitSellCalculationLevel;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup12;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSellTotalEquipmentExVAT;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSellTotalEquipmentVATRate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSellTotalEquipmentVAT;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSellTotalEquipmentCost;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientPONumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFinanceSystemPONumber;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private DevExpress.XtraLayout.SplitterItem splitterItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem15;
        private DevExpress.XtraLayout.LayoutControlGroup layGrpRemarks;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup19;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSelfBillingInvoicePaidDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSelfBillingInvoiceReceivedDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSelfBillingInvoiceID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSelfBillingInvoiceAmountPaid;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup17;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDateClientInvoiced;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientInvoiceID;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem16;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem17;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup18;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem20;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem19;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem21;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem24;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem29;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
    }
}

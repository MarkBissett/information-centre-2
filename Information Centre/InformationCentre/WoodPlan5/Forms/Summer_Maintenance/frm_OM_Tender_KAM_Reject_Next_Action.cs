﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseObjects;
using WoodPlan5.Properties;
using DevExpress.XtraEditors;
using DevExpress.Utils;

namespace WoodPlan5
{
    public partial class frm_OM_Tender_KAM_Reject_Next_Action : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        public int _NextStepID = 0;
        public int _RejectionReasonID = 0;
        public string _RejectionReason = "";
        public string _KAMQuoteRejectionRemarks = "";
           
        #endregion

        public frm_OM_Tender_KAM_Reject_Next_Action()
        {
            InitializeComponent();
        }

        private void frm_OM_Tender_KAM_Reject_Next_Action_Load(object sender, EventArgs e)
        {
            this.FormID = 500289;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            Application.DoEvents();  // Allow Form time to repaint itself //

            strConnectionString = this.GlobalSettings.ConnectionString;
            sp06502_OM_Tender_Quote_Rejection_Reasons_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06502_OM_Tender_Quote_Rejection_Reasons_With_BlankTableAdapter.Fill(dataSet_OM_Tender.sp06502_OM_Tender_Quote_Rejection_Reasons_With_Blank, 1);
            KAMQuoteRejectionReasonIDGridLookUpEdit.EditValue = 0;

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
        }

      
        private void btnOK_Click(object sender, EventArgs e)
        {
            if (checkEditSendBackToCM.Checked)
            {
                _NextStepID = 1;
            }
            else
            {
                _NextStepID = 2;
            }

            _RejectionReasonID = Convert.ToInt32(KAMQuoteRejectionReasonIDGridLookUpEdit.EditValue);
            _RejectionReason = KAMQuoteRejectionReasonIDGridLookUpEdit.Text;
            _KAMQuoteRejectionRemarks = KAMQuoteRejectionRemarksMemoEdit.Text;

            if (_RejectionReasonID <= 0)
            {
                XtraMessageBox.Show("Select the Rejection Reason before proceeding.", "KAM Reject Tender", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }








    }
}

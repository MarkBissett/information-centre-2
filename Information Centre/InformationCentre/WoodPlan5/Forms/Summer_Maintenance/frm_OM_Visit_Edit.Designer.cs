namespace WoodPlan5
{
    partial class frm_OM_Visit_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Visit_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule2 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue2 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition3 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling13 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling14 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions7 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject25 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject26 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject27 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject28 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition4 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling15 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition5 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling16 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling17 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling18 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling19 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling20 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule3 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue3 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling21 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling22 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling23 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling24 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling25 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions8 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject29 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject30 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject31 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject32 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling26 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bbiViewOnMap = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.SelfBillingInvoiceNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.sp06145OMVisitEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Visit = new WoodPlan5.DataSet_OM_Visit();
            this.ExtraCostsFinalisedCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.VisitEquipmentSellSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.VisitMaterialSellSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.VisitLabourSellSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.VisitEquipmentCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.VisitMaterialCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.VisitLabourCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.DoNotInvoiceClientCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.DoNotPayContractorCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.CalculateDaysButton = new DevExpress.XtraEditors.SimpleButton();
            this.SiteContractDaysSeparationPercentTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.DaysSeparationSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.StatusIssueIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06430OMVisitStatusIssuesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CompletionSheetNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CreatedFromVisitTemplateIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06141OMVisitTemplateHeadersWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.VisitStatusIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.StatusDescriptionButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.SiteIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientPOIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ManagerNotesMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ManagerNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.NoOnetoSignCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ClientInvoiceIDButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.SelfBillingInvoiceButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.CalculateSellButton = new DevExpress.XtraEditors.SimpleButton();
            this.CalculateCostButton = new DevExpress.XtraEditors.SimpleButton();
            this.SellCalculationLevelGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06134OMJobCalculationLevelDescriptorsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ClientPONumberButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.VisitTypeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06308OMVisitTypesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FriendlyVisitNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AccidentCommentMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.AccidentOnSiteCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ExtraWorkCommentsMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ExtraWorkTypeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06267OMExtraWorkTypesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ExtraWorkRequiredCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ReworkCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.TimelinessTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ImagesFolderOMTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientSignaturePathButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.VisitCategoryIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06250OMVisitCategoriesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IssueRemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.IssueTypeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06251OMIssueTypesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IssueFoundCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.WorkNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.FinishLongitudeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.FinishLatitudeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.StartLongitudeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.StartLatitudeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.VisitSellSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CostCalculationLevelGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ExpectedEndDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.ExpectedStartDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.CreatedByStaffNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.VisitNumberSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CreatedByStaffIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientContractIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteContractIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.VisitCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.EndDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.StartDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.LinkedToParentButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.VisitIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ItemForClientContractID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteContractID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForVisitID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCreatedByStaffID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForImagesFolderOM = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientPOID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForVisitStatusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteContractDaysSeparationPercent = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForExtraWorkRequired = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForExtraWorkComments = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForExtraWorkTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup17 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForClientPONumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCompletionSheetNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWorkNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem24 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupTeamCosts = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForCostCalculationLevel = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForVisitCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForVisitCostButton = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSelfBillingInvoice = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDoNotPayContractor = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForVisitLabourCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForVisitMaterialCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForVisitEquipmentCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSelfBillingInvoiceNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupClientSell = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSellCalculationLevel = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDoNotInvoiceClient = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForVisitSell = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForVisitSellButton = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientInvoiceID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem20 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForVisitLabourSell = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForVisitMaterialSell = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForVisitEquipmentSell = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem18 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem19 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForExtraCostsFinalised = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForLinkedToParent = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem23 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForVisitTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForVisitCategoryID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTimeliness = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStatusDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForIssueRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRework = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStatusIssueID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIssueTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup16 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForAccidentOnSite = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForAccidentComment = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCreatedByStaffName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCreatedFromVisitTemplateID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForVisitNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFriendlyVisitNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForExpectedStartDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForExpectedEndDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStartDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEndDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCalculateDaysButton = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDaysSeparation = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForNoOnetoSign = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForManagerName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientSignaturePath = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForManagerNotes = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem15 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem16 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem17 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup15 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForStartLatitude = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemforFinishLatitude = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStartLongitude = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFinishLongitude = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.sp06145_OM_Visit_EditTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06145_OM_Visit_EditTableAdapter();
            this.sp06134_OM_Job_Calculation_Level_DescriptorsTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06134_OM_Job_Calculation_Level_DescriptorsTableAdapter();
            this.sp06250_OM_Visit_Categories_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06250_OM_Visit_Categories_With_BlankTableAdapter();
            this.sp06251_OM_Issue_Types_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06251_OM_Issue_Types_With_BlankTableAdapter();
            this.sp06267_OM_Extra_Work_Types_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06267_OM_Extra_Work_Types_With_BlankTableAdapter();
            this.sp06308_OM_Visit_TypesTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06308_OM_Visit_TypesTableAdapter();
            this.sp06141_OM_Visit_Template_Headers_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06141_OM_Visit_Template_Headers_With_BlankTableAdapter();
            this.sp06430_OM_Visit_Status_Issues_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06430_OM_Visit_Status_Issues_With_BlankTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SelfBillingInvoiceNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06145OMVisitEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Visit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExtraCostsFinalisedCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitEquipmentSellSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitMaterialSellSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitLabourSellSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitEquipmentCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitMaterialCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitLabourCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DoNotInvoiceClientCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DoNotPayContractorCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteContractDaysSeparationPercentTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DaysSeparationSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusIssueIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06430OMVisitStatusIssuesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CompletionSheetNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedFromVisitTemplateIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06141OMVisitTemplateHeadersWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitStatusIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusDescriptionButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPOIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ManagerNotesMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ManagerNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoOnetoSignCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientInvoiceIDButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelfBillingInvoiceButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellCalculationLevelGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06134OMJobCalculationLevelDescriptorsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPONumberButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitTypeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06308OMVisitTypesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FriendlyVisitNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentCommentMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentOnSiteCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExtraWorkCommentsMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExtraWorkTypeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06267OMExtraWorkTypesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExtraWorkRequiredCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReworkCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimelinessTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImagesFolderOMTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientSignaturePathButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitCategoryIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06250OMVisitCategoriesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IssueRemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IssueTypeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06251OMIssueTypesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IssueFoundCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FinishLongitudeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FinishLatitudeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartLongitudeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartLatitudeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitSellSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostCalculationLevelGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpectedEndDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpectedEndDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpectedStartDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpectedStartDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByStaffNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitNumberSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByStaffIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientContractIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteContractIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToParentButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientContractID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteContractID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByStaffID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForImagesFolderOM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPOID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteContractDaysSeparationPercent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExtraWorkRequired)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExtraWorkComments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExtraWorkTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPONumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCompletionSheetNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupTeamCosts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostCalculationLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitCostButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSelfBillingInvoice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDoNotPayContractor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitLabourCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitMaterialCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitEquipmentCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSelfBillingInvoiceNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupClientSell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellCalculationLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDoNotInvoiceClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitSell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitSellButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientInvoiceID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitLabourSell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitMaterialSell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitEquipmentSell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExtraCostsFinalised)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToParent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitCategoryID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTimeliness)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIssueRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRework)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusIssueID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIssueTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccidentOnSite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccidentComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByStaffName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedFromVisitTemplateID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFriendlyVisitNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpectedStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpectedEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCalculateDaysButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDaysSeparation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNoOnetoSign)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForManagerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientSignaturePath)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForManagerNotes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartLatitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemforFinishLatitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartLongitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFinishLongitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(1383, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 682);
            this.barDockControlBottom.Size = new System.Drawing.Size(1383, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 656);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1383, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 656);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Images = this.imageCollection1;
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "ID";
            this.gridColumn19.FieldName = "ID";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.OptionsColumn.AllowFocus = false;
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            this.gridColumn19.Width = 53;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "ID";
            this.gridColumn4.FieldName = "ID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Width = 53;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "ID";
            this.gridColumn16.FieldName = "ID";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowFocus = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.Width = 53;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "ID";
            this.gridColumn13.FieldName = "ID";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Width = 53;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "ID";
            this.gridColumn10.FieldName = "ID";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Width = 53;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "ID";
            this.gridColumn1.FieldName = "ID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Width = 53;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "ID";
            this.gridColumn7.FieldName = "ID";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Width = 53;
            // 
            // colID1
            // 
            this.colID1.Caption = "ID";
            this.colID1.FieldName = "ID";
            this.colID1.Name = "colID1";
            this.colID1.OptionsColumn.AllowEdit = false;
            this.colID1.OptionsColumn.AllowFocus = false;
            this.colID1.OptionsColumn.ReadOnly = true;
            this.colID1.Width = 53;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation,
            this.bbiViewOnMap});
            this.barManager2.MaxItemId = 16;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiViewOnMap, true)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.ImageOptions.Image")));
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.ImageOptions.Image")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bbiViewOnMap
            // 
            this.bbiViewOnMap.Caption = "View On Map";
            this.bbiViewOnMap.Id = 15;
            this.bbiViewOnMap.ImageOptions.ImageIndex = 6;
            this.bbiViewOnMap.Name = "bbiViewOnMap";
            this.bbiViewOnMap.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem3.Text = "Show On Map - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to View the Site Location and the Visit Start and End Position on the Ma" +
    "p.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiViewOnMap.SuperTip = superToolTip3;
            this.bbiViewOnMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiViewOnMap_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageOptions.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem4.Text = "Form Mode - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.barStaticItemFormMode.SuperTip = superToolTip4;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageOptions.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(1383, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 682);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(1383, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 656);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(1383, 26);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 656);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Preview_16x16, "Preview_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Calculate_16x16, "Calculate_16x16", typeof(global::WoodPlan5.Properties.Resources), 5);
            this.imageCollection1.Images.SetKeyName(5, "Calculate_16x16");
            this.imageCollection1.InsertGalleryImage("geopoint_16x16.png", "images/maps/geopoint_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/maps/geopoint_16x16.png"), 6);
            this.imageCollection1.Images.SetKeyName(6, "geopoint_16x16.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.SelfBillingInvoiceNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ExtraCostsFinalisedCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.VisitEquipmentSellSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.VisitMaterialSellSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.VisitLabourSellSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.VisitEquipmentCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.VisitMaterialCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.VisitLabourCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.DoNotInvoiceClientCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.DoNotPayContractorCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.CalculateDaysButton);
            this.dataLayoutControl1.Controls.Add(this.SiteContractDaysSeparationPercentTextEdit);
            this.dataLayoutControl1.Controls.Add(this.DaysSeparationSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.StatusIssueIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.CompletionSheetNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CreatedFromVisitTemplateIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.VisitStatusIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.StatusDescriptionButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientPOIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ManagerNotesMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.ManagerNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.NoOnetoSignCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientInvoiceIDButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.SelfBillingInvoiceButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.CalculateSellButton);
            this.dataLayoutControl1.Controls.Add(this.CalculateCostButton);
            this.dataLayoutControl1.Controls.Add(this.SellCalculationLevelGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientPONumberButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.VisitTypeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.FriendlyVisitNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AccidentCommentMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.AccidentOnSiteCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ExtraWorkCommentsMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.ExtraWorkTypeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ExtraWorkRequiredCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ReworkCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.TimelinessTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ImagesFolderOMTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientSignaturePathButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.VisitCategoryIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.IssueRemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.IssueTypeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.IssueFoundCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.WorkNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.FinishLongitudeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.FinishLatitudeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.StartLongitudeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.StartLatitudeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.VisitSellSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.CostCalculationLevelGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ExpectedEndDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.ExpectedStartDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.CreatedByStaffNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.VisitNumberSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.CreatedByStaffIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientContractIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteContractIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.VisitCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.EndDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.StartDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.LinkedToParentButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.VisitIDTextEdit);
            this.dataLayoutControl1.DataSource = this.sp06145OMVisitEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForClientContractID,
            this.ItemForSiteContractID,
            this.ItemForVisitID,
            this.ItemForCreatedByStaffID,
            this.ItemForImagesFolderOM,
            this.ItemForClientPOID,
            this.ItemForClientID,
            this.ItemForVisitStatusID,
            this.ItemForSiteContractDaysSeparationPercent});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(43, 167, 441, 409);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1383, 656);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // SelfBillingInvoiceNumberTextEdit
            // 
            this.SelfBillingInvoiceNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "SelfBillingInvoiceNumber", true));
            this.SelfBillingInvoiceNumberTextEdit.Location = new System.Drawing.Point(931, 330);
            this.SelfBillingInvoiceNumberTextEdit.MenuManager = this.barManager1;
            this.SelfBillingInvoiceNumberTextEdit.Name = "SelfBillingInvoiceNumberTextEdit";
            this.SelfBillingInvoiceNumberTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SelfBillingInvoiceNumberTextEdit, true);
            this.SelfBillingInvoiceNumberTextEdit.Size = new System.Drawing.Size(124, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SelfBillingInvoiceNumberTextEdit, optionsSpelling1);
            this.SelfBillingInvoiceNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.SelfBillingInvoiceNumberTextEdit.TabIndex = 73;
            // 
            // sp06145OMVisitEditBindingSource
            // 
            this.sp06145OMVisitEditBindingSource.DataMember = "sp06145_OM_Visit_Edit";
            this.sp06145OMVisitEditBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // dataSet_OM_Visit
            // 
            this.dataSet_OM_Visit.DataSetName = "DataSet_OM_Visit";
            this.dataSet_OM_Visit.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ExtraCostsFinalisedCheckEdit
            // 
            this.ExtraCostsFinalisedCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "ExtraCostsFinalised", true));
            this.ExtraCostsFinalisedCheckEdit.Location = new System.Drawing.Point(1194, 93);
            this.ExtraCostsFinalisedCheckEdit.MenuManager = this.barManager1;
            this.ExtraCostsFinalisedCheckEdit.Name = "ExtraCostsFinalisedCheckEdit";
            this.ExtraCostsFinalisedCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.ExtraCostsFinalisedCheckEdit.Properties.ValueChecked = 1;
            this.ExtraCostsFinalisedCheckEdit.Properties.ValueUnchecked = 0;
            this.ExtraCostsFinalisedCheckEdit.Size = new System.Drawing.Size(76, 19);
            this.ExtraCostsFinalisedCheckEdit.StyleController = this.dataLayoutControl1;
            this.ExtraCostsFinalisedCheckEdit.TabIndex = 72;
            // 
            // VisitEquipmentSellSpinEdit
            // 
            this.VisitEquipmentSellSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "VisitEquipmentSell", true));
            this.VisitEquipmentSellSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.VisitEquipmentSellSpinEdit.Location = new System.Drawing.Point(1206, 256);
            this.VisitEquipmentSellSpinEdit.MenuManager = this.barManager1;
            this.VisitEquipmentSellSpinEdit.Name = "VisitEquipmentSellSpinEdit";
            this.VisitEquipmentSellSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.VisitEquipmentSellSpinEdit.Properties.Mask.EditMask = "c";
            this.VisitEquipmentSellSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.VisitEquipmentSellSpinEdit.Size = new System.Drawing.Size(96, 20);
            this.VisitEquipmentSellSpinEdit.StyleController = this.dataLayoutControl1;
            this.VisitEquipmentSellSpinEdit.TabIndex = 21;
            this.VisitEquipmentSellSpinEdit.EditValueChanged += new System.EventHandler(this.VisitEquipmentSellSpinEdit_EditValueChanged);
            this.VisitEquipmentSellSpinEdit.Validated += new System.EventHandler(this.VisitEquipmentSellSpinEdit_Validated);
            // 
            // VisitMaterialSellSpinEdit
            // 
            this.VisitMaterialSellSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "VisitMaterialSell", true));
            this.VisitMaterialSellSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.VisitMaterialSellSpinEdit.Location = new System.Drawing.Point(1206, 232);
            this.VisitMaterialSellSpinEdit.MenuManager = this.barManager1;
            this.VisitMaterialSellSpinEdit.Name = "VisitMaterialSellSpinEdit";
            this.VisitMaterialSellSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.VisitMaterialSellSpinEdit.Properties.Mask.EditMask = "c";
            this.VisitMaterialSellSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.VisitMaterialSellSpinEdit.Size = new System.Drawing.Size(96, 20);
            this.VisitMaterialSellSpinEdit.StyleController = this.dataLayoutControl1;
            this.VisitMaterialSellSpinEdit.TabIndex = 21;
            this.VisitMaterialSellSpinEdit.EditValueChanged += new System.EventHandler(this.VisitMaterialSellSpinEdit_EditValueChanged);
            this.VisitMaterialSellSpinEdit.Validated += new System.EventHandler(this.VisitMaterialSellSpinEdit_Validated);
            // 
            // VisitLabourSellSpinEdit
            // 
            this.VisitLabourSellSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "VisitLabourSell", true));
            this.VisitLabourSellSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.VisitLabourSellSpinEdit.Location = new System.Drawing.Point(1206, 208);
            this.VisitLabourSellSpinEdit.MenuManager = this.barManager1;
            this.VisitLabourSellSpinEdit.Name = "VisitLabourSellSpinEdit";
            this.VisitLabourSellSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.VisitLabourSellSpinEdit.Properties.Mask.EditMask = "c";
            this.VisitLabourSellSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.VisitLabourSellSpinEdit.Size = new System.Drawing.Size(96, 20);
            this.VisitLabourSellSpinEdit.StyleController = this.dataLayoutControl1;
            this.VisitLabourSellSpinEdit.TabIndex = 21;
            this.VisitLabourSellSpinEdit.EditValueChanged += new System.EventHandler(this.VisitLabourSellSpinEdit_EditValueChanged);
            this.VisitLabourSellSpinEdit.Validated += new System.EventHandler(this.VisitLabourSellSpinEdit_Validated);
            // 
            // VisitEquipmentCostSpinEdit
            // 
            this.VisitEquipmentCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "VisitEquipmentCost", true));
            this.VisitEquipmentCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.VisitEquipmentCostSpinEdit.Location = new System.Drawing.Point(931, 256);
            this.VisitEquipmentCostSpinEdit.MenuManager = this.barManager1;
            this.VisitEquipmentCostSpinEdit.Name = "VisitEquipmentCostSpinEdit";
            this.VisitEquipmentCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.VisitEquipmentCostSpinEdit.Properties.Mask.EditMask = "c";
            this.VisitEquipmentCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.VisitEquipmentCostSpinEdit.Size = new System.Drawing.Size(96, 20);
            this.VisitEquipmentCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.VisitEquipmentCostSpinEdit.TabIndex = 21;
            this.VisitEquipmentCostSpinEdit.EditValueChanged += new System.EventHandler(this.VisitEquipmentCostSpinEdit_EditValueChanged);
            this.VisitEquipmentCostSpinEdit.Validated += new System.EventHandler(this.VisitEquipmentCostSpinEdit_Validated);
            // 
            // VisitMaterialCostSpinEdit
            // 
            this.VisitMaterialCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "VisitMaterialCost", true));
            this.VisitMaterialCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.VisitMaterialCostSpinEdit.Location = new System.Drawing.Point(931, 232);
            this.VisitMaterialCostSpinEdit.MenuManager = this.barManager1;
            this.VisitMaterialCostSpinEdit.Name = "VisitMaterialCostSpinEdit";
            this.VisitMaterialCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.VisitMaterialCostSpinEdit.Properties.Mask.EditMask = "c";
            this.VisitMaterialCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.VisitMaterialCostSpinEdit.Size = new System.Drawing.Size(96, 20);
            this.VisitMaterialCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.VisitMaterialCostSpinEdit.TabIndex = 21;
            this.VisitMaterialCostSpinEdit.EditValueChanged += new System.EventHandler(this.VisitMaterialCostSpinEdit_EditValueChanged);
            this.VisitMaterialCostSpinEdit.Validated += new System.EventHandler(this.VisitMaterialCostSpinEdit_Validated);
            // 
            // VisitLabourCostSpinEdit
            // 
            this.VisitLabourCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "VisitLabourCost", true));
            this.VisitLabourCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.VisitLabourCostSpinEdit.Location = new System.Drawing.Point(931, 208);
            this.VisitLabourCostSpinEdit.MenuManager = this.barManager1;
            this.VisitLabourCostSpinEdit.Name = "VisitLabourCostSpinEdit";
            this.VisitLabourCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.VisitLabourCostSpinEdit.Properties.Mask.EditMask = "c";
            this.VisitLabourCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.VisitLabourCostSpinEdit.Size = new System.Drawing.Size(96, 20);
            this.VisitLabourCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.VisitLabourCostSpinEdit.TabIndex = 21;
            this.VisitLabourCostSpinEdit.EditValueChanged += new System.EventHandler(this.VisitLabourCostSpinEdit_EditValueChanged);
            this.VisitLabourCostSpinEdit.Validated += new System.EventHandler(this.VisitLabourCostSpinEdit_Validated);
            // 
            // DoNotInvoiceClientCheckEdit
            // 
            this.DoNotInvoiceClientCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "DoNotInvoiceClient", true));
            this.DoNotInvoiceClientCheckEdit.Location = new System.Drawing.Point(1206, 161);
            this.DoNotInvoiceClientCheckEdit.MenuManager = this.barManager1;
            this.DoNotInvoiceClientCheckEdit.Name = "DoNotInvoiceClientCheckEdit";
            this.DoNotInvoiceClientCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.DoNotInvoiceClientCheckEdit.Properties.ValueChecked = 1;
            this.DoNotInvoiceClientCheckEdit.Properties.ValueUnchecked = 0;
            this.DoNotInvoiceClientCheckEdit.Size = new System.Drawing.Size(76, 19);
            this.DoNotInvoiceClientCheckEdit.StyleController = this.dataLayoutControl1;
            this.DoNotInvoiceClientCheckEdit.TabIndex = 19;
            this.DoNotInvoiceClientCheckEdit.EditValueChanged += new System.EventHandler(this.DoNotInvoiceClientCheckEdit_EditValueChanged);
            this.DoNotInvoiceClientCheckEdit.Validated += new System.EventHandler(this.DoNotInvoiceClientCheckEdit_Validated);
            // 
            // DoNotPayContractorCheckEdit
            // 
            this.DoNotPayContractorCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "DoNotPayContractor", true));
            this.DoNotPayContractorCheckEdit.Location = new System.Drawing.Point(931, 161);
            this.DoNotPayContractorCheckEdit.MenuManager = this.barManager1;
            this.DoNotPayContractorCheckEdit.Name = "DoNotPayContractorCheckEdit";
            this.DoNotPayContractorCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.DoNotPayContractorCheckEdit.Properties.ValueChecked = 1;
            this.DoNotPayContractorCheckEdit.Properties.ValueUnchecked = 0;
            this.DoNotPayContractorCheckEdit.Size = new System.Drawing.Size(76, 19);
            this.DoNotPayContractorCheckEdit.StyleController = this.dataLayoutControl1;
            this.DoNotPayContractorCheckEdit.TabIndex = 18;
            this.DoNotPayContractorCheckEdit.EditValueChanged += new System.EventHandler(this.DoNotPayContractorCheckEdit_EditValueChanged);
            this.DoNotPayContractorCheckEdit.Validated += new System.EventHandler(this.DoNotPayContractorCheckEdit_Validated);
            // 
            // CalculateDaysButton
            // 
            this.CalculateDaysButton.ImageOptions.ImageIndex = 5;
            this.CalculateDaysButton.ImageOptions.ImageList = this.imageCollection1;
            this.CalculateDaysButton.Location = new System.Drawing.Point(371, 257);
            this.CalculateDaysButton.Name = "CalculateDaysButton";
            this.CalculateDaysButton.Size = new System.Drawing.Size(24, 22);
            this.CalculateDaysButton.StyleController = this.dataLayoutControl1;
            toolTipTitleItem5.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image4")));
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image5")));
            toolTipTitleItem5.Text = "Re-Calculate Minimum Days Between Visits - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = resources.GetString("toolTipItem5.Text");
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.CalculateDaysButton.SuperTip = superToolTip5;
            this.CalculateDaysButton.TabIndex = 61;
            this.CalculateDaysButton.Click += new System.EventHandler(this.CalculateDaysButton_Click);
            // 
            // SiteContractDaysSeparationPercentTextEdit
            // 
            this.SiteContractDaysSeparationPercentTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "SiteContractDaysSeparationPercent", true));
            this.SiteContractDaysSeparationPercentTextEdit.Location = new System.Drawing.Point(178, 155);
            this.SiteContractDaysSeparationPercentTextEdit.MenuManager = this.barManager1;
            this.SiteContractDaysSeparationPercentTextEdit.Name = "SiteContractDaysSeparationPercentTextEdit";
            this.SiteContractDaysSeparationPercentTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteContractDaysSeparationPercentTextEdit, true);
            this.SiteContractDaysSeparationPercentTextEdit.Size = new System.Drawing.Size(90, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteContractDaysSeparationPercentTextEdit, optionsSpelling2);
            this.SiteContractDaysSeparationPercentTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteContractDaysSeparationPercentTextEdit.TabIndex = 71;
            // 
            // DaysSeparationSpinEdit
            // 
            this.DaysSeparationSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "DaysSeparation", true));
            this.DaysSeparationSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.DaysSeparationSpinEdit.Location = new System.Drawing.Point(147, 257);
            this.DaysSeparationSpinEdit.MenuManager = this.barManager1;
            this.DaysSeparationSpinEdit.Name = "DaysSeparationSpinEdit";
            this.DaysSeparationSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DaysSeparationSpinEdit.Properties.IsFloatValue = false;
            this.DaysSeparationSpinEdit.Properties.Mask.EditMask = "N00";
            this.DaysSeparationSpinEdit.Properties.MaxValue = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.DaysSeparationSpinEdit.Size = new System.Drawing.Size(220, 20);
            this.DaysSeparationSpinEdit.StyleController = this.dataLayoutControl1;
            this.DaysSeparationSpinEdit.TabIndex = 4;
            // 
            // StatusIssueIDGridLookUpEdit
            // 
            this.StatusIssueIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "StatusIssueID", true));
            this.StatusIssueIDGridLookUpEdit.Location = new System.Drawing.Point(379, 431);
            this.StatusIssueIDGridLookUpEdit.MenuManager = this.barManager1;
            this.StatusIssueIDGridLookUpEdit.Name = "StatusIssueIDGridLookUpEdit";
            this.StatusIssueIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.StatusIssueIDGridLookUpEdit.Properties.DataSource = this.sp06430OMVisitStatusIssuesWithBlankBindingSource;
            this.StatusIssueIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.StatusIssueIDGridLookUpEdit.Properties.NullText = "";
            this.StatusIssueIDGridLookUpEdit.Properties.PopupView = this.gridView7;
            this.StatusIssueIDGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.StatusIssueIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.StatusIssueIDGridLookUpEdit.Size = new System.Drawing.Size(371, 20);
            this.StatusIssueIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.StatusIssueIDGridLookUpEdit.TabIndex = 9;
            // 
            // sp06430OMVisitStatusIssuesWithBlankBindingSource
            // 
            this.sp06430OMVisitStatusIssuesWithBlankBindingSource.DataMember = "sp06430_OM_Visit_Status_Issues_With_Blank";
            this.sp06430OMVisitStatusIssuesWithBlankBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21});
            this.gridView7.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.gridColumn19;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView7.FormatRules.Add(gridFormatRule1);
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView7.OptionsLayout.StoreAppearance = true;
            this.gridView7.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView7.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView7.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView7.OptionsView.ColumnAutoWidth = false;
            this.gridView7.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            this.gridView7.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView7.OptionsView.ShowIndicator = false;
            this.gridView7.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn21, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Visit Status Issue";
            this.gridColumn20.FieldName = "Description";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.AllowEdit = false;
            this.gridColumn20.OptionsColumn.AllowFocus = false;
            this.gridColumn20.OptionsColumn.ReadOnly = true;
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 0;
            this.gridColumn20.Width = 220;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Order";
            this.gridColumn21.FieldName = "RecordOrder";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.AllowEdit = false;
            this.gridColumn21.OptionsColumn.AllowFocus = false;
            this.gridColumn21.OptionsColumn.ReadOnly = true;
            // 
            // CompletionSheetNumberTextEdit
            // 
            this.CompletionSheetNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "CompletionSheetNumber", true));
            this.CompletionSheetNumberTextEdit.Location = new System.Drawing.Point(1194, 69);
            this.CompletionSheetNumberTextEdit.MenuManager = this.barManager1;
            this.CompletionSheetNumberTextEdit.Name = "CompletionSheetNumberTextEdit";
            this.CompletionSheetNumberTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CompletionSheetNumberTextEdit, true);
            this.CompletionSheetNumberTextEdit.Size = new System.Drawing.Size(148, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CompletionSheetNumberTextEdit, optionsSpelling3);
            this.CompletionSheetNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.CompletionSheetNumberTextEdit.TabIndex = 32;
            // 
            // CreatedFromVisitTemplateIDGridLookUpEdit
            // 
            this.CreatedFromVisitTemplateIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "CreatedFromVisitTemplateID", true));
            this.CreatedFromVisitTemplateIDGridLookUpEdit.Location = new System.Drawing.Point(522, 830);
            this.CreatedFromVisitTemplateIDGridLookUpEdit.MenuManager = this.barManager1;
            this.CreatedFromVisitTemplateIDGridLookUpEdit.Name = "CreatedFromVisitTemplateIDGridLookUpEdit";
            this.CreatedFromVisitTemplateIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CreatedFromVisitTemplateIDGridLookUpEdit.Properties.DataSource = this.sp06141OMVisitTemplateHeadersWithBlankBindingSource;
            this.CreatedFromVisitTemplateIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.CreatedFromVisitTemplateIDGridLookUpEdit.Properties.NullText = "";
            this.CreatedFromVisitTemplateIDGridLookUpEdit.Properties.PopupView = this.gridView1;
            this.CreatedFromVisitTemplateIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.CreatedFromVisitTemplateIDGridLookUpEdit.Size = new System.Drawing.Size(240, 20);
            this.CreatedFromVisitTemplateIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.CreatedFromVisitTemplateIDGridLookUpEdit.TabIndex = 11;
            // 
            // sp06141OMVisitTemplateHeadersWithBlankBindingSource
            // 
            this.sp06141OMVisitTemplateHeadersWithBlankBindingSource.DataMember = "sp06141_OM_Visit_Template_Headers_With_Blank";
            this.sp06141OMVisitTemplateHeadersWithBlankBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.gridColumn4;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn6, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Schedule Template";
            this.gridColumn5.FieldName = "Description";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 220;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Order";
            this.gridColumn6.FieldName = "RecordOrder";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            // 
            // VisitStatusIDTextEdit
            // 
            this.VisitStatusIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "VisitStatusID", true));
            this.VisitStatusIDTextEdit.Location = new System.Drawing.Point(132, 237);
            this.VisitStatusIDTextEdit.MenuManager = this.barManager1;
            this.VisitStatusIDTextEdit.Name = "VisitStatusIDTextEdit";
            this.VisitStatusIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.VisitStatusIDTextEdit, true);
            this.VisitStatusIDTextEdit.Size = new System.Drawing.Size(525, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.VisitStatusIDTextEdit, optionsSpelling4);
            this.VisitStatusIDTextEdit.StyleController = this.dataLayoutControl1;
            this.VisitStatusIDTextEdit.TabIndex = 69;
            // 
            // StatusDescriptionButtonEdit
            // 
            this.StatusDescriptionButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "StatusDescription", true));
            this.StatusDescriptionButtonEdit.EditValue = "";
            this.StatusDescriptionButtonEdit.Location = new System.Drawing.Point(522, 175);
            this.StatusDescriptionButtonEdit.MenuManager = this.barManager1;
            this.StatusDescriptionButtonEdit.Name = "StatusDescriptionButtonEdit";
            this.StatusDescriptionButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click me to Open Select Visit Status screen.", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.StatusDescriptionButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.StatusDescriptionButtonEdit.Size = new System.Drawing.Size(240, 20);
            this.StatusDescriptionButtonEdit.StyleController = this.dataLayoutControl1;
            this.StatusDescriptionButtonEdit.TabIndex = 7;
            this.StatusDescriptionButtonEdit.TabStop = false;
            this.StatusDescriptionButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.StatusDescriptionButtonEdit_ButtonClick);
            // 
            // SiteIDTextEdit
            // 
            this.SiteIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "SiteID", true));
            this.SiteIDTextEdit.Location = new System.Drawing.Point(147, 93);
            this.SiteIDTextEdit.MenuManager = this.barManager1;
            this.SiteIDTextEdit.Name = "SiteIDTextEdit";
            this.SiteIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteIDTextEdit, true);
            this.SiteIDTextEdit.Size = new System.Drawing.Size(248, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteIDTextEdit, optionsSpelling5);
            this.SiteIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteIDTextEdit.TabIndex = 65;
            // 
            // ClientIDTextEdit
            // 
            this.ClientIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "ClientID", true));
            this.ClientIDTextEdit.Location = new System.Drawing.Point(119, 223);
            this.ClientIDTextEdit.MenuManager = this.barManager1;
            this.ClientIDTextEdit.Name = "ClientIDTextEdit";
            this.ClientIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientIDTextEdit, true);
            this.ClientIDTextEdit.Size = new System.Drawing.Size(538, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientIDTextEdit, optionsSpelling6);
            this.ClientIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientIDTextEdit.TabIndex = 64;
            // 
            // ClientPOIDTextEdit
            // 
            this.ClientPOIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "ClientPOID", true));
            this.ClientPOIDTextEdit.Location = new System.Drawing.Point(124, 237);
            this.ClientPOIDTextEdit.MenuManager = this.barManager1;
            this.ClientPOIDTextEdit.Name = "ClientPOIDTextEdit";
            this.ClientPOIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientPOIDTextEdit, true);
            this.ClientPOIDTextEdit.Size = new System.Drawing.Size(208, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientPOIDTextEdit, optionsSpelling7);
            this.ClientPOIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientPOIDTextEdit.TabIndex = 59;
            // 
            // ManagerNotesMemoEdit
            // 
            this.ManagerNotesMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "ManagerNotes", true));
            this.ManagerNotesMemoEdit.Location = new System.Drawing.Point(159, 734);
            this.ManagerNotesMemoEdit.MenuManager = this.barManager1;
            this.ManagerNotesMemoEdit.Name = "ManagerNotesMemoEdit";
            this.ManagerNotesMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ManagerNotesMemoEdit, true);
            this.ManagerNotesMemoEdit.Size = new System.Drawing.Size(591, 44);
            this.scSpellChecker.SetSpellCheckerOptions(this.ManagerNotesMemoEdit, optionsSpelling8);
            this.ManagerNotesMemoEdit.StyleController = this.dataLayoutControl1;
            this.ManagerNotesMemoEdit.TabIndex = 31;
            // 
            // ManagerNameTextEdit
            // 
            this.ManagerNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "ManagerName", true));
            this.ManagerNameTextEdit.Location = new System.Drawing.Point(367, 710);
            this.ManagerNameTextEdit.MenuManager = this.barManager1;
            this.ManagerNameTextEdit.Name = "ManagerNameTextEdit";
            this.ManagerNameTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ManagerNameTextEdit, true);
            this.ManagerNameTextEdit.Size = new System.Drawing.Size(383, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ManagerNameTextEdit, optionsSpelling9);
            this.ManagerNameTextEdit.StyleController = this.dataLayoutControl1;
            this.ManagerNameTextEdit.TabIndex = 30;
            // 
            // NoOnetoSignCheckEdit
            // 
            this.NoOnetoSignCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "NoOnetoSign", true));
            this.NoOnetoSignCheckEdit.Location = new System.Drawing.Point(159, 710);
            this.NoOnetoSignCheckEdit.MenuManager = this.barManager1;
            this.NoOnetoSignCheckEdit.Name = "NoOnetoSignCheckEdit";
            this.NoOnetoSignCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.NoOnetoSignCheckEdit.Properties.ValueChecked = 1;
            this.NoOnetoSignCheckEdit.Properties.ValueUnchecked = 0;
            this.NoOnetoSignCheckEdit.Size = new System.Drawing.Size(81, 19);
            this.NoOnetoSignCheckEdit.StyleController = this.dataLayoutControl1;
            this.NoOnetoSignCheckEdit.TabIndex = 29;
            this.NoOnetoSignCheckEdit.EditValueChanged += new System.EventHandler(this.NoOnetoSignCheckEdit_EditValueChanged);
            this.NoOnetoSignCheckEdit.Validated += new System.EventHandler(this.NoOnetoSignCheckEdit_Validated);
            // 
            // ClientInvoiceIDButtonEdit
            // 
            this.ClientInvoiceIDButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "ClientInvoiceID", true));
            this.ClientInvoiceIDButtonEdit.Location = new System.Drawing.Point(1206, 306);
            this.ClientInvoiceIDButtonEdit.MenuManager = this.barManager1;
            this.ClientInvoiceIDButtonEdit.Name = "ClientInvoiceIDButtonEdit";
            this.ClientInvoiceIDButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Click me to View the Client Invoice", "view", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ClientInvoiceIDButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ClientInvoiceIDButtonEdit.Size = new System.Drawing.Size(124, 20);
            this.ClientInvoiceIDButtonEdit.StyleController = this.dataLayoutControl1;
            this.ClientInvoiceIDButtonEdit.TabIndex = 23;
            this.ClientInvoiceIDButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ClientInvoiceIDButtonEdit_ButtonClick);
            // 
            // SelfBillingInvoiceButtonEdit
            // 
            this.SelfBillingInvoiceButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "SelfBillingInvoice", true));
            this.SelfBillingInvoiceButtonEdit.Location = new System.Drawing.Point(931, 306);
            this.SelfBillingInvoiceButtonEdit.MenuManager = this.barManager1;
            this.SelfBillingInvoiceButtonEdit.Name = "SelfBillingInvoiceButtonEdit";
            this.SelfBillingInvoiceButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View", -1, true, true, false, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Click me to View the Self-Billing Invoice", "view", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.SelfBillingInvoiceButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.SelfBillingInvoiceButtonEdit.Size = new System.Drawing.Size(124, 20);
            this.SelfBillingInvoiceButtonEdit.StyleController = this.dataLayoutControl1;
            this.SelfBillingInvoiceButtonEdit.TabIndex = 22;
            this.SelfBillingInvoiceButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.SelfBillingInvoiceButtonEdit_ButtonClick);
            // 
            // CalculateSellButton
            // 
            this.CalculateSellButton.ImageOptions.ImageIndex = 5;
            this.CalculateSellButton.ImageOptions.ImageList = this.imageCollection1;
            this.CalculateSellButton.Location = new System.Drawing.Point(1306, 280);
            this.CalculateSellButton.Name = "CalculateSellButton";
            this.CalculateSellButton.Size = new System.Drawing.Size(24, 22);
            this.CalculateSellButton.StyleController = this.dataLayoutControl1;
            toolTipTitleItem6.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image6")));
            toolTipTitleItem6.Appearance.Options.UseImage = true;
            toolTipTitleItem6.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image7")));
            toolTipTitleItem6.Text = "Re-Calculate Sell Price - Information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Click me to re-calculate the Visit Sell Price based on the method selected in the" +
    " Sell Calculation box.\r\n\r\nNote: I am only enabled when the Sell Calculation meth" +
    "od is not Manual.";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this.CalculateSellButton.SuperTip = superToolTip6;
            this.CalculateSellButton.TabIndex = 61;
            this.CalculateSellButton.Click += new System.EventHandler(this.CalculateSellButton_Click);
            // 
            // CalculateCostButton
            // 
            this.CalculateCostButton.ImageOptions.ImageIndex = 5;
            this.CalculateCostButton.ImageOptions.ImageList = this.imageCollection1;
            this.CalculateCostButton.Location = new System.Drawing.Point(1031, 280);
            this.CalculateCostButton.Name = "CalculateCostButton";
            this.CalculateCostButton.Size = new System.Drawing.Size(24, 22);
            this.CalculateCostButton.StyleController = this.dataLayoutControl1;
            toolTipTitleItem7.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image8")));
            toolTipTitleItem7.Appearance.Options.UseImage = true;
            toolTipTitleItem7.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image9")));
            toolTipTitleItem7.Text = "Re-Calculate Cost - Information";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Click me to re-calculate the Visit Cost based on the method selected in the Cost " +
    "Calculation box.\r\n\r\nNote: I am only enabled when the Cost Calculation method is " +
    "not Manual.";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            this.CalculateCostButton.SuperTip = superToolTip7;
            this.CalculateCostButton.TabIndex = 60;
            this.CalculateCostButton.Click += new System.EventHandler(this.CalculateCostButton_Click);
            // 
            // SellCalculationLevelGridLookUpEdit
            // 
            this.SellCalculationLevelGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "SellCalculationLevel", true));
            this.SellCalculationLevelGridLookUpEdit.Location = new System.Drawing.Point(1206, 184);
            this.SellCalculationLevelGridLookUpEdit.MenuManager = this.barManager1;
            this.SellCalculationLevelGridLookUpEdit.Name = "SellCalculationLevelGridLookUpEdit";
            this.SellCalculationLevelGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SellCalculationLevelGridLookUpEdit.Properties.DataSource = this.sp06134OMJobCalculationLevelDescriptorsBindingSource;
            this.SellCalculationLevelGridLookUpEdit.Properties.DisplayMember = "Description";
            this.SellCalculationLevelGridLookUpEdit.Properties.NullText = "";
            this.SellCalculationLevelGridLookUpEdit.Properties.PopupView = this.gridView6;
            this.SellCalculationLevelGridLookUpEdit.Properties.ValueMember = "ID";
            this.SellCalculationLevelGridLookUpEdit.Size = new System.Drawing.Size(124, 20);
            this.SellCalculationLevelGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.SellCalculationLevelGridLookUpEdit.TabIndex = 17;
            this.SellCalculationLevelGridLookUpEdit.EditValueChanged += new System.EventHandler(this.SellCalculationLevelGridLookUpEdit_EditValueChanged);
            this.SellCalculationLevelGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.SellCalculationLevelGridLookUpEdit_Validating);
            this.SellCalculationLevelGridLookUpEdit.Validated += new System.EventHandler(this.SellCalculationLevelGridLookUpEdit_Validated);
            // 
            // sp06134OMJobCalculationLevelDescriptorsBindingSource
            // 
            this.sp06134OMJobCalculationLevelDescriptorsBindingSource.DataMember = "sp06134_OM_Job_Calculation_Level_Descriptors";
            this.sp06134OMJobCalculationLevelDescriptorsBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18});
            this.gridView6.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule2.ApplyToRow = true;
            gridFormatRule2.Column = this.gridColumn16;
            gridFormatRule2.Name = "Format0";
            formatConditionRuleValue2.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue2.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue2.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue2.Value1 = -1;
            gridFormatRule2.Rule = formatConditionRuleValue2;
            this.gridView6.FormatRules.Add(gridFormatRule2);
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView6.OptionsLayout.StoreAppearance = true;
            this.gridView6.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView6.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView6.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView6.OptionsView.ShowIndicator = false;
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn18, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Cost Calculation Level";
            this.gridColumn17.FieldName = "Description";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.AllowFocus = false;
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 0;
            this.gridColumn17.Width = 220;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Order";
            this.gridColumn18.FieldName = "RecordOrder";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.AllowFocus = false;
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            // 
            // ClientPONumberButtonEdit
            // 
            this.ClientPONumberButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "ClientPONumber", true));
            this.ClientPONumberButtonEdit.EditValue = "";
            this.ClientPONumberButtonEdit.Location = new System.Drawing.Point(919, 69);
            this.ClientPONumberButtonEdit.MenuManager = this.barManager1;
            this.ClientPONumberButtonEdit.Name = "ClientPONumberButtonEdit";
            this.ClientPONumberButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Click me to Open Select Client Purchase Order screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Close, "Clear", -1, true, true, false, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "Click me to Clear the selected Client PO", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ClientPONumberButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ClientPONumberButtonEdit.Size = new System.Drawing.Size(148, 20);
            this.ClientPONumberButtonEdit.StyleController = this.dataLayoutControl1;
            this.ClientPONumberButtonEdit.TabIndex = 15;
            this.ClientPONumberButtonEdit.TabStop = false;
            this.ClientPONumberButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ClientPONumberButtonEdit_ButtonClick);
            // 
            // VisitTypeIDGridLookUpEdit
            // 
            this.VisitTypeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "VisitTypeID", true));
            this.VisitTypeIDGridLookUpEdit.Location = new System.Drawing.Point(147, 127);
            this.VisitTypeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.VisitTypeIDGridLookUpEdit.Name = "VisitTypeIDGridLookUpEdit";
            this.VisitTypeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.VisitTypeIDGridLookUpEdit.Properties.DataSource = this.sp06308OMVisitTypesBindingSource;
            this.VisitTypeIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.VisitTypeIDGridLookUpEdit.Properties.NullText = "";
            this.VisitTypeIDGridLookUpEdit.Properties.PopupView = this.gridView5;
            this.VisitTypeIDGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.VisitTypeIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.VisitTypeIDGridLookUpEdit.Size = new System.Drawing.Size(248, 20);
            this.VisitTypeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.VisitTypeIDGridLookUpEdit.TabIndex = 1;
            this.VisitTypeIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.VisitTypeIDGridLookUpEdit_Validating);
            // 
            // sp06308OMVisitTypesBindingSource
            // 
            this.sp06308OMVisitTypesBindingSource.DataMember = "sp06308_OM_Visit_Types";
            this.sp06308OMVisitTypesBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.colActive});
            this.gridView5.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.gridColumn13;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridView5.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn15, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Visit Type";
            this.gridColumn14.FieldName = "Description";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 0;
            this.gridColumn14.Width = 174;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Order";
            this.gridColumn15.FieldName = "RecordOrder";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            // 
            // colActive
            // 
            this.colActive.Caption = "Active";
            this.colActive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colActive.FieldName = "Active";
            this.colActive.Name = "colActive";
            this.colActive.OptionsColumn.AllowEdit = false;
            this.colActive.OptionsColumn.AllowFocus = false;
            this.colActive.OptionsColumn.ReadOnly = true;
            this.colActive.Visible = true;
            this.colActive.VisibleIndex = 1;
            this.colActive.Width = 49;
            // 
            // FriendlyVisitNumberTextEdit
            // 
            this.FriendlyVisitNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "FriendlyVisitNumber", true));
            this.FriendlyVisitNumberTextEdit.Location = new System.Drawing.Point(522, 151);
            this.FriendlyVisitNumberTextEdit.MenuManager = this.barManager1;
            this.FriendlyVisitNumberTextEdit.Name = "FriendlyVisitNumberTextEdit";
            this.FriendlyVisitNumberTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.FriendlyVisitNumberTextEdit, true);
            this.FriendlyVisitNumberTextEdit.Size = new System.Drawing.Size(240, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.FriendlyVisitNumberTextEdit, optionsSpelling10);
            this.FriendlyVisitNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.FriendlyVisitNumberTextEdit.TabIndex = 6;
            // 
            // AccidentCommentMemoEdit
            // 
            this.AccidentCommentMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "AccidentComment", true));
            this.AccidentCommentMemoEdit.Location = new System.Drawing.Point(159, 606);
            this.AccidentCommentMemoEdit.MenuManager = this.barManager1;
            this.AccidentCommentMemoEdit.Name = "AccidentCommentMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.AccidentCommentMemoEdit, true);
            this.AccidentCommentMemoEdit.Size = new System.Drawing.Size(591, 44);
            this.scSpellChecker.SetSpellCheckerOptions(this.AccidentCommentMemoEdit, optionsSpelling11);
            this.AccidentCommentMemoEdit.StyleController = this.dataLayoutControl1;
            this.AccidentCommentMemoEdit.TabIndex = 4;
            // 
            // AccidentOnSiteCheckEdit
            // 
            this.AccidentOnSiteCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "AccidentOnSite", true));
            this.AccidentOnSiteCheckEdit.Location = new System.Drawing.Point(159, 583);
            this.AccidentOnSiteCheckEdit.MenuManager = this.barManager1;
            this.AccidentOnSiteCheckEdit.Name = "AccidentOnSiteCheckEdit";
            this.AccidentOnSiteCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.AccidentOnSiteCheckEdit.Properties.ValueChecked = 1;
            this.AccidentOnSiteCheckEdit.Properties.ValueUnchecked = 0;
            this.AccidentOnSiteCheckEdit.Size = new System.Drawing.Size(81, 19);
            this.AccidentOnSiteCheckEdit.StyleController = this.dataLayoutControl1;
            this.AccidentOnSiteCheckEdit.TabIndex = 3;
            // 
            // ExtraWorkCommentsMemoEdit
            // 
            this.ExtraWorkCommentsMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "ExtraWorkComments", true));
            this.ExtraWorkCommentsMemoEdit.Location = new System.Drawing.Point(919, 446);
            this.ExtraWorkCommentsMemoEdit.MenuManager = this.barManager1;
            this.ExtraWorkCommentsMemoEdit.Name = "ExtraWorkCommentsMemoEdit";
            this.ExtraWorkCommentsMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ExtraWorkCommentsMemoEdit, true);
            this.ExtraWorkCommentsMemoEdit.Size = new System.Drawing.Size(423, 25);
            this.scSpellChecker.SetSpellCheckerOptions(this.ExtraWorkCommentsMemoEdit, optionsSpelling12);
            this.ExtraWorkCommentsMemoEdit.StyleController = this.dataLayoutControl1;
            this.ExtraWorkCommentsMemoEdit.TabIndex = 7;
            // 
            // ExtraWorkTypeIDGridLookUpEdit
            // 
            this.ExtraWorkTypeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "ExtraWorkTypeID", true));
            this.ExtraWorkTypeIDGridLookUpEdit.Location = new System.Drawing.Point(1128, 422);
            this.ExtraWorkTypeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.ExtraWorkTypeIDGridLookUpEdit.Name = "ExtraWorkTypeIDGridLookUpEdit";
            this.ExtraWorkTypeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ExtraWorkTypeIDGridLookUpEdit.Properties.DataSource = this.sp06267OMExtraWorkTypesWithBlankBindingSource;
            this.ExtraWorkTypeIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.ExtraWorkTypeIDGridLookUpEdit.Properties.NullText = "";
            this.ExtraWorkTypeIDGridLookUpEdit.Properties.PopupView = this.gridView4;
            this.ExtraWorkTypeIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.ExtraWorkTypeIDGridLookUpEdit.Size = new System.Drawing.Size(214, 20);
            this.ExtraWorkTypeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.ExtraWorkTypeIDGridLookUpEdit.TabIndex = 6;
            // 
            // sp06267OMExtraWorkTypesWithBlankBindingSource
            // 
            this.sp06267OMExtraWorkTypesWithBlankBindingSource.DataMember = "sp06267_OM_Extra_Work_Types_With_Blank";
            this.sp06267OMExtraWorkTypesWithBlankBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition3.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition3.Appearance.Options.UseForeColor = true;
            styleFormatCondition3.ApplyToRow = true;
            styleFormatCondition3.Column = this.gridColumn10;
            styleFormatCondition3.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition3.Value1 = 0;
            this.gridView4.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition3});
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn12, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Extra Work Type";
            this.gridColumn11.FieldName = "Description";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 0;
            this.gridColumn11.Width = 220;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Order";
            this.gridColumn12.FieldName = "RecordOrder";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            // 
            // ExtraWorkRequiredCheckEdit
            // 
            this.ExtraWorkRequiredCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "ExtraWorkRequired", true));
            this.ExtraWorkRequiredCheckEdit.Location = new System.Drawing.Point(919, 422);
            this.ExtraWorkRequiredCheckEdit.MenuManager = this.barManager1;
            this.ExtraWorkRequiredCheckEdit.Name = "ExtraWorkRequiredCheckEdit";
            this.ExtraWorkRequiredCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.ExtraWorkRequiredCheckEdit.Properties.ValueChecked = 1;
            this.ExtraWorkRequiredCheckEdit.Properties.ValueUnchecked = 0;
            this.ExtraWorkRequiredCheckEdit.Size = new System.Drawing.Size(82, 19);
            this.ExtraWorkRequiredCheckEdit.StyleController = this.dataLayoutControl1;
            this.ExtraWorkRequiredCheckEdit.TabIndex = 5;
            // 
            // ReworkCheckEdit
            // 
            this.ReworkCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "Rework", true));
            this.ReworkCheckEdit.Location = new System.Drawing.Point(159, 431);
            this.ReworkCheckEdit.MenuManager = this.barManager1;
            this.ReworkCheckEdit.Name = "ReworkCheckEdit";
            this.ReworkCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.ReworkCheckEdit.Properties.ValueChecked = 1;
            this.ReworkCheckEdit.Properties.ValueUnchecked = 0;
            this.ReworkCheckEdit.Size = new System.Drawing.Size(93, 19);
            this.ReworkCheckEdit.StyleController = this.dataLayoutControl1;
            this.ReworkCheckEdit.TabIndex = 33;
            // 
            // TimelinessTextEdit
            // 
            this.TimelinessTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "Timeliness", true));
            this.TimelinessTextEdit.Location = new System.Drawing.Point(147, 175);
            this.TimelinessTextEdit.MenuManager = this.barManager1;
            this.TimelinessTextEdit.Name = "TimelinessTextEdit";
            this.TimelinessTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.TimelinessTextEdit, true);
            this.TimelinessTextEdit.Size = new System.Drawing.Size(248, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.TimelinessTextEdit, optionsSpelling13);
            this.TimelinessTextEdit.StyleController = this.dataLayoutControl1;
            this.TimelinessTextEdit.TabIndex = 8;
            // 
            // ImagesFolderOMTextEdit
            // 
            this.ImagesFolderOMTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "ImagesFolderOM", true));
            this.ImagesFolderOMTextEdit.Location = new System.Drawing.Point(124, 165);
            this.ImagesFolderOMTextEdit.MenuManager = this.barManager1;
            this.ImagesFolderOMTextEdit.Name = "ImagesFolderOMTextEdit";
            this.ImagesFolderOMTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ImagesFolderOMTextEdit, true);
            this.ImagesFolderOMTextEdit.Size = new System.Drawing.Size(533, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ImagesFolderOMTextEdit, optionsSpelling14);
            this.ImagesFolderOMTextEdit.StyleController = this.dataLayoutControl1;
            this.ImagesFolderOMTextEdit.TabIndex = 57;
            // 
            // ClientSignaturePathButtonEdit
            // 
            this.ClientSignaturePathButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "ClientSignaturePath", true));
            this.ClientSignaturePathButtonEdit.Location = new System.Drawing.Point(159, 782);
            this.ClientSignaturePathButtonEdit.MenuManager = this.barManager1;
            this.ClientSignaturePathButtonEdit.Name = "ClientSignaturePathButtonEdit";
            editorButtonImageOptions6.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions6.Image")));
            editorButtonImageOptions6.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.ClientSignaturePathButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose File", -1, true, true, true, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "Select File", "select file", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View File", -1, true, true, false, editorButtonImageOptions7, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject25, serializableAppearanceObject26, serializableAppearanceObject27, serializableAppearanceObject28, "", "view file", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ClientSignaturePathButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ClientSignaturePathButtonEdit.Size = new System.Drawing.Size(591, 22);
            this.ClientSignaturePathButtonEdit.StyleController = this.dataLayoutControl1;
            this.ClientSignaturePathButtonEdit.TabIndex = 28;
            this.ClientSignaturePathButtonEdit.TabStop = false;
            this.ClientSignaturePathButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ClientSignaturePathButtonEdit_ButtonClick);
            // 
            // VisitCategoryIDGridLookUpEdit
            // 
            this.VisitCategoryIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "VisitCategoryID", true));
            this.VisitCategoryIDGridLookUpEdit.Location = new System.Drawing.Point(522, 127);
            this.VisitCategoryIDGridLookUpEdit.MenuManager = this.barManager1;
            this.VisitCategoryIDGridLookUpEdit.Name = "VisitCategoryIDGridLookUpEdit";
            this.VisitCategoryIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.VisitCategoryIDGridLookUpEdit.Properties.DataSource = this.sp06250OMVisitCategoriesWithBlankBindingSource;
            this.VisitCategoryIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.VisitCategoryIDGridLookUpEdit.Properties.NullText = "";
            this.VisitCategoryIDGridLookUpEdit.Properties.PopupView = this.gridView3;
            this.VisitCategoryIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.VisitCategoryIDGridLookUpEdit.Size = new System.Drawing.Size(240, 20);
            this.VisitCategoryIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.VisitCategoryIDGridLookUpEdit.TabIndex = 10;
            this.VisitCategoryIDGridLookUpEdit.EditValueChanged += new System.EventHandler(this.VisitCategoryIDGridLookUpEdit_EditValueChanged);
            this.VisitCategoryIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.VisitCategoryIDGridLookUpEdit_Validating);
            this.VisitCategoryIDGridLookUpEdit.Validated += new System.EventHandler(this.VisitCategoryIDGridLookUpEdit_Validated);
            // 
            // sp06250OMVisitCategoriesWithBlankBindingSource
            // 
            this.sp06250OMVisitCategoriesWithBlankBindingSource.DataMember = "sp06250_OM_Visit_Categories_With_Blank";
            this.sp06250OMVisitCategoriesWithBlankBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition4.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition4.Appearance.Options.UseForeColor = true;
            styleFormatCondition4.ApplyToRow = true;
            styleFormatCondition4.Column = this.gridColumn1;
            styleFormatCondition4.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition4.Value1 = 0;
            this.gridView3.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition4});
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Visit Category";
            this.gridColumn2.FieldName = "Description";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 220;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Order";
            this.gridColumn3.FieldName = "RecordOrder";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            // 
            // IssueRemarksMemoEdit
            // 
            this.IssueRemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "IssueRemarks", true));
            this.IssueRemarksMemoEdit.Location = new System.Drawing.Point(159, 479);
            this.IssueRemarksMemoEdit.MenuManager = this.barManager1;
            this.IssueRemarksMemoEdit.Name = "IssueRemarksMemoEdit";
            this.IssueRemarksMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.IssueRemarksMemoEdit, true);
            this.IssueRemarksMemoEdit.Size = new System.Drawing.Size(591, 44);
            this.scSpellChecker.SetSpellCheckerOptions(this.IssueRemarksMemoEdit, optionsSpelling15);
            this.IssueRemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.IssueRemarksMemoEdit.TabIndex = 2;
            // 
            // IssueTypeIDGridLookUpEdit
            // 
            this.IssueTypeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "IssueTypeID", true));
            this.IssueTypeIDGridLookUpEdit.Location = new System.Drawing.Point(379, 455);
            this.IssueTypeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.IssueTypeIDGridLookUpEdit.Name = "IssueTypeIDGridLookUpEdit";
            this.IssueTypeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.IssueTypeIDGridLookUpEdit.Properties.DataSource = this.sp06251OMIssueTypesWithBlankBindingSource;
            this.IssueTypeIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.IssueTypeIDGridLookUpEdit.Properties.NullText = "";
            this.IssueTypeIDGridLookUpEdit.Properties.PopupView = this.gridView2;
            this.IssueTypeIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.IssueTypeIDGridLookUpEdit.Size = new System.Drawing.Size(371, 20);
            this.IssueTypeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.IssueTypeIDGridLookUpEdit.TabIndex = 1;
            // 
            // sp06251OMIssueTypesWithBlankBindingSource
            // 
            this.sp06251OMIssueTypesWithBlankBindingSource.DataMember = "sp06251_OM_Issue_Types_With_Blank";
            this.sp06251OMIssueTypesWithBlankBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition5.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition5.Appearance.Options.UseForeColor = true;
            styleFormatCondition5.ApplyToRow = true;
            styleFormatCondition5.Column = this.gridColumn7;
            styleFormatCondition5.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition5.Value1 = 0;
            this.gridView2.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition5});
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn9, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Issue Types";
            this.gridColumn8.FieldName = "Description";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 0;
            this.gridColumn8.Width = 220;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Order";
            this.gridColumn9.FieldName = "RecordOrder";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            // 
            // IssueFoundCheckEdit
            // 
            this.IssueFoundCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "IssueFound", true));
            this.IssueFoundCheckEdit.Location = new System.Drawing.Point(159, 455);
            this.IssueFoundCheckEdit.MenuManager = this.barManager1;
            this.IssueFoundCheckEdit.Name = "IssueFoundCheckEdit";
            this.IssueFoundCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.IssueFoundCheckEdit.Properties.ValueChecked = 1;
            this.IssueFoundCheckEdit.Properties.ValueUnchecked = 0;
            this.IssueFoundCheckEdit.Size = new System.Drawing.Size(93, 19);
            this.IssueFoundCheckEdit.StyleController = this.dataLayoutControl1;
            this.IssueFoundCheckEdit.TabIndex = 0;
            // 
            // WorkNumberTextEdit
            // 
            this.WorkNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "WorkNumber", true));
            this.WorkNumberTextEdit.Location = new System.Drawing.Point(919, 93);
            this.WorkNumberTextEdit.MenuManager = this.barManager1;
            this.WorkNumberTextEdit.Name = "WorkNumberTextEdit";
            this.WorkNumberTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.WorkNumberTextEdit, true);
            this.WorkNumberTextEdit.Size = new System.Drawing.Size(148, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.WorkNumberTextEdit, optionsSpelling16);
            this.WorkNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.WorkNumberTextEdit.TabIndex = 12;
            // 
            // FinishLongitudeTextEdit
            // 
            this.FinishLongitudeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "FinishLongitude", true));
            this.FinishLongitudeTextEdit.Location = new System.Drawing.Point(522, 351);
            this.FinishLongitudeTextEdit.MenuManager = this.barManager1;
            this.FinishLongitudeTextEdit.Name = "FinishLongitudeTextEdit";
            this.FinishLongitudeTextEdit.Properties.Mask.EditMask = "n8";
            this.FinishLongitudeTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.FinishLongitudeTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.FinishLongitudeTextEdit, true);
            this.FinishLongitudeTextEdit.Size = new System.Drawing.Size(228, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.FinishLongitudeTextEdit, optionsSpelling17);
            this.FinishLongitudeTextEdit.StyleController = this.dataLayoutControl1;
            this.FinishLongitudeTextEdit.TabIndex = 27;
            // 
            // FinishLatitudeTextEdit
            // 
            this.FinishLatitudeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "FinishLatitude", true));
            this.FinishLatitudeTextEdit.Location = new System.Drawing.Point(159, 351);
            this.FinishLatitudeTextEdit.MenuManager = this.barManager1;
            this.FinishLatitudeTextEdit.Name = "FinishLatitudeTextEdit";
            this.FinishLatitudeTextEdit.Properties.Mask.EditMask = "n8";
            this.FinishLatitudeTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.FinishLatitudeTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.FinishLatitudeTextEdit, true);
            this.FinishLatitudeTextEdit.Size = new System.Drawing.Size(236, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.FinishLatitudeTextEdit, optionsSpelling18);
            this.FinishLatitudeTextEdit.StyleController = this.dataLayoutControl1;
            this.FinishLatitudeTextEdit.TabIndex = 26;
            // 
            // StartLongitudeTextEdit
            // 
            this.StartLongitudeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "StartLongitude", true));
            this.StartLongitudeTextEdit.Location = new System.Drawing.Point(522, 327);
            this.StartLongitudeTextEdit.MenuManager = this.barManager1;
            this.StartLongitudeTextEdit.Name = "StartLongitudeTextEdit";
            this.StartLongitudeTextEdit.Properties.Mask.EditMask = "n8";
            this.StartLongitudeTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.StartLongitudeTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.StartLongitudeTextEdit, true);
            this.StartLongitudeTextEdit.Size = new System.Drawing.Size(228, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.StartLongitudeTextEdit, optionsSpelling19);
            this.StartLongitudeTextEdit.StyleController = this.dataLayoutControl1;
            this.StartLongitudeTextEdit.TabIndex = 25;
            // 
            // StartLatitudeTextEdit
            // 
            this.StartLatitudeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "StartLatitude", true));
            this.StartLatitudeTextEdit.Location = new System.Drawing.Point(159, 327);
            this.StartLatitudeTextEdit.MenuManager = this.barManager1;
            this.StartLatitudeTextEdit.Name = "StartLatitudeTextEdit";
            this.StartLatitudeTextEdit.Properties.Mask.EditMask = "n8";
            this.StartLatitudeTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.StartLatitudeTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.StartLatitudeTextEdit, true);
            this.StartLatitudeTextEdit.Size = new System.Drawing.Size(236, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.StartLatitudeTextEdit, optionsSpelling20);
            this.StartLatitudeTextEdit.StyleController = this.dataLayoutControl1;
            this.StartLatitudeTextEdit.TabIndex = 24;
            // 
            // VisitSellSpinEdit
            // 
            this.VisitSellSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "VisitSell", true));
            this.VisitSellSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.VisitSellSpinEdit.Location = new System.Drawing.Point(1206, 280);
            this.VisitSellSpinEdit.MenuManager = this.barManager1;
            this.VisitSellSpinEdit.Name = "VisitSellSpinEdit";
            this.VisitSellSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.VisitSellSpinEdit.Properties.Mask.EditMask = "c";
            this.VisitSellSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.VisitSellSpinEdit.Size = new System.Drawing.Size(96, 20);
            this.VisitSellSpinEdit.StyleController = this.dataLayoutControl1;
            this.VisitSellSpinEdit.TabIndex = 21;
            // 
            // CostCalculationLevelGridLookUpEdit
            // 
            this.CostCalculationLevelGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "CostCalculationLevel", true));
            this.CostCalculationLevelGridLookUpEdit.Location = new System.Drawing.Point(931, 184);
            this.CostCalculationLevelGridLookUpEdit.MenuManager = this.barManager1;
            this.CostCalculationLevelGridLookUpEdit.Name = "CostCalculationLevelGridLookUpEdit";
            this.CostCalculationLevelGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CostCalculationLevelGridLookUpEdit.Properties.DataSource = this.sp06134OMJobCalculationLevelDescriptorsBindingSource;
            this.CostCalculationLevelGridLookUpEdit.Properties.DisplayMember = "Description";
            this.CostCalculationLevelGridLookUpEdit.Properties.NullText = "";
            this.CostCalculationLevelGridLookUpEdit.Properties.PopupView = this.gridLookUpEdit1View;
            this.CostCalculationLevelGridLookUpEdit.Properties.ValueMember = "ID";
            this.CostCalculationLevelGridLookUpEdit.Size = new System.Drawing.Size(124, 20);
            this.CostCalculationLevelGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.CostCalculationLevelGridLookUpEdit.TabIndex = 16;
            this.CostCalculationLevelGridLookUpEdit.EditValueChanged += new System.EventHandler(this.CostCalculationLevelGridLookUpEdit_EditValueChanged);
            this.CostCalculationLevelGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.CostCalculationLevelGridLookUpEdit_Validating);
            this.CostCalculationLevelGridLookUpEdit.Validated += new System.EventHandler(this.CostCalculationLevelGridLookUpEdit_Validated);
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID1,
            this.colDescription,
            this.colOrder});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule3.ApplyToRow = true;
            gridFormatRule3.Column = this.colID1;
            gridFormatRule3.Name = "Format0";
            formatConditionRuleValue3.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue3.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue3.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue3.Value1 = -1;
            gridFormatRule3.Rule = formatConditionRuleValue3;
            this.gridLookUpEdit1View.FormatRules.Add(gridFormatRule3);
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Cost Calculation Level";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 220;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "RecordOrder";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            // 
            // ExpectedEndDateDateEdit
            // 
            this.ExpectedEndDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "ExpectedEndDate", true));
            this.ExpectedEndDateDateEdit.EditValue = null;
            this.ExpectedEndDateDateEdit.Location = new System.Drawing.Point(522, 209);
            this.ExpectedEndDateDateEdit.MenuManager = this.barManager1;
            this.ExpectedEndDateDateEdit.Name = "ExpectedEndDateDateEdit";
            this.ExpectedEndDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.ExpectedEndDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ExpectedEndDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ExpectedEndDateDateEdit.Properties.Mask.EditMask = "G";
            this.ExpectedEndDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ExpectedEndDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.ExpectedEndDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.ExpectedEndDateDateEdit.Size = new System.Drawing.Size(240, 20);
            this.ExpectedEndDateDateEdit.StyleController = this.dataLayoutControl1;
            this.ExpectedEndDateDateEdit.TabIndex = 3;
            this.ExpectedEndDateDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ExpectedEndDateDateEdit_Validating);
            // 
            // ExpectedStartDateDateEdit
            // 
            this.ExpectedStartDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "ExpectedStartDate", true));
            this.ExpectedStartDateDateEdit.EditValue = null;
            this.ExpectedStartDateDateEdit.Location = new System.Drawing.Point(147, 209);
            this.ExpectedStartDateDateEdit.MenuManager = this.barManager1;
            this.ExpectedStartDateDateEdit.Name = "ExpectedStartDateDateEdit";
            this.ExpectedStartDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.ExpectedStartDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ExpectedStartDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ExpectedStartDateDateEdit.Properties.Mask.EditMask = "G";
            this.ExpectedStartDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ExpectedStartDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.ExpectedStartDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.ExpectedStartDateDateEdit.Size = new System.Drawing.Size(248, 20);
            this.ExpectedStartDateDateEdit.StyleController = this.dataLayoutControl1;
            this.ExpectedStartDateDateEdit.TabIndex = 2;
            this.ExpectedStartDateDateEdit.EditValueChanged += new System.EventHandler(this.ExpectedStartDateDateEdit_EditValueChanged);
            this.ExpectedStartDateDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ExpectedStartDateDateEdit_Validating);
            this.ExpectedStartDateDateEdit.Validated += new System.EventHandler(this.ExpectedStartDateDateEdit_Validated);
            // 
            // CreatedByStaffNameTextEdit
            // 
            this.CreatedByStaffNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "CreatedByStaffName", true));
            this.CreatedByStaffNameTextEdit.Location = new System.Drawing.Point(147, 830);
            this.CreatedByStaffNameTextEdit.MenuManager = this.barManager1;
            this.CreatedByStaffNameTextEdit.Name = "CreatedByStaffNameTextEdit";
            this.CreatedByStaffNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CreatedByStaffNameTextEdit, true);
            this.CreatedByStaffNameTextEdit.Size = new System.Drawing.Size(248, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CreatedByStaffNameTextEdit, optionsSpelling21);
            this.CreatedByStaffNameTextEdit.StyleController = this.dataLayoutControl1;
            this.CreatedByStaffNameTextEdit.TabIndex = 18;
            // 
            // VisitNumberSpinEdit
            // 
            this.VisitNumberSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "VisitNumber", true));
            this.VisitNumberSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.VisitNumberSpinEdit.Location = new System.Drawing.Point(147, 151);
            this.VisitNumberSpinEdit.MenuManager = this.barManager1;
            this.VisitNumberSpinEdit.Name = "VisitNumberSpinEdit";
            this.VisitNumberSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.VisitNumberSpinEdit.Properties.IsFloatValue = false;
            this.VisitNumberSpinEdit.Properties.Mask.EditMask = "N00";
            this.VisitNumberSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.VisitNumberSpinEdit.Size = new System.Drawing.Size(248, 20);
            this.VisitNumberSpinEdit.StyleController = this.dataLayoutControl1;
            this.VisitNumberSpinEdit.TabIndex = 5;
            this.VisitNumberSpinEdit.Validating += new System.ComponentModel.CancelEventHandler(this.VisitNumberSpinEdit_Validating);
            // 
            // CreatedByStaffIDTextEdit
            // 
            this.CreatedByStaffIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "CreatedByStaffID", true));
            this.CreatedByStaffIDTextEdit.Location = new System.Drawing.Point(114, 69);
            this.CreatedByStaffIDTextEdit.MenuManager = this.barManager1;
            this.CreatedByStaffIDTextEdit.Name = "CreatedByStaffIDTextEdit";
            this.CreatedByStaffIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CreatedByStaffIDTextEdit, true);
            this.CreatedByStaffIDTextEdit.Size = new System.Drawing.Size(560, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CreatedByStaffIDTextEdit, optionsSpelling22);
            this.CreatedByStaffIDTextEdit.StyleController = this.dataLayoutControl1;
            this.CreatedByStaffIDTextEdit.TabIndex = 50;
            // 
            // ClientContractIDTextEdit
            // 
            this.ClientContractIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "ClientContractID", true));
            this.ClientContractIDTextEdit.Location = new System.Drawing.Point(121, 69);
            this.ClientContractIDTextEdit.MenuManager = this.barManager1;
            this.ClientContractIDTextEdit.Name = "ClientContractIDTextEdit";
            this.ClientContractIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientContractIDTextEdit, true);
            this.ClientContractIDTextEdit.Size = new System.Drawing.Size(536, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientContractIDTextEdit, optionsSpelling23);
            this.ClientContractIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientContractIDTextEdit.TabIndex = 49;
            // 
            // SiteContractIDTextEdit
            // 
            this.SiteContractIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "SiteContractID", true));
            this.SiteContractIDTextEdit.Location = new System.Drawing.Point(121, 69);
            this.SiteContractIDTextEdit.MenuManager = this.barManager1;
            this.SiteContractIDTextEdit.Name = "SiteContractIDTextEdit";
            this.SiteContractIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteContractIDTextEdit, true);
            this.SiteContractIDTextEdit.Size = new System.Drawing.Size(536, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteContractIDTextEdit, optionsSpelling24);
            this.SiteContractIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteContractIDTextEdit.TabIndex = 44;
            // 
            // VisitCostSpinEdit
            // 
            this.VisitCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "VisitCost", true));
            this.VisitCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.VisitCostSpinEdit.Location = new System.Drawing.Point(931, 280);
            this.VisitCostSpinEdit.MenuManager = this.barManager1;
            this.VisitCostSpinEdit.Name = "VisitCostSpinEdit";
            this.VisitCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.VisitCostSpinEdit.Properties.Mask.EditMask = "c";
            this.VisitCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.VisitCostSpinEdit.Size = new System.Drawing.Size(96, 20);
            this.VisitCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.VisitCostSpinEdit.TabIndex = 20;
            // 
            // EndDateDateEdit
            // 
            this.EndDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "EndDate", true));
            this.EndDateDateEdit.EditValue = null;
            this.EndDateDateEdit.Location = new System.Drawing.Point(522, 233);
            this.EndDateDateEdit.MenuManager = this.barManager1;
            this.EndDateDateEdit.Name = "EndDateDateEdit";
            this.EndDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.EndDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EndDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EndDateDateEdit.Properties.Mask.EditMask = "G";
            this.EndDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.EndDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.EndDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.EndDateDateEdit.Size = new System.Drawing.Size(240, 20);
            this.EndDateDateEdit.StyleController = this.dataLayoutControl1;
            this.EndDateDateEdit.TabIndex = 14;
            // 
            // StartDateDateEdit
            // 
            this.StartDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "StartDate", true));
            this.StartDateDateEdit.EditValue = null;
            this.StartDateDateEdit.Location = new System.Drawing.Point(147, 233);
            this.StartDateDateEdit.MenuManager = this.barManager1;
            this.StartDateDateEdit.Name = "StartDateDateEdit";
            this.StartDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.StartDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.StartDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.StartDateDateEdit.Properties.Mask.EditMask = "G";
            this.StartDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.StartDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.StartDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.StartDateDateEdit.Size = new System.Drawing.Size(248, 20);
            this.StartDateDateEdit.StyleController = this.dataLayoutControl1;
            this.StartDateDateEdit.TabIndex = 13;
            this.StartDateDateEdit.EditValueChanged += new System.EventHandler(this.StartDateDateEdit_EditValueChanged);
            this.StartDateDateEdit.Validated += new System.EventHandler(this.StartDateDateEdit_Validated);
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(796, 533);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(546, 327);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling25);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 8;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp06145OMVisitEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(12, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(161, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 24;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // LinkedToParentButtonEdit
            // 
            this.LinkedToParentButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "LinkedToParent", true));
            this.LinkedToParentButtonEdit.EditValue = "";
            this.LinkedToParentButtonEdit.Location = new System.Drawing.Point(147, 69);
            this.LinkedToParentButtonEdit.MenuManager = this.barManager1;
            this.LinkedToParentButtonEdit.Name = "LinkedToParentButtonEdit";
            this.LinkedToParentButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions8, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject29, serializableAppearanceObject30, serializableAppearanceObject31, serializableAppearanceObject32, "Click me to Open Select Site Contract screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.LinkedToParentButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.LinkedToParentButtonEdit.Size = new System.Drawing.Size(615, 20);
            this.LinkedToParentButtonEdit.StyleController = this.dataLayoutControl1;
            this.LinkedToParentButtonEdit.TabIndex = 0;
            this.LinkedToParentButtonEdit.TabStop = false;
            this.LinkedToParentButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.LinkedToParentButtonEdit_ButtonClick);
            this.LinkedToParentButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.LinkedToParentButtonEdit_Validating);
            // 
            // VisitIDTextEdit
            // 
            this.VisitIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06145OMVisitEditBindingSource, "VisitID", true));
            this.VisitIDTextEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.VisitIDTextEdit.Location = new System.Drawing.Point(97, 69);
            this.VisitIDTextEdit.MenuManager = this.barManager1;
            this.VisitIDTextEdit.Name = "VisitIDTextEdit";
            this.VisitIDTextEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.VisitIDTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.VisitIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.VisitIDTextEdit, true);
            this.VisitIDTextEdit.Size = new System.Drawing.Size(577, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.VisitIDTextEdit, optionsSpelling26);
            this.VisitIDTextEdit.StyleController = this.dataLayoutControl1;
            this.VisitIDTextEdit.TabIndex = 27;
            // 
            // ItemForClientContractID
            // 
            this.ItemForClientContractID.Control = this.ClientContractIDTextEdit;
            this.ItemForClientContractID.CustomizationFormText = "Client Contract ID:";
            this.ItemForClientContractID.Location = new System.Drawing.Point(0, 57);
            this.ItemForClientContractID.Name = "ItemForClientContractID";
            this.ItemForClientContractID.Size = new System.Drawing.Size(649, 24);
            this.ItemForClientContractID.Text = "Client Contract ID:";
            this.ItemForClientContractID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForSiteContractID
            // 
            this.ItemForSiteContractID.Control = this.SiteContractIDTextEdit;
            this.ItemForSiteContractID.CustomizationFormText = "Site Contract ID:";
            this.ItemForSiteContractID.Location = new System.Drawing.Point(0, 57);
            this.ItemForSiteContractID.Name = "ItemForSiteContractID";
            this.ItemForSiteContractID.Size = new System.Drawing.Size(649, 24);
            this.ItemForSiteContractID.Text = "Site Contract ID:";
            this.ItemForSiteContractID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForVisitID
            // 
            this.ItemForVisitID.Control = this.VisitIDTextEdit;
            this.ItemForVisitID.CustomizationFormText = "Visit ID:";
            this.ItemForVisitID.Location = new System.Drawing.Point(0, 57);
            this.ItemForVisitID.Name = "ItemForVisitID";
            this.ItemForVisitID.Size = new System.Drawing.Size(666, 24);
            this.ItemForVisitID.Text = "Visit ID:";
            this.ItemForVisitID.TextSize = new System.Drawing.Size(84, 13);
            // 
            // ItemForCreatedByStaffID
            // 
            this.ItemForCreatedByStaffID.Control = this.CreatedByStaffIDTextEdit;
            this.ItemForCreatedByStaffID.CustomizationFormText = "Created By Staff ID:";
            this.ItemForCreatedByStaffID.Location = new System.Drawing.Point(0, 57);
            this.ItemForCreatedByStaffID.Name = "ItemForCreatedByStaffID";
            this.ItemForCreatedByStaffID.Size = new System.Drawing.Size(666, 24);
            this.ItemForCreatedByStaffID.Text = "Created By Staff ID:";
            this.ItemForCreatedByStaffID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForImagesFolderOM
            // 
            this.ItemForImagesFolderOM.Control = this.ImagesFolderOMTextEdit;
            this.ItemForImagesFolderOM.CustomizationFormText = "Client Images Folder:";
            this.ItemForImagesFolderOM.Location = new System.Drawing.Point(0, 153);
            this.ItemForImagesFolderOM.Name = "ItemForImagesFolderOM";
            this.ItemForImagesFolderOM.Size = new System.Drawing.Size(649, 24);
            this.ItemForImagesFolderOM.Text = "Client Images Folder:";
            this.ItemForImagesFolderOM.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForClientPOID
            // 
            this.ItemForClientPOID.Control = this.ClientPOIDTextEdit;
            this.ItemForClientPOID.Location = new System.Drawing.Point(0, 225);
            this.ItemForClientPOID.Name = "ItemForClientPOID";
            this.ItemForClientPOID.Size = new System.Drawing.Size(324, 24);
            this.ItemForClientPOID.Text = "Client PO ID:";
            this.ItemForClientPOID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForClientID
            // 
            this.ItemForClientID.Control = this.ClientIDTextEdit;
            this.ItemForClientID.Location = new System.Drawing.Point(0, 211);
            this.ItemForClientID.Name = "ItemForClientID";
            this.ItemForClientID.Size = new System.Drawing.Size(649, 24);
            this.ItemForClientID.Text = "Client ID:";
            this.ItemForClientID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForVisitStatusID
            // 
            this.ItemForVisitStatusID.Control = this.VisitStatusIDTextEdit;
            this.ItemForVisitStatusID.Location = new System.Drawing.Point(0, 225);
            this.ItemForVisitStatusID.Name = "ItemForVisitStatusID";
            this.ItemForVisitStatusID.Size = new System.Drawing.Size(649, 24);
            this.ItemForVisitStatusID.Text = "System Status ID:";
            this.ItemForVisitStatusID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForSiteContractDaysSeparationPercent
            // 
            this.ItemForSiteContractDaysSeparationPercent.Control = this.SiteContractDaysSeparationPercentTextEdit;
            this.ItemForSiteContractDaysSeparationPercent.Location = new System.Drawing.Point(0, 143);
            this.ItemForSiteContractDaysSeparationPercent.Name = "ItemForSiteContractDaysSeparationPercent";
            this.ItemForSiteContractDaysSeparationPercent.Size = new System.Drawing.Size(260, 24);
            this.ItemForSiteContractDaysSeparationPercent.Text = "Site Contract Days Separation %:";
            this.ItemForSiteContractDaysSeparationPercent.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1366, 894);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.layoutControlGroup8,
            this.layoutControlGroup17,
            this.emptySpaceItem7,
            this.emptySpaceItem5,
            this.layoutControlGroup3,
            this.layoutControlGroup4,
            this.emptySpaceItem4,
            this.splitterItem1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1346, 874);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(165, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(1181, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(165, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.AllowHtmlStringInCaption = true;
            this.layoutControlGroup8.CustomizationFormText = "Extra Work";
            this.layoutControlGroup8.ExpandButtonVisible = true;
            this.layoutControlGroup8.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForExtraWorkRequired,
            this.ItemForExtraWorkComments,
            this.ItemForExtraWorkTypeID});
            this.layoutControlGroup8.Location = new System.Drawing.Point(772, 376);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Size = new System.Drawing.Size(574, 99);
            this.layoutControlGroup8.Text = "Extra Work";
            // 
            // ItemForExtraWorkRequired
            // 
            this.ItemForExtraWorkRequired.Control = this.ExtraWorkRequiredCheckEdit;
            this.ItemForExtraWorkRequired.CustomizationFormText = "Extra Work Required:";
            this.ItemForExtraWorkRequired.Location = new System.Drawing.Point(0, 0);
            this.ItemForExtraWorkRequired.Name = "ItemForExtraWorkRequired";
            this.ItemForExtraWorkRequired.Size = new System.Drawing.Size(209, 24);
            this.ItemForExtraWorkRequired.Text = "Extra Work Required:";
            this.ItemForExtraWorkRequired.TextSize = new System.Drawing.Size(120, 13);
            // 
            // ItemForExtraWorkComments
            // 
            this.ItemForExtraWorkComments.Control = this.ExtraWorkCommentsMemoEdit;
            this.ItemForExtraWorkComments.CustomizationFormText = "Extra Work Remarks:";
            this.ItemForExtraWorkComments.Location = new System.Drawing.Point(0, 24);
            this.ItemForExtraWorkComments.Name = "ItemForExtraWorkComments";
            this.ItemForExtraWorkComments.Size = new System.Drawing.Size(550, 29);
            this.ItemForExtraWorkComments.Text = "Extra Work Remarks:";
            this.ItemForExtraWorkComments.TextSize = new System.Drawing.Size(120, 13);
            // 
            // ItemForExtraWorkTypeID
            // 
            this.ItemForExtraWorkTypeID.Control = this.ExtraWorkTypeIDGridLookUpEdit;
            this.ItemForExtraWorkTypeID.CustomizationFormText = "Extra Work Type:";
            this.ItemForExtraWorkTypeID.Location = new System.Drawing.Point(209, 0);
            this.ItemForExtraWorkTypeID.Name = "ItemForExtraWorkTypeID";
            this.ItemForExtraWorkTypeID.Size = new System.Drawing.Size(341, 24);
            this.ItemForExtraWorkTypeID.Text = "Extra Work Type:";
            this.ItemForExtraWorkTypeID.TextSize = new System.Drawing.Size(120, 13);
            // 
            // layoutControlGroup17
            // 
            this.layoutControlGroup17.AllowHtmlStringInCaption = true;
            this.layoutControlGroup17.ExpandButtonVisible = true;
            this.layoutControlGroup17.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup17.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForClientPONumber,
            this.ItemForCompletionSheetNumber,
            this.ItemForWorkNumber,
            this.emptySpaceItem24,
            this.layoutControlGroupTeamCosts,
            this.layoutControlGroupClientSell,
            this.emptySpaceItem19,
            this.ItemForExtraCostsFinalised});
            this.layoutControlGroup17.Location = new System.Drawing.Point(772, 23);
            this.layoutControlGroup17.Name = "layoutControlGroup17";
            this.layoutControlGroup17.Size = new System.Drawing.Size(574, 343);
            this.layoutControlGroup17.Text = "Payment Details";
            // 
            // ItemForClientPONumber
            // 
            this.ItemForClientPONumber.Control = this.ClientPONumberButtonEdit;
            this.ItemForClientPONumber.Location = new System.Drawing.Point(0, 0);
            this.ItemForClientPONumber.Name = "ItemForClientPONumber";
            this.ItemForClientPONumber.Size = new System.Drawing.Size(275, 24);
            this.ItemForClientPONumber.Text = "Client PO #:";
            this.ItemForClientPONumber.TextSize = new System.Drawing.Size(120, 13);
            // 
            // ItemForCompletionSheetNumber
            // 
            this.ItemForCompletionSheetNumber.Control = this.CompletionSheetNumberTextEdit;
            this.ItemForCompletionSheetNumber.Location = new System.Drawing.Point(275, 0);
            this.ItemForCompletionSheetNumber.Name = "ItemForCompletionSheetNumber";
            this.ItemForCompletionSheetNumber.Size = new System.Drawing.Size(275, 24);
            this.ItemForCompletionSheetNumber.Text = "Completion Sheet #:";
            this.ItemForCompletionSheetNumber.TextSize = new System.Drawing.Size(120, 13);
            // 
            // ItemForWorkNumber
            // 
            this.ItemForWorkNumber.Control = this.WorkNumberTextEdit;
            this.ItemForWorkNumber.CustomizationFormText = "Work Number:";
            this.ItemForWorkNumber.Location = new System.Drawing.Point(0, 24);
            this.ItemForWorkNumber.Name = "ItemForWorkNumber";
            this.ItemForWorkNumber.Size = new System.Drawing.Size(275, 24);
            this.ItemForWorkNumber.Text = "Work Number:";
            this.ItemForWorkNumber.TextSize = new System.Drawing.Size(120, 13);
            // 
            // emptySpaceItem24
            // 
            this.emptySpaceItem24.AllowHotTrack = false;
            this.emptySpaceItem24.Location = new System.Drawing.Point(478, 24);
            this.emptySpaceItem24.Name = "emptySpaceItem24";
            this.emptySpaceItem24.Size = new System.Drawing.Size(72, 24);
            this.emptySpaceItem24.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupTeamCosts
            // 
            this.layoutControlGroupTeamCosts.AllowHtmlStringInCaption = true;
            this.layoutControlGroupTeamCosts.CaptionImageOptions.Location = DevExpress.Utils.GroupElementLocation.BeforeText;
            this.layoutControlGroupTeamCosts.CustomizationFormText = "Team <b>Costs</b>";
            this.layoutControlGroupTeamCosts.ExpandButtonVisible = true;
            this.layoutControlGroupTeamCosts.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroupTeamCosts.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForCostCalculationLevel,
            this.ItemForVisitCost,
            this.ItemForVisitCostButton,
            this.ItemForSelfBillingInvoice,
            this.ItemForDoNotPayContractor,
            this.emptySpaceItem9,
            this.ItemForVisitLabourCost,
            this.emptySpaceItem1,
            this.ItemForVisitMaterialCost,
            this.ItemForVisitEquipmentCost,
            this.ItemForSelfBillingInvoiceNumber});
            this.layoutControlGroupTeamCosts.Location = new System.Drawing.Point(0, 58);
            this.layoutControlGroupTeamCosts.Name = "layoutControlGroupTeamCosts";
            this.layoutControlGroupTeamCosts.Size = new System.Drawing.Size(275, 239);
            this.layoutControlGroupTeamCosts.Text = "Team <b>Costs</b>";
            // 
            // ItemForCostCalculationLevel
            // 
            this.ItemForCostCalculationLevel.Control = this.CostCalculationLevelGridLookUpEdit;
            this.ItemForCostCalculationLevel.CustomizationFormText = "Cost Calculation:";
            this.ItemForCostCalculationLevel.Location = new System.Drawing.Point(0, 23);
            this.ItemForCostCalculationLevel.Name = "ItemForCostCalculationLevel";
            this.ItemForCostCalculationLevel.Size = new System.Drawing.Size(251, 24);
            this.ItemForCostCalculationLevel.Text = "Cost Calculation:";
            this.ItemForCostCalculationLevel.TextSize = new System.Drawing.Size(120, 13);
            // 
            // ItemForVisitCost
            // 
            this.ItemForVisitCost.Control = this.VisitCostSpinEdit;
            this.ItemForVisitCost.CustomizationFormText = "Visit Cost:";
            this.ItemForVisitCost.Location = new System.Drawing.Point(0, 119);
            this.ItemForVisitCost.Name = "ItemForVisitCost";
            this.ItemForVisitCost.Size = new System.Drawing.Size(223, 26);
            this.ItemForVisitCost.Text = "Visit Total Cost:";
            this.ItemForVisitCost.TextSize = new System.Drawing.Size(120, 13);
            // 
            // ItemForVisitCostButton
            // 
            this.ItemForVisitCostButton.Control = this.CalculateCostButton;
            this.ItemForVisitCostButton.Location = new System.Drawing.Point(223, 119);
            this.ItemForVisitCostButton.MaxSize = new System.Drawing.Size(28, 26);
            this.ItemForVisitCostButton.MinSize = new System.Drawing.Size(28, 26);
            this.ItemForVisitCostButton.Name = "ItemForVisitCostButton";
            this.ItemForVisitCostButton.Size = new System.Drawing.Size(28, 26);
            this.ItemForVisitCostButton.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForVisitCostButton.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForVisitCostButton.TextVisible = false;
            // 
            // ItemForSelfBillingInvoice
            // 
            this.ItemForSelfBillingInvoice.Control = this.SelfBillingInvoiceButtonEdit;
            this.ItemForSelfBillingInvoice.Location = new System.Drawing.Point(0, 145);
            this.ItemForSelfBillingInvoice.Name = "ItemForSelfBillingInvoice";
            this.ItemForSelfBillingInvoice.Size = new System.Drawing.Size(251, 24);
            this.ItemForSelfBillingInvoice.Text = "Self-Billing Invoice:";
            this.ItemForSelfBillingInvoice.TextSize = new System.Drawing.Size(120, 13);
            // 
            // ItemForDoNotPayContractor
            // 
            this.ItemForDoNotPayContractor.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.ItemForDoNotPayContractor.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForDoNotPayContractor.Control = this.DoNotPayContractorCheckEdit;
            this.ItemForDoNotPayContractor.Location = new System.Drawing.Point(0, 0);
            this.ItemForDoNotPayContractor.Name = "ItemForDoNotPayContractor";
            this.ItemForDoNotPayContractor.Size = new System.Drawing.Size(203, 23);
            this.ItemForDoNotPayContractor.Text = "Do Not Pay Team:";
            this.ItemForDoNotPayContractor.TextSize = new System.Drawing.Size(120, 13);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.Location = new System.Drawing.Point(203, 0);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(48, 23);
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForVisitLabourCost
            // 
            this.ItemForVisitLabourCost.Control = this.VisitLabourCostSpinEdit;
            this.ItemForVisitLabourCost.Location = new System.Drawing.Point(0, 47);
            this.ItemForVisitLabourCost.Name = "ItemForVisitLabourCost";
            this.ItemForVisitLabourCost.Size = new System.Drawing.Size(223, 24);
            this.ItemForVisitLabourCost.Text = "Visit Labour Cost:";
            this.ItemForVisitLabourCost.TextSize = new System.Drawing.Size(120, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(223, 47);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(28, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(28, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(28, 72);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForVisitMaterialCost
            // 
            this.ItemForVisitMaterialCost.Control = this.VisitMaterialCostSpinEdit;
            this.ItemForVisitMaterialCost.Location = new System.Drawing.Point(0, 71);
            this.ItemForVisitMaterialCost.Name = "ItemForVisitMaterialCost";
            this.ItemForVisitMaterialCost.Size = new System.Drawing.Size(223, 24);
            this.ItemForVisitMaterialCost.Text = "Visit Material Cost:";
            this.ItemForVisitMaterialCost.TextSize = new System.Drawing.Size(120, 13);
            // 
            // ItemForVisitEquipmentCost
            // 
            this.ItemForVisitEquipmentCost.Control = this.VisitEquipmentCostSpinEdit;
            this.ItemForVisitEquipmentCost.Location = new System.Drawing.Point(0, 95);
            this.ItemForVisitEquipmentCost.Name = "ItemForVisitEquipmentCost";
            this.ItemForVisitEquipmentCost.Size = new System.Drawing.Size(223, 24);
            this.ItemForVisitEquipmentCost.Text = "Visit Equipment Cost:";
            this.ItemForVisitEquipmentCost.TextSize = new System.Drawing.Size(120, 13);
            // 
            // ItemForSelfBillingInvoiceNumber
            // 
            this.ItemForSelfBillingInvoiceNumber.Control = this.SelfBillingInvoiceNumberTextEdit;
            this.ItemForSelfBillingInvoiceNumber.Location = new System.Drawing.Point(0, 169);
            this.ItemForSelfBillingInvoiceNumber.Name = "ItemForSelfBillingInvoiceNumber";
            this.ItemForSelfBillingInvoiceNumber.Size = new System.Drawing.Size(251, 24);
            this.ItemForSelfBillingInvoiceNumber.Text = "Self-Billing Invoice #:";
            this.ItemForSelfBillingInvoiceNumber.TextSize = new System.Drawing.Size(120, 13);
            // 
            // layoutControlGroupClientSell
            // 
            this.layoutControlGroupClientSell.AllowHtmlStringInCaption = true;
            this.layoutControlGroupClientSell.ExpandButtonVisible = true;
            this.layoutControlGroupClientSell.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroupClientSell.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSellCalculationLevel,
            this.ItemForDoNotInvoiceClient,
            this.ItemForVisitSell,
            this.ItemForVisitSellButton,
            this.ItemForClientInvoiceID,
            this.emptySpaceItem20,
            this.ItemForVisitLabourSell,
            this.emptySpaceItem14,
            this.ItemForVisitMaterialSell,
            this.ItemForVisitEquipmentSell,
            this.emptySpaceItem18});
            this.layoutControlGroupClientSell.Location = new System.Drawing.Point(275, 58);
            this.layoutControlGroupClientSell.Name = "layoutControlGroupClientSell";
            this.layoutControlGroupClientSell.Size = new System.Drawing.Size(275, 239);
            this.layoutControlGroupClientSell.Text = "Client <b>Sell</b>";
            // 
            // ItemForSellCalculationLevel
            // 
            this.ItemForSellCalculationLevel.Control = this.SellCalculationLevelGridLookUpEdit;
            this.ItemForSellCalculationLevel.Location = new System.Drawing.Point(0, 23);
            this.ItemForSellCalculationLevel.Name = "ItemForSellCalculationLevel";
            this.ItemForSellCalculationLevel.Size = new System.Drawing.Size(251, 24);
            this.ItemForSellCalculationLevel.Text = "Sell Calculation:";
            this.ItemForSellCalculationLevel.TextSize = new System.Drawing.Size(120, 13);
            // 
            // ItemForDoNotInvoiceClient
            // 
            this.ItemForDoNotInvoiceClient.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.ItemForDoNotInvoiceClient.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForDoNotInvoiceClient.Control = this.DoNotInvoiceClientCheckEdit;
            this.ItemForDoNotInvoiceClient.Location = new System.Drawing.Point(0, 0);
            this.ItemForDoNotInvoiceClient.Name = "ItemForDoNotInvoiceClient";
            this.ItemForDoNotInvoiceClient.Size = new System.Drawing.Size(203, 23);
            this.ItemForDoNotInvoiceClient.Text = "Do Not Invoice Client:";
            this.ItemForDoNotInvoiceClient.TextSize = new System.Drawing.Size(120, 13);
            // 
            // ItemForVisitSell
            // 
            this.ItemForVisitSell.Control = this.VisitSellSpinEdit;
            this.ItemForVisitSell.CustomizationFormText = "Visit Sell:";
            this.ItemForVisitSell.Location = new System.Drawing.Point(0, 119);
            this.ItemForVisitSell.Name = "ItemForVisitSell";
            this.ItemForVisitSell.Size = new System.Drawing.Size(223, 26);
            this.ItemForVisitSell.Text = "Visit Total Sell:";
            this.ItemForVisitSell.TextSize = new System.Drawing.Size(120, 13);
            // 
            // ItemForVisitSellButton
            // 
            this.ItemForVisitSellButton.Control = this.CalculateSellButton;
            this.ItemForVisitSellButton.Location = new System.Drawing.Point(223, 119);
            this.ItemForVisitSellButton.MaxSize = new System.Drawing.Size(28, 26);
            this.ItemForVisitSellButton.MinSize = new System.Drawing.Size(28, 26);
            this.ItemForVisitSellButton.Name = "ItemForVisitSellButton";
            this.ItemForVisitSellButton.Size = new System.Drawing.Size(28, 26);
            this.ItemForVisitSellButton.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForVisitSellButton.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForVisitSellButton.TextVisible = false;
            // 
            // ItemForClientInvoiceID
            // 
            this.ItemForClientInvoiceID.Control = this.ClientInvoiceIDButtonEdit;
            this.ItemForClientInvoiceID.Location = new System.Drawing.Point(0, 145);
            this.ItemForClientInvoiceID.Name = "ItemForClientInvoiceID";
            this.ItemForClientInvoiceID.Size = new System.Drawing.Size(251, 24);
            this.ItemForClientInvoiceID.Text = "Client Invoice ID:";
            this.ItemForClientInvoiceID.TextSize = new System.Drawing.Size(120, 13);
            // 
            // emptySpaceItem20
            // 
            this.emptySpaceItem20.AllowHotTrack = false;
            this.emptySpaceItem20.Location = new System.Drawing.Point(203, 0);
            this.emptySpaceItem20.Name = "emptySpaceItem20";
            this.emptySpaceItem20.Size = new System.Drawing.Size(48, 23);
            this.emptySpaceItem20.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForVisitLabourSell
            // 
            this.ItemForVisitLabourSell.Control = this.VisitLabourSellSpinEdit;
            this.ItemForVisitLabourSell.Location = new System.Drawing.Point(0, 47);
            this.ItemForVisitLabourSell.Name = "ItemForVisitLabourSell";
            this.ItemForVisitLabourSell.Size = new System.Drawing.Size(223, 24);
            this.ItemForVisitLabourSell.Text = "Visit Labour Sell:";
            this.ItemForVisitLabourSell.TextSize = new System.Drawing.Size(120, 13);
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.Location = new System.Drawing.Point(223, 47);
            this.emptySpaceItem14.MaxSize = new System.Drawing.Size(28, 0);
            this.emptySpaceItem14.MinSize = new System.Drawing.Size(28, 10);
            this.emptySpaceItem14.Name = "emptySpaceItem14";
            this.emptySpaceItem14.Size = new System.Drawing.Size(28, 72);
            this.emptySpaceItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForVisitMaterialSell
            // 
            this.ItemForVisitMaterialSell.Control = this.VisitMaterialSellSpinEdit;
            this.ItemForVisitMaterialSell.Location = new System.Drawing.Point(0, 71);
            this.ItemForVisitMaterialSell.Name = "ItemForVisitMaterialSell";
            this.ItemForVisitMaterialSell.Size = new System.Drawing.Size(223, 24);
            this.ItemForVisitMaterialSell.Text = "Visit Material Sell:";
            this.ItemForVisitMaterialSell.TextSize = new System.Drawing.Size(120, 13);
            // 
            // ItemForVisitEquipmentSell
            // 
            this.ItemForVisitEquipmentSell.Control = this.VisitEquipmentSellSpinEdit;
            this.ItemForVisitEquipmentSell.Location = new System.Drawing.Point(0, 95);
            this.ItemForVisitEquipmentSell.Name = "ItemForVisitEquipmentSell";
            this.ItemForVisitEquipmentSell.Size = new System.Drawing.Size(223, 24);
            this.ItemForVisitEquipmentSell.Text = "Visit Equipment Sell:";
            this.ItemForVisitEquipmentSell.TextSize = new System.Drawing.Size(120, 13);
            // 
            // emptySpaceItem18
            // 
            this.emptySpaceItem18.AllowHotTrack = false;
            this.emptySpaceItem18.Location = new System.Drawing.Point(0, 169);
            this.emptySpaceItem18.Name = "emptySpaceItem18";
            this.emptySpaceItem18.Size = new System.Drawing.Size(251, 24);
            this.emptySpaceItem18.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem19
            // 
            this.emptySpaceItem19.AllowHotTrack = false;
            this.emptySpaceItem19.Location = new System.Drawing.Point(0, 48);
            this.emptySpaceItem19.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem19.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem19.Name = "emptySpaceItem19";
            this.emptySpaceItem19.Size = new System.Drawing.Size(550, 10);
            this.emptySpaceItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem19.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForExtraCostsFinalised
            // 
            this.ItemForExtraCostsFinalised.Control = this.ExtraCostsFinalisedCheckEdit;
            this.ItemForExtraCostsFinalised.Location = new System.Drawing.Point(275, 24);
            this.ItemForExtraCostsFinalised.MaxSize = new System.Drawing.Size(203, 23);
            this.ItemForExtraCostsFinalised.MinSize = new System.Drawing.Size(203, 23);
            this.ItemForExtraCostsFinalised.Name = "ItemForExtraCostsFinalised";
            this.ItemForExtraCostsFinalised.Size = new System.Drawing.Size(203, 24);
            this.ItemForExtraCostsFinalised.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForExtraCostsFinalised.Text = "Extra Costs Finalised:";
            this.ItemForExtraCostsFinalised.TextSize = new System.Drawing.Size(120, 13);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.Location = new System.Drawing.Point(772, 366);
            this.emptySpaceItem7.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem7.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(574, 10);
            this.emptySpaceItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 864);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(1346, 10);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CaptionImageOptions.Image = global::WoodPlan5.Properties.Resources.Notes_16x16;
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup3.Location = new System.Drawing.Point(772, 485);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(574, 379);
            this.layoutControlGroup3.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Comments:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(550, 331);
            this.ItemForRemarks.Text = "Comments:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForLinkedToParent,
            this.ItemForSiteID,
            this.emptySpaceItem23,
            this.ItemForVisitTypeID,
            this.ItemForVisitCategoryID,
            this.ItemForTimeliness,
            this.ItemForStatusDescription,
            this.layoutControlGroup7,
            this.layoutControlGroup16,
            this.ItemForCreatedByStaffName,
            this.ItemForCreatedFromVisitTemplateID,
            this.emptySpaceItem3,
            this.emptySpaceItem6,
            this.emptySpaceItem13,
            this.ItemForVisitNumber,
            this.ItemForFriendlyVisitNumber,
            this.ItemForExpectedStartDate,
            this.ItemForExpectedEndDate,
            this.ItemForStartDate,
            this.ItemForEndDate,
            this.ItemForCalculateDaysButton,
            this.ItemForDaysSeparation,
            this.emptySpaceItem10,
            this.layoutControlGroup9,
            this.emptySpaceItem8,
            this.emptySpaceItem15,
            this.emptySpaceItem16,
            this.emptySpaceItem17,
            this.layoutControlGroup15,
            this.emptySpaceItem12});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 23);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(766, 841);
            this.layoutControlGroup4.Text = "Visit Details";
            // 
            // ItemForLinkedToParent
            // 
            this.ItemForLinkedToParent.AllowHide = false;
            this.ItemForLinkedToParent.Control = this.LinkedToParentButtonEdit;
            this.ItemForLinkedToParent.CustomizationFormText = "Site Contract:";
            this.ItemForLinkedToParent.Location = new System.Drawing.Point(0, 0);
            this.ItemForLinkedToParent.Name = "ItemForLinkedToParent";
            this.ItemForLinkedToParent.Size = new System.Drawing.Size(742, 24);
            this.ItemForLinkedToParent.Text = "Site Contract:";
            this.ItemForLinkedToParent.TextSize = new System.Drawing.Size(120, 13);
            // 
            // ItemForSiteID
            // 
            this.ItemForSiteID.Control = this.SiteIDTextEdit;
            this.ItemForSiteID.Location = new System.Drawing.Point(0, 24);
            this.ItemForSiteID.Name = "ItemForSiteID";
            this.ItemForSiteID.Size = new System.Drawing.Size(375, 24);
            this.ItemForSiteID.Text = "Site ID:";
            this.ItemForSiteID.TextSize = new System.Drawing.Size(120, 13);
            // 
            // emptySpaceItem23
            // 
            this.emptySpaceItem23.AllowHotTrack = false;
            this.emptySpaceItem23.Location = new System.Drawing.Point(375, 24);
            this.emptySpaceItem23.Name = "emptySpaceItem23";
            this.emptySpaceItem23.Size = new System.Drawing.Size(367, 24);
            this.emptySpaceItem23.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForVisitTypeID
            // 
            this.ItemForVisitTypeID.Control = this.VisitTypeIDGridLookUpEdit;
            this.ItemForVisitTypeID.Location = new System.Drawing.Point(0, 58);
            this.ItemForVisitTypeID.Name = "ItemForVisitTypeID";
            this.ItemForVisitTypeID.Size = new System.Drawing.Size(375, 24);
            this.ItemForVisitTypeID.Text = "Visit Type:";
            this.ItemForVisitTypeID.TextSize = new System.Drawing.Size(120, 13);
            // 
            // ItemForVisitCategoryID
            // 
            this.ItemForVisitCategoryID.Control = this.VisitCategoryIDGridLookUpEdit;
            this.ItemForVisitCategoryID.CustomizationFormText = "Visit Category:";
            this.ItemForVisitCategoryID.Location = new System.Drawing.Point(375, 58);
            this.ItemForVisitCategoryID.Name = "ItemForVisitCategoryID";
            this.ItemForVisitCategoryID.Size = new System.Drawing.Size(367, 24);
            this.ItemForVisitCategoryID.Text = "Visit Category:";
            this.ItemForVisitCategoryID.TextSize = new System.Drawing.Size(120, 13);
            // 
            // ItemForTimeliness
            // 
            this.ItemForTimeliness.Control = this.TimelinessTextEdit;
            this.ItemForTimeliness.CustomizationFormText = "Visit Status:";
            this.ItemForTimeliness.Location = new System.Drawing.Point(0, 106);
            this.ItemForTimeliness.Name = "ItemForTimeliness";
            this.ItemForTimeliness.Size = new System.Drawing.Size(375, 24);
            this.ItemForTimeliness.Text = "Visit Status:";
            this.ItemForTimeliness.TextSize = new System.Drawing.Size(120, 13);
            // 
            // ItemForStatusDescription
            // 
            this.ItemForStatusDescription.Control = this.StatusDescriptionButtonEdit;
            this.ItemForStatusDescription.Location = new System.Drawing.Point(375, 106);
            this.ItemForStatusDescription.Name = "ItemForStatusDescription";
            this.ItemForStatusDescription.Size = new System.Drawing.Size(367, 24);
            this.ItemForStatusDescription.Text = "System Status:";
            this.ItemForStatusDescription.TextSize = new System.Drawing.Size(120, 13);
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.AllowHtmlStringInCaption = true;
            this.layoutControlGroup7.CustomizationFormText = "Issues";
            this.layoutControlGroup7.ExpandButtonVisible = true;
            this.layoutControlGroup7.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForIssueRemarks,
            this.ItemForRework,
            this.layoutControlItem2,
            this.ItemForStatusIssueID,
            this.ItemForIssueTypeID});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 328);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(742, 142);
            this.layoutControlGroup7.Text = "Issues";
            // 
            // ItemForIssueRemarks
            // 
            this.ItemForIssueRemarks.Control = this.IssueRemarksMemoEdit;
            this.ItemForIssueRemarks.CustomizationFormText = "Issue Remarks:";
            this.ItemForIssueRemarks.Location = new System.Drawing.Point(0, 48);
            this.ItemForIssueRemarks.MaxSize = new System.Drawing.Size(0, 48);
            this.ItemForIssueRemarks.MinSize = new System.Drawing.Size(137, 48);
            this.ItemForIssueRemarks.Name = "ItemForIssueRemarks";
            this.ItemForIssueRemarks.Size = new System.Drawing.Size(718, 48);
            this.ItemForIssueRemarks.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForIssueRemarks.Text = "Issue Remarks:";
            this.ItemForIssueRemarks.TextSize = new System.Drawing.Size(120, 13);
            // 
            // ItemForRework
            // 
            this.ItemForRework.Control = this.ReworkCheckEdit;
            this.ItemForRework.CustomizationFormText = "Rework:";
            this.ItemForRework.Location = new System.Drawing.Point(0, 0);
            this.ItemForRework.MaxSize = new System.Drawing.Size(220, 23);
            this.ItemForRework.MinSize = new System.Drawing.Size(220, 23);
            this.ItemForRework.Name = "ItemForRework";
            this.ItemForRework.Size = new System.Drawing.Size(220, 24);
            this.ItemForRework.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForRework.Text = "Rework:";
            this.ItemForRework.TextSize = new System.Drawing.Size(120, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.IssueFoundCheckEdit;
            this.layoutControlItem2.CustomizationFormText = "Issue Found:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(220, 23);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(220, 23);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(220, 24);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "Issue Found:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(120, 13);
            // 
            // ItemForStatusIssueID
            // 
            this.ItemForStatusIssueID.Control = this.StatusIssueIDGridLookUpEdit;
            this.ItemForStatusIssueID.Location = new System.Drawing.Point(220, 0);
            this.ItemForStatusIssueID.Name = "ItemForStatusIssueID";
            this.ItemForStatusIssueID.Size = new System.Drawing.Size(498, 24);
            this.ItemForStatusIssueID.Text = "Status Issue:";
            this.ItemForStatusIssueID.TextSize = new System.Drawing.Size(120, 13);
            // 
            // ItemForIssueTypeID
            // 
            this.ItemForIssueTypeID.Control = this.IssueTypeIDGridLookUpEdit;
            this.ItemForIssueTypeID.CustomizationFormText = "Issue Type:";
            this.ItemForIssueTypeID.Location = new System.Drawing.Point(220, 24);
            this.ItemForIssueTypeID.Name = "ItemForIssueTypeID";
            this.ItemForIssueTypeID.Size = new System.Drawing.Size(498, 24);
            this.ItemForIssueTypeID.Text = "Issue Type:";
            this.ItemForIssueTypeID.TextSize = new System.Drawing.Size(120, 13);
            // 
            // layoutControlGroup16
            // 
            this.layoutControlGroup16.AllowHtmlStringInCaption = true;
            this.layoutControlGroup16.CustomizationFormText = "Accidents";
            this.layoutControlGroup16.ExpandButtonVisible = true;
            this.layoutControlGroup16.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup16.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForAccidentOnSite,
            this.emptySpaceItem11,
            this.ItemForAccidentComment});
            this.layoutControlGroup16.Location = new System.Drawing.Point(0, 480);
            this.layoutControlGroup16.Name = "layoutControlGroup16";
            this.layoutControlGroup16.Size = new System.Drawing.Size(742, 117);
            this.layoutControlGroup16.Text = "Accidents";
            // 
            // ItemForAccidentOnSite
            // 
            this.ItemForAccidentOnSite.Control = this.AccidentOnSiteCheckEdit;
            this.ItemForAccidentOnSite.CustomizationFormText = "Accident On site:";
            this.ItemForAccidentOnSite.Location = new System.Drawing.Point(0, 0);
            this.ItemForAccidentOnSite.MaxSize = new System.Drawing.Size(208, 23);
            this.ItemForAccidentOnSite.MinSize = new System.Drawing.Size(208, 23);
            this.ItemForAccidentOnSite.Name = "ItemForAccidentOnSite";
            this.ItemForAccidentOnSite.Size = new System.Drawing.Size(208, 23);
            this.ItemForAccidentOnSite.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForAccidentOnSite.Text = "Accident On site:";
            this.ItemForAccidentOnSite.TextSize = new System.Drawing.Size(120, 13);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.Location = new System.Drawing.Point(208, 0);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(510, 23);
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForAccidentComment
            // 
            this.ItemForAccidentComment.Control = this.AccidentCommentMemoEdit;
            this.ItemForAccidentComment.CustomizationFormText = "Accident Comments:";
            this.ItemForAccidentComment.Location = new System.Drawing.Point(0, 23);
            this.ItemForAccidentComment.MaxSize = new System.Drawing.Size(0, 48);
            this.ItemForAccidentComment.MinSize = new System.Drawing.Size(137, 48);
            this.ItemForAccidentComment.Name = "ItemForAccidentComment";
            this.ItemForAccidentComment.Size = new System.Drawing.Size(718, 48);
            this.ItemForAccidentComment.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForAccidentComment.Text = "Accident Comments:";
            this.ItemForAccidentComment.TextSize = new System.Drawing.Size(120, 13);
            // 
            // ItemForCreatedByStaffName
            // 
            this.ItemForCreatedByStaffName.Control = this.CreatedByStaffNameTextEdit;
            this.ItemForCreatedByStaffName.CustomizationFormText = "Created By:";
            this.ItemForCreatedByStaffName.Location = new System.Drawing.Point(0, 761);
            this.ItemForCreatedByStaffName.Name = "ItemForCreatedByStaffName";
            this.ItemForCreatedByStaffName.Size = new System.Drawing.Size(375, 24);
            this.ItemForCreatedByStaffName.Text = "Created By:";
            this.ItemForCreatedByStaffName.TextSize = new System.Drawing.Size(120, 13);
            // 
            // ItemForCreatedFromVisitTemplateID
            // 
            this.ItemForCreatedFromVisitTemplateID.Control = this.CreatedFromVisitTemplateIDGridLookUpEdit;
            this.ItemForCreatedFromVisitTemplateID.Location = new System.Drawing.Point(375, 761);
            this.ItemForCreatedFromVisitTemplateID.Name = "ItemForCreatedFromVisitTemplateID";
            this.ItemForCreatedFromVisitTemplateID.Size = new System.Drawing.Size(367, 24);
            this.ItemForCreatedFromVisitTemplateID.Text = "Created From Template:";
            this.ItemForCreatedFromVisitTemplateID.TextSize = new System.Drawing.Size(120, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 470);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(742, 10);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 214);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(742, 10);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.Location = new System.Drawing.Point(0, 48);
            this.emptySpaceItem13.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem13.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem13.Name = "emptySpaceItem13";
            this.emptySpaceItem13.Size = new System.Drawing.Size(742, 10);
            this.emptySpaceItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForVisitNumber
            // 
            this.ItemForVisitNumber.Control = this.VisitNumberSpinEdit;
            this.ItemForVisitNumber.CustomizationFormText = "Visit Number:";
            this.ItemForVisitNumber.Location = new System.Drawing.Point(0, 82);
            this.ItemForVisitNumber.Name = "ItemForVisitNumber";
            this.ItemForVisitNumber.Size = new System.Drawing.Size(375, 24);
            this.ItemForVisitNumber.Text = "Visit Number:";
            this.ItemForVisitNumber.TextSize = new System.Drawing.Size(120, 13);
            // 
            // ItemForFriendlyVisitNumber
            // 
            this.ItemForFriendlyVisitNumber.Control = this.FriendlyVisitNumberTextEdit;
            this.ItemForFriendlyVisitNumber.Location = new System.Drawing.Point(375, 82);
            this.ItemForFriendlyVisitNumber.Name = "ItemForFriendlyVisitNumber";
            this.ItemForFriendlyVisitNumber.Size = new System.Drawing.Size(367, 24);
            this.ItemForFriendlyVisitNumber.Text = "Friendly Visit #:";
            this.ItemForFriendlyVisitNumber.TextSize = new System.Drawing.Size(120, 13);
            // 
            // ItemForExpectedStartDate
            // 
            this.ItemForExpectedStartDate.Control = this.ExpectedStartDateDateEdit;
            this.ItemForExpectedStartDate.CustomizationFormText = "Expected Start Date:";
            this.ItemForExpectedStartDate.Location = new System.Drawing.Point(0, 140);
            this.ItemForExpectedStartDate.Name = "ItemForExpectedStartDate";
            this.ItemForExpectedStartDate.Size = new System.Drawing.Size(375, 24);
            this.ItemForExpectedStartDate.Text = "Expected Start:";
            this.ItemForExpectedStartDate.TextSize = new System.Drawing.Size(120, 13);
            // 
            // ItemForExpectedEndDate
            // 
            this.ItemForExpectedEndDate.Control = this.ExpectedEndDateDateEdit;
            this.ItemForExpectedEndDate.CustomizationFormText = "Expected End Date:";
            this.ItemForExpectedEndDate.Location = new System.Drawing.Point(375, 140);
            this.ItemForExpectedEndDate.Name = "ItemForExpectedEndDate";
            this.ItemForExpectedEndDate.Size = new System.Drawing.Size(367, 24);
            this.ItemForExpectedEndDate.Text = "Expected End:";
            this.ItemForExpectedEndDate.TextSize = new System.Drawing.Size(120, 13);
            // 
            // ItemForStartDate
            // 
            this.ItemForStartDate.Control = this.StartDateDateEdit;
            this.ItemForStartDate.CustomizationFormText = "Actual Start Date:";
            this.ItemForStartDate.Location = new System.Drawing.Point(0, 164);
            this.ItemForStartDate.Name = "ItemForStartDate";
            this.ItemForStartDate.Size = new System.Drawing.Size(375, 24);
            this.ItemForStartDate.Text = "Actual Start:";
            this.ItemForStartDate.TextSize = new System.Drawing.Size(120, 13);
            // 
            // ItemForEndDate
            // 
            this.ItemForEndDate.Control = this.EndDateDateEdit;
            this.ItemForEndDate.CustomizationFormText = "Actual End Date:";
            this.ItemForEndDate.Location = new System.Drawing.Point(375, 164);
            this.ItemForEndDate.Name = "ItemForEndDate";
            this.ItemForEndDate.Size = new System.Drawing.Size(367, 24);
            this.ItemForEndDate.Text = "Actual End:";
            this.ItemForEndDate.TextSize = new System.Drawing.Size(120, 13);
            // 
            // ItemForCalculateDaysButton
            // 
            this.ItemForCalculateDaysButton.Control = this.CalculateDaysButton;
            this.ItemForCalculateDaysButton.CustomizationFormText = "Calculate Days Button:";
            this.ItemForCalculateDaysButton.Location = new System.Drawing.Point(347, 188);
            this.ItemForCalculateDaysButton.MaxSize = new System.Drawing.Size(28, 26);
            this.ItemForCalculateDaysButton.MinSize = new System.Drawing.Size(28, 26);
            this.ItemForCalculateDaysButton.Name = "ItemForCalculateDaysButton";
            this.ItemForCalculateDaysButton.Size = new System.Drawing.Size(28, 26);
            this.ItemForCalculateDaysButton.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForCalculateDaysButton.Text = "Calculate Days Button:";
            this.ItemForCalculateDaysButton.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForCalculateDaysButton.TextVisible = false;
            // 
            // ItemForDaysSeparation
            // 
            this.ItemForDaysSeparation.Control = this.DaysSeparationSpinEdit;
            this.ItemForDaysSeparation.Location = new System.Drawing.Point(0, 188);
            this.ItemForDaysSeparation.MinSize = new System.Drawing.Size(177, 24);
            this.ItemForDaysSeparation.Name = "ItemForDaysSeparation";
            this.ItemForDaysSeparation.Size = new System.Drawing.Size(347, 26);
            this.ItemForDaysSeparation.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForDaysSeparation.Text = "Days Between Visits:";
            this.ItemForDaysSeparation.TextSize = new System.Drawing.Size(120, 13);
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.Location = new System.Drawing.Point(0, 130);
            this.emptySpaceItem10.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem10.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(742, 10);
            this.emptySpaceItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.AllowHtmlStringInCaption = true;
            this.layoutControlGroup9.ExpandButtonVisible = true;
            this.layoutControlGroup9.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForNoOnetoSign,
            this.ItemForManagerName,
            this.ItemForClientSignaturePath,
            this.ItemForManagerNotes});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 607);
            this.layoutControlGroup9.Name = "layoutControlGroup9";
            this.layoutControlGroup9.Size = new System.Drawing.Size(742, 144);
            this.layoutControlGroup9.Text = "Client Signature";
            // 
            // ItemForNoOnetoSign
            // 
            this.ItemForNoOnetoSign.Control = this.NoOnetoSignCheckEdit;
            this.ItemForNoOnetoSign.Location = new System.Drawing.Point(0, 0);
            this.ItemForNoOnetoSign.MaxSize = new System.Drawing.Size(208, 23);
            this.ItemForNoOnetoSign.MinSize = new System.Drawing.Size(208, 23);
            this.ItemForNoOnetoSign.Name = "ItemForNoOnetoSign";
            this.ItemForNoOnetoSign.Size = new System.Drawing.Size(208, 24);
            this.ItemForNoOnetoSign.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForNoOnetoSign.Text = "No One To Sign:";
            this.ItemForNoOnetoSign.TextSize = new System.Drawing.Size(120, 13);
            // 
            // ItemForManagerName
            // 
            this.ItemForManagerName.Control = this.ManagerNameTextEdit;
            this.ItemForManagerName.Location = new System.Drawing.Point(208, 0);
            this.ItemForManagerName.Name = "ItemForManagerName";
            this.ItemForManagerName.Size = new System.Drawing.Size(510, 24);
            this.ItemForManagerName.Text = "Signatory Name:";
            this.ItemForManagerName.TextSize = new System.Drawing.Size(120, 13);
            // 
            // ItemForClientSignaturePath
            // 
            this.ItemForClientSignaturePath.Control = this.ClientSignaturePathButtonEdit;
            this.ItemForClientSignaturePath.CustomizationFormText = "Client Signature:";
            this.ItemForClientSignaturePath.Location = new System.Drawing.Point(0, 72);
            this.ItemForClientSignaturePath.Name = "ItemForClientSignaturePath";
            this.ItemForClientSignaturePath.Size = new System.Drawing.Size(718, 26);
            this.ItemForClientSignaturePath.Text = "Client Signature:";
            this.ItemForClientSignaturePath.TextSize = new System.Drawing.Size(120, 13);
            // 
            // ItemForManagerNotes
            // 
            this.ItemForManagerNotes.Control = this.ManagerNotesMemoEdit;
            this.ItemForManagerNotes.Location = new System.Drawing.Point(0, 24);
            this.ItemForManagerNotes.MaxSize = new System.Drawing.Size(0, 48);
            this.ItemForManagerNotes.MinSize = new System.Drawing.Size(137, 48);
            this.ItemForManagerNotes.Name = "ItemForManagerNotes";
            this.ItemForManagerNotes.Size = new System.Drawing.Size(718, 48);
            this.ItemForManagerNotes.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForManagerNotes.Text = "Signatory Notes:";
            this.ItemForManagerNotes.TextSize = new System.Drawing.Size(120, 13);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.Location = new System.Drawing.Point(0, 597);
            this.emptySpaceItem8.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem8.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(742, 10);
            this.emptySpaceItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem15
            // 
            this.emptySpaceItem15.AllowHotTrack = false;
            this.emptySpaceItem15.Location = new System.Drawing.Point(0, 751);
            this.emptySpaceItem15.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem15.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem15.Name = "emptySpaceItem15";
            this.emptySpaceItem15.Size = new System.Drawing.Size(742, 10);
            this.emptySpaceItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem15.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem16
            // 
            this.emptySpaceItem16.AllowHotTrack = false;
            this.emptySpaceItem16.Location = new System.Drawing.Point(0, 785);
            this.emptySpaceItem16.Name = "emptySpaceItem16";
            this.emptySpaceItem16.Size = new System.Drawing.Size(742, 10);
            this.emptySpaceItem16.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem17
            // 
            this.emptySpaceItem17.AllowHotTrack = false;
            this.emptySpaceItem17.Location = new System.Drawing.Point(375, 188);
            this.emptySpaceItem17.Name = "emptySpaceItem17";
            this.emptySpaceItem17.Size = new System.Drawing.Size(367, 26);
            this.emptySpaceItem17.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup15
            // 
            this.layoutControlGroup15.AllowHtmlStringInCaption = true;
            this.layoutControlGroup15.ExpandButtonVisible = true;
            this.layoutControlGroup15.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup15.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForStartLatitude,
            this.ItemforFinishLatitude,
            this.ItemForStartLongitude,
            this.ItemForFinishLongitude});
            this.layoutControlGroup15.Location = new System.Drawing.Point(0, 224);
            this.layoutControlGroup15.Name = "layoutControlGroup15";
            this.layoutControlGroup15.Size = new System.Drawing.Size(742, 94);
            this.layoutControlGroup15.Text = "Location";
            // 
            // ItemForStartLatitude
            // 
            this.ItemForStartLatitude.Control = this.StartLatitudeTextEdit;
            this.ItemForStartLatitude.CustomizationFormText = "Start Latitude:";
            this.ItemForStartLatitude.Location = new System.Drawing.Point(0, 0);
            this.ItemForStartLatitude.Name = "ItemForStartLatitude";
            this.ItemForStartLatitude.Size = new System.Drawing.Size(363, 24);
            this.ItemForStartLatitude.Text = "Start Latitude:";
            this.ItemForStartLatitude.TextSize = new System.Drawing.Size(120, 13);
            // 
            // ItemforFinishLatitude
            // 
            this.ItemforFinishLatitude.Control = this.FinishLatitudeTextEdit;
            this.ItemforFinishLatitude.CustomizationFormText = "Finish Latitude:";
            this.ItemforFinishLatitude.Location = new System.Drawing.Point(0, 24);
            this.ItemforFinishLatitude.Name = "ItemforFinishLatitude";
            this.ItemforFinishLatitude.Size = new System.Drawing.Size(363, 24);
            this.ItemforFinishLatitude.Text = "Finish Latitude:";
            this.ItemforFinishLatitude.TextSize = new System.Drawing.Size(120, 13);
            // 
            // ItemForStartLongitude
            // 
            this.ItemForStartLongitude.Control = this.StartLongitudeTextEdit;
            this.ItemForStartLongitude.CustomizationFormText = "Start Longitude:";
            this.ItemForStartLongitude.Location = new System.Drawing.Point(363, 0);
            this.ItemForStartLongitude.Name = "ItemForStartLongitude";
            this.ItemForStartLongitude.Size = new System.Drawing.Size(355, 24);
            this.ItemForStartLongitude.Text = "Start Longitude:";
            this.ItemForStartLongitude.TextSize = new System.Drawing.Size(120, 13);
            // 
            // ItemForFinishLongitude
            // 
            this.ItemForFinishLongitude.Control = this.FinishLongitudeTextEdit;
            this.ItemForFinishLongitude.CustomizationFormText = "Finish Longitude:";
            this.ItemForFinishLongitude.Location = new System.Drawing.Point(363, 24);
            this.ItemForFinishLongitude.Name = "ItemForFinishLongitude";
            this.ItemForFinishLongitude.Size = new System.Drawing.Size(355, 24);
            this.ItemForFinishLongitude.Text = "Finish Longitude:";
            this.ItemForFinishLongitude.TextSize = new System.Drawing.Size(120, 13);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.Location = new System.Drawing.Point(0, 318);
            this.emptySpaceItem12.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem12.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Size = new System.Drawing.Size(742, 10);
            this.emptySpaceItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(772, 475);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(574, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.Location = new System.Drawing.Point(766, 23);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(6, 841);
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp06145_OM_Visit_EditTableAdapter
            // 
            this.sp06145_OM_Visit_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp06134_OM_Job_Calculation_Level_DescriptorsTableAdapter
            // 
            this.sp06134_OM_Job_Calculation_Level_DescriptorsTableAdapter.ClearBeforeFill = true;
            // 
            // sp06250_OM_Visit_Categories_With_BlankTableAdapter
            // 
            this.sp06250_OM_Visit_Categories_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp06251_OM_Issue_Types_With_BlankTableAdapter
            // 
            this.sp06251_OM_Issue_Types_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp06267_OM_Extra_Work_Types_With_BlankTableAdapter
            // 
            this.sp06267_OM_Extra_Work_Types_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp06308_OM_Visit_TypesTableAdapter
            // 
            this.sp06308_OM_Visit_TypesTableAdapter.ClearBeforeFill = true;
            // 
            // sp06141_OM_Visit_Template_Headers_With_BlankTableAdapter
            // 
            this.sp06141_OM_Visit_Template_Headers_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp06430_OM_Visit_Status_Issues_With_BlankTableAdapter
            // 
            this.sp06430_OM_Visit_Status_Issues_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // frm_OM_Visit_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1383, 712);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_OM_Visit_Edit";
            this.Text = "Edit Visit";
            this.Activated += new System.EventHandler(this.frm_OM_Visit_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_OM_Visit_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_OM_Visit_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SelfBillingInvoiceNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06145OMVisitEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Visit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExtraCostsFinalisedCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitEquipmentSellSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitMaterialSellSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitLabourSellSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitEquipmentCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitMaterialCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitLabourCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DoNotInvoiceClientCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DoNotPayContractorCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteContractDaysSeparationPercentTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DaysSeparationSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusIssueIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06430OMVisitStatusIssuesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CompletionSheetNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedFromVisitTemplateIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06141OMVisitTemplateHeadersWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitStatusIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusDescriptionButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPOIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ManagerNotesMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ManagerNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoOnetoSignCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientInvoiceIDButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelfBillingInvoiceButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellCalculationLevelGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06134OMJobCalculationLevelDescriptorsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPONumberButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitTypeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06308OMVisitTypesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FriendlyVisitNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentCommentMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentOnSiteCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExtraWorkCommentsMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExtraWorkTypeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06267OMExtraWorkTypesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExtraWorkRequiredCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReworkCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimelinessTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImagesFolderOMTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientSignaturePathButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitCategoryIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06250OMVisitCategoriesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IssueRemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IssueTypeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06251OMIssueTypesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IssueFoundCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FinishLongitudeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FinishLatitudeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartLongitudeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartLatitudeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitSellSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostCalculationLevelGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpectedEndDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpectedEndDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpectedStartDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpectedStartDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByStaffNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitNumberSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByStaffIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientContractIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteContractIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToParentButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientContractID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteContractID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByStaffID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForImagesFolderOM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPOID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteContractDaysSeparationPercent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExtraWorkRequired)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExtraWorkComments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExtraWorkTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPONumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCompletionSheetNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupTeamCosts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostCalculationLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitCostButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSelfBillingInvoice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDoNotPayContractor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitLabourCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitMaterialCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitEquipmentCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSelfBillingInvoiceNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupClientSell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellCalculationLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDoNotInvoiceClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitSell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitSellButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientInvoiceID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitLabourSell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitMaterialSell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitEquipmentSell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExtraCostsFinalised)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToParent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitCategoryID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTimeliness)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIssueRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRework)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusIssueID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIssueTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccidentOnSite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccidentComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByStaffName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedFromVisitTemplateID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFriendlyVisitNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpectedStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpectedEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCalculateDaysButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDaysSeparation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNoOnetoSign)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForManagerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientSignaturePath)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForManagerNotes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartLatitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemforFinishLatitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartLongitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFinishLongitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.ButtonEdit LinkedToParentButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitID;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraEditors.TextEdit VisitIDTextEdit;
        private DevExpress.XtraEditors.TextEdit SiteContractIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteContractID;
        private DevExpress.XtraEditors.DateEdit StartDateDateEdit;
        private DevExpress.XtraEditors.DateEdit EndDateDateEdit;
        private DevExpress.XtraEditors.SpinEdit VisitCostSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitCost;
        private DevExpress.XtraEditors.TextEdit ClientContractIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientContractID;
        private System.Windows.Forms.BindingSource sp06145OMVisitEditBindingSource;
        private DataSet_OM_Visit dataSet_OM_Visit;
        private DataSet_OM_VisitTableAdapters.sp06145_OM_Visit_EditTableAdapter sp06145_OM_Visit_EditTableAdapter;
        private DevExpress.XtraEditors.TextEdit CreatedByStaffIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCreatedByStaffID;
        private DevExpress.XtraEditors.SpinEdit VisitNumberSpinEdit;
        private DevExpress.XtraEditors.TextEdit CreatedByStaffNameTextEdit;
        private DevExpress.XtraEditors.DateEdit ExpectedEndDateDateEdit;
        private DevExpress.XtraEditors.DateEdit ExpectedStartDateDateEdit;
        private DevExpress.XtraEditors.GridLookUpEdit CostCalculationLevelGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostCalculationLevel;
        private System.Windows.Forms.BindingSource sp06134OMJobCalculationLevelDescriptorsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DataSet_OM_VisitTableAdapters.sp06134_OM_Job_Calculation_Level_DescriptorsTableAdapter sp06134_OM_Job_Calculation_Level_DescriptorsTableAdapter;
        private DevExpress.XtraEditors.SpinEdit VisitSellSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitSell;
        private DevExpress.XtraEditors.TextEdit StartLatitudeTextEdit;
        private DevExpress.XtraEditors.TextEdit StartLongitudeTextEdit;
        private DevExpress.XtraEditors.TextEdit FinishLongitudeTextEdit;
        private DevExpress.XtraEditors.TextEdit FinishLatitudeTextEdit;
        private DevExpress.XtraEditors.TextEdit WorkNumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWorkNumber;
        private DevExpress.XtraEditors.CheckEdit IssueFoundCheckEdit;
        private DevExpress.XtraEditors.GridLookUpEdit IssueTypeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.MemoEdit IssueRemarksMemoEdit;
        private DevExpress.XtraEditors.GridLookUpEdit VisitCategoryIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private System.Windows.Forms.BindingSource sp06250OMVisitCategoriesWithBlankBindingSource;
        private System.Windows.Forms.BindingSource sp06251OMIssueTypesWithBlankBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DataSet_OM_VisitTableAdapters.sp06250_OM_Visit_Categories_With_BlankTableAdapter sp06250_OM_Visit_Categories_With_BlankTableAdapter;
        private DataSet_OM_VisitTableAdapters.sp06251_OM_Issue_Types_With_BlankTableAdapter sp06251_OM_Issue_Types_With_BlankTableAdapter;
        private DevExpress.XtraEditors.ButtonEdit ClientSignaturePathButtonEdit;
        private DevExpress.XtraEditors.TextEdit ImagesFolderOMTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForImagesFolderOM;
        private DevExpress.XtraEditors.TextEdit TimelinessTextEdit;
        private DevExpress.XtraEditors.CheckEdit ReworkCheckEdit;
        private DevExpress.XtraEditors.GridLookUpEdit ExtraWorkTypeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraEditors.CheckEdit ExtraWorkRequiredCheckEdit;
        private DevExpress.XtraEditors.MemoEdit ExtraWorkCommentsMemoEdit;
        private System.Windows.Forms.BindingSource sp06267OMExtraWorkTypesWithBlankBindingSource;
        private DataSet_OM_VisitTableAdapters.sp06267_OM_Extra_Work_Types_With_BlankTableAdapter sp06267_OM_Extra_Work_Types_With_BlankTableAdapter;
        private DevExpress.XtraEditors.MemoEdit AccidentCommentMemoEdit;
        private DevExpress.XtraEditors.CheckEdit AccidentOnSiteCheckEdit;
        private DevExpress.XtraEditors.TextEdit FriendlyVisitNumberTextEdit;
        private DevExpress.XtraEditors.GridLookUpEdit VisitTypeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colActive;
        private System.Windows.Forms.BindingSource sp06308OMVisitTypesBindingSource;
        private DataSet_OM_VisitTableAdapters.sp06308_OM_Visit_TypesTableAdapter sp06308_OM_Visit_TypesTableAdapter;
        private DevExpress.XtraEditors.TextEdit ClientPOIDTextEdit;
        private DevExpress.XtraEditors.ButtonEdit ClientPONumberButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientPOID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientPONumber;
        private DevExpress.XtraEditors.GridLookUpEdit SellCalculationLevelGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSellCalculationLevel;
        private DevExpress.XtraEditors.SimpleButton CalculateCostButton;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitCostButton;
        private DevExpress.XtraEditors.SimpleButton CalculateSellButton;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitSellButton;
        private DevExpress.XtraEditors.ButtonEdit ClientInvoiceIDButtonEdit;
        private DevExpress.XtraEditors.ButtonEdit SelfBillingInvoiceButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSelfBillingInvoice;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientInvoiceID;
        private DevExpress.XtraBars.BarButtonItem bbiViewOnMap;
        private DevExpress.XtraEditors.TextEdit SiteIDTextEdit;
        private DevExpress.XtraEditors.TextEdit ClientIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientID;
        private DevExpress.XtraEditors.CheckEdit NoOnetoSignCheckEdit;
        private DevExpress.XtraEditors.TextEdit ManagerNameTextEdit;
        private DevExpress.XtraEditors.MemoEdit ManagerNotesMemoEdit;
        private DevExpress.XtraEditors.TextEdit VisitStatusIDTextEdit;
        private DevExpress.XtraEditors.ButtonEdit StatusDescriptionButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitStatusID;
        private DevExpress.XtraEditors.GridLookUpEdit CreatedFromVisitTemplateIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private System.Windows.Forms.BindingSource sp06141OMVisitTemplateHeadersWithBlankBindingSource;
        private DataSet_OM_VisitTableAdapters.sp06141_OM_Visit_Template_Headers_With_BlankTableAdapter sp06141_OM_Visit_Template_Headers_With_BlankTableAdapter;
        private DevExpress.XtraEditors.TextEdit CompletionSheetNumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCompletionSheetNumber;
        private DevExpress.XtraEditors.GridLookUpEdit StatusIssueIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private System.Windows.Forms.BindingSource sp06430OMVisitStatusIssuesWithBlankBindingSource;
        private DataSet_OM_VisitTableAdapters.sp06430_OM_Visit_Status_Issues_With_BlankTableAdapter sp06430_OM_Visit_Status_Issues_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraEditors.SpinEdit DaysSeparationSpinEdit;
        private DevExpress.XtraEditors.TextEdit SiteContractDaysSeparationPercentTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteContractDaysSeparationPercent;
        private DevExpress.XtraEditors.SimpleButton CalculateDaysButton;
        private DevExpress.XtraEditors.CheckEdit DoNotInvoiceClientCheckEdit;
        private DevExpress.XtraEditors.CheckEdit DoNotPayContractorCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDoNotPayContractor;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDoNotInvoiceClient;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupTeamCosts;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupClientSell;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem20;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup17;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem24;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem19;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExtraWorkRequired;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExtraWorkComments;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExtraWorkTypeID;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedToParent;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteID;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem23;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitTypeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitCategoryID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTimeliness;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStatusDescription;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIssueRemarks;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRework;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStatusIssueID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIssueTypeID;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup16;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAccidentOnSite;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAccidentComment;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCreatedByStaffName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCreatedFromVisitTemplateID;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFriendlyVisitNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExpectedStartDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExpectedEndDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStartDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEndDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCalculateDaysButton;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDaysSeparation;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNoOnetoSign;
        private DevExpress.XtraLayout.LayoutControlItem ItemForManagerName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientSignaturePath;
        private DevExpress.XtraLayout.LayoutControlItem ItemForManagerNotes;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem15;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem16;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem17;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup15;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStartLatitude;
        private DevExpress.XtraLayout.LayoutControlItem ItemforFinishLatitude;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStartLongitude;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFinishLongitude;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private DevExpress.XtraEditors.SpinEdit VisitEquipmentSellSpinEdit;
        private DevExpress.XtraEditors.SpinEdit VisitMaterialSellSpinEdit;
        private DevExpress.XtraEditors.SpinEdit VisitLabourSellSpinEdit;
        private DevExpress.XtraEditors.SpinEdit VisitEquipmentCostSpinEdit;
        private DevExpress.XtraEditors.SpinEdit VisitMaterialCostSpinEdit;
        private DevExpress.XtraEditors.SpinEdit VisitLabourCostSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitLabourCost;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitMaterialCost;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitEquipmentCost;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitLabourSell;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitMaterialSell;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitEquipmentSell;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
        private DevExpress.XtraEditors.CheckEdit ExtraCostsFinalisedCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExtraCostsFinalised;
        private DevExpress.XtraEditors.TextEdit SelfBillingInvoiceNumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSelfBillingInvoiceNumber;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem18;
    }
}

﻿namespace WoodPlan5
{
    partial class frm_OM_Visit_Revisit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Visit_Revisit));
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.dataSet_OM_Visit = new WoodPlan5.DataSet_OM_Visit();
            this.dataSet_OM_Contract = new WoodPlan5.DataSet_OM_Contract();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.gridControlJob = new DevExpress.XtraGrid.GridControl();
            this.sp06129OMJobsLinkedtoVisitsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewJob = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colJobID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditInteger = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colJobStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReactive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colJobNoLongerRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRequiresAccessPermit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccessPermitID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPOID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinanceSystemPONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedDurationUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedDurationUnitsDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScheduleSentDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualDurationUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualDurationUnitsDescriptionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalMaterialExVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditMoney2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCostTotalMaterialVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCostTotalMaterialCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalMaterialExVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalMaterialVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalMaterialCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalEquipmentExVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalEquipmentVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalEquipmentCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalEquipmentExVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalEquipmentVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalEquipmentCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalLabourExVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalLabourVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalLabourCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalLabourExVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalLabourVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalLabourCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalCostExVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalCostVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalCostExVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalCostVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientInvoiceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateClientInvoiced = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingInvoiceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingInvoiceReceivedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingInvoicePaidDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingInvoiceAmountPaid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotPayContractor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotInvoiceClient = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colRouteOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditLatLong2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobStatusDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedDurationUnitsDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualDurationUnitsDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelected = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFullDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTenderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTenderJobID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinimumDaysFromLastVisit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaximumDaysFromLastVisit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDaysLeeway = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingCentreCodeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalEquipmentVATRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalLabourVATRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalMaterialVATRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalMaterialVATRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalEquipmentVATRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalLabourVATRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSitePostcode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToVisit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientAndContract = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReworkOriginalJobID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRework = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sp06129_OM_Jobs_Linked_to_VisitsTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06129_OM_Jobs_Linked_to_VisitsTableAdapter();
            this.dateEditJobStartDate = new DevExpress.XtraEditors.DateEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.StatusGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06405OMVisitReVisitAvailableStatusesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditJobEndDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.sp06405_OM_Visit_ReVisit_Available_StatusesTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06405_OM_Visit_ReVisit_Available_StatusesTableAdapter();
            this.memoEditRemarks = new DevExpress.XtraEditors.MemoEdit();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Visit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlJob)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06129OMJobsLinkedtoVisitsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewJob)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMoney2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditLatLong2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditJobStartDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditJobStartDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StatusGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06405OMVisitReVisitAvailableStatusesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditJobEndDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditJobEndDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditRemarks.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(801, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 616);
            this.barDockControlBottom.Size = new System.Drawing.Size(801, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 590);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(801, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 590);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // dataSet_OM_Visit
            // 
            this.dataSet_OM_Visit.DataSetName = "DataSet_OM_Visit";
            this.dataSet_OM_Visit.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_OM_Contract
            // 
            this.dataSet_OM_Contract.DataSetName = "DataSet_OM_Contract";
            this.dataSet_OM_Contract.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(626, 586);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(80, 22);
            this.btnOK.TabIndex = 13;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(714, 586);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 22);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.pictureEdit1);
            this.panelControl1.Location = new System.Drawing.Point(7, 33);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(787, 37);
            this.panelControl1.TabIndex = 29;
            // 
            // labelControl3
            // 
            this.labelControl3.AllowHtmlString = true;
            this.labelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl3.Location = new System.Drawing.Point(43, 5);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(738, 26);
            this.labelControl3.TabIndex = 31;
            this.labelControl3.Text = resources.GetString("labelControl3.Text");
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.EditValue = ((object)(resources.GetObject("pictureEdit1.EditValue")));
            this.pictureEdit1.Location = new System.Drawing.Point(5, 2);
            this.pictureEdit1.MenuManager = this.barManager1;
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.ShowMenu = false;
            this.pictureEdit1.Size = new System.Drawing.Size(32, 32);
            this.pictureEdit1.TabIndex = 30;
            // 
            // gridControlJob
            // 
            this.gridControlJob.DataSource = this.sp06129OMJobsLinkedtoVisitsBindingSource;
            this.gridControlJob.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlJob.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlJob.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlJob.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlJob.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlJob.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlJob.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlJob.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlJob.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlJob.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlJob.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlJob.Location = new System.Drawing.Point(0, 0);
            this.gridControlJob.MainView = this.gridViewJob;
            this.gridControlJob.MenuManager = this.barManager1;
            this.gridControlJob.Name = "gridControlJob";
            this.gridControlJob.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2,
            this.repositoryItemCheckEdit2,
            this.repositoryItemTextEditDateTime2,
            this.repositoryItemTextEditMoney2,
            this.repositoryItemTextEditPercentage2,
            this.repositoryItemTextEditLatLong2,
            this.repositoryItemTextEditInteger});
            this.gridControlJob.Size = new System.Drawing.Size(787, 364);
            this.gridControlJob.TabIndex = 30;
            this.gridControlJob.UseEmbeddedNavigator = true;
            this.gridControlJob.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewJob});
            // 
            // sp06129OMJobsLinkedtoVisitsBindingSource
            // 
            this.sp06129OMJobsLinkedtoVisitsBindingSource.DataMember = "sp06129_OM_Jobs_Linked_to_Visits";
            this.sp06129OMJobsLinkedtoVisitsBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // gridViewJob
            // 
            this.gridViewJob.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colJobID,
            this.gridColumn1,
            this.gridColumn2,
            this.colJobStatusID,
            this.colJobSubTypeID,
            this.colCreatedByStaffID1,
            this.colReactive,
            this.colJobNoLongerRequired,
            this.colRequiresAccessPermit,
            this.colAccessPermitID,
            this.colClientPONumber,
            this.colClientPOID,
            this.colFinanceSystemPONumber,
            this.gridColumn3,
            this.gridColumn4,
            this.colExpectedDurationUnits,
            this.colExpectedDurationUnitsDescriptorID,
            this.colScheduleSentDate,
            this.colActualStartDate,
            this.colActualEndDate,
            this.colActualDurationUnits,
            this.colActualDurationUnitsDescriptionID,
            this.colCostTotalMaterialExVAT,
            this.colCostTotalMaterialVAT,
            this.colCostTotalMaterialCost,
            this.colSellTotalMaterialExVAT,
            this.colSellTotalMaterialVAT,
            this.colSellTotalMaterialCost,
            this.colCostTotalEquipmentExVAT,
            this.colCostTotalEquipmentVAT,
            this.colCostTotalEquipmentCost,
            this.colSellTotalEquipmentExVAT,
            this.colSellTotalEquipmentVAT,
            this.colSellTotalEquipmentCost,
            this.colCostTotalLabourExVAT,
            this.colCostTotalLabourVAT,
            this.colCostTotalLabourCost,
            this.colSellTotalLabourExVAT,
            this.colSellTotalLabourVAT,
            this.colSellTotalLabourCost,
            this.colCostTotalCostExVAT,
            this.colCostTotalCostVAT,
            this.colCostTotalCost,
            this.colSellTotalCostExVAT,
            this.colSellTotalCostVAT,
            this.colSellTotalCost,
            this.colClientInvoiceID,
            this.colDateClientInvoiced,
            this.colSelfBillingInvoiceID,
            this.colSelfBillingInvoiceReceivedDate,
            this.colSelfBillingInvoicePaidDate,
            this.colSelfBillingInvoiceAmountPaid,
            this.colDoNotPayContractor,
            this.colDoNotInvoiceClient,
            this.colRemarks2,
            this.colRouteOrder,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.colJobStatusDescription,
            this.gridColumn9,
            this.gridColumn10,
            this.colCreatedByStaffName1,
            this.colExpectedDurationUnitsDescriptor,
            this.colActualDurationUnitsDescription,
            this.colJobTypeID1,
            this.colJobSubTypeDescription,
            this.colJobTypeDescription1,
            this.gridColumn11,
            this.colRecordType,
            this.colSelected,
            this.colFullDescription1,
            this.colTenderID,
            this.colTenderJobID,
            this.colMinimumDaysFromLastVisit,
            this.colMaximumDaysFromLastVisit,
            this.colDaysLeeway,
            this.colBillingCentreCodeID,
            this.gridColumn12,
            this.gridColumn13,
            this.colCostTotalEquipmentVATRate,
            this.colCostTotalLabourVATRate,
            this.colCostTotalMaterialVATRate,
            this.colSellTotalMaterialVATRate,
            this.colSellTotalEquipmentVATRate,
            this.colSellTotalLabourVATRate,
            this.gridColumn14,
            this.colClientID2,
            this.gridColumn15,
            this.colSiteID2,
            this.colLocationX,
            this.colLocationY,
            this.colSitePostcode1,
            this.colLinkedToVisit,
            this.colClientAndContract,
            this.colReworkOriginalJobID,
            this.colRework});
            this.gridViewJob.GridControl = this.gridControlJob;
            this.gridViewJob.GroupCount = 3;
            this.gridViewJob.Name = "gridViewJob";
            this.gridViewJob.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewJob.OptionsFind.AlwaysVisible = true;
            this.gridViewJob.OptionsFind.FindDelay = 2000;
            this.gridViewJob.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewJob.OptionsLayout.StoreAppearance = true;
            this.gridViewJob.OptionsLayout.StoreFormatRules = true;
            this.gridViewJob.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewJob.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewJob.OptionsSelection.MultiSelect = true;
            this.gridViewJob.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewJob.OptionsView.ColumnAutoWidth = false;
            this.gridViewJob.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewJob.OptionsView.ShowGroupPanel = false;
            this.gridViewJob.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientAndContract, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn9, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridViewJob.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridViewJob_PopupMenuShowing);
            this.gridViewJob.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridViewJob_SelectionChanged);
            this.gridViewJob.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridViewJob_CustomDrawEmptyForeground);
            this.gridViewJob.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridViewJob_CustomFilterDialog);
            this.gridViewJob.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridViewJob_FilterEditorCreated);
            this.gridViewJob.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridViewJob_MouseDown);
            this.gridViewJob.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewJob_MouseUp);
            this.gridViewJob.DoubleClick += new System.EventHandler(this.gridViewJob_DoubleClick);
            this.gridViewJob.GotFocus += new System.EventHandler(this.gridViewJob_GotFocus);
            // 
            // colJobID
            // 
            this.colJobID.Caption = "Job ID";
            this.colJobID.FieldName = "JobID";
            this.colJobID.Name = "colJobID";
            this.colJobID.OptionsColumn.AllowEdit = false;
            this.colJobID.OptionsColumn.AllowFocus = false;
            this.colJobID.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Visit ID";
            this.gridColumn1.FieldName = "VisitID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Width = 54;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Visit #";
            this.gridColumn2.ColumnEdit = this.repositoryItemTextEditInteger;
            this.gridColumn2.FieldName = "VisitNumber";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 12;
            this.gridColumn2.Width = 51;
            // 
            // repositoryItemTextEditInteger
            // 
            this.repositoryItemTextEditInteger.AutoHeight = false;
            this.repositoryItemTextEditInteger.Mask.EditMask = "n0";
            this.repositoryItemTextEditInteger.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditInteger.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditInteger.Name = "repositoryItemTextEditInteger";
            // 
            // colJobStatusID
            // 
            this.colJobStatusID.Caption = "Job Status ID";
            this.colJobStatusID.FieldName = "JobStatusID";
            this.colJobStatusID.Name = "colJobStatusID";
            this.colJobStatusID.OptionsColumn.AllowEdit = false;
            this.colJobStatusID.OptionsColumn.AllowFocus = false;
            this.colJobStatusID.OptionsColumn.ReadOnly = true;
            this.colJobStatusID.Width = 86;
            // 
            // colJobSubTypeID
            // 
            this.colJobSubTypeID.Caption = "Job Sub-Type ID";
            this.colJobSubTypeID.FieldName = "JobSubTypeID";
            this.colJobSubTypeID.Name = "colJobSubTypeID";
            this.colJobSubTypeID.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeID.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeID.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeID.Width = 102;
            // 
            // colCreatedByStaffID1
            // 
            this.colCreatedByStaffID1.Caption = "Created By Staff ID";
            this.colCreatedByStaffID1.FieldName = "CreatedByStaffID";
            this.colCreatedByStaffID1.Name = "colCreatedByStaffID1";
            this.colCreatedByStaffID1.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffID1.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffID1.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffID1.Width = 116;
            // 
            // colReactive
            // 
            this.colReactive.Caption = "Reactive";
            this.colReactive.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colReactive.FieldName = "Reactive";
            this.colReactive.Name = "colReactive";
            this.colReactive.OptionsColumn.AllowEdit = false;
            this.colReactive.OptionsColumn.AllowFocus = false;
            this.colReactive.OptionsColumn.ReadOnly = true;
            this.colReactive.Visible = true;
            this.colReactive.VisibleIndex = 6;
            this.colReactive.Width = 63;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // colJobNoLongerRequired
            // 
            this.colJobNoLongerRequired.Caption = "No Longer Required";
            this.colJobNoLongerRequired.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colJobNoLongerRequired.FieldName = "JobNoLongerRequired";
            this.colJobNoLongerRequired.Name = "colJobNoLongerRequired";
            this.colJobNoLongerRequired.OptionsColumn.AllowEdit = false;
            this.colJobNoLongerRequired.OptionsColumn.AllowFocus = false;
            this.colJobNoLongerRequired.OptionsColumn.ReadOnly = true;
            this.colJobNoLongerRequired.Visible = true;
            this.colJobNoLongerRequired.VisibleIndex = 14;
            this.colJobNoLongerRequired.Width = 116;
            // 
            // colRequiresAccessPermit
            // 
            this.colRequiresAccessPermit.Caption = "Access Permit Required";
            this.colRequiresAccessPermit.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colRequiresAccessPermit.FieldName = "RequiresAccessPermit";
            this.colRequiresAccessPermit.Name = "colRequiresAccessPermit";
            this.colRequiresAccessPermit.OptionsColumn.AllowEdit = false;
            this.colRequiresAccessPermit.OptionsColumn.AllowFocus = false;
            this.colRequiresAccessPermit.OptionsColumn.ReadOnly = true;
            this.colRequiresAccessPermit.Visible = true;
            this.colRequiresAccessPermit.VisibleIndex = 7;
            this.colRequiresAccessPermit.Width = 133;
            // 
            // colAccessPermitID
            // 
            this.colAccessPermitID.Caption = "Access Permit ID";
            this.colAccessPermitID.FieldName = "AccessPermitID";
            this.colAccessPermitID.Name = "colAccessPermitID";
            this.colAccessPermitID.OptionsColumn.AllowEdit = false;
            this.colAccessPermitID.OptionsColumn.AllowFocus = false;
            this.colAccessPermitID.OptionsColumn.ReadOnly = true;
            this.colAccessPermitID.Width = 101;
            // 
            // colClientPONumber
            // 
            this.colClientPONumber.Caption = "Client PO #";
            this.colClientPONumber.FieldName = "ClientPONumber";
            this.colClientPONumber.Name = "colClientPONumber";
            this.colClientPONumber.OptionsColumn.AllowEdit = false;
            this.colClientPONumber.OptionsColumn.AllowFocus = false;
            this.colClientPONumber.OptionsColumn.ReadOnly = true;
            this.colClientPONumber.Width = 76;
            // 
            // colClientPOID
            // 
            this.colClientPOID.Caption = "Client PO ID";
            this.colClientPOID.FieldName = "ClientPOID";
            this.colClientPOID.Name = "colClientPOID";
            this.colClientPOID.OptionsColumn.AllowEdit = false;
            this.colClientPOID.OptionsColumn.AllowFocus = false;
            this.colClientPOID.OptionsColumn.ReadOnly = true;
            this.colClientPOID.Width = 79;
            // 
            // colFinanceSystemPONumber
            // 
            this.colFinanceSystemPONumber.Caption = "Finance System PO #";
            this.colFinanceSystemPONumber.FieldName = "FinanceSystemPONumber";
            this.colFinanceSystemPONumber.Name = "colFinanceSystemPONumber";
            this.colFinanceSystemPONumber.OptionsColumn.AllowEdit = false;
            this.colFinanceSystemPONumber.OptionsColumn.AllowFocus = false;
            this.colFinanceSystemPONumber.OptionsColumn.ReadOnly = true;
            this.colFinanceSystemPONumber.Width = 124;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Expected Start";
            this.gridColumn3.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.gridColumn3.FieldName = "ExpectedStartDate";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 3;
            this.gridColumn3.Width = 106;
            // 
            // repositoryItemTextEditDateTime2
            // 
            this.repositoryItemTextEditDateTime2.AutoHeight = false;
            this.repositoryItemTextEditDateTime2.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime2.Name = "repositoryItemTextEditDateTime2";
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Expected End";
            this.gridColumn4.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.gridColumn4.FieldName = "ExpectedEndDate";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 4;
            this.gridColumn4.Width = 100;
            // 
            // colExpectedDurationUnits
            // 
            this.colExpectedDurationUnits.Caption = "Expected Duration";
            this.colExpectedDurationUnits.FieldName = "ExpectedDurationUnits";
            this.colExpectedDurationUnits.Name = "colExpectedDurationUnits";
            this.colExpectedDurationUnits.OptionsColumn.AllowEdit = false;
            this.colExpectedDurationUnits.OptionsColumn.AllowFocus = false;
            this.colExpectedDurationUnits.OptionsColumn.ReadOnly = true;
            this.colExpectedDurationUnits.Visible = true;
            this.colExpectedDurationUnits.VisibleIndex = 5;
            this.colExpectedDurationUnits.Width = 110;
            // 
            // colExpectedDurationUnitsDescriptorID
            // 
            this.colExpectedDurationUnitsDescriptorID.Caption = "Expected Duration Unit Descriptor ID";
            this.colExpectedDurationUnitsDescriptorID.FieldName = "ExpectedDurationUnitsDescriptorID";
            this.colExpectedDurationUnitsDescriptorID.Name = "colExpectedDurationUnitsDescriptorID";
            this.colExpectedDurationUnitsDescriptorID.OptionsColumn.AllowEdit = false;
            this.colExpectedDurationUnitsDescriptorID.OptionsColumn.AllowFocus = false;
            this.colExpectedDurationUnitsDescriptorID.OptionsColumn.ReadOnly = true;
            this.colExpectedDurationUnitsDescriptorID.Width = 198;
            // 
            // colScheduleSentDate
            // 
            this.colScheduleSentDate.Caption = "Schedule Sent";
            this.colScheduleSentDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colScheduleSentDate.FieldName = "ScheduleSentDate";
            this.colScheduleSentDate.Name = "colScheduleSentDate";
            this.colScheduleSentDate.OptionsColumn.AllowEdit = false;
            this.colScheduleSentDate.OptionsColumn.AllowFocus = false;
            this.colScheduleSentDate.OptionsColumn.ReadOnly = true;
            this.colScheduleSentDate.Visible = true;
            this.colScheduleSentDate.VisibleIndex = 10;
            this.colScheduleSentDate.Width = 106;
            // 
            // colActualStartDate
            // 
            this.colActualStartDate.Caption = "Actual Start";
            this.colActualStartDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colActualStartDate.FieldName = "ActualStartDate";
            this.colActualStartDate.Name = "colActualStartDate";
            this.colActualStartDate.OptionsColumn.AllowEdit = false;
            this.colActualStartDate.OptionsColumn.AllowFocus = false;
            this.colActualStartDate.OptionsColumn.ReadOnly = true;
            this.colActualStartDate.Visible = true;
            this.colActualStartDate.VisibleIndex = 11;
            this.colActualStartDate.Width = 100;
            // 
            // colActualEndDate
            // 
            this.colActualEndDate.Caption = "Actual End";
            this.colActualEndDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colActualEndDate.FieldName = "ActualEndDate";
            this.colActualEndDate.Name = "colActualEndDate";
            this.colActualEndDate.OptionsColumn.AllowEdit = false;
            this.colActualEndDate.OptionsColumn.AllowFocus = false;
            this.colActualEndDate.OptionsColumn.ReadOnly = true;
            this.colActualEndDate.Visible = true;
            this.colActualEndDate.VisibleIndex = 12;
            this.colActualEndDate.Width = 100;
            // 
            // colActualDurationUnits
            // 
            this.colActualDurationUnits.Caption = "Actual Duration";
            this.colActualDurationUnits.FieldName = "ActualDurationUnits";
            this.colActualDurationUnits.Name = "colActualDurationUnits";
            this.colActualDurationUnits.OptionsColumn.AllowEdit = false;
            this.colActualDurationUnits.OptionsColumn.AllowFocus = false;
            this.colActualDurationUnits.OptionsColumn.ReadOnly = true;
            this.colActualDurationUnits.Visible = true;
            this.colActualDurationUnits.VisibleIndex = 13;
            this.colActualDurationUnits.Width = 95;
            // 
            // colActualDurationUnitsDescriptionID
            // 
            this.colActualDurationUnitsDescriptionID.Caption = "Actual Duration Unit Descriptor ID";
            this.colActualDurationUnitsDescriptionID.FieldName = "ActualDurationUnitsDescriptionID";
            this.colActualDurationUnitsDescriptionID.Name = "colActualDurationUnitsDescriptionID";
            this.colActualDurationUnitsDescriptionID.OptionsColumn.AllowEdit = false;
            this.colActualDurationUnitsDescriptionID.OptionsColumn.AllowFocus = false;
            this.colActualDurationUnitsDescriptionID.OptionsColumn.ReadOnly = true;
            this.colActualDurationUnitsDescriptionID.Width = 183;
            // 
            // colCostTotalMaterialExVAT
            // 
            this.colCostTotalMaterialExVAT.Caption = "Material Cost Ex VAT";
            this.colCostTotalMaterialExVAT.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colCostTotalMaterialExVAT.FieldName = "CostTotalMaterialExVAT";
            this.colCostTotalMaterialExVAT.Name = "colCostTotalMaterialExVAT";
            this.colCostTotalMaterialExVAT.OptionsColumn.AllowEdit = false;
            this.colCostTotalMaterialExVAT.OptionsColumn.AllowFocus = false;
            this.colCostTotalMaterialExVAT.OptionsColumn.ReadOnly = true;
            this.colCostTotalMaterialExVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostTotalMaterialExVAT", "{0:c}")});
            this.colCostTotalMaterialExVAT.Width = 121;
            // 
            // repositoryItemTextEditMoney2
            // 
            this.repositoryItemTextEditMoney2.AutoHeight = false;
            this.repositoryItemTextEditMoney2.Mask.EditMask = "c";
            this.repositoryItemTextEditMoney2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditMoney2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditMoney2.Name = "repositoryItemTextEditMoney2";
            // 
            // colCostTotalMaterialVAT
            // 
            this.colCostTotalMaterialVAT.Caption = "Material Cost VAT";
            this.colCostTotalMaterialVAT.ColumnEdit = this.repositoryItemTextEditPercentage2;
            this.colCostTotalMaterialVAT.FieldName = "CostTotalMaterialVAT";
            this.colCostTotalMaterialVAT.Name = "colCostTotalMaterialVAT";
            this.colCostTotalMaterialVAT.OptionsColumn.AllowEdit = false;
            this.colCostTotalMaterialVAT.OptionsColumn.AllowFocus = false;
            this.colCostTotalMaterialVAT.OptionsColumn.ReadOnly = true;
            this.colCostTotalMaterialVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostTotalMaterialVAT", "{0:c}")});
            this.colCostTotalMaterialVAT.Width = 106;
            // 
            // repositoryItemTextEditPercentage2
            // 
            this.repositoryItemTextEditPercentage2.AutoHeight = false;
            this.repositoryItemTextEditPercentage2.Name = "repositoryItemTextEditPercentage2";
            // 
            // colCostTotalMaterialCost
            // 
            this.colCostTotalMaterialCost.Caption = "Material Cost Total";
            this.colCostTotalMaterialCost.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colCostTotalMaterialCost.FieldName = "CostTotalMaterialCost";
            this.colCostTotalMaterialCost.Name = "colCostTotalMaterialCost";
            this.colCostTotalMaterialCost.OptionsColumn.AllowEdit = false;
            this.colCostTotalMaterialCost.OptionsColumn.AllowFocus = false;
            this.colCostTotalMaterialCost.OptionsColumn.ReadOnly = true;
            this.colCostTotalMaterialCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostTotalMaterialCost", "{0:c}")});
            this.colCostTotalMaterialCost.Width = 111;
            // 
            // colSellTotalMaterialExVAT
            // 
            this.colSellTotalMaterialExVAT.Caption = "Material Sell Ex VAT";
            this.colSellTotalMaterialExVAT.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colSellTotalMaterialExVAT.FieldName = "SellTotalMaterialExVAT";
            this.colSellTotalMaterialExVAT.Name = "colSellTotalMaterialExVAT";
            this.colSellTotalMaterialExVAT.OptionsColumn.AllowEdit = false;
            this.colSellTotalMaterialExVAT.OptionsColumn.AllowFocus = false;
            this.colSellTotalMaterialExVAT.OptionsColumn.ReadOnly = true;
            this.colSellTotalMaterialExVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellTotalMaterialExVAT", "{0:c}")});
            this.colSellTotalMaterialExVAT.Width = 115;
            // 
            // colSellTotalMaterialVAT
            // 
            this.colSellTotalMaterialVAT.Caption = "Material Sell VAT";
            this.colSellTotalMaterialVAT.ColumnEdit = this.repositoryItemTextEditPercentage2;
            this.colSellTotalMaterialVAT.FieldName = "SellTotalMaterialVAT";
            this.colSellTotalMaterialVAT.Name = "colSellTotalMaterialVAT";
            this.colSellTotalMaterialVAT.OptionsColumn.AllowEdit = false;
            this.colSellTotalMaterialVAT.OptionsColumn.AllowFocus = false;
            this.colSellTotalMaterialVAT.OptionsColumn.ReadOnly = true;
            this.colSellTotalMaterialVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellTotalMaterialVAT", "{0:c}")});
            this.colSellTotalMaterialVAT.Width = 100;
            // 
            // colSellTotalMaterialCost
            // 
            this.colSellTotalMaterialCost.Caption = "Material Sell Total";
            this.colSellTotalMaterialCost.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colSellTotalMaterialCost.FieldName = "SellTotalMaterialCost";
            this.colSellTotalMaterialCost.Name = "colSellTotalMaterialCost";
            this.colSellTotalMaterialCost.OptionsColumn.AllowEdit = false;
            this.colSellTotalMaterialCost.OptionsColumn.AllowFocus = false;
            this.colSellTotalMaterialCost.OptionsColumn.ReadOnly = true;
            this.colSellTotalMaterialCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellTotalMaterialCost", "{0:c}")});
            this.colSellTotalMaterialCost.Width = 105;
            // 
            // colCostTotalEquipmentExVAT
            // 
            this.colCostTotalEquipmentExVAT.Caption = "Equipment Cost Ex VAT";
            this.colCostTotalEquipmentExVAT.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colCostTotalEquipmentExVAT.FieldName = "CostTotalEquipmentExVAT";
            this.colCostTotalEquipmentExVAT.Name = "colCostTotalEquipmentExVAT";
            this.colCostTotalEquipmentExVAT.OptionsColumn.AllowEdit = false;
            this.colCostTotalEquipmentExVAT.OptionsColumn.AllowFocus = false;
            this.colCostTotalEquipmentExVAT.OptionsColumn.ReadOnly = true;
            this.colCostTotalEquipmentExVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostTotalEquipmentExVAT", "{0:c}")});
            this.colCostTotalEquipmentExVAT.Width = 133;
            // 
            // colCostTotalEquipmentVAT
            // 
            this.colCostTotalEquipmentVAT.Caption = "Equipment Cost VAT";
            this.colCostTotalEquipmentVAT.ColumnEdit = this.repositoryItemTextEditPercentage2;
            this.colCostTotalEquipmentVAT.FieldName = "CostTotalEquipmentVAT";
            this.colCostTotalEquipmentVAT.Name = "colCostTotalEquipmentVAT";
            this.colCostTotalEquipmentVAT.OptionsColumn.AllowEdit = false;
            this.colCostTotalEquipmentVAT.OptionsColumn.AllowFocus = false;
            this.colCostTotalEquipmentVAT.OptionsColumn.ReadOnly = true;
            this.colCostTotalEquipmentVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostTotalEquipmentVAT", "{0:c}")});
            this.colCostTotalEquipmentVAT.Width = 118;
            // 
            // colCostTotalEquipmentCost
            // 
            this.colCostTotalEquipmentCost.Caption = "Equipment Cost Total";
            this.colCostTotalEquipmentCost.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colCostTotalEquipmentCost.FieldName = "CostTotalEquipmentCost";
            this.colCostTotalEquipmentCost.Name = "colCostTotalEquipmentCost";
            this.colCostTotalEquipmentCost.OptionsColumn.AllowEdit = false;
            this.colCostTotalEquipmentCost.OptionsColumn.AllowFocus = false;
            this.colCostTotalEquipmentCost.OptionsColumn.ReadOnly = true;
            this.colCostTotalEquipmentCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostTotalEquipmentCost", "{0:c}")});
            this.colCostTotalEquipmentCost.Width = 123;
            // 
            // colSellTotalEquipmentExVAT
            // 
            this.colSellTotalEquipmentExVAT.Caption = "Equipment Sell Ex VAT";
            this.colSellTotalEquipmentExVAT.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colSellTotalEquipmentExVAT.FieldName = "SellTotalEquipmentExVAT";
            this.colSellTotalEquipmentExVAT.Name = "colSellTotalEquipmentExVAT";
            this.colSellTotalEquipmentExVAT.OptionsColumn.AllowEdit = false;
            this.colSellTotalEquipmentExVAT.OptionsColumn.AllowFocus = false;
            this.colSellTotalEquipmentExVAT.OptionsColumn.ReadOnly = true;
            this.colSellTotalEquipmentExVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellTotalEquipmentExVAT", "{0:c}")});
            this.colSellTotalEquipmentExVAT.Width = 127;
            // 
            // colSellTotalEquipmentVAT
            // 
            this.colSellTotalEquipmentVAT.Caption = "Equipment Sell VAT";
            this.colSellTotalEquipmentVAT.ColumnEdit = this.repositoryItemTextEditPercentage2;
            this.colSellTotalEquipmentVAT.FieldName = "SellTotalEquipmentVAT";
            this.colSellTotalEquipmentVAT.Name = "colSellTotalEquipmentVAT";
            this.colSellTotalEquipmentVAT.OptionsColumn.AllowEdit = false;
            this.colSellTotalEquipmentVAT.OptionsColumn.AllowFocus = false;
            this.colSellTotalEquipmentVAT.OptionsColumn.ReadOnly = true;
            this.colSellTotalEquipmentVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellTotalEquipmentVAT", "{0:c}")});
            this.colSellTotalEquipmentVAT.Width = 112;
            // 
            // colSellTotalEquipmentCost
            // 
            this.colSellTotalEquipmentCost.Caption = "Equipment Sell Total";
            this.colSellTotalEquipmentCost.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colSellTotalEquipmentCost.FieldName = "SellTotalEquipmentCost";
            this.colSellTotalEquipmentCost.Name = "colSellTotalEquipmentCost";
            this.colSellTotalEquipmentCost.OptionsColumn.AllowEdit = false;
            this.colSellTotalEquipmentCost.OptionsColumn.AllowFocus = false;
            this.colSellTotalEquipmentCost.OptionsColumn.ReadOnly = true;
            this.colSellTotalEquipmentCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellTotalEquipmentCost", "{0:c}")});
            this.colSellTotalEquipmentCost.Width = 117;
            // 
            // colCostTotalLabourExVAT
            // 
            this.colCostTotalLabourExVAT.Caption = "Labour Cost Ex VAT";
            this.colCostTotalLabourExVAT.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colCostTotalLabourExVAT.FieldName = "CostTotalLabourExVAT";
            this.colCostTotalLabourExVAT.Name = "colCostTotalLabourExVAT";
            this.colCostTotalLabourExVAT.OptionsColumn.AllowEdit = false;
            this.colCostTotalLabourExVAT.OptionsColumn.AllowFocus = false;
            this.colCostTotalLabourExVAT.OptionsColumn.ReadOnly = true;
            this.colCostTotalLabourExVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostTotalLabourExVAT", "{0:c}")});
            this.colCostTotalLabourExVAT.Width = 116;
            // 
            // colCostTotalLabourVAT
            // 
            this.colCostTotalLabourVAT.Caption = "Labour Cost VAT";
            this.colCostTotalLabourVAT.ColumnEdit = this.repositoryItemTextEditPercentage2;
            this.colCostTotalLabourVAT.FieldName = "CostTotalLabourVAT";
            this.colCostTotalLabourVAT.Name = "colCostTotalLabourVAT";
            this.colCostTotalLabourVAT.OptionsColumn.AllowEdit = false;
            this.colCostTotalLabourVAT.OptionsColumn.AllowFocus = false;
            this.colCostTotalLabourVAT.OptionsColumn.ReadOnly = true;
            this.colCostTotalLabourVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostTotalLabourVAT", "{0:c}")});
            this.colCostTotalLabourVAT.Width = 101;
            // 
            // colCostTotalLabourCost
            // 
            this.colCostTotalLabourCost.Caption = "Labour Cost Total";
            this.colCostTotalLabourCost.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colCostTotalLabourCost.FieldName = "CostTotalLabourCost";
            this.colCostTotalLabourCost.Name = "colCostTotalLabourCost";
            this.colCostTotalLabourCost.OptionsColumn.AllowEdit = false;
            this.colCostTotalLabourCost.OptionsColumn.AllowFocus = false;
            this.colCostTotalLabourCost.OptionsColumn.ReadOnly = true;
            this.colCostTotalLabourCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostTotalLabourCost", "{0:c}")});
            this.colCostTotalLabourCost.Width = 106;
            // 
            // colSellTotalLabourExVAT
            // 
            this.colSellTotalLabourExVAT.Caption = "Labour Sell Ex VAT";
            this.colSellTotalLabourExVAT.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colSellTotalLabourExVAT.FieldName = "SellTotalLabourExVAT";
            this.colSellTotalLabourExVAT.Name = "colSellTotalLabourExVAT";
            this.colSellTotalLabourExVAT.OptionsColumn.AllowEdit = false;
            this.colSellTotalLabourExVAT.OptionsColumn.AllowFocus = false;
            this.colSellTotalLabourExVAT.OptionsColumn.ReadOnly = true;
            this.colSellTotalLabourExVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellTotalLabourExVAT", "{0:c}")});
            this.colSellTotalLabourExVAT.Width = 110;
            // 
            // colSellTotalLabourVAT
            // 
            this.colSellTotalLabourVAT.Caption = "Labour Sell VAT";
            this.colSellTotalLabourVAT.ColumnEdit = this.repositoryItemTextEditPercentage2;
            this.colSellTotalLabourVAT.FieldName = "SellTotalLabourVAT";
            this.colSellTotalLabourVAT.Name = "colSellTotalLabourVAT";
            this.colSellTotalLabourVAT.OptionsColumn.AllowEdit = false;
            this.colSellTotalLabourVAT.OptionsColumn.AllowFocus = false;
            this.colSellTotalLabourVAT.OptionsColumn.ReadOnly = true;
            this.colSellTotalLabourVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellTotalLabourVAT", "{0:c}")});
            this.colSellTotalLabourVAT.Width = 95;
            // 
            // colSellTotalLabourCost
            // 
            this.colSellTotalLabourCost.Caption = "Labour Sell Total";
            this.colSellTotalLabourCost.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colSellTotalLabourCost.FieldName = "SellTotalLabourCost";
            this.colSellTotalLabourCost.Name = "colSellTotalLabourCost";
            this.colSellTotalLabourCost.OptionsColumn.AllowEdit = false;
            this.colSellTotalLabourCost.OptionsColumn.AllowFocus = false;
            this.colSellTotalLabourCost.OptionsColumn.ReadOnly = true;
            this.colSellTotalLabourCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellTotalLabourCost", "{0:c}")});
            this.colSellTotalLabourCost.Width = 100;
            // 
            // colCostTotalCostExVAT
            // 
            this.colCostTotalCostExVAT.Caption = "Total Cost Ex VAT";
            this.colCostTotalCostExVAT.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colCostTotalCostExVAT.FieldName = "CostTotalCostExVAT";
            this.colCostTotalCostExVAT.Name = "colCostTotalCostExVAT";
            this.colCostTotalCostExVAT.OptionsColumn.AllowEdit = false;
            this.colCostTotalCostExVAT.OptionsColumn.AllowFocus = false;
            this.colCostTotalCostExVAT.OptionsColumn.ReadOnly = true;
            this.colCostTotalCostExVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostTotalCostExVAT", "{0:c}")});
            this.colCostTotalCostExVAT.Width = 107;
            // 
            // colCostTotalCostVAT
            // 
            this.colCostTotalCostVAT.Caption = "Total Cost VAT";
            this.colCostTotalCostVAT.ColumnEdit = this.repositoryItemTextEditPercentage2;
            this.colCostTotalCostVAT.FieldName = "CostTotalCostVAT";
            this.colCostTotalCostVAT.Name = "colCostTotalCostVAT";
            this.colCostTotalCostVAT.OptionsColumn.AllowEdit = false;
            this.colCostTotalCostVAT.OptionsColumn.AllowFocus = false;
            this.colCostTotalCostVAT.OptionsColumn.ReadOnly = true;
            this.colCostTotalCostVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostTotalCostVAT", "{0:c}")});
            this.colCostTotalCostVAT.Width = 92;
            // 
            // colCostTotalCost
            // 
            this.colCostTotalCost.Caption = "Total Cost";
            this.colCostTotalCost.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colCostTotalCost.FieldName = "CostTotalCost";
            this.colCostTotalCost.Name = "colCostTotalCost";
            this.colCostTotalCost.OptionsColumn.AllowEdit = false;
            this.colCostTotalCost.OptionsColumn.AllowFocus = false;
            this.colCostTotalCost.OptionsColumn.ReadOnly = true;
            this.colCostTotalCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostTotalCost", "{0:c}")});
            // 
            // colSellTotalCostExVAT
            // 
            this.colSellTotalCostExVAT.Caption = "Total Sell Ex VAT";
            this.colSellTotalCostExVAT.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colSellTotalCostExVAT.FieldName = "SellTotalCostExVAT";
            this.colSellTotalCostExVAT.Name = "colSellTotalCostExVAT";
            this.colSellTotalCostExVAT.OptionsColumn.AllowEdit = false;
            this.colSellTotalCostExVAT.OptionsColumn.AllowFocus = false;
            this.colSellTotalCostExVAT.OptionsColumn.ReadOnly = true;
            this.colSellTotalCostExVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellTotalCostExVAT", "{0:c}")});
            this.colSellTotalCostExVAT.Width = 101;
            // 
            // colSellTotalCostVAT
            // 
            this.colSellTotalCostVAT.Caption = "Total Sell VAT";
            this.colSellTotalCostVAT.ColumnEdit = this.repositoryItemTextEditPercentage2;
            this.colSellTotalCostVAT.FieldName = "SellTotalCostVAT";
            this.colSellTotalCostVAT.Name = "colSellTotalCostVAT";
            this.colSellTotalCostVAT.OptionsColumn.AllowEdit = false;
            this.colSellTotalCostVAT.OptionsColumn.AllowFocus = false;
            this.colSellTotalCostVAT.OptionsColumn.ReadOnly = true;
            this.colSellTotalCostVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellTotalCostVAT", "{0:c}")});
            this.colSellTotalCostVAT.Width = 86;
            // 
            // colSellTotalCost
            // 
            this.colSellTotalCost.Caption = "Total Sell";
            this.colSellTotalCost.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colSellTotalCost.FieldName = "SellTotalCost";
            this.colSellTotalCost.Name = "colSellTotalCost";
            this.colSellTotalCost.OptionsColumn.AllowEdit = false;
            this.colSellTotalCost.OptionsColumn.AllowFocus = false;
            this.colSellTotalCost.OptionsColumn.ReadOnly = true;
            this.colSellTotalCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellTotalCost", "{0:c}")});
            // 
            // colClientInvoiceID
            // 
            this.colClientInvoiceID.Caption = "Client Invoice ID";
            this.colClientInvoiceID.FieldName = "ClientInvoiceID";
            this.colClientInvoiceID.Name = "colClientInvoiceID";
            this.colClientInvoiceID.OptionsColumn.AllowEdit = false;
            this.colClientInvoiceID.OptionsColumn.AllowFocus = false;
            this.colClientInvoiceID.OptionsColumn.ReadOnly = true;
            this.colClientInvoiceID.Width = 100;
            // 
            // colDateClientInvoiced
            // 
            this.colDateClientInvoiced.Caption = "Client Invoice Date";
            this.colDateClientInvoiced.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colDateClientInvoiced.FieldName = "DateClientInvoiced";
            this.colDateClientInvoiced.Name = "colDateClientInvoiced";
            this.colDateClientInvoiced.OptionsColumn.AllowEdit = false;
            this.colDateClientInvoiced.OptionsColumn.AllowFocus = false;
            this.colDateClientInvoiced.OptionsColumn.ReadOnly = true;
            this.colDateClientInvoiced.Width = 112;
            // 
            // colSelfBillingInvoiceID
            // 
            this.colSelfBillingInvoiceID.Caption = "Self Billing Invoice ID";
            this.colSelfBillingInvoiceID.FieldName = "SelfBillingInvoiceID";
            this.colSelfBillingInvoiceID.Name = "colSelfBillingInvoiceID";
            this.colSelfBillingInvoiceID.OptionsColumn.AllowEdit = false;
            this.colSelfBillingInvoiceID.OptionsColumn.AllowFocus = false;
            this.colSelfBillingInvoiceID.OptionsColumn.ReadOnly = true;
            this.colSelfBillingInvoiceID.Width = 120;
            // 
            // colSelfBillingInvoiceReceivedDate
            // 
            this.colSelfBillingInvoiceReceivedDate.Caption = "Self Billing Invoice Received Date";
            this.colSelfBillingInvoiceReceivedDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colSelfBillingInvoiceReceivedDate.FieldName = "SelfBillingInvoiceReceivedDate";
            this.colSelfBillingInvoiceReceivedDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colSelfBillingInvoiceReceivedDate.Name = "colSelfBillingInvoiceReceivedDate";
            this.colSelfBillingInvoiceReceivedDate.OptionsColumn.AllowEdit = false;
            this.colSelfBillingInvoiceReceivedDate.OptionsColumn.AllowFocus = false;
            this.colSelfBillingInvoiceReceivedDate.OptionsColumn.ReadOnly = true;
            this.colSelfBillingInvoiceReceivedDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colSelfBillingInvoiceReceivedDate.Width = 179;
            // 
            // colSelfBillingInvoicePaidDate
            // 
            this.colSelfBillingInvoicePaidDate.Caption = "Self Billing Invoice Paid Date";
            this.colSelfBillingInvoicePaidDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colSelfBillingInvoicePaidDate.FieldName = "SelfBillingInvoicePaidDate";
            this.colSelfBillingInvoicePaidDate.Name = "colSelfBillingInvoicePaidDate";
            this.colSelfBillingInvoicePaidDate.OptionsColumn.AllowEdit = false;
            this.colSelfBillingInvoicePaidDate.OptionsColumn.AllowFocus = false;
            this.colSelfBillingInvoicePaidDate.OptionsColumn.ReadOnly = true;
            this.colSelfBillingInvoicePaidDate.Width = 155;
            // 
            // colSelfBillingInvoiceAmountPaid
            // 
            this.colSelfBillingInvoiceAmountPaid.Caption = "Self Billing Invoice Amunt Paid";
            this.colSelfBillingInvoiceAmountPaid.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colSelfBillingInvoiceAmountPaid.FieldName = "SelfBillingInvoiceAmountPaid";
            this.colSelfBillingInvoiceAmountPaid.Name = "colSelfBillingInvoiceAmountPaid";
            this.colSelfBillingInvoiceAmountPaid.OptionsColumn.AllowEdit = false;
            this.colSelfBillingInvoiceAmountPaid.OptionsColumn.AllowFocus = false;
            this.colSelfBillingInvoiceAmountPaid.OptionsColumn.ReadOnly = true;
            this.colSelfBillingInvoiceAmountPaid.Width = 163;
            // 
            // colDoNotPayContractor
            // 
            this.colDoNotPayContractor.Caption = "Do Not Pay Contractor";
            this.colDoNotPayContractor.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colDoNotPayContractor.FieldName = "DoNotPayContractor";
            this.colDoNotPayContractor.Name = "colDoNotPayContractor";
            this.colDoNotPayContractor.OptionsColumn.AllowEdit = false;
            this.colDoNotPayContractor.OptionsColumn.AllowFocus = false;
            this.colDoNotPayContractor.OptionsColumn.ReadOnly = true;
            this.colDoNotPayContractor.Visible = true;
            this.colDoNotPayContractor.VisibleIndex = 15;
            this.colDoNotPayContractor.Width = 130;
            // 
            // colDoNotInvoiceClient
            // 
            this.colDoNotInvoiceClient.Caption = "Do Not Invoice Client";
            this.colDoNotInvoiceClient.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colDoNotInvoiceClient.FieldName = "DoNotInvoiceClient";
            this.colDoNotInvoiceClient.Name = "colDoNotInvoiceClient";
            this.colDoNotInvoiceClient.OptionsColumn.AllowEdit = false;
            this.colDoNotInvoiceClient.OptionsColumn.AllowFocus = false;
            this.colDoNotInvoiceClient.OptionsColumn.ReadOnly = true;
            this.colDoNotInvoiceClient.Visible = true;
            this.colDoNotInvoiceClient.VisibleIndex = 16;
            this.colDoNotInvoiceClient.Width = 122;
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.ReadOnly = true;
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 17;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colRouteOrder
            // 
            this.colRouteOrder.Caption = "Route Order";
            this.colRouteOrder.FieldName = "RouteOrder";
            this.colRouteOrder.Name = "colRouteOrder";
            this.colRouteOrder.OptionsColumn.AllowEdit = false;
            this.colRouteOrder.OptionsColumn.AllowFocus = false;
            this.colRouteOrder.OptionsColumn.ReadOnly = true;
            this.colRouteOrder.Width = 81;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Start Latitude";
            this.gridColumn5.ColumnEdit = this.repositoryItemTextEditLatLong2;
            this.gridColumn5.FieldName = "StartLatitude";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Width = 87;
            // 
            // repositoryItemTextEditLatLong2
            // 
            this.repositoryItemTextEditLatLong2.AutoHeight = false;
            this.repositoryItemTextEditLatLong2.Mask.EditMask = "n8";
            this.repositoryItemTextEditLatLong2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditLatLong2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditLatLong2.Name = "repositoryItemTextEditLatLong2";
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Start Longitude";
            this.gridColumn6.ColumnEdit = this.repositoryItemTextEditLatLong2;
            this.gridColumn6.FieldName = "StartLongitude";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Width = 95;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Finish Latitude";
            this.gridColumn7.ColumnEdit = this.repositoryItemTextEditLatLong2;
            this.gridColumn7.FieldName = "FinishLatitude";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Width = 90;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Finish Longitude";
            this.gridColumn8.ColumnEdit = this.repositoryItemTextEditLatLong2;
            this.gridColumn8.FieldName = "FinishLongitude";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Width = 98;
            // 
            // colJobStatusDescription
            // 
            this.colJobStatusDescription.Caption = "Job Status";
            this.colJobStatusDescription.FieldName = "JobStatusDescription";
            this.colJobStatusDescription.Name = "colJobStatusDescription";
            this.colJobStatusDescription.OptionsColumn.AllowEdit = false;
            this.colJobStatusDescription.OptionsColumn.AllowFocus = false;
            this.colJobStatusDescription.OptionsColumn.ReadOnly = true;
            this.colJobStatusDescription.Visible = true;
            this.colJobStatusDescription.VisibleIndex = 2;
            this.colJobStatusDescription.Width = 129;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Site Name";
            this.gridColumn9.FieldName = "SiteName";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 8;
            this.gridColumn9.Width = 122;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Client Name";
            this.gridColumn10.FieldName = "ClientName";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Width = 125;
            // 
            // colCreatedByStaffName1
            // 
            this.colCreatedByStaffName1.Caption = "Created By";
            this.colCreatedByStaffName1.FieldName = "CreatedByStaffName";
            this.colCreatedByStaffName1.Name = "colCreatedByStaffName1";
            this.colCreatedByStaffName1.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffName1.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffName1.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffName1.Width = 89;
            // 
            // colExpectedDurationUnitsDescriptor
            // 
            this.colExpectedDurationUnitsDescriptor.Caption = "Expected Duration Units";
            this.colExpectedDurationUnitsDescriptor.FieldName = "ExpectedDurationUnitsDescriptor";
            this.colExpectedDurationUnitsDescriptor.Name = "colExpectedDurationUnitsDescriptor";
            this.colExpectedDurationUnitsDescriptor.OptionsColumn.AllowEdit = false;
            this.colExpectedDurationUnitsDescriptor.OptionsColumn.AllowFocus = false;
            this.colExpectedDurationUnitsDescriptor.OptionsColumn.ReadOnly = true;
            this.colExpectedDurationUnitsDescriptor.Width = 137;
            // 
            // colActualDurationUnitsDescription
            // 
            this.colActualDurationUnitsDescription.Caption = "Actual Duration Units";
            this.colActualDurationUnitsDescription.FieldName = "ActualDurationUnitsDescriptor";
            this.colActualDurationUnitsDescription.Name = "colActualDurationUnitsDescription";
            this.colActualDurationUnitsDescription.OptionsColumn.AllowEdit = false;
            this.colActualDurationUnitsDescription.OptionsColumn.AllowFocus = false;
            this.colActualDurationUnitsDescription.OptionsColumn.ReadOnly = true;
            this.colActualDurationUnitsDescription.Width = 122;
            // 
            // colJobTypeID1
            // 
            this.colJobTypeID1.Caption = "Job Type ID";
            this.colJobTypeID1.FieldName = "JobTypeID";
            this.colJobTypeID1.Name = "colJobTypeID1";
            this.colJobTypeID1.OptionsColumn.AllowEdit = false;
            this.colJobTypeID1.OptionsColumn.AllowFocus = false;
            this.colJobTypeID1.OptionsColumn.ReadOnly = true;
            this.colJobTypeID1.Width = 79;
            // 
            // colJobSubTypeDescription
            // 
            this.colJobSubTypeDescription.Caption = "Job Sub-Type";
            this.colJobSubTypeDescription.FieldName = "JobSubTypeDescription";
            this.colJobSubTypeDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colJobSubTypeDescription.Name = "colJobSubTypeDescription";
            this.colJobSubTypeDescription.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeDescription.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeDescription.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeDescription.Visible = true;
            this.colJobSubTypeDescription.VisibleIndex = 1;
            this.colJobSubTypeDescription.Width = 128;
            // 
            // colJobTypeDescription1
            // 
            this.colJobTypeDescription1.Caption = "Job Type";
            this.colJobTypeDescription1.FieldName = "JobTypeDescription";
            this.colJobTypeDescription1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colJobTypeDescription1.Name = "colJobTypeDescription1";
            this.colJobTypeDescription1.OptionsColumn.AllowEdit = false;
            this.colJobTypeDescription1.OptionsColumn.AllowFocus = false;
            this.colJobTypeDescription1.OptionsColumn.ReadOnly = true;
            this.colJobTypeDescription1.Visible = true;
            this.colJobTypeDescription1.VisibleIndex = 0;
            this.colJobTypeDescription1.Width = 132;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Linked Jobs";
            this.gridColumn11.FieldName = "LinkedJobCount";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Width = 76;
            // 
            // colRecordType
            // 
            this.colRecordType.Caption = "Record Type";
            this.colRecordType.FieldName = "RecordType";
            this.colRecordType.Name = "colRecordType";
            this.colRecordType.OptionsColumn.AllowEdit = false;
            this.colRecordType.OptionsColumn.AllowFocus = false;
            this.colRecordType.OptionsColumn.ReadOnly = true;
            this.colRecordType.Visible = true;
            this.colRecordType.VisibleIndex = 8;
            this.colRecordType.Width = 120;
            // 
            // colSelected
            // 
            this.colSelected.Caption = "Selected";
            this.colSelected.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colSelected.FieldName = "Selected";
            this.colSelected.Name = "colSelected";
            this.colSelected.Width = 62;
            // 
            // colFullDescription1
            // 
            this.colFullDescription1.Caption = "Full Description";
            this.colFullDescription1.FieldName = "FullDescription";
            this.colFullDescription1.Name = "colFullDescription1";
            this.colFullDescription1.OptionsColumn.AllowEdit = false;
            this.colFullDescription1.OptionsColumn.AllowFocus = false;
            this.colFullDescription1.OptionsColumn.ReadOnly = true;
            this.colFullDescription1.Width = 420;
            // 
            // colTenderID
            // 
            this.colTenderID.Caption = "Tender ID";
            this.colTenderID.FieldName = "TenderID";
            this.colTenderID.Name = "colTenderID";
            this.colTenderID.OptionsColumn.AllowEdit = false;
            this.colTenderID.OptionsColumn.AllowFocus = false;
            this.colTenderID.OptionsColumn.ReadOnly = true;
            this.colTenderID.Width = 69;
            // 
            // colTenderJobID
            // 
            this.colTenderJobID.Caption = "Tender Job ID";
            this.colTenderJobID.FieldName = "TenderJobID";
            this.colTenderJobID.Name = "colTenderJobID";
            this.colTenderJobID.OptionsColumn.AllowEdit = false;
            this.colTenderJobID.OptionsColumn.AllowFocus = false;
            this.colTenderJobID.OptionsColumn.ReadOnly = true;
            this.colTenderJobID.Width = 89;
            // 
            // colMinimumDaysFromLastVisit
            // 
            this.colMinimumDaysFromLastVisit.Caption = "Min Days From Last Visit";
            this.colMinimumDaysFromLastVisit.ColumnEdit = this.repositoryItemTextEditInteger;
            this.colMinimumDaysFromLastVisit.FieldName = "MinimumDaysFromLastVisit";
            this.colMinimumDaysFromLastVisit.Name = "colMinimumDaysFromLastVisit";
            this.colMinimumDaysFromLastVisit.OptionsColumn.AllowEdit = false;
            this.colMinimumDaysFromLastVisit.OptionsColumn.AllowFocus = false;
            this.colMinimumDaysFromLastVisit.OptionsColumn.ReadOnly = true;
            this.colMinimumDaysFromLastVisit.Visible = true;
            this.colMinimumDaysFromLastVisit.VisibleIndex = 18;
            this.colMinimumDaysFromLastVisit.Width = 136;
            // 
            // colMaximumDaysFromLastVisit
            // 
            this.colMaximumDaysFromLastVisit.Caption = "Max Days From Last Visit";
            this.colMaximumDaysFromLastVisit.ColumnEdit = this.repositoryItemTextEditInteger;
            this.colMaximumDaysFromLastVisit.FieldName = "MaximumDaysFromLastVisit";
            this.colMaximumDaysFromLastVisit.Name = "colMaximumDaysFromLastVisit";
            this.colMaximumDaysFromLastVisit.OptionsColumn.AllowEdit = false;
            this.colMaximumDaysFromLastVisit.OptionsColumn.AllowFocus = false;
            this.colMaximumDaysFromLastVisit.OptionsColumn.ReadOnly = true;
            this.colMaximumDaysFromLastVisit.Visible = true;
            this.colMaximumDaysFromLastVisit.VisibleIndex = 19;
            this.colMaximumDaysFromLastVisit.Width = 140;
            // 
            // colDaysLeeway
            // 
            this.colDaysLeeway.Caption = "Days Leeway";
            this.colDaysLeeway.ColumnEdit = this.repositoryItemTextEditInteger;
            this.colDaysLeeway.FieldName = "DaysLeeway";
            this.colDaysLeeway.Name = "colDaysLeeway";
            this.colDaysLeeway.OptionsColumn.AllowEdit = false;
            this.colDaysLeeway.OptionsColumn.AllowFocus = false;
            this.colDaysLeeway.OptionsColumn.ReadOnly = true;
            this.colDaysLeeway.Visible = true;
            this.colDaysLeeway.VisibleIndex = 20;
            this.colDaysLeeway.Width = 85;
            // 
            // colBillingCentreCodeID
            // 
            this.colBillingCentreCodeID.Caption = "Billing Centre Code ID";
            this.colBillingCentreCodeID.FieldName = "BillingCentreCodeID";
            this.colBillingCentreCodeID.Name = "colBillingCentreCodeID";
            this.colBillingCentreCodeID.OptionsColumn.AllowEdit = false;
            this.colBillingCentreCodeID.OptionsColumn.AllowFocus = false;
            this.colBillingCentreCodeID.OptionsColumn.ReadOnly = true;
            this.colBillingCentreCodeID.Width = 125;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Linked Documents";
            this.gridColumn12.FieldName = "LinkedDocumentCount";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.Width = 107;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Contract Description";
            this.gridColumn13.FieldName = "ContractDescription";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 9;
            this.gridColumn13.Width = 150;
            // 
            // colCostTotalEquipmentVATRate
            // 
            this.colCostTotalEquipmentVATRate.Caption = "Equipment Cost VAT Rate";
            this.colCostTotalEquipmentVATRate.ColumnEdit = this.repositoryItemTextEditPercentage2;
            this.colCostTotalEquipmentVATRate.FieldName = "CostTotalEquipmentVATRate";
            this.colCostTotalEquipmentVATRate.Name = "colCostTotalEquipmentVATRate";
            this.colCostTotalEquipmentVATRate.OptionsColumn.AllowEdit = false;
            this.colCostTotalEquipmentVATRate.OptionsColumn.AllowFocus = false;
            this.colCostTotalEquipmentVATRate.OptionsColumn.ReadOnly = true;
            this.colCostTotalEquipmentVATRate.Width = 144;
            // 
            // colCostTotalLabourVATRate
            // 
            this.colCostTotalLabourVATRate.Caption = "Labour Cost VAT Rate";
            this.colCostTotalLabourVATRate.ColumnEdit = this.repositoryItemTextEditPercentage2;
            this.colCostTotalLabourVATRate.FieldName = "CostTotalLabourVATRate";
            this.colCostTotalLabourVATRate.Name = "colCostTotalLabourVATRate";
            this.colCostTotalLabourVATRate.OptionsColumn.AllowEdit = false;
            this.colCostTotalLabourVATRate.OptionsColumn.AllowFocus = false;
            this.colCostTotalLabourVATRate.OptionsColumn.ReadOnly = true;
            this.colCostTotalLabourVATRate.Width = 127;
            // 
            // colCostTotalMaterialVATRate
            // 
            this.colCostTotalMaterialVATRate.Caption = "Material Cost VAT Rate";
            this.colCostTotalMaterialVATRate.ColumnEdit = this.repositoryItemTextEditPercentage2;
            this.colCostTotalMaterialVATRate.FieldName = "CostTotalMaterialVATRate";
            this.colCostTotalMaterialVATRate.Name = "colCostTotalMaterialVATRate";
            this.colCostTotalMaterialVATRate.OptionsColumn.AllowEdit = false;
            this.colCostTotalMaterialVATRate.OptionsColumn.AllowFocus = false;
            this.colCostTotalMaterialVATRate.OptionsColumn.ReadOnly = true;
            this.colCostTotalMaterialVATRate.Width = 132;
            // 
            // colSellTotalMaterialVATRate
            // 
            this.colSellTotalMaterialVATRate.Caption = "Material Sell VAT Rate";
            this.colSellTotalMaterialVATRate.ColumnEdit = this.repositoryItemTextEditPercentage2;
            this.colSellTotalMaterialVATRate.FieldName = "SellTotalMaterialVATRate";
            this.colSellTotalMaterialVATRate.Name = "colSellTotalMaterialVATRate";
            this.colSellTotalMaterialVATRate.OptionsColumn.AllowEdit = false;
            this.colSellTotalMaterialVATRate.OptionsColumn.AllowFocus = false;
            this.colSellTotalMaterialVATRate.OptionsColumn.ReadOnly = true;
            this.colSellTotalMaterialVATRate.Width = 126;
            // 
            // colSellTotalEquipmentVATRate
            // 
            this.colSellTotalEquipmentVATRate.Caption = "Equipment Sell VAT Rate";
            this.colSellTotalEquipmentVATRate.ColumnEdit = this.repositoryItemTextEditPercentage2;
            this.colSellTotalEquipmentVATRate.FieldName = "SellTotalEquipmentVATRate";
            this.colSellTotalEquipmentVATRate.Name = "colSellTotalEquipmentVATRate";
            this.colSellTotalEquipmentVATRate.OptionsColumn.AllowEdit = false;
            this.colSellTotalEquipmentVATRate.OptionsColumn.AllowFocus = false;
            this.colSellTotalEquipmentVATRate.OptionsColumn.ReadOnly = true;
            this.colSellTotalEquipmentVATRate.Width = 138;
            // 
            // colSellTotalLabourVATRate
            // 
            this.colSellTotalLabourVATRate.Caption = "Labour Sell VAT Rate";
            this.colSellTotalLabourVATRate.ColumnEdit = this.repositoryItemTextEditPercentage2;
            this.colSellTotalLabourVATRate.FieldName = "SellTotalLabourVATRate";
            this.colSellTotalLabourVATRate.Name = "colSellTotalLabourVATRate";
            this.colSellTotalLabourVATRate.OptionsColumn.AllowEdit = false;
            this.colSellTotalLabourVATRate.OptionsColumn.AllowFocus = false;
            this.colSellTotalLabourVATRate.OptionsColumn.ReadOnly = true;
            this.colSellTotalLabourVATRate.Width = 121;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Client Contract ID";
            this.gridColumn14.FieldName = "ClientContractID";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Width = 107;
            // 
            // colClientID2
            // 
            this.colClientID2.Caption = "Client ID";
            this.colClientID2.FieldName = "ClientID";
            this.colClientID2.Name = "colClientID2";
            this.colClientID2.OptionsColumn.AllowEdit = false;
            this.colClientID2.OptionsColumn.AllowFocus = false;
            this.colClientID2.OptionsColumn.ReadOnly = true;
            this.colClientID2.Width = 62;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Site Contract ID";
            this.gridColumn15.FieldName = "SiteContractID";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.Width = 98;
            // 
            // colSiteID2
            // 
            this.colSiteID2.Caption = "Site ID";
            this.colSiteID2.FieldName = "SiteID";
            this.colSiteID2.Name = "colSiteID2";
            this.colSiteID2.OptionsColumn.AllowEdit = false;
            this.colSiteID2.OptionsColumn.AllowFocus = false;
            this.colSiteID2.OptionsColumn.ReadOnly = true;
            this.colSiteID2.Visible = true;
            this.colSiteID2.VisibleIndex = 22;
            this.colSiteID2.Width = 51;
            // 
            // colLocationX
            // 
            this.colLocationX.Caption = "Site Latitude";
            this.colLocationX.ColumnEdit = this.repositoryItemTextEditLatLong2;
            this.colLocationX.FieldName = "LocationX";
            this.colLocationX.Name = "colLocationX";
            this.colLocationX.OptionsColumn.AllowEdit = false;
            this.colLocationX.OptionsColumn.AllowFocus = false;
            this.colLocationX.OptionsColumn.ReadOnly = true;
            this.colLocationX.Width = 81;
            // 
            // colLocationY
            // 
            this.colLocationY.Caption = "Site Longitude";
            this.colLocationY.ColumnEdit = this.repositoryItemTextEditLatLong2;
            this.colLocationY.FieldName = "LocationY";
            this.colLocationY.Name = "colLocationY";
            this.colLocationY.OptionsColumn.AllowEdit = false;
            this.colLocationY.OptionsColumn.AllowFocus = false;
            this.colLocationY.OptionsColumn.ReadOnly = true;
            this.colLocationY.Width = 89;
            // 
            // colSitePostcode1
            // 
            this.colSitePostcode1.Caption = "Site Postcode";
            this.colSitePostcode1.FieldName = "SitePostcode";
            this.colSitePostcode1.Name = "colSitePostcode1";
            this.colSitePostcode1.OptionsColumn.AllowEdit = false;
            this.colSitePostcode1.OptionsColumn.AllowFocus = false;
            this.colSitePostcode1.OptionsColumn.ReadOnly = true;
            this.colSitePostcode1.Width = 86;
            // 
            // colLinkedToVisit
            // 
            this.colLinkedToVisit.Caption = "Linked To Visit";
            this.colLinkedToVisit.FieldName = "LinkedToVisit";
            this.colLinkedToVisit.Name = "colLinkedToVisit";
            this.colLinkedToVisit.OptionsColumn.AllowEdit = false;
            this.colLinkedToVisit.OptionsColumn.AllowFocus = false;
            this.colLinkedToVisit.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.List;
            this.colLinkedToVisit.Width = 336;
            // 
            // colClientAndContract
            // 
            this.colClientAndContract.Caption = "Client \\ Contract";
            this.colClientAndContract.FieldName = "ClientAndContract";
            this.colClientAndContract.Name = "colClientAndContract";
            this.colClientAndContract.OptionsColumn.AllowEdit = false;
            this.colClientAndContract.OptionsColumn.AllowFocus = false;
            this.colClientAndContract.OptionsColumn.ReadOnly = true;
            this.colClientAndContract.Visible = true;
            this.colClientAndContract.VisibleIndex = 25;
            this.colClientAndContract.Width = 299;
            // 
            // colReworkOriginalJobID
            // 
            this.colReworkOriginalJobID.Caption = "Reworked Original Job ID";
            this.colReworkOriginalJobID.FieldName = "ReworkOriginalJobID";
            this.colReworkOriginalJobID.Name = "colReworkOriginalJobID";
            this.colReworkOriginalJobID.OptionsColumn.AllowEdit = false;
            this.colReworkOriginalJobID.OptionsColumn.AllowFocus = false;
            this.colReworkOriginalJobID.OptionsColumn.ReadOnly = true;
            this.colReworkOriginalJobID.Width = 142;
            // 
            // colRework
            // 
            this.colRework.Caption = "Rework";
            this.colRework.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colRework.FieldName = "Rework";
            this.colRework.Name = "colRework";
            this.colRework.OptionsColumn.AllowEdit = false;
            this.colRework.OptionsColumn.AllowFocus = false;
            this.colRework.OptionsColumn.ReadOnly = true;
            this.colRework.Visible = true;
            this.colRework.VisibleIndex = 21;
            this.colRework.Width = 57;
            // 
            // sp06129_OM_Jobs_Linked_to_VisitsTableAdapter
            // 
            this.sp06129_OM_Jobs_Linked_to_VisitsTableAdapter.ClearBeforeFill = true;
            // 
            // dateEditJobStartDate
            // 
            this.dateEditJobStartDate.EditValue = null;
            this.dateEditJobStartDate.Location = new System.Drawing.Point(87, 3);
            this.dateEditJobStartDate.MenuManager = this.barManager1;
            this.dateEditJobStartDate.Name = "dateEditJobStartDate";
            this.dateEditJobStartDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dateEditJobStartDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditJobStartDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditJobStartDate.Properties.Mask.EditMask = "g";
            this.dateEditJobStartDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditJobStartDate.Properties.MaxValue = new System.DateTime(2500, 12, 31, 0, 0, 0, 0);
            this.dateEditJobStartDate.Properties.MinValue = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dateEditJobStartDate.Size = new System.Drawing.Size(109, 20);
            this.dateEditJobStartDate.TabIndex = 31;
            // 
            // panelControl2
            // 
            this.panelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl2.Controls.Add(this.StatusGridLookUpEdit);
            this.panelControl2.Controls.Add(this.labelControl4);
            this.panelControl2.Controls.Add(this.dateEditJobEndDate);
            this.panelControl2.Controls.Add(this.labelControl2);
            this.panelControl2.Controls.Add(this.labelControl1);
            this.panelControl2.Controls.Add(this.dateEditJobStartDate);
            this.panelControl2.Location = new System.Drawing.Point(7, 584);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(612, 26);
            this.panelControl2.TabIndex = 32;
            // 
            // StatusGridLookUpEdit
            // 
            this.StatusGridLookUpEdit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.StatusGridLookUpEdit.Location = new System.Drawing.Point(445, 3);
            this.StatusGridLookUpEdit.MenuManager = this.barManager1;
            this.StatusGridLookUpEdit.Name = "StatusGridLookUpEdit";
            this.StatusGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.StatusGridLookUpEdit.Properties.DataSource = this.sp06405OMVisitReVisitAvailableStatusesBindingSource;
            this.StatusGridLookUpEdit.Properties.DisplayMember = "Description";
            this.StatusGridLookUpEdit.Properties.NullText = "";
            this.StatusGridLookUpEdit.Properties.PopupView = this.gridView3;
            this.StatusGridLookUpEdit.Properties.ValueMember = "ID";
            this.StatusGridLookUpEdit.Size = new System.Drawing.Size(162, 20);
            this.StatusGridLookUpEdit.TabIndex = 36;
            // 
            // sp06405OMVisitReVisitAvailableStatusesBindingSource
            // 
            this.sp06405OMVisitReVisitAvailableStatusesBindingSource.DataMember = "sp06405_OM_Visit_ReVisit_Available_Statuses";
            this.sp06405OMVisitReVisitAvailableStatusesBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID,
            this.colVisitID,
            this.colJobID1,
            this.colDescription,
            this.colRecordOrder});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colID
            // 
            this.colID.Caption = "ID";
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.OptionsColumn.AllowEdit = false;
            this.colID.OptionsColumn.AllowFocus = false;
            this.colID.OptionsColumn.ReadOnly = true;
            // 
            // colVisitID
            // 
            this.colVisitID.Caption = "Visit Status ID";
            this.colVisitID.FieldName = "VisitID";
            this.colVisitID.Name = "colVisitID";
            this.colVisitID.OptionsColumn.AllowEdit = false;
            this.colVisitID.OptionsColumn.AllowFocus = false;
            this.colVisitID.OptionsColumn.ReadOnly = true;
            this.colVisitID.Visible = true;
            this.colVisitID.VisibleIndex = 1;
            this.colVisitID.Width = 86;
            // 
            // colJobID1
            // 
            this.colJobID1.Caption = "Job Status ID";
            this.colJobID1.FieldName = "JobID";
            this.colJobID1.Name = "colJobID1";
            this.colJobID1.OptionsColumn.AllowEdit = false;
            this.colJobID1.OptionsColumn.AllowFocus = false;
            this.colJobID1.OptionsColumn.ReadOnly = true;
            this.colJobID1.Visible = true;
            this.colJobID1.VisibleIndex = 2;
            this.colJobID1.Width = 84;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Job, Visit Status";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 244;
            // 
            // colRecordOrder
            // 
            this.colRecordOrder.Caption = "Order";
            this.colRecordOrder.FieldName = "RecordOrder";
            this.colRecordOrder.Name = "colRecordOrder";
            this.colRecordOrder.OptionsColumn.AllowEdit = false;
            this.colRecordOrder.OptionsColumn.AllowFocus = false;
            this.colRecordOrder.OptionsColumn.ReadOnly = true;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(404, 6);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(35, 13);
            this.labelControl4.TabIndex = 35;
            this.labelControl4.Text = "Status:";
            // 
            // dateEditJobEndDate
            // 
            this.dateEditJobEndDate.EditValue = null;
            this.dateEditJobEndDate.Location = new System.Drawing.Point(283, 3);
            this.dateEditJobEndDate.MenuManager = this.barManager1;
            this.dateEditJobEndDate.Name = "dateEditJobEndDate";
            this.dateEditJobEndDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dateEditJobEndDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditJobEndDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditJobEndDate.Properties.Mask.EditMask = "g";
            this.dateEditJobEndDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditJobEndDate.Properties.MaxValue = new System.DateTime(2500, 12, 31, 0, 0, 0, 0);
            this.dateEditJobEndDate.Properties.MinValue = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dateEditJobEndDate.Size = new System.Drawing.Size(109, 20);
            this.dateEditJobEndDate.TabIndex = 33;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(207, 6);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(70, 13);
            this.labelControl2.TabIndex = 33;
            this.labelControl2.Text = "Expected End:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(6, 6);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(76, 13);
            this.labelControl1.TabIndex = 32;
            this.labelControl1.Text = "Expected Start:";
            // 
            // sp06405_OM_Visit_ReVisit_Available_StatusesTableAdapter
            // 
            this.sp06405_OM_Visit_ReVisit_Available_StatusesTableAdapter.ClearBeforeFill = true;
            // 
            // memoEditRemarks
            // 
            this.memoEditRemarks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoEditRemarks.Location = new System.Drawing.Point(0, 0);
            this.memoEditRemarks.MenuManager = this.barManager1;
            this.memoEditRemarks.Name = "memoEditRemarks";
            this.scSpellChecker.SetShowSpellCheckMenu(this.memoEditRemarks, true);
            this.memoEditRemarks.Size = new System.Drawing.Size(783, 106);
            this.scSpellChecker.SetSpellCheckerOptions(this.memoEditRemarks, optionsSpelling1);
            this.memoEditRemarks.TabIndex = 33;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(7, 76);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.gridControlJob);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl1.Panel2.Controls.Add(this.memoEditRemarks);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "Remarks - Any value entered will be appended to the existing Visit and copied to " +
    "the new Jobs";
            this.splitContainerControl1.Size = new System.Drawing.Size(787, 500);
            this.splitContainerControl1.SplitterPosition = 364;
            this.splitContainerControl1.TabIndex = 34;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // frm_OM_Visit_Revisit
            // 
            this.ClientSize = new System.Drawing.Size(801, 616);
            this.ControlBox = false;
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_OM_Visit_Revisit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Revisit Visits";
            this.Load += new System.EventHandler(this.frm_OM_Visit_Revisit_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.panelControl2, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Visit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlJob)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06129OMJobsLinkedtoVisitsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewJob)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMoney2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditLatLong2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditJobStartDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditJobStartDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StatusGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06405OMVisitReVisitAvailableStatusesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditJobEndDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditJobEndDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditRemarks.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DataSet_OM_Contract dataSet_OM_Contract;
        private DataSet_OM_Visit dataSet_OM_Visit;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraGrid.GridControl gridControlJob;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewJob;
        private DevExpress.XtraGrid.Columns.GridColumn colJobID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditInteger;
        private DevExpress.XtraGrid.Columns.GridColumn colJobStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffID1;
        private DevExpress.XtraGrid.Columns.GridColumn colReactive;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colJobNoLongerRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colRequiresAccessPermit;
        private DevExpress.XtraGrid.Columns.GridColumn colAccessPermitID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPONumber;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPOID;
        private DevExpress.XtraGrid.Columns.GridColumn colFinanceSystemPONumber;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedDurationUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedDurationUnitsDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colScheduleSentDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActualStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActualEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActualDurationUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colActualDurationUnitsDescriptionID;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalMaterialExVAT;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditMoney2;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalMaterialVAT;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage2;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalMaterialCost;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalMaterialExVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalMaterialVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalMaterialCost;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalEquipmentExVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalEquipmentVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalEquipmentCost;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalEquipmentExVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalEquipmentVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalEquipmentCost;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalLabourExVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalLabourVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalLabourCost;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalLabourExVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalLabourVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalLabourCost;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalCostExVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalCostVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalCost;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalCostExVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalCostVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalCost;
        private DevExpress.XtraGrid.Columns.GridColumn colClientInvoiceID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateClientInvoiced;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoiceID;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoiceReceivedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoicePaidDate;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoiceAmountPaid;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotPayContractor;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotInvoiceClient;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colRouteOrder;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditLatLong2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn colJobStatusDescription;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffName1;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedDurationUnitsDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colActualDurationUnitsDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordType;
        private DevExpress.XtraGrid.Columns.GridColumn colSelected;
        private DevExpress.XtraGrid.Columns.GridColumn colFullDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colTenderID;
        private DevExpress.XtraGrid.Columns.GridColumn colTenderJobID;
        private DevExpress.XtraGrid.Columns.GridColumn colMinimumDaysFromLastVisit;
        private DevExpress.XtraGrid.Columns.GridColumn colMaximumDaysFromLastVisit;
        private DevExpress.XtraGrid.Columns.GridColumn colDaysLeeway;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingCentreCodeID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalEquipmentVATRate;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalLabourVATRate;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalMaterialVATRate;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalMaterialVATRate;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalEquipmentVATRate;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalLabourVATRate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID2;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationX;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationY;
        private DevExpress.XtraGrid.Columns.GridColumn colSitePostcode1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToVisit;
        private DevExpress.XtraGrid.Columns.GridColumn colClientAndContract;
        private System.Windows.Forms.BindingSource sp06129OMJobsLinkedtoVisitsBindingSource;
        private DataSet_OM_VisitTableAdapters.sp06129_OM_Jobs_Linked_to_VisitsTableAdapter sp06129_OM_Jobs_Linked_to_VisitsTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colReworkOriginalJobID;
        private DevExpress.XtraGrid.Columns.GridColumn colRework;
        private DevExpress.XtraEditors.DateEdit dateEditJobStartDate;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.DateEdit dateEditJobEndDate;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private System.Windows.Forms.BindingSource sp06405OMVisitReVisitAvailableStatusesBindingSource;
        private DataSet_OM_VisitTableAdapters.sp06405_OM_Visit_ReVisit_Available_StatusesTableAdapter sp06405_OM_Visit_ReVisit_Available_StatusesTableAdapter;
        private DevExpress.XtraEditors.GridLookUpEdit StatusGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder;
        private DevExpress.XtraEditors.MemoEdit memoEditRemarks;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
    }
}

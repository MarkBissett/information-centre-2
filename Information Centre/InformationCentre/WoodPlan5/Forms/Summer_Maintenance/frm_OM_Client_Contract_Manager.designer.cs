namespace WoodPlan5
{
    partial class frm_OM_Client_Contract_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Client_Contract_Manager));
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            this.colActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.gridControlContract = new DevExpress.XtraGrid.GridControl();
            this.sp06052OMClientContractManagerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Contract = new WoodPlan5.DataSet_OM_Contract();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridViewContract = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGCCompanyID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractManagerID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSectorTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colYearlyPercentageIncrease = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCheckRPIDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastClientPaymentDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPreviousContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinanceClientCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingCentreCodeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGCCompanyName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractManager = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSectorType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedContractCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colLinkedSiteContractCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedParentContractCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedDocumentCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditLinkedDocumentsClientContract = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colContractDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReactive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientInstructions = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSignatureForClientBilling = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSignatureForTeamSelfBilling = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTenderProactiveDaysToReturnQuote = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditInteger = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colTenderReactiveDaysToReturnQuote = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCMTenderRequestNotesFile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditTenderRequestNotesFile = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colDefaultCompletionSheetTemplate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDefaultCompletionSheetTemplateID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataSet_GC_Core = new WoodPlan5.DataSet_GC_Core();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.popupContainerControlContractTypeFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnContractTypeFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp06050OMContractTypesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn159 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn160 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn161 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerControlDateRange = new DevExpress.XtraEditors.PopupContainerControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.btnDateRangeOK = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerControlContractStatusFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnContractStatusFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp06051OMContractStatusesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageYears = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControlYear = new DevExpress.XtraGrid.GridControl();
            this.sp06055OMClientContractManagerLinkedYearsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewYear = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientContractYearID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYearDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEndDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActive1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colClientValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditMoney2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colClientID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToParent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHTML3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLinkedDocumentCount1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditLinkedDocumentsClientContractYear = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colYearlyPercentageIncrease1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridControlProfile = new DevExpress.XtraGrid.GridControl();
            this.sp06056OMClientContractManagerLinkedProfilesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewProfile = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientContractProfileID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractYearID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvoiceDateDue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colInvoiceDateActual = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedBillAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colActualBillAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBilledByPersonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colClientContractID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYearDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYearStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYearEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYearActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colClientID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractStartDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractEndDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToParent1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHTML1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colBilledByPerson = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageResponsibilities = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlResponsibility = new DevExpress.XtraGrid.GridControl();
            this.sp06057OMClientContractManagerLinkedResponsibilitiesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewResponsibility = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPersonResponsibilityID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colResponsibleForRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colResponsibleForRecordTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colResponsibilityTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colClientID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractStartDate2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colContractEndDate2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToParent2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHTML4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colResponsibilityType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPersonTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPersonTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageClientPOs = new DevExpress.XtraTab.XtraTabPage();
            this.popupContainerControlContractDirectorFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnContractDirectorFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp06389OMClientContractManagerContractDirectorFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerControlConstructionManagerFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnConstructionManagerFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp06388OMClientContractManagerConstructionManagerFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerControlKAMFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnKAMFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp06019OMJobManagerKAMsFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Summer_Core = new WoodPlan5.DataSet_GC_Summer_Core();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colForename = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControlClientPO = new DevExpress.XtraGrid.GridControl();
            this.sp06326OMClientPOsLinkedToParentBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Client_PO = new WoodPlan5.DataSet_OM_Client_PO();
            this.gridViewClientPO = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientPurchaseOrderLinkID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPurchaseOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsDefault = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colRemarks4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEditRemarks = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colPONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecord = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHTML2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colContractDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime9 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEndDate2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartingValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency9 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colUsedValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemainingValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWarningValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageBillingRequirements = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlBillingRequirement = new DevExpress.XtraGrid.GridControl();
            this.sp01037CoreBillingRequirementItemsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_Common_Functionality = new WoodPlan5.DataSet_Common_Functionality();
            this.gridViewBillingRequirement = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBillingRequirementID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRequirementID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientBillRequirement = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colTeamSelfBillRequirement = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colRequirementFulfilled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCheckedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingRequirement = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecord1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCheckedByStaff = new DevExpress.XtraGrid.Columns.GridColumn();
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAddWizard = new DevExpress.XtraBars.BarButtonItem();
            this.bbiImport = new DevExpress.XtraBars.BarButtonItem();
            this.bsiSelectedCount = new DevExpress.XtraBars.BarStaticItem();
            this.repositoryItemCheckEditActiveOnly = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.toolTipController1 = new DevExpress.Utils.ToolTipController(this.components);
            this.repositoryItemPopupContainerEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.sp06050_OM_Contract_TypesTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06050_OM_Contract_TypesTableAdapter();
            this.sp06051_OM_Contract_StatusesTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06051_OM_Contract_StatusesTableAdapter();
            this.sp06052_OM_Client_Contract_ManagerTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06052_OM_Client_Contract_ManagerTableAdapter();
            this.sp06055_OM_Client_Contract_Manager_Linked_YearsTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06055_OM_Client_Contract_Manager_Linked_YearsTableAdapter();
            this.sp06056_OM_Client_Contract_Manager_Linked_ProfilesTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06056_OM_Client_Contract_Manager_Linked_ProfilesTableAdapter();
            this.sp06057_OM_Client_Contract_Manager_Linked_ResponsibilitiesTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06057_OM_Client_Contract_Manager_Linked_ResponsibilitiesTableAdapter();
            this.sp06326_OM_Client_POs_Linked_To_ParentTableAdapter = new WoodPlan5.DataSet_OM_Client_POTableAdapters.sp06326_OM_Client_POs_Linked_To_ParentTableAdapter();
            this.imageCollection2 = new DevExpress.Utils.ImageCollection(this.components);
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.hideContainerLeft = new DevExpress.XtraBars.Docking.AutoHideContainer();
            this.dockPanelFilters = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.popupContainerEditConstructionManager = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerEditContractDirector = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerEditKAMFilter = new DevExpress.XtraEditors.PopupContainerEdit();
            this.btnLoad = new DevExpress.XtraEditors.SimpleButton();
            this.btnClearAllFilters = new DevExpress.XtraEditors.SimpleButton();
            this.checkEditActiveOnly = new DevExpress.XtraEditors.CheckEdit();
            this.popupContainerEditDateRange = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerEditContractStatus = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerEditContractType = new DevExpress.XtraEditors.PopupContainerEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp06019_OM_Job_Manager_KAMs_FilterTableAdapter = new WoodPlan5.DataSet_GC_Summer_CoreTableAdapters.sp06019_OM_Job_Manager_KAMs_FilterTableAdapter();
            this.sp06388_OM_Client_Contract_Manager_Construction_Manager_FilterTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06388_OM_Client_Contract_Manager_Construction_Manager_FilterTableAdapter();
            this.sp06389_OM_Client_Contract_Manager_Contract_Director_FilterTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06389_OM_Client_Contract_Manager_Contract_Director_FilterTableAdapter();
            this.sp01037_Core_Billing_Requirement_ItemsTableAdapter = new WoodPlan5.DataSet_Common_FunctionalityTableAdapters.sp01037_Core_Billing_Requirement_ItemsTableAdapter();
            this.xtraGridBlendingBillingRequirement = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlContract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06052OMClientContractManagerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewContract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocumentsClientContract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditTenderRequestNotesFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlContractTypeFilter)).BeginInit();
            this.popupContainerControlContractTypeFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06050OMContractTypesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).BeginInit();
            this.popupContainerControlDateRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlContractStatusFilter)).BeginInit();
            this.popupContainerControlContractStatusFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06051OMContractStatusesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPageYears.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06055OMClientContractManagerLinkedYearsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMoney2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocumentsClientContractYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlProfile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06056OMClientContractManagerLinkedProfilesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewProfile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML1)).BeginInit();
            this.xtraTabPageResponsibilities.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlResponsibility)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06057OMClientContractManagerLinkedResponsibilitiesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewResponsibility)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML4)).BeginInit();
            this.xtraTabPageClientPOs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlContractDirectorFilter)).BeginInit();
            this.popupContainerControlContractDirectorFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06389OMClientContractManagerContractDirectorFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlConstructionManagerFilter)).BeginInit();
            this.popupContainerControlConstructionManagerFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06388OMClientContractManagerConstructionManagerFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlKAMFilter)).BeginInit();
            this.popupContainerControlKAMFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06019OMJobManagerKAMsFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Summer_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlClientPO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06326OMClientPOsLinkedToParentBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Client_PO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewClientPO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEditRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency9)).BeginInit();
            this.xtraTabPageBillingRequirements.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlBillingRequirement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01037CoreBillingRequirementItemsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Common_Functionality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBillingRequirement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditActiveOnly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.hideContainerLeft.SuspendLayout();
            this.dockPanelFilters.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditConstructionManager.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditContractDirector.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditKAMFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditActiveOnly.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditDateRange.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditContractStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditContractType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1027, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 564);
            this.barDockControlBottom.Size = new System.Drawing.Size(1027, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 564);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1027, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 564);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.standaloneBarDockControl1);
            this.barManager1.DockManager = this.dockManager1;
            this.barManager1.Images = this.imageCollection2;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiAddWizard,
            this.bbiRefresh,
            this.bbiImport,
            this.bsiSelectedCount});
            this.barManager1.MaxItemId = 64;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEdit1,
            this.repositoryItemCheckEditActiveOnly});
            this.barManager1.HighlightedLinkChanged += new DevExpress.XtraBars.HighlightedLinkChangedEventHandler(this.barManager1_HighlightedLinkChanged);
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colActive
            // 
            this.colActive.Caption = "Active";
            this.colActive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colActive.FieldName = "Active";
            this.colActive.Name = "colActive";
            this.colActive.OptionsColumn.AllowEdit = false;
            this.colActive.OptionsColumn.AllowFocus = false;
            this.colActive.OptionsColumn.ReadOnly = true;
            this.colActive.Visible = true;
            this.colActive.VisibleIndex = 4;
            this.colActive.Width = 51;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridControlContract
            // 
            this.gridControlContract.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControlContract.DataSource = this.sp06052OMClientContractManagerBindingSource;
            this.gridControlContract.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlContract.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlContract.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlContract.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlContract.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlContract.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlContract.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlContract.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlContract.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlContract.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlContract.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlContract.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selcted Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 8, true, true, "Linked Documents", "linked_document")});
            this.gridControlContract.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlContract_EmbeddedNavigator_ButtonClick);
            this.gridControlContract.Location = new System.Drawing.Point(-1, 43);
            this.gridControlContract.MainView = this.gridViewContract;
            this.gridControlContract.MenuManager = this.barManager1;
            this.gridControlContract.Name = "gridControlContract";
            this.gridControlContract.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemHyperLinkEdit1,
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemTextEditCurrency,
            this.repositoryItemTextEditPercentage,
            this.repositoryItemHyperLinkEditLinkedDocumentsClientContract,
            this.repositoryItemTextEditInteger,
            this.repositoryItemHyperLinkEditTenderRequestNotesFile});
            this.gridControlContract.Size = new System.Drawing.Size(1001, 265);
            this.gridControlContract.TabIndex = 4;
            this.gridControlContract.UseEmbeddedNavigator = true;
            this.gridControlContract.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewContract});
            // 
            // sp06052OMClientContractManagerBindingSource
            // 
            this.sp06052OMClientContractManagerBindingSource.DataMember = "sp06052_OM_Client_Contract_Manager";
            this.sp06052OMClientContractManagerBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // dataSet_OM_Contract
            // 
            this.dataSet_OM_Contract.DataSetName = "DataSet_OM_Contract";
            this.dataSet_OM_Contract.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16");
            this.imageCollection1.Images.SetKeyName(3, "arrow_up_blue_round.png");
            this.imageCollection1.Images.SetKeyName(4, "arrow_down_blue_round.png");
            this.imageCollection1.Images.SetKeyName(5, "Sort_16x16.png");
            this.imageCollection1.Images.SetKeyName(6, "Scheduler_16x16.png");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Preview_16x16, "Preview_16x16", typeof(global::WoodPlan5.Properties.Resources), 7);
            this.imageCollection1.Images.SetKeyName(7, "Preview_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.linked_documents_16_16, "linked_documents_16_16", typeof(global::WoodPlan5.Properties.Resources), 8);
            this.imageCollection1.Images.SetKeyName(8, "linked_documents_16_16");
            this.imageCollection1.InsertGalleryImage("addgroupheader_16x16.png", "images/reports/addgroupheader_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/reports/addgroupheader_16x16.png"), 9);
            this.imageCollection1.Images.SetKeyName(9, "addgroupheader_16x16.png");
            this.imageCollection1.InsertGalleryImage("groupheader_16x16.png", "images/reports/groupheader_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/reports/groupheader_16x16.png"), 10);
            this.imageCollection1.Images.SetKeyName(10, "groupheader_16x16.png");
            this.imageCollection1.InsertGalleryImage("clearfilter_16x16.png", "images/filter/clearfilter_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/filter/clearfilter_16x16.png"), 11);
            this.imageCollection1.Images.SetKeyName(11, "clearfilter_16x16.png");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.refresh_16x16, "refresh_16x16", typeof(global::WoodPlan5.Properties.Resources), 12);
            this.imageCollection1.Images.SetKeyName(12, "refresh_16x16");
            // 
            // gridViewContract
            // 
            this.gridViewContract.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientContractID,
            this.colClientID,
            this.colGCCompanyID,
            this.colContractTypeID,
            this.colContractStatusID,
            this.colContractManagerID,
            this.colStartDate,
            this.colEndDate,
            this.colActive,
            this.colSectorTypeID,
            this.colContractValue,
            this.colYearlyPercentageIncrease,
            this.colCheckRPIDate,
            this.colLastClientPaymentDate,
            this.colLinkedToPreviousContractID,
            this.colFinanceClientCode,
            this.colBillingCentreCodeID,
            this.colRemarks,
            this.colClientName,
            this.colGCCompanyName,
            this.colContractType,
            this.colContractStatus,
            this.colContractManager,
            this.colSectorType,
            this.colLinkedContractCount,
            this.colLinkedSiteContractCount,
            this.colLinkedParentContractCount,
            this.colLinkedDocumentCount,
            this.colContractDescription,
            this.colReactive,
            this.colClientInstructions,
            this.colSignatureForClientBilling,
            this.colSignatureForTeamSelfBilling,
            this.colTenderProactiveDaysToReturnQuote,
            this.colTenderReactiveDaysToReturnQuote,
            this.colCMTenderRequestNotesFile,
            this.colDefaultCompletionSheetTemplate,
            this.colDefaultCompletionSheetTemplateID});
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.colActive;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Gray;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridViewContract.FormatRules.Add(gridFormatRule1);
            this.gridViewContract.GridControl = this.gridControlContract;
            this.gridViewContract.Name = "gridViewContract";
            this.gridViewContract.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewContract.OptionsFind.AlwaysVisible = true;
            this.gridViewContract.OptionsFind.FindDelay = 2000;
            this.gridViewContract.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewContract.OptionsLayout.StoreAppearance = true;
            this.gridViewContract.OptionsLayout.StoreFormatRules = true;
            this.gridViewContract.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewContract.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewContract.OptionsSelection.MultiSelect = true;
            this.gridViewContract.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewContract.OptionsView.ColumnAutoWidth = false;
            this.gridViewContract.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewContract.OptionsView.ShowGroupPanel = false;
            this.gridViewContract.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colStartDate, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridViewContract.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridViewContract_CustomDrawCell);
            this.gridViewContract.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewContract_CustomRowCellEdit);
            this.gridViewContract.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewContract.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewContract.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewContract.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewContract_ShowingEditor);
            this.gridViewContract.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewContract.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewContract.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewContract.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewContract_MouseUp);
            this.gridViewContract.DoubleClick += new System.EventHandler(this.gridViewContract_DoubleClick);
            this.gridViewContract.GotFocus += new System.EventHandler(this.gridViewContract_GotFocus);
            // 
            // colClientContractID
            // 
            this.colClientContractID.Caption = "Client Contract ID";
            this.colClientContractID.FieldName = "ClientContractID";
            this.colClientContractID.Name = "colClientContractID";
            this.colClientContractID.OptionsColumn.AllowEdit = false;
            this.colClientContractID.OptionsColumn.AllowFocus = false;
            this.colClientContractID.OptionsColumn.ReadOnly = true;
            this.colClientContractID.Width = 107;
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // colGCCompanyID
            // 
            this.colGCCompanyID.Caption = "GC Company ID";
            this.colGCCompanyID.FieldName = "GCCompanyID";
            this.colGCCompanyID.Name = "colGCCompanyID";
            this.colGCCompanyID.OptionsColumn.AllowEdit = false;
            this.colGCCompanyID.OptionsColumn.AllowFocus = false;
            this.colGCCompanyID.OptionsColumn.ReadOnly = true;
            this.colGCCompanyID.Width = 97;
            // 
            // colContractTypeID
            // 
            this.colContractTypeID.Caption = "Contract Type ID";
            this.colContractTypeID.FieldName = "ContractTypeID";
            this.colContractTypeID.Name = "colContractTypeID";
            this.colContractTypeID.OptionsColumn.AllowEdit = false;
            this.colContractTypeID.OptionsColumn.AllowFocus = false;
            this.colContractTypeID.OptionsColumn.ReadOnly = true;
            this.colContractTypeID.Width = 104;
            // 
            // colContractStatusID
            // 
            this.colContractStatusID.Caption = "Contract Status ID";
            this.colContractStatusID.FieldName = "ContractStatusID";
            this.colContractStatusID.Name = "colContractStatusID";
            this.colContractStatusID.OptionsColumn.AllowEdit = false;
            this.colContractStatusID.OptionsColumn.AllowFocus = false;
            this.colContractStatusID.OptionsColumn.ReadOnly = true;
            this.colContractStatusID.Width = 111;
            // 
            // colContractManagerID
            // 
            this.colContractManagerID.Caption = "Contract Director ID";
            this.colContractManagerID.FieldName = "ContractDirectorID";
            this.colContractManagerID.Name = "colContractManagerID";
            this.colContractManagerID.OptionsColumn.AllowEdit = false;
            this.colContractManagerID.OptionsColumn.AllowFocus = false;
            this.colContractManagerID.OptionsColumn.ReadOnly = true;
            this.colContractManagerID.Width = 122;
            // 
            // colStartDate
            // 
            this.colStartDate.Caption = "Start Date";
            this.colStartDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colStartDate.FieldName = "StartDate";
            this.colStartDate.Name = "colStartDate";
            this.colStartDate.OptionsColumn.AllowEdit = false;
            this.colStartDate.OptionsColumn.AllowFocus = false;
            this.colStartDate.OptionsColumn.ReadOnly = true;
            this.colStartDate.Visible = true;
            this.colStartDate.VisibleIndex = 2;
            this.colStartDate.Width = 100;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colEndDate
            // 
            this.colEndDate.Caption = "End Date";
            this.colEndDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colEndDate.FieldName = "EndDate";
            this.colEndDate.Name = "colEndDate";
            this.colEndDate.OptionsColumn.AllowEdit = false;
            this.colEndDate.OptionsColumn.AllowFocus = false;
            this.colEndDate.OptionsColumn.ReadOnly = true;
            this.colEndDate.Visible = true;
            this.colEndDate.VisibleIndex = 3;
            this.colEndDate.Width = 100;
            // 
            // colSectorTypeID
            // 
            this.colSectorTypeID.Caption = "Sector Type ID";
            this.colSectorTypeID.FieldName = "SectorTypeID";
            this.colSectorTypeID.Name = "colSectorTypeID";
            this.colSectorTypeID.OptionsColumn.AllowEdit = false;
            this.colSectorTypeID.OptionsColumn.AllowFocus = false;
            this.colSectorTypeID.OptionsColumn.ReadOnly = true;
            this.colSectorTypeID.Width = 93;
            // 
            // colContractValue
            // 
            this.colContractValue.Caption = "Contract Value";
            this.colContractValue.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colContractValue.FieldName = "ContractValue";
            this.colContractValue.Name = "colContractValue";
            this.colContractValue.OptionsColumn.AllowEdit = false;
            this.colContractValue.OptionsColumn.AllowFocus = false;
            this.colContractValue.OptionsColumn.ReadOnly = true;
            this.colContractValue.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ContractValue", "{0:c2}")});
            this.colContractValue.Visible = true;
            this.colContractValue.VisibleIndex = 11;
            this.colContractValue.Width = 92;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // colYearlyPercentageIncrease
            // 
            this.colYearlyPercentageIncrease.Caption = "Yearly % Increase";
            this.colYearlyPercentageIncrease.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colYearlyPercentageIncrease.FieldName = "YearlyPercentageIncrease";
            this.colYearlyPercentageIncrease.Name = "colYearlyPercentageIncrease";
            this.colYearlyPercentageIncrease.OptionsColumn.AllowEdit = false;
            this.colYearlyPercentageIncrease.OptionsColumn.AllowFocus = false;
            this.colYearlyPercentageIncrease.OptionsColumn.ReadOnly = true;
            this.colYearlyPercentageIncrease.Visible = true;
            this.colYearlyPercentageIncrease.VisibleIndex = 12;
            this.colYearlyPercentageIncrease.Width = 110;
            // 
            // repositoryItemTextEditPercentage
            // 
            this.repositoryItemTextEditPercentage.AutoHeight = false;
            this.repositoryItemTextEditPercentage.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage.Name = "repositoryItemTextEditPercentage";
            // 
            // colCheckRPIDate
            // 
            this.colCheckRPIDate.Caption = "Check RPI Date";
            this.colCheckRPIDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colCheckRPIDate.FieldName = "CheckRPIDate";
            this.colCheckRPIDate.Name = "colCheckRPIDate";
            this.colCheckRPIDate.OptionsColumn.AllowEdit = false;
            this.colCheckRPIDate.OptionsColumn.AllowFocus = false;
            this.colCheckRPIDate.OptionsColumn.ReadOnly = true;
            this.colCheckRPIDate.Visible = true;
            this.colCheckRPIDate.VisibleIndex = 13;
            this.colCheckRPIDate.Width = 96;
            // 
            // colLastClientPaymentDate
            // 
            this.colLastClientPaymentDate.Caption = "Last Client Payment";
            this.colLastClientPaymentDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colLastClientPaymentDate.FieldName = "LastClientPaymentDate";
            this.colLastClientPaymentDate.Name = "colLastClientPaymentDate";
            this.colLastClientPaymentDate.OptionsColumn.AllowEdit = false;
            this.colLastClientPaymentDate.OptionsColumn.AllowFocus = false;
            this.colLastClientPaymentDate.OptionsColumn.ReadOnly = true;
            this.colLastClientPaymentDate.Visible = true;
            this.colLastClientPaymentDate.VisibleIndex = 14;
            this.colLastClientPaymentDate.Width = 116;
            // 
            // colLinkedToPreviousContractID
            // 
            this.colLinkedToPreviousContractID.Caption = "Linked To Previous Contract ID";
            this.colLinkedToPreviousContractID.FieldName = "LinkedToPreviousContractID";
            this.colLinkedToPreviousContractID.Name = "colLinkedToPreviousContractID";
            this.colLinkedToPreviousContractID.OptionsColumn.AllowEdit = false;
            this.colLinkedToPreviousContractID.OptionsColumn.AllowFocus = false;
            this.colLinkedToPreviousContractID.OptionsColumn.ReadOnly = true;
            this.colLinkedToPreviousContractID.Width = 169;
            // 
            // colFinanceClientCode
            // 
            this.colFinanceClientCode.Caption = "Finance Client Code";
            this.colFinanceClientCode.FieldName = "FinanceClientCode";
            this.colFinanceClientCode.Name = "colFinanceClientCode";
            this.colFinanceClientCode.OptionsColumn.AllowEdit = false;
            this.colFinanceClientCode.OptionsColumn.AllowFocus = false;
            this.colFinanceClientCode.OptionsColumn.ReadOnly = true;
            this.colFinanceClientCode.Width = 116;
            // 
            // colBillingCentreCodeID
            // 
            this.colBillingCentreCodeID.Caption = "Billing Centre Code ID";
            this.colBillingCentreCodeID.FieldName = "BillingCentreCodeID";
            this.colBillingCentreCodeID.Name = "colBillingCentreCodeID";
            this.colBillingCentreCodeID.OptionsColumn.AllowEdit = false;
            this.colBillingCentreCodeID.OptionsColumn.AllowFocus = false;
            this.colBillingCentreCodeID.OptionsColumn.ReadOnly = true;
            this.colBillingCentreCodeID.Width = 125;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 26;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name  ";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 0;
            this.colClientName.Width = 176;
            // 
            // colGCCompanyName
            // 
            this.colGCCompanyName.Caption = "GC Company";
            this.colGCCompanyName.FieldName = "GCCompanyName";
            this.colGCCompanyName.Name = "colGCCompanyName";
            this.colGCCompanyName.OptionsColumn.AllowEdit = false;
            this.colGCCompanyName.OptionsColumn.AllowFocus = false;
            this.colGCCompanyName.OptionsColumn.ReadOnly = true;
            this.colGCCompanyName.Visible = true;
            this.colGCCompanyName.VisibleIndex = 8;
            this.colGCCompanyName.Width = 132;
            // 
            // colContractType
            // 
            this.colContractType.Caption = "Contract Type";
            this.colContractType.FieldName = "ContractType";
            this.colContractType.Name = "colContractType";
            this.colContractType.OptionsColumn.AllowEdit = false;
            this.colContractType.OptionsColumn.AllowFocus = false;
            this.colContractType.OptionsColumn.ReadOnly = true;
            this.colContractType.Visible = true;
            this.colContractType.VisibleIndex = 9;
            this.colContractType.Width = 153;
            // 
            // colContractStatus
            // 
            this.colContractStatus.Caption = "Contract Status";
            this.colContractStatus.FieldName = "ContractStatus";
            this.colContractStatus.Name = "colContractStatus";
            this.colContractStatus.OptionsColumn.AllowEdit = false;
            this.colContractStatus.OptionsColumn.AllowFocus = false;
            this.colContractStatus.OptionsColumn.ReadOnly = true;
            this.colContractStatus.Visible = true;
            this.colContractStatus.VisibleIndex = 6;
            this.colContractStatus.Width = 122;
            // 
            // colContractManager
            // 
            this.colContractManager.Caption = "Contract Director";
            this.colContractManager.FieldName = "ContractDirector";
            this.colContractManager.Name = "colContractManager";
            this.colContractManager.OptionsColumn.AllowEdit = false;
            this.colContractManager.OptionsColumn.AllowFocus = false;
            this.colContractManager.OptionsColumn.ReadOnly = true;
            this.colContractManager.Visible = true;
            this.colContractManager.VisibleIndex = 7;
            this.colContractManager.Width = 122;
            // 
            // colSectorType
            // 
            this.colSectorType.Caption = "Sector Type";
            this.colSectorType.FieldName = "SectorType";
            this.colSectorType.Name = "colSectorType";
            this.colSectorType.OptionsColumn.AllowEdit = false;
            this.colSectorType.OptionsColumn.AllowFocus = false;
            this.colSectorType.OptionsColumn.ReadOnly = true;
            this.colSectorType.Visible = true;
            this.colSectorType.VisibleIndex = 10;
            this.colSectorType.Width = 151;
            // 
            // colLinkedContractCount
            // 
            this.colLinkedContractCount.Caption = "Linked Child Contracts";
            this.colLinkedContractCount.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colLinkedContractCount.FieldName = "LinkedContractCount";
            this.colLinkedContractCount.Name = "colLinkedContractCount";
            this.colLinkedContractCount.OptionsColumn.ReadOnly = true;
            this.colLinkedContractCount.Visible = true;
            this.colLinkedContractCount.VisibleIndex = 18;
            this.colLinkedContractCount.Width = 127;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            this.repositoryItemHyperLinkEdit1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit1_OpenLink);
            // 
            // colLinkedSiteContractCount
            // 
            this.colLinkedSiteContractCount.Caption = "Linked Site Contracts";
            this.colLinkedSiteContractCount.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colLinkedSiteContractCount.FieldName = "LinkedSiteContractCount";
            this.colLinkedSiteContractCount.Name = "colLinkedSiteContractCount";
            this.colLinkedSiteContractCount.OptionsColumn.ReadOnly = true;
            this.colLinkedSiteContractCount.Visible = true;
            this.colLinkedSiteContractCount.VisibleIndex = 19;
            this.colLinkedSiteContractCount.Width = 122;
            // 
            // colLinkedParentContractCount
            // 
            this.colLinkedParentContractCount.Caption = "Linked Parent Contracts";
            this.colLinkedParentContractCount.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colLinkedParentContractCount.FieldName = "LinkedParentContractCount";
            this.colLinkedParentContractCount.Name = "colLinkedParentContractCount";
            this.colLinkedParentContractCount.OptionsColumn.ReadOnly = true;
            this.colLinkedParentContractCount.Visible = true;
            this.colLinkedParentContractCount.VisibleIndex = 17;
            this.colLinkedParentContractCount.Width = 136;
            // 
            // colLinkedDocumentCount
            // 
            this.colLinkedDocumentCount.Caption = "Linked Documents";
            this.colLinkedDocumentCount.ColumnEdit = this.repositoryItemHyperLinkEditLinkedDocumentsClientContract;
            this.colLinkedDocumentCount.FieldName = "LinkedDocumentCount";
            this.colLinkedDocumentCount.Name = "colLinkedDocumentCount";
            this.colLinkedDocumentCount.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentCount.Visible = true;
            this.colLinkedDocumentCount.VisibleIndex = 20;
            this.colLinkedDocumentCount.Width = 107;
            // 
            // repositoryItemHyperLinkEditLinkedDocumentsClientContract
            // 
            this.repositoryItemHyperLinkEditLinkedDocumentsClientContract.AutoHeight = false;
            this.repositoryItemHyperLinkEditLinkedDocumentsClientContract.Name = "repositoryItemHyperLinkEditLinkedDocumentsClientContract";
            this.repositoryItemHyperLinkEditLinkedDocumentsClientContract.SingleClick = true;
            this.repositoryItemHyperLinkEditLinkedDocumentsClientContract.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditLinkedDocumentsClientContract_OpenLink);
            // 
            // colContractDescription
            // 
            this.colContractDescription.Caption = "Contract Description";
            this.colContractDescription.FieldName = "ContractDescription";
            this.colContractDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colContractDescription.Name = "colContractDescription";
            this.colContractDescription.OptionsColumn.AllowEdit = false;
            this.colContractDescription.OptionsColumn.AllowFocus = false;
            this.colContractDescription.OptionsColumn.ReadOnly = true;
            this.colContractDescription.Visible = true;
            this.colContractDescription.VisibleIndex = 1;
            this.colContractDescription.Width = 285;
            // 
            // colReactive
            // 
            this.colReactive.Caption = "Reactive";
            this.colReactive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colReactive.FieldName = "Reactive";
            this.colReactive.Name = "colReactive";
            this.colReactive.OptionsColumn.AllowEdit = false;
            this.colReactive.OptionsColumn.AllowFocus = false;
            this.colReactive.OptionsColumn.ReadOnly = true;
            this.colReactive.Visible = true;
            this.colReactive.VisibleIndex = 5;
            this.colReactive.Width = 63;
            // 
            // colClientInstructions
            // 
            this.colClientInstructions.Caption = "Client Instructions";
            this.colClientInstructions.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colClientInstructions.FieldName = "ClientInstructions";
            this.colClientInstructions.Name = "colClientInstructions";
            this.colClientInstructions.OptionsColumn.ReadOnly = true;
            this.colClientInstructions.Visible = true;
            this.colClientInstructions.VisibleIndex = 21;
            this.colClientInstructions.Width = 108;
            // 
            // colSignatureForClientBilling
            // 
            this.colSignatureForClientBilling.Caption = "Team Self-Billing Client Signature Required";
            this.colSignatureForClientBilling.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSignatureForClientBilling.FieldName = "SignatureForClientBilling";
            this.colSignatureForClientBilling.Name = "colSignatureForClientBilling";
            this.colSignatureForClientBilling.OptionsColumn.AllowEdit = false;
            this.colSignatureForClientBilling.OptionsColumn.AllowFocus = false;
            this.colSignatureForClientBilling.OptionsColumn.ReadOnly = true;
            this.colSignatureForClientBilling.Visible = true;
            this.colSignatureForClientBilling.VisibleIndex = 15;
            this.colSignatureForClientBilling.Width = 221;
            // 
            // colSignatureForTeamSelfBilling
            // 
            this.colSignatureForTeamSelfBilling.Caption = "Client Billing Client Signature Required";
            this.colSignatureForTeamSelfBilling.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSignatureForTeamSelfBilling.FieldName = "SignatureForTeamSelfBilling";
            this.colSignatureForTeamSelfBilling.Name = "colSignatureForTeamSelfBilling";
            this.colSignatureForTeamSelfBilling.OptionsColumn.AllowEdit = false;
            this.colSignatureForTeamSelfBilling.OptionsColumn.AllowFocus = false;
            this.colSignatureForTeamSelfBilling.OptionsColumn.ReadOnly = true;
            this.colSignatureForTeamSelfBilling.Visible = true;
            this.colSignatureForTeamSelfBilling.VisibleIndex = 16;
            this.colSignatureForTeamSelfBilling.Width = 200;
            // 
            // colTenderProactiveDaysToReturnQuote
            // 
            this.colTenderProactiveDaysToReturnQuote.Caption = "Proactive Tender Days To Return Quote";
            this.colTenderProactiveDaysToReturnQuote.ColumnEdit = this.repositoryItemTextEditInteger;
            this.colTenderProactiveDaysToReturnQuote.FieldName = "TenderProactiveDaysToReturnQuote";
            this.colTenderProactiveDaysToReturnQuote.Name = "colTenderProactiveDaysToReturnQuote";
            this.colTenderProactiveDaysToReturnQuote.OptionsColumn.AllowEdit = false;
            this.colTenderProactiveDaysToReturnQuote.OptionsColumn.AllowFocus = false;
            this.colTenderProactiveDaysToReturnQuote.OptionsColumn.ReadOnly = true;
            this.colTenderProactiveDaysToReturnQuote.Visible = true;
            this.colTenderProactiveDaysToReturnQuote.VisibleIndex = 22;
            this.colTenderProactiveDaysToReturnQuote.Width = 212;
            // 
            // repositoryItemTextEditInteger
            // 
            this.repositoryItemTextEditInteger.AutoHeight = false;
            this.repositoryItemTextEditInteger.Mask.EditMask = "n0";
            this.repositoryItemTextEditInteger.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditInteger.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditInteger.Name = "repositoryItemTextEditInteger";
            // 
            // colTenderReactiveDaysToReturnQuote
            // 
            this.colTenderReactiveDaysToReturnQuote.Caption = "Reactive Tender Days To Return Quote";
            this.colTenderReactiveDaysToReturnQuote.ColumnEdit = this.repositoryItemTextEditInteger;
            this.colTenderReactiveDaysToReturnQuote.FieldName = "TenderReactiveDaysToReturnQuote";
            this.colTenderReactiveDaysToReturnQuote.Name = "colTenderReactiveDaysToReturnQuote";
            this.colTenderReactiveDaysToReturnQuote.OptionsColumn.AllowEdit = false;
            this.colTenderReactiveDaysToReturnQuote.OptionsColumn.AllowFocus = false;
            this.colTenderReactiveDaysToReturnQuote.OptionsColumn.ReadOnly = true;
            this.colTenderReactiveDaysToReturnQuote.Visible = true;
            this.colTenderReactiveDaysToReturnQuote.VisibleIndex = 23;
            this.colTenderReactiveDaysToReturnQuote.Width = 209;
            // 
            // colCMTenderRequestNotesFile
            // 
            this.colCMTenderRequestNotesFile.Caption = "CM Tender Request Notes File";
            this.colCMTenderRequestNotesFile.ColumnEdit = this.repositoryItemHyperLinkEditTenderRequestNotesFile;
            this.colCMTenderRequestNotesFile.FieldName = "CMTenderRequestNotesFile";
            this.colCMTenderRequestNotesFile.Name = "colCMTenderRequestNotesFile";
            this.colCMTenderRequestNotesFile.OptionsColumn.ReadOnly = true;
            this.colCMTenderRequestNotesFile.Visible = true;
            this.colCMTenderRequestNotesFile.VisibleIndex = 24;
            this.colCMTenderRequestNotesFile.Width = 205;
            // 
            // repositoryItemHyperLinkEditTenderRequestNotesFile
            // 
            this.repositoryItemHyperLinkEditTenderRequestNotesFile.AutoHeight = false;
            this.repositoryItemHyperLinkEditTenderRequestNotesFile.Name = "repositoryItemHyperLinkEditTenderRequestNotesFile";
            this.repositoryItemHyperLinkEditTenderRequestNotesFile.SingleClick = true;
            this.repositoryItemHyperLinkEditTenderRequestNotesFile.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditTenderRequestNotesFile_OpenLink);
            // 
            // colDefaultCompletionSheetTemplate
            // 
            this.colDefaultCompletionSheetTemplate.Caption = "Completion Sheet Template";
            this.colDefaultCompletionSheetTemplate.FieldName = "DefaultCompletionSheetTemplate";
            this.colDefaultCompletionSheetTemplate.Name = "colDefaultCompletionSheetTemplate";
            this.colDefaultCompletionSheetTemplate.OptionsColumn.AllowEdit = false;
            this.colDefaultCompletionSheetTemplate.OptionsColumn.AllowFocus = false;
            this.colDefaultCompletionSheetTemplate.OptionsColumn.ReadOnly = true;
            this.colDefaultCompletionSheetTemplate.Visible = true;
            this.colDefaultCompletionSheetTemplate.VisibleIndex = 25;
            this.colDefaultCompletionSheetTemplate.Width = 186;
            // 
            // colDefaultCompletionSheetTemplateID
            // 
            this.colDefaultCompletionSheetTemplateID.Caption = "Completion Sheet Template ID";
            this.colDefaultCompletionSheetTemplateID.FieldName = "DefaultCompletionSheetTemplateID";
            this.colDefaultCompletionSheetTemplateID.Name = "colDefaultCompletionSheetTemplateID";
            this.colDefaultCompletionSheetTemplateID.OptionsColumn.AllowEdit = false;
            this.colDefaultCompletionSheetTemplateID.OptionsColumn.AllowFocus = false;
            this.colDefaultCompletionSheetTemplateID.OptionsColumn.ReadOnly = true;
            this.colDefaultCompletionSheetTemplateID.Width = 164;
            // 
            // dataSet_GC_Core
            // 
            this.dataSet_GC_Core.DataSetName = "DataSet_GC_Core";
            this.dataSet_GC_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(23, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControlContractTypeFilter);
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControlDateRange);
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControlContractStatusFilter);
            this.splitContainerControl1.Panel1.Controls.Add(this.gridControlContract);
            this.splitContainerControl1.Panel1.Controls.Add(this.standaloneBarDockControl1);
            this.splitContainerControl1.Panel1.Text = "Jobs";
            this.splitContainerControl1.Panel2.Controls.Add(this.xtraTabControl1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1004, 564);
            this.splitContainerControl1.SplitterPosition = 246;
            this.splitContainerControl1.TabIndex = 5;
            this.splitContainerControl1.Text = "splitContainerControl1";
            this.splitContainerControl1.SplitGroupPanelCollapsed += new DevExpress.XtraEditors.SplitGroupPanelCollapsedEventHandler(this.splitContainerControl1_SplitGroupPanelCollapsed);
            // 
            // popupContainerControlContractTypeFilter
            // 
            this.popupContainerControlContractTypeFilter.Controls.Add(this.btnContractTypeFilterOK);
            this.popupContainerControlContractTypeFilter.Controls.Add(this.gridControl1);
            this.popupContainerControlContractTypeFilter.Location = new System.Drawing.Point(23, 121);
            this.popupContainerControlContractTypeFilter.Name = "popupContainerControlContractTypeFilter";
            this.popupContainerControlContractTypeFilter.Size = new System.Drawing.Size(137, 126);
            this.popupContainerControlContractTypeFilter.TabIndex = 19;
            // 
            // btnContractTypeFilterOK
            // 
            this.btnContractTypeFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnContractTypeFilterOK.Location = new System.Drawing.Point(3, 103);
            this.btnContractTypeFilterOK.Name = "btnContractTypeFilterOK";
            this.btnContractTypeFilterOK.Size = new System.Drawing.Size(50, 20);
            this.btnContractTypeFilterOK.TabIndex = 12;
            this.btnContractTypeFilterOK.Text = "OK";
            this.btnContractTypeFilterOK.Click += new System.EventHandler(this.btnContractTypeFilterOK_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.DataSource = this.sp06050OMContractTypesBindingSource;
            this.gridControl1.Location = new System.Drawing.Point(3, 3);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(131, 98);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp06050OMContractTypesBindingSource
            // 
            this.sp06050OMContractTypesBindingSource.DataMember = "sp06050_OM_Contract_Types";
            this.sp06050OMContractTypesBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn159,
            this.gridColumn160,
            this.gridColumn161});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn161, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn159
            // 
            this.gridColumn159.Caption = "Contract Type";
            this.gridColumn159.FieldName = "Description";
            this.gridColumn159.Name = "gridColumn159";
            this.gridColumn159.OptionsColumn.AllowEdit = false;
            this.gridColumn159.OptionsColumn.AllowFocus = false;
            this.gridColumn159.OptionsColumn.ReadOnly = true;
            this.gridColumn159.Visible = true;
            this.gridColumn159.VisibleIndex = 0;
            this.gridColumn159.Width = 215;
            // 
            // gridColumn160
            // 
            this.gridColumn160.Caption = "ID";
            this.gridColumn160.FieldName = "ID";
            this.gridColumn160.Name = "gridColumn160";
            this.gridColumn160.OptionsColumn.AllowEdit = false;
            this.gridColumn160.OptionsColumn.AllowFocus = false;
            this.gridColumn160.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn161
            // 
            this.gridColumn161.Caption = "Order";
            this.gridColumn161.FieldName = "RecordOrder";
            this.gridColumn161.Name = "gridColumn161";
            this.gridColumn161.OptionsColumn.AllowEdit = false;
            this.gridColumn161.OptionsColumn.AllowFocus = false;
            this.gridColumn161.OptionsColumn.ReadOnly = true;
            // 
            // popupContainerControlDateRange
            // 
            this.popupContainerControlDateRange.Controls.Add(this.groupControl1);
            this.popupContainerControlDateRange.Controls.Add(this.btnDateRangeOK);
            this.popupContainerControlDateRange.Location = new System.Drawing.Point(326, 140);
            this.popupContainerControlDateRange.Name = "popupContainerControlDateRange";
            this.popupContainerControlDateRange.Size = new System.Drawing.Size(200, 105);
            this.popupContainerControlDateRange.TabIndex = 17;
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.dateEditFromDate);
            this.groupControl1.Controls.Add(this.dateEditToDate);
            this.groupControl1.Location = new System.Drawing.Point(3, 3);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(194, 76);
            this.groupControl1.TabIndex = 20;
            this.groupControl1.Text = "Date Range  [Expected Start Date]";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(6, 53);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(16, 13);
            this.labelControl2.TabIndex = 13;
            this.labelControl2.Text = "To:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(6, 27);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(28, 13);
            this.labelControl1.TabIndex = 12;
            this.labelControl1.Text = "From:";
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(40, 24);
            this.dateEditFromDate.MenuManager = this.barManager1;
            this.dateEditFromDate.Name = "dateEditFromDate";
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditFromDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditFromDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullValuePrompt = "No Date";
            this.dateEditFromDate.Size = new System.Drawing.Size(149, 20);
            this.dateEditFromDate.TabIndex = 10;
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(40, 50);
            this.dateEditToDate.MenuManager = this.barManager1;
            this.dateEditToDate.Name = "dateEditToDate";
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditToDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditToDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullValuePrompt = "No Date";
            this.dateEditToDate.Size = new System.Drawing.Size(149, 20);
            this.dateEditToDate.TabIndex = 11;
            // 
            // btnDateRangeOK
            // 
            this.btnDateRangeOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDateRangeOK.Location = new System.Drawing.Point(3, 82);
            this.btnDateRangeOK.Name = "btnDateRangeOK";
            this.btnDateRangeOK.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnDateRangeOK.Size = new System.Drawing.Size(50, 20);
            this.btnDateRangeOK.TabIndex = 19;
            this.btnDateRangeOK.Text = "OK";
            this.btnDateRangeOK.Click += new System.EventHandler(this.btnDateRangeOK_Click);
            // 
            // popupContainerControlContractStatusFilter
            // 
            this.popupContainerControlContractStatusFilter.Controls.Add(this.btnContractStatusFilterOK);
            this.popupContainerControlContractStatusFilter.Controls.Add(this.gridControl2);
            this.popupContainerControlContractStatusFilter.Location = new System.Drawing.Point(181, 121);
            this.popupContainerControlContractStatusFilter.Name = "popupContainerControlContractStatusFilter";
            this.popupContainerControlContractStatusFilter.Size = new System.Drawing.Size(139, 126);
            this.popupContainerControlContractStatusFilter.TabIndex = 6;
            // 
            // btnContractStatusFilterOK
            // 
            this.btnContractStatusFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnContractStatusFilterOK.Location = new System.Drawing.Point(3, 103);
            this.btnContractStatusFilterOK.Name = "btnContractStatusFilterOK";
            this.btnContractStatusFilterOK.Size = new System.Drawing.Size(50, 20);
            this.btnContractStatusFilterOK.TabIndex = 12;
            this.btnContractStatusFilterOK.Text = "OK";
            this.btnContractStatusFilterOK.Click += new System.EventHandler(this.btnContractStatusFilterOK_Click);
            // 
            // gridControl2
            // 
            this.gridControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl2.DataSource = this.sp06051OMContractStatusesBindingSource;
            this.gridControl2.Location = new System.Drawing.Point(3, 3);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(133, 98);
            this.gridControl2.TabIndex = 4;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp06051OMContractStatusesBindingSource
            // 
            this.sp06051OMContractStatusesBindingSource.DataMember = "sp06051_OM_Contract_Statuses";
            this.sp06051OMContractStatusesBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription1,
            this.colID5,
            this.colRecordOrder});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription1
            // 
            this.colDescription1.Caption = "Contract Status";
            this.colDescription1.FieldName = "Description";
            this.colDescription1.Name = "colDescription1";
            this.colDescription1.OptionsColumn.AllowEdit = false;
            this.colDescription1.OptionsColumn.AllowFocus = false;
            this.colDescription1.OptionsColumn.ReadOnly = true;
            this.colDescription1.Visible = true;
            this.colDescription1.VisibleIndex = 0;
            this.colDescription1.Width = 215;
            // 
            // colID5
            // 
            this.colID5.Caption = "ID";
            this.colID5.FieldName = "ID";
            this.colID5.Name = "colID5";
            this.colID5.OptionsColumn.AllowEdit = false;
            this.colID5.OptionsColumn.AllowFocus = false;
            this.colID5.OptionsColumn.ReadOnly = true;
            // 
            // colRecordOrder
            // 
            this.colRecordOrder.Caption = "Order";
            this.colRecordOrder.FieldName = "RecordOrder";
            this.colRecordOrder.Name = "colRecordOrder";
            this.colRecordOrder.OptionsColumn.AllowEdit = false;
            this.colRecordOrder.OptionsColumn.AllowFocus = false;
            this.colRecordOrder.OptionsColumn.ReadOnly = true;
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 1);
            this.standaloneBarDockControl1.Manager = this.barManager1;
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(1002, 42);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPageYears;
            this.xtraTabControl1.Size = new System.Drawing.Size(1004, 246);
            this.xtraTabControl1.TabIndex = 1;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageYears,
            this.xtraTabPageResponsibilities,
            this.xtraTabPageClientPOs,
            this.xtraTabPageBillingRequirements});
            // 
            // xtraTabPageYears
            // 
            this.xtraTabPageYears.Controls.Add(this.splitContainerControl2);
            this.xtraTabPageYears.Name = "xtraTabPageYears";
            this.xtraTabPageYears.Size = new System.Drawing.Size(999, 220);
            this.xtraTabPageYears.Text = "Years \\ Billing Profile";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl2.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl2.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl2.Panel1.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl2.Panel1.Controls.Add(this.gridControlYear);
            this.splitContainerControl2.Panel1.ShowCaption = true;
            this.splitContainerControl2.Panel1.Text = "Contract Years ";
            this.splitContainerControl2.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl2.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl2.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl2.Panel2.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl2.Panel2.Controls.Add(this.gridControlProfile);
            this.splitContainerControl2.Panel2.ShowCaption = true;
            this.splitContainerControl2.Panel2.Text = "Contract Year Billing Profiles ";
            this.splitContainerControl2.Size = new System.Drawing.Size(999, 220);
            this.splitContainerControl2.SplitterPosition = 450;
            this.splitContainerControl2.TabIndex = 0;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // gridControlYear
            // 
            this.gridControlYear.DataSource = this.sp06055OMClientContractManagerLinkedYearsBindingSource;
            this.gridControlYear.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlYear.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlYear.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlYear.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlYear.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlYear.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlYear.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlYear.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlYear.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlYear.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlYear.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlYear.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlYear.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selcted Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 8, true, true, "Linked Documents", "linked_document")});
            this.gridControlYear.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlYear_EmbeddedNavigator_ButtonClick);
            this.gridControlYear.Location = new System.Drawing.Point(0, 0);
            this.gridControlYear.MainView = this.gridViewYear;
            this.gridControlYear.MenuManager = this.barManager1;
            this.gridControlYear.Name = "gridControlYear";
            this.gridControlYear.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2,
            this.repositoryItemCheckEdit2,
            this.repositoryItemTextEditDateTime2,
            this.repositoryItemTextEditMoney2,
            this.repositoryItemHyperLinkEditLinkedDocumentsClientContractYear,
            this.repositoryItemTextEditPercentage2,
            this.repositoryItemTextEditHTML3});
            this.gridControlYear.Size = new System.Drawing.Size(425, 217);
            this.gridControlYear.TabIndex = 5;
            this.gridControlYear.UseEmbeddedNavigator = true;
            this.gridControlYear.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewYear});
            // 
            // sp06055OMClientContractManagerLinkedYearsBindingSource
            // 
            this.sp06055OMClientContractManagerLinkedYearsBindingSource.DataMember = "sp06055_OM_Client_Contract_Manager_Linked_Years";
            this.sp06055OMClientContractManagerLinkedYearsBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // gridViewYear
            // 
            this.gridViewYear.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientContractYearID,
            this.colClientContractID1,
            this.colYearDescription,
            this.colStartDate1,
            this.colEndDate1,
            this.colActive1,
            this.colClientValue,
            this.colRemarks1,
            this.colClientID1,
            this.colClientName1,
            this.colContractStartDate,
            this.colContractEndDate,
            this.colLinkedToParent,
            this.colLinkedDocumentCount1,
            this.colYearlyPercentageIncrease1});
            this.gridViewYear.GridControl = this.gridControlYear;
            this.gridViewYear.GroupCount = 1;
            this.gridViewYear.Name = "gridViewYear";
            this.gridViewYear.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewYear.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewYear.OptionsLayout.StoreAppearance = true;
            this.gridViewYear.OptionsLayout.StoreFormatRules = true;
            this.gridViewYear.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewYear.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewYear.OptionsSelection.MultiSelect = true;
            this.gridViewYear.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewYear.OptionsView.ColumnAutoWidth = false;
            this.gridViewYear.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewYear.OptionsView.ShowGroupPanel = false;
            this.gridViewYear.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToParent, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colStartDate1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewYear.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewYear_CustomRowCellEdit);
            this.gridViewYear.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewYear.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewYear.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewYear.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewYear_ShowingEditor);
            this.gridViewYear.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewYear.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewYear.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewYear.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewYear_MouseUp);
            this.gridViewYear.DoubleClick += new System.EventHandler(this.gridViewYear_DoubleClick);
            this.gridViewYear.GotFocus += new System.EventHandler(this.gridViewYear_GotFocus);
            // 
            // colClientContractYearID
            // 
            this.colClientContractYearID.Caption = "Client Contract Year ID";
            this.colClientContractYearID.FieldName = "ClientContractYearID";
            this.colClientContractYearID.Name = "colClientContractYearID";
            this.colClientContractYearID.OptionsColumn.AllowEdit = false;
            this.colClientContractYearID.OptionsColumn.AllowFocus = false;
            this.colClientContractYearID.OptionsColumn.ReadOnly = true;
            this.colClientContractYearID.Width = 132;
            // 
            // colClientContractID1
            // 
            this.colClientContractID1.Caption = "Client Contract ID";
            this.colClientContractID1.FieldName = "ClientContractID";
            this.colClientContractID1.Name = "colClientContractID1";
            this.colClientContractID1.OptionsColumn.AllowEdit = false;
            this.colClientContractID1.OptionsColumn.AllowFocus = false;
            this.colClientContractID1.OptionsColumn.ReadOnly = true;
            this.colClientContractID1.Width = 107;
            // 
            // colYearDescription
            // 
            this.colYearDescription.Caption = "Year Description";
            this.colYearDescription.FieldName = "YearDescription";
            this.colYearDescription.Name = "colYearDescription";
            this.colYearDescription.OptionsColumn.AllowEdit = false;
            this.colYearDescription.OptionsColumn.AllowFocus = false;
            this.colYearDescription.OptionsColumn.ReadOnly = true;
            this.colYearDescription.Visible = true;
            this.colYearDescription.VisibleIndex = 2;
            this.colYearDescription.Width = 157;
            // 
            // colStartDate1
            // 
            this.colStartDate1.Caption = "Start Date";
            this.colStartDate1.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colStartDate1.FieldName = "StartDate";
            this.colStartDate1.Name = "colStartDate1";
            this.colStartDate1.OptionsColumn.AllowEdit = false;
            this.colStartDate1.OptionsColumn.AllowFocus = false;
            this.colStartDate1.OptionsColumn.ReadOnly = true;
            this.colStartDate1.Visible = true;
            this.colStartDate1.VisibleIndex = 0;
            this.colStartDate1.Width = 118;
            // 
            // repositoryItemTextEditDateTime2
            // 
            this.repositoryItemTextEditDateTime2.AutoHeight = false;
            this.repositoryItemTextEditDateTime2.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime2.Name = "repositoryItemTextEditDateTime2";
            // 
            // colEndDate1
            // 
            this.colEndDate1.Caption = "End Date";
            this.colEndDate1.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colEndDate1.FieldName = "EndDate";
            this.colEndDate1.Name = "colEndDate1";
            this.colEndDate1.OptionsColumn.AllowEdit = false;
            this.colEndDate1.OptionsColumn.AllowFocus = false;
            this.colEndDate1.OptionsColumn.ReadOnly = true;
            this.colEndDate1.Visible = true;
            this.colEndDate1.VisibleIndex = 1;
            this.colEndDate1.Width = 100;
            // 
            // colActive1
            // 
            this.colActive1.Caption = "Active";
            this.colActive1.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colActive1.FieldName = "Active";
            this.colActive1.Name = "colActive1";
            this.colActive1.OptionsColumn.AllowEdit = false;
            this.colActive1.OptionsColumn.AllowFocus = false;
            this.colActive1.OptionsColumn.ReadOnly = true;
            this.colActive1.Visible = true;
            this.colActive1.VisibleIndex = 3;
            this.colActive1.Width = 51;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // colClientValue
            // 
            this.colClientValue.Caption = "Client Value";
            this.colClientValue.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colClientValue.FieldName = "ClientValue";
            this.colClientValue.Name = "colClientValue";
            this.colClientValue.OptionsColumn.AllowEdit = false;
            this.colClientValue.OptionsColumn.AllowFocus = false;
            this.colClientValue.OptionsColumn.ReadOnly = true;
            this.colClientValue.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ClientValue", "{0:C}")});
            this.colClientValue.Visible = true;
            this.colClientValue.VisibleIndex = 5;
            this.colClientValue.Width = 77;
            // 
            // repositoryItemTextEditMoney2
            // 
            this.repositoryItemTextEditMoney2.AutoHeight = false;
            this.repositoryItemTextEditMoney2.Mask.EditMask = "c";
            this.repositoryItemTextEditMoney2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditMoney2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditMoney2.Name = "repositoryItemTextEditMoney2";
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 7;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colClientID1
            // 
            this.colClientID1.Caption = "Client ID";
            this.colClientID1.FieldName = "ClientID";
            this.colClientID1.Name = "colClientID1";
            this.colClientID1.OptionsColumn.AllowEdit = false;
            this.colClientID1.OptionsColumn.AllowFocus = false;
            this.colClientID1.OptionsColumn.ReadOnly = true;
            // 
            // colClientName1
            // 
            this.colClientName1.Caption = "Client Name";
            this.colClientName1.FieldName = "ClientName";
            this.colClientName1.Name = "colClientName1";
            this.colClientName1.OptionsColumn.AllowEdit = false;
            this.colClientName1.OptionsColumn.AllowFocus = false;
            this.colClientName1.OptionsColumn.ReadOnly = true;
            this.colClientName1.Width = 145;
            // 
            // colContractStartDate
            // 
            this.colContractStartDate.Caption = "Contract Start Date";
            this.colContractStartDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colContractStartDate.FieldName = "ContractStartDate";
            this.colContractStartDate.Name = "colContractStartDate";
            this.colContractStartDate.OptionsColumn.AllowEdit = false;
            this.colContractStartDate.OptionsColumn.AllowFocus = false;
            this.colContractStartDate.OptionsColumn.ReadOnly = true;
            this.colContractStartDate.Width = 116;
            // 
            // colContractEndDate
            // 
            this.colContractEndDate.Caption = "Contract End Date";
            this.colContractEndDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colContractEndDate.FieldName = "ContractEndDate";
            this.colContractEndDate.Name = "colContractEndDate";
            this.colContractEndDate.OptionsColumn.AllowEdit = false;
            this.colContractEndDate.OptionsColumn.AllowFocus = false;
            this.colContractEndDate.OptionsColumn.ReadOnly = true;
            this.colContractEndDate.Width = 110;
            // 
            // colLinkedToParent
            // 
            this.colLinkedToParent.Caption = "Linked To Contract";
            this.colLinkedToParent.ColumnEdit = this.repositoryItemTextEditHTML3;
            this.colLinkedToParent.FieldName = "LinkedToParent";
            this.colLinkedToParent.Name = "colLinkedToParent";
            this.colLinkedToParent.OptionsColumn.AllowEdit = false;
            this.colLinkedToParent.OptionsColumn.AllowFocus = false;
            this.colLinkedToParent.OptionsColumn.ReadOnly = true;
            this.colLinkedToParent.Visible = true;
            this.colLinkedToParent.VisibleIndex = 8;
            this.colLinkedToParent.Width = 407;
            // 
            // repositoryItemTextEditHTML3
            // 
            this.repositoryItemTextEditHTML3.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTextEditHTML3.AutoHeight = false;
            this.repositoryItemTextEditHTML3.Name = "repositoryItemTextEditHTML3";
            // 
            // colLinkedDocumentCount1
            // 
            this.colLinkedDocumentCount1.Caption = "Linked Documents";
            this.colLinkedDocumentCount1.ColumnEdit = this.repositoryItemHyperLinkEditLinkedDocumentsClientContractYear;
            this.colLinkedDocumentCount1.FieldName = "LinkedDocumentCount";
            this.colLinkedDocumentCount1.Name = "colLinkedDocumentCount1";
            this.colLinkedDocumentCount1.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentCount1.Visible = true;
            this.colLinkedDocumentCount1.VisibleIndex = 6;
            this.colLinkedDocumentCount1.Width = 107;
            // 
            // repositoryItemHyperLinkEditLinkedDocumentsClientContractYear
            // 
            this.repositoryItemHyperLinkEditLinkedDocumentsClientContractYear.AutoHeight = false;
            this.repositoryItemHyperLinkEditLinkedDocumentsClientContractYear.Name = "repositoryItemHyperLinkEditLinkedDocumentsClientContractYear";
            this.repositoryItemHyperLinkEditLinkedDocumentsClientContractYear.SingleClick = true;
            this.repositoryItemHyperLinkEditLinkedDocumentsClientContractYear.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditLinkedDocumentsClientContractYear_OpenLink);
            // 
            // colYearlyPercentageIncrease1
            // 
            this.colYearlyPercentageIncrease1.Caption = "Yearly % Increase";
            this.colYearlyPercentageIncrease1.ColumnEdit = this.repositoryItemTextEditPercentage2;
            this.colYearlyPercentageIncrease1.FieldName = "YearlyPercentageIncrease";
            this.colYearlyPercentageIncrease1.Name = "colYearlyPercentageIncrease1";
            this.colYearlyPercentageIncrease1.OptionsColumn.AllowEdit = false;
            this.colYearlyPercentageIncrease1.OptionsColumn.AllowFocus = false;
            this.colYearlyPercentageIncrease1.OptionsColumn.ReadOnly = true;
            this.colYearlyPercentageIncrease1.Visible = true;
            this.colYearlyPercentageIncrease1.VisibleIndex = 4;
            this.colYearlyPercentageIncrease1.Width = 110;
            // 
            // repositoryItemTextEditPercentage2
            // 
            this.repositoryItemTextEditPercentage2.AutoHeight = false;
            this.repositoryItemTextEditPercentage2.Mask.EditMask = "P2";
            this.repositoryItemTextEditPercentage2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage2.Name = "repositoryItemTextEditPercentage2";
            // 
            // gridControlProfile
            // 
            this.gridControlProfile.DataSource = this.sp06056OMClientContractManagerLinkedProfilesBindingSource;
            this.gridControlProfile.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlProfile.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlProfile.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlProfile.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlProfile.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlProfile.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlProfile.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlProfile.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlProfile.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlProfile.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlProfile.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlProfile.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlProfile.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selcted Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 9, true, true, "Add from Template", "add_from_template"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 10, true, true, "Template Manager", "template_manager")});
            this.gridControlProfile.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlProfile_EmbeddedNavigator_ButtonClick);
            this.gridControlProfile.Location = new System.Drawing.Point(0, 0);
            this.gridControlProfile.MainView = this.gridViewProfile;
            this.gridControlProfile.MenuManager = this.barManager1;
            this.gridControlProfile.Name = "gridControlProfile";
            this.gridControlProfile.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3,
            this.repositoryItemCheckEdit3,
            this.repositoryItemTextEditDateEdit3,
            this.repositoryItemTextEditCurrency3,
            this.repositoryItemTextEditHTML1});
            this.gridControlProfile.Size = new System.Drawing.Size(518, 217);
            this.gridControlProfile.TabIndex = 6;
            this.gridControlProfile.UseEmbeddedNavigator = true;
            this.gridControlProfile.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewProfile});
            // 
            // sp06056OMClientContractManagerLinkedProfilesBindingSource
            // 
            this.sp06056OMClientContractManagerLinkedProfilesBindingSource.DataMember = "sp06056_OM_Client_Contract_Manager_Linked_Profiles";
            this.sp06056OMClientContractManagerLinkedProfilesBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // gridViewProfile
            // 
            this.gridViewProfile.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientContractProfileID,
            this.colClientContractYearID1,
            this.colInvoiceDateDue,
            this.colInvoiceDateActual,
            this.colEstimatedBillAmount,
            this.colActualBillAmount,
            this.colBilledByPersonID,
            this.colRemarks2,
            this.colClientContractID2,
            this.colYearDescription1,
            this.colYearStartDate,
            this.colYearEndDate,
            this.colYearActive,
            this.colClientID2,
            this.colClientName2,
            this.colContractStartDate1,
            this.colContractEndDate1,
            this.colLinkedToParent1,
            this.colBilledByPerson});
            this.gridViewProfile.GridControl = this.gridControlProfile;
            this.gridViewProfile.GroupCount = 1;
            this.gridViewProfile.Name = "gridViewProfile";
            this.gridViewProfile.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewProfile.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewProfile.OptionsLayout.StoreAppearance = true;
            this.gridViewProfile.OptionsLayout.StoreFormatRules = true;
            this.gridViewProfile.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewProfile.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewProfile.OptionsSelection.MultiSelect = true;
            this.gridViewProfile.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewProfile.OptionsView.ColumnAutoWidth = false;
            this.gridViewProfile.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewProfile.OptionsView.ShowGroupPanel = false;
            this.gridViewProfile.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToParent1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colInvoiceDateDue, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewProfile.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewProfile.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewProfile.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewProfile.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewProfile.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewProfile.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewProfile.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewProfile_MouseUp);
            this.gridViewProfile.DoubleClick += new System.EventHandler(this.gridViewYear_DoubleClick);
            this.gridViewProfile.GotFocus += new System.EventHandler(this.gridViewProfile_GotFocus);
            // 
            // colClientContractProfileID
            // 
            this.colClientContractProfileID.Caption = "Client Contract Profile ID";
            this.colClientContractProfileID.FieldName = "ClientContractProfileID";
            this.colClientContractProfileID.Name = "colClientContractProfileID";
            this.colClientContractProfileID.OptionsColumn.AllowEdit = false;
            this.colClientContractProfileID.OptionsColumn.AllowFocus = false;
            this.colClientContractProfileID.OptionsColumn.ReadOnly = true;
            this.colClientContractProfileID.Width = 153;
            // 
            // colClientContractYearID1
            // 
            this.colClientContractYearID1.Caption = "Client Contract Year ID";
            this.colClientContractYearID1.FieldName = "ClientContractYearID";
            this.colClientContractYearID1.Name = "colClientContractYearID1";
            this.colClientContractYearID1.OptionsColumn.AllowEdit = false;
            this.colClientContractYearID1.OptionsColumn.AllowFocus = false;
            this.colClientContractYearID1.OptionsColumn.ReadOnly = true;
            this.colClientContractYearID1.Width = 132;
            // 
            // colInvoiceDateDue
            // 
            this.colInvoiceDateDue.Caption = "Invoice Due";
            this.colInvoiceDateDue.ColumnEdit = this.repositoryItemTextEditDateEdit3;
            this.colInvoiceDateDue.FieldName = "InvoiceDateDue";
            this.colInvoiceDateDue.Name = "colInvoiceDateDue";
            this.colInvoiceDateDue.OptionsColumn.AllowEdit = false;
            this.colInvoiceDateDue.OptionsColumn.AllowFocus = false;
            this.colInvoiceDateDue.OptionsColumn.ReadOnly = true;
            this.colInvoiceDateDue.Visible = true;
            this.colInvoiceDateDue.VisibleIndex = 0;
            this.colInvoiceDateDue.Width = 114;
            // 
            // repositoryItemTextEditDateEdit3
            // 
            this.repositoryItemTextEditDateEdit3.AutoHeight = false;
            this.repositoryItemTextEditDateEdit3.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateEdit3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateEdit3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateEdit3.Name = "repositoryItemTextEditDateEdit3";
            // 
            // colInvoiceDateActual
            // 
            this.colInvoiceDateActual.Caption = "Invoice Actual";
            this.colInvoiceDateActual.ColumnEdit = this.repositoryItemTextEditDateEdit3;
            this.colInvoiceDateActual.FieldName = "InvoiceDateActual";
            this.colInvoiceDateActual.Name = "colInvoiceDateActual";
            this.colInvoiceDateActual.OptionsColumn.AllowEdit = false;
            this.colInvoiceDateActual.OptionsColumn.AllowFocus = false;
            this.colInvoiceDateActual.OptionsColumn.ReadOnly = true;
            this.colInvoiceDateActual.Visible = true;
            this.colInvoiceDateActual.VisibleIndex = 1;
            this.colInvoiceDateActual.Width = 100;
            // 
            // colEstimatedBillAmount
            // 
            this.colEstimatedBillAmount.Caption = "Estimated Bill";
            this.colEstimatedBillAmount.ColumnEdit = this.repositoryItemTextEditCurrency3;
            this.colEstimatedBillAmount.FieldName = "EstimatedBillAmount";
            this.colEstimatedBillAmount.Name = "colEstimatedBillAmount";
            this.colEstimatedBillAmount.OptionsColumn.AllowEdit = false;
            this.colEstimatedBillAmount.OptionsColumn.AllowFocus = false;
            this.colEstimatedBillAmount.OptionsColumn.ReadOnly = true;
            this.colEstimatedBillAmount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "EstimatedBillAmount", "{0:C}")});
            this.colEstimatedBillAmount.Visible = true;
            this.colEstimatedBillAmount.VisibleIndex = 2;
            this.colEstimatedBillAmount.Width = 83;
            // 
            // repositoryItemTextEditCurrency3
            // 
            this.repositoryItemTextEditCurrency3.AutoHeight = false;
            this.repositoryItemTextEditCurrency3.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency3.Name = "repositoryItemTextEditCurrency3";
            // 
            // colActualBillAmount
            // 
            this.colActualBillAmount.Caption = "Actual Bill";
            this.colActualBillAmount.ColumnEdit = this.repositoryItemTextEditCurrency3;
            this.colActualBillAmount.FieldName = "ActualBillAmount";
            this.colActualBillAmount.Name = "colActualBillAmount";
            this.colActualBillAmount.OptionsColumn.AllowEdit = false;
            this.colActualBillAmount.OptionsColumn.AllowFocus = false;
            this.colActualBillAmount.OptionsColumn.ReadOnly = true;
            this.colActualBillAmount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ActualBillAmount", "{0:C}")});
            this.colActualBillAmount.Visible = true;
            this.colActualBillAmount.VisibleIndex = 3;
            // 
            // colBilledByPersonID
            // 
            this.colBilledByPersonID.Caption = "Billed By Person ID";
            this.colBilledByPersonID.FieldName = "BilledByPersonID";
            this.colBilledByPersonID.Name = "colBilledByPersonID";
            this.colBilledByPersonID.OptionsColumn.AllowEdit = false;
            this.colBilledByPersonID.OptionsColumn.AllowFocus = false;
            this.colBilledByPersonID.OptionsColumn.ReadOnly = true;
            this.colBilledByPersonID.Width = 110;
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.ReadOnly = true;
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 5;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colClientContractID2
            // 
            this.colClientContractID2.Caption = "Client Contract ID";
            this.colClientContractID2.FieldName = "ClientContractID";
            this.colClientContractID2.Name = "colClientContractID2";
            this.colClientContractID2.OptionsColumn.AllowEdit = false;
            this.colClientContractID2.OptionsColumn.AllowFocus = false;
            this.colClientContractID2.OptionsColumn.ReadOnly = true;
            this.colClientContractID2.Width = 107;
            // 
            // colYearDescription1
            // 
            this.colYearDescription1.Caption = "Year Description";
            this.colYearDescription1.FieldName = "YearDescription";
            this.colYearDescription1.Name = "colYearDescription1";
            this.colYearDescription1.OptionsColumn.AllowEdit = false;
            this.colYearDescription1.OptionsColumn.AllowFocus = false;
            this.colYearDescription1.OptionsColumn.ReadOnly = true;
            this.colYearDescription1.Width = 99;
            // 
            // colYearStartDate
            // 
            this.colYearStartDate.Caption = "Year Start";
            this.colYearStartDate.ColumnEdit = this.repositoryItemTextEditDateEdit3;
            this.colYearStartDate.FieldName = "YearStartDate";
            this.colYearStartDate.Name = "colYearStartDate";
            this.colYearStartDate.OptionsColumn.AllowEdit = false;
            this.colYearStartDate.OptionsColumn.AllowFocus = false;
            this.colYearStartDate.OptionsColumn.ReadOnly = true;
            // 
            // colYearEndDate
            // 
            this.colYearEndDate.Caption = "Year End";
            this.colYearEndDate.ColumnEdit = this.repositoryItemTextEditDateEdit3;
            this.colYearEndDate.FieldName = "YearEndDate";
            this.colYearEndDate.Name = "colYearEndDate";
            this.colYearEndDate.OptionsColumn.AllowEdit = false;
            this.colYearEndDate.OptionsColumn.AllowFocus = false;
            this.colYearEndDate.OptionsColumn.ReadOnly = true;
            // 
            // colYearActive
            // 
            this.colYearActive.Caption = "Year Active";
            this.colYearActive.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colYearActive.FieldName = "YearActive";
            this.colYearActive.Name = "colYearActive";
            this.colYearActive.OptionsColumn.AllowEdit = false;
            this.colYearActive.OptionsColumn.AllowFocus = false;
            this.colYearActive.OptionsColumn.ReadOnly = true;
            this.colYearActive.Width = 76;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Caption = "Check";
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = 1;
            this.repositoryItemCheckEdit3.ValueUnchecked = 0;
            // 
            // colClientID2
            // 
            this.colClientID2.Caption = "Client ID";
            this.colClientID2.FieldName = "ClientID";
            this.colClientID2.Name = "colClientID2";
            this.colClientID2.OptionsColumn.AllowEdit = false;
            this.colClientID2.OptionsColumn.AllowFocus = false;
            this.colClientID2.OptionsColumn.ReadOnly = true;
            // 
            // colClientName2
            // 
            this.colClientName2.Caption = "Client Name";
            this.colClientName2.FieldName = "ClientName";
            this.colClientName2.Name = "colClientName2";
            this.colClientName2.OptionsColumn.AllowEdit = false;
            this.colClientName2.OptionsColumn.AllowFocus = false;
            this.colClientName2.OptionsColumn.ReadOnly = true;
            this.colClientName2.Width = 173;
            // 
            // colContractStartDate1
            // 
            this.colContractStartDate1.Caption = "Contract Start Date";
            this.colContractStartDate1.ColumnEdit = this.repositoryItemTextEditDateEdit3;
            this.colContractStartDate1.FieldName = "ContractStartDate";
            this.colContractStartDate1.Name = "colContractStartDate1";
            this.colContractStartDate1.OptionsColumn.AllowEdit = false;
            this.colContractStartDate1.OptionsColumn.AllowFocus = false;
            this.colContractStartDate1.OptionsColumn.ReadOnly = true;
            this.colContractStartDate1.Width = 116;
            // 
            // colContractEndDate1
            // 
            this.colContractEndDate1.Caption = "Contract End Date";
            this.colContractEndDate1.ColumnEdit = this.repositoryItemTextEditDateEdit3;
            this.colContractEndDate1.FieldName = "ContractEndDate";
            this.colContractEndDate1.Name = "colContractEndDate1";
            this.colContractEndDate1.OptionsColumn.AllowEdit = false;
            this.colContractEndDate1.OptionsColumn.AllowFocus = false;
            this.colContractEndDate1.OptionsColumn.ReadOnly = true;
            this.colContractEndDate1.Width = 110;
            // 
            // colLinkedToParent1
            // 
            this.colLinkedToParent1.Caption = "Linked To Year";
            this.colLinkedToParent1.ColumnEdit = this.repositoryItemTextEditHTML1;
            this.colLinkedToParent1.FieldName = "LinkedToParent";
            this.colLinkedToParent1.Name = "colLinkedToParent1";
            this.colLinkedToParent1.OptionsColumn.AllowEdit = false;
            this.colLinkedToParent1.OptionsColumn.AllowFocus = false;
            this.colLinkedToParent1.OptionsColumn.ReadOnly = true;
            this.colLinkedToParent1.Visible = true;
            this.colLinkedToParent1.VisibleIndex = 6;
            this.colLinkedToParent1.Width = 524;
            // 
            // repositoryItemTextEditHTML1
            // 
            this.repositoryItemTextEditHTML1.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTextEditHTML1.AutoHeight = false;
            this.repositoryItemTextEditHTML1.Name = "repositoryItemTextEditHTML1";
            // 
            // colBilledByPerson
            // 
            this.colBilledByPerson.Caption = "Billed By Person";
            this.colBilledByPerson.FieldName = "BilledByPerson";
            this.colBilledByPerson.Name = "colBilledByPerson";
            this.colBilledByPerson.OptionsColumn.AllowEdit = false;
            this.colBilledByPerson.OptionsColumn.AllowFocus = false;
            this.colBilledByPerson.OptionsColumn.ReadOnly = true;
            this.colBilledByPerson.Visible = true;
            this.colBilledByPerson.VisibleIndex = 4;
            this.colBilledByPerson.Width = 161;
            // 
            // xtraTabPageResponsibilities
            // 
            this.xtraTabPageResponsibilities.Controls.Add(this.gridControlResponsibility);
            this.xtraTabPageResponsibilities.Name = "xtraTabPageResponsibilities";
            this.xtraTabPageResponsibilities.Size = new System.Drawing.Size(999, 220);
            this.xtraTabPageResponsibilities.Text = "Person Responsibilities";
            // 
            // gridControlResponsibility
            // 
            this.gridControlResponsibility.DataSource = this.sp06057OMClientContractManagerLinkedResponsibilitiesBindingSource;
            this.gridControlResponsibility.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlResponsibility.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlResponsibility.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlResponsibility.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlResponsibility.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlResponsibility.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlResponsibility.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlResponsibility.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlResponsibility.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlResponsibility.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlResponsibility.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlResponsibility.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlResponsibility.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selcted Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "View Selected Record(s)", "view")});
            this.gridControlResponsibility.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlResponsibility_EmbeddedNavigator_ButtonClick);
            this.gridControlResponsibility.Location = new System.Drawing.Point(0, 0);
            this.gridControlResponsibility.MainView = this.gridViewResponsibility;
            this.gridControlResponsibility.MenuManager = this.barManager1;
            this.gridControlResponsibility.Name = "gridControlResponsibility";
            this.gridControlResponsibility.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4,
            this.repositoryItemTextEditDateTime4,
            this.repositoryItemTextEditHTML4});
            this.gridControlResponsibility.Size = new System.Drawing.Size(999, 220);
            this.gridControlResponsibility.TabIndex = 6;
            this.gridControlResponsibility.UseEmbeddedNavigator = true;
            this.gridControlResponsibility.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewResponsibility});
            // 
            // sp06057OMClientContractManagerLinkedResponsibilitiesBindingSource
            // 
            this.sp06057OMClientContractManagerLinkedResponsibilitiesBindingSource.DataMember = "sp06057_OM_Client_Contract_Manager_Linked_Responsibilities";
            this.sp06057OMClientContractManagerLinkedResponsibilitiesBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // gridViewResponsibility
            // 
            this.gridViewResponsibility.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPersonResponsibilityID,
            this.colStaffID,
            this.colResponsibleForRecordID,
            this.colResponsibleForRecordTypeID,
            this.colResponsibilityTypeID,
            this.colRemarks3,
            this.colClientID3,
            this.colClientName3,
            this.colContractStartDate2,
            this.colContractEndDate2,
            this.colLinkedToParent2,
            this.colResponsibilityType,
            this.colStaffName,
            this.colPersonTypeDescription,
            this.colPersonTypeID});
            this.gridViewResponsibility.GridControl = this.gridControlResponsibility;
            this.gridViewResponsibility.GroupCount = 1;
            this.gridViewResponsibility.Name = "gridViewResponsibility";
            this.gridViewResponsibility.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewResponsibility.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewResponsibility.OptionsLayout.StoreAppearance = true;
            this.gridViewResponsibility.OptionsLayout.StoreFormatRules = true;
            this.gridViewResponsibility.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewResponsibility.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewResponsibility.OptionsSelection.MultiSelect = true;
            this.gridViewResponsibility.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewResponsibility.OptionsView.ColumnAutoWidth = false;
            this.gridViewResponsibility.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewResponsibility.OptionsView.ShowGroupPanel = false;
            this.gridViewResponsibility.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToParent2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colResponsibilityType, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colStaffName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewResponsibility.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewResponsibility.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewResponsibility.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewResponsibility.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewResponsibility.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewResponsibility.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewResponsibility.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewResponsibility_MouseUp);
            this.gridViewResponsibility.DoubleClick += new System.EventHandler(this.gridViewResponsibility_DoubleClick);
            this.gridViewResponsibility.GotFocus += new System.EventHandler(this.gridViewResponsibility_GotFocus);
            // 
            // colPersonResponsibilityID
            // 
            this.colPersonResponsibilityID.Caption = "Person Responsibility ID";
            this.colPersonResponsibilityID.FieldName = "PersonResponsibilityID";
            this.colPersonResponsibilityID.Name = "colPersonResponsibilityID";
            this.colPersonResponsibilityID.OptionsColumn.AllowEdit = false;
            this.colPersonResponsibilityID.OptionsColumn.AllowFocus = false;
            this.colPersonResponsibilityID.OptionsColumn.ReadOnly = true;
            this.colPersonResponsibilityID.Width = 136;
            // 
            // colStaffID
            // 
            this.colStaffID.Caption = "Staff ID";
            this.colStaffID.FieldName = "StaffID";
            this.colStaffID.Name = "colStaffID";
            this.colStaffID.OptionsColumn.AllowEdit = false;
            this.colStaffID.OptionsColumn.AllowFocus = false;
            this.colStaffID.OptionsColumn.ReadOnly = true;
            // 
            // colResponsibleForRecordID
            // 
            this.colResponsibleForRecordID.Caption = "Responsible For Record ID";
            this.colResponsibleForRecordID.FieldName = "ResponsibleForRecordID";
            this.colResponsibleForRecordID.Name = "colResponsibleForRecordID";
            this.colResponsibleForRecordID.OptionsColumn.AllowEdit = false;
            this.colResponsibleForRecordID.OptionsColumn.AllowFocus = false;
            this.colResponsibleForRecordID.OptionsColumn.ReadOnly = true;
            this.colResponsibleForRecordID.Width = 148;
            // 
            // colResponsibleForRecordTypeID
            // 
            this.colResponsibleForRecordTypeID.Caption = "Responsible For Record Type ID";
            this.colResponsibleForRecordTypeID.FieldName = "ResponsibleForRecordTypeID";
            this.colResponsibleForRecordTypeID.Name = "colResponsibleForRecordTypeID";
            this.colResponsibleForRecordTypeID.OptionsColumn.AllowEdit = false;
            this.colResponsibleForRecordTypeID.OptionsColumn.AllowFocus = false;
            this.colResponsibleForRecordTypeID.OptionsColumn.ReadOnly = true;
            this.colResponsibleForRecordTypeID.Width = 175;
            // 
            // colResponsibilityTypeID
            // 
            this.colResponsibilityTypeID.Caption = "Responsibility Type ID";
            this.colResponsibilityTypeID.FieldName = "ResponsibilityTypeID";
            this.colResponsibilityTypeID.Name = "colResponsibilityTypeID";
            this.colResponsibilityTypeID.OptionsColumn.AllowEdit = false;
            this.colResponsibilityTypeID.OptionsColumn.AllowFocus = false;
            this.colResponsibilityTypeID.OptionsColumn.ReadOnly = true;
            this.colResponsibilityTypeID.Width = 127;
            // 
            // colRemarks3
            // 
            this.colRemarks3.Caption = "Remarks";
            this.colRemarks3.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.colRemarks3.FieldName = "Remarks";
            this.colRemarks3.Name = "colRemarks3";
            this.colRemarks3.OptionsColumn.ReadOnly = true;
            this.colRemarks3.Visible = true;
            this.colRemarks3.VisibleIndex = 3;
            this.colRemarks3.Width = 179;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // colClientID3
            // 
            this.colClientID3.Caption = "Client ID";
            this.colClientID3.FieldName = "ClientID";
            this.colClientID3.Name = "colClientID3";
            this.colClientID3.OptionsColumn.AllowEdit = false;
            this.colClientID3.OptionsColumn.AllowFocus = false;
            this.colClientID3.OptionsColumn.ReadOnly = true;
            // 
            // colClientName3
            // 
            this.colClientName3.Caption = "Client Name";
            this.colClientName3.FieldName = "ClientName";
            this.colClientName3.Name = "colClientName3";
            this.colClientName3.OptionsColumn.AllowEdit = false;
            this.colClientName3.OptionsColumn.AllowFocus = false;
            this.colClientName3.OptionsColumn.ReadOnly = true;
            this.colClientName3.Width = 169;
            // 
            // colContractStartDate2
            // 
            this.colContractStartDate2.Caption = "Contract Start";
            this.colContractStartDate2.ColumnEdit = this.repositoryItemTextEditDateTime4;
            this.colContractStartDate2.FieldName = "ContractStartDate";
            this.colContractStartDate2.Name = "colContractStartDate2";
            this.colContractStartDate2.OptionsColumn.AllowEdit = false;
            this.colContractStartDate2.OptionsColumn.AllowFocus = false;
            this.colContractStartDate2.OptionsColumn.ReadOnly = true;
            this.colContractStartDate2.Width = 106;
            // 
            // repositoryItemTextEditDateTime4
            // 
            this.repositoryItemTextEditDateTime4.AutoHeight = false;
            this.repositoryItemTextEditDateTime4.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime4.Name = "repositoryItemTextEditDateTime4";
            // 
            // colContractEndDate2
            // 
            this.colContractEndDate2.Caption = "Contract End";
            this.colContractEndDate2.ColumnEdit = this.repositoryItemTextEditDateTime4;
            this.colContractEndDate2.FieldName = "ContractEndDate";
            this.colContractEndDate2.Name = "colContractEndDate2";
            this.colContractEndDate2.OptionsColumn.AllowEdit = false;
            this.colContractEndDate2.OptionsColumn.AllowFocus = false;
            this.colContractEndDate2.OptionsColumn.ReadOnly = true;
            this.colContractEndDate2.Width = 102;
            // 
            // colLinkedToParent2
            // 
            this.colLinkedToParent2.Caption = "Linked To Contract";
            this.colLinkedToParent2.ColumnEdit = this.repositoryItemTextEditHTML4;
            this.colLinkedToParent2.FieldName = "LinkedToParent";
            this.colLinkedToParent2.Name = "colLinkedToParent2";
            this.colLinkedToParent2.OptionsColumn.AllowEdit = false;
            this.colLinkedToParent2.OptionsColumn.AllowFocus = false;
            this.colLinkedToParent2.OptionsColumn.ReadOnly = true;
            this.colLinkedToParent2.Visible = true;
            this.colLinkedToParent2.VisibleIndex = 1;
            this.colLinkedToParent2.Width = 319;
            // 
            // repositoryItemTextEditHTML4
            // 
            this.repositoryItemTextEditHTML4.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTextEditHTML4.AutoHeight = false;
            this.repositoryItemTextEditHTML4.Name = "repositoryItemTextEditHTML4";
            // 
            // colResponsibilityType
            // 
            this.colResponsibilityType.Caption = "Responsibility Type";
            this.colResponsibilityType.FieldName = "ResponsibilityType";
            this.colResponsibilityType.Name = "colResponsibilityType";
            this.colResponsibilityType.OptionsColumn.AllowEdit = false;
            this.colResponsibilityType.OptionsColumn.AllowFocus = false;
            this.colResponsibilityType.OptionsColumn.ReadOnly = true;
            this.colResponsibilityType.Visible = true;
            this.colResponsibilityType.VisibleIndex = 0;
            this.colResponsibilityType.Width = 176;
            // 
            // colStaffName
            // 
            this.colStaffName.Caption = "Person Name";
            this.colStaffName.FieldName = "StaffName";
            this.colStaffName.Name = "colStaffName";
            this.colStaffName.OptionsColumn.AllowEdit = false;
            this.colStaffName.OptionsColumn.AllowFocus = false;
            this.colStaffName.OptionsColumn.ReadOnly = true;
            this.colStaffName.Visible = true;
            this.colStaffName.VisibleIndex = 2;
            this.colStaffName.Width = 183;
            // 
            // colPersonTypeDescription
            // 
            this.colPersonTypeDescription.Caption = "Person Type";
            this.colPersonTypeDescription.FieldName = "PersonTypeDescription";
            this.colPersonTypeDescription.Name = "colPersonTypeDescription";
            this.colPersonTypeDescription.OptionsColumn.AllowEdit = false;
            this.colPersonTypeDescription.OptionsColumn.AllowFocus = false;
            this.colPersonTypeDescription.OptionsColumn.ReadOnly = true;
            this.colPersonTypeDescription.Visible = true;
            this.colPersonTypeDescription.VisibleIndex = 1;
            this.colPersonTypeDescription.Width = 98;
            // 
            // colPersonTypeID
            // 
            this.colPersonTypeID.Caption = "Person Type ID";
            this.colPersonTypeID.FieldName = "PersonTypeID";
            this.colPersonTypeID.Name = "colPersonTypeID";
            this.colPersonTypeID.OptionsColumn.AllowEdit = false;
            this.colPersonTypeID.OptionsColumn.AllowFocus = false;
            this.colPersonTypeID.OptionsColumn.ReadOnly = true;
            this.colPersonTypeID.Width = 93;
            // 
            // xtraTabPageClientPOs
            // 
            this.xtraTabPageClientPOs.Controls.Add(this.popupContainerControlContractDirectorFilter);
            this.xtraTabPageClientPOs.Controls.Add(this.popupContainerControlConstructionManagerFilter);
            this.xtraTabPageClientPOs.Controls.Add(this.popupContainerControlKAMFilter);
            this.xtraTabPageClientPOs.Controls.Add(this.gridControlClientPO);
            this.xtraTabPageClientPOs.Name = "xtraTabPageClientPOs";
            this.xtraTabPageClientPOs.Size = new System.Drawing.Size(999, 220);
            this.xtraTabPageClientPOs.Text = "Client Purchase Orders";
            // 
            // popupContainerControlContractDirectorFilter
            // 
            this.popupContainerControlContractDirectorFilter.Controls.Add(this.btnContractDirectorFilterOK);
            this.popupContainerControlContractDirectorFilter.Controls.Add(this.gridControl4);
            this.popupContainerControlContractDirectorFilter.Location = new System.Drawing.Point(336, 42);
            this.popupContainerControlContractDirectorFilter.Name = "popupContainerControlContractDirectorFilter";
            this.popupContainerControlContractDirectorFilter.Size = new System.Drawing.Size(131, 135);
            this.popupContainerControlContractDirectorFilter.TabIndex = 22;
            // 
            // btnContractDirectorFilterOK
            // 
            this.btnContractDirectorFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnContractDirectorFilterOK.Location = new System.Drawing.Point(3, 112);
            this.btnContractDirectorFilterOK.Name = "btnContractDirectorFilterOK";
            this.btnContractDirectorFilterOK.Size = new System.Drawing.Size(50, 20);
            this.btnContractDirectorFilterOK.TabIndex = 18;
            this.btnContractDirectorFilterOK.Text = "OK";
            this.btnContractDirectorFilterOK.Click += new System.EventHandler(this.btnContractDirectorFilterOK_Click);
            // 
            // gridControl4
            // 
            this.gridControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl4.DataSource = this.sp06389OMClientContractManagerContractDirectorFilterBindingSource;
            this.gridControl4.Location = new System.Drawing.Point(3, 3);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.Size = new System.Drawing.Size(125, 107);
            this.gridControl4.TabIndex = 1;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp06389OMClientContractManagerContractDirectorFilterBindingSource
            // 
            this.sp06389OMClientContractManagerContractDirectorFilterBindingSource.DataMember = "sp06389_OM_Client_Contract_Manager_Contract_Director_Filter";
            this.sp06389OMClientContractManagerContractDirectorFilterBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsFind.AlwaysVisible = true;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn4, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Surname";
            this.gridColumn4.FieldName = "Surname";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 0;
            this.gridColumn4.Width = 122;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Forename";
            this.gridColumn5.FieldName = "Forename";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 1;
            this.gridColumn5.Width = 82;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "ID";
            this.gridColumn6.FieldName = "ID";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Width = 53;
            // 
            // popupContainerControlConstructionManagerFilter
            // 
            this.popupContainerControlConstructionManagerFilter.Controls.Add(this.btnConstructionManagerFilterOK);
            this.popupContainerControlConstructionManagerFilter.Controls.Add(this.gridControl3);
            this.popupContainerControlConstructionManagerFilter.Location = new System.Drawing.Point(185, 42);
            this.popupContainerControlConstructionManagerFilter.Name = "popupContainerControlConstructionManagerFilter";
            this.popupContainerControlConstructionManagerFilter.Size = new System.Drawing.Size(131, 135);
            this.popupContainerControlConstructionManagerFilter.TabIndex = 21;
            // 
            // btnConstructionManagerFilterOK
            // 
            this.btnConstructionManagerFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnConstructionManagerFilterOK.Location = new System.Drawing.Point(3, 112);
            this.btnConstructionManagerFilterOK.Name = "btnConstructionManagerFilterOK";
            this.btnConstructionManagerFilterOK.Size = new System.Drawing.Size(50, 20);
            this.btnConstructionManagerFilterOK.TabIndex = 18;
            this.btnConstructionManagerFilterOK.Text = "OK";
            this.btnConstructionManagerFilterOK.Click += new System.EventHandler(this.btnConstructionManagerFilterOK_Click);
            // 
            // gridControl3
            // 
            this.gridControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl3.DataSource = this.sp06388OMClientContractManagerConstructionManagerFilterBindingSource;
            this.gridControl3.Location = new System.Drawing.Point(3, 3);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.Size = new System.Drawing.Size(125, 107);
            this.gridControl3.TabIndex = 1;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp06388OMClientContractManagerConstructionManagerFilterBindingSource
            // 
            this.sp06388OMClientContractManagerConstructionManagerFilterBindingSource.DataMember = "sp06388_OM_Client_Contract_Manager_Construction_Manager_Filter";
            this.sp06388OMClientContractManagerConstructionManagerFilterBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsFind.AlwaysVisible = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn1, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Surname";
            this.gridColumn1.FieldName = "Surname";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 122;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Forename";
            this.gridColumn2.FieldName = "Forename";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 82;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "ID";
            this.gridColumn3.FieldName = "ID";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Width = 53;
            // 
            // popupContainerControlKAMFilter
            // 
            this.popupContainerControlKAMFilter.Controls.Add(this.btnKAMFilterOK);
            this.popupContainerControlKAMFilter.Controls.Add(this.gridControl5);
            this.popupContainerControlKAMFilter.Location = new System.Drawing.Point(30, 45);
            this.popupContainerControlKAMFilter.Name = "popupContainerControlKAMFilter";
            this.popupContainerControlKAMFilter.Size = new System.Drawing.Size(131, 135);
            this.popupContainerControlKAMFilter.TabIndex = 20;
            // 
            // btnKAMFilterOK
            // 
            this.btnKAMFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnKAMFilterOK.Location = new System.Drawing.Point(3, 112);
            this.btnKAMFilterOK.Name = "btnKAMFilterOK";
            this.btnKAMFilterOK.Size = new System.Drawing.Size(50, 20);
            this.btnKAMFilterOK.TabIndex = 18;
            this.btnKAMFilterOK.Text = "OK";
            this.btnKAMFilterOK.Click += new System.EventHandler(this.btnKAMFilterOK_Click);
            // 
            // gridControl5
            // 
            this.gridControl5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl5.DataSource = this.sp06019OMJobManagerKAMsFilterBindingSource;
            this.gridControl5.Location = new System.Drawing.Point(3, 3);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.Size = new System.Drawing.Size(125, 107);
            this.gridControl5.TabIndex = 1;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp06019OMJobManagerKAMsFilterBindingSource
            // 
            this.sp06019OMJobManagerKAMsFilterBindingSource.DataMember = "sp06019_OM_Job_Manager_KAMs_Filter";
            this.sp06019OMJobManagerKAMsFilterBindingSource.DataSource = this.dataSet_GC_Summer_Core;
            // 
            // dataSet_GC_Summer_Core
            // 
            this.dataSet_GC_Summer_Core.DataSetName = "DataSet_GC_Summer_Core";
            this.dataSet_GC_Summer_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription3,
            this.colForename,
            this.colID1});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsFind.AlwaysVisible = true;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription3, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription3
            // 
            this.colDescription3.Caption = "Surname";
            this.colDescription3.FieldName = "Surname";
            this.colDescription3.Name = "colDescription3";
            this.colDescription3.OptionsColumn.AllowEdit = false;
            this.colDescription3.OptionsColumn.AllowFocus = false;
            this.colDescription3.OptionsColumn.ReadOnly = true;
            this.colDescription3.Visible = true;
            this.colDescription3.VisibleIndex = 0;
            this.colDescription3.Width = 122;
            // 
            // colForename
            // 
            this.colForename.Caption = "Forename";
            this.colForename.FieldName = "Forename";
            this.colForename.Name = "colForename";
            this.colForename.OptionsColumn.AllowEdit = false;
            this.colForename.OptionsColumn.AllowFocus = false;
            this.colForename.OptionsColumn.ReadOnly = true;
            this.colForename.Visible = true;
            this.colForename.VisibleIndex = 1;
            this.colForename.Width = 82;
            // 
            // colID1
            // 
            this.colID1.Caption = "ID";
            this.colID1.FieldName = "ID";
            this.colID1.Name = "colID1";
            this.colID1.OptionsColumn.AllowEdit = false;
            this.colID1.OptionsColumn.AllowFocus = false;
            this.colID1.OptionsColumn.ReadOnly = true;
            this.colID1.Width = 53;
            // 
            // gridControlClientPO
            // 
            this.gridControlClientPO.DataSource = this.sp06326OMClientPOsLinkedToParentBindingSource;
            this.gridControlClientPO.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlClientPO.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlClientPO.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlClientPO.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlClientPO.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlClientPO.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlClientPO.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlClientPO.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlClientPO.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlClientPO.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlClientPO.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlClientPO.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlClientPO.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selcted Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "View Selected Record(s)", "view")});
            this.gridControlClientPO.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlClientPO_EmbeddedNavigator_ButtonClick);
            this.gridControlClientPO.Location = new System.Drawing.Point(0, 0);
            this.gridControlClientPO.MainView = this.gridViewClientPO;
            this.gridControlClientPO.MenuManager = this.barManager1;
            this.gridControlClientPO.Name = "gridControlClientPO";
            this.gridControlClientPO.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEditRemarks,
            this.repositoryItemTextEditDateTime9,
            this.repositoryItemCheckEdit4,
            this.repositoryItemTextEditCurrency9,
            this.repositoryItemTextEditHTML2});
            this.gridControlClientPO.Size = new System.Drawing.Size(999, 220);
            this.gridControlClientPO.TabIndex = 7;
            this.gridControlClientPO.UseEmbeddedNavigator = true;
            this.gridControlClientPO.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewClientPO});
            // 
            // sp06326OMClientPOsLinkedToParentBindingSource
            // 
            this.sp06326OMClientPOsLinkedToParentBindingSource.DataMember = "sp06326_OM_Client_POs_Linked_To_Parent";
            this.sp06326OMClientPOsLinkedToParentBindingSource.DataSource = this.dataSet_OM_Client_PO;
            // 
            // dataSet_OM_Client_PO
            // 
            this.dataSet_OM_Client_PO.DataSetName = "DataSet_OM_Client_PO";
            this.dataSet_OM_Client_PO.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridViewClientPO
            // 
            this.gridViewClientPO.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientPurchaseOrderLinkID,
            this.colClientPurchaseOrderID,
            this.colLinkedToRecordID,
            this.colLinkedToRecordTypeID,
            this.colIsDefault,
            this.colRemarks4,
            this.colPONumber,
            this.colClientName4,
            this.colLinkedToRecordType,
            this.colLinkedToRecord,
            this.colContractDescription1,
            this.colSiteName,
            this.colVisitNumber,
            this.colStartDate2,
            this.colEndDate2,
            this.colStartingValue,
            this.colUsedValue,
            this.colRemainingValue,
            this.colWarningValue,
            this.colClientID4});
            this.gridViewClientPO.GridControl = this.gridControlClientPO;
            this.gridViewClientPO.GroupCount = 1;
            this.gridViewClientPO.Name = "gridViewClientPO";
            this.gridViewClientPO.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewClientPO.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewClientPO.OptionsLayout.StoreAppearance = true;
            this.gridViewClientPO.OptionsLayout.StoreFormatRules = true;
            this.gridViewClientPO.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewClientPO.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewClientPO.OptionsSelection.MultiSelect = true;
            this.gridViewClientPO.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewClientPO.OptionsView.ColumnAutoWidth = false;
            this.gridViewClientPO.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewClientPO.OptionsView.ShowGroupPanel = false;
            this.gridViewClientPO.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToRecord, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colStartDate2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEndDate2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPONumber, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewClientPO.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridViewClientPO_CustomDrawCell);
            this.gridViewClientPO.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewClientPO.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewClientPO.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewClientPO.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewClientPO.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewClientPO.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewClientPO.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewClientPO_MouseUp);
            this.gridViewClientPO.DoubleClick += new System.EventHandler(this.gridViewClientPO_DoubleClick);
            this.gridViewClientPO.GotFocus += new System.EventHandler(this.gridViewClientPO_GotFocus);
            // 
            // colClientPurchaseOrderLinkID
            // 
            this.colClientPurchaseOrderLinkID.Caption = "Client PO Link ID";
            this.colClientPurchaseOrderLinkID.FieldName = "ClientPurchaseOrderLinkID";
            this.colClientPurchaseOrderLinkID.Name = "colClientPurchaseOrderLinkID";
            this.colClientPurchaseOrderLinkID.OptionsColumn.AllowEdit = false;
            this.colClientPurchaseOrderLinkID.OptionsColumn.AllowFocus = false;
            this.colClientPurchaseOrderLinkID.OptionsColumn.ReadOnly = true;
            this.colClientPurchaseOrderLinkID.Width = 98;
            // 
            // colClientPurchaseOrderID
            // 
            this.colClientPurchaseOrderID.Caption = "Client PO ID";
            this.colClientPurchaseOrderID.FieldName = "ClientPurchaseOrderID";
            this.colClientPurchaseOrderID.Name = "colClientPurchaseOrderID";
            this.colClientPurchaseOrderID.OptionsColumn.AllowEdit = false;
            this.colClientPurchaseOrderID.OptionsColumn.AllowFocus = false;
            this.colClientPurchaseOrderID.OptionsColumn.ReadOnly = true;
            this.colClientPurchaseOrderID.Width = 77;
            // 
            // colLinkedToRecordID
            // 
            this.colLinkedToRecordID.Caption = "Linked To Record ID";
            this.colLinkedToRecordID.FieldName = "LinkedToRecordID";
            this.colLinkedToRecordID.Name = "colLinkedToRecordID";
            this.colLinkedToRecordID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordID.Width = 115;
            // 
            // colLinkedToRecordTypeID
            // 
            this.colLinkedToRecordTypeID.Caption = "Linked To Record Type ID";
            this.colLinkedToRecordTypeID.FieldName = "LinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.Name = "colLinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordTypeID.Width = 142;
            // 
            // colIsDefault
            // 
            this.colIsDefault.Caption = "Default";
            this.colIsDefault.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colIsDefault.FieldName = "IsDefault";
            this.colIsDefault.Name = "colIsDefault";
            this.colIsDefault.OptionsColumn.AllowEdit = false;
            this.colIsDefault.OptionsColumn.AllowFocus = false;
            this.colIsDefault.OptionsColumn.ReadOnly = true;
            this.colIsDefault.Visible = true;
            this.colIsDefault.VisibleIndex = 3;
            this.colIsDefault.Width = 54;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            this.repositoryItemCheckEdit4.ValueChecked = 1;
            this.repositoryItemCheckEdit4.ValueUnchecked = 0;
            // 
            // colRemarks4
            // 
            this.colRemarks4.Caption = "Remarks";
            this.colRemarks4.ColumnEdit = this.repositoryItemMemoExEditRemarks;
            this.colRemarks4.FieldName = "Remarks";
            this.colRemarks4.Name = "colRemarks4";
            this.colRemarks4.OptionsColumn.ReadOnly = true;
            this.colRemarks4.Visible = true;
            this.colRemarks4.VisibleIndex = 8;
            // 
            // repositoryItemMemoExEditRemarks
            // 
            this.repositoryItemMemoExEditRemarks.AutoHeight = false;
            this.repositoryItemMemoExEditRemarks.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEditRemarks.Name = "repositoryItemMemoExEditRemarks";
            this.repositoryItemMemoExEditRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEditRemarks.ShowIcon = false;
            // 
            // colPONumber
            // 
            this.colPONumber.Caption = "PO #";
            this.colPONumber.FieldName = "PONumber";
            this.colPONumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colPONumber.Name = "colPONumber";
            this.colPONumber.OptionsColumn.AllowEdit = false;
            this.colPONumber.OptionsColumn.AllowFocus = false;
            this.colPONumber.OptionsColumn.ReadOnly = true;
            this.colPONumber.Visible = true;
            this.colPONumber.VisibleIndex = 2;
            this.colPONumber.Width = 123;
            // 
            // colClientName4
            // 
            this.colClientName4.Caption = "Client Name";
            this.colClientName4.FieldName = "ClientName";
            this.colClientName4.Name = "colClientName4";
            this.colClientName4.OptionsColumn.AllowEdit = false;
            this.colClientName4.OptionsColumn.AllowFocus = false;
            this.colClientName4.OptionsColumn.ReadOnly = true;
            this.colClientName4.Width = 245;
            // 
            // colLinkedToRecordType
            // 
            this.colLinkedToRecordType.Caption = "Link Type";
            this.colLinkedToRecordType.FieldName = "LinkedToRecordType";
            this.colLinkedToRecordType.Name = "colLinkedToRecordType";
            this.colLinkedToRecordType.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordType.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordType.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordType.Width = 178;
            // 
            // colLinkedToRecord
            // 
            this.colLinkedToRecord.Caption = "Linked To Contract";
            this.colLinkedToRecord.ColumnEdit = this.repositoryItemTextEditHTML2;
            this.colLinkedToRecord.FieldName = "LinkedToRecord";
            this.colLinkedToRecord.Name = "colLinkedToRecord";
            this.colLinkedToRecord.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecord.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecord.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecord.Visible = true;
            this.colLinkedToRecord.VisibleIndex = 9;
            this.colLinkedToRecord.Width = 415;
            // 
            // repositoryItemTextEditHTML2
            // 
            this.repositoryItemTextEditHTML2.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTextEditHTML2.AutoHeight = false;
            this.repositoryItemTextEditHTML2.Name = "repositoryItemTextEditHTML2";
            // 
            // colContractDescription1
            // 
            this.colContractDescription1.Caption = "Contract";
            this.colContractDescription1.FieldName = "ContractDescription";
            this.colContractDescription1.Name = "colContractDescription1";
            this.colContractDescription1.OptionsColumn.AllowEdit = false;
            this.colContractDescription1.OptionsColumn.AllowFocus = false;
            this.colContractDescription1.OptionsColumn.ReadOnly = true;
            this.colContractDescription1.Width = 326;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.Width = 148;
            // 
            // colVisitNumber
            // 
            this.colVisitNumber.Caption = "Visit #";
            this.colVisitNumber.FieldName = "VisitNumber";
            this.colVisitNumber.Name = "colVisitNumber";
            this.colVisitNumber.OptionsColumn.AllowEdit = false;
            this.colVisitNumber.OptionsColumn.AllowFocus = false;
            this.colVisitNumber.OptionsColumn.ReadOnly = true;
            this.colVisitNumber.Width = 52;
            // 
            // colStartDate2
            // 
            this.colStartDate2.ColumnEdit = this.repositoryItemTextEditDateTime9;
            this.colStartDate2.FieldName = "StartDate";
            this.colStartDate2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colStartDate2.Name = "colStartDate2";
            this.colStartDate2.OptionsColumn.AllowEdit = false;
            this.colStartDate2.OptionsColumn.AllowFocus = false;
            this.colStartDate2.OptionsColumn.ReadOnly = true;
            this.colStartDate2.Visible = true;
            this.colStartDate2.VisibleIndex = 0;
            this.colStartDate2.Width = 116;
            // 
            // repositoryItemTextEditDateTime9
            // 
            this.repositoryItemTextEditDateTime9.AutoHeight = false;
            this.repositoryItemTextEditDateTime9.Mask.EditMask = "d";
            this.repositoryItemTextEditDateTime9.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime9.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime9.Name = "repositoryItemTextEditDateTime9";
            // 
            // colEndDate2
            // 
            this.colEndDate2.ColumnEdit = this.repositoryItemTextEditDateTime9;
            this.colEndDate2.FieldName = "EndDate";
            this.colEndDate2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colEndDate2.Name = "colEndDate2";
            this.colEndDate2.OptionsColumn.AllowEdit = false;
            this.colEndDate2.OptionsColumn.AllowFocus = false;
            this.colEndDate2.OptionsColumn.ReadOnly = true;
            this.colEndDate2.Visible = true;
            this.colEndDate2.VisibleIndex = 1;
            this.colEndDate2.Width = 85;
            // 
            // colStartingValue
            // 
            this.colStartingValue.ColumnEdit = this.repositoryItemTextEditCurrency9;
            this.colStartingValue.FieldName = "StartingValue";
            this.colStartingValue.Name = "colStartingValue";
            this.colStartingValue.OptionsColumn.AllowEdit = false;
            this.colStartingValue.OptionsColumn.AllowFocus = false;
            this.colStartingValue.OptionsColumn.ReadOnly = true;
            this.colStartingValue.Visible = true;
            this.colStartingValue.VisibleIndex = 4;
            this.colStartingValue.Width = 100;
            // 
            // repositoryItemTextEditCurrency9
            // 
            this.repositoryItemTextEditCurrency9.AutoHeight = false;
            this.repositoryItemTextEditCurrency9.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency9.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency9.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency9.Name = "repositoryItemTextEditCurrency9";
            // 
            // colUsedValue
            // 
            this.colUsedValue.ColumnEdit = this.repositoryItemTextEditCurrency9;
            this.colUsedValue.FieldName = "UsedValue";
            this.colUsedValue.Name = "colUsedValue";
            this.colUsedValue.OptionsColumn.AllowEdit = false;
            this.colUsedValue.OptionsColumn.AllowFocus = false;
            this.colUsedValue.OptionsColumn.ReadOnly = true;
            this.colUsedValue.Visible = true;
            this.colUsedValue.VisibleIndex = 5;
            this.colUsedValue.Width = 100;
            // 
            // colRemainingValue
            // 
            this.colRemainingValue.ColumnEdit = this.repositoryItemTextEditCurrency9;
            this.colRemainingValue.FieldName = "RemainingValue";
            this.colRemainingValue.Name = "colRemainingValue";
            this.colRemainingValue.OptionsColumn.AllowEdit = false;
            this.colRemainingValue.OptionsColumn.AllowFocus = false;
            this.colRemainingValue.OptionsColumn.ReadOnly = true;
            this.colRemainingValue.Visible = true;
            this.colRemainingValue.VisibleIndex = 6;
            this.colRemainingValue.Width = 100;
            // 
            // colWarningValue
            // 
            this.colWarningValue.ColumnEdit = this.repositoryItemTextEditCurrency9;
            this.colWarningValue.FieldName = "WarningValue";
            this.colWarningValue.Name = "colWarningValue";
            this.colWarningValue.OptionsColumn.AllowEdit = false;
            this.colWarningValue.OptionsColumn.AllowFocus = false;
            this.colWarningValue.OptionsColumn.ReadOnly = true;
            this.colWarningValue.Visible = true;
            this.colWarningValue.VisibleIndex = 7;
            this.colWarningValue.Width = 100;
            // 
            // colClientID4
            // 
            this.colClientID4.Caption = "Client ID";
            this.colClientID4.FieldName = "ClientID";
            this.colClientID4.Name = "colClientID4";
            this.colClientID4.OptionsColumn.AllowEdit = false;
            this.colClientID4.OptionsColumn.AllowFocus = false;
            this.colClientID4.OptionsColumn.ReadOnly = true;
            this.colClientID4.Width = 60;
            // 
            // xtraTabPageBillingRequirements
            // 
            this.xtraTabPageBillingRequirements.Controls.Add(this.gridControlBillingRequirement);
            this.xtraTabPageBillingRequirements.Name = "xtraTabPageBillingRequirements";
            this.xtraTabPageBillingRequirements.Size = new System.Drawing.Size(999, 220);
            this.xtraTabPageBillingRequirements.Text = "Billing Requirements";
            // 
            // gridControlBillingRequirement
            // 
            this.gridControlBillingRequirement.DataSource = this.sp01037CoreBillingRequirementItemsBindingSource;
            this.gridControlBillingRequirement.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlBillingRequirement.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlBillingRequirement.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlBillingRequirement.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlBillingRequirement.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlBillingRequirement.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlBillingRequirement.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlBillingRequirement.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlBillingRequirement.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlBillingRequirement.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlBillingRequirement.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlBillingRequirement.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlBillingRequirement.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selcted Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 9, true, true, "Add from Template", "add_from_template"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 10, true, true, "Template Manager", "template_manager")});
            this.gridControlBillingRequirement.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlBillingRequirement_EmbeddedNavigator_ButtonClick);
            this.gridControlBillingRequirement.Location = new System.Drawing.Point(0, 0);
            this.gridControlBillingRequirement.MainView = this.gridViewBillingRequirement;
            this.gridControlBillingRequirement.MenuManager = this.barManager1;
            this.gridControlBillingRequirement.Name = "gridControlBillingRequirement";
            this.gridControlBillingRequirement.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit5,
            this.repositoryItemCheckEdit5});
            this.gridControlBillingRequirement.Size = new System.Drawing.Size(999, 220);
            this.gridControlBillingRequirement.TabIndex = 7;
            this.gridControlBillingRequirement.UseEmbeddedNavigator = true;
            this.gridControlBillingRequirement.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewBillingRequirement});
            // 
            // sp01037CoreBillingRequirementItemsBindingSource
            // 
            this.sp01037CoreBillingRequirementItemsBindingSource.DataMember = "sp01037_Core_Billing_Requirement_Items";
            this.sp01037CoreBillingRequirementItemsBindingSource.DataSource = this.dataSet_Common_Functionality;
            // 
            // dataSet_Common_Functionality
            // 
            this.dataSet_Common_Functionality.DataSetName = "DataSet_Common_Functionality";
            this.dataSet_Common_Functionality.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridViewBillingRequirement
            // 
            this.gridViewBillingRequirement.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBillingRequirementID,
            this.colRequirementID,
            this.colLinkedToRecordID1,
            this.colLinkedToRecordTypeID1,
            this.colClientBillRequirement,
            this.colTeamSelfBillRequirement,
            this.colRemarks5,
            this.colRequirementFulfilled,
            this.colCheckedByStaffID,
            this.colBillingRequirement,
            this.colLinkedToRecord1,
            this.colLinkedToRecordType1,
            this.colCheckedByStaff});
            this.gridViewBillingRequirement.GridControl = this.gridControlBillingRequirement;
            this.gridViewBillingRequirement.GroupCount = 1;
            this.gridViewBillingRequirement.Name = "gridViewBillingRequirement";
            this.gridViewBillingRequirement.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewBillingRequirement.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewBillingRequirement.OptionsLayout.StoreAppearance = true;
            this.gridViewBillingRequirement.OptionsLayout.StoreFormatRules = true;
            this.gridViewBillingRequirement.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewBillingRequirement.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewBillingRequirement.OptionsSelection.MultiSelect = true;
            this.gridViewBillingRequirement.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewBillingRequirement.OptionsView.ColumnAutoWidth = false;
            this.gridViewBillingRequirement.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewBillingRequirement.OptionsView.ShowGroupPanel = false;
            this.gridViewBillingRequirement.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToRecord1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colBillingRequirement, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewBillingRequirement.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewBillingRequirement.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewBillingRequirement.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewBillingRequirement.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewBillingRequirement.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewBillingRequirement.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewBillingRequirement.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridControlBillingRequirement_MouseUp);
            this.gridViewBillingRequirement.DoubleClick += new System.EventHandler(this.gridControlBillingRequirement_DoubleClick);
            this.gridViewBillingRequirement.GotFocus += new System.EventHandler(this.gridControlBillingRequirement_GotFocus);
            // 
            // colBillingRequirementID
            // 
            this.colBillingRequirementID.Caption = "Billing Requirement ID";
            this.colBillingRequirementID.FieldName = "BillingRequirementID";
            this.colBillingRequirementID.Name = "colBillingRequirementID";
            this.colBillingRequirementID.OptionsColumn.AllowEdit = false;
            this.colBillingRequirementID.OptionsColumn.AllowFocus = false;
            this.colBillingRequirementID.OptionsColumn.ReadOnly = true;
            this.colBillingRequirementID.Width = 123;
            // 
            // colRequirementID
            // 
            this.colRequirementID.Caption = "Requirement ID";
            this.colRequirementID.FieldName = "RequirementID";
            this.colRequirementID.Name = "colRequirementID";
            this.colRequirementID.OptionsColumn.AllowEdit = false;
            this.colRequirementID.OptionsColumn.AllowFocus = false;
            this.colRequirementID.OptionsColumn.ReadOnly = true;
            this.colRequirementID.Width = 94;
            // 
            // colLinkedToRecordID1
            // 
            this.colLinkedToRecordID1.Caption = "Linked To Record ID";
            this.colLinkedToRecordID1.FieldName = "LinkedToRecordID";
            this.colLinkedToRecordID1.Name = "colLinkedToRecordID1";
            this.colLinkedToRecordID1.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordID1.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordID1.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordID1.Width = 115;
            // 
            // colLinkedToRecordTypeID1
            // 
            this.colLinkedToRecordTypeID1.Caption = "Linked to Record Type ID";
            this.colLinkedToRecordTypeID1.FieldName = "LinkedToRecordTypeID";
            this.colLinkedToRecordTypeID1.Name = "colLinkedToRecordTypeID1";
            this.colLinkedToRecordTypeID1.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordTypeID1.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordTypeID1.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordTypeID1.Width = 140;
            // 
            // colClientBillRequirement
            // 
            this.colClientBillRequirement.Caption = "Client Bill Requirement";
            this.colClientBillRequirement.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colClientBillRequirement.FieldName = "ClientBillRequirement";
            this.colClientBillRequirement.Name = "colClientBillRequirement";
            this.colClientBillRequirement.OptionsColumn.AllowEdit = false;
            this.colClientBillRequirement.OptionsColumn.AllowFocus = false;
            this.colClientBillRequirement.OptionsColumn.ReadOnly = true;
            this.colClientBillRequirement.Visible = true;
            this.colClientBillRequirement.VisibleIndex = 1;
            this.colClientBillRequirement.Width = 125;
            // 
            // repositoryItemCheckEdit5
            // 
            this.repositoryItemCheckEdit5.AutoHeight = false;
            this.repositoryItemCheckEdit5.Name = "repositoryItemCheckEdit5";
            this.repositoryItemCheckEdit5.ValueChecked = 1;
            this.repositoryItemCheckEdit5.ValueUnchecked = 0;
            // 
            // colTeamSelfBillRequirement
            // 
            this.colTeamSelfBillRequirement.Caption = "Team Self-Bill Requirement";
            this.colTeamSelfBillRequirement.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colTeamSelfBillRequirement.FieldName = "TeamSelfBillRequirement";
            this.colTeamSelfBillRequirement.Name = "colTeamSelfBillRequirement";
            this.colTeamSelfBillRequirement.OptionsColumn.AllowEdit = false;
            this.colTeamSelfBillRequirement.OptionsColumn.AllowFocus = false;
            this.colTeamSelfBillRequirement.OptionsColumn.ReadOnly = true;
            this.colTeamSelfBillRequirement.Visible = true;
            this.colTeamSelfBillRequirement.VisibleIndex = 2;
            this.colTeamSelfBillRequirement.Width = 146;
            // 
            // colRemarks5
            // 
            this.colRemarks5.Caption = "Remarks";
            this.colRemarks5.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.colRemarks5.FieldName = "Remarks";
            this.colRemarks5.Name = "colRemarks5";
            this.colRemarks5.OptionsColumn.ReadOnly = true;
            this.colRemarks5.Visible = true;
            this.colRemarks5.VisibleIndex = 3;
            this.colRemarks5.Width = 159;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // colRequirementFulfilled
            // 
            this.colRequirementFulfilled.Caption = "Requirement Fullfilled";
            this.colRequirementFulfilled.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colRequirementFulfilled.FieldName = "RequirementFulfilled";
            this.colRequirementFulfilled.Name = "colRequirementFulfilled";
            this.colRequirementFulfilled.OptionsColumn.AllowEdit = false;
            this.colRequirementFulfilled.OptionsColumn.AllowFocus = false;
            this.colRequirementFulfilled.OptionsColumn.ReadOnly = true;
            this.colRequirementFulfilled.Width = 121;
            // 
            // colCheckedByStaffID
            // 
            this.colCheckedByStaffID.Caption = "Checked By Staff ID";
            this.colCheckedByStaffID.FieldName = "CheckedByStaffID";
            this.colCheckedByStaffID.Name = "colCheckedByStaffID";
            this.colCheckedByStaffID.OptionsColumn.AllowEdit = false;
            this.colCheckedByStaffID.OptionsColumn.AllowFocus = false;
            this.colCheckedByStaffID.OptionsColumn.ReadOnly = true;
            this.colCheckedByStaffID.Width = 116;
            // 
            // colBillingRequirement
            // 
            this.colBillingRequirement.Caption = "Billing Requirement";
            this.colBillingRequirement.FieldName = "BillingRequirement";
            this.colBillingRequirement.Name = "colBillingRequirement";
            this.colBillingRequirement.OptionsColumn.AllowEdit = false;
            this.colBillingRequirement.OptionsColumn.AllowFocus = false;
            this.colBillingRequirement.OptionsColumn.ReadOnly = true;
            this.colBillingRequirement.Visible = true;
            this.colBillingRequirement.VisibleIndex = 0;
            this.colBillingRequirement.Width = 268;
            // 
            // colLinkedToRecord1
            // 
            this.colLinkedToRecord1.Caption = "Client Contract";
            this.colLinkedToRecord1.FieldName = "LinkedToRecord";
            this.colLinkedToRecord1.Name = "colLinkedToRecord1";
            this.colLinkedToRecord1.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecord1.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecord1.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecord1.Visible = true;
            this.colLinkedToRecord1.VisibleIndex = 4;
            this.colLinkedToRecord1.Width = 469;
            // 
            // colLinkedToRecordType1
            // 
            this.colLinkedToRecordType1.Caption = "Linked To Record Type";
            this.colLinkedToRecordType1.FieldName = "LinkedToRecordType";
            this.colLinkedToRecordType1.Name = "colLinkedToRecordType1";
            this.colLinkedToRecordType1.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordType1.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordType1.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordType1.Width = 142;
            // 
            // colCheckedByStaff
            // 
            this.colCheckedByStaff.Caption = "Checked By Staff";
            this.colCheckedByStaff.FieldName = "CheckedByStaff";
            this.colCheckedByStaff.Name = "colCheckedByStaff";
            this.colCheckedByStaff.OptionsColumn.AllowEdit = false;
            this.colCheckedByStaff.OptionsColumn.AllowFocus = false;
            this.colCheckedByStaff.OptionsColumn.ReadOnly = true;
            this.colCheckedByStaff.Width = 140;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bar1
            // 
            this.bar1.BarName = "GrittingCalloutToolbar";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.FloatLocation = new System.Drawing.Point(519, 261);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefresh, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddWizard, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiImport),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiSelectedCount, true)});
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.DisableCustomization = true;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar1.Text = "Gritting Callout Toolbar";
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiRefresh.Caption = "Refresh";
            this.bbiRefresh.Id = 58;
            this.bbiRefresh.ImageOptions.ImageIndex = 0;
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // bbiAddWizard
            // 
            this.bbiAddWizard.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiAddWizard.Caption = "Wizard";
            this.bbiAddWizard.Id = 42;
            this.bbiAddWizard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiAddWizard.ImageOptions.Image")));
            this.bbiAddWizard.Name = "bbiAddWizard";
            this.bbiAddWizard.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Add Wizard - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to open the Create Client Contract Wizard screen.\r\n\r\nThe Wizard will gui" +
    "de you step-by-step through the client contract creation process.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiAddWizard.SuperTip = superToolTip1;
            this.bbiAddWizard.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddWizard_ItemClick);
            // 
            // bbiImport
            // 
            this.bbiImport.Caption = "Import";
            this.bbiImport.Enabled = false;
            this.bbiImport.Id = 59;
            this.bbiImport.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiImport.ImageOptions.Image")));
            this.bbiImport.Name = "bbiImport";
            this.bbiImport.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bsiSelectedCount
            // 
            this.bsiSelectedCount.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bsiSelectedCount.Caption = "0 Client Contracts Selected";
            this.bsiSelectedCount.Id = 63;
            this.bsiSelectedCount.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_16x16;
            this.bsiSelectedCount.Name = "bsiSelectedCount";
            this.bsiSelectedCount.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bsiSelectedCount.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // repositoryItemCheckEditActiveOnly
            // 
            this.repositoryItemCheckEditActiveOnly.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditActiveOnly.Appearance.Options.UseBackColor = true;
            this.repositoryItemCheckEditActiveOnly.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditActiveOnly.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemCheckEditActiveOnly.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditActiveOnly.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemCheckEditActiveOnly.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditActiveOnly.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemCheckEditActiveOnly.AutoHeight = false;
            this.repositoryItemCheckEditActiveOnly.Caption = "Check";
            this.repositoryItemCheckEditActiveOnly.Name = "repositoryItemCheckEditActiveOnly";
            this.repositoryItemCheckEditActiveOnly.ValueChecked = 1;
            this.repositoryItemCheckEditActiveOnly.ValueUnchecked = 0;
            // 
            // toolTipController1
            // 
            this.toolTipController1.CloseOnClick = DevExpress.Utils.DefaultBoolean.True;
            // 
            // repositoryItemPopupContainerEdit1
            // 
            this.repositoryItemPopupContainerEdit1.AutoHeight = false;
            this.repositoryItemPopupContainerEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit1.Name = "repositoryItemPopupContainerEdit1";
            // 
            // sp06050_OM_Contract_TypesTableAdapter
            // 
            this.sp06050_OM_Contract_TypesTableAdapter.ClearBeforeFill = true;
            // 
            // sp06051_OM_Contract_StatusesTableAdapter
            // 
            this.sp06051_OM_Contract_StatusesTableAdapter.ClearBeforeFill = true;
            // 
            // sp06052_OM_Client_Contract_ManagerTableAdapter
            // 
            this.sp06052_OM_Client_Contract_ManagerTableAdapter.ClearBeforeFill = true;
            // 
            // sp06055_OM_Client_Contract_Manager_Linked_YearsTableAdapter
            // 
            this.sp06055_OM_Client_Contract_Manager_Linked_YearsTableAdapter.ClearBeforeFill = true;
            // 
            // sp06056_OM_Client_Contract_Manager_Linked_ProfilesTableAdapter
            // 
            this.sp06056_OM_Client_Contract_Manager_Linked_ProfilesTableAdapter.ClearBeforeFill = true;
            // 
            // sp06057_OM_Client_Contract_Manager_Linked_ResponsibilitiesTableAdapter
            // 
            this.sp06057_OM_Client_Contract_Manager_Linked_ResponsibilitiesTableAdapter.ClearBeforeFill = true;
            // 
            // sp06326_OM_Client_POs_Linked_To_ParentTableAdapter
            // 
            this.sp06326_OM_Client_POs_Linked_To_ParentTableAdapter.ClearBeforeFill = true;
            // 
            // imageCollection2
            // 
            this.imageCollection2.ImageSize = new System.Drawing.Size(32, 32);
            this.imageCollection2.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection2.ImageStream")));
            this.imageCollection2.InsertGalleryImage("refresh_32x32.png", "images/actions/refresh_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/refresh_32x32.png"), 0);
            this.imageCollection2.Images.SetKeyName(0, "refresh_32x32.png");
            // 
            // dockManager1
            // 
            this.dockManager1.AutoHideContainers.AddRange(new DevExpress.XtraBars.Docking.AutoHideContainer[] {
            this.hideContainerLeft});
            this.dockManager1.Form = this;
            this.dockManager1.MenuManager = this.barManager1;
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane"});
            // 
            // hideContainerLeft
            // 
            this.hideContainerLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.hideContainerLeft.Controls.Add(this.dockPanelFilters);
            this.hideContainerLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.hideContainerLeft.Location = new System.Drawing.Point(0, 0);
            this.hideContainerLeft.Name = "hideContainerLeft";
            this.hideContainerLeft.Size = new System.Drawing.Size(23, 564);
            // 
            // dockPanelFilters
            // 
            this.dockPanelFilters.Controls.Add(this.dockPanel1_Container);
            this.dockPanelFilters.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelFilters.ID = new System.Guid("e511c726-74d4-4ce2-92ad-5c1274e9525b");
            this.dockPanelFilters.Location = new System.Drawing.Point(0, 0);
            this.dockPanelFilters.Name = "dockPanelFilters";
            this.dockPanelFilters.Options.ShowCloseButton = false;
            this.dockPanelFilters.OriginalSize = new System.Drawing.Size(320, 200);
            this.dockPanelFilters.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelFilters.SavedIndex = 0;
            this.dockPanelFilters.Size = new System.Drawing.Size(320, 564);
            this.dockPanelFilters.Text = "Data Filter";
            this.dockPanelFilters.Visibility = DevExpress.XtraBars.Docking.DockVisibility.AutoHide;
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.layoutControl1);
            this.dockPanel1_Container.Location = new System.Drawing.Point(3, 29);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(314, 532);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.popupContainerEditConstructionManager);
            this.layoutControl1.Controls.Add(this.popupContainerEditContractDirector);
            this.layoutControl1.Controls.Add(this.popupContainerEditKAMFilter);
            this.layoutControl1.Controls.Add(this.btnLoad);
            this.layoutControl1.Controls.Add(this.btnClearAllFilters);
            this.layoutControl1.Controls.Add(this.checkEditActiveOnly);
            this.layoutControl1.Controls.Add(this.popupContainerEditDateRange);
            this.layoutControl1.Controls.Add(this.popupContainerEditContractStatus);
            this.layoutControl1.Controls.Add(this.popupContainerEditContractType);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(751, 361, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(314, 532);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // popupContainerEditConstructionManager
            // 
            this.popupContainerEditConstructionManager.EditValue = "All Construction Managers";
            this.popupContainerEditConstructionManager.Location = new System.Drawing.Point(97, 138);
            this.popupContainerEditConstructionManager.MenuManager = this.barManager1;
            this.popupContainerEditConstructionManager.Name = "popupContainerEditConstructionManager";
            this.popupContainerEditConstructionManager.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditConstructionManager.Properties.PopupControl = this.popupContainerControlConstructionManagerFilter;
            this.popupContainerEditConstructionManager.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditConstructionManager.Properties.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditKAMFilter_QueryResultValue);
            this.popupContainerEditConstructionManager.Size = new System.Drawing.Size(210, 20);
            this.popupContainerEditConstructionManager.StyleController = this.layoutControl1;
            this.popupContainerEditConstructionManager.TabIndex = 9;
            this.popupContainerEditConstructionManager.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditConstructionManager_QueryResultValue);
            // 
            // popupContainerEditContractDirector
            // 
            this.popupContainerEditContractDirector.EditValue = "All Contract Directors";
            this.popupContainerEditContractDirector.Location = new System.Drawing.Point(97, 162);
            this.popupContainerEditContractDirector.MenuManager = this.barManager1;
            this.popupContainerEditContractDirector.Name = "popupContainerEditContractDirector";
            this.popupContainerEditContractDirector.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditContractDirector.Properties.PopupControl = this.popupContainerControlContractDirectorFilter;
            this.popupContainerEditContractDirector.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditContractDirector.Properties.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditKAMFilter_QueryResultValue);
            this.popupContainerEditContractDirector.Size = new System.Drawing.Size(210, 20);
            this.popupContainerEditContractDirector.StyleController = this.layoutControl1;
            this.popupContainerEditContractDirector.TabIndex = 8;
            this.popupContainerEditContractDirector.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditContractDirector_QueryResultValue);
            // 
            // popupContainerEditKAMFilter
            // 
            this.popupContainerEditKAMFilter.EditValue = "All KAMs";
            this.popupContainerEditKAMFilter.Location = new System.Drawing.Point(97, 114);
            this.popupContainerEditKAMFilter.MenuManager = this.barManager1;
            this.popupContainerEditKAMFilter.Name = "popupContainerEditKAMFilter";
            this.popupContainerEditKAMFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditKAMFilter.Properties.PopupControl = this.popupContainerControlKAMFilter;
            this.popupContainerEditKAMFilter.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditKAMFilter.Properties.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditKAMFilter_QueryResultValue);
            this.popupContainerEditKAMFilter.Size = new System.Drawing.Size(210, 20);
            this.popupContainerEditKAMFilter.StyleController = this.layoutControl1;
            this.popupContainerEditKAMFilter.TabIndex = 7;
            this.popupContainerEditKAMFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditKAMFilter_QueryResultValue);
            // 
            // btnLoad
            // 
            this.btnLoad.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnLoad.ImageOptions.Image")));
            this.btnLoad.Location = new System.Drawing.Point(227, 196);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(80, 22);
            this.btnLoad.StyleController = this.layoutControl1;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Load Data - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to Load Contract Data.\r\n\r\nOnly data matching the specified Filter Criter" +
    "ia (From and To Dates and Person Responsibilities etc) will be loaded.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.btnLoad.SuperTip = superToolTip2;
            this.btnLoad.TabIndex = 17;
            this.btnLoad.Text = "Load Data";
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click_1);
            // 
            // btnClearAllFilters
            // 
            this.btnClearAllFilters.ImageOptions.ImageIndex = 11;
            this.btnClearAllFilters.ImageOptions.ImageList = this.imageCollection1;
            this.btnClearAllFilters.Location = new System.Drawing.Point(7, 196);
            this.btnClearAllFilters.Name = "btnClearAllFilters";
            this.btnClearAllFilters.Size = new System.Drawing.Size(90, 22);
            this.btnClearAllFilters.StyleController = this.layoutControl1;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Clear Filters - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to clear all filters (except Date Range fiilter).";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.btnClearAllFilters.SuperTip = superToolTip3;
            this.btnClearAllFilters.TabIndex = 21;
            this.btnClearAllFilters.Text = "Clear Filters";
            this.btnClearAllFilters.Click += new System.EventHandler(this.btnClearAllFilters_Click);
            // 
            // checkEditActiveOnly
            // 
            this.checkEditActiveOnly.Location = new System.Drawing.Point(97, 79);
            this.checkEditActiveOnly.MenuManager = this.barManager1;
            this.checkEditActiveOnly.Name = "checkEditActiveOnly";
            this.checkEditActiveOnly.Properties.Caption = "[Tick if Yes]";
            this.checkEditActiveOnly.Properties.ValueChecked = 1;
            this.checkEditActiveOnly.Properties.ValueUnchecked = 0;
            this.checkEditActiveOnly.Size = new System.Drawing.Size(210, 19);
            this.checkEditActiveOnly.StyleController = this.layoutControl1;
            this.checkEditActiveOnly.TabIndex = 7;
            // 
            // popupContainerEditDateRange
            // 
            this.popupContainerEditDateRange.EditValue = "No Date Range Filter";
            this.popupContainerEditDateRange.Location = new System.Drawing.Point(97, 55);
            this.popupContainerEditDateRange.MenuManager = this.barManager1;
            this.popupContainerEditDateRange.Name = "popupContainerEditDateRange";
            this.popupContainerEditDateRange.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditDateRange.Properties.PopupControl = this.popupContainerControlDateRange;
            this.popupContainerEditDateRange.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditDateRange.Size = new System.Drawing.Size(210, 20);
            this.popupContainerEditDateRange.StyleController = this.layoutControl1;
            this.popupContainerEditDateRange.TabIndex = 6;
            this.popupContainerEditDateRange.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditDateRange_QueryResultValue);
            // 
            // popupContainerEditContractStatus
            // 
            this.popupContainerEditContractStatus.EditValue = "No Contract Status Filter";
            this.popupContainerEditContractStatus.Location = new System.Drawing.Point(97, 31);
            this.popupContainerEditContractStatus.MenuManager = this.barManager1;
            this.popupContainerEditContractStatus.Name = "popupContainerEditContractStatus";
            this.popupContainerEditContractStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditContractStatus.Properties.PopupControl = this.popupContainerControlContractStatusFilter;
            this.popupContainerEditContractStatus.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditContractStatus.Size = new System.Drawing.Size(210, 20);
            this.popupContainerEditContractStatus.StyleController = this.layoutControl1;
            this.popupContainerEditContractStatus.TabIndex = 5;
            this.popupContainerEditContractStatus.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditContractStatus_QueryResultValue);
            // 
            // popupContainerEditContractType
            // 
            this.popupContainerEditContractType.EditValue = "No Contract Type Filter";
            this.popupContainerEditContractType.Location = new System.Drawing.Point(97, 7);
            this.popupContainerEditContractType.MenuManager = this.barManager1;
            this.popupContainerEditContractType.Name = "popupContainerEditContractType";
            this.popupContainerEditContractType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditContractType.Properties.PopupControl = this.popupContainerControlContractTypeFilter;
            this.popupContainerEditContractType.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditContractType.Size = new System.Drawing.Size(210, 20);
            this.popupContainerEditContractType.StyleController = this.layoutControl1;
            this.popupContainerEditContractType.TabIndex = 4;
            this.popupContainerEditContractType.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditContractType_QueryResultValue);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.emptySpaceItem2,
            this.layoutControlItem6,
            this.emptySpaceItem3,
            this.emptySpaceItem1,
            this.layoutControlItem7,
            this.emptySpaceItem4,
            this.layoutControlItem8,
            this.layoutControlItem9});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Size = new System.Drawing.Size(314, 532);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.popupContainerEditContractType;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(304, 24);
            this.layoutControlItem1.Text = "Contract Type:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(87, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.popupContainerEditContractStatus;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(304, 24);
            this.layoutControlItem2.Text = "Contract Status:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(87, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.popupContainerEditDateRange;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(304, 24);
            this.layoutControlItem3.Text = "Date Range:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(87, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.checkEditActiveOnly;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(304, 23);
            this.layoutControlItem4.Text = "Active Only:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(87, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.btnClearAllFilters;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 189);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(94, 26);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(94, 26);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(94, 26);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(94, 189);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(126, 26);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.btnLoad;
            this.layoutControlItem6.Location = new System.Drawing.Point(220, 189);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(84, 26);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(84, 26);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(84, 26);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 215);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(304, 307);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 179);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(304, 10);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.popupContainerEditKAMFilter;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 107);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(304, 24);
            this.layoutControlItem7.Text = "KAM:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(87, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 95);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 12);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 12);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(304, 12);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.popupContainerEditContractDirector;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 155);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(304, 24);
            this.layoutControlItem8.Text = "Contract Director:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(87, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.popupContainerEditConstructionManager;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 131);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(304, 24);
            this.layoutControlItem9.Text = "Construction Mgr:";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(87, 13);
            // 
            // sp06019_OM_Job_Manager_KAMs_FilterTableAdapter
            // 
            this.sp06019_OM_Job_Manager_KAMs_FilterTableAdapter.ClearBeforeFill = true;
            // 
            // sp06388_OM_Client_Contract_Manager_Construction_Manager_FilterTableAdapter
            // 
            this.sp06388_OM_Client_Contract_Manager_Construction_Manager_FilterTableAdapter.ClearBeforeFill = true;
            // 
            // sp06389_OM_Client_Contract_Manager_Contract_Director_FilterTableAdapter
            // 
            this.sp06389_OM_Client_Contract_Manager_Contract_Director_FilterTableAdapter.ClearBeforeFill = true;
            // 
            // sp01037_Core_Billing_Requirement_ItemsTableAdapter
            // 
            this.sp01037_Core_Billing_Requirement_ItemsTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlendingBillingRequirement
            // 
            this.xtraGridBlendingBillingRequirement.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlendingBillingRequirement.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlendingBillingRequirement.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlendingBillingRequirement.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlendingBillingRequirement.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlendingBillingRequirement.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlendingBillingRequirement.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlendingBillingRequirement.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlendingBillingRequirement.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlendingBillingRequirement.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlendingBillingRequirement.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlendingBillingRequirement.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlendingBillingRequirement.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlendingBillingRequirement.GridControl = this.gridControlBillingRequirement;
            // 
            // frm_OM_Client_Contract_Manager
            // 
            this.ClientSize = new System.Drawing.Size(1027, 564);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.hideContainerLeft);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_OM_Client_Contract_Manager";
            this.Text = "Client Contract Manager - Operations";
            this.Activated += new System.EventHandler(this.frm_OM_Client_Contract_Manager_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_OM_Client_Contract_Manager_FormClosing);
            this.Load += new System.EventHandler(this.frm_OM_Client_Contract_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.hideContainerLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlContract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06052OMClientContractManagerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewContract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocumentsClientContract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditTenderRequestNotesFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlContractTypeFilter)).EndInit();
            this.popupContainerControlContractTypeFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06050OMContractTypesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).EndInit();
            this.popupContainerControlDateRange.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlContractStatusFilter)).EndInit();
            this.popupContainerControlContractStatusFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06051OMContractStatusesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPageYears.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06055OMClientContractManagerLinkedYearsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMoney2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocumentsClientContractYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlProfile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06056OMClientContractManagerLinkedProfilesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewProfile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML1)).EndInit();
            this.xtraTabPageResponsibilities.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlResponsibility)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06057OMClientContractManagerLinkedResponsibilitiesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewResponsibility)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML4)).EndInit();
            this.xtraTabPageClientPOs.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlContractDirectorFilter)).EndInit();
            this.popupContainerControlContractDirectorFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06389OMClientContractManagerContractDirectorFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlConstructionManagerFilter)).EndInit();
            this.popupContainerControlConstructionManagerFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06388OMClientContractManagerConstructionManagerFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlKAMFilter)).EndInit();
            this.popupContainerControlKAMFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06019OMJobManagerKAMsFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Summer_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlClientPO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06326OMClientPOsLinkedToParentBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Client_PO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewClientPO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEditRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency9)).EndInit();
            this.xtraTabPageBillingRequirements.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlBillingRequirement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01037CoreBillingRequirementItemsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Common_Functionality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBillingRequirement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditActiveOnly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.hideContainerLeft.ResumeLayout(false);
            this.dockPanelFilters.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditConstructionManager.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditContractDirector.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditKAMFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditActiveOnly.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditDateRange.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditContractStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditContractType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraGrid.GridControl gridControlContract;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewContract;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DataSet_GC_Core dataSet_GC_Core;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.Utils.ToolTipController toolTipController1;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        public DevExpress.XtraBars.BarButtonItem bbiAddWizard;
        private WoodPlan5.DataSet_AT dataSet_AT;
        private WoodPlanDataSet woodPlanDataSet;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraBars.BarButtonItem bbiImport;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraGrid.GridControl gridControlYear;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewYear;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditMoney2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraGrid.GridControl gridControlProfile;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewProfile;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency3;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlContractStatusFilter;
        private DevExpress.XtraEditors.SimpleButton btnContractStatusFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colID5;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlDateRange;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraEditors.SimpleButton btnDateRangeOK;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit1;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlContractTypeFilter;
        private DevExpress.XtraEditors.SimpleButton btnContractTypeFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn159;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn160;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn161;
        private DataSet_OM_Contract dataSet_OM_Contract;
        private System.Windows.Forms.BindingSource sp06050OMContractTypesBindingSource;
        private DataSet_OM_ContractTableAdapters.sp06050_OM_Contract_TypesTableAdapter sp06050_OM_Contract_TypesTableAdapter;
        private System.Windows.Forms.BindingSource sp06051OMContractStatusesBindingSource;
        private DataSet_OM_ContractTableAdapters.sp06051_OM_Contract_StatusesTableAdapter sp06051_OM_Contract_StatusesTableAdapter;
        private System.Windows.Forms.BindingSource sp06052OMClientContractManagerBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colGCCompanyID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractManagerID;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActive;
        private DevExpress.XtraGrid.Columns.GridColumn colSectorTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractValue;
        private DevExpress.XtraGrid.Columns.GridColumn colYearlyPercentageIncrease;
        private DevExpress.XtraGrid.Columns.GridColumn colCheckRPIDate;
        private DevExpress.XtraGrid.Columns.GridColumn colLastClientPaymentDate;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPreviousContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colFinanceClientCode;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingCentreCodeID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colGCCompanyName;
        private DevExpress.XtraGrid.Columns.GridColumn colContractType;
        private DevExpress.XtraGrid.Columns.GridColumn colContractStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colContractManager;
        private DevExpress.XtraGrid.Columns.GridColumn colSectorType;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedContractCount;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedSiteContractCount;
        private DataSet_OM_ContractTableAdapters.sp06052_OM_Client_Contract_ManagerTableAdapter sp06052_OM_Client_Contract_ManagerTableAdapter;
        private System.Windows.Forms.BindingSource sp06055OMClientContractManagerLinkedYearsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractYearID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractID1;
        private DevExpress.XtraGrid.Columns.GridColumn colYearDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colActive1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientValue;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName1;
        private DevExpress.XtraGrid.Columns.GridColumn colContractStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colContractEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToParent;
        private DataSet_OM_ContractTableAdapters.sp06055_OM_Client_Contract_Manager_Linked_YearsTableAdapter sp06055_OM_Client_Contract_Manager_Linked_YearsTableAdapter;
        private System.Windows.Forms.BindingSource sp06056OMClientContractManagerLinkedProfilesBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractProfileID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractYearID1;
        private DevExpress.XtraGrid.Columns.GridColumn colInvoiceDateDue;
        private DevExpress.XtraGrid.Columns.GridColumn colInvoiceDateActual;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedBillAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colActualBillAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colBilledByPersonID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractID2;
        private DevExpress.XtraGrid.Columns.GridColumn colYearDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colYearStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colYearEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colYearActive;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID2;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName2;
        private DevExpress.XtraGrid.Columns.GridColumn colContractStartDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colContractEndDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToParent1;
        private DevExpress.XtraGrid.Columns.GridColumn colBilledByPerson;
        private DataSet_OM_ContractTableAdapters.sp06056_OM_Client_Contract_Manager_Linked_ProfilesTableAdapter sp06056_OM_Client_Contract_Manager_Linked_ProfilesTableAdapter;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageYears;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageResponsibilities;
        private DevExpress.XtraGrid.GridControl gridControlResponsibility;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewResponsibility;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime4;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private System.Windows.Forms.BindingSource sp06057OMClientContractManagerLinkedResponsibilitiesBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonResponsibilityID;
        private DevExpress.XtraGrid.Columns.GridColumn colStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colResponsibleForRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colResponsibleForRecordTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colResponsibilityTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks3;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID3;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName3;
        private DevExpress.XtraGrid.Columns.GridColumn colContractStartDate2;
        private DevExpress.XtraGrid.Columns.GridColumn colContractEndDate2;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToParent2;
        private DevExpress.XtraGrid.Columns.GridColumn colResponsibilityType;
        private DevExpress.XtraGrid.Columns.GridColumn colStaffName;
        private DataSet_OM_ContractTableAdapters.sp06057_OM_Client_Contract_Manager_Linked_ResponsibilitiesTableAdapter sp06057_OM_Client_Contract_Manager_Linked_ResponsibilitiesTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditActiveOnly;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedParentContractCount;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditLinkedDocumentsClientContract;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentCount1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditLinkedDocumentsClientContractYear;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colReactive;
        private DevExpress.XtraGrid.Columns.GridColumn colYearlyPercentageIncrease1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage2;
        private DevExpress.XtraGrid.Columns.GridColumn colClientInstructions;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageClientPOs;
        private DevExpress.XtraGrid.GridControl gridControlClientPO;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewClientPO;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEditRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime9;
        private System.Windows.Forms.BindingSource sp06326OMClientPOsLinkedToParentBindingSource;
        private DataSet_OM_Client_PO dataSet_OM_Client_PO;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPurchaseOrderLinkID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPurchaseOrderID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colIsDefault;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks4;
        private DevExpress.XtraGrid.Columns.GridColumn colPONumber;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName4;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordType;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecord;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitNumber;
        private DataSet_OM_Client_POTableAdapters.sp06326_OM_Client_POs_Linked_To_ParentTableAdapter sp06326_OM_Client_POs_Linked_To_ParentTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate2;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate2;
        private DevExpress.XtraGrid.Columns.GridColumn colStartingValue;
        private DevExpress.XtraGrid.Columns.GridColumn colUsedValue;
        private DevExpress.XtraGrid.Columns.GridColumn colRemainingValue;
        private DevExpress.XtraGrid.Columns.GridColumn colWarningValue;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency9;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID4;
        private DevExpress.Utils.ImageCollection imageCollection2;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonTypeID;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelFilters;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditContractType;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditDateRange;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditContractStatus;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.CheckEdit checkEditActiveOnly;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.SimpleButton btnClearAllFilters;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.SimpleButton btnLoad;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditKAMFilter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditConstructionManager;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditContractDirector;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlKAMFilter;
        private DevExpress.XtraEditors.SimpleButton btnKAMFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription3;
        private DevExpress.XtraGrid.Columns.GridColumn colForename;
        private DevExpress.XtraGrid.Columns.GridColumn colID1;
        private DataSet_GC_Summer_Core dataSet_GC_Summer_Core;
        private System.Windows.Forms.BindingSource sp06019OMJobManagerKAMsFilterBindingSource;
        private DataSet_GC_Summer_CoreTableAdapters.sp06019_OM_Job_Manager_KAMs_FilterTableAdapter sp06019_OM_Job_Manager_KAMs_FilterTableAdapter;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlConstructionManagerFilter;
        private DevExpress.XtraEditors.SimpleButton btnConstructionManagerFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlContractDirectorFilter;
        private DevExpress.XtraEditors.SimpleButton btnContractDirectorFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private System.Windows.Forms.BindingSource sp06388OMClientContractManagerConstructionManagerFilterBindingSource;
        private DataSet_OM_ContractTableAdapters.sp06388_OM_Client_Contract_Manager_Construction_Manager_FilterTableAdapter sp06388_OM_Client_Contract_Manager_Construction_Manager_FilterTableAdapter;
        private System.Windows.Forms.BindingSource sp06389OMClientContractManagerContractDirectorFilterBindingSource;
        private DataSet_OM_ContractTableAdapters.sp06389_OM_Client_Contract_Manager_Contract_Director_FilterTableAdapter sp06389_OM_Client_Contract_Manager_Contract_Director_FilterTableAdapter;
        private DevExpress.XtraBars.Docking.AutoHideContainer hideContainerLeft;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHTML1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHTML2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHTML3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHTML4;
        private DevExpress.XtraBars.BarStaticItem bsiSelectedCount;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageBillingRequirements;
        private DevExpress.XtraGrid.GridControl gridControlBillingRequirement;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewBillingRequirement;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private System.Windows.Forms.BindingSource sp01037CoreBillingRequirementItemsBindingSource;
        private DataSet_Common_Functionality dataSet_Common_Functionality;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingRequirementID;
        private DevExpress.XtraGrid.Columns.GridColumn colRequirementID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordID1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordTypeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientBillRequirement;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit5;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamSelfBillRequirement;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks5;
        private DevExpress.XtraGrid.Columns.GridColumn colRequirementFulfilled;
        private DevExpress.XtraGrid.Columns.GridColumn colCheckedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingRequirement;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecord1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordType1;
        private DevExpress.XtraGrid.Columns.GridColumn colCheckedByStaff;
        private DataSet_Common_FunctionalityTableAdapters.sp01037_Core_Billing_Requirement_ItemsTableAdapter sp01037_Core_Billing_Requirement_ItemsTableAdapter;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlendingBillingRequirement;
        private DevExpress.XtraGrid.Columns.GridColumn colSignatureForClientBilling;
        private DevExpress.XtraGrid.Columns.GridColumn colSignatureForTeamSelfBilling;
        private DevExpress.XtraGrid.Columns.GridColumn colTenderProactiveDaysToReturnQuote;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditInteger;
        private DevExpress.XtraGrid.Columns.GridColumn colTenderReactiveDaysToReturnQuote;
        private DevExpress.XtraGrid.Columns.GridColumn colCMTenderRequestNotesFile;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditTenderRequestNotesFile;
        private DevExpress.XtraGrid.Columns.GridColumn colDefaultCompletionSheetTemplate;
        private DevExpress.XtraGrid.Columns.GridColumn colDefaultCompletionSheetTemplateID;
    }
}

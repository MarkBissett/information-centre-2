﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_OM_Site_Contract_Wizard_Block_Edit_Site_Year_Billing_Info : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        public DateTime? dtInvoiceDateDue = null;
        public decimal? decEstimatedBillAmount = null;
        public DateTime? dtInvoiceDateActual = null;
        public decimal? decActualBillAmount = null;
        public string strRemarks = null;
        #endregion

        public frm_OM_Site_Contract_Wizard_Block_Edit_Site_Year_Billing_Info()
        {
            InitializeComponent();
        }

        private void frm_OM_Site_Contract_Wizard_Block_Edit_Site_Year_Billing_Info_Load(object sender, EventArgs e)
        {
            this.FormID = 500134;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            Application.DoEvents();  // Allow Form time to repaint itself //

            InvoiceDateDueDateEdit.EditValue = null;
            EstimatedBillAmountSpinEdit.EditValue = null;
            InvoiceDateActualDateEdit.EditValue = null;
            ActualBillAmountSpinEdit.EditValue = null;
            RemarksMemoEdit.EditValue = null;
            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (InvoiceDateDueDateEdit.EditValue != null) dtInvoiceDateDue = Convert.ToDateTime(InvoiceDateDueDateEdit.EditValue);
            if (EstimatedBillAmountSpinEdit.EditValue != null) decEstimatedBillAmount = Convert.ToDecimal(EstimatedBillAmountSpinEdit.EditValue);
            if (InvoiceDateActualDateEdit.EditValue != null) dtInvoiceDateActual = Convert.ToDateTime(InvoiceDateActualDateEdit.EditValue);
            if (ActualBillAmountSpinEdit.EditValue != null) decActualBillAmount = Convert.ToDecimal(ActualBillAmountSpinEdit.EditValue);
            if (RemarksMemoEdit.EditValue != null) strRemarks = RemarksMemoEdit.EditValue.ToString();
              
            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }






    }
}

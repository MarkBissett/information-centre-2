using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;  // Required for Path Statement //
using System.Xml;
using System.Linq;
using System.Xml.Linq;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraBars;
using DevExpress.XtraTab;
using DevExpress.XtraBars.Docking;
using DevExpress.XtraLayout;
using DevExpress.XtraDataLayout;

using DevExpress.Utils.Menu;  // Required to disable Column Chooser on Grids //
using DevExpress.XtraGrid.Localization;  // Required to disable Column Chooser on Grids //

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;
using LocusEffects;
using WoodPlan5.Classes.Operations;
using Utilities;  // Used by Datasets //

namespace WoodPlan5
{
    public partial class frm_OM_Tender_Manager : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        private string strConnectionStringREADONLY = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;
        bool iBool_TemplateManager = false;
        bool iBool_BillingRequirementTemplateManager = false;

        private Utils.enmFocusedGrid _enmFocusedGrid = Utils.enmFocusedGrid.Jobs;

        bool iBool_AddWizardButtonEnabled = false;
        bool iBool_SendButtonEnabled = false;
        bool iBool_SendQuoteToCMEnabled = false;
        bool iBool_ManualStatusChange = false;  // Picked up in this screen then passed through to Edit Tender screen //
        bool iBool_VisitDrillDownEnabled = false;
        bool iBool_EnableGridColumnChooser = true;
        bool iBool_VisitFullEditAccess = false;
        bool iBool_ManuallyCompleteExtraWorksVisitButtonEnabled = false;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        public RefreshGridState RefreshGridViewStateTender;
        public RefreshGridState RefreshGridViewStateVisit;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewStateHistory;
        public RefreshGridState RefreshGridViewStateCRM;
        public RefreshGridState RefreshGridViewStateLinkedDocument;

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        string i_str_AddedRecordIDsTender = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDsVisit = "";
        string i_str_AddedRecordIDsHistory = "";
        string i_str_AddedRecordIDsCRM = "";
        string i_str_AddedRecordIDsLinkedDocument = "";

        string i_str_selected_TenderStatus_ids = "";
        string i_str_selected_TenderStatus_descriptions = "";
        BaseObjects.GridCheckMarksSelection selection1;

        BaseObjects.GridCheckMarksSelection selectionTabPages;

        string i_str_selected_KAM_ids = "";
        string i_str_selected_KAM_names = "";
        BaseObjects.GridCheckMarksSelection selection2;

        string i_str_selected_CM_ids = "";
        string i_str_selected_CM_names = "";
        BaseObjects.GridCheckMarksSelection selection3;

        string i_str_selected_CreatedByStaff_ids = "";
        string i_str_selected_CreatedByStaff_descriptions = "";
        BaseObjects.GridCheckMarksSelection selection4;

        string i_str_selected_JobType_ids = "";
        string i_str_selected_JobType_descriptions = "";
        BaseObjects.GridCheckMarksSelection selection5;

        string i_str_selected_JobSubType_ids = "";
        string i_str_selected_JobSubType_descriptions = "";
        BaseObjects.GridCheckMarksSelection selection6;

        string i_str_selected_client_ids = "";
        string i_str_selected_client_names = "";
        string i_str_selected_site_ids = "";
        string i_str_selected_site_names = "";


        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        public string strPassedInDrillDownIDs = "";

        Bitmap bmpBlank = null;
        Bitmap bmpAlert = null;

        private DateTime i_dtStart = DateTime.MinValue;  // Used if Panel is Date Range //
        private DateTime i_dtEnd = DateTime.MaxValue;  // Used if Panel is Date Range //
        private DateTime i_dtStart2 = DateTime.MinValue;  // Used if Panel is Date From //
        private DateTime i_dtEnd2 = DateTime.MaxValue;  // Used if Panel is Date Range //
        private int intDaysPrior = 30;  // Used if Panel is Date From //
        private int intDaysAfter = 30;  // Used if Panel is Date From //

        private string i_str_selected_TabPages = "";

        private LocusEffects.LocusEffectsProvider locusEffectsProvider1;
        private ArrowLocusEffect m_customArrowLocusEffect1 = null;

        SuperToolTip superToolTipSiteContractFilter = null;
        SuperToolTipSetupArgs superToolTipSetupArgsSiteContractFilter = null;

        SuperToolTip superToolTipClientContractFilter = null;
        SuperToolTipSetupArgs superToolTipSetupArgsClientContractFilter = null;

        private DataSet_Selection DS_Selection1;

        private string strSignaturePath = "";  // Holds the first part of the path, retrieved from the System_Settings table //
        private string strLinkedDocumentPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //
        private string strTenderLinkedDocumentPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //
        private string strDefaultPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //
        private string i_str_ClientQuoteSpreadsheetExtractFolder = "";
        private string i_str_TenderRequestNotesFolder = "";

        private bool boolSyncTenderGridToPanel = false;

        // Following Used to merge updated changes back into the original dataset without reloading the whole dataset. IMPORTANT NOTE: backgroundWorker1 added as object to form and events set on it. //
        private string strEditedTenderIDs = "";
        bool boolProcessRunning = false;
        private int intErrorValue = 0;
        private string strErrorMessage = "";
        SqlDataAdapter sdaDataMerge = null;
        DataSet dsDataMerge = null;
        SqlCommand cmd = null;

        private Dictionary<string, bool> dictionaryDataRefreshesMain;

        #endregion

        public frm_OM_Tender_Manager()
        {
            InitializeComponent();
        }

        private void frm_OM_Tender_Manager_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 7022;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;
            strConnectionStringREADONLY = GlobalSettings.ConnectionStringREADONLY;

            Set_Grid_Highlighter_Transparent(this.Controls);

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, FormID, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);
            ProcessPermissionsForForm();

            bmpBlank = new Bitmap(16, 16);
            bmpAlert = new Bitmap(imageCollection1.Images[15], 16, 16);
            
            DS_Selection1 = new Utilities.DataSet_Selection((GridView)gridViewTender, strConnectionString);  // Used by DataSets //

            #region Get Default Paths
            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionStringREADONLY);
            try
            {
                strSignaturePath = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_PictureFilesFolder").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for Client Pictures and Job Pictures (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Linked Job Pictures Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            try
            {
                strLinkedDocumentPath = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_LinkedDocumentPath").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Linked Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            //if (!strLinkedDocumentPath.EndsWith("\\")) strLinkedDocumentPath += "\\";

            try
            {
                strTenderLinkedDocumentPath = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_TenderLinkedDocumentPath").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Tender Linked Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Tender Linked Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            //if (!strTenderLinkedDocumentPath.EndsWith("\\")) strTenderLinkedDocumentPath += "\\";

            try
            {
                i_str_ClientQuoteSpreadsheetExtractFolder = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_ClientQuoteSpreadsheetExtractFolder").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for the Saved Client Quote Spreadsheet Extracts (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Client Quote Spreadsheet Extract Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (!i_str_ClientQuoteSpreadsheetExtractFolder.EndsWith("\\")) i_str_ClientQuoteSpreadsheetExtractFolder += "\\";  // Add Backslash to end //

            try
            {
                i_str_TenderRequestNotesFolder = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_CMTenderRequestNotesFileFolder").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for the Tender Request Notes (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Tender Request Notes Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (!i_str_TenderRequestNotesFolder.EndsWith("\\")) i_str_TenderRequestNotesFolder += "\\";  // Add Backslash to end //                  
            #endregion
            
            sp06491_OM_Tender_ManagerTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            RefreshGridViewStateTender = new RefreshGridState(gridViewTender, "TenderID");
            gridControlTender.ForceInitialize();

            sp06495_OM_Visits_Linked_to_TenderTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;                       
            RefreshGridViewStateVisit = new RefreshGridState(gridViewVisit, "VisitID");

            #region Filter Panel

            buttonEditClientFilter.EditValue = "";

            // Set defualt Date Range. Days From Today values are set in LoadLastSavedUserScreenSettings method //
            i_dtStart = new DateTime(DateTime.Today.Year, 1, 1);
            i_dtEnd = new DateTime(DateTime.Today.AddYears(1).Year, 1, 1);
            dateEditFromDate.DateTime = i_dtStart;
            dateEditToDate.DateTime = i_dtEnd;
            popupContainerEditDateRange.EditValue = PopupContainerEditDateRange_Get_Selected();

            // Add record selection checkboxes to popup Visit System Status grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl4.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;
            sp06489_OM_Tender_Status_FilterTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            sp06489_OM_Tender_Status_FilterTableAdapter.Fill(dataSet_OM_Tender.sp06489_OM_Tender_Status_Filter);
            gridControl4.ForceInitialize();

            // Add record selection checkboxes to popup KAM Filter grid control //
            selection2 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl5.MainView);
            selection2.CheckMarkColumn.VisibleIndex = 0;
            selection2.CheckMarkColumn.Width = 30;
            sp06019_OM_Job_Manager_KAMs_FilterTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            sp06019_OM_Job_Manager_KAMs_FilterTableAdapter.Fill(dataSet_GC_Summer_Core.sp06019_OM_Job_Manager_KAMs_Filter);
            gridControl5.ForceInitialize();

            // Add record selection checkboxes to popup CM Filter grid control //
            selection3 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl9.MainView);
            selection3.CheckMarkColumn.VisibleIndex = 0;
            selection3.CheckMarkColumn.Width = 30;
            sp06020_OM_Job_Manager_CMs_FilterTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            sp06020_OM_Job_Manager_CMs_FilterTableAdapter.Fill(dataSet_GC_Summer_Core.sp06020_OM_Job_Manager_CMs_Filter);
            gridControl9.ForceInitialize();

            sp06490_OM_Tender_Staff_FilterTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            sp06490_OM_Tender_Staff_FilterTableAdapter.Fill(this.dataSet_OM_Tender.sp06490_OM_Tender_Staff_Filter);
            gridControl12.ForceInitialize();
            // Add record selection checkboxes to popup Callout Type grid control //
            selection4 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl12.MainView);
            selection4.CheckMarkColumn.VisibleIndex = 0;
            selection4.CheckMarkColumn.Width = 30;

            sp06493_OM_Job_Type_FilterTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            sp06493_OM_Job_Type_FilterTableAdapter.Fill(dataSet_OM_Tender.sp06493_OM_Job_Type_Filter);
            gridControl10.ForceInitialize();
            // Add record selection checkboxes to popup Callout Type grid control //
            selection5 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl10.MainView);
            selection5.CheckMarkColumn.VisibleIndex = 0;
            selection5.CheckMarkColumn.Width = 30;

            sp06494_OM_Job_Sub_Type_FilterTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            sp06494_OM_Job_Sub_Type_FilterTableAdapter.Fill(dataSet_OM_Tender.sp06494_OM_Job_Sub_Type_Filter);
            gridControl11.ForceInitialize();
            // Add record selection checkboxes to popup Callout Type grid control //
            selection6 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl11.MainView);
            selection6.CheckMarkColumn.VisibleIndex = 0;
            selection6.CheckMarkColumn.Width = 30;

            #endregion

            sp06515_OM_Tender_History_For_TenderTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            RefreshGridViewStateHistory = new RefreshGridState(gridViewHistory, "TenderHistoryID");

            sp05089_CRM_Contacts_Linked_To_RecordTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            RefreshGridViewStateCRM = new RefreshGridState(gridViewCRM, "CRMID");

            sp06514_OM_Linked_Documents_Linked_To_RecordTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            RefreshGridViewStateLinkedDocument = new RefreshGridState(gridView3, "LinkedDocumentID");

            #region populate_Available_TabPages_List and Dictionary Refreshes

            selectionTabPages = new BaseObjects.GridCheckMarksSelection((GridView)gridControl7.MainView);
            selectionTabPages.CheckMarkColumn.VisibleIndex = 0;
            selectionTabPages.CheckMarkColumn.Width = 30;

            dictionaryDataRefreshesMain = new Dictionary<string, bool>();

            String strDefaultLinkedLevelsToView = "";
            gridControl7.BeginUpdate();
            this.dataSet_GC_Core.sp04022_Core_Dummy_TabPageList.Rows.Clear();
            foreach (XtraTabPage i in xtraTabControl2.TabPages)
            {
                DataRow drNewRow;
                drNewRow = this.dataSet_GC_Core.sp04022_Core_Dummy_TabPageList.NewRow();
                drNewRow["TabPageName"] = i.Text;
                drNewRow["ParentLevel"] = "Tender";
                this.dataSet_GC_Core.sp04022_Core_Dummy_TabPageList.Rows.Add(drNewRow);
                dictionaryDataRefreshesMain.Add(i.Text, false);
                strDefaultLinkedLevelsToView += (i.Tag.ToString() == "1" ? i.Text + "," : "");
            }
            //dictionaryDataRefreshesMain.Add("VisitPanel", false);  // Extra row for the Vist Panel to control its refreshes //

            gridControl7.EndUpdate();
            gridControl7.ForceInitialize();
            GridView view = (GridView)gridControl7.MainView;
            view.ExpandAllGroups();

            // Make default Tab Pages visible [setting to control default visible held in each pages Tab setting - 1 = yes, 0 = no //
            int intFoundRow = 0;
            Array arrayItems = strDefaultLinkedLevelsToView.Split(',');  // Single quotes because char expected for delimeter //
            view.BeginUpdate();
            intFoundRow = 0;
            foreach (string strElement in arrayItems)
            {
                if (strElement == "") break;
                intFoundRow = view.LocateByValue(0, view.Columns["TabPageName"], strElement.Trim());
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                    view.MakeRowVisible(intFoundRow, false);
                }
            }
            view.EndUpdate();
            popupContainerEditShowTabPages.Text = PopupContainerEditShowTabPages_Get_Selected();

            #endregion

            if (strPassedInDrillDownIDs != "")  // Opened in drill-down mode //
            {
                tabbedControlGroupFilterType.SelectedTabPageIndex = 1;
                TenderIDsMemoEdit.EditValue = strPassedInDrillDownIDs;
                Load_Data();  // Load records //
            }
            else
            {
                tabbedControlGroupFilterType.SelectedTabPageIndex = 0;
                TenderIDsMemoEdit.EditValue = "";
            }
            popupContainerControlVisitSystemStatusFilter.Size = new System.Drawing.Size(270, 400);
            popupContainerControlKAMFilter.Size = new System.Drawing.Size(270, 400);
            popupContainerControlCMFilter.Size = new System.Drawing.Size(270, 400);
            popupContainerControlDateRange.Size = new System.Drawing.Size(210, 107);
            popupContainerControlShowTabPages.Size = new System.Drawing.Size(190, 450);
            popupContainerControlLabourFilter.Size = new System.Drawing.Size(270, 500);
            popupContainerControlJobTypeFilter.Size = new System.Drawing.Size(270, 400);
            popupContainerControlJobSubTypeFilter.Size = new System.Drawing.Size(270, 400);

            // Create a SuperToolTip //
            superToolTipSiteContractFilter = new SuperToolTip();
            superToolTipSetupArgsSiteContractFilter = new SuperToolTipSetupArgs();
            superToolTipSetupArgsSiteContractFilter.Title.Text = "Site Contract Filter - Information";
            superToolTipSetupArgsSiteContractFilter.Title.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            superToolTipSetupArgsSiteContractFilter.Contents.Text = "I store the currently applied Site Filter.\n\nNo Filter Set.";
            superToolTipSetupArgsSiteContractFilter.ShowFooterSeparator = false;
            superToolTipSetupArgsSiteContractFilter.Footer.Text = "";
            superToolTipSiteContractFilter.Setup(superToolTipSetupArgsSiteContractFilter);
            superToolTipSiteContractFilter.AllowHtmlText = DefaultBoolean.True;
            buttonEditSiteFilter.SuperTip = superToolTipSiteContractFilter;

            // Create a SuperToolTip //
            superToolTipClientContractFilter = new SuperToolTip();
            superToolTipSetupArgsClientContractFilter = new SuperToolTipSetupArgs();
            superToolTipSetupArgsClientContractFilter.Title.Text = "Client Contract Filter - Information";
            superToolTipSetupArgsClientContractFilter.Title.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            superToolTipSetupArgsClientContractFilter.Contents.Text = "I store the currently applied Client Filter.\n\nNo Filter Set.";
            superToolTipSetupArgsClientContractFilter.ShowFooterSeparator = false;
            superToolTipSetupArgsClientContractFilter.Footer.Text = "";
            superToolTipClientContractFilter.Setup(superToolTipSetupArgsClientContractFilter);
            superToolTipClientContractFilter.AllowHtmlText = DefaultBoolean.True;
            buttonEditClientFilter.SuperTip = superToolTipClientContractFilter;

            emptyEditor = new RepositoryItem();

            //StyleFormatCondition conditionQuoteOverDue = new DevExpress.XtraGrid.StyleFormatCondition();
            //conditionQuoteOverDue.Appearance.BackColor = Color.FromArgb(255, 112, 77);  // Pale Red //  
            //conditionQuoteOverDue.Appearance.Options.UseBackColor = true;
            //conditionQuoteOverDue.Appearance.ForeColor = Color.Black;
            //conditionQuoteOverDue.Appearance.Options.UseForeColor = true;
            //conditionQuoteOverDue.ApplyToRow = false;
            //conditionQuoteOverDue.Column = colDaysQuoteOverdue;
            //conditionQuoteOverDue.Condition = FormatConditionEnum.Expression;
            //conditionQuoteOverDue.Expression = "[DaysQuoteOverdue] > 0";
            //gridControlTender.MainView.FormatConditions.Add(conditionQuoteOverDue);

            Attach_Enter_To_VisitPanelFields();  // Detached on Form Closing Event //

        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            _KeepWaitFormOpen = false;
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();

            Application.DoEvents();  // Allow Form time to repaint itself //
            
            if (strPassedInDrillDownIDs == "")  // Not opened in drill-down mode so load last saved screen settings for current user //
            {
                LoadLastSavedUserScreenSettings();
            }
            gridControlTender.Focus();  // Need this to make tooltip on column start working as soon as form opens otherwise it doesn't start until user clicks into a grid //
        }

        private void frm_OM_Tender_Manager_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedRecordIDsTender))
                {
                    Set_DictionaryDataRefreshes_One_Value(dictionaryDataRefreshesMain, xtraTabPageVisits.Text, true);
                    Set_DictionaryDataRefreshes_One_Value(dictionaryDataRefreshesMain, xtraTabPageHistory.Text, true);  // Force Linked Visits to refresh //
                    Load_Data();
                }
                if (UpdateRefreshStatus == 2 || !string.IsNullOrEmpty(i_str_AddedRecordIDsVisit))
                {
                    if (!string.IsNullOrEmpty(i_str_AddedRecordIDsVisit)) Load_Data();  // New visit(s) added so refresh tenders so updated visit totals are shown //

                    Set_DictionaryDataRefreshes_One_Value(dictionaryDataRefreshesMain, xtraTabPageVisits.Text, true);
                    LoadLinkedRecords();
                }
                if (UpdateRefreshStatus == 3 || !string.IsNullOrEmpty(i_str_AddedRecordIDsHistory))
                {
                    Set_DictionaryDataRefreshes_One_Value(dictionaryDataRefreshesMain, xtraTabPageHistory.Text, true);
                    LoadLinkedRecords();
                }
                if (UpdateRefreshStatus == 4 || !string.IsNullOrEmpty(i_str_AddedRecordIDsCRM))
                {
                    Set_DictionaryDataRefreshes_One_Value(dictionaryDataRefreshesMain, xtraTabPageCRM.Text, true);
                    LoadLinkedRecords();
                }
                if (UpdateRefreshStatus == 5 || !string.IsNullOrEmpty(i_str_AddedRecordIDsLinkedDocument))
                {
                    Set_DictionaryDataRefreshes_One_Value(dictionaryDataRefreshesMain, xtraTabPageLinkedDocuments.Text, true);
                    LoadLinkedRecords();
                }
            }
            SetMenuStatus();
        }

        private void frm_OM_Tender_Manager_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strPassedInDrillDownIDs == "")  // Not opened in drill-down mode so save screen settings for current user //
            {
                if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
                {
                    // Store last used screen settings for current user //
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "TenderStatusFilter", i_str_selected_TenderStatus_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "KAMFilter", i_str_selected_KAM_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "CMFilter", i_str_selected_CM_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ClientFilter", i_str_selected_client_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "SiteFilter", i_str_selected_site_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "CreatedByStaffFilter", i_str_selected_CreatedByStaff_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "JobTypeFilter", i_str_selected_JobType_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "JobSubTypeFilter", i_str_selected_JobSubType_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "IgnoreCompleted", (checkEditIgnoreHistoric.Checked ? "1" : "0"));
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ShownTabPages", i_str_selected_TabPages);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "DateRangeMethod", (checkEditDateRange.Checked ? "0" : "1"));
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "DaysPrior", intDaysPrior.ToString());
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "DaysAfter", intDaysAfter.ToString());
                    default_screen_settings.SaveDefaultScreenSettings();
                }
            }
            Detach_Enter_From_VisitPanelFields();
            bbiLoadingCancel.PerformClick();  // Cancel any background loading of data //
        }

        private void Attach_Enter_To_VisitPanelFields()
        {
            foreach (var item in dataLayoutControl1.Items) 
            {
                LayoutControlItem i = item as LayoutControlItem;
                if (i != null) 
                {
                    var c = i.Control;
                    if (c is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)c).Enter += new EventHandler(Generic_Enter);
                }
            } 
        }
        private void Detach_Enter_From_VisitPanelFields()
        {
            foreach (var item in dataLayoutControl1.Items)
            {
                LayoutControlItem i = item as LayoutControlItem;
                if (i != null)
                {
                    var c = i.Control;
                    try
                    {
                        if (c is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)c).Enter -= new EventHandler(Generic_Enter);
                    }
                    catch (Exception) { }
                }
            }
        }
        private void Generic_Enter(object sender, EventArgs e)
        {
            if (!boolSyncTenderGridToPanel) return;

            Control c = (Control)sender;
            string strField = "";
            foreach (Binding thisBinding in c.DataBindings)
            {
                BindingMemberInfo bInfo = thisBinding.BindingMemberInfo;
                strField = bInfo.BindingField.ToString();
                break;
            }  

            if (string.IsNullOrWhiteSpace(strField)) return;

            GridView view = (GridView)gridControlTender.MainView;
            GridColumn col = view.Columns[strField];
            if (col == null) return;

            if (col.VisibleIndex <= -1) return;

            view.MakeColumnVisible(col);
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                string strItemFilter = "";
                int intFoundRow = 0;

                // Date Range From Today section //
                strItemFilter = default_screen_settings.RetrieveSetting("DaysPrior");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    spinEditDaysPrior.EditValue = Convert.ToInt32(strItemFilter);
                    intDaysPrior = Convert.ToInt32(strItemFilter);
                }
                else
                {
                    spinEditDaysPrior.EditValue = 30;
                }
                strItemFilter = default_screen_settings.RetrieveSetting("DaysAfter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    spinEditDaysAfter.EditValue = Convert.ToInt32(strItemFilter);
                    intDaysAfter = Convert.ToInt32(strItemFilter);
                }
                else
                {
                    spinEditDaysAfter.EditValue = 30;
                }
                strItemFilter = default_screen_settings.RetrieveSetting("DateRangeMethod");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    if (strItemFilter == "0")
                    {
                        checkEditDateRange.Checked = true;
                    }
                    else
                    {
                        checkEditDatesFromToday.Checked = true;
                    }
                }
                else
                {
                    checkEditDateRange.Checked = true;
                }
                if (checkEditDatesFromToday.Checked) popupContainerEditDateRange.Text = PopupContainerEditDateRange_Get_Selected();

                // Visit System Filter //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("TenderStatusFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl4.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["ID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEditVisitSystemStatusFilter.Text = PopupContainerEditVisitSystemStatusFilter_Get_Selected();
                }

                // KAM Filter //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("KAMFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl5.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["ID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEditKAMFilter.Text = PopupContainerEditKAMFilter_Get_Selected();
                }

                // CM Filter //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("CMFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl9.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["ID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEditCMFilter.Text = PopupContainerEditCMFilter_Get_Selected();
                }

                // Client Filter //
                strItemFilter = default_screen_settings.RetrieveSetting("ClientFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    i_str_selected_client_ids = strItemFilter;
                    var ResolveIDs = new DataSet_GC_Summer_CoreTableAdapters.QueriesTableAdapter();
                    ResolveIDs.ChangeConnectionString(strConnectionString);
                    try
                    {
                        buttonEditClientFilter.Text = ResolveIDs.sp06023_OM_Get_Clients_From_ClientIDs(strItemFilter).ToString();
                        i_str_selected_client_names = buttonEditClientFilter.Text;
                        
                        // Update Filter control's tooltip //
                        string strTooltipText = i_str_selected_client_names.Replace(", ", "\n");
                        superToolTipSetupArgsClientContractFilter.Contents.Text = "I store the currently applied Client Filter.\n\n" + strTooltipText;
                    }
                    catch (Exception) { }
                }

                // Site Filter //
                strItemFilter = default_screen_settings.RetrieveSetting("SiteFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    i_str_selected_site_ids = strItemFilter;
                    var ResolveIDs = new DataSet_GC_Summer_CoreTableAdapters.QueriesTableAdapter();
                    ResolveIDs.ChangeConnectionString(strConnectionString);
                    try
                    {
                        buttonEditSiteFilter.Text = ResolveIDs.sp06024_OM_Get_Sites_From_SiteIDs(strItemFilter).ToString();
                        i_str_selected_site_names = buttonEditSiteFilter.Text;
                        
                        // Update Filter control's tooltip //
                        string strTooltipText = i_str_selected_site_names.Replace(", ", "\n");
                        superToolTipSetupArgsSiteContractFilter.Contents.Text = "I store the currently applied Site Filter.\n\n" + strTooltipText;
                    }
                    catch (Exception) { }
                }

                // Labour Filter //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("CreatedByStaffFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl12.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["ID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEditLabourFilter.Text = PopupContainerEditLabourFilter_Get_Selected();
                }

                // Job Type Filter //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("JobTypeFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl10.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["ID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEditJobTypeFilter.Text = PopupContainerEditJobTypeFilter_Get_Selected();
                }

                // Job Sub-Type Filter //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("JobSubTypeFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl11.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["ID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEditJobSubTypeFilter.Text = PopupContainerEditJobSubTypeFilter_Get_Selected();
                }

                // Shown Tab Pages //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("ShownTabPages");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    // Clear any selection (may have been set when default visible tab pages were set on load of screen //
                    selectionTabPages.ClearSelection();

                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl7.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["TabPageName"], strElement.Trim());
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEditShowTabPages.Text = PopupContainerEditShowTabPages_Get_Selected();
                }

                // Ignore Historic //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("IgnoreCompleted");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    checkEditIgnoreHistoric.Checked = (strItemFilter == "1");
                }
                Load_Data();

                // Prepare LocusEffects and add custom effect //
                locusEffectsProvider1 = new LocusEffectsProvider();
                locusEffectsProvider1.Initialize();
                locusEffectsProvider1.FramesPerSecond = 30;
                m_customArrowLocusEffect1 = new ArrowLocusEffect();
                m_customArrowLocusEffect1.Name = "CustomeArrow1";
                m_customArrowLocusEffect1.AnimationStartColor = Color.Orange;
                m_customArrowLocusEffect1.AnimationEndColor = Color.Red;
                m_customArrowLocusEffect1.MovementMode = MovementMode.OneWayAlongVector;
                m_customArrowLocusEffect1.MovementCycles = 20;
                m_customArrowLocusEffect1.MovementAmplitude = 200;
                m_customArrowLocusEffect1.MovementVectorAngle = 45; //degrees
                m_customArrowLocusEffect1.LeadInTime = 0; //msec
                m_customArrowLocusEffect1.LeadOutTime = 1000; //msec
                m_customArrowLocusEffect1.AnimationTime = 2000; //msec
                locusEffectsProvider1.AddLocusEffect(m_customArrowLocusEffect1);

                Point location = dockPanelFilters.PointToScreen(Point.Empty);

                System.Drawing.Point screenPoint = new System.Drawing.Point(location.X + 10, location.Y + 5);
                locusEffectsProvider1.ShowLocusEffect(this, screenPoint, m_customArrowLocusEffect1.Name);
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            if (boolProcessRunning) return;  // Abort since process already running //
            
            dataSet_OM_Tender.sp06491_OM_Tender_Manager.Clear(); // Clear all existing data since the data picked up by the merge may be a smaller dataset due to any new filters in place //

            backgroundWorker1.RunWorkerAsync();
            boolProcessRunning = true;
            SetLoadingVisibility(true);
        }

        private void Load_Data()
        {
            if (boolProcessRunning) return;  // Abort since process already running //
            backgroundWorker1.RunWorkerAsync();
            boolProcessRunning = true;
            SetLoadingVisibility(true);
        }

        private void Populate_Tenders()
        {
            intErrorValue = 0;  // Reset error flag //
            strErrorMessage = "";
            sdaDataMerge = new SqlDataAdapter();
            dsDataMerge = new DataSet("NewDataSet");

            if (i_str_selected_site_ids == null) i_str_selected_site_ids = "";
            int intIgnoreHistoric = (checkEditIgnoreHistoric.Checked ? 1 : 0);

            DateTime dtStart = (checkEditDateRange.Checked ? i_dtStart : i_dtStart2);
            DateTime dtEnd = (checkEditDateRange.Checked ? i_dtEnd : i_dtEnd2);

            string strTempDrilldownIDs = "";
            if (UpdateRefreshStatus > 0)
            {
                UpdateRefreshStatus = 0;
                // Merge just changed rows back in to dataset - no need to reload the full dataset //
                strTempDrilldownIDs = (string.IsNullOrWhiteSpace(i_str_AddedRecordIDsTender) ? strEditedTenderIDs : i_str_AddedRecordIDsTender.Replace(';', ','));
                if (string.IsNullOrEmpty(strTempDrilldownIDs))
                {
                    backgroundWorker1.CancelAsync();  // Nothing to load so abort cleanly //
                    return;
                }
            }
            else if (tabbedControlGroupFilterType.SelectedTabPageIndex == 1 && !string.IsNullOrWhiteSpace(TenderIDsMemoEdit.EditValue.ToString()))
            {
                strTempDrilldownIDs = TenderIDsMemoEdit.EditValue.ToString();
            }
            try
            {
                using (var conn = new SqlConnection(strConnectionStringREADONLY))
                {
                    // Load data into memory - Background worked will merge this on RunWorkerCompleted event //
                    conn.Open();
                    cmd = new SqlCommand("sp06491_OM_Tender_Manager", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@DrillDownIDs", strTempDrilldownIDs));
                    cmd.Parameters.Add(new SqlParameter("@StartDate", dtStart));
                    cmd.Parameters.Add(new SqlParameter("@EndDate", dtEnd));
                    cmd.Parameters.Add(new SqlParameter("@KAMFilter", i_str_selected_KAM_ids));
                    cmd.Parameters.Add(new SqlParameter("@CMFilter", i_str_selected_CM_ids));
                    cmd.Parameters.Add(new SqlParameter("@ClientFilter", i_str_selected_client_ids));
                    cmd.Parameters.Add(new SqlParameter("@SiteFilter", i_str_selected_site_ids));
                    cmd.Parameters.Add(new SqlParameter("@StatusIDs", i_str_selected_TenderStatus_ids));
                    cmd.Parameters.Add(new SqlParameter("@CreatedByIDs", i_str_selected_CreatedByStaff_ids));
                    cmd.Parameters.Add(new SqlParameter("@IgnoreCompleted", intIgnoreHistoric));
                    cmd.Parameters.Add(new SqlParameter("@JobTypeIDs", i_str_selected_JobType_ids));
                    cmd.Parameters.Add(new SqlParameter("@JobSubTypeIDs", i_str_selected_JobSubType_ids));
                    sdaDataMerge = new SqlDataAdapter(cmd);
                    sdaDataMerge.Fill(dsDataMerge, "Table");
                    conn.Close();
                }
            }
         catch (SqlException ex)
            {
                if (ex.Number == -2)  // SQL Server Time-Out //
                {
                    intErrorValue = -2;
                    return;
                }
                else  // Other SQL Server issue //
                {
                    intErrorValue = -3;
                    strErrorMessage = ex.Message.ToString();
                    return;
                }
            }
            catch (Exception ex)  // Some other issue //
            {
                intErrorValue = -1;
                strErrorMessage = ex.Message.ToString();
                return;
            }
        }


        private void Set_DictionaryDataRefreshes_All_Value(Dictionary<string, bool> dictionary, bool boolValue)
        {
            // Set all dictionary values to passed in value. NOTE: Can't use a foreach loop to modify the contents of a dictionary as it causes an error //
            for (int i = 0; i < dictionary.Count; i++)
            {
                dictionary[dictionary.ElementAt(i).Key] = boolValue;
            }
        }
        private void Set_DictionaryDataRefreshes_One_Value(Dictionary<string, bool> dictionary, string strKey, bool boolValue)
        {
            if (dictionary.ContainsKey(strKey)) dictionary[strKey] = boolValue;
        }

        private void LoadLinkedRecords()
        {
            //if (dockPanelVisitDetails.Visibility == DockVisibility.Visible)
            //{
            //    string strText = "VisitPanel";
            //    if (dictionaryDataRefreshesMain.ContainsKey(strText))
            //    {
            //        bool boolRefreshRequired = false;
            //        dictionaryDataRefreshesMain.TryGetValue(strText, out boolRefreshRequired);
            //        if (boolRefreshRequired)
            //        {
            //            Load_Visit_Panel_Jobs();
            //            Load_Visit_Panel_Labour();
            //            dictionaryDataRefreshesMain[strText] = false;  // Mark this as loaded //
            //        }
            //    }
            //}

            if (splitContainerControl1.Collapsed) return;  // Don't bother loading related data as the parent panel is collapsed so the grids are invisible //

            bool boolSplashScreenVisibile = splashScreenManager.IsSplashFormVisible;
            if (!boolSplashScreenVisibile)
            {
                this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Loading Linked Data...");
            }

            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControlTender.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = Get_Selected_IDs(view, "TenderID");

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;

            if (xtraTabPageVisits.PageVisible)
            {
                if (xtraTabControl2.SelectedTabPage == xtraTabPageVisits)
                {
                    string strText = xtraTabPageVisits.Text;
                    if (dictionaryDataRefreshesMain.ContainsKey(strText))
                    {
                        bool boolRefreshRequired = false;
                        dictionaryDataRefreshesMain.TryGetValue(strText, out boolRefreshRequired);
                        if (boolRefreshRequired)
                        {
                            dictionaryDataRefreshesMain[strText] = false;  // Mark this page as loaded //

                            gridControlVisit.MainView.BeginUpdate();
                            RefreshGridViewStateVisit.SaveViewInfo();  // Store expanded groups and selected rows //
                            if (intCount == 0)
                            {
                                dataSet_OM_Tender.sp06495_OM_Visits_Linked_to_Tender.Clear();
                            }
                            else
                            {
                                try
                                {
                                    sp06495_OM_Visits_Linked_to_TenderTableAdapter.Fill(dataSet_OM_Tender.sp06495_OM_Visits_Linked_to_Tender, strSelectedIDs);
                                }
                                catch (Exception) { }
                                RefreshGridViewStateVisit.LoadViewInfo();  // Reload any expanded groups and selected rows //
                            }
                            gridControlVisit.MainView.EndUpdate();

                            // Highlight any recently added new rows //
                            if (i_str_AddedRecordIDsVisit != "")
                            {
                                strArray = i_str_AddedRecordIDsVisit.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                                intID = 0;
                                int intRowHandle = 0;
                                view = (GridView)gridControlVisit.MainView;
                                view.BeginSelection();
                                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                                foreach (string strElement in strArray)
                                {
                                    intID = Convert.ToInt32(strElement);
                                    intRowHandle = view.LocateByValue(0, view.Columns["VisitID"], intID);
                                    if (intRowHandle != GridControl.InvalidRowHandle)
                                    {
                                        view.SelectRow(intRowHandle);
                                        view.MakeRowVisible(intRowHandle, false);
                                    }
                                }
                                i_str_AddedRecordIDsVisit = "";
                                view.EndSelection();
                            }
                        }
                    }
                }
            }

            if (xtraTabPageHistory.PageVisible)
            {
                if (xtraTabControl2.SelectedTabPage == xtraTabPageHistory)
                {
                    string strText = xtraTabPageHistory.Text;
                    if (dictionaryDataRefreshesMain.ContainsKey(strText))
                    {
                        bool boolRefreshRequired = false;
                        dictionaryDataRefreshesMain.TryGetValue(strText, out boolRefreshRequired);
                        if (boolRefreshRequired)
                        {
                            dictionaryDataRefreshesMain[strText] = false;  // Mark this page as loaded //

                            gridControlHistory.MainView.BeginUpdate();
                            this.RefreshGridViewStateHistory.SaveViewInfo();  // Store expanded groups and selected rows //
                            if (intCount == 0)
                            {
                                dataSet_OM_Tender.sp06515_OM_Tender_History_For_Tender.Clear();
                            }
                            else
                            {
                                try
                                {
                                    sp06515_OM_Tender_History_For_TenderTableAdapter.Fill(dataSet_OM_Tender.sp06515_OM_Tender_History_For_Tender, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                                }
                                catch (Exception ex) { }
                                this.RefreshGridViewStateHistory.LoadViewInfo();  // Reload any expanded groups and selected rows //
                            }
                            gridControlHistory.MainView.EndUpdate();

                            view = (GridView)gridControlHistory.MainView;
                            view.ExpandAllGroups();

                            // Highlight any recently added new rows //
                            if (i_str_AddedRecordIDsHistory != "")
                            {
                                strArray = i_str_AddedRecordIDsHistory.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                                intID = 0;
                                int intRowHandle = 0;
                                view = (GridView)gridControlHistory.MainView;
                                view.BeginSelection();
                                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                                foreach (string strElement in strArray)
                                {
                                    intID = Convert.ToInt32(strElement);
                                    intRowHandle = view.LocateByValue(0, view.Columns["TenderHistoryID"], intID);
                                    if (intRowHandle != GridControl.InvalidRowHandle)
                                    {
                                        view.SelectRow(intRowHandle);
                                        view.MakeRowVisible(intRowHandle, false);
                                    }
                                }
                                i_str_AddedRecordIDsHistory = "";
                                view.EndSelection();
                            }
                        }
                    }
                }
            }

            if (xtraTabPageCRM.PageVisible)
            {
                if (xtraTabControl2.SelectedTabPage == xtraTabPageCRM)
                {
                    string strText = xtraTabPageCRM.Text;
                    if (dictionaryDataRefreshesMain.ContainsKey(strText))
                    {
                        bool boolRefreshRequired = false;
                        dictionaryDataRefreshesMain.TryGetValue(strText, out boolRefreshRequired);
                        if (boolRefreshRequired)
                        {
                            dictionaryDataRefreshesMain[strText] = false;  // Mark this page as loaded //               

                            gridControlCRM.MainView.BeginUpdate();
                            this.RefreshGridViewStateCRM.SaveViewInfo();  // Store expanded groups and selected rows //
                            if (intCount == 0)
                            {
                                this.woodPlanDataSet.sp05089_CRM_Contacts_Linked_To_Record.Clear();
                            }
                            else
                            {
                                try
                                {
                                    sp05089_CRM_Contacts_Linked_To_RecordTableAdapter.Fill(woodPlanDataSet.sp05089_CRM_Contacts_Linked_To_Record, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), 301);
                                }
                                catch (Exception ex) { }
                                this.RefreshGridViewStateCRM.LoadViewInfo();  // Reload any expanded groups and selected rows //
                            }
                            gridControlCRM.MainView.EndUpdate();

                            view = (GridView)gridControlCRM.MainView;
                            view.ExpandAllGroups();

                            // Highlight any recently added new rows //
                            if (i_str_AddedRecordIDsCRM != "")
                            {
                                strArray = i_str_AddedRecordIDsCRM.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                                intID = 0;
                                int intRowHandle = 0;
                                view = (GridView)gridControlCRM.MainView;
                                view.BeginSelection();
                                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                                foreach (string strElement in strArray)
                                {
                                    intID = Convert.ToInt32(strElement);
                                    intRowHandle = view.LocateByValue(0, view.Columns["CRMID"], intID);
                                    if (intRowHandle != GridControl.InvalidRowHandle)
                                    {
                                        view.SelectRow(intRowHandle);
                                        view.MakeRowVisible(intRowHandle, false);
                                    }
                                }
                                i_str_AddedRecordIDsCRM = "";
                                view.EndSelection();
                            }
                        }
                    }
                }
            }

            if (xtraTabPageLinkedDocuments.PageVisible)
            {
                if (xtraTabControl2.SelectedTabPage == xtraTabPageLinkedDocuments)
                {
                    string strText = xtraTabPageLinkedDocuments.Text;
                    if (dictionaryDataRefreshesMain.ContainsKey(strText))
                    {
                        bool boolRefreshRequired = false;
                        dictionaryDataRefreshesMain.TryGetValue(strText, out boolRefreshRequired);
                        if (boolRefreshRequired)
                        {
                            dictionaryDataRefreshesMain[strText] = false;  // Mark this page as loaded //               

                            gridControl3.MainView.BeginUpdate();
                            this.RefreshGridViewStateLinkedDocument.SaveViewInfo();  // Store expanded groups and selected rows //
                            if (intCount == 0)
                            {
                                dataSet_OM_Core.sp06514_OM_Linked_Documents_Linked_To_Record.Clear();
                            }
                            else
                            {
                                try
                                {
                                    sp06514_OM_Linked_Documents_Linked_To_RecordTableAdapter.Fill(dataSet_OM_Core.sp06514_OM_Linked_Documents_Linked_To_Record, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), 71);
                                }
                                catch (Exception ex) { }
                                this.RefreshGridViewStateLinkedDocument.LoadViewInfo();  // Reload any expanded groups and selected rows //
                            }
                            gridControl3.MainView.EndUpdate();

                            view = (GridView)gridControl3.MainView;
                            view.ExpandAllGroups();

                            // Highlight any recently added new rows //
                            if (i_str_AddedRecordIDsLinkedDocument != "")
                            {
                                strArray = i_str_AddedRecordIDsLinkedDocument.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                                intID = 0;
                                int intRowHandle = 0;
                                view = (GridView)gridControl3.MainView;
                                view.BeginSelection();
                                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                                foreach (string strElement in strArray)
                                {
                                    intID = Convert.ToInt32(strElement);
                                    intRowHandle = view.LocateByValue(0, view.Columns["LinkedDocumentID"], intID);
                                    if (intRowHandle != GridControl.InvalidRowHandle)
                                    {
                                        view.SelectRow(intRowHandle);
                                        view.MakeRowVisible(intRowHandle, false);
                                    }
                                }
                                i_str_AddedRecordIDsLinkedDocument = "";
                                view.EndSelection();
                            }
                        }
                    }
                }
            }


            if (!boolSplashScreenVisibile && splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();

        }

        private void bbiRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            //Load_Data();
            if (boolProcessRunning)
            {
                //MessageBox.Show("Process already running.");
                return;
            }
            backgroundWorker1.RunWorkerAsync();
            boolProcessRunning = true;
            SetLoadingVisibility(true);
        }

        public void UpdateFormRefreshStatus(int status, Utils.enmFocusedGrid grid, string newIds)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            switch (grid)
            {
                case Utils.enmFocusedGrid.Tender:
                    i_str_AddedRecordIDsTender = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsTender : newIds);
                    break;
                case Utils.enmFocusedGrid.Visit:
                    i_str_AddedRecordIDsVisit = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsVisit : newIds);
                    break;
                case Utils.enmFocusedGrid.CRM:
                    i_str_AddedRecordIDsCRM = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsCRM : newIds);
                    break;
                case Utils.enmFocusedGrid.History:
                    i_str_AddedRecordIDsHistory = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsHistory : newIds);
                    break;
                case Utils.enmFocusedGrid.LinkedDocument:
                    i_str_AddedRecordIDsLinkedDocument = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsLinkedDocument : newIds);
                    break;
            }
        }


        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        iBool_AllowAdd = sfpPermissions.blCreate;
                        iBool_AllowEdit = sfpPermissions.blUpdate;
                        iBool_AllowDelete = sfpPermissions.blDelete;
                        break;
                    case 1:  // Add Wizard Button //    
                        iBool_AddWizardButtonEnabled = sfpPermissions.blRead;
                        break;
                    case 2:  // Send Button //    
                        iBool_SendButtonEnabled = sfpPermissions.blRead;
                        break;
                    case 3:  // Send Quote To CM Button //    
                        iBool_SendQuoteToCMEnabled = sfpPermissions.blRead;
                        break;
                    case 4:  // Manually change Status functionality //    
                        iBool_ManualStatusChange = sfpPermissions.blRead;
                        break;
                    case 5:  // Visit Drill Down //    
                        iBool_VisitDrillDownEnabled = sfpPermissions.blRead;
                        break;
                }
            }
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, 7008, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);  // 7008 //
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        iBool_TemplateManager = sfpPermissions.blRead;
                        break;
                }
            }
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, 7018, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);  // 7018 //
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        iBool_BillingRequirementTemplateManager = sfpPermissions.blRead;
                        break;
                }
            }
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, 7009, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);  // Visit Manger - need to see if user has full Visit Edit access to all fields //
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 13:  // Whole Form //    
                        iBool_VisitFullEditAccess = sfpPermissions.blUpdate;
                        break;
                    case 14:  // Whole Form //    
                        iBool_ManuallyCompleteExtraWorksVisitButtonEnabled = sfpPermissions.blRead;
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bbiDatasetSelectionInverted.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            bsiAuditTrail.Enabled = false;
            bbiViewAuditTrail.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = (GridView)gridControlVisit.MainView;
            GridView TenderView = (GridView)gridControlTender.MainView;
            GridView VisitView = (GridView)gridControlVisit.MainView;

            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Tender:
                    view = (GridView)gridControlTender.MainView;
                    break;
                case Utils.enmFocusedGrid.Visit:
                    view = (GridView)gridControlVisit.MainView;
                    break;
                case Utils.enmFocusedGrid.Spraying:
                    view = (GridView)gridControlHistory.MainView;
                    break;

                case Utils.enmFocusedGrid.CRM:
                    view = (GridView)gridControlCRM.MainView;
                    break;
                case Utils.enmFocusedGrid.LinkedDocument:
                    view = (GridView)gridControl3.MainView;
                    break;
                default:
                    break;
            }
            int[] intRowHandles = view.GetSelectedRows();
            int[] intTenderRowHandles = TenderView.GetSelectedRows();
            int[] intVisitRowHandles = VisitView.GetSelectedRows();

            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Visit:
                    {
                        //if (iBool_AllowAdd)
                        //{
                        //    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        //    bsiAdd.Enabled = true;
                        //    bbiSingleAdd.Enabled = true;
                        //}
                        if (iBool_AllowEdit && intRowHandles.Length >= 1)
                        {
                            alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                            bsiEdit.Enabled = true;
                            bbiSingleEdit.Enabled = true;
                            if (intRowHandles.Length >= 2)
                            {
                                alItems.Add("iBlockEdit");
                                bbiBlockEdit.Enabled = true;
                            }
                        }
                        if (iBool_AllowDelete && intRowHandles.Length >= 1)
                        {
                            alItems.Add("iDelete");
                            bbiDelete.Enabled = true;
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.Tender:
                case Utils.enmFocusedGrid.History:
                    {
                        if (iBool_AllowAdd)
                        {
                            alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                            bsiAdd.Enabled = true;
                            bbiSingleAdd.Enabled = true;
                        }
                        if (iBool_AllowEdit && intRowHandles.Length >= 1)
                        {
                            alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                            bsiEdit.Enabled = true;
                            bbiSingleEdit.Enabled = true;
                            if (intRowHandles.Length >= 2)
                            {
                                alItems.Add("iBlockEdit");
                                bbiBlockEdit.Enabled = true;
                            }
                        }
                        if (iBool_AllowDelete && intRowHandles.Length >= 1)
                        {
                            alItems.Add("iDelete");
                            bbiDelete.Enabled = true;
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.CRM:
                    {
                        if (iBool_AllowAdd)
                        {
                            alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                            bsiAdd.Enabled = true;
                            bbiSingleAdd.Enabled = true;

                            if (intTenderRowHandles.Length > 1)
                            {
                                alItems.Add("iBlockAdd");
                                bbiBlockAdd.Enabled = true;
                            }
                            else
                            {
                                bbiBlockAdd.Enabled = false;
                            }
                        }
                        if (iBool_AllowEdit && intRowHandles.Length >= 1)
                        {
                            alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                            bsiEdit.Enabled = true;
                            bbiSingleEdit.Enabled = true;
                            if (intRowHandles.Length >= 2)
                            {
                                alItems.Add("iBlockEdit");
                                bbiBlockEdit.Enabled = true;
                            }
                        }
                        if (iBool_AllowDelete && intRowHandles.Length >= 1)
                        {
                            alItems.Add("iDelete");
                            bbiDelete.Enabled = true;
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.LinkedDocument:
                    {
                        if (iBool_AllowAdd)
                        {
                            alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                            bsiAdd.Enabled = true;
                            bbiSingleAdd.Enabled = true;

                            GridView viewParent = (GridView)gridControlTender.MainView;
                            int[] intRowHandlesParent = viewParent.GetSelectedRows();
                            if (intRowHandlesParent.Length >= 2)
                            {
                                alItems.Add("iBlockAdd");
                                bbiBlockAdd.Enabled = true;
                            }
                        }
                        //bbiBlockAdd.Enabled = false;
                        if (iBool_AllowEdit && intRowHandles.Length >= 1)
                        {
                            alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                            bsiEdit.Enabled = true;
                            bbiSingleEdit.Enabled = true;
                            if (intRowHandles.Length >= 2)
                            {
                                alItems.Add("iBlockEdit");
                                bbiBlockEdit.Enabled = true;
                            }
                        }
                        if (iBool_AllowDelete && intRowHandles.Length >= 1)
                        {
                            alItems.Add("iDelete");
                            bbiDelete.Enabled = true;
                        }
                    }
                    break;
                default:
                    break;
            }
            bsiAuditTrail.Enabled = (intRowHandles.Length > 0);
            bbiViewAuditTrail.Enabled = (intRowHandles.Length > 0);

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of navigator custom buttons //
            view = (GridView)gridControlTender.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlTender.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlTender.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControlTender.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControlTender.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);
            gridControlTender.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length > 0);
            gridControlTender.EmbeddedNavigator.Buttons.CustomButtons[5].Enabled = (intRowHandles.Length == 1);
            gridControlTender.EmbeddedNavigator.Buttons.CustomButtons[6].Enabled = (intRowHandles.Length == 1);

            gridControlVisit.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false; // iBool_AllowAdd;
            gridControlVisit.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intVisitRowHandles.Length > 0);
            gridControlVisit.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intVisitRowHandles.Length > 0);
            gridControlVisit.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intVisitRowHandles.Length > 0);
            gridControlVisit.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intVisitRowHandles.Length > 0);
            gridControlVisit.EmbeddedNavigator.Buttons.CustomButtons[5].Enabled = (intVisitRowHandles.Length == 1);
            gridControlVisit.EmbeddedNavigator.Buttons.CustomButtons[6].Enabled = (iBool_AllowEdit && intVisitRowHandles.Length > 0);
            gridControlVisit.EmbeddedNavigator.Buttons.CustomButtons[7].Enabled = (iBool_ManuallyCompleteExtraWorksVisitButtonEnabled && intVisitRowHandles.Length > 0);

            view = (GridView)gridControlHistory.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlHistory.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;

            view = (GridView)gridControlCRM.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlCRM.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlCRM.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControlCRM.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControlCRM.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);
            gridControlCRM.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length == 1);

            view = (GridView)gridControl3.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl3.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControl3.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControl3.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControl3.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length == 1);

            // Set Enabled Status of Main Toolbar Buttons //
            bbiToolbarAdd.Enabled = iBool_AllowAdd;
            bbiToolbarAddFromTemplate.Enabled = (iBool_AllowAdd && intTenderRowHandles.Length > 0);

            bbiAddVisitWizard.Enabled = iBool_AddWizardButtonEnabled;
            bbiAddJobWizard.Enabled = iBool_AddWizardButtonEnabled;
            bsiSendVisits.Enabled = iBool_SendButtonEnabled;
            bbiSelectJobsReadyToSend.Enabled = iBool_SendButtonEnabled;
            bbiSendJobs.Enabled = (iBool_SendButtonEnabled && intVisitRowHandles.Length > 0);
            bsiStatus.Enabled = (intTenderRowHandles.Length > 0);

            int intStatusID = 0;
            if (intTenderRowHandles.Length == 1)
            {
                intStatusID = Convert.ToInt32(TenderView.GetRowCellValue(intTenderRowHandles[0], "StatusID"));
                bsiSendQuoteToCM.Enabled = (iBool_SendQuoteToCMEnabled && intStatusID == 10);
                bbiSendQuoteToCM.Enabled = (iBool_SendQuoteToCMEnabled && intStatusID == 10);
                bbiQuoteNotRequired.Enabled = (iBool_SendQuoteToCMEnabled && intStatusID == 10);
                bbiCMReturnedQuote.Enabled = (intStatusID == 20);
                bsiKAMAuthorised.Enabled = (intStatusID == 30);
                bbiKAMAccepted.Enabled = (intStatusID == 30);
                bbiKAMRejected.Enabled = (intStatusID == 30);
                bbiSentToClient.Enabled = (intStatusID == 40);
                bsiClientResponse.Enabled = (intStatusID == 50 || intStatusID == 60);
                bbiClientAccepted.Enabled = (intStatusID == 50 || intStatusID == 60);
                bbiClientRejected.Enabled = (intStatusID == 50 || intStatusID == 60);
                bbiClientOnHold.Enabled = (intStatusID == 50 || intStatusID == 60);
                bsiVisitJobWizard.Enabled = (iBool_AddWizardButtonEnabled && intStatusID == 80);
                bbiAddVisitWizard.Enabled = (iBool_AddWizardButtonEnabled && intStatusID == 80);
                bbiAddJobWizard.Enabled = (iBool_AddWizardButtonEnabled && intVisitRowHandles.Length > 0 && intStatusID == 80);
                bbiTenderClosed.Enabled = (intStatusID == 80);
                bbiTenderCancelled.Enabled = !(intStatusID == 70 || intStatusID == 90 || intStatusID == 90 || intStatusID == 100);
            }
            else
            {
                bsiSendQuoteToCM.Enabled = false;
                bbiSendQuoteToCM.Enabled = false;
                bbiQuoteNotRequired.Enabled = false;
                bbiCMReturnedQuote.Enabled = false;
                bsiKAMAuthorised.Enabled = false;
                bbiKAMAccepted.Enabled = false;
                bbiKAMRejected.Enabled = false;
                bbiSentToClient.Enabled = false;
                bsiClientResponse.Enabled = false;
                bbiClientAccepted.Enabled = false;
                bbiClientRejected.Enabled = false;
                bbiClientOnHold.Enabled = false;
                bsiVisitJobWizard.Enabled = false;
                bbiAddVisitWizard.Enabled = false;
                bbiAddJobWizard.Enabled = false;
                bbiTenderClosed.Enabled = false;
                bbiTenderCancelled.Enabled = false;
            }

            bbiVisitDrillDown.Enabled = (iBool_VisitDrillDownEnabled && intVisitRowHandles.Length > 0);
            bbiViewSiteOnMap.Enabled = (intTenderRowHandles.Length > 0);
        }

        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockAddEvent(object sender, EventArgs e)
        {
            Block_Add();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }


        private void Add_Record()
        {
            GridView view = null;
            GridView ParentView = null;
            int[] intRowHandles;
            System.Reflection.MethodInfo method = null;
            switch (_enmFocusedGrid)
            {
                #region Tender
                case Utils.enmFocusedGrid.Tender:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlTender.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_OM_Tender_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)view.GetRow(intRowHandles[0]);
                            var row = (DataSet_OM_Tender.sp06491_OM_Tender_ManagerRow)rowView.Row;
                            //fChildForm.intLinkedToRecordID1 = row.ClientID;
                            //fChildForm.intLinkedToRecordID2 = row.ClientContractID;
                            //fChildForm.intLinkedToRecordID3 = row.SiteID;
                            //fChildForm.intLinkedToRecordID4 = row.SiteContractID;
                            //fChildForm.intLinkedToRecordID5 = row.TenderID;
                            //fChildForm.strLinkedToRecordDesc1 = row.ClientName;
                            //fChildForm.strLinkedToRecordDesc2 = row.ContractDescription;
                            //fChildForm.strLinkedToRecordDesc3 = row.SiteName;
                            //fChildForm.strLinkedToRecordDesc4 = row.SitePostcode;
                        }
                        fChildForm.iBool_ManualStatusChange = iBool_ManualStatusChange;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                #endregion

                #region Visit
                case Utils.enmFocusedGrid.Visit:
                    {
                        //if (!iBool_AllowAdd) return;

                        //view = (GridView)gridControlVisit.MainView;
                        //view.PostEditor();

                        //var fChildForm = new frm_OM_Visit_Edit();
                        //fChildForm.MdiParent = this.MdiParent;
                        //fChildForm.GlobalSettings = this.GlobalSettings;
                        //fChildForm.strRecordIDs = "";
                        //fChildForm.strFormMode = "add";
                        //fChildForm.strCaller = this.Name;
                        //fChildForm.intRecordCount = 0;
                        //fChildForm.FormPermissions = this.FormPermissions;
                        //DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        //fChildForm.splashScreenManager = splashScreenManager1;
                        //fChildForm.splashScreenManager.ShowWaitForm();
                        //fChildForm.iBool_VisitFullEditAccess = true;  // Hardcoded to true when adding as user needs access to all fields if they are adding //

                        //intRowHandles = view.GetSelectedRows();
                        //if (intRowHandles.Length == 1)
                        //{
                        //    var rowView = (DataRowView)view.GetRow(intRowHandles[0]);
                        //    var row = (DataSet_OM_Tender.sp06495_OM_Visits_Linked_to_TenderRow)rowView.Row;
                        //    fChildForm.intLinkedToRecordID = row.SiteContractID;
                        //    fChildForm.intPassedInClientContractID = row.ClientContractID;
                        //    fChildForm.decPassedInSiteContractDaysSeparationPercent = row.SiteContractDaysSeparationPercent;
                        //    string strLinkedToParent = HtmlRemoval.StripTagsCharArray(row.LinkedToParent);  // Strip any HTML Bold tags out of text //
                        //    fChildForm.strLinkedToRecordDesc = strLinkedToParent;
                        //    fChildForm.intClientID = row.ClientID;
                        //    fChildForm.intSiteID = row.SiteID;
                        //    using (var GetValue = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter())
                        //    {
                        //        GetValue.ChangeConnectionString(strConnectionString);
                        //        try
                        //        {
                        //            fChildForm.intMaxVisitID = Convert.ToInt32(GetValue.sp06148_OM_Get_Max_VisitNumber_From_SiteContractID(row.SiteContractID));
                        //        }
                        //        catch (Exception) { }
                        //    }
                        //}
                        //fChildForm.Show();

                        //method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        //if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                #endregion

                #region CRM
                case Utils.enmFocusedGrid.CRM:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlCRM.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_Core_CRM_Contact_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlTender.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_OM_Tender.sp06491_OM_Tender_ManagerRow)rowView.Row;
                            fChildForm.intClientID = row.ClientID;
                            fChildForm.strClientName = row.ClientName;
                            fChildForm.intLinkedToRecordID = row.TenderID;
                            fChildForm.strLinkedToRecordDesc = String.Format("Client: {0} Tender: {1}", row.ClientName, row.TenderID.ToString());
                            fChildForm.intRecordTypeID = 301;
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                #endregion

                #region Linked Documents
                case Utils.enmFocusedGrid.LinkedDocument:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControl3.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_OM_Linked_Document_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlTender.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_OM_Tender.sp06491_OM_Tender_ManagerRow)rowView.Row;
                            fChildForm.intLinkedToRecordID = row.TenderID;
                            fChildForm.strLinkedToRecordDesc = "Client: " + ParentView.GetRowCellValue(ParentView.FocusedRowHandle, "ClientName").ToString() + ", Tender: " + ParentView.GetRowCellValue(ParentView.FocusedRowHandle, "TenderID").ToString();
                        }
                        fChildForm.intRecordTypeID = 71;
                        fChildForm.strRecordTypeDescription = "Tender";
                        fChildForm.intRecordSubTypeID = 0;
                        fChildForm.strRecordSubTypeDescription = "";
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                #endregion

                default:
                    break;
            }
        }

        private void Add_Record_From_Template()
        {
            if (!iBool_AllowAdd) return;
            GridView view = (GridView)gridControlTender.MainView;
            view.PostEditor();
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount != 1)
            {
                XtraMessageBox.Show("Select just one tender record to serve as a Template before proceeding.", "Add From Template", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            var rowView = (DataRowView)view.GetRow(intRowHandles[0]);
            var row = (DataSet_OM_Tender.sp06491_OM_Tender_ManagerRow)rowView.Row;

            bool boolCopyClientInfo = false;
            bool boolCopySiteInfo = false;
            bool boolCopyJobInfo = false;
            bool boolCopyLabourInfo = false;

            var fChildForm2 = new frm_OM_Tender_Add_From_Template_Get_Data_To_copy();
            fChildForm2.GlobalSettings = this.GlobalSettings;
            fChildForm2._PassedInTenderID = row.TenderID;
            if (fChildForm2.ShowDialog() == DialogResult.OK)  // User clicked OK on child form //
            {
                boolCopyClientInfo = fChildForm2._ClientData;
                boolCopySiteInfo = fChildForm2._SiteData;
                boolCopyJobInfo = fChildForm2._JobData;
                boolCopyLabourInfo = fChildForm2._ProposedLabourData;
            }
            else
            {
                return;  // User aborted //
            }

            var fChildForm = new frm_OM_Tender_Edit();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strRecordIDs = "";
            fChildForm.strFormMode = "add";
            fChildForm.strCaller = this.Name;
            fChildForm.intRecordCount = 0;
            fChildForm.FormPermissions = this.FormPermissions;
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();

            if (boolCopyClientInfo)
            {
                fChildForm.intPassedInClientID = row.ClientID;
                fChildForm.strPassedInClientName = row.ClientName;
                fChildForm.intPassedInClientContractID = row.ClientContractID;
                fChildForm.strPassedInContractDescription = row.ContractDescription;
                fChildForm.intPassedInClientContactID = row.ClientContactID;
                fChildForm.strPassedInContactPersonName = row.ContactPersonName;
                fChildForm.strPassedInContactPersonPosition = row.ContactPersonPosition;
                fChildForm.strPassedInContactPersonTitle = row.ContactPersonTitle;
                fChildForm.intPassedInCMID = row.CM;
                fChildForm.strPassedInCMName = row.CMName;
            }
            if (boolCopySiteInfo)
            {
                fChildForm.intPassedSiteID = row.SiteID;
                fChildForm.strPassedInSiteName = row.SiteName;
                fChildForm.intPassedInSiteContractID = row.SiteContractID;
                fChildForm.strPassedInSiteCode = row.SiteCode;
                fChildForm.strPassedInSiteAddress = row.SiteAddress;
                fChildForm.strPassedInSitePostcode = row.SitePostcode;
                fChildForm.dblPassedInSiteLocationX = row.SiteLocationX;
                fChildForm.dblPassedInSiteLocationY = row.SiteLocationY;
                fChildForm.intPassedInKAMID = row.KAM;
                fChildForm.strPassedInKAMName = row.KAMName;
                fChildForm.strPassedInTenderDescription = row.TenderDescription;
                fChildForm.intPassedInReactive = row.Reactive;
                fChildForm.intPassedInTenderGroupID = row.TenderGroupID;
                fChildForm.intPassedInTenderReactiveDaysToReturnQuote = row.TenderReactiveDaysToReturnQuote;
                fChildForm.intPassedInTenderProactiveDaysToReturnQuote = row.TenderProactiveDaysToReturnQuote;
            }
            if (boolCopyJobInfo)
            {
                fChildForm.intPassedInWorkSubTypeID = row.WorkSubTypeID;
                fChildForm.intPassedInJobTypeID = row.JobTypeID;
                fChildForm.strPassedInJobSubTypeDescription = row.JobSubType;
                fChildForm.strPassedInJobTypeDescription = row.JobType;
            }
            if (boolCopyLabourInfo)
            {
                fChildForm.intPassedInProposedLabourID = row.ProposedLabourID;
                fChildForm.strPassedInProposedLabour = row.ProposedLabour;
                fChildForm.intPassedInProposedLabourTypeID = row.ProposedLabourTypeID;
                fChildForm.strPassedInProposedLabourType = row.ProposedLabourType;
                fChildForm.strPassedInProposedLabourLatitude = row.ProposedLabourLatitude;
                fChildForm.strPassedInProposedLabourLongitude = row.ProposedLabourLongitude;
                fChildForm.strPassedInProposedLabourPostcode = row.ProposedLabourPostcode;
            }
            fChildForm.iBool_ManualStatusChange = iBool_ManualStatusChange;
            fChildForm.Show();

            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
        }

        private void Block_Add()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (_enmFocusedGrid)
            {
                #region Visit
                case Utils.enmFocusedGrid.Visit:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlVisit.MainView;
                        view.PostEditor();

                        // Get Site Contracts to add visits to //
                        var fChildForm = new frm_OM_Select_Site_Contract_Multiple();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        if (fChildForm.ShowDialog() != DialogResult.OK) return;

                        string strSelectedSiteContractIDs = fChildForm.strSelectedChildIDs;
                        if (string.IsNullOrWhiteSpace(strSelectedSiteContractIDs)) return;

                        // Open Edit Visit screen in BlockAdd mode //
                        var fChildForm1 = new frm_OM_Visit_Edit();
                        fChildForm1.MdiParent = this.MdiParent;
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        fChildForm1.strFormMode = "blockadd";
                        fChildForm1.strCaller = this.Name;
                        fChildForm1.intRecordCount = intCount;
                        fChildForm1.strRecordIDs = strSelectedSiteContractIDs;
                        fChildForm1.intLinkedToRecordID = 0;
                        fChildForm1.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm1.splashScreenManager = splashScreenManager1;
                        fChildForm1.splashScreenManager.ShowWaitForm();
                        fChildForm1.iBool_VisitFullEditAccess = true;  // Hardcoded to true when adding as user needs access to all fields if they are adding //
                        fChildForm1.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm1, new object[] { null });
                    }
                    break;
                #endregion

                #region CRM
                case Utils.enmFocusedGrid.CRM:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlTender.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one Tender record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "TenderID")) + ',';
                        }

                        var fChildForm1 = new frm_Core_CRM_Contact_Edit();
                        fChildForm1.MdiParent = this.MdiParent;
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        fChildForm1.strRecordIDs = "";
                        fChildForm1.strFormMode = "blockadd";
                        fChildForm1.strCaller = this.Name;
                        fChildForm1.intRecordCount = intCount;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.intRecordTypeID = 301;

                        fChildForm1.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm1.splashScreenManager = splashScreenManager1;
                        fChildForm1.splashScreenManager.ShowWaitForm();
                        fChildForm1.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm1, new object[] { null });
                    }
                    break;
                #endregion

                #region Linked Documents
                case Utils.enmFocusedGrid.LinkedDocument:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlTender.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one Tender record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        string strCurrentID = "";
                        StringBuilder sb = new StringBuilder(); 
                        sb.Append(",");  // Start with a starting comma so contains check functions correctly //
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strCurrentID = view.GetRowCellValue(intRowHandle, "TenderID").ToString() + ",";
                            if (!sb.ToString().Contains("," + strCurrentID)) sb.Append(strCurrentID);
                        }
                        if (sb.Length > 0) sb.Remove(0, 1);  // Remove preceeding comma //

                        var fChildForm = new frm_OM_Linked_Document_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = sb.ToString();
                        fChildForm.strFormMode = "blockadd";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        fChildForm.intRecordTypeID = 71;
                        fChildForm.strRecordTypeDescription = "Tender";
                        fChildForm.intRecordSubTypeID = 0;
                        fChildForm.strRecordSubTypeDescription = "";
                        fChildForm.iBool_Job = false;
                        fChildForm.iBool_JobLabour = false;
                        fChildForm.iBool_JobEquipment = false;
                        fChildForm.iBool_JobMaterial = false;
                        fChildForm.iBool_JobPicture = false;
                        fChildForm.iBool_JobHSE = false;
                        fChildForm.iBool_JobCRM = false;
                        fChildForm.iBool_JobComment = false;
                        fChildForm.iBool_VisitWaste = false;
                        fChildForm.iBool_VisitSpraying = false;
                        fChildForm.iBool_JobExtraInfo = false;
                        fChildForm.iBool_JobPO = false;
                        fChildForm.iBool_ClientContract = false;
                        fChildForm.iBool_ClientContractYear = false;
                        fChildForm.iBool_SiteContract = false;
                        fChildForm.iBool_Visit = false;
                        fChildForm.iBool_Accident = false;
                        fChildForm.iBool_AccidentInjury = false;
                        fChildForm.iBool_Tender = true;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                #endregion

                default:
                    break;
            }
        }

        private void Block_Edit()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (_enmFocusedGrid)
            {
                #region Tender
                case Utils.enmFocusedGrid.Tender:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlTender.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        StringBuilder sb = new StringBuilder();
                        view.BeginSelection();
                        foreach (int intRowHandle in intRowHandles)
                        {
                            if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "LinkedVisitCount")) > 0 && !BaseObjects.Keyboard.IsKeyDown(Keys.ControlKey))  //  Visits created so prevent editing of Tender if the CTRL key is not held down by user //
                            {
                                view.UnselectRow(intRowHandle);
                            }
                            else
                            {
                                sb.Append(Convert.ToString(view.GetRowCellValue(intRowHandle, "TenderID")) + ',');
                            }
                        }
                        view.EndSelection();
                        if (string.IsNullOrWhiteSpace(sb.ToString()))
                        {
                            XtraMessageBox.Show("No selected Tenders are appropriate for Block Editing.\n\nNote: Once visits have been linked to a Tender the tender can no longer be edited. This process de-selects Tenders which have one or more Visits linked to them.", "Select Tenders for Block Editing", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        
                        strEditedTenderIDs = sb.ToString();

                        var fChildForm = new frm_OM_Tender_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strEditedTenderIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.iBool_ManualStatusChange = iBool_ManualStatusChange;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                #endregion

                #region Visit
                case Utils.enmFocusedGrid.Visit:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlVisit.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        StringBuilder sb = new StringBuilder();
                        bool boolAtLeastOneSelfBillingInvoice = false;
                        bool boolAtLeastOneClientInvoice = false;
                        foreach (int intRowHandle in intRowHandles)
                        {
                            sb.Append(Convert.ToString(view.GetRowCellValue(intRowHandle, "VisitID")) + ',');
                            if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "SelfBillingInvoiceID")) > 0) boolAtLeastOneSelfBillingInvoice = true;
                            if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "ClientInvoiceID")) > 0) boolAtLeastOneClientInvoice = true;
                        }

                        var fChildForm = new frm_OM_Visit_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = sb.ToString();
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm._AtLeastOneSelfBillingInvoice = boolAtLeastOneSelfBillingInvoice;
                        fChildForm._AtLeastOneClientInvoice = boolAtLeastOneClientInvoice;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.iBool_VisitFullEditAccess = iBool_VisitFullEditAccess;  // Determins if user has full edit access to all fields //
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
               #endregion

                #region CRM
               case Utils.enmFocusedGrid.CRM:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlCRM.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "CRMID")) + ',';
                        }

                        var fChildForm = new frm_Core_CRM_Contact_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.intRecordTypeID = 301;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
               #endregion

               #region Linked Documents
               case Utils.enmFocusedGrid.LinkedDocument:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl3.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        StringBuilder sb = new StringBuilder();
                        foreach (int intRowHandle in intRowHandles)
                        {
                            sb.Append(Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedDocumentID")) + ',');
                        }
                        var fChildForm = new frm_OM_Linked_Document_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = sb.ToString();
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.iBool_Job = false;
                        fChildForm.iBool_JobLabour = false;
                        fChildForm.iBool_JobEquipment = false;
                        fChildForm.iBool_JobMaterial = false;
                        fChildForm.iBool_JobPicture = false;
                        fChildForm.iBool_JobHSE = false;
                        fChildForm.iBool_JobCRM = false;
                        fChildForm.iBool_JobComment = false;
                        fChildForm.iBool_VisitWaste = false;
                        fChildForm.iBool_VisitSpraying = false;
                        fChildForm.iBool_JobExtraInfo = false;
                        fChildForm.iBool_JobPO = false;
                        fChildForm.iBool_ClientContract = false;
                        fChildForm.iBool_ClientContractYear = false;
                        fChildForm.iBool_SiteContract = false;
                        fChildForm.iBool_Visit = false;
                        fChildForm.iBool_Accident = false;
                        fChildForm.iBool_AccidentInjury = false;
                        fChildForm.iBool_Tender = true;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
               #endregion

               default:
                    break;
            }
        }

        private void Edit_Record()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (_enmFocusedGrid)
            {
                #region Tender
                case Utils.enmFocusedGrid.Tender:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlTender.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        StringBuilder sb = new StringBuilder();
                        view.BeginSelection();
                        foreach (int intRowHandle in intRowHandles)
                        {
                            //if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "LinkedVisitCount")) > 0 && !BaseObjects.Keyboard.IsKeyDown(Keys.ControlKey))  //  Visits created so prevent editing of Tender if the CTRL key is not held down by user //
                            //{
                            //    view.UnselectRow(intRowHandle);
                            //}
                            //else
                            //{
                                sb.Append(Convert.ToString(view.GetRowCellValue(intRowHandle, "TenderID")) + ',');
                            //}
                        }
                        view.EndSelection();
                        if (string.IsNullOrWhiteSpace(sb.ToString()))
                        {
                            XtraMessageBox.Show("No selected Tenders are appropriate for Editing.\n\nNote: Once visits have been linked to a Tender the Tender can no longer be edited. This process de-selects Tenders which have one or more Visits linked to them.", "Select Tenders for Editing", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;

                        strEditedTenderIDs = sb.ToString();  // Put IDs into the instance var so we can merge changes if any are made without reloading the whole screen //

                        var fChildForm = new frm_OM_Tender_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strEditedTenderIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.iBool_ManualStatusChange = iBool_ManualStatusChange;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                #endregion

                #region Visit
                case Utils.enmFocusedGrid.Visit:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlVisit.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        StringBuilder sb = new StringBuilder();
                        foreach (int intRowHandle in intRowHandles)
                        {
                            sb.Append(Convert.ToString(view.GetRowCellValue(intRowHandle, "VisitID")) + ',');
                        }

                        var fChildForm = new frm_OM_Visit_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = sb.ToString();
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.iBool_VisitFullEditAccess = iBool_VisitFullEditAccess;  // Determins if user has full edit access to all fields //
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                #endregion

                #region CRM
                case Utils.enmFocusedGrid.CRM:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlCRM.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "CRMID")) + ',';
                        }

                        var fChildForm = new frm_Core_CRM_Contact_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.intRecordTypeID = 301;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                #endregion

                #region Linked Documents
                case Utils.enmFocusedGrid.LinkedDocument:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl3.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        StringBuilder sb = new StringBuilder();
                        foreach (int intRowHandle in intRowHandles)
                        {
                            sb.Append(Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedDocumentID")) + ',');
                        }
                        var fChildForm = new frm_OM_Linked_Document_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = sb.ToString();
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.iBool_Job = false;
                        fChildForm.iBool_JobLabour = false;
                        fChildForm.iBool_JobEquipment = false;
                        fChildForm.iBool_JobMaterial = false;
                        fChildForm.iBool_JobPicture = false;
                        fChildForm.iBool_JobHSE = false;
                        fChildForm.iBool_JobCRM = false;
                        fChildForm.iBool_JobComment = false;
                        fChildForm.iBool_VisitWaste = false;
                        fChildForm.iBool_VisitSpraying = false;
                        fChildForm.iBool_JobExtraInfo = false;
                        fChildForm.iBool_JobPO = false;
                        fChildForm.iBool_ClientContract = false;
                        fChildForm.iBool_ClientContractYear = false;
                        fChildForm.iBool_SiteContract = false;
                        fChildForm.iBool_Visit = false;
                        fChildForm.iBool_Accident = false;
                        fChildForm.iBool_AccidentInjury = false;
                        fChildForm.iBool_Tender = true;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;

                #endregion

                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            string strRecordIDs = "";
            switch (_enmFocusedGrid)
            {
                #region Tender
                case Utils.enmFocusedGrid.Tender:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlTender.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Tenders to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        //view.BeginSelection();
                        //List<int> disallowedStatuses = new List<int> { 40, 80 };  // 40: Sent,  80 In-Progress //
                        //foreach (int intRowHandle in intRowHandles)
                        //{
                        //    if (disallowedStatuses.Contains(Convert.ToInt32(view.GetRowCellValue(intRowHandle, "JobStatusID"))))
                        //    {
                        //        view.UnselectRow(intRowHandle);
                        //    }
                        //}
                        //view.EndSelection();
                        // Check there is at least one row still selected //
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Tenders to delete by clicking on them then try again.\n\nOnly Tender not Completed can be selected. If these tenders are selected this process will de-select them.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Tender" : Convert.ToString(intRowHandles.Length) + " Tenders") + " selected for delete!\n\nProceed?\n\n<color=red>WARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Tender" : "these Tenders") + " will no longer be available for selection and any related records will also be deleted!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            Array.Reverse(intRowHandles);  // Reverse the order so the last record is deleted first //
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "TenderID")) + ",";
                                view.DeleteRow(intRowHandle);
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("tender", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            view.DeleteSelectedRows();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                #endregion

                #region Visit
                case Utils.enmFocusedGrid.Visit:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlVisit.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Visits to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        
                        view.BeginSelection();
                        List<int> disallowedStatuses = new List<int> { 15, 20 };  // 15: Sent,  20 In-Progress //
                        foreach (int intRowHandle in intRowHandles)
                        {
                            if (disallowedStatuses.Contains(Convert.ToInt32(view.GetRowCellValue(intRowHandle, "VisitStatusID"))))
                            {
                                view.UnselectRow(intRowHandle);
                            }
                        }
                        view.EndSelection();
                        // Check there is at least one row still selected //
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Visits to delete by clicking on them then try again.\n\nOnly Visits not Sent or In-Progress can be selected for deletion. If these visits are selected this process will de-select them.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Visit" : Convert.ToString(intRowHandles.Length) + " Visits") + " selected for delete!\n\nProceed?\n\n<color=red>WARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Visit" : "these Visits") + " will no longer be available for selection and any related records will also be deleted!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            Array.Reverse(intRowHandles);  // Reverse the order so the last record is deleted first //
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "VisitID")) + ",";
                                view.DeleteRow(intRowHandle);
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("visit", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            view.DeleteSelectedRows();
                            Reload_Selected_Tenders_Only();  // Reload parent level to update count of linked visits//

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                #endregion

                #region CRM
                case Utils.enmFocusedGrid.CRM:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlCRM.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked CRM to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked CRM" : Convert.ToString(intRowHandles.Length) + " Linked CRM") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked CRM" : "these Linked CRM") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            Array.Reverse(intRowHandles);  // Reverse the order so the last record is deleted first //
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "CRMID")) + ",";
                                view.DeleteRow(intRowHandle);
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("job_crm", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            view.DeleteSelectedRows();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                #endregion

                #region Linked Document
                 case Utils.enmFocusedGrid.LinkedDocument:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl3.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Documents to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Document" : Convert.ToString(intRowHandles.Length) + " Linked Documents") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Document" : "these Linked Documents") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            Array.Reverse(intRowHandles);  // Reverse the order so the last record is deleted first //
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedDocumentID")) + ",";
                                view.DeleteRow(intRowHandle);
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("linked_document", strRecordIDs);
                                }
                                catch (Exception) { }
                            }
                            view.DeleteSelectedRows();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                #endregion

                default:
                    break;
            }
        }

        private void View_Record()
        {
            GridView view = null;
            MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (_enmFocusedGrid)
            {
                #region Tender
                case Utils.enmFocusedGrid.Tender:
                    {
                        view = (GridView)gridControlTender.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        StringBuilder sb = new StringBuilder();
                        foreach (int intRowHandle in intRowHandles)
                        {
                            sb.Append(Convert.ToString(view.GetRowCellValue(intRowHandle, "TenderID")) + ',');
                        }
                        var fChildForm = new frm_OM_Tender_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = sb.ToString();
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                #endregion

                #region Visit
                case Utils.enmFocusedGrid.Visit:
                    {
                        view = (GridView)gridControlVisit.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        StringBuilder sb = new StringBuilder();
                        foreach (int intRowHandle in intRowHandles)
                        {
                            sb.Append(Convert.ToString(view.GetRowCellValue(intRowHandle, "VisitID")) + ',');
                        }
                        var fChildForm = new frm_OM_Visit_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = sb.ToString();
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                #endregion

                #region CRM
                case Utils.enmFocusedGrid.CRM:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlCRM.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        StringBuilder sb = new StringBuilder();
                        foreach (int intRowHandle in intRowHandles)
                        {
                            sb.Append(Convert.ToString(view.GetRowCellValue(intRowHandle, "CRMID")) + ',');
                        }

                        var fChildForm = new frm_Core_CRM_Contact_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = sb.ToString();
                        fChildForm.intRecordTypeID = 301;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                #endregion

                #region Linked Documents
                case Utils.enmFocusedGrid.LinkedDocument:
                    {
                        view = (GridView)gridControl3.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        StringBuilder sb = new StringBuilder();
                        foreach (int intRowHandle in intRowHandles)
                        {
                            sb.Append(Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedDocumentID")) + ',');
                        }
                        var fChildForm = new frm_OM_Linked_Document_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = sb.ToString();
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.iBool_Job = false;
                        fChildForm.iBool_JobLabour = false;
                        fChildForm.iBool_JobEquipment = false;
                        fChildForm.iBool_JobMaterial = false;
                        fChildForm.iBool_JobPicture = false;
                        fChildForm.iBool_JobHSE = false;
                        fChildForm.iBool_JobCRM = false;
                        fChildForm.iBool_JobComment = false;
                        fChildForm.iBool_VisitWaste = false;
                        fChildForm.iBool_VisitSpraying = false;
                        fChildForm.iBool_JobExtraInfo = false;
                        fChildForm.iBool_JobPO = false;
                        fChildForm.iBool_ClientContract = false;
                        fChildForm.iBool_ClientContractYear = false;
                        fChildForm.iBool_SiteContract = false;
                        fChildForm.iBool_Visit = false;
                        fChildForm.iBool_Accident = false;
                        fChildForm.iBool_AccidentInjury = false;
                        fChildForm.iBool_Tender = true;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                #endregion

                default:
                    break;
            }
        }

        public override void OnViewAuditTrail(object sender, EventArgs e)
        {
            View_Audit_Trail();
        }
        private void View_Audit_Trail()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Tender:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlTender.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view the audit trail for before proceeding.", "View Audit Trail", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "TenderID")) + ',';
                        }

                        var fChildForm = new frm_Core_Audit_Trail_Viewer();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInDrillDownIDs = strRecordIDs;
                        fChildForm.strPassedInDrillDownTypeID = "dbo.OM_Tender";
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Visit:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlVisit.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view the audit trail for before proceeding.", "View Audit Trail", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "VisitID")) + ',';
                        }

                        var fChildForm = new frm_Core_Audit_Trail_Viewer();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInDrillDownIDs = strRecordIDs;
                        fChildForm.strPassedInDrillDownTypeID = "dbo.OM_Visit";
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.CRM:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlCRM.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view the audit trail for before proceeding.", "View Audit Trail", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "CRMID")) + ',';
                        }

                        var fChildForm = new frm_Core_Audit_Trail_Viewer();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInDrillDownIDs = strRecordIDs;
                        fChildForm.strPassedInDrillDownTypeID = "dbo.GC_CRM";
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
            }
        }

        private string Get_Selected_IDs(GridView view, string ColumnName)
        {
            int intRowCount = view.DataRowCount;
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0) return "";
            StringBuilder sb = new StringBuilder();
            foreach (int intRowHandle in intRowHandles)
            {
                sb.Append(view.GetRowCellValue(intRowHandle, ColumnName).ToString() + ",");
            }
            return sb.ToString();
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridViewTender":
                    message = "No Tenders Available - Adjust any filters and click Refresh button";
                    break;
                case "gridViewVisit":
                    message = "No Linked Visits Available - Select one or more Tenders to view Linked Visits";
                    break;
                case "gridViewHistory":
                    message = "No History - Select one or more Tenders to view Linked History";
                    break;
                case "gridViewCRM":
                    message = "No CRM Contact - Select one or more Tenders to view Linked CRM Contact";
                    break;
                case "gridView3":
                    message = "No Linked Documents Available - Select one or more Tenders to view Linked Documents";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_MouseDown_Standard(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_Standard(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridViewTender":
                    Set_DictionaryDataRefreshes_All_Value(dictionaryDataRefreshesMain, true);  // Tell all pages they will need to refresh their data when they are the active page //
                    LoadLinkedRecords();
                    view = (GridView)gridControlVisit.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControlHistory.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControlCRM.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControl3.MainView;
                    view.ExpandAllGroups();
                    Set_Selected_Count();
                    break;
                case "gridViewVisit":
                    Set_Selected_Count();
                    break;
                default:
                    break;
            }
            SetMenuStatus();
        }

        #endregion


        #region GridView - Tender

        private void gridControlTender_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("reload_selected".Equals(e.Button.Tag))
                    {
                        Reload_Selected_Tenders_Only();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = gridViewTender;
                        int intRecordType = 71;  // Tender //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "TenderID"));
                        string strRecordDescription = "Client: " + view.GetRowCellValue(view.FocusedRowHandle, "ClientName").ToString() + ", Tender: " + view.GetRowCellValue(view.FocusedRowHandle, "TenderID").ToString();
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    else if ("template_add".Equals(e.Button.Tag))
                    {
                        Add_Record_From_Template();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewTender_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            switch (e.Column.FieldName)
            {
                case "DoNotInvoiceClient":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "DoNotInvoiceClient")) == 1)
                        {
                            //e.Appearance.BackColor = Color.LightCoral;
                            //e.Appearance.BackColor2 = Color.Red;
                            e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                            //e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                            //e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        }
                    }
                    break;
            }
        }

        private void gridViewTender_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            try
            {
                GridView view = (GridView)sender;
                switch (e.Column.FieldName)
                {
                    case "LinkedVisitCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedVisitCount")) <= 1) e.RepositoryItem = emptyEditor;
                        break;
                    case "LinkedJobCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedJobCount")) <= 1) e.RepositoryItem = emptyEditor;
                        break;
                    case "LinkedDocumentCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "TenderGroupID":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "TenderGroupID")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "DaysQuoteOverdue":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "DaysQuoteOverdue")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "ClientQuoteSpreadsheetExtractFile":
                        if (string.IsNullOrWhiteSpace(view.GetRowCellValue(e.RowHandle, "ClientQuoteSpreadsheetExtractFile").ToString())) e.RepositoryItem = emptyEditor;
                        break;

                    case "ProgressBarValue":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "StatusIssueID")) <= 0)
                        {
                            e.RepositoryItem = repositoryItemProgressBarGreen;
                        }
                        else
                        {
                            e.RepositoryItem = repositoryItemProgressBarRed;
                        }
                        break;
                    default:
                        break;
                }
            }
            catch (Exception) { }
        }

        private void gridViewTender_CustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
            if (e.Column.FieldName == "Alert" && e.IsGetData)
            {
                e.Value = bmpBlank;
                GridView view = (GridView)sender;
                DateTime dtEnd = DateTime.Now;
                int rHandle = (sender as GridView).GetRowHandle(e.ListSourceRowIndex);
                if (Convert.ToInt32(view.GetRowCellValue(rHandle, "LinkedLabourCount")) <= 0 || Convert.ToInt32(view.GetRowCellValue(rHandle, "JobStatusID")) < 20 || Convert.ToInt32(view.GetRowCellValue(rHandle, "DoNotPayContractor")) == 1 || Convert.ToInt32(view.GetRowCellValue(rHandle, "DoNotInvoiceClient")) == 1)
                {
                    e.Value = bmpAlert;
                }
            }
        }

        private void gridViewTender_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewTender_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.Tender;
            SetMenuStatus();
        }

        private void gridViewTender_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Tender;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                //bbiDatasetCreate.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                //bbiDatasetSelection.Enabled = (view.RowCount > 0);
                //bsiDataset.Enabled = true;
                //bbiDatasetSelectionInverted.Enabled = true;
                //bbiDatasetManager.Enabled = true;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewTender_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedVisitCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedVisitCount")) <= 1) e.Cancel = true;
                    break;
                case "LinkedJobCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedJobCount")) <= 1) e.Cancel = true;
                    break;
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) <= 0) e.Cancel = true;
                    break;
                case "TenderGroupID":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("TenderGroupID")) <= 0) e.Cancel = true;
                    break;
                case "DaysQuoteOverdue":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("DaysQuoteOverdue")) <= 0) e.Cancel = true;
                    break;
                case "ClientQuoteSpreadsheetExtractFile":
                    if (string.IsNullOrWhiteSpace(view.GetFocusedRowCellValue("ClientQuoteSpreadsheetExtractFile").ToString())) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEditLinkedDocumentsJob_OpenLink(object sender, OpenLinkEventArgs e)
        {
            // Drilldown to Linked Document Manager //
            int intRecordType = 71;  // Tender //
            int intRecordSubType = 0;  // Not Used //
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "TenderID"));
            string strRecordDescription = "Client: " + view.GetRowCellValue(view.FocusedRowHandle, "ClientName").ToString() + ", Tender: " + view.GetRowCellValue(view.FocusedRowHandle, "TenderID").ToString();
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        private void repositoryItemHyperLinkEditLinkedVisits2_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intTenderID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "TenderID"));
            if (intTenderID <= 0) return;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedVisitCount":
                    {
                        string strRecordIDs = "";
                        try
                        {
                            var GetValue = new DataSet_OM_TenderTableAdapters.QueriesTableAdapter();
                            GetValue.ChangeConnectionString(strConnectionString);
                            strRecordIDs = GetValue.sp06492_OM_Get_Visits_From_TenderIDs(intTenderID.ToString() + ",").ToString();
                        }
                        catch (Exception ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view linked visits [" + ex.Message + "].\n\nPlease try again. If the problem persists contact Technical Support.", "View Linked Visits", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                        Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "operations_visit");
                    }
                    break;
                case "LinkedJobCount":
                    {
                        string strRecordIDs = "";
                        try
                        {
                            var GetValue = new DataSet_OM_TenderTableAdapters.QueriesTableAdapter();
                            GetValue.ChangeConnectionString(strConnectionString);
                            strRecordIDs = GetValue.sp06523_OM_Get_Jobs_From_TenderIDs(intTenderID.ToString() + ",").ToString();
                        }
                        catch (Exception ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view linked jobs [" + ex.Message + "].\n\nPlease try again. If the problem persists contact Technical Support.", "View Linked Jobs", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                        Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "operations_job");
                    }
                    break;
            }
        }

        private void repositoryItemHyperLinkEditTenderGroupDescription_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intTenderGroupID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "TenderGroupID"));
            if (intTenderGroupID <= 0) return;
            Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, intTenderGroupID.ToString() + ",", "operations_tender_group");
        }

        private void repositoryItemHyperLinkEditClientQuoteExtractFilt_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "ClientQuoteSpreadsheetExtractFile").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Client Quote Spreadsheet Extract File Linked - unable to proceed.", "View Client Quote Spreadsheet Extract File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                if (Path.GetExtension(Path.Combine(i_str_ClientQuoteSpreadsheetExtractFolder + strFile)).ToLower() != ".pdf")
                {
                    System.Diagnostics.Process.Start(Path.Combine(i_str_ClientQuoteSpreadsheetExtractFolder + strFile));
                }
                else
                {
                    if (!BaseObjects.Keyboard.IsKeyDown(Keys.A))  // Only open in DevExpress PDF viewer if user is not holding down A key (CTRL Key doesn't work when fired from a grid as it's used for row selection) //
                    {
                        frm_Core_PDF_Viewer fChildForm = new frm_Core_PDF_Viewer();
                        fChildForm.strPDFFile = Path.Combine(i_str_ClientQuoteSpreadsheetExtractFolder + strFile);
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.Show();
                    }
                    else
                    {
                        System.Diagnostics.Process.Start(Path.Combine(i_str_ClientQuoteSpreadsheetExtractFolder + strFile));
                    }
                }
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view the Client Quote Spreadsheet Extract File: " + strFile + ".\n\nThe File may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Client Quote Spreadsheet Extract File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void Reload_Selected_Tenders_Only()
        {
            strEditedTenderIDs = Get_Selected_IDs((GridView)gridControlTender.MainView, "TenderID");
            if (string.IsNullOrWhiteSpace(strEditedTenderIDs)) return;
            UpdateRefreshStatus = 1;
            Set_DictionaryDataRefreshes_All_Value(dictionaryDataRefreshesMain, true);
            Load_Data();
        }

        #endregion


        #region GridView - Visit

        private void gridControlVisit_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        Set_DictionaryDataRefreshes_One_Value(dictionaryDataRefreshesMain, xtraTabPageVisits.Text, true);  // Force Data to refresh //
                        LoadLinkedRecords();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = gridViewVisit;
                        if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
                        int intRecordType = 52;  // Visit //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "VisitID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "ClientName").ToString() +
                                                        " [" + view.GetRowCellValue(view.FocusedRowHandle, "ContractDescription").ToString() + "]" +
                                                        ", Site: " + view.GetRowCellValue(view.FocusedRowHandle, "SiteName").ToString() +
                                                        ", Visit: " + view.GetRowCellValue(view.FocusedRowHandle, "VisitNumber").ToString();
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    else if ("ready_to_send".Equals(e.Button.Tag))
                    {
                        Set_Ready_To_Send();
                    }
                    else if ("manual_complete".Equals(e.Button.Tag))
                    {
                        Manually_Complete_Visits();
                    }
                    break;                   
                default:
                    break;
            }
        }

        private void gridViewVisit_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            switch (e.Column.FieldName)
            {
                case "LinkedJobCount":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedJobCount")) <= 0)
                        {
                            //e.Appearance.BackColor = Color.LightCoral;
                            //e.Appearance.BackColor2 = Color.Red;
                            e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                            //e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                            //e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        }
                    }
                    break;
                case "IssueFound":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "IssueFound")) != 0)
                        {
                            //e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                            e.Appearance.BackColor = Color.FromArgb(255, 153, 51);  // Peachy //  
                      }
                    }
                    break;
                case "Rework":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "Rework")) >= 1)
                        {
                            //e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                            e.Appearance.BackColor = Color.FromArgb(255, 153, 51);  // Peachy //  
                        }
                    }
                    break;
                case "AccidentOnSite":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "AccidentOnSite")) >= 1)
                        {
                            e.Appearance.BackColor = Color.FromArgb(255, 92, 51);
                        }
                    }
                    break;
                case "SuspendedJobCount":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "SuspendedJobCount")) >= 1)
                        {
                            //e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                            e.Appearance.BackColor = Color.FromArgb(255, 153, 51);  // Peachy //  
                        }
                    }
                    break;
                case "DoNotInvoiceClient":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "DoNotInvoiceClient")) == 1)
                        {
                            //e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                            e.Appearance.BackColor = Color.FromArgb(255, 153, 51);  // Peachy //  
                        }
                    }
                    break;
                case "DoNotPayContractor":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "DoNotPayContractor")) == 1)
                        {
                            //e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                            e.Appearance.BackColor = Color.FromArgb(255, 153, 51);  // Peachy //  
                        }
                    }
                    break;
            }            
        }

        private void gridViewVisit_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            try
            {
                GridView view = (GridView)sender;
                switch (e.Column.FieldName)
                {
                    case "LinkedJobCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedJobCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "LinkedOutstandingJobCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedOutstandingJobCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "LinkedCompletedJobCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedCompletedJobCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "LinkedDocumentCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "LabourCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LabourCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "LinkedVisitCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedVisitCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "SuspendedJobCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "SuspendedJobCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "ClientSignaturePath":
                        if (string.IsNullOrWhiteSpace(view.GetRowCellValue(e.RowHandle, "ClientSignaturePath").ToString())) e.RepositoryItem = emptyEditor;
                        break;
                    case "LinkedPictureCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedPictureCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "SelfBillingInvoice":
                        if (view.GetRowCellValue(e.RowHandle, "SelfBillingInvoice").ToString() != "Yes") e.RepositoryItem = emptyEditor;
                        break;
                    case "ClientInvoiceID":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "ClientInvoiceID")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception) { }
        }

        private void gridViewVisit_CustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
            if (e.Column.FieldName == "Alert2" && e.IsGetData)
            {
                e.Value = bmpBlank;
                GridView view = (GridView)sender;
                DateTime dtEnd = DateTime.Now;
                int rHandle = (sender as GridView).GetRowHandle(e.ListSourceRowIndex);
                if (view.GetRowCellValue(rHandle, "Timeliness").ToString() == "Overdue" ||Convert.ToInt32(view.GetRowCellValue(rHandle, "LinkedJobCount")) <= 0 ||  Convert.ToInt32(view.GetRowCellValue(rHandle, "IssueFound")) != 0 || Convert.ToInt32(view.GetRowCellValue(rHandle, "Rework")) >= 1 || Convert.ToInt32(view.GetRowCellValue(rHandle, "AccidentOnSite")) >= 1)
                {
                    e.Value = bmpAlert;
                }
            }
        }

        private void gridViewVisit_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewVisit_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.Visit;
            SetMenuStatus();
        }

        private void gridViewVisit_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Visit;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                int[] intRowHandles = view.GetSelectedRows();
                int intCount = intRowHandles.Length;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewVisit_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedJobCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedJobCount")) <= 0) e.Cancel = true;
                    break;
                case "LinkedOutstandingJobCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedOutstandingJobCount")) <= 0) e.Cancel = true;
                    break;
                case "LinkedCompletedJobCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedCompletedJobCount")) <= 0) e.Cancel = true;
                    break;
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) <= 0) e.Cancel = true;
                    break;
                case "LabourCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LabourCount")) <= 0) e.Cancel = true;
                    break;
                case "LinkedVisitCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedVisitCount")) <= 0) e.Cancel = true;
                    break;
                case "SuspendedJobCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("SuspendedJobCount")) <= 0) e.Cancel = true;
                    break;
                case "ClientSignaturePath":
                    if (string.IsNullOrWhiteSpace(view.GetFocusedRowCellValue("ClientSignaturePath").ToString())) e.Cancel = true;
                    break;
                case "LinkedPictureCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedPictureCount")) <= 0) e.Cancel = true;
                    break;
                case "SelfBillingInvoice":
                    if (view.GetFocusedRowCellValue("SelfBillingInvoice").ToString() != "Yes") e.Cancel = true;
                    break;
                case "ClientInvoiceID":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("ClientInvoiceID")) <= 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit1_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intClientContractID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "ClientContractID"));
            if (intClientContractID <= 0) return;
            string strRecordIDs = "";

            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedContractCount":
                    {
                        try
                        {
                            string strParameter = intClientContractID.ToString() + ",";
                            var GetValue = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter();
                            GetValue.ChangeConnectionString(strConnectionString);
                            strRecordIDs = GetValue.sp06053_OM_Get_Linked_Client_Contract_IDs(intClientContractID.ToString() + ",").ToString();
                        }
                        catch (Exception ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view client contracts linked to this contract [" + ex.Message + "].\n\nPlease try again. If the problem persists contact Technical Support.", "View Linked Client Contracts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                        Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "operations_client_contracts");
                    }
                    break;
                case "LinkedParentContractCount":
                    {
                        int intLinkedToPreviousContractID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "LinkedToPreviousContractID"));
                        Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, intLinkedToPreviousContractID.ToString() + ",", "operations_client_contracts");
                    }
                    break;
                case "LinkedSiteContractCount":
                    {
                        try
                        {
                            string strParameter = intClientContractID.ToString() + ",";
                            var GetValue = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter();
                            GetValue.ChangeConnectionString(strConnectionString);
                            strRecordIDs = GetValue.sp06054_OM_Get_Linked_Site_Contract_IDs(intClientContractID.ToString() + ",").ToString();
                        }
                        catch (Exception ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view site contracts linked to this contract [" + ex.Message + "].\n\nPlease try again. If the problem persists contact Technical Support.", "View Linked Site Contracts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                        Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "operations_site_contracts");
                    }
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEditLinkedDocumentsVisit_OpenLink(object sender, OpenLinkEventArgs e)
        {
            // Drilldown to Linked Document Manager //
            int intRecordType = 52;  // Visit //
            int intRecordSubType = 0;  // Not Used //
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "VisitID"));
            string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "ClientName").ToString() +
                                            " [" + view.GetRowCellValue(view.FocusedRowHandle, "ContractDescription").ToString() + "]" +
                                            ", Site: " + view.GetRowCellValue(view.FocusedRowHandle, "SiteName").ToString() +
                                            ", Visit: " + view.GetRowCellValue(view.FocusedRowHandle, "VisitNumber").ToString();
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        private void repositoryItemHyperLinkEditGrid1_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            if (view.FocusedColumn.FieldName == "LinkedPictureCount")
            {
                int intVisitID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "VisitID"));
                string strJobIDs = "";
                try
                {
                    var GetValue = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter();
                    GetValue.ChangeConnectionString(strConnectionString);
                    strJobIDs = GetValue.sp06130_OM_Visit_Manager_Get_Linked_Job_IDs(intVisitID.ToString() + ",", 0).ToString();
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view linked pictures [" + ex.Message + "].\n\nPlease try again. If the problem persists contact Technical Support.", "View Linked Pictures", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                Open_Picture_Viewer(strJobIDs, Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "ClientID")));
                return;
            }
            else
            {
                int intVisitID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "VisitID"));
                if (intVisitID <= 0) return;
                string strRecordIDs = "";

                int intRecordType = -1;
                switch (view.FocusedColumn.FieldName)
                {
                    case "LinkedJobCount":
                        intRecordType = 0;
                        break;
                    case "LinkedOutstandingJobCount":
                        intRecordType = 1;
                        break;
                    case "LinkedCompletedJobCount":
                        intRecordType = 2;
                        break;
                    case "SuspendedJobCount":
                        intRecordType = 3;
                        break;
                    default:
                        break;
                }
                if (intRecordType == -1) return;

                try
                {
                    string strParameter = intVisitID.ToString() + ",";
                    var GetValue = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter();
                    GetValue.ChangeConnectionString(strConnectionString);
                    strRecordIDs = GetValue.sp06130_OM_Visit_Manager_Get_Linked_Job_IDs(intVisitID.ToString() + ",", intRecordType).ToString();
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view linked jobs [" + ex.Message + "].\n\nPlease try again. If the problem persists contact Technical Support.", "View Linked Jobs", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "operations_job");
            }
        }

        private void repositoryItemHyperLinkEditLinkedVisits_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intSiteContractID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "SiteContractID"));
            if (intSiteContractID <= 0) return;
            string strRecordIDs = "";
            try
            {
                var GetValue = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter();
                GetValue.ChangeConnectionString(strConnectionString);
                strRecordIDs = GetValue.sp06248_OM_Visit_Manager_Get_Linked_Visit_IDs(intSiteContractID.ToString() + ",").ToString();
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view linked visits [" + ex.Message + "].\n\nPlease try again. If the problem persists contact Technical Support.", "View Linked Visits", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "operations_visit");
        }

        private void repositoryItemHyperLinkEditLabour_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intVisitID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "VisitID"));
            if (intVisitID <= 0) return;
            string strRecordIDs = "";
            try
            {
                string strParameter = intVisitID.ToString() + ",";
                var GetValue = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter();
                GetValue.ChangeConnectionString(strConnectionString);
                strRecordIDs = GetValue.sp06130_OM_Visit_Manager_Get_Linked_Job_IDs(intVisitID.ToString() + ",", 0).ToString();
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view linked job labour [" + ex.Message + "].\n\nPlease try again. If the problem persists contact Technical Support.", "View Linked Job Labour", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "operations_job_labour");
        }
        
        private void repositoryItemHyperLinkEditClientSignature_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFirstPartOfPathWithBackslash = strSignaturePath + "\\";
            string strClientPath = view.GetRowCellValue(view.FocusedRowHandle, "ImagesFolderOM").ToString() + "\\Signatures";
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "ClientSignaturePath").ToString();
            if (string.IsNullOrWhiteSpace(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Client Signature Linked - unable to proceed.", "View Client Signature", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(Path.Combine(strFirstPartOfPathWithBackslash + strClientPath + strFile));
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view Client Signature: " + strFile + ".\n\nThe file may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Client Signature", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void repositoryItemHyperLinkEditSelfBillingInvoice_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strSelfBillingInvoice = view.GetRowCellValue(view.FocusedRowHandle, "SelfBillingInvoice").ToString();
            if (strSelfBillingInvoice != "Yes")
            {
                XtraMessageBox.Show("This visit has not been invoiced yet - Unable to View Self-Billing Invoice.", "View Self-Billing Invoice.", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            int intVisitID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "VisitID"));
            if (intVisitID <= 0) return;
            OM_View_Self_Billing_Invoice viewInvoice = new OM_View_Self_Billing_Invoice();
            viewInvoice.View_Self_Billing_Invoice(this, this.GlobalSettings, strConnectionStringREADONLY, intVisitID.ToString() + ",");
        }

        private void repositoryItemHyperLinkEditClientInvoiceID_OpenLink(object sender, OpenLinkEventArgs e)
        {

        }

        private void Set_Ready_To_Send()
        {
            GridView view = (GridView)gridControlVisit.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more Visits to set as Ready To Send by clicking on them then try again.", "Set Visits Ready To Send", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            StringBuilder sbVisitIDs = new StringBuilder();

            foreach (int intRowHandle in intRowHandles)
            {
                if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "VisitStatusID")) > 5)
                {
                    view.UnselectRow(intRowHandle);
                }
                else
                {
                    sbVisitIDs.Append(Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "VisitID")) + ",");
                }
            }

            // Check there is at least one row still selected //
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more Visits to set as Ready To Send by clicking on them then try again.\n\nOnly Visits with a Status of Not Ready To send can be selected for setting as Ready To send. This process will automatically de-select visits without the correct status.", "Set Visits Ready To Send", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            string strMessage = "You have " + (intCount == 1 ? "1 Visit" : Convert.ToString(intRowHandles.Length) + " Visits") + " selected for setting as Ready To Send.\n\nProceed?";
            if (XtraMessageBox.Show(strMessage, "Set Visits Ready To Send", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;

            using (var SetReadyToSend = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter())
            {
                SetReadyToSend.ChangeConnectionString(strConnectionString);
                try
                {
                    SetReadyToSend.sp06537_OM_Set_Visit_Status(sbVisitIDs.ToString(), 10);  // 10 = Ready To Send Status //
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("An error occurred while attempting to set the selected visit(s) as Ready To Send - error: " + ex.Message + "\n\nPlease try again. If problems persist contact Technical Support.", "Set Visits Ready To Send", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

            }
            // Merge just changed rows back in to dataset - no need to reload the full dataset //
            Set_DictionaryDataRefreshes_One_Value(dictionaryDataRefreshesMain, xtraTabPageVisits.Text, true);  // Force Linked Visits to refresh //
            Load_Data();
            SetMenuStatus();

            XtraMessageBox.Show("Visit(s) set to Ready To Send Successfully.", "Set Visits Ready To Send", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void Manually_Complete_Visits()
        {
            GridView view = (GridView)gridControlVisit.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more Visits to set as Manually Completed by clicking on them then try again.", "Set Visits Manually Completed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            StringBuilder sbVisitIDs = new StringBuilder();
            string strTempSiteContractID = "";
            StringBuilder sbSiteContractIDs = new StringBuilder(",");  // Initialised with a preceeding comma to be used when checking if build string contains next value - Used at end of process to find any sent visits as a result of this process so they can be refreshed within the grid //

            foreach (int intRowHandle in intRowHandles)
            {
                sbVisitIDs.Append(Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "VisitID")) + ",");
                strTempSiteContractID = Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "SiteContractID")) + ",";
                if (!sbSiteContractIDs.ToString().Contains("," + strTempSiteContractID)) sbSiteContractIDs.Append(strTempSiteContractID);
            }
            string strSiteContractIDs = sbSiteContractIDs.ToString();
            if (strSiteContractIDs.StartsWith(",")) strSiteContractIDs = strSiteContractIDs.Remove(0, 1);  // Remove preceeding comma //

            // Check all selected visits have at least 1 appropriate job... //
            string strValidIDs = "";
            using (var GetIDs = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter())
            {
                GetIDs.ChangeConnectionString(strConnectionString);
                try
                {
                    strValidIDs = GetIDs.sp06385_OM_Get_Visits_To_Manually_Complete(sbVisitIDs.ToString()).ToString();
                }
                catch (Exception) { }
            }
            if (!string.IsNullOrWhiteSpace(strValidIDs)) strValidIDs = "," + strValidIDs + ",";
            sbVisitIDs.Clear();  // Reset //
            string strCurrentID = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strCurrentID = Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["VisitID"])) + ',';
                if (!strValidIDs.Contains("," + strCurrentID))  // Put leading comma in temporarily for checking purposes //
                {
                    view.UnselectRow(intRowHandle);
                }
                else
                {
                    sbVisitIDs.Append(strCurrentID);
                }
            }

            // Check there is at least one row still selected //
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more Visits to set as Manually Completed by clicking on them then try again.\n\nOnly Visits with at least one job can be selected for setting as Manually Completed. If visits are selected which don't have at least one job this process will automatically de-select them.", "Set Visits Manually Completed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            string strMessage = "You have " + (intCount == 1 ? "1 Visit" : Convert.ToString(intRowHandles.Length) + " Visits") + " selected for setting as Manually Completed.\n\nProceed?";
            if (XtraMessageBox.Show(strMessage, "Set Visits Manually Completed", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;

            // Ok to proceed - Get user to input any required remarks //
            var fChildForm = new frm_OM_Tender_Visit_Manually_Complete();
            fChildForm.GlobalSettings = this.GlobalSettings;
            if (fChildForm.ShowDialog() != DialogResult.OK) return;

            string strRemarks = fChildForm._Remarks;

            // Set Visits and linked unstarted jobs as Manually Completed //
            string strNewlySentVisitIDs = "";
            StringBuilder sbNewlySentVisitIDs = new StringBuilder();
            using (var ManuallyCompleteVisits = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter())
            {
                ManuallyCompleteVisits.ChangeConnectionString(strConnectionString);
                try
                {
                    ManuallyCompleteVisits.sp06533_OM_Manually_Complete_EW_Visits(sbVisitIDs.ToString(), strRemarks);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("An error occurred while attempting to set the selected visit(s) as Manually Completed - error: " + ex.Message + "\n\nPlease try again. If problems persist contact Technical Support.", "Set Visits Manually Completed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                // Need to pick up any visits which have been newly sent by the visit table trigger as a result of the manual completion as these will need their status refreshing on the screen if loaded //
                try
                {
                    strNewlySentVisitIDs = ManuallyCompleteVisits.sp06478_OM_Get_Newly_Sent_VisitIDs(strSiteContractIDs, sbVisitIDs.ToString()).ToString();
                }
                catch (Exception ex) { }
                if (!string.IsNullOrWhiteSpace(strNewlySentVisitIDs))
                {
                    int intFoundRow = 0;
                    Array arrayItems = strNewlySentVisitIDs.Split(',');
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = view.LocateByValue(0, view.Columns["VisitID"], Convert.ToInt32(strElement.Trim()));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            sbNewlySentVisitIDs.Append(strElement + ",");
                        }
                    }

                }
            }
            // Merge just changed rows back in to dataset - no need to reload the full dataset //
            Set_DictionaryDataRefreshes_One_Value(dictionaryDataRefreshesMain, xtraTabPageVisits.Text, true);  // Force Linked Visits to refresh //
            Load_Data();
            SetMenuStatus();

            XtraMessageBox.Show("Visit(s) set to Manually Completed Successfully.", "Set Visits Manually Completed", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        #endregion


        #region GridView - History

        private void gridControlHistory_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("reload".Equals(e.Button.Tag))
                    {
                        Set_DictionaryDataRefreshes_One_Value(dictionaryDataRefreshesMain, xtraTabPageHistory.Text, true);  // Force Data to refresh //
                        LoadLinkedRecords();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewHistory_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            try
            {
                if (e.ListSourceRowIndex < 0) return;
                GridView view = (GridView)sender;
                switch (e.Column.FieldName)
                {
                    case "AmountUsed":
                        {
                            string strMask = view.GetRowCellValue(e.ListSourceRowIndex, "AmountUsedDescriptor").ToString();
                            e.DisplayText = string.Format("{0:N2} " + strMask, e.Value);
                            break;
                        }
                    case "AmountUsedDiluted":
                        {
                            string strMask = view.GetRowCellValue(e.ListSourceRowIndex, "AmountUsedDescriptor").ToString();
                            e.DisplayText = string.Format("{0:N2} " + strMask, e.Value);
                            break;
                        }
                    default:
                        break;
                }
            }
            catch (Exception) { }
        }

        private void gridViewHistory_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewHistory_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.History;
            SetMenuStatus();
        }

        private void gridViewHistory_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.History;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView - CRM Contacts

        private void gridControlCRM_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        /*// Drilldown to Linked Document Manager //
                        GridView view = (GridView)gridControlBonuses.MainView;
                        int intRecordType = 11;  // Bonuses //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "BonusID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeName").ToString() + " - " + Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "BonusDueDate")).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date";
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription); */
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewCRM_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewCRM_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.CRM;
            SetMenuStatus();
        }

        private void gridViewCRM_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.CRM;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiDataset.Enabled = (view.SelectedRowsCount > 0);
                bbiDatasetCreate.Enabled = (view.SelectedRowsCount > 0);
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView - Linked Documents

        private void gridControl3_EmbeddedNavigator_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView3;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("email".Equals(e.Button.Tag))
                    {
                        Email_Linked_Documents();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView3_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.LinkedDocument;
            SetMenuStatus();
        }

        private void gridView3_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.LinkedDocument;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void repositoryItemHyperLinkEdit2_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "DocumentPath").ToString();
            string strExtension = view.GetRowCellValue(view.FocusedRowHandle, "DocumentExtension").ToString();
            string strFullPath = Path.Combine(strTenderLinkedDocumentPath, strFile);

            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Document Linked - unable to proceed.", "View Linked Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (!File.Exists(strFullPath))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Linked Document File is missing - unable to proceed.", "View Linked Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                if (strExtension.ToLower() != "pdf")
                {
                    System.Diagnostics.Process.Start(strFullPath);
                }
                else
                {
                    if (!BaseObjects.Keyboard.IsKeyDown(Keys.A))  // Only open in DevExpress PDF viewer if user is not holding down A key (CTRL Key doesn't work when fired from a grid as it's used for row selection) //
                    {
                        frm_Core_PDF_Viewer fChildForm = new frm_Core_PDF_Viewer();
                        fChildForm.strPDFFile = strFullPath;
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.Show();
                    }
                    else
                    {
                          System.Diagnostics.Process.Start(strFullPath);
                    }
                }
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view the Linked Document: " + strFullPath + ".\n\nThe File may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void Email_Linked_Documents()
        {
            GridView view = (GridView)gridControl3.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one Linked Document record to email before proceeding.", "Email Linked Document(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Get the file attachments to email //
            List<string> listFilesToEmail = new List<string>();
            foreach (int intRowHandle in intRowHandles)
            {
                listFilesToEmail.Add(Path.Combine(strTenderLinkedDocumentPath + Convert.ToString(view.GetRowCellValue(intRowHandle, "DocumentPath"))));
            }
            // Get the senders email address //
            string strFromEmailAddress = "";
            try
            {
                WoodPlanDataSetTableAdapters.QueriesTableAdapter GetSetting = new WoodPlanDataSetTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strFromEmailAddress = GetSetting.sp00240_Get_Staff_Email_Address(GlobalSettings.UserID).ToString();
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain your email address from the Staff table.\n\nError: " + ex.Message + "\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Senders Email Address", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (string.IsNullOrWhiteSpace(strFromEmailAddress))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("You have no email address recorded against your username within the Staff table.\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Senders Email Address", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Get the person to send the email to //
            //string strToEmailAddress = "";
            //view = (GridView)gridControl3.MainView;
            //view.GetSelectedRows();
            //if (intRowHandles.Length <= 0)
            //{
            //    DevExpress.XtraEditors.XtraMessageBox.Show("Select the parent survey before proceeding.", "Email Linked Document(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            //    return;
            //}
            //int intSurveyorID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "SurveyorID"));
            //string strSurveyRemarks = view.GetRowCellValue(intRowHandles[0], "ReactiveRemarks").ToString();
            //try
            //{
            //    WoodPlanDataSetTableAdapters.QueriesTableAdapter GetSetting = new WoodPlanDataSetTableAdapters.QueriesTableAdapter();
            //    GetSetting.ChangeConnectionString(strConnectionString);
            //    strToEmailAddress = GetSetting.sp00240_Get_Staff_Email_Address(intSurveyorID).ToString();
            //}
            //catch (Exception ex)
            //{
            //    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Surveyors email address from the Staff table.\n\nError: " + ex.Message + "\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Surveyors Email Address", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            //    return;
            //}
            //if (string.IsNullOrWhiteSpace(strToEmailAddress))
            //{
            //    DevExpress.XtraEditors.XtraMessageBox.Show("There is no email address recorded against the Surveyor within the Staff table.\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Surveyors Email Address", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            //    return;
            //}
            //List<string> _listEmailAddresses = new List<string>();
            //_listEmailAddresses.Add(strToEmailAddress);

            //// Create the subject line //
            //string strSubjectLine = "New ";
            //string strExtractedRemarks = "";
            //if (!string.IsNullOrWhiteSpace(strSurveyRemarks) && strSurveyRemarks.Contains("-"))
            //{
            //    strExtractedRemarks = strSurveyRemarks.Substring(0, strSurveyRemarks.IndexOf('-') + 8);
            //}
            //strSubjectLine += (!string.IsNullOrWhiteSpace(strExtractedRemarks) ? strExtractedRemarks : "Reactive Survey") + " please sync your tablet to view this job";

            // Create the message body //
            string strMessageBody = "";

            var frm_child = new frm_Core_Linked_Document_Email();
            frm_child.GlobalSettings = this.GlobalSettings;
            //frm_child._SubjectLine = strSubjectLine;
            frm_child._MessageBody = strMessageBody;
            frm_child._FilesToEmail = listFilesToEmail;
            //frm_child._EmailAddresses = _listEmailAddresses;
            frm_child._FromEmailAddress = strFromEmailAddress;
            frm_child.ShowDialog();

        }

        #endregion


        #region Date Range \ From Today Filter Panel

        private void btnDateRangeOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void popupContainerEditDateRange_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditDateRange_Get_Selected();
        }

        private string PopupContainerEditDateRange_Get_Selected()
        {
            string strValue = "";
            if (checkEditDateRange.Checked)  // Date Range //
            {
                if (dateEditFromDate.DateTime == DateTime.MinValue)
                {
                    i_dtStart = new DateTime(1900, 1, 1);
                }
                else
                {
                    i_dtStart = dateEditFromDate.DateTime;
                }
                if (dateEditToDate.DateTime == DateTime.MinValue)
                {
                    i_dtEnd = new DateTime(1900, 1, 1);
                }
                else
                {
                    i_dtEnd = dateEditToDate.DateTime;
                }
                if (dateEditFromDate.DateTime == DateTime.MinValue && dateEditToDate.DateTime == DateTime.MinValue)
                {
                    strValue = "No Date Range Filter";
                }
                else
                {
                    strValue = (i_dtStart == new DateTime(1900, 1, 1) ? "No Start" : i_dtStart.ToString("dd/MM/yyyy HH:mm")) + " - " + (i_dtEnd == new DateTime(1900, 1, 1) ? "No End" : i_dtEnd.ToString("dd/MM/yyyy HH:mm"));
                }
            }
            else  // From Date //
            {
                intDaysPrior = Convert.ToInt32(spinEditDaysPrior.EditValue);
                intDaysAfter = Convert.ToInt32(spinEditDaysAfter.EditValue);
                DateTime dtNow = DateTime.Today;
                i_dtStart2 = dtNow.AddDays((intDaysPrior * -1));
                i_dtEnd2 = dtNow.AddDays(intDaysAfter + 1).AddMilliseconds(-5); // Adding full day minus 5 milli-seconds to get 23:59:59.995 //
                strValue = i_dtStart2.ToString("dd/MM/yyyy HH:mm") + " - " + i_dtEnd2.ToString("dd/MM/yyyy HH:mm");
            }
            return strValue;
        }


        private void btnDateRangeFromTodayOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        #endregion


        private void buttonEditClientFilter_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "clear")
            {
                i_str_selected_client_ids = "";
                i_str_selected_client_names = "";
                buttonEditClientFilter.Text = "";
                
                // Update Filter control's tooltip //
                superToolTipSetupArgsClientContractFilter.Contents.Text = "I store the currently applied Client Filter.\n\nNo Filter Set.";
            }
            else if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var fChildForm = new frm_Core_Select_Client_Site();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intAllowChildSelection = 0;
                fChildForm.intMustSelectChildren = 0;
                fChildForm.strPassedInParentIDs = i_str_selected_client_ids;
                fChildForm.i_dtStart = DateTime.Today.AddMonths(-12);  // back a year //
                fChildForm.i_dtEnd = DateTime.Today.AddMonths(12);  // forward a year //
                fChildForm.intFilterSummerMaintenanceClient = 1;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    i_str_selected_client_ids = fChildForm.strSelectedParentIDs;
                    i_str_selected_client_names = fChildForm.strSelectedParentDescriptions;
                    buttonEditClientFilter.Text = i_str_selected_client_names;
                   
                    // Update Filter control's tooltip //
                    string strTooltipText = i_str_selected_client_names.Replace(", ", "\n");
                    superToolTipSetupArgsClientContractFilter.Contents.Text = "I store the currently applied Client Filter.\n\n" + strTooltipText;

                    if (!string.IsNullOrWhiteSpace(fChildForm.strSelectedChildIDs))
                    {
                        i_str_selected_site_ids = fChildForm.strSelectedChildIDs;
                        i_str_selected_site_names = fChildForm.strSelectedChildDescriptions;
                        buttonEditSiteFilter.Text = i_str_selected_site_names;
                        
                        // Update Filter control's tooltip //
                        strTooltipText = i_str_selected_site_names.Replace(", ", "\n");
                        superToolTipSetupArgsSiteContractFilter.Contents.Text = "I store the currently applied Site Filter.\n\n" + strTooltipText;
                    }
                    else
                    {
                        // Update Filter control's tooltip //
                        strTooltipText = i_str_selected_site_names.Replace(", ", "\n");
                        superToolTipSetupArgsSiteContractFilter.Contents.Text = "I store the currently applied Site Filter.\n\nNo Filter Set.";
                    }
                }
            }
        }

        private void buttonEditSiteFilter_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "clear")
            {
                i_str_selected_site_ids = "";
                i_str_selected_site_names = "";
                buttonEditSiteFilter.Text = "";
                
                // Update Filter control's tooltip //
                superToolTipSetupArgsSiteContractFilter.Contents.Text = "I store the currently applied Site Filter.\n\nNo Filter Set.";
            }
            else if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var fChildForm = new frm_Core_Select_Client_Site();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intAllowChildSelection = 1;
                fChildForm.intMustSelectChildren = 1;
                fChildForm.strPassedInParentIDs = i_str_selected_client_ids;
                fChildForm.strPassedInChildIDs = i_str_selected_site_ids;
                fChildForm.i_dtStart = DateTime.Today.AddMonths(-12);  // back a year //
                fChildForm.i_dtEnd = DateTime.Today.AddMonths(12);  // forward a year //
                fChildForm.intFilterSummerMaintenanceClient = 1;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    i_str_selected_client_ids = fChildForm.strSelectedParentIDs;
                    i_str_selected_client_names = fChildForm.strSelectedParentDescriptions;
                    buttonEditClientFilter.Text = i_str_selected_client_names;

                    // Update Filter control's tooltip //
                    string strTooltipText = i_str_selected_client_names.Replace(", ", "\n");
                    superToolTipSetupArgsClientContractFilter.Contents.Text = "I store the currently applied Client Filter.\n\n" + strTooltipText;

                    i_str_selected_site_ids = fChildForm.strSelectedChildIDs;
                    i_str_selected_site_names = fChildForm.strSelectedChildDescriptions;
                    buttonEditSiteFilter.Text = i_str_selected_site_names;

                    // Update Filter control's tooltip //
                    strTooltipText = i_str_selected_site_names.Replace(", ", "\n");
                    superToolTipSetupArgsSiteContractFilter.Contents.Text = "I store the currently applied Site Filter.\n\n" + strTooltipText;
                }
            }
        }


        #region Visit System Status Filter Panel

        private void btnVisitSystemStatusFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void popupContainerEditVisitSystemStatusFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditVisitSystemStatusFilter_Get_Selected();
        }

        private string PopupContainerEditVisitSystemStatusFilter_Get_Selected()
        {
            i_str_selected_TenderStatus_ids = "";    // Reset any prior values first //
            i_str_selected_TenderStatus_descriptions = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl4.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_TenderStatus_ids = "";
                return "No Tender Status Filter";

            }
            else if (selection1.SelectedCount <= 0)
            {
                i_str_selected_TenderStatus_ids = "";
                return "No Tender Status Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_TenderStatus_ids += Convert.ToString(view.GetRowCellValue(i, "ID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_TenderStatus_descriptions = Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_TenderStatus_descriptions += ", " + Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_TenderStatus_descriptions;
        }

        #endregion


        #region KAM Filter Panel

        private void popupContainerEditKAMFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditKAMFilter_Get_Selected();
        }

        private void btnKAMFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private string PopupContainerEditKAMFilter_Get_Selected()
        {
            i_str_selected_KAM_ids = "";    // Reset any prior values first //
            i_str_selected_KAM_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl5.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_KAM_ids = "";
                return "No KAM Filter";

            }
            else if (selection2.SelectedCount <= 0)
            {
                i_str_selected_KAM_ids = "";
                return "No KAM Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_KAM_ids += Convert.ToString(view.GetRowCellValue(i, "ID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_KAM_names = Convert.ToString(view.GetRowCellValue(i, "Surname")) + ": " + Convert.ToString(view.GetRowCellValue(i, "Forename"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_KAM_names += ", " + Convert.ToString(view.GetRowCellValue(i, "Surname")) + ": " + Convert.ToString(view.GetRowCellValue(i, "Forename"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_KAM_names;
        }

        #endregion


        #region CM Filter Panel

        private void popupContainerEditCMFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditCMFilter_Get_Selected();
        }

        private void btnCMFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private string PopupContainerEditCMFilter_Get_Selected()
        {
            i_str_selected_CM_ids = "";    // Reset any prior values first //
            i_str_selected_CM_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl9.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_CM_ids = "";
                return "No CM Filter";

            }
            else if (selection3.SelectedCount <= 0)
            {
                i_str_selected_CM_ids = "";
                return "No CM Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_CM_ids += Convert.ToString(view.GetRowCellValue(i, "ID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_CM_names = Convert.ToString(view.GetRowCellValue(i, "Surname")) + ": " + Convert.ToString(view.GetRowCellValue(i, "Forename"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_CM_names += ", " + Convert.ToString(view.GetRowCellValue(i, "Surname")) + ": " + Convert.ToString(view.GetRowCellValue(i, "Forename"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_CM_names;
        }

        #endregion


        #region Tab Pages Shown Panel

        private void btnShowTabPagesOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void popupContainerEditShowTabPages_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditShowTabPages_Get_Selected();
        }

        private string PopupContainerEditShowTabPages_Get_Selected()
        {
            i_str_selected_TabPages = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl7.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_TabPages = "";
                foreach (XtraTabPage i in xtraTabControl2.TabPages)
                {
                    i.PageVisible = true;
                }
                return "All Linked Data";

            }
            else if (selectionTabPages.SelectedCount <= 0)
            {
                i_str_selected_TabPages = "";
                foreach (XtraTabPage i in xtraTabControl2.TabPages)
                {
                    i.PageVisible = true;
                }
                return "All Linked Data";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        //i_str_selected_CallOutType_ids += Convert.ToString(view.GetRowCellValue(i, "Value")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_TabPages = Convert.ToString(view.GetRowCellValue(i, "TabPageName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_TabPages += ", " + Convert.ToString(view.GetRowCellValue(i, "TabPageName"));
                        }
                        intCount++;
                    }
                }
            }
            foreach (XtraTabPage i in xtraTabControl2.TabPages)
            {
                string strText = i.Text;
                i.PageVisible = i_str_selected_TabPages.Contains(strText);
            }
            return i_str_selected_TabPages;
        }

        #endregion


        #region Labour Filter Panel

        private void btnLabourFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void popupContainerEditLabourFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditLabourFilter_Get_Selected();
        }

        private string PopupContainerEditLabourFilter_Get_Selected()
        {
            i_str_selected_CreatedByStaff_ids = "";    // Reset any prior values first //
            i_str_selected_CreatedByStaff_descriptions = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl12.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_CreatedByStaff_ids = "";
                return "No Created By Filter";

            }
            else if (selection4.SelectedCount <= 0)
            {
                i_str_selected_CreatedByStaff_ids = "";
                return "No Created By Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_CreatedByStaff_ids += Convert.ToInt32(view.GetRowCellValue(i, "ID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_CreatedByStaff_descriptions = Convert.ToString(view.GetRowCellValue(i, "Name"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_CreatedByStaff_descriptions += ", " + Convert.ToString(view.GetRowCellValue(i, "Name"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_CreatedByStaff_descriptions;
        }

        #endregion


        #region Job Type Filter Panel

        private void btnJobTypeFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void popupContainerEditJobTypeFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditJobTypeFilter_Get_Selected();
        }

        private string PopupContainerEditJobTypeFilter_Get_Selected()
        {
            i_str_selected_JobType_ids = "";    // Reset any prior values first //
            i_str_selected_JobType_descriptions = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl10.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_JobType_ids = "";
                return "No Work Type Filter";

            }
            else if (selection5.SelectedCount <= 0)
            {
                i_str_selected_JobType_ids = "";
                return "No Work Type Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_JobType_ids += Convert.ToString(view.GetRowCellValue(i, "ID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_JobType_descriptions = Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_JobType_descriptions += ", " + Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_JobType_descriptions;
        }

        #endregion


        #region Job Sub-Type Filter Panel

        private void btnJobSubTypeFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void popupContainerEditJobSubTypeFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditJobSubTypeFilter_Get_Selected();
        }

        private string PopupContainerEditJobSubTypeFilter_Get_Selected()
        {
            i_str_selected_JobSubType_ids = "";    // Reset any prior values first //
            i_str_selected_JobSubType_descriptions = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl11.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_JobSubType_ids = "";
                return "No Work Sub-Type Filter";

            }
            else if (selection6.SelectedCount <= 0)
            {
                i_str_selected_JobSubType_ids = "";
                return "No Work Sub-Type Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_JobSubType_ids += Convert.ToString(view.GetRowCellValue(i, "ID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_JobSubType_descriptions = Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_JobSubType_descriptions += ", " + Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_JobSubType_descriptions;
        }

        #endregion


        private void btnFormatData_Click(object sender, EventArgs e)
        {
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Formatting data...");

            TenderIDsMemoEdit.EditValue = BaseObjects.ExtensionFunctions.FormatStringToCSV(TenderIDsMemoEdit.EditValue.ToString(), true);

            if (splashScreenManager1.IsSplashFormVisible) splashScreenManager1.CloseWaitForm();
        }


        #region Buttons

        private void bbiToolbarAdd_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlTender.MainView;
            view.GridControl.Select();  // Focus grid //
            _enmFocusedGrid = Utils.enmFocusedGrid.Tender;
            Add_Record();
        }

        private void bbiToolbarAddFromTemplate_ItemClick(object sender, ItemClickEventArgs e)
        {
            Add_Record_From_Template();
        }

        private void bbiSelectJobsReadyToSend_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlVisit.MainView;
            int intDataRowCount = view.DataRowCount;
            if (intDataRowCount <= 0) return;

            view.BeginSelection();
            view.ClearSelection();
            view.EndSelection();
                         
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < intDataRowCount; i++)
            {
                sb.Append(Convert.ToString(view.GetRowCellValue(i, "VisitID")) + ',');
            }

            string strValidRecordIDs = GetVisitIDsReadyToSend(sb.ToString());
            if (string.IsNullOrWhiteSpace(strValidRecordIDs))
            {
                XtraMessageBox.Show("No Visits are ready for Sending.\nCriteria as follows:\n--> At least one linked job with a Team\n--> Job Status = Job Creation Completed - Ready To Send", "Select Visits Ready To Send", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            strValidRecordIDs = "," + strValidRecordIDs + ",";

            for (int i = 0; i < intDataRowCount; i++)
            {
                if (strValidRecordIDs.Contains("," + Convert.ToString(view.GetRowCellValue(i, "VisitID")) + ",")) view.SelectRow(i);
            }

            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intRowHandles.Length <= 0)
            {
                XtraMessageBox.Show("No Visits are ready for Sending.\nCriteria as follows:\n--> At least one linked job with a Team\n--> Job Status = Job Creation Completed - Ready To Send", "Select Visits Ready To Send", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else
            {
                XtraMessageBox.Show(intRowHandles.Length + " Visit(s) selected as ready for Sending.", "Select Visits Ready To Send", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return;
        }

        private void bbiSendJobs_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlVisit.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more records to send before proceeding.", "Send Visits", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            StringBuilder sb = new StringBuilder();
            foreach (int intRowHandle in intRowHandles)
            {
                sb.Append(Convert.ToString(view.GetRowCellValue(intRowHandle, "VisitID")) + ',');
            }

            string strVisitIDsAllowed = "," + GetVisitIDsReadyToSend(sb.ToString()) + ",";

            sb.Clear();
            if (strVisitIDsAllowed.Length > 2)
            {
                foreach (int intRowHandle in intRowHandles)
                {
                    if (!strVisitIDsAllowed.Contains("," + Convert.ToString(view.GetRowCellValue(intRowHandle, "VisitID")) + ","))
                    {
                        view.UnselectRow(intRowHandle);
                    }
                    else
                    {
                        sb.Append(Convert.ToInt32(view.GetRowCellValue(intRowHandle, "VisitID")) + ",");
                    }
                }
            }

            if (sb.Length <= 0)
            {
                XtraMessageBox.Show("No Visits are ready for Sending. Note: This process de-select visits which are not appropriate to send.\nCriteria as follows:\n--> At least one linked job with a Team\n--> Job Status = Job Creation Completed - Ready To Send", "Select Visits Ready To Send", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;

            string strMessage = "You have " + (intCount == 1 ? "1 Visit" : Convert.ToString(intRowHandles.Length) + " Visits") + " selected for sending.\n\nProceed?";
            if (XtraMessageBox.Show(strMessage, "Send Visits", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;

            if (!splashScreenManager.IsSplashFormVisible)
            {
                this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Sending Visits...");
            }

            using (var SendRecords = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter())
            {
                SendRecords.ChangeConnectionString(strConnectionString + "Connect Timeout=3600;");
                SendRecords.ChangeCommandTimeout("dbo.sp06274_OM_Send_Due_Jobs", 3600);  // Timeout on SQL Connection to 1 hour - need line above too. //               
                try
                {
                    SendRecords.sp06274_OM_Send_Due_Jobs(sb.ToString());
                }
                catch (Exception) { }
            }

            if (splashScreenManager.IsSplashFormVisible)
            {
                splashScreenManager.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager.CloseWaitForm();
            }
            
            // Merge just changed rows back in to dataset - no need to reload the full dataset //
            Set_DictionaryDataRefreshes_One_Value(dictionaryDataRefreshesMain, xtraTabPageVisits.Text, true);
            LoadLinkedRecords();

            strMessage = intCount.ToString() + (intCount == 1 ? " Visit" : " Visits") + " Sent.";
            XtraMessageBox.Show(strMessage, "Send Visits", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private string GetVisitIDsReadyToSend(string VisitIDs)
        {
            string strRecordIDs = "";
            using (var SelectRecords = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter())
            {
                SelectRecords.ChangeConnectionString(strConnectionString);
                try
                {
                    strRecordIDs = SelectRecords.sp06273_OM_Get_Due_Jobs(VisitIDs).ToString();
                }
                catch (Exception) { }
            }
            return strRecordIDs;
        }

        private void bbiViewSiteOnMap_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlTender.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            View_On_Map(intRowHandles, "start");
        }

        private void bbiVisitDrillDown_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlVisit.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more visit records to drill down to before proceeding.", "Visits Drill Down", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            StringBuilder sb = new StringBuilder();
            foreach (int intRowHandle in intRowHandles)
            {
                sb.Append(Convert.ToString(view.GetRowCellValue(intRowHandle, "VisitID")) + ',');
            }

            Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, sb.ToString(), "operations_visit");
        }


        private void bbiSendQuoteToCM_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlTender.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount != 1)
            {
                XtraMessageBox.Show("Select just one Tender to send to CM before proceeding.", "Send Tender To Contract Manager", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            var currentRowView = (DataRowView)sp06491OMTenderManagerBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06491_OM_Tender_ManagerRow)currentRowView.Row;
            if (currentRow == null) return;

            #region Initial Data checks
            int intCM = currentRow.CM;
            if (intCM <= 0)
            {
                XtraMessageBox.Show("Unable to Send Tender to Contract Manager - the CM has not been selected for this Tender. Please select the CM before proceeding.", "Send Tender to Contract Manager", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            int intClientID = currentRow.ClientID;
            int intSiteID = currentRow.SiteID;
            if (intClientID <= 0 || intSiteID <= 0)
            {
                XtraMessageBox.Show("Unable to Send Tender to Contract Manager - the Client and\\or Site has not been selected for this Tender. Please select the Client and Site before proceeding.", "Send Tender to Contract Manager", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            int intTenderID = currentRow.TenderID;
            int intClientContractID = currentRow.ClientContractID;

            if (XtraMessageBox.Show("You are about to send the Quote to the Contract Manager.\n\nAre you sure you wish to proceed?", "Send Quote to CM", MessageBoxButtons.YesNo, MessageBoxIcon.Question, DefaultBoolean.True) == DialogResult.No) return;
            #endregion

            // Open screen to allow the user to imput any required comments //
            var fChildForm = new frm_OM_Tender_Send_CM_Email_Add_Comment();
            fChildForm.GlobalSettings = this.GlobalSettings;
            if (fChildForm.ShowDialog() != DialogResult.OK) return;

            string strCMComment = fChildForm._Comments;
            if (!string.IsNullOrWhiteSpace(strCMComment))  // Append to Tender CM Comment first and save to DB then pass to email // 
            {
                string strExistingComment = "";
                if (!DBNull.Value.Equals(currentRow["CMComments"])) strExistingComment = currentRow.CMComments;
                if (!string.IsNullOrWhiteSpace(strExistingComment)) strExistingComment += "\r\n\r\n";
                strExistingComment += "KAM Comment to CM via CM Quote\r\n" +
                                        "Date: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "\r\n" +
                                        "CM: " + (string.IsNullOrWhiteSpace(currentRow.CMName) ? "" : currentRow.CMName) + "\r\n" +
                                        "KAM: " + (string.IsNullOrWhiteSpace(currentRow.KAMName) ? "" : currentRow.KAMName) + "\r\n" +
                                        "Comment:\r\n" + strCMComment;
                currentRow.CMComments = strExistingComment;

                // Write back updated CM comments to DB //
                using (var UpdateRecord = new DataSet_OM_TenderTableAdapters.QueriesTableAdapter())
                {
                    UpdateRecord.ChangeConnectionString(strConnectionString);
                    try
                    {
                        UpdateRecord.sp06531_OM_Tender_Manager_Update_CMComment(intTenderID, strExistingComment);
                    }
                    catch (Exception) { }
                }
            }

            DateTime dtNow = DateTime.Now;
            char[] delimiters = new char[] { ',' };

            #region Get CM Email Address
            string strEmailAddresses = "";
            try
            {
                var GetSetting = new WoodPlanDataSetTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strEmailAddresses = GetSetting.sp00240_Get_Staff_Email_Address(intCM).ToString();
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the CM Email Address from the Staff table.\n\nError: " + ex.Message + "\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get CM Email Address", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (string.IsNullOrWhiteSpace(strEmailAddresses))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The CM has no Email address stored against their name in the Staff tabel. Please update the CM Staff record before trying again.", "Get CM Email Address", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            #endregion

            splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager.ShowWaitForm();
            splashScreenManager.SetWaitFormDescription("Sending Tender to CM...");

            try
            {
                #region Get Email Settings
                string strHTMLBodyFile = "";
                string strEmailFrom = "";
                string strEmailSubjectLine = "";
                string strCCToEmailAddress = "";

                string strSMTPMailServerAddress = "";
                string strSMTPMailServerUsername = "";
                string strSMTPMailServerPassword = "";
                string strSMTPMailServerPort = "";
                string strLinkedNotesDocument = "";

                SqlDataAdapter sdaSettings = new SqlDataAdapter();
                DataSet dsSettings = new DataSet("NewDataSet");

                using (var conn = new SqlConnection(strConnectionString))
                {
                    conn.Open();
                    using (var cmd = new SqlCommand())
                    {
                        cmd.CommandText = "sp06518_OM_Tender_CM_Email_Get_Email_Settings";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@ClientContractID", intClientContractID));
                        cmd.Parameters.Add(new SqlParameter("@TenderID", intTenderID));
                        cmd.Parameters.Add(new SqlParameter("@StaffID", GlobalSettings.UserID));
                        cmd.Connection = conn;
                        sdaSettings = new SqlDataAdapter(cmd);
                        sdaSettings.Fill(dsSettings, "Table");
                    }
                    conn.Close();
                }
                if (dsSettings.Tables[0].Rows.Count != 1)
                {
                    if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the 'Email Settings' (from the System Configuration Screen) - number of rows returned not equal to 1.\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                DataRow dr1 = dsSettings.Tables[0].Rows[0];
                strHTMLBodyFile = dr1["BodyFileName"].ToString();
                strEmailFrom = dr1["EmailFrom"].ToString();
                strEmailSubjectLine = dr1["SubjectLine"].ToString();
                strCCToEmailAddress = dr1["CCToName"].ToString();

                strLinkedNotesDocument = dr1["LinkedNotesDocument"].ToString();
                if (!string.IsNullOrWhiteSpace(strLinkedNotesDocument) && !string.IsNullOrWhiteSpace(i_str_TenderRequestNotesFolder)) strLinkedNotesDocument = Path.Combine(i_str_TenderRequestNotesFolder, strLinkedNotesDocument);

                strSMTPMailServerAddress = dr1["SMTPMailServerAddress"].ToString();
                strSMTPMailServerUsername = dr1["SMTPMailServerUsername"].ToString();
                strSMTPMailServerPassword = dr1["SMTPMailServerPassword"].ToString();
                strSMTPMailServerPort = dr1["SMTPMailServerPort"].ToString();
                if (string.IsNullOrEmpty(strSMTPMailServerPort) || !CheckingFunctions.IsNumeric(strSMTPMailServerPort)) strSMTPMailServerPort = "0";
                int intSMTPMailServerPort = Convert.ToInt32(strSMTPMailServerPort);

                if (string.IsNullOrEmpty(strHTMLBodyFile) || string.IsNullOrEmpty(strEmailFrom) || string.IsNullOrEmpty(strSMTPMailServerAddress))
                {
                    if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more of the email settings (Email Layout File, From Email Address and SMTP Mail Server Name) are missing from the System Configuration Screen.\n\nPlease update the System Settings then try again. If the problem persists, contact Technical Support.", "Get Email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                #endregion


                #region Ping Test
                bool boolNoInternet = BaseObjects.PingTest.Ping("www.google.com");
                if (!boolNoInternet) boolNoInternet = BaseObjects.PingTest.Ping("www.microsoft.com");  // try another site just in case that one is down //
                if (!boolNoInternet) boolNoInternet = BaseObjects.PingTest.Ping("www.yahoo.com");  // try another site just in case that one is down //
                if (!boolNoInternet) // alert user and halt process //
                {
                    if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to connect to the internet - unable to send.\n\nPlease check your internet connection then try again.\n\nIf the problem persists, contact Technical Support.", "Check Internet Connection", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                #endregion

                using (var conn = new SqlConnection(strConnectionString))
                {
                    conn.Open();
                    SqlDataAdapter sda = new SqlDataAdapter();
                    DataSet ds = new DataSet("NewDataSet");
                    using (var cmd = new SqlCommand())
                    {
                        cmd.CommandText = "sp06517_OM_Tender_CM_Email_Data";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@TenderID", intTenderID));
                        cmd.Connection = conn;
                        sda = new SqlDataAdapter(cmd);
                        sda.Fill(ds, "Table");
                    }
                    conn.Close();
                    string strBody = System.IO.File.ReadAllText(strHTMLBodyFile);

                    // Merge the data into the HTML //
                    DataRow dr = ds.Tables[0].Rows[0];
                    foreach (DataColumn dc in ds.Tables[0].Columns)
                    {
                        //strBody = strBody.Replace("%" + dc.Caption + "%", dr[dc].ToString());
                        strBody = strBody.Replace("%" + dc.Caption + "%", dr[dc].ToString().Replace("\r\n", "<br />"));
                    }

                    System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                    msg.From = new System.Net.Mail.MailAddress(strEmailFrom);
                    string[] strEmailTo = strEmailAddresses.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    if (strEmailTo.Length > 0)
                    {
                        foreach (string strEmailAddress in strEmailTo)
                        {
                            msg.To.Add(new System.Net.Mail.MailAddress(strEmailAddress));
                        }
                    }
                    else
                    {
                        msg.To.Add(new System.Net.Mail.MailAddress(strEmailAddresses));  // Original value wouldn't split as no commas so it's just one email address so use it //
                    }
                    msg.Subject = strEmailSubjectLine;
                    if (!string.IsNullOrEmpty(strCCToEmailAddress)) msg.CC.Add(strCCToEmailAddress);
                    msg.Priority = System.Net.Mail.MailPriority.High;
                    msg.IsBodyHtml = true;

                    System.Net.Mail.AlternateView plainView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(System.Text.RegularExpressions.Regex.Replace(strBody, @"<(.|\n)*?>", string.Empty), null, "text/plain");
                    System.Net.Mail.AlternateView htmlView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(strBody, null, "text/html");

                    // Create a new attachment //
                    if (!string.IsNullOrWhiteSpace(strLinkedNotesDocument))
                    {
                        System.Net.Mail.Attachment mailAttachment = new System.Net.Mail.Attachment(strLinkedNotesDocument);  // Attach Quote Notes Template file from client contract //
                        msg.Attachments.Add(mailAttachment);
                    }

                    msg.AlternateViews.Add(plainView);
                    msg.AlternateViews.Add(htmlView);

                    object userState = msg;
                    System.Net.Mail.SmtpClient emailClient = null;
                    if (intSMTPMailServerPort != 0)
                    {
                        emailClient = new System.Net.Mail.SmtpClient(strSMTPMailServerAddress, intSMTPMailServerPort);
                    }
                    else
                    {
                        emailClient = new System.Net.Mail.SmtpClient(strSMTPMailServerAddress);
                    }
                    if (!string.IsNullOrEmpty(strSMTPMailServerUsername) && !string.IsNullOrEmpty(strSMTPMailServerPassword))
                    {
                        System.Net.NetworkCredential basicCredential = new System.Net.NetworkCredential(strSMTPMailServerUsername, strSMTPMailServerPassword);
                        emailClient.UseDefaultCredentials = false;
                        emailClient.Credentials = basicCredential;
                    }
                    emailClient.SendAsync(msg, userState);

                    // Update the DB //
                    using (var UpdateTender = new DataSet_OM_TenderTableAdapters.QueriesTableAdapter())
                    {
                        UpdateTender.ChangeConnectionString(strConnectionString);
                        try
                        {
                            UpdateTender.sp06522_OM_Tender_Manager_Update_Status(intTenderID, 20, dtNow, 0, 0, "");
                        }
                        catch (Exception) { }
                    }
                }
            }
            catch (Exception ex)
            {
                if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                XtraMessageBox.Show("Unable to Send Tender to Contract Manager - an error occurred:\n\n" + ex.Message, "Send Tender to Contract Manager", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();

            Set_DictionaryDataRefreshes_One_Value(dictionaryDataRefreshesMain, xtraTabPageHistory.Text, true);  // Force Linked Visits to refresh //
            Load_Data();
            SetMenuStatus();

            XtraMessageBox.Show("Tender Sent to Contract Manager successfully.", "Send Tender to Contract Manager", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void bbiQuoteNotRequired_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlTender.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount != 1)
            {
                XtraMessageBox.Show("Select just one Tender to set as Quote Not Required.", "Set Tender as Quote Not Required", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            var currentRowView = (DataRowView)sp06491OMTenderManagerBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06491_OM_Tender_ManagerRow)currentRowView.Row;
            if (currentRow == null) return;

            DateTime dtNow = DateTime.Now;
            int intTenderID = currentRow.TenderID;

            // Update the DB //
            using (var UpdateTender = new DataSet_OM_TenderTableAdapters.QueriesTableAdapter())
            {
                UpdateTender.ChangeConnectionString(strConnectionString);
                try
                {
                    UpdateTender.sp06522_OM_Tender_Manager_Update_Status(intTenderID, 30, dtNow, 0, 0, "");
                }
                catch (Exception) { }
            }

            Set_DictionaryDataRefreshes_One_Value(dictionaryDataRefreshesMain, xtraTabPageHistory.Text, true);  // Force Linked Visits to refresh //
            Load_Data();
            SetMenuStatus();

            XtraMessageBox.Show("Tender set to Quote Not Required successfully.", "Set Tender as Quote Not Required", MessageBoxButtons.OK, MessageBoxIcon.Information);  
        }

        private void bbiCMReturnedQuote_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlTender.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount != 1)
            {
                XtraMessageBox.Show("Select just one Tender to set as CM Returned Quote.", "Set Tender as CM Returned Quote", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            var currentRowView = (DataRowView)sp06491OMTenderManagerBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06491_OM_Tender_ManagerRow)currentRowView.Row;
            if (currentRow == null) return;

            DateTime dtNow = DateTime.Now;
            int intTenderID = currentRow.TenderID;

            // Update the DB //
            using (var UpdateTender = new DataSet_OM_TenderTableAdapters.QueriesTableAdapter())
            {
                UpdateTender.ChangeConnectionString(strConnectionString);
                try
                {
                    UpdateTender.sp06522_OM_Tender_Manager_Update_Status(intTenderID, 30, dtNow, 0, 0, "");
                }
                catch (Exception) { }
            }

            Set_DictionaryDataRefreshes_One_Value(dictionaryDataRefreshesMain, xtraTabPageHistory.Text, true);  // Force Linked Visits to refresh //
            Load_Data();
            SetMenuStatus();

            XtraMessageBox.Show("Tender set to CM Returned Quote successfully.", "Set Tender as CM Returned Quote", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void bbiKAMAccepted_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlTender.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount != 1)
            {
                XtraMessageBox.Show("Select just one Tender to set as KAM Authorised.", "KAM Authorised - Ready To Send To Client", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (XtraMessageBox.Show("You have clicked KAM Authorised - This will set the Tender as Ready To Send To Client.\n\nAre you sure you wish to proceed?", "KAM Authorised Quote", MessageBoxButtons.YesNo, MessageBoxIcon.Question, DefaultBoolean.True) == DialogResult.No) return;

            var currentRowView = (DataRowView)sp06491OMTenderManagerBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06491_OM_Tender_ManagerRow)currentRowView.Row;
            if (currentRow == null) return;

            DateTime dtNow = DateTime.Now;
            int intTenderID = currentRow.TenderID;

            // Update the DB //
            using (var UpdateTender = new DataSet_OM_TenderTableAdapters.QueriesTableAdapter())
            {
                UpdateTender.ChangeConnectionString(strConnectionString);
                try
                {
                    UpdateTender.sp06522_OM_Tender_Manager_Update_Status(intTenderID, 40, dtNow, 0, 0, "");
                }
                catch (Exception) { }
            }

            Set_DictionaryDataRefreshes_One_Value(dictionaryDataRefreshesMain, xtraTabPageHistory.Text, true);  // Force Linked Visits to refresh //
            Load_Data();
            SetMenuStatus();

            XtraMessageBox.Show("Tender set to CM Returned Quote successfully.", "KAM Authorised - Ready To Send To Client", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void bbiKAMRejected_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlTender.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount != 1)
            {
                XtraMessageBox.Show("Select just one Tender to set as KAM Rejected Quote.", "KAM Rejected Quote", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (XtraMessageBox.Show("You have clicked KAM Rejected - You will asked to specify what to do next.\n\nAre you sure you wish to proceed?", "KAM Rejected Quote", MessageBoxButtons.YesNo, MessageBoxIcon.Question, DefaultBoolean.True) == DialogResult.No) return;

            var currentRowView = (DataRowView)sp06491OMTenderManagerBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06491_OM_Tender_ManagerRow)currentRowView.Row;
            if (currentRow == null) return;

            DateTime dtNow = DateTime.Now;
            int intTenderID = currentRow.TenderID;

            var fChildForm = new frm_OM_Tender_KAM_Reject_Next_Action();
            fChildForm.GlobalSettings = this.GlobalSettings;
            if (fChildForm.ShowDialog() != DialogResult.OK) return;

            int intKAMQuoteRejectionReasonID = fChildForm._RejectionReasonID;
            string strKAMQuoteRejectionRemarks = fChildForm._KAMQuoteRejectionRemarks;

            if (fChildForm._NextStepID == 1)  // Resend to CM //
            {
                // Update the DB //
                using (var UpdateTender = new DataSet_OM_TenderTableAdapters.QueriesTableAdapter())
                {
                    UpdateTender.ChangeConnectionString(strConnectionString);
                    try
                    {
                        UpdateTender.sp06522_OM_Tender_Manager_Update_Status(intTenderID, 10, dtNow, 0, intKAMQuoteRejectionReasonID, strKAMQuoteRejectionRemarks);
                    }
                    catch (Exception) { }
                }
                Set_DictionaryDataRefreshes_One_Value(dictionaryDataRefreshesMain, xtraTabPageHistory.Text, true);  // Force Linked Visits to refresh //
                Load_Data();
                SetMenuStatus();

                XtraMessageBox.Show("Tender set to Tender Created successfully.", "KAM Rejected Quote", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else  // Cancel Tender //
            {
                // Update the DB //
                using (var UpdateTender = new DataSet_OM_TenderTableAdapters.QueriesTableAdapter())
                {
                    UpdateTender.ChangeConnectionString(strConnectionString);
                    try
                    {
                        UpdateTender.sp06522_OM_Tender_Manager_Update_Status(intTenderID, 100, dtNow, 0, intKAMQuoteRejectionReasonID, "");
                    }
                    catch (Exception) { }
                }
                Set_DictionaryDataRefreshes_One_Value(dictionaryDataRefreshesMain, xtraTabPageHistory.Text, true);  // Force Linked Visits to refresh //
                Load_Data();
                SetMenuStatus();

                XtraMessageBox.Show("Tender Sent to Tender Declined (Internal) successfully.", "KAM Rejected Quote", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void bbiSentToClient_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlTender.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount != 1)
            {
                XtraMessageBox.Show("Select just one Tender to set as Sent To Client.", "Set Tender as Sent To Client", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (XtraMessageBox.Show("You have clicked Sent To Client - This will set the Tender as Tender Sent To Client and the current Date\\Time will be stored as when it was sent to the client.\n\nAre you sure you wish to proceed?", "Set Tender as Sent To Client", MessageBoxButtons.YesNo, MessageBoxIcon.Question, DefaultBoolean.True) == DialogResult.No) return;

            var currentRowView = (DataRowView)sp06491OMTenderManagerBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06491_OM_Tender_ManagerRow)currentRowView.Row;
            if (currentRow == null) return;

            decimal decQuotedTotalSell = currentRow.QuotedTotalSell;
            if (decQuotedTotalSell <= (decimal)0.00)
            {
                if (XtraMessageBox.Show("You have not entered any quoted costs for the Client Price.\n\nAre you sure you wish to proceed?", "Tender Sent to Client", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, DefaultBoolean.True) == DialogResult.No) return;
            }

            DateTime dtNow = DateTime.Now;
            int intTenderID = currentRow.TenderID;

            // Update the DB //
            using (var UpdateTender = new DataSet_OM_TenderTableAdapters.QueriesTableAdapter())
            {
                UpdateTender.ChangeConnectionString(strConnectionString);
                try
                {
                    UpdateTender.sp06522_OM_Tender_Manager_Update_Status(intTenderID, 50, dtNow, 0, 0, "");
                }
                catch (Exception) { }
            }

            Set_DictionaryDataRefreshes_One_Value(dictionaryDataRefreshesMain, xtraTabPageHistory.Text, true);  // Force Linked Visits to refresh //
            Load_Data();
            SetMenuStatus();

            XtraMessageBox.Show("Tender set to CM Returned Quote successfully.", "Set Tender as Sent To Client", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void bbiClientAccepted_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlTender.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount != 1)
            {
                XtraMessageBox.Show("Select just one Tender to set as Tender Accepted By Client.", "Set Tender as Tender Accepted By Client", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (XtraMessageBox.Show("You have clicked Client Accepted Quote - This will set the Tender as Tender Accepted.\n\nAre you sure you wish to proceed?", "Set Tender as Tender Accepted By Client", MessageBoxButtons.YesNo, MessageBoxIcon.Question, DefaultBoolean.True) == DialogResult.No) return;

            var currentRowView = (DataRowView)sp06491OMTenderManagerBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06491_OM_Tender_ManagerRow)currentRowView.Row;
            if (currentRow == null) return;

            DateTime dtNow = DateTime.Now;
            int intTenderID = currentRow.TenderID;

            // Update the DB //
            using (var UpdateTender = new DataSet_OM_TenderTableAdapters.QueriesTableAdapter())
            {
                UpdateTender.ChangeConnectionString(strConnectionString);
                try
                {
                    UpdateTender.sp06522_OM_Tender_Manager_Update_Status(intTenderID, 80, dtNow, 0, 0, "");
                }
                catch (Exception) { }
            }

            Set_DictionaryDataRefreshes_One_Value(dictionaryDataRefreshesMain, xtraTabPageHistory.Text, true);  // Force Linked Visits to refresh //
            Load_Data();
            SetMenuStatus();

            XtraMessageBox.Show("Tender set to CM Returned Quote successfully.", "Set Tender as Tender Accepted By Client", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void bbiClientRejected_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlTender.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount != 1)
            {
                XtraMessageBox.Show("Select just one Tender to set as Tender Rejected By Client.", "Set Tender as Tender Rejected By Client", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (XtraMessageBox.Show("You have clicked Client Declined Quote - This will set the Tender as Tender Declined.\n\nAre you sure you wish to proceed?", "Tender Declined By Client", MessageBoxButtons.YesNo, MessageBoxIcon.Question, DefaultBoolean.True) == DialogResult.No) return;

            var currentRowView = (DataRowView)sp06491OMTenderManagerBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06491_OM_Tender_ManagerRow)currentRowView.Row;
            if (currentRow == null) return;

            DateTime dtNow = DateTime.Now;
            int intTenderID = currentRow.TenderID;

            DialogResult dr = XtraMessageBox.Show("Would you like to Re-tender the quote?\n\n--> Click <b>Yes</b> to re-tender the quote.\n\n--> Click <b>No</b> to set the status to Tender Declined By Client. If select this you must select a Reason.\n\n--> Click <b>Cancel</b> to abort the status change.", "Tender Declined By Client", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, DefaultBoolean.True);
            switch (dr)
            {
                case DialogResult.Cancel:
                    return;
                case DialogResult.No:
                    {
                        // Get user to pick a reason //
                        var fChildForm = new frm_OM_Select_Status_Issue_For_Status();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInTenderStatusIDsFilter = "70,";
                        fChildForm.strPassedInTenderStatusDescriptionFilter = "Tender Declined By Client";
                        fChildForm.intOriginalTenderStatusIssueID = 70;
                        if (fChildForm.ShowDialog() != DialogResult.OK) return;

                        int intStatusIssueID = fChildForm.intSelectedTenderStatusIssueID;

                        // Update the DB //
                        using (var UpdateTender = new DataSet_OM_TenderTableAdapters.QueriesTableAdapter())
                        {
                            UpdateTender.ChangeConnectionString(strConnectionString);
                            try
                            {
                                UpdateTender.sp06522_OM_Tender_Manager_Update_Status(intTenderID, 70, dtNow, intStatusIssueID, 0, "");
                            }
                            catch (Exception) { }
                        }
                        Set_DictionaryDataRefreshes_One_Value(dictionaryDataRefreshesMain, xtraTabPageHistory.Text, true);  // Force Linked Visits to refresh //
                        Load_Data();
                        SetMenuStatus();

                        XtraMessageBox.Show("Tender set to Tender Declined By Client successfully.", "Set Tender as Tender Rejected By Client", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case DialogResult.Yes:
                    {
                        // Update the DB //
                        using (var UpdateTender = new DataSet_OM_TenderTableAdapters.QueriesTableAdapter())
                        {
                            UpdateTender.ChangeConnectionString(strConnectionString);
                            try
                            {
                                UpdateTender.sp06522_OM_Tender_Manager_Update_Status(intTenderID, 10, dtNow, 0, 0, "");
                            }
                            catch (Exception) { }
                        }
                        Set_DictionaryDataRefreshes_One_Value(dictionaryDataRefreshesMain, xtraTabPageHistory.Text, true);  // Force Linked Visits to refresh //
                        Load_Data();
                        SetMenuStatus();

                        XtraMessageBox.Show("Tender set to Tender Created successfully.", "Set Tender as Tender Rejected By Client", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                default:
                    break;
            }
        }

        private void bbiClientOnHold_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlTender.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount != 1)
            {
                XtraMessageBox.Show("Select just one Tender to set as Client On-hold Quote.", "Set Tender as Client On-hold", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (XtraMessageBox.Show("You have clicked Client On-hold Quote - This will set the Tender as Tender On-hold.\n\nAre you sure you wish to proceed?", "Tender On-hold", MessageBoxButtons.YesNo, MessageBoxIcon.Question, DefaultBoolean.True) == DialogResult.No) return;

            var currentRowView = (DataRowView)sp06491OMTenderManagerBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06491_OM_Tender_ManagerRow)currentRowView.Row;
            if (currentRow == null) return;

            DateTime dtNow = DateTime.Now;
            int intTenderID = currentRow.TenderID;

            // Update the DB //
            using (var UpdateTender = new DataSet_OM_TenderTableAdapters.QueriesTableAdapter())
            {
                UpdateTender.ChangeConnectionString(strConnectionString);
                try
                {
                    UpdateTender.sp06522_OM_Tender_Manager_Update_Status(intTenderID, 60, dtNow, 0, 0, "");
                }
                catch (Exception) { }
            }

            Set_DictionaryDataRefreshes_One_Value(dictionaryDataRefreshesMain, xtraTabPageHistory.Text, true);  // Force Linked Visits to refresh //
            Load_Data();
            SetMenuStatus();

            XtraMessageBox.Show("Tender set to Client On-hold successfully.", "Set Tender as Client On-hold", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void bbiAddVisitWizard_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (strFormMode == "view" || strFormMode == "blockadd" || strFormMode == "blockedit") return;

            var currentRowView = (DataRowView)sp06491OMTenderManagerBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06491_OM_Tender_ManagerRow)currentRowView.Row;
            if (currentRow == null) return;

            if (currentRow.TreeProtected == 1 && currentRow.PlanningAuthorityOkToProceed <= 0)
            {
                XtraMessageBox.Show("You have Tree Protected ticked within the TPO section but Ok To Proceed within the Planning section is not ticked.\n\n<color=red>You cannot start works until you have Planning Permissions.</color>\n\nPlease edit the tender and complete the Planning section and tick Ok to Proceed before continuing.", "Create Visits", MessageBoxButtons.OK, MessageBoxIcon.Stop, DefaultBoolean.True);
                return;
            }

            int intSiteID = 0;
            try { intSiteID = (string.IsNullOrEmpty(currentRow.SiteID.ToString()) ? 0 : Convert.ToInt32(currentRow.SiteID)); }
            catch (Exception) { };
            if (intSiteID == 0)
            {
                XtraMessageBox.Show("Please select the Site before attempting to create Visits.", "Create Visits", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (XtraMessageBox.Show("You have clicked Create Visits - This will open the Visit Wizard screen to allow the creation of one or more visits and jobs.\n\nAre you sure you wish to proceed?", "Create Visits", MessageBoxButtons.YesNo, MessageBoxIcon.Question, DefaultBoolean.True) == DialogResult.No) return;


            // Open Visit Wizard //
            var fChildForm = new frm_OM_Tender_Visit_Wizard();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strRecordIDs = "";
            fChildForm.strFormMode = "add";
            fChildForm.strCaller = this.Name;
            fChildForm.intRecordCount = 0;
            fChildForm.i_intPassedInTenderID = currentRow.TenderID;
            fChildForm.i_strPassedInTenderRevisionNumber = currentRow.RevisionNumber;
            fChildForm.i_intPassedInClientContractID = currentRow.ClientContractID;
            fChildForm.i_intPassedInClientID = currentRow.ClientID;
            fChildForm.i_intPassedInSiteID = intSiteID;
            fChildForm.i_strPassedInClientName = currentRow.ClientName;
            fChildForm.i_strPassedInClientContractDescription = currentRow.ContractDescription;
            fChildForm.i_strPassedInSiteName = currentRow.SiteName;
            fChildForm.i_intPassedInSiteContractID = currentRow.SiteContractID;
            fChildForm.i_strPassedInWorkDescription = currentRow.WorkType;

            fChildForm.i_decPassedInTenderLabourCost = currentRow.ActualLabourCost;
            fChildForm.i_decPassedInTenderMaterialCost = currentRow.ActualMaterialCost;
            fChildForm.i_decPassedInTenderEquipmentCost = currentRow.ActualEquipmentCost;
            fChildForm.i_decPassedInTenderTotalCost = currentRow.ActualTotalCost;

            fChildForm.i_decPassedInTenderLabourSell = currentRow.AcceptedLabourSell;
            fChildForm.i_decPassedInTenderMaterialSell = currentRow.AcceptedMaterialSell;
            fChildForm.i_decPassedInTenderEquipmentSell = currentRow.AcceptedEquipmentSell;
            fChildForm.i_decPassedInTenderTotalSell = currentRow.AcceptedTotalSell;

            fChildForm.i_intPassedInJobSubTypeID = currentRow.WorkSubTypeID;
            fChildForm.i_intPassedInLabourTypeID = currentRow.ProposedLabourTypeID;
            fChildForm.i_intPassedInLabourID = currentRow.ProposedLabourID;
            fChildForm.i_strPassedInLabourName = currentRow.ProposedLabour;
            fChildForm.i_dbPassedInLabourLatitude = currentRow.ProposedLabourLatitude;
            fChildForm.i_dbPassedInLabourLongitude = currentRow.ProposedLabourLongitude;
            fChildForm.i_strPassedInLabourPostcode = currentRow.ProposedLabourPostcode;
            fChildForm.i_dtPassedInWorkCompletedBy = currentRow.RequiredWorkCompletedDate;

            fChildForm.frmCaller = null; // Enable link so we can write back the number of created visits (NOT USED BY THIS SCREEN //

            fChildForm.FormPermissions = this.FormPermissions;
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();
            fChildForm.Show();

            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
        }

        private void bbiAddJobWizard_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlTender.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount != 1)
            {
                XtraMessageBox.Show("Select just one Tender to add Jobs to before proceeding.", "Create Jobs", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            var currentRowView = (DataRowView)sp06491OMTenderManagerBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06491_OM_Tender_ManagerRow)currentRowView.Row;
            if (currentRow == null) return;
            if (currentRow.TreeProtected == 1 && currentRow.PlanningAuthorityOkToProceed <= 0)
            {
                XtraMessageBox.Show("You have Tree Protected ticked within the TPO section of the selected tender but Ok To Proceed within the Planning section is not ticked.\n\n<color=red>You cannot start works until you have Planning Permissions.</color>\n\nPlease edit the tender and complete the Planning section and tick Ok to Proceed before continuing.", "Create Jobs", MessageBoxButtons.OK, MessageBoxIcon.Stop, DefaultBoolean.True);
                return;
            }

            int intSiteID = 0;
            try { intSiteID = (string.IsNullOrEmpty(currentRow.SiteID.ToString()) ? 0 : Convert.ToInt32(currentRow.SiteID)); }
            catch (Exception) { };
            if (intSiteID == 0)
            {
                XtraMessageBox.Show("Please edit the selected tender and select the Site before attempting to create Jobs.", "Create Jobs", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (XtraMessageBox.Show("You have clicked Create Jobs - This will open the Job Wizard screen to allow the creation of one or more jobs.\n\nAre you sure you wish to proceed?", "Create Jobs", MessageBoxButtons.YesNo, MessageBoxIcon.Question, DefaultBoolean.True) == DialogResult.No) return;


            string strClientContractIDs = ",";
            string strCurrentClientcontractID = "";
            string strSiteContractIDs = ",";
            string strCurrentSiteContractID = "";
            string strVisitIDs = "";

            view = (GridView)gridControlVisit.MainView;
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount > 0)
            {
                foreach (int intRowHandle in intRowHandles)
                {
                    strVisitIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "VisitID")) + ',';

                    strCurrentClientcontractID = Convert.ToString(view.GetRowCellValue(intRowHandle, "ClientContractID"));
                    if (!strClientContractIDs.Contains("," + strCurrentClientcontractID + ",")) strClientContractIDs += strCurrentClientcontractID + ",";

                    strCurrentSiteContractID = Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractID"));
                    if (!strSiteContractIDs.Contains("," + strCurrentSiteContractID + ",")) strSiteContractIDs += strCurrentSiteContractID + ",";
                }
            }
            strClientContractIDs = strClientContractIDs.TrimStart(',');
            strSiteContractIDs = strSiteContractIDs.TrimStart(',');


            // Open Job Wizard //
            var fChildForm = new frm_OM_Job_Wizard();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strRecordIDs = "";
            fChildForm.strFormMode = "add";
            fChildForm.strCaller = this.Name;
            fChildForm.intRecordCount = 0;
            fChildForm.FormPermissions = this.FormPermissions;
            fChildForm.i_str_PassedInClientContractIDs = strClientContractIDs;
            fChildForm.i_str_PassedInSiteContractIDs = strSiteContractIDs;
            fChildForm.i_str_PassedInVisitIDs = strVisitIDs;
            fChildForm.i_intPassedInTenderID = currentRow.TenderID;
            fChildForm.i_intPassedInJobSubTypeID = currentRow.WorkSubTypeID;
            fChildForm.i_intPassedInLabourTypeID = currentRow.ProposedLabourTypeID;
            fChildForm.i_intPassedInLabourID = currentRow.ProposedLabourID;
            fChildForm.i_strPassedInLabourName = currentRow.ProposedLabour;
            fChildForm.i_dbPassedInLabourLatitude = currentRow.ProposedLabourLatitude;
            fChildForm.i_dbPassedInLabourLongitude = currentRow.ProposedLabourLongitude;
            fChildForm.i_strPassedInLabourPostcode = currentRow.ProposedLabourPostcode;
            fChildForm.i_dtPassedInWorkCompletedBy = currentRow.RequiredWorkCompletedDate;

            string i_strPassedInTenderRevisionNumber = currentRow.RevisionNumber;
            string i_strPassedInClientName = currentRow.ClientName;
            string i_strPassedInClientContractDescription = currentRow.ContractDescription;
            string i_strPassedInSiteName = currentRow.SiteName;
            string i_strPassedInWorkDescription = currentRow.WorkType;
            DateTime? i_dtPassedInWorkCompletedBy = currentRow.RequiredWorkCompletedDate;

            fChildForm.i_strPassedInTenderDescription = "<b>Tender ID: </b>" + fChildForm.i_intPassedInTenderID.ToString() + (string.IsNullOrWhiteSpace(i_strPassedInTenderRevisionNumber) ? "" : " \\ " + i_strPassedInTenderRevisionNumber)
                                                        + "  <b>Client Name: </b>" + (string.IsNullOrWhiteSpace(i_strPassedInClientName) ? "" : i_strPassedInClientName)
                                                        + "  <b>Contract: </b>" + (string.IsNullOrWhiteSpace(i_strPassedInClientContractDescription) ? "" : i_strPassedInClientContractDescription)
                                                        + "  <b>Site Name: </b>" + (string.IsNullOrWhiteSpace(i_strPassedInSiteName) ? "" : i_strPassedInSiteName)
                                                        + "  <b>Work: </b>" + (string.IsNullOrWhiteSpace(i_strPassedInWorkDescription) ? "" : i_strPassedInWorkDescription)
                                                        + "  -  <b>Work Completed By: </b>" + ((i_dtPassedInWorkCompletedBy == null || i_dtPassedInWorkCompletedBy == DateTime.MinValue) ? "Not Specified" : "<color=red>" + i_dtPassedInWorkCompletedBy.ToString() + "</color>");


            fChildForm.frmCaller = null;

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager2 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager2;
            fChildForm.splashScreenManager.ShowWaitForm();
            fChildForm.Show();

            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
        }

        private void bbiTenderClosed_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlTender.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount != 1)
            {
                XtraMessageBox.Show("Select just one Tender to set as Closed.", "Set Tender as Closed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            var currentRowView = (DataRowView)sp06491OMTenderManagerBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06491_OM_Tender_ManagerRow)currentRowView.Row;
            if (currentRow == null) return;

            int intVisits = currentRow.LinkedVisitCount;
            int intJobs = currentRow.LinkedJobCount;
            if (intVisits <= 0 || intJobs <= 0)
            {
                XtraMessageBox.Show("You must create at least one visit with at least on job before the tender can be set as Tender Closed.", "Set Tender as Closed", MessageBoxButtons.OK, MessageBoxIcon.Stop, DefaultBoolean.True);
                return;
            }

            if (XtraMessageBox.Show("You have clicked Tender Closed - This will set the Tender as Tender Closed and lock the record to prevent any further changes - are you sure you wish to proceed?", "Set Tender as Closed", MessageBoxButtons.YesNo, MessageBoxIcon.Question, DefaultBoolean.True) == DialogResult.No) return;

            DateTime dtNow = DateTime.Now;
            int intTenderID = currentRow.TenderID;

            // Update the DB //
            using (var UpdateTender = new DataSet_OM_TenderTableAdapters.QueriesTableAdapter())
            {
                UpdateTender.ChangeConnectionString(strConnectionString);
                try
                {
                    UpdateTender.sp06522_OM_Tender_Manager_Update_Status(intTenderID, 90, dtNow, 0, 0, "");
                }
                catch (Exception) { }
            }
            Set_DictionaryDataRefreshes_One_Value(dictionaryDataRefreshesMain, xtraTabPageHistory.Text, true);  // Force Linked Visits to refresh //
            Load_Data();
            SetMenuStatus();

            XtraMessageBox.Show("Tender set to Closed successfully.", "Set Tender as Closed", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void bbiTenderCancelled_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlTender.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount != 1)
            {
                XtraMessageBox.Show("Select just one Tender to set as Tender Declined (Internal).", "Set Tender as Tender Declined (Internal)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (XtraMessageBox.Show("You have clicked Cancel Tender - <color=red>This will set the Tender as Tender Declined (Internal)</color> and lock the record to prevent any further changes!\n\nAre you sure you wish to proceed?\n\nIf you proceed you must select a Reason.", "Tender Declined (Internal)", MessageBoxButtons.YesNo, MessageBoxIcon.Question, DefaultBoolean.True) == DialogResult.No) return;
            
            var fChildForm = new frm_OM_Select_Status_Issue_For_Status();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strPassedInTenderStatusIDsFilter = "100,";
            fChildForm.strPassedInTenderStatusDescriptionFilter = "Tender Declined (Internal)";
            fChildForm.intOriginalTenderStatusIssueID = 100;
            if (fChildForm.ShowDialog() != DialogResult.OK) return;

            var currentRowView = (DataRowView)sp06491OMTenderManagerBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06491_OM_Tender_ManagerRow)currentRowView.Row;
            if (currentRow == null) return;
            
            DateTime dtNow = DateTime.Now;
            int intTenderID = currentRow.TenderID;
            int intStatusIssueID = fChildForm.intSelectedTenderStatusIssueID;

            // Update the DB //
            using (var UpdateTender = new DataSet_OM_TenderTableAdapters.QueriesTableAdapter())
            {
                UpdateTender.ChangeConnectionString(strConnectionString);
                try
                {
                    UpdateTender.sp06522_OM_Tender_Manager_Update_Status(intTenderID, 100, dtNow, intStatusIssueID, 0, "");
                }
                catch (Exception) { }
            }
            Set_DictionaryDataRefreshes_One_Value(dictionaryDataRefreshesMain, xtraTabPageHistory.Text, true);  // Force Linked Visits to refresh //
            Load_Data();
            SetMenuStatus();

            XtraMessageBox.Show("Tender set to Closed successfully.", "Set Tender as Closed", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }


        #endregion
        

        #region DataSet

        public override void OnDatasetCreateEvent(object sender, EventArgs e)
        {
            //int[] intRowHandles;
            //int intCount = 0;
            //GridView view = null;
            //string strCurrentID = "";
            //string strSelectedIDs = ",";
            //string strColumnName = "";
            //switch (_enmFocusedGrid)
            //{
            //    case Utils.enmFocusedGrid.Jobs:
            //        view = (GridView)gridControlVisit.MainView;
            //        strColumnName = "JobID";
            //        break;
            //    default:
            //        return;
            //}
            //intRowHandles = view.GetSelectedRows();
            //intCount = intRowHandles.Length;
            //if (intCount <= 0)
            //{
            //    DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to add to the dataset before proceeding!", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            //    return;
            //}
            //intCount = 0;
            //foreach (int intRowHandle in intRowHandles)
            //{
            //    strCurrentID = view.GetRowCellValue(intRowHandle, view.Columns[strColumnName]).ToString() + ',';
            //    if (!strSelectedIDs.Contains("," + strCurrentID))  // Put leading comma in temporarily for checking purposes //
            //    {
            //        strSelectedIDs += strCurrentID;
            //        intCount++;
            //    }
            //}
            //strSelectedIDs = strSelectedIDs.Remove(0, 1);  // Remove leading ',' //
            //CreateDataset("Job", intCount, strSelectedIDs);
        }

        private void CreateDataset(string strType, int intRecordCount, string strSelectedRecordIDs)
        {
            //frmDatasetCreate fChildForm = new frmDatasetCreate();
            //fChildForm.GlobalSettings = this.GlobalSettings;
            //fChildForm.i_str_dataset_type = strType;
            //fChildForm.i_int_selected_job_count = intRecordCount;

            //if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
            //{
            //    DevExpress.XtraEditors.XtraMessageBox.Show("No Dataset Created.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    return;
            //}
            //else
            //{
            //    int intAction = fChildForm.i_int_returned_action;
            //    string strName = fChildForm.i_str_dataset_name;
            //    strType = fChildForm.i_str_dataset_type;
            //    int intDatasetID = fChildForm.i_int_selected_dataset;
            //    string strDatasetType = fChildForm.i_str_dataset_type;
            //    int intReturnValue = 0;
            //    switch (intAction)
            //    {
            //        case 1:  // Create New dataset and dataset members //
            //            Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter AddDataset = new Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter();
            //            AddDataset.ChangeConnectionString(strConnectionString);

            //            intReturnValue = Convert.ToInt32(AddDataset.sp01224_AT_Dataset_create(strDatasetType, strName, this.GlobalSettings.UserID, strSelectedRecordIDs));
            //            if (intReturnValue <= 0)
            //            {
            //                DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while attempting to create the dataset!\n\nNo Dataset Created.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //            }
            //            return;
            //        case 2:  // Append to dataset members //
            //            Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter AppendDataset = new Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter();
            //            AppendDataset.ChangeConnectionString(strConnectionString);
            //            intReturnValue = Convert.ToInt32(AppendDataset.sp01226_AT_Dataset_append_dataset_members(intDatasetID, strSelectedRecordIDs));
            //            if (intReturnValue <= 0)
            //            {
            //                DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while attempting to append the selected records to the dataset!\n\nNo Records Append to the Dataset.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //            }
            //            return;
            //        case 3: // Replace dataset members //
            //            Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter ReplaceDataset = new Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter();
            //            ReplaceDataset.ChangeConnectionString(strConnectionString);
            //            intReturnValue = Convert.ToInt32(ReplaceDataset.sp01227_AT_Dataset_replace_dataset_members(intDatasetID, strSelectedRecordIDs));
            //            if (intReturnValue <= 0)
            //            {
            //                DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while attempting to replace the records in the selected dataset!\n\nNo Records Replaced in the Dataset.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //            }
            //            return;
            //        default:
            //            return;
            //    }
            //}
        }

        public override void OnDatasetSelectionEvent(object sender, EventArgs e)
        {
            //frmDatasetSelection fChildForm = new frmDatasetSelection();
            //Form frmMain = this.MdiParent;
            //int intSelectedDataset;
            //switch (_enmFocusedGrid)
            //{
            //    case Utils.enmFocusedGrid.Jobs:
            //        frmMain.AddOwnedForm(fChildForm);
            //        fChildForm.GlobalSettings = this.GlobalSettings;
            //        fChildForm.i_str_dataset_types = "Job,"; // Comma seperated list needs to be passed //

            //        if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
            //        {
            //            return;
            //        }
            //        intSelectedDataset = fChildForm.i_int_selected_dataset;
            //        DS_Selection1.SelectRecordsFromDataset(intSelectedDataset, "JobID");
            //        break;
            //    default:
            //        break;
            //}
        }

        public override void OnDatasetSelectionInvertedEvent(object sender, EventArgs e)
        {
            //frmDatasetSelection fChildForm = new frmDatasetSelection();
            //Form frmMain = this.MdiParent;
            //int intSelectedDataset;
            //switch (_enmFocusedGrid)
            //{
            //    case Utils.enmFocusedGrid.Jobs:
            //        frmMain.AddOwnedForm(fChildForm);
            //        fChildForm.GlobalSettings = this.GlobalSettings;
            //        fChildForm.i_str_dataset_types = "Job,"; // Comma seperated list needs to be passed //

            //        if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
            //        {
            //            return;
            //        }
            //        intSelectedDataset = fChildForm.i_int_selected_dataset;
            //        DS_Selection1.SelectRecordsFromDatasetInverted(intSelectedDataset, "JobID");
            //        break;
            //    default:
            //        break;
            //}
        }

        public override void OnDatasetManagerEvent(object sender, EventArgs e)
        {
            var fChildForm = new frm_DatasetManager();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.FormPermissions = this.FormPermissions;
            fChildForm._i_PassedInFilterTypes = "Job,";
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();
            fChildForm.Show();

            MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
        }

        #endregion


 

        private void Set_Selected_Count()
        {
            GridView view = (GridView)gridControlTender.MainView;
            int intSelectedCount = view.GetSelectedRows().Length;
            view = (GridView)gridControlVisit.MainView;
            int intSelectedCount2 = view.GetSelectedRows().Length;
            if (intSelectedCount == 0 && intSelectedCount2 == 0)
            {
                bsiSelectedCount.Visibility = BarItemVisibility.Never;
            }
            else
            {
                bsiSelectedCount.Visibility = BarItemVisibility.Always;
                string strText = "";
                if (intSelectedCount > 0)
                {
                    strText = (intSelectedCount == 1 ? "1 Tender Selected" : "<color=red>" + intSelectedCount.ToString() + " Tenders</Color> Selected");
                }
                if (intSelectedCount > 0 && intSelectedCount2 > 0) strText += "<br>";
                if (intSelectedCount2 > 0)
                {
                    strText += (intSelectedCount2 == 1 ? "1 Visit Selected" : "<color=red>" + intSelectedCount2.ToString() + " Visits</Color> Selected");
                }
                bsiSelectedCount.Caption = strText;
            }
        }

        private void splitContainerControl1_SplitGroupPanelCollapsed(object sender, SplitGroupPanelCollapsedEventArgs e)
        {
            if (!e.Collapsed)
            {
                Application.DoEvents();  // Allow Form time to repaint itself //
                LoadLinkedRecords();
            }
        }

        private void barManager1_HighlightedLinkChanged(object sender, DevExpress.XtraBars.HighlightedLinkChangedEventArgs e)
        {
            // This event is required by the ToolTipControl object [it handles firing the Tooltips on BarSubItem objects on the toolbar because they don't show Tooltips] //
            toolTipController1.HideHint();
            if (e.Link == null) return;

            BarSubItemLink link = e.PrevLink as BarSubItemLink;
            if (link != null) link.CloseMenu();

            if (e.Link.Item is BarLargeButtonItem) return;

            var Info = new ToolTipControlInfo {Object = e.Link.Item, SuperTip = e.Link.Item.SuperTip};

            toolTipController1.ShowHint(Info);
        }


        private void Linked_Document_Drill_Down(int intRecordType, int intRecordSubType, int intRecordID, string strRecordDescription)
        {
            // Drilldown to Linked Document Manager //
            string strRecordIDs = "";
            DataSet_GC_Summer_CoreTableAdapters.QueriesTableAdapter GetSetting = new DataSet_GC_Summer_CoreTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            try
            {
                strRecordIDs = GetSetting.sp06049_OM_Linked_Docs_Drilldown_Get_IDs(intRecordID, intRecordType, intRecordSubType).ToString();
            }
            catch (Exception) { }
            if (string.IsNullOrWhiteSpace(strRecordIDs)) strRecordIDs = "-1,";  // Make sure the form starts in drilldown mode [use a dummy id] //
            Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "om_linked_documents", intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        private void toolTipController1_GetActiveObjectInfo(object sender, ToolTipControllerGetActiveObjectInfoEventArgs e)
        {
            try
            {
                switch (e.SelectedControl.Name)
                {
                    case "gridControlVisit":
                        {
                            GridView view = gridControlVisit.GetViewAt(e.ControlMousePosition) as GridView;
                            if (view == null) return;
                            GridHitInfo hi = view.CalcHitInfo(e.ControlMousePosition);
                            if (hi.InRowCell && hi.Column.Name == "Alert2")
                            {
                                if (view.GetRowCellValue(hi.RowHandle, "Timeliness").ToString() == "Overdue" || Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "LinkedJobCount")) <= 0 || Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "IssueFound")) != 0 || Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "Rework")) >= 1 || Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "AccidentOnSite")) >= 1)
                                {
                                    string strMessage = "The Following Warnings are present for this Visit:";
                                    if (view.GetRowCellValue(hi.RowHandle, "Timeliness").ToString() == "Overdue") strMessage += "\n--> Visit Overdue.";
                                    if (Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "LinkedJobCount")) <= 0) strMessage += "\n--> No Linked Jobs.";
                                    //if (Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "LinkedOutstandingJobCount")) > 0) strMessage += "\n--> Outstanding Jobs.";
                                    if (Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "IssueFound")) != 0) strMessage += "\n--> Issue(s) Found.";
                                    if (Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "Rework")) > 0) strMessage += "\n--> Rework Required.";
                                    if (Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "AccidentOnSite")) > 0) strMessage += "\n--> Accident On Site.";

                                    SuperToolTip st = new SuperToolTip();
                                    SuperToolTipSetupArgs args = new SuperToolTipSetupArgs(st);
                                    args.Title.Text = "Warning Icons - Information";
                                    args.Title.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
                                    args.Contents.Text = strMessage;
                                    args.ShowFooterSeparator = false;
                                    args.Footer.Text = "";
                                    st.Setup(args);
                                    ToolTipControlInfo info = new ToolTipControlInfo(new CellToolTipInfo(hi.RowHandle, hi.Column, "cell"), args.Contents.Text);
                                    info.SuperTip = st;
                                    info.ToolTipType = ToolTipType.SuperTip;
                                    e.Info = info;
                                    return;
                                }
                                else return;  // No Tooltip //
                            }
                            if (hi.InColumnPanel && hi.Column != null && hi.Column.Name == "Alert2")
                            {
                                SuperToolTip st = new SuperToolTip();
                                SuperToolTipSetupArgs args = new SuperToolTipSetupArgs(st);
                                args.Title.Text = "Warning Icons - Information";
                                args.Title.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
                                args.Contents.Text = "This column displays warning icons for Visits when certain conditions are met. Hover over the warning icon on visits to see the warning(s).";
                                args.ShowFooterSeparator = false;
                                args.Footer.Text = "";
                                st.Setup(args);
                                ToolTipControlInfo info = new ToolTipControlInfo(new CellToolTipInfo(hi.RowHandle, hi.Column, "cell"), args.Contents.Text);
                                info.SuperTip = st;
                                info.ToolTipType = ToolTipType.SuperTip;
                                e.Info = info;
                                return;
                            }
                        }
                        return;
                    case "gridControlJob":
                        {
                            GridView view = gridControlTender.GetViewAt(e.ControlMousePosition) as GridView;
                            if (view == null) return;
                            GridHitInfo hi = view.CalcHitInfo(e.ControlMousePosition);
                            if (hi.InRowCell && hi.Column.Name == "Alert")
                            {
                                if (Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "LinkedLabourCount")) <= 0 || Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "JobStatusID")) < 20 || Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "DoNotPayContractor")) == 1 || Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "DoNotInvoiceClient")) == 1)
                                {
                                    string strMessage = "The Following Warnings are present for this Job:";
                                    if (Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "DoNotPayContractor")) == 1) strMessage += "\n--> Do Not Pay Team.";
                                    if (Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "DoNotInvoiceClient")) == 1) strMessage += "\n--> Do Not Invoice Client.";
                                    if (Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "LinkedLabourCount")) <= 0) strMessage += "\n--> Unable to Send - At least one Labour Team must be linked.";
                                    if (Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "JobStatusID")) < 20) strMessage += "\n--> Unable to Send - Job Status must be >= Job Creation Completed - Ready To Send.";

                                    SuperToolTip st = new SuperToolTip();
                                    SuperToolTipSetupArgs args = new SuperToolTipSetupArgs(st);
                                    args.Title.Text = "Warning Icons - Information";
                                    args.Title.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
                                    args.Contents.Text = strMessage;
                                    args.ShowFooterSeparator = false;
                                    args.Footer.Text = "";
                                    st.Setup(args);
                                    ToolTipControlInfo info = new ToolTipControlInfo(new CellToolTipInfo(hi.RowHandle, hi.Column, "cell"), args.Contents.Text);
                                    info.SuperTip = st;
                                    info.ToolTipType = ToolTipType.SuperTip;
                                    e.Info = info;
                                    return;
                                }
                                else return;  // No Tooltip //
                            }
                            if (hi.InColumnPanel && hi.Column != null && hi.Column.Name == "Alert")
                            {
                                SuperToolTip st = new SuperToolTip();
                                SuperToolTipSetupArgs args = new SuperToolTipSetupArgs(st);
                                args.Title.Text = "Warning Icons - Information";
                                args.Title.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
                                args.Contents.Text = "This column displays warning icons for jobs when certain conditions are met. Hover over the warning icon on jobs to see the warning(s).";
                                args.ShowFooterSeparator = false;
                                args.Footer.Text = "";
                                st.Setup(args);
                                ToolTipControlInfo info = new ToolTipControlInfo(new CellToolTipInfo(hi.RowHandle, hi.Column, "cell"), args.Contents.Text);
                                info.SuperTip = st;
                                info.ToolTipType = ToolTipType.SuperTip;
                                e.Info = info;
                                return;
                            }
                            /*if (hi.HitTest == GridHitTest.GroupPanel)
                            {
                                info = new ToolTipControlInfo(hi.HitTest, "Group panel");
                                return;
                            }*/

                            /*if (hi.HitTest == GridHitTest.RowIndicator)
                            {
                                info = new ToolTipControlInfo(GridHitTest.RowIndicator.ToString() + hi.RowHandle.ToString(), "Row Handle: " + hi.RowHandle.ToString());
                                ToolTipTitleItem titleItem1 = new ToolTipTitleItem();
                                byte[] cellIm = view.GetRowCellValue(hi.RowHandle, "Alert") as byte[];
                                Image im = null;
                                if (cellIm != null)
                                {
                                    MemoryStream ms = new MemoryStream(cellIm);

                                    im = Image.FromStream(ms);
                                }

                                ToolTipItem item1 = new ToolTipItem();
                                item1.Image = im;
                                sTooltip1.Items.Add(item1);
                            }*/
                            //info = new ToolTipControlInfo(hi.HitTest, "");
                            //info.SuperTip = superToolTipSiteContractFilter;
                        }
                        return;
                    default:
                        break;
                }
            }
            catch (Exception) { }
        }

        private void hideContainerLeft_Click(object sender, EventArgs e)
        {

        }

        private void btnClearAllFilters_Click(object sender, EventArgs e)
        {
            i_str_selected_client_ids = "";
            i_str_selected_client_names = "";
            buttonEditClientFilter.Text = "";
            superToolTipSetupArgsClientContractFilter.Contents.Text = "I store the currently applied Client Filter.\n\nNo Filter Set.";
           
            i_str_selected_site_ids = "";
            i_str_selected_site_names = "";
            buttonEditSiteFilter.Text = "";
            superToolTipSetupArgsSiteContractFilter.Contents.Text = "I store the currently applied Site Filter.\n\nNo Filter Set.";

            selection1.ClearSelection();
            popupContainerEditVisitSystemStatusFilter.Text = PopupContainerEditVisitSystemStatusFilter_Get_Selected();

            selection2.ClearSelection();
            popupContainerEditKAMFilter.Text = PopupContainerEditKAMFilter_Get_Selected();

            selection3.ClearSelection();
            popupContainerEditCMFilter.Text = PopupContainerEditCMFilter_Get_Selected();

            selection4.ClearSelection();
            popupContainerEditLabourFilter.Text = PopupContainerEditLabourFilter_Get_Selected();

            selection5.ClearSelection();
            popupContainerEditJobTypeFilter.Text = PopupContainerEditJobTypeFilter_Get_Selected();

            selection6.ClearSelection();
            popupContainerEditJobSubTypeFilter.Text = PopupContainerEditJobSubTypeFilter_Get_Selected();

            TenderIDsMemoEdit.EditValue = "";
        }

        private void bciFilterVisitsSelected_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlVisit.MainView;
            if (bciFilterVisitsSelected.Checked)  // Filter Selected rows //
            {
                try
                {
                    int[] intRowHandles = view.GetSelectedRows();
                    int intCount = intRowHandles.Length;
                    DataRow dr = null;
                    if (intCount <= 0)
                    {
                        XtraMessageBox.Show("Select one or more Visit records to filter by before proceeding.", "Filter Selected Visit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    gridControlVisit.BeginUpdate();
                    foreach (int intRowHandle in intRowHandles)
                    {
                        dr = view.GetDataRow(intRowHandle);
                        if (dr != null) dr["Selected"] = 1;
                    }
                }
                catch (Exception) { }
                view.ActiveFilter.Clear();
                view.ActiveFilter.NonColumnFilter = "[Selected] = 1";
                gridControlVisit.EndUpdate();
            }
            else  // Clear Filter //
            {
                gridControlVisit.BeginUpdate();
                try
                {
                    view.ActiveFilter.Clear();
                    foreach (DataRow dr in dataSet_OM_Tender.sp06495_OM_Visits_Linked_to_Tender.Rows)
                    {
                        if (Convert.ToInt32(dr["Selected"]) == 1) dr["Selected"] = 0;
                    }
                }
                catch (Exception) { }
            }
            gridControlVisit.EndUpdate();
        }

        private void bciFilterTenderSelected_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlTender.MainView;
            if (bciFilterTenderSelected.Checked)  // Filter Selected rows //
            {
                try
                {
                    int[] intRowHandles = view.GetSelectedRows();
                    int intCount = intRowHandles.Length;
                    DataRow dr = null;
                    if (intCount <= 0)
                    {
                        XtraMessageBox.Show("Select one or more Tender records to filter by before proceeding.", "Filter Selected Tender Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    gridControlTender.BeginUpdate();
                    foreach (int intRowHandle in intRowHandles)
                    {
                        dr = view.GetDataRow(intRowHandle);
                        if (dr != null) dr["Selected"] = 1;
                    }
                }
                catch (Exception) { }
                view.ActiveFilter.Clear();
                view.ActiveFilter.NonColumnFilter = "[Selected] = 1";
                gridControlTender.EndUpdate();
            }
            else  // Clear Filter //
            {
                gridControlTender.BeginUpdate();
                try
                {
                    view.ActiveFilter.Clear();
                    foreach (DataRow dr in dataSet_OM_Tender.sp06491_OM_Tender_Manager.Rows)
                    {
                        if (Convert.ToInt32(dr["Selected"]) == 1) dr["Selected"] = 0;
                    }
                }
                catch (Exception) { }
            }
            gridControlTender.EndUpdate();

        }

 

        private void hideContainerLeft_Click_1(object sender, EventArgs e)
        {

        }

        private void checkEditDateRange_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                popupContainerEditDateRange.Properties.PopupControl = popupContainerControlDateRange;
                popupContainerEditDateRange.Text = PopupContainerEditDateRange_Get_Selected();
            }
        }

        private void checkEditDatesFromToday_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                popupContainerEditDateRange.Properties.PopupControl = popupContainerControlDateRangeFromToday;
                popupContainerEditDateRange.Text = PopupContainerEditDateRange_Get_Selected();
            }
        }

        private void xtraTabControl2_SelectedPageChanged(object sender, TabPageChangedEventArgs e)
        {
            // Delayed loading of data on pages //
            try
            {
                string strText = e.Page.Text;
                if (dictionaryDataRefreshesMain.ContainsKey(strText))
                {
                    bool boolRefreshRequired = false;
                    dictionaryDataRefreshesMain.TryGetValue(strText, out boolRefreshRequired);
                    if (boolRefreshRequired) LoadLinkedRecords();
                }
            }
            catch (Exception) { }
        }

        private void Open_Picture_Viewer(string VisitIDs, int ClientID)
        {
            var fChildForm = new frm_OM_Picture_Viewer();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm._PassedInRecordIDs = VisitIDs;
            fChildForm._PassedInRecordTypeID = 1;  // Jobs for Visit //
            fChildForm._ClientID = ClientID;
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();
            fChildForm.Show();
            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
            return;
        }

        private void View_On_Map(int[] intRowHandles, string strCentrePointType)
        {
            GridView view = (GridView)gridControlTender.MainView;
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more Tenders to show their Site on the map before proceeding.", "View Tender Sites on Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strSelectedIDs = "";
            string strClientIDs = ",";
            string strSiteIDs = ",";
            string strTempClientID = "";
            string strTempSiteID = "";
            string strSelectedFullSiteDescriptions = "";
            foreach (int intRowHandle in intRowHandles)
            {
                //strSelectedIDs += view.GetRowCellValue(intRowHandle, view.Columns["VisitID"]).ToString() + ',';
                strTempClientID = view.GetRowCellValue(intRowHandle, view.Columns["ClientID"]).ToString();
                strTempSiteID = view.GetRowCellValue(intRowHandle, view.Columns["SiteID"]).ToString();
                if (!strClientIDs.Contains("," + strTempClientID + ',')) strClientIDs += strTempClientID + ",";
                if (!strSiteIDs.Contains("," + strTempSiteID + ','))
                {
                    strSiteIDs += strTempSiteID + ",";
                    strSelectedFullSiteDescriptions += "Client: " + Convert.ToString(view.GetRowCellValue(intRowHandle, "ClientName")) + ", Site: " + Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteName")) + " | ";  // Pipe used as deimiter //
                }
            }
            if (strClientIDs.Length > 0) strClientIDs = strClientIDs.Remove(0, 1);  // Remove preceeding comma //
            if (strSiteIDs.Length > 0) strSiteIDs = strSiteIDs.Remove(0, 1);  // Remove preceeding comma //

            var fChildForm = new frm_OM_Visit_Mapping_View();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.i_str_PassedInVisitIDs = strSelectedIDs;
            fChildForm.i_str_PassedInSiteIDs = strSiteIDs;
            fChildForm.i_str_PassedInClientIDs = strClientIDs;
            fChildForm.i_str_selected_Site_descriptions = strSelectedFullSiteDescriptions;
            fChildForm.i_bool_LoadVisitsInMapOnStart = false;
            fChildForm.i_bool_LoadSitesInMapOnStart = true;
            fChildForm.i_str_CentrePointType = strCentrePointType;
            fChildForm.strFormMode = "view";
            fChildForm.FormPermissions = this.FormPermissions;
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();
            fChildForm.Show();

            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });

        }


        #region Visit Panel

        private void dockPanelVisitDetails_Expanded(object sender, DockPanelEventArgs e)
        {
            //string strText = "VisitPanel";
            //if (dictionaryDataRefreshesMain.ContainsKey(strText))
            //{
            //    bool boolRefreshRequired = false;
            //    dictionaryDataRefreshesMain.TryGetValue(strText, out boolRefreshRequired);
            //    if (boolRefreshRequired)
            //    {
            //        Load_Visit_Panel_Jobs();
            //        Load_Visit_Panel_Labour();
            //        dictionaryDataRefreshesMain[strText] = false;  // Mark this as loaded //
            //    }
            //}
        }

        private void dockPanelVisitDetails_CustomButtonChecked(object sender, DevExpress.XtraBars.Docking2010.ButtonEventArgs e)
        {
            if (e.Button.Properties.Tag.ToString() == "sync")
            {
                boolSyncTenderGridToPanel = true;
            }
        }
        private void dockPanelVisitDetails_CustomButtonUnchecked(object sender, DevExpress.XtraBars.Docking2010.ButtonEventArgs e)
        {
            if (e.Button.Properties.Tag.ToString() == "sync")
            {
                boolSyncTenderGridToPanel = false;
            }
        }

        private void LinkedPictureCountHyperLinkEdit_OpenLink(object sender, OpenLinkEventArgs e)
        {
            //GridView view = (GridView)gridControlVisit.MainView;
            //if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;

            //HyperLinkEdit hle = (HyperLinkEdit)sender;
            //if (Convert.ToInt32(hle.EditValue) <= 0) return;           

            //var fChildForm = new frm_OM_Picture_Viewer();
            //fChildForm.MdiParent = this.MdiParent;
            //fChildForm.GlobalSettings = this.GlobalSettings;
            //fChildForm._RecordIDs = view.GetRowCellValue(view.FocusedRowHandle, "VisitID").ToString() + ",";
            //fChildForm._RecordTypeID = 1;  // Visits //
            //fChildForm._ClientID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "ClientID"));
            //DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            //fChildForm.splashScreenManager = splashScreenManager1;
            //fChildForm.splashScreenManager.ShowWaitForm();
            //fChildForm.Show();
            //System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            //if (method != null) method.Invoke(fChildForm, new object[] { null });
            //return;
        }

        private void ClientSignaturePathHyperLinkEdit_OpenLink(object sender, OpenLinkEventArgs e)
        {
            //GridView view = (GridView)gridControlVisit.MainView;
            //if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            
            //HyperLinkEdit hle = (HyperLinkEdit)sender;
            //if (string.IsNullOrWhiteSpace(hle.EditValue.ToString())) return;
            //string strFirstPartOfPathWithBackslash = strSignaturePath + "\\";
            //string strClientPath = view.GetRowCellValue(view.FocusedRowHandle, "ImagesFolderOM").ToString() + "\\Signatures";
            //string strFile = hle.EditValue.ToString();
            //if (string.IsNullOrWhiteSpace(strFile))
            //{
            //    DevExpress.XtraEditors.XtraMessageBox.Show("No Client Signature Linked - unable to proceed.", "View Client Signature", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            //    return;
            //}
            //try
            //{
            //    System.Diagnostics.Process.Start(Path.Combine(strFirstPartOfPathWithBackslash + strClientPath + strFile));
            //}
            //catch
            //{
            //    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view Client Signature: " + strFile + ".\n\nThe file may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Client Signature", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            //}
        }

        private void ContractDescriptionMemoExEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            switch (e.Button.Kind)
            {
                case ButtonPredefines.Glyph:
                    {
                        if ("view".Equals(e.Button.Tag))
                        {
                            {
                                GridView view = (GridView)gridControlTender.MainView;
                                if (view.FocusedRowHandle == GridControl.InvalidRowHandle)
                                {
                                    XtraMessageBox.Show("Select a child Visit prior to viewing the parent Client Contract.", "View Client Contract", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    return;
                                }
                                int intClientContractID = Convert.ToInt32(view.GetFocusedRowCellValue("ClientContractID"));
                                if (intClientContractID <= 0)
                                {
                                    XtraMessageBox.Show("Select a Visit with a linked Client Contract before proceeding.", "View Client Contract", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    return;
                                }
                                Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, intClientContractID.ToString() + ",", "operations_client_contracts");
                            }
                        }
                        break;
                    }
            }
        }

        private void SiteAddressMemoExEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            switch (e.Button.Kind)
            {
                case ButtonPredefines.Glyph:
                    {
                        if ("view_on_map".Equals(e.Button.Tag))
                        {
                            GridView view = (GridView)gridControlTender.MainView;
                            if (view.FocusedRowHandle == GridControl.InvalidRowHandle)
                            {
                                XtraMessageBox.Show("Select a Tender to show the site on the map before proceeding.", "View Tender Site on Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                return;
                            }
                            int[] intRowHandles = new int[1];
                            intRowHandles[0] = view.FocusedRowHandle;
                            int intCount = intRowHandles.Length;
                            View_On_Map(intRowHandles, "start");
                        }
                    }
                    break;
            }
        }

        private void Load_Visit_Panel_Labour()
        {
            //bool boolSplashScreenVisibile = splashScreenManager.IsSplashFormVisible;
            //if (!boolSplashScreenVisibile)
            //{
            //    this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            //    splashScreenManager.ShowWaitForm();
            //    splashScreenManager.SetWaitFormDescription("Loading Linked Data...");
            //}

            //GridView view = (GridView)gridControlVisit.MainView;
            //gridControlVisitPanelLabour.MainView.BeginUpdate();
            //if (view.FocusedRowHandle == GridControl.InvalidRowHandle)
            //{
            //    dataSet_OM_Visit.sp06475_OM_Visit_Panel_Single_Visit_Labour.Clear();
            //}
            //else
            //{
            //    try
            //    {
            //        sp06475_OM_Visit_Panel_Single_Visit_LabourTableAdapter.Fill(dataSet_OM_Visit.sp06475_OM_Visit_Panel_Single_Visit_Labour, view.GetFocusedRowCellValue("VisitID").ToString() + ",");
            //    }
            //    catch (Exception) { }
            //}
            //gridControlVisitPanelLabour.MainView.EndUpdate();
            //if (!boolSplashScreenVisibile && splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }

        #endregion


        #region Background Worker

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            Populate_Tenders();
            if (backgroundWorker1.CancellationPending)
            {
                e.Cancel = true;
                return;
            }
            e.Result = "Success";
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            strEditedTenderIDs = "";  // Clear - it will be re-populated if we need to do a partial reload by the caller //
            UpdateRefreshStatus = 0; // Clear - it will be re-populated if we need to do a partial reload by the caller //

            if (e.Cancelled)
            {
                sdaDataMerge = null;
                dsDataMerge = null;
                SetLoadingVisibility(false);
                boolProcessRunning = false;
                return;
            }
            else if (e.Error != null)
            {
                MessageBox.Show("An Error Occurred While Attempting to Load Data.\n\nError: " + (e.Error as Exception).ToString());
            }
            else
            {
                if (intErrorValue < 0)
                {
                    sdaDataMerge = null;
                    dsDataMerge = null;
                    SetLoadingVisibility(false);
                    boolProcessRunning = false;
                    switch (intErrorValue)
                    {
                        case -2:
                            {
                                DevExpress.XtraEditors.XtraMessageBox.Show("The screen has not been able to return your data due to the volume of records in your request. Please filter your request by using the data filter or contact Technical Support for further help.", "Load Tenders", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                            break;
                        case -3:
                            {
                                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Loading Tender Data.\n\nMessage = [" + strErrorMessage + "].\n\nIf the problem persists, contact Technical Support.", "Load Tenders", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            }
                            break;
                        case -1:
                            {
                                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Loading Tender Data.\n\nMessage = [" + strErrorMessage + "].\n\nIf the problem persists, contact Technical Support.", "Load Tenders", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            }
                            break;
                        default:
                            break;
                    }
                    return;
                }
                try
                {
                    beiLoadingProgress.EditValue = "Merging Data...";
                    Application.DoEvents();   // Allow Form time to repaint itself so updated caption is made visible form line above //
                    dataSet_OM_Tender.sp06491_OM_Tender_Manager.Merge(dsDataMerge.Tables[0]);

                    sdaDataMerge = null;
                    dsDataMerge = null;

                    GridView view = (GridView)gridControlTender.MainView;
                    if (tabbedControlGroupFilterType.SelectedTabPageIndex == 1 && strPassedInDrillDownIDs != "") view.ExpandAllGroups();  // Drilldown so expand all groups //

                    // Highlight any recently added new rows //
                    if (i_str_AddedRecordIDsTender != "")
                    {
                        char[] delimiters = new char[] { ';' };
                        string[] strArray = i_str_AddedRecordIDsTender.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                        int intID = 0;
                        int intRowHandle = 0;
                        view.BeginSelection();
                        view.ClearSelection(); // Clear any current selection so just the new record is selected //
                        foreach (string strElement in strArray)
                        {
                            intID = Convert.ToInt32(strElement);
                            intRowHandle = view.LocateByValue(0, view.Columns["TenderID"], intID);
                            if (intRowHandle != GridControl.InvalidRowHandle)
                            {
                                view.SelectRow(intRowHandle);
                                view.MakeRowVisible(intRowHandle, false);
                                view.SetRowCellValue(intRowHandle, "CheckMarkSelection", 1);  // Tick the new record so it is picked up by the calendar if it is active //
                            }
                        }
                        i_str_AddedRecordIDsTender = "";
                        view.EndSelection();
                    }
                    SetLoadingVisibility(false);
                    boolProcessRunning = false;
                    LoadLinkedRecords();  // Refresh any children //
                }
                catch (Exception) { }
            }
        }

        private void bbiLoadingCancel_ItemClick(object sender, ItemClickEventArgs e)
        {
            //notify background worker we want to cancel the operation.
            //this code doesn't actually cancel or kill the thread that is executing the job.
            try
            {
                cmd.Cancel();  // Kill the SQL Command //
                cmd = null;
                backgroundWorker1.CancelAsync();  // Tell the thread it is cancelled - the line 2 above does the cancel since there is no loop in the DoWork to check for the cancelled status of the thread //
            }
            catch (Exception) { }
        }

        private void SetLoadingVisibility(Boolean boolVisible)
        {
            bbiRefresh.Visibility = (boolVisible ? BarItemVisibility.Never : BarItemVisibility.Always);
            beiLoadingProgress.Visibility = (boolVisible ? BarItemVisibility.Always : BarItemVisibility.Never);
            bbiLoadingCancel.Visibility = (boolVisible ? BarItemVisibility.Always : BarItemVisibility.Never);
            if (!boolVisible) beiLoadingProgress.EditValue = "Loading Data...";
        }














        #endregion



    }
}



﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraEditors.Controls;
using DevExpress.Skins;
using DevExpress.XtraScheduler;
using DevExpress.Utils.Animation;  // Required by Transition Effects //
using DevExpress.XtraScheduler;  // Required by Recurrence //
using DevExpress.XtraScheduler.UI;
using DevExpress.XtraScheduler.Compatibility;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;
using WoodPlan5.Classes.Operations;

namespace WoodPlan5
{
    public partial class frm_OM_Visit_Wizard : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        bool internalRowFocusing;

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;
        bool iBool_TemplateManager = false;

        bool iBool_ContractChangesMade = false;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //
        private bool ibool_FormStillLoading = true;

        public int i_intClientID = 0;

        public DateTime i_dtStart;
        public DateTime i_dtEnd;

        public string i_str_PassedInClientContractIDs = "";
        public string i_str_PassedInSiteContractIDs = "";

        private string i_str_SelectedClientContractID = "";
        private string i_str_SelectedSiteContractID = "";

        private int intDefaultVisitCategoryID = 0;

        BaseObjects.GridCheckMarksSelection selection1;
        BaseObjects.GridCheckMarksSelection selection2;
        private Utils.enmFocusedGrid _enmFocusedGrid = Utils.enmFocusedGrid.Sites;

        private bool _boolIgnoreSchedulerSelection = false;

        private RecurrenceControlBase currentRecurrenceControl;

        #endregion

        public frm_OM_Visit_Wizard()
        {
            InitializeComponent();
        }

        private void frm_OM_Visit_Wizard_Load(object sender, EventArgs e)
        {
            this.FormID = 500152;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            // Get Form Permissions (For Edit Templates Button) //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, 7008, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);  // 7008 //
            ProcessPermissionsForForm();

            Set_Grid_Highlighter_Transparent(this.Controls);

            xtraTabControl1.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;  // Hide Table Page headers so it doesn't look like a pageframe //
            xtraTabControl1.SelectedTabPage = this.xtraTabPageWelcome;

            i_dtStart = DateTime.Today.AddMonths(-6);
            i_dtEnd = DateTime.Today.AddMonths(6);
            dateEditFromDate.DateTime = i_dtStart;
            dateEditToDate.DateTime = i_dtEnd;

            dateEditApplyDate1.DateTime = DateTime.Today.AddDays(7).AddHours((double)0);
            
            // Reccurence //
            Set_Page3_Recurrence_Controls_Visible(radioGroupRecurrence.EditValue.ToString());
            currentRecurrenceControl = weeklyRecurrenceControl1;
            dateEditRecurrenceEnd.DateTime = DateTime.Today.AddYears(1);
            foreach (var control in  weeklyRecurrenceControl1.Controls)  // Set Defaults //
	        {
                if (control.GetType().ToString() == "DevExpress.XtraScheduler.UI.WeekDaysCheckEdit")
                {
                    DevExpress.XtraScheduler.UI.WeekDaysCheckEdit ctrl = (DevExpress.XtraScheduler.UI.WeekDaysCheckEdit)control;
                    ctrl.WeekDays = DevExpress.XtraScheduler.WeekDays.WorkDays;
                }
	        }

            try
            {
                DataSet_OM_VisitTableAdapters.QueriesTableAdapter GetDefaultValue = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter();
                GetDefaultValue.ChangeConnectionString(strConnectionString);
                intDefaultVisitCategoryID = Convert.ToInt32(GetDefaultValue.sp06356_OM_Default_Visit_Category_ID());
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Visit Category ID.\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Visit Category ID", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            // Populate Schedulers Label Colours using HSL (Hue, Saturation and Lightness) //
            schedulerStorage1.Appointments.Labels.Clear();
            int intCount = 0;
            int step = 240 / 60;  // Increment Colours in 4 not 1 as the differences are too subtle between colours with an increment of 1 //
            for (int i = 0; i < 240; i += step)  // 60 different colours generated //
            {
                intCount++;
                BaseObjects.HSLColor color = new BaseObjects.HSLColor((double)i, 240, 160);
                schedulerStorage1.Appointments.Labels.Add(color, "Visit - " + intCount.ToString(), "Visit - " + intCount.ToString());
            }

            // Make sure Schedule is not bound to data initially - only want to do this when the Calendar page is activated otherwise it slows things down too much! //
            schedulerControl1.BeginUpdate();
            schedulerStorage1.Appointments.DataSource = null;
            schedulerControl1.EndUpdate();
            schedulerControl1.Start = DateTime.Today;

            try
            {
                sp06250_OM_Visit_Categories_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06250_OM_Visit_Categories_With_BlankTableAdapter.Fill(dataSet_OM_Visit.sp06250_OM_Visit_Categories_With_Blank, 0);

                sp06142_OM_Visit_Template_Headers_SelectTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06142_OM_Visit_Template_Headers_SelectTableAdapter.Fill(dataSet_OM_Visit.sp06142_OM_Visit_Template_Headers_Select);

                sp06143_OM_Visit_Template_Items_SelectTableAdapter.Connection.ConnectionString = strConnectionString;

                sp06133_OM_Visit_Gap_Unit_DescriptorsTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06133_OM_Visit_Gap_Unit_DescriptorsTableAdapter.Fill(dataSet_OM_Visit.sp06133_OM_Visit_Gap_Unit_Descriptors, 0);

                sp06134_OM_Job_Calculation_Level_DescriptorsTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06134_OM_Job_Calculation_Level_DescriptorsTableAdapter.Fill(dataSet_OM_Visit.sp06134_OM_Job_Calculation_Level_Descriptors, 0);

                sp06308_OM_Visit_TypesTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06308_OM_Visit_TypesTableAdapter.Fill(dataSet_OM_Visit.sp06308_OM_Visit_Types, 0);

                sp06141_OM_Visit_Template_Headers_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06141_OM_Visit_Template_Headers_With_BlankTableAdapter.Fill(dataSet_OM_Visit.sp06141_OM_Visit_Template_Headers_With_Blank);
            }
            catch (Exception) { }

            Set_Page3_Controls_Visible(0);

            sp06131_OM_Visit_Wizard_Client_ContractsTableAdapter.Connection.ConnectionString = strConnectionString;
            gridControl1.ForceInitialize();
            gridControl1.ForceInitialize();
            Load_Client_Contracts();
            // Add record selection checkboxes to popup Callout Type grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
            selection1.CheckMarkColumn.Width = 30;
            selection1.CheckMarkColumn.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            selection1.CheckMarkColumn.VisibleIndex = 0;

            sp06144_OM_Visit_Wizard_Site_Contract_Start_DatesTableAdapter.Connection.ConnectionString = strConnectionString;
            gridControl4.ForceInitialize();

            sp06145_OM_Visit_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            gridControl6.ForceInitialize();

            sp06132_OM_Visit_Wizard_Site_Contracts_For_Client_ContractTableAdapter.Connection.ConnectionString = strConnectionString;
            gridControl2.ForceInitialize();
            // Add record selection checkboxes to popup Callout Type grid control //
            selection2 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl2.MainView);
            selection2.CheckMarkColumn.Width = 30;
            selection2.CheckMarkColumn.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            selection2.CheckMarkColumn.VisibleIndex = 0;

            // Set Starting Page //
            if (string.IsNullOrWhiteSpace(i_str_PassedInClientContractIDs) || string.IsNullOrWhiteSpace(i_str_PassedInSiteContractIDs))
            {
                i_str_SelectedClientContractID = "";
                barEditItemDateRange.EditValue = PopupContainerEditDateRange_Get_Selected();
                xtraTabControl1.SelectedTabPage = this.xtraTabPageWelcome;
                btnWelcomeNext.Focus();

                timerWelcomePage.Start();  // allow wizard to move from the Welcome page after 2 seconds. //
            }
            else  // Passed in Client Contract ID and Site Contract ID //
            {
                GridView view = (GridView)gridControl1.MainView;
                i_str_SelectedClientContractID = "";
                barEditItemDateRange.EditValue = PopupContainerEditDateRange_Get_Selected();
                xtraTabControl1.SelectedTabPage = this.xtraTabPageWelcome;
                btnWelcomeNext.Focus();

                int intValueToFind = 0;
                int intFoundRow = 0;
                char[] delimiters = new char[] { ',' };
                string[] strArray = i_str_PassedInClientContractIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                foreach (string strID in strArray)
                {
                    intValueToFind = Convert.ToInt32(strID);
                    intFoundRow = view.LocateByValue(0, view.Columns["ClientContractID"], intValueToFind);
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                        view.MakeRowVisible(intFoundRow, false);
                        barEditItemDateRange.EditValue = "Custom Filter";
                        i_str_SelectedClientContractID += strID + ",";
                    }
                }
                Set_Selected_Client_Count_Label();
                if (!string.IsNullOrWhiteSpace(i_str_SelectedClientContractID))  // Clients Found //
                {
                    MoveToPage2();
                    gridControl2.Focus();
                    selection2.ClearSelection();  // Clear any selected records so we just selected the passed in Site Contract IDs //

                    view = (GridView)gridControl2.MainView;
                    i_str_SelectedSiteContractID = "";

                    intValueToFind = 0;
                    intFoundRow = 0;
                    strArray = i_str_PassedInSiteContractIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string strID in strArray)
                    {
                        intValueToFind = Convert.ToInt32(strID);
                        intFoundRow = view.LocateByValue(0, view.Columns["SiteContractID"], intValueToFind);
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            view.MakeRowVisible(intFoundRow, false);
                            barEditItemDateRange.EditValue = "Custom Filter";
                            i_str_SelectedSiteContractID += strID + ",";
                        }
                    }
                    Set_Selected_Site_Count_Label();

                    if (!string.IsNullOrWhiteSpace(i_str_SelectedSiteContractID))  // Sites Found //
                    {
                        MoveToPage3();
                        radioGroupNumberOfVisitsMethod.Focus();
                    }
                }
            }
            emptyEditor = new RepositoryItem();
            ibool_FormStillLoading = false;
        }
        private void timerWelcomePage_Tick(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep1;
            timerWelcomePage.Stop();
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            ConfigureFormAccordingToMode();
            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            switch (strFormMode)
            {
                case "add":
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    break;
                default:
                    break;
            }
            ibool_FormStillLoading = false;

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(xtraTabPageStep2.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //
        }


        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        iBool_TemplateManager = sfpPermissions.blRead;
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            GridView view = null;
            int[] intRowHandles;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Visit:
                    view = (GridView)gridControl6.MainView;
                    intRowHandles = view.GetSelectedRows();
                     if (intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                   if (intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    break;
                default:
                    break;
            }
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            view = (GridView)gridControl4.MainView;
            intRowHandles = view.GetSelectedRows();
            simpleButtonApplyDate1.Enabled = (strFormMode != "view" && intRowHandles.Length > 0);

            view = (GridView)gridControl6.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl6.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (strFormMode != "view" && intRowHandles.Length > 0);
            btnApplyVisitDaysSeparation.Enabled = (strFormMode != "view" && intRowHandles.Length > 0);
            btnCalculateVisitDaysSeparation.Enabled = (strFormMode != "view" && intRowHandles.Length > 0);

            VisitDurationSpinEdit.Enabled = (strFormMode != "view" && intRowHandles.Length > 0);
            VisitDescriptorComboBoxEdit.Enabled = (strFormMode != "view" && intRowHandles.Length > 0);
            VisitTimeEdit.Enabled = (strFormMode != "view" && intRowHandles.Length > 0);
            btnApplyVisitDuration.Enabled = (strFormMode != "view" && intRowHandles.Length > 0);
            string strValue = VisitDescriptorComboBoxEdit.EditValue.ToString();
            Set_Visit_Hours_Edit_Status(strValue);
        }

        private void frm_OM_Visit_Wizard_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

            }
        }

        private void frm_OM_Visit_Wizard_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
            {
                // Store last used screen settings for current user //
                //default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "CompanyFilter", i_str_selected_Company_ids);
                //default_screen_settings.SaveDefaultScreenSettings();
            }

            // Clear timer in the off chance it is still running //
            timerWelcomePage.Stop();
            timerWelcomePage = null;
        }


        private void xtraTabControl1_SelectedPageChanging(object sender, DevExpress.XtraTab.TabPageChangingEventArgs e)
        {
            // Remember to set each pages Tag property to the page number in the designer //
            if (Convert.ToInt32(e.Page.Tag) > Convert.ToInt32(e.PrevPage.Tag))
            {
                ((PushTransition)transitionManager1.Transitions[xtraTabControl1].TransitionType).Parameters.EffectOptions = DevExpress.Utils.Animation.PushEffectOptions.FromRight;
            }
            else
            {
                ((PushTransition)transitionManager1.Transitions[xtraTabControl1].TransitionType).Parameters.EffectOptions = DevExpress.Utils.Animation.PushEffectOptions.FromLeft;
            }
            transitionManager1.StartTransition(xtraTabControl1);
        }
        private void xtraTabControl1_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            // Remember to set each pages Tag property to the page number in the designer //
            transitionManager1.EndTransition();
        }

        public override void PostViewClick(object sender, EventArgs e, int row)
        {
            GridView view = (GridView)sender;
            switch (view.GridControl.MainView.Name)
            {
                case "gridView1":
                    {
                        if (row == GridControl.InvalidRowHandle) return;
                        Set_Selected_Client_Count_Label();
                    }
                    break;
                case "gridView2":
                    {
                        if (row == GridControl.InvalidRowHandle) return;
                        Set_Selected_Site_Count_Label();
                    }
                    break;
                default:
                    break;
            }
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Client Contracts Available - click the Refresh button";
                    break;
                case "gridView2":
                    message = "No Site Contracts Available - select one or more Client Contracts from Step 1";
                    break;
                case "gridView3":
                    message = "No Client Sites Available - tick one or more sites in Step 2 before proceeding";
                    break;
                case "gridView18":
                    message = "No Linked Visit Template Items Available - select a Visit Template Header to view linked Visit Template Items";
                    break;
                case "gridView4":
                    message = "No Sites Available - tick one or more sites in Step 2 before proceeding";
                    break;
                case "gridView6":
                    message = "No Visits Available - Select Site Contracts in Step 2 of the Wizard";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_FocusedRowChanged_NoGroupSelection(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView3":
                    Load_Template_Items();
                    break;
                default:
                    break;
            }
        }

        public void GridView_MouseDown_NoGroupSelection(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        public void GridView_MouseDown_Standard(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_Standard(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            SetMenuStatus();
        }

        #endregion


        #region Welcome Page

        private void btnWelcomeNext_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep1;
        }

        #endregion


        #region Step 1 Page

        private void btnStep1Previous_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageWelcome;
        }
        private void btnStep1Next_Click(object sender, EventArgs e)
        {
            MoveToPage2();
        }
        private void MoveToPage2()
        {
            GridView view = (GridView)gridControl1.MainView;
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //         

            int[] intRowHandles = view.GetSelectedRows();
            int intCount = selection1.SelectedCount;
            int intProcessedCount = 0;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select at least one Client Contract to create the Visits for before proceeding.", "Create Visit(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "CheckMarkSelection")) == 1)
                {
                    sb.Append(view.GetRowCellValue(i, "ClientContractID").ToString() + ",");
                    intProcessedCount++;
                    if (intProcessedCount >= intCount) break;
                }
            }
            string strSelectedClientContractIDs = sb.ToString();
            if (strSelectedClientContractIDs != i_str_SelectedClientContractID || ibool_FormStillLoading)
            {
                i_str_SelectedClientContractID = strSelectedClientContractIDs;
                if (string.IsNullOrWhiteSpace(i_str_SelectedClientContractID)) return;
                try
                {
                    sp06132_OM_Visit_Wizard_Site_Contracts_For_Client_ContractTableAdapter.Fill(dataSet_OM_Visit.sp06132_OM_Visit_Wizard_Site_Contracts_For_Client_Contract, i_str_SelectedClientContractID, 0);
                }
                catch (Exception) { }
                selection2.SelectAll();
                Set_Selected_Site_Count_Label();
            }
            view = (GridView)gridControl2.MainView;
            view.ExpandAllGroups();
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep2;
        }

        private void bbiRefreshClientContracts_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            i_str_PassedInClientContractIDs = "";  // Clear any passed in Contract IDs //
            Load_Client_Contracts();
        }

        private void Load_Client_Contracts()
        {
            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();

            int intActive = Convert.ToInt32(barEditItemActive.EditValue);
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();

            DateTime dtStartDate = new DateTime(2000, 1, 1);
            DateTime dtEndDate = new DateTime(2500, 1, 1);
            if (string.IsNullOrWhiteSpace(i_str_PassedInClientContractIDs))
            {
                dtStartDate = i_dtStart;
                dtEndDate = i_dtEnd;
            }

            try
            {
                sp06131_OM_Visit_Wizard_Client_ContractsTableAdapter.Fill(dataSet_OM_Visit.sp06131_OM_Visit_Wizard_Client_Contracts, i_str_PassedInClientContractIDs, dtStartDate, dtEndDate, intActive);
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Loading Client Contracts.\n\nMessage = [" + ex.Message + "].\n\nPlease close the screen then try again. If the problem persists, contact Technical Support.", "Load Client Contracts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            view.EndUpdate();
            if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
        }

        private void Set_Selected_Client_Count_Label()
        {
            labelControlSelectedClientCount.Text = "       " + selection1.SelectedCount.ToString() + " Selected Client Contract(s)";
        }


        #region GridView1

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("reload".Equals(e.Button.Tag))
                    {
                        Load_Client_Contracts();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            btnStep1Next.PerformClick();
        }
 
        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Contract;
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Contract;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                int[] intRowHandles = view.GetSelectedRows();
                int intCount = intRowHandles.Length;
                bsiRecordTicking.Enabled = (intCount > 0);
                bbiTick.Enabled = (intCount > 0);
                bbiUntick.Enabled = (intCount > 0);

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region Date Range Filter Panel

        private void btnDateRangeOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void repositoryItemPopupContainerEditDateRange_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditDateRange_Get_Selected();
        }

        private string PopupContainerEditDateRange_Get_Selected()
        {
            string strValue = "";
            if (dateEditFromDate.DateTime == DateTime.MinValue)
            {
                i_dtStart = new DateTime(1900, 1, 1);
            }
            else
            {
                i_dtStart = dateEditFromDate.DateTime;
            }
            if (dateEditToDate.DateTime == DateTime.MinValue)
            {
                i_dtEnd = new DateTime(1900, 1, 1);
            }
            else
            {
                i_dtEnd = dateEditToDate.DateTime;
            }
            if (dateEditFromDate.DateTime == DateTime.MinValue && dateEditToDate.DateTime == DateTime.MinValue)
            {
                strValue = "No Date Range Filter";
            }
            else
            {
                strValue = (i_dtStart == new DateTime(1900, 1, 1) ? "No Start" : i_dtStart.ToString("dd/MM/yyyy HH:mm")) + " - " + (i_dtEnd == new DateTime(1900, 1, 1) ? "No End" : i_dtEnd.ToString("dd/MM/yyyy HH:mm"));
            }
            return strValue;
        }

        #endregion

        #endregion


        #region Step 2 Page

        private void btnStep2Previous_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep1;
        }
        private void btnStep2Next_Click(object sender, EventArgs e)
        {
            MoveToPage3();
        }
        private void MoveToPage3()
        {
            GridView view = (GridView)gridControl2.MainView;
            view.PostEditor();
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //  

            int intCount = selection2.SelectedCount;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select at least one Site Contract to create the Visits for before proceeding.", "Create Visit(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            SelectedSiteContractCountTextEdit.EditValue = selection2.SelectedCount;

            bool boolAllowDefaultScheduleTemplate = true;  // Var to hold if we are allowed to use the "Use Default Schedule Template" option //
            int intProcessedCount = 0;
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "CheckMarkSelection")) == 1)
                {
                    sb.Append(view.GetRowCellValue(i, "SiteContractID").ToString() + ",");
                    if (boolAllowDefaultScheduleTemplate && Convert.ToInt32(view.GetRowCellValue(i, "DefaultScheduleTemplateID")) <= 0) boolAllowDefaultScheduleTemplate = false;
                    intProcessedCount++;
                    if (intProcessedCount >= intCount) break;
                }
            }
            string strSelectedSiteContractIDs = sb.ToString();
            // Enable \ Disable "Use Default Schedule Template" option //
            labelControlDefaultScheduleTemplateUnavailable.Visible = !boolAllowDefaultScheduleTemplate;
            radioGroupNumberOfVisitsMethod.Properties.Items[0].Enabled = boolAllowDefaultScheduleTemplate;
            if (!boolAllowDefaultScheduleTemplate && Convert.ToInt32(radioGroupNumberOfVisitsMethod.EditValue) == 0) radioGroupNumberOfVisitsMethod.EditValue = 2;

            if (strSelectedSiteContractIDs != i_str_SelectedSiteContractID || ibool_FormStillLoading)  // Load Site Contract Visit Start Dates Grid //
            {
                i_str_SelectedSiteContractID = strSelectedSiteContractIDs;
                if (string.IsNullOrWhiteSpace(i_str_SelectedSiteContractID)) return;
                view = (GridView)gridControl4.MainView;
                view.BeginUpdate();
                try
                {
                    sp06144_OM_Visit_Wizard_Site_Contract_Start_DatesTableAdapter.Fill(dataSet_OM_Visit.sp06144_OM_Visit_Wizard_Site_Contract_Start_Dates, i_str_SelectedSiteContractID, dateEditApplyDate1.DateTime);
                }
                catch (Exception) { }
                view.ExpandAllGroups();
                view.EndUpdate();
            }
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep3;
        }

        private void Set_Selected_Site_Count_Label()
        {
            labelControlSelectedSiteCount.Text = "       " + selection2.SelectedCount.ToString() + " Selected Site Contract(s)";
        }


        #region GridView2

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Sites;
            SetMenuStatus();
        }

        private void gridView2_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Sites;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                int[] intRowHandles = view.GetSelectedRows();
                int intCount = intRowHandles.Length;
                bsiRecordTicking.Enabled = (intCount > 0);
                bbiTick.Enabled = (intCount > 0);
                bbiUntick.Enabled = (intCount > 0);

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion

        #endregion


        #region Step 3 Page

        private void btnStep3Previous_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep2;
        }
        private void btnStep3Next_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl3.MainView;
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //  
            
            GridView view2 = (GridView)gridControl18.MainView;
            if (!ReferenceEquals(view2.ActiveFilter.Criteria, null)) view2.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view2.FindFilterText)) view2.ApplyFindFilter("");  // Clear any Find in place //  

            RadioGroup rg = (RadioGroup)radioGroupNumberOfVisitsMethod;
            if (Convert.ToInt32(rg.EditValue) == 1)  // No Template //
            {
                if (Convert.ToInt32(spinEditNumberOfVisits.EditValue) <= 0)
                {
                    XtraMessageBox.Show("Enter the number of Visits to create for the selected Site Contracts before before proceeding.", "Create Visit(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            else if (Convert.ToInt32(rg.EditValue) == 2)  // Template Selected //
            {
                if (view.FocusedRowHandle == GridControl.InvalidRowHandle)
                {
                    XtraMessageBox.Show("Select a Visit Template Header before before proceeding.", "Create Visit(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                if (view2.DataRowCount <= 0)
                {
                    XtraMessageBox.Show("Select a Visit Template Header which has at least one linked Vist Item before before proceeding.", "Create Visit(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            else if (Convert.ToInt32(rg.EditValue) == 3)  // Recurrence Selected //
            {
                // Check Recurrence stuff is valid //
                ValidationArgs args = new ValidationArgs();
                currentRecurrenceControl.ValidateValues(args);
                if (args.Valid)
                {
                    args = new ValidationArgs();
                    currentRecurrenceControl.CheckForWarnings(args);
                    if (!args.Valid)
                    {
                        MessageBox.Show(this, args.ErrorMessage, "Create Visit(s)", MessageBoxButtons.OKCancel, MessageBoxIcon.Stop);
                        if (args.Control != null) ((Control)args.Control).Focus();
                        return;
                    }
                }
                else
                {
                    MessageBox.Show(this, args.ErrorMessage, "Create Visit(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    if (args.Control != null) ((Control)args.Control).Focus();
                    return;
                }

            }
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep4;
        }


        #region GridView3

        private void gridControl3_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("template_manager".Equals(e.Button.Tag))
                    {
                        var fChildForm = new frm_OM_Visit_Template_Manager();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();
                        System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        gridControl3.MainView.BeginUpdate();
                        try
                        {
                            sp06142_OM_Visit_Template_Headers_SelectTableAdapter.Fill(dataSet_OM_Visit.sp06142_OM_Visit_Template_Headers_Select);
                        }
                        catch (Exception) { }
                        gridControl3.MainView.EndUpdate();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.NONE;
            SetMenuStatus();
        }

        private void gridView3_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.NONE;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiRecordTicking.Enabled = false;
                bbiTick.Enabled = false;
                bbiUntick.Enabled = false;
                
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView18

        private void gridControl18_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("reload".Equals(e.Button.Tag))
                    {
                        Load_Template_Items();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView18_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.NONE;
        }

        private void gridView18_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.NONE;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiRecordTicking.Enabled = false;
                bbiTick.Enabled = false;
                bbiUntick.Enabled = false;
                
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        private void radioGroupNumberOfVisitsMethod_EditValueChanged(object sender, EventArgs e)
        {
            RadioGroup rg = (RadioGroup)sender;
            Set_Page3_Controls_Visible(Convert.ToInt32(rg.EditValue));
        }
        private void Set_Page3_Controls_Visible(int Status)
        {
            labelControlNumberOfVisits.Visible = (Status == 1);
            spinEditNumberOfVisits.Visible = (Status == 1);
            spinEditNumberOfVisits.Enabled = (Status == 1);
            labelControlGapBetweenVisits.Visible = (Status == 1);
            spinEditGapBetweenVisitsUnits.Visible = (Status == 1);
            spinEditGapBetweenVisitsUnits.Enabled = (Status == 1);
            gridLookUpEditGapBetweenVisitsDescriptor.Visible = (Status == 1);
            gridLookUpEditGapBetweenVisitsDescriptor.Enabled = (Status == 1);
            labelControlCostCalculationLevel.Visible = (Status == 1);
            gridLookUpEditCostCalculationLevel.Visible = (Status == 1);
            gridLookUpEditCostCalculationLevel.Enabled = (Status == 1);
            labelControlSellCalculationLevel.Visible = (Status == 1);
            gridLookUpEditSellCalculationLevel.Visible = (Status == 1);
            gridLookUpEditSellCalculationLevel.Enabled = (Status == 1);

            gridControl3.Visible = (Status == 2);
            gridControl3.Enabled = (Status == 2);
            gridControl18.Visible = (Status == 2);
            gridControl18.Enabled = (Status == 2);
            splitContainerControl7.Visible = (Status == 2);
            splitContainerControl7.Enabled = (Status == 2);

            radioGroupRecurrence.Visible = (Status == 3);
            radioGroupRecurrence.Enabled = (Status == 3);
            dailyRecurrenceControl1.Visible = (Status == 3);
            dailyRecurrenceControl1.Enabled = (Status == 3);
            weeklyRecurrenceControl1.Visible = (Status == 3);
            weeklyRecurrenceControl1.Enabled = (Status == 3);
            monthlyRecurrenceControl1.Visible = (Status == 3);
            monthlyRecurrenceControl1.Enabled = (Status == 3);
            yearlyRecurrenceControl1.Visible = (Status == 3);
            yearlyRecurrenceControl1.Enabled = (Status == 3);

            Set_Page3_Recurrence_Controls_Visible(radioGroupRecurrence.EditValue.ToString());
            radioGroupEndType.Visible = (Status == 3);
            radioGroupEndType.Enabled = (Status == 3);
            Set_Page3_Recurrence_End_Controls_Visible(radioGroupEndType.EditValue.ToString());
        }

        private void Load_Template_Items()
        {
            GridView view = (GridView)gridControl3.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            StringBuilder sb = new StringBuilder();
            foreach (int intRowHandle in intRowHandles)
            {
                sb.Append(Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["TemplateHeaderID"])) + ',');
            }
            string strSelectedIDs = sb.ToString();
            gridControl18.MainView.BeginUpdate();
            if (intCount == 0)
            {
                dataSet_OM_Visit.sp06143_OM_Visit_Template_Items_Select.Clear();
            }
            else // Load users selection //
            {
                try
                {
                    sp06143_OM_Visit_Template_Items_SelectTableAdapter.Fill(dataSet_OM_Visit.sp06143_OM_Visit_Template_Items_Select, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                }
                catch (Exception) { } 
            }
            gridControl18.MainView.EndUpdate();
        }

        private void radioGroupRecurrence_EditValueChanged(object sender, EventArgs e)
        {
            RadioGroup rg = (RadioGroup)sender;
            Set_Page3_Recurrence_Controls_Visible(rg.EditValue.ToString());
            switch (rg.EditValue.ToString())
            {
                case "Daily":
                    {
                        currentRecurrenceControl = dailyRecurrenceControl1;
                    }
                    break;
                case "Weekly":
                    {
                        currentRecurrenceControl = weeklyRecurrenceControl1;
                    }
                    break;
                case "Monthly":
                    {
                        currentRecurrenceControl = monthlyRecurrenceControl1;
                    }
                    break;
                case "Yearly":
                    {
                        currentRecurrenceControl = yearlyRecurrenceControl1;
                    }
                    break;

                default:
                    break;
            }
        }
        private void Set_Page3_Recurrence_Controls_Visible(string strRecurrence)
        {
            dailyRecurrenceControl1.Visible = (strRecurrence == "Daily" && (int)radioGroupNumberOfVisitsMethod.EditValue == 3);
            dailyRecurrenceControl1.Enabled = (strRecurrence == "Daily" && (int)radioGroupNumberOfVisitsMethod.EditValue == 3);
            weeklyRecurrenceControl1.Visible = (strRecurrence == "Weekly" && (int)radioGroupNumberOfVisitsMethod.EditValue == 3);
            weeklyRecurrenceControl1.Enabled = (strRecurrence == "Weekly" && (int)radioGroupNumberOfVisitsMethod.EditValue == 3);
            monthlyRecurrenceControl1.Visible = (strRecurrence == "Monthly" && (int)radioGroupNumberOfVisitsMethod.EditValue == 3);
            monthlyRecurrenceControl1.Enabled = (strRecurrence == "Monthly" && (int)radioGroupNumberOfVisitsMethod.EditValue == 3);
            yearlyRecurrenceControl1.Visible = (strRecurrence == "Yearly" && (int)radioGroupNumberOfVisitsMethod.EditValue == 3);
            yearlyRecurrenceControl1.Enabled = (strRecurrence == "Yearly" && (int)radioGroupNumberOfVisitsMethod.EditValue == 3);
        }

        private void radioGroupEndType_EditValueChanged(object sender, EventArgs e)
        {
            RadioGroup rg = (RadioGroup)sender;
            Set_Page3_Recurrence_End_Controls_Visible(rg.EditValue.ToString());
        }
        private void Set_Page3_Recurrence_End_Controls_Visible(string strEndType)
        {
            if ((int)radioGroupNumberOfVisitsMethod.EditValue != 3)
            {
                spinEditOccurrenceCount.Visible = false;
                spinEditOccurrenceCount.Enabled = false;
                labelControlOccurrences.Visible = false;
                dateEditRecurrenceEnd.Visible = false;
                dateEditRecurrenceEnd.Enabled = false;
            }
            else if (strEndType == "Occurrences")
            {
                spinEditOccurrenceCount.Visible = true;
                spinEditOccurrenceCount.Enabled = true;
                labelControlOccurrences.Visible = true;
                dateEditRecurrenceEnd.Visible = true;
                dateEditRecurrenceEnd.Enabled = false;
            }
            else  // EndDate //
            {
                spinEditOccurrenceCount.Visible = true;
                spinEditOccurrenceCount.Enabled = false;
                labelControlOccurrences.Visible = true;
                dateEditRecurrenceEnd.Visible = true;
                dateEditRecurrenceEnd.Enabled = true;
            }
        }

        #endregion


        #region Step 4 Page
        
        private void btnStep4Previous_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep3;
        }
        private void btnStep4Next_Click(object sender, EventArgs e)
        {
            Clear_Visits();  // Clear any visits already created //

            GridView viewSource = (GridView)gridControl4.MainView;
            viewSource.PostEditor();
            if (!ReferenceEquals(viewSource.ActiveFilter.Criteria, null)) viewSource.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(viewSource.FindFilterText)) viewSource.ApplyFindFilter("");  // Clear any Find in place //  

            GridView viewNew = (GridView)gridControl6.MainView;
            int intCount = viewNew.DataRowCount;
            if (intCount <= 0)
            {
                int intClientContractID = 0;
                int intSiteContractID = 0;
                string strClientName = "";
                string strContractDescription = "";
                string strSiteName = "";
                int intCreatedByID = GlobalSettings.UserID;
                string strCreatedByName = GlobalSettings.UserSurname + ": " + GlobalSettings.UserForename;
                DateTime dtStartDate = DateTime.MinValue;
                DateTime dtCalculatedDate = DateTime.MinValue;
                int intVisitNumber = 0;
                int intOnHold = 0;
                DateTime dtOnHoldStartDate = DateTime.MinValue;
                DateTime dtOnHoldEndDate = DateTime.MinValue;
                decimal decSiteContractDaysSeparationPercentage = (decimal)0.00;

                DateTime dtRecurrenceEndDate = dateEditRecurrenceEnd.DateTime;

                if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();
                intCount = viewSource.DataRowCount;
                viewNew.BeginUpdate();
                schedulerControl1.BeginUpdate();
                try
                {
                    for (int i = 0; i < intCount; i++)
                    {
                        intClientContractID = Convert.ToInt32(viewSource.GetRowCellValue(i, "ClientContractID"));
                        intSiteContractID = Convert.ToInt32(viewSource.GetRowCellValue(i, "SiteContractID"));
                        strClientName = viewSource.GetRowCellValue(i, "ClientName").ToString();
                        strContractDescription = viewSource.GetRowCellValue(i, "ContractDescription").ToString();
                        strSiteName = viewSource.GetRowCellValue(i, "SiteName").ToString();
                        intVisitNumber = Convert.ToInt32(viewSource.GetRowCellValue(i, "VisitNumberMax"));
                        intOnHold = Convert.ToInt32(viewSource.GetRowCellValue(i, "OnHold"));
                        dtOnHoldStartDate = (string.IsNullOrEmpty(viewSource.GetRowCellValue(i, "OnHoldStartDate").ToString()) ? Convert.ToDateTime("2010-01-01") : Convert.ToDateTime(viewSource.GetRowCellValue(i, "OnHoldStartDate")));
                        dtOnHoldEndDate = (string.IsNullOrEmpty(viewSource.GetRowCellValue(i, "OnHoldEndDate").ToString()) ? Convert.ToDateTime("2500-01-01") : Convert.ToDateTime(viewSource.GetRowCellValue(i, "OnHoldEndDate")));
                        decSiteContractDaysSeparationPercentage = Convert.ToDecimal(viewSource.GetRowCellValue(i, "SiteContractDaysSeparationPercent"));

                        // Add Visits //
                        if (Convert.ToInt32(radioGroupNumberOfVisitsMethod.EditValue) == 1)  // User Specified Number of Visits
                        {
                            int intVisitCount = Convert.ToInt32(spinEditNumberOfVisits.EditValue);
                            int intGapBetweenVisitsUnits = Convert.ToInt32(spinEditGapBetweenVisitsUnits.EditValue);
                            int intGapBetweenVisitsDescriptor = Convert.ToInt32(gridLookUpEditGapBetweenVisitsDescriptor.EditValue);
                            int intCostCalculationLevel = Convert.ToInt32(gridLookUpEditCostCalculationLevel.EditValue);
                            int intSellCalculationLevel = Convert.ToInt32(gridLookUpEditSellCalculationLevel.EditValue);
                            dtStartDate = Convert.ToDateTime(viewSource.GetRowCellValue(i, "VisitStartDate"));
                            for (int j = 0; j < intVisitCount; j++)
                            {
                                switch (intGapBetweenVisitsDescriptor)
                                {
                                    case 1:  // Days //
                                        dtCalculatedDate = dtStartDate.AddDays(j * intGapBetweenVisitsUnits);
                                        break;
                                    case 2:  // Weeks //
                                        dtCalculatedDate = dtStartDate.AddDays(j * (7 * intGapBetweenVisitsUnits));
                                        break;
                                    case 3:  // Months //
                                        dtCalculatedDate = dtStartDate.AddMonths(j * intGapBetweenVisitsUnits);
                                        break;
                                    case 4:  // Years //
                                        dtCalculatedDate = dtStartDate.AddYears(j * intGapBetweenVisitsUnits);
                                        break;
                                    default:
                                        break;
                                }
                                intVisitNumber++;
                                DataRow drNewRow;
                                drNewRow = dataSet_OM_Visit.sp06145_OM_Visit_Edit.NewRow();
                                drNewRow["strMode"] = "add";
                                drNewRow["strRecordIDs"] = "";
                                drNewRow["SiteContractID"] = intSiteContractID;
                                drNewRow["VisitNumber"] = intVisitNumber;
                                drNewRow["CreatedByStaffID"] = intCreatedByID;
                                drNewRow["ExpectedStartDate"] = dtCalculatedDate;
                                drNewRow["ExpectedEndDate"] = dtCalculatedDate.AddDays(1).AddMilliseconds(-5); //.AddHours((double)1);
                                drNewRow["CostCalculationLevel"] = intCostCalculationLevel;
                                drNewRow["SellCalculationLevel"] = intSellCalculationLevel;
                                drNewRow["VisitCost"] = (decimal)0.00;
                                drNewRow["VisitSell"] = (decimal)0.00;
                                drNewRow["StartLatitude"] = (decimal)0.00;
                                drNewRow["StartLongitude"] = (decimal)0.00;
                                drNewRow["FinishLatitude"] = (decimal)0.00;
                                drNewRow["FinishLongitude"] = (decimal)0.00;
                                drNewRow["ClientContractID"] = intClientContractID;
                                drNewRow["ClientName"] = strClientName;
                                drNewRow["ContractDescription"] = strContractDescription;
                                drNewRow["SiteName"] = strSiteName;
                                drNewRow["CreatedByStaffName"] = strCreatedByName;
                                drNewRow["CalendarLabel"] = intVisitNumber.ToString() + " - " + strSiteName;
                                drNewRow["LabelColourID"] = intVisitNumber;
                                drNewRow["VisitStatusID"] = (intOnHold == 1 && ((dtCalculatedDate <= dtOnHoldEndDate) && (dtCalculatedDate.AddDays(1).AddMilliseconds(-5) >= dtOnHoldStartDate)) ? 55 : 10);  // On-Hold and Date Ranges overlap //
                                drNewRow["VisitCategoryID"] = intDefaultVisitCategoryID;
                                drNewRow["IssueFound"] = 0;
                                drNewRow["IssueTypeID"] = 0;
                                drNewRow["VisitTypeID"] = 1;
                                drNewRow["CreatedFromVisitTemplateID"] = 0;
                                drNewRow["DaysSeparation"] = 0;
                                drNewRow["SiteContractDaysSeparationPercent"] = decSiteContractDaysSeparationPercentage;

                                // Update default Client PO # /
                                int intRecordTypeID = 1;
                                int intDefaultPOID = 0;
                                string strDefaultPONumber = "";
                                SqlConnection SQlConn = new SqlConnection(strConnectionString);
                                SqlCommand cmd = null;
                                cmd = new SqlCommand("sp06328_OM_Default_Client_PO_Number_For_Record", SQlConn);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add(new SqlParameter("@RecordID", intSiteContractID));
                                cmd.Parameters.Add(new SqlParameter("@RecordTypeID", intRecordTypeID));
                                SqlDataAdapter sdaPO = new SqlDataAdapter(cmd);
                                DataSet dsPO = new DataSet("NewDataSet");
                                sdaPO.Fill(dsPO, "Table");
                                foreach (DataRow dr in dsPO.Tables[0].Rows)
                                {
                                    intDefaultPOID = Convert.ToInt32(dr["ClientPurchaseOrderID"]);
                                    strDefaultPONumber = dr["PONumber"].ToString();
                                    break;
                                }
                                if (intDefaultPOID <= 0)  // No Site Contract specific Client PO ID so try for Client contract specific //
                                {
                                    intRecordTypeID = 0;
                                    cmd = null;
                                    cmd = new SqlCommand("sp06328_OM_Default_Client_PO_Number_For_Record", SQlConn);
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.Parameters.Add(new SqlParameter("@RecordID", intClientContractID));
                                    cmd.Parameters.Add(new SqlParameter("@RecordTypeID", intRecordTypeID));
                                    sdaPO = new SqlDataAdapter(cmd);
                                    dsPO = new DataSet("NewDataSet");
                                    sdaPO.Fill(dsPO, "Table");
                                    foreach (DataRow dr in dsPO.Tables[0].Rows)
                                    {
                                        intDefaultPOID = Convert.ToInt32(dr["ClientPurchaseOrderID"]);
                                        strDefaultPONumber = dr["PONumber"].ToString();
                                        break;
                                    }
                                }
                                drNewRow["ClientPOID"] = intDefaultPOID;
                                drNewRow["ClientPONumber"] = strDefaultPONumber;

                                dataSet_OM_Visit.sp06145_OM_Visit_Edit.Rows.Add(drNewRow);
                            }
                        }
                        else if (Convert.ToInt32(radioGroupNumberOfVisitsMethod.EditValue) == 2) // Add from Selected Template //
                        {
                            GridView TemplateView = (GridView)gridControl18.MainView;
                            int intVisitCount = TemplateView.DataRowCount;
                            dtStartDate = Convert.ToDateTime(viewSource.GetRowCellValue(i, "VisitStartDate"));

                            int intTemplateID = 0;
                            GridView TemplateHeaderView = (GridView)gridControl3.MainView;
                            if (TemplateHeaderView.FocusedRowHandle != GridControl.InvalidRowHandle)
                            {
                                intTemplateID = Convert.ToInt32(TemplateHeaderView.GetRowCellValue(TemplateHeaderView.FocusedRowHandle, "TemplateHeaderID"));
                            }
                            for (int j = 0; j < intVisitCount; j++)
                            {
                                int intGapBetweenVisitsUnits = Convert.ToInt32(TemplateView.GetRowCellValue(j, "UnitsFromStart"));
                                int intGapBetweenVisitsDescriptor = Convert.ToInt32(TemplateView.GetRowCellValue(j, "UnitsFromStartDescriptorID"));
                                int intCostCalculationLevel = Convert.ToInt32(TemplateView.GetRowCellValue(j, "CostCalculationLevel"));
                                int intSellCalculationLevel = Convert.ToInt32(TemplateView.GetRowCellValue(j, "SellCalculationLevel"));
                                int intVisitCategoryID = Convert.ToInt32(TemplateView.GetRowCellValue(j, "VisitCategoryID"));
                                decimal decDurationUnits = Convert.ToDecimal(TemplateView.GetRowCellValue(j, "DurationUnits"));
                                int intDurationUnitsDescriptorID = Convert.ToInt32(TemplateView.GetRowCellValue(j, "DurationUnitsDescriptorID"));
                                switch (intGapBetweenVisitsDescriptor)
                                {
                                    case 1:  // Days //
                                        dtCalculatedDate = dtStartDate.AddDays(intGapBetweenVisitsUnits);
                                        break;
                                    case 2:  // Weeks //
                                        dtCalculatedDate = dtStartDate.AddDays(7 * intGapBetweenVisitsUnits);
                                        break;
                                    case 3:  // Months //
                                        dtCalculatedDate = dtStartDate.AddMonths(intGapBetweenVisitsUnits);
                                        break;
                                    case 4:  // Years //
                                        dtCalculatedDate = dtStartDate.AddYears(intGapBetweenVisitsUnits);
                                        break;
                                    default:
                                        break;
                                }
                                intVisitNumber++;
                                DataRow drNewRow;
                                drNewRow = dataSet_OM_Visit.sp06145_OM_Visit_Edit.NewRow();
                                drNewRow["strMode"] = "add";
                                drNewRow["strRecordIDs"] = "";
                                drNewRow["SiteContractID"] = intSiteContractID;
                                drNewRow["VisitNumber"] = intVisitNumber;
                                drNewRow["CreatedByStaffID"] = intCreatedByID;
                                drNewRow["ExpectedStartDate"] = dtCalculatedDate;

                                if (decDurationUnits <= (decimal)0.00 || intDurationUnitsDescriptorID <= 0)
                                {

                                    drNewRow["ExpectedEndDate"] = dtCalculatedDate.AddDays(1).AddMilliseconds(-5); //.AddHours((double)1);
                                }
                                else
                                {
                                    drNewRow["ExpectedEndDate"] = CalculateEndDate(dtCalculatedDate, decDurationUnits, intDurationUnitsDescriptorID).AddDays(1).AddMilliseconds(-5);
                                }

                                drNewRow["CostCalculationLevel"] = intCostCalculationLevel;
                                drNewRow["SellCalculationLevel"] = intSellCalculationLevel;
                                drNewRow["VisitCost"] = (decimal)0.00;
                                drNewRow["VisitSell"] = (decimal)0.00;
                                drNewRow["StartLatitude"] = (decimal)0.00;
                                drNewRow["StartLongitude"] = (decimal)0.00;
                                drNewRow["FinishLatitude"] = (decimal)0.00;
                                drNewRow["FinishLongitude"] = (decimal)0.00;
                                drNewRow["ClientContractID"] = intClientContractID;
                                drNewRow["ClientName"] = strClientName;
                                drNewRow["ContractDescription"] = strContractDescription;
                                drNewRow["SiteName"] = strSiteName;
                                drNewRow["CreatedByStaffName"] = strCreatedByName;
                                drNewRow["CalendarLabel"] = intVisitNumber.ToString() + " - " + strSiteName;
                                drNewRow["LabelColourID"] = intVisitNumber;
                                drNewRow["VisitStatusID"] = (intOnHold == 1 && ((dtCalculatedDate <= dtOnHoldEndDate) && (dtCalculatedDate.AddDays(1).AddMilliseconds(-5) >= dtOnHoldStartDate)) ? 55 : 10);  // On-Hold and Date Ranges overlap //
                                drNewRow["VisitCategoryID"] = intVisitCategoryID;
                                drNewRow["IssueFound"] = 0;
                                drNewRow["IssueTypeID"] = 0;
                                drNewRow["VisitTypeID"] = 1;
                                drNewRow["CreatedFromVisitTemplateID"] = intTemplateID;
                                drNewRow["DaysSeparation"] = 0;
                                drNewRow["SiteContractDaysSeparationPercent"] = decSiteContractDaysSeparationPercentage;
                                dataSet_OM_Visit.sp06145_OM_Visit_Edit.Rows.Add(drNewRow);
                            }
                        }
                        else if (Convert.ToInt32(radioGroupNumberOfVisitsMethod.EditValue) == 3) // Add from Recurrence //
                        {
                            int intCostCalculationLevel = Convert.ToInt32(gridLookUpEditCostCalculationLevel.EditValue);
                            int intSellCalculationLevel = Convert.ToInt32(gridLookUpEditSellCalculationLevel.EditValue);
                            dtStartDate = Convert.ToDateTime(viewSource.GetRowCellValue(i, "VisitStartDate"));

                            string strRecurrenceXML = CreateRecurrenceXML(dtStartDate);
                            if (string.IsNullOrWhiteSpace(strRecurrenceXML)) strRecurrenceXML = "";

                            SchedulerStorage storage = new SchedulerStorage();
                            Appointment pattern = StaticAppointmentFactory.CreateAppointment(AppointmentType.Pattern);
                            pattern.RecurrenceInfo.FromXml(strRecurrenceXML);

                            pattern.Start = pattern.RecurrenceInfo.Start;
                            pattern.End = pattern.RecurrenceInfo.End;

                            int occurrenceCount = 0;
                            if (pattern.RecurrenceInfo.Range == RecurrenceRange.NoEndDate)
                            {
                                occurrenceCount = 365;
                            }
                            else if (pattern.RecurrenceInfo.Range == RecurrenceRange.OccurrenceCount)
                            {
                                occurrenceCount = pattern.RecurrenceInfo.OccurrenceCount;
                            }

                            OccurrenceCalculator calculator = OccurrenceCalculator.CreateInstance(pattern.RecurrenceInfo);
                            DateTime recurreceChainEnd = pattern.End;
                            if (pattern.RecurrenceInfo.Range != RecurrenceRange.EndByDate) recurreceChainEnd = calculator.CalcOccurrenceStartTime(occurrenceCount - 1).Add(pattern.RecurrenceInfo.Duration);

                            AppointmentBaseCollection occurrences = calculator.CalcOccurrences(new TimeInterval(pattern.Start, recurreceChainEnd), pattern);
                            for (int j = 0; j < occurrences.Count; j++)
                            {
                                intVisitNumber++;
                                DataRow drNewRow;
                                drNewRow = dataSet_OM_Visit.sp06145_OM_Visit_Edit.NewRow();
                                drNewRow["strMode"] = "add";
                                drNewRow["strRecordIDs"] = "";
                                drNewRow["SiteContractID"] = intSiteContractID;
                                drNewRow["VisitNumber"] = intVisitNumber;
                                drNewRow["CreatedByStaffID"] = intCreatedByID;
                                drNewRow["ExpectedStartDate"] = occurrences[j].Start.ToLocalTime();
                                drNewRow["ExpectedEndDate"] = occurrences[j].Start.ToLocalTime().AddDays(1).AddMilliseconds(-5); //.AddHours((double)1);
                                drNewRow["CostCalculationLevel"] = intCostCalculationLevel;
                                drNewRow["SellCalculationLevel"] = intSellCalculationLevel;
                                drNewRow["VisitCost"] = (decimal)0.00;
                                drNewRow["VisitSell"] = (decimal)0.00;
                                drNewRow["StartLatitude"] = (decimal)0.00;
                                drNewRow["StartLongitude"] = (decimal)0.00;
                                drNewRow["FinishLatitude"] = (decimal)0.00;
                                drNewRow["FinishLongitude"] = (decimal)0.00;
                                drNewRow["ClientContractID"] = intClientContractID;
                                drNewRow["ClientName"] = strClientName;
                                drNewRow["ContractDescription"] = strContractDescription;
                                drNewRow["SiteName"] = strSiteName;
                                drNewRow["CreatedByStaffName"] = strCreatedByName;
                                drNewRow["CalendarLabel"] = intVisitNumber.ToString() + " - " + strSiteName;
                                drNewRow["LabelColourID"] = intVisitNumber;
                                drNewRow["VisitStatusID"] = (intOnHold == 1 && ((dtCalculatedDate <= dtOnHoldEndDate) && (dtCalculatedDate.AddDays(1).AddMilliseconds(-5) >= dtOnHoldStartDate)) ? 55 : 10);  // On-Hold and Date Ranges overlap //
                                drNewRow["VisitCategoryID"] = intDefaultVisitCategoryID;
                                drNewRow["IssueFound"] = 0;
                                drNewRow["IssueTypeID"] = 0;
                                drNewRow["VisitTypeID"] = 1;
                                drNewRow["CreatedFromVisitTemplateID"] = 0;
                                drNewRow["DaysSeparation"] = 0;
                                drNewRow["SiteContractDaysSeparationPercent"] = decSiteContractDaysSeparationPercentage;

                                // Update default Client PO # /
                                int intRecordTypeID = 1;
                                int intDefaultPOID = 0;
                                string strDefaultPONumber = "";
                                SqlConnection SQlConn = new SqlConnection(strConnectionString);
                                SqlCommand cmd = null;
                                cmd = new SqlCommand("sp06328_OM_Default_Client_PO_Number_For_Record", SQlConn);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add(new SqlParameter("@RecordID", intSiteContractID));
                                cmd.Parameters.Add(new SqlParameter("@RecordTypeID", intRecordTypeID));
                                SqlDataAdapter sdaPO = new SqlDataAdapter(cmd);
                                DataSet dsPO = new DataSet("NewDataSet");
                                sdaPO.Fill(dsPO, "Table");
                                foreach (DataRow dr in dsPO.Tables[0].Rows)
                                {
                                    intDefaultPOID = Convert.ToInt32(dr["ClientPurchaseOrderID"]);
                                    strDefaultPONumber = dr["PONumber"].ToString();
                                    break;
                                }
                                if (intDefaultPOID <= 0)  // No Site Contract specific Client PO ID so try for Client contract specific //
                                {
                                    intRecordTypeID = 0;
                                    cmd = null;
                                    cmd = new SqlCommand("sp06328_OM_Default_Client_PO_Number_For_Record", SQlConn);
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.Parameters.Add(new SqlParameter("@RecordID", intClientContractID));
                                    cmd.Parameters.Add(new SqlParameter("@RecordTypeID", intRecordTypeID));
                                    sdaPO = new SqlDataAdapter(cmd);
                                    dsPO = new DataSet("NewDataSet");
                                    sdaPO.Fill(dsPO, "Table");
                                    foreach (DataRow dr in dsPO.Tables[0].Rows)
                                    {
                                        intDefaultPOID = Convert.ToInt32(dr["ClientPurchaseOrderID"]);
                                        strDefaultPONumber = dr["PONumber"].ToString();
                                        break;
                                    }
                                }
                                drNewRow["ClientPOID"] = intDefaultPOID;
                                drNewRow["ClientPONumber"] = strDefaultPONumber;

                                dataSet_OM_Visit.sp06145_OM_Visit_Edit.Rows.Add(drNewRow);
                            }
                        }
                        else // Add from the parent Site Contracts Default Schedule Template //
                        {
                            int intDefaultScheduleTemplateID = 0;
                            GridView gvSelectedSiteContracts = (GridView)gridControl2.MainView;
                            int intFoundRow = gvSelectedSiteContracts.LocateByValue(0, gvSelectedSiteContracts.Columns["SiteContractID"], intSiteContractID);
                            if (intFoundRow != GridControl.InvalidRowHandle)
                            {
                                intDefaultScheduleTemplateID = Convert.ToInt32(gvSelectedSiteContracts.GetRowCellValue(intFoundRow, "DefaultScheduleTemplateID"));
                            }
                            SqlConnection SQlConn = new SqlConnection(strConnectionString);
                            SqlCommand cmd = null;
                            cmd = new SqlCommand("sp06143_OM_Visit_Template_Items_Select", SQlConn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@RecordIds", intDefaultScheduleTemplateID.ToString() + ","));
                            SqlDataAdapter sdaTemplateItems = new SqlDataAdapter(cmd);
                            DataSet dsTemplateItems = new DataSet("NewDataSet");
                            sdaTemplateItems.Fill(dsTemplateItems, "Table");

                            int intVisitCount = dsTemplateItems.Tables[0].Rows.Count;
                            dtStartDate = Convert.ToDateTime(viewSource.GetRowCellValue(i, "VisitStartDate"));
                            foreach (DataRow dr in dsTemplateItems.Tables[0].Rows)
                            {
                                int intGapBetweenVisitsUnits = Convert.ToInt32(dr["UnitsFromStart"]);
                                int intGapBetweenVisitsDescriptor = Convert.ToInt32(dr["UnitsFromStartDescriptorID"]);
                                int intCostCalculationLevel = Convert.ToInt32(dr["CostCalculationLevel"]);
                                int intSellCalculationLevel = Convert.ToInt32(dr["SellCalculationLevel"]);
                                int intVisitCategoryID = Convert.ToInt32(dr["VisitCategoryID"]);
                                decimal decDurationUnits = Convert.ToInt32(dr["DurationUnits"]);
                                int intDurationUnitsDescriptorID = Convert.ToInt32(dr["DurationUnitsDescriptorID"]);
                                switch (intGapBetweenVisitsDescriptor)
                                {
                                    case 1:  // Days //
                                        dtCalculatedDate = dtStartDate.AddDays(intGapBetweenVisitsUnits);
                                        break;
                                    case 2:  // Weeks //
                                        dtCalculatedDate = dtStartDate.AddDays(7 * intGapBetweenVisitsUnits);
                                        break;
                                    case 3:  // Months //
                                        dtCalculatedDate = dtStartDate.AddMonths(intGapBetweenVisitsUnits);
                                        break;
                                    case 4:  // Years //
                                        dtCalculatedDate = dtStartDate.AddYears(intGapBetweenVisitsUnits);
                                        break;
                                    default:
                                        break;
                                }
                                intVisitNumber++;
                                DataRow drNewRow;
                                drNewRow = dataSet_OM_Visit.sp06145_OM_Visit_Edit.NewRow();
                                drNewRow["strMode"] = "add";
                                drNewRow["strRecordIDs"] = "";
                                drNewRow["SiteContractID"] = intSiteContractID;
                                drNewRow["VisitNumber"] = intVisitNumber;
                                drNewRow["CreatedByStaffID"] = intCreatedByID;
                                drNewRow["ExpectedStartDate"] = dtCalculatedDate;

                                if (decDurationUnits <= (decimal)0.00 || intDurationUnitsDescriptorID <= 0)
                                {

                                    drNewRow["ExpectedEndDate"] = dtCalculatedDate.AddDays(1).AddMilliseconds(-5); //.AddHours((double)1);
                                }
                                else
                                {
                                    drNewRow["ExpectedEndDate"] = CalculateEndDate(dtCalculatedDate, decDurationUnits, intDurationUnitsDescriptorID);
                                }

                                drNewRow["CostCalculationLevel"] = intCostCalculationLevel;
                                drNewRow["SellCalculationLevel"] = intSellCalculationLevel;
                                drNewRow["VisitCost"] = (decimal)0.00;
                                drNewRow["VisitSell"] = (decimal)0.00;
                                drNewRow["StartLatitude"] = (decimal)0.00;
                                drNewRow["StartLongitude"] = (decimal)0.00;
                                drNewRow["FinishLatitude"] = (decimal)0.00;
                                drNewRow["FinishLongitude"] = (decimal)0.00;
                                drNewRow["ClientContractID"] = intClientContractID;
                                drNewRow["ClientName"] = strClientName;
                                drNewRow["ContractDescription"] = strContractDescription;
                                drNewRow["SiteName"] = strSiteName;
                                drNewRow["CreatedByStaffName"] = strCreatedByName;
                                drNewRow["CalendarLabel"] = intVisitNumber.ToString() + " - " + strSiteName;
                                drNewRow["LabelColourID"] = intVisitNumber;
                                drNewRow["VisitStatusID"] = (intOnHold == 1 && ((dtCalculatedDate <= dtOnHoldEndDate) && (dtCalculatedDate.AddDays(1).AddMilliseconds(-5) >= dtOnHoldStartDate)) ? 55 : 10);  // On-Hold and Date Ranges overlap //
                                drNewRow["VisitCategoryID"] = intVisitCategoryID;
                                drNewRow["IssueFound"] = 0;
                                drNewRow["IssueTypeID"] = 0;
                                drNewRow["VisitTypeID"] = 1;
                                drNewRow["CreatedFromVisitTemplateID"] = intDefaultScheduleTemplateID;
                                drNewRow["DaysSeparation"] = 0;
                                drNewRow["SiteContractDaysSeparationPercent"] = decSiteContractDaysSeparationPercentage;
                                dataSet_OM_Visit.sp06145_OM_Visit_Edit.Rows.Add(drNewRow);
                            }
                        }
                    }
                    viewNew.ExpandAllGroups();
                }
                catch (Exception) { }
                schedulerControl1.EndUpdate();
                viewNew.EndUpdate();

                // Update Visit Days Separation //
                viewNew.BeginDataUpdate();
                try
                {
                    for (int i = 0; i < viewNew.DataRowCount; i++)
                    {
                        Calculate_Min_Days_Between_Visits(i, false, null, null);
                    }
                }
                catch (Exception) { }
                viewNew.EndDataUpdate();

                if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
            }
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep5;
        }
        private DateTime CalculateEndDate(DateTime StartDate, decimal Units, int UnitDescriptorID)
        {
            DateTime EndDate = StartDate;
            if (StartDate <= DateTime.MinValue) return EndDate;
            if (Units <= (decimal)0.00 || UnitDescriptorID == 0) return EndDate;
 
            switch (UnitDescriptorID)
            {
                case 1:  // Hours //
                    {
                        EndDate = Convert.ToDateTime(StartDate).AddHours((double)Units);
                    }
                    break;
                case 2:  // Days //
                    {
                        EndDate = Convert.ToDateTime(StartDate).AddDays((double)Units);
                    }
                    break;
                case 3:  // Weeks //
                    {
                        EndDate = Convert.ToDateTime(StartDate).AddDays((double)Units * (double)7);
                    }
                    break;
                case 4:  // Months //
                    {
                        EndDate = Convert.ToDateTime(StartDate).AddMonths((int)Units);
                    }
                    break;
                case 5:  // Years //
                    {
                        EndDate = Convert.ToDateTime(StartDate).AddYears((int)Units);
                    }
                    break;
                default:
                    break;
            }
            return EndDate;
        }

        private string CreateRecurrenceXML(DateTime StartDate)
        {
            string strReturnedXML = "";
            if (radioGroupRecurrence.EditValue.ToString() == "Daily")
            {
                dailyRecurrenceControl1.RecurrenceInfo.Start = StartDate;
                if (radioGroupEndType.EditValue.ToString() == "EndDate")
                {
                    dailyRecurrenceControl1.RecurrenceInfo.Range = RecurrenceRange.EndByDate;
                    dailyRecurrenceControl1.RecurrenceInfo.End = dateEditRecurrenceEnd.DateTime;

                }
                else 
                {
                    dailyRecurrenceControl1.RecurrenceInfo.Range = RecurrenceRange.OccurrenceCount;
                    dailyRecurrenceControl1.RecurrenceInfo.OccurrenceCount = (int)spinEditOccurrenceCount.Value;
                }
                strReturnedXML = dailyRecurrenceControl1.RecurrenceInfo.ToXml();
            }
            else if (radioGroupRecurrence.EditValue.ToString() == "Weekly")
            {
                weeklyRecurrenceControl1.RecurrenceInfo.Start = StartDate;
                if (radioGroupEndType.EditValue.ToString() == "EndDate")
                {
                    weeklyRecurrenceControl1.RecurrenceInfo.Range = RecurrenceRange.EndByDate;
                    weeklyRecurrenceControl1.RecurrenceInfo.End = dateEditRecurrenceEnd.DateTime;
                }
                else
                {
                    weeklyRecurrenceControl1.RecurrenceInfo.Range = RecurrenceRange.OccurrenceCount;
                    weeklyRecurrenceControl1.RecurrenceInfo.OccurrenceCount = (int)spinEditOccurrenceCount.Value;
                }
                strReturnedXML = weeklyRecurrenceControl1.RecurrenceInfo.ToXml();
            }
            else if (radioGroupRecurrence.EditValue.ToString() == "Monthly")
            {
                monthlyRecurrenceControl1.RecurrenceInfo.Start = StartDate;
                if (radioGroupEndType.EditValue.ToString() == "EndDate")
                {
                    monthlyRecurrenceControl1.RecurrenceInfo.Range = RecurrenceRange.EndByDate;
                    monthlyRecurrenceControl1.RecurrenceInfo.End = dateEditRecurrenceEnd.DateTime;
                }
                else
                {
                    monthlyRecurrenceControl1.RecurrenceInfo.Range = RecurrenceRange.OccurrenceCount;
                    monthlyRecurrenceControl1.RecurrenceInfo.OccurrenceCount = (int)spinEditOccurrenceCount.Value;
                }
                strReturnedXML = monthlyRecurrenceControl1.RecurrenceInfo.ToXml();
            }
            else  // Yearly //
            {
                yearlyRecurrenceControl1.RecurrenceInfo.Start = StartDate;
                if (radioGroupEndType.EditValue.ToString() == "EndDate")
                {
                    yearlyRecurrenceControl1.RecurrenceInfo.Range = RecurrenceRange.EndByDate;
                    yearlyRecurrenceControl1.RecurrenceInfo.End = dateEditRecurrenceEnd.DateTime;
                }
                else
                {
                    yearlyRecurrenceControl1.RecurrenceInfo.Range = RecurrenceRange.OccurrenceCount;
                    yearlyRecurrenceControl1.RecurrenceInfo.OccurrenceCount = (int)spinEditOccurrenceCount.Value;
                }
                strReturnedXML = yearlyRecurrenceControl1.RecurrenceInfo.ToXml();
            }
            return strReturnedXML;
        }

        private void Clear_Visits()
        {
            // Clear any generated Visits //
            GridView view = (GridView)gridControl6.MainView;
            if (view.DataRowCount > 0)
            {
                view.BeginUpdate();
                dataSet_OM_Visit.sp06145_OM_Visit_Edit.Rows.Clear();
                view.EndUpdate();
            }
        }

        private void simpleButtonApplyDate1_Click(object sender, EventArgs e)
        {
            int intRecordCount = 0;
            GridView view = (GridView)gridControl4.MainView;
            view.PostEditor();
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            intRecordCount = intRowHandles.Length;
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Visits to block edit then try again.", "Block Edit Visit Start Dates", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            DateTime dt = dateEditApplyDate1.DateTime;
            view.BeginDataUpdate();
            try
            {
                foreach (int intRowHandle in intRowHandles)
                {
                    view.SetRowCellValue(intRowHandle, "VisitStartDate", dt);
                }
            }
            catch (Exception) { }
            view.EndDataUpdate();
            DevExpress.XtraEditors.XtraMessageBox.Show("Visit(s) Block Edited Successfully.", "Block Edit Visit Start Dates", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }


        #region GridView4

        private void gridView4_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.VisitStartDate;
            SetMenuStatus();
        }

        private void gridView4_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.VisitStartDate;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiRecordTicking.Enabled = false;
                bbiTick.Enabled = false;
                bbiUntick.Enabled = false;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion

        #endregion


        #region Step 5 Page

        private void btnStep5Previous_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep4;
        }
        private void btnStep5Next_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl6.MainView;
            view.PostEditor();
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //  
            if (view.DataRowCount <= 0)
            {
                XtraMessageBox.Show("No visit set up to create. Set up one or more visits using the preceeding steps before proceeding.", "Create Visit(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            xtraTabControl1.SelectedTabPage = this.xtraTabPageFinish;
        }

        private void xtraTabControl2_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            switch (xtraTabControl2.SelectedTabPage.Name)
            {
                case "xtraTabPage5GridView":
                    {
                        schedulerControl1.BeginUpdate();
                        schedulerStorage1.Appointments.DataSource = null;
                        schedulerStorage1.RefreshData();
                        schedulerControl1.EndUpdate();
                        SetMenuStatus();
                    }
                    break;
                default:  // Calendar page //
                    {                       
                        schedulerControl1.BeginUpdate();
                        schedulerStorage1.Appointments.DataSource = sp06145OMVisitEditBindingSource;
                        schedulerStorage1.RefreshData();
                        schedulerControl1.EndUpdate();

                        GridView view = (GridView)gridControl6.MainView;
                        if (view.DataRowCount > 0)
                        {
                            int intVisitID = 0;
                            int[] intRowHandles = view.GetSelectedRows();
                            if (intRowHandles.Length > 0)  // Make first selected row from grid visible in scheduler //
                            {
                                intVisitID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "VisitID"));
                            }
                            else  // Make first row from grid visible in scheduler //
                            {
                                intVisitID = Convert.ToInt32(view.GetRowCellValue(0, "VisitID"));
                            }
                            Appointment apt = null;
                            AppointmentCollection aptCol = schedulerStorage1.Appointments.Items;
                            for (int x = 0; x < aptCol.Count; ++x)
                            {
                                apt = schedulerStorage1.Appointments.Items[x];
                                if (Convert.ToInt32(apt.Id) == intVisitID)
                                {
                                    _boolIgnoreSchedulerSelection = true;
                                    schedulerControl1.SelectedAppointments.Add(apt);
                                    schedulerControl1.ActiveView.SelectAppointment(apt);  // Make it visible on scheduler //
                                    break;
                                }
                            }
                            break;
                        }

                        SetMenuStatus();
                    }
                    break;
            }
        }


        #region GridView6

        private void gridControl6_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView6_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Visit;
            SetMenuStatus();
        }

        private void gridView6_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Visit;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiRecordTicking.Enabled = false;
                bbiTick.Enabled = false;
                bbiUntick.Enabled = false;
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void repositoryItemButtonEditClientPONumber_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var currentRowView = (DataRowView)sp06145OMVisitEditBindingSource.Current;
                var currentRow = (DataSet_OM_Visit.sp06145_OM_Visit_EditRow)currentRowView.Row;
                if (currentRow == null) return;
                int intClientContractID = 0;
                try { intClientContractID = (string.IsNullOrEmpty(currentRow.ClientContractID.ToString()) ? 0 : Convert.ToInt32(currentRow.ClientContractID)); }
                catch (Exception) { }

                int intSiteContractID = 0;
                try { intSiteContractID = (string.IsNullOrEmpty(currentRow.SiteContractID.ToString()) ? 0 : Convert.ToInt32(currentRow.SiteContractID)); }
                catch (Exception) { }

                int intClientPOID = 0;
                try { intClientPOID = (string.IsNullOrEmpty(currentRow.ClientPOID.ToString()) ? 0 : Convert.ToInt32(currentRow.ClientPOID)); }
                catch (Exception) { }

                var fChildForm = new frm_OM_Select_Client_PO_Multi_Page();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.boolPage1Enabled = true;
                fChildForm.boolPage2Enabled = true;
                fChildForm.boolPage3Enabled = false; // Disable Visit page //
                fChildForm.intActivePage = 2;
                fChildForm._PassedInClientContractIDs = intClientContractID.ToString() + ",";
                fChildForm._PassedInSiteContractIDs = intSiteContractID.ToString() + ",";
                fChildForm._PassedInVisitIDs = "";
                fChildForm.intOriginalPOID = intClientPOID;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow.ClientPOID = fChildForm.intSelectedPOID;
                    currentRow.ClientPONumber = fChildForm.strSelectedPONumbers;
                    sp06145OMVisitEditBindingSource.EndEdit();
                }
            }
            else if (e.Button.Tag.ToString() == "clear")
            {
                var currentRowView = (DataRowView)sp06145OMVisitEditBindingSource.Current;
                var currentRow = (DataSet_OM_Visit.sp06145_OM_Visit_EditRow)currentRowView.Row;
                if (currentRow == null) return;
                currentRow.ClientPOID = 0;
                currentRow.ClientPONumber = "";
                sp06145OMVisitEditBindingSource.EndEdit();
            }
        }

        private void repositoryItemDateEdit1_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void repositoryItemDateEdit1_Validating(object sender, CancelEventArgs e)
        {
            ibool_ignoreValidation = true;
            DateEdit de = (DateEdit)sender;
            GridView view = (GridView)gridControl6.MainView;
            if (view.FocusedColumn.Name == "colExpectedStartDate")
            {
                Calculate_Min_Days_Between_Visits(view.FocusedRowHandle, true, de.DateTime, null);
                view.FocusedColumn = view.GetVisibleColumn(view.FocusedColumn.VisibleIndex - 1);

                // Get visit in grid with closest higher date for same Site Contract if there is one and re-calculate the Days Separation since this is the record proceeding it and it's start date has changed effecting the Days Separation to the next visit. //
                // Get existing rows visit ID first so we can ignore this record. //
                int intOriginalVisitID = 0;
                try { intOriginalVisitID = (string.IsNullOrEmpty(view.GetFocusedRowCellValue("VisitID").ToString()) ? 0 : Convert.ToInt32(view.GetFocusedRowCellValue("VisitID"))); }
                catch (Exception) { }
                int intSiteContractID = 0;
                try { intSiteContractID = (string.IsNullOrEmpty(view.GetFocusedRowCellValue("SiteContractID").ToString()) ? 0 : Convert.ToInt32(view.GetFocusedRowCellValue("SiteContractID"))); }
                catch (Exception) { }
                if (intOriginalVisitID != 0 && intSiteContractID != 0)
                {
                    DateTime dtRowStartDate = DateTime.MinValue;
                    DateTime dtNearestHigherStartDate = DateTime.MaxValue;
                    int intRowSiteContractID = 0;
                    int intVisitID = 0;
                    int intNextRowID = -1;
                    try
                    {
                        for (int i = 0; i < view.DataRowCount; i++)  // Find next row //
                        {
                            dtRowStartDate = Convert.ToDateTime(view.GetRowCellValue(i, "ExpectedStartDate"));
                            intRowSiteContractID = Convert.ToInt32(view.GetRowCellValue(i, "SiteContractID"));
                            intVisitID = Convert.ToInt32(view.GetRowCellValue(i, "VisitID"));
                            if (intRowSiteContractID == intSiteContractID && intVisitID != intOriginalVisitID && dtRowStartDate > de.DateTime && dtRowStartDate < dtNearestHigherStartDate)
                            {
                                dtNearestHigherStartDate = dtRowStartDate;
                                intNextRowID = i;
                            }
                        }
                        if (intNextRowID >= 0) Calculate_Min_Days_Between_Visits(intNextRowID, false, null, de.DateTime);  // Update next rows Days Separation //
                    }
                    catch (Exception) { }
                }
            }
            
        }

        private void repositoryItemSpinEditDaysSeparation_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "calculate")
            {
                GridView view = (GridView)gridControl6.MainView;               
                Calculate_Min_Days_Between_Visits(view.FocusedRowHandle, true, null, null);
            }
        }
        private void Calculate_Min_Days_Between_Visits(int intRow, bool ShowErrorMessages, DateTime? PassedInStartDate, DateTime? PassedInPreviousDate)
        {
            /* Following commented out by MB - 18/12/2017 --

            // PassedInStartDate is optional can be null //
            GridView view = (GridView)gridControl6.MainView;
            view.PostEditor();

            int intSiteContractID = 0;
            try { intSiteContractID = (string.IsNullOrEmpty(view.GetRowCellValue(intRow, "SiteContractID").ToString()) ? 0 : Convert.ToInt32(view.GetRowCellValue(intRow, "SiteContractID"))); }
            catch (Exception) { }
            if (intSiteContractID <= 0 && ShowErrorMessages)
            {
                XtraMessageBox.Show("Select the Site Contract first before clicking the Re-Calculate Minimum Days Between Visits button.", "Re-Calculate Minimum Days Between Visits", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            DateTime dtExpectedStartDate = DateTime.MinValue;
            if (PassedInStartDate != null)
            {
                dtExpectedStartDate = Convert.ToDateTime(PassedInStartDate);
            }
            else
            {
                try { dtExpectedStartDate = (string.IsNullOrEmpty(view.GetRowCellValue(intRow, "ExpectedStartDate").ToString()) ? DateTime.MinValue : Convert.ToDateTime(view.GetRowCellValue(intRow, "ExpectedStartDate"))); }
                catch (Exception) { }
            }
            if (dtExpectedStartDate <= DateTime.MinValue && ShowErrorMessages)
            {
                XtraMessageBox.Show("Enter the Visit Expected Start Date first before clicking the Re-Calculate Minimum Days Between Visits button.", "Re-Calculate Minimum Days Between Visits", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            int intOriginalVisitID = 0;
            try { intOriginalVisitID = (string.IsNullOrEmpty(view.GetRowCellValue(intRow, "VisitID").ToString()) ? 0 : Convert.ToInt32(view.GetRowCellValue(intRow, "VisitID"))); }
            catch (Exception) { }

            decimal decDaysSeparationPercentage = (decimal)0.00;
            try { decDaysSeparationPercentage = (string.IsNullOrEmpty(view.GetRowCellValue(intRow, "SiteContractDaysSeparationPercent").ToString()) ? (decimal)0.00 : Convert.ToDecimal(view.GetRowCellValue(intRow, "SiteContractDaysSeparationPercent"))); }
            catch (Exception) { }
            if (decDaysSeparationPercentage <= (decimal)0.00 && ShowErrorMessages)
            {
                XtraMessageBox.Show("The selected Site Contract has no value set for the Days Separation %. Unable to calculate Minimum Days Between Visits.", "Re-Calculate Minimum Days Between Visits", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            int intCurrentValue = 0;
            try { intCurrentValue = (string.IsNullOrEmpty(view.GetRowCellValue(intRow, "DaysSeparation").ToString()) ? 0 : Convert.ToInt32(view.GetRowCellValue(intRow, "DaysSeparation"))); }
            catch (Exception) { }

            int intDaysSeparation = 0;
            DateTime dtPreviousDate = DateTime.MaxValue;
            int intRowSiteContractID = 0;
            int intVisitID = 0;
            //DateTime NoNextDate = new DateTime(2500, 1, 1);
            DateTime NoPreviousDate = new DateTime(1900, 1, 1);
            DateTime dtRowStartDate = NoPreviousDate;
            using (var GetValue = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter())
            {
                GetValue.ChangeConnectionString(strConnectionString);
                try
                {
                    if (PassedInPreviousDate == null)
                    {
                        dtPreviousDate = Convert.ToDateTime(GetValue.sp06438_OM_Get_Next_Scheduled_Visit_Date(intSiteContractID, dtExpectedStartDate));

                        foreach (DataRow dr in dataSet_OM_Visit.sp06145_OM_Visit_Edit.Rows)
                        {
                            dtRowStartDate = Convert.ToDateTime(dr["ExpectedStartDate"]);
                            intRowSiteContractID = Convert.ToInt32(dr["SiteContractID"]);
                            intVisitID = Convert.ToInt32(dr["VisitID"]);
                            //if (intVisitID != intOriginalVisitID && dtRowStartDate > dtExpectedStartDate && dtRowStartDate < dtNextDate && intRowSiteContractID == intSiteContractID) dtNextDate = dtRowStartDate;
                            if (intVisitID != intOriginalVisitID && dtRowStartDate < dtExpectedStartDate && dtRowStartDate > dtPreviousDate && intRowSiteContractID == intSiteContractID) dtPreviousDate = dtRowStartDate;
                        }
                    }
                    else
                    {
                        dtPreviousDate = Convert.ToDateTime(PassedInPreviousDate);
                    }
                    //intDaysSeparation = (dtNextDate == NoNextDate ? 0 : Convert.ToInt32((Convert.ToDecimal((dtNextDate - dtExpectedStartDate).TotalDays) * decDaysSeparationPercentage) / (decimal)100.00));
                    intDaysSeparation = (dtPreviousDate == NoPreviousDate ? 0 : Convert.ToInt32((Convert.ToDecimal((dtExpectedStartDate - dtPreviousDate).TotalDays) * decDaysSeparationPercentage) / (decimal)100.00));
                    if (intDaysSeparation != intCurrentValue)
                    {
                        view.SetRowCellValue(intRow, "DaysSeparation", (intDaysSeparation < 0 ? 0 : intDaysSeparation));
                        sp06145OMVisitEditBindingSource.EndEdit();
                    }
                }
                catch (Exception) { }
            }
            */
        }

        #endregion


        #region Scheduler

        struct ModifiedAppointment
        {
            public int s_VisitID;
            public String s_SiteName;
        }

        private void schedulerControl1_AppointmentResized(object sender, AppointmentResizeEventArgs e)
        {
            Appointment apt = e.SourceAppointment;
            if (apt == null) return;
            //int intVisitID = Convert.ToInt32(apt.CustomFields["VisitID"]);
            int intVisitID = Convert.ToInt32(apt.Id);
            e.SourceAppointment.Start = e.EditedAppointment.Start;
            e.SourceAppointment.End = e.EditedAppointment.End;

            // Update GridView //
            GridView view = (GridView)gridControl6.MainView;
            int intFoundRow = view.LocateByValue(0, view.Columns["VisitID"], intVisitID);
            if (intFoundRow >= 0)
            {
                view.BeginUpdate();
                view.SetRowCellValue(intFoundRow, "ExpectedStartDate", e.EditedAppointment.Start);
                view.SetRowCellValue(intFoundRow, "ExpectedEndDate", e.EditedAppointment.End);
                view.EndUpdate();
            }
        }

        private void schedulerControl1_AppointmentDrop(object sender, AppointmentDragEventArgs e)
        {
            Appointment apt = e.SourceAppointment;
            if (apt == null) return;
            int intVisitID = Convert.ToInt32(apt.Id);
            e.SourceAppointment.Start = e.EditedAppointment.Start;
            e.SourceAppointment.End = e.EditedAppointment.End;

            // Update GridView //
            GridView view = (GridView)gridControl6.MainView;
            int intFoundRow = view.LocateByValue(0, view.Columns["VisitID"], intVisitID);
            if (intFoundRow >= 0)
            {
                view.BeginUpdate();
                view.SetRowCellValue(intFoundRow, "ExpectedStartDate", e.EditedAppointment.Start);
                view.SetRowCellValue(intFoundRow, "ExpectedEndDate", e.EditedAppointment.End);
                view.EndUpdate();
            }
        }

        private void schedulerControl1_SelectionChanged(object sender, EventArgs e)
        {
            if (_boolIgnoreSchedulerSelection)
            {
                _boolIgnoreSchedulerSelection = false;
                return;
            }
            int intFoundRow = 0;
            GridView view = (GridView)gridControl6.MainView;
            view.BeginUpdate();
            view.BeginSelection();
            try
            {
                view.ClearSelection();

                for (int i = 0; i < schedulerControl1.SelectedAppointments.Count; i++)
                {
                    Appointment apt = schedulerControl1.SelectedAppointments[i];
                    intFoundRow = view.LocateByValue(0, view.Columns["VisitID"], Convert.ToInt32(apt.Id));
                    if (intFoundRow >= 0)
                    {
                        view.SelectRow(intFoundRow);
                        view.MakeRowVisible(intFoundRow);
                    }
                }
            }
            catch (Exception) { }
            view.EndSelection();
            view.EndUpdate();
        }


        private void schedulerControl1_PopupMenuShowing(object sender, DevExpress.XtraScheduler.PopupMenuShowingEventArgs e)
        {
            if (e.Menu.Id == DevExpress.XtraScheduler.SchedulerMenuItemId.AppointmentMenu)
            {
                SchedulerPopupMenu subMenu = e.Menu.Items[4] as SchedulerPopupMenu;
                if (subMenu != null)
                {
                    subMenu.Caption = "Set Visit Number As";
                    int intCount = 1;
                    foreach (DXMenuItem menuItem in subMenu.Items)
                    {
                        menuItem.Tag = intCount;
                        intCount++;
                        menuItem.Click += new EventHandler(LabelMenuItem_Clicked);
                        e.Menu.CloseUp += LabelMenu_CloseUp;
                    }

                }
            }
            if (e.Menu.Id == DevExpress.XtraScheduler.SchedulerMenuItemId.DefaultMenu)
            {
                // Find the "New Appointment" menu item and rename it. //
                SchedulerMenuItem item = e.Menu.GetMenuItemById(SchedulerMenuItemId.NewAppointment);
                if (item != null) item.Caption = "&New Visit";

                // Find the "New All Day Appointment" menu item and rename it. //
                item = e.Menu.GetMenuItemById(SchedulerMenuItemId.NewAllDayEvent);
                if (item != null) item.Caption = "New &All Day Visit";
            }
        }
        private void LabelMenuItem_Clicked(object sender, EventArgs e)
        {
            DXMenuItem menuItem = (DXMenuItem)sender;
            int intVisitNumber = Convert.ToInt32(menuItem.Tag);
            if (intVisitNumber <= 0) return;

            // Put modified appointments into an array of structures (as the appointments will be updated by the setRowCellValue in the View update and it will cause a problem if we process the appointments later in loop as the appointment collection changes as a result) //
            List<ModifiedAppointment> list = new List<ModifiedAppointment>();
            AppointmentBaseCollection collection = schedulerControl1.SelectedAppointments;
            foreach (Appointment apt in collection)
            {
                if (apt == null) break;
                ModifiedAppointment[] arr = new ModifiedAppointment[1];
                arr[0].s_VisitID = Convert.ToInt32(apt.Id);
                arr[0].s_SiteName = apt.CustomFields["SiteName"].ToString();
                list.Add(arr[0]);
            }

            schedulerControl1.BeginUpdate();
            GridView view = (GridView)gridControl6.MainView;
            view.BeginUpdate();
            view.BeginDataUpdate();

            foreach (ModifiedAppointment modifiedAppointment in list)
            {
                int intVisitID = modifiedAppointment.s_VisitID;
                string strSiteName = modifiedAppointment.s_SiteName;
                //apt.Location = intVisitNumber.ToString() + " - " + strSiteName;

                // Update GridView //
                int intFoundRow = view.LocateByValue(0, view.Columns["VisitID"], intVisitID);
                if (intFoundRow >= 0)
                {
                    view.SetRowCellValue(intFoundRow, "VisitNumber", intVisitNumber);
                    view.SetRowCellValue(intFoundRow, "LabelColourID", intVisitNumber);
                    view.SetRowCellValue(intFoundRow, "CalendarLabel", intVisitNumber.ToString() + " - " + strSiteName);
                }
            }
            view.EndDataUpdate();
            view.EndUpdate();
            schedulerStorage1.RefreshData();
            schedulerControl1.EndUpdate();

        }
        private void LabelMenu_CloseUp(object sender, EventArgs e)
        {
            SchedulerPopupMenu menu = sender as SchedulerPopupMenu;
            foreach (DXMenuItem menuItem in menu.Items)
            {
                menuItem.Click -= LabelMenuItem_Clicked; // Removing the event //
            }
            menu.CloseUp -= LabelMenu_CloseUp;  // Now remove this event //
        }

        bool DeleteProcessRunning = false;
        bool CancelDelete = false;
        private void schedulerStorage1_AppointmentDeleting(object sender, DevExpress.XtraScheduler.PersistentObjectCancelEventArgs e)
        {
            // If multiple items are selected, this event is fired each time for each selected item //
            if (!DeleteProcessRunning)
            {
                int intSelectedCount = schedulerControl1.SelectedAppointments.Count;
                if (intSelectedCount <= 0) return;

                string strMessage = (intSelectedCount == 1 ? "You are about to delete a new visit." : "you are about to delete " + intSelectedCount.ToString() + " new visits.");
                if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage + "\n\nAre you sure you wish to proceed?", "Delete New Visits", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    CancelDelete = false;
                }
                else
                {
                    CancelDelete = true;
                }
                DeleteProcessRunning = true;
            }
            if (DeleteProcessRunning)
            {
                e.Cancel = CancelDelete;
                if (!CancelDelete)
                {
                    Appointment apt = schedulerControl1.SelectedAppointments[0];
                    ResolveVisitNumbers(Convert.ToInt32(apt.Id));
                }
            }
            if (schedulerControl1.SelectedAppointments.Count <= 1)
            {
                DeleteProcessRunning = false;
                CancelDelete = false;
            }
        }
        
        private void schedulerStorage1_AppointmentsDeleted(object sender, PersistentObjectsEventArgs e)
        {
            if (!DeleteProcessRunning)  // Fire just once after last appointment deleted //
            {
                schedulerControl1.BeginUpdate();
                schedulerStorage1.RefreshData();
                schedulerControl1.EndUpdate();
                SetMenuStatus();
            }
        }


        private void ResolveVisitNumbers(int intVisitID)
        {
            DataRow foundRow = dataSet_OM_Visit.sp06145_OM_Visit_Edit.Rows.Find(intVisitID);  // Find DataRow from passed in primary key //
            if (foundRow == null) return;
            int intSiteContractID = Convert.ToInt32(foundRow["SiteContractID"]);
            if (intSiteContractID <= 0) return;
            int intVisitNumber = Convert.ToInt32(foundRow["VisitNumber"]);
            if (intVisitNumber <= 0) return;
            int intTempVisitNumber = 0;

            GridView view = (GridView)gridControl6.MainView;
            view.BeginUpdate();
            schedulerControl1.BeginUpdate();

            int intCount = view.DataRowCount;
            if (intCount <= 0) return;
            for (int i = 0; i < intCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "SiteContractID")) == intSiteContractID)
                {
                    intTempVisitNumber = Convert.ToInt32(view.GetRowCellValue(i, "VisitNumber"));
                    if (intTempVisitNumber > intVisitNumber)
                    {
                        view.SetRowCellValue(i, "CalendarLabel", (intTempVisitNumber - 1).ToString() + " - " + view.GetRowCellValue(i, "SiteName").ToString());
                        view.SetRowCellValue(i, "VisitNumber", (intTempVisitNumber - 1));
                        view.SetRowCellValue(i, "LabelColourID", (intTempVisitNumber - 1));
                    }
                }
            }
            schedulerControl1.EndUpdate();
            view.EndUpdate();
        }

        #endregion


        private void btnApplyVisitDaysSeparation_Click(object sender, EventArgs e)
        {
            int intRecordCount = 0;
            GridView view = (GridView)gridControl6.MainView;
            view.PostEditor();
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            intRecordCount = intRowHandles.Length;
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Visits to Block Edit Visit Days Separation then try again.", "Block Edit Visit Days Separation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            int intDaysSeparation = Convert.ToInt32(VisitDaysSeparationSpinEdit.EditValue);
            view.BeginDataUpdate();
            try
            {
                foreach (int intRowHandle in intRowHandles)
                {
                    view.SetRowCellValue(intRowHandle, "DaysSeparation", intDaysSeparation);
                }
                sp06145OMVisitEditBindingSource.EndEdit();
            }
            catch (Exception) { }
            view.EndDataUpdate();
            DevExpress.XtraEditors.XtraMessageBox.Show("Visit(s) Days Separation Block Edited Successfully.", "Block Edit Visit Days Separation", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void btnCalculateVisitDaysSeparation_Click(object sender, EventArgs e)
        {
            int intRecordCount = 0;
            GridView view = (GridView)gridControl6.MainView;
            view.PostEditor();
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            intRecordCount = intRowHandles.Length;
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Visits to Calculate Visit Days Separation then try again.", "Calculate Visit Days Separation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            view.BeginDataUpdate();
            try
            {
                foreach (int intRowHandle in intRowHandles)
                {
                    Calculate_Min_Days_Between_Visits(intRowHandle, false, null, null);
                }
            }
            catch (Exception) { }
            view.EndDataUpdate();
            DevExpress.XtraEditors.XtraMessageBox.Show("Visit(s) Days Separation Calculated Successfully.", "Calculate Visit Days Separation", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void btnApplyVisitNumber_Click(object sender, EventArgs e)
        {
            int intRecordCount = 0;
            GridView view = (GridView)gridControl6.MainView;
            view.PostEditor();
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            intRecordCount = intRowHandles.Length;
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Visits to Block Edit Visit Number then try again.", "Block Edit Visit Number", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            int intVisitNumber = Convert.ToInt32(VisitNumberSpinEdit.EditValue);
            view.BeginDataUpdate();
            try
            {
                foreach (int intRowHandle in intRowHandles)
                {
                    view.SetRowCellValue(intRowHandle, "VisitNumber", intVisitNumber);
                }
                sp06145OMVisitEditBindingSource.EndEdit();
            }
            catch (Exception) { }
            view.EndDataUpdate();
            DevExpress.XtraEditors.XtraMessageBox.Show("Visit(s) Number Block Edited Successfully.", "Block Edit Visit Number", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }


        private void btnApplyVisitDuration_Click(object sender, EventArgs e)
        {
            int intRecordCount = 0;
            GridView view = (GridView)gridControl6.MainView;
            view.PostEditor();
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            intRecordCount = intRowHandles.Length;
            if (intRowHandles.Length < 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Visits to Block Edit Visit Duration then try again.", "Block Edit Visit Duration", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            int intDaysSeparation = Convert.ToInt32(VisitDaysSeparationSpinEdit.EditValue);
            view.BeginDataUpdate();
            DateTime dtExpectedStartDate = new DateTime();
            DateTime dtExpectedEndDate = new DateTime();
            int intVisitDuration = Convert.ToInt32(VisitDurationSpinEdit.EditValue);
            string strVisitDurationDescriptor = VisitDescriptorComboBoxEdit.EditValue.ToString();
            TimeSpan ts = VisitTimeEdit.TimeSpan;
            if (intVisitDuration < 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Enter Duration Units and select Unit Descriptor and optionally set the Time then try again.", "Block Edit Visit Duration", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                foreach (int intRowHandle in intRowHandles)
                {
                    try { if (!string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "ExpectedStartDate").ToString())) dtExpectedStartDate = Convert.ToDateTime(view.GetRowCellValue(intRowHandle, "ExpectedStartDate")); }
                    catch (Exception) { }

                    if (dtExpectedStartDate != DateTime.MinValue)
                    {
                        switch (strVisitDurationDescriptor)
	                        {
                                case "Minutes":
                                    {
                                        dtExpectedEndDate = dtExpectedStartDate.AddMinutes(intVisitDuration);
                                    }
                                    break;
                                case "Hours":
                                    {
                                        dtExpectedEndDate = dtExpectedStartDate.AddHours(intVisitDuration);
                                    }
                                    break;
                                case "Days":
                                    {
                                        dtExpectedEndDate = dtExpectedStartDate.AddDays(intVisitDuration);
                                        dtExpectedEndDate = dtExpectedEndDate.Add(ts);
                                        dtExpectedEndDate = dtExpectedEndDate.AddMilliseconds(-5);
                                    }
                                    break;
                                case "Weeks":
                                    {
                                        dtExpectedEndDate = dtExpectedStartDate.AddDays(intVisitDuration * 7);
                                        dtExpectedEndDate = dtExpectedEndDate.Add(ts);
                                        dtExpectedEndDate = dtExpectedEndDate.AddMilliseconds(-5);
                                    }
                                    break;
                                case "Months":
                                    {
                                        dtExpectedEndDate = dtExpectedStartDate.AddMonths(intVisitDuration);
                                        dtExpectedEndDate = dtExpectedEndDate.Add(ts);
                                        dtExpectedEndDate = dtExpectedEndDate.AddMilliseconds(-5);
                                    }
                                    break;
                                case "Years":
                                    {
                                        dtExpectedEndDate = dtExpectedEndDate.Add(ts);
                                        dtExpectedEndDate = dtExpectedStartDate.AddYears(intVisitDuration);
                                        dtExpectedEndDate = dtExpectedEndDate.AddMilliseconds(-5);
                                    }
                                    break;
                                default:
                                    break;
                        }
                        view.SetRowCellValue(intRowHandle, "ExpectedEndDate", dtExpectedEndDate);                                               
                    }

                }
                sp06145OMVisitEditBindingSource.EndEdit();
            }
            catch (Exception) { }
            view.EndDataUpdate();
            DevExpress.XtraEditors.XtraMessageBox.Show("Visit(s) Duration Block Edited Successfully.", "Block Edit Visit Duration", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void VisitDescriptorComboBoxEdit_EditValueChanged(object sender, EventArgs e)
        {
            ComboBoxEdit cbe = (ComboBoxEdit)sender;
            string strValue = cbe.EditValue.ToString();
            Set_Visit_Hours_Edit_Status(strValue);
        }
        private void Set_Visit_Hours_Edit_Status(string strValue)
        {
            VisitTimeEdit.Enabled = (strValue == "Minutes" || strValue == "Hours" ? false : true);
        }


        #endregion


        #region Finish Page

        private void btnFinishPrevious_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep5;
        }
        private void btnFinish_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl6.MainView;  // Linked Site Contracts  //
            view.PostEditor();

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Saving Changes...");

            try 
            {
                sp06145_OM_Visit_EditTableAdapter.Update(dataSet_OM_Visit);  // Insert, Update and Delete queries defined in Table Adapter //
                foreach (DataRow dr in dataSet_OM_Visit.sp06145_OM_Visit_Edit.Rows)
                {
                    dr["strMode"] = "edit";
                }
                sp06145OMVisitEditBindingSource.EndEdit();  // Commit any changes from grid to underlying datasource //
            }
            catch (System.Exception ex)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                XtraMessageBox.Show("An error occurred while saving the Visit changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
              
            // Pick up the new IDs so they can be passed back to the parent manager screen so the new records can be highlighted //
            string strNewIDs = "";
            int intVisitID = 0;
            string strVisitIDs = "";
            string strTempSiteContractID = "";
            string strTempClientContractID = "";

            StringBuilder sbSiteContractIDs = new StringBuilder(",");  // Initialised with a preceeding comma to be used when checking if build string contains next value //
            StringBuilder sbClientContractIDs = new StringBuilder(",");  // Initialised with a preceeding comma to be used when checking if build string contains next value //

            foreach (DataRow dr in dataSet_OM_Visit.sp06145_OM_Visit_Edit.Rows)
            {
                intVisitID = Convert.ToInt32(dr["VisitID"]);
                strNewIDs += intVisitID.ToString() + ";";
                strVisitIDs += intVisitID.ToString() + ",";

                strTempSiteContractID = dr["SiteContractID"].ToString() + ",";
                if (!sbSiteContractIDs.ToString().Contains("," + strTempSiteContractID)) sbSiteContractIDs.Append(strTempSiteContractID);

                strTempClientContractID = dr["ClientContractID"].ToString() + ",";
                if (!sbClientContractIDs.ToString().Contains("," + strTempClientContractID)) sbClientContractIDs.Append(strTempClientContractID);
            }
            string strSiteContractIDs = sbSiteContractIDs.ToString();
            string strClientContractIDs = sbClientContractIDs.ToString();

            if (strSiteContractIDs.StartsWith(",")) strSiteContractIDs = strSiteContractIDs.Remove(0, 1);  // Remove preceeding comma //
            if (strClientContractIDs.StartsWith(",")) strClientContractIDs = strClientContractIDs.Remove(0, 1);  // Remove preceeding comma //

            // Notify any open instances of Parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                if (Application.OpenForms[strCaller] != null)
                {
                    if (strCaller == "frm_OM_Visit_Manager") (Application.OpenForms[strCaller] as frm_OM_Visit_Manager).UpdateFormRefreshStatus(1, Utils.enmFocusedGrid.Visit, strNewIDs);
                    else if (strCaller == "frm_OM_Site_Contract_Manager") (Application.OpenForms[strCaller] as frm_OM_Site_Contract_Manager).UpdateFormRefreshStatus(1, Utils.enmFocusedGrid.Contract, "");
                }
            }
            this.dataSet_OM_Contract.AcceptChanges();

            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }

            if (checkEdit2.Checked)  // Open Job Wizard //
            {
                if (this.ParentForm != null)
                {
                    var fChildForm = new frm_OM_Job_Wizard();
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strRecordIDs = "";
                    fChildForm.strFormMode = "add";
                    fChildForm.strCaller = this.strCaller;
                    fChildForm.intRecordCount = 0;
                    fChildForm.FormPermissions = this.FormPermissions;
                    fChildForm.i_str_PassedInClientContractIDs = strClientContractIDs;
                    fChildForm.i_str_PassedInSiteContractIDs = strSiteContractIDs;
                    fChildForm.i_str_PassedInVisitIDs = strVisitIDs;
                    DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager2 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    fChildForm.splashScreenManager = splashScreenManager2;
                    fChildForm.splashScreenManager.ShowWaitForm();
                    fChildForm.Show();

                    System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm, new object[] { null });
                }
                else
                {
                    XtraMessageBox.Show("Unable to open another form from this form... The form must be docked inside the main container form first.", "Open Job Wizard", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else if (checkEdit3.Checked)  // Re-open Visit Wizard to create another Visit //
            {
                if (this.ParentForm != null)
                {
                    var fChildForm = new frm_OM_Visit_Wizard();
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strRecordIDs = "";
                    fChildForm.strFormMode = "add";
                    fChildForm.strCaller = this.strCaller;
                    fChildForm.intRecordCount = 0;
                    fChildForm.FormPermissions = this.FormPermissions;
                    DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager2 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    fChildForm.splashScreenManager = splashScreenManager2;
                    fChildForm.splashScreenManager.ShowWaitForm();
                    fChildForm.Show();

                    System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm, new object[] { null });
                }
                else
                {
                    XtraMessageBox.Show("Unable to open another form from this form... The form must be docked inside the main container form first.", "Open Visit Wizard", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
          
            this.Close();
        }

        #endregion


        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }


        private void Block_Edit_Record()
        {
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Visit:
                    {
                        Block_Edit_Visits();
                    }
                    break;
                default:
                    break;
            }
        }

        private void Block_Edit_Visits()
        {
            int intRecordCount = 0;
            GridView view = (GridView)gridControl6.MainView;
            int intRowCount = view.DataRowCount;
            view.PostEditor();
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            intRecordCount = intRowHandles.Length;
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Visits to block edit then try again.", "Block Edit Visits", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            var fChildForm = new frm_OM_Visit_Wizard_Block_Edit_Visit_Info();
            fChildForm.GlobalSettings = this.GlobalSettings;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Updating...");
                
                intRowHandles = view.GetSelectedRows();
                intRecordCount = intRowHandles.Length;
                if (intRowHandles.Length <= 0) return;
                view.BeginUpdate();
                view.BeginSort();
                try
                {
                    foreach (int intRowHandle in intRowHandles)
                    {
                        int intCascadeDates = fChildForm.intCascadeDates;
                        double dblDaysDifferenceStart = (double)0;
                        double dblDaysDifferenceEnd = (double)0;
                        int intSiteContractID = 0;
                        int intVisitNumber = 0;
                        if (intCascadeDates == 1 && (fChildForm.dtVisitStartDate != null || fChildForm.dtVisitEndDate != null))
                        {
                            // Find any related Visits after this one and add the number of days to these //
                            if (fChildForm.dtVisitStartDate != null)
                            {
                                DateTime d1 = Convert.ToDateTime(fChildForm.dtVisitStartDate);
                                DateTime d2 = Convert.ToDateTime(view.GetRowCellValue(intRowHandle, "ExpectedStartDate"));
                                TimeSpan t = d1 - d2;
                                dblDaysDifferenceStart = t.TotalDays;
                            }
                            if (fChildForm.dtVisitEndDate != null)
                            {
                                DateTime d1 = Convert.ToDateTime(fChildForm.dtVisitEndDate);
                                DateTime d2 = Convert.ToDateTime(view.GetRowCellValue(intRowHandle, "ExpectedEndDate"));
                                TimeSpan t = d1 - d2;
                                dblDaysDifferenceEnd = t.TotalDays;
                            }
                            intSiteContractID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "SiteContractID"));
                            intVisitNumber = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "VisitNumber"));

                            for (int i = 0; i < intRowCount; i++)
                            {
                                if (Convert.ToInt32(view.GetRowCellValue(i, "SiteContractID")) == intSiteContractID && Convert.ToInt32(view.GetRowCellValue(i, "VisitNumber")) > intVisitNumber)
                                {
                                    view.SetRowCellValue(i, "ExpectedStartDate", Convert.ToDateTime(view.GetRowCellValue(i, "ExpectedStartDate")).AddDays(dblDaysDifferenceStart));
                                    view.SetRowCellValue(i, "ExpectedEndDate", Convert.ToDateTime(view.GetRowCellValue(i, "ExpectedEndDate")).AddDays(dblDaysDifferenceEnd));
                                }
                            }
                        }

                        if (fChildForm.intVisitNumber != null) view.SetRowCellValue(intRowHandle, "VisitNumber", fChildForm.intVisitNumber);
                        if (fChildForm.dtVisitStartDate != null) view.SetRowCellValue(intRowHandle, "ExpectedStartDate", fChildForm.dtVisitStartDate);
                        if (fChildForm.dtVisitEndDate != null) view.SetRowCellValue(intRowHandle, "ExpectedEndDate", fChildForm.dtVisitEndDate);
                        if (fChildForm.intCostCalculationLevel != null) view.SetRowCellValue(intRowHandle, "CostCalculationLevel", fChildForm.intCostCalculationLevel);
                        if (fChildForm.intSellCalculationLevel != null) view.SetRowCellValue(intRowHandle, "SellCalculationLevel", fChildForm.intSellCalculationLevel);
                        if (fChildForm.intVisitCategoryID != null) view.SetRowCellValue(intRowHandle, "VisitCategoryID", fChildForm.intVisitCategoryID);
                        if (fChildForm.intVisitTypeID != null) view.SetRowCellValue(intRowHandle, "VisitTypeID", fChildForm.intVisitTypeID);
                        if (fChildForm.strWorkNumber != null) view.SetRowCellValue(intRowHandle, "WorkNumber", fChildForm.strWorkNumber);
                        if (fChildForm.intClientPOID != null) view.SetRowCellValue(intRowHandle, "ClientPOID", fChildForm.intClientPOID);
                        if (fChildForm.strClientPONumber != null) view.SetRowCellValue(intRowHandle, "ClientPONumber", fChildForm.strClientPONumber);
                        if (fChildForm.intDaysSeparation != null) view.SetRowCellValue(intRowHandle, "DaysSeparation", fChildForm.intDaysSeparation);
                    }
                }
                catch (Exception) { }
                view.EndSort();
                view.EndUpdate();
            }
            if (splashScreenManager.IsSplashFormVisible)
            {
                splashScreenManager.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager.CloseWaitForm();
            }
        }

        private void Delete_Record()
        {
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Visit:
                    {
                        if (strFormMode == "view") return;
                        int[] intRowHandles;
                        int intCount = 0;
                        GridView view = null;
                        string strMessage = "";

                        view = (GridView)gridControl6.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more new Visits to delete by clicking on them then try again.", "Delete New Visits", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 new Visit" : Convert.ToString(intRowHandles.Length) + " new Visits") + " selected for deleting!\n\nProceed?";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete New Visits", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            //view.BeginUpdate();
                            /*foreach (int intRowHandle in intRowHandles)
                            {
                                int intVisitID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "VisitID"));
                                ResolveVisitNumbers(intVisitID);
                                view.DeleteRow(intRowHandles[intRowHandle]);
                            }*/


                            for (int i = intRowHandles.Length - 1; i >= 0; i--)
                            {
                                int intVisitID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "VisitID"));
                                ResolveVisitNumbers(intVisitID);
                                view.DeleteRow(intRowHandles[i]);
                            }

                            // Update Visit Days Separation //
                            view.BeginDataUpdate();
                            try
                            {
                                for (int i = 0; i < view.DataRowCount; i++)
                                {
                                    Calculate_Min_Days_Between_Visits(i, false, null, null);
                                }
                            }
                            catch (Exception) { }
                            view.EndDataUpdate();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            schedulerControl1.BeginUpdate();
                            schedulerStorage1.RefreshData();
                            schedulerControl1.EndUpdate();
                            //view.EndUpdate();
                            SetMenuStatus();
                        }
                    }
                    break;
                default:
                    break;
            }
        }


        private void bbiTick_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = null;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Contract:
                    {
                        view = (GridView)gridControl1.MainView;
                    }
                    break;
                case Utils.enmFocusedGrid.Sites:
                    {
                        view = (GridView)gridControl2.MainView;
                    }
                    break;
                default:
                    return;
            }

            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length > 0)
            {
                view.BeginUpdate();
                view.BeginSelection();
                foreach (int intRowHandle in intRowHandles)
                {
                    view.SetRowCellValue(intRowHandle, "CheckMarkSelection", 1);
                }
                view.EndSelection();
                view.EndUpdate();
            }
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Contract:
                    {
                        Set_Selected_Client_Count_Label();
                    }
                    break;
                case Utils.enmFocusedGrid.Sites:
                    {
                        Set_Selected_Site_Count_Label();
                    }
                    break;
                default:
                    return;
            }
        }

        private void bbiUntick_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = null;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Contract:
                    {
                        view = (GridView)gridControl1.MainView;
                    }
                    break;
                case Utils.enmFocusedGrid.Sites:
                    {
                        view = (GridView)gridControl2.MainView;
                    }
                    break;
                default:
                    return;
            }
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length > 0)
            {
                view.BeginUpdate();
                view.BeginSelection();
                foreach (int intRowHandle in intRowHandles)
                {
                    view.SetRowCellValue(intRowHandle, "CheckMarkSelection", 0);
                }
                view.EndSelection();
                view.EndUpdate();
            }
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Contract:
                    {
                        Set_Selected_Client_Count_Label();
                    }
                    break;
                case Utils.enmFocusedGrid.Sites:
                    {
                        Set_Selected_Site_Count_Label();
                    }
                    break;
                default:
                    return;
            }
        }




 


 

  




    }
}

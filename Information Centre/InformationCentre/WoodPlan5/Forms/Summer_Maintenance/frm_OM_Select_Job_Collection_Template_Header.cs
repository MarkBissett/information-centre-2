using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WoodPlan5.Properties;
using BaseObjects;
using DevExpress.LookAndFeel;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Drawing;

namespace WoodPlan5
{
    public partial class frm_OM_Select_Job_Collection_Template_Header : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        public string _PassedInClientIDsFilter = "";
        public int intPassedInTemplateHeaderID = 0;       
        public int intSelectedTemplateHeaderID = 0;
        public string strSelectedTemplateHeaderDescription = "";
        public string strSelectedItemIDs = "";
        public bool _boolApplyRules = true;
        public bool AllowApplyRules = true;

        #endregion

        public frm_OM_Select_Job_Collection_Template_Header()
        {
            InitializeComponent();
        }

        private void frm_OM_Select_Job_Collection_Template_Header_Load(object sender, EventArgs e)
        {
            this.FormID = 500162;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            sp06173_OM_Job_Collection_Template_Items_SelectTableAdapter.Connection.ConnectionString = strConnectionString;
            
            try
            {
                sp06172_OM_Job_Collection_Template_Headers_SelectTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06172_OM_Job_Collection_Template_Headers_SelectTableAdapter.Fill(dataSet_OM_Job.sp06172_OM_Job_Collection_Template_Headers_Select, _PassedInClientIDsFilter);
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the Template Headers list. This screen will now close.\n\nPlease try again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            gridControl1.ForceInitialize();
            GridView view = (GridView)gridControl1.MainView;
            if (intPassedInTemplateHeaderID != 0)  // Record selected so try to find and highlight //
            {
                int intFoundRow = view.LocateByValue(0, view.Columns["TemplateHeaderID"], intPassedInTemplateHeaderID);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.FocusedRowHandle = intFoundRow;
                    view.MakeRowVisible(intFoundRow, false);
                }
            }

            gridControl2.ForceInitialize();
            view = (GridView)gridControl2.MainView;
            Set_Apply_Rules_Visible_Status();
            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
        }

        public override void PostLoadView(object objParameter)
        {
            Set_Apply_Rules_Visible_Status();
        }

        bool internalRowFocusing;

        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Job Collection Template Headers Available";
                    break;
                case "gridView2":
                    message = "No Job Collection Template Items Available - Select a Job Collection Template Header to see Related Job Collection Template Items";
                    break;
                case "gridView3":
                    message = "No Linked Template Items Rules Available - Select a Job Collection Template Item record to view Linked Template Item Rules";
                    break;
                case "gridView4":
                    message = "No Linked Template Items Rule Visits Available - Select a Job Collection Template Item Rule record to view Linked Template Item Rule Visits";
                    break;
                case "gridView5":
                    message = "No Linked Template Items Visit Categories Available - Select a Job Collection Template Item record to view Linked Template Item Visit Categories";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_FocusedRowChanged_NoGroupSelection(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    LoadLinkedData1();
                    break;
                case "gridView2":
                    LoadLinkedRecordsRules();
                    view = (GridView)gridControl3.MainView;
                    view.ExpandAllGroups();
                    LoadLinkedRecordsVisitCategories();
                    view = (GridView)gridControl5.MainView;
                    view.ExpandAllGroups();
                    break;
                case "gridView3":
                    LoadLinkedRecordsRuleVisits();
                    view = (GridView)gridControl4.MainView;
                    view.ExpandAllGroups();
                    break;
                default:
                    break;
            }
        }

        public void GridView_MouseDown_NoGroupSelection(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        private void LoadLinkedData1()
        {
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["JobCollectionTemplateHeaderID"])) + ',';
            }

            gridControl2.MainView.BeginUpdate();
            if (intCount == 0)
            {
                dataSet_OM_Job.sp06173_OM_Job_Collection_Template_Items_Select.Clear();
            }
            else
            {
                try
                {
                    sp06173_OM_Job_Collection_Template_Items_SelectTableAdapter.Fill(dataSet_OM_Job.sp06173_OM_Job_Collection_Template_Items_Select, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while loading the related records.\n\nTry selecting a parent record again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            gridControl2.MainView.EndUpdate();
        }

        private void LoadLinkedRecordsRules()
        {
            GridView view = (GridView)gridControl2.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["JobCollectionTemplateItemID"])) + ',';
            }

            // Populate Linked Records //
            gridControl3.MainView.BeginUpdate();
            if (intCount == 0)
            {
                dataSet_OM_Job.sp06332_OM_Job_Collection_Template_Item_Rules.Clear();
            }
            else // Load users selection //
            {
                sp06332_OM_Job_Collection_Template_Item_RulesTableAdapter.Fill(dataSet_OM_Job.sp06332_OM_Job_Collection_Template_Item_Rules, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
            }
            gridControl3.MainView.EndUpdate();
        }

        private void LoadLinkedRecordsRuleVisits()
        {
            GridView view = (GridView)gridControl3.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["JobCollectionTemplateItemRuleID"])) + ',';
            }

            // Populate Linked Records //
            gridControl4.MainView.BeginUpdate();
            if (intCount == 0)
            {
                dataSet_OM_Job.sp06335_OM_Job_Collection_Template_Item_Rule_Visits.Clear();
            }
            else // Load users selection //
            {
                sp06335_OM_Job_Collection_Template_Item_Rule_VisitsTableAdapter.Fill(dataSet_OM_Job.sp06335_OM_Job_Collection_Template_Item_Rule_Visits, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
            }
            gridControl4.MainView.EndUpdate();
        }

        private void LoadLinkedRecordsVisitCategories()
        {
            GridView view = (GridView)gridControl2.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["JobCollectionTemplateItemID"])) + ',';
            }

            // Populate Linked Records //
            gridControl5.MainView.BeginUpdate();
            if (intCount == 0)
            {
                dataSet_OM_Job.sp06340_OM_Job_Collection_Template_Item_Visit_Categories.Clear();
            }
            else // Load users selection //
            {
                sp06340_OM_Job_Collection_Template_Item_Visit_CategoriesTableAdapter.Fill(dataSet_OM_Job.sp06340_OM_Job_Collection_Template_Item_Visit_Categories, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
            }
            gridControl5.MainView.EndUpdate();
        }

        private void Set_Apply_Rules_Visible_Status()
        {
            labelControl1.Visible = (AllowApplyRules ? true : false);
            checkEditApplyRules.Visible = (AllowApplyRules ? true : false);
            checkEditApplyRules.Enabled = (AllowApplyRules ? true : false);
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (intSelectedTemplateHeaderID <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select a record from the Template Header list before proceeding.", "Select Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void GetSelectedDetails()
        {
            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                intSelectedTemplateHeaderID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "JobCollectionTemplateHeaderID"));
                strSelectedTemplateHeaderDescription = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "Description"))) ? "Unknown Template Header" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "Description")));
            }

            strSelectedItemIDs = "";
            view = (GridView)gridControl2.MainView;
            int intCount = view.DataRowCount;
            for (int i = 0; i < intCount; i++)
            {
                strSelectedItemIDs += view.GetRowCellValue(i, "JobSubTypeID").ToString() + ",";
            }
            _boolApplyRules = checkEditApplyRules.Checked;
        }

        private void gridControl2_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {

        }

        private void gridControl4_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {

        }

        private void gridControl5_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {

        }



    }
}


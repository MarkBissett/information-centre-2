﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_OM_Site_Contract_Wizard_Block_Edit_Default_Material_Info : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        public string strMaterialDescription = null;
        public int? intCostUnitDescriptorID = null;
        public int? intSellUnitDescriptorID = null;
        public decimal? decCostPerUnitExVat = null;
        public decimal? decCostPerUnitVatRate = null;
        public decimal? decSellPerUnitExVat = null;
        public decimal? decSellPerUnitVatRate = null;
        public string strRemarks = null;

        public int? intMaterialID = null;
        #endregion

        public frm_OM_Site_Contract_Wizard_Block_Edit_Default_Material_Info()
        {
            InitializeComponent();
        }

        private void frm_OM_Site_Contract_Wizard_Block_Edit_Default_Material_Info_Load(object sender, EventArgs e)
        {
            this.FormID = 500139;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            Application.DoEvents();  // Allow Form time to repaint itself //
            
            strConnectionString = this.GlobalSettings.ConnectionString;
            sp06108_OM_Unit_Descriptors_PicklistTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06108_OM_Unit_Descriptors_PicklistTableAdapter.Fill(dataSet_OM_Contract.sp06108_OM_Unit_Descriptors_Picklist, 1);

            MaterialIDTextEdit.EditValue = null;
            MaterialDescriptionButtonEdit.EditValue = null;
            CostUnitDescriptorIDGridLookUpEdit.EditValue = null;
            SellUnitDescriptorIDGridLookUpEdit.EditValue = null;
            CostPerUnitExVatSpinEdit.EditValue = null;
            CostPerUnitVatRateSpinEdit.EditValue = null;
            SellPerUnitExVatSpinEdit.EditValue = null;
            SellPerUnitVatRateSpinEdit.EditValue = null;
            RemarksMemoEdit.EditValue = null;
            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
        }

        private void MaterialDescriptionButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var fChildForm = new frm_OM_Select_Material();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalID = 0;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    MaterialIDTextEdit.EditValue = fChildForm.intSelectedID;
                    MaterialDescriptionButtonEdit.EditValue = fChildForm.strSelectedDescription1;
                    CostUnitDescriptorIDGridLookUpEdit.EditValue = fChildForm.intSelectedDescription2;
                    SellUnitDescriptorIDGridLookUpEdit.EditValue = fChildForm.intSelectedDescription2;
                    CostPerUnitExVatSpinEdit.EditValue = fChildForm.decSelectedCostExVat;
                    CostPerUnitVatRateSpinEdit.EditValue = fChildForm.decSelectedCostVatRate;
                    SellPerUnitExVatSpinEdit.EditValue = fChildForm.decSelectedSellExVat;
                    SellPerUnitVatRateSpinEdit.EditValue = fChildForm.decSelectedSellVatRate;
                }
            }
        }

       
        private void btnOK_Click(object sender, EventArgs e)
        {
            if (MaterialIDTextEdit.EditValue != null) intMaterialID = Convert.ToInt32(MaterialIDTextEdit.EditValue);
            if (MaterialDescriptionButtonEdit.EditValue != null) strMaterialDescription = MaterialDescriptionButtonEdit.EditValue.ToString();
            if (CostUnitDescriptorIDGridLookUpEdit.EditValue != null) intCostUnitDescriptorID = Convert.ToInt32(CostUnitDescriptorIDGridLookUpEdit.EditValue);
            if (SellUnitDescriptorIDGridLookUpEdit.EditValue != null) intSellUnitDescriptorID = Convert.ToInt32(SellUnitDescriptorIDGridLookUpEdit.EditValue);
            if (CostPerUnitExVatSpinEdit.EditValue != null) decCostPerUnitExVat = Convert.ToDecimal(CostPerUnitExVatSpinEdit.EditValue);           
            if (CostPerUnitVatRateSpinEdit.EditValue != null) decCostPerUnitVatRate = Convert.ToDecimal(CostPerUnitVatRateSpinEdit.EditValue);
            if (SellPerUnitExVatSpinEdit.EditValue != null) decSellPerUnitExVat = Convert.ToDecimal(SellPerUnitExVatSpinEdit.EditValue);
            if (SellPerUnitVatRateSpinEdit.EditValue != null) decSellPerUnitVatRate = Convert.ToDecimal(SellPerUnitVatRateSpinEdit.EditValue);
            if (RemarksMemoEdit.EditValue != null) strRemarks = RemarksMemoEdit.EditValue.ToString();
              
            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }



 





    }
}

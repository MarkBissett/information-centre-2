namespace WoodPlan5
{
    partial class frm_OM_Visit_Labour_Calculated_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Visit_Labour_Calculated_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling13 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling14 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling15 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling16 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling17 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling18 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.JobCountTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.sp06359OMVisitLabourEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Visit = new WoodPlan5.DataSet_OM_Visit();
            this.LabourCostTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.VisitTotalCostTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.InternalExternalTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LinkedToPersonTypeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LabourNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CostCalculationLevelDescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LabourTypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LabourIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.VisitLabourCalculatedIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.VisitNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.VisitIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PercentageOfValueSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.ItemForVisitLabourCalculatedID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForVisitID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLabourID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLabourTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layGrpAddress = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForLabourName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLinkedToPersonType = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForInternalExternal = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForVisitTotalCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPercentageOfValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLabourCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobCount = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteName = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForClientName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForVisitNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCostCalculationLevelDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp06359_OM_Visit_Labour_EditTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06359_OM_Visit_Labour_EditTableAdapter();
            this.ItemForSelfBillingInvoiceHeaderID = new DevExpress.XtraLayout.LayoutControlItem();
            this.SelfBillingInvoiceHeaderIDSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SelfBillingAmountSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ItemForSelfBillingAmount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.SelfBillingCommentMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ItemForSelfBillingComment = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.JobCountTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06359OMVisitLabourEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Visit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabourCostTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitTotalCostTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InternalExternalTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToPersonTypeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabourNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostCalculationLevelDescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabourTypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabourIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitLabourCalculatedIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PercentageOfValueSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitLabourCalculatedID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLabourID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLabourTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLabourName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToPersonType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInternalExternal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitTotalCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPercentageOfValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLabourCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostCalculationLevelDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSelfBillingInvoiceHeaderID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelfBillingInvoiceHeaderIDSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelfBillingAmountSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSelfBillingAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelfBillingCommentMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSelfBillingComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(686, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 609);
            this.barDockControlBottom.Size = new System.Drawing.Size(686, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 583);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(686, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 583);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 16;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.Glyph")));
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem2.Image")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(686, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 609);
            this.barDockControl2.Size = new System.Drawing.Size(686, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 583);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(686, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 583);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.SelfBillingCommentMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.SelfBillingAmountSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.JobCountTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LabourCostTextEdit);
            this.dataLayoutControl1.Controls.Add(this.VisitTotalCostTextEdit);
            this.dataLayoutControl1.Controls.Add(this.InternalExternalTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LinkedToPersonTypeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LabourNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CostCalculationLevelDescriptionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LabourTypeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LabourIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.VisitLabourCalculatedIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.VisitNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.VisitIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PercentageOfValueSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.SelfBillingInvoiceHeaderIDSpinEdit);
            this.dataLayoutControl1.DataSource = this.sp06359OMVisitLabourEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForVisitLabourCalculatedID,
            this.ItemForVisitID,
            this.ItemForLabourID,
            this.ItemForLabourTypeID,
            this.ItemForClientID,
            this.ItemForSiteID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1234, 162, 301, 422);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(686, 583);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // JobCountTextEdit
            // 
            this.JobCountTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06359OMVisitLabourEditBindingSource, "JobCount", true));
            this.JobCountTextEdit.Location = new System.Drawing.Point(164, 284);
            this.JobCountTextEdit.MenuManager = this.barManager1;
            this.JobCountTextEdit.Name = "JobCountTextEdit";
            this.JobCountTextEdit.Properties.Mask.EditMask = "n0";
            this.JobCountTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.JobCountTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.JobCountTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.JobCountTextEdit, true);
            this.JobCountTextEdit.Size = new System.Drawing.Size(140, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.JobCountTextEdit, optionsSpelling2);
            this.JobCountTextEdit.StyleController = this.dataLayoutControl1;
            this.JobCountTextEdit.TabIndex = 78;
            // 
            // sp06359OMVisitLabourEditBindingSource
            // 
            this.sp06359OMVisitLabourEditBindingSource.DataMember = "sp06359_OM_Visit_Labour_Edit";
            this.sp06359OMVisitLabourEditBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // dataSet_OM_Visit
            // 
            this.dataSet_OM_Visit.DataSetName = "DataSet_OM_Visit";
            this.dataSet_OM_Visit.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // LabourCostTextEdit
            // 
            this.LabourCostTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06359OMVisitLabourEditBindingSource, "LabourCost", true));
            this.LabourCostTextEdit.Location = new System.Drawing.Point(164, 356);
            this.LabourCostTextEdit.MenuManager = this.barManager1;
            this.LabourCostTextEdit.Name = "LabourCostTextEdit";
            this.LabourCostTextEdit.Properties.Mask.EditMask = "c";
            this.LabourCostTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.LabourCostTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.LabourCostTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LabourCostTextEdit, true);
            this.LabourCostTextEdit.Size = new System.Drawing.Size(140, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LabourCostTextEdit, optionsSpelling3);
            this.LabourCostTextEdit.StyleController = this.dataLayoutControl1;
            this.LabourCostTextEdit.TabIndex = 77;
            // 
            // VisitTotalCostTextEdit
            // 
            this.VisitTotalCostTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06359OMVisitLabourEditBindingSource, "VisitTotalCost", true));
            this.VisitTotalCostTextEdit.Location = new System.Drawing.Point(164, 308);
            this.VisitTotalCostTextEdit.MenuManager = this.barManager1;
            this.VisitTotalCostTextEdit.Name = "VisitTotalCostTextEdit";
            this.VisitTotalCostTextEdit.Properties.Mask.EditMask = "c";
            this.VisitTotalCostTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.VisitTotalCostTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.VisitTotalCostTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.VisitTotalCostTextEdit, true);
            this.VisitTotalCostTextEdit.Size = new System.Drawing.Size(140, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.VisitTotalCostTextEdit, optionsSpelling4);
            this.VisitTotalCostTextEdit.StyleController = this.dataLayoutControl1;
            this.VisitTotalCostTextEdit.TabIndex = 76;
            // 
            // InternalExternalTextEdit
            // 
            this.InternalExternalTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06359OMVisitLabourEditBindingSource, "InternalExternal", true));
            this.InternalExternalTextEdit.Location = new System.Drawing.Point(164, 260);
            this.InternalExternalTextEdit.MenuManager = this.barManager1;
            this.InternalExternalTextEdit.Name = "InternalExternalTextEdit";
            this.InternalExternalTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.InternalExternalTextEdit, true);
            this.InternalExternalTextEdit.Size = new System.Drawing.Size(140, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.InternalExternalTextEdit, optionsSpelling5);
            this.InternalExternalTextEdit.StyleController = this.dataLayoutControl1;
            this.InternalExternalTextEdit.TabIndex = 75;
            // 
            // LinkedToPersonTypeTextEdit
            // 
            this.LinkedToPersonTypeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06359OMVisitLabourEditBindingSource, "LinkedToPersonType", true));
            this.LinkedToPersonTypeTextEdit.Location = new System.Drawing.Point(164, 236);
            this.LinkedToPersonTypeTextEdit.MenuManager = this.barManager1;
            this.LinkedToPersonTypeTextEdit.Name = "LinkedToPersonTypeTextEdit";
            this.LinkedToPersonTypeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LinkedToPersonTypeTextEdit, true);
            this.LinkedToPersonTypeTextEdit.Size = new System.Drawing.Size(140, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LinkedToPersonTypeTextEdit, optionsSpelling6);
            this.LinkedToPersonTypeTextEdit.StyleController = this.dataLayoutControl1;
            this.LinkedToPersonTypeTextEdit.TabIndex = 74;
            // 
            // LabourNameTextEdit
            // 
            this.LabourNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06359OMVisitLabourEditBindingSource, "LabourName", true));
            this.LabourNameTextEdit.Location = new System.Drawing.Point(164, 212);
            this.LabourNameTextEdit.MenuManager = this.barManager1;
            this.LabourNameTextEdit.Name = "LabourNameTextEdit";
            this.LabourNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LabourNameTextEdit, true);
            this.LabourNameTextEdit.Size = new System.Drawing.Size(486, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LabourNameTextEdit, optionsSpelling7);
            this.LabourNameTextEdit.StyleController = this.dataLayoutControl1;
            this.LabourNameTextEdit.TabIndex = 73;
            // 
            // CostCalculationLevelDescriptionTextEdit
            // 
            this.CostCalculationLevelDescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06359OMVisitLabourEditBindingSource, "CostCalculationLevelDescription", true));
            this.CostCalculationLevelDescriptionTextEdit.Location = new System.Drawing.Point(140, 107);
            this.CostCalculationLevelDescriptionTextEdit.MenuManager = this.barManager1;
            this.CostCalculationLevelDescriptionTextEdit.Name = "CostCalculationLevelDescriptionTextEdit";
            this.CostCalculationLevelDescriptionTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CostCalculationLevelDescriptionTextEdit, true);
            this.CostCalculationLevelDescriptionTextEdit.Size = new System.Drawing.Size(534, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CostCalculationLevelDescriptionTextEdit, optionsSpelling8);
            this.CostCalculationLevelDescriptionTextEdit.StyleController = this.dataLayoutControl1;
            this.CostCalculationLevelDescriptionTextEdit.TabIndex = 72;
            // 
            // LabourTypeIDTextEdit
            // 
            this.LabourTypeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06359OMVisitLabourEditBindingSource, "LabourTypeID", true));
            this.LabourTypeIDTextEdit.Location = new System.Drawing.Point(107, 438);
            this.LabourTypeIDTextEdit.MenuManager = this.barManager1;
            this.LabourTypeIDTextEdit.Name = "LabourTypeIDTextEdit";
            this.LabourTypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LabourTypeIDTextEdit, true);
            this.LabourTypeIDTextEdit.Size = new System.Drawing.Size(567, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LabourTypeIDTextEdit, optionsSpelling9);
            this.LabourTypeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.LabourTypeIDTextEdit.TabIndex = 71;
            // 
            // LabourIDTextEdit
            // 
            this.LabourIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06359OMVisitLabourEditBindingSource, "LabourID", true));
            this.LabourIDTextEdit.Location = new System.Drawing.Point(107, 438);
            this.LabourIDTextEdit.MenuManager = this.barManager1;
            this.LabourIDTextEdit.Name = "LabourIDTextEdit";
            this.LabourIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LabourIDTextEdit, true);
            this.LabourIDTextEdit.Size = new System.Drawing.Size(567, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LabourIDTextEdit, optionsSpelling10);
            this.LabourIDTextEdit.StyleController = this.dataLayoutControl1;
            this.LabourIDTextEdit.TabIndex = 70;
            // 
            // VisitLabourCalculatedIDTextEdit
            // 
            this.VisitLabourCalculatedIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06359OMVisitLabourEditBindingSource, "VisitLabourCalculatedID", true));
            this.VisitLabourCalculatedIDTextEdit.Location = new System.Drawing.Point(141, 366);
            this.VisitLabourCalculatedIDTextEdit.MenuManager = this.barManager1;
            this.VisitLabourCalculatedIDTextEdit.Name = "VisitLabourCalculatedIDTextEdit";
            this.VisitLabourCalculatedIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.VisitLabourCalculatedIDTextEdit, true);
            this.VisitLabourCalculatedIDTextEdit.Size = new System.Drawing.Size(533, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.VisitLabourCalculatedIDTextEdit, optionsSpelling11);
            this.VisitLabourCalculatedIDTextEdit.StyleController = this.dataLayoutControl1;
            this.VisitLabourCalculatedIDTextEdit.TabIndex = 68;
            // 
            // ClientNameTextEdit
            // 
            this.ClientNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06359OMVisitLabourEditBindingSource, "ClientName", true));
            this.ClientNameTextEdit.Location = new System.Drawing.Point(140, 35);
            this.ClientNameTextEdit.MenuManager = this.barManager1;
            this.ClientNameTextEdit.Name = "ClientNameTextEdit";
            this.ClientNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientNameTextEdit, true);
            this.ClientNameTextEdit.Size = new System.Drawing.Size(534, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientNameTextEdit, optionsSpelling12);
            this.ClientNameTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientNameTextEdit.TabIndex = 57;
            this.ClientNameTextEdit.TabStop = false;
            // 
            // VisitNumberTextEdit
            // 
            this.VisitNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06359OMVisitLabourEditBindingSource, "VisitNumber", true));
            this.VisitNumberTextEdit.Location = new System.Drawing.Point(140, 83);
            this.VisitNumberTextEdit.MenuManager = this.barManager1;
            this.VisitNumberTextEdit.Name = "VisitNumberTextEdit";
            this.VisitNumberTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.VisitNumberTextEdit, true);
            this.VisitNumberTextEdit.Size = new System.Drawing.Size(534, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.VisitNumberTextEdit, optionsSpelling13);
            this.VisitNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.VisitNumberTextEdit.TabIndex = 56;
            this.VisitNumberTextEdit.TabStop = false;
            // 
            // SiteNameTextEdit
            // 
            this.SiteNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06359OMVisitLabourEditBindingSource, "SiteName", true));
            this.SiteNameTextEdit.Location = new System.Drawing.Point(140, 59);
            this.SiteNameTextEdit.MenuManager = this.barManager1;
            this.SiteNameTextEdit.Name = "SiteNameTextEdit";
            this.SiteNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteNameTextEdit, true);
            this.SiteNameTextEdit.Size = new System.Drawing.Size(534, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteNameTextEdit, optionsSpelling14);
            this.SiteNameTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteNameTextEdit.TabIndex = 55;
            this.SiteNameTextEdit.TabStop = false;
            // 
            // VisitIDTextEdit
            // 
            this.VisitIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06359OMVisitLabourEditBindingSource, "VisitID", true));
            this.VisitIDTextEdit.Location = new System.Drawing.Point(107, 414);
            this.VisitIDTextEdit.MenuManager = this.barManager1;
            this.VisitIDTextEdit.Name = "VisitIDTextEdit";
            this.VisitIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.VisitIDTextEdit, true);
            this.VisitIDTextEdit.Size = new System.Drawing.Size(567, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.VisitIDTextEdit, optionsSpelling15);
            this.VisitIDTextEdit.StyleController = this.dataLayoutControl1;
            this.VisitIDTextEdit.TabIndex = 53;
            // 
            // SiteIDTextEdit
            // 
            this.SiteIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06359OMVisitLabourEditBindingSource, "SiteID", true));
            this.SiteIDTextEdit.Location = new System.Drawing.Point(140, 366);
            this.SiteIDTextEdit.MenuManager = this.barManager1;
            this.SiteIDTextEdit.Name = "SiteIDTextEdit";
            this.SiteIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteIDTextEdit, true);
            this.SiteIDTextEdit.Size = new System.Drawing.Size(534, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteIDTextEdit, optionsSpelling16);
            this.SiteIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteIDTextEdit.TabIndex = 52;
            // 
            // ClientIDTextEdit
            // 
            this.ClientIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06359OMVisitLabourEditBindingSource, "ClientID", true));
            this.ClientIDTextEdit.Location = new System.Drawing.Point(140, 318);
            this.ClientIDTextEdit.MenuManager = this.barManager1;
            this.ClientIDTextEdit.Name = "ClientIDTextEdit";
            this.ClientIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientIDTextEdit, true);
            this.ClientIDTextEdit.Size = new System.Drawing.Size(534, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientIDTextEdit, optionsSpelling17);
            this.ClientIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientIDTextEdit.TabIndex = 51;
            // 
            // PercentageOfValueSpinEdit
            // 
            this.PercentageOfValueSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06359OMVisitLabourEditBindingSource, "PercentageOfValue", true));
            this.PercentageOfValueSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.PercentageOfValueSpinEdit.Location = new System.Drawing.Point(164, 332);
            this.PercentageOfValueSpinEdit.MenuManager = this.barManager1;
            this.PercentageOfValueSpinEdit.Name = "PercentageOfValueSpinEdit";
            this.PercentageOfValueSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PercentageOfValueSpinEdit.Properties.Mask.EditMask = "P";
            this.PercentageOfValueSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.PercentageOfValueSpinEdit.Properties.MaxValue = new decimal(new int[] {
            99999,
            0,
            0,
            131072});
            this.PercentageOfValueSpinEdit.Size = new System.Drawing.Size(140, 20);
            this.PercentageOfValueSpinEdit.StyleController = this.dataLayoutControl1;
            this.PercentageOfValueSpinEdit.TabIndex = 45;
            this.PercentageOfValueSpinEdit.EditValueChanged += new System.EventHandler(this.PercentageOfValueSpinEdit_EditValueChanged);
            this.PercentageOfValueSpinEdit.Validated += new System.EventHandler(this.PercentageOfValueSpinEdit_Validated);
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06359OMVisitLabourEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, 212);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(614, 196);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling18);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 35;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp06359OMVisitLabourEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(140, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(131, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 24;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // ItemForVisitLabourCalculatedID
            // 
            this.ItemForVisitLabourCalculatedID.Control = this.VisitLabourCalculatedIDTextEdit;
            this.ItemForVisitLabourCalculatedID.Location = new System.Drawing.Point(0, 72);
            this.ItemForVisitLabourCalculatedID.Name = "ItemForVisitLabourCalculatedID";
            this.ItemForVisitLabourCalculatedID.Size = new System.Drawing.Size(666, 24);
            this.ItemForVisitLabourCalculatedID.Text = "Visit Labour Calculated ID:";
            this.ItemForVisitLabourCalculatedID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForVisitID
            // 
            this.ItemForVisitID.Control = this.VisitIDTextEdit;
            this.ItemForVisitID.CustomizationFormText = "Visit ID:";
            this.ItemForVisitID.Location = new System.Drawing.Point(0, 120);
            this.ItemForVisitID.Name = "ItemForVisitID";
            this.ItemForVisitID.Size = new System.Drawing.Size(666, 24);
            this.ItemForVisitID.Text = "Visit ID:";
            this.ItemForVisitID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForLabourID
            // 
            this.ItemForLabourID.Control = this.LabourIDTextEdit;
            this.ItemForLabourID.Location = new System.Drawing.Point(0, 144);
            this.ItemForLabourID.Name = "ItemForLabourID";
            this.ItemForLabourID.Size = new System.Drawing.Size(666, 24);
            this.ItemForLabourID.Text = "Labour ID:";
            this.ItemForLabourID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForLabourTypeID
            // 
            this.ItemForLabourTypeID.Control = this.LabourTypeIDTextEdit;
            this.ItemForLabourTypeID.Location = new System.Drawing.Point(0, 144);
            this.ItemForLabourTypeID.Name = "ItemForLabourTypeID";
            this.ItemForLabourTypeID.Size = new System.Drawing.Size(666, 24);
            this.ItemForLabourTypeID.Text = "Labour Type ID:";
            this.ItemForLabourTypeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForClientID
            // 
            this.ItemForClientID.Control = this.ClientIDTextEdit;
            this.ItemForClientID.CustomizationFormText = "Client ID:";
            this.ItemForClientID.Location = new System.Drawing.Point(0, 24);
            this.ItemForClientID.Name = "ItemForClientID";
            this.ItemForClientID.Size = new System.Drawing.Size(666, 24);
            this.ItemForClientID.Text = "Client ID:";
            this.ItemForClientID.TextSize = new System.Drawing.Size(125, 13);
            // 
            // ItemForSiteID
            // 
            this.ItemForSiteID.Control = this.SiteIDTextEdit;
            this.ItemForSiteID.CustomizationFormText = "Site ID:";
            this.ItemForSiteID.Location = new System.Drawing.Point(0, 72);
            this.ItemForSiteID.Name = "ItemForSiteID";
            this.ItemForSiteID.Size = new System.Drawing.Size(666, 24);
            this.ItemForSiteID.Text = "Site ID:";
            this.ItemForSiteID.TextSize = new System.Drawing.Size(125, 13);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(686, 583);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.layoutControlGroup6,
            this.ItemForSiteName,
            this.emptySpaceItem6,
            this.ItemForClientName,
            this.ItemForVisitNumber,
            this.ItemForCostCalculationLevelDescription});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(666, 424);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(128, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(128, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(128, 23);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(263, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(403, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(128, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(135, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Details";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 130);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(666, 294);
            this.layoutControlGroup6.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layGrpAddress;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(642, 248);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layGrpAddress,
            this.layoutControlGroup4});
            // 
            // layGrpAddress
            // 
            this.layGrpAddress.CustomizationFormText = "Address";
            this.layGrpAddress.ExpandButtonVisible = true;
            this.layGrpAddress.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layGrpAddress.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForLabourName,
            this.ItemForLabourCost,
            this.ItemForPercentageOfValue,
            this.ItemForVisitTotalCost,
            this.layoutControlGroup5,
            this.emptySpaceItem5,
            this.ItemForJobCount,
            this.ItemForInternalExternal,
            this.ItemForLinkedToPersonType,
            this.emptySpaceItem4});
            this.layGrpAddress.Location = new System.Drawing.Point(0, 0);
            this.layGrpAddress.Name = "layGrpAddress";
            this.layGrpAddress.Size = new System.Drawing.Size(618, 200);
            this.layGrpAddress.Text = "Details";
            // 
            // ItemForLabourName
            // 
            this.ItemForLabourName.Control = this.LabourNameTextEdit;
            this.ItemForLabourName.Location = new System.Drawing.Point(0, 0);
            this.ItemForLabourName.Name = "ItemForLabourName";
            this.ItemForLabourName.Size = new System.Drawing.Size(618, 24);
            this.ItemForLabourName.Text = "Labour Name:";
            this.ItemForLabourName.TextSize = new System.Drawing.Size(125, 13);
            // 
            // ItemForLinkedToPersonType
            // 
            this.ItemForLinkedToPersonType.Control = this.LinkedToPersonTypeTextEdit;
            this.ItemForLinkedToPersonType.Location = new System.Drawing.Point(0, 24);
            this.ItemForLinkedToPersonType.MaxSize = new System.Drawing.Size(272, 24);
            this.ItemForLinkedToPersonType.MinSize = new System.Drawing.Size(272, 24);
            this.ItemForLinkedToPersonType.Name = "ItemForLinkedToPersonType";
            this.ItemForLinkedToPersonType.Size = new System.Drawing.Size(272, 24);
            this.ItemForLinkedToPersonType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForLinkedToPersonType.Text = "Linked Type:";
            this.ItemForLinkedToPersonType.TextSize = new System.Drawing.Size(125, 13);
            // 
            // ItemForInternalExternal
            // 
            this.ItemForInternalExternal.Control = this.InternalExternalTextEdit;
            this.ItemForInternalExternal.Location = new System.Drawing.Point(0, 48);
            this.ItemForInternalExternal.Name = "ItemForInternalExternal";
            this.ItemForInternalExternal.Size = new System.Drawing.Size(272, 24);
            this.ItemForInternalExternal.Text = "Internal \\ External:";
            this.ItemForInternalExternal.TextSize = new System.Drawing.Size(125, 13);
            // 
            // ItemForVisitTotalCost
            // 
            this.ItemForVisitTotalCost.Control = this.VisitTotalCostTextEdit;
            this.ItemForVisitTotalCost.Location = new System.Drawing.Point(0, 96);
            this.ItemForVisitTotalCost.Name = "ItemForVisitTotalCost";
            this.ItemForVisitTotalCost.Size = new System.Drawing.Size(272, 24);
            this.ItemForVisitTotalCost.Text = "Visit Total Cost:";
            this.ItemForVisitTotalCost.TextSize = new System.Drawing.Size(125, 13);
            // 
            // ItemForPercentageOfValue
            // 
            this.ItemForPercentageOfValue.Control = this.PercentageOfValueSpinEdit;
            this.ItemForPercentageOfValue.CustomizationFormText = "Percentage Of Visit Value:";
            this.ItemForPercentageOfValue.Location = new System.Drawing.Point(0, 120);
            this.ItemForPercentageOfValue.Name = "ItemForPercentageOfValue";
            this.ItemForPercentageOfValue.Size = new System.Drawing.Size(272, 24);
            this.ItemForPercentageOfValue.Text = "Percentage Of Visit Value:";
            this.ItemForPercentageOfValue.TextSize = new System.Drawing.Size(125, 13);
            // 
            // ItemForLabourCost
            // 
            this.ItemForLabourCost.Control = this.LabourCostTextEdit;
            this.ItemForLabourCost.Location = new System.Drawing.Point(0, 144);
            this.ItemForLabourCost.Name = "ItemForLabourCost";
            this.ItemForLabourCost.Size = new System.Drawing.Size(272, 24);
            this.ItemForLabourCost.Text = "Calculated Labour Cost:";
            this.ItemForLabourCost.TextSize = new System.Drawing.Size(125, 13);
            // 
            // ItemForJobCount
            // 
            this.ItemForJobCount.Control = this.JobCountTextEdit;
            this.ItemForJobCount.Location = new System.Drawing.Point(0, 72);
            this.ItemForJobCount.Name = "ItemForJobCount";
            this.ItemForJobCount.Size = new System.Drawing.Size(272, 24);
            this.ItemForJobCount.Text = "Job Count:";
            this.ItemForJobCount.TextSize = new System.Drawing.Size(125, 13);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.Location = new System.Drawing.Point(246, 0);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(76, 48);
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup4.CaptionImage")));
            this.layoutControlGroup4.CustomizationFormText = "Remarks";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(618, 200);
            this.layoutControlGroup4.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Comments:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(618, 200);
            this.ItemForRemarks.Text = "Comments:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // ItemForSiteName
            // 
            this.ItemForSiteName.Control = this.SiteNameTextEdit;
            this.ItemForSiteName.CustomizationFormText = "Site Name:";
            this.ItemForSiteName.Location = new System.Drawing.Point(0, 47);
            this.ItemForSiteName.Name = "ItemForSiteName";
            this.ItemForSiteName.Size = new System.Drawing.Size(666, 24);
            this.ItemForSiteName.Text = "Site Name:";
            this.ItemForSiteName.TextSize = new System.Drawing.Size(125, 13);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 119);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(0, 11);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(10, 11);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(666, 11);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForClientName
            // 
            this.ItemForClientName.Control = this.ClientNameTextEdit;
            this.ItemForClientName.CustomizationFormText = "Client Name:";
            this.ItemForClientName.Location = new System.Drawing.Point(0, 23);
            this.ItemForClientName.Name = "ItemForClientName";
            this.ItemForClientName.Size = new System.Drawing.Size(666, 24);
            this.ItemForClientName.Text = "Client Name:";
            this.ItemForClientName.TextSize = new System.Drawing.Size(125, 13);
            // 
            // ItemForVisitNumber
            // 
            this.ItemForVisitNumber.Control = this.VisitNumberTextEdit;
            this.ItemForVisitNumber.CustomizationFormText = "Visit Number:";
            this.ItemForVisitNumber.Location = new System.Drawing.Point(0, 71);
            this.ItemForVisitNumber.Name = "ItemForVisitNumber";
            this.ItemForVisitNumber.Size = new System.Drawing.Size(666, 24);
            this.ItemForVisitNumber.Text = "Visit Number:";
            this.ItemForVisitNumber.TextSize = new System.Drawing.Size(125, 13);
            // 
            // ItemForCostCalculationLevelDescription
            // 
            this.ItemForCostCalculationLevelDescription.Control = this.CostCalculationLevelDescriptionTextEdit;
            this.ItemForCostCalculationLevelDescription.Location = new System.Drawing.Point(0, 95);
            this.ItemForCostCalculationLevelDescription.Name = "ItemForCostCalculationLevelDescription";
            this.ItemForCostCalculationLevelDescription.Size = new System.Drawing.Size(666, 24);
            this.ItemForCostCalculationLevelDescription.Text = "Cost Calculation Level:";
            this.ItemForCostCalculationLevelDescription.TextSize = new System.Drawing.Size(125, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AllowDrawBackground = false;
            this.layoutControlGroup3.CustomizationFormText = "autoGeneratedGroup1";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem3});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 424);
            this.layoutControlGroup3.Name = "autoGeneratedGroup1";
            this.layoutControlGroup3.Size = new System.Drawing.Size(666, 139);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(666, 139);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp06359_OM_Visit_Labour_EditTableAdapter
            // 
            this.sp06359_OM_Visit_Labour_EditTableAdapter.ClearBeforeFill = true;
            // 
            // ItemForSelfBillingInvoiceHeaderID
            // 
            this.ItemForSelfBillingInvoiceHeaderID.Control = this.SelfBillingInvoiceHeaderIDSpinEdit;
            this.ItemForSelfBillingInvoiceHeaderID.Location = new System.Drawing.Point(0, 0);
            this.ItemForSelfBillingInvoiceHeaderID.MaxSize = new System.Drawing.Size(246, 24);
            this.ItemForSelfBillingInvoiceHeaderID.MinSize = new System.Drawing.Size(246, 24);
            this.ItemForSelfBillingInvoiceHeaderID.Name = "ItemForSelfBillingInvoiceHeaderID";
            this.ItemForSelfBillingInvoiceHeaderID.Size = new System.Drawing.Size(246, 24);
            this.ItemForSelfBillingInvoiceHeaderID.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForSelfBillingInvoiceHeaderID.Text = "Invoice Header ID:";
            this.ItemForSelfBillingInvoiceHeaderID.TextSize = new System.Drawing.Size(125, 13);
            // 
            // SelfBillingInvoiceHeaderIDSpinEdit
            // 
            this.SelfBillingInvoiceHeaderIDSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06359OMVisitLabourEditBindingSource, "SelfBillingInvoiceHeaderID", true));
            this.SelfBillingInvoiceHeaderIDSpinEdit.Location = new System.Drawing.Point(448, 270);
            this.SelfBillingInvoiceHeaderIDSpinEdit.MenuManager = this.barManager1;
            this.SelfBillingInvoiceHeaderIDSpinEdit.Name = "SelfBillingInvoiceHeaderIDSpinEdit";
            this.SelfBillingInvoiceHeaderIDSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SelfBillingInvoiceHeaderIDSpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.SelfBillingInvoiceHeaderIDSpinEdit.Properties.IsFloatValue = false;
            this.SelfBillingInvoiceHeaderIDSpinEdit.Properties.Mask.EditMask = "f0";
            this.SelfBillingInvoiceHeaderIDSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SelfBillingInvoiceHeaderIDSpinEdit.Size = new System.Drawing.Size(114, 20);
            this.SelfBillingInvoiceHeaderIDSpinEdit.StyleController = this.dataLayoutControl1;
            this.SelfBillingInvoiceHeaderIDSpinEdit.TabIndex = 79;
            // 
            // SelfBillingAmountSpinEdit
            // 
            this.SelfBillingAmountSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06359OMVisitLabourEditBindingSource, "SelfBillingAmount", true));
            this.SelfBillingAmountSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SelfBillingAmountSpinEdit.Location = new System.Drawing.Point(448, 294);
            this.SelfBillingAmountSpinEdit.MenuManager = this.barManager1;
            this.SelfBillingAmountSpinEdit.Name = "SelfBillingAmountSpinEdit";
            this.SelfBillingAmountSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SelfBillingAmountSpinEdit.Properties.Mask.EditMask = "c";
            this.SelfBillingAmountSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SelfBillingAmountSpinEdit.Size = new System.Drawing.Size(114, 20);
            this.SelfBillingAmountSpinEdit.StyleController = this.dataLayoutControl1;
            this.SelfBillingAmountSpinEdit.TabIndex = 80;
            // 
            // ItemForSelfBillingAmount
            // 
            this.ItemForSelfBillingAmount.Control = this.SelfBillingAmountSpinEdit;
            this.ItemForSelfBillingAmount.Location = new System.Drawing.Point(0, 24);
            this.ItemForSelfBillingAmount.Name = "ItemForSelfBillingAmount";
            this.ItemForSelfBillingAmount.Size = new System.Drawing.Size(246, 24);
            this.ItemForSelfBillingAmount.Text = "Bill Amount:";
            this.ItemForSelfBillingAmount.TextSize = new System.Drawing.Size(125, 13);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.ExpandButtonVisible = true;
            this.layoutControlGroup5.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSelfBillingInvoiceHeaderID,
            this.emptySpaceItem9,
            this.ItemForSelfBillingAmount,
            this.ItemForSelfBillingComment});
            this.layoutControlGroup5.Location = new System.Drawing.Point(272, 24);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(346, 166);
            this.layoutControlGroup5.Text = "Team Self-Billing";
            // 
            // SelfBillingCommentMemoEdit
            // 
            this.SelfBillingCommentMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06359OMVisitLabourEditBindingSource, "SelfBillingComment", true));
            this.SelfBillingCommentMemoEdit.Location = new System.Drawing.Point(448, 318);
            this.SelfBillingCommentMemoEdit.MenuManager = this.barManager1;
            this.SelfBillingCommentMemoEdit.Name = "SelfBillingCommentMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.SelfBillingCommentMemoEdit, true);
            this.SelfBillingCommentMemoEdit.Size = new System.Drawing.Size(190, 68);
            this.scSpellChecker.SetSpellCheckerOptions(this.SelfBillingCommentMemoEdit, optionsSpelling1);
            this.SelfBillingCommentMemoEdit.StyleController = this.dataLayoutControl1;
            toolTipTitleItem4.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Text = "Self-Billing Comments - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Any value entered in me will be shown on the generated Team Self-Billing Invoice." +
    "";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.SelfBillingCommentMemoEdit.SuperTip = superToolTip4;
            this.SelfBillingCommentMemoEdit.TabIndex = 81;
            // 
            // ItemForSelfBillingComment
            // 
            this.ItemForSelfBillingComment.Control = this.SelfBillingCommentMemoEdit;
            this.ItemForSelfBillingComment.Location = new System.Drawing.Point(0, 48);
            this.ItemForSelfBillingComment.MaxSize = new System.Drawing.Size(0, 72);
            this.ItemForSelfBillingComment.MinSize = new System.Drawing.Size(142, 72);
            this.ItemForSelfBillingComment.Name = "ItemForSelfBillingComment";
            this.ItemForSelfBillingComment.Size = new System.Drawing.Size(322, 72);
            this.ItemForSelfBillingComment.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForSelfBillingComment.Text = "Comment:";
            this.ItemForSelfBillingComment.TextSize = new System.Drawing.Size(125, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(272, 190);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(346, 10);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 168);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(272, 32);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // frm_OM_Visit_Labour_Calculated_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(686, 639);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_OM_Visit_Labour_Calculated_Edit";
            this.Text = "Edit Visit Labour Calculated";
            this.Activated += new System.EventHandler(this.frm_OM_Visit_Labour_Calculated_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_OM_Visit_Labour_Calculated_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_OM_Visit_Labour_Calculated_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.JobCountTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06359OMVisitLabourEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Visit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabourCostTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitTotalCostTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InternalExternalTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToPersonTypeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabourNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostCalculationLevelDescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabourTypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabourIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitLabourCalculatedIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PercentageOfValueSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitLabourCalculatedID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLabourID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLabourTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLabourName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToPersonType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInternalExternal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitTotalCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPercentageOfValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLabourCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostCalculationLevelDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSelfBillingInvoiceHeaderID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelfBillingInvoiceHeaderIDSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelfBillingAmountSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSelfBillingAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelfBillingCommentMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSelfBillingComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraLayout.LayoutControlGroup layGrpAddress;
        private DevExpress.XtraEditors.SpinEdit PercentageOfValueSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPercentageOfValue;
        private DevExpress.XtraEditors.TextEdit SiteIDTextEdit;
        private DevExpress.XtraEditors.TextEdit ClientIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteID;
        private DevExpress.XtraEditors.TextEdit VisitIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitID;
        private DevExpress.XtraEditors.TextEdit SiteNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteName;
        private DevExpress.XtraEditors.TextEdit VisitNumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitNumber;
        private DevExpress.XtraEditors.TextEdit ClientNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientName;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DataSet_OM_Visit dataSet_OM_Visit;
        private DevExpress.XtraEditors.TextEdit VisitLabourCalculatedIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitLabourCalculatedID;
        private System.Windows.Forms.BindingSource sp06359OMVisitLabourEditBindingSource;
        private DataSet_OM_VisitTableAdapters.sp06359_OM_Visit_Labour_EditTableAdapter sp06359_OM_Visit_Labour_EditTableAdapter;
        private DevExpress.XtraEditors.TextEdit LabourIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLabourID;
        private DevExpress.XtraEditors.TextEdit LabourTypeIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLabourTypeID;
        private DevExpress.XtraEditors.TextEdit CostCalculationLevelDescriptionTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostCalculationLevelDescription;
        private DevExpress.XtraEditors.TextEdit LabourNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLabourName;
        private DevExpress.XtraEditors.TextEdit LinkedToPersonTypeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedToPersonType;
        private DevExpress.XtraEditors.TextEdit InternalExternalTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInternalExternal;
        private DevExpress.XtraEditors.TextEdit VisitTotalCostTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitTotalCost;
        private DevExpress.XtraEditors.TextEdit LabourCostTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLabourCost;
        private DevExpress.XtraEditors.TextEdit JobCountTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobCount;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraEditors.SpinEdit SelfBillingInvoiceHeaderIDSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSelfBillingInvoiceHeaderID;
        private DevExpress.XtraEditors.MemoEdit SelfBillingCommentMemoEdit;
        private DevExpress.XtraEditors.SpinEdit SelfBillingAmountSpinEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSelfBillingAmount;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSelfBillingComment;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
    }
}

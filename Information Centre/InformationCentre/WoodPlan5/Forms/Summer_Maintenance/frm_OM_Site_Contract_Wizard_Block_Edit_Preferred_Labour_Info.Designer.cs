﻿namespace WoodPlan5
{
    partial class frm_OM_Site_Contract_Wizard_Block_Edit_Preferred_Labour_Info
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Site_Contract_Wizard_Block_Edit_Preferred_Labour_Info));
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.LinkedToPersonTypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LinkedToPersonTypeButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.LocationYTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LocationXTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TeamPostcodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LatLongSiteDistanceSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.PostcodeSiteDistanceSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CISValueSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CISPercentageSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SellPerUnitVatRateSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CostPerUnitVatRateSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SellPerUnitExVatSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SellUnitDescriptorIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06101OMWorkUnitTypesPicklistBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Contract = new WoodPlan5.DataSet_OM_Contract();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CostUnitDescriptorIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ContractorIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ContractorNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.CostPerUnitExVatSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ItemForContractorID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTeamPostcode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLocationY = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTeamLocationX = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLinkedToPersonTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCostPerUnitExVat = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForContractorName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCostUnitDescriptorID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSellUnitDescriptorID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSellPerUnitExVat = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCostPerUnitVatRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSellPerUnitVatRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPostcodeSiteDistance = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCISValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCISPercentage = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLatLongSiteDistance = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.sp06101_OM_Work_Unit_Types_PicklistTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06101_OM_Work_Unit_Types_PicklistTableAdapter();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bsiInformation = new DevExpress.XtraBars.BarStaticItem();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToPersonTypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToPersonTypeButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LocationYTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LocationXTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeamPostcodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LatLongSiteDistanceSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PostcodeSiteDistanceSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CISValueSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CISPercentageSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellPerUnitVatRateSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostPerUnitVatRateSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellPerUnitExVatSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellUnitDescriptorIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06101OMWorkUnitTypesPicklistBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostUnitDescriptorIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractorIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractorNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostPerUnitExVatSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTeamPostcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLocationY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTeamLocationX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToPersonTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostPerUnitExVat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractorName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostUnitDescriptorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellUnitDescriptorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellPerUnitExVat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostPerUnitVatRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellPerUnitVatRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPostcodeSiteDistance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCISValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCISPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLatLongSiteDistance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(515, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 369);
            this.barDockControlBottom.Size = new System.Drawing.Size(515, 30);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 343);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(515, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 343);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bsiInformation});
            this.barManager1.MaxItemId = 31;
            this.barManager1.StatusBar = this.bar1;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "ID";
            this.gridColumn1.FieldName = "ID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn1.Width = 53;
            // 
            // colID1
            // 
            this.colID1.Caption = "ID";
            this.colID1.FieldName = "ID";
            this.colID1.Name = "colID1";
            this.colID1.OptionsColumn.AllowEdit = false;
            this.colID1.OptionsColumn.AllowFocus = false;
            this.colID1.OptionsColumn.ReadOnly = true;
            this.colID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colID1.Width = 53;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutControl1.Controls.Add(this.LinkedToPersonTypeIDTextEdit);
            this.layoutControl1.Controls.Add(this.LinkedToPersonTypeButtonEdit);
            this.layoutControl1.Controls.Add(this.LocationYTextEdit);
            this.layoutControl1.Controls.Add(this.LocationXTextEdit);
            this.layoutControl1.Controls.Add(this.TeamPostcodeTextEdit);
            this.layoutControl1.Controls.Add(this.LatLongSiteDistanceSpinEdit);
            this.layoutControl1.Controls.Add(this.PostcodeSiteDistanceSpinEdit);
            this.layoutControl1.Controls.Add(this.CISValueSpinEdit);
            this.layoutControl1.Controls.Add(this.CISPercentageSpinEdit);
            this.layoutControl1.Controls.Add(this.SellPerUnitVatRateSpinEdit);
            this.layoutControl1.Controls.Add(this.CostPerUnitVatRateSpinEdit);
            this.layoutControl1.Controls.Add(this.SellPerUnitExVatSpinEdit);
            this.layoutControl1.Controls.Add(this.SellUnitDescriptorIDGridLookUpEdit);
            this.layoutControl1.Controls.Add(this.CostUnitDescriptorIDGridLookUpEdit);
            this.layoutControl1.Controls.Add(this.ContractorIDTextEdit);
            this.layoutControl1.Controls.Add(this.ContractorNameButtonEdit);
            this.layoutControl1.Controls.Add(this.CostPerUnitExVatSpinEdit);
            this.layoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.layoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForContractorID,
            this.ItemForTeamPostcode,
            this.ItemForLocationY,
            this.ItemForTeamLocationX,
            this.ItemForLinkedToPersonTypeID});
            this.layoutControl1.Location = new System.Drawing.Point(0, 28);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1213, 293, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(514, 309);
            this.layoutControl1.TabIndex = 4;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // LinkedToPersonTypeIDTextEdit
            // 
            this.LinkedToPersonTypeIDTextEdit.Location = new System.Drawing.Point(141, 277);
            this.LinkedToPersonTypeIDTextEdit.MenuManager = this.barManager1;
            this.LinkedToPersonTypeIDTextEdit.Name = "LinkedToPersonTypeIDTextEdit";
            this.LinkedToPersonTypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LinkedToPersonTypeIDTextEdit, true);
            this.LinkedToPersonTypeIDTextEdit.Size = new System.Drawing.Size(361, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LinkedToPersonTypeIDTextEdit, optionsSpelling1);
            this.LinkedToPersonTypeIDTextEdit.StyleController = this.layoutControl1;
            this.LinkedToPersonTypeIDTextEdit.TabIndex = 35;
            // 
            // LinkedToPersonTypeButtonEdit
            // 
            this.LinkedToPersonTypeButtonEdit.EditValue = "";
            this.LinkedToPersonTypeButtonEdit.Location = new System.Drawing.Point(132, 12);
            this.LinkedToPersonTypeButtonEdit.MenuManager = this.barManager1;
            this.LinkedToPersonTypeButtonEdit.Name = "LinkedToPersonTypeButtonEdit";
            this.LinkedToPersonTypeButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "Click me to Open Select Person Type screen", "choose", null, true)});
            this.LinkedToPersonTypeButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.LinkedToPersonTypeButtonEdit.Size = new System.Drawing.Size(370, 20);
            this.LinkedToPersonTypeButtonEdit.StyleController = this.layoutControl1;
            this.LinkedToPersonTypeButtonEdit.TabIndex = 15;
            this.LinkedToPersonTypeButtonEdit.TabStop = false;
            this.LinkedToPersonTypeButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.LinkedToPersonTypeButtonEdit_ButtonClick);
            // 
            // LocationYTextEdit
            // 
            this.LocationYTextEdit.Location = new System.Drawing.Point(132, 12);
            this.LocationYTextEdit.MenuManager = this.barManager1;
            this.LocationYTextEdit.Name = "LocationYTextEdit";
            this.LocationYTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LocationYTextEdit, true);
            this.LocationYTextEdit.Size = new System.Drawing.Size(370, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LocationYTextEdit, optionsSpelling2);
            this.LocationYTextEdit.StyleController = this.layoutControl1;
            this.LocationYTextEdit.TabIndex = 34;
            // 
            // LocationXTextEdit
            // 
            this.LocationXTextEdit.Location = new System.Drawing.Point(132, 12);
            this.LocationXTextEdit.MenuManager = this.barManager1;
            this.LocationXTextEdit.Name = "LocationXTextEdit";
            this.LocationXTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LocationXTextEdit, true);
            this.LocationXTextEdit.Size = new System.Drawing.Size(370, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LocationXTextEdit, optionsSpelling3);
            this.LocationXTextEdit.StyleController = this.layoutControl1;
            this.LocationXTextEdit.TabIndex = 32;
            // 
            // TeamPostcodeTextEdit
            // 
            this.TeamPostcodeTextEdit.Location = new System.Drawing.Point(132, 12);
            this.TeamPostcodeTextEdit.MenuManager = this.barManager1;
            this.TeamPostcodeTextEdit.Name = "TeamPostcodeTextEdit";
            this.TeamPostcodeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.TeamPostcodeTextEdit, true);
            this.TeamPostcodeTextEdit.Size = new System.Drawing.Size(370, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.TeamPostcodeTextEdit, optionsSpelling4);
            this.TeamPostcodeTextEdit.StyleController = this.layoutControl1;
            this.TeamPostcodeTextEdit.TabIndex = 31;
            // 
            // LatLongSiteDistanceSpinEdit
            // 
            this.LatLongSiteDistanceSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.LatLongSiteDistanceSpinEdit.Location = new System.Drawing.Point(383, 180);
            this.LatLongSiteDistanceSpinEdit.MenuManager = this.barManager1;
            this.LatLongSiteDistanceSpinEdit.Name = "LatLongSiteDistanceSpinEdit";
            this.LatLongSiteDistanceSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LatLongSiteDistanceSpinEdit.Properties.Mask.EditMask = "######0.00 Miles";
            this.LatLongSiteDistanceSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.LatLongSiteDistanceSpinEdit.Size = new System.Drawing.Size(119, 20);
            this.LatLongSiteDistanceSpinEdit.StyleController = this.layoutControl1;
            this.LatLongSiteDistanceSpinEdit.TabIndex = 10;
            // 
            // PostcodeSiteDistanceSpinEdit
            // 
            this.PostcodeSiteDistanceSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.PostcodeSiteDistanceSpinEdit.Location = new System.Drawing.Point(132, 180);
            this.PostcodeSiteDistanceSpinEdit.MenuManager = this.barManager1;
            this.PostcodeSiteDistanceSpinEdit.Name = "PostcodeSiteDistanceSpinEdit";
            this.PostcodeSiteDistanceSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PostcodeSiteDistanceSpinEdit.Properties.Mask.EditMask = "######0.00 Miles";
            this.PostcodeSiteDistanceSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.PostcodeSiteDistanceSpinEdit.Size = new System.Drawing.Size(127, 20);
            this.PostcodeSiteDistanceSpinEdit.StyleController = this.layoutControl1;
            this.PostcodeSiteDistanceSpinEdit.TabIndex = 9;
            // 
            // CISValueSpinEdit
            // 
            this.CISValueSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CISValueSpinEdit.Location = new System.Drawing.Point(132, 156);
            this.CISValueSpinEdit.MenuManager = this.barManager1;
            this.CISValueSpinEdit.Name = "CISValueSpinEdit";
            this.CISValueSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CISValueSpinEdit.Properties.Mask.EditMask = "c";
            this.CISValueSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CISValueSpinEdit.Size = new System.Drawing.Size(127, 20);
            this.CISValueSpinEdit.StyleController = this.layoutControl1;
            this.CISValueSpinEdit.TabIndex = 7;
            // 
            // CISPercentageSpinEdit
            // 
            this.CISPercentageSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CISPercentageSpinEdit.Location = new System.Drawing.Point(383, 156);
            this.CISPercentageSpinEdit.MenuManager = this.barManager1;
            this.CISPercentageSpinEdit.Name = "CISPercentageSpinEdit";
            this.CISPercentageSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CISPercentageSpinEdit.Properties.Mask.EditMask = "P";
            this.CISPercentageSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CISPercentageSpinEdit.Properties.MaxValue = new decimal(new int[] {
            10000,
            0,
            0,
            131072});
            this.CISPercentageSpinEdit.Size = new System.Drawing.Size(119, 20);
            this.CISPercentageSpinEdit.StyleController = this.layoutControl1;
            this.CISPercentageSpinEdit.TabIndex = 8;
            // 
            // SellPerUnitVatRateSpinEdit
            // 
            this.SellPerUnitVatRateSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SellPerUnitVatRateSpinEdit.Location = new System.Drawing.Point(383, 132);
            this.SellPerUnitVatRateSpinEdit.MenuManager = this.barManager1;
            this.SellPerUnitVatRateSpinEdit.Name = "SellPerUnitVatRateSpinEdit";
            this.SellPerUnitVatRateSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SellPerUnitVatRateSpinEdit.Properties.Mask.EditMask = "P";
            this.SellPerUnitVatRateSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SellPerUnitVatRateSpinEdit.Properties.MaxValue = new decimal(new int[] {
            10000,
            0,
            0,
            131072});
            this.SellPerUnitVatRateSpinEdit.Size = new System.Drawing.Size(119, 20);
            this.SellPerUnitVatRateSpinEdit.StyleController = this.layoutControl1;
            this.SellPerUnitVatRateSpinEdit.TabIndex = 6;
            // 
            // CostPerUnitVatRateSpinEdit
            // 
            this.CostPerUnitVatRateSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CostPerUnitVatRateSpinEdit.Location = new System.Drawing.Point(383, 108);
            this.CostPerUnitVatRateSpinEdit.MenuManager = this.barManager1;
            this.CostPerUnitVatRateSpinEdit.Name = "CostPerUnitVatRateSpinEdit";
            this.CostPerUnitVatRateSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CostPerUnitVatRateSpinEdit.Properties.Mask.EditMask = "P";
            this.CostPerUnitVatRateSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CostPerUnitVatRateSpinEdit.Properties.MaxValue = new decimal(new int[] {
            10000,
            0,
            0,
            131072});
            this.CostPerUnitVatRateSpinEdit.Size = new System.Drawing.Size(119, 20);
            this.CostPerUnitVatRateSpinEdit.StyleController = this.layoutControl1;
            this.CostPerUnitVatRateSpinEdit.TabIndex = 4;
            // 
            // SellPerUnitExVatSpinEdit
            // 
            this.SellPerUnitExVatSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SellPerUnitExVatSpinEdit.Location = new System.Drawing.Point(132, 132);
            this.SellPerUnitExVatSpinEdit.MenuManager = this.barManager1;
            this.SellPerUnitExVatSpinEdit.Name = "SellPerUnitExVatSpinEdit";
            this.SellPerUnitExVatSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SellPerUnitExVatSpinEdit.Properties.Mask.EditMask = "c";
            this.SellPerUnitExVatSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SellPerUnitExVatSpinEdit.Size = new System.Drawing.Size(127, 20);
            this.SellPerUnitExVatSpinEdit.StyleController = this.layoutControl1;
            this.SellPerUnitExVatSpinEdit.TabIndex = 5;
            // 
            // SellUnitDescriptorIDGridLookUpEdit
            // 
            this.SellUnitDescriptorIDGridLookUpEdit.Location = new System.Drawing.Point(132, 84);
            this.SellUnitDescriptorIDGridLookUpEdit.MenuManager = this.barManager1;
            this.SellUnitDescriptorIDGridLookUpEdit.Name = "SellUnitDescriptorIDGridLookUpEdit";
            this.SellUnitDescriptorIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SellUnitDescriptorIDGridLookUpEdit.Properties.DataSource = this.sp06101OMWorkUnitTypesPicklistBindingSource;
            this.SellUnitDescriptorIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.SellUnitDescriptorIDGridLookUpEdit.Properties.NullText = "";
            this.SellUnitDescriptorIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.SellUnitDescriptorIDGridLookUpEdit.Properties.View = this.gridView1;
            this.SellUnitDescriptorIDGridLookUpEdit.Size = new System.Drawing.Size(370, 20);
            this.SellUnitDescriptorIDGridLookUpEdit.StyleController = this.layoutControl1;
            this.SellUnitDescriptorIDGridLookUpEdit.TabIndex = 2;
            // 
            // sp06101OMWorkUnitTypesPicklistBindingSource
            // 
            this.sp06101OMWorkUnitTypesPicklistBindingSource.DataMember = "sp06101_OM_Work_Unit_Types_Picklist";
            this.sp06101OMWorkUnitTypesPicklistBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // dataSet_OM_Contract
            // 
            this.dataSet_OM_Contract.DataSetName = "DataSet_OM_Contract";
            this.dataSet_OM_Contract.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.gridColumn1;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Unit Descriptor";
            this.gridColumn2.FieldName = "Description";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 220;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Order";
            this.gridColumn3.FieldName = "RecordOrder";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // CostUnitDescriptorIDGridLookUpEdit
            // 
            this.CostUnitDescriptorIDGridLookUpEdit.Location = new System.Drawing.Point(132, 60);
            this.CostUnitDescriptorIDGridLookUpEdit.MenuManager = this.barManager1;
            this.CostUnitDescriptorIDGridLookUpEdit.Name = "CostUnitDescriptorIDGridLookUpEdit";
            this.CostUnitDescriptorIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CostUnitDescriptorIDGridLookUpEdit.Properties.DataSource = this.sp06101OMWorkUnitTypesPicklistBindingSource;
            this.CostUnitDescriptorIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.CostUnitDescriptorIDGridLookUpEdit.Properties.NullText = "";
            this.CostUnitDescriptorIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.CostUnitDescriptorIDGridLookUpEdit.Properties.View = this.gridLookUpEdit1View;
            this.CostUnitDescriptorIDGridLookUpEdit.Size = new System.Drawing.Size(370, 20);
            this.CostUnitDescriptorIDGridLookUpEdit.StyleController = this.layoutControl1;
            this.CostUnitDescriptorIDGridLookUpEdit.TabIndex = 1;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID1,
            this.colDescription,
            this.colOrder});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.colID1;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Unit Descriptor";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 220;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "RecordOrder";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            this.colOrder.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // ContractorIDTextEdit
            // 
            this.ContractorIDTextEdit.Location = new System.Drawing.Point(127, 36);
            this.ContractorIDTextEdit.MenuManager = this.barManager1;
            this.ContractorIDTextEdit.Name = "ContractorIDTextEdit";
            this.ContractorIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ContractorIDTextEdit, true);
            this.ContractorIDTextEdit.Size = new System.Drawing.Size(348, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ContractorIDTextEdit, optionsSpelling5);
            this.ContractorIDTextEdit.StyleController = this.layoutControl1;
            this.ContractorIDTextEdit.TabIndex = 23;
            // 
            // ContractorNameButtonEdit
            // 
            this.ContractorNameButtonEdit.Location = new System.Drawing.Point(132, 36);
            this.ContractorNameButtonEdit.MenuManager = this.barManager1;
            this.ContractorNameButtonEdit.Name = "ContractorNameButtonEdit";
            this.ContractorNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "Click me to open the Select Team screen", "choose", null, true)});
            this.ContractorNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ContractorNameButtonEdit.Size = new System.Drawing.Size(370, 20);
            this.ContractorNameButtonEdit.StyleController = this.layoutControl1;
            this.ContractorNameButtonEdit.TabIndex = 0;
            this.ContractorNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ContractorNameButtonEdit_ButtonClick);
            // 
            // CostPerUnitExVatSpinEdit
            // 
            this.CostPerUnitExVatSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CostPerUnitExVatSpinEdit.Location = new System.Drawing.Point(132, 108);
            this.CostPerUnitExVatSpinEdit.MenuManager = this.barManager1;
            this.CostPerUnitExVatSpinEdit.Name = "CostPerUnitExVatSpinEdit";
            this.CostPerUnitExVatSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CostPerUnitExVatSpinEdit.Properties.Mask.EditMask = "c";
            this.CostPerUnitExVatSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CostPerUnitExVatSpinEdit.Size = new System.Drawing.Size(127, 20);
            this.CostPerUnitExVatSpinEdit.StyleController = this.layoutControl1;
            this.CostPerUnitExVatSpinEdit.TabIndex = 3;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.Location = new System.Drawing.Point(132, 204);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.RemarksMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(370, 93);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling6);
            this.RemarksMemoEdit.StyleController = this.layoutControl1;
            this.RemarksMemoEdit.TabIndex = 11;
            // 
            // ItemForContractorID
            // 
            this.ItemForContractorID.Control = this.ContractorIDTextEdit;
            this.ItemForContractorID.CustomizationFormText = "Contractor ID:";
            this.ItemForContractorID.Location = new System.Drawing.Point(0, 24);
            this.ItemForContractorID.Name = "ItemForContractorID";
            this.ItemForContractorID.Size = new System.Drawing.Size(467, 24);
            this.ItemForContractorID.Text = "Contractor ID:";
            this.ItemForContractorID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForTeamPostcode
            // 
            this.ItemForTeamPostcode.Control = this.TeamPostcodeTextEdit;
            this.ItemForTeamPostcode.CustomizationFormText = "Team Postcode:";
            this.ItemForTeamPostcode.Location = new System.Drawing.Point(0, 0);
            this.ItemForTeamPostcode.Name = "ItemForTeamPostcode";
            this.ItemForTeamPostcode.Size = new System.Drawing.Size(494, 24);
            this.ItemForTeamPostcode.Text = "Team Postcode:";
            this.ItemForTeamPostcode.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForLocationY
            // 
            this.ItemForLocationY.Control = this.LocationYTextEdit;
            this.ItemForLocationY.CustomizationFormText = "Team Longitude:";
            this.ItemForLocationY.Location = new System.Drawing.Point(0, 0);
            this.ItemForLocationY.Name = "ItemForLocationY";
            this.ItemForLocationY.Size = new System.Drawing.Size(494, 24);
            this.ItemForLocationY.Text = "Team Longitude:";
            this.ItemForLocationY.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForTeamLocationX
            // 
            this.ItemForTeamLocationX.Control = this.LocationXTextEdit;
            this.ItemForTeamLocationX.CustomizationFormText = "Team Latitude:";
            this.ItemForTeamLocationX.Location = new System.Drawing.Point(0, 0);
            this.ItemForTeamLocationX.Name = "ItemForTeamLocationX";
            this.ItemForTeamLocationX.Size = new System.Drawing.Size(494, 24);
            this.ItemForTeamLocationX.Text = "Team Latitude:";
            this.ItemForTeamLocationX.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForLinkedToPersonTypeID
            // 
            this.ItemForLinkedToPersonTypeID.Control = this.LinkedToPersonTypeIDTextEdit;
            this.ItemForLinkedToPersonTypeID.Location = new System.Drawing.Point(0, 265);
            this.ItemForLinkedToPersonTypeID.Name = "ItemForLinkedToPersonTypeID";
            this.ItemForLinkedToPersonTypeID.Size = new System.Drawing.Size(494, 24);
            this.ItemForLinkedToPersonTypeID.Text = "Linked To Person Type ID:";
            this.ItemForLinkedToPersonTypeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks,
            this.ItemForCostPerUnitExVat,
            this.ItemForContractorName,
            this.ItemForCostUnitDescriptorID,
            this.ItemForSellUnitDescriptorID,
            this.ItemForSellPerUnitExVat,
            this.ItemForCostPerUnitVatRate,
            this.ItemForSellPerUnitVatRate,
            this.ItemForPostcodeSiteDistance,
            this.ItemForCISValue,
            this.ItemForCISPercentage,
            this.ItemForLatLongSiteDistance,
            this.layoutControlItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(514, 309);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 192);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(494, 97);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(117, 13);
            // 
            // ItemForCostPerUnitExVat
            // 
            this.ItemForCostPerUnitExVat.Control = this.CostPerUnitExVatSpinEdit;
            this.ItemForCostPerUnitExVat.CustomizationFormText = "Cost Per Unit Ex VAT:";
            this.ItemForCostPerUnitExVat.Location = new System.Drawing.Point(0, 96);
            this.ItemForCostPerUnitExVat.Name = "ItemForCostPerUnitExVat";
            this.ItemForCostPerUnitExVat.Size = new System.Drawing.Size(251, 24);
            this.ItemForCostPerUnitExVat.Text = "Cost Per Unit Ex VAT:";
            this.ItemForCostPerUnitExVat.TextSize = new System.Drawing.Size(117, 13);
            // 
            // ItemForContractorName
            // 
            this.ItemForContractorName.Control = this.ContractorNameButtonEdit;
            this.ItemForContractorName.CustomizationFormText = "Labour Name:";
            this.ItemForContractorName.Location = new System.Drawing.Point(0, 24);
            this.ItemForContractorName.Name = "ItemForContractorName";
            this.ItemForContractorName.Size = new System.Drawing.Size(494, 24);
            this.ItemForContractorName.Text = "Labour Name:";
            this.ItemForContractorName.TextSize = new System.Drawing.Size(117, 13);
            // 
            // ItemForCostUnitDescriptorID
            // 
            this.ItemForCostUnitDescriptorID.Control = this.CostUnitDescriptorIDGridLookUpEdit;
            this.ItemForCostUnitDescriptorID.CustomizationFormText = "Cost Unit Descriptor:";
            this.ItemForCostUnitDescriptorID.Location = new System.Drawing.Point(0, 48);
            this.ItemForCostUnitDescriptorID.Name = "ItemForCostUnitDescriptorID";
            this.ItemForCostUnitDescriptorID.Size = new System.Drawing.Size(494, 24);
            this.ItemForCostUnitDescriptorID.Text = "Cost Unit Descriptor:";
            this.ItemForCostUnitDescriptorID.TextSize = new System.Drawing.Size(117, 13);
            // 
            // ItemForSellUnitDescriptorID
            // 
            this.ItemForSellUnitDescriptorID.Control = this.SellUnitDescriptorIDGridLookUpEdit;
            this.ItemForSellUnitDescriptorID.CustomizationFormText = "Sell Unit Descriptor:";
            this.ItemForSellUnitDescriptorID.Location = new System.Drawing.Point(0, 72);
            this.ItemForSellUnitDescriptorID.Name = "ItemForSellUnitDescriptorID";
            this.ItemForSellUnitDescriptorID.Size = new System.Drawing.Size(494, 24);
            this.ItemForSellUnitDescriptorID.Text = "Sell Unit Descriptor:";
            this.ItemForSellUnitDescriptorID.TextSize = new System.Drawing.Size(117, 13);
            // 
            // ItemForSellPerUnitExVat
            // 
            this.ItemForSellPerUnitExVat.Control = this.SellPerUnitExVatSpinEdit;
            this.ItemForSellPerUnitExVat.CustomizationFormText = "Sell Per Unit Ex VAT:";
            this.ItemForSellPerUnitExVat.Location = new System.Drawing.Point(0, 120);
            this.ItemForSellPerUnitExVat.Name = "ItemForSellPerUnitExVat";
            this.ItemForSellPerUnitExVat.Size = new System.Drawing.Size(251, 24);
            this.ItemForSellPerUnitExVat.Text = "Sell Per Unit Ex VAT:";
            this.ItemForSellPerUnitExVat.TextSize = new System.Drawing.Size(117, 13);
            // 
            // ItemForCostPerUnitVatRate
            // 
            this.ItemForCostPerUnitVatRate.Control = this.CostPerUnitVatRateSpinEdit;
            this.ItemForCostPerUnitVatRate.CustomizationFormText = "Cost Per Unit VAT Rate:";
            this.ItemForCostPerUnitVatRate.Location = new System.Drawing.Point(251, 96);
            this.ItemForCostPerUnitVatRate.Name = "ItemForCostPerUnitVatRate";
            this.ItemForCostPerUnitVatRate.Size = new System.Drawing.Size(243, 24);
            this.ItemForCostPerUnitVatRate.Text = "Cost Per Unit VAT Rate:";
            this.ItemForCostPerUnitVatRate.TextSize = new System.Drawing.Size(117, 13);
            // 
            // ItemForSellPerUnitVatRate
            // 
            this.ItemForSellPerUnitVatRate.Control = this.SellPerUnitVatRateSpinEdit;
            this.ItemForSellPerUnitVatRate.CustomizationFormText = "Sell Per Unit VAT Rate:";
            this.ItemForSellPerUnitVatRate.Location = new System.Drawing.Point(251, 120);
            this.ItemForSellPerUnitVatRate.Name = "ItemForSellPerUnitVatRate";
            this.ItemForSellPerUnitVatRate.Size = new System.Drawing.Size(243, 24);
            this.ItemForSellPerUnitVatRate.Text = "Sell Per Unit VAT Rate:";
            this.ItemForSellPerUnitVatRate.TextSize = new System.Drawing.Size(117, 13);
            // 
            // ItemForPostcodeSiteDistance
            // 
            this.ItemForPostcodeSiteDistance.Control = this.PostcodeSiteDistanceSpinEdit;
            this.ItemForPostcodeSiteDistance.CustomizationFormText = "Site Postcode Distance:";
            this.ItemForPostcodeSiteDistance.Location = new System.Drawing.Point(0, 168);
            this.ItemForPostcodeSiteDistance.Name = "ItemForPostcodeSiteDistance";
            this.ItemForPostcodeSiteDistance.Size = new System.Drawing.Size(251, 24);
            this.ItemForPostcodeSiteDistance.Text = "Site Postcode Distance:";
            this.ItemForPostcodeSiteDistance.TextSize = new System.Drawing.Size(117, 13);
            // 
            // ItemForCISValue
            // 
            this.ItemForCISValue.Control = this.CISValueSpinEdit;
            this.ItemForCISValue.CustomizationFormText = "CIS Value:";
            this.ItemForCISValue.Location = new System.Drawing.Point(0, 144);
            this.ItemForCISValue.Name = "ItemForCISValue";
            this.ItemForCISValue.Size = new System.Drawing.Size(251, 24);
            this.ItemForCISValue.Text = "CIS Value:";
            this.ItemForCISValue.TextSize = new System.Drawing.Size(117, 13);
            // 
            // ItemForCISPercentage
            // 
            this.ItemForCISPercentage.Control = this.CISPercentageSpinEdit;
            this.ItemForCISPercentage.CustomizationFormText = "CIS %:";
            this.ItemForCISPercentage.Location = new System.Drawing.Point(251, 144);
            this.ItemForCISPercentage.Name = "ItemForCISPercentage";
            this.ItemForCISPercentage.Size = new System.Drawing.Size(243, 24);
            this.ItemForCISPercentage.Text = "CIS %:";
            this.ItemForCISPercentage.TextSize = new System.Drawing.Size(117, 13);
            // 
            // ItemForLatLongSiteDistance
            // 
            this.ItemForLatLongSiteDistance.Control = this.LatLongSiteDistanceSpinEdit;
            this.ItemForLatLongSiteDistance.CustomizationFormText = "Site Lat\\Long Distance:";
            this.ItemForLatLongSiteDistance.Location = new System.Drawing.Point(251, 168);
            this.ItemForLatLongSiteDistance.Name = "ItemForLatLongSiteDistance";
            this.ItemForLatLongSiteDistance.Size = new System.Drawing.Size(243, 24);
            this.ItemForLatLongSiteDistance.Text = "Site Lat \\ Long Distance:";
            this.ItemForLatLongSiteDistance.TextSize = new System.Drawing.Size(117, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.LinkedToPersonTypeButtonEdit;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(494, 24);
            this.layoutControlItem1.Text = "Labour Type:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(117, 13);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(295, 337);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(96, 22);
            this.btnOK.TabIndex = 13;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(406, 337);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(96, 22);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // sp06101_OM_Work_Unit_Types_PicklistTableAdapter
            // 
            this.sp06101_OM_Work_Unit_Types_PicklistTableAdapter.ClearBeforeFill = true;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 3";
            this.bar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiInformation)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 3";
            // 
            // bsiInformation
            // 
            this.bsiInformation.Caption = "Complete the fields displayed. Any fields left blank will not be overwritten.";
            this.bsiInformation.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiInformation.Glyph")));
            this.bsiInformation.Id = 30;
            this.bsiInformation.Name = "bsiInformation";
            this.bsiInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bsiInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // frm_OM_Site_Contract_Wizard_Block_Edit_Preferred_Labour_Info
            // 
            this.ClientSize = new System.Drawing.Size(515, 399);
            this.ControlBox = false;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_OM_Site_Contract_Wizard_Block_Edit_Preferred_Labour_Info";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Contract Wizard - Block Edit Preferred Labour";
            this.Load += new System.EventHandler(this.frm_OM_Site_Contract_Wizard_Block_Edit_Preferred_Labour_Info_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToPersonTypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToPersonTypeButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LocationYTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LocationXTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeamPostcodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LatLongSiteDistanceSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PostcodeSiteDistanceSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CISValueSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CISPercentageSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellPerUnitVatRateSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostPerUnitVatRateSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellPerUnitExVatSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellUnitDescriptorIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06101OMWorkUnitTypesPicklistBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostUnitDescriptorIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractorIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractorNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostPerUnitExVatSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTeamPostcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLocationY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTeamLocationX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToPersonTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostPerUnitExVat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractorName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostUnitDescriptorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellUnitDescriptorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellPerUnitExVat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostPerUnitVatRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellPerUnitVatRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPostcodeSiteDistance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCISValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCISPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLatLongSiteDistance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraEditors.SpinEdit CostPerUnitExVatSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostPerUnitExVat;
        private DevExpress.XtraEditors.TextEdit ContractorIDTextEdit;
        private DevExpress.XtraEditors.ButtonEdit ContractorNameButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContractorID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContractorName;
        private DevExpress.XtraEditors.GridLookUpEdit CostUnitDescriptorIDGridLookUpEdit;
        private System.Windows.Forms.BindingSource sp06101OMWorkUnitTypesPicklistBindingSource;
        private DataSet_OM_Contract dataSet_OM_Contract;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn colID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostUnitDescriptorID;
        private DataSet_OM_ContractTableAdapters.sp06101_OM_Work_Unit_Types_PicklistTableAdapter sp06101_OM_Work_Unit_Types_PicklistTableAdapter;
        private DevExpress.XtraEditors.GridLookUpEdit SellUnitDescriptorIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSellUnitDescriptorID;
        private DevExpress.XtraEditors.SpinEdit SellPerUnitExVatSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSellPerUnitExVat;
        private DevExpress.XtraEditors.SpinEdit CISPercentageSpinEdit;
        private DevExpress.XtraEditors.SpinEdit SellPerUnitVatRateSpinEdit;
        private DevExpress.XtraEditors.SpinEdit CostPerUnitVatRateSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCISPercentage;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostPerUnitVatRate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSellPerUnitVatRate;
        private DevExpress.XtraEditors.SpinEdit CISValueSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCISValue;
        private DevExpress.XtraEditors.SpinEdit PostcodeSiteDistanceSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPostcodeSiteDistance;
        private DevExpress.XtraEditors.SpinEdit LatLongSiteDistanceSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLatLongSiteDistance;
        private DevExpress.XtraEditors.TextEdit LocationYTextEdit;
        private DevExpress.XtraEditors.TextEdit LocationXTextEdit;
        private DevExpress.XtraEditors.TextEdit TeamPostcodeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTeamPostcode;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTeamLocationX;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLocationY;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarStaticItem bsiInformation;
        private DevExpress.XtraEditors.ButtonEdit LinkedToPersonTypeButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.TextEdit LinkedToPersonTypeIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedToPersonTypeID;
    }
}

﻿namespace WoodPlan5
{
    partial class frm_OM_Site_Contract_Wizard_Block_Edit_Default_Equipment_Info
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Site_Contract_Wizard_Block_Edit_Default_Equipment_Info));
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.OwnerTypeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.OwnerNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SellPerUnitVatRateSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CostPerUnitVatRateSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SellPerUnitExVatSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SellUnitDescriptorIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06101OMWorkUnitTypesPicklistBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Contract = new WoodPlan5.DataSet_OM_Contract();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CostUnitDescriptorIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.EquipmentIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.EquipmentTypeButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.CostPerUnitExVatSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ItemForEquipmentID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOwnerName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOwnerType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCostPerUnitExVat = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEquipmentType = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCostUnitDescriptorID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSellUnitDescriptorID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSellPerUnitExVat = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCostPerUnitVatRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSellPerUnitVatRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.sp06101_OM_Work_Unit_Types_PicklistTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06101_OM_Work_Unit_Types_PicklistTableAdapter();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bsiInformation = new DevExpress.XtraBars.BarStaticItem();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OwnerTypeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OwnerNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellPerUnitVatRateSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostPerUnitVatRateSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellPerUnitExVatSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellUnitDescriptorIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06101OMWorkUnitTypesPicklistBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostUnitDescriptorIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EquipmentIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EquipmentTypeButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostPerUnitExVatSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipmentID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOwnerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOwnerType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostPerUnitExVat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipmentType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostUnitDescriptorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellUnitDescriptorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellPerUnitExVat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostPerUnitVatRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellPerUnitVatRate)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(515, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 359);
            this.barDockControlBottom.Size = new System.Drawing.Size(515, 30);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 333);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(515, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 333);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bsiInformation});
            this.barManager1.MaxItemId = 31;
            this.barManager1.StatusBar = this.bar1;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "ID";
            this.gridColumn1.FieldName = "ID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn1.Width = 53;
            // 
            // colID1
            // 
            this.colID1.Caption = "ID";
            this.colID1.FieldName = "ID";
            this.colID1.Name = "colID1";
            this.colID1.OptionsColumn.AllowEdit = false;
            this.colID1.OptionsColumn.AllowFocus = false;
            this.colID1.OptionsColumn.ReadOnly = true;
            this.colID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colID1.Width = 53;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutControl1.Controls.Add(this.OwnerTypeTextEdit);
            this.layoutControl1.Controls.Add(this.OwnerNameTextEdit);
            this.layoutControl1.Controls.Add(this.SellPerUnitVatRateSpinEdit);
            this.layoutControl1.Controls.Add(this.CostPerUnitVatRateSpinEdit);
            this.layoutControl1.Controls.Add(this.SellPerUnitExVatSpinEdit);
            this.layoutControl1.Controls.Add(this.SellUnitDescriptorIDGridLookUpEdit);
            this.layoutControl1.Controls.Add(this.CostUnitDescriptorIDGridLookUpEdit);
            this.layoutControl1.Controls.Add(this.EquipmentIDTextEdit);
            this.layoutControl1.Controls.Add(this.EquipmentTypeButtonEdit);
            this.layoutControl1.Controls.Add(this.CostPerUnitExVatSpinEdit);
            this.layoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.layoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForEquipmentID,
            this.ItemForOwnerName,
            this.ItemForOwnerType});
            this.layoutControl1.Location = new System.Drawing.Point(0, 28);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1213, 293, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(514, 299);
            this.layoutControl1.TabIndex = 4;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // OwnerTypeTextEdit
            // 
            this.OwnerTypeTextEdit.Location = new System.Drawing.Point(132, 12);
            this.OwnerTypeTextEdit.MenuManager = this.barManager1;
            this.OwnerTypeTextEdit.Name = "OwnerTypeTextEdit";
            this.OwnerTypeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.OwnerTypeTextEdit, true);
            this.OwnerTypeTextEdit.Size = new System.Drawing.Size(370, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.OwnerTypeTextEdit, optionsSpelling1);
            this.OwnerTypeTextEdit.StyleController = this.layoutControl1;
            this.OwnerTypeTextEdit.TabIndex = 25;
            // 
            // OwnerNameTextEdit
            // 
            this.OwnerNameTextEdit.Location = new System.Drawing.Point(132, 12);
            this.OwnerNameTextEdit.MenuManager = this.barManager1;
            this.OwnerNameTextEdit.Name = "OwnerNameTextEdit";
            this.OwnerNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.OwnerNameTextEdit, true);
            this.OwnerNameTextEdit.Size = new System.Drawing.Size(370, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.OwnerNameTextEdit, optionsSpelling2);
            this.OwnerNameTextEdit.StyleController = this.layoutControl1;
            this.OwnerNameTextEdit.TabIndex = 24;
            // 
            // SellPerUnitVatRateSpinEdit
            // 
            this.SellPerUnitVatRateSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SellPerUnitVatRateSpinEdit.Location = new System.Drawing.Point(381, 108);
            this.SellPerUnitVatRateSpinEdit.MenuManager = this.barManager1;
            this.SellPerUnitVatRateSpinEdit.Name = "SellPerUnitVatRateSpinEdit";
            this.SellPerUnitVatRateSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SellPerUnitVatRateSpinEdit.Properties.Mask.EditMask = "P";
            this.SellPerUnitVatRateSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SellPerUnitVatRateSpinEdit.Properties.MaxValue = new decimal(new int[] {
            10000,
            0,
            0,
            131072});
            this.SellPerUnitVatRateSpinEdit.Size = new System.Drawing.Size(121, 20);
            this.SellPerUnitVatRateSpinEdit.StyleController = this.layoutControl1;
            this.SellPerUnitVatRateSpinEdit.TabIndex = 6;
            // 
            // CostPerUnitVatRateSpinEdit
            // 
            this.CostPerUnitVatRateSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CostPerUnitVatRateSpinEdit.Location = new System.Drawing.Point(381, 84);
            this.CostPerUnitVatRateSpinEdit.MenuManager = this.barManager1;
            this.CostPerUnitVatRateSpinEdit.Name = "CostPerUnitVatRateSpinEdit";
            this.CostPerUnitVatRateSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CostPerUnitVatRateSpinEdit.Properties.Mask.EditMask = "P";
            this.CostPerUnitVatRateSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CostPerUnitVatRateSpinEdit.Properties.MaxValue = new decimal(new int[] {
            10000,
            0,
            0,
            131072});
            this.CostPerUnitVatRateSpinEdit.Size = new System.Drawing.Size(121, 20);
            this.CostPerUnitVatRateSpinEdit.StyleController = this.layoutControl1;
            this.CostPerUnitVatRateSpinEdit.TabIndex = 4;
            // 
            // SellPerUnitExVatSpinEdit
            // 
            this.SellPerUnitExVatSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SellPerUnitExVatSpinEdit.Location = new System.Drawing.Point(130, 108);
            this.SellPerUnitExVatSpinEdit.MenuManager = this.barManager1;
            this.SellPerUnitExVatSpinEdit.Name = "SellPerUnitExVatSpinEdit";
            this.SellPerUnitExVatSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SellPerUnitExVatSpinEdit.Properties.Mask.EditMask = "c";
            this.SellPerUnitExVatSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SellPerUnitExVatSpinEdit.Size = new System.Drawing.Size(129, 20);
            this.SellPerUnitExVatSpinEdit.StyleController = this.layoutControl1;
            this.SellPerUnitExVatSpinEdit.TabIndex = 5;
            // 
            // SellUnitDescriptorIDGridLookUpEdit
            // 
            this.SellUnitDescriptorIDGridLookUpEdit.Location = new System.Drawing.Point(130, 60);
            this.SellUnitDescriptorIDGridLookUpEdit.MenuManager = this.barManager1;
            this.SellUnitDescriptorIDGridLookUpEdit.Name = "SellUnitDescriptorIDGridLookUpEdit";
            this.SellUnitDescriptorIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SellUnitDescriptorIDGridLookUpEdit.Properties.DataSource = this.sp06101OMWorkUnitTypesPicklistBindingSource;
            this.SellUnitDescriptorIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.SellUnitDescriptorIDGridLookUpEdit.Properties.NullText = "";
            this.SellUnitDescriptorIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.SellUnitDescriptorIDGridLookUpEdit.Properties.View = this.gridView1;
            this.SellUnitDescriptorIDGridLookUpEdit.Size = new System.Drawing.Size(372, 20);
            this.SellUnitDescriptorIDGridLookUpEdit.StyleController = this.layoutControl1;
            this.SellUnitDescriptorIDGridLookUpEdit.TabIndex = 2;
            // 
            // sp06101OMWorkUnitTypesPicklistBindingSource
            // 
            this.sp06101OMWorkUnitTypesPicklistBindingSource.DataMember = "sp06101_OM_Work_Unit_Types_Picklist";
            this.sp06101OMWorkUnitTypesPicklistBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // dataSet_OM_Contract
            // 
            this.dataSet_OM_Contract.DataSetName = "DataSet_OM_Contract";
            this.dataSet_OM_Contract.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.gridColumn1;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Unit Descriptor";
            this.gridColumn2.FieldName = "Description";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 220;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Order";
            this.gridColumn3.FieldName = "RecordOrder";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // CostUnitDescriptorIDGridLookUpEdit
            // 
            this.CostUnitDescriptorIDGridLookUpEdit.Location = new System.Drawing.Point(130, 36);
            this.CostUnitDescriptorIDGridLookUpEdit.MenuManager = this.barManager1;
            this.CostUnitDescriptorIDGridLookUpEdit.Name = "CostUnitDescriptorIDGridLookUpEdit";
            this.CostUnitDescriptorIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CostUnitDescriptorIDGridLookUpEdit.Properties.DataSource = this.sp06101OMWorkUnitTypesPicklistBindingSource;
            this.CostUnitDescriptorIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.CostUnitDescriptorIDGridLookUpEdit.Properties.NullText = "";
            this.CostUnitDescriptorIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.CostUnitDescriptorIDGridLookUpEdit.Properties.View = this.gridLookUpEdit1View;
            this.CostUnitDescriptorIDGridLookUpEdit.Size = new System.Drawing.Size(372, 20);
            this.CostUnitDescriptorIDGridLookUpEdit.StyleController = this.layoutControl1;
            this.CostUnitDescriptorIDGridLookUpEdit.TabIndex = 1;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID1,
            this.colDescription,
            this.colOrder});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.colID1;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Unit Descriptor";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 220;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "RecordOrder";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            this.colOrder.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // EquipmentIDTextEdit
            // 
            this.EquipmentIDTextEdit.Location = new System.Drawing.Point(132, 12);
            this.EquipmentIDTextEdit.MenuManager = this.barManager1;
            this.EquipmentIDTextEdit.Name = "EquipmentIDTextEdit";
            this.EquipmentIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EquipmentIDTextEdit, true);
            this.EquipmentIDTextEdit.Size = new System.Drawing.Size(370, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EquipmentIDTextEdit, optionsSpelling3);
            this.EquipmentIDTextEdit.StyleController = this.layoutControl1;
            this.EquipmentIDTextEdit.TabIndex = 23;
            // 
            // EquipmentTypeButtonEdit
            // 
            this.EquipmentTypeButtonEdit.Location = new System.Drawing.Point(130, 12);
            this.EquipmentTypeButtonEdit.MenuManager = this.barManager1;
            this.EquipmentTypeButtonEdit.Name = "EquipmentTypeButtonEdit";
            this.EquipmentTypeButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "Click me to open the Select Equipment screen", "choose", null, true)});
            this.EquipmentTypeButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.EquipmentTypeButtonEdit.Size = new System.Drawing.Size(372, 20);
            this.EquipmentTypeButtonEdit.StyleController = this.layoutControl1;
            this.EquipmentTypeButtonEdit.TabIndex = 0;
            this.EquipmentTypeButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.EquipmentTypeButtonEdit_ButtonClick);
            // 
            // CostPerUnitExVatSpinEdit
            // 
            this.CostPerUnitExVatSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CostPerUnitExVatSpinEdit.Location = new System.Drawing.Point(130, 84);
            this.CostPerUnitExVatSpinEdit.MenuManager = this.barManager1;
            this.CostPerUnitExVatSpinEdit.Name = "CostPerUnitExVatSpinEdit";
            this.CostPerUnitExVatSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CostPerUnitExVatSpinEdit.Properties.Mask.EditMask = "c";
            this.CostPerUnitExVatSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CostPerUnitExVatSpinEdit.Size = new System.Drawing.Size(129, 20);
            this.CostPerUnitExVatSpinEdit.StyleController = this.layoutControl1;
            this.CostPerUnitExVatSpinEdit.TabIndex = 3;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.Location = new System.Drawing.Point(130, 132);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.RemarksMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(372, 155);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling4);
            this.RemarksMemoEdit.StyleController = this.layoutControl1;
            this.RemarksMemoEdit.TabIndex = 11;
            this.RemarksMemoEdit.UseOptimizedRendering = true;
            // 
            // ItemForEquipmentID
            // 
            this.ItemForEquipmentID.Control = this.EquipmentIDTextEdit;
            this.ItemForEquipmentID.CustomizationFormText = "Equipment ID:";
            this.ItemForEquipmentID.Location = new System.Drawing.Point(0, 0);
            this.ItemForEquipmentID.Name = "ItemForEquipmentID";
            this.ItemForEquipmentID.Size = new System.Drawing.Size(494, 24);
            this.ItemForEquipmentID.Text = "Equipment ID:";
            this.ItemForEquipmentID.TextSize = new System.Drawing.Size(50, 20);
            this.ItemForEquipmentID.TextToControlDistance = 5;
            // 
            // ItemForOwnerName
            // 
            this.ItemForOwnerName.Control = this.OwnerNameTextEdit;
            this.ItemForOwnerName.CustomizationFormText = "Owner Name:";
            this.ItemForOwnerName.Location = new System.Drawing.Point(0, 0);
            this.ItemForOwnerName.Name = "ItemForOwnerName";
            this.ItemForOwnerName.Size = new System.Drawing.Size(494, 24);
            this.ItemForOwnerName.Text = "Owner Name:";
            this.ItemForOwnerName.TextSize = new System.Drawing.Size(50, 20);
            this.ItemForOwnerName.TextToControlDistance = 5;
            // 
            // ItemForOwnerType
            // 
            this.ItemForOwnerType.Control = this.OwnerTypeTextEdit;
            this.ItemForOwnerType.CustomizationFormText = "Owner Type:";
            this.ItemForOwnerType.Location = new System.Drawing.Point(0, 0);
            this.ItemForOwnerType.Name = "ItemForOwnerType";
            this.ItemForOwnerType.Size = new System.Drawing.Size(494, 24);
            this.ItemForOwnerType.Text = "Owner Type:";
            this.ItemForOwnerType.TextSize = new System.Drawing.Size(50, 20);
            this.ItemForOwnerType.TextToControlDistance = 5;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks,
            this.ItemForCostPerUnitExVat,
            this.ItemForEquipmentType,
            this.ItemForCostUnitDescriptorID,
            this.ItemForSellUnitDescriptorID,
            this.ItemForSellPerUnitExVat,
            this.ItemForCostPerUnitVatRate,
            this.ItemForSellPerUnitVatRate});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(514, 299);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 120);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(494, 159);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(115, 13);
            // 
            // ItemForCostPerUnitExVat
            // 
            this.ItemForCostPerUnitExVat.Control = this.CostPerUnitExVatSpinEdit;
            this.ItemForCostPerUnitExVat.CustomizationFormText = "Cost Per Unit Ex VAT:";
            this.ItemForCostPerUnitExVat.Location = new System.Drawing.Point(0, 72);
            this.ItemForCostPerUnitExVat.Name = "ItemForCostPerUnitExVat";
            this.ItemForCostPerUnitExVat.Size = new System.Drawing.Size(251, 24);
            this.ItemForCostPerUnitExVat.Text = "Cost Per Unit Ex VAT:";
            this.ItemForCostPerUnitExVat.TextSize = new System.Drawing.Size(115, 13);
            // 
            // ItemForEquipmentType
            // 
            this.ItemForEquipmentType.Control = this.EquipmentTypeButtonEdit;
            this.ItemForEquipmentType.CustomizationFormText = "Equipment Type:";
            this.ItemForEquipmentType.Location = new System.Drawing.Point(0, 0);
            this.ItemForEquipmentType.Name = "ItemForEquipmentType";
            this.ItemForEquipmentType.Size = new System.Drawing.Size(494, 24);
            this.ItemForEquipmentType.Text = "Equipment Type:";
            this.ItemForEquipmentType.TextSize = new System.Drawing.Size(115, 13);
            // 
            // ItemForCostUnitDescriptorID
            // 
            this.ItemForCostUnitDescriptorID.Control = this.CostUnitDescriptorIDGridLookUpEdit;
            this.ItemForCostUnitDescriptorID.CustomizationFormText = "Cost Unit Descriptor:";
            this.ItemForCostUnitDescriptorID.Location = new System.Drawing.Point(0, 24);
            this.ItemForCostUnitDescriptorID.Name = "ItemForCostUnitDescriptorID";
            this.ItemForCostUnitDescriptorID.Size = new System.Drawing.Size(494, 24);
            this.ItemForCostUnitDescriptorID.Text = "Cost Unit Descriptor:";
            this.ItemForCostUnitDescriptorID.TextSize = new System.Drawing.Size(115, 13);
            // 
            // ItemForSellUnitDescriptorID
            // 
            this.ItemForSellUnitDescriptorID.Control = this.SellUnitDescriptorIDGridLookUpEdit;
            this.ItemForSellUnitDescriptorID.CustomizationFormText = "Sell Unit Descriptor:";
            this.ItemForSellUnitDescriptorID.Location = new System.Drawing.Point(0, 48);
            this.ItemForSellUnitDescriptorID.Name = "ItemForSellUnitDescriptorID";
            this.ItemForSellUnitDescriptorID.Size = new System.Drawing.Size(494, 24);
            this.ItemForSellUnitDescriptorID.Text = "Sell Unit Descriptor:";
            this.ItemForSellUnitDescriptorID.TextSize = new System.Drawing.Size(115, 13);
            // 
            // ItemForSellPerUnitExVat
            // 
            this.ItemForSellPerUnitExVat.Control = this.SellPerUnitExVatSpinEdit;
            this.ItemForSellPerUnitExVat.CustomizationFormText = "Sell Per Unit Ex VAT:";
            this.ItemForSellPerUnitExVat.Location = new System.Drawing.Point(0, 96);
            this.ItemForSellPerUnitExVat.Name = "ItemForSellPerUnitExVat";
            this.ItemForSellPerUnitExVat.Size = new System.Drawing.Size(251, 24);
            this.ItemForSellPerUnitExVat.Text = "Sell Per Unit Ex VAT:";
            this.ItemForSellPerUnitExVat.TextSize = new System.Drawing.Size(115, 13);
            // 
            // ItemForCostPerUnitVatRate
            // 
            this.ItemForCostPerUnitVatRate.Control = this.CostPerUnitVatRateSpinEdit;
            this.ItemForCostPerUnitVatRate.CustomizationFormText = "Cost Per Unit VAT Rate:";
            this.ItemForCostPerUnitVatRate.Location = new System.Drawing.Point(251, 72);
            this.ItemForCostPerUnitVatRate.Name = "ItemForCostPerUnitVatRate";
            this.ItemForCostPerUnitVatRate.Size = new System.Drawing.Size(243, 24);
            this.ItemForCostPerUnitVatRate.Text = "Cost Per Unit VAT Rate:";
            this.ItemForCostPerUnitVatRate.TextSize = new System.Drawing.Size(115, 13);
            // 
            // ItemForSellPerUnitVatRate
            // 
            this.ItemForSellPerUnitVatRate.Control = this.SellPerUnitVatRateSpinEdit;
            this.ItemForSellPerUnitVatRate.CustomizationFormText = "Sell Per Unit VAT Rate:";
            this.ItemForSellPerUnitVatRate.Location = new System.Drawing.Point(251, 96);
            this.ItemForSellPerUnitVatRate.Name = "ItemForSellPerUnitVatRate";
            this.ItemForSellPerUnitVatRate.Size = new System.Drawing.Size(243, 24);
            this.ItemForSellPerUnitVatRate.Text = "Sell Per Unit VAT Rate:";
            this.ItemForSellPerUnitVatRate.TextSize = new System.Drawing.Size(115, 13);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(295, 327);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(96, 22);
            this.btnOK.TabIndex = 13;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(406, 327);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(96, 22);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // sp06101_OM_Work_Unit_Types_PicklistTableAdapter
            // 
            this.sp06101_OM_Work_Unit_Types_PicklistTableAdapter.ClearBeforeFill = true;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 3";
            this.bar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiInformation)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 3";
            // 
            // bsiInformation
            // 
            this.bsiInformation.Caption = "Complete the fields displayed. Any fields left blank will not be overwritten.";
            this.bsiInformation.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiInformation.Glyph")));
            this.bsiInformation.Id = 30;
            this.bsiInformation.Name = "bsiInformation";
            this.bsiInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bsiInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // frm_OM_Site_Contract_Wizard_Block_Edit_Default_Equipment_Info
            // 
            this.ClientSize = new System.Drawing.Size(515, 389);
            this.ControlBox = false;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_OM_Site_Contract_Wizard_Block_Edit_Default_Equipment_Info";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Contract Wizard - Block Edit Default Equipment Cost";
            this.Load += new System.EventHandler(this.frm_OM_Site_Contract_Wizard_Block_Edit_Default_Equipment_Info_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.OwnerTypeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OwnerNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellPerUnitVatRateSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostPerUnitVatRateSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellPerUnitExVatSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellUnitDescriptorIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06101OMWorkUnitTypesPicklistBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostUnitDescriptorIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EquipmentIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EquipmentTypeButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostPerUnitExVatSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipmentID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOwnerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOwnerType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostPerUnitExVat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipmentType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostUnitDescriptorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellUnitDescriptorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellPerUnitExVat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostPerUnitVatRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellPerUnitVatRate)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraEditors.SpinEdit CostPerUnitExVatSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostPerUnitExVat;
        private DevExpress.XtraEditors.TextEdit EquipmentIDTextEdit;
        private DevExpress.XtraEditors.ButtonEdit EquipmentTypeButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEquipmentID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEquipmentType;
        private DevExpress.XtraEditors.GridLookUpEdit CostUnitDescriptorIDGridLookUpEdit;
        private System.Windows.Forms.BindingSource sp06101OMWorkUnitTypesPicklistBindingSource;
        private DataSet_OM_Contract dataSet_OM_Contract;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn colID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostUnitDescriptorID;
        private DataSet_OM_ContractTableAdapters.sp06101_OM_Work_Unit_Types_PicklistTableAdapter sp06101_OM_Work_Unit_Types_PicklistTableAdapter;
        private DevExpress.XtraEditors.GridLookUpEdit SellUnitDescriptorIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSellUnitDescriptorID;
        private DevExpress.XtraEditors.SpinEdit SellPerUnitExVatSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSellPerUnitExVat;
        private DevExpress.XtraEditors.SpinEdit SellPerUnitVatRateSpinEdit;
        private DevExpress.XtraEditors.SpinEdit CostPerUnitVatRateSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostPerUnitVatRate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSellPerUnitVatRate;
        private DevExpress.XtraEditors.TextEdit OwnerTypeTextEdit;
        private DevExpress.XtraEditors.TextEdit OwnerNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOwnerName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOwnerType;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarStaticItem bsiInformation;
    }
}

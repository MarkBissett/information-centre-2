using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;

using WoodPlan5.Properties;
using WoodPlan5.Classes.Operations;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_OM_Client_Contract_Edit : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        public int intLinkedToRecordID = 0;
        public string strLinkedToRecordDesc = "";

        private string strDefaultPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //
        
        #endregion

        public frm_OM_Client_Contract_Edit()
        {
            InitializeComponent();
        }
        
        private void frm_OM_Client_Contract_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 50013300;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            sp06060_OM_GC_Companies_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06060_OM_GC_Companies_With_BlankTableAdapter.Fill(dataSet_OM_Contract.sp06060_OM_GC_Companies_With_Blank);

            sp06061_OM_Sectors_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06061_OM_Sectors_With_BlankTableAdapter.Fill(dataSet_OM_Contract.sp06061_OM_Sectors_With_Blank);

            sp06062_OM_Contract_Statuses_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06062_OM_Contract_Statuses_With_BlankTableAdapter.Fill(dataSet_OM_Contract.sp06062_OM_Contract_Statuses_With_Blank);

            sp06063_OM_Contract_Types_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06063_OM_Contract_Types_With_BlankTableAdapter.Fill(dataSet_OM_Contract.sp06063_OM_Contract_Types_With_Blank);

            sp01055_Core_Get_Report_LayoutsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01055_Core_Get_Report_LayoutsTableAdapter.Fill(dataSet_Common_Functionality.sp01055_Core_Get_Report_Layouts, 7, 101, 1);

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_CMTenderRequestNotesFileFolder").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Tender Notes File path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Tender Notes File path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (!strDefaultPath.EndsWith("\\")) strDefaultPath += "\\";  // Add Backslash to end //

            // Populate Main Dataset //
            sp06059_OM_Client_Contract_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();

            //LoadCombos();

            switch (strFormMode)
            {
                case "add":
                case "blockadd":
                    try
                    {
                        DataRow drNewRow = dataSet_OM_Contract.sp06059_OM_Client_Contract_Edit.NewRow();
                        drNewRow["strMode"] = strFormMode;
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["ClientContractID"] = 0;
                        drNewRow["ClientID"] = intLinkedToRecordID;
                        drNewRow["ClientName"] = strLinkedToRecordDesc;
                        drNewRow["ContractDescription"] = "";
                        drNewRow["Reactive"] = 0;
                        drNewRow["GCCompanyID"] = 0;
                        drNewRow["SectorTypeID"] = 0;
                        drNewRow["ContractStatusID"] = 1;
                        drNewRow["ContractDirectorID"] = 0;
                        drNewRow["ContractDirector"] = "";
                        drNewRow["ContractTypeID"] = 0;
                        drNewRow["Active"] = 1;
                        drNewRow["ContractValue"] = (decimal)0.00;
                        drNewRow["YearlyPercentageIncrease"] = (decimal)0.00;
                        drNewRow["BillingCentreCodeID"] = 0;
                        drNewRow["LinkedToPreviousContractID"] = 0;
                        drNewRow["LinkedToPreviousContract"] = "";
                        drNewRow["CostCentreID"] = 0;
                        drNewRow["CostCentreCode"] = "";
                        drNewRow["CostCentre"] = "";
                        drNewRow["BillingCentreCode"] = "";
                        drNewRow["SignatureForTeamSelfBilling"] = 0;
                        drNewRow["SignatureForClientBilling"] = 0;
                        drNewRow["TenderReactiveDaysToReturnQuote"] = 0;
                        drNewRow["TenderProactiveDaysToReturnQuote"] = 0;
                        drNewRow["DefaultCompletionSheetTemplateID"] = 0;
                        dataSet_OM_Contract.sp06059_OM_Client_Contract_Edit.Rows.Add(drNewRow);
                    }
                    catch (Exception ex)
                    {
                    }
                    break;
                case "blockedit":
                    try
                    {
                        //DataRow drNewRow;
                        DataRow drNewRow = dataSet_OM_Contract.sp06059_OM_Client_Contract_Edit.NewRow();
                        drNewRow["strMode"] = strFormMode;
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        dataSet_OM_Contract.sp06059_OM_Client_Contract_Edit.Rows.Add(drNewRow);
                        drNewRow.AcceptChanges();
                    }
                    catch (Exception ex)
                    {
                    }
                    break;
                case "edit":
                case "view":
                    try
                    {
                        sp06059_OM_Client_Contract_EditTableAdapter.Fill(this.dataSet_OM_Contract.sp06059_OM_Client_Contract_Edit, strRecordIDs, strFormMode);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);

                    }
                    break;
            }
            dataLayoutControl1.EndUpdate();

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
        }
       
        private void frm_OM_Client_Contract_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_OM_Client_Contract_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strFormMode == "view") return;
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }

 
        private void Attach_EditValueChanged_To_Children(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (this.dataSet_OM_Contract.sp06059_OM_Client_Contract_Edit.Rows.Count == 0)
            {
                _KeepWaitFormOpen = false;
                splashScreenManager.CloseWaitForm();
                XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Client Contract", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        ClientNameButtonEdit.Focus();

                        ClientNameButtonEdit.Properties.ReadOnly = false;
                        ClientNameButtonEdit.Properties.Buttons[0].Enabled = true;
                        ContractDescriptionTextEdit.Properties.ReadOnly = false;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        GCCompanyIDGridLookUpEdit.Focus();

                        ClientNameButtonEdit.Properties.ReadOnly = true;
                        ClientNameButtonEdit.Properties.Buttons[0].Enabled = false;
                        ContractDescriptionTextEdit.Properties.ReadOnly = true;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        ClientNameButtonEdit.Focus();

                        ClientNameButtonEdit.Properties.ReadOnly = false;
                        ClientNameButtonEdit.Properties.Buttons[0].Enabled = true;
                        ContractDescriptionTextEdit.Properties.ReadOnly = false;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        GCCompanyIDGridLookUpEdit.Focus();

                        ClientNameButtonEdit.Properties.ReadOnly = true;
                        ClientNameButtonEdit.Properties.Buttons[0].Enabled = false;
                        ContractDescriptionTextEdit.Properties.ReadOnly = true;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Viewing";
                        barStaticItemFormMode.ImageIndex = 4;  // View //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();
            SetEditorButtons();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }
        }

        private void SetEditorButtons()
        {
            // Get Form Permissions //
        }
        
        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_OM_Contract.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Saving Changes...");

            this.sp06059OMClientContractEditBindingSource.EndEdit();
            try
            {
                sp06059_OM_Client_Contract_EditTableAdapter.Update(dataSet_OM_Contract);  // Insert and Update queries defined in Table Adapter //
            }
            catch (Exception ex)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                var currentRowView = (DataRowView)sp06059OMClientContractEditBindingSource.Current;
                var currentRow = (DataSet_OM_Contract.sp06059_OM_Client_Contract_EditRow)currentRowView.Row;
                this.strFormMode = "edit";  // Switch mode to Edit so than any subsequent changes update this record //
                if (currentRow != null)
                {
                    currentRow.strMode = "edit";
                    currentRow.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                    strNewIDs = currentRow.ClientContractID + ";";
                }
            }

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_OM_Client_Contract_Manager")
                    {
                        var fParentForm = (frm_OM_Client_Contract_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, Utils.enmFocusedGrid.Contract, strNewIDs);
                    }
                }
            }

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < this.dataSet_OM_Contract.sp06059_OM_Client_Contract_Edit.Rows.Count; i++)
            {
                switch (this.dataSet_OM_Contract.sp06059_OM_Client_Contract_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }

        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void ClientNameButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var currentRowView = (DataRowView)sp06059OMClientContractEditBindingSource.Current;
                var currentRow = (DataSet_OM_Contract.sp06059_OM_Client_Contract_EditRow)currentRowView.Row;
                if (currentRow == null) return;
                int intClientID = (string.IsNullOrEmpty(currentRow.ClientID.ToString()) ? 0 : Convert.ToInt32(currentRow.ClientID));
                var fChildForm = new frm_EP_Select_Client();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalClientID = intClientID;
                fChildForm.intFilterSummerMaintenanceClient = 1;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow.ClientID = fChildForm.intSelectedClientID;
                    currentRow.ClientName = fChildForm.strSelectedClientName;

                    sp06059OMClientContractEditBindingSource.EndEdit();
                }
            }
        }
        private void ClientNameButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(ClientNameButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(ClientNameButtonEdit, "");
            }
        }

        private void ContractDescriptionTextEdit_Validating(object sender, CancelEventArgs e)
        {
            TextEdit be = (TextEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(ContractDescriptionTextEdit, "Enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(ContractDescriptionTextEdit, "");
            }
        }
        
        private void GCCompanyIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && (string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "0"))
            {
                dxErrorProvider1.SetError(GCCompanyIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(GCCompanyIDGridLookUpEdit, "");
            }
        }

        private void SectorTypeIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && (string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "0"))
            {
                dxErrorProvider1.SetError(SectorTypeIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(SectorTypeIDGridLookUpEdit, "");
            }
        }

        private void ContractStatusIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && (string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "0"))
            {
                dxErrorProvider1.SetError(ContractStatusIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(ContractStatusIDGridLookUpEdit, "");
            }
        }

        private void ContractDirectorButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)sp06059OMClientContractEditBindingSource.Current;
                if (currentRow == null) return;
                int ContractDirectorID = (string.IsNullOrEmpty(currentRow["ContractDirectorID"].ToString()) ? 0 : Convert.ToInt32(currentRow["ContractDirectorID"]));

                var fChildForm = new frm_HR_Select_Staff();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalStaffID = ContractDirectorID;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    if (ContractDirectorID != fChildForm.intSelectedStaffID)
                    {
                        currentRow["ContractDirectorID"] = fChildForm.intSelectedStaffID;
                        currentRow["ContractDirector"] = fChildForm.strSelectedStaffName;
                        sp06059OMClientContractEditBindingSource.EndEdit();
                    }
                }
            }
        }
        private void ContractDirectorButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(ContractDirectorButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(ContractDirectorButtonEdit, "");
            }
        }

        private void ContractTypeIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && (string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "0"))
            {
                dxErrorProvider1.SetError(ContractTypeIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(ContractTypeIDGridLookUpEdit, "");
            }
        }

        private void StartDateDateEdit_Validating(object sender, CancelEventArgs e)
        {
            DateEdit de = (DateEdit)sender;
            if (this.strFormMode != "blockedit" && (de.EditValue == null || string.IsNullOrEmpty(de.EditValue.ToString())))
            {
                dxErrorProvider1.SetError(StartDateDateEdit, "Select\\enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(StartDateDateEdit, "");
            }
        }

        private void BillingCentreCodeButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)sp06059OMClientContractEditBindingSource.Current;
                if (currentRow == null) return;
                int intBillingCentreCodeID = (string.IsNullOrEmpty(currentRow["BillingCentreCodeID"].ToString()) ? 0 : Convert.ToInt32(currentRow["BillingCentreCodeID"]));
                int intClientID = (string.IsNullOrEmpty(currentRow["ClientID"].ToString()) ? 0 : Convert.ToInt32(currentRow["ClientID"]));

                var fChildForm = new frm_OM_Select_Cost_Centre();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalBillingCodeID = intBillingCentreCodeID;
                fChildForm.strOriginalDepartmentIDs = (intClientID == 0 ? "" : intClientID.ToString() + ","); ;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    if (intBillingCentreCodeID != fChildForm.intSelectedBillingCentreCodeID)
                    {
                        currentRow["BillingCentreCodeID"] = fChildForm.intSelectedBillingCentreCodeID;
                        currentRow["CostCentreID"] = fChildForm.intSelectedCostCentreID;
                        currentRow["CostCentreCode"] = fChildForm.strSelectedCostCentreCodes;
                        currentRow["CostCentre"] = fChildForm.strSelectedCostCentres;
                        currentRow["BillingCentreCode"] = fChildForm.strSelectedBillingCentreCode;
                        sp06059OMClientContractEditBindingSource.EndEdit();
                    }
                }
            }
        }

        private void LinkedToPreviousContractButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)sp06059OMClientContractEditBindingSource.Current;
                if (currentRow == null) return;
                int intOriginalID = (string.IsNullOrEmpty(currentRow["LinkedToPreviousContractID"].ToString()) ? 0 : Convert.ToInt32(currentRow["LinkedToPreviousContractID"]));
                int intExcludeID = (string.IsNullOrEmpty(currentRow["ClientContractID"].ToString()) ? 0 : Convert.ToInt32(currentRow["ClientContractID"]));

                var fChildForm = new frm_OM_Select_Client_Contract();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalSelectedID = intOriginalID;
                fChildForm._strExcludeIDs = intExcludeID.ToString() + ",";
                fChildForm.i_dt_FromDate = DateTime.Today.AddMonths(-6);
                fChildForm.i_dt_ToDate = DateTime.Today.AddMonths(6);
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    if (intOriginalID != fChildForm.intSelectedID)
                    {
                        currentRow["LinkedToPreviousContractID"] = fChildForm.intSelectedID;
                        currentRow["LinkedToPreviousContract"] = fChildForm.strSelectedDescription;
                        sp06059OMClientContractEditBindingSource.EndEdit();
                    }
                }
            }
        }

        private void CMTenderRequestNotesFileButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            var currentRowView = (DataRowView)sp06059OMClientContractEditBindingSource.Current;
            var currentRow = (DataSet_OM_Contract.sp06059_OM_Client_Contract_EditRow)currentRowView.Row;
            if (currentRow == null) return;
            switch (e.Button.Tag.ToString())
            {
                case "choose":
                    {
                        if (strFormMode == "view") return;
                        using (OpenFileDialog dlg = new OpenFileDialog())
                        {
                            dlg.DefaultExt = "tab";
                            dlg.Filter = "";
                            if (strDefaultPath != "") dlg.InitialDirectory = strDefaultPath;
                            dlg.Multiselect = false;
                            dlg.ShowDialog();
                            if (dlg.FileNames.Length > 0)
                            {
                                string strTempFileName = "";
                                string strTempResult = "";
                                foreach (string filename in dlg.FileNames)
                                {
                                    if (strDefaultPath != "")
                                        if (!filename.ToLower().StartsWith(strDefaultPath.ToLower()))
                                        {
                                            DevExpress.XtraEditors.XtraMessageBox.Show("You can only select files from under the default folder [as specified in the System Configuration Screen - Default Tender Request Notes File Folder] or one of it's sub-folders.\n\nFile Selection Aborted!", "Select File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                            break;
                                        }
                                    strTempFileName = filename.Substring(strDefaultPath.Length + 1);  // 1 added to remove backslash from start of filename //
                                    if (strTempResult == "")
                                    {
                                        if (!strTempResult.Contains(strTempFileName)) strTempResult += strTempFileName;
                                    }
                                    else
                                    {
                                        if (!strTempResult.Contains(strTempFileName)) strTempResult += "\r\n" + strTempFileName;
                                    }
                                }
                                currentRow.CMTenderRequestNotesFile = strTempResult;
                                sp06059OMClientContractEditBindingSource.EndEdit();
                            }
                        }
                    }
                    break;
                case "view":
                    {
                        string strFile = currentRow.CMTenderRequestNotesFile.ToString();

                        if (string.IsNullOrEmpty(strFile))
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Link the File to be Viewed before proceeding.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        try
                        {
                            if (Path.GetExtension(Path.Combine(strDefaultPath + strFile)).ToLower() != ".pdf")
                            {
                                System.Diagnostics.Process.Start(Path.Combine(strDefaultPath + strFile));
                            }
                            else
                            {
                                if (!BaseObjects.Keyboard.IsKeyDown(Keys.A))  // Only open in DevExpress PDF viewer if user is not holding down A key (CTRL Key doesn't work when fired from a grid as it's used for row selection) //
                                {
                                    frm_Core_PDF_Viewer fChildForm = new frm_Core_PDF_Viewer();
                                    fChildForm.strPDFFile = Path.Combine(strDefaultPath + strFile);
                                    fChildForm.MdiParent = this.MdiParent;
                                    fChildForm.GlobalSettings = this.GlobalSettings;
                                    fChildForm.Show();
                                }
                                else
                                {
                                    System.Diagnostics.Process.Start(Path.Combine(strDefaultPath + strFile));
                                }
                            }
                        }
                        catch
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view the Linked File: " + strFile + ".\n\nThe File may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                    break;

                case "clear":
                    {
                        if (strFormMode == "view") return;
                        currentRow.CMTenderRequestNotesFile = "";
                        sp06059OMClientContractEditBindingSource.EndEdit();
                    }
                    break;
            }
        }

        #endregion







    }
}


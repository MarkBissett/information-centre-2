﻿namespace WoodPlan5
{
    partial class frm_OM_Site_Contract_Wizard_Block_Edit_Site_Year_Billing_Info
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Site_Contract_Wizard_Block_Edit_Site_Year_Billing_Info));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.InvoiceDateActualDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.ActualBillAmountSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.EstimatedBillAmountSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.InvoiceDateDueDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForActualBillAmount = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForInvoiceDateDue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEstimatedBillAmount = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForInvoiceDateActual = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bsiInformation = new DevExpress.XtraBars.BarStaticItem();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.InvoiceDateActualDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvoiceDateActualDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualBillAmountSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EstimatedBillAmountSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvoiceDateDueDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvoiceDateDueDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualBillAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInvoiceDateDue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEstimatedBillAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInvoiceDateActual)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(488, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 288);
            this.barDockControlBottom.Size = new System.Drawing.Size(488, 30);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 262);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(488, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 262);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bsiInformation});
            this.barManager1.MaxItemId = 31;
            this.barManager1.StatusBar = this.bar1;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutControl1.Controls.Add(this.InvoiceDateActualDateEdit);
            this.layoutControl1.Controls.Add(this.ActualBillAmountSpinEdit);
            this.layoutControl1.Controls.Add(this.EstimatedBillAmountSpinEdit);
            this.layoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.layoutControl1.Controls.Add(this.InvoiceDateDueDateEdit);
            this.layoutControl1.Location = new System.Drawing.Point(0, 28);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1213, 293, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(487, 229);
            this.layoutControl1.TabIndex = 4;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // InvoiceDateActualDateEdit
            // 
            this.InvoiceDateActualDateEdit.EditValue = null;
            this.InvoiceDateActualDateEdit.Location = new System.Drawing.Point(127, 60);
            this.InvoiceDateActualDateEdit.MenuManager = this.barManager1;
            this.InvoiceDateActualDateEdit.Name = "InvoiceDateActualDateEdit";
            this.InvoiceDateActualDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.InvoiceDateActualDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.InvoiceDateActualDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.InvoiceDateActualDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.InvoiceDateActualDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.InvoiceDateActualDateEdit.Size = new System.Drawing.Size(138, 20);
            this.InvoiceDateActualDateEdit.StyleController = this.layoutControl1;
            this.InvoiceDateActualDateEdit.TabIndex = 17;
            // 
            // ActualBillAmountSpinEdit
            // 
            this.ActualBillAmountSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ActualBillAmountSpinEdit.Location = new System.Drawing.Point(127, 84);
            this.ActualBillAmountSpinEdit.MenuManager = this.barManager1;
            this.ActualBillAmountSpinEdit.Name = "ActualBillAmountSpinEdit";
            this.ActualBillAmountSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ActualBillAmountSpinEdit.Properties.Mask.EditMask = "c";
            this.ActualBillAmountSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ActualBillAmountSpinEdit.Size = new System.Drawing.Size(138, 20);
            this.ActualBillAmountSpinEdit.StyleController = this.layoutControl1;
            this.ActualBillAmountSpinEdit.TabIndex = 21;
            // 
            // EstimatedBillAmountSpinEdit
            // 
            this.EstimatedBillAmountSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.EstimatedBillAmountSpinEdit.Location = new System.Drawing.Point(127, 36);
            this.EstimatedBillAmountSpinEdit.MenuManager = this.barManager1;
            this.EstimatedBillAmountSpinEdit.Name = "EstimatedBillAmountSpinEdit";
            this.EstimatedBillAmountSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EstimatedBillAmountSpinEdit.Properties.Mask.EditMask = "c";
            this.EstimatedBillAmountSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.EstimatedBillAmountSpinEdit.Size = new System.Drawing.Size(138, 20);
            this.EstimatedBillAmountSpinEdit.StyleController = this.layoutControl1;
            this.EstimatedBillAmountSpinEdit.TabIndex = 20;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.Location = new System.Drawing.Point(127, 108);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.RemarksMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(348, 109);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling1);
            this.RemarksMemoEdit.StyleController = this.layoutControl1;
            this.RemarksMemoEdit.TabIndex = 18;
            this.RemarksMemoEdit.UseOptimizedRendering = true;
            // 
            // InvoiceDateDueDateEdit
            // 
            this.InvoiceDateDueDateEdit.EditValue = null;
            this.InvoiceDateDueDateEdit.Location = new System.Drawing.Point(127, 12);
            this.InvoiceDateDueDateEdit.MenuManager = this.barManager1;
            this.InvoiceDateDueDateEdit.Name = "InvoiceDateDueDateEdit";
            this.InvoiceDateDueDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.InvoiceDateDueDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.InvoiceDateDueDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.InvoiceDateDueDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.InvoiceDateDueDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.InvoiceDateDueDateEdit.Size = new System.Drawing.Size(138, 20);
            this.InvoiceDateDueDateEdit.StyleController = this.layoutControl1;
            this.InvoiceDateDueDateEdit.TabIndex = 16;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks,
            this.ItemForActualBillAmount,
            this.emptySpaceItem3,
            this.ItemForInvoiceDateDue,
            this.ItemForEstimatedBillAmount,
            this.ItemForInvoiceDateActual});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(487, 229);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 96);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(467, 113);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(112, 13);
            // 
            // ItemForActualBillAmount
            // 
            this.ItemForActualBillAmount.Control = this.ActualBillAmountSpinEdit;
            this.ItemForActualBillAmount.CustomizationFormText = "Actual Invoice Amount:";
            this.ItemForActualBillAmount.Location = new System.Drawing.Point(0, 72);
            this.ItemForActualBillAmount.Name = "ItemForActualBillAmount";
            this.ItemForActualBillAmount.Size = new System.Drawing.Size(257, 24);
            this.ItemForActualBillAmount.Text = "Actual Invoice Amount:";
            this.ItemForActualBillAmount.TextSize = new System.Drawing.Size(112, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(257, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(210, 96);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForInvoiceDateDue
            // 
            this.ItemForInvoiceDateDue.Control = this.InvoiceDateDueDateEdit;
            this.ItemForInvoiceDateDue.CustomizationFormText = "Invoice Due Date:";
            this.ItemForInvoiceDateDue.Location = new System.Drawing.Point(0, 0);
            this.ItemForInvoiceDateDue.MaxSize = new System.Drawing.Size(257, 24);
            this.ItemForInvoiceDateDue.MinSize = new System.Drawing.Size(257, 24);
            this.ItemForInvoiceDateDue.Name = "ItemForInvoiceDateDue";
            this.ItemForInvoiceDateDue.Size = new System.Drawing.Size(257, 24);
            this.ItemForInvoiceDateDue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForInvoiceDateDue.Text = "Invoice Due Date:";
            this.ItemForInvoiceDateDue.TextSize = new System.Drawing.Size(112, 13);
            // 
            // ItemForEstimatedBillAmount
            // 
            this.ItemForEstimatedBillAmount.Control = this.EstimatedBillAmountSpinEdit;
            this.ItemForEstimatedBillAmount.CustomizationFormText = "Estimated Bill Amount:";
            this.ItemForEstimatedBillAmount.Location = new System.Drawing.Point(0, 24);
            this.ItemForEstimatedBillAmount.Name = "ItemForEstimatedBillAmount";
            this.ItemForEstimatedBillAmount.Size = new System.Drawing.Size(257, 24);
            this.ItemForEstimatedBillAmount.Text = "Estimated Bill Amount:";
            this.ItemForEstimatedBillAmount.TextSize = new System.Drawing.Size(112, 13);
            // 
            // ItemForInvoiceDateActual
            // 
            this.ItemForInvoiceDateActual.Control = this.InvoiceDateActualDateEdit;
            this.ItemForInvoiceDateActual.CustomizationFormText = "Actual Invoice Date:";
            this.ItemForInvoiceDateActual.Location = new System.Drawing.Point(0, 48);
            this.ItemForInvoiceDateActual.Name = "ItemForInvoiceDateActual";
            this.ItemForInvoiceDateActual.Size = new System.Drawing.Size(257, 24);
            this.ItemForInvoiceDateActual.Text = "Actual Invoice Date:";
            this.ItemForInvoiceDateActual.TextSize = new System.Drawing.Size(112, 13);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(268, 257);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(96, 22);
            this.btnOK.TabIndex = 13;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(379, 257);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(96, 22);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 3";
            this.bar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiInformation)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 3";
            // 
            // bsiInformation
            // 
            this.bsiInformation.Caption = "Complete the fields displayed. Any fields left blank will not be overwritten.";
            this.bsiInformation.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiInformation.Glyph")));
            this.bsiInformation.Id = 30;
            this.bsiInformation.Name = "bsiInformation";
            this.bsiInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bsiInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // frm_OM_Site_Contract_Wizard_Block_Edit_Site_Year_Billing_Info
            // 
            this.ClientSize = new System.Drawing.Size(488, 318);
            this.ControlBox = false;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_OM_Site_Contract_Wizard_Block_Edit_Site_Year_Billing_Info";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Contract Wizard - Block Edit Contract Year Billing Profile";
            this.Load += new System.EventHandler(this.frm_OM_Site_Contract_Wizard_Block_Edit_Site_Year_Billing_Info_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.InvoiceDateActualDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvoiceDateActualDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualBillAmountSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EstimatedBillAmountSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvoiceDateDueDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvoiceDateDueDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualBillAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInvoiceDateDue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEstimatedBillAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInvoiceDateActual)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraEditors.SpinEdit EstimatedBillAmountSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEstimatedBillAmount;
        private DevExpress.XtraEditors.SpinEdit ActualBillAmountSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActualBillAmount;
        private DevExpress.XtraEditors.DateEdit InvoiceDateDueDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInvoiceDateDue;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.DateEdit InvoiceDateActualDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInvoiceDateActual;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarStaticItem bsiInformation;
    }
}

﻿namespace WoodPlan5
{
    partial class frm_OM_Visit_Mapping_View
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Visit_Mapping_View));
            DevExpress.XtraMap.MapItemAttributeMapping mapItemAttributeMapping1 = new DevExpress.XtraMap.MapItemAttributeMapping();
            DevExpress.XtraMap.MapItemAttributeMapping mapItemAttributeMapping2 = new DevExpress.XtraMap.MapItemAttributeMapping();
            DevExpress.XtraMap.MapItemAttributeMapping mapItemAttributeMapping3 = new DevExpress.XtraMap.MapItemAttributeMapping();
            DevExpress.XtraMap.MapItemAttributeMapping mapItemAttributeMapping4 = new DevExpress.XtraMap.MapItemAttributeMapping();
            DevExpress.XtraMap.MapItemAttributeMapping mapItemAttributeMapping5 = new DevExpress.XtraMap.MapItemAttributeMapping();
            DevExpress.XtraMap.MapItemAttributeMapping mapItemAttributeMapping6 = new DevExpress.XtraMap.MapItemAttributeMapping();
            DevExpress.XtraMap.MapItemAttributeMapping mapItemAttributeMapping7 = new DevExpress.XtraMap.MapItemAttributeMapping();
            DevExpress.XtraMap.MapItemAttributeMapping mapItemAttributeMapping8 = new DevExpress.XtraMap.MapItemAttributeMapping();
            DevExpress.XtraMap.MapItemAttributeMapping mapItemAttributeMapping9 = new DevExpress.XtraMap.MapItemAttributeMapping();
            DevExpress.XtraMap.MapItemAttributeMapping mapItemAttributeMapping10 = new DevExpress.XtraMap.MapItemAttributeMapping();
            DevExpress.XtraMap.MapItemAttributeMapping mapItemAttributeMapping11 = new DevExpress.XtraMap.MapItemAttributeMapping();
            DevExpress.XtraMap.MapItemAttributeMapping mapItemAttributeMapping12 = new DevExpress.XtraMap.MapItemAttributeMapping();
            DevExpress.XtraMap.MapItemAttributeMapping mapItemAttributeMapping13 = new DevExpress.XtraMap.MapItemAttributeMapping();
            DevExpress.XtraMap.MapItemAttributeMapping mapItemAttributeMapping14 = new DevExpress.XtraMap.MapItemAttributeMapping();
            DevExpress.XtraMap.MapItemAttributeMapping mapItemAttributeMapping15 = new DevExpress.XtraMap.MapItemAttributeMapping();
            DevExpress.XtraMap.MapItemAttributeMapping mapItemAttributeMapping16 = new DevExpress.XtraMap.MapItemAttributeMapping();
            DevExpress.XtraMap.MapItemAttributeMapping mapItemAttributeMapping17 = new DevExpress.XtraMap.MapItemAttributeMapping();
            DevExpress.XtraMap.MapItemAttributeMapping mapItemAttributeMapping18 = new DevExpress.XtraMap.MapItemAttributeMapping();
            DevExpress.XtraMap.MapItemAttributeMapping mapItemAttributeMapping19 = new DevExpress.XtraMap.MapItemAttributeMapping();
            DevExpress.XtraMap.MapItemAttributeMapping mapItemAttributeMapping20 = new DevExpress.XtraMap.MapItemAttributeMapping();
            DevExpress.XtraMap.MapItemAttributeMapping mapItemAttributeMapping21 = new DevExpress.XtraMap.MapItemAttributeMapping();
            DevExpress.XtraMap.MapItemAttributeMapping mapItemAttributeMapping22 = new DevExpress.XtraMap.MapItemAttributeMapping();
            DevExpress.XtraMap.MapItemAttributeMapping mapItemAttributeMapping23 = new DevExpress.XtraMap.MapItemAttributeMapping();
            DevExpress.XtraMap.MapItemAttributeMapping mapItemAttributeMapping24 = new DevExpress.XtraMap.MapItemAttributeMapping();
            DevExpress.XtraMap.MapItemAttributeMapping mapItemAttributeMapping25 = new DevExpress.XtraMap.MapItemAttributeMapping();
            DevExpress.XtraMap.MapItemAttributeMapping mapItemAttributeMapping26 = new DevExpress.XtraMap.MapItemAttributeMapping();
            DevExpress.XtraMap.MapItemAttributeMapping mapItemAttributeMapping27 = new DevExpress.XtraMap.MapItemAttributeMapping();
            DevExpress.XtraMap.MapItemAttributeMapping mapItemAttributeMapping28 = new DevExpress.XtraMap.MapItemAttributeMapping();
            DevExpress.XtraMap.MapItemAttributeMapping mapItemAttributeMapping29 = new DevExpress.XtraMap.MapItemAttributeMapping();
            DevExpress.XtraMap.MapItemAttributeMapping mapItemAttributeMapping30 = new DevExpress.XtraMap.MapItemAttributeMapping();
            DevExpress.XtraMap.MapItemAttributeMapping mapItemAttributeMapping31 = new DevExpress.XtraMap.MapItemAttributeMapping();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule2 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue2 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule3 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue3 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.Utils.SuperToolTip superToolTip10 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem10 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem10 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip11 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem11 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem11 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem9 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem9 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            this.colRecordEnabled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colLayerOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.imageTilesLayer1 = new DevExpress.XtraMap.ImageTilesLayer();
            this.bingMapDataProvider1 = new DevExpress.XtraMap.BingMapDataProvider();
            this.vectorItemsLayer1 = new DevExpress.XtraMap.VectorItemsLayer();
            this.listSourceDataAdapter1 = new DevExpress.XtraMap.ListSourceDataAdapter();
            this.sp06155OMVisitMappingSitesMapObjectsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Visit = new WoodPlan5.DataSet_OM_Visit();
            this.vectorItemsLayer2 = new DevExpress.XtraMap.VectorItemsLayer();
            this.listSourceDataAdapter2 = new DevExpress.XtraMap.ListSourceDataAdapter();
            this.sp06153OMMappingVisitsMapObjectsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.vectorItemsLayer3 = new DevExpress.XtraMap.VectorItemsLayer();
            this.listSourceDataAdapter3 = new DevExpress.XtraMap.ListSourceDataAdapter();
            this.sp06277OMMappingJobsMapObjectsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Job = new WoodPlan5.DataSet_OM_Job();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp06154OMVisitMappingSitesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSitePostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditLatLong = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colThematicValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMapLabel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sp06152OMMappingVisitsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.popupContainerControlMapType = new DevExpress.XtraEditors.PopupContainerControl();
            this.groupControlMapType = new DevExpress.XtraEditors.GroupControl();
            this.checkEdit4 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit3 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.btnOKMapType = new DevExpress.XtraEditors.SimpleButton();
            this.mapControl1 = new BaseObjects.ExtendedMapControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.toolTipController1 = new DevExpress.Utils.ToolTipController(this.components);
            this.barEditItemMapType = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditMapType = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.repositoryItemRadioGroup1 = new DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanelLayerManager = new DevExpress.XtraBars.Docking.DockPanel();
            this.controlContainer1 = new DevExpress.XtraBars.Docking.ControlContainer();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp06354OMMappingLayerManagerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Core = new WoodPlan5.DataSet_OM_Core();
            this.imageCollection16x16 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLayerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dockPanelLegend = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel2_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit3 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit5 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit8 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit4 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit6 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit7 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.dockPanelData = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.btnClearSitesFromMap = new DevExpress.XtraEditors.SimpleButton();
            this.buttonEditChooseSites = new DevExpress.XtraEditors.ButtonEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btnLoadSitesIntoMap = new DevExpress.XtraEditors.SimpleButton();
            this.btnLoadVisits = new DevExpress.XtraEditors.SimpleButton();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.btnLoadJobs = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn40 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn41 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn42 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn43 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn44 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn45 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn46 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn47 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn48 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn49 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn50 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn51 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellCalculationLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellCalculationLevelDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnClearVisitsFromMap = new DevExpress.XtraEditors.SimpleButton();
            this.btnLoadVisitsIntoMap = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp06276OMMappingJobsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colJobID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReactive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colJobNoLongerRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRequiresAccessPermit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccessPermitID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPOID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinanceSystemPONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDate2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colExpectedEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedDurationUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNumeric2DP2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colScheduleSentDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualDurationUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientInvoiceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateClientInvoiced = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingInvoiceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingInvoiceReceivedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingInvoicePaidDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotPayContractor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotInvoiceClient = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colRouteOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditLatLong2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colStartLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinishLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinishLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobStatusDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedDurationUnitsDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualDurationUnitsDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFullDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaximumDaysFromLastVisit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinimumDaysFromLastVisit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDaysLeeway = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingCentreCodeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSitePostcode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToVisit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientAndContract = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRework = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReworkOriginalJobID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnClearJobsFromMap = new DevExpress.XtraEditors.SimpleButton();
            this.btnLoadJobsIntoMap = new DevExpress.XtraEditors.SimpleButton();
            this.pmSiteGridMenu = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiViewSiteOnMap = new DevExpress.XtraBars.BarButtonItem();
            this.bbiExportSitesToExcel = new DevExpress.XtraBars.BarButtonItem();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bciLegend = new DevExpress.XtraBars.BarCheckItem();
            this.bciNavigation = new DevExpress.XtraBars.BarCheckItem();
            this.bciMiniMap = new DevExpress.XtraBars.BarCheckItem();
            this.bciLayerManager = new DevExpress.XtraBars.BarCheckItem();
            this.bsiMapObjectCount = new DevExpress.XtraBars.BarStaticItem();
            this.bbiMapPrintPreview = new DevExpress.XtraBars.BarButtonItem();
            this.pmMapControl = new DevExpress.XtraBars.PopupMenu(this.components);
            this.sp06152_OM_Mapping_VisitsTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06152_OM_Mapping_VisitsTableAdapter();
            this.sp06153_OM_Mapping_Visits_Map_ObjectsTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06153_OM_Mapping_Visits_Map_ObjectsTableAdapter();
            this.sp06154_OM_Visit_Mapping_SitesTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06154_OM_Visit_Mapping_SitesTableAdapter();
            this.sp06155_OM_Visit_Mapping_Sites_Map_ObjectsTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06155_OM_Visit_Mapping_Sites_Map_ObjectsTableAdapter();
            this.imageCollection32x32 = new DevExpress.Utils.ImageCollection(this.components);
            this.sp06276_OM_Mapping_JobsTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06276_OM_Mapping_JobsTableAdapter();
            this.sp06277_OM_Mapping_Jobs_Map_ObjectsTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06277_OM_Mapping_Jobs_Map_ObjectsTableAdapter();
            this.pmVisitGridMenu = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiViewVisitStartOnMap = new DevExpress.XtraBars.BarButtonItem();
            this.bbiViewVisitFinishOnMap = new DevExpress.XtraBars.BarButtonItem();
            this.bbiExportVisitsToExcel = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.pmJobGridMenu = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiViewJobStartOnMap = new DevExpress.XtraBars.BarButtonItem();
            this.bbiViewJobFinishOnMap = new DevExpress.XtraBars.BarButtonItem();
            this.bbiExportJobsToExcel = new DevExpress.XtraBars.BarButtonItem();
            this.sp06354_OM_Mapping_Layer_ManagerTableAdapter = new WoodPlan5.DataSet_OM_CoreTableAdapters.sp06354_OM_Mapping_Layer_ManagerTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06155OMVisitMappingSitesMapObjectsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Visit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06153OMMappingVisitsMapObjectsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06277OMMappingJobsMapObjectsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Job)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06154OMVisitMappingSitesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditLatLong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06152OMMappingVisitsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlMapType)).BeginInit();
            this.popupContainerControlMapType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlMapType)).BeginInit();
            this.groupControlMapType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mapControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditMapType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanelLayerManager.SuspendLayout();
            this.controlContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06354OMMappingLayerManagerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection16x16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            this.dockPanelLegend.SuspendLayout();
            this.dockPanel2_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            this.dockPanelData.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditChooseSites.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06276OMMappingJobsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDate2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric2DP2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditLatLong2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmSiteGridMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmMapControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection32x32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmVisitGridMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmJobGridMenu)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(891, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 695);
            this.barDockControlBottom.Size = new System.Drawing.Size(891, 42);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 695);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(891, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 695);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockManager = this.dockManager1;
            this.barManager1.Images = this.imageCollection32x32;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barEditItemMapType,
            this.bbiViewSiteOnMap,
            this.bsiMapObjectCount,
            this.bbiExportSitesToExcel,
            this.bbiMapPrintPreview,
            this.bciLegend,
            this.bbiViewVisitStartOnMap,
            this.bbiViewVisitFinishOnMap,
            this.bbiExportVisitsToExcel,
            this.barButtonItem1,
            this.bbiViewJobStartOnMap,
            this.bbiViewJobFinishOnMap,
            this.bbiExportJobsToExcel,
            this.bciMiniMap,
            this.bciLayerManager,
            this.bciNavigation});
            this.barManager1.MaxItemId = 52;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemRadioGroup1,
            this.repositoryItemPopupContainerEditMapType});
            this.barManager1.StatusBar = this.bar2;
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colRecordEnabled
            // 
            this.colRecordEnabled.Caption = "Site Enabled";
            this.colRecordEnabled.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colRecordEnabled.FieldName = "RecordEnabled";
            this.colRecordEnabled.Name = "colRecordEnabled";
            this.colRecordEnabled.OptionsColumn.AllowEdit = false;
            this.colRecordEnabled.OptionsColumn.AllowFocus = false;
            this.colRecordEnabled.OptionsColumn.ReadOnly = true;
            this.colRecordEnabled.Width = 80;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colLayerOrder
            // 
            this.colLayerOrder.Caption = "Layer Order";
            this.colLayerOrder.FieldName = "LayerOrder";
            this.colLayerOrder.Name = "colLayerOrder";
            this.colLayerOrder.OptionsColumn.AllowEdit = false;
            this.colLayerOrder.OptionsColumn.AllowFocus = false;
            this.colLayerOrder.OptionsColumn.ReadOnly = true;
            this.colLayerOrder.Visible = true;
            this.colLayerOrder.VisibleIndex = 1;
            this.imageTilesLayer1.DataProvider = this.bingMapDataProvider1;
            this.imageTilesLayer1.ZIndex = 4;
            this.bingMapDataProvider1.BingKey = "As_rq12T3oFpxWh52cGqK5yhqcHov7wkd4orPObzZFgo2N92CqPDc1xO_uokdUdT";
            this.vectorItemsLayer1.Data = this.listSourceDataAdapter1;
            this.vectorItemsLayer1.ToolTipPattern = "<b>Client:</b> {ClientName}\r\n<b>Site:</b>    {SiteName}\r\n";
            this.vectorItemsLayer1.ZIndex = 3;
            mapItemAttributeMapping1.Member = "SiteID";
            mapItemAttributeMapping1.Name = "SiteID";
            mapItemAttributeMapping1.ValueType = DevExpress.XtraMap.FieldValueType.Integer;
            mapItemAttributeMapping2.Member = "ClientID";
            mapItemAttributeMapping2.Name = "ClientID";
            mapItemAttributeMapping2.ValueType = DevExpress.XtraMap.FieldValueType.Integer;
            mapItemAttributeMapping3.Member = "SiteName";
            mapItemAttributeMapping3.Name = "SiteName";
            mapItemAttributeMapping3.ValueType = DevExpress.XtraMap.FieldValueType.String;
            mapItemAttributeMapping4.Member = "ClientName";
            mapItemAttributeMapping4.Name = "ClientName";
            mapItemAttributeMapping4.ValueType = DevExpress.XtraMap.FieldValueType.String;
            this.listSourceDataAdapter1.AttributeMappings.Add(mapItemAttributeMapping1);
            this.listSourceDataAdapter1.AttributeMappings.Add(mapItemAttributeMapping2);
            this.listSourceDataAdapter1.AttributeMappings.Add(mapItemAttributeMapping3);
            this.listSourceDataAdapter1.AttributeMappings.Add(mapItemAttributeMapping4);
            this.listSourceDataAdapter1.DataSource = this.sp06155OMVisitMappingSitesMapObjectsBindingSource;
            this.listSourceDataAdapter1.DefaultMapItemType = DevExpress.XtraMap.MapItemType.Custom;
            this.listSourceDataAdapter1.Mappings.ImageIndex = "ThematicValue";
            this.listSourceDataAdapter1.Mappings.Latitude = "Latitude";
            this.listSourceDataAdapter1.Mappings.Longitude = "Longitude";
            this.listSourceDataAdapter1.Mappings.Text = "MapLabel";
            // 
            // sp06155OMVisitMappingSitesMapObjectsBindingSource
            // 
            this.sp06155OMVisitMappingSitesMapObjectsBindingSource.DataMember = "sp06155_OM_Visit_Mapping_Sites_Map_Objects";
            this.sp06155OMVisitMappingSitesMapObjectsBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // dataSet_OM_Visit
            // 
            this.dataSet_OM_Visit.DataSetName = "DataSet_OM_Visit";
            this.dataSet_OM_Visit.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            this.vectorItemsLayer2.Data = this.listSourceDataAdapter2;
            this.vectorItemsLayer2.ToolTipPattern = resources.GetString("vectorItemsLayer2.ToolTipPattern");
            this.vectorItemsLayer2.ZIndex = 2;
            mapItemAttributeMapping5.Member = "ExpectedStartDate";
            mapItemAttributeMapping5.Name = "ExpectedStartDate";
            mapItemAttributeMapping5.ValueType = DevExpress.XtraMap.FieldValueType.DateTime;
            mapItemAttributeMapping6.Member = "ExpectedEndDate";
            mapItemAttributeMapping6.Name = "ExpectedEndDate";
            mapItemAttributeMapping6.ValueType = DevExpress.XtraMap.FieldValueType.DateTime;
            mapItemAttributeMapping7.Member = "StartDate";
            mapItemAttributeMapping7.Name = "StartDate";
            mapItemAttributeMapping7.ValueType = DevExpress.XtraMap.FieldValueType.DateTime;
            mapItemAttributeMapping8.Member = "EndDate";
            mapItemAttributeMapping8.Name = "EndDate";
            mapItemAttributeMapping8.ValueType = DevExpress.XtraMap.FieldValueType.DateTime;
            mapItemAttributeMapping9.Member = "ClientName";
            mapItemAttributeMapping9.Name = "ClientName";
            mapItemAttributeMapping9.ValueType = DevExpress.XtraMap.FieldValueType.String;
            mapItemAttributeMapping10.Member = "ContractDescription";
            mapItemAttributeMapping10.Name = "ContractDescription";
            mapItemAttributeMapping10.ValueType = DevExpress.XtraMap.FieldValueType.String;
            mapItemAttributeMapping11.Member = "SiteName";
            mapItemAttributeMapping11.Name = "SiteName";
            mapItemAttributeMapping11.ValueType = DevExpress.XtraMap.FieldValueType.String;
            mapItemAttributeMapping12.Member = "VisitNumber";
            mapItemAttributeMapping12.Name = "VisitNumber";
            mapItemAttributeMapping12.ValueType = DevExpress.XtraMap.FieldValueType.Integer;
            mapItemAttributeMapping13.Member = "VisitID";
            mapItemAttributeMapping13.Name = "VisitID";
            mapItemAttributeMapping13.ValueType = DevExpress.XtraMap.FieldValueType.Integer;
            mapItemAttributeMapping14.Member = "LinkedJobCount";
            mapItemAttributeMapping14.Name = "LinkedJobCount";
            mapItemAttributeMapping14.ValueType = DevExpress.XtraMap.FieldValueType.Integer;
            mapItemAttributeMapping15.Member = "LinkedCompletedJobCount";
            mapItemAttributeMapping15.Name = "LinkedCompletedJobCount";
            mapItemAttributeMapping15.ValueType = DevExpress.XtraMap.FieldValueType.Integer;
            mapItemAttributeMapping16.Member = "LinkedOutstandingJobCount";
            mapItemAttributeMapping16.Name = "LinkedOutstandingJobCount";
            mapItemAttributeMapping16.ValueType = DevExpress.XtraMap.FieldValueType.Integer;
            mapItemAttributeMapping17.Member = "RecordType";
            mapItemAttributeMapping17.Name = "RecordType";
            mapItemAttributeMapping17.ValueType = DevExpress.XtraMap.FieldValueType.String;
            this.listSourceDataAdapter2.AttributeMappings.Add(mapItemAttributeMapping5);
            this.listSourceDataAdapter2.AttributeMappings.Add(mapItemAttributeMapping6);
            this.listSourceDataAdapter2.AttributeMappings.Add(mapItemAttributeMapping7);
            this.listSourceDataAdapter2.AttributeMappings.Add(mapItemAttributeMapping8);
            this.listSourceDataAdapter2.AttributeMappings.Add(mapItemAttributeMapping9);
            this.listSourceDataAdapter2.AttributeMappings.Add(mapItemAttributeMapping10);
            this.listSourceDataAdapter2.AttributeMappings.Add(mapItemAttributeMapping11);
            this.listSourceDataAdapter2.AttributeMappings.Add(mapItemAttributeMapping12);
            this.listSourceDataAdapter2.AttributeMappings.Add(mapItemAttributeMapping13);
            this.listSourceDataAdapter2.AttributeMappings.Add(mapItemAttributeMapping14);
            this.listSourceDataAdapter2.AttributeMappings.Add(mapItemAttributeMapping15);
            this.listSourceDataAdapter2.AttributeMappings.Add(mapItemAttributeMapping16);
            this.listSourceDataAdapter2.AttributeMappings.Add(mapItemAttributeMapping17);
            this.listSourceDataAdapter2.DataSource = this.sp06153OMMappingVisitsMapObjectsBindingSource;
            this.listSourceDataAdapter2.DefaultMapItemType = DevExpress.XtraMap.MapItemType.Custom;
            this.listSourceDataAdapter2.Mappings.ImageIndex = "ThematicValue";
            this.listSourceDataAdapter2.Mappings.Latitude = "Latitude";
            this.listSourceDataAdapter2.Mappings.Longitude = "Longitude";
            this.listSourceDataAdapter2.Mappings.Text = "MapLabel";
            // 
            // sp06153OMMappingVisitsMapObjectsBindingSource
            // 
            this.sp06153OMMappingVisitsMapObjectsBindingSource.DataMember = "sp06153_OM_Mapping_Visits_Map_Objects";
            this.sp06153OMMappingVisitsMapObjectsBindingSource.DataSource = this.dataSet_OM_Visit;
            this.vectorItemsLayer3.Data = this.listSourceDataAdapter3;
            this.vectorItemsLayer3.ToolTipPattern = resources.GetString("vectorItemsLayer3.ToolTipPattern");
            this.vectorItemsLayer3.ZIndex = 1;
            mapItemAttributeMapping18.Member = "JobID";
            mapItemAttributeMapping18.Name = "JobID";
            mapItemAttributeMapping18.ValueType = DevExpress.XtraMap.FieldValueType.Integer;
            mapItemAttributeMapping19.Member = "VisitID";
            mapItemAttributeMapping19.Name = "VisitID";
            mapItemAttributeMapping19.ValueType = DevExpress.XtraMap.FieldValueType.Integer;
            mapItemAttributeMapping20.Member = "ClientName";
            mapItemAttributeMapping20.Name = "ClientName";
            mapItemAttributeMapping20.ValueType = DevExpress.XtraMap.FieldValueType.String;
            mapItemAttributeMapping21.Member = "ContractDescription";
            mapItemAttributeMapping21.Name = "ContractDescription";
            mapItemAttributeMapping21.ValueType = DevExpress.XtraMap.FieldValueType.String;
            mapItemAttributeMapping22.Member = "SiteName";
            mapItemAttributeMapping22.Name = "SiteName";
            mapItemAttributeMapping22.ValueType = DevExpress.XtraMap.FieldValueType.String;
            mapItemAttributeMapping23.Member = "VisitNumber";
            mapItemAttributeMapping23.Name = "VisitNumber";
            mapItemAttributeMapping23.ValueType = DevExpress.XtraMap.FieldValueType.Integer;
            mapItemAttributeMapping24.Member = "JobTypeDescription";
            mapItemAttributeMapping24.Name = "JobTypeDescription";
            mapItemAttributeMapping24.ValueType = DevExpress.XtraMap.FieldValueType.String;
            mapItemAttributeMapping25.Member = "JobSubTypeDescription";
            mapItemAttributeMapping25.Name = "JobSubTypeDescription";
            mapItemAttributeMapping25.ValueType = DevExpress.XtraMap.FieldValueType.String;
            mapItemAttributeMapping26.Member = "JobStatusDescription";
            mapItemAttributeMapping26.Name = "JobStatusDescription";
            mapItemAttributeMapping26.ValueType = DevExpress.XtraMap.FieldValueType.String;
            mapItemAttributeMapping27.Member = "ExpectedStartDate";
            mapItemAttributeMapping27.Name = "ExpectedStartDate";
            mapItemAttributeMapping27.ValueType = DevExpress.XtraMap.FieldValueType.DateTime;
            mapItemAttributeMapping28.Member = "ExpectedEndDate";
            mapItemAttributeMapping28.Name = "ExpectedEndDate";
            mapItemAttributeMapping28.ValueType = DevExpress.XtraMap.FieldValueType.DateTime;
            mapItemAttributeMapping29.Member = "ActualStartDate";
            mapItemAttributeMapping29.Name = "ActualStartDate";
            mapItemAttributeMapping29.ValueType = DevExpress.XtraMap.FieldValueType.DateTime;
            mapItemAttributeMapping30.Member = "ActualEndDate";
            mapItemAttributeMapping30.Name = "ActualEndDate";
            mapItemAttributeMapping30.ValueType = DevExpress.XtraMap.FieldValueType.DateTime;
            mapItemAttributeMapping31.Member = "ObjectType";
            mapItemAttributeMapping31.Name = "ObjectType";
            mapItemAttributeMapping31.ValueType = DevExpress.XtraMap.FieldValueType.String;
            this.listSourceDataAdapter3.AttributeMappings.Add(mapItemAttributeMapping18);
            this.listSourceDataAdapter3.AttributeMappings.Add(mapItemAttributeMapping19);
            this.listSourceDataAdapter3.AttributeMappings.Add(mapItemAttributeMapping20);
            this.listSourceDataAdapter3.AttributeMappings.Add(mapItemAttributeMapping21);
            this.listSourceDataAdapter3.AttributeMappings.Add(mapItemAttributeMapping22);
            this.listSourceDataAdapter3.AttributeMappings.Add(mapItemAttributeMapping23);
            this.listSourceDataAdapter3.AttributeMappings.Add(mapItemAttributeMapping24);
            this.listSourceDataAdapter3.AttributeMappings.Add(mapItemAttributeMapping25);
            this.listSourceDataAdapter3.AttributeMappings.Add(mapItemAttributeMapping26);
            this.listSourceDataAdapter3.AttributeMappings.Add(mapItemAttributeMapping27);
            this.listSourceDataAdapter3.AttributeMappings.Add(mapItemAttributeMapping28);
            this.listSourceDataAdapter3.AttributeMappings.Add(mapItemAttributeMapping29);
            this.listSourceDataAdapter3.AttributeMappings.Add(mapItemAttributeMapping30);
            this.listSourceDataAdapter3.AttributeMappings.Add(mapItemAttributeMapping31);
            this.listSourceDataAdapter3.DataSource = this.sp06277OMMappingJobsMapObjectsBindingSource;
            this.listSourceDataAdapter3.DefaultMapItemType = DevExpress.XtraMap.MapItemType.Custom;
            this.listSourceDataAdapter3.Mappings.ImageIndex = "ThematicValue";
            this.listSourceDataAdapter3.Mappings.Latitude = "Latitude";
            this.listSourceDataAdapter3.Mappings.Longitude = "Longitude";
            this.listSourceDataAdapter3.Mappings.Text = "MapLabel";
            // 
            // sp06277OMMappingJobsMapObjectsBindingSource
            // 
            this.sp06277OMMappingJobsMapObjectsBindingSource.DataMember = "sp06277_OM_Mapping_Jobs_Map_Objects";
            this.sp06277OMMappingJobsMapObjectsBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // dataSet_OM_Job
            // 
            this.dataSet_OM_Job.DataSetName = "DataSet_OM_Job";
            this.dataSet_OM_Job.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.DataSource = this.sp06154OMVisitMappingSitesBindingSource;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 21);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemTextEditLatLong,
            this.repositoryItemCheckEdit1});
            this.gridControl1.Size = new System.Drawing.Size(372, 289);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp06154OMVisitMappingSitesBindingSource
            // 
            this.sp06154OMVisitMappingSitesBindingSource.DataMember = "sp06154_OM_Visit_Mapping_Sites";
            this.sp06154OMVisitMappingSitesBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSiteID,
            this.colClientID,
            this.colSiteTypeID,
            this.colSiteName,
            this.colSiteAddressLine1,
            this.colSiteAddressLine2,
            this.colSiteAddressLine3,
            this.colSiteAddressLine4,
            this.colSiteAddressLine5,
            this.colSitePostcode,
            this.colRemarks,
            this.colLatitude,
            this.colLongitude,
            this.colSiteType,
            this.colClientName,
            this.colThematicValue,
            this.colMapLabel,
            this.colRecordEnabled});
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.colRecordEnabled;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Gray;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView1.FormatRules.Add(gridFormatRule1);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsFind.FindDelay = 2000;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colSiteID
            // 
            this.colSiteID.Caption = "Site ID";
            this.colSiteID.FieldName = "SiteID";
            this.colSiteID.Name = "colSiteID";
            this.colSiteID.OptionsColumn.AllowEdit = false;
            this.colSiteID.OptionsColumn.AllowFocus = false;
            this.colSiteID.OptionsColumn.ReadOnly = true;
            this.colSiteID.Visible = true;
            this.colSiteID.VisibleIndex = 0;
            this.colSiteID.Width = 51;
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // colSiteTypeID
            // 
            this.colSiteTypeID.Caption = "Site Type ID";
            this.colSiteTypeID.FieldName = "SiteTypeID";
            this.colSiteTypeID.Name = "colSiteTypeID";
            this.colSiteTypeID.OptionsColumn.AllowEdit = false;
            this.colSiteTypeID.OptionsColumn.AllowFocus = false;
            this.colSiteTypeID.OptionsColumn.ReadOnly = true;
            this.colSiteTypeID.Width = 80;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.Visible = true;
            this.colSiteName.VisibleIndex = 1;
            this.colSiteName.Width = 160;
            // 
            // colSiteAddressLine1
            // 
            this.colSiteAddressLine1.Caption = "Address Line 1";
            this.colSiteAddressLine1.FieldName = "SiteAddressLine1";
            this.colSiteAddressLine1.Name = "colSiteAddressLine1";
            this.colSiteAddressLine1.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine1.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine1.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine1.Visible = true;
            this.colSiteAddressLine1.VisibleIndex = 3;
            this.colSiteAddressLine1.Width = 91;
            // 
            // colSiteAddressLine2
            // 
            this.colSiteAddressLine2.Caption = "Address Line 2";
            this.colSiteAddressLine2.FieldName = "SiteAddressLine2";
            this.colSiteAddressLine2.Name = "colSiteAddressLine2";
            this.colSiteAddressLine2.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine2.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine2.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine2.Visible = true;
            this.colSiteAddressLine2.VisibleIndex = 4;
            this.colSiteAddressLine2.Width = 91;
            // 
            // colSiteAddressLine3
            // 
            this.colSiteAddressLine3.Caption = "Address Line 3";
            this.colSiteAddressLine3.FieldName = "SiteAddressLine3";
            this.colSiteAddressLine3.Name = "colSiteAddressLine3";
            this.colSiteAddressLine3.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine3.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine3.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine3.Visible = true;
            this.colSiteAddressLine3.VisibleIndex = 5;
            this.colSiteAddressLine3.Width = 91;
            // 
            // colSiteAddressLine4
            // 
            this.colSiteAddressLine4.Caption = "Address Line 4";
            this.colSiteAddressLine4.FieldName = "SiteAddressLine4";
            this.colSiteAddressLine4.Name = "colSiteAddressLine4";
            this.colSiteAddressLine4.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine4.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine4.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine4.Visible = true;
            this.colSiteAddressLine4.VisibleIndex = 6;
            this.colSiteAddressLine4.Width = 91;
            // 
            // colSiteAddressLine5
            // 
            this.colSiteAddressLine5.Caption = "Address Line 5";
            this.colSiteAddressLine5.FieldName = "SiteAddressLine5";
            this.colSiteAddressLine5.Name = "colSiteAddressLine5";
            this.colSiteAddressLine5.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine5.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine5.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine5.Visible = true;
            this.colSiteAddressLine5.VisibleIndex = 7;
            this.colSiteAddressLine5.Width = 91;
            // 
            // colSitePostcode
            // 
            this.colSitePostcode.Caption = "Postcode";
            this.colSitePostcode.FieldName = "SitePostcode";
            this.colSitePostcode.Name = "colSitePostcode";
            this.colSitePostcode.OptionsColumn.AllowEdit = false;
            this.colSitePostcode.OptionsColumn.AllowFocus = false;
            this.colSitePostcode.OptionsColumn.ReadOnly = true;
            this.colSitePostcode.Visible = true;
            this.colSitePostcode.VisibleIndex = 8;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 9;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colLatitude
            // 
            this.colLatitude.Caption = "Latitude";
            this.colLatitude.ColumnEdit = this.repositoryItemTextEditLatLong;
            this.colLatitude.FieldName = "Latitude";
            this.colLatitude.Name = "colLatitude";
            this.colLatitude.OptionsColumn.AllowEdit = false;
            this.colLatitude.OptionsColumn.AllowFocus = false;
            this.colLatitude.OptionsColumn.ReadOnly = true;
            this.colLatitude.Visible = true;
            this.colLatitude.VisibleIndex = 10;
            // 
            // repositoryItemTextEditLatLong
            // 
            this.repositoryItemTextEditLatLong.AutoHeight = false;
            this.repositoryItemTextEditLatLong.Mask.EditMask = "n8";
            this.repositoryItemTextEditLatLong.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditLatLong.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditLatLong.Name = "repositoryItemTextEditLatLong";
            // 
            // colLongitude
            // 
            this.colLongitude.Caption = "Longitude";
            this.colLongitude.ColumnEdit = this.repositoryItemTextEditLatLong;
            this.colLongitude.FieldName = "Longitude";
            this.colLongitude.Name = "colLongitude";
            this.colLongitude.OptionsColumn.AllowEdit = false;
            this.colLongitude.OptionsColumn.AllowFocus = false;
            this.colLongitude.OptionsColumn.ReadOnly = true;
            this.colLongitude.Visible = true;
            this.colLongitude.VisibleIndex = 11;
            // 
            // colSiteType
            // 
            this.colSiteType.Caption = "Site Type";
            this.colSiteType.FieldName = "SiteType";
            this.colSiteType.Name = "colSiteType";
            this.colSiteType.OptionsColumn.AllowEdit = false;
            this.colSiteType.OptionsColumn.AllowFocus = false;
            this.colSiteType.OptionsColumn.ReadOnly = true;
            this.colSiteType.Visible = true;
            this.colSiteType.VisibleIndex = 2;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 12;
            this.colClientName.Width = 241;
            // 
            // colThematicValue
            // 
            this.colThematicValue.Caption = "Thematic Value";
            this.colThematicValue.FieldName = "ThematicValue";
            this.colThematicValue.Name = "colThematicValue";
            this.colThematicValue.OptionsColumn.AllowEdit = false;
            this.colThematicValue.OptionsColumn.AllowFocus = false;
            this.colThematicValue.OptionsColumn.ReadOnly = true;
            this.colThematicValue.Width = 93;
            // 
            // colMapLabel
            // 
            this.colMapLabel.Caption = "Map Label";
            this.colMapLabel.FieldName = "MapLabel";
            this.colMapLabel.Name = "colMapLabel";
            this.colMapLabel.OptionsColumn.AllowEdit = false;
            this.colMapLabel.OptionsColumn.AllowFocus = false;
            this.colMapLabel.OptionsColumn.ReadOnly = true;
            // 
            // sp06152OMMappingVisitsBindingSource
            // 
            this.sp06152OMMappingVisitsBindingSource.DataMember = "sp06152_OM_Mapping_Visits";
            this.sp06152OMMappingVisitsBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // popupContainerControlMapType
            // 
            this.popupContainerControlMapType.Controls.Add(this.groupControlMapType);
            this.popupContainerControlMapType.Controls.Add(this.btnOKMapType);
            this.popupContainerControlMapType.Location = new System.Drawing.Point(456, 72);
            this.popupContainerControlMapType.Name = "popupContainerControlMapType";
            this.popupContainerControlMapType.Size = new System.Drawing.Size(129, 157);
            this.popupContainerControlMapType.TabIndex = 9;
            // 
            // groupControlMapType
            // 
            this.groupControlMapType.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControlMapType.Controls.Add(this.checkEdit4);
            this.groupControlMapType.Controls.Add(this.checkEdit3);
            this.groupControlMapType.Controls.Add(this.checkEdit2);
            this.groupControlMapType.Controls.Add(this.checkEdit1);
            this.groupControlMapType.Location = new System.Drawing.Point(3, 4);
            this.groupControlMapType.Name = "groupControlMapType";
            this.groupControlMapType.Size = new System.Drawing.Size(123, 123);
            this.groupControlMapType.TabIndex = 3;
            this.groupControlMapType.Text = "Map Type";
            // 
            // checkEdit4
            // 
            this.checkEdit4.Location = new System.Drawing.Point(5, 99);
            this.checkEdit4.MenuManager = this.barManager1;
            this.checkEdit4.Name = "checkEdit4";
            this.checkEdit4.Properties.Caption = "Open Street Map";
            this.checkEdit4.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit4.Properties.RadioGroupIndex = 1;
            this.checkEdit4.Size = new System.Drawing.Size(110, 19);
            this.checkEdit4.TabIndex = 3;
            this.checkEdit4.TabStop = false;
            // 
            // checkEdit3
            // 
            this.checkEdit3.Location = new System.Drawing.Point(5, 74);
            this.checkEdit3.MenuManager = this.barManager1;
            this.checkEdit3.Name = "checkEdit3";
            this.checkEdit3.Properties.Caption = "Bing Road";
            this.checkEdit3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit3.Properties.RadioGroupIndex = 1;
            this.checkEdit3.Size = new System.Drawing.Size(110, 19);
            this.checkEdit3.TabIndex = 2;
            this.checkEdit3.TabStop = false;
            // 
            // checkEdit2
            // 
            this.checkEdit2.Location = new System.Drawing.Point(5, 49);
            this.checkEdit2.MenuManager = this.barManager1;
            this.checkEdit2.Name = "checkEdit2";
            this.checkEdit2.Properties.Caption = "Bing Satellite";
            this.checkEdit2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit2.Properties.RadioGroupIndex = 1;
            this.checkEdit2.Size = new System.Drawing.Size(110, 19);
            this.checkEdit2.TabIndex = 1;
            this.checkEdit2.TabStop = false;
            // 
            // checkEdit1
            // 
            this.checkEdit1.EditValue = true;
            this.checkEdit1.Location = new System.Drawing.Point(5, 24);
            this.checkEdit1.MenuManager = this.barManager1;
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "Bing Hybrid";
            this.checkEdit1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit1.Properties.RadioGroupIndex = 1;
            this.checkEdit1.Size = new System.Drawing.Size(110, 19);
            this.checkEdit1.TabIndex = 0;
            // 
            // btnOKMapType
            // 
            this.btnOKMapType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOKMapType.Location = new System.Drawing.Point(3, 131);
            this.btnOKMapType.Name = "btnOKMapType";
            this.btnOKMapType.Size = new System.Drawing.Size(38, 23);
            this.btnOKMapType.TabIndex = 2;
            this.btnOKMapType.Text = "OK";
            this.btnOKMapType.Click += new System.EventHandler(this.btnOKMapType_Click);
            // 
            // mapControl1
            // 
            this.mapControl1.CenterPoint = new DevExpress.XtraMap.GeoPoint(51.63060168D, 0.41083011D);
            this.mapControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mapControl1.ImageList = this.imageCollection1;
            this.mapControl1.Layers.Add(this.imageTilesLayer1);
            this.mapControl1.Layers.Add(this.vectorItemsLayer1);
            this.mapControl1.Layers.Add(this.vectorItemsLayer2);
            this.mapControl1.Layers.Add(this.vectorItemsLayer3);
            this.mapControl1.Location = new System.Drawing.Point(379, 0);
            this.mapControl1.Name = "mapControl1";
            this.mapControl1.Size = new System.Drawing.Size(512, 695);
            this.mapControl1.TabIndex = 0;
            this.mapControl1.ToolTipController = this.toolTipController1;
            this.mapControl1.ZoomLevel = 12D;
            this.mapControl1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.mapControl1_MouseUp);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageSize = new System.Drawing.Size(32, 37);
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "Blue_blank.png");
            this.imageCollection1.Images.SetKeyName(1, "number_0.png");
            this.imageCollection1.Images.SetKeyName(2, "number_1.png");
            this.imageCollection1.Images.SetKeyName(3, "number_2.png");
            this.imageCollection1.Images.SetKeyName(4, "number_3.png");
            this.imageCollection1.Images.SetKeyName(5, "number_4.png");
            this.imageCollection1.Images.SetKeyName(6, "number_5.png");
            this.imageCollection1.Images.SetKeyName(7, "number_6.png");
            this.imageCollection1.Images.SetKeyName(8, "number_7.png");
            this.imageCollection1.Images.SetKeyName(9, "number_8.png");
            this.imageCollection1.Images.SetKeyName(10, "number_9.png");
            this.imageCollection1.Images.SetKeyName(11, "number_10.png");
            this.imageCollection1.Images.SetKeyName(12, "number_11.png");
            this.imageCollection1.Images.SetKeyName(13, "number_12.png");
            this.imageCollection1.Images.SetKeyName(14, "number_13.png");
            this.imageCollection1.Images.SetKeyName(15, "number_14.png");
            this.imageCollection1.Images.SetKeyName(16, "number_15.png");
            this.imageCollection1.Images.SetKeyName(17, "number_16.png");
            this.imageCollection1.Images.SetKeyName(18, "number_17.png");
            this.imageCollection1.Images.SetKeyName(19, "number_18.png");
            this.imageCollection1.Images.SetKeyName(20, "number_19.png");
            this.imageCollection1.Images.SetKeyName(21, "number_20.png");
            this.imageCollection1.Images.SetKeyName(22, "number_21.png");
            this.imageCollection1.Images.SetKeyName(23, "number_22.png");
            this.imageCollection1.Images.SetKeyName(24, "number_23.png");
            this.imageCollection1.Images.SetKeyName(25, "number_24.png");
            this.imageCollection1.Images.SetKeyName(26, "number_25.png");
            this.imageCollection1.Images.SetKeyName(27, "number_26.png");
            this.imageCollection1.Images.SetKeyName(28, "number_27.png");
            this.imageCollection1.Images.SetKeyName(29, "number_28.png");
            this.imageCollection1.Images.SetKeyName(30, "number_29.png");
            this.imageCollection1.Images.SetKeyName(31, "number_30.png");
            this.imageCollection1.Images.SetKeyName(32, "number_31.png");
            this.imageCollection1.Images.SetKeyName(33, "number_32.png");
            this.imageCollection1.Images.SetKeyName(34, "number_33.png");
            this.imageCollection1.Images.SetKeyName(35, "number_34.png");
            this.imageCollection1.Images.SetKeyName(36, "number_35.png");
            this.imageCollection1.Images.SetKeyName(37, "number_36.png");
            this.imageCollection1.Images.SetKeyName(38, "number_37.png");
            this.imageCollection1.Images.SetKeyName(39, "number_38.png");
            this.imageCollection1.Images.SetKeyName(40, "number_39.png");
            this.imageCollection1.Images.SetKeyName(41, "number_40.png");
            this.imageCollection1.Images.SetKeyName(42, "number_41.png");
            this.imageCollection1.Images.SetKeyName(43, "number_42.png");
            this.imageCollection1.Images.SetKeyName(44, "number_43.png");
            this.imageCollection1.Images.SetKeyName(45, "number_44.png");
            this.imageCollection1.Images.SetKeyName(46, "number_45.png");
            this.imageCollection1.Images.SetKeyName(47, "number_46.png");
            this.imageCollection1.Images.SetKeyName(48, "number_47.png");
            this.imageCollection1.Images.SetKeyName(49, "number_48.png");
            this.imageCollection1.Images.SetKeyName(50, "number_49.png");
            this.imageCollection1.Images.SetKeyName(51, "number_50.png");
            this.imageCollection1.Images.SetKeyName(52, "number_51.png");
            this.imageCollection1.Images.SetKeyName(53, "number_52.png");
            this.imageCollection1.Images.SetKeyName(54, "number_53.png");
            this.imageCollection1.Images.SetKeyName(55, "number_54.png");
            this.imageCollection1.Images.SetKeyName(56, "number_55.png");
            this.imageCollection1.Images.SetKeyName(57, "number_56.png");
            this.imageCollection1.Images.SetKeyName(58, "number_57.png");
            this.imageCollection1.Images.SetKeyName(59, "number_58.png");
            this.imageCollection1.Images.SetKeyName(60, "number_59.png");
            this.imageCollection1.Images.SetKeyName(61, "number_60.png");
            this.imageCollection1.Images.SetKeyName(62, "number_61.png");
            this.imageCollection1.Images.SetKeyName(63, "number_62.png");
            this.imageCollection1.Images.SetKeyName(64, "number_63.png");
            this.imageCollection1.Images.SetKeyName(65, "number_64.png");
            this.imageCollection1.Images.SetKeyName(66, "number_65.png");
            this.imageCollection1.Images.SetKeyName(67, "number_66.png");
            this.imageCollection1.Images.SetKeyName(68, "number_67.png");
            this.imageCollection1.Images.SetKeyName(69, "number_68.png");
            this.imageCollection1.Images.SetKeyName(70, "number_69.png");
            this.imageCollection1.Images.SetKeyName(71, "number_70.png");
            this.imageCollection1.Images.SetKeyName(72, "number_71.png");
            this.imageCollection1.Images.SetKeyName(73, "number_72.png");
            this.imageCollection1.Images.SetKeyName(74, "number_73.png");
            this.imageCollection1.Images.SetKeyName(75, "number_74.png");
            this.imageCollection1.Images.SetKeyName(76, "number_75.png");
            this.imageCollection1.Images.SetKeyName(77, "number_76.png");
            this.imageCollection1.Images.SetKeyName(78, "number_77.png");
            this.imageCollection1.Images.SetKeyName(79, "number_78.png");
            this.imageCollection1.Images.SetKeyName(80, "number_79.png");
            this.imageCollection1.Images.SetKeyName(81, "number_80.png");
            this.imageCollection1.Images.SetKeyName(82, "number_81.png");
            this.imageCollection1.Images.SetKeyName(83, "number_82.png");
            this.imageCollection1.Images.SetKeyName(84, "number_83.png");
            this.imageCollection1.Images.SetKeyName(85, "number_84.png");
            this.imageCollection1.Images.SetKeyName(86, "number_85.png");
            this.imageCollection1.Images.SetKeyName(87, "number_86.png");
            this.imageCollection1.Images.SetKeyName(88, "number_87.png");
            this.imageCollection1.Images.SetKeyName(89, "number_88.png");
            this.imageCollection1.Images.SetKeyName(90, "number_89.png");
            this.imageCollection1.Images.SetKeyName(91, "number_90.png");
            this.imageCollection1.Images.SetKeyName(92, "number_91.png");
            this.imageCollection1.Images.SetKeyName(93, "number_92.png");
            this.imageCollection1.Images.SetKeyName(94, "number_93.png");
            this.imageCollection1.Images.SetKeyName(95, "number_94.png");
            this.imageCollection1.Images.SetKeyName(96, "number_95.png");
            this.imageCollection1.Images.SetKeyName(97, "number_96.png");
            this.imageCollection1.Images.SetKeyName(98, "number_97.png");
            this.imageCollection1.Images.SetKeyName(99, "number_98.png");
            this.imageCollection1.Images.SetKeyName(100, "number_99.png");
            this.imageCollection1.Images.SetKeyName(101, "number_100.png");
            this.imageCollection1.Images.SetKeyName(102, "number_0.png");
            this.imageCollection1.Images.SetKeyName(103, "number_1.png");
            this.imageCollection1.Images.SetKeyName(104, "number_2.png");
            this.imageCollection1.Images.SetKeyName(105, "number_3.png");
            this.imageCollection1.Images.SetKeyName(106, "number_4.png");
            this.imageCollection1.Images.SetKeyName(107, "number_5.png");
            this.imageCollection1.Images.SetKeyName(108, "number_6.png");
            this.imageCollection1.Images.SetKeyName(109, "number_7.png");
            this.imageCollection1.Images.SetKeyName(110, "number_8.png");
            this.imageCollection1.Images.SetKeyName(111, "number_9.png");
            this.imageCollection1.Images.SetKeyName(112, "number_10.png");
            this.imageCollection1.Images.SetKeyName(113, "number_11.png");
            this.imageCollection1.Images.SetKeyName(114, "number_12.png");
            this.imageCollection1.Images.SetKeyName(115, "number_13.png");
            this.imageCollection1.Images.SetKeyName(116, "number_14.png");
            this.imageCollection1.Images.SetKeyName(117, "number_15.png");
            this.imageCollection1.Images.SetKeyName(118, "number_16.png");
            this.imageCollection1.Images.SetKeyName(119, "number_17.png");
            this.imageCollection1.Images.SetKeyName(120, "number_18.png");
            this.imageCollection1.Images.SetKeyName(121, "number_19.png");
            this.imageCollection1.Images.SetKeyName(122, "number_20.png");
            this.imageCollection1.Images.SetKeyName(123, "number_21.png");
            this.imageCollection1.Images.SetKeyName(124, "number_22.png");
            this.imageCollection1.Images.SetKeyName(125, "number_23.png");
            this.imageCollection1.Images.SetKeyName(126, "number_24.png");
            this.imageCollection1.Images.SetKeyName(127, "number_25.png");
            this.imageCollection1.Images.SetKeyName(128, "number_26.png");
            this.imageCollection1.Images.SetKeyName(129, "number_27.png");
            this.imageCollection1.Images.SetKeyName(130, "number_28.png");
            this.imageCollection1.Images.SetKeyName(131, "number_29.png");
            this.imageCollection1.Images.SetKeyName(132, "number_30.png");
            this.imageCollection1.Images.SetKeyName(133, "number_31.png");
            this.imageCollection1.Images.SetKeyName(134, "number_32.png");
            this.imageCollection1.Images.SetKeyName(135, "number_33.png");
            this.imageCollection1.Images.SetKeyName(136, "number_34.png");
            this.imageCollection1.Images.SetKeyName(137, "number_35.png");
            this.imageCollection1.Images.SetKeyName(138, "number_36.png");
            this.imageCollection1.Images.SetKeyName(139, "number_37.png");
            this.imageCollection1.Images.SetKeyName(140, "number_38.png");
            this.imageCollection1.Images.SetKeyName(141, "number_39.png");
            this.imageCollection1.Images.SetKeyName(142, "number_40.png");
            this.imageCollection1.Images.SetKeyName(143, "number_41.png");
            this.imageCollection1.Images.SetKeyName(144, "number_42.png");
            this.imageCollection1.Images.SetKeyName(145, "number_43.png");
            this.imageCollection1.Images.SetKeyName(146, "number_44.png");
            this.imageCollection1.Images.SetKeyName(147, "number_45.png");
            this.imageCollection1.Images.SetKeyName(148, "number_46.png");
            this.imageCollection1.Images.SetKeyName(149, "number_47.png");
            this.imageCollection1.Images.SetKeyName(150, "number_48.png");
            this.imageCollection1.Images.SetKeyName(151, "number_49.png");
            this.imageCollection1.Images.SetKeyName(152, "number_50.png");
            this.imageCollection1.Images.SetKeyName(153, "number_51.png");
            this.imageCollection1.Images.SetKeyName(154, "number_52.png");
            this.imageCollection1.Images.SetKeyName(155, "number_53.png");
            this.imageCollection1.Images.SetKeyName(156, "number_54.png");
            this.imageCollection1.Images.SetKeyName(157, "number_55.png");
            this.imageCollection1.Images.SetKeyName(158, "number_56.png");
            this.imageCollection1.Images.SetKeyName(159, "number_57.png");
            this.imageCollection1.Images.SetKeyName(160, "number_58.png");
            this.imageCollection1.Images.SetKeyName(161, "number_59.png");
            this.imageCollection1.Images.SetKeyName(162, "number_60.png");
            this.imageCollection1.Images.SetKeyName(163, "number_61.png");
            this.imageCollection1.Images.SetKeyName(164, "number_62.png");
            this.imageCollection1.Images.SetKeyName(165, "number_63.png");
            this.imageCollection1.Images.SetKeyName(166, "number_64.png");
            this.imageCollection1.Images.SetKeyName(167, "number_65.png");
            this.imageCollection1.Images.SetKeyName(168, "number_66.png");
            this.imageCollection1.Images.SetKeyName(169, "number_67.png");
            this.imageCollection1.Images.SetKeyName(170, "number_68.png");
            this.imageCollection1.Images.SetKeyName(171, "number_69.png");
            this.imageCollection1.Images.SetKeyName(172, "number_70.png");
            this.imageCollection1.Images.SetKeyName(173, "number_71.png");
            this.imageCollection1.Images.SetKeyName(174, "number_72.png");
            this.imageCollection1.Images.SetKeyName(175, "number_73.png");
            this.imageCollection1.Images.SetKeyName(176, "number_74.png");
            this.imageCollection1.Images.SetKeyName(177, "number_75.png");
            this.imageCollection1.Images.SetKeyName(178, "number_76.png");
            this.imageCollection1.Images.SetKeyName(179, "number_77.png");
            this.imageCollection1.Images.SetKeyName(180, "number_78.png");
            this.imageCollection1.Images.SetKeyName(181, "number_79.png");
            this.imageCollection1.Images.SetKeyName(182, "number_80.png");
            this.imageCollection1.Images.SetKeyName(183, "number_81.png");
            this.imageCollection1.Images.SetKeyName(184, "number_82.png");
            this.imageCollection1.Images.SetKeyName(185, "number_83.png");
            this.imageCollection1.Images.SetKeyName(186, "number_84.png");
            this.imageCollection1.Images.SetKeyName(187, "number_85.png");
            this.imageCollection1.Images.SetKeyName(188, "number_86.png");
            this.imageCollection1.Images.SetKeyName(189, "number_87.png");
            this.imageCollection1.Images.SetKeyName(190, "number_88.png");
            this.imageCollection1.Images.SetKeyName(191, "number_89.png");
            this.imageCollection1.Images.SetKeyName(192, "number_90.png");
            this.imageCollection1.Images.SetKeyName(193, "number_91.png");
            this.imageCollection1.Images.SetKeyName(194, "number_92.png");
            this.imageCollection1.Images.SetKeyName(195, "number_93.png");
            this.imageCollection1.Images.SetKeyName(196, "number_94.png");
            this.imageCollection1.Images.SetKeyName(197, "number_95.png");
            this.imageCollection1.Images.SetKeyName(198, "number_96.png");
            this.imageCollection1.Images.SetKeyName(199, "number_97.png");
            this.imageCollection1.Images.SetKeyName(200, "number_98.png");
            this.imageCollection1.Images.SetKeyName(201, "number_99.png");
            this.imageCollection1.Images.SetKeyName(202, "number_100.png");
            this.imageCollection1.Images.SetKeyName(203, "number_0.png");
            this.imageCollection1.Images.SetKeyName(204, "number_1.png");
            this.imageCollection1.Images.SetKeyName(205, "number_2.png");
            this.imageCollection1.Images.SetKeyName(206, "number_3.png");
            this.imageCollection1.Images.SetKeyName(207, "number_4.png");
            this.imageCollection1.Images.SetKeyName(208, "number_5.png");
            this.imageCollection1.Images.SetKeyName(209, "number_6.png");
            this.imageCollection1.Images.SetKeyName(210, "number_7.png");
            this.imageCollection1.Images.SetKeyName(211, "number_8.png");
            this.imageCollection1.Images.SetKeyName(212, "number_9.png");
            this.imageCollection1.Images.SetKeyName(213, "number_10.png");
            this.imageCollection1.Images.SetKeyName(214, "number_11.png");
            this.imageCollection1.Images.SetKeyName(215, "number_12.png");
            this.imageCollection1.Images.SetKeyName(216, "number_13.png");
            this.imageCollection1.Images.SetKeyName(217, "number_14.png");
            this.imageCollection1.Images.SetKeyName(218, "number_15.png");
            this.imageCollection1.Images.SetKeyName(219, "number_16.png");
            this.imageCollection1.Images.SetKeyName(220, "number_17.png");
            this.imageCollection1.Images.SetKeyName(221, "number_18.png");
            this.imageCollection1.Images.SetKeyName(222, "number_19.png");
            this.imageCollection1.Images.SetKeyName(223, "number_20.png");
            this.imageCollection1.Images.SetKeyName(224, "number_21.png");
            this.imageCollection1.Images.SetKeyName(225, "number_22.png");
            this.imageCollection1.Images.SetKeyName(226, "number_23.png");
            this.imageCollection1.Images.SetKeyName(227, "number_24.png");
            this.imageCollection1.Images.SetKeyName(228, "number_25.png");
            this.imageCollection1.Images.SetKeyName(229, "number_26.png");
            this.imageCollection1.Images.SetKeyName(230, "number_27.png");
            this.imageCollection1.Images.SetKeyName(231, "number_28.png");
            this.imageCollection1.Images.SetKeyName(232, "number_29.png");
            this.imageCollection1.Images.SetKeyName(233, "number_30.png");
            this.imageCollection1.Images.SetKeyName(234, "number_31.png");
            this.imageCollection1.Images.SetKeyName(235, "number_32.png");
            this.imageCollection1.Images.SetKeyName(236, "number_33.png");
            this.imageCollection1.Images.SetKeyName(237, "number_34.png");
            this.imageCollection1.Images.SetKeyName(238, "number_35.png");
            this.imageCollection1.Images.SetKeyName(239, "number_36.png");
            this.imageCollection1.Images.SetKeyName(240, "number_37.png");
            this.imageCollection1.Images.SetKeyName(241, "number_38.png");
            this.imageCollection1.Images.SetKeyName(242, "number_39.png");
            this.imageCollection1.Images.SetKeyName(243, "number_40.png");
            this.imageCollection1.Images.SetKeyName(244, "number_41.png");
            this.imageCollection1.Images.SetKeyName(245, "number_42.png");
            this.imageCollection1.Images.SetKeyName(246, "number_43.png");
            this.imageCollection1.Images.SetKeyName(247, "number_44.png");
            this.imageCollection1.Images.SetKeyName(248, "number_45.png");
            this.imageCollection1.Images.SetKeyName(249, "number_46.png");
            this.imageCollection1.Images.SetKeyName(250, "number_47.png");
            this.imageCollection1.Images.SetKeyName(251, "number_48.png");
            this.imageCollection1.Images.SetKeyName(252, "number_49.png");
            this.imageCollection1.Images.SetKeyName(253, "number_50.png");
            this.imageCollection1.Images.SetKeyName(254, "number_51.png");
            this.imageCollection1.Images.SetKeyName(255, "number_52.png");
            this.imageCollection1.Images.SetKeyName(256, "number_53.png");
            this.imageCollection1.Images.SetKeyName(257, "number_54.png");
            this.imageCollection1.Images.SetKeyName(258, "number_55.png");
            this.imageCollection1.Images.SetKeyName(259, "number_56.png");
            this.imageCollection1.Images.SetKeyName(260, "number_57.png");
            this.imageCollection1.Images.SetKeyName(261, "number_58.png");
            this.imageCollection1.Images.SetKeyName(262, "number_59.png");
            this.imageCollection1.Images.SetKeyName(263, "number_60.png");
            this.imageCollection1.Images.SetKeyName(264, "number_61.png");
            this.imageCollection1.Images.SetKeyName(265, "number_62.png");
            this.imageCollection1.Images.SetKeyName(266, "number_63.png");
            this.imageCollection1.Images.SetKeyName(267, "number_64.png");
            this.imageCollection1.Images.SetKeyName(268, "number_65.png");
            this.imageCollection1.Images.SetKeyName(269, "number_66.png");
            this.imageCollection1.Images.SetKeyName(270, "number_67.png");
            this.imageCollection1.Images.SetKeyName(271, "number_68.png");
            this.imageCollection1.Images.SetKeyName(272, "number_69.png");
            this.imageCollection1.Images.SetKeyName(273, "number_70.png");
            this.imageCollection1.Images.SetKeyName(274, "number_71.png");
            this.imageCollection1.Images.SetKeyName(275, "number_72.png");
            this.imageCollection1.Images.SetKeyName(276, "number_73.png");
            this.imageCollection1.Images.SetKeyName(277, "number_74.png");
            this.imageCollection1.Images.SetKeyName(278, "number_75.png");
            this.imageCollection1.Images.SetKeyName(279, "number_76.png");
            this.imageCollection1.Images.SetKeyName(280, "number_77.png");
            this.imageCollection1.Images.SetKeyName(281, "number_78.png");
            this.imageCollection1.Images.SetKeyName(282, "number_79.png");
            this.imageCollection1.Images.SetKeyName(283, "number_80.png");
            this.imageCollection1.Images.SetKeyName(284, "number_81.png");
            this.imageCollection1.Images.SetKeyName(285, "number_82.png");
            this.imageCollection1.Images.SetKeyName(286, "number_83.png");
            this.imageCollection1.Images.SetKeyName(287, "number_84.png");
            this.imageCollection1.Images.SetKeyName(288, "number_85.png");
            this.imageCollection1.Images.SetKeyName(289, "number_86.png");
            this.imageCollection1.Images.SetKeyName(290, "number_87.png");
            this.imageCollection1.Images.SetKeyName(291, "number_88.png");
            this.imageCollection1.Images.SetKeyName(292, "number_89.png");
            this.imageCollection1.Images.SetKeyName(293, "number_90.png");
            this.imageCollection1.Images.SetKeyName(294, "number_91.png");
            this.imageCollection1.Images.SetKeyName(295, "number_92.png");
            this.imageCollection1.Images.SetKeyName(296, "number_93.png");
            this.imageCollection1.Images.SetKeyName(297, "number_94.png");
            this.imageCollection1.Images.SetKeyName(298, "number_95.png");
            this.imageCollection1.Images.SetKeyName(299, "number_96.png");
            this.imageCollection1.Images.SetKeyName(300, "number_97.png");
            this.imageCollection1.Images.SetKeyName(301, "number_98.png");
            this.imageCollection1.Images.SetKeyName(302, "number_99.png");
            this.imageCollection1.Images.SetKeyName(303, "number_100.png");
            this.imageCollection1.Images.SetKeyName(304, "number_0.png");
            this.imageCollection1.Images.SetKeyName(305, "number_1.png");
            this.imageCollection1.Images.SetKeyName(306, "number_2.png");
            this.imageCollection1.Images.SetKeyName(307, "number_3.png");
            this.imageCollection1.Images.SetKeyName(308, "number_4.png");
            this.imageCollection1.Images.SetKeyName(309, "number_5.png");
            this.imageCollection1.Images.SetKeyName(310, "number_6.png");
            this.imageCollection1.Images.SetKeyName(311, "number_7.png");
            this.imageCollection1.Images.SetKeyName(312, "number_8.png");
            this.imageCollection1.Images.SetKeyName(313, "number_9.png");
            this.imageCollection1.Images.SetKeyName(314, "number_10.png");
            this.imageCollection1.Images.SetKeyName(315, "number_11.png");
            this.imageCollection1.Images.SetKeyName(316, "number_12.png");
            this.imageCollection1.Images.SetKeyName(317, "number_13.png");
            this.imageCollection1.Images.SetKeyName(318, "number_14.png");
            this.imageCollection1.Images.SetKeyName(319, "number_15.png");
            this.imageCollection1.Images.SetKeyName(320, "number_16.png");
            this.imageCollection1.Images.SetKeyName(321, "number_17.png");
            this.imageCollection1.Images.SetKeyName(322, "number_18.png");
            this.imageCollection1.Images.SetKeyName(323, "number_19.png");
            this.imageCollection1.Images.SetKeyName(324, "number_20.png");
            this.imageCollection1.Images.SetKeyName(325, "number_21.png");
            this.imageCollection1.Images.SetKeyName(326, "number_22.png");
            this.imageCollection1.Images.SetKeyName(327, "number_23.png");
            this.imageCollection1.Images.SetKeyName(328, "number_24.png");
            this.imageCollection1.Images.SetKeyName(329, "number_25.png");
            this.imageCollection1.Images.SetKeyName(330, "number_26.png");
            this.imageCollection1.Images.SetKeyName(331, "number_27.png");
            this.imageCollection1.Images.SetKeyName(332, "number_28.png");
            this.imageCollection1.Images.SetKeyName(333, "number_29.png");
            this.imageCollection1.Images.SetKeyName(334, "number_30.png");
            this.imageCollection1.Images.SetKeyName(335, "number_31.png");
            this.imageCollection1.Images.SetKeyName(336, "number_32.png");
            this.imageCollection1.Images.SetKeyName(337, "number_33.png");
            this.imageCollection1.Images.SetKeyName(338, "number_34.png");
            this.imageCollection1.Images.SetKeyName(339, "number_35.png");
            this.imageCollection1.Images.SetKeyName(340, "number_36.png");
            this.imageCollection1.Images.SetKeyName(341, "number_37.png");
            this.imageCollection1.Images.SetKeyName(342, "number_38.png");
            this.imageCollection1.Images.SetKeyName(343, "number_39.png");
            this.imageCollection1.Images.SetKeyName(344, "number_40.png");
            this.imageCollection1.Images.SetKeyName(345, "number_41.png");
            this.imageCollection1.Images.SetKeyName(346, "number_42.png");
            this.imageCollection1.Images.SetKeyName(347, "number_43.png");
            this.imageCollection1.Images.SetKeyName(348, "number_44.png");
            this.imageCollection1.Images.SetKeyName(349, "number_45.png");
            this.imageCollection1.Images.SetKeyName(350, "number_46.png");
            this.imageCollection1.Images.SetKeyName(351, "number_47.png");
            this.imageCollection1.Images.SetKeyName(352, "number_48.png");
            this.imageCollection1.Images.SetKeyName(353, "number_49.png");
            this.imageCollection1.Images.SetKeyName(354, "number_50.png");
            this.imageCollection1.Images.SetKeyName(355, "number_51.png");
            this.imageCollection1.Images.SetKeyName(356, "number_52.png");
            this.imageCollection1.Images.SetKeyName(357, "number_53.png");
            this.imageCollection1.Images.SetKeyName(358, "number_54.png");
            this.imageCollection1.Images.SetKeyName(359, "number_55.png");
            this.imageCollection1.Images.SetKeyName(360, "number_56.png");
            this.imageCollection1.Images.SetKeyName(361, "number_57.png");
            this.imageCollection1.Images.SetKeyName(362, "number_58.png");
            this.imageCollection1.Images.SetKeyName(363, "number_59.png");
            this.imageCollection1.Images.SetKeyName(364, "number_60.png");
            this.imageCollection1.Images.SetKeyName(365, "number_61.png");
            this.imageCollection1.Images.SetKeyName(366, "number_62.png");
            this.imageCollection1.Images.SetKeyName(367, "number_63.png");
            this.imageCollection1.Images.SetKeyName(368, "number_64.png");
            this.imageCollection1.Images.SetKeyName(369, "number_65.png");
            this.imageCollection1.Images.SetKeyName(370, "number_66.png");
            this.imageCollection1.Images.SetKeyName(371, "number_67.png");
            this.imageCollection1.Images.SetKeyName(372, "number_68.png");
            this.imageCollection1.Images.SetKeyName(373, "number_69.png");
            this.imageCollection1.Images.SetKeyName(374, "number_70.png");
            this.imageCollection1.Images.SetKeyName(375, "number_71.png");
            this.imageCollection1.Images.SetKeyName(376, "number_72.png");
            this.imageCollection1.Images.SetKeyName(377, "number_73.png");
            this.imageCollection1.Images.SetKeyName(378, "number_74.png");
            this.imageCollection1.Images.SetKeyName(379, "number_75.png");
            this.imageCollection1.Images.SetKeyName(380, "number_76.png");
            this.imageCollection1.Images.SetKeyName(381, "number_77.png");
            this.imageCollection1.Images.SetKeyName(382, "number_78.png");
            this.imageCollection1.Images.SetKeyName(383, "number_79.png");
            this.imageCollection1.Images.SetKeyName(384, "number_80.png");
            this.imageCollection1.Images.SetKeyName(385, "number_81.png");
            this.imageCollection1.Images.SetKeyName(386, "number_82.png");
            this.imageCollection1.Images.SetKeyName(387, "number_83.png");
            this.imageCollection1.Images.SetKeyName(388, "number_84.png");
            this.imageCollection1.Images.SetKeyName(389, "number_85.png");
            this.imageCollection1.Images.SetKeyName(390, "number_86.png");
            this.imageCollection1.Images.SetKeyName(391, "number_87.png");
            this.imageCollection1.Images.SetKeyName(392, "number_88.png");
            this.imageCollection1.Images.SetKeyName(393, "number_89.png");
            this.imageCollection1.Images.SetKeyName(394, "number_90.png");
            this.imageCollection1.Images.SetKeyName(395, "number_91.png");
            this.imageCollection1.Images.SetKeyName(396, "number_92.png");
            this.imageCollection1.Images.SetKeyName(397, "number_93.png");
            this.imageCollection1.Images.SetKeyName(398, "number_94.png");
            this.imageCollection1.Images.SetKeyName(399, "number_95.png");
            this.imageCollection1.Images.SetKeyName(400, "number_96.png");
            this.imageCollection1.Images.SetKeyName(401, "number_97.png");
            this.imageCollection1.Images.SetKeyName(402, "number_98.png");
            this.imageCollection1.Images.SetKeyName(403, "number_99.png");
            this.imageCollection1.Images.SetKeyName(404, "number_100.png");
            this.imageCollection1.Images.SetKeyName(405, "number_0.png");
            this.imageCollection1.Images.SetKeyName(406, "number_1.png");
            this.imageCollection1.Images.SetKeyName(407, "number_2.png");
            this.imageCollection1.Images.SetKeyName(408, "number_3.png");
            this.imageCollection1.Images.SetKeyName(409, "number_4.png");
            this.imageCollection1.Images.SetKeyName(410, "number_5.png");
            this.imageCollection1.Images.SetKeyName(411, "number_6.png");
            this.imageCollection1.Images.SetKeyName(412, "number_7.png");
            this.imageCollection1.Images.SetKeyName(413, "number_8.png");
            this.imageCollection1.Images.SetKeyName(414, "number_9.png");
            this.imageCollection1.Images.SetKeyName(415, "number_10.png");
            this.imageCollection1.Images.SetKeyName(416, "number_11.png");
            this.imageCollection1.Images.SetKeyName(417, "number_12.png");
            this.imageCollection1.Images.SetKeyName(418, "number_13.png");
            this.imageCollection1.Images.SetKeyName(419, "number_14.png");
            this.imageCollection1.Images.SetKeyName(420, "number_15.png");
            this.imageCollection1.Images.SetKeyName(421, "number_16.png");
            this.imageCollection1.Images.SetKeyName(422, "number_17.png");
            this.imageCollection1.Images.SetKeyName(423, "number_18.png");
            this.imageCollection1.Images.SetKeyName(424, "number_19.png");
            this.imageCollection1.Images.SetKeyName(425, "number_20.png");
            this.imageCollection1.Images.SetKeyName(426, "number_21.png");
            this.imageCollection1.Images.SetKeyName(427, "number_22.png");
            this.imageCollection1.Images.SetKeyName(428, "number_23.png");
            this.imageCollection1.Images.SetKeyName(429, "number_24.png");
            this.imageCollection1.Images.SetKeyName(430, "number_25.png");
            this.imageCollection1.Images.SetKeyName(431, "number_26.png");
            this.imageCollection1.Images.SetKeyName(432, "number_27.png");
            this.imageCollection1.Images.SetKeyName(433, "number_28.png");
            this.imageCollection1.Images.SetKeyName(434, "number_29.png");
            this.imageCollection1.Images.SetKeyName(435, "number_30.png");
            this.imageCollection1.Images.SetKeyName(436, "number_31.png");
            this.imageCollection1.Images.SetKeyName(437, "number_32.png");
            this.imageCollection1.Images.SetKeyName(438, "number_33.png");
            this.imageCollection1.Images.SetKeyName(439, "number_34.png");
            this.imageCollection1.Images.SetKeyName(440, "number_35.png");
            this.imageCollection1.Images.SetKeyName(441, "number_36.png");
            this.imageCollection1.Images.SetKeyName(442, "number_37.png");
            this.imageCollection1.Images.SetKeyName(443, "number_38.png");
            this.imageCollection1.Images.SetKeyName(444, "number_39.png");
            this.imageCollection1.Images.SetKeyName(445, "number_40.png");
            this.imageCollection1.Images.SetKeyName(446, "number_41.png");
            this.imageCollection1.Images.SetKeyName(447, "number_42.png");
            this.imageCollection1.Images.SetKeyName(448, "number_43.png");
            this.imageCollection1.Images.SetKeyName(449, "number_44.png");
            this.imageCollection1.Images.SetKeyName(450, "number_45.png");
            this.imageCollection1.Images.SetKeyName(451, "number_46.png");
            this.imageCollection1.Images.SetKeyName(452, "number_47.png");
            this.imageCollection1.Images.SetKeyName(453, "number_48.png");
            this.imageCollection1.Images.SetKeyName(454, "number_49.png");
            this.imageCollection1.Images.SetKeyName(455, "number_50.png");
            this.imageCollection1.Images.SetKeyName(456, "number_51.png");
            this.imageCollection1.Images.SetKeyName(457, "number_52.png");
            this.imageCollection1.Images.SetKeyName(458, "number_53.png");
            this.imageCollection1.Images.SetKeyName(459, "number_54.png");
            this.imageCollection1.Images.SetKeyName(460, "number_55.png");
            this.imageCollection1.Images.SetKeyName(461, "number_56.png");
            this.imageCollection1.Images.SetKeyName(462, "number_57.png");
            this.imageCollection1.Images.SetKeyName(463, "number_58.png");
            this.imageCollection1.Images.SetKeyName(464, "number_59.png");
            this.imageCollection1.Images.SetKeyName(465, "number_60.png");
            this.imageCollection1.Images.SetKeyName(466, "number_61.png");
            this.imageCollection1.Images.SetKeyName(467, "number_62.png");
            this.imageCollection1.Images.SetKeyName(468, "number_63.png");
            this.imageCollection1.Images.SetKeyName(469, "number_64.png");
            this.imageCollection1.Images.SetKeyName(470, "number_65.png");
            this.imageCollection1.Images.SetKeyName(471, "number_66.png");
            this.imageCollection1.Images.SetKeyName(472, "number_67.png");
            this.imageCollection1.Images.SetKeyName(473, "number_68.png");
            this.imageCollection1.Images.SetKeyName(474, "number_69.png");
            this.imageCollection1.Images.SetKeyName(475, "number_70.png");
            this.imageCollection1.Images.SetKeyName(476, "number_71.png");
            this.imageCollection1.Images.SetKeyName(477, "number_72.png");
            this.imageCollection1.Images.SetKeyName(478, "number_73.png");
            this.imageCollection1.Images.SetKeyName(479, "number_74.png");
            this.imageCollection1.Images.SetKeyName(480, "number_75.png");
            this.imageCollection1.Images.SetKeyName(481, "number_76.png");
            this.imageCollection1.Images.SetKeyName(482, "number_77.png");
            this.imageCollection1.Images.SetKeyName(483, "number_78.png");
            this.imageCollection1.Images.SetKeyName(484, "number_79.png");
            this.imageCollection1.Images.SetKeyName(485, "number_80.png");
            this.imageCollection1.Images.SetKeyName(486, "number_81.png");
            this.imageCollection1.Images.SetKeyName(487, "number_82.png");
            this.imageCollection1.Images.SetKeyName(488, "number_83.png");
            this.imageCollection1.Images.SetKeyName(489, "number_84.png");
            this.imageCollection1.Images.SetKeyName(490, "number_85.png");
            this.imageCollection1.Images.SetKeyName(491, "number_86.png");
            this.imageCollection1.Images.SetKeyName(492, "number_87.png");
            this.imageCollection1.Images.SetKeyName(493, "number_88.png");
            this.imageCollection1.Images.SetKeyName(494, "number_89.png");
            this.imageCollection1.Images.SetKeyName(495, "number_90.png");
            this.imageCollection1.Images.SetKeyName(496, "number_91.png");
            this.imageCollection1.Images.SetKeyName(497, "number_92.png");
            this.imageCollection1.Images.SetKeyName(498, "number_93.png");
            this.imageCollection1.Images.SetKeyName(499, "number_94.png");
            this.imageCollection1.Images.SetKeyName(500, "number_95.png");
            this.imageCollection1.Images.SetKeyName(501, "number_96.png");
            this.imageCollection1.Images.SetKeyName(502, "number_97.png");
            this.imageCollection1.Images.SetKeyName(503, "number_98.png");
            this.imageCollection1.Images.SetKeyName(504, "number_99.png");
            this.imageCollection1.Images.SetKeyName(505, "number_100.png");
            this.imageCollection1.Images.SetKeyName(506, "number.png");
            this.imageCollection1.Images.SetKeyName(507, "number.png");
            // 
            // toolTipController1
            // 
            this.toolTipController1.AllowHtmlText = true;
            // 
            // barEditItemMapType
            // 
            this.barEditItemMapType.Caption = "Map Type";
            this.barEditItemMapType.Edit = this.repositoryItemPopupContainerEditMapType;
            this.barEditItemMapType.EditValue = "Bing Hybrid";
            this.barEditItemMapType.EditWidth = 105;
            this.barEditItemMapType.Id = 33;
            this.barEditItemMapType.Name = "barEditItemMapType";
            // 
            // repositoryItemPopupContainerEditMapType
            // 
            this.repositoryItemPopupContainerEditMapType.AutoHeight = false;
            this.repositoryItemPopupContainerEditMapType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditMapType.Name = "repositoryItemPopupContainerEditMapType";
            this.repositoryItemPopupContainerEditMapType.PopupControl = this.popupContainerControlMapType;
            this.repositoryItemPopupContainerEditMapType.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditMapType_QueryResultValue);
            // 
            // repositoryItemRadioGroup1
            // 
            this.repositoryItemRadioGroup1.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Bing Hybid"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Bing Satellite"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Bing Road"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(3, "Open Street Map")});
            this.repositoryItemRadioGroup1.Name = "repositoryItemRadioGroup1";
            // 
            // dockManager1
            // 
            this.dockManager1.DockingOptions.HideImmediatelyOnAutoHide = true;
            this.dockManager1.DockModeVS2005FadeFramesCount = 1;
            this.dockManager1.DockModeVS2005FadeSpeed = 1;
            this.dockManager1.Form = this;
            this.dockManager1.HiddenPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanelLayerManager,
            this.dockPanelLegend});
            this.dockManager1.MenuManager = this.barManager1;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanelData});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // dockPanelLayerManager
            // 
            this.dockPanelLayerManager.Controls.Add(this.controlContainer1);
            this.dockPanelLayerManager.Dock = DevExpress.XtraBars.Docking.DockingStyle.Float;
            this.dockPanelLayerManager.FloatLocation = new System.Drawing.Point(827, 416);
            this.dockPanelLayerManager.FloatSize = new System.Drawing.Size(179, 182);
            this.dockPanelLayerManager.ID = new System.Guid("79293116-8e35-42de-a6e0-b74f5ce123b0");
            this.dockPanelLayerManager.Location = new System.Drawing.Point(-32768, -32768);
            this.dockPanelLayerManager.Name = "dockPanelLayerManager";
            this.dockPanelLayerManager.Options.ShowCloseButton = false;
            this.dockPanelLayerManager.Options.ShowMaximizeButton = false;
            this.dockPanelLayerManager.OriginalSize = new System.Drawing.Size(240, 200);
            this.dockPanelLayerManager.SavedIndex = 1;
            this.dockPanelLayerManager.Size = new System.Drawing.Size(179, 182);
            this.dockPanelLayerManager.Text = "Layer Manager";
            this.dockPanelLayerManager.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            this.dockPanelLayerManager.ClosingPanel += new DevExpress.XtraBars.Docking.DockPanelCancelEventHandler(this.dockPanelLayerManager_ClosingPanel);
            this.dockPanelLayerManager.DockChanged += new System.EventHandler(this.dockPanelLayerManager_DockChanged);
            // 
            // controlContainer1
            // 
            this.controlContainer1.Controls.Add(this.gridControl4);
            this.controlContainer1.Location = new System.Drawing.Point(4, 30);
            this.controlContainer1.Name = "controlContainer1";
            this.controlContainer1.Size = new System.Drawing.Size(171, 148);
            this.controlContainer1.TabIndex = 0;
            // 
            // gridControl4
            // 
            this.gridControl4.DataSource = this.sp06354OMMappingLayerManagerBindingSource;
            this.gridControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.First.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.First.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.ImageList = this.imageCollection16x16;
            this.gridControl4.EmbeddedNavigator.Buttons.Last.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Last.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Next.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Next.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.NextPage.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Prev.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Prev.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.PrevPage.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl4.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "Move Layer Up", "up"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Move Layer Down", "down")});
            this.gridControl4.EmbeddedNavigator.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.None;
            this.gridControl4.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl4_EmbeddedNavigator_ButtonClick);
            this.gridControl4.Location = new System.Drawing.Point(0, 0);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.Size = new System.Drawing.Size(171, 148);
            this.gridControl4.TabIndex = 0;
            this.gridControl4.UseEmbeddedNavigator = true;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp06354OMMappingLayerManagerBindingSource
            // 
            this.sp06354OMMappingLayerManagerBindingSource.DataMember = "sp06354_OM_Mapping_Layer_Manager";
            this.sp06354OMMappingLayerManagerBindingSource.DataSource = this.dataSet_OM_Core;
            // 
            // dataSet_OM_Core
            // 
            this.dataSet_OM_Core.DataSetName = "DataSet_OM_Core";
            this.dataSet_OM_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection16x16
            // 
            this.imageCollection16x16.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection16x16.ImageStream")));
            this.imageCollection16x16.InsertGalleryImage("geopoint_16x16.png", "images/maps/geopoint_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/maps/geopoint_16x16.png"), 0);
            this.imageCollection16x16.Images.SetKeyName(0, "geopoint_16x16.png");
            this.imageCollection16x16.InsertImage(global::WoodPlan5.Properties.Resources.refresh_16x16, "refresh_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection16x16.Images.SetKeyName(1, "refresh_16x16");
            this.imageCollection16x16.InsertGalleryImage("next_16x16.png", "images/navigation/next_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/navigation/next_16x16.png"), 2);
            this.imageCollection16x16.Images.SetKeyName(2, "next_16x16.png");
            this.imageCollection16x16.InsertGalleryImage("previous_16x16.png", "images/navigation/previous_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/navigation/previous_16x16.png"), 3);
            this.imageCollection16x16.Images.SetKeyName(3, "previous_16x16.png");
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLayerName,
            this.colLayerOrder});
            gridFormatRule2.ApplyToRow = true;
            gridFormatRule2.Column = this.colLayerOrder;
            gridFormatRule2.Name = "Format0";
            formatConditionRuleValue2.Appearance.ForeColor = System.Drawing.Color.Gray;
            formatConditionRuleValue2.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue2.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue2.Value1 = 0;
            gridFormatRule2.Rule = formatConditionRuleValue2;
            gridFormatRule3.ApplyToRow = true;
            gridFormatRule3.Column = this.colLayerOrder;
            gridFormatRule3.Name = "Format1";
            formatConditionRuleValue3.Appearance.ForeColor = System.Drawing.Color.Gray;
            formatConditionRuleValue3.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue3.Condition = DevExpress.XtraEditors.FormatCondition.GreaterOrEqual;
            formatConditionRuleValue3.Value1 = 4;
            gridFormatRule3.Rule = formatConditionRuleValue3;
            this.gridView4.FormatRules.Add(gridFormatRule2);
            this.gridView4.FormatRules.Add(gridFormatRule3);
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsCustomization.AllowColumnMoving = false;
            this.gridView4.OptionsCustomization.AllowColumnResizing = false;
            this.gridView4.OptionsCustomization.AllowFilter = false;
            this.gridView4.OptionsCustomization.AllowGroup = false;
            this.gridView4.OptionsCustomization.AllowSort = false;
            this.gridView4.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView4.OptionsFilter.AllowFilterEditor = false;
            this.gridView4.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView4.OptionsFilter.AllowMRUFilterList = false;
            this.gridView4.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView4.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView4.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.gridView4.OptionsFind.AllowFindPanel = false;
            this.gridView4.OptionsMenu.EnableColumnMenu = false;
            this.gridView4.OptionsMenu.EnableFooterMenu = false;
            this.gridView4.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridView4.OptionsMenu.ShowAddNewSummaryItem = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsMenu.ShowAutoFilterRowItem = false;
            this.gridView4.OptionsMenu.ShowDateTimeGroupIntervalItems = false;
            this.gridView4.OptionsMenu.ShowGroupSortSummaryItems = false;
            this.gridView4.OptionsMenu.ShowSplitItem = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLayerOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView4.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView4.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.GridView_FocusedRowChanged_NoGroupSelection);
            this.gridView4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_NoGroupSelection);
            // 
            // colLayerName
            // 
            this.colLayerName.Caption = "Layer Name";
            this.colLayerName.FieldName = "LayerName";
            this.colLayerName.Name = "colLayerName";
            this.colLayerName.OptionsColumn.AllowEdit = false;
            this.colLayerName.OptionsColumn.AllowFocus = false;
            this.colLayerName.OptionsColumn.ReadOnly = true;
            this.colLayerName.Visible = true;
            this.colLayerName.VisibleIndex = 0;
            // 
            // dockPanelLegend
            // 
            this.dockPanelLegend.Controls.Add(this.dockPanel2_Container);
            this.dockPanelLegend.Dock = DevExpress.XtraBars.Docking.DockingStyle.Float;
            this.dockPanelLegend.FloatLocation = new System.Drawing.Point(1073, 364);
            this.dockPanelLegend.FloatSize = new System.Drawing.Size(107, 424);
            this.dockPanelLegend.ID = new System.Guid("0d0cde50-e32f-4cbd-8782-11b54073044a");
            this.dockPanelLegend.Location = new System.Drawing.Point(-32768, -32768);
            this.dockPanelLegend.Name = "dockPanelLegend";
            this.dockPanelLegend.Options.ShowCloseButton = false;
            this.dockPanelLegend.Options.ShowMaximizeButton = false;
            this.dockPanelLegend.OriginalSize = new System.Drawing.Size(109, 300);
            this.dockPanelLegend.SavedIndex = 0;
            this.dockPanelLegend.Size = new System.Drawing.Size(107, 424);
            this.dockPanelLegend.Text = "Legend";
            this.dockPanelLegend.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            this.dockPanelLegend.ClosingPanel += new DevExpress.XtraBars.Docking.DockPanelCancelEventHandler(this.dockPanelLegend_ClosingPanel);
            this.dockPanelLegend.DockChanged += new System.EventHandler(this.dockPanelLegend_DockChanged);
            // 
            // dockPanel2_Container
            // 
            this.dockPanel2_Container.Controls.Add(this.groupControl3);
            this.dockPanel2_Container.Controls.Add(this.groupControl2);
            this.dockPanel2_Container.Controls.Add(this.groupControl1);
            this.dockPanel2_Container.Location = new System.Drawing.Point(4, 30);
            this.dockPanel2_Container.Name = "dockPanel2_Container";
            this.dockPanel2_Container.Size = new System.Drawing.Size(99, 390);
            this.dockPanel2_Container.TabIndex = 0;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.labelControl2);
            this.groupControl3.Controls.Add(this.pictureEdit3);
            this.groupControl3.Controls.Add(this.pictureEdit5);
            this.groupControl3.Controls.Add(this.labelControl8);
            this.groupControl3.Location = new System.Drawing.Point(0, 290);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(101, 103);
            this.groupControl3.TabIndex = 17;
            this.groupControl3.Text = "Job";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(6, 35);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(40, 13);
            this.labelControl2.TabIndex = 6;
            this.labelControl2.Text = "Started:";
            // 
            // pictureEdit3
            // 
            this.pictureEdit3.EditValue = ((object)(resources.GetObject("pictureEdit3.EditValue")));
            this.pictureEdit3.Location = new System.Drawing.Point(66, 24);
            this.pictureEdit3.MenuManager = this.barManager1;
            this.pictureEdit3.Name = "pictureEdit3";
            this.pictureEdit3.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit3.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit3.Size = new System.Drawing.Size(32, 37);
            this.pictureEdit3.TabIndex = 12;
            // 
            // pictureEdit5
            // 
            this.pictureEdit5.EditValue = ((object)(resources.GetObject("pictureEdit5.EditValue")));
            this.pictureEdit5.Location = new System.Drawing.Point(66, 63);
            this.pictureEdit5.MenuManager = this.barManager1;
            this.pictureEdit5.Name = "pictureEdit5";
            this.pictureEdit5.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit5.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit5.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit5.Size = new System.Drawing.Size(32, 37);
            this.pictureEdit5.TabIndex = 13;
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(6, 74);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(51, 13);
            this.labelControl8.TabIndex = 2;
            this.labelControl8.Text = "Completed";
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.labelControl9);
            this.groupControl2.Controls.Add(this.pictureEdit8);
            this.groupControl2.Controls.Add(this.labelControl3);
            this.groupControl2.Controls.Add(this.pictureEdit2);
            this.groupControl2.Controls.Add(this.labelControl5);
            this.groupControl2.Controls.Add(this.pictureEdit4);
            this.groupControl2.Controls.Add(this.labelControl6);
            this.groupControl2.Controls.Add(this.pictureEdit6);
            this.groupControl2.Controls.Add(this.pictureEdit7);
            this.groupControl2.Controls.Add(this.labelControl7);
            this.groupControl2.Location = new System.Drawing.Point(0, 67);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(101, 220);
            this.groupControl2.TabIndex = 16;
            this.groupControl2.Text = "Visit";
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(6, 105);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(47, 26);
            this.labelControl9.TabIndex = 18;
            this.labelControl9.Text = "Started\r\n[No GPS]:";
            // 
            // pictureEdit8
            // 
            this.pictureEdit8.EditValue = ((object)(resources.GetObject("pictureEdit8.EditValue")));
            this.pictureEdit8.Location = new System.Drawing.Point(66, 102);
            this.pictureEdit8.MenuManager = this.barManager1;
            this.pictureEdit8.Name = "pictureEdit8";
            this.pictureEdit8.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit8.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit8.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit8.Size = new System.Drawing.Size(32, 37);
            this.pictureEdit8.TabIndex = 17;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(4, 183);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(51, 26);
            this.labelControl3.TabIndex = 16;
            this.labelControl3.Text = "Completed\r\n[No GPS]:";
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.EditValue = ((object)(resources.GetObject("pictureEdit2.EditValue")));
            this.pictureEdit2.Location = new System.Drawing.Point(66, 180);
            this.pictureEdit2.MenuManager = this.barManager1;
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit2.Size = new System.Drawing.Size(32, 37);
            toolTipTitleItem10.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem10.Text = "Completed [No GPS] - Information";
            toolTipItem10.LeftIndent = 6;
            toolTipItem10.Text = "When no GPS coordinates have been record for a visit end location, the parent Sit" +
    "e coordinates are used instead.";
            superToolTip10.Items.Add(toolTipTitleItem10);
            superToolTip10.Items.Add(toolTipItem10);
            this.pictureEdit2.SuperTip = superToolTip10;
            this.pictureEdit2.TabIndex = 15;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(6, 35);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(60, 13);
            this.labelControl5.TabIndex = 6;
            this.labelControl5.Text = "Not Started:";
            // 
            // pictureEdit4
            // 
            this.pictureEdit4.EditValue = ((object)(resources.GetObject("pictureEdit4.EditValue")));
            this.pictureEdit4.Location = new System.Drawing.Point(66, 141);
            this.pictureEdit4.MenuManager = this.barManager1;
            this.pictureEdit4.Name = "pictureEdit4";
            this.pictureEdit4.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit4.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit4.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit4.Size = new System.Drawing.Size(32, 37);
            this.pictureEdit4.TabIndex = 14;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(4, 152);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(55, 13);
            this.labelControl6.TabIndex = 4;
            this.labelControl6.Text = "Completed:";
            // 
            // pictureEdit6
            // 
            this.pictureEdit6.EditValue = ((object)(resources.GetObject("pictureEdit6.EditValue")));
            this.pictureEdit6.Location = new System.Drawing.Point(66, 24);
            this.pictureEdit6.MenuManager = this.barManager1;
            this.pictureEdit6.Name = "pictureEdit6";
            this.pictureEdit6.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit6.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit6.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit6.Size = new System.Drawing.Size(32, 37);
            toolTipTitleItem11.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem11.Appearance.Options.UseImage = true;
            toolTipTitleItem11.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem11.Text = "Visits Not Started - Information";
            toolTipItem11.LeftIndent = 6;
            toolTipItem11.Text = "This icon is only used for visits which have not been started. \r\n\r\nThe positionin" +
    "g on the map is taken from the parent Site\'s Latitude and Longitude.";
            superToolTip11.Items.Add(toolTipTitleItem11);
            superToolTip11.Items.Add(toolTipItem11);
            this.pictureEdit6.SuperTip = superToolTip11;
            this.pictureEdit6.TabIndex = 12;
            // 
            // pictureEdit7
            // 
            this.pictureEdit7.EditValue = ((object)(resources.GetObject("pictureEdit7.EditValue")));
            this.pictureEdit7.Location = new System.Drawing.Point(66, 63);
            this.pictureEdit7.MenuManager = this.barManager1;
            this.pictureEdit7.Name = "pictureEdit7";
            this.pictureEdit7.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit7.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit7.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit7.Size = new System.Drawing.Size(32, 37);
            this.pictureEdit7.TabIndex = 13;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(6, 74);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(40, 13);
            this.labelControl7.TabIndex = 2;
            this.labelControl7.Text = "Started:";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.pictureEdit1);
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(101, 64);
            this.groupControl1.TabIndex = 15;
            this.groupControl1.Text = "Site";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(6, 35);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(22, 13);
            this.labelControl4.TabIndex = 6;
            this.labelControl4.Text = "Site:";
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.EditValue = ((object)(resources.GetObject("pictureEdit1.EditValue")));
            this.pictureEdit1.Location = new System.Drawing.Point(66, 24);
            this.pictureEdit1.MenuManager = this.barManager1;
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Size = new System.Drawing.Size(32, 37);
            this.pictureEdit1.TabIndex = 12;
            // 
            // dockPanelData
            // 
            this.dockPanelData.Controls.Add(this.dockPanel1_Container);
            this.dockPanelData.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelData.ID = new System.Guid("23bf8cf4-eb0d-4e72-9053-e9fd8ce4a0d6");
            this.dockPanelData.Location = new System.Drawing.Point(0, 0);
            this.dockPanelData.Name = "dockPanelData";
            this.dockPanelData.Options.ShowCloseButton = false;
            this.dockPanelData.OriginalSize = new System.Drawing.Size(379, 200);
            this.dockPanelData.Size = new System.Drawing.Size(379, 695);
            this.dockPanelData.Text = "Data";
            this.dockPanelData.DockChanged += new System.EventHandler(this.dockPanelData_DockChanged);
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.splitContainerControl1);
            this.dockPanel1_Container.Location = new System.Drawing.Point(3, 29);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(372, 663);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.btnClearSitesFromMap);
            this.splitContainerControl1.Panel1.Controls.Add(this.buttonEditChooseSites);
            this.splitContainerControl1.Panel1.Controls.Add(this.labelControl1);
            this.splitContainerControl1.Panel1.Controls.Add(this.btnLoadSitesIntoMap);
            this.splitContainerControl1.Panel1.Controls.Add(this.btnLoadVisits);
            this.splitContainerControl1.Panel1.Controls.Add(this.gridControl1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(372, 663);
            this.splitContainerControl1.SplitterPosition = 334;
            this.splitContainerControl1.TabIndex = 2;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // btnClearSitesFromMap
            // 
            this.btnClearSitesFromMap.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClearSitesFromMap.Location = new System.Drawing.Point(140, 311);
            this.btnClearSitesFromMap.Name = "btnClearSitesFromMap";
            this.btnClearSitesFromMap.Size = new System.Drawing.Size(113, 23);
            this.btnClearSitesFromMap.TabIndex = 7;
            this.btnClearSitesFromMap.Text = "Clear Sites from Map";
            this.btnClearSitesFromMap.Click += new System.EventHandler(this.btnClearSitesFromMap_Click);
            // 
            // buttonEditChooseSites
            // 
            this.buttonEditChooseSites.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonEditChooseSites.Location = new System.Drawing.Point(71, 0);
            this.buttonEditChooseSites.MenuManager = this.barManager1;
            this.buttonEditChooseSites.Name = "buttonEditChooseSites";
            this.buttonEditChooseSites.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.buttonEditChooseSites.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.buttonEditChooseSites.Size = new System.Drawing.Size(301, 20);
            this.buttonEditChooseSites.TabIndex = 6;
            this.buttonEditChooseSites.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEditChooseSites_ButtonClick);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(2, 3);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(66, 13);
            this.labelControl1.TabIndex = 5;
            this.labelControl1.Text = "Choose Sites:";
            // 
            // btnLoadSitesIntoMap
            // 
            this.btnLoadSitesIntoMap.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.False;
            this.btnLoadSitesIntoMap.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLoadSitesIntoMap.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnLoadSitesIntoMap.Appearance.Options.UseFont = true;
            this.btnLoadSitesIntoMap.ImageOptions.ImageIndex = 0;
            this.btnLoadSitesIntoMap.ImageOptions.ImageList = this.imageCollection16x16;
            this.btnLoadSitesIntoMap.Location = new System.Drawing.Point(0, 311);
            this.btnLoadSitesIntoMap.Name = "btnLoadSitesIntoMap";
            this.btnLoadSitesIntoMap.Size = new System.Drawing.Size(141, 23);
            toolTipTitleItem5.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Text = "Load Sites into Map - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to load the selected sites (ticked) into the map.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.btnLoadSitesIntoMap.SuperTip = superToolTip5;
            this.btnLoadSitesIntoMap.TabIndex = 4;
            this.btnLoadSitesIntoMap.Text = "Load Sites into Map";
            this.btnLoadSitesIntoMap.Click += new System.EventHandler(this.btnLoadSitesIntoMap_Click);
            // 
            // btnLoadVisits
            // 
            this.btnLoadVisits.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLoadVisits.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnLoadVisits.Appearance.Options.UseFont = true;
            this.btnLoadVisits.ImageOptions.ImageIndex = 1;
            this.btnLoadVisits.ImageOptions.ImageList = this.imageCollection16x16;
            this.btnLoadVisits.Location = new System.Drawing.Point(255, 311);
            this.btnLoadVisits.Name = "btnLoadVisits";
            this.btnLoadVisits.Size = new System.Drawing.Size(92, 23);
            toolTipTitleItem6.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Appearance.Options.UseImage = true;
            toolTipTitleItem6.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Text = "Load Visits - Information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Click me to Load all linked visits from the selected (ticked) sites into the Visi" +
    "t Grid.";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this.btnLoadVisits.SuperTip = superToolTip6;
            this.btnLoadVisits.TabIndex = 3;
            this.btnLoadVisits.Text = "Load Visits";
            this.btnLoadVisits.Click += new System.EventHandler(this.btnLoadVisits_Click);
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.btnLoadJobs);
            this.splitContainerControl2.Panel1.Controls.Add(this.gridControl2);
            this.splitContainerControl2.Panel1.Controls.Add(this.btnClearVisitsFromMap);
            this.splitContainerControl2.Panel1.Controls.Add(this.btnLoadVisitsIntoMap);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.gridControl3);
            this.splitContainerControl2.Panel2.Controls.Add(this.btnClearJobsFromMap);
            this.splitContainerControl2.Panel2.Controls.Add(this.btnLoadJobsIntoMap);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(372, 323);
            this.splitContainerControl2.SplitterPosition = 138;
            this.splitContainerControl2.TabIndex = 4;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // btnLoadJobs
            // 
            this.btnLoadJobs.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLoadJobs.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnLoadJobs.Appearance.Options.UseFont = true;
            this.btnLoadJobs.ImageOptions.ImageIndex = 1;
            this.btnLoadJobs.ImageOptions.ImageList = this.imageCollection16x16;
            this.btnLoadJobs.Location = new System.Drawing.Point(258, 115);
            this.btnLoadJobs.Name = "btnLoadJobs";
            this.btnLoadJobs.Size = new System.Drawing.Size(88, 23);
            toolTipTitleItem7.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Appearance.Options.UseImage = true;
            toolTipTitleItem7.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Text = "Load Jobs - Information";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Click me to Load all linked Jobs from the selected (ticked) visits into the Job G" +
    "rid.";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            this.btnLoadJobs.SuperTip = superToolTip7;
            this.btnLoadJobs.TabIndex = 4;
            this.btnLoadJobs.Text = "Load Jobs";
            this.btnLoadJobs.Click += new System.EventHandler(this.btnLoadJobs_Click);
            // 
            // gridControl2
            // 
            this.gridControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl2.DataSource = this.sp06152OMMappingVisitsBindingSource;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit2,
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit2});
            this.gridControl2.Size = new System.Drawing.Size(372, 114);
            this.gridControl2.TabIndex = 2;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27,
            this.gridColumn28,
            this.gridColumn29,
            this.gridColumn30,
            this.gridColumn31,
            this.gridColumn32,
            this.gridColumn33,
            this.gridColumn34,
            this.gridColumn35,
            this.gridColumn36,
            this.gridColumn37,
            this.gridColumn38,
            this.gridColumn39,
            this.gridColumn40,
            this.gridColumn41,
            this.gridColumn42,
            this.gridColumn43,
            this.gridColumn44,
            this.gridColumn45,
            this.gridColumn46,
            this.gridColumn47,
            this.gridColumn48,
            this.gridColumn49,
            this.gridColumn50,
            this.gridColumn51,
            this.colSellCalculationLevel,
            this.colSellCalculationLevelDescription});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupCount = 3;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsFind.AlwaysVisible = true;
            this.gridView2.OptionsFind.FindDelay = 2000;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsLayout.StoreFormatRules = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn35, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn43, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn41, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView2_MouseUp);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Visit ID";
            this.gridColumn1.FieldName = "VisitID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Site Contract ID";
            this.gridColumn2.FieldName = "SiteContractID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Width = 98;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Visit #";
            this.gridColumn3.FieldName = "VisitNumber";
            this.gridColumn3.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 0;
            this.gridColumn3.Width = 74;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Created By Staff ID";
            this.gridColumn4.FieldName = "CreatedByStaffID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Width = 116;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Expected Start";
            this.gridColumn5.ColumnEdit = this.repositoryItemTextEdit1;
            this.gridColumn5.FieldName = "ExpectedStartDate";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 1;
            this.gridColumn5.Width = 100;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "g";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Expected End";
            this.gridColumn6.ColumnEdit = this.repositoryItemTextEdit1;
            this.gridColumn6.FieldName = "ExpectedEndDate";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 2;
            this.gridColumn6.Width = 100;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Start";
            this.gridColumn7.ColumnEdit = this.repositoryItemTextEdit1;
            this.gridColumn7.FieldName = "StartDate";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 3;
            this.gridColumn7.Width = 100;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "End";
            this.gridColumn8.ColumnEdit = this.repositoryItemTextEdit1;
            this.gridColumn8.FieldName = "EndDate";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 4;
            this.gridColumn8.Width = 100;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Cost Calculation ID";
            this.gridColumn9.FieldName = "CostCalculationLevel";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Width = 140;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Visit Cost";
            this.gridColumn10.ColumnEdit = this.repositoryItemTextEdit2;
            this.gridColumn10.FieldName = "VisitCost";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VisitCost", "{0:c}")});
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Mask.EditMask = "c";
            this.repositoryItemTextEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Visit Sell";
            this.gridColumn11.ColumnEdit = this.repositoryItemTextEdit2;
            this.gridColumn11.FieldName = "VisitSell";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VisitSell", "{0:c}")});
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Remarks";
            this.gridColumn12.FieldName = "Remarks";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 12;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Start Latitude";
            this.gridColumn13.FieldName = "StartLatitude";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 5;
            this.gridColumn13.Width = 87;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Start Longitude";
            this.gridColumn14.FieldName = "StartLongitude";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 6;
            this.gridColumn14.Width = 95;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Finish Latitude";
            this.gridColumn15.FieldName = "FinishLatitude";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 7;
            this.gridColumn15.Width = 90;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Finish Longitude";
            this.gridColumn16.FieldName = "FinishLongitude";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowFocus = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 8;
            this.gridColumn16.Width = 98;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Cost Calculation";
            this.gridColumn17.FieldName = "CostCalculationLevelDescription";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.AllowFocus = false;
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.Width = 100;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Client Contract ID";
            this.gridColumn18.FieldName = "ClientContractID";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.AllowFocus = false;
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            this.gridColumn18.Width = 107;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Site ID";
            this.gridColumn19.FieldName = "SiteID";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.OptionsColumn.AllowFocus = false;
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Site Contract Start";
            this.gridColumn20.FieldName = "SiteContractStartDate";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.AllowEdit = false;
            this.gridColumn20.OptionsColumn.AllowFocus = false;
            this.gridColumn20.OptionsColumn.ReadOnly = true;
            this.gridColumn20.Width = 111;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Site Contract End";
            this.gridColumn21.FieldName = "SiteContractEndDate";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.AllowEdit = false;
            this.gridColumn21.OptionsColumn.AllowFocus = false;
            this.gridColumn21.OptionsColumn.ReadOnly = true;
            this.gridColumn21.Width = 105;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Site Contract Active";
            this.gridColumn22.ColumnEdit = this.repositoryItemCheckEdit2;
            this.gridColumn22.FieldName = "SiteContractActive";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.AllowEdit = false;
            this.gridColumn22.OptionsColumn.AllowFocus = false;
            this.gridColumn22.OptionsColumn.ReadOnly = true;
            this.gridColumn22.Width = 117;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Site Contract Value";
            this.gridColumn23.ColumnEdit = this.repositoryItemTextEdit2;
            this.gridColumn23.FieldName = "SiteContractValue";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.AllowEdit = false;
            this.gridColumn23.OptionsColumn.AllowFocus = false;
            this.gridColumn23.OptionsColumn.ReadOnly = true;
            this.gridColumn23.Width = 113;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Client ID";
            this.gridColumn24.FieldName = "ClientID";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.AllowEdit = false;
            this.gridColumn24.OptionsColumn.AllowFocus = false;
            this.gridColumn24.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "GC Compnay ID";
            this.gridColumn25.FieldName = "GCCompanyID";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.OptionsColumn.AllowEdit = false;
            this.gridColumn25.OptionsColumn.AllowFocus = false;
            this.gridColumn25.OptionsColumn.ReadOnly = true;
            this.gridColumn25.Width = 97;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Contract Type ID";
            this.gridColumn26.FieldName = "ContractTypeID";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.OptionsColumn.AllowEdit = false;
            this.gridColumn26.OptionsColumn.AllowFocus = false;
            this.gridColumn26.OptionsColumn.ReadOnly = true;
            this.gridColumn26.Width = 104;
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Contract Status ID";
            this.gridColumn27.FieldName = "ContractStatusID";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.OptionsColumn.AllowEdit = false;
            this.gridColumn27.OptionsColumn.AllowFocus = false;
            this.gridColumn27.OptionsColumn.ReadOnly = true;
            this.gridColumn27.Width = 111;
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "Contract Director ID";
            this.gridColumn28.FieldName = "ContractDirectorID";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.OptionsColumn.AllowEdit = false;
            this.gridColumn28.OptionsColumn.AllowFocus = false;
            this.gridColumn28.OptionsColumn.ReadOnly = true;
            this.gridColumn28.Width = 118;
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "Client Contract Start";
            this.gridColumn29.ColumnEdit = this.repositoryItemTextEdit1;
            this.gridColumn29.FieldName = "ClientContractStartDate";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.OptionsColumn.AllowEdit = false;
            this.gridColumn29.OptionsColumn.AllowFocus = false;
            this.gridColumn29.OptionsColumn.ReadOnly = true;
            this.gridColumn29.Width = 120;
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "Client Contract End";
            this.gridColumn30.ColumnEdit = this.repositoryItemTextEdit1;
            this.gridColumn30.FieldName = "ClientContractEndDate";
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.OptionsColumn.AllowEdit = false;
            this.gridColumn30.OptionsColumn.AllowFocus = false;
            this.gridColumn30.OptionsColumn.ReadOnly = true;
            this.gridColumn30.Width = 114;
            // 
            // gridColumn31
            // 
            this.gridColumn31.Caption = "Client Contract Active";
            this.gridColumn31.ColumnEdit = this.repositoryItemCheckEdit2;
            this.gridColumn31.FieldName = "ClientContractActive";
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.OptionsColumn.AllowEdit = false;
            this.gridColumn31.OptionsColumn.AllowFocus = false;
            this.gridColumn31.OptionsColumn.ReadOnly = true;
            this.gridColumn31.Width = 126;
            // 
            // gridColumn32
            // 
            this.gridColumn32.Caption = "Sector Type ID";
            this.gridColumn32.FieldName = "SectorTypeID";
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.OptionsColumn.AllowEdit = false;
            this.gridColumn32.OptionsColumn.AllowFocus = false;
            this.gridColumn32.OptionsColumn.ReadOnly = true;
            this.gridColumn32.Width = 93;
            // 
            // gridColumn33
            // 
            this.gridColumn33.Caption = "Client Contract Value";
            this.gridColumn33.ColumnEdit = this.repositoryItemTextEdit2;
            this.gridColumn33.FieldName = "ClientContractValue";
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.OptionsColumn.AllowEdit = false;
            this.gridColumn33.OptionsColumn.AllowFocus = false;
            this.gridColumn33.OptionsColumn.ReadOnly = true;
            this.gridColumn33.Width = 122;
            // 
            // gridColumn34
            // 
            this.gridColumn34.Caption = "Finance Client Code";
            this.gridColumn34.FieldName = "FinanceClientCode";
            this.gridColumn34.Name = "gridColumn34";
            this.gridColumn34.OptionsColumn.AllowEdit = false;
            this.gridColumn34.OptionsColumn.AllowFocus = false;
            this.gridColumn34.OptionsColumn.ReadOnly = true;
            this.gridColumn34.Width = 116;
            // 
            // gridColumn35
            // 
            this.gridColumn35.Caption = "Client Name";
            this.gridColumn35.FieldName = "ClientName";
            this.gridColumn35.Name = "gridColumn35";
            this.gridColumn35.OptionsColumn.AllowEdit = false;
            this.gridColumn35.OptionsColumn.AllowFocus = false;
            this.gridColumn35.OptionsColumn.ReadOnly = true;
            this.gridColumn35.Visible = true;
            this.gridColumn35.VisibleIndex = 14;
            this.gridColumn35.Width = 174;
            // 
            // gridColumn36
            // 
            this.gridColumn36.Caption = "GC Company Name";
            this.gridColumn36.FieldName = "GCCompanyName";
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.OptionsColumn.AllowEdit = false;
            this.gridColumn36.OptionsColumn.AllowFocus = false;
            this.gridColumn36.OptionsColumn.ReadOnly = true;
            this.gridColumn36.Width = 113;
            // 
            // gridColumn37
            // 
            this.gridColumn37.Caption = "Contract Type";
            this.gridColumn37.FieldName = "ContractType";
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.OptionsColumn.AllowEdit = false;
            this.gridColumn37.OptionsColumn.AllowFocus = false;
            this.gridColumn37.OptionsColumn.ReadOnly = true;
            this.gridColumn37.Width = 117;
            // 
            // gridColumn38
            // 
            this.gridColumn38.Caption = "Contract Status";
            this.gridColumn38.FieldName = "ContractStatus";
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.OptionsColumn.AllowEdit = false;
            this.gridColumn38.OptionsColumn.AllowFocus = false;
            this.gridColumn38.OptionsColumn.ReadOnly = true;
            this.gridColumn38.Width = 117;
            // 
            // gridColumn39
            // 
            this.gridColumn39.Caption = "Contract Director";
            this.gridColumn39.FieldName = "ContractDirector";
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.OptionsColumn.AllowEdit = false;
            this.gridColumn39.OptionsColumn.AllowFocus = false;
            this.gridColumn39.OptionsColumn.ReadOnly = true;
            this.gridColumn39.Width = 122;
            // 
            // gridColumn40
            // 
            this.gridColumn40.Caption = "Sector Type";
            this.gridColumn40.FieldName = "SectorType";
            this.gridColumn40.Name = "gridColumn40";
            this.gridColumn40.OptionsColumn.AllowEdit = false;
            this.gridColumn40.OptionsColumn.AllowFocus = false;
            this.gridColumn40.OptionsColumn.ReadOnly = true;
            this.gridColumn40.Width = 107;
            // 
            // gridColumn41
            // 
            this.gridColumn41.Caption = "Site Name";
            this.gridColumn41.FieldName = "SiteName";
            this.gridColumn41.Name = "gridColumn41";
            this.gridColumn41.OptionsColumn.AllowEdit = false;
            this.gridColumn41.OptionsColumn.AllowFocus = false;
            this.gridColumn41.OptionsColumn.ReadOnly = true;
            this.gridColumn41.Visible = true;
            this.gridColumn41.VisibleIndex = 13;
            this.gridColumn41.Width = 227;
            // 
            // gridColumn42
            // 
            this.gridColumn42.Caption = "Linked To Site Contract";
            this.gridColumn42.FieldName = "LinkedToParent";
            this.gridColumn42.Name = "gridColumn42";
            this.gridColumn42.OptionsColumn.AllowEdit = false;
            this.gridColumn42.OptionsColumn.AllowFocus = false;
            this.gridColumn42.OptionsColumn.ReadOnly = true;
            this.gridColumn42.Width = 333;
            // 
            // gridColumn43
            // 
            this.gridColumn43.Caption = "Contract Description";
            this.gridColumn43.FieldName = "ContractDescription";
            this.gridColumn43.Name = "gridColumn43";
            this.gridColumn43.OptionsColumn.AllowEdit = false;
            this.gridColumn43.OptionsColumn.AllowFocus = false;
            this.gridColumn43.OptionsColumn.ReadOnly = true;
            this.gridColumn43.Visible = true;
            this.gridColumn43.VisibleIndex = 15;
            this.gridColumn43.Width = 225;
            // 
            // gridColumn44
            // 
            this.gridColumn44.Caption = "Site Postcode";
            this.gridColumn44.FieldName = "SitePostcode";
            this.gridColumn44.Name = "gridColumn44";
            this.gridColumn44.OptionsColumn.AllowEdit = false;
            this.gridColumn44.OptionsColumn.AllowFocus = false;
            this.gridColumn44.OptionsColumn.ReadOnly = true;
            this.gridColumn44.Width = 86;
            // 
            // gridColumn45
            // 
            this.gridColumn45.Caption = "Site Latitude";
            this.gridColumn45.FieldName = "SiteLocationX";
            this.gridColumn45.Name = "gridColumn45";
            this.gridColumn45.OptionsColumn.AllowEdit = false;
            this.gridColumn45.OptionsColumn.AllowFocus = false;
            this.gridColumn45.OptionsColumn.ReadOnly = true;
            this.gridColumn45.Width = 81;
            // 
            // gridColumn46
            // 
            this.gridColumn46.Caption = "Site Longitude";
            this.gridColumn46.FieldName = "SiteLocationY";
            this.gridColumn46.Name = "gridColumn46";
            this.gridColumn46.OptionsColumn.AllowEdit = false;
            this.gridColumn46.OptionsColumn.AllowFocus = false;
            this.gridColumn46.OptionsColumn.ReadOnly = true;
            this.gridColumn46.Width = 89;
            // 
            // gridColumn47
            // 
            this.gridColumn47.Caption = "Linked Documents";
            this.gridColumn47.FieldName = "LinkedDocumentCount";
            this.gridColumn47.Name = "gridColumn47";
            this.gridColumn47.OptionsColumn.ReadOnly = true;
            this.gridColumn47.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "LinkedDocumentCount", "{0:n}")});
            this.gridColumn47.Width = 107;
            // 
            // gridColumn48
            // 
            this.gridColumn48.Caption = "Linked Jobs Total";
            this.gridColumn48.FieldName = "LinkedJobCount";
            this.gridColumn48.Name = "gridColumn48";
            this.gridColumn48.OptionsColumn.ReadOnly = true;
            this.gridColumn48.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "LinkedJobCount", "{0:n}")});
            this.gridColumn48.Visible = true;
            this.gridColumn48.VisibleIndex = 9;
            this.gridColumn48.Width = 103;
            // 
            // gridColumn49
            // 
            this.gridColumn49.Caption = "Linked Jobs Outstanding";
            this.gridColumn49.FieldName = "LinkedOutstandingJobCount";
            this.gridColumn49.Name = "gridColumn49";
            this.gridColumn49.OptionsColumn.ReadOnly = true;
            this.gridColumn49.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "LinkedOutstandingJobCount", "{0:n}")});
            this.gridColumn49.Visible = true;
            this.gridColumn49.VisibleIndex = 10;
            this.gridColumn49.Width = 138;
            // 
            // gridColumn50
            // 
            this.gridColumn50.Caption = "Linked Jobs Completed";
            this.gridColumn50.FieldName = "LinkedCompletedJobCount";
            this.gridColumn50.Name = "gridColumn50";
            this.gridColumn50.OptionsColumn.ReadOnly = true;
            this.gridColumn50.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "LinkedCompletedJobCount", "{0:n}")});
            this.gridColumn50.Visible = true;
            this.gridColumn50.VisibleIndex = 11;
            this.gridColumn50.Width = 130;
            // 
            // gridColumn51
            // 
            this.gridColumn51.Caption = "Created By";
            this.gridColumn51.FieldName = "CreatedByStaffName";
            this.gridColumn51.Name = "gridColumn51";
            this.gridColumn51.OptionsColumn.AllowEdit = false;
            this.gridColumn51.OptionsColumn.AllowFocus = false;
            this.gridColumn51.OptionsColumn.ReadOnly = true;
            this.gridColumn51.Width = 122;
            // 
            // colSellCalculationLevel
            // 
            this.colSellCalculationLevel.Caption = "Sell Calculation ID";
            this.colSellCalculationLevel.FieldName = "SellCalculationLevel";
            this.colSellCalculationLevel.Name = "colSellCalculationLevel";
            this.colSellCalculationLevel.OptionsColumn.AllowEdit = false;
            this.colSellCalculationLevel.OptionsColumn.AllowFocus = false;
            this.colSellCalculationLevel.OptionsColumn.ReadOnly = true;
            this.colSellCalculationLevel.Width = 104;
            // 
            // colSellCalculationLevelDescription
            // 
            this.colSellCalculationLevelDescription.Caption = "Sell Calculation";
            this.colSellCalculationLevelDescription.FieldName = "SellCalculationLevelDescription";
            this.colSellCalculationLevelDescription.Name = "colSellCalculationLevelDescription";
            this.colSellCalculationLevelDescription.OptionsColumn.AllowEdit = false;
            this.colSellCalculationLevelDescription.OptionsColumn.AllowFocus = false;
            this.colSellCalculationLevelDescription.OptionsColumn.ReadOnly = true;
            this.colSellCalculationLevelDescription.Width = 100;
            // 
            // btnClearVisitsFromMap
            // 
            this.btnClearVisitsFromMap.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClearVisitsFromMap.Location = new System.Drawing.Point(143, 115);
            this.btnClearVisitsFromMap.Name = "btnClearVisitsFromMap";
            this.btnClearVisitsFromMap.Size = new System.Drawing.Size(113, 23);
            this.btnClearVisitsFromMap.TabIndex = 3;
            this.btnClearVisitsFromMap.Text = "Clear Visits from Map";
            this.btnClearVisitsFromMap.Click += new System.EventHandler(this.btnClearVisitsFromMap_Click);
            // 
            // btnLoadVisitsIntoMap
            // 
            this.btnLoadVisitsIntoMap.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLoadVisitsIntoMap.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnLoadVisitsIntoMap.Appearance.Options.UseFont = true;
            this.btnLoadVisitsIntoMap.ImageOptions.ImageIndex = 0;
            this.btnLoadVisitsIntoMap.ImageOptions.ImageList = this.imageCollection16x16;
            this.btnLoadVisitsIntoMap.Location = new System.Drawing.Point(0, 115);
            this.btnLoadVisitsIntoMap.Name = "btnLoadVisitsIntoMap";
            this.btnLoadVisitsIntoMap.Size = new System.Drawing.Size(144, 23);
            toolTipTitleItem8.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem8.Appearance.Options.UseImage = true;
            toolTipTitleItem8.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem8.Text = "Load Visits into Map - Information";
            toolTipItem8.LeftIndent = 6;
            toolTipItem8.Text = "Click me to load the selected visits (ticked) into the map.";
            superToolTip8.Items.Add(toolTipTitleItem8);
            superToolTip8.Items.Add(toolTipItem8);
            this.btnLoadVisitsIntoMap.SuperTip = superToolTip8;
            this.btnLoadVisitsIntoMap.TabIndex = 1;
            this.btnLoadVisitsIntoMap.Text = "Load Visits into Map";
            this.btnLoadVisitsIntoMap.Click += new System.EventHandler(this.btnLoadVisitsIntoMap_Click);
            // 
            // gridControl3
            // 
            this.gridControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl3.DataSource = this.sp06276OMMappingJobsBindingSource;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl3.Location = new System.Drawing.Point(0, 1);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit3,
            this.repositoryItemTextEditDate2,
            this.repositoryItemTextEditNumeric2DP2,
            this.repositoryItemMemoExEdit2,
            this.repositoryItemTextEditLatLong2});
            this.gridControl3.Size = new System.Drawing.Size(372, 154);
            this.gridControl3.TabIndex = 3;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp06276OMMappingJobsBindingSource
            // 
            this.sp06276OMMappingJobsBindingSource.DataMember = "sp06276_OM_Mapping_Jobs";
            this.sp06276OMMappingJobsBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colJobID,
            this.colVisitID,
            this.colVisitNumber,
            this.colReactive,
            this.colJobNoLongerRequired,
            this.colRequiresAccessPermit,
            this.colAccessPermitID,
            this.colClientPONumber,
            this.colClientPOID,
            this.colFinanceSystemPONumber,
            this.colExpectedStartDate,
            this.colExpectedEndDate,
            this.colExpectedDurationUnits,
            this.colScheduleSentDate,
            this.colActualStartDate,
            this.colActualEndDate,
            this.colActualDurationUnits,
            this.colClientInvoiceID,
            this.colDateClientInvoiced,
            this.colSelfBillingInvoiceID,
            this.colSelfBillingInvoiceReceivedDate,
            this.colSelfBillingInvoicePaidDate,
            this.colDoNotPayContractor,
            this.colDoNotInvoiceClient,
            this.colRemarks1,
            this.colRouteOrder,
            this.colStartLatitude,
            this.colStartLongitude,
            this.colFinishLatitude,
            this.colFinishLongitude,
            this.colJobStatusDescription,
            this.colSiteName1,
            this.colClientName1,
            this.colCreatedByStaffName,
            this.colExpectedDurationUnitsDescriptor,
            this.colActualDurationUnitsDescriptor,
            this.colJobTypeID,
            this.colJobSubTypeDescription,
            this.colJobTypeDescription,
            this.colRecordType,
            this.colFullDescription,
            this.colMaximumDaysFromLastVisit,
            this.colMinimumDaysFromLastVisit,
            this.colDaysLeeway,
            this.colBillingCentreCodeID,
            this.colContractDescription,
            this.colClientID1,
            this.colClientContractID,
            this.colSiteID1,
            this.colSiteContractID,
            this.colSitePostcode1,
            this.colLocationX,
            this.colLocationY,
            this.colLinkedToVisit,
            this.colClientAndContract,
            this.colRework,
            this.colReworkOriginalJobID});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.GroupCount = 4;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsFind.AlwaysVisible = true;
            this.gridView3.OptionsFind.FindDelay = 2000;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsLayout.StoreFormatRules = true;
            this.gridView3.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.MultiSelect = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colContractDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colVisitNumber, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colExpectedStartDate, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView3.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView3.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView3.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView3.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView3_MouseUp);
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colJobID
            // 
            this.colJobID.Caption = "Job ID";
            this.colJobID.FieldName = "JobID";
            this.colJobID.Name = "colJobID";
            this.colJobID.OptionsColumn.AllowEdit = false;
            this.colJobID.OptionsColumn.AllowFocus = false;
            this.colJobID.OptionsColumn.ReadOnly = true;
            this.colJobID.Width = 52;
            // 
            // colVisitID
            // 
            this.colVisitID.Caption = "Visit ID";
            this.colVisitID.FieldName = "VisitID";
            this.colVisitID.Name = "colVisitID";
            this.colVisitID.OptionsColumn.AllowEdit = false;
            this.colVisitID.OptionsColumn.AllowFocus = false;
            this.colVisitID.OptionsColumn.ReadOnly = true;
            this.colVisitID.Width = 54;
            // 
            // colVisitNumber
            // 
            this.colVisitNumber.Caption = "Visit #";
            this.colVisitNumber.FieldName = "VisitNumber";
            this.colVisitNumber.Name = "colVisitNumber";
            this.colVisitNumber.OptionsColumn.AllowEdit = false;
            this.colVisitNumber.OptionsColumn.AllowFocus = false;
            this.colVisitNumber.OptionsColumn.ReadOnly = true;
            this.colVisitNumber.Width = 51;
            // 
            // colReactive
            // 
            this.colReactive.Caption = "Reactive";
            this.colReactive.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colReactive.FieldName = "Reactive";
            this.colReactive.Name = "colReactive";
            this.colReactive.OptionsColumn.AllowEdit = false;
            this.colReactive.OptionsColumn.AllowFocus = false;
            this.colReactive.OptionsColumn.ReadOnly = true;
            this.colReactive.Visible = true;
            this.colReactive.VisibleIndex = 11;
            this.colReactive.Width = 63;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Caption = "Check";
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = 1;
            this.repositoryItemCheckEdit3.ValueUnchecked = 0;
            // 
            // colJobNoLongerRequired
            // 
            this.colJobNoLongerRequired.Caption = "No longer Required";
            this.colJobNoLongerRequired.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colJobNoLongerRequired.FieldName = "JobNoLongerRequired";
            this.colJobNoLongerRequired.Name = "colJobNoLongerRequired";
            this.colJobNoLongerRequired.OptionsColumn.AllowEdit = false;
            this.colJobNoLongerRequired.OptionsColumn.AllowFocus = false;
            this.colJobNoLongerRequired.OptionsColumn.ReadOnly = true;
            this.colJobNoLongerRequired.Visible = true;
            this.colJobNoLongerRequired.VisibleIndex = 12;
            this.colJobNoLongerRequired.Width = 113;
            // 
            // colRequiresAccessPermit
            // 
            this.colRequiresAccessPermit.Caption = "Needs Access Permit";
            this.colRequiresAccessPermit.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colRequiresAccessPermit.FieldName = "RequiresAccessPermit";
            this.colRequiresAccessPermit.Name = "colRequiresAccessPermit";
            this.colRequiresAccessPermit.OptionsColumn.AllowEdit = false;
            this.colRequiresAccessPermit.OptionsColumn.AllowFocus = false;
            this.colRequiresAccessPermit.OptionsColumn.ReadOnly = true;
            this.colRequiresAccessPermit.Width = 120;
            // 
            // colAccessPermitID
            // 
            this.colAccessPermitID.Caption = "Permit ID";
            this.colAccessPermitID.FieldName = "AccessPermitID";
            this.colAccessPermitID.Name = "colAccessPermitID";
            this.colAccessPermitID.OptionsColumn.AllowEdit = false;
            this.colAccessPermitID.OptionsColumn.AllowFocus = false;
            this.colAccessPermitID.OptionsColumn.ReadOnly = true;
            // 
            // colClientPONumber
            // 
            this.colClientPONumber.Caption = "Client PO #";
            this.colClientPONumber.FieldName = "ClientPONumber";
            this.colClientPONumber.Name = "colClientPONumber";
            this.colClientPONumber.OptionsColumn.AllowEdit = false;
            this.colClientPONumber.OptionsColumn.AllowFocus = false;
            this.colClientPONumber.OptionsColumn.ReadOnly = true;
            this.colClientPONumber.Width = 76;
            // 
            // colClientPOID
            // 
            this.colClientPOID.Caption = "Client PO ID";
            this.colClientPOID.FieldName = "ClientPOID";
            this.colClientPOID.Name = "colClientPOID";
            this.colClientPOID.OptionsColumn.AllowEdit = false;
            this.colClientPOID.OptionsColumn.AllowFocus = false;
            this.colClientPOID.OptionsColumn.ReadOnly = true;
            this.colClientPOID.Width = 79;
            // 
            // colFinanceSystemPONumber
            // 
            this.colFinanceSystemPONumber.Caption = "Finance PO #";
            this.colFinanceSystemPONumber.FieldName = "FinanceSystemPONumber";
            this.colFinanceSystemPONumber.Name = "colFinanceSystemPONumber";
            this.colFinanceSystemPONumber.OptionsColumn.AllowEdit = false;
            this.colFinanceSystemPONumber.OptionsColumn.AllowFocus = false;
            this.colFinanceSystemPONumber.OptionsColumn.ReadOnly = true;
            this.colFinanceSystemPONumber.Width = 86;
            // 
            // colExpectedStartDate
            // 
            this.colExpectedStartDate.Caption = "Expected Start";
            this.colExpectedStartDate.ColumnEdit = this.repositoryItemTextEditDate2;
            this.colExpectedStartDate.FieldName = "ExpectedStartDate";
            this.colExpectedStartDate.Name = "colExpectedStartDate";
            this.colExpectedStartDate.OptionsColumn.AllowEdit = false;
            this.colExpectedStartDate.OptionsColumn.AllowFocus = false;
            this.colExpectedStartDate.OptionsColumn.ReadOnly = true;
            this.colExpectedStartDate.Visible = true;
            this.colExpectedStartDate.VisibleIndex = 3;
            this.colExpectedStartDate.Width = 106;
            // 
            // repositoryItemTextEditDate2
            // 
            this.repositoryItemTextEditDate2.AutoHeight = false;
            this.repositoryItemTextEditDate2.Mask.EditMask = "g";
            this.repositoryItemTextEditDate2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDate2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDate2.Name = "repositoryItemTextEditDate2";
            // 
            // colExpectedEndDate
            // 
            this.colExpectedEndDate.Caption = "Expected End";
            this.colExpectedEndDate.ColumnEdit = this.repositoryItemTextEditDate2;
            this.colExpectedEndDate.FieldName = "ExpectedEndDate";
            this.colExpectedEndDate.Name = "colExpectedEndDate";
            this.colExpectedEndDate.OptionsColumn.AllowEdit = false;
            this.colExpectedEndDate.OptionsColumn.AllowFocus = false;
            this.colExpectedEndDate.OptionsColumn.ReadOnly = true;
            this.colExpectedEndDate.Visible = true;
            this.colExpectedEndDate.VisibleIndex = 4;
            this.colExpectedEndDate.Width = 87;
            // 
            // colExpectedDurationUnits
            // 
            this.colExpectedDurationUnits.Caption = "Expected Duration";
            this.colExpectedDurationUnits.ColumnEdit = this.repositoryItemTextEditNumeric2DP2;
            this.colExpectedDurationUnits.FieldName = "ExpectedDurationUnits";
            this.colExpectedDurationUnits.Name = "colExpectedDurationUnits";
            this.colExpectedDurationUnits.OptionsColumn.AllowEdit = false;
            this.colExpectedDurationUnits.OptionsColumn.AllowFocus = false;
            this.colExpectedDurationUnits.OptionsColumn.ReadOnly = true;
            this.colExpectedDurationUnits.Visible = true;
            this.colExpectedDurationUnits.VisibleIndex = 5;
            this.colExpectedDurationUnits.Width = 110;
            // 
            // repositoryItemTextEditNumeric2DP2
            // 
            this.repositoryItemTextEditNumeric2DP2.AutoHeight = false;
            this.repositoryItemTextEditNumeric2DP2.Mask.EditMask = "n2";
            this.repositoryItemTextEditNumeric2DP2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumeric2DP2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumeric2DP2.Name = "repositoryItemTextEditNumeric2DP2";
            // 
            // colScheduleSentDate
            // 
            this.colScheduleSentDate.Caption = "Schedule Sent";
            this.colScheduleSentDate.ColumnEdit = this.repositoryItemTextEditDate2;
            this.colScheduleSentDate.FieldName = "ScheduleSentDate";
            this.colScheduleSentDate.Name = "colScheduleSentDate";
            this.colScheduleSentDate.OptionsColumn.AllowEdit = false;
            this.colScheduleSentDate.OptionsColumn.AllowFocus = false;
            this.colScheduleSentDate.OptionsColumn.ReadOnly = true;
            this.colScheduleSentDate.Width = 89;
            // 
            // colActualStartDate
            // 
            this.colActualStartDate.Caption = "Actual Start";
            this.colActualStartDate.ColumnEdit = this.repositoryItemTextEditDate2;
            this.colActualStartDate.FieldName = "ActualStartDate";
            this.colActualStartDate.Name = "colActualStartDate";
            this.colActualStartDate.OptionsColumn.AllowEdit = false;
            this.colActualStartDate.OptionsColumn.AllowFocus = false;
            this.colActualStartDate.OptionsColumn.ReadOnly = true;
            this.colActualStartDate.Visible = true;
            this.colActualStartDate.VisibleIndex = 7;
            this.colActualStartDate.Width = 78;
            // 
            // colActualEndDate
            // 
            this.colActualEndDate.Caption = "Actual End";
            this.colActualEndDate.ColumnEdit = this.repositoryItemTextEditDate2;
            this.colActualEndDate.FieldName = "ActualEndDate";
            this.colActualEndDate.Name = "colActualEndDate";
            this.colActualEndDate.OptionsColumn.AllowEdit = false;
            this.colActualEndDate.OptionsColumn.AllowFocus = false;
            this.colActualEndDate.OptionsColumn.ReadOnly = true;
            this.colActualEndDate.Visible = true;
            this.colActualEndDate.VisibleIndex = 8;
            // 
            // colActualDurationUnits
            // 
            this.colActualDurationUnits.Caption = "Actual Duration";
            this.colActualDurationUnits.ColumnEdit = this.repositoryItemTextEditNumeric2DP2;
            this.colActualDurationUnits.FieldName = "ActualDurationUnits";
            this.colActualDurationUnits.Name = "colActualDurationUnits";
            this.colActualDurationUnits.OptionsColumn.AllowEdit = false;
            this.colActualDurationUnits.OptionsColumn.AllowFocus = false;
            this.colActualDurationUnits.OptionsColumn.ReadOnly = true;
            this.colActualDurationUnits.Visible = true;
            this.colActualDurationUnits.VisibleIndex = 9;
            this.colActualDurationUnits.Width = 95;
            // 
            // colClientInvoiceID
            // 
            this.colClientInvoiceID.Caption = "Client Invoice ID";
            this.colClientInvoiceID.FieldName = "ClientInvoiceID";
            this.colClientInvoiceID.Name = "colClientInvoiceID";
            this.colClientInvoiceID.OptionsColumn.AllowEdit = false;
            this.colClientInvoiceID.OptionsColumn.AllowFocus = false;
            this.colClientInvoiceID.OptionsColumn.ReadOnly = true;
            this.colClientInvoiceID.Width = 100;
            // 
            // colDateClientInvoiced
            // 
            this.colDateClientInvoiced.Caption = "Client Invoiced Date";
            this.colDateClientInvoiced.ColumnEdit = this.repositoryItemTextEditDate2;
            this.colDateClientInvoiced.FieldName = "DateClientInvoiced";
            this.colDateClientInvoiced.Name = "colDateClientInvoiced";
            this.colDateClientInvoiced.OptionsColumn.AllowEdit = false;
            this.colDateClientInvoiced.OptionsColumn.AllowFocus = false;
            this.colDateClientInvoiced.OptionsColumn.ReadOnly = true;
            this.colDateClientInvoiced.Visible = true;
            this.colDateClientInvoiced.VisibleIndex = 15;
            this.colDateClientInvoiced.Width = 118;
            // 
            // colSelfBillingInvoiceID
            // 
            this.colSelfBillingInvoiceID.Caption = "Self Billing Invoice ID";
            this.colSelfBillingInvoiceID.FieldName = "SelfBillingInvoiceID";
            this.colSelfBillingInvoiceID.Name = "colSelfBillingInvoiceID";
            this.colSelfBillingInvoiceID.OptionsColumn.AllowEdit = false;
            this.colSelfBillingInvoiceID.OptionsColumn.AllowFocus = false;
            this.colSelfBillingInvoiceID.OptionsColumn.ReadOnly = true;
            this.colSelfBillingInvoiceID.Width = 121;
            // 
            // colSelfBillingInvoiceReceivedDate
            // 
            this.colSelfBillingInvoiceReceivedDate.Caption = "Self Billing Received Date";
            this.colSelfBillingInvoiceReceivedDate.ColumnEdit = this.repositoryItemTextEditDate2;
            this.colSelfBillingInvoiceReceivedDate.FieldName = "SelfBillingInvoiceReceivedDate";
            this.colSelfBillingInvoiceReceivedDate.Name = "colSelfBillingInvoiceReceivedDate";
            this.colSelfBillingInvoiceReceivedDate.OptionsColumn.AllowEdit = false;
            this.colSelfBillingInvoiceReceivedDate.OptionsColumn.AllowFocus = false;
            this.colSelfBillingInvoiceReceivedDate.OptionsColumn.ReadOnly = true;
            this.colSelfBillingInvoiceReceivedDate.Width = 141;
            // 
            // colSelfBillingInvoicePaidDate
            // 
            this.colSelfBillingInvoicePaidDate.Caption = "Sef Billing Received Date";
            this.colSelfBillingInvoicePaidDate.ColumnEdit = this.repositoryItemTextEditDate2;
            this.colSelfBillingInvoicePaidDate.FieldName = "SelfBillingInvoicePaidDate";
            this.colSelfBillingInvoicePaidDate.Name = "colSelfBillingInvoicePaidDate";
            this.colSelfBillingInvoicePaidDate.OptionsColumn.AllowEdit = false;
            this.colSelfBillingInvoicePaidDate.OptionsColumn.AllowFocus = false;
            this.colSelfBillingInvoicePaidDate.OptionsColumn.ReadOnly = true;
            this.colSelfBillingInvoicePaidDate.Width = 139;
            // 
            // colDoNotPayContractor
            // 
            this.colDoNotPayContractor.Caption = "Don\'t Pay Contractor";
            this.colDoNotPayContractor.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colDoNotPayContractor.FieldName = "DoNotPayContractor";
            this.colDoNotPayContractor.Name = "colDoNotPayContractor";
            this.colDoNotPayContractor.OptionsColumn.AllowEdit = false;
            this.colDoNotPayContractor.OptionsColumn.AllowFocus = false;
            this.colDoNotPayContractor.OptionsColumn.ReadOnly = true;
            this.colDoNotPayContractor.Visible = true;
            this.colDoNotPayContractor.VisibleIndex = 13;
            this.colDoNotPayContractor.Width = 122;
            // 
            // colDoNotInvoiceClient
            // 
            this.colDoNotInvoiceClient.Caption = "Don\'t Invoice Client";
            this.colDoNotInvoiceClient.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colDoNotInvoiceClient.FieldName = "DoNotInvoiceClient";
            this.colDoNotInvoiceClient.Name = "colDoNotInvoiceClient";
            this.colDoNotInvoiceClient.OptionsColumn.AllowEdit = false;
            this.colDoNotInvoiceClient.OptionsColumn.AllowFocus = false;
            this.colDoNotInvoiceClient.OptionsColumn.ReadOnly = true;
            this.colDoNotInvoiceClient.Visible = true;
            this.colDoNotInvoiceClient.VisibleIndex = 14;
            this.colDoNotInvoiceClient.Width = 114;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 20;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colRouteOrder
            // 
            this.colRouteOrder.Caption = "Route Order";
            this.colRouteOrder.FieldName = "RouteOrder";
            this.colRouteOrder.Name = "colRouteOrder";
            this.colRouteOrder.OptionsColumn.AllowEdit = false;
            this.colRouteOrder.OptionsColumn.AllowFocus = false;
            this.colRouteOrder.OptionsColumn.ReadOnly = true;
            this.colRouteOrder.Width = 81;
            // 
            // colStartLatitude
            // 
            this.colStartLatitude.Caption = "Start Latitude";
            this.colStartLatitude.ColumnEdit = this.repositoryItemTextEditLatLong2;
            this.colStartLatitude.FieldName = "StartLatitude";
            this.colStartLatitude.Name = "colStartLatitude";
            this.colStartLatitude.OptionsColumn.AllowEdit = false;
            this.colStartLatitude.OptionsColumn.AllowFocus = false;
            this.colStartLatitude.OptionsColumn.ReadOnly = true;
            this.colStartLatitude.Width = 87;
            // 
            // repositoryItemTextEditLatLong2
            // 
            this.repositoryItemTextEditLatLong2.AutoHeight = false;
            this.repositoryItemTextEditLatLong2.Mask.EditMask = "###0.0000000000";
            this.repositoryItemTextEditLatLong2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditLatLong2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditLatLong2.Name = "repositoryItemTextEditLatLong2";
            // 
            // colStartLongitude
            // 
            this.colStartLongitude.Caption = "Start Longitude";
            this.colStartLongitude.ColumnEdit = this.repositoryItemTextEditLatLong2;
            this.colStartLongitude.FieldName = "StartLongitude";
            this.colStartLongitude.Name = "colStartLongitude";
            this.colStartLongitude.OptionsColumn.AllowEdit = false;
            this.colStartLongitude.OptionsColumn.AllowFocus = false;
            this.colStartLongitude.OptionsColumn.ReadOnly = true;
            this.colStartLongitude.Width = 95;
            // 
            // colFinishLatitude
            // 
            this.colFinishLatitude.Caption = "Finish Latitude";
            this.colFinishLatitude.ColumnEdit = this.repositoryItemTextEditLatLong2;
            this.colFinishLatitude.FieldName = "FinishLatitude";
            this.colFinishLatitude.Name = "colFinishLatitude";
            this.colFinishLatitude.OptionsColumn.AllowEdit = false;
            this.colFinishLatitude.OptionsColumn.AllowFocus = false;
            this.colFinishLatitude.OptionsColumn.ReadOnly = true;
            this.colFinishLatitude.Width = 90;
            // 
            // colFinishLongitude
            // 
            this.colFinishLongitude.Caption = "Finish Longitude";
            this.colFinishLongitude.ColumnEdit = this.repositoryItemTextEditLatLong2;
            this.colFinishLongitude.FieldName = "FinishLongitude";
            this.colFinishLongitude.Name = "colFinishLongitude";
            this.colFinishLongitude.OptionsColumn.AllowEdit = false;
            this.colFinishLongitude.OptionsColumn.AllowFocus = false;
            this.colFinishLongitude.OptionsColumn.ReadOnly = true;
            this.colFinishLongitude.Width = 98;
            // 
            // colJobStatusDescription
            // 
            this.colJobStatusDescription.Caption = "Job Status";
            this.colJobStatusDescription.FieldName = "JobStatusDescription";
            this.colJobStatusDescription.Name = "colJobStatusDescription";
            this.colJobStatusDescription.OptionsColumn.AllowEdit = false;
            this.colJobStatusDescription.OptionsColumn.AllowFocus = false;
            this.colJobStatusDescription.OptionsColumn.ReadOnly = true;
            this.colJobStatusDescription.Visible = true;
            this.colJobStatusDescription.VisibleIndex = 2;
            this.colJobStatusDescription.Width = 98;
            // 
            // colSiteName1
            // 
            this.colSiteName1.Caption = "Site Name";
            this.colSiteName1.FieldName = "SiteName";
            this.colSiteName1.Name = "colSiteName1";
            this.colSiteName1.OptionsColumn.AllowEdit = false;
            this.colSiteName1.OptionsColumn.AllowFocus = false;
            this.colSiteName1.OptionsColumn.ReadOnly = true;
            // 
            // colClientName1
            // 
            this.colClientName1.Caption = "Client Name";
            this.colClientName1.FieldName = "ClientName";
            this.colClientName1.Name = "colClientName1";
            this.colClientName1.OptionsColumn.AllowEdit = false;
            this.colClientName1.OptionsColumn.AllowFocus = false;
            this.colClientName1.OptionsColumn.ReadOnly = true;
            this.colClientName1.Width = 78;
            // 
            // colCreatedByStaffName
            // 
            this.colCreatedByStaffName.Caption = "Created By";
            this.colCreatedByStaffName.FieldName = "CreatedByStaffName";
            this.colCreatedByStaffName.Name = "colCreatedByStaffName";
            this.colCreatedByStaffName.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffName.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffName.OptionsColumn.ReadOnly = true;
            // 
            // colExpectedDurationUnitsDescriptor
            // 
            this.colExpectedDurationUnitsDescriptor.Caption = "Expected Duration Descriptor";
            this.colExpectedDurationUnitsDescriptor.FieldName = "ExpectedDurationUnitsDescriptor";
            this.colExpectedDurationUnitsDescriptor.Name = "colExpectedDurationUnitsDescriptor";
            this.colExpectedDurationUnitsDescriptor.OptionsColumn.AllowEdit = false;
            this.colExpectedDurationUnitsDescriptor.OptionsColumn.AllowFocus = false;
            this.colExpectedDurationUnitsDescriptor.OptionsColumn.ReadOnly = true;
            this.colExpectedDurationUnitsDescriptor.Visible = true;
            this.colExpectedDurationUnitsDescriptor.VisibleIndex = 6;
            this.colExpectedDurationUnitsDescriptor.Width = 162;
            // 
            // colActualDurationUnitsDescriptor
            // 
            this.colActualDurationUnitsDescriptor.Caption = "Actual Duration Descriptor";
            this.colActualDurationUnitsDescriptor.FieldName = "ActualDurationUnitsDescriptor";
            this.colActualDurationUnitsDescriptor.Name = "colActualDurationUnitsDescriptor";
            this.colActualDurationUnitsDescriptor.OptionsColumn.AllowEdit = false;
            this.colActualDurationUnitsDescriptor.OptionsColumn.AllowFocus = false;
            this.colActualDurationUnitsDescriptor.OptionsColumn.ReadOnly = true;
            this.colActualDurationUnitsDescriptor.Visible = true;
            this.colActualDurationUnitsDescriptor.VisibleIndex = 10;
            this.colActualDurationUnitsDescriptor.Width = 147;
            // 
            // colJobTypeID
            // 
            this.colJobTypeID.Caption = "Job Type ID";
            this.colJobTypeID.FieldName = "JobTypeID";
            this.colJobTypeID.Name = "colJobTypeID";
            this.colJobTypeID.OptionsColumn.AllowEdit = false;
            this.colJobTypeID.OptionsColumn.AllowFocus = false;
            this.colJobTypeID.OptionsColumn.ReadOnly = true;
            this.colJobTypeID.Width = 79;
            // 
            // colJobSubTypeDescription
            // 
            this.colJobSubTypeDescription.Caption = "Job Sub-Type";
            this.colJobSubTypeDescription.FieldName = "JobSubTypeDescription";
            this.colJobSubTypeDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colJobSubTypeDescription.Name = "colJobSubTypeDescription";
            this.colJobSubTypeDescription.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeDescription.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeDescription.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeDescription.Visible = true;
            this.colJobSubTypeDescription.VisibleIndex = 1;
            this.colJobSubTypeDescription.Width = 95;
            // 
            // colJobTypeDescription
            // 
            this.colJobTypeDescription.Caption = "Job Type";
            this.colJobTypeDescription.FieldName = "JobTypeDescription";
            this.colJobTypeDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colJobTypeDescription.Name = "colJobTypeDescription";
            this.colJobTypeDescription.OptionsColumn.AllowEdit = false;
            this.colJobTypeDescription.OptionsColumn.AllowFocus = false;
            this.colJobTypeDescription.OptionsColumn.ReadOnly = true;
            this.colJobTypeDescription.Visible = true;
            this.colJobTypeDescription.VisibleIndex = 0;
            this.colJobTypeDescription.Width = 92;
            // 
            // colRecordType
            // 
            this.colRecordType.Caption = "Record Type";
            this.colRecordType.FieldName = "RecordType";
            this.colRecordType.Name = "colRecordType";
            this.colRecordType.OptionsColumn.AllowEdit = false;
            this.colRecordType.OptionsColumn.AllowFocus = false;
            this.colRecordType.OptionsColumn.ReadOnly = true;
            this.colRecordType.Width = 82;
            // 
            // colFullDescription
            // 
            this.colFullDescription.Caption = "Full Description";
            this.colFullDescription.FieldName = "FullDescription";
            this.colFullDescription.Name = "colFullDescription";
            this.colFullDescription.OptionsColumn.AllowEdit = false;
            this.colFullDescription.OptionsColumn.AllowFocus = false;
            this.colFullDescription.OptionsColumn.ReadOnly = true;
            this.colFullDescription.Width = 93;
            // 
            // colMaximumDaysFromLastVisit
            // 
            this.colMaximumDaysFromLastVisit.Caption = "Max Days From Last Visit";
            this.colMaximumDaysFromLastVisit.FieldName = "MaximumDaysFromLastVisit";
            this.colMaximumDaysFromLastVisit.Name = "colMaximumDaysFromLastVisit";
            this.colMaximumDaysFromLastVisit.OptionsColumn.AllowEdit = false;
            this.colMaximumDaysFromLastVisit.OptionsColumn.AllowFocus = false;
            this.colMaximumDaysFromLastVisit.OptionsColumn.ReadOnly = true;
            this.colMaximumDaysFromLastVisit.Visible = true;
            this.colMaximumDaysFromLastVisit.VisibleIndex = 16;
            this.colMaximumDaysFromLastVisit.Width = 140;
            // 
            // colMinimumDaysFromLastVisit
            // 
            this.colMinimumDaysFromLastVisit.Caption = "Min Days From Last Visit";
            this.colMinimumDaysFromLastVisit.FieldName = "MinimumDaysFromLastVisit";
            this.colMinimumDaysFromLastVisit.Name = "colMinimumDaysFromLastVisit";
            this.colMinimumDaysFromLastVisit.OptionsColumn.AllowEdit = false;
            this.colMinimumDaysFromLastVisit.OptionsColumn.AllowFocus = false;
            this.colMinimumDaysFromLastVisit.OptionsColumn.ReadOnly = true;
            this.colMinimumDaysFromLastVisit.Visible = true;
            this.colMinimumDaysFromLastVisit.VisibleIndex = 17;
            this.colMinimumDaysFromLastVisit.Width = 136;
            // 
            // colDaysLeeway
            // 
            this.colDaysLeeway.Caption = "Days Leeway";
            this.colDaysLeeway.FieldName = "DaysLeeway";
            this.colDaysLeeway.Name = "colDaysLeeway";
            this.colDaysLeeway.OptionsColumn.AllowEdit = false;
            this.colDaysLeeway.OptionsColumn.AllowFocus = false;
            this.colDaysLeeway.OptionsColumn.ReadOnly = true;
            this.colDaysLeeway.Visible = true;
            this.colDaysLeeway.VisibleIndex = 18;
            this.colDaysLeeway.Width = 85;
            // 
            // colBillingCentreCodeID
            // 
            this.colBillingCentreCodeID.Caption = "Billing Centre Code ID";
            this.colBillingCentreCodeID.FieldName = "BillingCentreCodeID";
            this.colBillingCentreCodeID.Name = "colBillingCentreCodeID";
            this.colBillingCentreCodeID.OptionsColumn.AllowEdit = false;
            this.colBillingCentreCodeID.OptionsColumn.AllowFocus = false;
            this.colBillingCentreCodeID.OptionsColumn.ReadOnly = true;
            this.colBillingCentreCodeID.Width = 125;
            // 
            // colContractDescription
            // 
            this.colContractDescription.Caption = "Contract Description";
            this.colContractDescription.FieldName = "ContractDescription";
            this.colContractDescription.Name = "colContractDescription";
            this.colContractDescription.OptionsColumn.AllowEdit = false;
            this.colContractDescription.OptionsColumn.AllowFocus = false;
            this.colContractDescription.OptionsColumn.ReadOnly = true;
            this.colContractDescription.Width = 119;
            // 
            // colClientID1
            // 
            this.colClientID1.Caption = "Client ID";
            this.colClientID1.FieldName = "ClientID";
            this.colClientID1.Name = "colClientID1";
            this.colClientID1.OptionsColumn.AllowEdit = false;
            this.colClientID1.OptionsColumn.AllowFocus = false;
            this.colClientID1.OptionsColumn.ReadOnly = true;
            // 
            // colClientContractID
            // 
            this.colClientContractID.Caption = "Client Contract ID";
            this.colClientContractID.FieldName = "ClientContractID";
            this.colClientContractID.Name = "colClientContractID";
            this.colClientContractID.OptionsColumn.AllowEdit = false;
            this.colClientContractID.OptionsColumn.AllowFocus = false;
            this.colClientContractID.OptionsColumn.ReadOnly = true;
            this.colClientContractID.Width = 107;
            // 
            // colSiteID1
            // 
            this.colSiteID1.Caption = "Site ID";
            this.colSiteID1.FieldName = "SiteID";
            this.colSiteID1.Name = "colSiteID1";
            this.colSiteID1.OptionsColumn.AllowEdit = false;
            this.colSiteID1.OptionsColumn.AllowFocus = false;
            this.colSiteID1.OptionsColumn.ReadOnly = true;
            // 
            // colSiteContractID
            // 
            this.colSiteContractID.Caption = "Site Contract ID";
            this.colSiteContractID.FieldName = "SiteContractID";
            this.colSiteContractID.Name = "colSiteContractID";
            this.colSiteContractID.OptionsColumn.AllowEdit = false;
            this.colSiteContractID.OptionsColumn.AllowFocus = false;
            this.colSiteContractID.OptionsColumn.ReadOnly = true;
            this.colSiteContractID.Width = 98;
            // 
            // colSitePostcode1
            // 
            this.colSitePostcode1.Caption = "Site Postcode";
            this.colSitePostcode1.FieldName = "SitePostcode";
            this.colSitePostcode1.Name = "colSitePostcode1";
            this.colSitePostcode1.OptionsColumn.AllowEdit = false;
            this.colSitePostcode1.OptionsColumn.AllowFocus = false;
            this.colSitePostcode1.OptionsColumn.ReadOnly = true;
            this.colSitePostcode1.Width = 86;
            // 
            // colLocationX
            // 
            this.colLocationX.Caption = "Site Latitude";
            this.colLocationX.ColumnEdit = this.repositoryItemTextEditLatLong2;
            this.colLocationX.FieldName = "LocationX";
            this.colLocationX.Name = "colLocationX";
            this.colLocationX.OptionsColumn.AllowEdit = false;
            this.colLocationX.OptionsColumn.AllowFocus = false;
            this.colLocationX.OptionsColumn.ReadOnly = true;
            this.colLocationX.Width = 81;
            // 
            // colLocationY
            // 
            this.colLocationY.Caption = "Site Longitude";
            this.colLocationY.ColumnEdit = this.repositoryItemTextEditLatLong2;
            this.colLocationY.FieldName = "LocationY";
            this.colLocationY.Name = "colLocationY";
            this.colLocationY.OptionsColumn.AllowEdit = false;
            this.colLocationY.OptionsColumn.AllowFocus = false;
            this.colLocationY.OptionsColumn.ReadOnly = true;
            this.colLocationY.Width = 89;
            // 
            // colLinkedToVisit
            // 
            this.colLinkedToVisit.Caption = "Linked To Visit";
            this.colLinkedToVisit.FieldName = "LinkedToVisit";
            this.colLinkedToVisit.Name = "colLinkedToVisit";
            this.colLinkedToVisit.OptionsColumn.AllowEdit = false;
            this.colLinkedToVisit.OptionsColumn.AllowFocus = false;
            this.colLinkedToVisit.OptionsColumn.ReadOnly = true;
            this.colLinkedToVisit.Width = 88;
            // 
            // colClientAndContract
            // 
            this.colClientAndContract.Caption = "Client and Contract";
            this.colClientAndContract.FieldName = "ClientAndContract";
            this.colClientAndContract.Name = "colClientAndContract";
            this.colClientAndContract.OptionsColumn.AllowEdit = false;
            this.colClientAndContract.OptionsColumn.AllowFocus = false;
            this.colClientAndContract.OptionsColumn.ReadOnly = true;
            this.colClientAndContract.Width = 117;
            // 
            // colRework
            // 
            this.colRework.Caption = "Rework";
            this.colRework.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colRework.FieldName = "Rework";
            this.colRework.Name = "colRework";
            this.colRework.OptionsColumn.AllowEdit = false;
            this.colRework.OptionsColumn.AllowFocus = false;
            this.colRework.OptionsColumn.ReadOnly = true;
            this.colRework.Visible = true;
            this.colRework.VisibleIndex = 19;
            this.colRework.Width = 57;
            // 
            // colReworkOriginalJobID
            // 
            this.colReworkOriginalJobID.Caption = "Rework Original Job ID";
            this.colReworkOriginalJobID.FieldName = "ReworkOriginalJobID";
            this.colReworkOriginalJobID.Name = "colReworkOriginalJobID";
            this.colReworkOriginalJobID.OptionsColumn.AllowEdit = false;
            this.colReworkOriginalJobID.OptionsColumn.AllowFocus = false;
            this.colReworkOriginalJobID.OptionsColumn.ReadOnly = true;
            this.colReworkOriginalJobID.Width = 130;
            // 
            // btnClearJobsFromMap
            // 
            this.btnClearJobsFromMap.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClearJobsFromMap.Location = new System.Drawing.Point(139, 156);
            this.btnClearJobsFromMap.Name = "btnClearJobsFromMap";
            this.btnClearJobsFromMap.Size = new System.Drawing.Size(113, 23);
            this.btnClearJobsFromMap.TabIndex = 6;
            this.btnClearJobsFromMap.Text = "Clear Jobs from Map";
            this.btnClearJobsFromMap.Click += new System.EventHandler(this.btnClearJobsFromMap_Click);
            // 
            // btnLoadJobsIntoMap
            // 
            this.btnLoadJobsIntoMap.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLoadJobsIntoMap.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnLoadJobsIntoMap.Appearance.Options.UseFont = true;
            this.btnLoadJobsIntoMap.ImageOptions.ImageIndex = 0;
            this.btnLoadJobsIntoMap.ImageOptions.ImageList = this.imageCollection16x16;
            this.btnLoadJobsIntoMap.Location = new System.Drawing.Point(0, 156);
            this.btnLoadJobsIntoMap.Name = "btnLoadJobsIntoMap";
            this.btnLoadJobsIntoMap.Size = new System.Drawing.Size(140, 23);
            toolTipTitleItem9.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem9.Appearance.Options.UseImage = true;
            toolTipTitleItem9.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem9.Text = "Load Jobs into Map - Information";
            toolTipItem9.LeftIndent = 6;
            toolTipItem9.Text = "Click me to load the selected jobs (ticked) into the map.";
            superToolTip9.Items.Add(toolTipTitleItem9);
            superToolTip9.Items.Add(toolTipItem9);
            this.btnLoadJobsIntoMap.SuperTip = superToolTip9;
            this.btnLoadJobsIntoMap.TabIndex = 5;
            this.btnLoadJobsIntoMap.Text = "Load Jobs into Map";
            this.btnLoadJobsIntoMap.Click += new System.EventHandler(this.btnLoadJobsIntoMap_Click);
            // 
            // pmSiteGridMenu
            // 
            this.pmSiteGridMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiViewSiteOnMap),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiExportSitesToExcel, true)});
            this.pmSiteGridMenu.Manager = this.barManager1;
            this.pmSiteGridMenu.Name = "pmSiteGridMenu";
            // 
            // bbiViewSiteOnMap
            // 
            this.bbiViewSiteOnMap.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiViewSiteOnMap.Caption = "View selected <b>Site</b> on Map";
            this.bbiViewSiteOnMap.Id = 34;
            this.bbiViewSiteOnMap.ImageOptions.ImageIndex = 2;
            this.bbiViewSiteOnMap.Name = "bbiViewSiteOnMap";
            this.bbiViewSiteOnMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiViewSiteOnMap_ItemClick);
            // 
            // bbiExportSitesToExcel
            // 
            this.bbiExportSitesToExcel.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiExportSitesToExcel.Caption = "Export <b>Sites</b> To Excel";
            this.bbiExportSitesToExcel.Id = 39;
            this.bbiExportSitesToExcel.ImageOptions.ImageIndex = 1;
            this.bbiExportSitesToExcel.Name = "bbiExportSitesToExcel";
            this.bbiExportSitesToExcel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiExportSitesToExcel_ItemClick);
            // 
            // bar2
            // 
            this.bar2.BarName = "Custom 3";
            this.bar2.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemMapType),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciLegend, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciNavigation, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciMiniMap, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciLayerManager, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiMapObjectCount, true)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Custom 3";
            // 
            // bciLegend
            // 
            this.bciLegend.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left;
            this.bciLegend.Caption = "Legend";
            this.bciLegend.Id = 41;
            this.bciLegend.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bciLegend.ImageOptions.Image")));
            this.bciLegend.Name = "bciLegend";
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Legend - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to Show \\ Hide the Map Legend.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bciLegend.SuperTip = superToolTip1;
            this.bciLegend.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciLegend_CheckedChanged);
            // 
            // bciNavigation
            // 
            this.bciNavigation.BindableChecked = true;
            this.bciNavigation.Caption = "Navigation Pane";
            this.bciNavigation.Checked = true;
            this.bciNavigation.Id = 51;
            this.bciNavigation.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bciNavigation.ImageOptions.Image")));
            this.bciNavigation.Name = "bciNavigation";
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Navigation Pane - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to Show \\ Hide the Navigation Pane.\r\n\r\nThe Navigation Pane shows you the" +
    " current Latitude and Longitude of the mouse pointer when over the map and allow" +
    "s you to pan and zoom the map.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bciNavigation.SuperTip = superToolTip2;
            this.bciNavigation.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciNavigation_CheckedChanged);
            // 
            // bciMiniMap
            // 
            this.bciMiniMap.BindableChecked = true;
            this.bciMiniMap.Caption = "Mini Map";
            this.bciMiniMap.Checked = true;
            this.bciMiniMap.Id = 49;
            this.bciMiniMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bciMiniMap.ImageOptions.Image")));
            this.bciMiniMap.Name = "bciMiniMap";
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Mini Map - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to Show \\ Hide the Mini Map.\r\n\r\nThe Mini Map provides overview navigatio" +
    "n of the map.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bciMiniMap.SuperTip = superToolTip3;
            this.bciMiniMap.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciMiniMap_CheckedChanged);
            // 
            // bciLayerManager
            // 
            this.bciLayerManager.Caption = "Layer Manager";
            this.bciLayerManager.Id = 50;
            this.bciLayerManager.ImageOptions.ImageIndex = 3;
            this.bciLayerManager.Name = "bciLayerManager";
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Text = "Layer Manager - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = resources.GetString("toolTipItem4.Text");
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.bciLayerManager.SuperTip = superToolTip4;
            this.bciLayerManager.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciLayerManager_CheckedChanged);
            // 
            // bsiMapObjectCount
            // 
            this.bsiMapObjectCount.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left;
            this.bsiMapObjectCount.Caption = "Map Object Count: 0";
            this.bsiMapObjectCount.Id = 35;
            this.bsiMapObjectCount.Name = "bsiMapObjectCount";
            // 
            // bbiMapPrintPreview
            // 
            this.bbiMapPrintPreview.Caption = "Print Preview";
            this.bbiMapPrintPreview.Id = 40;
            this.bbiMapPrintPreview.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiMapPrintPreview.ImageOptions.Image")));
            this.bbiMapPrintPreview.Name = "bbiMapPrintPreview";
            this.bbiMapPrintPreview.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiMapPrintPreview_ItemClick);
            // 
            // pmMapControl
            // 
            this.pmMapControl.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiMapPrintPreview)});
            this.pmMapControl.Manager = this.barManager1;
            this.pmMapControl.MenuCaption = "Map Menu";
            this.pmMapControl.Name = "pmMapControl";
            this.pmMapControl.ShowCaption = true;
            // 
            // sp06152_OM_Mapping_VisitsTableAdapter
            // 
            this.sp06152_OM_Mapping_VisitsTableAdapter.ClearBeforeFill = true;
            // 
            // sp06153_OM_Mapping_Visits_Map_ObjectsTableAdapter
            // 
            this.sp06153_OM_Mapping_Visits_Map_ObjectsTableAdapter.ClearBeforeFill = true;
            // 
            // sp06154_OM_Visit_Mapping_SitesTableAdapter
            // 
            this.sp06154_OM_Visit_Mapping_SitesTableAdapter.ClearBeforeFill = true;
            // 
            // sp06155_OM_Visit_Mapping_Sites_Map_ObjectsTableAdapter
            // 
            this.sp06155_OM_Visit_Mapping_Sites_Map_ObjectsTableAdapter.ClearBeforeFill = true;
            // 
            // imageCollection32x32
            // 
            this.imageCollection32x32.ImageSize = new System.Drawing.Size(32, 32);
            this.imageCollection32x32.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection32x32.ImageStream")));
            this.imageCollection32x32.InsertGalleryImage("info_32x32.png", "images/support/info_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/support/info_32x32.png"), 0);
            this.imageCollection32x32.Images.SetKeyName(0, "info_32x32.png");
            this.imageCollection32x32.InsertGalleryImage("exporttoxls_32x32.png", "images/export/exporttoxls_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/export/exporttoxls_32x32.png"), 1);
            this.imageCollection32x32.Images.SetKeyName(1, "exporttoxls_32x32.png");
            this.imageCollection32x32.InsertGalleryImage("geopointmap_32x32.png", "images/maps/geopointmap_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/maps/geopointmap_32x32.png"), 2);
            this.imageCollection32x32.Images.SetKeyName(2, "geopointmap_32x32.png");
            this.imageCollection32x32.InsertGalleryImage("merge_32x32.png", "images/actions/merge_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/merge_32x32.png"), 3);
            this.imageCollection32x32.Images.SetKeyName(3, "merge_32x32.png");
            // 
            // sp06276_OM_Mapping_JobsTableAdapter
            // 
            this.sp06276_OM_Mapping_JobsTableAdapter.ClearBeforeFill = true;
            // 
            // sp06277_OM_Mapping_Jobs_Map_ObjectsTableAdapter
            // 
            this.sp06277_OM_Mapping_Jobs_Map_ObjectsTableAdapter.ClearBeforeFill = true;
            // 
            // pmVisitGridMenu
            // 
            this.pmVisitGridMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiViewVisitStartOnMap),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiViewVisitFinishOnMap),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiExportVisitsToExcel, true)});
            this.pmVisitGridMenu.Manager = this.barManager1;
            this.pmVisitGridMenu.Name = "pmVisitGridMenu";
            // 
            // bbiViewVisitStartOnMap
            // 
            this.bbiViewVisitStartOnMap.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiViewVisitStartOnMap.Caption = "View selected <b>Visit Start</b> on Map";
            this.bbiViewVisitStartOnMap.Id = 42;
            this.bbiViewVisitStartOnMap.ImageOptions.ImageIndex = 2;
            this.bbiViewVisitStartOnMap.Name = "bbiViewVisitStartOnMap";
            this.bbiViewVisitStartOnMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiViewVisitStartOnMap_ItemClick);
            // 
            // bbiViewVisitFinishOnMap
            // 
            this.bbiViewVisitFinishOnMap.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiViewVisitFinishOnMap.Caption = "View selected <b>Visit Finish</b> on Map";
            this.bbiViewVisitFinishOnMap.Id = 43;
            this.bbiViewVisitFinishOnMap.ImageOptions.ImageIndex = 2;
            this.bbiViewVisitFinishOnMap.Name = "bbiViewVisitFinishOnMap";
            this.bbiViewVisitFinishOnMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiViewVisitFinishOnMap_ItemClick);
            // 
            // bbiExportVisitsToExcel
            // 
            this.bbiExportVisitsToExcel.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiExportVisitsToExcel.Caption = "Export <b>Visits</b> to Excel";
            this.bbiExportVisitsToExcel.Id = 44;
            this.bbiExportVisitsToExcel.ImageOptions.ImageIndex = 1;
            this.bbiExportVisitsToExcel.Name = "bbiExportVisitsToExcel";
            this.bbiExportVisitsToExcel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiExportVisitsToExcel_ItemClick);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "barButtonItem1";
            this.barButtonItem1.Id = 45;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // pmJobGridMenu
            // 
            this.pmJobGridMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiViewJobStartOnMap),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiViewJobFinishOnMap),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiExportJobsToExcel, true)});
            this.pmJobGridMenu.Manager = this.barManager1;
            this.pmJobGridMenu.Name = "pmJobGridMenu";
            // 
            // bbiViewJobStartOnMap
            // 
            this.bbiViewJobStartOnMap.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiViewJobStartOnMap.Caption = "View selected <b>Job Start</b> on Map";
            this.bbiViewJobStartOnMap.Id = 46;
            this.bbiViewJobStartOnMap.ImageOptions.ImageIndex = 2;
            this.bbiViewJobStartOnMap.Name = "bbiViewJobStartOnMap";
            this.bbiViewJobStartOnMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiViewJobStartOnMap_ItemClick);
            // 
            // bbiViewJobFinishOnMap
            // 
            this.bbiViewJobFinishOnMap.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiViewJobFinishOnMap.Caption = "View selected <b>Job Finish</b> on Map";
            this.bbiViewJobFinishOnMap.Id = 47;
            this.bbiViewJobFinishOnMap.ImageOptions.ImageIndex = 2;
            this.bbiViewJobFinishOnMap.Name = "bbiViewJobFinishOnMap";
            this.bbiViewJobFinishOnMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiViewJobFinishOnMap_ItemClick);
            // 
            // bbiExportJobsToExcel
            // 
            this.bbiExportJobsToExcel.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiExportJobsToExcel.Caption = "Export <b>Jobs</b> to Excel";
            this.bbiExportJobsToExcel.Id = 48;
            this.bbiExportJobsToExcel.ImageOptions.ImageIndex = 1;
            this.bbiExportJobsToExcel.Name = "bbiExportJobsToExcel";
            this.bbiExportJobsToExcel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiExportJobsToExcel_ItemClick);
            // 
            // sp06354_OM_Mapping_Layer_ManagerTableAdapter
            // 
            this.sp06354_OM_Mapping_Layer_ManagerTableAdapter.ClearBeforeFill = true;
            // 
            // frm_OM_Visit_Mapping_View
            // 
            this.ClientSize = new System.Drawing.Size(891, 737);
            this.Controls.Add(this.popupContainerControlMapType);
            this.Controls.Add(this.mapControl1);
            this.Controls.Add(this.dockPanelData);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_OM_Visit_Mapping_View";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Mapping - View Visits";
            this.Activated += new System.EventHandler(this.frm_OM_Visit_Mapping_View_Activated);
            this.Deactivate += new System.EventHandler(this.frm_OM_Visit_Mapping_View_Deactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_OM_Visit_Mapping_View_FormClosing);
            this.Load += new System.EventHandler(this.frm_OM_Visit_Mapping_View_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dockPanelData, 0);
            this.Controls.SetChildIndex(this.mapControl1, 0);
            this.Controls.SetChildIndex(this.popupContainerControlMapType, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06155OMVisitMappingSitesMapObjectsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Visit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06153OMMappingVisitsMapObjectsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06277OMMappingJobsMapObjectsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Job)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06154OMVisitMappingSitesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditLatLong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06152OMMappingVisitsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlMapType)).EndInit();
            this.popupContainerControlMapType.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControlMapType)).EndInit();
            this.groupControlMapType.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mapControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditMapType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanelLayerManager.ResumeLayout(false);
            this.controlContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06354OMMappingLayerManagerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection16x16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            this.dockPanelLegend.ResumeLayout(false);
            this.dockPanel2_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            this.dockPanelData.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditChooseSites.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06276OMMappingJobsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDate2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric2DP2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditLatLong2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmSiteGridMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmMapControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection32x32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmVisitGridMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmJobGridMenu)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private BaseObjects.ExtendedMapControl mapControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup repositoryItemRadioGroup1;
        private DevExpress.XtraBars.BarEditItem barEditItemMapType;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditMapType;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlMapType;
        private DevExpress.XtraEditors.GroupControl groupControlMapType;
        private DevExpress.XtraEditors.SimpleButton btnOKMapType;
        private DevExpress.XtraEditors.CheckEdit checkEdit4;
        private DevExpress.XtraEditors.CheckEdit checkEdit3;
        private DevExpress.XtraEditors.CheckEdit checkEdit2;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelData;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.Utils.ToolTipController toolTipController1;
        private DevExpress.XtraBars.PopupMenu pmSiteGridMenu;
        private DevExpress.XtraBars.BarButtonItem bbiViewSiteOnMap;
        private DevExpress.XtraEditors.SimpleButton btnLoadVisitsIntoMap;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarStaticItem bsiMapObjectCount;
        private DevExpress.XtraBars.BarButtonItem bbiExportSitesToExcel;
        private DevExpress.XtraBars.BarButtonItem bbiMapPrintPreview;
        private DevExpress.XtraBars.PopupMenu pmMapControl;
        private System.Windows.Forms.BindingSource sp06152OMMappingVisitsBindingSource;
        private DataSet_OM_Visit dataSet_OM_Visit;
        private DataSet_OM_VisitTableAdapters.sp06152_OM_Mapping_VisitsTableAdapter sp06152_OM_Mapping_VisitsTableAdapter;
        private System.Windows.Forms.BindingSource sp06153OMMappingVisitsMapObjectsBindingSource;
        private DataSet_OM_VisitTableAdapters.sp06153_OM_Mapping_Visits_Map_ObjectsTableAdapter sp06153_OM_Mapping_Visits_Map_ObjectsTableAdapter;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelLegend;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel2_Container;
        public DevExpress.XtraBars.BarCheckItem bciLegend;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.SimpleButton btnLoadSitesIntoMap;
        private DevExpress.XtraEditors.SimpleButton btnLoadVisits;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn37;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn39;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn40;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn41;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn42;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn43;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn44;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn45;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn46;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn47;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn48;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn49;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn50;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn51;
        private System.Windows.Forms.BindingSource sp06154OMVisitMappingSitesBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine3;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine4;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine5;
        private DevExpress.XtraGrid.Columns.GridColumn colSitePostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colLatitude;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditLatLong;
        private DevExpress.XtraGrid.Columns.GridColumn colLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteType;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colThematicValue;
        private DevExpress.XtraGrid.Columns.GridColumn colMapLabel;
        private DataSet_OM_VisitTableAdapters.sp06154_OM_Visit_Mapping_SitesTableAdapter sp06154_OM_Visit_Mapping_SitesTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordEnabled;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private System.Windows.Forms.BindingSource sp06155OMVisitMappingSitesMapObjectsBindingSource;
        private DataSet_OM_VisitTableAdapters.sp06155_OM_Visit_Mapping_Sites_Map_ObjectsTableAdapter sp06155_OM_Visit_Mapping_Sites_Map_ObjectsTableAdapter;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.PictureEdit pictureEdit4;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.PictureEdit pictureEdit6;
        private DevExpress.XtraEditors.PictureEdit pictureEdit7;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.ButtonEdit buttonEditChooseSites;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.Utils.ImageCollection imageCollection32x32;
        private DevExpress.XtraEditors.SimpleButton btnClearSitesFromMap;
        private DevExpress.XtraEditors.SimpleButton btnClearVisitsFromMap;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraEditors.SimpleButton btnLoadJobs;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDate2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumeric2DP2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraEditors.SimpleButton btnClearJobsFromMap;
        private DevExpress.XtraEditors.SimpleButton btnLoadJobsIntoMap;
        private System.Windows.Forms.BindingSource sp06276OMMappingJobsBindingSource;
        private DataSet_OM_Job dataSet_OM_Job;
        private DevExpress.XtraGrid.Columns.GridColumn colJobID;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitID;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colReactive;
        private DevExpress.XtraGrid.Columns.GridColumn colJobNoLongerRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colRequiresAccessPermit;
        private DevExpress.XtraGrid.Columns.GridColumn colAccessPermitID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPONumber;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPOID;
        private DevExpress.XtraGrid.Columns.GridColumn colFinanceSystemPONumber;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedDurationUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colScheduleSentDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActualStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActualEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActualDurationUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colClientInvoiceID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateClientInvoiced;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoiceID;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoiceReceivedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoicePaidDate;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotPayContractor;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotInvoiceClient;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colRouteOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colStartLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colStartLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colFinishLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colFinishLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colJobStatusDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName1;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedDurationUnitsDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colActualDurationUnitsDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordType;
        private DevExpress.XtraGrid.Columns.GridColumn colFullDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colMaximumDaysFromLastVisit;
        private DevExpress.XtraGrid.Columns.GridColumn colMinimumDaysFromLastVisit;
        private DevExpress.XtraGrid.Columns.GridColumn colDaysLeeway;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingCentreCodeID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colSitePostcode1;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationX;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationY;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToVisit;
        private DevExpress.XtraGrid.Columns.GridColumn colClientAndContract;
        private DevExpress.XtraGrid.Columns.GridColumn colRework;
        private DevExpress.XtraGrid.Columns.GridColumn colReworkOriginalJobID;
        private DataSet_OM_JobTableAdapters.sp06276_OM_Mapping_JobsTableAdapter sp06276_OM_Mapping_JobsTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditLatLong2;
        private System.Windows.Forms.BindingSource sp06277OMMappingJobsMapObjectsBindingSource;
        private DataSet_OM_JobTableAdapters.sp06277_OM_Mapping_Jobs_Map_ObjectsTableAdapter sp06277_OM_Mapping_Jobs_Map_ObjectsTableAdapter;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.PictureEdit pictureEdit3;
        private DevExpress.XtraEditors.PictureEdit pictureEdit5;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraBars.BarButtonItem bbiViewVisitStartOnMap;
        private DevExpress.XtraBars.BarButtonItem bbiViewVisitFinishOnMap;
        private DevExpress.XtraBars.BarButtonItem bbiExportVisitsToExcel;
        private DevExpress.XtraBars.PopupMenu pmVisitGridMenu;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem bbiViewJobStartOnMap;
        private DevExpress.XtraBars.BarButtonItem bbiViewJobFinishOnMap;
        private DevExpress.XtraBars.BarButtonItem bbiExportJobsToExcel;
        private DevExpress.XtraBars.PopupMenu pmJobGridMenu;
        private DevExpress.XtraBars.BarCheckItem bciMiniMap;
        private DevExpress.XtraGrid.Columns.GridColumn colSellCalculationLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colSellCalculationLevelDescription;
        private DevExpress.Utils.ImageCollection imageCollection16x16;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelLayerManager;
        private DevExpress.XtraBars.Docking.ControlContainer controlContainer1;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DataSet_OM_Core dataSet_OM_Core;
        private System.Windows.Forms.BindingSource sp06354OMMappingLayerManagerBindingSource;
        private DataSet_OM_CoreTableAdapters.sp06354_OM_Mapping_Layer_ManagerTableAdapter sp06354_OM_Mapping_Layer_ManagerTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colLayerName;
        private DevExpress.XtraGrid.Columns.GridColumn colLayerOrder;
        private DevExpress.XtraBars.BarCheckItem bciLayerManager;
        private DevExpress.XtraBars.BarCheckItem bciNavigation;
        private DevExpress.XtraMap.ImageTilesLayer imageTilesLayer1;
        private DevExpress.XtraMap.BingMapDataProvider bingMapDataProvider1;
        private DevExpress.XtraMap.VectorItemsLayer vectorItemsLayer1;
        private DevExpress.XtraMap.ListSourceDataAdapter listSourceDataAdapter1;
        private DevExpress.XtraMap.VectorItemsLayer vectorItemsLayer2;
        private DevExpress.XtraMap.ListSourceDataAdapter listSourceDataAdapter2;
        private DevExpress.XtraMap.VectorItemsLayer vectorItemsLayer3;
        private DevExpress.XtraMap.ListSourceDataAdapter listSourceDataAdapter3;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.PictureEdit pictureEdit8;
    }
}

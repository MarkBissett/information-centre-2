namespace WoodPlan5
{
    partial class frm_OM_Job_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Job_Manager));
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition3 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.Utils.SuperToolTip superToolTip12 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem12 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem12 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip13 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem13 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem13 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip14 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem14 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem14 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem9 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem9 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip10 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem10 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem10 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip11 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem11 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem11 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip19 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem19 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem19 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip16 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem16 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem16 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip17 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem17 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem17 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip18 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem18 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem18 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip15 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem15 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem15 = new DevExpress.Utils.ToolTipItem();
            this.colDisabled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colDisabled2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit17 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colJobStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.gridControlJob = new DevExpress.XtraGrid.GridControl();
            this.sp06031OMJobManagerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Job = new WoodPlan5.DataSet_OM_Job();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridViewJob = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colJobID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditInteger = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colJobSubTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReactive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colJobNoLongerRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRequiresAccessPermit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccessPermitID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPOID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinanceSystemPONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colExpectedEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedDurationUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colExpectedDurationUnitsDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScheduleSentDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualDurationUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualDurationUnitsDescriptionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalMaterialExVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCostTotalMaterialVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalMaterialCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalMaterialExVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalMaterialVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalMaterialCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalEquipmentExVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalEquipmentVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalEquipmentCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalEquipmentExVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalEquipmentVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalEquipmentCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalLabourExVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalLabourVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalLabourCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalLabourExVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalLabourVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSellTotalLabourCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalCostExVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalCostVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalCostExVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalCostVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientInvoiceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateClientInvoiced = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingInvoiceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingInvoiceReceivedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingInvoicePaidDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingInvoiceAmountPaid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotPayContractor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotInvoiceClient = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colRouteOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditLatLong = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colStartLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinishLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinishLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobStatusDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedDurationUnitsDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualDurationUnitsDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedJobCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colRecordType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelected = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFullDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHTML = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colTenderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTenderJobID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinimumDaysFromLastVisit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaximumDaysFromLastVisit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDaysLeeway = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingCentreCodeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedDocumentCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditLinkedDocumentsJob_OpenLink = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colContractDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalEquipmentVATRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalLabourVATRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalMaterialVATRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalMaterialVATRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalEquipmentVATRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalLabourVATRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colImagesFolderOM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRework = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReworkOriginalJobID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSitePostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedLabourCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDaysUntilDue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditIntegerDays = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCancelledReasonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCancelledReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colManuallyCompleted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Alert = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemPictureEditAlert = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.colLinkedPictureCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoWorkRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFriendlyVisitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitCostCalculationLevelID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitSellCalculationLevelID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitCostCalculationLevelDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitSellCalculationLevelDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedFromVisitTemplate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedFromVisitTemplateID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDisplayOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMandatory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSuspendedReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSuspendedRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.toolTipController1 = new DevExpress.Utils.ToolTipController(this.components);
            this.dataSet_GC_Summer_Core = new WoodPlan5.DataSet_GC_Summer_Core();
            this.dataSet_GC_Core = new WoodPlan5.DataSet_GC_Core();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.popupContainerControlDateRange = new DevExpress.XtraEditors.PopupContainerControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.btnDateRangeOK = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerControlRecordTypeFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl8 = new DevExpress.XtraGrid.GridControl();
            this.sp06018OMJobManagerRecordTypesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.btnRecordTypeFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerControlJobStatusFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnJobStatusFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp06025OMJobStatusesFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerControlShowTabPages = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl7 = new DevExpress.XtraGrid.GridControl();
            this.sp04022CoreDummyTabPageListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTabPageName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.btnShowTabPagesOK = new DevExpress.XtraEditors.SimpleButton();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageLabour = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControlLabour = new DevExpress.XtraGrid.GridControl();
            this.sp06033OMJobManagerLinkedlabourBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewLabour = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLabourUsedID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostUnitsUsed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DP2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCostUnitDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellUnitsUsed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellUnitDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostPerUnitExVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCostPerUnitVatRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSellPerUnitExVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellPerUnitVatRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostValueExVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostValueVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostValueTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellValueExVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellValueVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellValueTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubTypeDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedStartDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colVisitNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFullDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHTML2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelected1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colCostUnitDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellUnitDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInternalExternal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCISPercentage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCISValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPurchaseOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEndDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMobileTel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditEmail = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colViewRecord = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditViewRecord = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridSplitContainer5 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControlLabourTiming = new DevExpress.XtraGrid.GridControl();
            this.sp06279OMLabourTimeOnOffSiteBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewLabourTiming = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLabourUsedTimingID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLabourUsedID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamMemberID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDateTime2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime11 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEndDateTime2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDuration = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit11 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colPdaCreatedID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit15 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colTeamMemberName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubTypeDescription3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeDescription4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedStartDate3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitNumber3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFullDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHTML3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colContractorName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitID4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageEquipment = new DevExpress.XtraTab.XtraTabPage();
            this.popupContainerControlMaterialTypeFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnMaterialTypeFilter = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl14 = new DevExpress.XtraGrid.GridControl();
            this.sp06030OMMaterialFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView14 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitsInStock = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DP3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colReorderLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWarningLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerControlJobTypeFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnJobTypeFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl10 = new DevExpress.XtraGrid.GridControl();
            this.sp06026OMJobTypeFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView10 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerControlEquipmentTypeFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnEquipmentFilter = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl13 = new DevExpress.XtraGrid.GridControl();
            this.sp06029OMEquipmentTypeFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView13 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerControlJobSubTypeFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnJobSubTypeFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl11 = new DevExpress.XtraGrid.GridControl();
            this.sp06027OMJobSubTypeFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView11 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControlEquipment = new DevExpress.XtraGrid.GridControl();
            this.sp06034OMJobManagerLinkedEquipmentBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewEquipment = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DP4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn158 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn159 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn160 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn161 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHTML4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn162 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn163 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn164 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn165 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDateTime1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEndDateTime1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPurchaseOrderID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedPictureCount1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditEquipmentPictures = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colImagesFolderOM1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageMaterials = new DevExpress.XtraTab.XtraTabPage();
            this.popupContainerControlLabourFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnLabourFilter = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl12 = new DevExpress.XtraGrid.GridControl();
            this.sp06028OMLabourFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView12 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerControlVisitTypeFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp06308OMVisitTypesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Visit = new WoodPlan5.DataSet_OM_Visit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn224 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn226 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn227 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActive1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit21 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.btnVisitTypeFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControlMaterial = new DevExpress.XtraGrid.GridControl();
            this.sp06035OMJobManagerLinkedMaterialsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewMaterial = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn166 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn168 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn170 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNumeric2DP4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn171 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn172 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn173 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn174 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn175 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn176 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn177 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn178 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn179 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn180 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn181 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn182 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn183 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn184 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit10 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn185 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn186 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn187 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn188 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn189 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn190 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn191 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn192 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn193 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHTML5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn194 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn195 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn196 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn197 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPurchaseOrderID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedPictureCount2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditMaterialPictures = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colImagesFolderOM2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageExtraInfo = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlExtraInfo = new DevExpress.XtraGrid.GridControl();
            this.sp06041OMJobManagerLinkedExtraInfoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewExtraInfo = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn69 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn70 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn71 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn72 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn73 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn74 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn75 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn76 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime10 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn115 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn116 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHTML6 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn118 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit14 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colDataTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEditorTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEditorMask = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDataEntryMinValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDataEntryMaxValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDataEntryRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOnScreenLabel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAttributeOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDefaultValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValueRecorded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValueRecordedText = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAttributeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValueFormula = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValueFormulaEvaluateLocation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValueFormulaEvaluateLocationDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValueFormulaEvaluateOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHideFromApp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageSpraying = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlSpraying = new DevExpress.XtraGrid.GridControl();
            this.sp06038OMJobManagerLinkedSprayingBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewSpraying = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn98 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn101 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn99 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn109 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DP7 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn110 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn123 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn124 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn125 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn126 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn127 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn128 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn129 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn130 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime7 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn131 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn132 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHTML7 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn204 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn205 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit13 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn206 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordTypeID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDilutionRatio = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNumericRatio = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colAmountUsedDiluted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageWaste = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlWaste = new DevExpress.XtraGrid.GridControl();
            this.sp06036OMJobManagerLinkedWasteBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewWaste = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn83 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn86 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWasteTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn88 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNumeric2DP5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn89 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn102 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn103 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn104 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn105 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn106 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn107 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn167 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn198 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn199 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn200 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHTML8 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn201 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn202 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit12 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn203 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWasteDisposalCentreID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWasteDisposalCentreName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWasteDisposalCentreAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordTypeID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWasteDocumentExtension = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWasteDocumentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditWasteDocument = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colWasteDocumentPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPagePictures = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer3 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControlPicture = new DevExpress.XtraGrid.GridControl();
            this.sp06037OMJobManagerLinkedPicturesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewPicture = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn84 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn85 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn87 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn90 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn91 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn92 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime6 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn93 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn95 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn96 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHTML9 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn97 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShortLinkedRecordDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedStartDate2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeDescription3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubTypeDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitNumber2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelected2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPictureType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageComments = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlComment = new DevExpress.XtraGrid.GridControl();
            this.sp06040OMJobManagerLinkedCommentsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewComment = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn52 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn53 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn54 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn55 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn56 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn57 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn60 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn63 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime9 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn64 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn65 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHTML10 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn67 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit10 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colLinkedToRecordTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordedByID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordedByTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordedByType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordedBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateRecorded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisibleToClient = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageHealthAndSafety = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlHealthAndSafety = new DevExpress.XtraGrid.GridControl();
            this.sp06039OMJobManagerLinkedSafetyRequirementBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewHealthAndSafety = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn58 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn59 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn61 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn62 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn77 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn78 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn79 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn80 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn81 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn82 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn100 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn108 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime8 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn111 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn112 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHTML11 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn113 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn114 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit11 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.xtraTabPageCRM = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer4 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControlCRM = new DevExpress.XtraGrid.GridControl();
            this.sp05089CRMContactsLinkedToRecordBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.gridViewCRM = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCRMID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContactID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactDueDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colContactMadeDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactMethodID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit12 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactDirectionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedRecordDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHTML12 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCreatedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactMethod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactDirectionDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedRecordTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateCreated1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContact = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.popupContainerControlConstructionManagerFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnConstructionManagerFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp06294OMJobManagerConstructionManagerFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerControlKAMFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnKAMFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp06019OMJobManagerKAMsFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colForename = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerControlCMFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnCMFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl9 = new DevExpress.XtraGrid.GridControl();
            this.sp06020OMJobManagerCMsFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView9 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPagePurchaseOrders = new DevExpress.XtraTab.XtraTabPage();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.dataSet_GC_Reports = new WoodPlan5.DataSet_GC_Reports();
            this.popupContainerEditDateRange = new DevExpress.XtraEditors.PopupContainerEdit();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.btnClearAllFilters = new DevExpress.XtraEditors.SimpleButton();
            this.checkEditStaff = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditTeam = new DevExpress.XtraEditors.CheckEdit();
            this.popupContainerEditVisitTypeFilter = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerEditConstructionManagerFilter = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerEditShowTabPages = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerEditMaterialTypeFilter = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerEditEquipmentTypeFilter = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerEditLabourFilter = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerEditJobSubTypeFilter = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerEditJobTypeFilter = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerEditRecordTypeFilter = new DevExpress.XtraEditors.PopupContainerEdit();
            this.buttonEditSiteFilter = new DevExpress.XtraEditors.ButtonEdit();
            this.buttonEditClientFilter = new DevExpress.XtraEditors.ButtonEdit();
            this.popupContainerEditCMFilter = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerEditJobStatusFilter = new DevExpress.XtraEditors.PopupContainerEdit();
            this.checkEditColourCode = new DevExpress.XtraEditors.CheckEdit();
            this.btnLoad = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerEditKAMFilter = new DevExpress.XtraEditors.PopupContainerEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForLabourFilter = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForClearAllFilters = new DevExpress.XtraLayout.LayoutControlItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bsiSendJobs = new DevExpress.XtraBars.BarSubItem();
            this.bbiSelectJobsReadyToSend = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSendJobs = new DevExpress.XtraBars.BarButtonItem();
            this.bbiReassignJobs = new DevExpress.XtraBars.BarButtonItem();
            this.bsiManuallyComplete = new DevExpress.XtraBars.BarSubItem();
            this.bbiSelectJobsToComplete = new DevExpress.XtraBars.BarButtonItem();
            this.bbiManuallyCompleteJobs = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAuthorise = new DevExpress.XtraBars.BarButtonItem();
            this.bbiPay = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bciFilterSelected = new DevExpress.XtraBars.BarCheckItem();
            this.bsiSelectedCount = new DevExpress.XtraBars.BarStaticItem();
            this.bbiAddWizard = new DevExpress.XtraBars.BarButtonItem();
            this.bsiViewOnMap = new DevExpress.XtraBars.BarSubItem();
            this.bbiViewSiteOnMap = new DevExpress.XtraBars.BarButtonItem();
            this.bbiViewJobOnMap = new DevExpress.XtraBars.BarButtonItem();
            this.bbiOptimization = new DevExpress.XtraBars.BarButtonItem();
            this.pmOptimisation = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiGetDataToRouteOptimize = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSendDataToRouteOptimizer = new DevExpress.XtraBars.BarButtonItem();
            this.bbiGetDataFromRouteOptimizer = new DevExpress.XtraBars.BarButtonItem();
            this.bbiManuallyComplete = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSendJobs2 = new DevExpress.XtraBars.BarButtonItem();
            this.sp04092_GC_Gritting_Callout_Pictures_For_CalloutTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04092_GC_Gritting_Callout_Pictures_For_CalloutTableAdapter();
            this.sp05089_CRM_Contacts_Linked_To_RecordTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp05089_CRM_Contacts_Linked_To_RecordTableAdapter();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.hideContainerLeft = new DevExpress.XtraBars.Docking.AutoHideContainer();
            this.dockPanelFilters = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.sp04022_Core_Dummy_TabPageListTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04022_Core_Dummy_TabPageListTableAdapter();
            this.sp06018_OM_Job_Manager_Record_TypesTableAdapter = new WoodPlan5.DataSet_GC_Summer_CoreTableAdapters.sp06018_OM_Job_Manager_Record_TypesTableAdapter();
            this.sp06019_OM_Job_Manager_KAMs_FilterTableAdapter = new WoodPlan5.DataSet_GC_Summer_CoreTableAdapters.sp06019_OM_Job_Manager_KAMs_FilterTableAdapter();
            this.sp06020_OM_Job_Manager_CMs_FilterTableAdapter = new WoodPlan5.DataSet_GC_Summer_CoreTableAdapters.sp06020_OM_Job_Manager_CMs_FilterTableAdapter();
            this.sp06025_OM_Job_Statuses_FilterTableAdapter = new WoodPlan5.DataSet_GC_Summer_CoreTableAdapters.sp06025_OM_Job_Statuses_FilterTableAdapter();
            this.sp06026_OM_Job_Type_FilterTableAdapter = new WoodPlan5.DataSet_GC_Summer_CoreTableAdapters.sp06026_OM_Job_Type_FilterTableAdapter();
            this.sp06027_OM_Job_Sub_Type_FilterTableAdapter = new WoodPlan5.DataSet_GC_Summer_CoreTableAdapters.sp06027_OM_Job_Sub_Type_FilterTableAdapter();
            this.sp06028_OM_Labour_FilterTableAdapter = new WoodPlan5.DataSet_GC_Summer_CoreTableAdapters.sp06028_OM_Labour_FilterTableAdapter();
            this.sp06029_OM_Equipment_Type_FilterTableAdapter = new WoodPlan5.DataSet_GC_Summer_CoreTableAdapters.sp06029_OM_Equipment_Type_FilterTableAdapter();
            this.sp06030_OM_Material_FilterTableAdapter = new WoodPlan5.DataSet_GC_Summer_CoreTableAdapters.sp06030_OM_Material_FilterTableAdapter();
            this.sp06031_OM_Job_ManagerTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06031_OM_Job_ManagerTableAdapter();
            this.sp06033_OM_Job_Manager_Linked_labourTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06033_OM_Job_Manager_Linked_labourTableAdapter();
            this.sp06034_OM_Job_Manager_Linked_EquipmentTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06034_OM_Job_Manager_Linked_EquipmentTableAdapter();
            this.sp06035_OM_Job_Manager_Linked_MaterialsTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06035_OM_Job_Manager_Linked_MaterialsTableAdapter();
            this.sp06036_OM_Job_Manager_Linked_WasteTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06036_OM_Job_Manager_Linked_WasteTableAdapter();
            this.sp06037_OM_Job_Manager_Linked_PicturesTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06037_OM_Job_Manager_Linked_PicturesTableAdapter();
            this.sp06038_OM_Job_Manager_Linked_SprayingTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06038_OM_Job_Manager_Linked_SprayingTableAdapter();
            this.sp06039_OM_Job_Manager_Linked_Safety_RequirementTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06039_OM_Job_Manager_Linked_Safety_RequirementTableAdapter();
            this.sp06040_OM_Job_Manager_Linked_CommentsTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06040_OM_Job_Manager_Linked_CommentsTableAdapter();
            this.sp06041_OM_Job_Manager_Linked_Extra_InfoTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06041_OM_Job_Manager_Linked_Extra_InfoTableAdapter();
            this.sp06279_OM_Labour_Time_On_Off_SiteTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06279_OM_Labour_Time_On_Off_SiteTableAdapter();
            this.sp06294_OM_Job_Manager_Construction_Manager_FilterTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06294_OM_Job_Manager_Construction_Manager_FilterTableAdapter();
            this.sp06308_OM_Visit_TypesTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06308_OM_Visit_TypesTableAdapter();
            this.imageCollection2 = new DevExpress.Utils.ImageCollection(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlJob)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06031OMJobManagerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Job)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewJob)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditLatLong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocumentsJob_OpenLink)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditIntegerDays)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEditAlert)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Summer_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).BeginInit();
            this.popupContainerControlDateRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlRecordTypeFilter)).BeginInit();
            this.popupContainerControlRecordTypeFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06018OMJobManagerRecordTypesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlJobStatusFilter)).BeginInit();
            this.popupContainerControlJobStatusFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06025OMJobStatusesFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlShowTabPages)).BeginInit();
            this.popupContainerControlShowTabPages.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04022CoreDummyTabPageListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPageLabour.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlLabour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06033OMJobManagerLinkedlabourBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLabour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditViewRecord)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer5)).BeginInit();
            this.gridSplitContainer5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlLabourTiming)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06279OMLabourTimeOnOffSiteBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLabourTiming)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML3)).BeginInit();
            this.xtraTabPageEquipment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlMaterialTypeFilter)).BeginInit();
            this.popupContainerControlMaterialTypeFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06030OMMaterialFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlJobTypeFilter)).BeginInit();
            this.popupContainerControlJobTypeFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06026OMJobTypeFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlEquipmentTypeFilter)).BeginInit();
            this.popupContainerControlEquipmentTypeFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06029OMEquipmentTypeFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlJobSubTypeFilter)).BeginInit();
            this.popupContainerControlJobSubTypeFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06027OMJobSubTypeFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlEquipment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06034OMJobManagerLinkedEquipmentBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEquipment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditEquipmentPictures)).BeginInit();
            this.xtraTabPageMaterials.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlLabourFilter)).BeginInit();
            this.popupContainerControlLabourFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06028OMLabourFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlVisitTypeFilter)).BeginInit();
            this.popupContainerControlVisitTypeFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06308OMVisitTypesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Visit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlMaterial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06035OMJobManagerLinkedMaterialsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMaterial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric2DP4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditMaterialPictures)).BeginInit();
            this.xtraTabPageExtraInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExtraInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06041OMJobManagerLinkedExtraInfoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExtraInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit14)).BeginInit();
            this.xtraTabPageSpraying.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSpraying)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06038OMJobManagerLinkedSprayingBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSpraying)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumericRatio)).BeginInit();
            this.xtraTabPageWaste.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWaste)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06036OMJobManagerLinkedWasteBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWaste)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric2DP5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditWasteDocument)).BeginInit();
            this.xtraTabPagePictures.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).BeginInit();
            this.gridSplitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06037OMJobManagerLinkedPicturesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML9)).BeginInit();
            this.xtraTabPageComments.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06040OMJobManagerLinkedCommentsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit10)).BeginInit();
            this.xtraTabPageHealthAndSafety.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlHealthAndSafety)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06039OMJobManagerLinkedSafetyRequirementBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewHealthAndSafety)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit11)).BeginInit();
            this.xtraTabPageCRM.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer4)).BeginInit();
            this.gridSplitContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCRM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp05089CRMContactsLinkedToRecordBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCRM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlConstructionManagerFilter)).BeginInit();
            this.popupContainerControlConstructionManagerFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06294OMJobManagerConstructionManagerFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlKAMFilter)).BeginInit();
            this.popupContainerControlKAMFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06019OMJobManagerKAMsFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlCMFilter)).BeginInit();
            this.popupContainerControlCMFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06020OMJobManagerCMsFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).BeginInit();
            this.xtraTabPagePurchaseOrders.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Reports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditDateRange.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditStaff.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditTeam.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditVisitTypeFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditConstructionManagerFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditShowTabPages.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditMaterialTypeFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditEquipmentTypeFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditLabourFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditJobSubTypeFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditJobTypeFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditRecordTypeFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditSiteFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditClientFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditCMFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditJobStatusFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditColourCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditKAMFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLabourFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClearAllFilters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmOptimisation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.hideContainerLeft.SuspendLayout();
            this.dockPanelFilters.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection2)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1080, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 547);
            this.barDockControlBottom.Size = new System.Drawing.Size(1080, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 547);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1080, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 547);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.standaloneBarDockControl1);
            this.barManager1.DockManager = this.dockManager1;
            this.barManager1.Images = this.imageCollection2;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiSendJobs2,
            this.bbiReassignJobs,
            this.bbiViewSiteOnMap,
            this.bbiAuthorise,
            this.bbiPay,
            this.bsiSendJobs,
            this.bbiSelectJobsReadyToSend,
            this.bbiSendJobs,
            this.bsiViewOnMap,
            this.bbiViewJobOnMap,
            this.bbiAddWizard,
            this.bbiManuallyComplete,
            this.bsiManuallyComplete,
            this.bbiSelectJobsToComplete,
            this.bbiManuallyCompleteJobs,
            this.bbiOptimization,
            this.bbiGetDataToRouteOptimize,
            this.bbiSendDataToRouteOptimizer,
            this.bbiGetDataFromRouteOptimizer,
            this.bciFilterSelected,
            this.bbiRefresh,
            this.bsiSelectedCount});
            this.barManager1.MaxItemId = 59;
            this.barManager1.HighlightedLinkChanged += new DevExpress.XtraBars.HighlightedLinkChangedEventHandler(this.barManager1_HighlightedLinkChanged);
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colDisabled
            // 
            this.colDisabled.Caption = "Disabled";
            this.colDisabled.ColumnEdit = this.repositoryItemCheckEdit6;
            this.colDisabled.FieldName = "Disabled";
            this.colDisabled.Name = "colDisabled";
            this.colDisabled.OptionsColumn.AllowEdit = false;
            this.colDisabled.OptionsColumn.AllowFocus = false;
            this.colDisabled.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemCheckEdit6
            // 
            this.repositoryItemCheckEdit6.AutoHeight = false;
            this.repositoryItemCheckEdit6.Caption = "Check";
            this.repositoryItemCheckEdit6.Name = "repositoryItemCheckEdit6";
            this.repositoryItemCheckEdit6.ValueChecked = 1;
            this.repositoryItemCheckEdit6.ValueUnchecked = 0;
            // 
            // colDisabled2
            // 
            this.colDisabled2.Caption = "Disabled";
            this.colDisabled2.ColumnEdit = this.repositoryItemCheckEdit8;
            this.colDisabled2.FieldName = "Disabled";
            this.colDisabled2.Name = "colDisabled2";
            this.colDisabled2.OptionsColumn.AllowEdit = false;
            this.colDisabled2.OptionsColumn.AllowFocus = false;
            this.colDisabled2.OptionsColumn.ReadOnly = true;
            this.colDisabled2.Width = 61;
            // 
            // repositoryItemCheckEdit8
            // 
            this.repositoryItemCheckEdit8.AutoHeight = false;
            this.repositoryItemCheckEdit8.Caption = "Check";
            this.repositoryItemCheckEdit8.Name = "repositoryItemCheckEdit8";
            this.repositoryItemCheckEdit8.ValueChecked = 1;
            this.repositoryItemCheckEdit8.ValueUnchecked = 0;
            // 
            // colActive
            // 
            this.colActive.Caption = "Active";
            this.colActive.ColumnEdit = this.repositoryItemCheckEdit17;
            this.colActive.FieldName = "Active";
            this.colActive.Name = "colActive";
            this.colActive.OptionsColumn.AllowEdit = false;
            this.colActive.OptionsColumn.AllowFocus = false;
            this.colActive.OptionsColumn.ReadOnly = true;
            this.colActive.Visible = true;
            this.colActive.VisibleIndex = 1;
            this.colActive.Width = 51;
            // 
            // repositoryItemCheckEdit17
            // 
            this.repositoryItemCheckEdit17.AutoHeight = false;
            this.repositoryItemCheckEdit17.Caption = "Check";
            this.repositoryItemCheckEdit17.Name = "repositoryItemCheckEdit17";
            this.repositoryItemCheckEdit17.ValueChecked = 1;
            this.repositoryItemCheckEdit17.ValueUnchecked = 0;
            // 
            // colJobStatusID
            // 
            this.colJobStatusID.Caption = "Job Status ID";
            this.colJobStatusID.FieldName = "JobStatusID";
            this.colJobStatusID.Name = "colJobStatusID";
            this.colJobStatusID.OptionsColumn.AllowEdit = false;
            this.colJobStatusID.OptionsColumn.AllowFocus = false;
            this.colJobStatusID.OptionsColumn.ReadOnly = true;
            this.colJobStatusID.Width = 86;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridControlJob
            // 
            this.gridControlJob.DataSource = this.sp06031OMJobManagerBindingSource;
            this.gridControlJob.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlJob.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlJob.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlJob.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlJob.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlJob.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlJob.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlJob.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlJob.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlJob.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlJob.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlJob.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlJob.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selcted Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 8, true, true, "Linked Documents", "linked_document"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 14, true, true, "Sync Job Start\\End to Visit Start\\End", "sync_time")});
            this.gridControlJob.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlJob_EmbeddedNavigator_ButtonClick);
            this.gridControlJob.Location = new System.Drawing.Point(0, 0);
            this.gridControlJob.MainView = this.gridViewJob;
            this.gridControlJob.MenuManager = this.barManager1;
            this.gridControlJob.Name = "gridControlJob";
            this.gridControlJob.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemHyperLinkEdit1,
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemTextEdit2DP,
            this.repositoryItemTextEditCurrency,
            this.repositoryItemTextEditPercentage,
            this.repositoryItemTextEditLatLong,
            this.repositoryItemTextEditInteger,
            this.repositoryItemHyperLinkEditLinkedDocumentsJob_OpenLink,
            this.repositoryItemTextEditIntegerDays,
            this.repositoryItemPictureEditAlert,
            this.repositoryItemTextEditHTML});
            this.gridControlJob.Size = new System.Drawing.Size(1053, 283);
            this.gridControlJob.TabIndex = 4;
            this.gridControlJob.ToolTipController = this.toolTipController1;
            this.gridControlJob.UseEmbeddedNavigator = true;
            this.gridControlJob.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewJob});
            // 
            // sp06031OMJobManagerBindingSource
            // 
            this.sp06031OMJobManagerBindingSource.DataMember = "sp06031_OM_Job_Manager";
            this.sp06031OMJobManagerBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // dataSet_OM_Job
            // 
            this.dataSet_OM_Job.DataSetName = "DataSet_OM_Job";
            this.dataSet_OM_Job.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16");
            this.imageCollection1.Images.SetKeyName(3, "arrow_up_blue_round.png");
            this.imageCollection1.Images.SetKeyName(4, "arrow_down_blue_round.png");
            this.imageCollection1.Images.SetKeyName(5, "Sort_16x16.png");
            this.imageCollection1.Images.SetKeyName(6, "Scheduler_16x16.png");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Preview_16x16, "Preview_16x16", typeof(global::WoodPlan5.Properties.Resources), 7);
            this.imageCollection1.Images.SetKeyName(7, "Preview_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.linked_documents_16_16, "linked_documents_16_16", typeof(global::WoodPlan5.Properties.Resources), 8);
            this.imageCollection1.Images.SetKeyName(8, "linked_documents_16_16");
            this.imageCollection1.InsertGalleryImage("refresh2_16x16.png", "images/actions/refresh2_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/refresh2_16x16.png"), 9);
            this.imageCollection1.Images.SetKeyName(9, "refresh2_16x16.png");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention2_16, "attention2_16", typeof(global::WoodPlan5.Properties.Resources), 10);
            this.imageCollection1.Images.SetKeyName(10, "attention2_16");
            this.imageCollection1.InsertGalleryImage("clearfilter_16x16.png", "images/filter/clearfilter_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/filter/clearfilter_16x16.png"), 11);
            this.imageCollection1.Images.SetKeyName(11, "clearfilter_16x16.png");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.refresh_16x16, "refresh_16x16", typeof(global::WoodPlan5.Properties.Resources), 12);
            this.imageCollection1.Images.SetKeyName(12, "refresh_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.BlockAdd_16x16, "BlockAdd_16x16", typeof(global::WoodPlan5.Properties.Resources), 13);
            this.imageCollection1.Images.SetKeyName(13, "BlockAdd_16x16");
            this.imageCollection1.InsertGalleryImage("showworktimeonly_16x16.png", "images/scheduling/showworktimeonly_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/scheduling/showworktimeonly_16x16.png"), 14);
            this.imageCollection1.Images.SetKeyName(14, "showworktimeonly_16x16.png");
            this.imageCollection1.InsertGalleryImage("picturebox_16x16.png", "images/toolbox%20items/picturebox_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/toolbox%20items/picturebox_16x16.png"), 15);
            this.imageCollection1.Images.SetKeyName(15, "picturebox_16x16.png");
            // 
            // gridViewJob
            // 
            this.gridViewJob.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colJobID,
            this.colVisitID,
            this.colVisitNumber,
            this.colJobStatusID,
            this.colJobSubTypeID,
            this.colCreatedByStaffID1,
            this.colReactive,
            this.colJobNoLongerRequired,
            this.colRequiresAccessPermit,
            this.colAccessPermitID,
            this.colClientPONumber,
            this.colClientPOID,
            this.colFinanceSystemPONumber,
            this.colExpectedStartDate,
            this.colExpectedEndDate,
            this.colExpectedDurationUnits,
            this.colExpectedDurationUnitsDescriptorID,
            this.colScheduleSentDate,
            this.colActualStartDate,
            this.colActualEndDate,
            this.colActualDurationUnits,
            this.colActualDurationUnitsDescriptionID,
            this.colCostTotalMaterialExVAT,
            this.colCostTotalMaterialVAT,
            this.colCostTotalMaterialCost,
            this.colSellTotalMaterialExVAT,
            this.colSellTotalMaterialVAT,
            this.colSellTotalMaterialCost,
            this.colCostTotalEquipmentExVAT,
            this.colCostTotalEquipmentVAT,
            this.colCostTotalEquipmentCost,
            this.colSellTotalEquipmentExVAT,
            this.colSellTotalEquipmentVAT,
            this.colSellTotalEquipmentCost,
            this.colCostTotalLabourExVAT,
            this.colCostTotalLabourVAT,
            this.colCostTotalLabourCost,
            this.colSellTotalLabourExVAT,
            this.colSellTotalLabourVAT,
            this.colSellTotalLabourCost,
            this.colCostTotalCostExVAT,
            this.colCostTotalCostVAT,
            this.colCostTotalCost,
            this.colSellTotalCostExVAT,
            this.colSellTotalCostVAT,
            this.colSellTotalCost,
            this.colClientInvoiceID,
            this.colDateClientInvoiced,
            this.colSelfBillingInvoiceID,
            this.colSelfBillingInvoiceReceivedDate,
            this.colSelfBillingInvoicePaidDate,
            this.colSelfBillingInvoiceAmountPaid,
            this.colDoNotPayContractor,
            this.colDoNotInvoiceClient,
            this.colRemarks2,
            this.colRouteOrder,
            this.colStartLatitude,
            this.colStartLongitude,
            this.colFinishLatitude,
            this.colFinishLongitude,
            this.colJobStatusDescription,
            this.colSiteName,
            this.colClientName,
            this.colCreatedByStaffName1,
            this.colExpectedDurationUnitsDescriptor,
            this.colActualDurationUnitsDescription,
            this.colJobTypeID1,
            this.colJobSubTypeDescription,
            this.colJobTypeDescription1,
            this.colLinkedJobCount,
            this.colRecordType,
            this.colSelected,
            this.colFullDescription1,
            this.colTenderID,
            this.colTenderJobID,
            this.colMinimumDaysFromLastVisit,
            this.colMaximumDaysFromLastVisit,
            this.colDaysLeeway,
            this.colBillingCentreCodeID,
            this.colLinkedDocumentCount,
            this.colContractDescription,
            this.colCostTotalEquipmentVATRate,
            this.colCostTotalLabourVATRate,
            this.colCostTotalMaterialVATRate,
            this.colSellTotalMaterialVATRate,
            this.colSellTotalEquipmentVATRate,
            this.colSellTotalLabourVATRate,
            this.colClientContractID,
            this.colClientID2,
            this.colSiteContractID,
            this.colSiteID2,
            this.colImagesFolderOM,
            this.colRework,
            this.colReworkOriginalJobID,
            this.colLocationX,
            this.colLocationY,
            this.colSitePostcode,
            this.colLinkedLabourCount,
            this.colDaysUntilDue,
            this.colCancelledReasonID,
            this.colCancelledReason,
            this.colManuallyCompleted,
            this.Alert,
            this.colLinkedPictureCount,
            this.colNoWorkRequired,
            this.colFriendlyVisitNumber,
            this.colClientPO,
            this.colVisitType,
            this.colVisitTypeID,
            this.colSiteCode,
            this.colVisitCostCalculationLevelID,
            this.colVisitSellCalculationLevelID,
            this.colVisitCostCalculationLevelDescription,
            this.colVisitSellCalculationLevelDescription,
            this.colCreatedFromVisitTemplate,
            this.colCreatedFromVisitTemplateID,
            this.colDisplayOrder,
            this.colMandatory,
            this.colSuspendedReason,
            this.colSuspendedRemarks});
            this.gridViewJob.GridControl = this.gridControlJob;
            this.gridViewJob.Name = "gridViewJob";
            this.gridViewJob.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewJob.OptionsFind.AlwaysVisible = true;
            this.gridViewJob.OptionsFind.FindDelay = 2000;
            this.gridViewJob.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewJob.OptionsLayout.StoreAppearance = true;
            this.gridViewJob.OptionsLayout.StoreFormatRules = true;
            this.gridViewJob.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewJob.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewJob.OptionsSelection.MultiSelect = true;
            this.gridViewJob.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewJob.OptionsView.ColumnAutoWidth = false;
            this.gridViewJob.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewJob.OptionsView.ShowGroupPanel = false;
            this.gridViewJob.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colVisitNumber, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDisplayOrder, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colJobTypeDescription1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colJobSubTypeDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewJob.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridViewJob_CustomDrawCell);
            this.gridViewJob.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewJob_CustomRowCellEdit);
            this.gridViewJob.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridViewJob_PopupMenuShowing);
            this.gridViewJob.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewJob.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewJob.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewJob_ShowingEditor);
            this.gridViewJob.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewJob.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewJob.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.gridViewJob_CustomUnboundColumnData);
            this.gridViewJob.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(this.gridViewJob_CustomColumnDisplayText);
            this.gridViewJob.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewJob.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewJob_MouseUp);
            this.gridViewJob.DoubleClick += new System.EventHandler(this.gridViewJob_DoubleClick);
            this.gridViewJob.GotFocus += new System.EventHandler(this.gridViewJob_GotFocus);
            // 
            // colJobID
            // 
            this.colJobID.Caption = "Job ID";
            this.colJobID.FieldName = "JobID";
            this.colJobID.Name = "colJobID";
            this.colJobID.OptionsColumn.AllowEdit = false;
            this.colJobID.OptionsColumn.AllowFocus = false;
            this.colJobID.OptionsColumn.ReadOnly = true;
            // 
            // colVisitID
            // 
            this.colVisitID.Caption = "Visit ID";
            this.colVisitID.FieldName = "VisitID";
            this.colVisitID.Name = "colVisitID";
            this.colVisitID.OptionsColumn.AllowEdit = false;
            this.colVisitID.OptionsColumn.AllowFocus = false;
            this.colVisitID.OptionsColumn.ReadOnly = true;
            this.colVisitID.Width = 54;
            // 
            // colVisitNumber
            // 
            this.colVisitNumber.Caption = "Visit #";
            this.colVisitNumber.ColumnEdit = this.repositoryItemTextEditInteger;
            this.colVisitNumber.FieldName = "VisitNumber";
            this.colVisitNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colVisitNumber.Name = "colVisitNumber";
            this.colVisitNumber.OptionsColumn.AllowEdit = false;
            this.colVisitNumber.OptionsColumn.AllowFocus = false;
            this.colVisitNumber.OptionsColumn.ReadOnly = true;
            this.colVisitNumber.Visible = true;
            this.colVisitNumber.VisibleIndex = 4;
            this.colVisitNumber.Width = 64;
            // 
            // repositoryItemTextEditInteger
            // 
            this.repositoryItemTextEditInteger.AutoHeight = false;
            this.repositoryItemTextEditInteger.Mask.EditMask = "f0";
            this.repositoryItemTextEditInteger.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditInteger.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditInteger.Name = "repositoryItemTextEditInteger";
            // 
            // colJobSubTypeID
            // 
            this.colJobSubTypeID.Caption = "Job Sub-Type ID";
            this.colJobSubTypeID.FieldName = "JobSubTypeID";
            this.colJobSubTypeID.Name = "colJobSubTypeID";
            this.colJobSubTypeID.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeID.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeID.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeID.Width = 102;
            // 
            // colCreatedByStaffID1
            // 
            this.colCreatedByStaffID1.Caption = "Created By Staff ID";
            this.colCreatedByStaffID1.FieldName = "CreatedByStaffID";
            this.colCreatedByStaffID1.Name = "colCreatedByStaffID1";
            this.colCreatedByStaffID1.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffID1.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffID1.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffID1.Width = 116;
            // 
            // colReactive
            // 
            this.colReactive.Caption = "Reactive";
            this.colReactive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colReactive.FieldName = "Reactive";
            this.colReactive.Name = "colReactive";
            this.colReactive.OptionsColumn.AllowEdit = false;
            this.colReactive.OptionsColumn.AllowFocus = false;
            this.colReactive.OptionsColumn.ReadOnly = true;
            this.colReactive.Visible = true;
            this.colReactive.VisibleIndex = 16;
            this.colReactive.Width = 63;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colJobNoLongerRequired
            // 
            this.colJobNoLongerRequired.Caption = "No Longer Required";
            this.colJobNoLongerRequired.FieldName = "JobNoLongerRequired";
            this.colJobNoLongerRequired.Name = "colJobNoLongerRequired";
            this.colJobNoLongerRequired.OptionsColumn.AllowEdit = false;
            this.colJobNoLongerRequired.OptionsColumn.AllowFocus = false;
            this.colJobNoLongerRequired.OptionsColumn.ReadOnly = true;
            this.colJobNoLongerRequired.Visible = true;
            this.colJobNoLongerRequired.VisibleIndex = 27;
            this.colJobNoLongerRequired.Width = 116;
            // 
            // colRequiresAccessPermit
            // 
            this.colRequiresAccessPermit.Caption = "Access Permit Required";
            this.colRequiresAccessPermit.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colRequiresAccessPermit.FieldName = "RequiresAccessPermit";
            this.colRequiresAccessPermit.Name = "colRequiresAccessPermit";
            this.colRequiresAccessPermit.OptionsColumn.AllowEdit = false;
            this.colRequiresAccessPermit.OptionsColumn.AllowFocus = false;
            this.colRequiresAccessPermit.OptionsColumn.ReadOnly = true;
            this.colRequiresAccessPermit.Visible = true;
            this.colRequiresAccessPermit.VisibleIndex = 19;
            this.colRequiresAccessPermit.Width = 133;
            // 
            // colAccessPermitID
            // 
            this.colAccessPermitID.Caption = "Access Permit ID";
            this.colAccessPermitID.FieldName = "AccessPermitID";
            this.colAccessPermitID.Name = "colAccessPermitID";
            this.colAccessPermitID.OptionsColumn.AllowEdit = false;
            this.colAccessPermitID.OptionsColumn.AllowFocus = false;
            this.colAccessPermitID.OptionsColumn.ReadOnly = true;
            this.colAccessPermitID.Width = 101;
            // 
            // colClientPONumber
            // 
            this.colClientPONumber.Caption = "Client PO # [Manual]";
            this.colClientPONumber.FieldName = "ClientPONumber";
            this.colClientPONumber.Name = "colClientPONumber";
            this.colClientPONumber.OptionsColumn.AllowEdit = false;
            this.colClientPONumber.OptionsColumn.AllowFocus = false;
            this.colClientPONumber.OptionsColumn.ReadOnly = true;
            this.colClientPONumber.Visible = true;
            this.colClientPONumber.VisibleIndex = 60;
            this.colClientPONumber.Width = 119;
            // 
            // colClientPOID
            // 
            this.colClientPOID.Caption = "Client PO ID";
            this.colClientPOID.FieldName = "ClientPOID";
            this.colClientPOID.Name = "colClientPOID";
            this.colClientPOID.OptionsColumn.AllowEdit = false;
            this.colClientPOID.OptionsColumn.AllowFocus = false;
            this.colClientPOID.OptionsColumn.ReadOnly = true;
            this.colClientPOID.Width = 79;
            // 
            // colFinanceSystemPONumber
            // 
            this.colFinanceSystemPONumber.Caption = "Finance System PO #";
            this.colFinanceSystemPONumber.FieldName = "FinanceSystemPONumber";
            this.colFinanceSystemPONumber.Name = "colFinanceSystemPONumber";
            this.colFinanceSystemPONumber.OptionsColumn.AllowEdit = false;
            this.colFinanceSystemPONumber.OptionsColumn.AllowFocus = false;
            this.colFinanceSystemPONumber.OptionsColumn.ReadOnly = true;
            this.colFinanceSystemPONumber.Visible = true;
            this.colFinanceSystemPONumber.VisibleIndex = 62;
            this.colFinanceSystemPONumber.Width = 124;
            // 
            // colExpectedStartDate
            // 
            this.colExpectedStartDate.Caption = "Expected Start";
            this.colExpectedStartDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colExpectedStartDate.FieldName = "ExpectedStartDate";
            this.colExpectedStartDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colExpectedStartDate.Name = "colExpectedStartDate";
            this.colExpectedStartDate.OptionsColumn.AllowEdit = false;
            this.colExpectedStartDate.OptionsColumn.AllowFocus = false;
            this.colExpectedStartDate.OptionsColumn.ReadOnly = true;
            this.colExpectedStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colExpectedStartDate.Visible = true;
            this.colExpectedStartDate.VisibleIndex = 12;
            this.colExpectedStartDate.Width = 106;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colExpectedEndDate
            // 
            this.colExpectedEndDate.Caption = "Expected End";
            this.colExpectedEndDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colExpectedEndDate.FieldName = "ExpectedEndDate";
            this.colExpectedEndDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colExpectedEndDate.Name = "colExpectedEndDate";
            this.colExpectedEndDate.OptionsColumn.AllowEdit = false;
            this.colExpectedEndDate.OptionsColumn.AllowFocus = false;
            this.colExpectedEndDate.OptionsColumn.ReadOnly = true;
            this.colExpectedEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colExpectedEndDate.Visible = true;
            this.colExpectedEndDate.VisibleIndex = 13;
            this.colExpectedEndDate.Width = 100;
            // 
            // colExpectedDurationUnits
            // 
            this.colExpectedDurationUnits.Caption = "Expected Duration";
            this.colExpectedDurationUnits.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colExpectedDurationUnits.FieldName = "ExpectedDurationUnits";
            this.colExpectedDurationUnits.Name = "colExpectedDurationUnits";
            this.colExpectedDurationUnits.OptionsColumn.AllowEdit = false;
            this.colExpectedDurationUnits.OptionsColumn.AllowFocus = false;
            this.colExpectedDurationUnits.OptionsColumn.ReadOnly = true;
            this.colExpectedDurationUnits.Visible = true;
            this.colExpectedDurationUnits.VisibleIndex = 14;
            this.colExpectedDurationUnits.Width = 110;
            // 
            // repositoryItemTextEdit2DP
            // 
            this.repositoryItemTextEdit2DP.AutoHeight = false;
            this.repositoryItemTextEdit2DP.Mask.EditMask = "f";
            this.repositoryItemTextEdit2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP.Name = "repositoryItemTextEdit2DP";
            // 
            // colExpectedDurationUnitsDescriptorID
            // 
            this.colExpectedDurationUnitsDescriptorID.Caption = "Expected Duration Unit Descriptor ID";
            this.colExpectedDurationUnitsDescriptorID.FieldName = "ExpectedDurationUnitsDescriptorID";
            this.colExpectedDurationUnitsDescriptorID.Name = "colExpectedDurationUnitsDescriptorID";
            this.colExpectedDurationUnitsDescriptorID.OptionsColumn.AllowEdit = false;
            this.colExpectedDurationUnitsDescriptorID.OptionsColumn.AllowFocus = false;
            this.colExpectedDurationUnitsDescriptorID.OptionsColumn.ReadOnly = true;
            this.colExpectedDurationUnitsDescriptorID.Width = 198;
            // 
            // colScheduleSentDate
            // 
            this.colScheduleSentDate.Caption = "Schedule Sent";
            this.colScheduleSentDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colScheduleSentDate.FieldName = "ScheduleSentDate";
            this.colScheduleSentDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colScheduleSentDate.Name = "colScheduleSentDate";
            this.colScheduleSentDate.OptionsColumn.AllowEdit = false;
            this.colScheduleSentDate.OptionsColumn.AllowFocus = false;
            this.colScheduleSentDate.OptionsColumn.ReadOnly = true;
            this.colScheduleSentDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colScheduleSentDate.Visible = true;
            this.colScheduleSentDate.VisibleIndex = 22;
            this.colScheduleSentDate.Width = 106;
            // 
            // colActualStartDate
            // 
            this.colActualStartDate.Caption = "Actual Start";
            this.colActualStartDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colActualStartDate.FieldName = "ActualStartDate";
            this.colActualStartDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colActualStartDate.Name = "colActualStartDate";
            this.colActualStartDate.OptionsColumn.AllowEdit = false;
            this.colActualStartDate.OptionsColumn.AllowFocus = false;
            this.colActualStartDate.OptionsColumn.ReadOnly = true;
            this.colActualStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colActualStartDate.Visible = true;
            this.colActualStartDate.VisibleIndex = 23;
            this.colActualStartDate.Width = 100;
            // 
            // colActualEndDate
            // 
            this.colActualEndDate.Caption = "Actual End";
            this.colActualEndDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colActualEndDate.FieldName = "ActualEndDate";
            this.colActualEndDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colActualEndDate.Name = "colActualEndDate";
            this.colActualEndDate.OptionsColumn.AllowEdit = false;
            this.colActualEndDate.OptionsColumn.AllowFocus = false;
            this.colActualEndDate.OptionsColumn.ReadOnly = true;
            this.colActualEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colActualEndDate.Visible = true;
            this.colActualEndDate.VisibleIndex = 24;
            this.colActualEndDate.Width = 100;
            // 
            // colActualDurationUnits
            // 
            this.colActualDurationUnits.Caption = "Actual Duration";
            this.colActualDurationUnits.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colActualDurationUnits.FieldName = "ActualDurationUnits";
            this.colActualDurationUnits.Name = "colActualDurationUnits";
            this.colActualDurationUnits.OptionsColumn.AllowEdit = false;
            this.colActualDurationUnits.OptionsColumn.AllowFocus = false;
            this.colActualDurationUnits.OptionsColumn.ReadOnly = true;
            this.colActualDurationUnits.Visible = true;
            this.colActualDurationUnits.VisibleIndex = 25;
            this.colActualDurationUnits.Width = 95;
            // 
            // colActualDurationUnitsDescriptionID
            // 
            this.colActualDurationUnitsDescriptionID.Caption = "Actual Duration Unit Descriptor ID";
            this.colActualDurationUnitsDescriptionID.FieldName = "ActualDurationUnitsDescriptionID";
            this.colActualDurationUnitsDescriptionID.Name = "colActualDurationUnitsDescriptionID";
            this.colActualDurationUnitsDescriptionID.OptionsColumn.AllowEdit = false;
            this.colActualDurationUnitsDescriptionID.OptionsColumn.AllowFocus = false;
            this.colActualDurationUnitsDescriptionID.OptionsColumn.ReadOnly = true;
            this.colActualDurationUnitsDescriptionID.Width = 183;
            // 
            // colCostTotalMaterialExVAT
            // 
            this.colCostTotalMaterialExVAT.Caption = "Material Cost Ex VAT";
            this.colCostTotalMaterialExVAT.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCostTotalMaterialExVAT.FieldName = "CostTotalMaterialExVAT";
            this.colCostTotalMaterialExVAT.Name = "colCostTotalMaterialExVAT";
            this.colCostTotalMaterialExVAT.OptionsColumn.AllowEdit = false;
            this.colCostTotalMaterialExVAT.OptionsColumn.AllowFocus = false;
            this.colCostTotalMaterialExVAT.OptionsColumn.ReadOnly = true;
            this.colCostTotalMaterialExVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostTotalMaterialExVAT", "{0:c}")});
            this.colCostTotalMaterialExVAT.Visible = true;
            this.colCostTotalMaterialExVAT.VisibleIndex = 29;
            this.colCostTotalMaterialExVAT.Width = 121;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // colCostTotalMaterialVAT
            // 
            this.colCostTotalMaterialVAT.Caption = "Material Cost VAT";
            this.colCostTotalMaterialVAT.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCostTotalMaterialVAT.FieldName = "CostTotalMaterialVAT";
            this.colCostTotalMaterialVAT.Name = "colCostTotalMaterialVAT";
            this.colCostTotalMaterialVAT.OptionsColumn.AllowEdit = false;
            this.colCostTotalMaterialVAT.OptionsColumn.AllowFocus = false;
            this.colCostTotalMaterialVAT.OptionsColumn.ReadOnly = true;
            this.colCostTotalMaterialVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostTotalMaterialVAT", "{0:c}")});
            this.colCostTotalMaterialVAT.Visible = true;
            this.colCostTotalMaterialVAT.VisibleIndex = 31;
            this.colCostTotalMaterialVAT.Width = 106;
            // 
            // colCostTotalMaterialCost
            // 
            this.colCostTotalMaterialCost.Caption = "Material Cost Total";
            this.colCostTotalMaterialCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCostTotalMaterialCost.FieldName = "CostTotalMaterialCost";
            this.colCostTotalMaterialCost.Name = "colCostTotalMaterialCost";
            this.colCostTotalMaterialCost.OptionsColumn.AllowEdit = false;
            this.colCostTotalMaterialCost.OptionsColumn.AllowFocus = false;
            this.colCostTotalMaterialCost.OptionsColumn.ReadOnly = true;
            this.colCostTotalMaterialCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostTotalMaterialCost", "{0:c}")});
            this.colCostTotalMaterialCost.Visible = true;
            this.colCostTotalMaterialCost.VisibleIndex = 32;
            this.colCostTotalMaterialCost.Width = 111;
            // 
            // colSellTotalMaterialExVAT
            // 
            this.colSellTotalMaterialExVAT.Caption = "Material Sell Ex VAT";
            this.colSellTotalMaterialExVAT.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSellTotalMaterialExVAT.FieldName = "SellTotalMaterialExVAT";
            this.colSellTotalMaterialExVAT.Name = "colSellTotalMaterialExVAT";
            this.colSellTotalMaterialExVAT.OptionsColumn.AllowEdit = false;
            this.colSellTotalMaterialExVAT.OptionsColumn.AllowFocus = false;
            this.colSellTotalMaterialExVAT.OptionsColumn.ReadOnly = true;
            this.colSellTotalMaterialExVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellTotalMaterialExVAT", "{0:c}")});
            this.colSellTotalMaterialExVAT.Visible = true;
            this.colSellTotalMaterialExVAT.VisibleIndex = 33;
            this.colSellTotalMaterialExVAT.Width = 115;
            // 
            // colSellTotalMaterialVAT
            // 
            this.colSellTotalMaterialVAT.Caption = "Material Sell VAT";
            this.colSellTotalMaterialVAT.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSellTotalMaterialVAT.FieldName = "SellTotalMaterialVAT";
            this.colSellTotalMaterialVAT.Name = "colSellTotalMaterialVAT";
            this.colSellTotalMaterialVAT.OptionsColumn.AllowEdit = false;
            this.colSellTotalMaterialVAT.OptionsColumn.AllowFocus = false;
            this.colSellTotalMaterialVAT.OptionsColumn.ReadOnly = true;
            this.colSellTotalMaterialVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellTotalMaterialVAT", "{0:c}")});
            this.colSellTotalMaterialVAT.Visible = true;
            this.colSellTotalMaterialVAT.VisibleIndex = 35;
            this.colSellTotalMaterialVAT.Width = 100;
            // 
            // colSellTotalMaterialCost
            // 
            this.colSellTotalMaterialCost.Caption = "Material Sell Total";
            this.colSellTotalMaterialCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSellTotalMaterialCost.FieldName = "SellTotalMaterialCost";
            this.colSellTotalMaterialCost.Name = "colSellTotalMaterialCost";
            this.colSellTotalMaterialCost.OptionsColumn.AllowEdit = false;
            this.colSellTotalMaterialCost.OptionsColumn.AllowFocus = false;
            this.colSellTotalMaterialCost.OptionsColumn.ReadOnly = true;
            this.colSellTotalMaterialCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellTotalMaterialCost", "{0:c}")});
            this.colSellTotalMaterialCost.Visible = true;
            this.colSellTotalMaterialCost.VisibleIndex = 36;
            this.colSellTotalMaterialCost.Width = 105;
            // 
            // colCostTotalEquipmentExVAT
            // 
            this.colCostTotalEquipmentExVAT.Caption = "Equipment Cost Ex VAT";
            this.colCostTotalEquipmentExVAT.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCostTotalEquipmentExVAT.FieldName = "CostTotalEquipmentExVAT";
            this.colCostTotalEquipmentExVAT.Name = "colCostTotalEquipmentExVAT";
            this.colCostTotalEquipmentExVAT.OptionsColumn.AllowEdit = false;
            this.colCostTotalEquipmentExVAT.OptionsColumn.AllowFocus = false;
            this.colCostTotalEquipmentExVAT.OptionsColumn.ReadOnly = true;
            this.colCostTotalEquipmentExVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostTotalEquipmentExVAT", "{0:c}")});
            this.colCostTotalEquipmentExVAT.Visible = true;
            this.colCostTotalEquipmentExVAT.VisibleIndex = 37;
            this.colCostTotalEquipmentExVAT.Width = 133;
            // 
            // colCostTotalEquipmentVAT
            // 
            this.colCostTotalEquipmentVAT.Caption = "Equipment Cost VAT";
            this.colCostTotalEquipmentVAT.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCostTotalEquipmentVAT.FieldName = "CostTotalEquipmentVAT";
            this.colCostTotalEquipmentVAT.Name = "colCostTotalEquipmentVAT";
            this.colCostTotalEquipmentVAT.OptionsColumn.AllowEdit = false;
            this.colCostTotalEquipmentVAT.OptionsColumn.AllowFocus = false;
            this.colCostTotalEquipmentVAT.OptionsColumn.ReadOnly = true;
            this.colCostTotalEquipmentVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostTotalEquipmentVAT", "{0:c}")});
            this.colCostTotalEquipmentVAT.Visible = true;
            this.colCostTotalEquipmentVAT.VisibleIndex = 39;
            this.colCostTotalEquipmentVAT.Width = 118;
            // 
            // colCostTotalEquipmentCost
            // 
            this.colCostTotalEquipmentCost.Caption = "Equipment Cost Total";
            this.colCostTotalEquipmentCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCostTotalEquipmentCost.FieldName = "CostTotalEquipmentCost";
            this.colCostTotalEquipmentCost.Name = "colCostTotalEquipmentCost";
            this.colCostTotalEquipmentCost.OptionsColumn.AllowEdit = false;
            this.colCostTotalEquipmentCost.OptionsColumn.AllowFocus = false;
            this.colCostTotalEquipmentCost.OptionsColumn.ReadOnly = true;
            this.colCostTotalEquipmentCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostTotalEquipmentCost", "{0:c}")});
            this.colCostTotalEquipmentCost.Visible = true;
            this.colCostTotalEquipmentCost.VisibleIndex = 40;
            this.colCostTotalEquipmentCost.Width = 123;
            // 
            // colSellTotalEquipmentExVAT
            // 
            this.colSellTotalEquipmentExVAT.Caption = "Equipment Sell Ex VAT";
            this.colSellTotalEquipmentExVAT.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSellTotalEquipmentExVAT.FieldName = "SellTotalEquipmentExVAT";
            this.colSellTotalEquipmentExVAT.Name = "colSellTotalEquipmentExVAT";
            this.colSellTotalEquipmentExVAT.OptionsColumn.AllowEdit = false;
            this.colSellTotalEquipmentExVAT.OptionsColumn.AllowFocus = false;
            this.colSellTotalEquipmentExVAT.OptionsColumn.ReadOnly = true;
            this.colSellTotalEquipmentExVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellTotalEquipmentExVAT", "{0:c}")});
            this.colSellTotalEquipmentExVAT.Visible = true;
            this.colSellTotalEquipmentExVAT.VisibleIndex = 41;
            this.colSellTotalEquipmentExVAT.Width = 127;
            // 
            // colSellTotalEquipmentVAT
            // 
            this.colSellTotalEquipmentVAT.Caption = "Equipment Sell VAT";
            this.colSellTotalEquipmentVAT.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSellTotalEquipmentVAT.FieldName = "SellTotalEquipmentVAT";
            this.colSellTotalEquipmentVAT.Name = "colSellTotalEquipmentVAT";
            this.colSellTotalEquipmentVAT.OptionsColumn.AllowEdit = false;
            this.colSellTotalEquipmentVAT.OptionsColumn.AllowFocus = false;
            this.colSellTotalEquipmentVAT.OptionsColumn.ReadOnly = true;
            this.colSellTotalEquipmentVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellTotalEquipmentVAT", "{0:c}")});
            this.colSellTotalEquipmentVAT.Visible = true;
            this.colSellTotalEquipmentVAT.VisibleIndex = 43;
            this.colSellTotalEquipmentVAT.Width = 112;
            // 
            // colSellTotalEquipmentCost
            // 
            this.colSellTotalEquipmentCost.Caption = "Equipment Sell Total";
            this.colSellTotalEquipmentCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSellTotalEquipmentCost.FieldName = "SellTotalEquipmentCost";
            this.colSellTotalEquipmentCost.Name = "colSellTotalEquipmentCost";
            this.colSellTotalEquipmentCost.OptionsColumn.AllowEdit = false;
            this.colSellTotalEquipmentCost.OptionsColumn.AllowFocus = false;
            this.colSellTotalEquipmentCost.OptionsColumn.ReadOnly = true;
            this.colSellTotalEquipmentCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellTotalEquipmentCost", "{0:c}")});
            this.colSellTotalEquipmentCost.Visible = true;
            this.colSellTotalEquipmentCost.VisibleIndex = 44;
            this.colSellTotalEquipmentCost.Width = 117;
            // 
            // colCostTotalLabourExVAT
            // 
            this.colCostTotalLabourExVAT.Caption = "Labour Cost Ex VAT";
            this.colCostTotalLabourExVAT.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCostTotalLabourExVAT.FieldName = "CostTotalLabourExVAT";
            this.colCostTotalLabourExVAT.Name = "colCostTotalLabourExVAT";
            this.colCostTotalLabourExVAT.OptionsColumn.AllowEdit = false;
            this.colCostTotalLabourExVAT.OptionsColumn.AllowFocus = false;
            this.colCostTotalLabourExVAT.OptionsColumn.ReadOnly = true;
            this.colCostTotalLabourExVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostTotalLabourExVAT", "{0:c}")});
            this.colCostTotalLabourExVAT.Visible = true;
            this.colCostTotalLabourExVAT.VisibleIndex = 45;
            this.colCostTotalLabourExVAT.Width = 116;
            // 
            // colCostTotalLabourVAT
            // 
            this.colCostTotalLabourVAT.Caption = "Labour Cost VAT";
            this.colCostTotalLabourVAT.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCostTotalLabourVAT.FieldName = "CostTotalLabourVAT";
            this.colCostTotalLabourVAT.Name = "colCostTotalLabourVAT";
            this.colCostTotalLabourVAT.OptionsColumn.AllowEdit = false;
            this.colCostTotalLabourVAT.OptionsColumn.AllowFocus = false;
            this.colCostTotalLabourVAT.OptionsColumn.ReadOnly = true;
            this.colCostTotalLabourVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostTotalLabourVAT", "{0:c}")});
            this.colCostTotalLabourVAT.Visible = true;
            this.colCostTotalLabourVAT.VisibleIndex = 47;
            this.colCostTotalLabourVAT.Width = 101;
            // 
            // colCostTotalLabourCost
            // 
            this.colCostTotalLabourCost.Caption = "Labour Cost Total";
            this.colCostTotalLabourCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCostTotalLabourCost.FieldName = "CostTotalLabourCost";
            this.colCostTotalLabourCost.Name = "colCostTotalLabourCost";
            this.colCostTotalLabourCost.OptionsColumn.AllowEdit = false;
            this.colCostTotalLabourCost.OptionsColumn.AllowFocus = false;
            this.colCostTotalLabourCost.OptionsColumn.ReadOnly = true;
            this.colCostTotalLabourCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostTotalLabourCost", "{0:c}")});
            this.colCostTotalLabourCost.Visible = true;
            this.colCostTotalLabourCost.VisibleIndex = 48;
            this.colCostTotalLabourCost.Width = 106;
            // 
            // colSellTotalLabourExVAT
            // 
            this.colSellTotalLabourExVAT.Caption = "Labour Sell Ex VAT";
            this.colSellTotalLabourExVAT.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSellTotalLabourExVAT.FieldName = "SellTotalLabourExVAT";
            this.colSellTotalLabourExVAT.Name = "colSellTotalLabourExVAT";
            this.colSellTotalLabourExVAT.OptionsColumn.AllowEdit = false;
            this.colSellTotalLabourExVAT.OptionsColumn.AllowFocus = false;
            this.colSellTotalLabourExVAT.OptionsColumn.ReadOnly = true;
            this.colSellTotalLabourExVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellTotalLabourExVAT", "{0:c}")});
            this.colSellTotalLabourExVAT.Visible = true;
            this.colSellTotalLabourExVAT.VisibleIndex = 49;
            this.colSellTotalLabourExVAT.Width = 110;
            // 
            // colSellTotalLabourVAT
            // 
            this.colSellTotalLabourVAT.Caption = "Labour Sell VAT";
            this.colSellTotalLabourVAT.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colSellTotalLabourVAT.FieldName = "SellTotalLabourVAT";
            this.colSellTotalLabourVAT.Name = "colSellTotalLabourVAT";
            this.colSellTotalLabourVAT.OptionsColumn.AllowEdit = false;
            this.colSellTotalLabourVAT.OptionsColumn.AllowFocus = false;
            this.colSellTotalLabourVAT.OptionsColumn.ReadOnly = true;
            this.colSellTotalLabourVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellTotalLabourVAT", "{0:c}")});
            this.colSellTotalLabourVAT.Visible = true;
            this.colSellTotalLabourVAT.VisibleIndex = 51;
            this.colSellTotalLabourVAT.Width = 95;
            // 
            // repositoryItemTextEditPercentage
            // 
            this.repositoryItemTextEditPercentage.AutoHeight = false;
            this.repositoryItemTextEditPercentage.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage.Name = "repositoryItemTextEditPercentage";
            // 
            // colSellTotalLabourCost
            // 
            this.colSellTotalLabourCost.Caption = "Labour Sell Total";
            this.colSellTotalLabourCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSellTotalLabourCost.FieldName = "SellTotalLabourCost";
            this.colSellTotalLabourCost.Name = "colSellTotalLabourCost";
            this.colSellTotalLabourCost.OptionsColumn.AllowEdit = false;
            this.colSellTotalLabourCost.OptionsColumn.AllowFocus = false;
            this.colSellTotalLabourCost.OptionsColumn.ReadOnly = true;
            this.colSellTotalLabourCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellTotalLabourCost", "{0:c}")});
            this.colSellTotalLabourCost.Visible = true;
            this.colSellTotalLabourCost.VisibleIndex = 52;
            this.colSellTotalLabourCost.Width = 100;
            // 
            // colCostTotalCostExVAT
            // 
            this.colCostTotalCostExVAT.Caption = "Total Cost Ex VAT";
            this.colCostTotalCostExVAT.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCostTotalCostExVAT.FieldName = "CostTotalCostExVAT";
            this.colCostTotalCostExVAT.Name = "colCostTotalCostExVAT";
            this.colCostTotalCostExVAT.OptionsColumn.AllowEdit = false;
            this.colCostTotalCostExVAT.OptionsColumn.AllowFocus = false;
            this.colCostTotalCostExVAT.OptionsColumn.ReadOnly = true;
            this.colCostTotalCostExVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostTotalCostExVAT", "{0:c}")});
            this.colCostTotalCostExVAT.Visible = true;
            this.colCostTotalCostExVAT.VisibleIndex = 53;
            this.colCostTotalCostExVAT.Width = 107;
            // 
            // colCostTotalCostVAT
            // 
            this.colCostTotalCostVAT.Caption = "Total Cost VAT";
            this.colCostTotalCostVAT.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCostTotalCostVAT.FieldName = "CostTotalCostVAT";
            this.colCostTotalCostVAT.Name = "colCostTotalCostVAT";
            this.colCostTotalCostVAT.OptionsColumn.AllowEdit = false;
            this.colCostTotalCostVAT.OptionsColumn.AllowFocus = false;
            this.colCostTotalCostVAT.OptionsColumn.ReadOnly = true;
            this.colCostTotalCostVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostTotalCostVAT", "{0:c}")});
            this.colCostTotalCostVAT.Visible = true;
            this.colCostTotalCostVAT.VisibleIndex = 54;
            this.colCostTotalCostVAT.Width = 92;
            // 
            // colCostTotalCost
            // 
            this.colCostTotalCost.Caption = "Total Cost";
            this.colCostTotalCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCostTotalCost.FieldName = "CostTotalCost";
            this.colCostTotalCost.Name = "colCostTotalCost";
            this.colCostTotalCost.OptionsColumn.AllowEdit = false;
            this.colCostTotalCost.OptionsColumn.AllowFocus = false;
            this.colCostTotalCost.OptionsColumn.ReadOnly = true;
            this.colCostTotalCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostTotalCost", "{0:c}")});
            this.colCostTotalCost.Visible = true;
            this.colCostTotalCost.VisibleIndex = 55;
            // 
            // colSellTotalCostExVAT
            // 
            this.colSellTotalCostExVAT.Caption = "Total Sell Ex VAT";
            this.colSellTotalCostExVAT.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colSellTotalCostExVAT.FieldName = "SellTotalCostExVAT";
            this.colSellTotalCostExVAT.Name = "colSellTotalCostExVAT";
            this.colSellTotalCostExVAT.OptionsColumn.AllowEdit = false;
            this.colSellTotalCostExVAT.OptionsColumn.AllowFocus = false;
            this.colSellTotalCostExVAT.OptionsColumn.ReadOnly = true;
            this.colSellTotalCostExVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellTotalCostExVAT", "{0:c}")});
            this.colSellTotalCostExVAT.Visible = true;
            this.colSellTotalCostExVAT.VisibleIndex = 56;
            this.colSellTotalCostExVAT.Width = 101;
            // 
            // colSellTotalCostVAT
            // 
            this.colSellTotalCostVAT.Caption = "Total Sell VAT";
            this.colSellTotalCostVAT.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSellTotalCostVAT.FieldName = "SellTotalCostVAT";
            this.colSellTotalCostVAT.Name = "colSellTotalCostVAT";
            this.colSellTotalCostVAT.OptionsColumn.AllowEdit = false;
            this.colSellTotalCostVAT.OptionsColumn.AllowFocus = false;
            this.colSellTotalCostVAT.OptionsColumn.ReadOnly = true;
            this.colSellTotalCostVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellTotalCostVAT", "{0:c}")});
            this.colSellTotalCostVAT.Visible = true;
            this.colSellTotalCostVAT.VisibleIndex = 57;
            this.colSellTotalCostVAT.Width = 86;
            // 
            // colSellTotalCost
            // 
            this.colSellTotalCost.Caption = "Total Sell";
            this.colSellTotalCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSellTotalCost.FieldName = "SellTotalCost";
            this.colSellTotalCost.Name = "colSellTotalCost";
            this.colSellTotalCost.OptionsColumn.AllowEdit = false;
            this.colSellTotalCost.OptionsColumn.AllowFocus = false;
            this.colSellTotalCost.OptionsColumn.ReadOnly = true;
            this.colSellTotalCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellTotalCost", "{0:c}")});
            this.colSellTotalCost.Visible = true;
            this.colSellTotalCost.VisibleIndex = 58;
            // 
            // colClientInvoiceID
            // 
            this.colClientInvoiceID.Caption = "Client Invoice ID";
            this.colClientInvoiceID.FieldName = "ClientInvoiceID";
            this.colClientInvoiceID.Name = "colClientInvoiceID";
            this.colClientInvoiceID.OptionsColumn.AllowEdit = false;
            this.colClientInvoiceID.OptionsColumn.AllowFocus = false;
            this.colClientInvoiceID.OptionsColumn.ReadOnly = true;
            this.colClientInvoiceID.Width = 100;
            // 
            // colDateClientInvoiced
            // 
            this.colDateClientInvoiced.Caption = "Client Invoice Date";
            this.colDateClientInvoiced.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDateClientInvoiced.FieldName = "DateClientInvoiced";
            this.colDateClientInvoiced.Name = "colDateClientInvoiced";
            this.colDateClientInvoiced.OptionsColumn.AllowEdit = false;
            this.colDateClientInvoiced.OptionsColumn.AllowFocus = false;
            this.colDateClientInvoiced.OptionsColumn.ReadOnly = true;
            this.colDateClientInvoiced.Visible = true;
            this.colDateClientInvoiced.VisibleIndex = 61;
            this.colDateClientInvoiced.Width = 112;
            // 
            // colSelfBillingInvoiceID
            // 
            this.colSelfBillingInvoiceID.Caption = "Self Billing Invoice ID";
            this.colSelfBillingInvoiceID.FieldName = "SelfBillingInvoiceID";
            this.colSelfBillingInvoiceID.Name = "colSelfBillingInvoiceID";
            this.colSelfBillingInvoiceID.OptionsColumn.AllowEdit = false;
            this.colSelfBillingInvoiceID.OptionsColumn.AllowFocus = false;
            this.colSelfBillingInvoiceID.OptionsColumn.ReadOnly = true;
            this.colSelfBillingInvoiceID.Width = 120;
            // 
            // colSelfBillingInvoiceReceivedDate
            // 
            this.colSelfBillingInvoiceReceivedDate.Caption = "Self Billing Invoice Received Date";
            this.colSelfBillingInvoiceReceivedDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colSelfBillingInvoiceReceivedDate.FieldName = "SelfBillingInvoiceReceivedDate";
            this.colSelfBillingInvoiceReceivedDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colSelfBillingInvoiceReceivedDate.Name = "colSelfBillingInvoiceReceivedDate";
            this.colSelfBillingInvoiceReceivedDate.OptionsColumn.AllowEdit = false;
            this.colSelfBillingInvoiceReceivedDate.OptionsColumn.AllowFocus = false;
            this.colSelfBillingInvoiceReceivedDate.OptionsColumn.ReadOnly = true;
            this.colSelfBillingInvoiceReceivedDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colSelfBillingInvoiceReceivedDate.Visible = true;
            this.colSelfBillingInvoiceReceivedDate.VisibleIndex = 63;
            this.colSelfBillingInvoiceReceivedDate.Width = 179;
            // 
            // colSelfBillingInvoicePaidDate
            // 
            this.colSelfBillingInvoicePaidDate.Caption = "Self Billing Invoice Paid Date";
            this.colSelfBillingInvoicePaidDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colSelfBillingInvoicePaidDate.FieldName = "SelfBillingInvoicePaidDate";
            this.colSelfBillingInvoicePaidDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colSelfBillingInvoicePaidDate.Name = "colSelfBillingInvoicePaidDate";
            this.colSelfBillingInvoicePaidDate.OptionsColumn.AllowEdit = false;
            this.colSelfBillingInvoicePaidDate.OptionsColumn.AllowFocus = false;
            this.colSelfBillingInvoicePaidDate.OptionsColumn.ReadOnly = true;
            this.colSelfBillingInvoicePaidDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colSelfBillingInvoicePaidDate.Visible = true;
            this.colSelfBillingInvoicePaidDate.VisibleIndex = 64;
            this.colSelfBillingInvoicePaidDate.Width = 155;
            // 
            // colSelfBillingInvoiceAmountPaid
            // 
            this.colSelfBillingInvoiceAmountPaid.Caption = "Self Billing Invoice Amunt Paid";
            this.colSelfBillingInvoiceAmountPaid.FieldName = "SelfBillingInvoiceAmountPaid";
            this.colSelfBillingInvoiceAmountPaid.Name = "colSelfBillingInvoiceAmountPaid";
            this.colSelfBillingInvoiceAmountPaid.OptionsColumn.AllowEdit = false;
            this.colSelfBillingInvoiceAmountPaid.OptionsColumn.AllowFocus = false;
            this.colSelfBillingInvoiceAmountPaid.OptionsColumn.ReadOnly = true;
            this.colSelfBillingInvoiceAmountPaid.Visible = true;
            this.colSelfBillingInvoiceAmountPaid.VisibleIndex = 65;
            this.colSelfBillingInvoiceAmountPaid.Width = 163;
            // 
            // colDoNotPayContractor
            // 
            this.colDoNotPayContractor.Caption = "Do Not Pay Contractor";
            this.colDoNotPayContractor.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDoNotPayContractor.FieldName = "DoNotPayContractor";
            this.colDoNotPayContractor.Name = "colDoNotPayContractor";
            this.colDoNotPayContractor.OptionsColumn.AllowEdit = false;
            this.colDoNotPayContractor.OptionsColumn.AllowFocus = false;
            this.colDoNotPayContractor.OptionsColumn.ReadOnly = true;
            this.colDoNotPayContractor.Visible = true;
            this.colDoNotPayContractor.VisibleIndex = 66;
            this.colDoNotPayContractor.Width = 130;
            // 
            // colDoNotInvoiceClient
            // 
            this.colDoNotInvoiceClient.Caption = "Do Not Invoice Client";
            this.colDoNotInvoiceClient.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDoNotInvoiceClient.FieldName = "DoNotInvoiceClient";
            this.colDoNotInvoiceClient.Name = "colDoNotInvoiceClient";
            this.colDoNotInvoiceClient.OptionsColumn.AllowEdit = false;
            this.colDoNotInvoiceClient.OptionsColumn.AllowFocus = false;
            this.colDoNotInvoiceClient.OptionsColumn.ReadOnly = true;
            this.colDoNotInvoiceClient.Visible = true;
            this.colDoNotInvoiceClient.VisibleIndex = 67;
            this.colDoNotInvoiceClient.Width = 122;
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.ReadOnly = true;
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 68;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colRouteOrder
            // 
            this.colRouteOrder.Caption = "Route Order";
            this.colRouteOrder.FieldName = "RouteOrder";
            this.colRouteOrder.Name = "colRouteOrder";
            this.colRouteOrder.OptionsColumn.AllowEdit = false;
            this.colRouteOrder.OptionsColumn.AllowFocus = false;
            this.colRouteOrder.OptionsColumn.ReadOnly = true;
            this.colRouteOrder.Visible = true;
            this.colRouteOrder.VisibleIndex = 69;
            this.colRouteOrder.Width = 81;
            // 
            // colStartLatitude
            // 
            this.colStartLatitude.Caption = "Start Latitude";
            this.colStartLatitude.ColumnEdit = this.repositoryItemTextEditLatLong;
            this.colStartLatitude.FieldName = "StartLatitude";
            this.colStartLatitude.Name = "colStartLatitude";
            this.colStartLatitude.OptionsColumn.AllowEdit = false;
            this.colStartLatitude.OptionsColumn.AllowFocus = false;
            this.colStartLatitude.OptionsColumn.ReadOnly = true;
            this.colStartLatitude.Visible = true;
            this.colStartLatitude.VisibleIndex = 70;
            this.colStartLatitude.Width = 87;
            // 
            // repositoryItemTextEditLatLong
            // 
            this.repositoryItemTextEditLatLong.AutoHeight = false;
            this.repositoryItemTextEditLatLong.Mask.EditMask = "###0.0000000";
            this.repositoryItemTextEditLatLong.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditLatLong.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditLatLong.Name = "repositoryItemTextEditLatLong";
            // 
            // colStartLongitude
            // 
            this.colStartLongitude.Caption = "Start Longitude";
            this.colStartLongitude.ColumnEdit = this.repositoryItemTextEditLatLong;
            this.colStartLongitude.FieldName = "StartLongitude";
            this.colStartLongitude.Name = "colStartLongitude";
            this.colStartLongitude.OptionsColumn.AllowEdit = false;
            this.colStartLongitude.OptionsColumn.AllowFocus = false;
            this.colStartLongitude.OptionsColumn.ReadOnly = true;
            this.colStartLongitude.Visible = true;
            this.colStartLongitude.VisibleIndex = 71;
            this.colStartLongitude.Width = 95;
            // 
            // colFinishLatitude
            // 
            this.colFinishLatitude.Caption = "Finish Latitude";
            this.colFinishLatitude.ColumnEdit = this.repositoryItemTextEditLatLong;
            this.colFinishLatitude.FieldName = "FinishLatitude";
            this.colFinishLatitude.Name = "colFinishLatitude";
            this.colFinishLatitude.OptionsColumn.AllowEdit = false;
            this.colFinishLatitude.OptionsColumn.AllowFocus = false;
            this.colFinishLatitude.OptionsColumn.ReadOnly = true;
            this.colFinishLatitude.Visible = true;
            this.colFinishLatitude.VisibleIndex = 72;
            this.colFinishLatitude.Width = 90;
            // 
            // colFinishLongitude
            // 
            this.colFinishLongitude.Caption = "Finish Longitude";
            this.colFinishLongitude.ColumnEdit = this.repositoryItemTextEditLatLong;
            this.colFinishLongitude.FieldName = "FinishLongitude";
            this.colFinishLongitude.Name = "colFinishLongitude";
            this.colFinishLongitude.OptionsColumn.AllowEdit = false;
            this.colFinishLongitude.OptionsColumn.AllowFocus = false;
            this.colFinishLongitude.OptionsColumn.ReadOnly = true;
            this.colFinishLongitude.Visible = true;
            this.colFinishLongitude.VisibleIndex = 73;
            this.colFinishLongitude.Width = 98;
            // 
            // colJobStatusDescription
            // 
            this.colJobStatusDescription.Caption = "Job Status";
            this.colJobStatusDescription.FieldName = "JobStatusDescription";
            this.colJobStatusDescription.Name = "colJobStatusDescription";
            this.colJobStatusDescription.OptionsColumn.AllowEdit = false;
            this.colJobStatusDescription.OptionsColumn.AllowFocus = false;
            this.colJobStatusDescription.OptionsColumn.ReadOnly = true;
            this.colJobStatusDescription.Visible = true;
            this.colJobStatusDescription.VisibleIndex = 10;
            this.colJobStatusDescription.Width = 129;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.Visible = true;
            this.colSiteName.VisibleIndex = 3;
            this.colSiteName.Width = 122;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 1;
            this.colClientName.Width = 125;
            // 
            // colCreatedByStaffName1
            // 
            this.colCreatedByStaffName1.Caption = "Created By";
            this.colCreatedByStaffName1.FieldName = "CreatedByStaffName";
            this.colCreatedByStaffName1.Name = "colCreatedByStaffName1";
            this.colCreatedByStaffName1.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffName1.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffName1.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffName1.Visible = true;
            this.colCreatedByStaffName1.VisibleIndex = 74;
            this.colCreatedByStaffName1.Width = 89;
            // 
            // colExpectedDurationUnitsDescriptor
            // 
            this.colExpectedDurationUnitsDescriptor.Caption = "Expected Duration Units";
            this.colExpectedDurationUnitsDescriptor.FieldName = "ExpectedDurationUnitsDescriptor";
            this.colExpectedDurationUnitsDescriptor.Name = "colExpectedDurationUnitsDescriptor";
            this.colExpectedDurationUnitsDescriptor.OptionsColumn.AllowEdit = false;
            this.colExpectedDurationUnitsDescriptor.OptionsColumn.AllowFocus = false;
            this.colExpectedDurationUnitsDescriptor.OptionsColumn.ReadOnly = true;
            this.colExpectedDurationUnitsDescriptor.Visible = true;
            this.colExpectedDurationUnitsDescriptor.VisibleIndex = 15;
            this.colExpectedDurationUnitsDescriptor.Width = 137;
            // 
            // colActualDurationUnitsDescription
            // 
            this.colActualDurationUnitsDescription.Caption = "Actual Duration Units";
            this.colActualDurationUnitsDescription.FieldName = "ActualDurationUnitsDescriptor";
            this.colActualDurationUnitsDescription.Name = "colActualDurationUnitsDescription";
            this.colActualDurationUnitsDescription.OptionsColumn.AllowEdit = false;
            this.colActualDurationUnitsDescription.OptionsColumn.AllowFocus = false;
            this.colActualDurationUnitsDescription.OptionsColumn.ReadOnly = true;
            this.colActualDurationUnitsDescription.Visible = true;
            this.colActualDurationUnitsDescription.VisibleIndex = 26;
            this.colActualDurationUnitsDescription.Width = 122;
            // 
            // colJobTypeID1
            // 
            this.colJobTypeID1.Caption = "Job Type ID";
            this.colJobTypeID1.FieldName = "JobTypeID";
            this.colJobTypeID1.Name = "colJobTypeID1";
            this.colJobTypeID1.OptionsColumn.AllowEdit = false;
            this.colJobTypeID1.OptionsColumn.AllowFocus = false;
            this.colJobTypeID1.OptionsColumn.ReadOnly = true;
            this.colJobTypeID1.Width = 79;
            // 
            // colJobSubTypeDescription
            // 
            this.colJobSubTypeDescription.Caption = "Job Sub-Type";
            this.colJobSubTypeDescription.FieldName = "JobSubTypeDescription";
            this.colJobSubTypeDescription.Name = "colJobSubTypeDescription";
            this.colJobSubTypeDescription.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeDescription.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeDescription.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeDescription.Visible = true;
            this.colJobSubTypeDescription.VisibleIndex = 9;
            this.colJobSubTypeDescription.Width = 118;
            // 
            // colJobTypeDescription1
            // 
            this.colJobTypeDescription1.Caption = "Job Type";
            this.colJobTypeDescription1.FieldName = "JobTypeDescription";
            this.colJobTypeDescription1.Name = "colJobTypeDescription1";
            this.colJobTypeDescription1.OptionsColumn.AllowEdit = false;
            this.colJobTypeDescription1.OptionsColumn.AllowFocus = false;
            this.colJobTypeDescription1.OptionsColumn.ReadOnly = true;
            this.colJobTypeDescription1.Visible = true;
            this.colJobTypeDescription1.VisibleIndex = 7;
            this.colJobTypeDescription1.Width = 113;
            // 
            // colLinkedJobCount
            // 
            this.colLinkedJobCount.Caption = "Linked Jobs";
            this.colLinkedJobCount.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colLinkedJobCount.FieldName = "LinkedJobCount";
            this.colLinkedJobCount.Name = "colLinkedJobCount";
            this.colLinkedJobCount.OptionsColumn.ReadOnly = true;
            this.colLinkedJobCount.Visible = true;
            this.colLinkedJobCount.VisibleIndex = 75;
            this.colLinkedJobCount.Width = 76;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            this.repositoryItemHyperLinkEdit1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit1_OpenLink);
            // 
            // colRecordType
            // 
            this.colRecordType.Caption = "Record Type";
            this.colRecordType.FieldName = "RecordType";
            this.colRecordType.Name = "colRecordType";
            this.colRecordType.OptionsColumn.AllowEdit = false;
            this.colRecordType.OptionsColumn.AllowFocus = false;
            this.colRecordType.OptionsColumn.ReadOnly = true;
            this.colRecordType.Visible = true;
            this.colRecordType.VisibleIndex = 20;
            this.colRecordType.Width = 120;
            // 
            // colSelected
            // 
            this.colSelected.Caption = "Selected";
            this.colSelected.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSelected.FieldName = "Selected";
            this.colSelected.Name = "colSelected";
            this.colSelected.Width = 62;
            // 
            // colFullDescription1
            // 
            this.colFullDescription1.Caption = "Full Description";
            this.colFullDescription1.ColumnEdit = this.repositoryItemTextEditHTML;
            this.colFullDescription1.FieldName = "FullDescription";
            this.colFullDescription1.Name = "colFullDescription1";
            this.colFullDescription1.OptionsColumn.AllowEdit = false;
            this.colFullDescription1.OptionsColumn.AllowFocus = false;
            this.colFullDescription1.OptionsColumn.ReadOnly = true;
            this.colFullDescription1.Width = 420;
            // 
            // repositoryItemTextEditHTML
            // 
            this.repositoryItemTextEditHTML.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTextEditHTML.AutoHeight = false;
            this.repositoryItemTextEditHTML.Name = "repositoryItemTextEditHTML";
            // 
            // colTenderID
            // 
            this.colTenderID.Caption = "Tender ID";
            this.colTenderID.FieldName = "TenderID";
            this.colTenderID.Name = "colTenderID";
            this.colTenderID.OptionsColumn.AllowEdit = false;
            this.colTenderID.OptionsColumn.AllowFocus = false;
            this.colTenderID.OptionsColumn.ReadOnly = true;
            this.colTenderID.Width = 69;
            // 
            // colTenderJobID
            // 
            this.colTenderJobID.Caption = "Tender Job ID";
            this.colTenderJobID.FieldName = "TenderJobID";
            this.colTenderJobID.Name = "colTenderJobID";
            this.colTenderJobID.OptionsColumn.AllowEdit = false;
            this.colTenderJobID.OptionsColumn.AllowFocus = false;
            this.colTenderJobID.OptionsColumn.ReadOnly = true;
            this.colTenderJobID.Width = 89;
            // 
            // colMinimumDaysFromLastVisit
            // 
            this.colMinimumDaysFromLastVisit.Caption = "Min Days From Last Visit";
            this.colMinimumDaysFromLastVisit.ColumnEdit = this.repositoryItemTextEditInteger;
            this.colMinimumDaysFromLastVisit.FieldName = "MinimumDaysFromLastVisit";
            this.colMinimumDaysFromLastVisit.Name = "colMinimumDaysFromLastVisit";
            this.colMinimumDaysFromLastVisit.OptionsColumn.AllowEdit = false;
            this.colMinimumDaysFromLastVisit.OptionsColumn.AllowFocus = false;
            this.colMinimumDaysFromLastVisit.OptionsColumn.ReadOnly = true;
            this.colMinimumDaysFromLastVisit.Visible = true;
            this.colMinimumDaysFromLastVisit.VisibleIndex = 79;
            this.colMinimumDaysFromLastVisit.Width = 136;
            // 
            // colMaximumDaysFromLastVisit
            // 
            this.colMaximumDaysFromLastVisit.Caption = "Max Days From Last Visit";
            this.colMaximumDaysFromLastVisit.ColumnEdit = this.repositoryItemTextEditInteger;
            this.colMaximumDaysFromLastVisit.FieldName = "MaximumDaysFromLastVisit";
            this.colMaximumDaysFromLastVisit.Name = "colMaximumDaysFromLastVisit";
            this.colMaximumDaysFromLastVisit.OptionsColumn.AllowEdit = false;
            this.colMaximumDaysFromLastVisit.OptionsColumn.AllowFocus = false;
            this.colMaximumDaysFromLastVisit.OptionsColumn.ReadOnly = true;
            this.colMaximumDaysFromLastVisit.Visible = true;
            this.colMaximumDaysFromLastVisit.VisibleIndex = 80;
            this.colMaximumDaysFromLastVisit.Width = 140;
            // 
            // colDaysLeeway
            // 
            this.colDaysLeeway.Caption = "Days Leeway";
            this.colDaysLeeway.ColumnEdit = this.repositoryItemTextEditInteger;
            this.colDaysLeeway.FieldName = "DaysLeeway";
            this.colDaysLeeway.Name = "colDaysLeeway";
            this.colDaysLeeway.OptionsColumn.AllowEdit = false;
            this.colDaysLeeway.OptionsColumn.AllowFocus = false;
            this.colDaysLeeway.OptionsColumn.ReadOnly = true;
            this.colDaysLeeway.Visible = true;
            this.colDaysLeeway.VisibleIndex = 81;
            this.colDaysLeeway.Width = 85;
            // 
            // colBillingCentreCodeID
            // 
            this.colBillingCentreCodeID.Caption = "Billing Centre Code ID";
            this.colBillingCentreCodeID.FieldName = "BillingCentreCodeID";
            this.colBillingCentreCodeID.Name = "colBillingCentreCodeID";
            this.colBillingCentreCodeID.OptionsColumn.AllowEdit = false;
            this.colBillingCentreCodeID.OptionsColumn.AllowFocus = false;
            this.colBillingCentreCodeID.OptionsColumn.ReadOnly = true;
            this.colBillingCentreCodeID.Width = 125;
            // 
            // colLinkedDocumentCount
            // 
            this.colLinkedDocumentCount.Caption = "Linked Documents";
            this.colLinkedDocumentCount.ColumnEdit = this.repositoryItemHyperLinkEditLinkedDocumentsJob_OpenLink;
            this.colLinkedDocumentCount.FieldName = "LinkedDocumentCount";
            this.colLinkedDocumentCount.Name = "colLinkedDocumentCount";
            this.colLinkedDocumentCount.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentCount.Visible = true;
            this.colLinkedDocumentCount.VisibleIndex = 77;
            this.colLinkedDocumentCount.Width = 107;
            // 
            // repositoryItemHyperLinkEditLinkedDocumentsJob_OpenLink
            // 
            this.repositoryItemHyperLinkEditLinkedDocumentsJob_OpenLink.AutoHeight = false;
            this.repositoryItemHyperLinkEditLinkedDocumentsJob_OpenLink.Name = "repositoryItemHyperLinkEditLinkedDocumentsJob_OpenLink";
            this.repositoryItemHyperLinkEditLinkedDocumentsJob_OpenLink.ReadOnly = true;
            this.repositoryItemHyperLinkEditLinkedDocumentsJob_OpenLink.SingleClick = true;
            this.repositoryItemHyperLinkEditLinkedDocumentsJob_OpenLink.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditLinkedDocumentsJob_OpenLink_OpenLink);
            // 
            // colContractDescription
            // 
            this.colContractDescription.Caption = "Contract Description";
            this.colContractDescription.FieldName = "ContractDescription";
            this.colContractDescription.Name = "colContractDescription";
            this.colContractDescription.OptionsColumn.AllowEdit = false;
            this.colContractDescription.OptionsColumn.AllowFocus = false;
            this.colContractDescription.OptionsColumn.ReadOnly = true;
            this.colContractDescription.Visible = true;
            this.colContractDescription.VisibleIndex = 21;
            this.colContractDescription.Width = 150;
            // 
            // colCostTotalEquipmentVATRate
            // 
            this.colCostTotalEquipmentVATRate.Caption = "Equipment Cost VAT Rate";
            this.colCostTotalEquipmentVATRate.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colCostTotalEquipmentVATRate.FieldName = "CostTotalEquipmentVATRate";
            this.colCostTotalEquipmentVATRate.Name = "colCostTotalEquipmentVATRate";
            this.colCostTotalEquipmentVATRate.OptionsColumn.AllowEdit = false;
            this.colCostTotalEquipmentVATRate.OptionsColumn.AllowFocus = false;
            this.colCostTotalEquipmentVATRate.OptionsColumn.ReadOnly = true;
            this.colCostTotalEquipmentVATRate.Visible = true;
            this.colCostTotalEquipmentVATRate.VisibleIndex = 38;
            this.colCostTotalEquipmentVATRate.Width = 144;
            // 
            // colCostTotalLabourVATRate
            // 
            this.colCostTotalLabourVATRate.Caption = "Labour Cost VAT Rate";
            this.colCostTotalLabourVATRate.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colCostTotalLabourVATRate.FieldName = "CostTotalLabourVATRate";
            this.colCostTotalLabourVATRate.Name = "colCostTotalLabourVATRate";
            this.colCostTotalLabourVATRate.OptionsColumn.AllowEdit = false;
            this.colCostTotalLabourVATRate.OptionsColumn.AllowFocus = false;
            this.colCostTotalLabourVATRate.OptionsColumn.ReadOnly = true;
            this.colCostTotalLabourVATRate.Visible = true;
            this.colCostTotalLabourVATRate.VisibleIndex = 46;
            this.colCostTotalLabourVATRate.Width = 127;
            // 
            // colCostTotalMaterialVATRate
            // 
            this.colCostTotalMaterialVATRate.Caption = "Material Cost VAT Rate";
            this.colCostTotalMaterialVATRate.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colCostTotalMaterialVATRate.FieldName = "CostTotalMaterialVATRate";
            this.colCostTotalMaterialVATRate.Name = "colCostTotalMaterialVATRate";
            this.colCostTotalMaterialVATRate.OptionsColumn.AllowEdit = false;
            this.colCostTotalMaterialVATRate.OptionsColumn.AllowFocus = false;
            this.colCostTotalMaterialVATRate.OptionsColumn.ReadOnly = true;
            this.colCostTotalMaterialVATRate.Visible = true;
            this.colCostTotalMaterialVATRate.VisibleIndex = 30;
            this.colCostTotalMaterialVATRate.Width = 132;
            // 
            // colSellTotalMaterialVATRate
            // 
            this.colSellTotalMaterialVATRate.Caption = "Material Sell VAT Rate";
            this.colSellTotalMaterialVATRate.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colSellTotalMaterialVATRate.FieldName = "SellTotalMaterialVATRate";
            this.colSellTotalMaterialVATRate.Name = "colSellTotalMaterialVATRate";
            this.colSellTotalMaterialVATRate.OptionsColumn.AllowEdit = false;
            this.colSellTotalMaterialVATRate.OptionsColumn.AllowFocus = false;
            this.colSellTotalMaterialVATRate.OptionsColumn.ReadOnly = true;
            this.colSellTotalMaterialVATRate.Visible = true;
            this.colSellTotalMaterialVATRate.VisibleIndex = 34;
            this.colSellTotalMaterialVATRate.Width = 126;
            // 
            // colSellTotalEquipmentVATRate
            // 
            this.colSellTotalEquipmentVATRate.Caption = "Equipment Sell VAT Rate";
            this.colSellTotalEquipmentVATRate.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colSellTotalEquipmentVATRate.FieldName = "SellTotalEquipmentVATRate";
            this.colSellTotalEquipmentVATRate.Name = "colSellTotalEquipmentVATRate";
            this.colSellTotalEquipmentVATRate.OptionsColumn.AllowEdit = false;
            this.colSellTotalEquipmentVATRate.OptionsColumn.AllowFocus = false;
            this.colSellTotalEquipmentVATRate.OptionsColumn.ReadOnly = true;
            this.colSellTotalEquipmentVATRate.Visible = true;
            this.colSellTotalEquipmentVATRate.VisibleIndex = 42;
            this.colSellTotalEquipmentVATRate.Width = 138;
            // 
            // colSellTotalLabourVATRate
            // 
            this.colSellTotalLabourVATRate.Caption = "Labour Sell VAT Rate";
            this.colSellTotalLabourVATRate.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colSellTotalLabourVATRate.FieldName = "SellTotalLabourVATRate";
            this.colSellTotalLabourVATRate.Name = "colSellTotalLabourVATRate";
            this.colSellTotalLabourVATRate.OptionsColumn.AllowEdit = false;
            this.colSellTotalLabourVATRate.OptionsColumn.AllowFocus = false;
            this.colSellTotalLabourVATRate.OptionsColumn.ReadOnly = true;
            this.colSellTotalLabourVATRate.Visible = true;
            this.colSellTotalLabourVATRate.VisibleIndex = 50;
            this.colSellTotalLabourVATRate.Width = 121;
            // 
            // colClientContractID
            // 
            this.colClientContractID.Caption = "Client Contract ID";
            this.colClientContractID.FieldName = "ClientContractID";
            this.colClientContractID.Name = "colClientContractID";
            this.colClientContractID.OptionsColumn.AllowEdit = false;
            this.colClientContractID.OptionsColumn.AllowFocus = false;
            this.colClientContractID.OptionsColumn.ReadOnly = true;
            this.colClientContractID.Width = 107;
            // 
            // colClientID2
            // 
            this.colClientID2.Caption = "Client ID";
            this.colClientID2.FieldName = "ClientID";
            this.colClientID2.Name = "colClientID2";
            this.colClientID2.OptionsColumn.AllowEdit = false;
            this.colClientID2.OptionsColumn.AllowFocus = false;
            this.colClientID2.OptionsColumn.ReadOnly = true;
            this.colClientID2.Width = 62;
            // 
            // colSiteContractID
            // 
            this.colSiteContractID.Caption = "Site Contract ID";
            this.colSiteContractID.FieldName = "SiteContractID";
            this.colSiteContractID.Name = "colSiteContractID";
            this.colSiteContractID.OptionsColumn.AllowEdit = false;
            this.colSiteContractID.OptionsColumn.AllowFocus = false;
            this.colSiteContractID.OptionsColumn.ReadOnly = true;
            this.colSiteContractID.Width = 98;
            // 
            // colSiteID2
            // 
            this.colSiteID2.Caption = "Site ID";
            this.colSiteID2.FieldName = "SiteID";
            this.colSiteID2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colSiteID2.Name = "colSiteID2";
            this.colSiteID2.OptionsColumn.AllowEdit = false;
            this.colSiteID2.OptionsColumn.AllowFocus = false;
            this.colSiteID2.OptionsColumn.ReadOnly = true;
            this.colSiteID2.Visible = true;
            this.colSiteID2.VisibleIndex = 2;
            this.colSiteID2.Width = 53;
            // 
            // colImagesFolderOM
            // 
            this.colImagesFolderOM.Caption = "Images Folder";
            this.colImagesFolderOM.FieldName = "ImagesFolderOM";
            this.colImagesFolderOM.Name = "colImagesFolderOM";
            this.colImagesFolderOM.OptionsColumn.AllowEdit = false;
            this.colImagesFolderOM.OptionsColumn.AllowFocus = false;
            this.colImagesFolderOM.OptionsColumn.ReadOnly = true;
            this.colImagesFolderOM.Width = 89;
            // 
            // colRework
            // 
            this.colRework.Caption = "Rework";
            this.colRework.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colRework.FieldName = "Rework";
            this.colRework.Name = "colRework";
            this.colRework.OptionsColumn.AllowEdit = false;
            this.colRework.OptionsColumn.AllowFocus = false;
            this.colRework.OptionsColumn.ReadOnly = true;
            this.colRework.Visible = true;
            this.colRework.VisibleIndex = 86;
            this.colRework.Width = 57;
            // 
            // colReworkOriginalJobID
            // 
            this.colReworkOriginalJobID.Caption = "Rework Original Job ID";
            this.colReworkOriginalJobID.FieldName = "ReworkOriginalJobID";
            this.colReworkOriginalJobID.Name = "colReworkOriginalJobID";
            this.colReworkOriginalJobID.OptionsColumn.AllowEdit = false;
            this.colReworkOriginalJobID.OptionsColumn.AllowFocus = false;
            this.colReworkOriginalJobID.OptionsColumn.ReadOnly = true;
            this.colReworkOriginalJobID.Width = 130;
            // 
            // colLocationX
            // 
            this.colLocationX.Caption = "Site Latitude";
            this.colLocationX.ColumnEdit = this.repositoryItemTextEditLatLong;
            this.colLocationX.FieldName = "LocationX";
            this.colLocationX.Name = "colLocationX";
            this.colLocationX.OptionsColumn.AllowEdit = false;
            this.colLocationX.OptionsColumn.AllowFocus = false;
            this.colLocationX.OptionsColumn.ReadOnly = true;
            this.colLocationX.Width = 81;
            // 
            // colLocationY
            // 
            this.colLocationY.Caption = "Site Longitude";
            this.colLocationY.ColumnEdit = this.repositoryItemTextEditLatLong;
            this.colLocationY.FieldName = "LocationY";
            this.colLocationY.Name = "colLocationY";
            this.colLocationY.OptionsColumn.AllowEdit = false;
            this.colLocationY.OptionsColumn.AllowFocus = false;
            this.colLocationY.OptionsColumn.ReadOnly = true;
            this.colLocationY.Width = 89;
            // 
            // colSitePostcode
            // 
            this.colSitePostcode.Caption = "Site Postcode";
            this.colSitePostcode.FieldName = "SitePostcode";
            this.colSitePostcode.Name = "colSitePostcode";
            this.colSitePostcode.OptionsColumn.AllowEdit = false;
            this.colSitePostcode.OptionsColumn.AllowFocus = false;
            this.colSitePostcode.OptionsColumn.ReadOnly = true;
            this.colSitePostcode.Width = 86;
            // 
            // colLinkedLabourCount
            // 
            this.colLinkedLabourCount.Caption = "Linked Labour";
            this.colLinkedLabourCount.FieldName = "LinkedLabourCount";
            this.colLinkedLabourCount.Name = "colLinkedLabourCount";
            this.colLinkedLabourCount.OptionsColumn.AllowEdit = false;
            this.colLinkedLabourCount.OptionsColumn.AllowFocus = false;
            this.colLinkedLabourCount.OptionsColumn.ReadOnly = true;
            this.colLinkedLabourCount.Visible = true;
            this.colLinkedLabourCount.VisibleIndex = 76;
            this.colLinkedLabourCount.Width = 87;
            // 
            // colDaysUntilDue
            // 
            this.colDaysUntilDue.Caption = "Due In";
            this.colDaysUntilDue.ColumnEdit = this.repositoryItemTextEditIntegerDays;
            this.colDaysUntilDue.FieldName = "DaysUntilDue";
            this.colDaysUntilDue.Name = "colDaysUntilDue";
            this.colDaysUntilDue.OptionsColumn.AllowEdit = false;
            this.colDaysUntilDue.OptionsColumn.AllowFocus = false;
            this.colDaysUntilDue.OptionsColumn.ReadOnly = true;
            this.colDaysUntilDue.Visible = true;
            this.colDaysUntilDue.VisibleIndex = 11;
            this.colDaysUntilDue.Width = 67;
            // 
            // repositoryItemTextEditIntegerDays
            // 
            this.repositoryItemTextEditIntegerDays.AutoHeight = false;
            this.repositoryItemTextEditIntegerDays.Mask.EditMask = "#####0 Days";
            this.repositoryItemTextEditIntegerDays.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditIntegerDays.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditIntegerDays.Name = "repositoryItemTextEditIntegerDays";
            // 
            // colCancelledReasonID
            // 
            this.colCancelledReasonID.Caption = "Cancelled Reason ID";
            this.colCancelledReasonID.FieldName = "CancelledReasonID";
            this.colCancelledReasonID.Name = "colCancelledReasonID";
            this.colCancelledReasonID.OptionsColumn.AllowEdit = false;
            this.colCancelledReasonID.OptionsColumn.AllowFocus = false;
            this.colCancelledReasonID.OptionsColumn.ReadOnly = true;
            this.colCancelledReasonID.Width = 120;
            // 
            // colCancelledReason
            // 
            this.colCancelledReason.Caption = "Cancelled Reason";
            this.colCancelledReason.FieldName = "CancelledReason";
            this.colCancelledReason.Name = "colCancelledReason";
            this.colCancelledReason.OptionsColumn.AllowEdit = false;
            this.colCancelledReason.OptionsColumn.AllowFocus = false;
            this.colCancelledReason.OptionsColumn.ReadOnly = true;
            this.colCancelledReason.Visible = true;
            this.colCancelledReason.VisibleIndex = 28;
            this.colCancelledReason.Width = 106;
            // 
            // colManuallyCompleted
            // 
            this.colManuallyCompleted.Caption = "Manually Completed";
            this.colManuallyCompleted.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colManuallyCompleted.FieldName = "ManuallyCompleted";
            this.colManuallyCompleted.Name = "colManuallyCompleted";
            this.colManuallyCompleted.OptionsColumn.AllowEdit = false;
            this.colManuallyCompleted.OptionsColumn.AllowFocus = false;
            this.colManuallyCompleted.OptionsColumn.ReadOnly = true;
            this.colManuallyCompleted.Visible = true;
            this.colManuallyCompleted.VisibleIndex = 85;
            this.colManuallyCompleted.Width = 117;
            // 
            // Alert
            // 
            this.Alert.AppearanceCell.Options.UseTextOptions = true;
            this.Alert.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Alert.ColumnEdit = this.repositoryItemPictureEditAlert;
            this.Alert.CustomizationCaption = "Alert";
            this.Alert.FieldName = "Alert";
            this.Alert.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.Alert.ImageOptions.Alignment = System.Drawing.StringAlignment.Center;
            this.Alert.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("Alert.ImageOptions.Image")));
            this.Alert.Name = "Alert";
            this.Alert.OptionsColumn.AllowEdit = false;
            this.Alert.OptionsColumn.AllowFocus = false;
            this.Alert.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.Alert.OptionsColumn.AllowSize = false;
            this.Alert.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.Alert.OptionsColumn.FixedWidth = true;
            this.Alert.OptionsColumn.ReadOnly = true;
            this.Alert.OptionsColumn.ShowCaption = false;
            this.Alert.OptionsColumn.ShowInExpressionEditor = false;
            this.Alert.OptionsFilter.AllowAutoFilter = false;
            this.Alert.OptionsFilter.AllowFilter = false;
            this.Alert.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Alert.Visible = true;
            this.Alert.VisibleIndex = 0;
            this.Alert.Width = 24;
            // 
            // repositoryItemPictureEditAlert
            // 
            this.repositoryItemPictureEditAlert.Name = "repositoryItemPictureEditAlert";
            this.repositoryItemPictureEditAlert.ReadOnly = true;
            this.repositoryItemPictureEditAlert.ShowMenu = false;
            // 
            // colLinkedPictureCount
            // 
            this.colLinkedPictureCount.Caption = "Linked Pictures";
            this.colLinkedPictureCount.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colLinkedPictureCount.FieldName = "LinkedPictureCount";
            this.colLinkedPictureCount.Name = "colLinkedPictureCount";
            this.colLinkedPictureCount.OptionsColumn.ReadOnly = true;
            this.colLinkedPictureCount.Visible = true;
            this.colLinkedPictureCount.VisibleIndex = 78;
            this.colLinkedPictureCount.Width = 90;
            // 
            // colNoWorkRequired
            // 
            this.colNoWorkRequired.Caption = "No Work Required";
            this.colNoWorkRequired.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colNoWorkRequired.FieldName = "NoWorkRequired";
            this.colNoWorkRequired.Name = "colNoWorkRequired";
            this.colNoWorkRequired.OptionsColumn.AllowEdit = false;
            this.colNoWorkRequired.OptionsColumn.AllowFocus = false;
            this.colNoWorkRequired.OptionsColumn.ReadOnly = true;
            this.colNoWorkRequired.Visible = true;
            this.colNoWorkRequired.VisibleIndex = 82;
            this.colNoWorkRequired.Width = 106;
            // 
            // colFriendlyVisitNumber
            // 
            this.colFriendlyVisitNumber.Caption = "Friendly Visit #";
            this.colFriendlyVisitNumber.FieldName = "FriendlyVisitNumber";
            this.colFriendlyVisitNumber.Name = "colFriendlyVisitNumber";
            this.colFriendlyVisitNumber.OptionsColumn.AllowEdit = false;
            this.colFriendlyVisitNumber.OptionsColumn.AllowFocus = false;
            this.colFriendlyVisitNumber.OptionsColumn.ReadOnly = true;
            this.colFriendlyVisitNumber.Visible = true;
            this.colFriendlyVisitNumber.VisibleIndex = 6;
            this.colFriendlyVisitNumber.Width = 90;
            // 
            // colClientPO
            // 
            this.colClientPO.Caption = "Client PO #";
            this.colClientPO.FieldName = "ClientPO";
            this.colClientPO.Name = "colClientPO";
            this.colClientPO.OptionsColumn.AllowEdit = false;
            this.colClientPO.OptionsColumn.AllowFocus = false;
            this.colClientPO.OptionsColumn.ReadOnly = true;
            this.colClientPO.Visible = true;
            this.colClientPO.VisibleIndex = 59;
            this.colClientPO.Width = 100;
            // 
            // colVisitType
            // 
            this.colVisitType.Caption = "Visit Type";
            this.colVisitType.FieldName = "VisitType";
            this.colVisitType.Name = "colVisitType";
            this.colVisitType.OptionsColumn.AllowEdit = false;
            this.colVisitType.OptionsColumn.AllowFocus = false;
            this.colVisitType.OptionsColumn.ReadOnly = true;
            this.colVisitType.Visible = true;
            this.colVisitType.VisibleIndex = 8;
            this.colVisitType.Width = 113;
            // 
            // colVisitTypeID
            // 
            this.colVisitTypeID.Caption = "Visit Type ID";
            this.colVisitTypeID.FieldName = "VisitTypeID";
            this.colVisitTypeID.Name = "colVisitTypeID";
            this.colVisitTypeID.OptionsColumn.AllowEdit = false;
            this.colVisitTypeID.OptionsColumn.AllowFocus = false;
            this.colVisitTypeID.OptionsColumn.ReadOnly = true;
            this.colVisitTypeID.Width = 79;
            // 
            // colSiteCode
            // 
            this.colSiteCode.Caption = "Site Code";
            this.colSiteCode.FieldName = "SiteCode";
            this.colSiteCode.Name = "colSiteCode";
            this.colSiteCode.OptionsColumn.AllowEdit = false;
            this.colSiteCode.OptionsColumn.AllowFocus = false;
            this.colSiteCode.OptionsColumn.ReadOnly = true;
            this.colSiteCode.Visible = true;
            this.colSiteCode.VisibleIndex = 5;
            // 
            // colVisitCostCalculationLevelID
            // 
            this.colVisitCostCalculationLevelID.Caption = "Visit Cost Calculation Method ID";
            this.colVisitCostCalculationLevelID.FieldName = "VisitCostCalculationLevelID";
            this.colVisitCostCalculationLevelID.Name = "colVisitCostCalculationLevelID";
            this.colVisitCostCalculationLevelID.OptionsColumn.AllowEdit = false;
            this.colVisitCostCalculationLevelID.OptionsColumn.AllowFocus = false;
            this.colVisitCostCalculationLevelID.OptionsColumn.ReadOnly = true;
            this.colVisitCostCalculationLevelID.Width = 171;
            // 
            // colVisitSellCalculationLevelID
            // 
            this.colVisitSellCalculationLevelID.Caption = "Visit Sell Calculation Method ID";
            this.colVisitSellCalculationLevelID.FieldName = "VisitSellCalculationLevelID";
            this.colVisitSellCalculationLevelID.Name = "colVisitSellCalculationLevelID";
            this.colVisitSellCalculationLevelID.OptionsColumn.AllowEdit = false;
            this.colVisitSellCalculationLevelID.OptionsColumn.AllowFocus = false;
            this.colVisitSellCalculationLevelID.OptionsColumn.ReadOnly = true;
            this.colVisitSellCalculationLevelID.Width = 165;
            // 
            // colVisitCostCalculationLevelDescription
            // 
            this.colVisitCostCalculationLevelDescription.Caption = "Visit Cost Calculation Method";
            this.colVisitCostCalculationLevelDescription.FieldName = "VisitCostCalculationLevelDescription";
            this.colVisitCostCalculationLevelDescription.Name = "colVisitCostCalculationLevelDescription";
            this.colVisitCostCalculationLevelDescription.OptionsColumn.AllowEdit = false;
            this.colVisitCostCalculationLevelDescription.OptionsColumn.AllowFocus = false;
            this.colVisitCostCalculationLevelDescription.OptionsColumn.ReadOnly = true;
            this.colVisitCostCalculationLevelDescription.Width = 157;
            // 
            // colVisitSellCalculationLevelDescription
            // 
            this.colVisitSellCalculationLevelDescription.Caption = "Visit Sell Calculation Method";
            this.colVisitSellCalculationLevelDescription.FieldName = "VisitSellCalculationLevelDescription";
            this.colVisitSellCalculationLevelDescription.Name = "colVisitSellCalculationLevelDescription";
            this.colVisitSellCalculationLevelDescription.OptionsColumn.AllowEdit = false;
            this.colVisitSellCalculationLevelDescription.OptionsColumn.AllowFocus = false;
            this.colVisitSellCalculationLevelDescription.OptionsColumn.ReadOnly = true;
            this.colVisitSellCalculationLevelDescription.Width = 151;
            // 
            // colCreatedFromVisitTemplate
            // 
            this.colCreatedFromVisitTemplate.Caption = "Created From Visit Template";
            this.colCreatedFromVisitTemplate.FieldName = "CreatedFromVisitTemplate";
            this.colCreatedFromVisitTemplate.Name = "colCreatedFromVisitTemplate";
            this.colCreatedFromVisitTemplate.OptionsColumn.AllowEdit = false;
            this.colCreatedFromVisitTemplate.OptionsColumn.AllowFocus = false;
            this.colCreatedFromVisitTemplate.OptionsColumn.ReadOnly = true;
            this.colCreatedFromVisitTemplate.Visible = true;
            this.colCreatedFromVisitTemplate.VisibleIndex = 87;
            this.colCreatedFromVisitTemplate.Width = 154;
            // 
            // colCreatedFromVisitTemplateID
            // 
            this.colCreatedFromVisitTemplateID.Caption = "Created From Visit Template ID";
            this.colCreatedFromVisitTemplateID.FieldName = "CreatedFromVisitTemplateID";
            this.colCreatedFromVisitTemplateID.Name = "colCreatedFromVisitTemplateID";
            this.colCreatedFromVisitTemplateID.OptionsColumn.AllowEdit = false;
            this.colCreatedFromVisitTemplateID.OptionsColumn.AllowFocus = false;
            this.colCreatedFromVisitTemplateID.OptionsColumn.ReadOnly = true;
            this.colCreatedFromVisitTemplateID.Width = 168;
            // 
            // colDisplayOrder
            // 
            this.colDisplayOrder.Caption = "App Display Order";
            this.colDisplayOrder.FieldName = "DisplayOrder";
            this.colDisplayOrder.Name = "colDisplayOrder";
            this.colDisplayOrder.OptionsColumn.AllowEdit = false;
            this.colDisplayOrder.OptionsColumn.AllowFocus = false;
            this.colDisplayOrder.OptionsColumn.ReadOnly = true;
            this.colDisplayOrder.Visible = true;
            this.colDisplayOrder.VisibleIndex = 18;
            this.colDisplayOrder.Width = 119;
            // 
            // colMandatory
            // 
            this.colMandatory.Caption = "Mandatory";
            this.colMandatory.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colMandatory.FieldName = "Mandatory";
            this.colMandatory.Name = "colMandatory";
            this.colMandatory.OptionsColumn.AllowEdit = false;
            this.colMandatory.OptionsColumn.AllowFocus = false;
            this.colMandatory.OptionsColumn.ReadOnly = true;
            this.colMandatory.Visible = true;
            this.colMandatory.VisibleIndex = 17;
            // 
            // colSuspendedReason
            // 
            this.colSuspendedReason.Caption = "Suspended Reason";
            this.colSuspendedReason.FieldName = "SuspendedReason";
            this.colSuspendedReason.Name = "colSuspendedReason";
            this.colSuspendedReason.OptionsColumn.AllowEdit = false;
            this.colSuspendedReason.OptionsColumn.AllowFocus = false;
            this.colSuspendedReason.OptionsColumn.ReadOnly = true;
            this.colSuspendedReason.Visible = true;
            this.colSuspendedReason.VisibleIndex = 83;
            this.colSuspendedReason.Width = 111;
            // 
            // colSuspendedRemarks
            // 
            this.colSuspendedRemarks.Caption = "Suspended Remarks";
            this.colSuspendedRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colSuspendedRemarks.FieldName = "SuspendedRemarks";
            this.colSuspendedRemarks.Name = "colSuspendedRemarks";
            this.colSuspendedRemarks.OptionsColumn.ReadOnly = true;
            this.colSuspendedRemarks.Visible = true;
            this.colSuspendedRemarks.VisibleIndex = 84;
            this.colSuspendedRemarks.Width = 116;
            // 
            // toolTipController1
            // 
            this.toolTipController1.CloseOnClick = DevExpress.Utils.DefaultBoolean.True;
            this.toolTipController1.ToolTipType = DevExpress.Utils.ToolTipType.SuperTip;
            this.toolTipController1.GetActiveObjectInfo += new DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventHandler(this.toolTipController1_GetActiveObjectInfo);
            // 
            // dataSet_GC_Summer_Core
            // 
            this.dataSet_GC_Summer_Core.DataSetName = "DataSet_GC_Summer_Core";
            this.dataSet_GC_Summer_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_GC_Core
            // 
            this.dataSet_GC_Core.DataSetName = "DataSet_GC_Core";
            this.dataSet_GC_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(23, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl1.Panel1.Controls.Add(this.standaloneBarDockControl1);
            this.splitContainerControl1.Panel1.Text = "Jobs";
            this.splitContainerControl1.Panel2.Controls.Add(this.xtraTabControl1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1057, 547);
            this.splitContainerControl1.SplitterPosition = 213;
            this.splitContainerControl1.TabIndex = 5;
            this.splitContainerControl1.Text = "splitContainerControl1";
            this.splitContainerControl1.SplitGroupPanelCollapsed += new DevExpress.XtraEditors.SplitGroupPanelCollapsedEventHandler(this.splitContainerControl1_SplitGroupPanelCollapsed);
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSplitContainer1.Grid = this.gridControlJob;
            this.gridSplitContainer1.Location = new System.Drawing.Point(-1, 43);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.popupContainerControlDateRange);
            this.gridSplitContainer1.Panel1.Controls.Add(this.popupContainerControlRecordTypeFilter);
            this.gridSplitContainer1.Panel1.Controls.Add(this.popupContainerControlJobStatusFilter);
            this.gridSplitContainer1.Panel1.Controls.Add(this.popupContainerControlShowTabPages);
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControlJob);
            this.gridSplitContainer1.Size = new System.Drawing.Size(1053, 283);
            this.gridSplitContainer1.TabIndex = 19;
            // 
            // popupContainerControlDateRange
            // 
            this.popupContainerControlDateRange.Controls.Add(this.groupControl1);
            this.popupContainerControlDateRange.Controls.Add(this.btnDateRangeOK);
            this.popupContainerControlDateRange.Location = new System.Drawing.Point(486, 94);
            this.popupContainerControlDateRange.Name = "popupContainerControlDateRange";
            this.popupContainerControlDateRange.Size = new System.Drawing.Size(200, 107);
            this.popupContainerControlDateRange.TabIndex = 16;
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.dateEditFromDate);
            this.groupControl1.Controls.Add(this.dateEditToDate);
            this.groupControl1.Location = new System.Drawing.Point(3, 5);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(194, 76);
            this.groupControl1.TabIndex = 20;
            this.groupControl1.Text = "Date Range  [Expected Start Date]";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(6, 53);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(16, 13);
            this.labelControl2.TabIndex = 13;
            this.labelControl2.Text = "To:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(6, 27);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(28, 13);
            this.labelControl1.TabIndex = 12;
            this.labelControl1.Text = "From:";
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(40, 24);
            this.dateEditFromDate.MenuManager = this.barManager1;
            this.dateEditFromDate.Name = "dateEditFromDate";
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditFromDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditFromDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullValuePrompt = "No Date";
            this.dateEditFromDate.Size = new System.Drawing.Size(149, 20);
            this.dateEditFromDate.TabIndex = 10;
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(40, 50);
            this.dateEditToDate.MenuManager = this.barManager1;
            this.dateEditToDate.Name = "dateEditToDate";
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditToDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditToDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullValuePrompt = "No Date";
            this.dateEditToDate.Size = new System.Drawing.Size(149, 20);
            this.dateEditToDate.TabIndex = 11;
            // 
            // btnDateRangeOK
            // 
            this.btnDateRangeOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDateRangeOK.Location = new System.Drawing.Point(3, 84);
            this.btnDateRangeOK.Name = "btnDateRangeOK";
            this.btnDateRangeOK.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnDateRangeOK.Size = new System.Drawing.Size(50, 20);
            this.btnDateRangeOK.TabIndex = 19;
            this.btnDateRangeOK.Text = "OK";
            this.btnDateRangeOK.Click += new System.EventHandler(this.btnDateRangeOK_Click);
            // 
            // popupContainerControlRecordTypeFilter
            // 
            this.popupContainerControlRecordTypeFilter.Controls.Add(this.gridControl8);
            this.popupContainerControlRecordTypeFilter.Controls.Add(this.btnRecordTypeFilterOK);
            this.popupContainerControlRecordTypeFilter.Location = new System.Drawing.Point(184, 91);
            this.popupContainerControlRecordTypeFilter.Name = "popupContainerControlRecordTypeFilter";
            this.popupContainerControlRecordTypeFilter.Size = new System.Drawing.Size(151, 126);
            this.popupContainerControlRecordTypeFilter.TabIndex = 8;
            // 
            // gridControl8
            // 
            this.gridControl8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl8.DataSource = this.sp06018OMJobManagerRecordTypesBindingSource;
            this.gridControl8.Location = new System.Drawing.Point(3, 3);
            this.gridControl8.MainView = this.gridView8;
            this.gridControl8.MenuManager = this.barManager1;
            this.gridControl8.Name = "gridControl8";
            this.gridControl8.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit4});
            this.gridControl8.Size = new System.Drawing.Size(145, 99);
            this.gridControl8.TabIndex = 1;
            this.gridControl8.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView8});
            // 
            // sp06018OMJobManagerRecordTypesBindingSource
            // 
            this.sp06018OMJobManagerRecordTypesBindingSource.DataMember = "sp06018_OM_Job_Manager_Record_Types";
            this.sp06018OMJobManagerRecordTypesBindingSource.DataSource = this.dataSet_GC_Summer_Core;
            // 
            // gridView8
            // 
            this.gridView8.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID,
            this.colDescription2,
            this.colOrder1});
            this.gridView8.GridControl = this.gridControl8;
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView8.OptionsLayout.StoreAppearance = true;
            this.gridView8.OptionsView.AutoCalcPreviewLineCount = true;
            this.gridView8.OptionsView.ColumnAutoWidth = false;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            this.gridView8.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView8.OptionsView.ShowIndicator = false;
            this.gridView8.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder1, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colID
            // 
            this.colID.Caption = "ID";
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.OptionsColumn.AllowEdit = false;
            this.colID.OptionsColumn.AllowFocus = false;
            this.colID.OptionsColumn.ReadOnly = true;
            // 
            // colDescription2
            // 
            this.colDescription2.Caption = "Record Type";
            this.colDescription2.FieldName = "Description";
            this.colDescription2.Name = "colDescription2";
            this.colDescription2.OptionsColumn.AllowEdit = false;
            this.colDescription2.OptionsColumn.AllowFocus = false;
            this.colDescription2.OptionsColumn.ReadOnly = true;
            this.colDescription2.Visible = true;
            this.colDescription2.VisibleIndex = 0;
            this.colDescription2.Width = 143;
            // 
            // colOrder1
            // 
            this.colOrder1.Caption = "Order";
            this.colOrder1.FieldName = "Order";
            this.colOrder1.Name = "colOrder1";
            this.colOrder1.OptionsColumn.AllowEdit = false;
            this.colOrder1.OptionsColumn.AllowFocus = false;
            this.colOrder1.OptionsColumn.ReadOnly = true;
            this.colOrder1.Width = 63;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Caption = "Check";
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            // 
            // btnRecordTypeFilterOK
            // 
            this.btnRecordTypeFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRecordTypeFilterOK.Location = new System.Drawing.Point(3, 104);
            this.btnRecordTypeFilterOK.Name = "btnRecordTypeFilterOK";
            this.btnRecordTypeFilterOK.Size = new System.Drawing.Size(50, 20);
            this.btnRecordTypeFilterOK.TabIndex = 0;
            this.btnRecordTypeFilterOK.Text = "OK";
            this.btnRecordTypeFilterOK.Click += new System.EventHandler(this.btnRecordTypeFilterOK_Click);
            // 
            // popupContainerControlJobStatusFilter
            // 
            this.popupContainerControlJobStatusFilter.Controls.Add(this.btnJobStatusFilterOK);
            this.popupContainerControlJobStatusFilter.Controls.Add(this.gridControl3);
            this.popupContainerControlJobStatusFilter.Location = new System.Drawing.Point(341, 89);
            this.popupContainerControlJobStatusFilter.Name = "popupContainerControlJobStatusFilter";
            this.popupContainerControlJobStatusFilter.Size = new System.Drawing.Size(139, 126);
            this.popupContainerControlJobStatusFilter.TabIndex = 5;
            // 
            // btnJobStatusFilterOK
            // 
            this.btnJobStatusFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnJobStatusFilterOK.Location = new System.Drawing.Point(3, 103);
            this.btnJobStatusFilterOK.Name = "btnJobStatusFilterOK";
            this.btnJobStatusFilterOK.Size = new System.Drawing.Size(50, 20);
            this.btnJobStatusFilterOK.TabIndex = 12;
            this.btnJobStatusFilterOK.Text = "OK";
            this.btnJobStatusFilterOK.Click += new System.EventHandler(this.btnJobStatusFilterOK_Click);
            // 
            // gridControl3
            // 
            this.gridControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl3.DataSource = this.sp06025OMJobStatusesFilterBindingSource;
            this.gridControl3.Location = new System.Drawing.Point(3, 3);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.Size = new System.Drawing.Size(133, 98);
            this.gridControl3.TabIndex = 4;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp06025OMJobStatusesFilterBindingSource
            // 
            this.sp06025OMJobStatusesFilterBindingSource.DataMember = "sp06025_OM_Job_Statuses_Filter";
            this.sp06025OMJobStatusesFilterBindingSource.DataSource = this.dataSet_GC_Summer_Core;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription1,
            this.colID5,
            this.colRecordOrder});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colDescription1
            // 
            this.colDescription1.Caption = "Job Status";
            this.colDescription1.FieldName = "Description";
            this.colDescription1.Name = "colDescription1";
            this.colDescription1.OptionsColumn.AllowEdit = false;
            this.colDescription1.OptionsColumn.AllowFocus = false;
            this.colDescription1.OptionsColumn.ReadOnly = true;
            this.colDescription1.Visible = true;
            this.colDescription1.VisibleIndex = 0;
            this.colDescription1.Width = 215;
            // 
            // colID5
            // 
            this.colID5.Caption = "ID";
            this.colID5.FieldName = "ID";
            this.colID5.Name = "colID5";
            this.colID5.OptionsColumn.AllowEdit = false;
            this.colID5.OptionsColumn.AllowFocus = false;
            this.colID5.OptionsColumn.ReadOnly = true;
            // 
            // colRecordOrder
            // 
            this.colRecordOrder.Caption = "Order";
            this.colRecordOrder.FieldName = "RecordOrder";
            this.colRecordOrder.Name = "colRecordOrder";
            this.colRecordOrder.OptionsColumn.AllowEdit = false;
            this.colRecordOrder.OptionsColumn.AllowFocus = false;
            this.colRecordOrder.OptionsColumn.ReadOnly = true;
            // 
            // popupContainerControlShowTabPages
            // 
            this.popupContainerControlShowTabPages.Controls.Add(this.gridControl7);
            this.popupContainerControlShowTabPages.Controls.Add(this.btnShowTabPagesOK);
            this.popupContainerControlShowTabPages.Location = new System.Drawing.Point(11, 91);
            this.popupContainerControlShowTabPages.Name = "popupContainerControlShowTabPages";
            this.popupContainerControlShowTabPages.Size = new System.Drawing.Size(167, 125);
            this.popupContainerControlShowTabPages.TabIndex = 7;
            // 
            // gridControl7
            // 
            this.gridControl7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl7.DataSource = this.sp04022CoreDummyTabPageListBindingSource;
            this.gridControl7.Location = new System.Drawing.Point(3, 3);
            this.gridControl7.MainView = this.gridView7;
            this.gridControl7.MenuManager = this.barManager1;
            this.gridControl7.Name = "gridControl7";
            this.gridControl7.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit7});
            this.gridControl7.Size = new System.Drawing.Size(161, 98);
            this.gridControl7.TabIndex = 1;
            this.gridControl7.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView7});
            // 
            // sp04022CoreDummyTabPageListBindingSource
            // 
            this.sp04022CoreDummyTabPageListBindingSource.DataMember = "sp04022_Core_Dummy_TabPageList";
            this.sp04022CoreDummyTabPageListBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTabPageName});
            this.gridView7.GridControl = this.gridControl7;
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView7.OptionsLayout.StoreAppearance = true;
            this.gridView7.OptionsView.AutoCalcPreviewLineCount = true;
            this.gridView7.OptionsView.ColumnAutoWidth = false;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            this.gridView7.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView7.OptionsView.ShowIndicator = false;
            // 
            // colTabPageName
            // 
            this.colTabPageName.Caption = "Linked Records";
            this.colTabPageName.FieldName = "TabPageName";
            this.colTabPageName.Name = "colTabPageName";
            this.colTabPageName.OptionsColumn.AllowEdit = false;
            this.colTabPageName.OptionsColumn.AllowFocus = false;
            this.colTabPageName.OptionsColumn.ReadOnly = true;
            this.colTabPageName.Visible = true;
            this.colTabPageName.VisibleIndex = 0;
            this.colTabPageName.Width = 156;
            // 
            // repositoryItemCheckEdit7
            // 
            this.repositoryItemCheckEdit7.AutoHeight = false;
            this.repositoryItemCheckEdit7.Caption = "Check";
            this.repositoryItemCheckEdit7.Name = "repositoryItemCheckEdit7";
            // 
            // btnShowTabPagesOK
            // 
            this.btnShowTabPagesOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnShowTabPagesOK.Location = new System.Drawing.Point(3, 103);
            this.btnShowTabPagesOK.Name = "btnShowTabPagesOK";
            this.btnShowTabPagesOK.Size = new System.Drawing.Size(50, 20);
            this.btnShowTabPagesOK.TabIndex = 0;
            this.btnShowTabPagesOK.Text = "OK";
            this.btnShowTabPagesOK.Click += new System.EventHandler(this.btnShowTabPagesOK_Click);
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 1);
            this.standaloneBarDockControl1.Manager = this.barManager1;
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(1055, 42);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPageLabour;
            this.xtraTabControl1.Size = new System.Drawing.Size(1057, 213);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageLabour,
            this.xtraTabPageEquipment,
            this.xtraTabPageMaterials,
            this.xtraTabPageExtraInfo,
            this.xtraTabPageSpraying,
            this.xtraTabPageWaste,
            this.xtraTabPagePictures,
            this.xtraTabPageComments,
            this.xtraTabPageHealthAndSafety,
            this.xtraTabPageCRM,
            this.xtraTabPagePurchaseOrders});
            // 
            // xtraTabPageLabour
            // 
            this.xtraTabPageLabour.Controls.Add(this.splitContainerControl2);
            this.xtraTabPageLabour.Name = "xtraTabPageLabour";
            this.xtraTabPageLabour.Size = new System.Drawing.Size(1052, 187);
            this.xtraTabPageLabour.Text = "Labour";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.gridSplitContainer2);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl2.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl2.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl2.Panel2.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl2.Panel2.Controls.Add(this.gridSplitContainer5);
            this.splitContainerControl2.Panel2.ShowCaption = true;
            this.splitContainerControl2.Panel2.Text = "Times On \\ Off Site";
            this.splitContainerControl2.Size = new System.Drawing.Size(1052, 187);
            this.splitContainerControl2.SplitterPosition = 523;
            this.splitContainerControl2.TabIndex = 1;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer2.Grid = this.gridControlLabour;
            this.gridSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.gridControlLabour);
            this.gridSplitContainer2.Size = new System.Drawing.Size(523, 187);
            this.gridSplitContainer2.TabIndex = 0;
            // 
            // gridControlLabour
            // 
            this.gridControlLabour.DataSource = this.sp06033OMJobManagerLinkedlabourBindingSource;
            this.gridControlLabour.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlLabour.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlLabour.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlLabour.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 8, true, true, "Linked Documents", "linked_document")});
            this.gridControlLabour.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlLabour_EmbeddedNavigator_ButtonClick);
            this.gridControlLabour.Location = new System.Drawing.Point(0, 0);
            this.gridControlLabour.MainView = this.gridViewLabour;
            this.gridControlLabour.MenuManager = this.barManager1;
            this.gridControlLabour.Name = "gridControlLabour";
            this.gridControlLabour.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2,
            this.repositoryItemTextEditCurrency2,
            this.repositoryItemTextEditPercentage2,
            this.repositoryItemCheckEdit2,
            this.repositoryItemTextEditDateTime2,
            this.repositoryItemTextEdit2DP2,
            this.repositoryItemHyperLinkEditEmail,
            this.repositoryItemHyperLinkEditViewRecord,
            this.repositoryItemTextEditHTML2});
            this.gridControlLabour.Size = new System.Drawing.Size(523, 187);
            this.gridControlLabour.TabIndex = 0;
            this.gridControlLabour.UseEmbeddedNavigator = true;
            this.gridControlLabour.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewLabour});
            // 
            // sp06033OMJobManagerLinkedlabourBindingSource
            // 
            this.sp06033OMJobManagerLinkedlabourBindingSource.DataMember = "sp06033_OM_Job_Manager_Linked_labour";
            this.sp06033OMJobManagerLinkedlabourBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridViewLabour
            // 
            this.gridViewLabour.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLabourUsedID,
            this.colContractorID,
            this.colJobID1,
            this.colCostUnitsUsed,
            this.colCostUnitDescriptorID,
            this.colSellUnitsUsed,
            this.colSellUnitDescriptorID,
            this.colCostPerUnitExVat,
            this.colCostPerUnitVatRate,
            this.colSellPerUnitExVat,
            this.colSellPerUnitVatRate,
            this.colCostValueExVat,
            this.colCostValueVat,
            this.colCostValueTotal,
            this.colSellValueExVat,
            this.colSellValueVat,
            this.colSellValueTotal,
            this.colRemarks,
            this.colClientID,
            this.colSiteID,
            this.colSiteName1,
            this.colClientName1,
            this.colJobSubTypeDescription1,
            this.colJobTypeDescription2,
            this.colExpectedStartDate1,
            this.colVisitNumber1,
            this.colFullDescription,
            this.colContractorName,
            this.colSelected1,
            this.colCostUnitDescriptor,
            this.colSellUnitDescriptor,
            this.colInternalExternal,
            this.colCISPercentage,
            this.colCISValue,
            this.colPurchaseOrderID,
            this.colStartDateTime,
            this.colEndDateTime,
            this.colVisitID1,
            this.colLinkedToPersonType,
            this.colLinkedToPersonTypeID,
            this.colTelephone1,
            this.colTelephone2,
            this.colMobileTel,
            this.colEmail,
            this.colViewRecord});
            this.gridViewLabour.GridControl = this.gridControlLabour;
            this.gridViewLabour.GroupCount = 1;
            this.gridViewLabour.Name = "gridViewLabour";
            this.gridViewLabour.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewLabour.OptionsFind.FindDelay = 2000;
            this.gridViewLabour.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewLabour.OptionsLayout.StoreAppearance = true;
            this.gridViewLabour.OptionsLayout.StoreFormatRules = true;
            this.gridViewLabour.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewLabour.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewLabour.OptionsSelection.MultiSelect = true;
            this.gridViewLabour.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewLabour.OptionsView.ColumnAutoWidth = false;
            this.gridViewLabour.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewLabour.OptionsView.ShowGroupPanel = false;
            this.gridViewLabour.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colFullDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToPersonType, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colContractorName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewLabour.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewLabour_CustomRowCellEdit);
            this.gridViewLabour.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewLabour.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewLabour.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewLabour.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewLabour_ShowingEditor);
            this.gridViewLabour.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewLabour.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewLabour.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(this.gridViewLabour_CustomColumnDisplayText);
            this.gridViewLabour.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewLabour.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewLabour_MouseUp);
            this.gridViewLabour.DoubleClick += new System.EventHandler(this.gridViewLabour_DoubleClick);
            this.gridViewLabour.GotFocus += new System.EventHandler(this.gridViewLabour_GotFocus);
            // 
            // colLabourUsedID
            // 
            this.colLabourUsedID.Caption = "Labour Used ID";
            this.colLabourUsedID.FieldName = "LabourUsedID";
            this.colLabourUsedID.Name = "colLabourUsedID";
            this.colLabourUsedID.OptionsColumn.AllowEdit = false;
            this.colLabourUsedID.OptionsColumn.AllowFocus = false;
            this.colLabourUsedID.OptionsColumn.ReadOnly = true;
            this.colLabourUsedID.Width = 108;
            // 
            // colContractorID
            // 
            this.colContractorID.Caption = "Contractor ID";
            this.colContractorID.FieldName = "ContractorID";
            this.colContractorID.Name = "colContractorID";
            this.colContractorID.OptionsColumn.AllowEdit = false;
            this.colContractorID.OptionsColumn.AllowFocus = false;
            this.colContractorID.OptionsColumn.ReadOnly = true;
            // 
            // colJobID1
            // 
            this.colJobID1.Caption = "Job ID";
            this.colJobID1.FieldName = "JobID";
            this.colJobID1.Name = "colJobID1";
            this.colJobID1.OptionsColumn.AllowEdit = false;
            this.colJobID1.OptionsColumn.AllowFocus = false;
            this.colJobID1.OptionsColumn.ReadOnly = true;
            // 
            // colCostUnitsUsed
            // 
            this.colCostUnitsUsed.Caption = "Cost Units Used";
            this.colCostUnitsUsed.ColumnEdit = this.repositoryItemTextEdit2DP2;
            this.colCostUnitsUsed.FieldName = "CostUnitsUsed";
            this.colCostUnitsUsed.Name = "colCostUnitsUsed";
            this.colCostUnitsUsed.OptionsColumn.AllowEdit = false;
            this.colCostUnitsUsed.OptionsColumn.AllowFocus = false;
            this.colCostUnitsUsed.OptionsColumn.ReadOnly = true;
            this.colCostUnitsUsed.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostUnitsUsed", "{0:n2}")});
            this.colCostUnitsUsed.Visible = true;
            this.colCostUnitsUsed.VisibleIndex = 6;
            this.colCostUnitsUsed.Width = 97;
            // 
            // repositoryItemTextEdit2DP2
            // 
            this.repositoryItemTextEdit2DP2.AutoHeight = false;
            this.repositoryItemTextEdit2DP2.Mask.EditMask = "f2";
            this.repositoryItemTextEdit2DP2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP2.Name = "repositoryItemTextEdit2DP2";
            // 
            // colCostUnitDescriptorID
            // 
            this.colCostUnitDescriptorID.Caption = "Cost Unit Descriptor ID";
            this.colCostUnitDescriptorID.FieldName = "CostUnitDescriptorID";
            this.colCostUnitDescriptorID.Name = "colCostUnitDescriptorID";
            this.colCostUnitDescriptorID.OptionsColumn.AllowEdit = false;
            this.colCostUnitDescriptorID.OptionsColumn.AllowFocus = false;
            this.colCostUnitDescriptorID.OptionsColumn.ReadOnly = true;
            this.colCostUnitDescriptorID.Width = 131;
            // 
            // colSellUnitsUsed
            // 
            this.colSellUnitsUsed.Caption = "Sell Units Used";
            this.colSellUnitsUsed.ColumnEdit = this.repositoryItemTextEdit2DP2;
            this.colSellUnitsUsed.FieldName = "SellUnitsUsed";
            this.colSellUnitsUsed.Name = "colSellUnitsUsed";
            this.colSellUnitsUsed.OptionsColumn.AllowEdit = false;
            this.colSellUnitsUsed.OptionsColumn.AllowFocus = false;
            this.colSellUnitsUsed.OptionsColumn.ReadOnly = true;
            this.colSellUnitsUsed.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellUnitsUsed", "{0:n2}")});
            this.colSellUnitsUsed.Visible = true;
            this.colSellUnitsUsed.VisibleIndex = 12;
            this.colSellUnitsUsed.Width = 91;
            // 
            // colSellUnitDescriptorID
            // 
            this.colSellUnitDescriptorID.Caption = "Sell Unit Descriptor ID";
            this.colSellUnitDescriptorID.FieldName = "SellUnitDescriptorID";
            this.colSellUnitDescriptorID.Name = "colSellUnitDescriptorID";
            this.colSellUnitDescriptorID.OptionsColumn.AllowEdit = false;
            this.colSellUnitDescriptorID.OptionsColumn.AllowFocus = false;
            this.colSellUnitDescriptorID.OptionsColumn.ReadOnly = true;
            this.colSellUnitDescriptorID.Width = 125;
            // 
            // colCostPerUnitExVat
            // 
            this.colCostPerUnitExVat.Caption = "Cost Per Unit Ex VAT";
            this.colCostPerUnitExVat.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colCostPerUnitExVat.FieldName = "CostPerUnitExVat";
            this.colCostPerUnitExVat.Name = "colCostPerUnitExVat";
            this.colCostPerUnitExVat.OptionsColumn.AllowEdit = false;
            this.colCostPerUnitExVat.OptionsColumn.AllowFocus = false;
            this.colCostPerUnitExVat.OptionsColumn.ReadOnly = true;
            this.colCostPerUnitExVat.Visible = true;
            this.colCostPerUnitExVat.VisibleIndex = 7;
            this.colCostPerUnitExVat.Width = 121;
            // 
            // repositoryItemTextEditCurrency2
            // 
            this.repositoryItemTextEditCurrency2.AutoHeight = false;
            this.repositoryItemTextEditCurrency2.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency2.Name = "repositoryItemTextEditCurrency2";
            // 
            // colCostPerUnitVatRate
            // 
            this.colCostPerUnitVatRate.Caption = "Cost VAT Rate";
            this.colCostPerUnitVatRate.ColumnEdit = this.repositoryItemTextEditPercentage2;
            this.colCostPerUnitVatRate.FieldName = "CostPerUnitVatRate";
            this.colCostPerUnitVatRate.Name = "colCostPerUnitVatRate";
            this.colCostPerUnitVatRate.OptionsColumn.AllowEdit = false;
            this.colCostPerUnitVatRate.OptionsColumn.AllowFocus = false;
            this.colCostPerUnitVatRate.OptionsColumn.ReadOnly = true;
            this.colCostPerUnitVatRate.Visible = true;
            this.colCostPerUnitVatRate.VisibleIndex = 8;
            this.colCostPerUnitVatRate.Width = 91;
            // 
            // repositoryItemTextEditPercentage2
            // 
            this.repositoryItemTextEditPercentage2.AutoHeight = false;
            this.repositoryItemTextEditPercentage2.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage2.Name = "repositoryItemTextEditPercentage2";
            // 
            // colSellPerUnitExVat
            // 
            this.colSellPerUnitExVat.Caption = "Sell Per Unit Ex VAT";
            this.colSellPerUnitExVat.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colSellPerUnitExVat.FieldName = "SellPerUnitExVat";
            this.colSellPerUnitExVat.Name = "colSellPerUnitExVat";
            this.colSellPerUnitExVat.OptionsColumn.AllowEdit = false;
            this.colSellPerUnitExVat.OptionsColumn.AllowFocus = false;
            this.colSellPerUnitExVat.OptionsColumn.ReadOnly = true;
            this.colSellPerUnitExVat.Visible = true;
            this.colSellPerUnitExVat.VisibleIndex = 13;
            this.colSellPerUnitExVat.Width = 115;
            // 
            // colSellPerUnitVatRate
            // 
            this.colSellPerUnitVatRate.Caption = "Sell VAT Rate";
            this.colSellPerUnitVatRate.ColumnEdit = this.repositoryItemTextEditPercentage2;
            this.colSellPerUnitVatRate.FieldName = "SellPerUnitVatRate";
            this.colSellPerUnitVatRate.Name = "colSellPerUnitVatRate";
            this.colSellPerUnitVatRate.OptionsColumn.AllowEdit = false;
            this.colSellPerUnitVatRate.OptionsColumn.AllowFocus = false;
            this.colSellPerUnitVatRate.OptionsColumn.ReadOnly = true;
            this.colSellPerUnitVatRate.Visible = true;
            this.colSellPerUnitVatRate.VisibleIndex = 14;
            this.colSellPerUnitVatRate.Width = 85;
            // 
            // colCostValueExVat
            // 
            this.colCostValueExVat.Caption = "Cost Ex VAT";
            this.colCostValueExVat.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colCostValueExVat.FieldName = "CostValueExVat";
            this.colCostValueExVat.Name = "colCostValueExVat";
            this.colCostValueExVat.OptionsColumn.AllowEdit = false;
            this.colCostValueExVat.OptionsColumn.AllowFocus = false;
            this.colCostValueExVat.OptionsColumn.ReadOnly = true;
            this.colCostValueExVat.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostValueExVat", "{0:c}")});
            this.colCostValueExVat.Visible = true;
            this.colCostValueExVat.VisibleIndex = 9;
            this.colCostValueExVat.Width = 80;
            // 
            // colCostValueVat
            // 
            this.colCostValueVat.Caption = "Cost VAT";
            this.colCostValueVat.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colCostValueVat.FieldName = "CostValueVat";
            this.colCostValueVat.Name = "colCostValueVat";
            this.colCostValueVat.OptionsColumn.AllowEdit = false;
            this.colCostValueVat.OptionsColumn.AllowFocus = false;
            this.colCostValueVat.OptionsColumn.ReadOnly = true;
            this.colCostValueVat.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostValueVat", "{0:c}")});
            this.colCostValueVat.Visible = true;
            this.colCostValueVat.VisibleIndex = 10;
            // 
            // colCostValueTotal
            // 
            this.colCostValueTotal.Caption = "Cost Total";
            this.colCostValueTotal.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colCostValueTotal.FieldName = "CostValueTotal";
            this.colCostValueTotal.Name = "colCostValueTotal";
            this.colCostValueTotal.OptionsColumn.AllowEdit = false;
            this.colCostValueTotal.OptionsColumn.AllowFocus = false;
            this.colCostValueTotal.OptionsColumn.ReadOnly = true;
            this.colCostValueTotal.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostValueTotal", "{0:c}")});
            this.colCostValueTotal.Visible = true;
            this.colCostValueTotal.VisibleIndex = 11;
            // 
            // colSellValueExVat
            // 
            this.colSellValueExVat.Caption = "Sell Ex VAT";
            this.colSellValueExVat.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colSellValueExVat.FieldName = "SellValueExVat";
            this.colSellValueExVat.Name = "colSellValueExVat";
            this.colSellValueExVat.OptionsColumn.AllowEdit = false;
            this.colSellValueExVat.OptionsColumn.AllowFocus = false;
            this.colSellValueExVat.OptionsColumn.ReadOnly = true;
            this.colSellValueExVat.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellValueExVat", "{0:c}")});
            this.colSellValueExVat.Visible = true;
            this.colSellValueExVat.VisibleIndex = 15;
            // 
            // colSellValueVat
            // 
            this.colSellValueVat.Caption = "Sell VAT";
            this.colSellValueVat.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colSellValueVat.FieldName = "SellValueVat";
            this.colSellValueVat.Name = "colSellValueVat";
            this.colSellValueVat.OptionsColumn.AllowEdit = false;
            this.colSellValueVat.OptionsColumn.AllowFocus = false;
            this.colSellValueVat.OptionsColumn.ReadOnly = true;
            this.colSellValueVat.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellValueVat", "{0:c}")});
            this.colSellValueVat.Visible = true;
            this.colSellValueVat.VisibleIndex = 16;
            // 
            // colSellValueTotal
            // 
            this.colSellValueTotal.Caption = "Sell Total";
            this.colSellValueTotal.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colSellValueTotal.FieldName = "SellValueTotal";
            this.colSellValueTotal.Name = "colSellValueTotal";
            this.colSellValueTotal.OptionsColumn.AllowEdit = false;
            this.colSellValueTotal.OptionsColumn.AllowFocus = false;
            this.colSellValueTotal.OptionsColumn.ReadOnly = true;
            this.colSellValueTotal.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellValueTotal", "{0:c}")});
            this.colSellValueTotal.Visible = true;
            this.colSellValueTotal.VisibleIndex = 17;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 20;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // colSiteID
            // 
            this.colSiteID.Caption = "Site ID";
            this.colSiteID.FieldName = "SiteID";
            this.colSiteID.Name = "colSiteID";
            this.colSiteID.OptionsColumn.AllowEdit = false;
            this.colSiteID.OptionsColumn.AllowFocus = false;
            this.colSiteID.OptionsColumn.ReadOnly = true;
            // 
            // colSiteName1
            // 
            this.colSiteName1.Caption = "Site Name";
            this.colSiteName1.FieldName = "SiteName";
            this.colSiteName1.Name = "colSiteName1";
            this.colSiteName1.OptionsColumn.AllowEdit = false;
            this.colSiteName1.OptionsColumn.AllowFocus = false;
            this.colSiteName1.OptionsColumn.ReadOnly = true;
            this.colSiteName1.Width = 133;
            // 
            // colClientName1
            // 
            this.colClientName1.Caption = "Client Name";
            this.colClientName1.FieldName = "ClientName";
            this.colClientName1.Name = "colClientName1";
            this.colClientName1.OptionsColumn.AllowEdit = false;
            this.colClientName1.OptionsColumn.AllowFocus = false;
            this.colClientName1.OptionsColumn.ReadOnly = true;
            this.colClientName1.Width = 139;
            // 
            // colJobSubTypeDescription1
            // 
            this.colJobSubTypeDescription1.Caption = "Job Sub-Type";
            this.colJobSubTypeDescription1.FieldName = "JobSubTypeDescription";
            this.colJobSubTypeDescription1.Name = "colJobSubTypeDescription1";
            this.colJobSubTypeDescription1.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeDescription1.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeDescription1.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeDescription1.Width = 150;
            // 
            // colJobTypeDescription2
            // 
            this.colJobTypeDescription2.Caption = "Job Type";
            this.colJobTypeDescription2.FieldName = "JobTypeDescription";
            this.colJobTypeDescription2.Name = "colJobTypeDescription2";
            this.colJobTypeDescription2.OptionsColumn.AllowEdit = false;
            this.colJobTypeDescription2.OptionsColumn.AllowFocus = false;
            this.colJobTypeDescription2.OptionsColumn.ReadOnly = true;
            this.colJobTypeDescription2.Width = 169;
            // 
            // colExpectedStartDate1
            // 
            this.colExpectedStartDate1.Caption = "Expected Start Date";
            this.colExpectedStartDate1.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colExpectedStartDate1.FieldName = "ExpectedStartDate";
            this.colExpectedStartDate1.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colExpectedStartDate1.Name = "colExpectedStartDate1";
            this.colExpectedStartDate1.OptionsColumn.AllowEdit = false;
            this.colExpectedStartDate1.OptionsColumn.AllowFocus = false;
            this.colExpectedStartDate1.OptionsColumn.ReadOnly = true;
            this.colExpectedStartDate1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colExpectedStartDate1.Visible = true;
            this.colExpectedStartDate1.VisibleIndex = 3;
            this.colExpectedStartDate1.Width = 119;
            // 
            // repositoryItemTextEditDateTime2
            // 
            this.repositoryItemTextEditDateTime2.AutoHeight = false;
            this.repositoryItemTextEditDateTime2.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime2.Name = "repositoryItemTextEditDateTime2";
            // 
            // colVisitNumber1
            // 
            this.colVisitNumber1.Caption = "Visit #";
            this.colVisitNumber1.FieldName = "VisitNumber";
            this.colVisitNumber1.Name = "colVisitNumber1";
            this.colVisitNumber1.OptionsColumn.AllowEdit = false;
            this.colVisitNumber1.OptionsColumn.AllowFocus = false;
            this.colVisitNumber1.OptionsColumn.ReadOnly = true;
            // 
            // colFullDescription
            // 
            this.colFullDescription.Caption = "Linked To Job";
            this.colFullDescription.ColumnEdit = this.repositoryItemTextEditHTML2;
            this.colFullDescription.FieldName = "FullDescription";
            this.colFullDescription.Name = "colFullDescription";
            this.colFullDescription.OptionsColumn.AllowEdit = false;
            this.colFullDescription.OptionsColumn.AllowFocus = false;
            this.colFullDescription.OptionsColumn.ReadOnly = true;
            this.colFullDescription.Visible = true;
            this.colFullDescription.VisibleIndex = 18;
            this.colFullDescription.Width = 415;
            // 
            // repositoryItemTextEditHTML2
            // 
            this.repositoryItemTextEditHTML2.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTextEditHTML2.AutoHeight = false;
            this.repositoryItemTextEditHTML2.Name = "repositoryItemTextEditHTML2";
            // 
            // colContractorName
            // 
            this.colContractorName.Caption = "Labour Name";
            this.colContractorName.FieldName = "ContractorName";
            this.colContractorName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colContractorName.Name = "colContractorName";
            this.colContractorName.OptionsColumn.AllowEdit = false;
            this.colContractorName.OptionsColumn.AllowFocus = false;
            this.colContractorName.OptionsColumn.ReadOnly = true;
            this.colContractorName.Visible = true;
            this.colContractorName.VisibleIndex = 1;
            this.colContractorName.Width = 181;
            // 
            // colSelected1
            // 
            this.colSelected1.Caption = "Selected";
            this.colSelected1.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colSelected1.FieldName = "Selected";
            this.colSelected1.Name = "colSelected1";
            this.colSelected1.OptionsColumn.AllowEdit = false;
            this.colSelected1.OptionsColumn.AllowFocus = false;
            this.colSelected1.OptionsColumn.ReadOnly = true;
            this.colSelected1.Width = 62;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // colCostUnitDescriptor
            // 
            this.colCostUnitDescriptor.Caption = "Cost Unit Descriptor";
            this.colCostUnitDescriptor.FieldName = "CostUnitDescriptor";
            this.colCostUnitDescriptor.Name = "colCostUnitDescriptor";
            this.colCostUnitDescriptor.OptionsColumn.AllowEdit = false;
            this.colCostUnitDescriptor.OptionsColumn.AllowFocus = false;
            this.colCostUnitDescriptor.OptionsColumn.ReadOnly = true;
            this.colCostUnitDescriptor.Width = 117;
            // 
            // colSellUnitDescriptor
            // 
            this.colSellUnitDescriptor.Caption = "Sell Unit Descriptor";
            this.colSellUnitDescriptor.FieldName = "SellUnitDescriptor";
            this.colSellUnitDescriptor.Name = "colSellUnitDescriptor";
            this.colSellUnitDescriptor.OptionsColumn.AllowEdit = false;
            this.colSellUnitDescriptor.OptionsColumn.AllowFocus = false;
            this.colSellUnitDescriptor.OptionsColumn.ReadOnly = true;
            this.colSellUnitDescriptor.Width = 111;
            // 
            // colInternalExternal
            // 
            this.colInternalExternal.Caption = "Internal \\ External";
            this.colInternalExternal.FieldName = "InternalExternal";
            this.colInternalExternal.Name = "colInternalExternal";
            this.colInternalExternal.OptionsColumn.AllowEdit = false;
            this.colInternalExternal.OptionsColumn.AllowFocus = false;
            this.colInternalExternal.OptionsColumn.ReadOnly = true;
            this.colInternalExternal.Visible = true;
            this.colInternalExternal.VisibleIndex = 2;
            this.colInternalExternal.Width = 107;
            // 
            // colCISPercentage
            // 
            this.colCISPercentage.Caption = "CIS %";
            this.colCISPercentage.ColumnEdit = this.repositoryItemTextEditPercentage2;
            this.colCISPercentage.FieldName = "CISPercentage";
            this.colCISPercentage.Name = "colCISPercentage";
            this.colCISPercentage.OptionsColumn.AllowEdit = false;
            this.colCISPercentage.OptionsColumn.AllowFocus = false;
            this.colCISPercentage.OptionsColumn.ReadOnly = true;
            this.colCISPercentage.Visible = true;
            this.colCISPercentage.VisibleIndex = 18;
            // 
            // colCISValue
            // 
            this.colCISValue.Caption = "CIS Value";
            this.colCISValue.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colCISValue.FieldName = "CISValue";
            this.colCISValue.Name = "colCISValue";
            this.colCISValue.OptionsColumn.AllowEdit = false;
            this.colCISValue.OptionsColumn.AllowFocus = false;
            this.colCISValue.OptionsColumn.ReadOnly = true;
            this.colCISValue.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CISValue", "{0:c}")});
            this.colCISValue.Visible = true;
            this.colCISValue.VisibleIndex = 19;
            // 
            // colPurchaseOrderID
            // 
            this.colPurchaseOrderID.Caption = "PO ID";
            this.colPurchaseOrderID.FieldName = "PurchaseOrderID";
            this.colPurchaseOrderID.Name = "colPurchaseOrderID";
            this.colPurchaseOrderID.OptionsColumn.AllowEdit = false;
            this.colPurchaseOrderID.OptionsColumn.AllowFocus = false;
            this.colPurchaseOrderID.OptionsColumn.ReadOnly = true;
            this.colPurchaseOrderID.Width = 49;
            // 
            // colStartDateTime
            // 
            this.colStartDateTime.Caption = "Start Date\\Time";
            this.colStartDateTime.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colStartDateTime.FieldName = "StartDateTime";
            this.colStartDateTime.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colStartDateTime.Name = "colStartDateTime";
            this.colStartDateTime.OptionsColumn.AllowEdit = false;
            this.colStartDateTime.OptionsColumn.AllowFocus = false;
            this.colStartDateTime.OptionsColumn.ReadOnly = true;
            this.colStartDateTime.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colStartDateTime.Visible = true;
            this.colStartDateTime.VisibleIndex = 4;
            this.colStartDateTime.Width = 100;
            // 
            // colEndDateTime
            // 
            this.colEndDateTime.Caption = "End Date\\Time";
            this.colEndDateTime.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colEndDateTime.FieldName = "EndDateTime";
            this.colEndDateTime.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colEndDateTime.Name = "colEndDateTime";
            this.colEndDateTime.OptionsColumn.AllowEdit = false;
            this.colEndDateTime.OptionsColumn.AllowFocus = false;
            this.colEndDateTime.OptionsColumn.ReadOnly = true;
            this.colEndDateTime.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colEndDateTime.Visible = true;
            this.colEndDateTime.VisibleIndex = 5;
            this.colEndDateTime.Width = 100;
            // 
            // colVisitID1
            // 
            this.colVisitID1.Caption = "Visit ID";
            this.colVisitID1.FieldName = "VisitID";
            this.colVisitID1.Name = "colVisitID1";
            this.colVisitID1.OptionsColumn.AllowEdit = false;
            this.colVisitID1.OptionsColumn.AllowFocus = false;
            this.colVisitID1.OptionsColumn.ReadOnly = true;
            this.colVisitID1.Width = 54;
            // 
            // colLinkedToPersonType
            // 
            this.colLinkedToPersonType.Caption = "Labour Type";
            this.colLinkedToPersonType.FieldName = "LinkedToPersonType";
            this.colLinkedToPersonType.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colLinkedToPersonType.Name = "colLinkedToPersonType";
            this.colLinkedToPersonType.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonType.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonType.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonType.Visible = true;
            this.colLinkedToPersonType.VisibleIndex = 0;
            this.colLinkedToPersonType.Width = 115;
            // 
            // colLinkedToPersonTypeID
            // 
            this.colLinkedToPersonTypeID.Caption = "Labour Type ID";
            this.colLinkedToPersonTypeID.FieldName = "LinkedToPersonTypeID";
            this.colLinkedToPersonTypeID.Name = "colLinkedToPersonTypeID";
            this.colLinkedToPersonTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonTypeID.Width = 93;
            // 
            // colTelephone1
            // 
            this.colTelephone1.Caption = "Telephone 1";
            this.colTelephone1.FieldName = "Telephone1";
            this.colTelephone1.Name = "colTelephone1";
            this.colTelephone1.OptionsColumn.AllowEdit = false;
            this.colTelephone1.OptionsColumn.AllowFocus = false;
            this.colTelephone1.OptionsColumn.ReadOnly = true;
            this.colTelephone1.Visible = true;
            this.colTelephone1.VisibleIndex = 21;
            this.colTelephone1.Width = 80;
            // 
            // colTelephone2
            // 
            this.colTelephone2.Caption = "Telephone 2";
            this.colTelephone2.FieldName = "Telephone2";
            this.colTelephone2.Name = "colTelephone2";
            this.colTelephone2.OptionsColumn.AllowEdit = false;
            this.colTelephone2.OptionsColumn.AllowFocus = false;
            this.colTelephone2.OptionsColumn.ReadOnly = true;
            this.colTelephone2.Visible = true;
            this.colTelephone2.VisibleIndex = 22;
            this.colTelephone2.Width = 80;
            // 
            // colMobileTel
            // 
            this.colMobileTel.Caption = "Mobile Tel";
            this.colMobileTel.FieldName = "MobileTel";
            this.colMobileTel.Name = "colMobileTel";
            this.colMobileTel.OptionsColumn.AllowEdit = false;
            this.colMobileTel.OptionsColumn.AllowFocus = false;
            this.colMobileTel.OptionsColumn.ReadOnly = true;
            this.colMobileTel.Visible = true;
            this.colMobileTel.VisibleIndex = 23;
            this.colMobileTel.Width = 80;
            // 
            // colEmail
            // 
            this.colEmail.Caption = "Email";
            this.colEmail.ColumnEdit = this.repositoryItemHyperLinkEditEmail;
            this.colEmail.FieldName = "Email";
            this.colEmail.Name = "colEmail";
            this.colEmail.OptionsColumn.ReadOnly = true;
            this.colEmail.Visible = true;
            this.colEmail.VisibleIndex = 24;
            this.colEmail.Width = 220;
            // 
            // repositoryItemHyperLinkEditEmail
            // 
            this.repositoryItemHyperLinkEditEmail.AutoHeight = false;
            this.repositoryItemHyperLinkEditEmail.Name = "repositoryItemHyperLinkEditEmail";
            this.repositoryItemHyperLinkEditEmail.SingleClick = true;
            this.repositoryItemHyperLinkEditEmail.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditEmail_OpenLink);
            // 
            // colViewRecord
            // 
            this.colViewRecord.Caption = "View";
            this.colViewRecord.ColumnEdit = this.repositoryItemHyperLinkEditViewRecord;
            this.colViewRecord.FieldName = "ViewRecord";
            this.colViewRecord.Name = "colViewRecord";
            this.colViewRecord.OptionsColumn.ReadOnly = true;
            this.colViewRecord.Visible = true;
            this.colViewRecord.VisibleIndex = 25;
            this.colViewRecord.Width = 41;
            // 
            // repositoryItemHyperLinkEditViewRecord
            // 
            this.repositoryItemHyperLinkEditViewRecord.AutoHeight = false;
            this.repositoryItemHyperLinkEditViewRecord.Name = "repositoryItemHyperLinkEditViewRecord";
            this.repositoryItemHyperLinkEditViewRecord.SingleClick = true;
            this.repositoryItemHyperLinkEditViewRecord.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditViewRecord_OpenLink);
            // 
            // gridSplitContainer5
            // 
            this.gridSplitContainer5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer5.Grid = this.gridControlLabourTiming;
            this.gridSplitContainer5.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer5.Name = "gridSplitContainer5";
            this.gridSplitContainer5.Panel1.Controls.Add(this.gridControlLabourTiming);
            this.gridSplitContainer5.Size = new System.Drawing.Size(498, 184);
            this.gridSplitContainer5.TabIndex = 0;
            // 
            // gridControlLabourTiming
            // 
            this.gridControlLabourTiming.DataSource = this.sp06279OMLabourTimeOnOffSiteBindingSource;
            this.gridControlLabourTiming.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlLabourTiming.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlLabourTiming.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlLabourTiming.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlLabourTiming.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlLabourTiming.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlLabourTiming.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlLabourTiming.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlLabourTiming.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlLabourTiming.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlLabourTiming.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlLabourTiming.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlLabourTiming.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 13, true, true, "Block Add Records", "blockadd"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "View Selected Record(s)", "view")});
            this.gridControlLabourTiming.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlLabourTiming_EmbeddedNavigator_ButtonClick);
            this.gridControlLabourTiming.Location = new System.Drawing.Point(0, 0);
            this.gridControlLabourTiming.MainView = this.gridViewLabourTiming;
            this.gridControlLabourTiming.MenuManager = this.barManager1;
            this.gridControlLabourTiming.Name = "gridControlLabourTiming";
            this.gridControlLabourTiming.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit11,
            this.repositoryItemTextEditDateTime11,
            this.repositoryItemTextEdit3,
            this.repositoryItemCheckEdit15,
            this.repositoryItemTextEditHTML3});
            this.gridControlLabourTiming.Size = new System.Drawing.Size(498, 184);
            this.gridControlLabourTiming.TabIndex = 1;
            this.gridControlLabourTiming.UseEmbeddedNavigator = true;
            this.gridControlLabourTiming.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewLabourTiming});
            // 
            // sp06279OMLabourTimeOnOffSiteBindingSource
            // 
            this.sp06279OMLabourTimeOnOffSiteBindingSource.DataMember = "sp06279_OM_Labour_Time_On_Off_Site";
            this.sp06279OMLabourTimeOnOffSiteBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridViewLabourTiming
            // 
            this.gridViewLabourTiming.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLabourUsedTimingID,
            this.colLabourUsedID1,
            this.colTeamMemberID,
            this.colStartDateTime2,
            this.colEndDateTime2,
            this.colDuration,
            this.colRemarks1,
            this.colPdaCreatedID,
            this.colTeamMemberName,
            this.colContractorID1,
            this.colJobID2,
            this.colClientID3,
            this.colSiteID3,
            this.colSiteName3,
            this.colClientName3,
            this.colJobSubTypeDescription3,
            this.colJobTypeDescription4,
            this.colExpectedStartDate3,
            this.colVisitNumber3,
            this.colFullDescription2,
            this.colContractorName1,
            this.colVisitID4,
            this.colLinkedToPersonType1,
            this.colLinkedToPersonTypeID1});
            this.gridViewLabourTiming.GridControl = this.gridControlLabourTiming;
            this.gridViewLabourTiming.GroupCount = 1;
            this.gridViewLabourTiming.Name = "gridViewLabourTiming";
            this.gridViewLabourTiming.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewLabourTiming.OptionsFind.FindDelay = 2000;
            this.gridViewLabourTiming.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewLabourTiming.OptionsLayout.StoreAppearance = true;
            this.gridViewLabourTiming.OptionsLayout.StoreFormatRules = true;
            this.gridViewLabourTiming.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewLabourTiming.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewLabourTiming.OptionsSelection.MultiSelect = true;
            this.gridViewLabourTiming.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewLabourTiming.OptionsView.ColumnAutoWidth = false;
            this.gridViewLabourTiming.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewLabourTiming.OptionsView.ShowGroupPanel = false;
            this.gridViewLabourTiming.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colFullDescription2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToPersonType1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colContractorName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTeamMemberName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewLabourTiming.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewLabourTiming.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewLabourTiming.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewLabourTiming.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewLabourTiming.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewLabourTiming.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewLabourTiming.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewLabourTiming_MouseUp);
            this.gridViewLabourTiming.DoubleClick += new System.EventHandler(this.gridViewLabourTiming_DoubleClick);
            this.gridViewLabourTiming.GotFocus += new System.EventHandler(this.gridViewLabourTiming_GotFocus);
            // 
            // colLabourUsedTimingID
            // 
            this.colLabourUsedTimingID.Caption = "Labour Used Timing ID";
            this.colLabourUsedTimingID.FieldName = "LabourUsedTimingID";
            this.colLabourUsedTimingID.Name = "colLabourUsedTimingID";
            this.colLabourUsedTimingID.OptionsColumn.AllowEdit = false;
            this.colLabourUsedTimingID.OptionsColumn.AllowFocus = false;
            this.colLabourUsedTimingID.OptionsColumn.ReadOnly = true;
            this.colLabourUsedTimingID.Width = 128;
            // 
            // colLabourUsedID1
            // 
            this.colLabourUsedID1.Caption = "Labour Used ID";
            this.colLabourUsedID1.FieldName = "LabourUsedID";
            this.colLabourUsedID1.Name = "colLabourUsedID1";
            this.colLabourUsedID1.OptionsColumn.AllowEdit = false;
            this.colLabourUsedID1.OptionsColumn.AllowFocus = false;
            this.colLabourUsedID1.OptionsColumn.ReadOnly = true;
            this.colLabourUsedID1.Width = 95;
            // 
            // colTeamMemberID
            // 
            this.colTeamMemberID.Caption = "Team Member ID";
            this.colTeamMemberID.FieldName = "TeamMemberID";
            this.colTeamMemberID.Name = "colTeamMemberID";
            this.colTeamMemberID.OptionsColumn.AllowEdit = false;
            this.colTeamMemberID.OptionsColumn.AllowFocus = false;
            this.colTeamMemberID.OptionsColumn.ReadOnly = true;
            this.colTeamMemberID.Width = 102;
            // 
            // colStartDateTime2
            // 
            this.colStartDateTime2.Caption = "Start Date";
            this.colStartDateTime2.ColumnEdit = this.repositoryItemTextEditDateTime11;
            this.colStartDateTime2.FieldName = "StartDateTime";
            this.colStartDateTime2.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colStartDateTime2.Name = "colStartDateTime2";
            this.colStartDateTime2.OptionsColumn.AllowEdit = false;
            this.colStartDateTime2.OptionsColumn.AllowFocus = false;
            this.colStartDateTime2.OptionsColumn.ReadOnly = true;
            this.colStartDateTime2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colStartDateTime2.Visible = true;
            this.colStartDateTime2.VisibleIndex = 3;
            this.colStartDateTime2.Width = 100;
            // 
            // repositoryItemTextEditDateTime11
            // 
            this.repositoryItemTextEditDateTime11.AutoHeight = false;
            this.repositoryItemTextEditDateTime11.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime11.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime11.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime11.Name = "repositoryItemTextEditDateTime11";
            // 
            // colEndDateTime2
            // 
            this.colEndDateTime2.Caption = "End Date";
            this.colEndDateTime2.ColumnEdit = this.repositoryItemTextEditDateTime11;
            this.colEndDateTime2.FieldName = "EndDateTime";
            this.colEndDateTime2.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colEndDateTime2.Name = "colEndDateTime2";
            this.colEndDateTime2.OptionsColumn.AllowEdit = false;
            this.colEndDateTime2.OptionsColumn.AllowFocus = false;
            this.colEndDateTime2.OptionsColumn.ReadOnly = true;
            this.colEndDateTime2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colEndDateTime2.Visible = true;
            this.colEndDateTime2.VisibleIndex = 4;
            this.colEndDateTime2.Width = 100;
            // 
            // colDuration
            // 
            this.colDuration.Caption = "Duration";
            this.colDuration.ColumnEdit = this.repositoryItemTextEdit3;
            this.colDuration.FieldName = "Duration";
            this.colDuration.Name = "colDuration";
            this.colDuration.OptionsColumn.AllowEdit = false;
            this.colDuration.OptionsColumn.AllowFocus = false;
            this.colDuration.OptionsColumn.ReadOnly = true;
            this.colDuration.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Duration", "{0:######0.00 Hours}")});
            this.colDuration.Visible = true;
            this.colDuration.VisibleIndex = 5;
            this.colDuration.Width = 85;
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Mask.EditMask = "#####0.00 Hours";
            this.repositoryItemTextEdit3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit11;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 7;
            // 
            // repositoryItemMemoExEdit11
            // 
            this.repositoryItemMemoExEdit11.AutoHeight = false;
            this.repositoryItemMemoExEdit11.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit11.Name = "repositoryItemMemoExEdit11";
            this.repositoryItemMemoExEdit11.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit11.ShowIcon = false;
            // 
            // colPdaCreatedID
            // 
            this.colPdaCreatedID.Caption = "Device Created";
            this.colPdaCreatedID.ColumnEdit = this.repositoryItemCheckEdit15;
            this.colPdaCreatedID.FieldName = "PdaCreatedID";
            this.colPdaCreatedID.Name = "colPdaCreatedID";
            this.colPdaCreatedID.OptionsColumn.AllowEdit = false;
            this.colPdaCreatedID.OptionsColumn.AllowFocus = false;
            this.colPdaCreatedID.OptionsColumn.ReadOnly = true;
            this.colPdaCreatedID.Visible = true;
            this.colPdaCreatedID.VisibleIndex = 6;
            this.colPdaCreatedID.Width = 83;
            // 
            // repositoryItemCheckEdit15
            // 
            this.repositoryItemCheckEdit15.AutoHeight = false;
            this.repositoryItemCheckEdit15.Caption = "Check";
            this.repositoryItemCheckEdit15.Name = "repositoryItemCheckEdit15";
            this.repositoryItemCheckEdit15.ValueChecked = 1;
            this.repositoryItemCheckEdit15.ValueUnchecked = 0;
            // 
            // colTeamMemberName
            // 
            this.colTeamMemberName.Caption = "Team Member Name";
            this.colTeamMemberName.FieldName = "TeamMemberName";
            this.colTeamMemberName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colTeamMemberName.Name = "colTeamMemberName";
            this.colTeamMemberName.OptionsColumn.AllowEdit = false;
            this.colTeamMemberName.OptionsColumn.AllowFocus = false;
            this.colTeamMemberName.OptionsColumn.ReadOnly = true;
            this.colTeamMemberName.Visible = true;
            this.colTeamMemberName.VisibleIndex = 2;
            this.colTeamMemberName.Width = 133;
            // 
            // colContractorID1
            // 
            this.colContractorID1.Caption = "Contractor ID";
            this.colContractorID1.FieldName = "ContractorID";
            this.colContractorID1.Name = "colContractorID1";
            this.colContractorID1.OptionsColumn.AllowEdit = false;
            this.colContractorID1.OptionsColumn.AllowFocus = false;
            this.colContractorID1.OptionsColumn.ReadOnly = true;
            this.colContractorID1.Width = 87;
            // 
            // colJobID2
            // 
            this.colJobID2.Caption = "Job ID";
            this.colJobID2.FieldName = "JobID";
            this.colJobID2.Name = "colJobID2";
            this.colJobID2.OptionsColumn.AllowEdit = false;
            this.colJobID2.OptionsColumn.AllowFocus = false;
            this.colJobID2.OptionsColumn.ReadOnly = true;
            // 
            // colClientID3
            // 
            this.colClientID3.Caption = "Client ID";
            this.colClientID3.FieldName = "ClientID";
            this.colClientID3.Name = "colClientID3";
            this.colClientID3.OptionsColumn.AllowEdit = false;
            this.colClientID3.OptionsColumn.AllowFocus = false;
            this.colClientID3.OptionsColumn.ReadOnly = true;
            // 
            // colSiteID3
            // 
            this.colSiteID3.Caption = "Site ID";
            this.colSiteID3.FieldName = "SiteID";
            this.colSiteID3.Name = "colSiteID3";
            this.colSiteID3.OptionsColumn.AllowEdit = false;
            this.colSiteID3.OptionsColumn.AllowFocus = false;
            this.colSiteID3.OptionsColumn.ReadOnly = true;
            // 
            // colSiteName3
            // 
            this.colSiteName3.Caption = "Site Name";
            this.colSiteName3.FieldName = "SiteName";
            this.colSiteName3.Name = "colSiteName3";
            this.colSiteName3.OptionsColumn.AllowEdit = false;
            this.colSiteName3.OptionsColumn.AllowFocus = false;
            this.colSiteName3.OptionsColumn.ReadOnly = true;
            this.colSiteName3.Width = 188;
            // 
            // colClientName3
            // 
            this.colClientName3.Caption = "Client Name";
            this.colClientName3.FieldName = "ClientName";
            this.colClientName3.Name = "colClientName3";
            this.colClientName3.OptionsColumn.AllowEdit = false;
            this.colClientName3.OptionsColumn.AllowFocus = false;
            this.colClientName3.OptionsColumn.ReadOnly = true;
            this.colClientName3.Width = 177;
            // 
            // colJobSubTypeDescription3
            // 
            this.colJobSubTypeDescription3.Caption = "Job Sub-Type Description";
            this.colJobSubTypeDescription3.FieldName = "JobSubTypeDescription";
            this.colJobSubTypeDescription3.Name = "colJobSubTypeDescription3";
            this.colJobSubTypeDescription3.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeDescription3.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeDescription3.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeDescription3.Width = 154;
            // 
            // colJobTypeDescription4
            // 
            this.colJobTypeDescription4.Caption = "Job Type Description";
            this.colJobTypeDescription4.FieldName = "JobTypeDescription";
            this.colJobTypeDescription4.Name = "colJobTypeDescription4";
            this.colJobTypeDescription4.OptionsColumn.AllowEdit = false;
            this.colJobTypeDescription4.OptionsColumn.AllowFocus = false;
            this.colJobTypeDescription4.OptionsColumn.ReadOnly = true;
            this.colJobTypeDescription4.Width = 145;
            // 
            // colExpectedStartDate3
            // 
            this.colExpectedStartDate3.Caption = "Job Expected Start Date";
            this.colExpectedStartDate3.ColumnEdit = this.repositoryItemTextEditDateTime11;
            this.colExpectedStartDate3.FieldName = "ExpectedStartDate";
            this.colExpectedStartDate3.Name = "colExpectedStartDate3";
            this.colExpectedStartDate3.OptionsColumn.AllowEdit = false;
            this.colExpectedStartDate3.OptionsColumn.AllowFocus = false;
            this.colExpectedStartDate3.OptionsColumn.ReadOnly = true;
            this.colExpectedStartDate3.Width = 139;
            // 
            // colVisitNumber3
            // 
            this.colVisitNumber3.Caption = "Visit Number";
            this.colVisitNumber3.FieldName = "VisitNumber";
            this.colVisitNumber3.Name = "colVisitNumber3";
            this.colVisitNumber3.OptionsColumn.AllowEdit = false;
            this.colVisitNumber3.OptionsColumn.AllowFocus = false;
            this.colVisitNumber3.OptionsColumn.ReadOnly = true;
            this.colVisitNumber3.Width = 80;
            // 
            // colFullDescription2
            // 
            this.colFullDescription2.Caption = "Linked To";
            this.colFullDescription2.ColumnEdit = this.repositoryItemTextEditHTML3;
            this.colFullDescription2.FieldName = "FullDescription";
            this.colFullDescription2.Name = "colFullDescription2";
            this.colFullDescription2.OptionsColumn.AllowEdit = false;
            this.colFullDescription2.OptionsColumn.AllowFocus = false;
            this.colFullDescription2.OptionsColumn.ReadOnly = true;
            this.colFullDescription2.Visible = true;
            this.colFullDescription2.VisibleIndex = 8;
            this.colFullDescription2.Width = 341;
            // 
            // repositoryItemTextEditHTML3
            // 
            this.repositoryItemTextEditHTML3.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTextEditHTML3.AutoHeight = false;
            this.repositoryItemTextEditHTML3.Name = "repositoryItemTextEditHTML3";
            // 
            // colContractorName1
            // 
            this.colContractorName1.Caption = "Labour Name";
            this.colContractorName1.FieldName = "ContractorName";
            this.colContractorName1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colContractorName1.Name = "colContractorName1";
            this.colContractorName1.OptionsColumn.AllowEdit = false;
            this.colContractorName1.OptionsColumn.AllowFocus = false;
            this.colContractorName1.OptionsColumn.ReadOnly = true;
            this.colContractorName1.Visible = true;
            this.colContractorName1.VisibleIndex = 1;
            this.colContractorName1.Width = 131;
            // 
            // colVisitID4
            // 
            this.colVisitID4.Caption = "Visit ID";
            this.colVisitID4.FieldName = "VisitID";
            this.colVisitID4.Name = "colVisitID4";
            this.colVisitID4.OptionsColumn.AllowEdit = false;
            this.colVisitID4.OptionsColumn.AllowFocus = false;
            this.colVisitID4.OptionsColumn.ReadOnly = true;
            this.colVisitID4.Width = 57;
            // 
            // colLinkedToPersonType1
            // 
            this.colLinkedToPersonType1.Caption = "Labour Type";
            this.colLinkedToPersonType1.FieldName = "LinkedToPersonType";
            this.colLinkedToPersonType1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colLinkedToPersonType1.Name = "colLinkedToPersonType1";
            this.colLinkedToPersonType1.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonType1.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonType1.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonType1.Visible = true;
            this.colLinkedToPersonType1.VisibleIndex = 0;
            this.colLinkedToPersonType1.Width = 92;
            // 
            // colLinkedToPersonTypeID1
            // 
            this.colLinkedToPersonTypeID1.Caption = "Labour Type ID";
            this.colLinkedToPersonTypeID1.FieldName = "LinkedToPersonTypeID";
            this.colLinkedToPersonTypeID1.Name = "colLinkedToPersonTypeID1";
            this.colLinkedToPersonTypeID1.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonTypeID1.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonTypeID1.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonTypeID1.Width = 93;
            // 
            // xtraTabPageEquipment
            // 
            this.xtraTabPageEquipment.Controls.Add(this.popupContainerControlMaterialTypeFilter);
            this.xtraTabPageEquipment.Controls.Add(this.popupContainerControlJobTypeFilter);
            this.xtraTabPageEquipment.Controls.Add(this.popupContainerControlEquipmentTypeFilter);
            this.xtraTabPageEquipment.Controls.Add(this.popupContainerControlJobSubTypeFilter);
            this.xtraTabPageEquipment.Controls.Add(this.gridControlEquipment);
            this.xtraTabPageEquipment.Name = "xtraTabPageEquipment";
            this.xtraTabPageEquipment.Size = new System.Drawing.Size(1052, 187);
            this.xtraTabPageEquipment.Text = "Equipment";
            // 
            // popupContainerControlMaterialTypeFilter
            // 
            this.popupContainerControlMaterialTypeFilter.Controls.Add(this.btnMaterialTypeFilter);
            this.popupContainerControlMaterialTypeFilter.Controls.Add(this.gridControl14);
            this.popupContainerControlMaterialTypeFilter.Location = new System.Drawing.Point(521, 29);
            this.popupContainerControlMaterialTypeFilter.Name = "popupContainerControlMaterialTypeFilter";
            this.popupContainerControlMaterialTypeFilter.Size = new System.Drawing.Size(139, 126);
            this.popupContainerControlMaterialTypeFilter.TabIndex = 21;
            // 
            // btnMaterialTypeFilter
            // 
            this.btnMaterialTypeFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnMaterialTypeFilter.Location = new System.Drawing.Point(3, 103);
            this.btnMaterialTypeFilter.Name = "btnMaterialTypeFilter";
            this.btnMaterialTypeFilter.Size = new System.Drawing.Size(50, 20);
            this.btnMaterialTypeFilter.TabIndex = 12;
            this.btnMaterialTypeFilter.Text = "OK";
            this.btnMaterialTypeFilter.Click += new System.EventHandler(this.btnMaterialTypeFilterOK_Click);
            // 
            // gridControl14
            // 
            this.gridControl14.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl14.DataSource = this.sp06030OMMaterialFilterBindingSource;
            this.gridControl14.Location = new System.Drawing.Point(3, 3);
            this.gridControl14.MainView = this.gridView14;
            this.gridControl14.MenuManager = this.barManager1;
            this.gridControl14.Name = "gridControl14";
            this.gridControl14.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit2DP3});
            this.gridControl14.Size = new System.Drawing.Size(133, 98);
            this.gridControl14.TabIndex = 4;
            this.gridControl14.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView14});
            // 
            // sp06030OMMaterialFilterBindingSource
            // 
            this.sp06030OMMaterialFilterBindingSource.DataMember = "sp06030_OM_Material_Filter";
            this.sp06030OMMaterialFilterBindingSource.DataSource = this.dataSet_GC_Summer_Core;
            // 
            // gridView14
            // 
            this.gridView14.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID7,
            this.colDescription7,
            this.colUnitDescriptorID,
            this.colUnitsInStock,
            this.colReorderLevel,
            this.colWarningLevel,
            this.colRecordOrder3,
            this.colUnitDescriptor});
            this.gridView14.GridControl = this.gridControl14;
            this.gridView14.Name = "gridView14";
            this.gridView14.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView14.OptionsFind.AlwaysVisible = true;
            this.gridView14.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView14.OptionsLayout.StoreAppearance = true;
            this.gridView14.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView14.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView14.OptionsView.ColumnAutoWidth = false;
            this.gridView14.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView14.OptionsView.ShowGroupPanel = false;
            this.gridView14.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView14.OptionsView.ShowIndicator = false;
            this.gridView14.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder3, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colID7
            // 
            this.colID7.Caption = "ID";
            this.colID7.FieldName = "ID";
            this.colID7.Name = "colID7";
            this.colID7.OptionsColumn.AllowEdit = false;
            this.colID7.OptionsColumn.AllowFocus = false;
            this.colID7.OptionsColumn.ReadOnly = true;
            // 
            // colDescription7
            // 
            this.colDescription7.Caption = "Material";
            this.colDescription7.FieldName = "Description";
            this.colDescription7.Name = "colDescription7";
            this.colDescription7.OptionsColumn.AllowEdit = false;
            this.colDescription7.OptionsColumn.AllowFocus = false;
            this.colDescription7.OptionsColumn.ReadOnly = true;
            this.colDescription7.Visible = true;
            this.colDescription7.VisibleIndex = 0;
            this.colDescription7.Width = 200;
            // 
            // colUnitDescriptorID
            // 
            this.colUnitDescriptorID.Caption = "Unit Descriptor ID";
            this.colUnitDescriptorID.FieldName = "UnitDescriptorID";
            this.colUnitDescriptorID.Name = "colUnitDescriptorID";
            this.colUnitDescriptorID.OptionsColumn.AllowEdit = false;
            this.colUnitDescriptorID.OptionsColumn.AllowFocus = false;
            this.colUnitDescriptorID.OptionsColumn.ReadOnly = true;
            this.colUnitDescriptorID.Width = 106;
            // 
            // colUnitsInStock
            // 
            this.colUnitsInStock.Caption = "Units In Stock";
            this.colUnitsInStock.ColumnEdit = this.repositoryItemTextEdit2DP3;
            this.colUnitsInStock.FieldName = "UnitsInStock";
            this.colUnitsInStock.Name = "colUnitsInStock";
            this.colUnitsInStock.OptionsColumn.AllowEdit = false;
            this.colUnitsInStock.OptionsColumn.AllowFocus = false;
            this.colUnitsInStock.OptionsColumn.ReadOnly = true;
            this.colUnitsInStock.Visible = true;
            this.colUnitsInStock.VisibleIndex = 2;
            this.colUnitsInStock.Width = 87;
            // 
            // repositoryItemTextEdit2DP3
            // 
            this.repositoryItemTextEdit2DP3.AutoHeight = false;
            this.repositoryItemTextEdit2DP3.Mask.EditMask = "######0.00";
            this.repositoryItemTextEdit2DP3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP3.Name = "repositoryItemTextEdit2DP3";
            // 
            // colReorderLevel
            // 
            this.colReorderLevel.Caption = "Reorder Level";
            this.colReorderLevel.ColumnEdit = this.repositoryItemTextEdit2DP3;
            this.colReorderLevel.FieldName = "ReorderLevel";
            this.colReorderLevel.Name = "colReorderLevel";
            this.colReorderLevel.OptionsColumn.AllowEdit = false;
            this.colReorderLevel.OptionsColumn.AllowFocus = false;
            this.colReorderLevel.OptionsColumn.ReadOnly = true;
            this.colReorderLevel.Visible = true;
            this.colReorderLevel.VisibleIndex = 3;
            this.colReorderLevel.Width = 88;
            // 
            // colWarningLevel
            // 
            this.colWarningLevel.Caption = "Warning Level";
            this.colWarningLevel.ColumnEdit = this.repositoryItemTextEdit2DP3;
            this.colWarningLevel.FieldName = "WarningLevel";
            this.colWarningLevel.Name = "colWarningLevel";
            this.colWarningLevel.OptionsColumn.AllowEdit = false;
            this.colWarningLevel.OptionsColumn.AllowFocus = false;
            this.colWarningLevel.OptionsColumn.ReadOnly = true;
            this.colWarningLevel.Visible = true;
            this.colWarningLevel.VisibleIndex = 4;
            this.colWarningLevel.Width = 89;
            // 
            // colRecordOrder3
            // 
            this.colRecordOrder3.Caption = "Order";
            this.colRecordOrder3.FieldName = "RecordOrder";
            this.colRecordOrder3.Name = "colRecordOrder3";
            this.colRecordOrder3.OptionsColumn.AllowEdit = false;
            this.colRecordOrder3.OptionsColumn.AllowFocus = false;
            this.colRecordOrder3.OptionsColumn.ReadOnly = true;
            // 
            // colUnitDescriptor
            // 
            this.colUnitDescriptor.Caption = "Unit Descriptor";
            this.colUnitDescriptor.FieldName = "UnitDescriptor";
            this.colUnitDescriptor.Name = "colUnitDescriptor";
            this.colUnitDescriptor.OptionsColumn.AllowEdit = false;
            this.colUnitDescriptor.OptionsColumn.AllowFocus = false;
            this.colUnitDescriptor.OptionsColumn.ReadOnly = true;
            this.colUnitDescriptor.Visible = true;
            this.colUnitDescriptor.VisibleIndex = 1;
            this.colUnitDescriptor.Width = 92;
            // 
            // popupContainerControlJobTypeFilter
            // 
            this.popupContainerControlJobTypeFilter.Controls.Add(this.btnJobTypeFilterOK);
            this.popupContainerControlJobTypeFilter.Controls.Add(this.gridControl10);
            this.popupContainerControlJobTypeFilter.Location = new System.Drawing.Point(88, 29);
            this.popupContainerControlJobTypeFilter.Name = "popupContainerControlJobTypeFilter";
            this.popupContainerControlJobTypeFilter.Size = new System.Drawing.Size(139, 126);
            this.popupContainerControlJobTypeFilter.TabIndex = 18;
            // 
            // btnJobTypeFilterOK
            // 
            this.btnJobTypeFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnJobTypeFilterOK.Location = new System.Drawing.Point(3, 103);
            this.btnJobTypeFilterOK.Name = "btnJobTypeFilterOK";
            this.btnJobTypeFilterOK.Size = new System.Drawing.Size(50, 20);
            this.btnJobTypeFilterOK.TabIndex = 12;
            this.btnJobTypeFilterOK.Text = "OK";
            this.btnJobTypeFilterOK.Click += new System.EventHandler(this.btnJobTypeFilterOK_Click);
            // 
            // gridControl10
            // 
            this.gridControl10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl10.DataSource = this.sp06026OMJobTypeFilterBindingSource;
            this.gridControl10.Location = new System.Drawing.Point(3, 3);
            this.gridControl10.MainView = this.gridView10;
            this.gridControl10.MenuManager = this.barManager1;
            this.gridControl10.Name = "gridControl10";
            this.gridControl10.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit6});
            this.gridControl10.Size = new System.Drawing.Size(133, 98);
            this.gridControl10.TabIndex = 4;
            this.gridControl10.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView10});
            // 
            // sp06026OMJobTypeFilterBindingSource
            // 
            this.sp06026OMJobTypeFilterBindingSource.DataMember = "sp06026_OM_Job_Type_Filter";
            this.sp06026OMJobTypeFilterBindingSource.DataSource = this.dataSet_GC_Summer_Core;
            // 
            // gridView10
            // 
            this.gridView10.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID2,
            this.colDescription4,
            this.colDisabled});
            styleFormatCondition3.Appearance.ForeColor = System.Drawing.Color.Gray;
            styleFormatCondition3.Appearance.Options.UseForeColor = true;
            styleFormatCondition3.ApplyToRow = true;
            styleFormatCondition3.Column = this.colDisabled;
            styleFormatCondition3.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition3.Value1 = "1";
            this.gridView10.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition3});
            this.gridView10.GridControl = this.gridControl10;
            this.gridView10.Name = "gridView10";
            this.gridView10.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView10.OptionsFind.AlwaysVisible = true;
            this.gridView10.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView10.OptionsLayout.StoreAppearance = true;
            this.gridView10.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView10.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView10.OptionsView.ColumnAutoWidth = false;
            this.gridView10.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView10.OptionsView.ShowGroupPanel = false;
            this.gridView10.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView10.OptionsView.ShowIndicator = false;
            this.gridView10.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription4, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colID2
            // 
            this.colID2.Caption = "ID";
            this.colID2.FieldName = "ID";
            this.colID2.Name = "colID2";
            this.colID2.OptionsColumn.AllowEdit = false;
            this.colID2.OptionsColumn.AllowFocus = false;
            this.colID2.OptionsColumn.ReadOnly = true;
            this.colID2.Width = 57;
            // 
            // colDescription4
            // 
            this.colDescription4.Caption = "Job Type";
            this.colDescription4.FieldName = "Description";
            this.colDescription4.Name = "colDescription4";
            this.colDescription4.OptionsColumn.AllowEdit = false;
            this.colDescription4.OptionsColumn.AllowFocus = false;
            this.colDescription4.OptionsColumn.ReadOnly = true;
            this.colDescription4.Visible = true;
            this.colDescription4.VisibleIndex = 0;
            this.colDescription4.Width = 200;
            // 
            // popupContainerControlEquipmentTypeFilter
            // 
            this.popupContainerControlEquipmentTypeFilter.Controls.Add(this.btnEquipmentFilter);
            this.popupContainerControlEquipmentTypeFilter.Controls.Add(this.gridControl13);
            this.popupContainerControlEquipmentTypeFilter.Location = new System.Drawing.Point(379, 29);
            this.popupContainerControlEquipmentTypeFilter.Name = "popupContainerControlEquipmentTypeFilter";
            this.popupContainerControlEquipmentTypeFilter.Size = new System.Drawing.Size(139, 126);
            this.popupContainerControlEquipmentTypeFilter.TabIndex = 20;
            // 
            // btnEquipmentFilter
            // 
            this.btnEquipmentFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnEquipmentFilter.Location = new System.Drawing.Point(3, 103);
            this.btnEquipmentFilter.Name = "btnEquipmentFilter";
            this.btnEquipmentFilter.Size = new System.Drawing.Size(50, 20);
            this.btnEquipmentFilter.TabIndex = 12;
            this.btnEquipmentFilter.Text = "OK";
            this.btnEquipmentFilter.Click += new System.EventHandler(this.btnEquipmentTypeFilterOK_Click);
            // 
            // gridControl13
            // 
            this.gridControl13.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl13.DataSource = this.sp06029OMEquipmentTypeFilterBindingSource;
            this.gridControl13.Location = new System.Drawing.Point(3, 3);
            this.gridControl13.MainView = this.gridView13;
            this.gridControl13.MenuManager = this.barManager1;
            this.gridControl13.Name = "gridControl13";
            this.gridControl13.Size = new System.Drawing.Size(133, 98);
            this.gridControl13.TabIndex = 4;
            this.gridControl13.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView13});
            // 
            // sp06029OMEquipmentTypeFilterBindingSource
            // 
            this.sp06029OMEquipmentTypeFilterBindingSource.DataMember = "sp06029_OM_Equipment_Type_Filter";
            this.sp06029OMEquipmentTypeFilterBindingSource.DataSource = this.dataSet_GC_Summer_Core;
            // 
            // gridView13
            // 
            this.gridView13.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID6,
            this.colDescription6,
            this.colRecordOrder2});
            this.gridView13.GridControl = this.gridControl13;
            this.gridView13.Name = "gridView13";
            this.gridView13.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView13.OptionsFind.AlwaysVisible = true;
            this.gridView13.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView13.OptionsLayout.StoreAppearance = true;
            this.gridView13.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView13.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView13.OptionsView.ColumnAutoWidth = false;
            this.gridView13.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView13.OptionsView.ShowGroupPanel = false;
            this.gridView13.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView13.OptionsView.ShowIndicator = false;
            this.gridView13.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder2, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colID6
            // 
            this.colID6.Caption = "ID";
            this.colID6.FieldName = "ID";
            this.colID6.Name = "colID6";
            this.colID6.OptionsColumn.AllowEdit = false;
            this.colID6.OptionsColumn.AllowFocus = false;
            this.colID6.OptionsColumn.ReadOnly = true;
            // 
            // colDescription6
            // 
            this.colDescription6.Caption = "Equipment Type";
            this.colDescription6.FieldName = "Description";
            this.colDescription6.Name = "colDescription6";
            this.colDescription6.OptionsColumn.AllowEdit = false;
            this.colDescription6.OptionsColumn.AllowFocus = false;
            this.colDescription6.OptionsColumn.ReadOnly = true;
            this.colDescription6.Visible = true;
            this.colDescription6.VisibleIndex = 0;
            this.colDescription6.Width = 200;
            // 
            // colRecordOrder2
            // 
            this.colRecordOrder2.Caption = "Order";
            this.colRecordOrder2.FieldName = "RecordOrder";
            this.colRecordOrder2.Name = "colRecordOrder2";
            this.colRecordOrder2.OptionsColumn.AllowEdit = false;
            this.colRecordOrder2.OptionsColumn.AllowFocus = false;
            this.colRecordOrder2.OptionsColumn.ReadOnly = true;
            // 
            // popupContainerControlJobSubTypeFilter
            // 
            this.popupContainerControlJobSubTypeFilter.Controls.Add(this.btnJobSubTypeFilterOK);
            this.popupContainerControlJobSubTypeFilter.Controls.Add(this.gridControl11);
            this.popupContainerControlJobSubTypeFilter.Location = new System.Drawing.Point(233, 29);
            this.popupContainerControlJobSubTypeFilter.Name = "popupContainerControlJobSubTypeFilter";
            this.popupContainerControlJobSubTypeFilter.Size = new System.Drawing.Size(140, 126);
            this.popupContainerControlJobSubTypeFilter.TabIndex = 19;
            // 
            // btnJobSubTypeFilterOK
            // 
            this.btnJobSubTypeFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnJobSubTypeFilterOK.Location = new System.Drawing.Point(3, 103);
            this.btnJobSubTypeFilterOK.Name = "btnJobSubTypeFilterOK";
            this.btnJobSubTypeFilterOK.Size = new System.Drawing.Size(50, 20);
            this.btnJobSubTypeFilterOK.TabIndex = 12;
            this.btnJobSubTypeFilterOK.Text = "OK";
            this.btnJobSubTypeFilterOK.Click += new System.EventHandler(this.btnJobSubTypeFilterOK_Click);
            // 
            // gridControl11
            // 
            this.gridControl11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl11.DataSource = this.sp06027OMJobSubTypeFilterBindingSource;
            this.gridControl11.Location = new System.Drawing.Point(3, 3);
            this.gridControl11.MainView = this.gridView11;
            this.gridControl11.MenuManager = this.barManager1;
            this.gridControl11.Name = "gridControl11";
            this.gridControl11.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit8});
            this.gridControl11.Size = new System.Drawing.Size(134, 98);
            this.gridControl11.TabIndex = 4;
            this.gridControl11.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView11});
            // 
            // sp06027OMJobSubTypeFilterBindingSource
            // 
            this.sp06027OMJobSubTypeFilterBindingSource.DataMember = "sp06027_OM_Job_Sub_Type_Filter";
            this.sp06027OMJobSubTypeFilterBindingSource.DataSource = this.dataSet_GC_Summer_Core;
            // 
            // gridView11
            // 
            this.gridView11.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID3,
            this.colDescription5,
            this.colRecordOrder1,
            this.colJobTypeID,
            this.colJobTypeDescription,
            this.colDisabled2});
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Gray;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.colDisabled2;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 1;
            this.gridView11.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridView11.GridControl = this.gridControl11;
            this.gridView11.GroupCount = 1;
            this.gridView11.Name = "gridView11";
            this.gridView11.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView11.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView11.OptionsFind.AlwaysVisible = true;
            this.gridView11.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView11.OptionsLayout.StoreAppearance = true;
            this.gridView11.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView11.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView11.OptionsView.ColumnAutoWidth = false;
            this.gridView11.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView11.OptionsView.ShowGroupPanel = false;
            this.gridView11.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView11.OptionsView.ShowIndicator = false;
            this.gridView11.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colJobTypeDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder1, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colID3
            // 
            this.colID3.Caption = "Job Sub-Type ID";
            this.colID3.FieldName = "ID";
            this.colID3.Name = "colID3";
            this.colID3.OptionsColumn.AllowEdit = false;
            this.colID3.OptionsColumn.AllowFocus = false;
            this.colID3.OptionsColumn.ReadOnly = true;
            this.colID3.Width = 101;
            // 
            // colDescription5
            // 
            this.colDescription5.Caption = "Job Sub-Type";
            this.colDescription5.FieldName = "Description";
            this.colDescription5.Name = "colDescription5";
            this.colDescription5.OptionsColumn.AllowEdit = false;
            this.colDescription5.OptionsColumn.AllowFocus = false;
            this.colDescription5.OptionsColumn.ReadOnly = true;
            this.colDescription5.Visible = true;
            this.colDescription5.VisibleIndex = 0;
            this.colDescription5.Width = 200;
            // 
            // colRecordOrder1
            // 
            this.colRecordOrder1.Caption = "Job Sub-Type Order";
            this.colRecordOrder1.FieldName = "RecordOrder";
            this.colRecordOrder1.Name = "colRecordOrder1";
            this.colRecordOrder1.OptionsColumn.AllowEdit = false;
            this.colRecordOrder1.OptionsColumn.AllowFocus = false;
            this.colRecordOrder1.OptionsColumn.ReadOnly = true;
            this.colRecordOrder1.Width = 131;
            // 
            // colJobTypeID
            // 
            this.colJobTypeID.Caption = "Job Type ID";
            this.colJobTypeID.FieldName = "JobTypeID";
            this.colJobTypeID.Name = "colJobTypeID";
            this.colJobTypeID.OptionsColumn.AllowEdit = false;
            this.colJobTypeID.OptionsColumn.AllowFocus = false;
            this.colJobTypeID.OptionsColumn.ReadOnly = true;
            this.colJobTypeID.Width = 79;
            // 
            // colJobTypeDescription
            // 
            this.colJobTypeDescription.Caption = "Job Type";
            this.colJobTypeDescription.FieldName = "JobTypeDescription";
            this.colJobTypeDescription.Name = "colJobTypeDescription";
            this.colJobTypeDescription.OptionsColumn.AllowEdit = false;
            this.colJobTypeDescription.OptionsColumn.AllowFocus = false;
            this.colJobTypeDescription.OptionsColumn.ReadOnly = true;
            this.colJobTypeDescription.Visible = true;
            this.colJobTypeDescription.VisibleIndex = 1;
            this.colJobTypeDescription.Width = 200;
            // 
            // gridControlEquipment
            // 
            this.gridControlEquipment.DataSource = this.sp06034OMJobManagerLinkedEquipmentBindingSource;
            this.gridControlEquipment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlEquipment.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlEquipment.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlEquipment.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlEquipment.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlEquipment.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlEquipment.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlEquipment.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlEquipment.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlEquipment.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlEquipment.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlEquipment.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlEquipment.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 8, true, true, "Linked Documents", "linked_document"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 15, true, true, "Linked Pictures", "linked_pictures")});
            this.gridControlEquipment.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlEquipment_EmbeddedNavigator_ButtonClick);
            this.gridControlEquipment.Location = new System.Drawing.Point(0, 0);
            this.gridControlEquipment.MainView = this.gridViewEquipment;
            this.gridControlEquipment.MenuManager = this.barManager1;
            this.gridControlEquipment.Name = "gridControlEquipment";
            this.gridControlEquipment.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4,
            this.repositoryItemTextEditCurrency3,
            this.repositoryItemTextEditPercentage3,
            this.repositoryItemCheckEdit9,
            this.repositoryItemTextEditDateTime3,
            this.repositoryItemTextEdit2DP4,
            this.repositoryItemTextEditHTML4,
            this.repositoryItemHyperLinkEditEquipmentPictures});
            this.gridControlEquipment.Size = new System.Drawing.Size(1052, 187);
            this.gridControlEquipment.TabIndex = 1;
            this.gridControlEquipment.UseEmbeddedNavigator = true;
            this.gridControlEquipment.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewEquipment});
            // 
            // sp06034OMJobManagerLinkedEquipmentBindingSource
            // 
            this.sp06034OMJobManagerLinkedEquipmentBindingSource.DataMember = "sp06034_OM_Job_Manager_Linked_Equipment";
            this.sp06034OMJobManagerLinkedEquipmentBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridViewEquipment
            // 
            this.gridViewEquipment.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn4,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27,
            this.gridColumn28,
            this.gridColumn29,
            this.gridColumn30,
            this.gridColumn31,
            this.gridColumn32,
            this.gridColumn158,
            this.gridColumn159,
            this.gridColumn160,
            this.gridColumn161,
            this.gridColumn162,
            this.gridColumn163,
            this.gridColumn164,
            this.gridColumn165,
            this.colOwnerName,
            this.colOwnerType,
            this.colStartDateTime1,
            this.colEndDateTime1,
            this.colPurchaseOrderID2,
            this.colVisitID3,
            this.colLinkedPictureCount1,
            this.colImagesFolderOM1});
            this.gridViewEquipment.GridControl = this.gridControlEquipment;
            this.gridViewEquipment.GroupCount = 1;
            this.gridViewEquipment.Name = "gridViewEquipment";
            this.gridViewEquipment.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewEquipment.OptionsFind.FindDelay = 2000;
            this.gridViewEquipment.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewEquipment.OptionsLayout.StoreAppearance = true;
            this.gridViewEquipment.OptionsLayout.StoreFormatRules = true;
            this.gridViewEquipment.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewEquipment.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewEquipment.OptionsSelection.MultiSelect = true;
            this.gridViewEquipment.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewEquipment.OptionsView.ColumnAutoWidth = false;
            this.gridViewEquipment.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewEquipment.OptionsView.ShowGroupPanel = false;
            this.gridViewEquipment.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn161, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn162, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewEquipment.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewEquipment_CustomRowCellEdit);
            this.gridViewEquipment.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewEquipment.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewEquipment.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewEquipment.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewEquipment_ShowingEditor);
            this.gridViewEquipment.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewEquipment.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewEquipment.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(this.gridViewEquipment_CustomColumnDisplayText);
            this.gridViewEquipment.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewEquipment.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewEquipment_MouseUp);
            this.gridViewEquipment.DoubleClick += new System.EventHandler(this.gridViewEquipment_DoubleClick);
            this.gridViewEquipment.GotFocus += new System.EventHandler(this.gridViewEquipment_GotFocus);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Equipment Used ID";
            this.gridColumn1.FieldName = "EquipmentUsedID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Width = 108;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Equipment ID";
            this.gridColumn2.FieldName = "EquipmentID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Job ID";
            this.gridColumn4.FieldName = "JobID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Cost Units Used";
            this.gridColumn13.ColumnEdit = this.repositoryItemTextEdit2DP4;
            this.gridColumn13.FieldName = "CostUnitsUsed";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostUnitsUsed", "{0:n2}")});
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 5;
            this.gridColumn13.Width = 97;
            // 
            // repositoryItemTextEdit2DP4
            // 
            this.repositoryItemTextEdit2DP4.AutoHeight = false;
            this.repositoryItemTextEdit2DP4.Mask.EditMask = "f2";
            this.repositoryItemTextEdit2DP4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP4.Name = "repositoryItemTextEdit2DP4";
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Cost Unit Descriptor ID";
            this.gridColumn14.FieldName = "CostUnitDescriptorID";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Width = 131;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Sell Units Used";
            this.gridColumn15.ColumnEdit = this.repositoryItemTextEdit2DP4;
            this.gridColumn15.FieldName = "SellUnitsUsed";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellUnitsUsed", "{0:n2}")});
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 11;
            this.gridColumn15.Width = 91;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Sell Unit Descriptor ID";
            this.gridColumn16.FieldName = "SellUnitDescriptorID";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowFocus = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.Width = 125;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Cost Per Unit Ex VAT";
            this.gridColumn17.ColumnEdit = this.repositoryItemTextEditCurrency3;
            this.gridColumn17.FieldName = "CostPerUnitExVat";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.AllowFocus = false;
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 6;
            this.gridColumn17.Width = 121;
            // 
            // repositoryItemTextEditCurrency3
            // 
            this.repositoryItemTextEditCurrency3.AutoHeight = false;
            this.repositoryItemTextEditCurrency3.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency3.Name = "repositoryItemTextEditCurrency3";
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Cost VAT Rate";
            this.gridColumn18.ColumnEdit = this.repositoryItemTextEditPercentage3;
            this.gridColumn18.FieldName = "CostPerUnitVatRate";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.AllowFocus = false;
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 7;
            this.gridColumn18.Width = 91;
            // 
            // repositoryItemTextEditPercentage3
            // 
            this.repositoryItemTextEditPercentage3.AutoHeight = false;
            this.repositoryItemTextEditPercentage3.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage3.Name = "repositoryItemTextEditPercentage3";
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Sell Per Unit Ex VAT";
            this.gridColumn19.ColumnEdit = this.repositoryItemTextEditCurrency3;
            this.gridColumn19.FieldName = "SellPerUnitExVat";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.OptionsColumn.AllowFocus = false;
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 12;
            this.gridColumn19.Width = 115;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Sell VAT Rate";
            this.gridColumn20.ColumnEdit = this.repositoryItemTextEditPercentage3;
            this.gridColumn20.FieldName = "SellPerUnitVatRate";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.AllowEdit = false;
            this.gridColumn20.OptionsColumn.AllowFocus = false;
            this.gridColumn20.OptionsColumn.ReadOnly = true;
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 13;
            this.gridColumn20.Width = 85;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Cost Ex VAT";
            this.gridColumn21.ColumnEdit = this.repositoryItemTextEditCurrency3;
            this.gridColumn21.FieldName = "CostValueExVat";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.AllowEdit = false;
            this.gridColumn21.OptionsColumn.AllowFocus = false;
            this.gridColumn21.OptionsColumn.ReadOnly = true;
            this.gridColumn21.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostValueExVat", "{0:c}")});
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 8;
            this.gridColumn21.Width = 80;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Cost VAT";
            this.gridColumn22.ColumnEdit = this.repositoryItemTextEditCurrency3;
            this.gridColumn22.FieldName = "CostValueVat";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.AllowEdit = false;
            this.gridColumn22.OptionsColumn.AllowFocus = false;
            this.gridColumn22.OptionsColumn.ReadOnly = true;
            this.gridColumn22.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostValueVat", "{0:c}")});
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 9;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Cost Total";
            this.gridColumn23.ColumnEdit = this.repositoryItemTextEditCurrency3;
            this.gridColumn23.FieldName = "CostValueTotal";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.AllowEdit = false;
            this.gridColumn23.OptionsColumn.AllowFocus = false;
            this.gridColumn23.OptionsColumn.ReadOnly = true;
            this.gridColumn23.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostValueTotal", "{0:c}")});
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 10;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Sell Ex VAT";
            this.gridColumn24.ColumnEdit = this.repositoryItemTextEditCurrency3;
            this.gridColumn24.FieldName = "SellValueExVat";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.AllowEdit = false;
            this.gridColumn24.OptionsColumn.AllowFocus = false;
            this.gridColumn24.OptionsColumn.ReadOnly = true;
            this.gridColumn24.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellValueExVat", "{0:c}")});
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 14;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Sell VAT";
            this.gridColumn25.ColumnEdit = this.repositoryItemTextEditCurrency3;
            this.gridColumn25.FieldName = "SellValueVat";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.OptionsColumn.AllowEdit = false;
            this.gridColumn25.OptionsColumn.AllowFocus = false;
            this.gridColumn25.OptionsColumn.ReadOnly = true;
            this.gridColumn25.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellValueVat", "{0:c}")});
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 15;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Sell Total";
            this.gridColumn26.ColumnEdit = this.repositoryItemTextEditCurrency3;
            this.gridColumn26.FieldName = "SellValueTotal";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.OptionsColumn.AllowEdit = false;
            this.gridColumn26.OptionsColumn.AllowFocus = false;
            this.gridColumn26.OptionsColumn.ReadOnly = true;
            this.gridColumn26.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellValueTotal", "{0:c}")});
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 16;
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Remarks";
            this.gridColumn27.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.gridColumn27.FieldName = "Remarks";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.OptionsColumn.ReadOnly = true;
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 18;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "Client ID";
            this.gridColumn28.FieldName = "ClientID";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.OptionsColumn.AllowEdit = false;
            this.gridColumn28.OptionsColumn.AllowFocus = false;
            this.gridColumn28.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "Site ID";
            this.gridColumn29.FieldName = "SiteID";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.OptionsColumn.AllowEdit = false;
            this.gridColumn29.OptionsColumn.AllowFocus = false;
            this.gridColumn29.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "Site Name";
            this.gridColumn30.FieldName = "SiteName";
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.OptionsColumn.AllowEdit = false;
            this.gridColumn30.OptionsColumn.AllowFocus = false;
            this.gridColumn30.OptionsColumn.ReadOnly = true;
            this.gridColumn30.Width = 133;
            // 
            // gridColumn31
            // 
            this.gridColumn31.Caption = "Client Name";
            this.gridColumn31.FieldName = "ClientName";
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.OptionsColumn.AllowEdit = false;
            this.gridColumn31.OptionsColumn.AllowFocus = false;
            this.gridColumn31.OptionsColumn.ReadOnly = true;
            this.gridColumn31.Width = 139;
            // 
            // gridColumn32
            // 
            this.gridColumn32.Caption = "Job Sub-Type";
            this.gridColumn32.FieldName = "JobSubTypeDescription";
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.OptionsColumn.AllowEdit = false;
            this.gridColumn32.OptionsColumn.AllowFocus = false;
            this.gridColumn32.OptionsColumn.ReadOnly = true;
            this.gridColumn32.Width = 150;
            // 
            // gridColumn158
            // 
            this.gridColumn158.Caption = "Job Type";
            this.gridColumn158.FieldName = "JobTypeDescription";
            this.gridColumn158.Name = "gridColumn158";
            this.gridColumn158.OptionsColumn.AllowEdit = false;
            this.gridColumn158.OptionsColumn.AllowFocus = false;
            this.gridColumn158.OptionsColumn.ReadOnly = true;
            this.gridColumn158.Width = 169;
            // 
            // gridColumn159
            // 
            this.gridColumn159.Caption = "Expected Start Date";
            this.gridColumn159.ColumnEdit = this.repositoryItemTextEditDateTime3;
            this.gridColumn159.FieldName = "ExpectedStartDate";
            this.gridColumn159.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn159.Name = "gridColumn159";
            this.gridColumn159.OptionsColumn.AllowEdit = false;
            this.gridColumn159.OptionsColumn.AllowFocus = false;
            this.gridColumn159.OptionsColumn.ReadOnly = true;
            this.gridColumn159.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn159.Width = 119;
            // 
            // repositoryItemTextEditDateTime3
            // 
            this.repositoryItemTextEditDateTime3.AutoHeight = false;
            this.repositoryItemTextEditDateTime3.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime3.Name = "repositoryItemTextEditDateTime3";
            // 
            // gridColumn160
            // 
            this.gridColumn160.Caption = "Visit #";
            this.gridColumn160.FieldName = "VisitNumber";
            this.gridColumn160.Name = "gridColumn160";
            this.gridColumn160.OptionsColumn.AllowEdit = false;
            this.gridColumn160.OptionsColumn.AllowFocus = false;
            this.gridColumn160.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn161
            // 
            this.gridColumn161.Caption = "Linked To Job";
            this.gridColumn161.ColumnEdit = this.repositoryItemTextEditHTML4;
            this.gridColumn161.FieldName = "FullDescription";
            this.gridColumn161.Name = "gridColumn161";
            this.gridColumn161.OptionsColumn.AllowEdit = false;
            this.gridColumn161.OptionsColumn.AllowFocus = false;
            this.gridColumn161.OptionsColumn.ReadOnly = true;
            this.gridColumn161.Visible = true;
            this.gridColumn161.VisibleIndex = 18;
            this.gridColumn161.Width = 415;
            // 
            // repositoryItemTextEditHTML4
            // 
            this.repositoryItemTextEditHTML4.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTextEditHTML4.AutoHeight = false;
            this.repositoryItemTextEditHTML4.Name = "repositoryItemTextEditHTML4";
            // 
            // gridColumn162
            // 
            this.gridColumn162.Caption = "Equipment Type";
            this.gridColumn162.FieldName = "EquipmentType";
            this.gridColumn162.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn162.Name = "gridColumn162";
            this.gridColumn162.OptionsColumn.AllowEdit = false;
            this.gridColumn162.OptionsColumn.AllowFocus = false;
            this.gridColumn162.OptionsColumn.ReadOnly = true;
            this.gridColumn162.Visible = true;
            this.gridColumn162.VisibleIndex = 0;
            this.gridColumn162.Width = 136;
            // 
            // gridColumn163
            // 
            this.gridColumn163.Caption = "Selected";
            this.gridColumn163.ColumnEdit = this.repositoryItemCheckEdit9;
            this.gridColumn163.FieldName = "Selected";
            this.gridColumn163.Name = "gridColumn163";
            this.gridColumn163.OptionsColumn.AllowEdit = false;
            this.gridColumn163.OptionsColumn.AllowFocus = false;
            this.gridColumn163.OptionsColumn.ReadOnly = true;
            this.gridColumn163.Width = 62;
            // 
            // repositoryItemCheckEdit9
            // 
            this.repositoryItemCheckEdit9.AutoHeight = false;
            this.repositoryItemCheckEdit9.Caption = "Check";
            this.repositoryItemCheckEdit9.Name = "repositoryItemCheckEdit9";
            this.repositoryItemCheckEdit9.ValueChecked = 1;
            this.repositoryItemCheckEdit9.ValueUnchecked = 0;
            // 
            // gridColumn164
            // 
            this.gridColumn164.Caption = "Cost Unit Descriptor";
            this.gridColumn164.FieldName = "CostUnitDescriptor";
            this.gridColumn164.Name = "gridColumn164";
            this.gridColumn164.OptionsColumn.AllowEdit = false;
            this.gridColumn164.OptionsColumn.AllowFocus = false;
            this.gridColumn164.OptionsColumn.ReadOnly = true;
            this.gridColumn164.Width = 117;
            // 
            // gridColumn165
            // 
            this.gridColumn165.Caption = "Sell Unit Descriptor";
            this.gridColumn165.FieldName = "SellUnitDescriptor";
            this.gridColumn165.Name = "gridColumn165";
            this.gridColumn165.OptionsColumn.AllowEdit = false;
            this.gridColumn165.OptionsColumn.AllowFocus = false;
            this.gridColumn165.OptionsColumn.ReadOnly = true;
            this.gridColumn165.Width = 111;
            // 
            // colOwnerName
            // 
            this.colOwnerName.Caption = "Owner Name";
            this.colOwnerName.FieldName = "OwnerName";
            this.colOwnerName.Name = "colOwnerName";
            this.colOwnerName.OptionsColumn.AllowEdit = false;
            this.colOwnerName.OptionsColumn.AllowFocus = false;
            this.colOwnerName.OptionsColumn.ReadOnly = true;
            this.colOwnerName.Visible = true;
            this.colOwnerName.VisibleIndex = 1;
            this.colOwnerName.Width = 133;
            // 
            // colOwnerType
            // 
            this.colOwnerType.Caption = "Owner Type";
            this.colOwnerType.FieldName = "OwnerType";
            this.colOwnerType.Name = "colOwnerType";
            this.colOwnerType.OptionsColumn.AllowEdit = false;
            this.colOwnerType.OptionsColumn.AllowFocus = false;
            this.colOwnerType.OptionsColumn.ReadOnly = true;
            this.colOwnerType.Visible = true;
            this.colOwnerType.VisibleIndex = 2;
            this.colOwnerType.Width = 80;
            // 
            // colStartDateTime1
            // 
            this.colStartDateTime1.Caption = "Start Date\\Time";
            this.colStartDateTime1.ColumnEdit = this.repositoryItemTextEditDateTime3;
            this.colStartDateTime1.FieldName = "StartDateTime";
            this.colStartDateTime1.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colStartDateTime1.Name = "colStartDateTime1";
            this.colStartDateTime1.OptionsColumn.AllowEdit = false;
            this.colStartDateTime1.OptionsColumn.AllowFocus = false;
            this.colStartDateTime1.OptionsColumn.ReadOnly = true;
            this.colStartDateTime1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colStartDateTime1.Visible = true;
            this.colStartDateTime1.VisibleIndex = 3;
            this.colStartDateTime1.Width = 100;
            // 
            // colEndDateTime1
            // 
            this.colEndDateTime1.Caption = "End Date\\Time";
            this.colEndDateTime1.ColumnEdit = this.repositoryItemTextEditDateTime3;
            this.colEndDateTime1.FieldName = "EndDateTime";
            this.colEndDateTime1.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colEndDateTime1.Name = "colEndDateTime1";
            this.colEndDateTime1.OptionsColumn.AllowEdit = false;
            this.colEndDateTime1.OptionsColumn.AllowFocus = false;
            this.colEndDateTime1.OptionsColumn.ReadOnly = true;
            this.colEndDateTime1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colEndDateTime1.Visible = true;
            this.colEndDateTime1.VisibleIndex = 4;
            this.colEndDateTime1.Width = 100;
            // 
            // colPurchaseOrderID2
            // 
            this.colPurchaseOrderID2.Caption = "PO ID";
            this.colPurchaseOrderID2.FieldName = "PurchaseOrderID";
            this.colPurchaseOrderID2.Name = "colPurchaseOrderID2";
            this.colPurchaseOrderID2.OptionsColumn.AllowEdit = false;
            this.colPurchaseOrderID2.OptionsColumn.AllowFocus = false;
            this.colPurchaseOrderID2.OptionsColumn.ReadOnly = true;
            this.colPurchaseOrderID2.Width = 49;
            // 
            // colVisitID3
            // 
            this.colVisitID3.Caption = "Visit ID";
            this.colVisitID3.FieldName = "VisitID";
            this.colVisitID3.Name = "colVisitID3";
            this.colVisitID3.OptionsColumn.AllowEdit = false;
            this.colVisitID3.OptionsColumn.AllowFocus = false;
            this.colVisitID3.OptionsColumn.ReadOnly = true;
            this.colVisitID3.Width = 54;
            // 
            // colLinkedPictureCount1
            // 
            this.colLinkedPictureCount1.Caption = "Linked Pictures";
            this.colLinkedPictureCount1.ColumnEdit = this.repositoryItemHyperLinkEditEquipmentPictures;
            this.colLinkedPictureCount1.FieldName = "LinkedPictureCount";
            this.colLinkedPictureCount1.Name = "colLinkedPictureCount1";
            this.colLinkedPictureCount1.OptionsColumn.ReadOnly = true;
            this.colLinkedPictureCount1.Visible = true;
            this.colLinkedPictureCount1.VisibleIndex = 17;
            this.colLinkedPictureCount1.Width = 90;
            // 
            // repositoryItemHyperLinkEditEquipmentPictures
            // 
            this.repositoryItemHyperLinkEditEquipmentPictures.AutoHeight = false;
            this.repositoryItemHyperLinkEditEquipmentPictures.Name = "repositoryItemHyperLinkEditEquipmentPictures";
            this.repositoryItemHyperLinkEditEquipmentPictures.SingleClick = true;
            this.repositoryItemHyperLinkEditEquipmentPictures.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditEquipmentPictures_OpenLink);
            // 
            // colImagesFolderOM1
            // 
            this.colImagesFolderOM1.Caption = "Client Images";
            this.colImagesFolderOM1.FieldName = "ImagesFolderOM";
            this.colImagesFolderOM1.Name = "colImagesFolderOM1";
            this.colImagesFolderOM1.OptionsColumn.AllowEdit = false;
            this.colImagesFolderOM1.OptionsColumn.AllowFocus = false;
            this.colImagesFolderOM1.OptionsColumn.ReadOnly = true;
            this.colImagesFolderOM1.Width = 84;
            // 
            // xtraTabPageMaterials
            // 
            this.xtraTabPageMaterials.Controls.Add(this.popupContainerControlLabourFilter);
            this.xtraTabPageMaterials.Controls.Add(this.popupContainerControlVisitTypeFilter);
            this.xtraTabPageMaterials.Controls.Add(this.gridControlMaterial);
            this.xtraTabPageMaterials.Name = "xtraTabPageMaterials";
            this.xtraTabPageMaterials.Size = new System.Drawing.Size(1052, 187);
            this.xtraTabPageMaterials.Text = "Materials";
            // 
            // popupContainerControlLabourFilter
            // 
            this.popupContainerControlLabourFilter.Controls.Add(this.btnLabourFilter);
            this.popupContainerControlLabourFilter.Controls.Add(this.gridControl12);
            this.popupContainerControlLabourFilter.Location = new System.Drawing.Point(74, 31);
            this.popupContainerControlLabourFilter.Name = "popupContainerControlLabourFilter";
            this.popupContainerControlLabourFilter.Size = new System.Drawing.Size(139, 126);
            this.popupContainerControlLabourFilter.TabIndex = 21;
            // 
            // btnLabourFilter
            // 
            this.btnLabourFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLabourFilter.Location = new System.Drawing.Point(3, 103);
            this.btnLabourFilter.Name = "btnLabourFilter";
            this.btnLabourFilter.Size = new System.Drawing.Size(50, 20);
            this.btnLabourFilter.TabIndex = 12;
            this.btnLabourFilter.Text = "OK";
            this.btnLabourFilter.Click += new System.EventHandler(this.btnLabourFilterOK_Click);
            // 
            // gridControl12
            // 
            this.gridControl12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl12.DataSource = this.sp06028OMLabourFilterBindingSource;
            this.gridControl12.Location = new System.Drawing.Point(3, 3);
            this.gridControl12.MainView = this.gridView12;
            this.gridControl12.MenuManager = this.barManager1;
            this.gridControl12.Name = "gridControl12";
            this.gridControl12.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit17});
            this.gridControl12.Size = new System.Drawing.Size(133, 98);
            this.gridControl12.TabIndex = 4;
            this.gridControl12.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView12});
            // 
            // sp06028OMLabourFilterBindingSource
            // 
            this.sp06028OMLabourFilterBindingSource.DataMember = "sp06028_OM_Labour_Filter";
            this.sp06028OMLabourFilterBindingSource.DataSource = this.dataSet_GC_Summer_Core;
            // 
            // gridView12
            // 
            this.gridView12.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID4,
            this.colName,
            this.colActive,
            this.colTypeID,
            this.colTypeDescription});
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Gray;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colActive;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView12.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView12.GridControl = this.gridControl12;
            this.gridView12.GroupCount = 1;
            this.gridView12.Name = "gridView12";
            this.gridView12.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView12.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView12.OptionsFind.AlwaysVisible = true;
            this.gridView12.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView12.OptionsLayout.StoreAppearance = true;
            this.gridView12.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView12.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView12.OptionsView.ColumnAutoWidth = false;
            this.gridView12.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView12.OptionsView.ShowGroupPanel = false;
            this.gridView12.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView12.OptionsView.ShowIndicator = false;
            this.gridView12.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTypeDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colID4
            // 
            this.colID4.Caption = "ID";
            this.colID4.FieldName = "ID";
            this.colID4.Name = "colID4";
            this.colID4.OptionsColumn.AllowEdit = false;
            this.colID4.OptionsColumn.AllowFocus = false;
            this.colID4.OptionsColumn.ReadOnly = true;
            // 
            // colName
            // 
            this.colName.Caption = "Name";
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.OptionsColumn.AllowEdit = false;
            this.colName.OptionsColumn.AllowFocus = false;
            this.colName.OptionsColumn.ReadOnly = true;
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            this.colName.Width = 214;
            // 
            // colTypeID
            // 
            this.colTypeID.Caption = "Type ID";
            this.colTypeID.FieldName = "TypeID";
            this.colTypeID.Name = "colTypeID";
            this.colTypeID.OptionsColumn.AllowEdit = false;
            this.colTypeID.OptionsColumn.AllowFocus = false;
            this.colTypeID.OptionsColumn.ReadOnly = true;
            // 
            // colTypeDescription
            // 
            this.colTypeDescription.Caption = "Labour Type";
            this.colTypeDescription.FieldName = "TypeDescription";
            this.colTypeDescription.Name = "colTypeDescription";
            this.colTypeDescription.OptionsColumn.AllowEdit = false;
            this.colTypeDescription.OptionsColumn.AllowFocus = false;
            this.colTypeDescription.OptionsColumn.ReadOnly = true;
            this.colTypeDescription.Visible = true;
            this.colTypeDescription.VisibleIndex = 2;
            this.colTypeDescription.Width = 153;
            // 
            // popupContainerControlVisitTypeFilter
            // 
            this.popupContainerControlVisitTypeFilter.Controls.Add(this.gridControl2);
            this.popupContainerControlVisitTypeFilter.Controls.Add(this.btnVisitTypeFilterOK);
            this.popupContainerControlVisitTypeFilter.Location = new System.Drawing.Point(344, 31);
            this.popupContainerControlVisitTypeFilter.Name = "popupContainerControlVisitTypeFilter";
            this.popupContainerControlVisitTypeFilter.Size = new System.Drawing.Size(239, 126);
            this.popupContainerControlVisitTypeFilter.TabIndex = 24;
            // 
            // gridControl2
            // 
            this.gridControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl2.DataSource = this.sp06308OMVisitTypesBindingSource;
            this.gridControl2.Location = new System.Drawing.Point(3, 3);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit21});
            this.gridControl2.Size = new System.Drawing.Size(233, 99);
            this.gridControl2.TabIndex = 1;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp06308OMVisitTypesBindingSource
            // 
            this.sp06308OMVisitTypesBindingSource.DataMember = "sp06308_OM_Visit_Types";
            this.sp06308OMVisitTypesBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // dataSet_OM_Visit
            // 
            this.dataSet_OM_Visit.DataSetName = "DataSet_OM_Visit";
            this.dataSet_OM_Visit.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn224,
            this.gridColumn226,
            this.gridColumn227,
            this.colActive1});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsView.AutoCalcPreviewLineCount = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn227, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn224
            // 
            this.gridColumn224.Caption = "ID";
            this.gridColumn224.FieldName = "ID";
            this.gridColumn224.Name = "gridColumn224";
            this.gridColumn224.OptionsColumn.AllowEdit = false;
            this.gridColumn224.OptionsColumn.AllowFocus = false;
            this.gridColumn224.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn226
            // 
            this.gridColumn226.Caption = "Visit Type";
            this.gridColumn226.FieldName = "Description";
            this.gridColumn226.Name = "gridColumn226";
            this.gridColumn226.OptionsColumn.AllowEdit = false;
            this.gridColumn226.OptionsColumn.AllowFocus = false;
            this.gridColumn226.OptionsColumn.ReadOnly = true;
            this.gridColumn226.Visible = true;
            this.gridColumn226.VisibleIndex = 0;
            this.gridColumn226.Width = 143;
            // 
            // gridColumn227
            // 
            this.gridColumn227.Caption = "Order";
            this.gridColumn227.FieldName = "Order";
            this.gridColumn227.Name = "gridColumn227";
            this.gridColumn227.OptionsColumn.AllowEdit = false;
            this.gridColumn227.OptionsColumn.AllowFocus = false;
            this.gridColumn227.OptionsColumn.ReadOnly = true;
            this.gridColumn227.Width = 63;
            // 
            // colActive1
            // 
            this.colActive1.Caption = "Active";
            this.colActive1.ColumnEdit = this.repositoryItemCheckEdit21;
            this.colActive1.FieldName = "Active";
            this.colActive1.Name = "colActive1";
            this.colActive1.OptionsColumn.AllowEdit = false;
            this.colActive1.OptionsColumn.AllowFocus = false;
            this.colActive1.OptionsColumn.ReadOnly = true;
            this.colActive1.Visible = true;
            this.colActive1.VisibleIndex = 1;
            this.colActive1.Width = 48;
            // 
            // repositoryItemCheckEdit21
            // 
            this.repositoryItemCheckEdit21.AutoHeight = false;
            this.repositoryItemCheckEdit21.Caption = "Check";
            this.repositoryItemCheckEdit21.Name = "repositoryItemCheckEdit21";
            this.repositoryItemCheckEdit21.ValueChecked = 1;
            this.repositoryItemCheckEdit21.ValueUnchecked = 0;
            // 
            // btnVisitTypeFilterOK
            // 
            this.btnVisitTypeFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnVisitTypeFilterOK.Location = new System.Drawing.Point(3, 104);
            this.btnVisitTypeFilterOK.Name = "btnVisitTypeFilterOK";
            this.btnVisitTypeFilterOK.Size = new System.Drawing.Size(50, 20);
            this.btnVisitTypeFilterOK.TabIndex = 0;
            this.btnVisitTypeFilterOK.Text = "OK";
            this.btnVisitTypeFilterOK.Click += new System.EventHandler(this.btnVisitTypeFilterOK_Click);
            // 
            // gridControlMaterial
            // 
            this.gridControlMaterial.DataSource = this.sp06035OMJobManagerLinkedMaterialsBindingSource;
            this.gridControlMaterial.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlMaterial.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlMaterial.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlMaterial.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlMaterial.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlMaterial.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlMaterial.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlMaterial.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlMaterial.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlMaterial.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlMaterial.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlMaterial.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlMaterial.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 8, true, true, "Linked Documents", "linked_document"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 15, true, true, "Linked Pictures", "linked_pictures")});
            this.gridControlMaterial.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlMaterial_EmbeddedNavigator_ButtonClick);
            this.gridControlMaterial.Location = new System.Drawing.Point(0, 0);
            this.gridControlMaterial.MainView = this.gridViewMaterial;
            this.gridControlMaterial.MenuManager = this.barManager1;
            this.gridControlMaterial.Name = "gridControlMaterial";
            this.gridControlMaterial.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit3,
            this.repositoryItemTextEditNumeric2DP4,
            this.repositoryItemTextEditPercentage4,
            this.repositoryItemTextEditCurrency4,
            this.repositoryItemMemoExEdit10,
            this.repositoryItemTextEditDateTime4,
            this.repositoryItemTextEditHTML5,
            this.repositoryItemHyperLinkEditMaterialPictures});
            this.gridControlMaterial.Size = new System.Drawing.Size(1052, 187);
            this.gridControlMaterial.TabIndex = 1;
            this.gridControlMaterial.UseEmbeddedNavigator = true;
            this.gridControlMaterial.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewMaterial});
            // 
            // sp06035OMJobManagerLinkedMaterialsBindingSource
            // 
            this.sp06035OMJobManagerLinkedMaterialsBindingSource.DataMember = "sp06035_OM_Job_Manager_Linked_Materials";
            this.sp06035OMJobManagerLinkedMaterialsBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridViewMaterial
            // 
            this.gridViewMaterial.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn3,
            this.gridColumn166,
            this.gridColumn168,
            this.gridColumn170,
            this.gridColumn171,
            this.gridColumn172,
            this.gridColumn173,
            this.gridColumn174,
            this.gridColumn175,
            this.gridColumn176,
            this.gridColumn177,
            this.gridColumn178,
            this.gridColumn179,
            this.gridColumn180,
            this.gridColumn181,
            this.gridColumn182,
            this.gridColumn183,
            this.gridColumn184,
            this.gridColumn185,
            this.gridColumn186,
            this.gridColumn187,
            this.gridColumn188,
            this.gridColumn189,
            this.gridColumn190,
            this.gridColumn191,
            this.gridColumn192,
            this.gridColumn193,
            this.gridColumn194,
            this.gridColumn195,
            this.gridColumn196,
            this.gridColumn197,
            this.colPurchaseOrderID1,
            this.colVisitID2,
            this.colLinkedPictureCount2,
            this.colImagesFolderOM2});
            this.gridViewMaterial.GridControl = this.gridControlMaterial;
            this.gridViewMaterial.GroupCount = 1;
            this.gridViewMaterial.Name = "gridViewMaterial";
            this.gridViewMaterial.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewMaterial.OptionsFind.FindDelay = 2000;
            this.gridViewMaterial.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewMaterial.OptionsLayout.StoreAppearance = true;
            this.gridViewMaterial.OptionsLayout.StoreFormatRules = true;
            this.gridViewMaterial.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewMaterial.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewMaterial.OptionsSelection.MultiSelect = true;
            this.gridViewMaterial.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewMaterial.OptionsView.ColumnAutoWidth = false;
            this.gridViewMaterial.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewMaterial.OptionsView.ShowGroupPanel = false;
            this.gridViewMaterial.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn193, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn194, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewMaterial.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewMaterial_CustomRowCellEdit);
            this.gridViewMaterial.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewMaterial.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewMaterial.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewMaterial.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewMaterial_ShowingEditor);
            this.gridViewMaterial.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewMaterial.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewMaterial.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(this.gridViewMaterial_CustomColumnDisplayText);
            this.gridViewMaterial.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewMaterial.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewMaterial_MouseUp);
            this.gridViewMaterial.DoubleClick += new System.EventHandler(this.gridViewMaterial_DoubleClick);
            this.gridViewMaterial.GotFocus += new System.EventHandler(this.gridViewMaterial_GotFocus);
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Material Used ID";
            this.gridColumn3.FieldName = "MaterialUsedID";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Width = 108;
            // 
            // gridColumn166
            // 
            this.gridColumn166.Caption = "Material ID";
            this.gridColumn166.FieldName = "MaterialID";
            this.gridColumn166.Name = "gridColumn166";
            this.gridColumn166.OptionsColumn.AllowEdit = false;
            this.gridColumn166.OptionsColumn.AllowFocus = false;
            this.gridColumn166.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn168
            // 
            this.gridColumn168.Caption = "Job ID";
            this.gridColumn168.FieldName = "JobID";
            this.gridColumn168.Name = "gridColumn168";
            this.gridColumn168.OptionsColumn.AllowEdit = false;
            this.gridColumn168.OptionsColumn.AllowFocus = false;
            this.gridColumn168.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn170
            // 
            this.gridColumn170.Caption = "Cost Units Used";
            this.gridColumn170.ColumnEdit = this.repositoryItemTextEditNumeric2DP4;
            this.gridColumn170.FieldName = "CostUnitsUsed";
            this.gridColumn170.Name = "gridColumn170";
            this.gridColumn170.OptionsColumn.AllowEdit = false;
            this.gridColumn170.OptionsColumn.AllowFocus = false;
            this.gridColumn170.OptionsColumn.ReadOnly = true;
            this.gridColumn170.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostUnitsUsed", "{0:n2}")});
            this.gridColumn170.Visible = true;
            this.gridColumn170.VisibleIndex = 1;
            this.gridColumn170.Width = 97;
            // 
            // repositoryItemTextEditNumeric2DP4
            // 
            this.repositoryItemTextEditNumeric2DP4.AutoHeight = false;
            this.repositoryItemTextEditNumeric2DP4.Mask.EditMask = "f2";
            this.repositoryItemTextEditNumeric2DP4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumeric2DP4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumeric2DP4.Name = "repositoryItemTextEditNumeric2DP4";
            // 
            // gridColumn171
            // 
            this.gridColumn171.Caption = "Cost Unit Descriptor ID";
            this.gridColumn171.FieldName = "CostUnitDescriptorID";
            this.gridColumn171.Name = "gridColumn171";
            this.gridColumn171.OptionsColumn.AllowEdit = false;
            this.gridColumn171.OptionsColumn.AllowFocus = false;
            this.gridColumn171.OptionsColumn.ReadOnly = true;
            this.gridColumn171.Width = 131;
            // 
            // gridColumn172
            // 
            this.gridColumn172.Caption = "Sell Units Used";
            this.gridColumn172.ColumnEdit = this.repositoryItemTextEditNumeric2DP4;
            this.gridColumn172.FieldName = "SellUnitsUsed";
            this.gridColumn172.Name = "gridColumn172";
            this.gridColumn172.OptionsColumn.AllowEdit = false;
            this.gridColumn172.OptionsColumn.AllowFocus = false;
            this.gridColumn172.OptionsColumn.ReadOnly = true;
            this.gridColumn172.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellUnitsUsed", "{0:n2}")});
            this.gridColumn172.Visible = true;
            this.gridColumn172.VisibleIndex = 7;
            this.gridColumn172.Width = 91;
            // 
            // gridColumn173
            // 
            this.gridColumn173.Caption = "Sell Unit Descriptor ID";
            this.gridColumn173.FieldName = "SellUnitDescriptorID";
            this.gridColumn173.Name = "gridColumn173";
            this.gridColumn173.OptionsColumn.AllowEdit = false;
            this.gridColumn173.OptionsColumn.AllowFocus = false;
            this.gridColumn173.OptionsColumn.ReadOnly = true;
            this.gridColumn173.Width = 125;
            // 
            // gridColumn174
            // 
            this.gridColumn174.Caption = "Cost Per Unit Ex VAT";
            this.gridColumn174.ColumnEdit = this.repositoryItemTextEditCurrency4;
            this.gridColumn174.FieldName = "CostPerUnitExVat";
            this.gridColumn174.Name = "gridColumn174";
            this.gridColumn174.OptionsColumn.AllowEdit = false;
            this.gridColumn174.OptionsColumn.AllowFocus = false;
            this.gridColumn174.OptionsColumn.ReadOnly = true;
            this.gridColumn174.Visible = true;
            this.gridColumn174.VisibleIndex = 2;
            this.gridColumn174.Width = 121;
            // 
            // repositoryItemTextEditCurrency4
            // 
            this.repositoryItemTextEditCurrency4.AutoHeight = false;
            this.repositoryItemTextEditCurrency4.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency4.Name = "repositoryItemTextEditCurrency4";
            // 
            // gridColumn175
            // 
            this.gridColumn175.Caption = "Cost VAT Rate";
            this.gridColumn175.ColumnEdit = this.repositoryItemTextEditPercentage4;
            this.gridColumn175.FieldName = "CostPerUnitVatRate";
            this.gridColumn175.Name = "gridColumn175";
            this.gridColumn175.OptionsColumn.AllowEdit = false;
            this.gridColumn175.OptionsColumn.AllowFocus = false;
            this.gridColumn175.OptionsColumn.ReadOnly = true;
            this.gridColumn175.Visible = true;
            this.gridColumn175.VisibleIndex = 3;
            this.gridColumn175.Width = 91;
            // 
            // repositoryItemTextEditPercentage4
            // 
            this.repositoryItemTextEditPercentage4.AutoHeight = false;
            this.repositoryItemTextEditPercentage4.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage4.Name = "repositoryItemTextEditPercentage4";
            // 
            // gridColumn176
            // 
            this.gridColumn176.Caption = "Sell Per Unit Ex VAT";
            this.gridColumn176.ColumnEdit = this.repositoryItemTextEditCurrency4;
            this.gridColumn176.FieldName = "SellPerUnitExVat";
            this.gridColumn176.Name = "gridColumn176";
            this.gridColumn176.OptionsColumn.AllowEdit = false;
            this.gridColumn176.OptionsColumn.AllowFocus = false;
            this.gridColumn176.OptionsColumn.ReadOnly = true;
            this.gridColumn176.Visible = true;
            this.gridColumn176.VisibleIndex = 8;
            this.gridColumn176.Width = 115;
            // 
            // gridColumn177
            // 
            this.gridColumn177.Caption = "Sell VAT Rate";
            this.gridColumn177.ColumnEdit = this.repositoryItemTextEditPercentage4;
            this.gridColumn177.FieldName = "SellPerUnitVatRate";
            this.gridColumn177.Name = "gridColumn177";
            this.gridColumn177.OptionsColumn.AllowEdit = false;
            this.gridColumn177.OptionsColumn.AllowFocus = false;
            this.gridColumn177.OptionsColumn.ReadOnly = true;
            this.gridColumn177.Visible = true;
            this.gridColumn177.VisibleIndex = 9;
            this.gridColumn177.Width = 85;
            // 
            // gridColumn178
            // 
            this.gridColumn178.Caption = "Cost Ex VAT";
            this.gridColumn178.ColumnEdit = this.repositoryItemTextEditCurrency4;
            this.gridColumn178.FieldName = "CostValueExVat";
            this.gridColumn178.Name = "gridColumn178";
            this.gridColumn178.OptionsColumn.AllowEdit = false;
            this.gridColumn178.OptionsColumn.AllowFocus = false;
            this.gridColumn178.OptionsColumn.ReadOnly = true;
            this.gridColumn178.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostValueExVat", "{0:c}")});
            this.gridColumn178.Visible = true;
            this.gridColumn178.VisibleIndex = 4;
            this.gridColumn178.Width = 80;
            // 
            // gridColumn179
            // 
            this.gridColumn179.Caption = "Cost VAT";
            this.gridColumn179.ColumnEdit = this.repositoryItemTextEditCurrency4;
            this.gridColumn179.FieldName = "CostValueVat";
            this.gridColumn179.Name = "gridColumn179";
            this.gridColumn179.OptionsColumn.AllowEdit = false;
            this.gridColumn179.OptionsColumn.AllowFocus = false;
            this.gridColumn179.OptionsColumn.ReadOnly = true;
            this.gridColumn179.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostValueVat", "{0:c}")});
            this.gridColumn179.Visible = true;
            this.gridColumn179.VisibleIndex = 5;
            // 
            // gridColumn180
            // 
            this.gridColumn180.Caption = "Cost Total";
            this.gridColumn180.ColumnEdit = this.repositoryItemTextEditCurrency4;
            this.gridColumn180.FieldName = "CostValueTotal";
            this.gridColumn180.Name = "gridColumn180";
            this.gridColumn180.OptionsColumn.AllowEdit = false;
            this.gridColumn180.OptionsColumn.AllowFocus = false;
            this.gridColumn180.OptionsColumn.ReadOnly = true;
            this.gridColumn180.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostValueTotal", "{0:c}")});
            this.gridColumn180.Visible = true;
            this.gridColumn180.VisibleIndex = 6;
            // 
            // gridColumn181
            // 
            this.gridColumn181.Caption = "Sell Ex VAT";
            this.gridColumn181.ColumnEdit = this.repositoryItemTextEditCurrency4;
            this.gridColumn181.FieldName = "SellValueExVat";
            this.gridColumn181.Name = "gridColumn181";
            this.gridColumn181.OptionsColumn.AllowEdit = false;
            this.gridColumn181.OptionsColumn.AllowFocus = false;
            this.gridColumn181.OptionsColumn.ReadOnly = true;
            this.gridColumn181.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellValueExVat", "{0:c}")});
            this.gridColumn181.Visible = true;
            this.gridColumn181.VisibleIndex = 10;
            // 
            // gridColumn182
            // 
            this.gridColumn182.Caption = "Sell VAT";
            this.gridColumn182.ColumnEdit = this.repositoryItemTextEditCurrency4;
            this.gridColumn182.FieldName = "SellValueVat";
            this.gridColumn182.Name = "gridColumn182";
            this.gridColumn182.OptionsColumn.AllowEdit = false;
            this.gridColumn182.OptionsColumn.AllowFocus = false;
            this.gridColumn182.OptionsColumn.ReadOnly = true;
            this.gridColumn182.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellValueVat", "{0:c}")});
            this.gridColumn182.Visible = true;
            this.gridColumn182.VisibleIndex = 11;
            // 
            // gridColumn183
            // 
            this.gridColumn183.Caption = "Sell Total";
            this.gridColumn183.ColumnEdit = this.repositoryItemTextEditCurrency4;
            this.gridColumn183.FieldName = "SellValueTotal";
            this.gridColumn183.Name = "gridColumn183";
            this.gridColumn183.OptionsColumn.AllowEdit = false;
            this.gridColumn183.OptionsColumn.AllowFocus = false;
            this.gridColumn183.OptionsColumn.ReadOnly = true;
            this.gridColumn183.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellValueTotal", "{0:c}")});
            this.gridColumn183.Visible = true;
            this.gridColumn183.VisibleIndex = 12;
            // 
            // gridColumn184
            // 
            this.gridColumn184.Caption = "Remarks";
            this.gridColumn184.ColumnEdit = this.repositoryItemMemoExEdit10;
            this.gridColumn184.FieldName = "Remarks";
            this.gridColumn184.Name = "gridColumn184";
            this.gridColumn184.OptionsColumn.ReadOnly = true;
            this.gridColumn184.Visible = true;
            this.gridColumn184.VisibleIndex = 14;
            // 
            // repositoryItemMemoExEdit10
            // 
            this.repositoryItemMemoExEdit10.AutoHeight = false;
            this.repositoryItemMemoExEdit10.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit10.Name = "repositoryItemMemoExEdit10";
            this.repositoryItemMemoExEdit10.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit10.ShowIcon = false;
            // 
            // gridColumn185
            // 
            this.gridColumn185.Caption = "Client ID";
            this.gridColumn185.FieldName = "ClientID";
            this.gridColumn185.Name = "gridColumn185";
            this.gridColumn185.OptionsColumn.AllowEdit = false;
            this.gridColumn185.OptionsColumn.AllowFocus = false;
            this.gridColumn185.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn186
            // 
            this.gridColumn186.Caption = "Site ID";
            this.gridColumn186.FieldName = "SiteID";
            this.gridColumn186.Name = "gridColumn186";
            this.gridColumn186.OptionsColumn.AllowEdit = false;
            this.gridColumn186.OptionsColumn.AllowFocus = false;
            this.gridColumn186.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn187
            // 
            this.gridColumn187.Caption = "Site Name";
            this.gridColumn187.FieldName = "SiteName";
            this.gridColumn187.Name = "gridColumn187";
            this.gridColumn187.OptionsColumn.AllowEdit = false;
            this.gridColumn187.OptionsColumn.AllowFocus = false;
            this.gridColumn187.OptionsColumn.ReadOnly = true;
            this.gridColumn187.Width = 133;
            // 
            // gridColumn188
            // 
            this.gridColumn188.Caption = "Client Name";
            this.gridColumn188.FieldName = "ClientName";
            this.gridColumn188.Name = "gridColumn188";
            this.gridColumn188.OptionsColumn.AllowEdit = false;
            this.gridColumn188.OptionsColumn.AllowFocus = false;
            this.gridColumn188.OptionsColumn.ReadOnly = true;
            this.gridColumn188.Width = 139;
            // 
            // gridColumn189
            // 
            this.gridColumn189.Caption = "Job Sub-Type";
            this.gridColumn189.FieldName = "JobSubTypeDescription";
            this.gridColumn189.Name = "gridColumn189";
            this.gridColumn189.OptionsColumn.AllowEdit = false;
            this.gridColumn189.OptionsColumn.AllowFocus = false;
            this.gridColumn189.OptionsColumn.ReadOnly = true;
            this.gridColumn189.Width = 150;
            // 
            // gridColumn190
            // 
            this.gridColumn190.Caption = "Job Type";
            this.gridColumn190.FieldName = "JobTypeDescription";
            this.gridColumn190.Name = "gridColumn190";
            this.gridColumn190.OptionsColumn.AllowEdit = false;
            this.gridColumn190.OptionsColumn.AllowFocus = false;
            this.gridColumn190.OptionsColumn.ReadOnly = true;
            this.gridColumn190.Width = 169;
            // 
            // gridColumn191
            // 
            this.gridColumn191.Caption = "Expected Start Date";
            this.gridColumn191.ColumnEdit = this.repositoryItemTextEditDateTime4;
            this.gridColumn191.FieldName = "ExpectedStartDate";
            this.gridColumn191.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn191.Name = "gridColumn191";
            this.gridColumn191.OptionsColumn.AllowEdit = false;
            this.gridColumn191.OptionsColumn.AllowFocus = false;
            this.gridColumn191.OptionsColumn.ReadOnly = true;
            this.gridColumn191.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn191.Width = 119;
            // 
            // repositoryItemTextEditDateTime4
            // 
            this.repositoryItemTextEditDateTime4.AutoHeight = false;
            this.repositoryItemTextEditDateTime4.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime4.Name = "repositoryItemTextEditDateTime4";
            // 
            // gridColumn192
            // 
            this.gridColumn192.Caption = "Visit #";
            this.gridColumn192.FieldName = "VisitNumber";
            this.gridColumn192.Name = "gridColumn192";
            this.gridColumn192.OptionsColumn.AllowEdit = false;
            this.gridColumn192.OptionsColumn.AllowFocus = false;
            this.gridColumn192.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn193
            // 
            this.gridColumn193.Caption = "Linked To Job";
            this.gridColumn193.ColumnEdit = this.repositoryItemTextEditHTML5;
            this.gridColumn193.FieldName = "FullDescription";
            this.gridColumn193.Name = "gridColumn193";
            this.gridColumn193.OptionsColumn.AllowEdit = false;
            this.gridColumn193.OptionsColumn.AllowFocus = false;
            this.gridColumn193.OptionsColumn.ReadOnly = true;
            this.gridColumn193.Visible = true;
            this.gridColumn193.VisibleIndex = 14;
            this.gridColumn193.Width = 415;
            // 
            // repositoryItemTextEditHTML5
            // 
            this.repositoryItemTextEditHTML5.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTextEditHTML5.AutoHeight = false;
            this.repositoryItemTextEditHTML5.Name = "repositoryItemTextEditHTML5";
            // 
            // gridColumn194
            // 
            this.gridColumn194.Caption = "Material Name";
            this.gridColumn194.FieldName = "MaterialName";
            this.gridColumn194.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn194.Name = "gridColumn194";
            this.gridColumn194.OptionsColumn.AllowEdit = false;
            this.gridColumn194.OptionsColumn.AllowFocus = false;
            this.gridColumn194.OptionsColumn.ReadOnly = true;
            this.gridColumn194.Visible = true;
            this.gridColumn194.VisibleIndex = 0;
            this.gridColumn194.Width = 181;
            // 
            // gridColumn195
            // 
            this.gridColumn195.Caption = "Selected";
            this.gridColumn195.ColumnEdit = this.repositoryItemCheckEdit3;
            this.gridColumn195.FieldName = "Selected";
            this.gridColumn195.Name = "gridColumn195";
            this.gridColumn195.OptionsColumn.AllowEdit = false;
            this.gridColumn195.OptionsColumn.AllowFocus = false;
            this.gridColumn195.OptionsColumn.ReadOnly = true;
            this.gridColumn195.Width = 62;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Caption = "Check";
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = 1;
            this.repositoryItemCheckEdit3.ValueUnchecked = 0;
            // 
            // gridColumn196
            // 
            this.gridColumn196.Caption = "Cost Unit Descriptor";
            this.gridColumn196.FieldName = "CostUnitDescriptor";
            this.gridColumn196.Name = "gridColumn196";
            this.gridColumn196.OptionsColumn.AllowEdit = false;
            this.gridColumn196.OptionsColumn.AllowFocus = false;
            this.gridColumn196.OptionsColumn.ReadOnly = true;
            this.gridColumn196.Width = 117;
            // 
            // gridColumn197
            // 
            this.gridColumn197.Caption = "Sell Unit Descriptor";
            this.gridColumn197.FieldName = "SellUnitDescriptor";
            this.gridColumn197.Name = "gridColumn197";
            this.gridColumn197.OptionsColumn.AllowEdit = false;
            this.gridColumn197.OptionsColumn.AllowFocus = false;
            this.gridColumn197.OptionsColumn.ReadOnly = true;
            this.gridColumn197.Width = 111;
            // 
            // colPurchaseOrderID1
            // 
            this.colPurchaseOrderID1.Caption = "PO ID";
            this.colPurchaseOrderID1.FieldName = "PurchaseOrderID";
            this.colPurchaseOrderID1.Name = "colPurchaseOrderID1";
            this.colPurchaseOrderID1.OptionsColumn.AllowEdit = false;
            this.colPurchaseOrderID1.OptionsColumn.AllowFocus = false;
            this.colPurchaseOrderID1.OptionsColumn.ReadOnly = true;
            this.colPurchaseOrderID1.Width = 49;
            // 
            // colVisitID2
            // 
            this.colVisitID2.Caption = "Visit ID";
            this.colVisitID2.FieldName = "VisitID";
            this.colVisitID2.Name = "colVisitID2";
            this.colVisitID2.OptionsColumn.AllowEdit = false;
            this.colVisitID2.OptionsColumn.AllowFocus = false;
            this.colVisitID2.OptionsColumn.ReadOnly = true;
            this.colVisitID2.Width = 54;
            // 
            // colLinkedPictureCount2
            // 
            this.colLinkedPictureCount2.Caption = "Linked Pictures";
            this.colLinkedPictureCount2.ColumnEdit = this.repositoryItemHyperLinkEditMaterialPictures;
            this.colLinkedPictureCount2.FieldName = "LinkedPictureCount";
            this.colLinkedPictureCount2.Name = "colLinkedPictureCount2";
            this.colLinkedPictureCount2.OptionsColumn.ReadOnly = true;
            this.colLinkedPictureCount2.Visible = true;
            this.colLinkedPictureCount2.VisibleIndex = 13;
            this.colLinkedPictureCount2.Width = 90;
            // 
            // repositoryItemHyperLinkEditMaterialPictures
            // 
            this.repositoryItemHyperLinkEditMaterialPictures.AutoHeight = false;
            this.repositoryItemHyperLinkEditMaterialPictures.Name = "repositoryItemHyperLinkEditMaterialPictures";
            this.repositoryItemHyperLinkEditMaterialPictures.SingleClick = true;
            this.repositoryItemHyperLinkEditMaterialPictures.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditMaterialPictures_OpenLink);
            // 
            // colImagesFolderOM2
            // 
            this.colImagesFolderOM2.Caption = "Client Images";
            this.colImagesFolderOM2.FieldName = "ImagesFolderOM";
            this.colImagesFolderOM2.Name = "colImagesFolderOM2";
            this.colImagesFolderOM2.OptionsColumn.AllowEdit = false;
            this.colImagesFolderOM2.OptionsColumn.AllowFocus = false;
            this.colImagesFolderOM2.OptionsColumn.ReadOnly = true;
            this.colImagesFolderOM2.Width = 84;
            // 
            // xtraTabPageExtraInfo
            // 
            this.xtraTabPageExtraInfo.Controls.Add(this.gridControlExtraInfo);
            this.xtraTabPageExtraInfo.Name = "xtraTabPageExtraInfo";
            this.xtraTabPageExtraInfo.Size = new System.Drawing.Size(1052, 187);
            this.xtraTabPageExtraInfo.Text = "Extra Info";
            // 
            // gridControlExtraInfo
            // 
            this.gridControlExtraInfo.DataSource = this.sp06041OMJobManagerLinkedExtraInfoBindingSource;
            this.gridControlExtraInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlExtraInfo.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlExtraInfo.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlExtraInfo.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlExtraInfo.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlExtraInfo.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlExtraInfo.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlExtraInfo.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlExtraInfo.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlExtraInfo.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlExtraInfo.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlExtraInfo.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlExtraInfo.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 8, true, true, "Linked Documents", "linked_document"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 9, true, true, "Clear and Re-Add Extra Attributes", "add extra attributes")});
            this.gridControlExtraInfo.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlExtraInfo_EmbeddedNavigator_ButtonClick);
            this.gridControlExtraInfo.Location = new System.Drawing.Point(0, 0);
            this.gridControlExtraInfo.MainView = this.gridViewExtraInfo;
            this.gridControlExtraInfo.MenuManager = this.barManager1;
            this.gridControlExtraInfo.Name = "gridControlExtraInfo";
            this.gridControlExtraInfo.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit9,
            this.repositoryItemCheckEdit14,
            this.repositoryItemTextEditDateTime10,
            this.repositoryItemTextEditHTML6});
            this.gridControlExtraInfo.Size = new System.Drawing.Size(1052, 187);
            this.gridControlExtraInfo.TabIndex = 1;
            this.gridControlExtraInfo.UseEmbeddedNavigator = true;
            this.gridControlExtraInfo.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExtraInfo});
            // 
            // sp06041OMJobManagerLinkedExtraInfoBindingSource
            // 
            this.sp06041OMJobManagerLinkedExtraInfoBindingSource.DataMember = "sp06041_OM_Job_Manager_Linked_Extra_Info";
            this.sp06041OMJobManagerLinkedExtraInfoBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridViewExtraInfo
            // 
            this.gridViewExtraInfo.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn34,
            this.gridColumn35,
            this.gridColumn38,
            this.gridColumn69,
            this.gridColumn70,
            this.gridColumn71,
            this.gridColumn72,
            this.gridColumn73,
            this.gridColumn74,
            this.gridColumn75,
            this.gridColumn76,
            this.gridColumn115,
            this.gridColumn116,
            this.gridColumn118,
            this.colDataTypeID,
            this.colEditorTypeID,
            this.colEditorMask,
            this.colDataEntryMinValue,
            this.colDataEntryMaxValue,
            this.colDataEntryRequired,
            this.colOnScreenLabel,
            this.colAttributeOrder,
            this.colDefaultValue,
            this.colValueRecorded,
            this.colValueRecordedText,
            this.colAttributeName,
            this.colValueFormula,
            this.colValueFormulaEvaluateLocation,
            this.colValueFormulaEvaluateLocationDesc,
            this.colValueFormulaEvaluateOrder,
            this.colHideFromApp});
            this.gridViewExtraInfo.GridControl = this.gridControlExtraInfo;
            this.gridViewExtraInfo.GroupCount = 1;
            this.gridViewExtraInfo.Name = "gridViewExtraInfo";
            this.gridViewExtraInfo.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewExtraInfo.OptionsFind.FindDelay = 2000;
            this.gridViewExtraInfo.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewExtraInfo.OptionsLayout.StoreAppearance = true;
            this.gridViewExtraInfo.OptionsLayout.StoreFormatRules = true;
            this.gridViewExtraInfo.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewExtraInfo.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewExtraInfo.OptionsSelection.MultiSelect = true;
            this.gridViewExtraInfo.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewExtraInfo.OptionsView.ColumnAutoWidth = false;
            this.gridViewExtraInfo.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewExtraInfo.OptionsView.ShowGroupPanel = false;
            this.gridViewExtraInfo.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn116, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAttributeOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewExtraInfo.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewExtraInfo_CustomRowCellEdit);
            this.gridViewExtraInfo.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewExtraInfo.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewExtraInfo.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewExtraInfo.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewExtraInfo_ShowingEditor);
            this.gridViewExtraInfo.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewExtraInfo.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewExtraInfo.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(this.gridViewExtraInfo_CustomColumnDisplayText);
            this.gridViewExtraInfo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewExtraInfo.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewExtraInfo_MouseUp);
            this.gridViewExtraInfo.DoubleClick += new System.EventHandler(this.gridViewExtraInfo_DoubleClick);
            this.gridViewExtraInfo.GotFocus += new System.EventHandler(this.gridViewExtraInfo_GotFocus);
            // 
            // gridColumn34
            // 
            this.gridColumn34.Caption = "Job Attribute ID";
            this.gridColumn34.FieldName = "JobAttributeID";
            this.gridColumn34.Name = "gridColumn34";
            this.gridColumn34.OptionsColumn.AllowEdit = false;
            this.gridColumn34.OptionsColumn.AllowFocus = false;
            this.gridColumn34.OptionsColumn.ReadOnly = true;
            this.gridColumn34.Width = 108;
            // 
            // gridColumn35
            // 
            this.gridColumn35.Caption = "Master Job Attribute ID";
            this.gridColumn35.FieldName = "MasterJobAttributeID";
            this.gridColumn35.Name = "gridColumn35";
            this.gridColumn35.OptionsColumn.AllowEdit = false;
            this.gridColumn35.OptionsColumn.AllowFocus = false;
            this.gridColumn35.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn38
            // 
            this.gridColumn38.Caption = "Job ID";
            this.gridColumn38.FieldName = "JobID";
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.OptionsColumn.AllowEdit = false;
            this.gridColumn38.OptionsColumn.AllowFocus = false;
            this.gridColumn38.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn69
            // 
            this.gridColumn69.Caption = "Remarks";
            this.gridColumn69.ColumnEdit = this.repositoryItemMemoExEdit9;
            this.gridColumn69.FieldName = "Remarks";
            this.gridColumn69.Name = "gridColumn69";
            this.gridColumn69.OptionsColumn.ReadOnly = true;
            this.gridColumn69.Visible = true;
            this.gridColumn69.VisibleIndex = 3;
            this.gridColumn69.Width = 122;
            // 
            // repositoryItemMemoExEdit9
            // 
            this.repositoryItemMemoExEdit9.AutoHeight = false;
            this.repositoryItemMemoExEdit9.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit9.Name = "repositoryItemMemoExEdit9";
            this.repositoryItemMemoExEdit9.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit9.ShowIcon = false;
            // 
            // gridColumn70
            // 
            this.gridColumn70.Caption = "Client ID";
            this.gridColumn70.FieldName = "ClientID";
            this.gridColumn70.Name = "gridColumn70";
            this.gridColumn70.OptionsColumn.AllowEdit = false;
            this.gridColumn70.OptionsColumn.AllowFocus = false;
            this.gridColumn70.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn71
            // 
            this.gridColumn71.Caption = "Site ID";
            this.gridColumn71.FieldName = "SiteID";
            this.gridColumn71.Name = "gridColumn71";
            this.gridColumn71.OptionsColumn.AllowEdit = false;
            this.gridColumn71.OptionsColumn.AllowFocus = false;
            this.gridColumn71.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn72
            // 
            this.gridColumn72.Caption = "Site Name";
            this.gridColumn72.FieldName = "SiteName";
            this.gridColumn72.Name = "gridColumn72";
            this.gridColumn72.OptionsColumn.AllowEdit = false;
            this.gridColumn72.OptionsColumn.AllowFocus = false;
            this.gridColumn72.OptionsColumn.ReadOnly = true;
            this.gridColumn72.Width = 133;
            // 
            // gridColumn73
            // 
            this.gridColumn73.Caption = "Client Name";
            this.gridColumn73.FieldName = "ClientName";
            this.gridColumn73.Name = "gridColumn73";
            this.gridColumn73.OptionsColumn.AllowEdit = false;
            this.gridColumn73.OptionsColumn.AllowFocus = false;
            this.gridColumn73.OptionsColumn.ReadOnly = true;
            this.gridColumn73.Width = 139;
            // 
            // gridColumn74
            // 
            this.gridColumn74.Caption = "Job Sub-Type";
            this.gridColumn74.FieldName = "JobSubTypeDescription";
            this.gridColumn74.Name = "gridColumn74";
            this.gridColumn74.OptionsColumn.AllowEdit = false;
            this.gridColumn74.OptionsColumn.AllowFocus = false;
            this.gridColumn74.OptionsColumn.ReadOnly = true;
            this.gridColumn74.Width = 150;
            // 
            // gridColumn75
            // 
            this.gridColumn75.Caption = "Job Type";
            this.gridColumn75.FieldName = "JobTypeDescription";
            this.gridColumn75.Name = "gridColumn75";
            this.gridColumn75.OptionsColumn.AllowEdit = false;
            this.gridColumn75.OptionsColumn.AllowFocus = false;
            this.gridColumn75.OptionsColumn.ReadOnly = true;
            this.gridColumn75.Width = 169;
            // 
            // gridColumn76
            // 
            this.gridColumn76.Caption = "Expected Start Date";
            this.gridColumn76.ColumnEdit = this.repositoryItemTextEditDateTime10;
            this.gridColumn76.FieldName = "ExpectedStartDate";
            this.gridColumn76.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn76.Name = "gridColumn76";
            this.gridColumn76.OptionsColumn.AllowEdit = false;
            this.gridColumn76.OptionsColumn.AllowFocus = false;
            this.gridColumn76.OptionsColumn.ReadOnly = true;
            this.gridColumn76.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn76.Width = 119;
            // 
            // repositoryItemTextEditDateTime10
            // 
            this.repositoryItemTextEditDateTime10.AutoHeight = false;
            this.repositoryItemTextEditDateTime10.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime10.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime10.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime10.Name = "repositoryItemTextEditDateTime10";
            // 
            // gridColumn115
            // 
            this.gridColumn115.Caption = "Visit #";
            this.gridColumn115.FieldName = "VisitNumber";
            this.gridColumn115.Name = "gridColumn115";
            this.gridColumn115.OptionsColumn.AllowEdit = false;
            this.gridColumn115.OptionsColumn.AllowFocus = false;
            this.gridColumn115.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn116
            // 
            this.gridColumn116.Caption = "Linked To Job";
            this.gridColumn116.ColumnEdit = this.repositoryItemTextEditHTML6;
            this.gridColumn116.FieldName = "FullDescription";
            this.gridColumn116.Name = "gridColumn116";
            this.gridColumn116.OptionsColumn.AllowEdit = false;
            this.gridColumn116.OptionsColumn.AllowFocus = false;
            this.gridColumn116.OptionsColumn.ReadOnly = true;
            this.gridColumn116.Visible = true;
            this.gridColumn116.VisibleIndex = 3;
            this.gridColumn116.Width = 415;
            // 
            // repositoryItemTextEditHTML6
            // 
            this.repositoryItemTextEditHTML6.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTextEditHTML6.AutoHeight = false;
            this.repositoryItemTextEditHTML6.Name = "repositoryItemTextEditHTML6";
            // 
            // gridColumn118
            // 
            this.gridColumn118.Caption = "Selected";
            this.gridColumn118.ColumnEdit = this.repositoryItemCheckEdit14;
            this.gridColumn118.FieldName = "Selected";
            this.gridColumn118.Name = "gridColumn118";
            this.gridColumn118.OptionsColumn.AllowEdit = false;
            this.gridColumn118.OptionsColumn.AllowFocus = false;
            this.gridColumn118.OptionsColumn.ReadOnly = true;
            this.gridColumn118.Width = 62;
            // 
            // repositoryItemCheckEdit14
            // 
            this.repositoryItemCheckEdit14.AutoHeight = false;
            this.repositoryItemCheckEdit14.Caption = "Check";
            this.repositoryItemCheckEdit14.Name = "repositoryItemCheckEdit14";
            this.repositoryItemCheckEdit14.ValueChecked = 1;
            this.repositoryItemCheckEdit14.ValueUnchecked = 0;
            // 
            // colDataTypeID
            // 
            this.colDataTypeID.Caption = "Data Type ID";
            this.colDataTypeID.FieldName = "DataTypeID";
            this.colDataTypeID.Name = "colDataTypeID";
            this.colDataTypeID.OptionsColumn.AllowEdit = false;
            this.colDataTypeID.OptionsColumn.AllowFocus = false;
            this.colDataTypeID.OptionsColumn.ReadOnly = true;
            this.colDataTypeID.Width = 85;
            // 
            // colEditorTypeID
            // 
            this.colEditorTypeID.Caption = "Editor Type ID";
            this.colEditorTypeID.FieldName = "EditorTypeID";
            this.colEditorTypeID.Name = "colEditorTypeID";
            this.colEditorTypeID.OptionsColumn.AllowEdit = false;
            this.colEditorTypeID.OptionsColumn.AllowFocus = false;
            this.colEditorTypeID.OptionsColumn.ReadOnly = true;
            this.colEditorTypeID.Width = 90;
            // 
            // colEditorMask
            // 
            this.colEditorMask.Caption = "Editor Mask";
            this.colEditorMask.FieldName = "EditorMask";
            this.colEditorMask.Name = "colEditorMask";
            this.colEditorMask.OptionsColumn.AllowEdit = false;
            this.colEditorMask.OptionsColumn.AllowFocus = false;
            this.colEditorMask.OptionsColumn.ReadOnly = true;
            this.colEditorMask.Width = 169;
            // 
            // colDataEntryMinValue
            // 
            this.colDataEntryMinValue.Caption = "Min Value";
            this.colDataEntryMinValue.FieldName = "DataEntryMinValue";
            this.colDataEntryMinValue.Name = "colDataEntryMinValue";
            this.colDataEntryMinValue.OptionsColumn.AllowEdit = false;
            this.colDataEntryMinValue.OptionsColumn.AllowFocus = false;
            this.colDataEntryMinValue.OptionsColumn.ReadOnly = true;
            // 
            // colDataEntryMaxValue
            // 
            this.colDataEntryMaxValue.Caption = "Max Value";
            this.colDataEntryMaxValue.FieldName = "DataEntryMaxValue";
            this.colDataEntryMaxValue.Name = "colDataEntryMaxValue";
            this.colDataEntryMaxValue.OptionsColumn.AllowEdit = false;
            this.colDataEntryMaxValue.OptionsColumn.AllowFocus = false;
            this.colDataEntryMaxValue.OptionsColumn.ReadOnly = true;
            // 
            // colDataEntryRequired
            // 
            this.colDataEntryRequired.Caption = "Required";
            this.colDataEntryRequired.ColumnEdit = this.repositoryItemCheckEdit14;
            this.colDataEntryRequired.FieldName = "DataEntryRequired";
            this.colDataEntryRequired.Name = "colDataEntryRequired";
            this.colDataEntryRequired.OptionsColumn.AllowEdit = false;
            this.colDataEntryRequired.OptionsColumn.AllowFocus = false;
            this.colDataEntryRequired.OptionsColumn.ReadOnly = true;
            this.colDataEntryRequired.Visible = true;
            this.colDataEntryRequired.VisibleIndex = 4;
            this.colDataEntryRequired.Width = 64;
            // 
            // colOnScreenLabel
            // 
            this.colOnScreenLabel.Caption = "Description";
            this.colOnScreenLabel.FieldName = "OnScreenLabel";
            this.colOnScreenLabel.Name = "colOnScreenLabel";
            this.colOnScreenLabel.OptionsColumn.AllowEdit = false;
            this.colOnScreenLabel.OptionsColumn.AllowFocus = false;
            this.colOnScreenLabel.OptionsColumn.ReadOnly = true;
            this.colOnScreenLabel.Visible = true;
            this.colOnScreenLabel.VisibleIndex = 1;
            this.colOnScreenLabel.Width = 244;
            // 
            // colAttributeOrder
            // 
            this.colAttributeOrder.Caption = "Order";
            this.colAttributeOrder.FieldName = "AttributeOrder";
            this.colAttributeOrder.Name = "colAttributeOrder";
            this.colAttributeOrder.OptionsColumn.AllowEdit = false;
            this.colAttributeOrder.OptionsColumn.AllowFocus = false;
            this.colAttributeOrder.OptionsColumn.ReadOnly = true;
            // 
            // colDefaultValue
            // 
            this.colDefaultValue.Caption = "Default Value";
            this.colDefaultValue.FieldName = "DefaultValue";
            this.colDefaultValue.Name = "colDefaultValue";
            this.colDefaultValue.OptionsColumn.AllowEdit = false;
            this.colDefaultValue.OptionsColumn.AllowFocus = false;
            this.colDefaultValue.OptionsColumn.ReadOnly = true;
            this.colDefaultValue.Width = 85;
            // 
            // colValueRecorded
            // 
            this.colValueRecorded.Caption = "Value Recorded";
            this.colValueRecorded.FieldName = "ValueRecorded";
            this.colValueRecorded.Name = "colValueRecorded";
            this.colValueRecorded.OptionsColumn.AllowEdit = false;
            this.colValueRecorded.OptionsColumn.AllowFocus = false;
            this.colValueRecorded.OptionsColumn.ReadOnly = true;
            this.colValueRecorded.Width = 96;
            // 
            // colValueRecordedText
            // 
            this.colValueRecordedText.Caption = "Value";
            this.colValueRecordedText.FieldName = "ValueRecordedText";
            this.colValueRecordedText.Name = "colValueRecordedText";
            this.colValueRecordedText.OptionsColumn.ReadOnly = true;
            this.colValueRecordedText.Visible = true;
            this.colValueRecordedText.VisibleIndex = 2;
            this.colValueRecordedText.Width = 304;
            // 
            // colAttributeName
            // 
            this.colAttributeName.Caption = "Attribute Name";
            this.colAttributeName.FieldName = "AttributeName";
            this.colAttributeName.Name = "colAttributeName";
            this.colAttributeName.OptionsColumn.AllowEdit = false;
            this.colAttributeName.OptionsColumn.AllowFocus = false;
            this.colAttributeName.OptionsColumn.ReadOnly = true;
            this.colAttributeName.Visible = true;
            this.colAttributeName.VisibleIndex = 0;
            this.colAttributeName.Width = 113;
            // 
            // colValueFormula
            // 
            this.colValueFormula.Caption = "Formula";
            this.colValueFormula.ColumnEdit = this.repositoryItemMemoExEdit9;
            this.colValueFormula.FieldName = "ValueFormula";
            this.colValueFormula.Name = "colValueFormula";
            this.colValueFormula.OptionsColumn.ReadOnly = true;
            this.colValueFormula.Visible = true;
            this.colValueFormula.VisibleIndex = 6;
            // 
            // colValueFormulaEvaluateLocation
            // 
            this.colValueFormulaEvaluateLocation.Caption = "Formula Evaluate Location ID";
            this.colValueFormulaEvaluateLocation.FieldName = "ValueFormulaEvaluateLocation";
            this.colValueFormulaEvaluateLocation.Name = "colValueFormulaEvaluateLocation";
            this.colValueFormulaEvaluateLocation.OptionsColumn.AllowEdit = false;
            this.colValueFormulaEvaluateLocation.OptionsColumn.AllowFocus = false;
            this.colValueFormulaEvaluateLocation.OptionsColumn.ReadOnly = true;
            this.colValueFormulaEvaluateLocation.Width = 159;
            // 
            // colValueFormulaEvaluateLocationDesc
            // 
            this.colValueFormulaEvaluateLocationDesc.Caption = "Formula Evaluate Location";
            this.colValueFormulaEvaluateLocationDesc.FieldName = "ValueFormulaEvaluateLocationDesc";
            this.colValueFormulaEvaluateLocationDesc.Name = "colValueFormulaEvaluateLocationDesc";
            this.colValueFormulaEvaluateLocationDesc.OptionsColumn.AllowEdit = false;
            this.colValueFormulaEvaluateLocationDesc.OptionsColumn.AllowFocus = false;
            this.colValueFormulaEvaluateLocationDesc.OptionsColumn.ReadOnly = true;
            this.colValueFormulaEvaluateLocationDesc.Visible = true;
            this.colValueFormulaEvaluateLocationDesc.VisibleIndex = 7;
            this.colValueFormulaEvaluateLocationDesc.Width = 145;
            // 
            // colValueFormulaEvaluateOrder
            // 
            this.colValueFormulaEvaluateOrder.Caption = "Formula Evaluate Order";
            this.colValueFormulaEvaluateOrder.FieldName = "ValueFormulaEvaluateOrder";
            this.colValueFormulaEvaluateOrder.Name = "colValueFormulaEvaluateOrder";
            this.colValueFormulaEvaluateOrder.OptionsColumn.AllowEdit = false;
            this.colValueFormulaEvaluateOrder.OptionsColumn.AllowFocus = false;
            this.colValueFormulaEvaluateOrder.OptionsColumn.ReadOnly = true;
            this.colValueFormulaEvaluateOrder.Visible = true;
            this.colValueFormulaEvaluateOrder.VisibleIndex = 8;
            this.colValueFormulaEvaluateOrder.Width = 133;
            // 
            // colHideFromApp
            // 
            this.colHideFromApp.Caption = "Hide From App";
            this.colHideFromApp.ColumnEdit = this.repositoryItemCheckEdit14;
            this.colHideFromApp.FieldName = "HideFromApp";
            this.colHideFromApp.Name = "colHideFromApp";
            this.colHideFromApp.OptionsColumn.AllowEdit = false;
            this.colHideFromApp.OptionsColumn.AllowFocus = false;
            this.colHideFromApp.OptionsColumn.ReadOnly = true;
            this.colHideFromApp.Visible = true;
            this.colHideFromApp.VisibleIndex = 5;
            this.colHideFromApp.Width = 89;
            // 
            // xtraTabPageSpraying
            // 
            this.xtraTabPageSpraying.Controls.Add(this.gridControlSpraying);
            this.xtraTabPageSpraying.Name = "xtraTabPageSpraying";
            this.xtraTabPageSpraying.Size = new System.Drawing.Size(1052, 187);
            this.xtraTabPageSpraying.Text = "Spraying";
            // 
            // gridControlSpraying
            // 
            this.gridControlSpraying.DataSource = this.sp06038OMJobManagerLinkedSprayingBindingSource;
            this.gridControlSpraying.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlSpraying.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlSpraying.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlSpraying.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlSpraying.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlSpraying.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlSpraying.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlSpraying.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlSpraying.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlSpraying.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlSpraying.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlSpraying.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlSpraying.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 8, true, true, "Linked Documents", "linked_document")});
            this.gridControlSpraying.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlSpraying_EmbeddedNavigator_ButtonClick);
            this.gridControlSpraying.Location = new System.Drawing.Point(0, 0);
            this.gridControlSpraying.MainView = this.gridViewSpraying;
            this.gridControlSpraying.MenuManager = this.barManager1;
            this.gridControlSpraying.Name = "gridControlSpraying";
            this.gridControlSpraying.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit8,
            this.repositoryItemCheckEdit13,
            this.repositoryItemTextEditDateTime7,
            this.repositoryItemTextEdit2DP7,
            this.repositoryItemTextEditNumericRatio,
            this.repositoryItemTextEditHTML7});
            this.gridControlSpraying.Size = new System.Drawing.Size(1052, 187);
            this.gridControlSpraying.TabIndex = 1;
            this.gridControlSpraying.UseEmbeddedNavigator = true;
            this.gridControlSpraying.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewSpraying});
            // 
            // sp06038OMJobManagerLinkedSprayingBindingSource
            // 
            this.sp06038OMJobManagerLinkedSprayingBindingSource.DataMember = "sp06038_OM_Job_Manager_Linked_Spraying";
            this.sp06038OMJobManagerLinkedSprayingBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridViewSpraying
            // 
            this.gridViewSpraying.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn98,
            this.gridColumn101,
            this.gridColumn99,
            this.gridColumn109,
            this.gridColumn110,
            this.gridColumn123,
            this.gridColumn124,
            this.gridColumn125,
            this.gridColumn126,
            this.gridColumn127,
            this.gridColumn128,
            this.gridColumn129,
            this.gridColumn130,
            this.gridColumn131,
            this.gridColumn132,
            this.gridColumn204,
            this.gridColumn205,
            this.gridColumn206,
            this.colLinkedToRecordTypeID3,
            this.colDilutionRatio,
            this.colAmountUsedDiluted});
            this.gridViewSpraying.GridControl = this.gridControlSpraying;
            this.gridViewSpraying.GroupCount = 1;
            this.gridViewSpraying.Name = "gridViewSpraying";
            this.gridViewSpraying.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewSpraying.OptionsFind.FindDelay = 2000;
            this.gridViewSpraying.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewSpraying.OptionsLayout.StoreAppearance = true;
            this.gridViewSpraying.OptionsLayout.StoreFormatRules = true;
            this.gridViewSpraying.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewSpraying.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewSpraying.OptionsSelection.MultiSelect = true;
            this.gridViewSpraying.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewSpraying.OptionsView.ColumnAutoWidth = false;
            this.gridViewSpraying.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewSpraying.OptionsView.ShowGroupPanel = false;
            this.gridViewSpraying.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn132, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn204, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewSpraying.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewSpraying.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewSpraying.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewSpraying.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewSpraying.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewSpraying.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(this.gridViewSpraying_CustomColumnDisplayText);
            this.gridViewSpraying.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewSpraying.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewSpraying_MouseUp);
            this.gridViewSpraying.DoubleClick += new System.EventHandler(this.gridViewSpraying_DoubleClick);
            this.gridViewSpraying.GotFocus += new System.EventHandler(this.gridViewSpraying_GotFocus);
            // 
            // gridColumn98
            // 
            this.gridColumn98.Caption = "Spraying ID";
            this.gridColumn98.FieldName = "SprayingID";
            this.gridColumn98.Name = "gridColumn98";
            this.gridColumn98.OptionsColumn.AllowEdit = false;
            this.gridColumn98.OptionsColumn.AllowFocus = false;
            this.gridColumn98.OptionsColumn.ReadOnly = true;
            this.gridColumn98.Width = 108;
            // 
            // gridColumn101
            // 
            this.gridColumn101.Caption = "Job ID";
            this.gridColumn101.FieldName = "LinkedToRecordID";
            this.gridColumn101.Name = "gridColumn101";
            this.gridColumn101.OptionsColumn.AllowEdit = false;
            this.gridColumn101.OptionsColumn.AllowFocus = false;
            this.gridColumn101.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn99
            // 
            this.gridColumn99.Caption = "Chemical ID";
            this.gridColumn99.FieldName = "ChemicalID";
            this.gridColumn99.Name = "gridColumn99";
            this.gridColumn99.OptionsColumn.AllowEdit = false;
            this.gridColumn99.OptionsColumn.AllowFocus = false;
            this.gridColumn99.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn109
            // 
            this.gridColumn109.Caption = "Amount Used";
            this.gridColumn109.ColumnEdit = this.repositoryItemTextEdit2DP7;
            this.gridColumn109.FieldName = "AmountUsed";
            this.gridColumn109.Name = "gridColumn109";
            this.gridColumn109.OptionsColumn.AllowEdit = false;
            this.gridColumn109.OptionsColumn.AllowFocus = false;
            this.gridColumn109.OptionsColumn.ReadOnly = true;
            this.gridColumn109.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "AmountUsed", "{0:n2}")});
            this.gridColumn109.Visible = true;
            this.gridColumn109.VisibleIndex = 1;
            this.gridColumn109.Width = 97;
            // 
            // repositoryItemTextEdit2DP7
            // 
            this.repositoryItemTextEdit2DP7.AutoHeight = false;
            this.repositoryItemTextEdit2DP7.Mask.EditMask = "f2";
            this.repositoryItemTextEdit2DP7.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP7.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP7.Name = "repositoryItemTextEdit2DP7";
            // 
            // gridColumn110
            // 
            this.gridColumn110.Caption = "Amount Used Descriptor ID";
            this.gridColumn110.FieldName = "AmountUsedDescriptorID";
            this.gridColumn110.Name = "gridColumn110";
            this.gridColumn110.OptionsColumn.AllowEdit = false;
            this.gridColumn110.OptionsColumn.AllowFocus = false;
            this.gridColumn110.OptionsColumn.ReadOnly = true;
            this.gridColumn110.Width = 131;
            // 
            // gridColumn123
            // 
            this.gridColumn123.Caption = "Remarks";
            this.gridColumn123.ColumnEdit = this.repositoryItemMemoExEdit8;
            this.gridColumn123.FieldName = "Remarks";
            this.gridColumn123.Name = "gridColumn123";
            this.gridColumn123.OptionsColumn.ReadOnly = true;
            this.gridColumn123.Visible = true;
            this.gridColumn123.VisibleIndex = 4;
            // 
            // repositoryItemMemoExEdit8
            // 
            this.repositoryItemMemoExEdit8.AutoHeight = false;
            this.repositoryItemMemoExEdit8.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit8.Name = "repositoryItemMemoExEdit8";
            this.repositoryItemMemoExEdit8.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit8.ShowIcon = false;
            // 
            // gridColumn124
            // 
            this.gridColumn124.Caption = "Client ID";
            this.gridColumn124.FieldName = "ClientID";
            this.gridColumn124.Name = "gridColumn124";
            this.gridColumn124.OptionsColumn.AllowEdit = false;
            this.gridColumn124.OptionsColumn.AllowFocus = false;
            this.gridColumn124.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn125
            // 
            this.gridColumn125.Caption = "Site ID";
            this.gridColumn125.FieldName = "SiteID";
            this.gridColumn125.Name = "gridColumn125";
            this.gridColumn125.OptionsColumn.AllowEdit = false;
            this.gridColumn125.OptionsColumn.AllowFocus = false;
            this.gridColumn125.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn126
            // 
            this.gridColumn126.Caption = "Site Name";
            this.gridColumn126.FieldName = "SiteName";
            this.gridColumn126.Name = "gridColumn126";
            this.gridColumn126.OptionsColumn.AllowEdit = false;
            this.gridColumn126.OptionsColumn.AllowFocus = false;
            this.gridColumn126.OptionsColumn.ReadOnly = true;
            this.gridColumn126.Width = 133;
            // 
            // gridColumn127
            // 
            this.gridColumn127.Caption = "Client Name";
            this.gridColumn127.FieldName = "ClientName";
            this.gridColumn127.Name = "gridColumn127";
            this.gridColumn127.OptionsColumn.AllowEdit = false;
            this.gridColumn127.OptionsColumn.AllowFocus = false;
            this.gridColumn127.OptionsColumn.ReadOnly = true;
            this.gridColumn127.Width = 139;
            // 
            // gridColumn128
            // 
            this.gridColumn128.Caption = "Job Sub-Type";
            this.gridColumn128.FieldName = "JobSubTypeDescription";
            this.gridColumn128.Name = "gridColumn128";
            this.gridColumn128.OptionsColumn.AllowEdit = false;
            this.gridColumn128.OptionsColumn.AllowFocus = false;
            this.gridColumn128.OptionsColumn.ReadOnly = true;
            this.gridColumn128.Width = 150;
            // 
            // gridColumn129
            // 
            this.gridColumn129.Caption = "Job Type";
            this.gridColumn129.FieldName = "JobTypeDescription";
            this.gridColumn129.Name = "gridColumn129";
            this.gridColumn129.OptionsColumn.AllowEdit = false;
            this.gridColumn129.OptionsColumn.AllowFocus = false;
            this.gridColumn129.OptionsColumn.ReadOnly = true;
            this.gridColumn129.Width = 169;
            // 
            // gridColumn130
            // 
            this.gridColumn130.Caption = "Expected Start Date";
            this.gridColumn130.ColumnEdit = this.repositoryItemTextEditDateTime7;
            this.gridColumn130.FieldName = "ExpectedStartDate";
            this.gridColumn130.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn130.Name = "gridColumn130";
            this.gridColumn130.OptionsColumn.AllowEdit = false;
            this.gridColumn130.OptionsColumn.AllowFocus = false;
            this.gridColumn130.OptionsColumn.ReadOnly = true;
            this.gridColumn130.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn130.Width = 119;
            // 
            // repositoryItemTextEditDateTime7
            // 
            this.repositoryItemTextEditDateTime7.AutoHeight = false;
            this.repositoryItemTextEditDateTime7.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime7.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime7.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime7.Name = "repositoryItemTextEditDateTime7";
            // 
            // gridColumn131
            // 
            this.gridColumn131.Caption = "Visit #";
            this.gridColumn131.FieldName = "VisitNumber";
            this.gridColumn131.Name = "gridColumn131";
            this.gridColumn131.OptionsColumn.AllowEdit = false;
            this.gridColumn131.OptionsColumn.AllowFocus = false;
            this.gridColumn131.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn132
            // 
            this.gridColumn132.Caption = "Linked To Job";
            this.gridColumn132.ColumnEdit = this.repositoryItemTextEditHTML7;
            this.gridColumn132.FieldName = "FullDescription";
            this.gridColumn132.Name = "gridColumn132";
            this.gridColumn132.OptionsColumn.AllowEdit = false;
            this.gridColumn132.OptionsColumn.AllowFocus = false;
            this.gridColumn132.OptionsColumn.ReadOnly = true;
            this.gridColumn132.Visible = true;
            this.gridColumn132.VisibleIndex = 3;
            this.gridColumn132.Width = 415;
            // 
            // repositoryItemTextEditHTML7
            // 
            this.repositoryItemTextEditHTML7.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTextEditHTML7.AutoHeight = false;
            this.repositoryItemTextEditHTML7.Name = "repositoryItemTextEditHTML7";
            // 
            // gridColumn204
            // 
            this.gridColumn204.Caption = "Chemical Name";
            this.gridColumn204.FieldName = "ChemicalName";
            this.gridColumn204.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn204.Name = "gridColumn204";
            this.gridColumn204.OptionsColumn.AllowEdit = false;
            this.gridColumn204.OptionsColumn.AllowFocus = false;
            this.gridColumn204.OptionsColumn.ReadOnly = true;
            this.gridColumn204.Visible = true;
            this.gridColumn204.VisibleIndex = 0;
            this.gridColumn204.Width = 181;
            // 
            // gridColumn205
            // 
            this.gridColumn205.Caption = "Selected";
            this.gridColumn205.ColumnEdit = this.repositoryItemCheckEdit13;
            this.gridColumn205.FieldName = "Selected";
            this.gridColumn205.Name = "gridColumn205";
            this.gridColumn205.OptionsColumn.AllowEdit = false;
            this.gridColumn205.OptionsColumn.AllowFocus = false;
            this.gridColumn205.OptionsColumn.ReadOnly = true;
            this.gridColumn205.Width = 62;
            // 
            // repositoryItemCheckEdit13
            // 
            this.repositoryItemCheckEdit13.AutoHeight = false;
            this.repositoryItemCheckEdit13.Caption = "Check";
            this.repositoryItemCheckEdit13.Name = "repositoryItemCheckEdit13";
            this.repositoryItemCheckEdit13.ValueChecked = 1;
            this.repositoryItemCheckEdit13.ValueUnchecked = 0;
            // 
            // gridColumn206
            // 
            this.gridColumn206.Caption = "Amount Used Descriptor";
            this.gridColumn206.FieldName = "AmountUsedDescriptor";
            this.gridColumn206.Name = "gridColumn206";
            this.gridColumn206.OptionsColumn.AllowEdit = false;
            this.gridColumn206.OptionsColumn.AllowFocus = false;
            this.gridColumn206.OptionsColumn.ReadOnly = true;
            this.gridColumn206.Width = 117;
            // 
            // colLinkedToRecordTypeID3
            // 
            this.colLinkedToRecordTypeID3.Caption = "Linked To Record Type ID";
            this.colLinkedToRecordTypeID3.FieldName = "LinkedToRecordTypeID";
            this.colLinkedToRecordTypeID3.Name = "colLinkedToRecordTypeID3";
            this.colLinkedToRecordTypeID3.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordTypeID3.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordTypeID3.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordTypeID3.Width = 144;
            // 
            // colDilutionRatio
            // 
            this.colDilutionRatio.Caption = "Dilution Ratio";
            this.colDilutionRatio.ColumnEdit = this.repositoryItemTextEditNumericRatio;
            this.colDilutionRatio.FieldName = "DilutionRatio";
            this.colDilutionRatio.Name = "colDilutionRatio";
            this.colDilutionRatio.OptionsColumn.AllowEdit = false;
            this.colDilutionRatio.OptionsColumn.AllowFocus = false;
            this.colDilutionRatio.OptionsColumn.ReadOnly = true;
            this.colDilutionRatio.Visible = true;
            this.colDilutionRatio.VisibleIndex = 2;
            this.colDilutionRatio.Width = 84;
            // 
            // repositoryItemTextEditNumericRatio
            // 
            this.repositoryItemTextEditNumericRatio.AutoHeight = false;
            this.repositoryItemTextEditNumericRatio.Mask.EditMask = "1 : #####0.00";
            this.repositoryItemTextEditNumericRatio.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumericRatio.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumericRatio.Name = "repositoryItemTextEditNumericRatio";
            // 
            // colAmountUsedDiluted
            // 
            this.colAmountUsedDiluted.Caption = "Diluted Amount Used";
            this.colAmountUsedDiluted.ColumnEdit = this.repositoryItemTextEdit2DP7;
            this.colAmountUsedDiluted.FieldName = "AmountUsedDiluted";
            this.colAmountUsedDiluted.Name = "colAmountUsedDiluted";
            this.colAmountUsedDiluted.OptionsColumn.AllowEdit = false;
            this.colAmountUsedDiluted.OptionsColumn.AllowFocus = false;
            this.colAmountUsedDiluted.OptionsColumn.ReadOnly = true;
            this.colAmountUsedDiluted.Visible = true;
            this.colAmountUsedDiluted.VisibleIndex = 3;
            this.colAmountUsedDiluted.Width = 121;
            // 
            // xtraTabPageWaste
            // 
            this.xtraTabPageWaste.Controls.Add(this.gridControlWaste);
            this.xtraTabPageWaste.Name = "xtraTabPageWaste";
            this.xtraTabPageWaste.Size = new System.Drawing.Size(1052, 187);
            this.xtraTabPageWaste.Text = "Waste";
            // 
            // gridControlWaste
            // 
            this.gridControlWaste.DataSource = this.sp06036OMJobManagerLinkedWasteBindingSource;
            this.gridControlWaste.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlWaste.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlWaste.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlWaste.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlWaste.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlWaste.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlWaste.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlWaste.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlWaste.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlWaste.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlWaste.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlWaste.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlWaste.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 8, true, true, "Linked Documents", "linked_document")});
            this.gridControlWaste.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlWaste_EmbeddedNavigator_ButtonClick);
            this.gridControlWaste.Location = new System.Drawing.Point(0, 0);
            this.gridControlWaste.MainView = this.gridViewWaste;
            this.gridControlWaste.MenuManager = this.barManager1;
            this.gridControlWaste.Name = "gridControlWaste";
            this.gridControlWaste.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit7,
            this.repositoryItemCheckEdit12,
            this.repositoryItemTextEditDateTime5,
            this.repositoryItemTextEditNumeric2DP5,
            this.repositoryItemTextEditHTML8,
            this.repositoryItemHyperLinkEditWasteDocument});
            this.gridControlWaste.Size = new System.Drawing.Size(1052, 187);
            this.gridControlWaste.TabIndex = 1;
            this.gridControlWaste.UseEmbeddedNavigator = true;
            this.gridControlWaste.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewWaste});
            // 
            // sp06036OMJobManagerLinkedWasteBindingSource
            // 
            this.sp06036OMJobManagerLinkedWasteBindingSource.DataMember = "sp06036_OM_Job_Manager_Linked_Waste";
            this.sp06036OMJobManagerLinkedWasteBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridViewWaste
            // 
            this.gridViewWaste.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn83,
            this.gridColumn86,
            this.colWasteTypeID,
            this.gridColumn88,
            this.gridColumn89,
            this.gridColumn102,
            this.gridColumn103,
            this.gridColumn104,
            this.gridColumn105,
            this.gridColumn106,
            this.gridColumn107,
            this.gridColumn167,
            this.gridColumn198,
            this.gridColumn199,
            this.gridColumn200,
            this.gridColumn201,
            this.gridColumn202,
            this.gridColumn203,
            this.colWasteDisposalCentreID,
            this.colWasteDisposalCentreName,
            this.colWasteDisposalCentreAddressLine1,
            this.colLatitude,
            this.colLongitude,
            this.colLinkedToRecordTypeID2,
            this.colWasteDocumentExtension,
            this.colWasteDocumentID,
            this.colWasteDocumentPath});
            this.gridViewWaste.GridControl = this.gridControlWaste;
            this.gridViewWaste.GroupCount = 1;
            this.gridViewWaste.Name = "gridViewWaste";
            this.gridViewWaste.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewWaste.OptionsFind.FindDelay = 2000;
            this.gridViewWaste.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewWaste.OptionsLayout.StoreAppearance = true;
            this.gridViewWaste.OptionsLayout.StoreFormatRules = true;
            this.gridViewWaste.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewWaste.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewWaste.OptionsSelection.MultiSelect = true;
            this.gridViewWaste.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewWaste.OptionsView.ColumnAutoWidth = false;
            this.gridViewWaste.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewWaste.OptionsView.ShowGroupPanel = false;
            this.gridViewWaste.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn200, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn201, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewWaste.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewWaste_CustomRowCellEdit);
            this.gridViewWaste.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewWaste.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewWaste.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewWaste.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewWaste_ShowingEditor);
            this.gridViewWaste.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewWaste.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewWaste.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(this.gridViewWaste_CustomColumnDisplayText);
            this.gridViewWaste.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewWaste.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewWaste_MouseUp);
            this.gridViewWaste.DoubleClick += new System.EventHandler(this.gridViewWaste_DoubleClick);
            this.gridViewWaste.GotFocus += new System.EventHandler(this.gridViewWaste_GotFocus);
            // 
            // gridColumn83
            // 
            this.gridColumn83.Caption = "Waste ID";
            this.gridColumn83.FieldName = "WasteID";
            this.gridColumn83.Name = "gridColumn83";
            this.gridColumn83.OptionsColumn.AllowEdit = false;
            this.gridColumn83.OptionsColumn.AllowFocus = false;
            this.gridColumn83.OptionsColumn.ReadOnly = true;
            this.gridColumn83.Width = 108;
            // 
            // gridColumn86
            // 
            this.gridColumn86.Caption = "Job ID";
            this.gridColumn86.FieldName = "LinkedToRecordID";
            this.gridColumn86.Name = "gridColumn86";
            this.gridColumn86.OptionsColumn.AllowEdit = false;
            this.gridColumn86.OptionsColumn.AllowFocus = false;
            this.gridColumn86.OptionsColumn.ReadOnly = true;
            // 
            // colWasteTypeID
            // 
            this.colWasteTypeID.Caption = "Waste Type ID";
            this.colWasteTypeID.FieldName = "WasteTypeID";
            this.colWasteTypeID.Name = "colWasteTypeID";
            this.colWasteTypeID.Width = 93;
            // 
            // gridColumn88
            // 
            this.gridColumn88.Caption = "Waste Amount";
            this.gridColumn88.ColumnEdit = this.repositoryItemTextEditNumeric2DP5;
            this.gridColumn88.FieldName = "WasteAmount";
            this.gridColumn88.Name = "gridColumn88";
            this.gridColumn88.OptionsColumn.AllowEdit = false;
            this.gridColumn88.OptionsColumn.AllowFocus = false;
            this.gridColumn88.OptionsColumn.ReadOnly = true;
            this.gridColumn88.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "WasteAmount", "{0:n2}")});
            this.gridColumn88.Visible = true;
            this.gridColumn88.VisibleIndex = 3;
            this.gridColumn88.Width = 146;
            // 
            // repositoryItemTextEditNumeric2DP5
            // 
            this.repositoryItemTextEditNumeric2DP5.AutoHeight = false;
            this.repositoryItemTextEditNumeric2DP5.Mask.EditMask = "f2";
            this.repositoryItemTextEditNumeric2DP5.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumeric2DP5.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumeric2DP5.Name = "repositoryItemTextEditNumeric2DP5";
            // 
            // gridColumn89
            // 
            this.gridColumn89.Caption = "Waste Amount Unit Descriptor";
            this.gridColumn89.FieldName = "WasteAmountUnitDescriptorID";
            this.gridColumn89.Name = "gridColumn89";
            this.gridColumn89.OptionsColumn.AllowEdit = false;
            this.gridColumn89.OptionsColumn.AllowFocus = false;
            this.gridColumn89.OptionsColumn.ReadOnly = true;
            this.gridColumn89.Width = 131;
            // 
            // gridColumn102
            // 
            this.gridColumn102.Caption = "Remarks";
            this.gridColumn102.ColumnEdit = this.repositoryItemMemoExEdit7;
            this.gridColumn102.FieldName = "Remarks";
            this.gridColumn102.Name = "gridColumn102";
            this.gridColumn102.OptionsColumn.ReadOnly = true;
            this.gridColumn102.Visible = true;
            this.gridColumn102.VisibleIndex = 4;
            // 
            // repositoryItemMemoExEdit7
            // 
            this.repositoryItemMemoExEdit7.AutoHeight = false;
            this.repositoryItemMemoExEdit7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit7.Name = "repositoryItemMemoExEdit7";
            this.repositoryItemMemoExEdit7.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit7.ShowIcon = false;
            // 
            // gridColumn103
            // 
            this.gridColumn103.Caption = "Client ID";
            this.gridColumn103.FieldName = "ClientID";
            this.gridColumn103.Name = "gridColumn103";
            this.gridColumn103.OptionsColumn.AllowEdit = false;
            this.gridColumn103.OptionsColumn.AllowFocus = false;
            this.gridColumn103.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn104
            // 
            this.gridColumn104.Caption = "Site ID";
            this.gridColumn104.FieldName = "SiteID";
            this.gridColumn104.Name = "gridColumn104";
            this.gridColumn104.OptionsColumn.AllowEdit = false;
            this.gridColumn104.OptionsColumn.AllowFocus = false;
            this.gridColumn104.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn105
            // 
            this.gridColumn105.Caption = "Site Name";
            this.gridColumn105.FieldName = "SiteName";
            this.gridColumn105.Name = "gridColumn105";
            this.gridColumn105.OptionsColumn.AllowEdit = false;
            this.gridColumn105.OptionsColumn.AllowFocus = false;
            this.gridColumn105.OptionsColumn.ReadOnly = true;
            this.gridColumn105.Width = 133;
            // 
            // gridColumn106
            // 
            this.gridColumn106.Caption = "Client Name";
            this.gridColumn106.FieldName = "ClientName";
            this.gridColumn106.Name = "gridColumn106";
            this.gridColumn106.OptionsColumn.AllowEdit = false;
            this.gridColumn106.OptionsColumn.AllowFocus = false;
            this.gridColumn106.OptionsColumn.ReadOnly = true;
            this.gridColumn106.Width = 139;
            // 
            // gridColumn107
            // 
            this.gridColumn107.Caption = "Job Sub-Type";
            this.gridColumn107.FieldName = "JobSubTypeDescription";
            this.gridColumn107.Name = "gridColumn107";
            this.gridColumn107.OptionsColumn.AllowEdit = false;
            this.gridColumn107.OptionsColumn.AllowFocus = false;
            this.gridColumn107.OptionsColumn.ReadOnly = true;
            this.gridColumn107.Width = 150;
            // 
            // gridColumn167
            // 
            this.gridColumn167.Caption = "Job Type";
            this.gridColumn167.FieldName = "JobTypeDescription";
            this.gridColumn167.Name = "gridColumn167";
            this.gridColumn167.OptionsColumn.AllowEdit = false;
            this.gridColumn167.OptionsColumn.AllowFocus = false;
            this.gridColumn167.OptionsColumn.ReadOnly = true;
            this.gridColumn167.Width = 169;
            // 
            // gridColumn198
            // 
            this.gridColumn198.Caption = "Expected Start Date";
            this.gridColumn198.ColumnEdit = this.repositoryItemTextEditDateTime5;
            this.gridColumn198.FieldName = "ExpectedStartDate";
            this.gridColumn198.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn198.Name = "gridColumn198";
            this.gridColumn198.OptionsColumn.AllowEdit = false;
            this.gridColumn198.OptionsColumn.AllowFocus = false;
            this.gridColumn198.OptionsColumn.ReadOnly = true;
            this.gridColumn198.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn198.Width = 119;
            // 
            // repositoryItemTextEditDateTime5
            // 
            this.repositoryItemTextEditDateTime5.AutoHeight = false;
            this.repositoryItemTextEditDateTime5.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime5.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime5.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime5.Name = "repositoryItemTextEditDateTime5";
            // 
            // gridColumn199
            // 
            this.gridColumn199.Caption = "Visit #";
            this.gridColumn199.FieldName = "VisitNumber";
            this.gridColumn199.Name = "gridColumn199";
            this.gridColumn199.OptionsColumn.AllowEdit = false;
            this.gridColumn199.OptionsColumn.AllowFocus = false;
            this.gridColumn199.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn200
            // 
            this.gridColumn200.Caption = "Linked To Job";
            this.gridColumn200.ColumnEdit = this.repositoryItemTextEditHTML8;
            this.gridColumn200.FieldName = "FullDescription";
            this.gridColumn200.Name = "gridColumn200";
            this.gridColumn200.OptionsColumn.AllowEdit = false;
            this.gridColumn200.OptionsColumn.AllowFocus = false;
            this.gridColumn200.OptionsColumn.ReadOnly = true;
            this.gridColumn200.Visible = true;
            this.gridColumn200.VisibleIndex = 5;
            this.gridColumn200.Width = 415;
            // 
            // repositoryItemTextEditHTML8
            // 
            this.repositoryItemTextEditHTML8.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTextEditHTML8.AutoHeight = false;
            this.repositoryItemTextEditHTML8.Name = "repositoryItemTextEditHTML8";
            // 
            // gridColumn201
            // 
            this.gridColumn201.Caption = "Waste Type";
            this.gridColumn201.FieldName = "WasteType";
            this.gridColumn201.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn201.Name = "gridColumn201";
            this.gridColumn201.OptionsColumn.AllowEdit = false;
            this.gridColumn201.OptionsColumn.AllowFocus = false;
            this.gridColumn201.OptionsColumn.ReadOnly = true;
            this.gridColumn201.Visible = true;
            this.gridColumn201.VisibleIndex = 0;
            this.gridColumn201.Width = 202;
            // 
            // gridColumn202
            // 
            this.gridColumn202.Caption = "Selected";
            this.gridColumn202.ColumnEdit = this.repositoryItemCheckEdit12;
            this.gridColumn202.FieldName = "Selected";
            this.gridColumn202.Name = "gridColumn202";
            this.gridColumn202.OptionsColumn.AllowEdit = false;
            this.gridColumn202.OptionsColumn.AllowFocus = false;
            this.gridColumn202.OptionsColumn.ReadOnly = true;
            this.gridColumn202.Width = 62;
            // 
            // repositoryItemCheckEdit12
            // 
            this.repositoryItemCheckEdit12.AutoHeight = false;
            this.repositoryItemCheckEdit12.Caption = "Check";
            this.repositoryItemCheckEdit12.Name = "repositoryItemCheckEdit12";
            this.repositoryItemCheckEdit12.ValueChecked = 1;
            this.repositoryItemCheckEdit12.ValueUnchecked = 0;
            // 
            // gridColumn203
            // 
            this.gridColumn203.Caption = "Waste Unit Descriptor";
            this.gridColumn203.FieldName = "WasteAmountUnitDescriptor";
            this.gridColumn203.Name = "gridColumn203";
            this.gridColumn203.OptionsColumn.AllowEdit = false;
            this.gridColumn203.OptionsColumn.AllowFocus = false;
            this.gridColumn203.OptionsColumn.ReadOnly = true;
            this.gridColumn203.Width = 117;
            // 
            // colWasteDisposalCentreID
            // 
            this.colWasteDisposalCentreID.Caption = "Disposal Centre ID";
            this.colWasteDisposalCentreID.FieldName = "WasteDisposalCentreID";
            this.colWasteDisposalCentreID.Name = "colWasteDisposalCentreID";
            this.colWasteDisposalCentreID.OptionsColumn.AllowEdit = false;
            this.colWasteDisposalCentreID.OptionsColumn.AllowFocus = false;
            this.colWasteDisposalCentreID.OptionsColumn.ReadOnly = true;
            this.colWasteDisposalCentreID.Width = 110;
            // 
            // colWasteDisposalCentreName
            // 
            this.colWasteDisposalCentreName.Caption = "Disposal Centre";
            this.colWasteDisposalCentreName.FieldName = "WasteDisposalCentreName";
            this.colWasteDisposalCentreName.Name = "colWasteDisposalCentreName";
            this.colWasteDisposalCentreName.OptionsColumn.AllowEdit = false;
            this.colWasteDisposalCentreName.OptionsColumn.AllowFocus = false;
            this.colWasteDisposalCentreName.OptionsColumn.ReadOnly = true;
            this.colWasteDisposalCentreName.Visible = true;
            this.colWasteDisposalCentreName.VisibleIndex = 1;
            this.colWasteDisposalCentreName.Width = 167;
            // 
            // colWasteDisposalCentreAddressLine1
            // 
            this.colWasteDisposalCentreAddressLine1.Caption = "Disposal Centre Address";
            this.colWasteDisposalCentreAddressLine1.FieldName = "WasteDisposalCentreAddressLine1";
            this.colWasteDisposalCentreAddressLine1.Name = "colWasteDisposalCentreAddressLine1";
            this.colWasteDisposalCentreAddressLine1.OptionsColumn.AllowEdit = false;
            this.colWasteDisposalCentreAddressLine1.OptionsColumn.AllowFocus = false;
            this.colWasteDisposalCentreAddressLine1.OptionsColumn.ReadOnly = true;
            this.colWasteDisposalCentreAddressLine1.Visible = true;
            this.colWasteDisposalCentreAddressLine1.VisibleIndex = 2;
            this.colWasteDisposalCentreAddressLine1.Width = 168;
            // 
            // colLatitude
            // 
            this.colLatitude.Caption = "Latitude";
            this.colLatitude.FieldName = "Latitude";
            this.colLatitude.Name = "colLatitude";
            this.colLatitude.OptionsColumn.AllowEdit = false;
            this.colLatitude.OptionsColumn.AllowFocus = false;
            this.colLatitude.OptionsColumn.ReadOnly = true;
            // 
            // colLongitude
            // 
            this.colLongitude.Caption = "Longitude";
            this.colLongitude.FieldName = "Longitude";
            this.colLongitude.Name = "colLongitude";
            this.colLongitude.OptionsColumn.AllowEdit = false;
            this.colLongitude.OptionsColumn.AllowFocus = false;
            this.colLongitude.OptionsColumn.ReadOnly = true;
            // 
            // colLinkedToRecordTypeID2
            // 
            this.colLinkedToRecordTypeID2.Caption = "Linked To Record Type ID";
            this.colLinkedToRecordTypeID2.FieldName = "LinkedToRecordTypeID";
            this.colLinkedToRecordTypeID2.Name = "colLinkedToRecordTypeID2";
            this.colLinkedToRecordTypeID2.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordTypeID2.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordTypeID2.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordTypeID2.Width = 144;
            // 
            // colWasteDocumentExtension
            // 
            this.colWasteDocumentExtension.Caption = "Waste Document Extention";
            this.colWasteDocumentExtension.FieldName = "WasteDocumentExtension";
            this.colWasteDocumentExtension.Name = "colWasteDocumentExtension";
            this.colWasteDocumentExtension.OptionsColumn.AllowEdit = false;
            this.colWasteDocumentExtension.OptionsColumn.AllowFocus = false;
            this.colWasteDocumentExtension.OptionsColumn.ReadOnly = true;
            this.colWasteDocumentExtension.Width = 150;
            // 
            // colWasteDocumentID
            // 
            this.colWasteDocumentID.Caption = "Waste Document ID";
            this.colWasteDocumentID.ColumnEdit = this.repositoryItemHyperLinkEditWasteDocument;
            this.colWasteDocumentID.FieldName = "WasteDocumentID";
            this.colWasteDocumentID.Name = "colWasteDocumentID";
            this.colWasteDocumentID.OptionsColumn.ReadOnly = true;
            this.colWasteDocumentID.Visible = true;
            this.colWasteDocumentID.VisibleIndex = 5;
            this.colWasteDocumentID.Width = 115;
            // 
            // repositoryItemHyperLinkEditWasteDocument
            // 
            this.repositoryItemHyperLinkEditWasteDocument.AutoHeight = false;
            this.repositoryItemHyperLinkEditWasteDocument.Name = "repositoryItemHyperLinkEditWasteDocument";
            this.repositoryItemHyperLinkEditWasteDocument.SingleClick = true;
            this.repositoryItemHyperLinkEditWasteDocument.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditWasteDocument_OpenLink);
            // 
            // colWasteDocumentPath
            // 
            this.colWasteDocumentPath.Caption = "Waste Document Path";
            this.colWasteDocumentPath.ColumnEdit = this.repositoryItemHyperLinkEditWasteDocument;
            this.colWasteDocumentPath.FieldName = "WasteDocumentPath";
            this.colWasteDocumentPath.Name = "colWasteDocumentPath";
            this.colWasteDocumentPath.OptionsColumn.ReadOnly = true;
            this.colWasteDocumentPath.Visible = true;
            this.colWasteDocumentPath.VisibleIndex = 6;
            this.colWasteDocumentPath.Width = 350;
            // 
            // xtraTabPagePictures
            // 
            this.xtraTabPagePictures.Controls.Add(this.gridSplitContainer3);
            this.xtraTabPagePictures.Name = "xtraTabPagePictures";
            this.xtraTabPagePictures.Size = new System.Drawing.Size(1052, 187);
            this.xtraTabPagePictures.Text = "Pictures";
            // 
            // gridSplitContainer3
            // 
            this.gridSplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer3.Grid = this.gridControlPicture;
            this.gridSplitContainer3.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer3.Name = "gridSplitContainer3";
            this.gridSplitContainer3.Panel1.Controls.Add(this.gridControlPicture);
            this.gridSplitContainer3.Size = new System.Drawing.Size(1052, 187);
            this.gridSplitContainer3.TabIndex = 0;
            // 
            // gridControlPicture
            // 
            this.gridControlPicture.DataSource = this.sp06037OMJobManagerLinkedPicturesBindingSource;
            this.gridControlPicture.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlPicture.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlPicture.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlPicture.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlPicture.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlPicture.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlPicture.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlPicture.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlPicture.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlPicture.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlPicture.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlPicture.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlPicture.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 8, true, true, "Linked Documents", "linked_document")});
            this.gridControlPicture.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlPicture_EmbeddedNavigator_ButtonClick);
            this.gridControlPicture.Location = new System.Drawing.Point(0, 0);
            this.gridControlPicture.MainView = this.gridViewPicture;
            this.gridControlPicture.MenuManager = this.barManager1;
            this.gridControlPicture.Name = "gridControlPicture";
            this.gridControlPicture.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEditDateTime6,
            this.repositoryItemHyperLinkEdit2,
            this.repositoryItemMemoExEdit3,
            this.repositoryItemTextEditHTML9});
            this.gridControlPicture.Size = new System.Drawing.Size(1052, 187);
            this.gridControlPicture.TabIndex = 0;
            this.gridControlPicture.UseEmbeddedNavigator = true;
            this.gridControlPicture.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPicture});
            // 
            // sp06037OMJobManagerLinkedPicturesBindingSource
            // 
            this.sp06037OMJobManagerLinkedPicturesBindingSource.DataMember = "sp06037_OM_Job_Manager_Linked_Pictures";
            this.sp06037OMJobManagerLinkedPicturesBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridViewPicture
            // 
            this.gridViewPicture.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn84,
            this.gridColumn85,
            this.gridColumn87,
            this.gridColumn90,
            this.gridColumn91,
            this.gridColumn92,
            this.gridColumn93,
            this.gridColumn95,
            this.gridColumn96,
            this.gridColumn97,
            this.colShortLinkedRecordDescription,
            this.colClientID1,
            this.colSiteID1,
            this.colClientName2,
            this.colSiteName2,
            this.colExpectedStartDate2,
            this.colJobTypeDescription3,
            this.colJobSubTypeDescription2,
            this.colVisitNumber2,
            this.colSelected2,
            this.colPictureType});
            this.gridViewPicture.GridControl = this.gridControlPicture;
            this.gridViewPicture.GroupCount = 1;
            this.gridViewPicture.Name = "gridViewPicture";
            this.gridViewPicture.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewPicture.OptionsFind.FindDelay = 2000;
            this.gridViewPicture.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewPicture.OptionsLayout.StoreAppearance = true;
            this.gridViewPicture.OptionsLayout.StoreFormatRules = true;
            this.gridViewPicture.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewPicture.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewPicture.OptionsSelection.MultiSelect = true;
            this.gridViewPicture.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewPicture.OptionsView.ColumnAutoWidth = false;
            this.gridViewPicture.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewPicture.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewPicture.OptionsView.ShowGroupPanel = false;
            this.gridViewPicture.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn96, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn92, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridViewPicture.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewPicture_CustomRowCellEdit);
            this.gridViewPicture.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewPicture.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewPicture.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewPicture.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView4_ShowingEditor);
            this.gridViewPicture.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewPicture.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewPicture.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewPicture.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewPicture_MouseUp);
            this.gridViewPicture.DoubleClick += new System.EventHandler(this.gridViewPicture_DoubleClick);
            this.gridViewPicture.GotFocus += new System.EventHandler(this.gridViewPicture_GotFocus);
            // 
            // gridColumn84
            // 
            this.gridColumn84.Caption = "Picture ID";
            this.gridColumn84.FieldName = "PictureID";
            this.gridColumn84.Name = "gridColumn84";
            this.gridColumn84.OptionsColumn.AllowEdit = false;
            this.gridColumn84.OptionsColumn.AllowFocus = false;
            this.gridColumn84.OptionsColumn.ReadOnly = true;
            this.gridColumn84.Width = 105;
            // 
            // gridColumn85
            // 
            this.gridColumn85.Caption = "Linked To Record ID";
            this.gridColumn85.FieldName = "LinkedToRecordID";
            this.gridColumn85.Name = "gridColumn85";
            this.gridColumn85.OptionsColumn.AllowEdit = false;
            this.gridColumn85.OptionsColumn.AllowFocus = false;
            this.gridColumn85.OptionsColumn.ReadOnly = true;
            this.gridColumn85.Width = 117;
            // 
            // gridColumn87
            // 
            this.gridColumn87.Caption = "Linked To Record Type ID";
            this.gridColumn87.FieldName = "LinkedToRecordTypeID";
            this.gridColumn87.Name = "gridColumn87";
            this.gridColumn87.OptionsColumn.AllowEdit = false;
            this.gridColumn87.OptionsColumn.AllowFocus = false;
            this.gridColumn87.OptionsColumn.ReadOnly = true;
            this.gridColumn87.Width = 144;
            // 
            // gridColumn90
            // 
            this.gridColumn90.Caption = "Picture Type ID";
            this.gridColumn90.FieldName = "PictureTypeID";
            this.gridColumn90.Name = "gridColumn90";
            this.gridColumn90.OptionsColumn.AllowEdit = false;
            this.gridColumn90.OptionsColumn.AllowFocus = false;
            this.gridColumn90.OptionsColumn.ReadOnly = true;
            this.gridColumn90.Width = 95;
            // 
            // gridColumn91
            // 
            this.gridColumn91.Caption = "Picture Path";
            this.gridColumn91.ColumnEdit = this.repositoryItemHyperLinkEdit2;
            this.gridColumn91.FieldName = "PicturePath";
            this.gridColumn91.Name = "gridColumn91";
            this.gridColumn91.OptionsColumn.ReadOnly = true;
            this.gridColumn91.Visible = true;
            this.gridColumn91.VisibleIndex = 2;
            this.gridColumn91.Width = 472;
            // 
            // repositoryItemHyperLinkEdit2
            // 
            this.repositoryItemHyperLinkEdit2.AutoHeight = false;
            this.repositoryItemHyperLinkEdit2.Name = "repositoryItemHyperLinkEdit2";
            this.repositoryItemHyperLinkEdit2.SingleClick = true;
            this.repositoryItemHyperLinkEdit2.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit2_OpenLink);
            // 
            // gridColumn92
            // 
            this.gridColumn92.Caption = "Date Taken";
            this.gridColumn92.ColumnEdit = this.repositoryItemTextEditDateTime6;
            this.gridColumn92.FieldName = "DateTimeTaken";
            this.gridColumn92.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn92.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn92.Name = "gridColumn92";
            this.gridColumn92.OptionsColumn.AllowEdit = false;
            this.gridColumn92.OptionsColumn.AllowFocus = false;
            this.gridColumn92.OptionsColumn.ReadOnly = true;
            this.gridColumn92.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn92.Visible = true;
            this.gridColumn92.VisibleIndex = 0;
            this.gridColumn92.Width = 141;
            // 
            // repositoryItemTextEditDateTime6
            // 
            this.repositoryItemTextEditDateTime6.AutoHeight = false;
            this.repositoryItemTextEditDateTime6.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime6.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime6.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime6.Name = "repositoryItemTextEditDateTime6";
            // 
            // gridColumn93
            // 
            this.gridColumn93.Caption = "Added By Staff ID";
            this.gridColumn93.FieldName = "AddedByStaffID";
            this.gridColumn93.Name = "gridColumn93";
            this.gridColumn93.OptionsColumn.AllowEdit = false;
            this.gridColumn93.OptionsColumn.AllowFocus = false;
            this.gridColumn93.OptionsColumn.ReadOnly = true;
            this.gridColumn93.Width = 108;
            // 
            // gridColumn95
            // 
            this.gridColumn95.Caption = "Remarks";
            this.gridColumn95.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.gridColumn95.FieldName = "Remarks";
            this.gridColumn95.Name = "gridColumn95";
            this.gridColumn95.OptionsColumn.ReadOnly = true;
            this.gridColumn95.Visible = true;
            this.gridColumn95.VisibleIndex = 4;
            this.gridColumn95.Width = 163;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // gridColumn96
            // 
            this.gridColumn96.Caption = "Linked To Job";
            this.gridColumn96.ColumnEdit = this.repositoryItemTextEditHTML9;
            this.gridColumn96.FieldName = "LinkedRecordDescription";
            this.gridColumn96.Name = "gridColumn96";
            this.gridColumn96.OptionsColumn.AllowEdit = false;
            this.gridColumn96.OptionsColumn.AllowFocus = false;
            this.gridColumn96.OptionsColumn.ReadOnly = true;
            this.gridColumn96.Visible = true;
            this.gridColumn96.VisibleIndex = 5;
            this.gridColumn96.Width = 303;
            // 
            // repositoryItemTextEditHTML9
            // 
            this.repositoryItemTextEditHTML9.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTextEditHTML9.AutoHeight = false;
            this.repositoryItemTextEditHTML9.Name = "repositoryItemTextEditHTML9";
            // 
            // gridColumn97
            // 
            this.gridColumn97.Caption = "Added By";
            this.gridColumn97.FieldName = "AddedByStaffName";
            this.gridColumn97.Name = "gridColumn97";
            this.gridColumn97.OptionsColumn.AllowEdit = false;
            this.gridColumn97.OptionsColumn.AllowFocus = false;
            this.gridColumn97.OptionsColumn.ReadOnly = true;
            this.gridColumn97.Visible = true;
            this.gridColumn97.VisibleIndex = 1;
            this.gridColumn97.Width = 108;
            // 
            // colShortLinkedRecordDescription
            // 
            this.colShortLinkedRecordDescription.Caption = "Linked To Job (Short Desc)";
            this.colShortLinkedRecordDescription.ColumnEdit = this.repositoryItemTextEditHTML9;
            this.colShortLinkedRecordDescription.FieldName = "ShortLinkedRecordDescription";
            this.colShortLinkedRecordDescription.Name = "colShortLinkedRecordDescription";
            this.colShortLinkedRecordDescription.OptionsColumn.AllowEdit = false;
            this.colShortLinkedRecordDescription.OptionsColumn.AllowFocus = false;
            this.colShortLinkedRecordDescription.OptionsColumn.ReadOnly = true;
            this.colShortLinkedRecordDescription.Width = 231;
            // 
            // colClientID1
            // 
            this.colClientID1.Caption = "Client ID";
            this.colClientID1.FieldName = "ClientID";
            this.colClientID1.Name = "colClientID1";
            this.colClientID1.OptionsColumn.AllowEdit = false;
            this.colClientID1.OptionsColumn.AllowFocus = false;
            this.colClientID1.OptionsColumn.ReadOnly = true;
            // 
            // colSiteID1
            // 
            this.colSiteID1.Caption = "Site ID";
            this.colSiteID1.FieldName = "SiteID";
            this.colSiteID1.Name = "colSiteID1";
            this.colSiteID1.OptionsColumn.AllowEdit = false;
            this.colSiteID1.OptionsColumn.AllowFocus = false;
            this.colSiteID1.OptionsColumn.ReadOnly = true;
            // 
            // colClientName2
            // 
            this.colClientName2.Caption = "Client Name";
            this.colClientName2.FieldName = "ClientName";
            this.colClientName2.Name = "colClientName2";
            this.colClientName2.OptionsColumn.AllowEdit = false;
            this.colClientName2.OptionsColumn.AllowFocus = false;
            this.colClientName2.OptionsColumn.ReadOnly = true;
            this.colClientName2.Width = 169;
            // 
            // colSiteName2
            // 
            this.colSiteName2.Caption = "Site Name";
            this.colSiteName2.FieldName = "SiteName";
            this.colSiteName2.Name = "colSiteName2";
            this.colSiteName2.OptionsColumn.AllowEdit = false;
            this.colSiteName2.OptionsColumn.AllowFocus = false;
            this.colSiteName2.OptionsColumn.ReadOnly = true;
            this.colSiteName2.Width = 167;
            // 
            // colExpectedStartDate2
            // 
            this.colExpectedStartDate2.Caption = "Expected Start Date";
            this.colExpectedStartDate2.ColumnEdit = this.repositoryItemTextEditDateTime6;
            this.colExpectedStartDate2.FieldName = "ExpectedStartDate";
            this.colExpectedStartDate2.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colExpectedStartDate2.Name = "colExpectedStartDate2";
            this.colExpectedStartDate2.OptionsColumn.AllowEdit = false;
            this.colExpectedStartDate2.OptionsColumn.AllowFocus = false;
            this.colExpectedStartDate2.OptionsColumn.ReadOnly = true;
            this.colExpectedStartDate2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colExpectedStartDate2.Width = 119;
            // 
            // colJobTypeDescription3
            // 
            this.colJobTypeDescription3.Caption = "Job Type";
            this.colJobTypeDescription3.FieldName = "JobTypeDescription";
            this.colJobTypeDescription3.Name = "colJobTypeDescription3";
            this.colJobTypeDescription3.OptionsColumn.AllowEdit = false;
            this.colJobTypeDescription3.OptionsColumn.AllowFocus = false;
            this.colJobTypeDescription3.OptionsColumn.ReadOnly = true;
            this.colJobTypeDescription3.Width = 145;
            // 
            // colJobSubTypeDescription2
            // 
            this.colJobSubTypeDescription2.Caption = "Job Sub-Type";
            this.colJobSubTypeDescription2.FieldName = "JobSubTypeDescription";
            this.colJobSubTypeDescription2.Name = "colJobSubTypeDescription2";
            this.colJobSubTypeDescription2.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeDescription2.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeDescription2.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeDescription2.Width = 157;
            // 
            // colVisitNumber2
            // 
            this.colVisitNumber2.Caption = "Visit #";
            this.colVisitNumber2.FieldName = "VisitNumber";
            this.colVisitNumber2.Name = "colVisitNumber2";
            this.colVisitNumber2.OptionsColumn.AllowEdit = false;
            this.colVisitNumber2.OptionsColumn.AllowFocus = false;
            this.colVisitNumber2.OptionsColumn.ReadOnly = true;
            // 
            // colSelected2
            // 
            this.colSelected2.Caption = "Selected";
            this.colSelected2.FieldName = "Selected";
            this.colSelected2.Name = "colSelected2";
            this.colSelected2.OptionsColumn.AllowEdit = false;
            this.colSelected2.OptionsColumn.AllowFocus = false;
            this.colSelected2.OptionsColumn.ReadOnly = true;
            // 
            // colPictureType
            // 
            this.colPictureType.Caption = "Picture Type";
            this.colPictureType.FieldName = "PictureType";
            this.colPictureType.Name = "colPictureType";
            this.colPictureType.OptionsColumn.AllowEdit = false;
            this.colPictureType.OptionsColumn.AllowFocus = false;
            this.colPictureType.Visible = true;
            this.colPictureType.VisibleIndex = 3;
            this.colPictureType.Width = 190;
            // 
            // xtraTabPageComments
            // 
            this.xtraTabPageComments.Controls.Add(this.gridControlComment);
            this.xtraTabPageComments.Name = "xtraTabPageComments";
            this.xtraTabPageComments.Size = new System.Drawing.Size(1052, 187);
            this.xtraTabPageComments.Text = "Comments";
            // 
            // gridControlComment
            // 
            this.gridControlComment.DataSource = this.sp06040OMJobManagerLinkedCommentsBindingSource;
            this.gridControlComment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlComment.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlComment.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlComment.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlComment.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlComment.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlComment.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlComment.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlComment.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlComment.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlComment.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlComment.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlComment.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 8, true, true, "Linked Documents", "linked_document")});
            this.gridControlComment.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlComment_EmbeddedNavigator_ButtonClick);
            this.gridControlComment.Location = new System.Drawing.Point(0, 0);
            this.gridControlComment.MainView = this.gridViewComment;
            this.gridControlComment.MenuManager = this.barManager1;
            this.gridControlComment.Name = "gridControlComment";
            this.gridControlComment.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit5,
            this.repositoryItemCheckEdit10,
            this.repositoryItemTextEditDateTime9,
            this.repositoryItemTextEditHTML10});
            this.gridControlComment.Size = new System.Drawing.Size(1052, 187);
            this.gridControlComment.TabIndex = 1;
            this.gridControlComment.UseEmbeddedNavigator = true;
            this.gridControlComment.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewComment});
            // 
            // sp06040OMJobManagerLinkedCommentsBindingSource
            // 
            this.sp06040OMJobManagerLinkedCommentsBindingSource.DataMember = "sp06040_OM_Job_Manager_Linked_Comments";
            this.sp06040OMJobManagerLinkedCommentsBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridViewComment
            // 
            this.gridViewComment.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn33,
            this.gridColumn36,
            this.gridColumn52,
            this.gridColumn53,
            this.gridColumn54,
            this.gridColumn55,
            this.gridColumn56,
            this.gridColumn57,
            this.gridColumn60,
            this.gridColumn63,
            this.gridColumn64,
            this.gridColumn65,
            this.gridColumn67,
            this.colLinkedToRecordTypeID,
            this.colRecordedByID,
            this.colRecordedByTypeID,
            this.colRecordedByType,
            this.colRecordedBy,
            this.colDateRecorded,
            this.colVisibleToClient});
            this.gridViewComment.GridControl = this.gridControlComment;
            this.gridViewComment.GroupCount = 1;
            this.gridViewComment.Name = "gridViewComment";
            this.gridViewComment.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewComment.OptionsFind.FindDelay = 2000;
            this.gridViewComment.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewComment.OptionsLayout.StoreAppearance = true;
            this.gridViewComment.OptionsLayout.StoreFormatRules = true;
            this.gridViewComment.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewComment.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewComment.OptionsSelection.MultiSelect = true;
            this.gridViewComment.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewComment.OptionsView.ColumnAutoWidth = false;
            this.gridViewComment.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewComment.OptionsView.ShowGroupPanel = false;
            this.gridViewComment.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn65, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateRecorded, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridViewComment.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewComment.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewComment.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewComment.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewComment.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewComment.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewComment.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewComment_MouseUp);
            this.gridViewComment.DoubleClick += new System.EventHandler(this.gridViewComment_DoubleClick);
            this.gridViewComment.GotFocus += new System.EventHandler(this.gridViewComment_GotFocus);
            // 
            // gridColumn33
            // 
            this.gridColumn33.Caption = "Comment ID";
            this.gridColumn33.FieldName = "CommentID";
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.OptionsColumn.AllowEdit = false;
            this.gridColumn33.OptionsColumn.AllowFocus = false;
            this.gridColumn33.OptionsColumn.ReadOnly = true;
            this.gridColumn33.Width = 108;
            // 
            // gridColumn36
            // 
            this.gridColumn36.Caption = "Job ID";
            this.gridColumn36.FieldName = "JobID";
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.OptionsColumn.AllowEdit = false;
            this.gridColumn36.OptionsColumn.AllowFocus = false;
            this.gridColumn36.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn52
            // 
            this.gridColumn52.Caption = "Comment";
            this.gridColumn52.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.gridColumn52.FieldName = "Comment";
            this.gridColumn52.Name = "gridColumn52";
            this.gridColumn52.OptionsColumn.ReadOnly = true;
            this.gridColumn52.Visible = true;
            this.gridColumn52.VisibleIndex = 4;
            this.gridColumn52.Width = 271;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // gridColumn53
            // 
            this.gridColumn53.Caption = "Client ID";
            this.gridColumn53.FieldName = "ClientID";
            this.gridColumn53.Name = "gridColumn53";
            this.gridColumn53.OptionsColumn.AllowEdit = false;
            this.gridColumn53.OptionsColumn.AllowFocus = false;
            this.gridColumn53.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn54
            // 
            this.gridColumn54.Caption = "Site ID";
            this.gridColumn54.FieldName = "SiteID";
            this.gridColumn54.Name = "gridColumn54";
            this.gridColumn54.OptionsColumn.AllowEdit = false;
            this.gridColumn54.OptionsColumn.AllowFocus = false;
            this.gridColumn54.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn55
            // 
            this.gridColumn55.Caption = "Site Name";
            this.gridColumn55.FieldName = "SiteName";
            this.gridColumn55.Name = "gridColumn55";
            this.gridColumn55.OptionsColumn.AllowEdit = false;
            this.gridColumn55.OptionsColumn.AllowFocus = false;
            this.gridColumn55.OptionsColumn.ReadOnly = true;
            this.gridColumn55.Width = 133;
            // 
            // gridColumn56
            // 
            this.gridColumn56.Caption = "Client Name";
            this.gridColumn56.FieldName = "ClientName";
            this.gridColumn56.Name = "gridColumn56";
            this.gridColumn56.OptionsColumn.AllowEdit = false;
            this.gridColumn56.OptionsColumn.AllowFocus = false;
            this.gridColumn56.OptionsColumn.ReadOnly = true;
            this.gridColumn56.Width = 139;
            // 
            // gridColumn57
            // 
            this.gridColumn57.Caption = "Job Sub-Type";
            this.gridColumn57.FieldName = "JobSubTypeDescription";
            this.gridColumn57.Name = "gridColumn57";
            this.gridColumn57.OptionsColumn.AllowEdit = false;
            this.gridColumn57.OptionsColumn.AllowFocus = false;
            this.gridColumn57.OptionsColumn.ReadOnly = true;
            this.gridColumn57.Width = 150;
            // 
            // gridColumn60
            // 
            this.gridColumn60.Caption = "Job Type";
            this.gridColumn60.FieldName = "JobTypeDescription";
            this.gridColumn60.Name = "gridColumn60";
            this.gridColumn60.OptionsColumn.AllowEdit = false;
            this.gridColumn60.OptionsColumn.AllowFocus = false;
            this.gridColumn60.OptionsColumn.ReadOnly = true;
            this.gridColumn60.Width = 169;
            // 
            // gridColumn63
            // 
            this.gridColumn63.Caption = "Expected Start Date";
            this.gridColumn63.ColumnEdit = this.repositoryItemTextEditDateTime9;
            this.gridColumn63.FieldName = "ExpectedStartDate";
            this.gridColumn63.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn63.Name = "gridColumn63";
            this.gridColumn63.OptionsColumn.AllowEdit = false;
            this.gridColumn63.OptionsColumn.AllowFocus = false;
            this.gridColumn63.OptionsColumn.ReadOnly = true;
            this.gridColumn63.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn63.Width = 119;
            // 
            // repositoryItemTextEditDateTime9
            // 
            this.repositoryItemTextEditDateTime9.AutoHeight = false;
            this.repositoryItemTextEditDateTime9.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime9.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime9.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime9.Name = "repositoryItemTextEditDateTime9";
            // 
            // gridColumn64
            // 
            this.gridColumn64.Caption = "Visit #";
            this.gridColumn64.FieldName = "VisitNumber";
            this.gridColumn64.Name = "gridColumn64";
            this.gridColumn64.OptionsColumn.AllowEdit = false;
            this.gridColumn64.OptionsColumn.AllowFocus = false;
            this.gridColumn64.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn65
            // 
            this.gridColumn65.Caption = "Linked To Job";
            this.gridColumn65.ColumnEdit = this.repositoryItemTextEditHTML10;
            this.gridColumn65.FieldName = "FullDescription";
            this.gridColumn65.Name = "gridColumn65";
            this.gridColumn65.OptionsColumn.AllowEdit = false;
            this.gridColumn65.OptionsColumn.AllowFocus = false;
            this.gridColumn65.OptionsColumn.ReadOnly = true;
            this.gridColumn65.Visible = true;
            this.gridColumn65.VisibleIndex = 5;
            this.gridColumn65.Width = 415;
            // 
            // repositoryItemTextEditHTML10
            // 
            this.repositoryItemTextEditHTML10.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTextEditHTML10.AutoHeight = false;
            this.repositoryItemTextEditHTML10.Name = "repositoryItemTextEditHTML10";
            // 
            // gridColumn67
            // 
            this.gridColumn67.Caption = "Selected";
            this.gridColumn67.ColumnEdit = this.repositoryItemCheckEdit10;
            this.gridColumn67.FieldName = "Selected";
            this.gridColumn67.Name = "gridColumn67";
            this.gridColumn67.OptionsColumn.AllowEdit = false;
            this.gridColumn67.OptionsColumn.AllowFocus = false;
            this.gridColumn67.OptionsColumn.ReadOnly = true;
            this.gridColumn67.Width = 62;
            // 
            // repositoryItemCheckEdit10
            // 
            this.repositoryItemCheckEdit10.AutoHeight = false;
            this.repositoryItemCheckEdit10.Caption = "Check";
            this.repositoryItemCheckEdit10.Name = "repositoryItemCheckEdit10";
            this.repositoryItemCheckEdit10.ValueChecked = 1;
            this.repositoryItemCheckEdit10.ValueUnchecked = 0;
            // 
            // colLinkedToRecordTypeID
            // 
            this.colLinkedToRecordTypeID.Caption = "Linked To Record Type ID";
            this.colLinkedToRecordTypeID.FieldName = "LinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.Name = "colLinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordTypeID.Width = 144;
            // 
            // colRecordedByID
            // 
            this.colRecordedByID.Caption = "Recorded By ID";
            this.colRecordedByID.FieldName = "RecordedByID";
            this.colRecordedByID.Name = "colRecordedByID";
            this.colRecordedByID.OptionsColumn.AllowEdit = false;
            this.colRecordedByID.OptionsColumn.AllowFocus = false;
            this.colRecordedByID.OptionsColumn.ReadOnly = true;
            this.colRecordedByID.Width = 96;
            // 
            // colRecordedByTypeID
            // 
            this.colRecordedByTypeID.Caption = "Recorded By Type ID";
            this.colRecordedByTypeID.FieldName = "RecordedByTypeID";
            this.colRecordedByTypeID.Name = "colRecordedByTypeID";
            this.colRecordedByTypeID.OptionsColumn.AllowEdit = false;
            this.colRecordedByTypeID.OptionsColumn.AllowFocus = false;
            this.colRecordedByTypeID.OptionsColumn.ReadOnly = true;
            this.colRecordedByTypeID.Width = 123;
            // 
            // colRecordedByType
            // 
            this.colRecordedByType.Caption = "Recorded By Type";
            this.colRecordedByType.FieldName = "RecordedByType";
            this.colRecordedByType.Name = "colRecordedByType";
            this.colRecordedByType.OptionsColumn.AllowEdit = false;
            this.colRecordedByType.OptionsColumn.AllowFocus = false;
            this.colRecordedByType.OptionsColumn.ReadOnly = true;
            this.colRecordedByType.Visible = true;
            this.colRecordedByType.VisibleIndex = 2;
            this.colRecordedByType.Width = 109;
            // 
            // colRecordedBy
            // 
            this.colRecordedBy.Caption = "Recorded By";
            this.colRecordedBy.FieldName = "RecordedBy";
            this.colRecordedBy.Name = "colRecordedBy";
            this.colRecordedBy.OptionsColumn.AllowEdit = false;
            this.colRecordedBy.OptionsColumn.AllowFocus = false;
            this.colRecordedBy.OptionsColumn.ReadOnly = true;
            this.colRecordedBy.Visible = true;
            this.colRecordedBy.VisibleIndex = 1;
            this.colRecordedBy.Width = 155;
            // 
            // colDateRecorded
            // 
            this.colDateRecorded.Caption = "Date Recorded";
            this.colDateRecorded.ColumnEdit = this.repositoryItemTextEditDateTime9;
            this.colDateRecorded.FieldName = "DateRecorded";
            this.colDateRecorded.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colDateRecorded.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateRecorded.Name = "colDateRecorded";
            this.colDateRecorded.OptionsColumn.AllowEdit = false;
            this.colDateRecorded.OptionsColumn.AllowFocus = false;
            this.colDateRecorded.OptionsColumn.ReadOnly = true;
            this.colDateRecorded.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateRecorded.Visible = true;
            this.colDateRecorded.VisibleIndex = 0;
            this.colDateRecorded.Width = 141;
            // 
            // colVisibleToClient
            // 
            this.colVisibleToClient.Caption = "Visible To Client";
            this.colVisibleToClient.ColumnEdit = this.repositoryItemCheckEdit10;
            this.colVisibleToClient.FieldName = "VisibleToClient";
            this.colVisibleToClient.Name = "colVisibleToClient";
            this.colVisibleToClient.OptionsColumn.AllowEdit = false;
            this.colVisibleToClient.OptionsColumn.AllowFocus = false;
            this.colVisibleToClient.OptionsColumn.ReadOnly = true;
            this.colVisibleToClient.Visible = true;
            this.colVisibleToClient.VisibleIndex = 3;
            this.colVisibleToClient.Width = 95;
            // 
            // xtraTabPageHealthAndSafety
            // 
            this.xtraTabPageHealthAndSafety.Controls.Add(this.gridControlHealthAndSafety);
            this.xtraTabPageHealthAndSafety.Name = "xtraTabPageHealthAndSafety";
            this.xtraTabPageHealthAndSafety.Size = new System.Drawing.Size(1052, 187);
            this.xtraTabPageHealthAndSafety.Text = "Health And Safety Requirements";
            // 
            // gridControlHealthAndSafety
            // 
            this.gridControlHealthAndSafety.DataSource = this.sp06039OMJobManagerLinkedSafetyRequirementBindingSource;
            this.gridControlHealthAndSafety.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlHealthAndSafety.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlHealthAndSafety.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlHealthAndSafety.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlHealthAndSafety.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlHealthAndSafety.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlHealthAndSafety.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlHealthAndSafety.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlHealthAndSafety.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlHealthAndSafety.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlHealthAndSafety.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlHealthAndSafety.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlHealthAndSafety.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 8, true, true, "Linked Documents", "linked_document")});
            this.gridControlHealthAndSafety.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlHealthAndSafety_EmbeddedNavigator_ButtonClick);
            this.gridControlHealthAndSafety.Location = new System.Drawing.Point(0, 0);
            this.gridControlHealthAndSafety.MainView = this.gridViewHealthAndSafety;
            this.gridControlHealthAndSafety.MenuManager = this.barManager1;
            this.gridControlHealthAndSafety.Name = "gridControlHealthAndSafety";
            this.gridControlHealthAndSafety.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit6,
            this.repositoryItemCheckEdit11,
            this.repositoryItemTextEditDateTime8,
            this.repositoryItemTextEditHTML11});
            this.gridControlHealthAndSafety.Size = new System.Drawing.Size(1052, 187);
            this.gridControlHealthAndSafety.TabIndex = 1;
            this.gridControlHealthAndSafety.UseEmbeddedNavigator = true;
            this.gridControlHealthAndSafety.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewHealthAndSafety});
            // 
            // sp06039OMJobManagerLinkedSafetyRequirementBindingSource
            // 
            this.sp06039OMJobManagerLinkedSafetyRequirementBindingSource.DataMember = "sp06039_OM_Job_Manager_Linked_Safety_Requirement";
            this.sp06039OMJobManagerLinkedSafetyRequirementBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridViewHealthAndSafety
            // 
            this.gridViewHealthAndSafety.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn58,
            this.gridColumn59,
            this.gridColumn61,
            this.gridColumn62,
            this.gridColumn77,
            this.gridColumn78,
            this.gridColumn79,
            this.gridColumn80,
            this.gridColumn81,
            this.gridColumn82,
            this.gridColumn100,
            this.gridColumn108,
            this.gridColumn111,
            this.gridColumn112,
            this.gridColumn113,
            this.gridColumn114});
            this.gridViewHealthAndSafety.GridControl = this.gridControlHealthAndSafety;
            this.gridViewHealthAndSafety.GroupCount = 1;
            this.gridViewHealthAndSafety.Name = "gridViewHealthAndSafety";
            this.gridViewHealthAndSafety.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewHealthAndSafety.OptionsFind.FindDelay = 2000;
            this.gridViewHealthAndSafety.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewHealthAndSafety.OptionsLayout.StoreAppearance = true;
            this.gridViewHealthAndSafety.OptionsLayout.StoreFormatRules = true;
            this.gridViewHealthAndSafety.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewHealthAndSafety.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewHealthAndSafety.OptionsSelection.MultiSelect = true;
            this.gridViewHealthAndSafety.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewHealthAndSafety.OptionsView.ColumnAutoWidth = false;
            this.gridViewHealthAndSafety.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewHealthAndSafety.OptionsView.ShowGroupPanel = false;
            this.gridViewHealthAndSafety.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn112, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn113, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewHealthAndSafety.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewHealthAndSafety.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewHealthAndSafety.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewHealthAndSafety.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewHealthAndSafety.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewHealthAndSafety.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewHealthAndSafety.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewHealthAndSafety_MouseUp);
            this.gridViewHealthAndSafety.DoubleClick += new System.EventHandler(this.gridViewHealthAndSafety_DoubleClick);
            this.gridViewHealthAndSafety.GotFocus += new System.EventHandler(this.gridViewHealthAndSafety_GotFocus);
            // 
            // gridColumn58
            // 
            this.gridColumn58.Caption = "Safety Requirement ID";
            this.gridColumn58.FieldName = "SafetyUsedID";
            this.gridColumn58.Name = "gridColumn58";
            this.gridColumn58.OptionsColumn.AllowEdit = false;
            this.gridColumn58.OptionsColumn.AllowFocus = false;
            this.gridColumn58.OptionsColumn.ReadOnly = true;
            this.gridColumn58.Width = 108;
            // 
            // gridColumn59
            // 
            this.gridColumn59.Caption = "Labour ID";
            this.gridColumn59.FieldName = "SafetyID";
            this.gridColumn59.Name = "gridColumn59";
            this.gridColumn59.OptionsColumn.AllowEdit = false;
            this.gridColumn59.OptionsColumn.AllowFocus = false;
            this.gridColumn59.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn61
            // 
            this.gridColumn61.Caption = "Job ID";
            this.gridColumn61.FieldName = "JobID";
            this.gridColumn61.Name = "gridColumn61";
            this.gridColumn61.OptionsColumn.AllowEdit = false;
            this.gridColumn61.OptionsColumn.AllowFocus = false;
            this.gridColumn61.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn62
            // 
            this.gridColumn62.Caption = "Created From Safety Required ID";
            this.gridColumn62.FieldName = "CreatedFromSafetyRequiredID";
            this.gridColumn62.Name = "gridColumn62";
            this.gridColumn62.OptionsColumn.AllowEdit = false;
            this.gridColumn62.OptionsColumn.AllowFocus = false;
            this.gridColumn62.OptionsColumn.ReadOnly = true;
            this.gridColumn62.Width = 183;
            // 
            // gridColumn77
            // 
            this.gridColumn77.Caption = "Remarks";
            this.gridColumn77.ColumnEdit = this.repositoryItemMemoExEdit6;
            this.gridColumn77.FieldName = "Remarks";
            this.gridColumn77.Name = "gridColumn77";
            this.gridColumn77.OptionsColumn.ReadOnly = true;
            this.gridColumn77.Visible = true;
            this.gridColumn77.VisibleIndex = 1;
            this.gridColumn77.Width = 176;
            // 
            // repositoryItemMemoExEdit6
            // 
            this.repositoryItemMemoExEdit6.AutoHeight = false;
            this.repositoryItemMemoExEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit6.Name = "repositoryItemMemoExEdit6";
            this.repositoryItemMemoExEdit6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit6.ShowIcon = false;
            // 
            // gridColumn78
            // 
            this.gridColumn78.Caption = "Client ID";
            this.gridColumn78.FieldName = "ClientID";
            this.gridColumn78.Name = "gridColumn78";
            this.gridColumn78.OptionsColumn.AllowEdit = false;
            this.gridColumn78.OptionsColumn.AllowFocus = false;
            this.gridColumn78.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn79
            // 
            this.gridColumn79.Caption = "Site ID";
            this.gridColumn79.FieldName = "SiteID";
            this.gridColumn79.Name = "gridColumn79";
            this.gridColumn79.OptionsColumn.AllowEdit = false;
            this.gridColumn79.OptionsColumn.AllowFocus = false;
            this.gridColumn79.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn80
            // 
            this.gridColumn80.Caption = "Site Name";
            this.gridColumn80.FieldName = "SiteName";
            this.gridColumn80.Name = "gridColumn80";
            this.gridColumn80.OptionsColumn.AllowEdit = false;
            this.gridColumn80.OptionsColumn.AllowFocus = false;
            this.gridColumn80.OptionsColumn.ReadOnly = true;
            this.gridColumn80.Width = 133;
            // 
            // gridColumn81
            // 
            this.gridColumn81.Caption = "Client Name";
            this.gridColumn81.FieldName = "ClientName";
            this.gridColumn81.Name = "gridColumn81";
            this.gridColumn81.OptionsColumn.AllowEdit = false;
            this.gridColumn81.OptionsColumn.AllowFocus = false;
            this.gridColumn81.OptionsColumn.ReadOnly = true;
            this.gridColumn81.Width = 139;
            // 
            // gridColumn82
            // 
            this.gridColumn82.Caption = "Job Sub-Type";
            this.gridColumn82.FieldName = "JobSubTypeDescription";
            this.gridColumn82.Name = "gridColumn82";
            this.gridColumn82.OptionsColumn.AllowEdit = false;
            this.gridColumn82.OptionsColumn.AllowFocus = false;
            this.gridColumn82.OptionsColumn.ReadOnly = true;
            this.gridColumn82.Width = 150;
            // 
            // gridColumn100
            // 
            this.gridColumn100.Caption = "Job Type";
            this.gridColumn100.FieldName = "JobTypeDescription";
            this.gridColumn100.Name = "gridColumn100";
            this.gridColumn100.OptionsColumn.AllowEdit = false;
            this.gridColumn100.OptionsColumn.AllowFocus = false;
            this.gridColumn100.OptionsColumn.ReadOnly = true;
            this.gridColumn100.Width = 169;
            // 
            // gridColumn108
            // 
            this.gridColumn108.Caption = "Expected Start Date";
            this.gridColumn108.ColumnEdit = this.repositoryItemTextEditDateTime8;
            this.gridColumn108.FieldName = "ExpectedStartDate";
            this.gridColumn108.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn108.Name = "gridColumn108";
            this.gridColumn108.OptionsColumn.AllowEdit = false;
            this.gridColumn108.OptionsColumn.AllowFocus = false;
            this.gridColumn108.OptionsColumn.ReadOnly = true;
            this.gridColumn108.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn108.Width = 119;
            // 
            // repositoryItemTextEditDateTime8
            // 
            this.repositoryItemTextEditDateTime8.AutoHeight = false;
            this.repositoryItemTextEditDateTime8.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime8.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime8.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime8.Name = "repositoryItemTextEditDateTime8";
            // 
            // gridColumn111
            // 
            this.gridColumn111.Caption = "Visit #";
            this.gridColumn111.FieldName = "VisitNumber";
            this.gridColumn111.Name = "gridColumn111";
            this.gridColumn111.OptionsColumn.AllowEdit = false;
            this.gridColumn111.OptionsColumn.AllowFocus = false;
            this.gridColumn111.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn112
            // 
            this.gridColumn112.Caption = "Linked To Job";
            this.gridColumn112.ColumnEdit = this.repositoryItemTextEditHTML11;
            this.gridColumn112.FieldName = "FullDescription";
            this.gridColumn112.Name = "gridColumn112";
            this.gridColumn112.OptionsColumn.AllowEdit = false;
            this.gridColumn112.OptionsColumn.AllowFocus = false;
            this.gridColumn112.OptionsColumn.ReadOnly = true;
            this.gridColumn112.Visible = true;
            this.gridColumn112.VisibleIndex = 2;
            this.gridColumn112.Width = 415;
            // 
            // repositoryItemTextEditHTML11
            // 
            this.repositoryItemTextEditHTML11.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTextEditHTML11.AutoHeight = false;
            this.repositoryItemTextEditHTML11.Name = "repositoryItemTextEditHTML11";
            // 
            // gridColumn113
            // 
            this.gridColumn113.Caption = "Safety Requirement Description";
            this.gridColumn113.FieldName = "SafetyDescription";
            this.gridColumn113.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn113.Name = "gridColumn113";
            this.gridColumn113.OptionsColumn.AllowEdit = false;
            this.gridColumn113.OptionsColumn.AllowFocus = false;
            this.gridColumn113.OptionsColumn.ReadOnly = true;
            this.gridColumn113.Visible = true;
            this.gridColumn113.VisibleIndex = 0;
            this.gridColumn113.Width = 210;
            // 
            // gridColumn114
            // 
            this.gridColumn114.Caption = "Selected";
            this.gridColumn114.ColumnEdit = this.repositoryItemCheckEdit11;
            this.gridColumn114.FieldName = "Selected";
            this.gridColumn114.Name = "gridColumn114";
            this.gridColumn114.OptionsColumn.AllowEdit = false;
            this.gridColumn114.OptionsColumn.AllowFocus = false;
            this.gridColumn114.OptionsColumn.ReadOnly = true;
            this.gridColumn114.Width = 62;
            // 
            // repositoryItemCheckEdit11
            // 
            this.repositoryItemCheckEdit11.AutoHeight = false;
            this.repositoryItemCheckEdit11.Caption = "Check";
            this.repositoryItemCheckEdit11.Name = "repositoryItemCheckEdit11";
            this.repositoryItemCheckEdit11.ValueChecked = 1;
            this.repositoryItemCheckEdit11.ValueUnchecked = 0;
            // 
            // xtraTabPageCRM
            // 
            this.xtraTabPageCRM.Controls.Add(this.gridSplitContainer4);
            this.xtraTabPageCRM.Name = "xtraTabPageCRM";
            this.xtraTabPageCRM.Size = new System.Drawing.Size(1052, 187);
            this.xtraTabPageCRM.Text = "CRM";
            // 
            // gridSplitContainer4
            // 
            this.gridSplitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer4.Grid = this.gridControlCRM;
            this.gridSplitContainer4.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer4.Name = "gridSplitContainer4";
            this.gridSplitContainer4.Panel1.Controls.Add(this.popupContainerControlConstructionManagerFilter);
            this.gridSplitContainer4.Panel1.Controls.Add(this.popupContainerControlKAMFilter);
            this.gridSplitContainer4.Panel1.Controls.Add(this.popupContainerControlCMFilter);
            this.gridSplitContainer4.Panel1.Controls.Add(this.gridControlCRM);
            this.gridSplitContainer4.Size = new System.Drawing.Size(1052, 187);
            this.gridSplitContainer4.TabIndex = 0;
            // 
            // gridControlCRM
            // 
            this.gridControlCRM.DataSource = this.sp05089CRMContactsLinkedToRecordBindingSource;
            this.gridControlCRM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlCRM.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlCRM.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlCRM.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlCRM.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlCRM.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlCRM.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlCRM.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlCRM.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlCRM.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlCRM.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlCRM.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlCRM.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selcted Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 8, true, true, "Linked Documents", "linked_document")});
            this.gridControlCRM.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlCRM_EmbeddedNavigator_ButtonClick);
            this.gridControlCRM.Location = new System.Drawing.Point(0, 0);
            this.gridControlCRM.MainView = this.gridViewCRM;
            this.gridControlCRM.MenuManager = this.barManager1;
            this.gridControlCRM.Name = "gridControlCRM";
            this.gridControlCRM.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit12,
            this.repositoryItemHyperLinkEdit3,
            this.repositoryItemTextDateTime,
            this.repositoryItemTextEditHTML12});
            this.gridControlCRM.Size = new System.Drawing.Size(1052, 187);
            this.gridControlCRM.TabIndex = 6;
            this.gridControlCRM.UseEmbeddedNavigator = true;
            this.gridControlCRM.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewCRM});
            // 
            // sp05089CRMContactsLinkedToRecordBindingSource
            // 
            this.sp05089CRMContactsLinkedToRecordBindingSource.DataMember = "sp05089_CRM_Contacts_Linked_To_Record";
            this.sp05089CRMContactsLinkedToRecordBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridViewCRM
            // 
            this.gridViewCRM.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCRMID,
            this.gridColumn7,
            this.colClientContactID,
            this.colLinkedToRecordTypeID1,
            this.colLinkedToRecordID1,
            this.colContactDueDateTime,
            this.colContactMadeDateTime,
            this.colContactMethodID,
            this.colContactTypeID,
            this.gridColumn8,
            this.gridColumn9,
            this.colStatusID,
            this.colCreatedByStaffID,
            this.colContactedByStaffID,
            this.colContactDirectionID,
            this.colLinkedRecordDescription1,
            this.colCreatedByStaffName,
            this.colContactedByStaffName,
            this.colContactMethod,
            this.colContactType,
            this.colStatusDescription,
            this.colContactDirectionDescription,
            this.gridColumn10,
            this.colLinkedRecordTypeDescription,
            this.colDateCreated1,
            this.colClientContact});
            this.gridViewCRM.GridControl = this.gridControlCRM;
            this.gridViewCRM.GroupCount = 1;
            this.gridViewCRM.Name = "gridViewCRM";
            this.gridViewCRM.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewCRM.OptionsFind.FindDelay = 2000;
            this.gridViewCRM.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewCRM.OptionsLayout.StoreAppearance = true;
            this.gridViewCRM.OptionsLayout.StoreFormatRules = true;
            this.gridViewCRM.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewCRM.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewCRM.OptionsSelection.MultiSelect = true;
            this.gridViewCRM.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewCRM.OptionsView.ColumnAutoWidth = false;
            this.gridViewCRM.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewCRM.OptionsView.ShowGroupPanel = false;
            this.gridViewCRM.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedRecordDescription1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateCreated1, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridViewCRM.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewCRM.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewCRM.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewCRM.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewCRM.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewCRM.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewCRM.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewCRM_MouseUp);
            this.gridViewCRM.DoubleClick += new System.EventHandler(this.gridViewCRM_DoubleClick);
            this.gridViewCRM.GotFocus += new System.EventHandler(this.gridViewCRM_GotFocus);
            // 
            // colCRMID
            // 
            this.colCRMID.Caption = "CRM ID";
            this.colCRMID.FieldName = "CRMID";
            this.colCRMID.Name = "colCRMID";
            this.colCRMID.OptionsColumn.AllowEdit = false;
            this.colCRMID.OptionsColumn.AllowFocus = false;
            this.colCRMID.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Client ID";
            this.gridColumn7.FieldName = "ClientID";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            // 
            // colClientContactID
            // 
            this.colClientContactID.Caption = "Client Contact ID";
            this.colClientContactID.FieldName = "ClientContactID";
            this.colClientContactID.Name = "colClientContactID";
            this.colClientContactID.OptionsColumn.AllowEdit = false;
            this.colClientContactID.OptionsColumn.AllowFocus = false;
            this.colClientContactID.OptionsColumn.ReadOnly = true;
            this.colClientContactID.Width = 100;
            // 
            // colLinkedToRecordTypeID1
            // 
            this.colLinkedToRecordTypeID1.Caption = "Linked To Record Type ID";
            this.colLinkedToRecordTypeID1.FieldName = "LinkedToRecordTypeID";
            this.colLinkedToRecordTypeID1.Name = "colLinkedToRecordTypeID1";
            this.colLinkedToRecordTypeID1.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordTypeID1.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordTypeID1.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordTypeID1.Width = 141;
            // 
            // colLinkedToRecordID1
            // 
            this.colLinkedToRecordID1.Caption = "Linked To Record ID";
            this.colLinkedToRecordID1.FieldName = "LinkedToRecordID";
            this.colLinkedToRecordID1.Name = "colLinkedToRecordID1";
            this.colLinkedToRecordID1.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordID1.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordID1.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordID1.Width = 114;
            // 
            // colContactDueDateTime
            // 
            this.colContactDueDateTime.Caption = "Contact Due";
            this.colContactDueDateTime.ColumnEdit = this.repositoryItemTextDateTime;
            this.colContactDueDateTime.FieldName = "ContactDueDateTime";
            this.colContactDueDateTime.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colContactDueDateTime.Name = "colContactDueDateTime";
            this.colContactDueDateTime.OptionsColumn.AllowEdit = false;
            this.colContactDueDateTime.OptionsColumn.AllowFocus = false;
            this.colContactDueDateTime.OptionsColumn.ReadOnly = true;
            this.colContactDueDateTime.Visible = true;
            this.colContactDueDateTime.VisibleIndex = 3;
            this.colContactDueDateTime.Width = 108;
            // 
            // repositoryItemTextDateTime
            // 
            this.repositoryItemTextDateTime.AutoHeight = false;
            this.repositoryItemTextDateTime.Mask.EditMask = "g";
            this.repositoryItemTextDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextDateTime.Name = "repositoryItemTextDateTime";
            // 
            // colContactMadeDateTime
            // 
            this.colContactMadeDateTime.Caption = "Contact Made";
            this.colContactMadeDateTime.ColumnEdit = this.repositoryItemTextDateTime;
            this.colContactMadeDateTime.FieldName = "ContactMadeDateTime";
            this.colContactMadeDateTime.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colContactMadeDateTime.Name = "colContactMadeDateTime";
            this.colContactMadeDateTime.OptionsColumn.AllowEdit = false;
            this.colContactMadeDateTime.OptionsColumn.AllowFocus = false;
            this.colContactMadeDateTime.OptionsColumn.ReadOnly = true;
            this.colContactMadeDateTime.Visible = true;
            this.colContactMadeDateTime.VisibleIndex = 4;
            this.colContactMadeDateTime.Width = 102;
            // 
            // colContactMethodID
            // 
            this.colContactMethodID.Caption = "Contact Method ID";
            this.colContactMethodID.FieldName = "ContactMethodID";
            this.colContactMethodID.Name = "colContactMethodID";
            this.colContactMethodID.OptionsColumn.AllowEdit = false;
            this.colContactMethodID.OptionsColumn.AllowFocus = false;
            this.colContactMethodID.OptionsColumn.ReadOnly = true;
            this.colContactMethodID.Width = 109;
            // 
            // colContactTypeID
            // 
            this.colContactTypeID.Caption = "Contact Type ID";
            this.colContactTypeID.FieldName = "ContactTypeID";
            this.colContactTypeID.Name = "colContactTypeID";
            this.colContactTypeID.OptionsColumn.AllowEdit = false;
            this.colContactTypeID.OptionsColumn.AllowFocus = false;
            this.colContactTypeID.OptionsColumn.ReadOnly = true;
            this.colContactTypeID.Width = 97;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Description";
            this.gridColumn8.FieldName = "Description";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 7;
            this.gridColumn8.Width = 260;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Remarks";
            this.gridColumn9.ColumnEdit = this.repositoryItemMemoExEdit12;
            this.gridColumn9.FieldName = "Remarks";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 12;
            // 
            // repositoryItemMemoExEdit12
            // 
            this.repositoryItemMemoExEdit12.AutoHeight = false;
            this.repositoryItemMemoExEdit12.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit12.Name = "repositoryItemMemoExEdit12";
            this.repositoryItemMemoExEdit12.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit12.ShowIcon = false;
            // 
            // colStatusID
            // 
            this.colStatusID.Caption = "Status ID";
            this.colStatusID.FieldName = "StatusID";
            this.colStatusID.Name = "colStatusID";
            this.colStatusID.OptionsColumn.AllowEdit = false;
            this.colStatusID.OptionsColumn.AllowFocus = false;
            this.colStatusID.OptionsColumn.ReadOnly = true;
            // 
            // colCreatedByStaffID
            // 
            this.colCreatedByStaffID.Caption = "Created By Staff ID";
            this.colCreatedByStaffID.FieldName = "CreatedByStaffID";
            this.colCreatedByStaffID.Name = "colCreatedByStaffID";
            this.colCreatedByStaffID.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffID.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffID.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffID.Width = 113;
            // 
            // colContactedByStaffID
            // 
            this.colContactedByStaffID.Caption = "Contacted By";
            this.colContactedByStaffID.FieldName = "ContactedByStaffID";
            this.colContactedByStaffID.Name = "colContactedByStaffID";
            this.colContactedByStaffID.OptionsColumn.AllowEdit = false;
            this.colContactedByStaffID.OptionsColumn.AllowFocus = false;
            this.colContactedByStaffID.OptionsColumn.ReadOnly = true;
            this.colContactedByStaffID.Width = 83;
            // 
            // colContactDirectionID
            // 
            this.colContactDirectionID.Caption = "Contact Direction ID";
            this.colContactDirectionID.FieldName = "ContactDirectionID";
            this.colContactDirectionID.Name = "colContactDirectionID";
            this.colContactDirectionID.OptionsColumn.AllowEdit = false;
            this.colContactDirectionID.OptionsColumn.AllowFocus = false;
            this.colContactDirectionID.OptionsColumn.ReadOnly = true;
            this.colContactDirectionID.Width = 101;
            // 
            // colLinkedRecordDescription1
            // 
            this.colLinkedRecordDescription1.Caption = "Linked To Job";
            this.colLinkedRecordDescription1.ColumnEdit = this.repositoryItemTextEditHTML12;
            this.colLinkedRecordDescription1.FieldName = "LinkedRecordDescription";
            this.colLinkedRecordDescription1.Name = "colLinkedRecordDescription1";
            this.colLinkedRecordDescription1.OptionsColumn.ReadOnly = true;
            this.colLinkedRecordDescription1.Width = 191;
            // 
            // repositoryItemTextEditHTML12
            // 
            this.repositoryItemTextEditHTML12.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTextEditHTML12.AutoHeight = false;
            this.repositoryItemTextEditHTML12.Name = "repositoryItemTextEditHTML12";
            // 
            // colCreatedByStaffName
            // 
            this.colCreatedByStaffName.Caption = "Created By";
            this.colCreatedByStaffName.FieldName = "CreatedByStaffName";
            this.colCreatedByStaffName.Name = "colCreatedByStaffName";
            this.colCreatedByStaffName.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffName.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffName.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffName.Visible = true;
            this.colCreatedByStaffName.VisibleIndex = 11;
            this.colCreatedByStaffName.Width = 121;
            // 
            // colContactedByStaffName
            // 
            this.colContactedByStaffName.Caption = "GC Contact Person";
            this.colContactedByStaffName.FieldName = "ContactedByStaffName";
            this.colContactedByStaffName.Name = "colContactedByStaffName";
            this.colContactedByStaffName.OptionsColumn.AllowEdit = false;
            this.colContactedByStaffName.OptionsColumn.AllowFocus = false;
            this.colContactedByStaffName.OptionsColumn.ReadOnly = true;
            this.colContactedByStaffName.Visible = true;
            this.colContactedByStaffName.VisibleIndex = 5;
            this.colContactedByStaffName.Width = 133;
            // 
            // colContactMethod
            // 
            this.colContactMethod.Caption = "Contact Method";
            this.colContactMethod.FieldName = "ContactMethod";
            this.colContactMethod.Name = "colContactMethod";
            this.colContactMethod.OptionsColumn.AllowEdit = false;
            this.colContactMethod.OptionsColumn.AllowFocus = false;
            this.colContactMethod.OptionsColumn.ReadOnly = true;
            this.colContactMethod.Visible = true;
            this.colContactMethod.VisibleIndex = 8;
            this.colContactMethod.Width = 104;
            // 
            // colContactType
            // 
            this.colContactType.Caption = "Contact Type";
            this.colContactType.FieldName = "ContactType";
            this.colContactType.Name = "colContactType";
            this.colContactType.OptionsColumn.AllowEdit = false;
            this.colContactType.OptionsColumn.AllowFocus = false;
            this.colContactType.OptionsColumn.ReadOnly = true;
            this.colContactType.Visible = true;
            this.colContactType.VisibleIndex = 9;
            this.colContactType.Width = 102;
            // 
            // colStatusDescription
            // 
            this.colStatusDescription.Caption = "Status";
            this.colStatusDescription.FieldName = "StatusDescription";
            this.colStatusDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colStatusDescription.Name = "colStatusDescription";
            this.colStatusDescription.OptionsColumn.AllowEdit = false;
            this.colStatusDescription.OptionsColumn.AllowFocus = false;
            this.colStatusDescription.OptionsColumn.ReadOnly = true;
            this.colStatusDescription.Visible = true;
            this.colStatusDescription.VisibleIndex = 1;
            // 
            // colContactDirectionDescription
            // 
            this.colContactDirectionDescription.Caption = "Contact Direction";
            this.colContactDirectionDescription.FieldName = "ContactDirectionDescription";
            this.colContactDirectionDescription.Name = "colContactDirectionDescription";
            this.colContactDirectionDescription.OptionsColumn.AllowEdit = false;
            this.colContactDirectionDescription.OptionsColumn.AllowFocus = false;
            this.colContactDirectionDescription.OptionsColumn.ReadOnly = true;
            this.colContactDirectionDescription.Visible = true;
            this.colContactDirectionDescription.VisibleIndex = 10;
            this.colContactDirectionDescription.Width = 101;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Client";
            this.gridColumn10.FieldName = "ClientName";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 2;
            this.gridColumn10.Width = 204;
            // 
            // colLinkedRecordTypeDescription
            // 
            this.colLinkedRecordTypeDescription.Caption = "Linked Record Type";
            this.colLinkedRecordTypeDescription.FieldName = "LinkedRecordTypeDescription";
            this.colLinkedRecordTypeDescription.Name = "colLinkedRecordTypeDescription";
            this.colLinkedRecordTypeDescription.OptionsColumn.AllowEdit = false;
            this.colLinkedRecordTypeDescription.OptionsColumn.AllowFocus = false;
            this.colLinkedRecordTypeDescription.OptionsColumn.ReadOnly = true;
            this.colLinkedRecordTypeDescription.Width = 112;
            // 
            // colDateCreated1
            // 
            this.colDateCreated1.Caption = "Date Created";
            this.colDateCreated1.ColumnEdit = this.repositoryItemTextDateTime;
            this.colDateCreated1.FieldName = "DateCreated";
            this.colDateCreated1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colDateCreated1.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateCreated1.Name = "colDateCreated1";
            this.colDateCreated1.OptionsColumn.AllowEdit = false;
            this.colDateCreated1.OptionsColumn.AllowFocus = false;
            this.colDateCreated1.OptionsColumn.ReadOnly = true;
            this.colDateCreated1.Visible = true;
            this.colDateCreated1.VisibleIndex = 0;
            this.colDateCreated1.Width = 120;
            // 
            // colClientContact
            // 
            this.colClientContact.Caption = "Client Contact Person";
            this.colClientContact.FieldName = "ClientContact";
            this.colClientContact.Name = "colClientContact";
            this.colClientContact.OptionsColumn.AllowEdit = false;
            this.colClientContact.OptionsColumn.AllowFocus = false;
            this.colClientContact.OptionsColumn.ReadOnly = true;
            this.colClientContact.Visible = true;
            this.colClientContact.VisibleIndex = 6;
            this.colClientContact.Width = 122;
            // 
            // repositoryItemHyperLinkEdit3
            // 
            this.repositoryItemHyperLinkEdit3.AutoHeight = false;
            this.repositoryItemHyperLinkEdit3.Name = "repositoryItemHyperLinkEdit3";
            this.repositoryItemHyperLinkEdit3.SingleClick = true;
            // 
            // popupContainerControlConstructionManagerFilter
            // 
            this.popupContainerControlConstructionManagerFilter.Controls.Add(this.btnConstructionManagerFilterOK);
            this.popupContainerControlConstructionManagerFilter.Controls.Add(this.gridControl1);
            this.popupContainerControlConstructionManagerFilter.Location = new System.Drawing.Point(391, 28);
            this.popupContainerControlConstructionManagerFilter.Name = "popupContainerControlConstructionManagerFilter";
            this.popupContainerControlConstructionManagerFilter.Size = new System.Drawing.Size(132, 135);
            this.popupContainerControlConstructionManagerFilter.TabIndex = 18;
            // 
            // btnConstructionManagerFilterOK
            // 
            this.btnConstructionManagerFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnConstructionManagerFilterOK.Location = new System.Drawing.Point(3, 112);
            this.btnConstructionManagerFilterOK.Name = "btnConstructionManagerFilterOK";
            this.btnConstructionManagerFilterOK.Size = new System.Drawing.Size(50, 20);
            this.btnConstructionManagerFilterOK.TabIndex = 18;
            this.btnConstructionManagerFilterOK.Text = "OK";
            this.btnConstructionManagerFilterOK.Click += new System.EventHandler(this.btnConstructionManagerFilterOK_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.DataSource = this.sp06294OMJobManagerConstructionManagerFilterBindingSource;
            this.gridControl1.Location = new System.Drawing.Point(3, 3);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(126, 107);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp06294OMJobManagerConstructionManagerFilterBindingSource
            // 
            this.sp06294OMJobManagerConstructionManagerFilterBindingSource.DataMember = "sp06294_OM_Job_Manager_Construction_Manager_Filter";
            this.sp06294OMJobManagerConstructionManagerFilterBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn12,
            this.gridColumn37,
            this.gridColumn39});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn12, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Surname";
            this.gridColumn12.FieldName = "Surname";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 0;
            this.gridColumn12.Width = 122;
            // 
            // gridColumn37
            // 
            this.gridColumn37.Caption = "Forename";
            this.gridColumn37.FieldName = "Forename";
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.OptionsColumn.AllowEdit = false;
            this.gridColumn37.OptionsColumn.AllowFocus = false;
            this.gridColumn37.OptionsColumn.ReadOnly = true;
            this.gridColumn37.Visible = true;
            this.gridColumn37.VisibleIndex = 1;
            this.gridColumn37.Width = 82;
            // 
            // gridColumn39
            // 
            this.gridColumn39.Caption = "ID";
            this.gridColumn39.FieldName = "ID";
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.OptionsColumn.AllowEdit = false;
            this.gridColumn39.OptionsColumn.AllowFocus = false;
            this.gridColumn39.OptionsColumn.ReadOnly = true;
            this.gridColumn39.Width = 53;
            // 
            // popupContainerControlKAMFilter
            // 
            this.popupContainerControlKAMFilter.Controls.Add(this.btnKAMFilterOK);
            this.popupContainerControlKAMFilter.Controls.Add(this.gridControl5);
            this.popupContainerControlKAMFilter.Location = new System.Drawing.Point(90, 25);
            this.popupContainerControlKAMFilter.Name = "popupContainerControlKAMFilter";
            this.popupContainerControlKAMFilter.Size = new System.Drawing.Size(131, 135);
            this.popupContainerControlKAMFilter.TabIndex = 15;
            // 
            // btnKAMFilterOK
            // 
            this.btnKAMFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnKAMFilterOK.Location = new System.Drawing.Point(3, 112);
            this.btnKAMFilterOK.Name = "btnKAMFilterOK";
            this.btnKAMFilterOK.Size = new System.Drawing.Size(50, 20);
            this.btnKAMFilterOK.TabIndex = 18;
            this.btnKAMFilterOK.Text = "OK";
            this.btnKAMFilterOK.Click += new System.EventHandler(this.btnKAMFilterOK_Click);
            // 
            // gridControl5
            // 
            this.gridControl5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl5.DataSource = this.sp06019OMJobManagerKAMsFilterBindingSource;
            this.gridControl5.Location = new System.Drawing.Point(3, 3);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.Size = new System.Drawing.Size(125, 107);
            this.gridControl5.TabIndex = 1;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp06019OMJobManagerKAMsFilterBindingSource
            // 
            this.sp06019OMJobManagerKAMsFilterBindingSource.DataMember = "sp06019_OM_Job_Manager_KAMs_Filter";
            this.sp06019OMJobManagerKAMsFilterBindingSource.DataSource = this.dataSet_GC_Summer_Core;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription3,
            this.colForename,
            this.colID1});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsFind.AlwaysVisible = true;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription3, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription3
            // 
            this.colDescription3.Caption = "Surname";
            this.colDescription3.FieldName = "Surname";
            this.colDescription3.Name = "colDescription3";
            this.colDescription3.OptionsColumn.AllowEdit = false;
            this.colDescription3.OptionsColumn.AllowFocus = false;
            this.colDescription3.OptionsColumn.ReadOnly = true;
            this.colDescription3.Visible = true;
            this.colDescription3.VisibleIndex = 0;
            this.colDescription3.Width = 122;
            // 
            // colForename
            // 
            this.colForename.Caption = "Forename";
            this.colForename.FieldName = "Forename";
            this.colForename.Name = "colForename";
            this.colForename.OptionsColumn.AllowEdit = false;
            this.colForename.OptionsColumn.AllowFocus = false;
            this.colForename.OptionsColumn.ReadOnly = true;
            this.colForename.Visible = true;
            this.colForename.VisibleIndex = 1;
            this.colForename.Width = 82;
            // 
            // colID1
            // 
            this.colID1.Caption = "ID";
            this.colID1.FieldName = "ID";
            this.colID1.Name = "colID1";
            this.colID1.OptionsColumn.AllowEdit = false;
            this.colID1.OptionsColumn.AllowFocus = false;
            this.colID1.OptionsColumn.ReadOnly = true;
            this.colID1.Width = 53;
            // 
            // popupContainerControlCMFilter
            // 
            this.popupContainerControlCMFilter.Controls.Add(this.btnCMFilterOK);
            this.popupContainerControlCMFilter.Controls.Add(this.gridControl9);
            this.popupContainerControlCMFilter.Location = new System.Drawing.Point(237, 28);
            this.popupContainerControlCMFilter.Name = "popupContainerControlCMFilter";
            this.popupContainerControlCMFilter.Size = new System.Drawing.Size(132, 135);
            this.popupContainerControlCMFilter.TabIndex = 17;
            // 
            // btnCMFilterOK
            // 
            this.btnCMFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCMFilterOK.Location = new System.Drawing.Point(3, 112);
            this.btnCMFilterOK.Name = "btnCMFilterOK";
            this.btnCMFilterOK.Size = new System.Drawing.Size(50, 20);
            this.btnCMFilterOK.TabIndex = 18;
            this.btnCMFilterOK.Text = "OK";
            this.btnCMFilterOK.Click += new System.EventHandler(this.btnCMFilterOK_Click);
            // 
            // gridControl9
            // 
            this.gridControl9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl9.DataSource = this.sp06020OMJobManagerCMsFilterBindingSource;
            this.gridControl9.Location = new System.Drawing.Point(3, 3);
            this.gridControl9.MainView = this.gridView9;
            this.gridControl9.MenuManager = this.barManager1;
            this.gridControl9.Name = "gridControl9";
            this.gridControl9.Size = new System.Drawing.Size(126, 107);
            this.gridControl9.TabIndex = 1;
            this.gridControl9.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView9});
            // 
            // sp06020OMJobManagerCMsFilterBindingSource
            // 
            this.sp06020OMJobManagerCMsFilterBindingSource.DataMember = "sp06020_OM_Job_Manager_CMs_Filter";
            this.sp06020OMJobManagerCMsFilterBindingSource.DataSource = this.dataSet_GC_Summer_Core;
            // 
            // gridView9
            // 
            this.gridView9.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn11});
            this.gridView9.GridControl = this.gridControl9;
            this.gridView9.Name = "gridView9";
            this.gridView9.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView9.OptionsFind.AlwaysVisible = true;
            this.gridView9.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView9.OptionsLayout.StoreAppearance = true;
            this.gridView9.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView9.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView9.OptionsView.ColumnAutoWidth = false;
            this.gridView9.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView9.OptionsView.ShowGroupPanel = false;
            this.gridView9.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView9.OptionsView.ShowIndicator = false;
            this.gridView9.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn5, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Surname";
            this.gridColumn5.FieldName = "Surname";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 122;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Forename";
            this.gridColumn6.FieldName = "Forename";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 1;
            this.gridColumn6.Width = 82;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "ID";
            this.gridColumn11.FieldName = "ID";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Width = 53;
            // 
            // xtraTabPagePurchaseOrders
            // 
            this.xtraTabPagePurchaseOrders.Controls.Add(this.labelControl3);
            this.xtraTabPagePurchaseOrders.Name = "xtraTabPagePurchaseOrders";
            this.xtraTabPagePurchaseOrders.Size = new System.Drawing.Size(1052, 187);
            this.xtraTabPagePurchaseOrders.Text = "Purchase Orders";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(14, 15);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(53, 13);
            this.labelControl3.TabIndex = 0;
            this.labelControl3.Text = "Future Use";
            // 
            // dataSet_GC_Reports
            // 
            this.dataSet_GC_Reports.DataSetName = "DataSet_GC_Reports";
            this.dataSet_GC_Reports.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // popupContainerEditDateRange
            // 
            this.popupContainerEditDateRange.EditValue = "No Date Range Filter";
            this.popupContainerEditDateRange.Location = new System.Drawing.Point(108, 241);
            this.popupContainerEditDateRange.MenuManager = this.barManager1;
            this.popupContainerEditDateRange.Name = "popupContainerEditDateRange";
            this.popupContainerEditDateRange.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditDateRange.Properties.PopupControl = this.popupContainerControlDateRange;
            this.popupContainerEditDateRange.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditDateRange.Size = new System.Drawing.Size(211, 20);
            this.popupContainerEditDateRange.StyleController = this.layoutControl2;
            this.popupContainerEditDateRange.TabIndex = 2;
            this.popupContainerEditDateRange.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditDateRange_QueryResultValue);
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.btnClearAllFilters);
            this.layoutControl2.Controls.Add(this.checkEditStaff);
            this.layoutControl2.Controls.Add(this.checkEditTeam);
            this.layoutControl2.Controls.Add(this.popupContainerEditVisitTypeFilter);
            this.layoutControl2.Controls.Add(this.popupContainerEditConstructionManagerFilter);
            this.layoutControl2.Controls.Add(this.popupContainerEditShowTabPages);
            this.layoutControl2.Controls.Add(this.popupContainerEditMaterialTypeFilter);
            this.layoutControl2.Controls.Add(this.popupContainerEditEquipmentTypeFilter);
            this.layoutControl2.Controls.Add(this.popupContainerEditLabourFilter);
            this.layoutControl2.Controls.Add(this.popupContainerEditJobSubTypeFilter);
            this.layoutControl2.Controls.Add(this.popupContainerEditJobTypeFilter);
            this.layoutControl2.Controls.Add(this.popupContainerEditRecordTypeFilter);
            this.layoutControl2.Controls.Add(this.buttonEditSiteFilter);
            this.layoutControl2.Controls.Add(this.buttonEditClientFilter);
            this.layoutControl2.Controls.Add(this.popupContainerEditCMFilter);
            this.layoutControl2.Controls.Add(this.popupContainerEditJobStatusFilter);
            this.layoutControl2.Controls.Add(this.popupContainerEditDateRange);
            this.layoutControl2.Controls.Add(this.checkEditColourCode);
            this.layoutControl2.Controls.Add(this.btnLoad);
            this.layoutControl2.Controls.Add(this.popupContainerEditKAMFilter);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1360, 294, 488, 516);
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(338, 515);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // btnClearAllFilters
            // 
            this.btnClearAllFilters.ImageOptions.ImageIndex = 11;
            this.btnClearAllFilters.ImageOptions.ImageList = this.imageCollection1;
            this.btnClearAllFilters.Location = new System.Drawing.Point(158, 558);
            this.btnClearAllFilters.Name = "btnClearAllFilters";
            this.btnClearAllFilters.Size = new System.Drawing.Size(90, 22);
            this.btnClearAllFilters.StyleController = this.layoutControl2;
            toolTipTitleItem12.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem12.Appearance.Options.UseImage = true;
            toolTipTitleItem12.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem12.Text = "Clear Filters - Information";
            toolTipItem12.LeftIndent = 6;
            toolTipItem12.Text = "Click me to clear all filters (except Date Filter).";
            superToolTip12.Items.Add(toolTipTitleItem12);
            superToolTip12.Items.Add(toolTipItem12);
            this.btnClearAllFilters.SuperTip = superToolTip12;
            this.btnClearAllFilters.TabIndex = 21;
            this.btnClearAllFilters.Text = "Clear Filters";
            this.btnClearAllFilters.Click += new System.EventHandler(this.btnClearAllFilters_Click);
            // 
            // checkEditStaff
            // 
            this.checkEditStaff.Location = new System.Drawing.Point(187, 453);
            this.checkEditStaff.MenuManager = this.barManager1;
            this.checkEditStaff.Name = "checkEditStaff";
            this.checkEditStaff.Properties.Caption = "Staff:";
            this.checkEditStaff.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditStaff.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.checkEditStaff.Properties.RadioGroupIndex = 1;
            this.checkEditStaff.Size = new System.Drawing.Size(49, 19);
            this.checkEditStaff.StyleController = this.layoutControl2;
            this.checkEditStaff.TabIndex = 18;
            this.checkEditStaff.TabStop = false;
            this.checkEditStaff.CheckedChanged += new System.EventHandler(this.checkEditStaff_CheckedChanged);
            // 
            // checkEditTeam
            // 
            this.checkEditTeam.EditValue = true;
            this.checkEditTeam.Location = new System.Drawing.Point(122, 453);
            this.checkEditTeam.MenuManager = this.barManager1;
            this.checkEditTeam.Name = "checkEditTeam";
            this.checkEditTeam.Properties.Caption = "Team:";
            this.checkEditTeam.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditTeam.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.checkEditTeam.Properties.RadioGroupIndex = 1;
            this.checkEditTeam.Size = new System.Drawing.Size(51, 19);
            this.checkEditTeam.StyleController = this.layoutControl2;
            this.checkEditTeam.TabIndex = 17;
            this.checkEditTeam.CheckedChanged += new System.EventHandler(this.checkEditTeam_CheckedChanged);
            // 
            // popupContainerEditVisitTypeFilter
            // 
            this.popupContainerEditVisitTypeFilter.EditValue = "No Visit Type Filter";
            this.popupContainerEditVisitTypeFilter.Location = new System.Drawing.Point(108, 41);
            this.popupContainerEditVisitTypeFilter.MenuManager = this.barManager1;
            this.popupContainerEditVisitTypeFilter.Name = "popupContainerEditVisitTypeFilter";
            this.popupContainerEditVisitTypeFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditVisitTypeFilter.Properties.PopupControl = this.popupContainerControlVisitTypeFilter;
            this.popupContainerEditVisitTypeFilter.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditVisitTypeFilter.Size = new System.Drawing.Size(211, 20);
            this.popupContainerEditVisitTypeFilter.StyleController = this.layoutControl2;
            this.popupContainerEditVisitTypeFilter.TabIndex = 6;
            this.popupContainerEditVisitTypeFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditVisitTypeFilter_QueryResultValue);
            // 
            // popupContainerEditConstructionManagerFilter
            // 
            this.popupContainerEditConstructionManagerFilter.EditValue = "No Construction Manager Filter";
            this.popupContainerEditConstructionManagerFilter.Location = new System.Drawing.Point(108, 161);
            this.popupContainerEditConstructionManagerFilter.MenuManager = this.barManager1;
            this.popupContainerEditConstructionManagerFilter.Name = "popupContainerEditConstructionManagerFilter";
            this.popupContainerEditConstructionManagerFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditConstructionManagerFilter.Properties.PopupControl = this.popupContainerControlConstructionManagerFilter;
            this.popupContainerEditConstructionManagerFilter.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditConstructionManagerFilter.Size = new System.Drawing.Size(211, 20);
            this.popupContainerEditConstructionManagerFilter.StyleController = this.layoutControl2;
            this.popupContainerEditConstructionManagerFilter.TabIndex = 16;
            this.popupContainerEditConstructionManagerFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditConstructionManagerFilter_QueryResultValue);
            // 
            // popupContainerEditShowTabPages
            // 
            this.popupContainerEditShowTabPages.EditValue = "All Linked Data";
            this.popupContainerEditShowTabPages.Location = new System.Drawing.Point(96, 534);
            this.popupContainerEditShowTabPages.MenuManager = this.barManager1;
            this.popupContainerEditShowTabPages.Name = "popupContainerEditShowTabPages";
            this.popupContainerEditShowTabPages.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditShowTabPages.Properties.PopupControl = this.popupContainerControlShowTabPages;
            this.popupContainerEditShowTabPages.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditShowTabPages.Size = new System.Drawing.Size(235, 20);
            this.popupContainerEditShowTabPages.StyleController = this.layoutControl2;
            superToolTip13.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem13.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem13.Appearance.Options.UseImage = true;
            toolTipTitleItem13.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem13.Text = "Linked Data - Information";
            toolTipItem13.LeftIndent = 6;
            toolTipItem13.Text = resources.GetString("toolTipItem13.Text");
            superToolTip13.Items.Add(toolTipTitleItem13);
            superToolTip13.Items.Add(toolTipItem13);
            this.popupContainerEditShowTabPages.SuperTip = superToolTip13;
            this.popupContainerEditShowTabPages.TabIndex = 15;
            this.popupContainerEditShowTabPages.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditShowTabPages_QueryResultValue);
            // 
            // popupContainerEditMaterialTypeFilter
            // 
            this.popupContainerEditMaterialTypeFilter.EditValue = "No Material Filter";
            this.popupContainerEditMaterialTypeFilter.Location = new System.Drawing.Point(108, 385);
            this.popupContainerEditMaterialTypeFilter.MenuManager = this.barManager1;
            this.popupContainerEditMaterialTypeFilter.Name = "popupContainerEditMaterialTypeFilter";
            this.popupContainerEditMaterialTypeFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditMaterialTypeFilter.Properties.PopupControl = this.popupContainerControlMaterialTypeFilter;
            this.popupContainerEditMaterialTypeFilter.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditMaterialTypeFilter.Size = new System.Drawing.Size(211, 20);
            this.popupContainerEditMaterialTypeFilter.StyleController = this.layoutControl2;
            this.popupContainerEditMaterialTypeFilter.TabIndex = 14;
            this.popupContainerEditMaterialTypeFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditMaterialTypeFilter_QueryResultValue);
            // 
            // popupContainerEditEquipmentTypeFilter
            // 
            this.popupContainerEditEquipmentTypeFilter.EditValue = "No Equipment Type Filter";
            this.popupContainerEditEquipmentTypeFilter.Location = new System.Drawing.Point(108, 361);
            this.popupContainerEditEquipmentTypeFilter.MenuManager = this.barManager1;
            this.popupContainerEditEquipmentTypeFilter.Name = "popupContainerEditEquipmentTypeFilter";
            this.popupContainerEditEquipmentTypeFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditEquipmentTypeFilter.Properties.PopupControl = this.popupContainerControlEquipmentTypeFilter;
            this.popupContainerEditEquipmentTypeFilter.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditEquipmentTypeFilter.Size = new System.Drawing.Size(211, 20);
            this.popupContainerEditEquipmentTypeFilter.StyleController = this.layoutControl2;
            this.popupContainerEditEquipmentTypeFilter.TabIndex = 13;
            this.popupContainerEditEquipmentTypeFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditEquipmentTypeFilter_QueryResultValue);
            // 
            // popupContainerEditLabourFilter
            // 
            this.popupContainerEditLabourFilter.EditValue = "No Labour Filter";
            this.popupContainerEditLabourFilter.Location = new System.Drawing.Point(120, 476);
            this.popupContainerEditLabourFilter.MenuManager = this.barManager1;
            this.popupContainerEditLabourFilter.Name = "popupContainerEditLabourFilter";
            this.popupContainerEditLabourFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditLabourFilter.Properties.PopupControl = this.popupContainerControlLabourFilter;
            this.popupContainerEditLabourFilter.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditLabourFilter.Size = new System.Drawing.Size(187, 20);
            this.popupContainerEditLabourFilter.StyleController = this.layoutControl2;
            this.popupContainerEditLabourFilter.TabIndex = 12;
            this.popupContainerEditLabourFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditLabourFilter_QueryResultValue);
            // 
            // popupContainerEditJobSubTypeFilter
            // 
            this.popupContainerEditJobSubTypeFilter.EditValue = "No Job Sub-Type Filter";
            this.popupContainerEditJobSubTypeFilter.Location = new System.Drawing.Point(108, 313);
            this.popupContainerEditJobSubTypeFilter.MenuManager = this.barManager1;
            this.popupContainerEditJobSubTypeFilter.Name = "popupContainerEditJobSubTypeFilter";
            this.popupContainerEditJobSubTypeFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditJobSubTypeFilter.Properties.PopupControl = this.popupContainerControlJobSubTypeFilter;
            this.popupContainerEditJobSubTypeFilter.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditJobSubTypeFilter.Size = new System.Drawing.Size(211, 20);
            this.popupContainerEditJobSubTypeFilter.StyleController = this.layoutControl2;
            this.popupContainerEditJobSubTypeFilter.TabIndex = 11;
            this.popupContainerEditJobSubTypeFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditJobSubTypeFilter_QueryResultValue);
            // 
            // popupContainerEditJobTypeFilter
            // 
            this.popupContainerEditJobTypeFilter.EditValue = "No Job Type Filter";
            this.popupContainerEditJobTypeFilter.Location = new System.Drawing.Point(108, 289);
            this.popupContainerEditJobTypeFilter.MenuManager = this.barManager1;
            this.popupContainerEditJobTypeFilter.Name = "popupContainerEditJobTypeFilter";
            this.popupContainerEditJobTypeFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditJobTypeFilter.Properties.PopupControl = this.popupContainerControlJobTypeFilter;
            this.popupContainerEditJobTypeFilter.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditJobTypeFilter.Size = new System.Drawing.Size(211, 20);
            this.popupContainerEditJobTypeFilter.StyleController = this.layoutControl2;
            this.popupContainerEditJobTypeFilter.TabIndex = 10;
            this.popupContainerEditJobTypeFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditJobTypeFilter_QueryResultValue);
            // 
            // popupContainerEditRecordTypeFilter
            // 
            this.popupContainerEditRecordTypeFilter.EditValue = "No Record Type Filter";
            this.popupContainerEditRecordTypeFilter.Location = new System.Drawing.Point(108, 265);
            this.popupContainerEditRecordTypeFilter.MenuManager = this.barManager1;
            this.popupContainerEditRecordTypeFilter.Name = "popupContainerEditRecordTypeFilter";
            this.popupContainerEditRecordTypeFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditRecordTypeFilter.Properties.PopupControl = this.popupContainerControlRecordTypeFilter;
            this.popupContainerEditRecordTypeFilter.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditRecordTypeFilter.Size = new System.Drawing.Size(211, 20);
            this.popupContainerEditRecordTypeFilter.StyleController = this.layoutControl2;
            this.popupContainerEditRecordTypeFilter.TabIndex = 9;
            this.popupContainerEditRecordTypeFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditRecordTypeFilter_QueryResultValue);
            // 
            // buttonEditSiteFilter
            // 
            this.buttonEditSiteFilter.Location = new System.Drawing.Point(108, 89);
            this.buttonEditSiteFilter.MenuManager = this.barManager1;
            this.buttonEditSiteFilter.Name = "buttonEditSiteFilter";
            this.buttonEditSiteFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click to open Choose Site Filter Screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Clear Filter", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.buttonEditSiteFilter.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.buttonEditSiteFilter.Size = new System.Drawing.Size(211, 20);
            this.buttonEditSiteFilter.StyleController = this.layoutControl2;
            this.buttonEditSiteFilter.TabIndex = 8;
            this.buttonEditSiteFilter.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEditSiteFilter_ButtonClick);
            // 
            // buttonEditClientFilter
            // 
            this.buttonEditClientFilter.Location = new System.Drawing.Point(108, 65);
            this.buttonEditClientFilter.MenuManager = this.barManager1;
            this.buttonEditClientFilter.Name = "buttonEditClientFilter";
            this.buttonEditClientFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Click to open Choose Client Filter Screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Clear Filter", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.buttonEditClientFilter.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.buttonEditClientFilter.Size = new System.Drawing.Size(211, 20);
            this.buttonEditClientFilter.StyleController = this.layoutControl2;
            this.buttonEditClientFilter.TabIndex = 6;
            this.buttonEditClientFilter.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEditClientFilter_ButtonClick);
            // 
            // popupContainerEditCMFilter
            // 
            this.popupContainerEditCMFilter.EditValue = "No Contract Manager Filter";
            this.popupContainerEditCMFilter.Location = new System.Drawing.Point(108, 137);
            this.popupContainerEditCMFilter.MenuManager = this.barManager1;
            this.popupContainerEditCMFilter.Name = "popupContainerEditCMFilter";
            this.popupContainerEditCMFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditCMFilter.Properties.PopupControl = this.popupContainerControlCMFilter;
            this.popupContainerEditCMFilter.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditCMFilter.Size = new System.Drawing.Size(211, 20);
            this.popupContainerEditCMFilter.StyleController = this.layoutControl2;
            this.popupContainerEditCMFilter.TabIndex = 5;
            this.popupContainerEditCMFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditCMFilter_QueryResultValue);
            // 
            // popupContainerEditJobStatusFilter
            // 
            this.popupContainerEditJobStatusFilter.EditValue = "No Job Status Filter";
            this.popupContainerEditJobStatusFilter.Location = new System.Drawing.Point(108, 337);
            this.popupContainerEditJobStatusFilter.MenuManager = this.barManager1;
            this.popupContainerEditJobStatusFilter.Name = "popupContainerEditJobStatusFilter";
            this.popupContainerEditJobStatusFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditJobStatusFilter.Properties.PopupControl = this.popupContainerControlJobStatusFilter;
            this.popupContainerEditJobStatusFilter.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditJobStatusFilter.Size = new System.Drawing.Size(211, 20);
            this.popupContainerEditJobStatusFilter.StyleController = this.layoutControl2;
            this.popupContainerEditJobStatusFilter.TabIndex = 0;
            this.popupContainerEditJobStatusFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditJobStatusFilter_QueryResultValue);
            // 
            // checkEditColourCode
            // 
            this.checkEditColourCode.Enabled = false;
            this.checkEditColourCode.Location = new System.Drawing.Point(96, 558);
            this.checkEditColourCode.MenuManager = this.barManager1;
            this.checkEditColourCode.Name = "checkEditColourCode";
            this.checkEditColourCode.Properties.Caption = "";
            this.checkEditColourCode.Size = new System.Drawing.Size(58, 19);
            this.checkEditColourCode.StyleController = this.layoutControl2;
            this.checkEditColourCode.TabIndex = 3;
            this.checkEditColourCode.CheckedChanged += new System.EventHandler(this.checkEditColourCode_CheckedChanged);
            // 
            // btnLoad
            // 
            this.btnLoad.ImageOptions.ImageIndex = 12;
            this.btnLoad.ImageOptions.ImageList = this.imageCollection1;
            this.btnLoad.Location = new System.Drawing.Point(252, 558);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(79, 22);
            this.btnLoad.StyleController = this.layoutControl2;
            toolTipTitleItem14.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem14.Appearance.Options.UseImage = true;
            toolTipTitleItem14.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem14.Text = "Load Data - Information";
            toolTipItem14.LeftIndent = 6;
            toolTipItem14.Text = "Click me to Load Callout Data.\r\n\r\nOnly data matching the specified Filter Criteri" +
    "a (From and To Dates and Callout Status) will be loaded.";
            superToolTip14.Items.Add(toolTipTitleItem14);
            superToolTip14.Items.Add(toolTipItem14);
            this.btnLoad.SuperTip = superToolTip14;
            this.btnLoad.TabIndex = 4;
            this.btnLoad.Text = "Load Data";
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // popupContainerEditKAMFilter
            // 
            this.popupContainerEditKAMFilter.EditValue = "No KAM Filter";
            this.popupContainerEditKAMFilter.Location = new System.Drawing.Point(108, 113);
            this.popupContainerEditKAMFilter.MenuManager = this.barManager1;
            this.popupContainerEditKAMFilter.Name = "popupContainerEditKAMFilter";
            this.popupContainerEditKAMFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditKAMFilter.Properties.PopupControl = this.popupContainerControlKAMFilter;
            this.popupContainerEditKAMFilter.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditKAMFilter.Size = new System.Drawing.Size(211, 20);
            this.popupContainerEditKAMFilter.StyleController = this.layoutControl2;
            this.popupContainerEditKAMFilter.TabIndex = 1;
            this.popupContainerEditKAMFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditKAMFilter_QueryResultValue);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "Root";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem15,
            this.layoutControlGroup1,
            this.layoutControlGroup3,
            this.layoutControlItem5,
            this.emptySpaceItem1,
            this.layoutControlItem3,
            this.emptySpaceItem2,
            this.emptySpaceItem3,
            this.ItemForClearAllFilters});
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup2.Size = new System.Drawing.Size(338, 597);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.popupContainerEditShowTabPages;
            this.layoutControlItem15.CustomizationFormText = "Linked Data:";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 527);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(328, 24);
            this.layoutControlItem15.Text = "Linked Data:";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(86, 13);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.ExpandButtonVisible = true;
            this.layoutControlGroup1.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem17,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem4,
            this.layoutControlItem6,
            this.layoutControlItem16});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(328, 190);
            this.layoutControlGroup1.Text = "Visit Filters";
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.popupContainerEditVisitTypeFilter;
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(304, 24);
            this.layoutControlItem17.Text = "Visit Type:";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(86, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.buttonEditClientFilter;
            this.layoutControlItem7.CustomizationFormText = "Client:";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(304, 24);
            this.layoutControlItem7.Text = "Client:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(86, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.buttonEditSiteFilter;
            this.layoutControlItem8.CustomizationFormText = "Site:";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(304, 24);
            this.layoutControlItem8.Text = "Site:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(86, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.popupContainerEditKAMFilter;
            this.layoutControlItem4.CustomizationFormText = "KAM:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(304, 24);
            this.layoutControlItem4.Text = "KAM:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(86, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.popupContainerEditCMFilter;
            this.layoutControlItem6.CustomizationFormText = "CM:";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(304, 24);
            this.layoutControlItem6.Text = "CM:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(86, 13);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.popupContainerEditConstructionManagerFilter;
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(304, 24);
            this.layoutControlItem16.Text = "Construction Mgr:";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(86, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem9,
            this.layoutControlItem2,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem1,
            this.layoutControlGroup4,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.emptySpaceItem6});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 200);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(328, 317);
            this.layoutControlGroup3.Text = "Job Filters";
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.popupContainerEditRecordTypeFilter;
            this.layoutControlItem9.CustomizationFormText = "Record Type:";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(304, 24);
            this.layoutControlItem9.Text = "Record Type:";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(86, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.popupContainerEditJobStatusFilter;
            this.layoutControlItem2.CustomizationFormText = "Job Status:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(304, 24);
            this.layoutControlItem2.Text = "Job Status:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(86, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.popupContainerEditJobTypeFilter;
            this.layoutControlItem10.CustomizationFormText = "Job Type:";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(304, 24);
            this.layoutControlItem10.Text = "Job Type:";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(86, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.popupContainerEditJobSubTypeFilter;
            this.layoutControlItem11.CustomizationFormText = "Job Sub-Type:";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(304, 24);
            this.layoutControlItem11.Text = "Job Sub-Type:";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(86, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.popupContainerEditDateRange;
            this.layoutControlItem1.CustomizationFormText = "Date Range:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(304, 24);
            this.layoutControlItem1.Text = "Date Range:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(86, 13);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForLabourFilter,
            this.layoutControlItem18,
            this.emptySpaceItem4,
            this.layoutControlItem19,
            this.emptySpaceItem5,
            this.emptySpaceItem7});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 178);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(304, 93);
            this.layoutControlGroup4.Text = "Labour";
            // 
            // ItemForLabourFilter
            // 
            this.ItemForLabourFilter.Control = this.popupContainerEditLabourFilter;
            this.ItemForLabourFilter.CustomizationFormText = "Labour:";
            this.ItemForLabourFilter.Location = new System.Drawing.Point(0, 23);
            this.ItemForLabourFilter.Name = "ItemForLabourFilter";
            this.ItemForLabourFilter.Size = new System.Drawing.Size(280, 24);
            this.ItemForLabourFilter.Text = "Team:";
            this.ItemForLabourFilter.TextSize = new System.Drawing.Size(86, 13);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.checkEditTeam;
            this.layoutControlItem18.Location = new System.Drawing.Point(91, 0);
            this.layoutControlItem18.MaxSize = new System.Drawing.Size(55, 23);
            this.layoutControlItem18.MinSize = new System.Drawing.Size(55, 23);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(55, 23);
            this.layoutControlItem18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem18.Text = "Team Filter:";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(209, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(71, 23);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.checkEditStaff;
            this.layoutControlItem19.Location = new System.Drawing.Point(156, 0);
            this.layoutControlItem19.MaxSize = new System.Drawing.Size(53, 23);
            this.layoutControlItem19.MinSize = new System.Drawing.Size(53, 23);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(53, 23);
            this.layoutControlItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem19.Text = "Staff Filter:";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextVisible = false;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(91, 0);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(91, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(91, 23);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.Location = new System.Drawing.Point(146, 0);
            this.emptySpaceItem7.MaxSize = new System.Drawing.Size(10, 0);
            this.emptySpaceItem7.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(10, 23);
            this.emptySpaceItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.popupContainerEditEquipmentTypeFilter;
            this.layoutControlItem13.CustomizationFormText = "Equipment:";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(304, 24);
            this.layoutControlItem13.Text = "Equipment Type:";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(86, 13);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.popupContainerEditMaterialTypeFilter;
            this.layoutControlItem14.CustomizationFormText = "Materials:";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 144);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(304, 24);
            this.layoutControlItem14.Text = "Material:";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(86, 13);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 168);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(304, 10);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.checkEditColourCode;
            this.layoutControlItem5.CustomizationFormText = "Colour Code:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 551);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(151, 26);
            this.layoutControlItem5.Text = "Colour Code:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(86, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 577);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(245, 10);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnLoad;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(245, 551);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(83, 26);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(83, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(83, 36);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 190);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(328, 10);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 517);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(328, 10);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForClearAllFilters
            // 
            this.ItemForClearAllFilters.Control = this.btnClearAllFilters;
            this.ItemForClearAllFilters.Location = new System.Drawing.Point(151, 551);
            this.ItemForClearAllFilters.MaxSize = new System.Drawing.Size(94, 26);
            this.ItemForClearAllFilters.MinSize = new System.Drawing.Size(94, 26);
            this.ItemForClearAllFilters.Name = "ItemForClearAllFilters";
            this.ItemForClearAllFilters.Size = new System.Drawing.Size(94, 26);
            this.ItemForClearAllFilters.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForClearAllFilters.Text = "Clear All Filters";
            this.ItemForClearAllFilters.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForClearAllFilters.TextVisible = false;
            // 
            // bar1
            // 
            this.bar1.BarName = "GrittingCalloutToolbar";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.FloatLocation = new System.Drawing.Point(519, 261);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiSendJobs, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiReassignJobs, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiManuallyComplete, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAuthorise, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiPay, true),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiRefresh, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciFilterSelected, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiSelectedCount, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddWizard, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiViewOnMap, true)});
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.DisableCustomization = true;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar1.Text = "Gritting Callout Toolbar";
            // 
            // bsiSendJobs
            // 
            this.bsiSendJobs.Caption = "Send Jobs";
            this.bsiSendJobs.Id = 33;
            this.bsiSendJobs.ImageOptions.ImageIndex = 3;
            this.bsiSendJobs.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSelectJobsReadyToSend),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSendJobs, true)});
            this.bsiSendJobs.Name = "bsiSendJobs";
            this.bsiSendJobs.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bbiSelectJobsReadyToSend
            // 
            this.bbiSelectJobsReadyToSend.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiSelectJobsReadyToSend.Caption = "<b>Select</b> \'Ready to Send\' Jobs";
            this.bbiSelectJobsReadyToSend.Id = 34;
            this.bbiSelectJobsReadyToSend.ImageOptions.ImageIndex = 5;
            this.bbiSelectJobsReadyToSend.Name = "bbiSelectJobsReadyToSend";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Select \'Ready To Send\' Jobs - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to select all jobs which are ready to send.\r\n\r\n<b><u><color=red>Selectio" +
    "n Criteria</color></u></b>\r\nJob Status < Sent\r\nDue Date <= 7 days\r\nAt least one " +
    "Labour Team specified";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiSelectJobsReadyToSend.SuperTip = superToolTip1;
            this.bbiSelectJobsReadyToSend.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSelectJobsReadyToSend_ItemClick);
            // 
            // bbiSendJobs
            // 
            this.bbiSendJobs.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiSendJobs.Caption = "<b>Send</b> Jobs";
            this.bbiSendJobs.Id = 35;
            this.bbiSendJobs.ImageOptions.ImageIndex = 3;
            this.bbiSendJobs.Name = "bbiSendJobs";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Send Jobs - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = resources.GetString("toolTipItem2.Text");
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiSendJobs.SuperTip = superToolTip2;
            this.bbiSendJobs.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSendJobs_ItemClick);
            // 
            // bbiReassignJobs
            // 
            this.bbiReassignJobs.Caption = "Reassign";
            this.bbiReassignJobs.Id = 29;
            this.bbiReassignJobs.ImageOptions.ImageIndex = 2;
            this.bbiReassignJobs.Name = "bbiReassignJobs";
            this.bbiReassignJobs.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Reassign Jobs - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to reassign the selected Job(s) to different Teams.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiReassignJobs.SuperTip = superToolTip3;
            this.bbiReassignJobs.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiReassignJobs_ItemClick);
            // 
            // bsiManuallyComplete
            // 
            this.bsiManuallyComplete.Caption = "Complete";
            this.bsiManuallyComplete.Id = 44;
            this.bsiManuallyComplete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiManuallyComplete.ImageOptions.Image")));
            this.bsiManuallyComplete.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSelectJobsToComplete),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiManuallyCompleteJobs, true)});
            this.bsiManuallyComplete.Name = "bsiManuallyComplete";
            this.bsiManuallyComplete.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bbiSelectJobsToComplete
            // 
            this.bbiSelectJobsToComplete.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiSelectJobsToComplete.Caption = "<B>Select</b> Incomplete Jobs";
            this.bbiSelectJobsToComplete.Id = 46;
            this.bbiSelectJobsToComplete.ImageOptions.ImageIndex = 5;
            this.bbiSelectJobsToComplete.Name = "bbiSelectJobsToComplete";
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Text = "Select Incomplete Jobs - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to select all jobs which are not yet complete.\r\n\r\n<b><u><color=red>Selec" +
    "tion Criteria</color></u></b>\r\nJob Status < Complete\r\nAt least one Labour Team s" +
    "pecified";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.bbiSelectJobsToComplete.SuperTip = superToolTip4;
            this.bbiSelectJobsToComplete.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSelectJobsToComplete_ItemClick);
            // 
            // bbiManuallyCompleteJobs
            // 
            this.bbiManuallyCompleteJobs.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiManuallyCompleteJobs.Caption = "Manually <b>Complete</b> Jobs";
            this.bbiManuallyCompleteJobs.Id = 47;
            this.bbiManuallyCompleteJobs.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiManuallyCompleteJobs.ImageOptions.Image")));
            this.bbiManuallyCompleteJobs.Name = "bbiManuallyCompleteJobs";
            superToolTip5.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem5.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Text = "Manually Complete Jobs - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to manually complete the selected jobs.\r\n\r\n<b>Note:</b> Only jobs with a" +
    " selected Labour Team and a Status less than completed can be manually completed" +
    ".";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.bbiManuallyCompleteJobs.SuperTip = superToolTip5;
            this.bbiManuallyCompleteJobs.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiManuallyCompleteJobs_ItemClick);
            // 
            // bbiAuthorise
            // 
            this.bbiAuthorise.Caption = "Authorise";
            this.bbiAuthorise.Id = 31;
            this.bbiAuthorise.ImageOptions.ImageIndex = 6;
            this.bbiAuthorise.Name = "bbiAuthorise";
            this.bbiAuthorise.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip6.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem6.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Appearance.Options.UseImage = true;
            toolTipTitleItem6.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Text = "Authorise Payment - Information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Click me to <b>authorise for payment</b> the selected job(s).";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this.bbiAuthorise.SuperTip = superToolTip6;
            this.bbiAuthorise.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAuthorise_ItemClick);
            // 
            // bbiPay
            // 
            this.bbiPay.Caption = "Pay";
            this.bbiPay.Id = 32;
            this.bbiPay.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPay.ImageOptions.Image")));
            this.bbiPay.Name = "bbiPay";
            this.bbiPay.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip7.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem7.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Appearance.Options.UseImage = true;
            toolTipTitleItem7.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Text = "Pay - Information";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Click me to <b>pay</b> the selected job(s).";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            this.bbiPay.SuperTip = superToolTip7;
            this.bbiPay.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiPay_ItemClick);
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Caption = "Refresh";
            this.bbiRefresh.Id = 57;
            this.bbiRefresh.ImageOptions.ImageIndex = 4;
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // bciFilterSelected
            // 
            this.bciFilterSelected.Caption = "Selected";
            this.bciFilterSelected.Id = 55;
            this.bciFilterSelected.ImageOptions.ImageIndex = 0;
            this.bciFilterSelected.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bciFilterSelected.ImageOptions.LargeImage")));
            this.bciFilterSelected.Name = "bciFilterSelected";
            this.bciFilterSelected.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionInMenu;
            superToolTip8.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem8.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem8.Appearance.Options.UseImage = true;
            toolTipTitleItem8.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem8.Text = "Filter Selected Jobs - Information";
            toolTipItem8.LeftIndent = 6;
            toolTipItem8.Text = resources.GetString("toolTipItem8.Text");
            superToolTip8.Items.Add(toolTipTitleItem8);
            superToolTip8.Items.Add(toolTipItem8);
            this.bciFilterSelected.SuperTip = superToolTip8;
            this.bciFilterSelected.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciFilterSelected_CheckedChanged);
            // 
            // bsiSelectedCount
            // 
            this.bsiSelectedCount.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bsiSelectedCount.Caption = "0 Jobs Selected";
            this.bsiSelectedCount.Id = 58;
            this.bsiSelectedCount.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_16x16;
            this.bsiSelectedCount.ItemAppearance.Disabled.Options.UseTextOptions = true;
            this.bsiSelectedCount.ItemAppearance.Disabled.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bsiSelectedCount.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.bsiSelectedCount.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bsiSelectedCount.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.bsiSelectedCount.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bsiSelectedCount.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.bsiSelectedCount.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bsiSelectedCount.Name = "bsiSelectedCount";
            this.bsiSelectedCount.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bsiSelectedCount.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // bbiAddWizard
            // 
            this.bbiAddWizard.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiAddWizard.Caption = "Wizard";
            this.bbiAddWizard.Id = 42;
            this.bbiAddWizard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiAddWizard.ImageOptions.Image")));
            this.bbiAddWizard.Name = "bbiAddWizard";
            this.bbiAddWizard.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem9.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem9.Appearance.Options.UseImage = true;
            toolTipTitleItem9.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem9.Text = "Add Wizard - Information";
            toolTipItem9.LeftIndent = 6;
            toolTipItem9.Text = "Click me to open the Create Job Wizard screen.\r\n\r\nThe Wizard will guide you step-" +
    "by-step through the job creation process.";
            superToolTip9.Items.Add(toolTipTitleItem9);
            superToolTip9.Items.Add(toolTipItem9);
            this.bbiAddWizard.SuperTip = superToolTip9;
            this.bbiAddWizard.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddWizard_ItemClick);
            // 
            // bsiViewOnMap
            // 
            this.bsiViewOnMap.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bsiViewOnMap.Caption = "Mapping";
            this.bsiViewOnMap.Id = 36;
            this.bsiViewOnMap.ImageOptions.ImageIndex = 1;
            this.bsiViewOnMap.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiViewSiteOnMap),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiViewJobOnMap, true)});
            this.bsiViewOnMap.Name = "bsiViewOnMap";
            this.bsiViewOnMap.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bbiViewSiteOnMap
            // 
            this.bbiViewSiteOnMap.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiViewSiteOnMap.Caption = "View <b>Site</b> On Map";
            this.bbiViewSiteOnMap.Id = 30;
            this.bbiViewSiteOnMap.ImageOptions.ImageIndex = 1;
            this.bbiViewSiteOnMap.Name = "bbiViewSiteOnMap";
            superToolTip10.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem10.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem10.Appearance.Options.UseImage = true;
            toolTipTitleItem10.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem10.Text = "View Site On Map - Information";
            toolTipItem10.LeftIndent = 6;
            toolTipItem10.Text = "Click me to View the Site on the Map.\r\n\r\n<b>Note:</b> An internet connection is r" +
    "equired.";
            superToolTip10.Items.Add(toolTipTitleItem10);
            superToolTip10.Items.Add(toolTipItem10);
            this.bbiViewSiteOnMap.SuperTip = superToolTip10;
            this.bbiViewSiteOnMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiViewSiteOnMap_ItemClick);
            // 
            // bbiViewJobOnMap
            // 
            this.bbiViewJobOnMap.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiViewJobOnMap.Caption = "View <b>Job</b> Start and Finish Position On Map";
            this.bbiViewJobOnMap.Id = 37;
            this.bbiViewJobOnMap.ImageOptions.ImageIndex = 1;
            this.bbiViewJobOnMap.Name = "bbiViewJobOnMap";
            superToolTip11.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem11.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem11.Appearance.Options.UseImage = true;
            toolTipTitleItem11.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem11.Text = "View Job Start and Finish Position on Map - Information";
            toolTipItem11.LeftIndent = 6;
            toolTipItem11.Text = "Click me to View the Job Start and End position on the Map.\r\n\r\n<b>Note:</b> An in" +
    "ternet connection is required.";
            superToolTip11.Items.Add(toolTipTitleItem11);
            superToolTip11.Items.Add(toolTipItem11);
            this.bbiViewJobOnMap.SuperTip = superToolTip11;
            this.bbiViewJobOnMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiViewJobOnMap_ItemClick);
            // 
            // bbiOptimization
            // 
            this.bbiOptimization.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.bbiOptimization.Caption = "Optimization";
            this.bbiOptimization.DropDownControl = this.pmOptimisation;
            this.bbiOptimization.Id = 49;
            this.bbiOptimization.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiOptimization.ImageOptions.Image")));
            this.bbiOptimization.Name = "bbiOptimization";
            this.bbiOptimization.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip19.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem19.Appearance.Options.UseImage = true;
            toolTipTitleItem19.Text = "Optimization - Information";
            toolTipItem19.LeftIndent = 6;
            toolTipItem19.Text = "Click me to send the <b>selected</b> data to the Route Optimizer.";
            superToolTip19.Items.Add(toolTipTitleItem19);
            superToolTip19.Items.Add(toolTipItem19);
            this.bbiOptimization.SuperTip = superToolTip19;
            this.bbiOptimization.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiOptimization_ItemClick);
            // 
            // pmOptimisation
            // 
            this.pmOptimisation.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiGetDataToRouteOptimize),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSendDataToRouteOptimizer),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiGetDataFromRouteOptimizer, true)});
            this.pmOptimisation.Manager = this.barManager1;
            this.pmOptimisation.Name = "pmOptimisation";
            // 
            // bbiGetDataToRouteOptimize
            // 
            this.bbiGetDataToRouteOptimize.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiGetDataToRouteOptimize.Caption = "<b>Select</b> Data to Route Optimize";
            this.bbiGetDataToRouteOptimize.Id = 50;
            this.bbiGetDataToRouteOptimize.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGetDataToRouteOptimize.ImageOptions.Image")));
            this.bbiGetDataToRouteOptimize.Name = "bbiGetDataToRouteOptimize";
            toolTipTitleItem16.Appearance.Options.UseImage = true;
            toolTipTitleItem16.Text = "Select Data To Route Optimize - Information";
            toolTipItem16.LeftIndent = 6;
            toolTipItem16.Text = "Click me to select data with an appropriate status for sending to the Route Optim" +
    "izer.";
            superToolTip16.Items.Add(toolTipTitleItem16);
            superToolTip16.Items.Add(toolTipItem16);
            this.bbiGetDataToRouteOptimize.SuperTip = superToolTip16;
            this.bbiGetDataToRouteOptimize.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiGetDataToRouteOptimize_ItemClick);
            // 
            // bbiSendDataToRouteOptimizer
            // 
            this.bbiSendDataToRouteOptimizer.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiSendDataToRouteOptimizer.Caption = "<b>Send</b> Data to Route Optimizer...";
            this.bbiSendDataToRouteOptimizer.Id = 51;
            this.bbiSendDataToRouteOptimizer.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSendDataToRouteOptimizer.ImageOptions.Image")));
            this.bbiSendDataToRouteOptimizer.Name = "bbiSendDataToRouteOptimizer";
            toolTipTitleItem17.Appearance.Options.UseImage = true;
            toolTipTitleItem17.Text = "Send Data to Route Optimizer - Information";
            toolTipItem17.LeftIndent = 6;
            toolTipItem17.Text = "Click me to send the selected callout records to the Route Optimizer. \r\nRoutes ar" +
    "e created for the selected callouts and optionally, the best located teams can b" +
    "e assigned to the callouts.";
            superToolTip17.Items.Add(toolTipTitleItem17);
            superToolTip17.Items.Add(toolTipItem17);
            this.bbiSendDataToRouteOptimizer.SuperTip = superToolTip17;
            this.bbiSendDataToRouteOptimizer.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSendDataToRouteOptimizer_ItemClick);
            // 
            // bbiGetDataFromRouteOptimizer
            // 
            this.bbiGetDataFromRouteOptimizer.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiGetDataFromRouteOptimizer.Caption = "<b>Get</b> Data from Route Optimizer...";
            this.bbiGetDataFromRouteOptimizer.Id = 52;
            this.bbiGetDataFromRouteOptimizer.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGetDataFromRouteOptimizer.ImageOptions.Image")));
            this.bbiGetDataFromRouteOptimizer.Name = "bbiGetDataFromRouteOptimizer";
            toolTipTitleItem18.Appearance.Options.UseImage = true;
            toolTipTitleItem18.Text = "Get Data from Route Optimizer - Information";
            toolTipItem18.LeftIndent = 6;
            toolTipItem18.Text = "Click me to collect the results from the Route Optimizer and update the callout r" +
    "ecords with their route.\r\n";
            superToolTip18.Items.Add(toolTipTitleItem18);
            superToolTip18.Items.Add(toolTipItem18);
            this.bbiGetDataFromRouteOptimizer.SuperTip = superToolTip18;
            this.bbiGetDataFromRouteOptimizer.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiGetDataFromRouteOptimizer_ItemClick);
            // 
            // bbiManuallyComplete
            // 
            this.bbiManuallyComplete.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left;
            this.bbiManuallyComplete.Caption = "Complete";
            this.bbiManuallyComplete.Id = 43;
            this.bbiManuallyComplete.Name = "bbiManuallyComplete";
            this.bbiManuallyComplete.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip15.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem15.Text = "Manually Complete Job - Information";
            toolTipItem15.LeftIndent = 6;
            superToolTip15.Items.Add(toolTipTitleItem15);
            superToolTip15.Items.Add(toolTipItem15);
            this.bbiManuallyComplete.SuperTip = superToolTip15;
            // 
            // bbiSendJobs2
            // 
            this.bbiSendJobs2.Caption = "Send Jobs";
            this.bbiSendJobs2.Id = 27;
            this.bbiSendJobs2.Name = "bbiSendJobs2";
            this.bbiSendJobs2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // sp04092_GC_Gritting_Callout_Pictures_For_CalloutTableAdapter
            // 
            this.sp04092_GC_Gritting_Callout_Pictures_For_CalloutTableAdapter.ClearBeforeFill = true;
            // 
            // sp05089_CRM_Contacts_Linked_To_RecordTableAdapter
            // 
            this.sp05089_CRM_Contacts_Linked_To_RecordTableAdapter.ClearBeforeFill = true;
            // 
            // dockManager1
            // 
            this.dockManager1.AutoHideContainers.AddRange(new DevExpress.XtraBars.Docking.AutoHideContainer[] {
            this.hideContainerLeft});
            this.dockManager1.Form = this;
            this.dockManager1.MenuManager = this.barManager1;
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // hideContainerLeft
            // 
            this.hideContainerLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.hideContainerLeft.Controls.Add(this.dockPanelFilters);
            this.hideContainerLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.hideContainerLeft.Location = new System.Drawing.Point(0, 0);
            this.hideContainerLeft.Name = "hideContainerLeft";
            this.hideContainerLeft.Size = new System.Drawing.Size(23, 547);
            // 
            // dockPanelFilters
            // 
            this.dockPanelFilters.Controls.Add(this.dockPanel1_Container);
            this.dockPanelFilters.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelFilters.FloatSize = new System.Drawing.Size(857, 163);
            this.dockPanelFilters.FloatVertical = true;
            this.dockPanelFilters.ID = new System.Guid("0dba7f23-2bf1-4991-a30a-3346dfc46be4");
            this.dockPanelFilters.Location = new System.Drawing.Point(0, 0);
            this.dockPanelFilters.Name = "dockPanelFilters";
            this.dockPanelFilters.Options.ShowCloseButton = false;
            this.dockPanelFilters.Options.ShowMaximizeButton = false;
            this.dockPanelFilters.OriginalSize = new System.Drawing.Size(344, 164);
            this.dockPanelFilters.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelFilters.SavedIndex = 0;
            this.dockPanelFilters.Size = new System.Drawing.Size(344, 547);
            this.dockPanelFilters.Text = "Data Filter";
            this.dockPanelFilters.Visibility = DevExpress.XtraBars.Docking.DockVisibility.AutoHide;
            this.dockPanelFilters.DockChanged += new System.EventHandler(this.dockPanelFilters_DockChanged);
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.layoutControl2);
            this.dockPanel1_Container.Location = new System.Drawing.Point(3, 29);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(338, 515);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // sp04022_Core_Dummy_TabPageListTableAdapter
            // 
            this.sp04022_Core_Dummy_TabPageListTableAdapter.ClearBeforeFill = true;
            // 
            // sp06018_OM_Job_Manager_Record_TypesTableAdapter
            // 
            this.sp06018_OM_Job_Manager_Record_TypesTableAdapter.ClearBeforeFill = true;
            // 
            // sp06019_OM_Job_Manager_KAMs_FilterTableAdapter
            // 
            this.sp06019_OM_Job_Manager_KAMs_FilterTableAdapter.ClearBeforeFill = true;
            // 
            // sp06020_OM_Job_Manager_CMs_FilterTableAdapter
            // 
            this.sp06020_OM_Job_Manager_CMs_FilterTableAdapter.ClearBeforeFill = true;
            // 
            // sp06025_OM_Job_Statuses_FilterTableAdapter
            // 
            this.sp06025_OM_Job_Statuses_FilterTableAdapter.ClearBeforeFill = true;
            // 
            // sp06026_OM_Job_Type_FilterTableAdapter
            // 
            this.sp06026_OM_Job_Type_FilterTableAdapter.ClearBeforeFill = true;
            // 
            // sp06027_OM_Job_Sub_Type_FilterTableAdapter
            // 
            this.sp06027_OM_Job_Sub_Type_FilterTableAdapter.ClearBeforeFill = true;
            // 
            // sp06028_OM_Labour_FilterTableAdapter
            // 
            this.sp06028_OM_Labour_FilterTableAdapter.ClearBeforeFill = true;
            // 
            // sp06029_OM_Equipment_Type_FilterTableAdapter
            // 
            this.sp06029_OM_Equipment_Type_FilterTableAdapter.ClearBeforeFill = true;
            // 
            // sp06030_OM_Material_FilterTableAdapter
            // 
            this.sp06030_OM_Material_FilterTableAdapter.ClearBeforeFill = true;
            // 
            // sp06031_OM_Job_ManagerTableAdapter
            // 
            this.sp06031_OM_Job_ManagerTableAdapter.ClearBeforeFill = true;
            // 
            // sp06033_OM_Job_Manager_Linked_labourTableAdapter
            // 
            this.sp06033_OM_Job_Manager_Linked_labourTableAdapter.ClearBeforeFill = true;
            // 
            // sp06034_OM_Job_Manager_Linked_EquipmentTableAdapter
            // 
            this.sp06034_OM_Job_Manager_Linked_EquipmentTableAdapter.ClearBeforeFill = true;
            // 
            // sp06035_OM_Job_Manager_Linked_MaterialsTableAdapter
            // 
            this.sp06035_OM_Job_Manager_Linked_MaterialsTableAdapter.ClearBeforeFill = true;
            // 
            // sp06036_OM_Job_Manager_Linked_WasteTableAdapter
            // 
            this.sp06036_OM_Job_Manager_Linked_WasteTableAdapter.ClearBeforeFill = true;
            // 
            // sp06037_OM_Job_Manager_Linked_PicturesTableAdapter
            // 
            this.sp06037_OM_Job_Manager_Linked_PicturesTableAdapter.ClearBeforeFill = true;
            // 
            // sp06038_OM_Job_Manager_Linked_SprayingTableAdapter
            // 
            this.sp06038_OM_Job_Manager_Linked_SprayingTableAdapter.ClearBeforeFill = true;
            // 
            // sp06039_OM_Job_Manager_Linked_Safety_RequirementTableAdapter
            // 
            this.sp06039_OM_Job_Manager_Linked_Safety_RequirementTableAdapter.ClearBeforeFill = true;
            // 
            // sp06040_OM_Job_Manager_Linked_CommentsTableAdapter
            // 
            this.sp06040_OM_Job_Manager_Linked_CommentsTableAdapter.ClearBeforeFill = true;
            // 
            // sp06041_OM_Job_Manager_Linked_Extra_InfoTableAdapter
            // 
            this.sp06041_OM_Job_Manager_Linked_Extra_InfoTableAdapter.ClearBeforeFill = true;
            // 
            // sp06279_OM_Labour_Time_On_Off_SiteTableAdapter
            // 
            this.sp06279_OM_Labour_Time_On_Off_SiteTableAdapter.ClearBeforeFill = true;
            // 
            // sp06294_OM_Job_Manager_Construction_Manager_FilterTableAdapter
            // 
            this.sp06294_OM_Job_Manager_Construction_Manager_FilterTableAdapter.ClearBeforeFill = true;
            // 
            // sp06308_OM_Visit_TypesTableAdapter
            // 
            this.sp06308_OM_Visit_TypesTableAdapter.ClearBeforeFill = true;
            // 
            // imageCollection2
            // 
            this.imageCollection2.ImageSize = new System.Drawing.Size(32, 32);
            this.imageCollection2.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection2.ImageStream")));
            this.imageCollection2.InsertGalleryImage("masterfilter_32x32.png", "images/filter/masterfilter_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/filter/masterfilter_32x32.png"), 0);
            this.imageCollection2.Images.SetKeyName(0, "masterfilter_32x32.png");
            this.imageCollection2.InsertGalleryImage("geopointmap_32x32.png", "images/maps/geopointmap_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/maps/geopointmap_32x32.png"), 1);
            this.imageCollection2.Images.SetKeyName(1, "geopointmap_32x32.png");
            this.imageCollection2.InsertImage(global::WoodPlan5.Properties.Resources.team_reassign_32, "team_reassign_32", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection2.Images.SetKeyName(2, "team_reassign_32");
            this.imageCollection2.InsertImage(global::WoodPlan5.Properties.Resources.team_send_32x32, "team_send_32x32", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection2.Images.SetKeyName(3, "team_send_32x32");
            this.imageCollection2.InsertGalleryImage("refresh_32x32.png", "images/actions/refresh_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/refresh_32x32.png"), 4);
            this.imageCollection2.Images.SetKeyName(4, "refresh_32x32.png");
            this.imageCollection2.InsertGalleryImage("contentarrangeinrows_32x32.png", "images/alignment/contentarrangeinrows_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/alignment/contentarrangeinrows_32x32.png"), 5);
            this.imageCollection2.Images.SetKeyName(5, "contentarrangeinrows_32x32.png");
            this.imageCollection2.InsertGalleryImage("apply_32x32.png", "images/actions/apply_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/apply_32x32.png"), 6);
            this.imageCollection2.Images.SetKeyName(6, "apply_32x32.png");
            // 
            // frm_OM_Job_Manager
            // 
            this.ClientSize = new System.Drawing.Size(1080, 547);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.hideContainerLeft);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_OM_Job_Manager";
            this.Text = "Job Manager - Operations";
            this.Activated += new System.EventHandler(this.frm_OM_Job_Manager_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_OM_Job_Manager_FormClosing);
            this.Load += new System.EventHandler(this.frm_OM_Job_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.hideContainerLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlJob)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06031OMJobManagerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Job)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewJob)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditLatLong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocumentsJob_OpenLink)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditIntegerDays)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEditAlert)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Summer_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).EndInit();
            this.popupContainerControlDateRange.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlRecordTypeFilter)).EndInit();
            this.popupContainerControlRecordTypeFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06018OMJobManagerRecordTypesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlJobStatusFilter)).EndInit();
            this.popupContainerControlJobStatusFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06025OMJobStatusesFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlShowTabPages)).EndInit();
            this.popupContainerControlShowTabPages.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04022CoreDummyTabPageListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPageLabour.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlLabour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06033OMJobManagerLinkedlabourBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLabour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditViewRecord)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer5)).EndInit();
            this.gridSplitContainer5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlLabourTiming)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06279OMLabourTimeOnOffSiteBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLabourTiming)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML3)).EndInit();
            this.xtraTabPageEquipment.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlMaterialTypeFilter)).EndInit();
            this.popupContainerControlMaterialTypeFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06030OMMaterialFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlJobTypeFilter)).EndInit();
            this.popupContainerControlJobTypeFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06026OMJobTypeFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlEquipmentTypeFilter)).EndInit();
            this.popupContainerControlEquipmentTypeFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06029OMEquipmentTypeFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlJobSubTypeFilter)).EndInit();
            this.popupContainerControlJobSubTypeFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06027OMJobSubTypeFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlEquipment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06034OMJobManagerLinkedEquipmentBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEquipment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditEquipmentPictures)).EndInit();
            this.xtraTabPageMaterials.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlLabourFilter)).EndInit();
            this.popupContainerControlLabourFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06028OMLabourFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlVisitTypeFilter)).EndInit();
            this.popupContainerControlVisitTypeFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06308OMVisitTypesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Visit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlMaterial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06035OMJobManagerLinkedMaterialsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMaterial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric2DP4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditMaterialPictures)).EndInit();
            this.xtraTabPageExtraInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExtraInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06041OMJobManagerLinkedExtraInfoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExtraInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit14)).EndInit();
            this.xtraTabPageSpraying.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSpraying)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06038OMJobManagerLinkedSprayingBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSpraying)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumericRatio)).EndInit();
            this.xtraTabPageWaste.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWaste)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06036OMJobManagerLinkedWasteBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWaste)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric2DP5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditWasteDocument)).EndInit();
            this.xtraTabPagePictures.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).EndInit();
            this.gridSplitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06037OMJobManagerLinkedPicturesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML9)).EndInit();
            this.xtraTabPageComments.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06040OMJobManagerLinkedCommentsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit10)).EndInit();
            this.xtraTabPageHealthAndSafety.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlHealthAndSafety)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06039OMJobManagerLinkedSafetyRequirementBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewHealthAndSafety)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit11)).EndInit();
            this.xtraTabPageCRM.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer4)).EndInit();
            this.gridSplitContainer4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCRM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp05089CRMContactsLinkedToRecordBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCRM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlConstructionManagerFilter)).EndInit();
            this.popupContainerControlConstructionManagerFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06294OMJobManagerConstructionManagerFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlKAMFilter)).EndInit();
            this.popupContainerControlKAMFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06019OMJobManagerKAMsFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlCMFilter)).EndInit();
            this.popupContainerControlCMFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06020OMJobManagerCMsFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).EndInit();
            this.xtraTabPagePurchaseOrders.ResumeLayout(false);
            this.xtraTabPagePurchaseOrders.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Reports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditDateRange.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditStaff.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditTeam.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditVisitTypeFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditConstructionManagerFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditShowTabPages.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditMaterialTypeFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditEquipmentTypeFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditLabourFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditJobSubTypeFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditJobTypeFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditRecordTypeFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditSiteFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditClientFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditCMFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditJobStatusFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditColourCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditKAMFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLabourFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClearAllFilters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmOptimisation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.hideContainerLeft.ResumeLayout(false);
            this.dockPanelFilters.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraGrid.GridControl gridControlJob;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewJob;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageLabour;
        private DevExpress.XtraGrid.GridControl gridControlLabour;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewLabour;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DataSet_GC_Core dataSet_GC_Core;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraEditors.SimpleButton btnLoad;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditJobStatusFilter;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlJobStatusFilter;
        private DevExpress.XtraEditors.SimpleButton btnJobStatusFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colID5;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage;
        private DevExpress.XtraEditors.CheckEdit checkEditColourCode;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiSendJobs2;
        private DevExpress.XtraBars.BarButtonItem bbiReassignJobs;
        private DevExpress.XtraBars.BarButtonItem bbiViewSiteOnMap;
        private DevExpress.XtraTab.XtraTabPage xtraTabPagePictures;
        private DevExpress.XtraBars.BarButtonItem bbiAuthorise;
        private DevExpress.XtraBars.BarButtonItem bbiPay;
        private DevExpress.XtraBars.BarSubItem bsiSendJobs;
        private DevExpress.XtraBars.BarButtonItem bbiSelectJobsReadyToSend;
        private DevExpress.XtraBars.BarButtonItem bbiSendJobs;
        private DevExpress.XtraBars.BarSubItem bsiViewOnMap;
        private DevExpress.XtraBars.BarButtonItem bbiViewJobOnMap;
        private DevExpress.XtraGrid.GridControl gridControlPicture;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPicture;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime6;
        private DataSet_GC_CoreTableAdapters.sp04092_GC_Gritting_Callout_Pictures_For_CalloutTableAdapter sp04092_GC_Gritting_Callout_Pictures_For_CalloutTableAdapter;
        private DevExpress.Utils.ToolTipController toolTipController1;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        public DevExpress.XtraBars.BarButtonItem bbiAddWizard;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP2;
        private DevExpress.XtraBars.BarButtonItem bbiManuallyComplete;
        private DevExpress.XtraBars.BarSubItem bsiManuallyComplete;
        private DevExpress.XtraBars.BarButtonItem bbiSelectJobsToComplete;
        private DevExpress.XtraBars.BarButtonItem bbiManuallyCompleteJobs;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlKAMFilter;
        private DevExpress.XtraEditors.SimpleButton btnKAMFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditKAMFilter;
        private DataSet_GC_Reports dataSet_GC_Reports;
        private WoodPlan5.DataSet_AT dataSet_AT;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer3;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageCRM;
        private DevExpress.XtraGrid.GridControl gridControlCRM;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewCRM;
        private DevExpress.XtraGrid.Columns.GridColumn colCRMID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContactID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordTypeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordID1;
        private DevExpress.XtraGrid.Columns.GridColumn colContactDueDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colContactMadeDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colContactMethodID;
        private DevExpress.XtraGrid.Columns.GridColumn colContactTypeID;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit12;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colContactedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colContactDirectionID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colContactedByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colContactMethod;
        private DevExpress.XtraGrid.Columns.GridColumn colContactType;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colContactDirectionDescription;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit3;
        private System.Windows.Forms.BindingSource sp05089CRMContactsLinkedToRecordBindingSource;
        private WoodPlanDataSet woodPlanDataSet;
        private WoodPlanDataSetTableAdapters.sp05089_CRM_Contacts_Linked_To_RecordTableAdapter sp05089_CRM_Contacts_Linked_To_RecordTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colDateCreated1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContact;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer4;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageMaterials;
        private DevExpress.XtraGrid.GridControl gridControlMaterial;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewMaterial;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraBars.BarButtonItem bbiOptimization;
        private DevExpress.XtraBars.BarButtonItem bbiGetDataToRouteOptimize;
        private DevExpress.XtraBars.BarButtonItem bbiSendDataToRouteOptimizer;
        private DevExpress.XtraBars.BarButtonItem bbiGetDataFromRouteOptimizer;
        private DevExpress.XtraBars.PopupMenu pmOptimisation;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlDateRange;
        private DevExpress.XtraEditors.SimpleButton btnDateRangeOK;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditDateRange;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageEquipment;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageComments;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageHealthAndSafety;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelFilters;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.ButtonEdit buttonEditSiteFilter;
        private DevExpress.XtraEditors.ButtonEdit buttonEditClientFilter;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditCMFilter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditJobSubTypeFilter;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditJobTypeFilter;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditRecordTypeFilter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditMaterialTypeFilter;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditEquipmentTypeFilter;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditLabourFilter;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLabourFilter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditShowTabPages;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlShowTabPages;
        private DevExpress.XtraGrid.GridControl gridControl7;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraGrid.Columns.GridColumn colTabPageName;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit7;
        private DevExpress.XtraEditors.SimpleButton btnShowTabPagesOK;
        private System.Windows.Forms.BindingSource sp04022CoreDummyTabPageListBindingSource;
        private DataSet_GC_CoreTableAdapters.sp04022_Core_Dummy_TabPageListTableAdapter sp04022_Core_Dummy_TabPageListTableAdapter;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlRecordTypeFilter;
        private DevExpress.XtraGrid.GridControl gridControl8;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraEditors.SimpleButton btnRecordTypeFilterOK;
        private DataSet_GC_Summer_Core dataSet_GC_Summer_Core;
        private System.Windows.Forms.BindingSource sp06018OMJobManagerRecordTypesBindingSource;
        private DataSet_GC_Summer_CoreTableAdapters.sp06018_OM_Job_Manager_Record_TypesTableAdapter sp06018_OM_Job_Manager_Record_TypesTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder1;
        private System.Windows.Forms.BindingSource sp06019OMJobManagerKAMsFilterBindingSource;
        private DataSet_GC_Summer_CoreTableAdapters.sp06019_OM_Job_Manager_KAMs_FilterTableAdapter sp06019_OM_Job_Manager_KAMs_FilterTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription3;
        private DevExpress.XtraGrid.Columns.GridColumn colID1;
        private DevExpress.XtraGrid.Columns.GridColumn colForename;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlCMFilter;
        private DevExpress.XtraEditors.SimpleButton btnCMFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl9;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private System.Windows.Forms.BindingSource sp06020OMJobManagerCMsFilterBindingSource;
        private DataSet_GC_Summer_CoreTableAdapters.sp06020_OM_Job_Manager_CMs_FilterTableAdapter sp06020_OM_Job_Manager_CMs_FilterTableAdapter;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlJobTypeFilter;
        private DevExpress.XtraEditors.SimpleButton btnJobTypeFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl10;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView10;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlJobSubTypeFilter;
        private DevExpress.XtraEditors.SimpleButton btnJobSubTypeFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl11;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView11;
        private System.Windows.Forms.BindingSource sp06025OMJobStatusesFilterBindingSource;
        private DataSet_GC_Summer_CoreTableAdapters.sp06025_OM_Job_Statuses_FilterTableAdapter sp06025_OM_Job_Statuses_FilterTableAdapter;
        private System.Windows.Forms.BindingSource sp06026OMJobTypeFilterBindingSource;
        private DataSet_GC_Summer_CoreTableAdapters.sp06026_OM_Job_Type_FilterTableAdapter sp06026_OM_Job_Type_FilterTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colID2;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription4;
        private DevExpress.XtraGrid.Columns.GridColumn colDisabled;
        private System.Windows.Forms.BindingSource sp06027OMJobSubTypeFilterBindingSource;
        private DataSet_GC_Summer_CoreTableAdapters.sp06027_OM_Job_Sub_Type_FilterTableAdapter sp06027_OM_Job_Sub_Type_FilterTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colID3;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription5;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colDisabled2;
        private System.Windows.Forms.BindingSource sp06028OMLabourFilterBindingSource;
        private DataSet_GC_Summer_CoreTableAdapters.sp06028_OM_Labour_FilterTableAdapter sp06028_OM_Labour_FilterTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit8;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlEquipmentTypeFilter;
        private DevExpress.XtraEditors.SimpleButton btnEquipmentFilter;
        private DevExpress.XtraGrid.GridControl gridControl13;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView13;
        private System.Windows.Forms.BindingSource sp06029OMEquipmentTypeFilterBindingSource;
        private DataSet_GC_Summer_CoreTableAdapters.sp06029_OM_Equipment_Type_FilterTableAdapter sp06029_OM_Equipment_Type_FilterTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colID6;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription6;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder2;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlMaterialTypeFilter;
        private DevExpress.XtraEditors.SimpleButton btnMaterialTypeFilter;
        private DevExpress.XtraGrid.GridControl gridControl14;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView14;
        private System.Windows.Forms.BindingSource sp06030OMMaterialFilterBindingSource;
        private DataSet_GC_Summer_CoreTableAdapters.sp06030_OM_Material_FilterTableAdapter sp06030_OM_Material_FilterTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colID7;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription7;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitsInStock;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP3;
        private DevExpress.XtraGrid.Columns.GridColumn colReorderLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colWarningLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder3;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colJobID;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colJobStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffID1;
        private DevExpress.XtraGrid.Columns.GridColumn colReactive;
        private DevExpress.XtraGrid.Columns.GridColumn colJobNoLongerRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colRequiresAccessPermit;
        private DevExpress.XtraGrid.Columns.GridColumn colAccessPermitID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPONumber;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPOID;
        private DevExpress.XtraGrid.Columns.GridColumn colFinanceSystemPONumber;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedDurationUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedDurationUnitsDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colScheduleSentDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActualStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActualEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActualDurationUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colActualDurationUnitsDescriptionID;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalMaterialExVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalMaterialVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalMaterialCost;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalMaterialExVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalMaterialVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalMaterialCost;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalEquipmentExVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalEquipmentVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalEquipmentCost;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalEquipmentExVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalEquipmentVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalEquipmentCost;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalLabourExVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalLabourVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalLabourCost;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalLabourExVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalLabourVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalLabourCost;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalCostExVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalCostVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalCost;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalCostExVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalCostVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalCost;
        private DevExpress.XtraGrid.Columns.GridColumn colClientInvoiceID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateClientInvoiced;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoiceID;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoiceReceivedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoicePaidDate;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoiceAmountPaid;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotPayContractor;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotInvoiceClient;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraGrid.Columns.GridColumn colRouteOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colStartLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colStartLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colFinishLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colFinishLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colJobStatusDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffName1;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedDurationUnitsDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colActualDurationUnitsDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedJobCount;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordType;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditLatLong;
        private DevExpress.XtraBars.BarCheckItem bciFilterSelected;
        private DevExpress.XtraGrid.Columns.GridColumn colSelected;
        private DevExpress.XtraGrid.GridControl gridControlEquipment;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewEquipment;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage3;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit9;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP4;
        private DevExpress.XtraGrid.GridControl gridControlComment;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewComment;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit10;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime9;
        private DevExpress.XtraGrid.GridControl gridControlHealthAndSafety;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewHealthAndSafety;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit11;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime8;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageWaste;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageSpraying;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageExtraInfo;
        private DevExpress.XtraGrid.GridControl gridControlWaste;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewWaste;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit7;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit12;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime5;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumeric2DP5;
        private DevExpress.XtraGrid.GridControl gridControlSpraying;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewSpraying;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit8;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit13;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime7;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP7;
        private DevExpress.XtraGrid.GridControl gridControlExtraInfo;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewExtraInfo;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit9;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit14;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime10;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourUsedID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobID1;
        private DevExpress.XtraGrid.Columns.GridColumn colCostUnitsUsed;
        private DevExpress.XtraGrid.Columns.GridColumn colCostUnitDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colSellUnitsUsed;
        private DevExpress.XtraGrid.Columns.GridColumn colSellUnitDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colCostPerUnitExVat;
        private DevExpress.XtraGrid.Columns.GridColumn colCostPerUnitVatRate;
        private DevExpress.XtraGrid.Columns.GridColumn colSellPerUnitExVat;
        private DevExpress.XtraGrid.Columns.GridColumn colSellPerUnitVatRate;
        private DevExpress.XtraGrid.Columns.GridColumn colCostValueExVat;
        private DevExpress.XtraGrid.Columns.GridColumn colCostValueVat;
        private DevExpress.XtraGrid.Columns.GridColumn colCostValueTotal;
        private DevExpress.XtraGrid.Columns.GridColumn colSellValueExVat;
        private DevExpress.XtraGrid.Columns.GridColumn colSellValueVat;
        private DevExpress.XtraGrid.Columns.GridColumn colSellValueTotal;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedStartDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colFullDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colSelected1;
        private DevExpress.XtraGrid.Columns.GridColumn colCostUnitDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colSellUnitDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colInternalExternal;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn158;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn159;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn160;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn161;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn162;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn163;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn164;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn165;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerName;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerType;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn166;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn168;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn170;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn171;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn172;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn173;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn174;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn175;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn176;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn177;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn178;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn179;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn180;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn181;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn182;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn183;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn184;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn185;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn186;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn187;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn188;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn189;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn190;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn191;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn192;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn193;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn194;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn195;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn196;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn197;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumeric2DP4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage4;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit10;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn83;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn86;
        private DevExpress.XtraGrid.Columns.GridColumn colWasteTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn88;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn89;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn102;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn103;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn104;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn105;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn106;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn107;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn167;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn198;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn199;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn200;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn201;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn202;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn203;
        private DevExpress.XtraGrid.Columns.GridColumn colWasteDisposalCentreID;
        private DevExpress.XtraGrid.Columns.GridColumn colWasteDisposalCentreName;
        private DevExpress.XtraGrid.Columns.GridColumn colWasteDisposalCentreAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn84;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn85;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn87;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn90;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn91;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn92;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn93;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn95;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn96;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn97;
        private DevExpress.XtraGrid.Columns.GridColumn colShortLinkedRecordDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName2;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedStartDate2;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeDescription3;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitNumber2;
        private DevExpress.XtraGrid.Columns.GridColumn colSelected2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn98;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn101;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn99;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn109;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn110;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn123;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn124;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn125;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn126;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn127;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn128;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn129;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn130;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn131;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn132;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn204;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn205;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn206;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn58;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn59;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn61;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn62;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn77;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn78;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn79;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn80;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn81;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn82;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn100;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn108;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn111;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn112;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn113;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn114;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn52;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn53;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn54;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn55;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn56;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn57;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn60;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn63;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn64;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn65;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn67;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordedByID;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordedByTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordedByType;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordedBy;
        private DevExpress.XtraGrid.Columns.GridColumn colDateRecorded;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn69;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn70;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn71;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn72;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn73;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn74;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn75;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn76;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn115;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn116;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn118;
        private DevExpress.XtraGrid.Columns.GridColumn colDataTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colEditorTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDataEntryMinValue;
        private DevExpress.XtraGrid.Columns.GridColumn colDataEntryMaxValue;
        private DevExpress.XtraGrid.Columns.GridColumn colDataEntryRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colOnScreenLabel;
        private DevExpress.XtraGrid.Columns.GridColumn colAttributeOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colDefaultValue;
        private DevExpress.XtraGrid.Columns.GridColumn colValueRecorded;
        private DevExpress.XtraGrid.Columns.GridColumn colValueRecordedText;
        private DevExpress.XtraGrid.Columns.GridColumn colEditorMask;
        private DevExpress.XtraGrid.Columns.GridColumn colFullDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colVisibleToClient;
        private DevExpress.XtraTab.XtraTabPage xtraTabPagePurchaseOrders;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitID;
        private DevExpress.XtraGrid.Columns.GridColumn colTenderID;
        private DevExpress.XtraGrid.Columns.GridColumn colTenderJobID;
        private DevExpress.XtraGrid.Columns.GridColumn colMinimumDaysFromLastVisit;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditInteger;
        private DevExpress.XtraGrid.Columns.GridColumn colMaximumDaysFromLastVisit;
        private DevExpress.XtraGrid.Columns.GridColumn colDaysLeeway;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingCentreCodeID;
        private DevExpress.XtraGrid.Columns.GridColumn colCISPercentage;
        private DevExpress.XtraGrid.Columns.GridColumn colCISValue;
        private DevExpress.XtraGrid.Columns.GridColumn colPurchaseOrderID;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitID1;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDateTime1;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDateTime1;
        private DevExpress.XtraGrid.Columns.GridColumn colPurchaseOrderID2;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitID3;
        private DevExpress.XtraGrid.Columns.GridColumn colPurchaseOrderID1;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitID2;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditLinkedDocumentsJob_OpenLink;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDescription;
        private System.Windows.Forms.BindingSource sp06031OMJobManagerBindingSource;
        private DataSet_OM_Job dataSet_OM_Job;
        private DataSet_OM_JobTableAdapters.sp06031_OM_Job_ManagerTableAdapter sp06031_OM_Job_ManagerTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalEquipmentVATRate;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalLabourVATRate;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalMaterialVATRate;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalMaterialVATRate;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalEquipmentVATRate;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalLabourVATRate;
        private System.Windows.Forms.BindingSource sp06033OMJobManagerLinkedlabourBindingSource;
        private DataSet_OM_JobTableAdapters.sp06033_OM_Job_Manager_Linked_labourTableAdapter sp06033_OM_Job_Manager_Linked_labourTableAdapter;
        private System.Windows.Forms.BindingSource sp06034OMJobManagerLinkedEquipmentBindingSource;
        private DataSet_OM_JobTableAdapters.sp06034_OM_Job_Manager_Linked_EquipmentTableAdapter sp06034_OM_Job_Manager_Linked_EquipmentTableAdapter;
        private System.Windows.Forms.BindingSource sp06035OMJobManagerLinkedMaterialsBindingSource;
        private DataSet_OM_JobTableAdapters.sp06035_OM_Job_Manager_Linked_MaterialsTableAdapter sp06035_OM_Job_Manager_Linked_MaterialsTableAdapter;
        private System.Windows.Forms.BindingSource sp06036OMJobManagerLinkedWasteBindingSource;
        private DataSet_OM_JobTableAdapters.sp06036_OM_Job_Manager_Linked_WasteTableAdapter sp06036_OM_Job_Manager_Linked_WasteTableAdapter;
        private System.Windows.Forms.BindingSource sp06037OMJobManagerLinkedPicturesBindingSource;
        private DataSet_OM_JobTableAdapters.sp06037_OM_Job_Manager_Linked_PicturesTableAdapter sp06037_OM_Job_Manager_Linked_PicturesTableAdapter;
        private System.Windows.Forms.BindingSource sp06038OMJobManagerLinkedSprayingBindingSource;
        private DataSet_OM_JobTableAdapters.sp06038_OM_Job_Manager_Linked_SprayingTableAdapter sp06038_OM_Job_Manager_Linked_SprayingTableAdapter;
        private System.Windows.Forms.BindingSource sp06041OMJobManagerLinkedExtraInfoBindingSource;
        private System.Windows.Forms.BindingSource sp06040OMJobManagerLinkedCommentsBindingSource;
        private System.Windows.Forms.BindingSource sp06039OMJobManagerLinkedSafetyRequirementBindingSource;
        private DataSet_OM_JobTableAdapters.sp06039_OM_Job_Manager_Linked_Safety_RequirementTableAdapter sp06039_OM_Job_Manager_Linked_Safety_RequirementTableAdapter;
        private DataSet_OM_JobTableAdapters.sp06040_OM_Job_Manager_Linked_CommentsTableAdapter sp06040_OM_Job_Manager_Linked_CommentsTableAdapter;
        private DataSet_OM_JobTableAdapters.sp06041_OM_Job_Manager_Linked_Extra_InfoTableAdapter sp06041_OM_Job_Manager_Linked_Extra_InfoTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID2;
        private DevExpress.XtraGrid.Columns.GridColumn colPictureType;
        private DevExpress.XtraGrid.Columns.GridColumn colImagesFolderOM;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraGrid.Columns.GridColumn colRework;
        private DevExpress.XtraGrid.Columns.GridColumn colReworkOriginalJobID;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationX;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationY;
        private DevExpress.XtraGrid.Columns.GridColumn colSitePostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedLabourCount;
        private DevExpress.XtraGrid.Columns.GridColumn colDaysUntilDue;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditIntegerDays;
        private DevExpress.XtraBars.BarStaticItem bsiSelectedCount;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordTypeID2;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordTypeID3;
        private DevExpress.XtraGrid.Columns.GridColumn colDilutionRatio;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumericRatio;
        private DevExpress.XtraGrid.Columns.GridColumn colAmountUsedDiluted;
        private DevExpress.XtraGrid.Columns.GridColumn colCancelledReasonID;
        private DevExpress.XtraGrid.Columns.GridColumn colCancelledReason;
        private DevExpress.XtraGrid.Columns.GridColumn colManuallyCompleted;
        private DevExpress.XtraGrid.Columns.GridColumn Alert;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEditAlert;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraGrid.GridControl gridControlLabourTiming;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewLabourTiming;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime11;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit11;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit15;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer5;
        private System.Windows.Forms.BindingSource sp06279OMLabourTimeOnOffSiteBindingSource;
        private DataSet_OM_JobTableAdapters.sp06279_OM_Labour_Time_On_Off_SiteTableAdapter sp06279_OM_Labour_Time_On_Off_SiteTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourUsedTimingID;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourUsedID1;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamMemberID;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDateTime2;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDateTime2;
        private DevExpress.XtraGrid.Columns.GridColumn colDuration;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaCreatedID;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamMemberName;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorID1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobID2;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID3;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID3;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName3;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName3;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeDescription3;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeDescription4;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedStartDate3;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitNumber3;
        private DevExpress.XtraGrid.Columns.GridColumn colFullDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorName1;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitID4;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditConstructionManagerFilter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlConstructionManagerFilter;
        private DevExpress.XtraEditors.SimpleButton btnConstructionManagerFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn37;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn39;
        private System.Windows.Forms.BindingSource sp06294OMJobManagerConstructionManagerFilterBindingSource;
        private DataSet_OM_JobTableAdapters.sp06294_OM_Job_Manager_Construction_Manager_FilterTableAdapter sp06294_OM_Job_Manager_Construction_Manager_FilterTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedPictureCount;
        private DevExpress.XtraGrid.Columns.GridColumn colNoWorkRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colFriendlyVisitNumber;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlVisitTypeFilter;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private System.Windows.Forms.BindingSource sp06308OMVisitTypesBindingSource;
        private DataSet_OM_Visit dataSet_OM_Visit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn224;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn226;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn227;
        private DevExpress.XtraGrid.Columns.GridColumn colActive1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit21;
        private DevExpress.XtraEditors.SimpleButton btnVisitTypeFilterOK;
        private DataSet_OM_VisitTableAdapters.sp06308_OM_Visit_TypesTableAdapter sp06308_OM_Visit_TypesTableAdapter;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditVisitTypeFilter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonType;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonTypeID;
        private DevExpress.XtraEditors.CheckEdit checkEditStaff;
        private DevExpress.XtraEditors.CheckEdit checkEditTeam;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlLabourFilter;
        private DevExpress.XtraEditors.SimpleButton btnLabourFilter;
        private DevExpress.XtraGrid.GridControl gridControl12;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView12;
        private DevExpress.XtraGrid.Columns.GridColumn colID4;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colActive;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit17;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeDescription;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonType1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonTypeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPO;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitType;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCode;
        private DevExpress.XtraEditors.SimpleButton btnClearAllFilters;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClearAllFilters;
        private DevExpress.XtraBars.Docking.AutoHideContainer hideContainerLeft;
        private DevExpress.Utils.ImageCollection imageCollection2;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone1;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone2;
        private DevExpress.XtraGrid.Columns.GridColumn colMobileTel;
        private DevExpress.XtraGrid.Columns.GridColumn colEmail;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colViewRecord;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditViewRecord;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitCostCalculationLevelID;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitSellCalculationLevelID;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitCostCalculationLevelDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitSellCalculationLevelDescription;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHTML;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHTML2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHTML3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHTML4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHTML5;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHTML6;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHTML7;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHTML8;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHTML9;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHTML10;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHTML11;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHTML12;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedFromVisitTemplate;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedFromVisitTemplateID;
        private DevExpress.XtraGrid.Columns.GridColumn colDisplayOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colMandatory;
        private DevExpress.XtraGrid.Columns.GridColumn colSuspendedReason;
        private DevExpress.XtraGrid.Columns.GridColumn colSuspendedRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colWasteDocumentExtension;
        private DevExpress.XtraGrid.Columns.GridColumn colWasteDocumentID;
        private DevExpress.XtraGrid.Columns.GridColumn colWasteDocumentPath;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditWasteDocument;
        private DevExpress.XtraGrid.Columns.GridColumn colAttributeName;
        private DevExpress.XtraGrid.Columns.GridColumn colValueFormula;
        private DevExpress.XtraGrid.Columns.GridColumn colValueFormulaEvaluateLocation;
        private DevExpress.XtraGrid.Columns.GridColumn colValueFormulaEvaluateLocationDesc;
        private DevExpress.XtraGrid.Columns.GridColumn colValueFormulaEvaluateOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colHideFromApp;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedPictureCount1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditEquipmentPictures;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedPictureCount2;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditMaterialPictures;
        private DevExpress.XtraGrid.Columns.GridColumn colImagesFolderOM1;
        private DevExpress.XtraGrid.Columns.GridColumn colImagesFolderOM2;
    }
}

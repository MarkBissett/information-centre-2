using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_OM_Select_Client_PO_Multi_Page : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        GridHitInfo downHitInfo = null;

        public string _Mode = "single";  // single or multiple //

        public bool boolPage1Enabled = true;
        public bool boolPage2Enabled = true;
        public bool boolPage3Enabled = true;
        public int intActivePage = 1;

        public int intOriginalPOID = 0;
        public string strOriginalPOIDs = "";
        public string _PassedInClientContractIDs = "";
        public string _PassedInSiteContractIDs = "";
        public string _PassedInVisitIDs = "";

        public int intSelectedPOID = 0;
        public string strSelectedPOIDs = "";
        public string strSelectedPONumbers = "";

        BaseObjects.GridCheckMarksSelection selection1;
        BaseObjects.GridCheckMarksSelection selection2;
        BaseObjects.GridCheckMarksSelection selection3;

        #endregion

        public frm_OM_Select_Client_PO_Multi_Page()
        {
            InitializeComponent();
        }

        private void frm_OM_Select_Client_PO_Multi_Page_Load(object sender, EventArgs e)
        {
            this.LockThisWindow();
            this.FormID = 500238;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            sp06329_OM_Client_PO_Filtered_By_Client_ContractTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06330_OM_Client_PO_Filtered_By_Site_ContractTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06331_OM_Client_PO_Filtered_By_VisitTableAdapter.Connection.ConnectionString = strConnectionString;
            GridView view = (GridView)gridControl1.MainView;

            switch (intActivePage)
            {
                case 1:
                    xtraTabControl1.SelectedTabPage = xtraTabPageClientContract;
                    view = (GridView)gridControl1.MainView;
                   break;
                case 2:
                    xtraTabControl1.SelectedTabPage = xtraTabPageSiteContract;
                    view = (GridView)gridControl2.MainView;
                    break;
                case 3:
                    xtraTabControl1.SelectedTabPage = xtraTabPageVisit;
                    view = (GridView)gridControl3.MainView;
                    break;
                default:
                    break;
            }        
            xtraTabPageClientContract.PageEnabled = boolPage1Enabled;
            xtraTabPageSiteContract.PageEnabled = boolPage2Enabled;
            xtraTabPageVisit.PageEnabled = boolPage3Enabled;

            LoadData();
            gridControl1.ForceInitialize();

            if (_Mode != "single")
            {
                // Add record selection checkboxes to popup grid control //
                selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
                selection1.CheckMarkColumn.VisibleIndex = 0;
                selection1.CheckMarkColumn.Width = 30;

                // Add record selection checkboxes to popup grid control //
                selection2 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl2.MainView);
                selection2.CheckMarkColumn.VisibleIndex = 0;
                selection2.CheckMarkColumn.Width = 30;

                // Add record selection checkboxes to popup grid control //
                selection3 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl3.MainView);
                selection3.CheckMarkColumn.VisibleIndex = 0;
                selection3.CheckMarkColumn.Width = 30;

                Array arrayRecords = strOriginalPOIDs.Split(',');  // Single quotes because char expected for delimeter //
                view.BeginUpdate();
                int intFoundRow = 0;
                foreach (string strElement in arrayRecords)
                {
                    if (strElement == "") break;
                    intFoundRow = view.LocateByValue(0, view.Columns["ClientPurchaseOrderID"], Convert.ToInt32(strElement));
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                        view.MakeRowVisible(intFoundRow, false);
                    }
                }
                view.EndUpdate();
            }
            else  // Single selection mode //
            {
                if (intOriginalPOID != 0)  // Record selected so try to find and highlight //
                {
                    int intFoundRow = view.LocateByValue(0, view.Columns["ClientPurchaseOrderID"], intOriginalPOID);
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.FocusedRowHandle = intFoundRow;
                        view.MakeRowVisible(intFoundRow, false);
                    }
                }
            }
            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
        }

        public override void PostLoadView(object objParameter)
        {
        }

        private void LoadData()
        {
            int intIgnoreHistoric = Convert.ToInt32(beiIgnoreHistoric.EditValue);
            GridView view = (GridView)gridControl1.MainView;
            if (!string.IsNullOrWhiteSpace(_PassedInClientContractIDs))
            {
                view.BeginUpdate();
                sp06329_OM_Client_PO_Filtered_By_Client_ContractTableAdapter.Fill(dataSet_OM_Client_PO.sp06329_OM_Client_PO_Filtered_By_Client_Contract, _PassedInClientContractIDs, intIgnoreHistoric);
                view.EndUpdate();
            }

            if (!string.IsNullOrWhiteSpace(_PassedInSiteContractIDs))
            {
                view = (GridView)gridControl1.MainView;
                view.BeginUpdate();
                sp06330_OM_Client_PO_Filtered_By_Site_ContractTableAdapter.Fill(dataSet_OM_Client_PO.sp06330_OM_Client_PO_Filtered_By_Site_Contract, _PassedInSiteContractIDs, intIgnoreHistoric);
                view.EndUpdate();
            }

            if (!string.IsNullOrWhiteSpace(_PassedInVisitIDs))
            {
                view = (GridView)gridControl1.MainView;
                view.BeginUpdate();
                sp06331_OM_Client_PO_Filtered_By_VisitTableAdapter.Fill(dataSet_OM_Client_PO.sp06331_OM_Client_PO_Filtered_By_Visit, _PassedInVisitIDs, intIgnoreHistoric);
                view.EndUpdate();
            }
        }

        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            LoadData();
        }

        bool internalRowFocusing;
 

        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Client Contract Purchase Orders Available";
                    break;
                case "gridView2":
                    message = "No Site Contract Purchase Orders Available";
                    break;
                case "gridView3":
                    message = "No Visit Purchase Orders Available";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_FocusedRowChanged_NoGroupSelection(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
        }

        public void GridView_MouseDown_NoGroupSelection(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        #region GridView1

        private void gridView1_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "StartingValue")
            {
                decimal decWarningValue = Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "StartingValue"));
                if (decWarningValue <= (decimal)0.00)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
            else if (e.Column.FieldName == "WarningValue")
            {
                decimal decWarningValue = Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "WarningValue"));
                decimal decRemainingValue = Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "RemainingValue"));
                if (decWarningValue > decRemainingValue && decWarningValue > (decimal)0.00)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
            else if (e.Column.FieldName == "RemainingValue")
            {
                decimal decRemainingValue = Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "RemainingValue"));
                if (decRemainingValue <= (decimal)0.00)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
        }

        #endregion


        #region GridView2

        private void gridView2_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "StartingValue")
            {
                decimal decWarningValue = Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "StartingValue"));
                if (decWarningValue <= (decimal)0.00)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
            else if (e.Column.FieldName == "WarningValue")
            {
                decimal decWarningValue = Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "WarningValue"));
                decimal decRemainingValue = Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "RemainingValue"));
                if (decWarningValue > decRemainingValue && decWarningValue > (decimal)0.00)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
            else if (e.Column.FieldName == "RemainingValue")
            {
                decimal decRemainingValue = Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "RemainingValue"));
                if (decRemainingValue <= (decimal)0.00)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
        }

        #endregion


        #region GridView3

        private void gridView3_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "StartingValue")
            {
                decimal decWarningValue = Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "StartingValue"));
                if (decWarningValue <= (decimal)0.00)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
            else if (e.Column.FieldName == "WarningValue")
            {
                decimal decWarningValue = Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "WarningValue"));
                decimal decRemainingValue = Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "RemainingValue"));
                if (decWarningValue > decRemainingValue && decWarningValue > (decimal)0.00)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
            else if (e.Column.FieldName == "RemainingValue")
            {
                decimal decRemainingValue = Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "RemainingValue"));
                if (decRemainingValue <= (decimal)0.00)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
        }

        #endregion


        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (_Mode != "single")
            {
                if (string.IsNullOrEmpty(strSelectedPONumbers))
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records by ticking them before proceeding.", "Select Client PO Code", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            else  // Single selection mode //
            {
                if (intSelectedPOID == 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Select a record before proceeding.", "Select Client PO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void GetSelectedDetails()
        {
            GridView view = (GridView)gridControl1.MainView;
            switch (xtraTabControl1.SelectedTabPage.Name)
            {
                case "xtraTabPageClientContract":
                    view = (GridView)gridControl1.MainView;
                    break;
                case "xtraTabPageSiteContract":
                    view = (GridView)gridControl2.MainView;
                    break;
                case "xtraTabPageVisit":
                    view = (GridView)gridControl3.MainView;
                    break;
                default:
                    break;
            }
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //  
            if (_Mode != "single")
            {
                strSelectedPOIDs = "";    // Reset any prior values first //
                strSelectedPONumbers = "";  // Reset any prior values first //
                if (selection1.SelectedCount <= 0) return;

                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        strSelectedPOIDs += Convert.ToString(view.GetRowCellValue(i, "ClientPurchaseOrderID")) + ",";
                        if (intCount == 0)
                        {
                            strSelectedPONumbers = Convert.ToString(view.GetRowCellValue(i, "PONumber"));
                        }
                        else if (intCount >= 1)
                        {
                            strSelectedPONumbers += ", " + Convert.ToString(view.GetRowCellValue(i, "PONumber"));
                        }
                        intCount++;
                    }
                }
            }
            else  // Single record selection //
            {
                intSelectedPOID = 0;  // Reset any prior values first //
                strSelectedPONumbers = "";  // Reset any prior values first //
                if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
                {
                    intSelectedPOID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "ClientPurchaseOrderID"));
                    strSelectedPONumbers = Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "PONumber"));
                }
            }
        }




    }
}


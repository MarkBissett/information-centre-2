using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;
using WoodPlan5.Properties;
using WoodPlan5.Classes.Operations;
using BaseObjects;
using System.Net.Mail;
using System.Globalization;

namespace WoodPlan5
{
    public partial class frm_OM_Points_Redemption_Edit : BaseObjects.frmBase
    {
        #region Instance Variables
        
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        public enum SentenceCase { Upper, Lower, Title, AsIs };
        public enum FormMode { add, edit, view, delete, blockadd, blockedit };
        bool bool_FormLoading = true;
        #endregion

        public frm_OM_Points_Redemption_Edit()
        {
            InitializeComponent();
        }

        private void frm_OM_Points_Redemption_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 703003;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;
            sp06800_OM_Team_RedemptionListItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06808_OM_Team_RedemptionItemTypeTableAdapter.Connection.ConnectionString = strConnectionString;
            try
            {
                sp06808_OM_Team_RedemptionItemTypeTableAdapter.Fill(this.dataSet_Teams.sp06808_OM_Team_RedemptionItemType);
            }
            catch (Exception) { }
            
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                case "blockadd":
                    try
                    {
                        //DataRow drNewRow;
                        var drNewRow = this.dataSet_Teams.sp06800_OM_Team_RedemptionListItem.Newsp06800_OM_Team_RedemptionListItemRow();
                        drNewRow.Mode = strFormMode;
                        drNewRow.RecordID = strRecordIDs;
                        drNewRow.ReferenceCode = "";
                        drNewRow.ItemDescription = "";
                        drNewRow.NumberOfPoints = 0;
                        drNewRow.PointsRatio = 15;
                        dataSet_Teams.sp06800_OM_Team_RedemptionListItem.Addsp06800_OM_Team_RedemptionListItemRow(drNewRow);
                    }
                    catch (Exception ex)
                    {
                        //MessageBox.Show(ex.Message);
                    }
                    break;
                case "blockedit":
                    try
                    {
                        //DataRow drNewRow;
                        var drNewRow = this.dataSet_Teams.sp06800_OM_Team_RedemptionListItem.Newsp06800_OM_Team_RedemptionListItemRow();
                        drNewRow.Mode = strFormMode;
                        drNewRow.RecordID = strRecordIDs;

                        dataSet_Teams.sp06800_OM_Team_RedemptionListItem.Addsp06800_OM_Team_RedemptionListItemRow(drNewRow);
                        drNewRow.AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                case "view":
                    try
                    {
                        sp06800_OM_Team_RedemptionListItemTableAdapter.Fill(this.dataSet_Teams.sp06800_OM_Team_RedemptionListItem, strRecordIDs, strFormMode);
                    }
                    catch (Exception)
                    {
                    }
                    break;
            }
            dataLayoutControl1.EndUpdate();

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
        }


        private void Attach_EditValueChanged_To_Children(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged += Generic_EditChanged;
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged -= Generic_EditChanged;
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (this.dataSet_Teams.sp06800_OM_Team_RedemptionListItem.Rows.Count == 0)
            {
                _KeepWaitFormOpen = false;
                if (splashScreenManager.IsSplashFormVisible)
                {
                    splashScreenManager.CloseWaitForm();
                }
                XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Points Redemption List", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.strFormMode = "blockedit";
                dxErrorProvider.ClearErrors();
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();

            _KeepWaitFormOpen = false;
            bool_FormLoading = false;
            if (splashScreenManager.IsSplashFormVisible)
            {
                splashScreenManager.CloseWaitForm();
            }
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        public bool IsValid(string emailaddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailaddress);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        ItemTypeIDLookUpEdit.Focus();
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        ItemTypeIDLookUpEdit.Focus();
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        ItemTypeIDLookUpEdit.Focus();
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        ItemTypeIDLookUpEdit.Focus();
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Viewing";
                        barStaticItemFormMode.ImageIndex = 4;  // View //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();
            SetEditorButtons();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }
        }

        private void SetEditorButtons()
        {
        }
        
        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_Teams.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }

        private void frm_OM_Points_Redemption_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_OM_Points_Redemption_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strFormMode == "view") return;
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider, dataLayoutControl1);
                XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            this.sp06800OMTeamRedemptionListItemBindingSource.EndEdit();
            try
            {
                this.sp06800_OM_Team_RedemptionListItemTableAdapter.Update(dataSet_Teams);  // Insert and Update queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                var currentRowView = (DataRowView)sp06800OMTeamRedemptionListItemBindingSource.Current;
                var currentRow = (DataSet_Teams.sp06800_OM_Team_RedemptionListItemRow)currentRowView.Row;
                if (currentRow != null) strNewIDs = currentRow.PointsRedemptionListID + ";";

                // Switch mode to Edit so than any subsequent changes update this record //
                this.strFormMode = "edit";
                if (currentRow != null)
                {
                    currentRow.Mode = "edit";
                    currentRow.AcceptChanges();  // Commit the Mode column change value so save button is not re-enabled until any further user initiated data change is made //
                }
            }

            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_OM_Points_Team_Redemption_Manager")
                    {
                        var fParentForm = (frm_OM_Points_Team_Redemption_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(2, "",strNewIDs, "");
                    }
                }
            }

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < this.dataSet_Teams.sp06800_OM_Team_RedemptionListItem.Rows.Count; i++)
            {
                switch (this.dataSet_Teams.sp06800_OM_Team_RedemptionListItem.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider, dataLayoutControl1);
                    XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }

        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        #region Validate Method
        private bool validateTextBox(TextEdit txtBox, SentenceCase SentenceCase)
        {
            if (bool_FormLoading)
                return true;

            bool valid = true;
            string ErrorText = "Please Enter a Value.";

            if (txtBox.Text == "")
            {
                valid = false;
                switch (txtBox.Name)
                {
                    case "ReferenceCodeTextEdit":
                        ErrorText = "Please provide the Reference Code.";
                        break;                        
                    case "ItemDescriptionTextEdit":
                        ErrorText = "Please provide the Item Description.";
                        break;
                }
            }
           
            if (txtBox.Text != "")
            {
                switch (SentenceCase)
                {
                    case SentenceCase.Upper:
                        txtBox.Text = txtBox.Text.ToUpper();
                        break;
                    case SentenceCase.Lower:
                        txtBox.Text = txtBox.Text.ToLower();
                        break;
                    case SentenceCase.Title:
                        txtBox.Text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(txtBox.Text);
                        break;
                }
            }

            dxErrorProvider.SetError(txtBox, ErrorText);
            if (valid)
            {
                dxErrorProvider.SetError(txtBox, "");
            }
            return valid;
        }

        private bool validateLookupEdit(LookUpEdit lookupEdit)
        {
            bool valid = true;
            if (bool_FormLoading)
                return valid;

            string ErrorText = "Please Select Value.";

            if (lookupEdit.Tag == null ? false : true)
            {
                ErrorText = "Please Select " + lookupEdit.Tag + ".";
            }
            if ((lookupEdit.EditValue == null) || this.strFormMode.ToLower() != "blockedit" && string.IsNullOrEmpty(lookupEdit.EditValue.ToString()))
            {
                dxErrorProvider.SetError(lookupEdit, ErrorText);
                valid = false;  // Show stop icon as field is invalid //
                return valid;
            }
            else
            {
                dxErrorProvider.SetError(lookupEdit, "");
                return valid;
            }
        }

        private bool validateSpinEdit(SpinEdit spnEdit)
        {
            bool valid = true;

            if (bool_FormLoading)
                return valid;

            if (spnEdit.EditValue == null || spnEdit.EditValue.ToString() == "")
            {
                dxErrorProvider.SetError(spnEdit, "Please enter a valid " + spnEdit.Tag + " value");
                //spnEdit.Focus();
                return valid = false;  // Show stop icon as field is invalid //
            }
            else
            {
                dxErrorProvider.SetError(spnEdit, "");
                return valid;
            }

        }
        #endregion

        private void ItemTypeIDLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            if (validateLookupEdit((LookUpEdit)sender))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void ReferenceCodeTextEdit_Validating(object sender, CancelEventArgs e)
        {
            if (((TextEdit)sender).Name == "ReferenceCodeTextEdit")
            {
                if (validateTextBox((TextEdit)sender, SentenceCase.Title))
                {
                    e.Cancel = false;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void ItemDescriptionTextEdit_Validating(object sender, CancelEventArgs e)
        {
            if (((TextEdit)sender).Name == "ItemDescriptionTextEdit")
            {
                if (validateTextBox((TextEdit)sender, SentenceCase.Title))
                {
                    e.Cancel = false;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void NumberOfPointsSpinEdit_Validating(object sender, CancelEventArgs e)
        {
            if (validateSpinEdit((SpinEdit)sender))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void PointsRatioSpinEdit_Validating(object sender, CancelEventArgs e)
        {
            if (validateSpinEdit((SpinEdit)sender))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }

        }
        private void ItemTypeIDLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ItemTypeIDLookUpEdit.Properties.GetDisplayText(ItemTypeIDLookUpEdit.EditValue) != "")
            {
                ReferenceCodeTextEdit.EditValue = ItemTypeIDLookUpEdit.Properties.GetDataSourceValue("Code", ItemTypeIDLookUpEdit.ItemIndex);
                ReferenceCodeTextEdit.Focus();
            }
        }
        #endregion

    }
}


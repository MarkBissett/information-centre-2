﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseObjects;
using WoodPlan5.Properties;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //
using System.IO;  // Required for Path Statement //

namespace WoodPlan5
{
    public partial class frm_OM_Tender_Send_CM_Email_Add_Comment : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        private string strConnectionStringREADONLY = "";
        Settings set = Settings.Default;
        GridHitInfo downHitInfo = null;
        public BaseObjects.GridCheckMarksSelection selection1;

        private string strLinkedDocumentPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //
        public string strPassedInTenderIDs = "";
        public string _Comments = "";
           
        #endregion

        public frm_OM_Tender_Send_CM_Email_Add_Comment()
        {
            InitializeComponent();
        }

        private void frm_OM_Tender_Send_CM_Email_Add_Comment_Load(object sender, EventArgs e)
        {
            this.FormID = 500294;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            strConnectionStringREADONLY = GlobalSettings.ConnectionStringREADONLY;
            Application.DoEvents();  // Allow Form time to repaint itself //

            Set_Grid_Highlighter_Transparent(this.Controls);

            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            try
            {
                strLinkedDocumentPath = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_TenderLinkedDocumentPath").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Linked Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            //if (!strLinkedDocumentPath.EndsWith("\\")) strLinkedDocumentPath += "\\";

            memoEditComments.EditValue = "";

            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl3.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;
            sp06514_OM_Linked_Documents_Linked_To_RecordTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            try
            {
                sp06514_OM_Linked_Documents_Linked_To_RecordTableAdapter.Fill(dataSet_OM_Core.sp06514_OM_Linked_Documents_Linked_To_Record, (string.IsNullOrEmpty(strPassedInTenderIDs) ? "" : strPassedInTenderIDs), 71);
            }
            catch (Exception ex) { }
            gridControl3.ForceInitialize();

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
        }


        private void checkEditNo_CheckedChanged(object sender, EventArgs e)
        {
            _Comments = "";
            memoEditComments.Properties.ReadOnly = true;
            gridControl3.Enabled = false;
        }

        private void checkEditYes_CheckedChanged(object sender, EventArgs e)
        {
            memoEditComments.Properties.ReadOnly = false;
            gridControl3.Enabled = true;
        }


        private void btnOK_Click(object sender, EventArgs e)
        {
            _Comments = "";
            if (checkEditYes.Checked)
            {
                _Comments = memoEditComments.EditValue.ToString();
                if (string.IsNullOrWhiteSpace(_Comments) && selection1.SelectedCount <= 0)
                {
                    XtraMessageBox.Show("Enter the comments and \\ or tick the Linked Documents to include on the CM Quote Email, or select No comments.", "Send CM Tender Quote - Check Comment \\ Linked Documents", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }


        #region GridView - Linked Documents

        private void gridView3_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Linked Documents Available - Select one or more Tenders to view Linked Documents");
        }

        private void gridView3_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridView3_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        bool internalRowFocusing;
        private void gridView3_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
        }

        private void gridView3_MouseDown(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void gridView3_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void repositoryItemHyperLinkEdit2_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "DocumentPath").ToString();
            string strExtension = view.GetRowCellValue(view.FocusedRowHandle, "DocumentExtension").ToString();
            string strFullPath = Path.Combine(strLinkedDocumentPath, strFile);
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Document Linked - unable to proceed.", "View Linked Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (!File.Exists(strFullPath))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Linked Document File is missing - unable to proceed.", "View Linked Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                if (strExtension.ToLower() != "pdf")
                {
                    System.Diagnostics.Process.Start(strFullPath);
                }
                else
                {
                    if (!BaseObjects.Keyboard.IsKeyDown(Keys.A))  // Only open in DevExpress PDF viewer if user is not holding down A key (CTRL Key doesn't work when fired from a grid as it's used for row selection) //
                    {
                        frm_Core_PDF_Viewer fChildForm = new frm_Core_PDF_Viewer();
                        fChildForm.strPDFFile = strFullPath;
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.Show();
                    }
                    else
                    {
                        System.Diagnostics.Process.Start(strFullPath);
                    }
                }
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view the Linked Document: " + strFile + ".\n\nThe File may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void Email_Linked_Documents()
        {
            GridView view = (GridView)gridControl3.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one Linked Document record to email before proceeding.", "Email Linked Document(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Get the file attachments to email //
            List<string> listFilesToEmail = new List<string>();
            foreach (int intRowHandle in intRowHandles)
            {
                listFilesToEmail.Add(Path.Combine(strLinkedDocumentPath + Convert.ToString(view.GetRowCellValue(intRowHandle, "DocumentPath"))));
            }
            // Get the senders email address //
            string strFromEmailAddress = "";
            try
            {
                WoodPlanDataSetTableAdapters.QueriesTableAdapter GetSetting = new WoodPlanDataSetTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strFromEmailAddress = GetSetting.sp00240_Get_Staff_Email_Address(GlobalSettings.UserID).ToString();
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain your email address from the Staff table.\n\nError: " + ex.Message + "\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Senders Email Address", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (string.IsNullOrWhiteSpace(strFromEmailAddress))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("You have no email address recorded against your username within the Staff table.\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Senders Email Address", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Get the person to send the email to //
            //string strToEmailAddress = "";
            //view = (GridView)gridControl3.MainView;
            //view.GetSelectedRows();
            //if (intRowHandles.Length <= 0)
            //{
            //    DevExpress.XtraEditors.XtraMessageBox.Show("Select the parent survey before proceeding.", "Email Linked Document(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            //    return;
            //}
            //int intSurveyorID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "SurveyorID"));
            //string strSurveyRemarks = view.GetRowCellValue(intRowHandles[0], "ReactiveRemarks").ToString();
            //try
            //{
            //    WoodPlanDataSetTableAdapters.QueriesTableAdapter GetSetting = new WoodPlanDataSetTableAdapters.QueriesTableAdapter();
            //    GetSetting.ChangeConnectionString(strConnectionString);
            //    strToEmailAddress = GetSetting.sp00240_Get_Staff_Email_Address(intSurveyorID).ToString();
            //}
            //catch (Exception ex)
            //{
            //    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Surveyors email address from the Staff table.\n\nError: " + ex.Message + "\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Surveyors Email Address", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            //    return;
            //}
            //if (string.IsNullOrWhiteSpace(strToEmailAddress))
            //{
            //    DevExpress.XtraEditors.XtraMessageBox.Show("There is no email address recorded against the Surveyor within the Staff table.\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Surveyors Email Address", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            //    return;
            //}
            //List<string> _listEmailAddresses = new List<string>();
            //_listEmailAddresses.Add(strToEmailAddress);

            //// Create the subject line //
            //string strSubjectLine = "New ";
            //string strExtractedRemarks = "";
            //if (!string.IsNullOrWhiteSpace(strSurveyRemarks) && strSurveyRemarks.Contains("-"))
            //{
            //    strExtractedRemarks = strSurveyRemarks.Substring(0, strSurveyRemarks.IndexOf('-') + 8);
            //}
            //strSubjectLine += (!string.IsNullOrWhiteSpace(strExtractedRemarks) ? strExtractedRemarks : "Reactive Survey") + " please sync your tablet to view this job";

            // Create the message body //
            string strMessageBody = "";

            var frm_child = new frm_Core_Linked_Document_Email();
            frm_child.GlobalSettings = this.GlobalSettings;
            //frm_child._SubjectLine = strSubjectLine;
            frm_child._MessageBody = strMessageBody;
            frm_child._FilesToEmail = listFilesToEmail;
            //frm_child._EmailAddresses = _listEmailAddresses;
            frm_child._FromEmailAddress = strFromEmailAddress;
            frm_child.ShowDialog();

        }

        #endregion





    }
}

﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraEditors.Controls;
using DevExpress.Skins;
using DevExpress.XtraScheduler;
using DevExpress.Utils.Animation;  // Required by Transition Effects //

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;
using WoodPlan5.Classes.Operations;

namespace WoodPlan5
{
    public partial class frm_OM_Site_Contract_Off_Hold_Wizard : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        bool internalRowFocusing;

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;
        bool iBool_TemplateManager = false;

        bool iBool_ContractChangesMade = false;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //
        private bool ibool_FormStillLoading = true;

        public string i_str_PassedInSiteContractIDs = "";

        private string i_str_SelectedSiteContractID = "";

        private int intDefaultVisitCategoryID = 0;

        private Utils.enmFocusedGrid _enmFocusedGrid = Utils.enmFocusedGrid.Sites;

        BaseObjects.GridCheckMarksSelection selection1;

        #endregion

        public frm_OM_Site_Contract_Off_Hold_Wizard()
        {
            InitializeComponent();
        }

        private void frm_OM_Site_Contract_Off_Hold_Wizard_Load(object sender, EventArgs e)
        {
            this.FormID = 406;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            // Get Form Permissions (For Edit Templates Button) //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, 7008, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);  // 7008 //
            ProcessPermissionsForForm();

            xtraTabControl1.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;  // Hide Tab Page headers so it doesn't look like a pageframe //
            xtraTabControl1.SelectedTabPage = this.xtraTabPageWelcome;
            //xtraTabControl1.SelectedTabPage = this.xtraTabPageStep1;  //  Jump past start page to Step 1 //

            try
            {
                sp06410_OM_Site_Contract_Wizard_Off_Hold_Site_ContractsTableAdapter.Connection.ConnectionString = strConnectionString;

                sp06411_OM_Site_Contract_Wizard_Off_Hold_VisitsTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06411_OM_Site_Contract_Wizard_Off_Hold_VisitsTableAdapter.ClearBeforeFill = false;  // ***** STOP any previous results from being cleared ***** //
            }
            catch (Exception) { }
            emptyEditor = new RepositoryItem();
            ibool_FormStillLoading = false;

            gridControl2.ForceInitialize();
            Load_Site_Contracts();

            // Add record selection checkboxes to grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl4.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;
            gridControl4.ForceInitialize();
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            ConfigureFormAccordingToMode();
            _KeepWaitFormOpen = false;
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            switch (strFormMode)
            {
                case "add":
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    break;
                default:
                    break;
            }
            ibool_FormStillLoading = false;

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(xtraTabPageStep1.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //
        }


        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        iBool_TemplateManager = sfpPermissions.blRead;
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            GridView view = null;
            int[] intRowHandles;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Sites:
                    view = (GridView)gridControl2.MainView;
                    intRowHandles = view.GetSelectedRows();
                    if (intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                    //if (intRowHandles.Length >= 1)
                    //{
                    //    alItems.Add("iDelete");
                    //    bbiDelete.Enabled = true;
                    //}
                    break;
                default:
                    break;
            }
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            view = (GridView)gridControl2.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl2.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (intRowHandles.Length > 0);
        }

        private void frm_OM_Site_Contract_Off_Hold_Wizard_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

            }
        }

        private void frm_OM_Site_Contract_Off_Hold_Wizard_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
            {
                // Store last used screen settings for current user //
                //default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "CompanyFilter", i_str_selected_Company_ids);
                //default_screen_settings.SaveDefaultScreenSettings();
            }
        }


        private void xtraTabControl1_SelectedPageChanging(object sender, DevExpress.XtraTab.TabPageChangingEventArgs e)
        {
            // Remember to set each pages Tag property to the page number in the designer //
            if (Convert.ToInt32(e.Page.Tag) > Convert.ToInt32(e.PrevPage.Tag))
            {
                ((PushTransition)transitionManager1.Transitions[xtraTabControl1].TransitionType).Parameters.EffectOptions = DevExpress.Utils.Animation.PushEffectOptions.FromRight;
            }
            else
            {
                ((PushTransition)transitionManager1.Transitions[xtraTabControl1].TransitionType).Parameters.EffectOptions = DevExpress.Utils.Animation.PushEffectOptions.FromLeft;
            }
            transitionManager1.StartTransition(xtraTabControl1);
        }
        private void xtraTabControl1_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            // Remember to set each pages Tag property to the page number in the designer //
            transitionManager1.EndTransition();
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView2":
                    message = "No Site Contracts Available - Adjust any in-place filters";
                    break;
                case "gridView4":
                    message = "No Visits Available";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_MouseDown_Standard(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_Standard(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            SetMenuStatus();
        }

        #endregion


        #region Welcome Page

        private void btnWelcomeNext_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep1;
        }

        #endregion


        #region Step 1 Page

        private void btnStep1Previous_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageWelcome;
        }
        private void btnStep1Next_Click(object sender, EventArgs e)
        {
            MoveToPage2();
        }

        private void MoveToPage2()
        {
            GridView view = (GridView)gridControl2.MainView;
            view.PostEditor();
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //         

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Processing Data...");

            GridView viewVisits = (GridView)gridControl4.MainView;
            viewVisits.BeginUpdate();
            try
            {
                dataSet_OM_Contract.sp06411_OM_Site_Contract_Wizard_Off_Hold_Visits.Rows.Clear();  // Clear any Visit records already loaded //
                selection1.ClearSelection();
                int intSiteContractID = 0;
                DateTime dtOnHoldStartDate = DateTime.MinValue;
                DateTime dtOnHoldEndDate = DateTime.MaxValue;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    intSiteContractID = Convert.ToInt32(view.GetRowCellValue(i, "SiteContractID"));
                    dtOnHoldStartDate = (view.GetRowCellValue(i, "OnHoldStartDate") != DBNull.Value ? Convert.ToDateTime(view.GetRowCellValue(i, "OnHoldStartDate")) : new DateTime(2000, 1, 1));
                    dtOnHoldEndDate = (view.GetRowCellValue(i, "OnHoldEndDate") != DBNull.Value ? Convert.ToDateTime(view.GetRowCellValue(i, "OnHoldEndDate")) : new DateTime(2500, 1, 1));
                    sp06411_OM_Site_Contract_Wizard_Off_Hold_VisitsTableAdapter.Fill(dataSet_OM_Contract.sp06411_OM_Site_Contract_Wizard_Off_Hold_Visits, intSiteContractID, dtOnHoldStartDate, dtOnHoldEndDate);  // ClearBeforeFill is false on Load event of form //
                }
            }
            catch (Exception Ex) { }
            viewVisits.ExpandAllGroups();
            viewVisits.EndUpdate();
            selection1.SelectAll();
            Set_Selected_Visit_Count_Label();
            
            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }

            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep2;
        }

        private void Load_Site_Contracts()
        {
            if (!splashScreenManager.IsSplashFormVisible)
            {
                this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Loading Site Contracts...");
            }

            GridView view = (GridView)gridControl2.MainView;
            view.BeginUpdate();
            try
            {
                sp06410_OM_Site_Contract_Wizard_Off_Hold_Site_ContractsTableAdapter.Fill(dataSet_OM_Contract.sp06410_OM_Site_Contract_Wizard_Off_Hold_Site_Contracts, i_str_PassedInSiteContractIDs);
            }
            catch (Exception) { }
            view.ExpandAllGroups();
            view.EndUpdate();
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }


        #region GridView2

        private void gridControl2_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("block edit".Equals(e.Button.Tag))
                    {
                        Block_Edit_Record();
                    }
                    break;
                default:
                    break;
            }

        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Sites;
            SetMenuStatus();
        }

        private void gridView2_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Sites;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiRecordTicking.Enabled = false;
                bbiTick.Enabled = false;
                bbiUntick.Enabled = false;
                
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #endregion


        #region Step 2 Page

        private void btnStep2Previous_Click(object sender, EventArgs e)
       {
           xtraTabControl1.SelectedTabPage = this.xtraTabPageStep1;
       }
        private void btnStep2Next_Click(object sender, EventArgs e)
        {
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Processing Data...");

            GridView view = (GridView)gridControl4.MainView;
            view.PostEditor();
            GridView viewSites = (GridView)gridControl2.MainView;
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //         

            try
            {
                int intFoundRow = 0;
                int intSiteContractID = 0;
                var strFilterClause = "";
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < viewSites.DataRowCount; i++)
                {
                    sb.Clear();
                    intSiteContractID = Convert.ToInt32(viewSites.GetRowCellValue(i, "SiteContractID"));
   
                    strFilterClause = "SiteContractID = " + intSiteContractID.ToString();
                    DataRow[] filteredRows_Visits = dataSet_OM_Contract.sp06411_OM_Site_Contract_Wizard_Off_Hold_Visits.Select(strFilterClause);
                    foreach (DataRow dr_Visit in filteredRows_Visits)
                    {
                        intFoundRow = view.LocateByValue(0, view.Columns["VisitID"], Convert.ToInt32(dr_Visit["VisitID"]));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            if (Convert.ToInt32(view.GetRowCellValue(intFoundRow, "CheckMarkSelection")) == 1) sb.Append(dr_Visit["VisitID"].ToString() + ",");
                        }
                    }
                    viewSites.SetRowCellValue(i, "VisitIDs", sb.ToString());
                }
                sp06410OMSiteContractWizardOffHoldSiteContractsBindingSource.EndEdit();  // Commit any changs from grid to underlying datasource //
            }
            catch (Exception Ex) { }

            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }

            xtraTabControl1.SelectedTabPage = this.xtraTabPageFinish;
        }

        private void Set_Selected_Visit_Count_Label()
        {
            labelControlSelectedVisitCount.Text = selection1.SelectedCount.ToString() + " Selected " + (selection1.SelectedCount == 1 ? "Visit" : "Visits");
        }

        #region GridView4

        private void gridView4_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Visit;
            SetMenuStatus();
        }

        private void gridView4_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Visit;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                int[] intRowHandles = view.GetSelectedRows();
                int intCount = intRowHandles.Length;
                bsiRecordTicking.Enabled = (intCount > 0);
                bbiTick.Enabled = (intCount > 0);
                bbiUntick.Enabled = (intCount > 0);

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion

        #endregion


        #region Finish Page

        private void btnFinishPrevious_Click(object sender, EventArgs e)
       {
           xtraTabControl1.SelectedTabPage = this.xtraTabPageStep2;
       }
        private void btnFinish_Click(object sender, EventArgs e)
        {
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Saving Changes...");

            try
            {
                sp06410_OM_Site_Contract_Wizard_Off_Hold_Site_ContractsTableAdapter.Update(dataSet_OM_Contract);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                XtraMessageBox.Show("An error occurred while updating the Site contract(s) [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
              
            // Notify any open instances of Parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                if (Application.OpenForms[strCaller] != null)
                {
                    if (strCaller == "frm_OM_Visit_Manager") (Application.OpenForms[strCaller] as frm_OM_Visit_Manager).UpdateFormRefreshStatus(1, Utils.enmFocusedGrid.Visit, "");
                    else if (strCaller == "frm_OM_Site_Contract_Manager") (Application.OpenForms[strCaller] as frm_OM_Site_Contract_Manager).UpdateFormRefreshStatus(1, Utils.enmFocusedGrid.Contract, "");
                }
            }
            this.dataSet_OM_Contract.AcceptChanges();

            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }

            this.Close();
        }

        #endregion


        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }


        private void Block_Edit_Record()
        {
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Sites:
                    {
                        Block_Edit_Off_Hold();
                    }
                    break;
                default:
                    break;
            }
        }

        private void Block_Edit_Off_Hold()
        {
            int intRecordCount = 0;
            GridView view = (GridView)gridControl2.MainView;
            int intRowCount = view.DataRowCount;
            view.PostEditor();
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            intRecordCount = intRowHandles.Length;
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Site Contracts to block edit then try again.", "Block Edit Site Contracts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            var fChildForm = new frm_OM_Site_Contract_Off_Hold_Wizard_Block_Edit_Off_Hold();
            fChildForm.GlobalSettings = this.GlobalSettings;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Updating...");
                
                intRowHandles = view.GetSelectedRows();
                intRecordCount = intRowHandles.Length;
                if (intRowHandles.Length <= 0) return;
                view.BeginUpdate();
                view.BeginSort();
                try
                {
                    foreach (int intRowHandle in intRowHandles)
                    {
                        if (fChildForm.intClearOnHoldData != null) view.SetRowCellValue(intRowHandle, "ClearOnHoldData", fChildForm.intClearOnHoldData);
                        if (fChildForm.strRemarks != null) view.SetRowCellValue(intRowHandle, "Remarks", fChildForm.strRemarks);
                    }
                }
                catch (Exception) { }
                view.EndSort();
                view.EndUpdate();
            }
            if (splashScreenManager.IsSplashFormVisible)
            {
                splashScreenManager.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager.CloseWaitForm();
            }
        }

        private void Delete_Record()
        {

        }


        public override void PostViewClick(object sender, EventArgs e, int row)
        {
            GridView view = (GridView)sender;
            if (view.GridControl.MainView.Name != "gridView4") return;  // Only fire for appropriate view //
            if (row == GridControl.InvalidRowHandle) return;
            Set_Selected_Visit_Count_Label();
        }

        private void bbiTick_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = null;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Visit:
                    {
                        view = (GridView)gridControl4.MainView;
                    }
                    break;
                default:
                    return;
            }
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length > 0)
            {
                view.BeginUpdate();
                view.BeginSelection();
                foreach (int intRowHandle in intRowHandles)
                {
                    view.SetRowCellValue(intRowHandle, "CheckMarkSelection", 1);
                }
                view.EndSelection();
                view.EndUpdate();
            }
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Visit:
                    {
                        Set_Selected_Visit_Count_Label();
                    }
                    break;
                default:
                    return;
            }
        }

        private void bbiUntick_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = null;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Visit:
                    {
                        view = (GridView)gridControl4.MainView;
                    }
                    break;
                default:
                    return;
            }
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length > 0)
            {
                view.BeginUpdate();
                view.BeginSelection();
                foreach (int intRowHandle in intRowHandles)
                {
                    view.SetRowCellValue(intRowHandle, "CheckMarkSelection", 0);
                }
                view.EndSelection();
                view.EndUpdate();
            }
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Visit:
                    {
                        Set_Selected_Visit_Count_Label();
                    }
                    break;
                default:
                    return;
            }
        }










    }
}

﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Collections.Generic;

using DevExpress.XtraBars.Ribbon;
using DevExpress.Utils;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;

using WoodPlan5.Properties;
using BaseObjects;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace WoodPlan5
{
    public partial class frm_OM_Picture_Viewer : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string _PassedInRecordIDs = "";
        public int _PassedInRecordTypeID = 0;
        public int _PassedInPictureTypeID = 0;
        int _RecordID = 0;
        int _RecordTypeID = 0;
        string _RecordTypeDescription = "";
        string _RecordDescription = "";

        public string _Path = "";
        public int _ClientID = 0;
        string _ImagesFolderOM = "";

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;

        SqlDataAdapter sdaData = null;
        DataSet dsData = null;
        
        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //
        string i_str_AddedRecordIDsPicture = "";

        private int intThumbNailWidth = 120;
        private int intThumbNailHeight = 90;
        private int intZoomedWidth = 300;
        private int intZoomedHeight = 225;

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        #endregion

        public frm_OM_Picture_Viewer()
        {
            InitializeComponent();
        }

        private void frm_OM_Picture_Viewer_Load(object sender, EventArgs e)
        {
            this.FormID = 7024;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, FormID, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);
            ProcessPermissionsForForm();


            galleryControl1.Gallery.ItemImageLayout = ImageLayoutMode.ZoomInside;
            galleryControl1.Gallery.ImageSize = new Size(120, 90);
            //galleryControl1.Gallery.ImageSize = new Size(240, 180);
            galleryControl1.Gallery.ShowItemText = true;

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                _Path = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_PictureFilesFolder").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for Job Pictures (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Linked Job Pictures Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            sp06487_OM_Picture_Viewer_Picture_Types_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06487_OM_Picture_Viewer_Picture_Types_With_BlankTableAdapter.Fill(dataSet_OM_Visit.sp06487_OM_Picture_Viewer_Picture_Types_With_Blank, 1);
            beiSetPictureType.EditValue = 0;

            char[] delimiters = new char[] { ',' };
            string[] strArray;
            strArray = _PassedInRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            if (strArray.Length >= 1)
            {
                _RecordID = Convert.ToInt32(strArray[0]);

                // Get details about the record //
                try
                {
                    SqlDataAdapter sdaD = new SqlDataAdapter();
                    DataSet dsD = new DataSet("NewDataSet");
                    using (var SQlConn = new SqlConnection(strConnectionString))
                    {
                        using (var cmd = new SqlCommand("sp06524_OM_Picture_Viewer_Get_Parent_Info", SQlConn))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@RecordID", _RecordID));
                            cmd.Parameters.Add(new SqlParameter("@RecordTypeID", _PassedInRecordTypeID));
                            cmd.Parameters.Add(new SqlParameter("@PassedInPath", _Path));
                            dsD.Clear();  // Remove old values first //
                            sdaD = new SqlDataAdapter(cmd);
                            sdaD.Fill(dsD, "Table");

                            if (dsD.Tables[0].Rows.Count == 1)
                            {
                                DataRow dr = dsD.Tables[0].Rows[0];
                                _RecordDescription = dr["RecordDescription"].ToString();
                                _ImagesFolderOM = dr["ClientImagesfolder"].ToString();
                                _RecordTypeDescription = dr["RecordTypeDescription"].ToString();
                                _RecordID = Convert.ToInt32(dr["LinkedToRecordID"]);
                                _RecordTypeID = Convert.ToInt32(dr["LinkedToRecordTypeID"]);
                            }

                        }
                    }
                }
                catch (Exception) { }
            }

            Load_Data();
        }

        private void frm_OM_Picture_Viewer_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedRecordIDsPicture)) Load_Data();
            SetMenuStatus();
        }

        private void frm_OM_Picture_Viewer_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
            {
                // Store last used screen settings for current user //
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ImageZoomLevel", beiImageSize.EditValue.ToString());
                default_screen_settings.SaveDefaultScreenSettings();
            }
        }
        
        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            ConfigureFormAccordingToMode();
            
            LoadLastSavedUserScreenSettings();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                string strItemFilter = default_screen_settings.RetrieveSetting("ImageZoomLevel");
                if (!string.IsNullOrWhiteSpace(strItemFilter))
                {
                    int n = 1;
                    bool isNumeric = int.TryParse(strItemFilter, out n);
                    if (isNumeric && n >= 1 && n <= 10) beiImageSize.EditValue = n;
                }
            }
        }

        private void ConfigureFormAccordingToMode()
        {
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                }
            }
        }
        
        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            List<GalleryItem> list = new List<GalleryItem>(galleryControl1.Gallery.GetCheckedItems());

            bbiAdd.Enabled = (iBool_AllowAdd && _ClientID > 0);
            bbiEdit.Enabled = (iBool_AllowAdd && list.Count > 0 && _ClientID > 0);
            bbiDeleteImage.Enabled = (iBool_AllowAdd && list.Count > 0 && _ClientID > 0);
            bsiSetPictureType.Enabled = (list.Count > 0);
            bbiEmailToClient.Enabled = (list.Count > 0 && _ClientID > 0);
            bbiEmailToNoAddress.Enabled = (list.Count > 0 && _ClientID > 0);
        }

        private void Load_Data()
        {
            galleryControl1.Gallery.Groups.Clear();  // Empty any existing data within the gallary //

            sdaData = new SqlDataAdapter();
            dsData = new DataSet("NewDataSet");
            using (var SQlConn = new SqlConnection(strConnectionString))
            {
                using (var cmd = new SqlCommand("sp06295_OM_Picture_Viewer", SQlConn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@RecordIDs", _PassedInRecordIDs));
                    cmd.Parameters.Add(new SqlParameter("@PassedInPath", _Path));
                    cmd.Parameters.Add(new SqlParameter("@RecordTypeID", _PassedInRecordTypeID));
                    dsData.Clear();  // Remove old values first //
                    sdaData = new SqlDataAdapter(cmd);
                    sdaData.Fill(dsData, "Table");
                    dsData.Tables[0].PrimaryKey = new DataColumn[] { dsData.Tables[0].Columns["PictureID"] };  // Add primary key as this will be needed when doing a Find row //
                }
            }
            int intLastVisitID = 0;
            GalleryItemGroup group1 = new GalleryItemGroup();
            foreach (DataRow dr in dsData.Tables[0].Rows)
            {
                int intVisitID = Convert.ToInt32(dr["VisitID"]);
                int intJobID = Convert.ToInt32(dr["JobID"]);
                if (intLastVisitID != intVisitID)
                {
                    group1 = new GalleryItemGroup();
                    group1.Caption = dr["GroupDescription"].ToString();
                    galleryControl1.Gallery.Groups.Add(group1);
                    intLastVisitID = intVisitID;
                }
                // Add the Image //
                try
                {
                    Image img = Image.FromFile(dr["PicturePath"].ToString());
                    group1.Items.Add(new GalleryItem(img, img, dr["RecordDescription"].ToString(), dr["RecordSubDescription"].ToString(), 0, 0, dr["PictureID"].ToString(), ""));

                    // clear resources used to stop too much memory being used //
                    img = null;
                    GC.Collect();
                }
                catch (Exception) { }
            }
        }


        private void galleryControl1_Gallery_ItemClick(object sender, GalleryItemClickEventArgs e)
        {
        }

        private void galleryControl1_Gallery_ItemDoubleClick(object sender, GalleryItemClickEventArgs e)
        {
            if (e.Item == null) return;

            // Pick up PictureID from Tag of Gallery item then find matching row in dataset //
            DataRow dr = dsData.Tables[0].Rows.Find(Convert.ToInt32(e.Item.Tag));
            if (dr == null)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Picture Linked - unable to proceed.", "View Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strFile = dr["PicturePath"].ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Picture Linked - unable to proceed.", "View Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(strFile);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view picture: " + strFile + ".\n\nThe picture may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void galleryControl1_Gallery_ItemCheckedChanged(object sender, GalleryItemEventArgs e)
        {
            SetMenuStatus();
        }


        private void bbiEmailToClient_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            List<GalleryItem> list = new List<GalleryItem>(galleryControl1.Gallery.GetCheckedItems());
            if (list.Count <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one picture to email to the client by clicking on it.\n\nNote you can drag the mouse to create a selection rectangle to select pictures or you can use CTRL and the Shift Keys.", "Email Client Pictures", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Get Save Folder //
            string strSubject = "";
            string strCCToEmailAddress = "";
            string strEmailAddress = "";
            try
            {
                var GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strSubject = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_VisitPicturesEmailToClientSubjectLine").ToString();
                strCCToEmailAddress = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_VisitPicturesEmailToClientCCToEmailAddress").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Email Subject Line and\\or Email CC To Address (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Open Select Receipient List to find who to send the email to //
            var fChildForm = new frm_Core_Select_Client_Contact();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm._Mode = "multiple";
            fChildForm.strPassedInClientIDsFilter = _ClientID.ToString() + ",";
            fChildForm.strPassedInContactTypeIDsFilter = "4,";  // 4: Email //

            if (fChildForm.ShowDialog() != DialogResult.OK) return;
            strEmailAddress = fChildForm.strSelectedDescription1;

            string strDateTime = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");

            var fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Preparing Email...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();

            // Note can't use a simple mailto clause and a System.Diagnostics.Process.Start since it won't allow attachments so use custom MapiMailMessage class //
            try
            {
                string strBody = "";
                //MapiMailMessage message = new MapiMailMessage();
                //message.Subject = strSubject;
                Outlook.Application outlookApp = new Outlook.Application();
                Outlook._MailItem oMailItem = (Outlook._MailItem)outlookApp.CreateItem(Outlook.OlItemType.olMailItem);
                Outlook.Inspector oInspector = oMailItem.GetInspector;
                Outlook.Recipients oRecips = (Outlook.Recipients)oMailItem.Recipients;
                oMailItem.BodyFormat = Outlook.OlBodyFormat.olFormatHTML;

                char[] delimiters = new char[] { ',' };
                Array arrayItems = strEmailAddress.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                foreach (string strElement in arrayItems)
                {
                    //message.Recipients.Add(strElement);
                    Outlook.Recipient oRecip = (Outlook.Recipient)oRecips.Add(strElement);
                    oRecip.Resolve();
                }
                //if (!string.IsNullOrWhiteSpace(strCCToEmailAddress)) message.Recipients.Add(strCCToEmailAddress, MapiMailMessage.RecipientType.CC);
                if (!string.IsNullOrWhiteSpace(strCCToEmailAddress))
                {
                    //Add CC
                    Outlook.Recipient oCCRecip = oRecips.Add(strCCToEmailAddress);
                    oCCRecip.Type = (int)Outlook.OlMailRecipientType.olCC;
                    oCCRecip.Resolve();
                }

                // Add pictures and create message body text //
                if (bciShowDescriptionOnEmail.Checked) strBody = "<tr></tr><tr></tr><tr></tr>";  // Put a few blank lines in so the user can put whatever text in they require //
                foreach (GalleryItem item in list)
                {
                    int intPictureID = Convert.ToInt32(item.Tag);
                    DataRow dr = dsData.Tables[0].Rows.Find(intPictureID);
                    if (dr != null)
                    {
                        //message.Files.Add(dr["PicturePath"].ToString());
                        oMailItem.Attachments.Add(dr["PicturePath"].ToString());
                        if (bciShowDescriptionOnEmail.Checked) strBody += dr["EmailToClientDescription"].ToString();
                        
                        strSubject = dr["EmailSubjectLine"].ToString();
                    }
                }
                //message.Body = strBody;

                if (fProgress != null)
                {
                    fProgress.SetProgressValue(100);
                    fProgress.Close();
                    fProgress = null;
                }
                //message.ShowDialog();  // Display Email Window to User //
                oMailItem.Subject = strSubject;
                //oMailItem.Body = strBody;
                oMailItem.HTMLBody = strBody;
                oMailItem.Display();  // Display the mailbox... oMailItem.Display(true) will open it modally //
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to create the email.\n\nError: " + ex.Message + "\n\nPlease try again. If the problem persists, contact Technical Support.", "Email Client Pictures", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void bbiEmailToNoAddress_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            List<GalleryItem> list = new List<GalleryItem>(galleryControl1.Gallery.GetCheckedItems());
            if (list.Count <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one picture to email by clicking on it.\n\nNote you can drag the mouse to create a selection rectangle to select pictures or you can use CTRL and the Shift Keys.", "Email Pictures", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Get Save Folder //
            string strSubject = "";
            string strCCToEmailAddress = "";
            try
            {
                var GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strSubject = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_VisitPicturesEmailToClientSubjectLine").ToString();
                strCCToEmailAddress = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_VisitPicturesEmailToClientCCToEmailAddress").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Email Subject Line and\\or Email CC To Address (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }


            string strDateTime = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");

            var fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Preparing Email...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();

            // Note can't use a simple mailto clause and a System.Diagnostics.Process.Start since it won't allow attachments so use custom MapiMailMessage class //
            try
            {
                string strBody = "";
                //MapiMailMessage message = new MapiMailMessage();
                //message.Subject = strSubject;
                Outlook.Application outlookApp = new Outlook.Application();
                Outlook._MailItem oMailItem = (Outlook._MailItem)outlookApp.CreateItem(Outlook.OlItemType.olMailItem);
                Outlook.Inspector oInspector = oMailItem.GetInspector;
                Outlook.Recipients oRecips = (Outlook.Recipients)oMailItem.Recipients;
                oMailItem.BodyFormat = Outlook.OlBodyFormat.olFormatHTML;

                //if (!string.IsNullOrWhiteSpace(strCCToEmailAddress)) message.Recipients.Add(strCCToEmailAddress, MapiMailMessage.RecipientType.CC);
                if (!string.IsNullOrWhiteSpace(strCCToEmailAddress))
                {
                    //Add CC
                    Outlook.Recipient oCCRecip = oRecips.Add(strCCToEmailAddress);
                    oCCRecip.Type = (int)Outlook.OlMailRecipientType.olCC;
                    oCCRecip.Resolve();
                }

                // Add pictures and create message body text //
                strBody = "\n\n\n";  // Put a few blank lines in so the user can put whatever text in they require //
                foreach (GalleryItem item in list)
                {
                    int intPictureID = Convert.ToInt32(item.Tag);
                    DataRow dr = dsData.Tables[0].Rows.Find(intPictureID);
                    if (dr != null)
                    {
                        //message.Files.Add(dr["PicturePath"].ToString());
                        oMailItem.Attachments.Add(dr["PicturePath"].ToString());
                        if (bciShowDescriptionOnEmail.Checked) strBody += dr["EmailToClientDescription"].ToString();
                       
                        strSubject = dr["EmailSubjectLine"].ToString();
                    }
                }
                //message.Body = strBody;

                if (fProgress != null)
                {
                    fProgress.SetProgressValue(100);
                    fProgress.Close();
                    fProgress = null;
                }
                //message.ShowDialog();  // Display Email Window to User //
                oMailItem.Subject = strSubject;
                //oMailItem.Body = strBody;
                oMailItem.HTMLBody = strBody;
                oMailItem.Display();  // Display the mailbox... oMailItem.Display(true) will open it modally //
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to create the email.\n\nError: " + ex.Message + "\n\nPlease try again. If the problem persists, contact Technical Support.", "Email Pictures", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void repositoryItemGridLookUpEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("set_type".Equals(e.Button.Tag))
                {
                    Set_Picture_Record_Type();
                }
            }
        }


        private void Set_Picture_Record_Type()
        {
            List<GalleryItem> list = new List<GalleryItem>(galleryControl1.Gallery.GetCheckedItems());
            if (list.Count <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one picture to change the picture type by clicking on it before proceeding.\n\nNote you can drag the mouse to create a selection rectangle to select pictures or you can use CTRL and the Shift Keys.", "Set Picture Type", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            int intTypeID = 0;
            try
            {
                intTypeID = Convert.ToInt32(beiSetPictureType.EditValue);
            }
            catch (Exception) { }

            if (intTypeID < 0) return;

            StringBuilder sb = new StringBuilder();
            foreach (GalleryItem item in list)
            {
                int intPictureID = Convert.ToInt32(item.Tag);
                DataRow dr = dsData.Tables[0].Rows.Find(intPictureID);
                if (dr != null)
                {
                    sb.Append(intPictureID.ToString() + ",");
                }
            }
            if (string.IsNullOrWhiteSpace(sb.ToString())) return;

            // ***** IMPORTANT NOTE: ***** Fallowing doesn't work. regardless of if the user Selects Yes or No, it goes down the yes path - so commented out due to this weird bug.
            //string strMessage = "You have " + (list.Count == 1 ? "1 Picture" : Convert.ToString(list.Count) + " Pictures") + " selected for updating!\n\nProceed?\n\n<color=red>WARNING: If you proceed " + (list.Count == 1 ? "this Picture" : "these Pictures") + " will be updated with the selected Picture Type.</color>";
            //if (XtraMessageBox.Show(strMessage, "Update Picture Record Type", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.No) return;

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Updating...");
            using (var UpdateRecords = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter())
            {
                UpdateRecords.ChangeConnectionString(strConnectionString);
                try
                {
                    UpdateRecords.sp06488_OM_Picture_Update_Picture_Type(sb.ToString(), intTypeID);
                }
                catch (Exception) { }
            }

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_OM_Job_Manager")
                    {
                        var fParentForm = (frm_OM_Job_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(2, Classes.Operations.Utils.enmFocusedGrid.Pictures, "");
                    }
                    else if (frmChild.Name == "frm_OM_Visit_Manager")
                    {
                        var fParentForm = (frm_OM_Visit_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(53, Classes.Operations.Utils.enmFocusedGrid.Pictures, "");
                    }
                }
            }

            Load_Data();

            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }
        }

        private void bbiAdd_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!iBool_AllowAdd) return;

            var fChildForm = new frm_OM_Picture_Link_Edit();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strRecordIDs = "";
            fChildForm.strFormMode = "add";
            fChildForm.strCaller = this.Name;
            fChildForm.intRecordCount = 0;
            fChildForm.FormPermissions = this.FormPermissions;
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();

            List<GalleryItem> list = new List<GalleryItem>(galleryControl1.Gallery.GetCheckedItems());
            if (list.Count > 0)
            {
                foreach (GalleryItem item in list)
                {
                    int intPictureID = Convert.ToInt32(item.Tag);
                    DataRow dr = dsData.Tables[0].Rows.Find(intPictureID);
                    if (dr != null)
                    {
                        fChildForm.intLinkedToRecordID = Convert.ToInt32(dr["LinkedToRecordID"]);
                        fChildForm.strLinkedToRecordDesc = HtmlRemoval.StripTagsCharArray(dr["LinkedToRecord"].ToString());
                        fChildForm.intPictureTypeID = Convert.ToInt32(dr["PictureTypeID"]);
                    }
                    break;
                }
            }
            else
            {
                fChildForm.intLinkedToRecordID = _RecordID;
                fChildForm.strLinkedToRecordDesc = HtmlRemoval.StripTagsCharArray(_RecordDescription);
                fChildForm.intPictureTypeID = _PassedInPictureTypeID;
            }
            fChildForm.intRecordTypeID = _PassedInRecordTypeID;
            fChildForm.strRecordTypeDescription = _RecordTypeDescription;
            fChildForm.strImagesFolderOM = _ImagesFolderOM;

            fChildForm.Show();

            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
        }

        private void bbiEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!iBool_AllowEdit) return;
            List<GalleryItem> list = new List<GalleryItem>(galleryControl1.Gallery.GetCheckedItems());
            if (list.Count <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one picture to edit by clicking on it.\n\nNote you can drag the mouse to create a selection rectangle to select pictures or you can use CTRL and the Shift Keys.", "Edit Pictures", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            StringBuilder sb = new StringBuilder();
            foreach (GalleryItem item in list)
            {
                int intPictureID = Convert.ToInt32(item.Tag);
                DataRow dr = dsData.Tables[0].Rows.Find(intPictureID);
                if (dr != null)
                {
                    sb.Append(intPictureID.ToString() + ",");
                }
            }
            if (string.IsNullOrWhiteSpace(sb.ToString())) return;

            var fChildForm = new frm_OM_Picture_Link_Edit();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strRecordIDs = sb.ToString();
            fChildForm.strFormMode = "edit";
            fChildForm.strCaller = this.Name;
            fChildForm.intRecordCount = list.Count;
            fChildForm.FormPermissions = this.FormPermissions;
            var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();
            fChildForm.Show();

            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
        }

        private void bbiDeleteImage_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!iBool_AllowDelete) return;
            List<GalleryItem> list = new List<GalleryItem>(galleryControl1.Gallery.GetCheckedItems());
            if (list.Count <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one picture to delete by clicking on it.\n\nNote you can drag the mouse to create a selection rectangle to select pictures or you can use CTRL and the Shift Keys.", "Delete Pictures", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            int intCount = list.Count;
            string strMessage = "You have " + (intCount == 1 ? "1 Picture" : Convert.ToString(intCount) + " Pictures") + " selected for delete!\n\nProceed?\n\n<color=red>WARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Picture" : "these Pictures") + " will no longer be available for selection. The original stored image(s) will not be deleted.</color>";
            if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
            {
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager1.ShowWaitForm();
                splashScreenManager1.SetWaitFormDescription("Deleting...");

                StringBuilder sb = new StringBuilder();
                foreach (GalleryItem item in list)
                {
                    int intPictureID = Convert.ToInt32(item.Tag);
                    DataRow dr = dsData.Tables[0].Rows.Find(intPictureID);
                    if (dr != null)
                    {
                        sb.Append(intPictureID.ToString() + ",");
                    }
                }
                if (string.IsNullOrWhiteSpace(sb.ToString())) return;
            
                using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                {
                    RemoveRecords.ChangeConnectionString(strConnectionString);
                    try
                    {
                        RemoveRecords.sp06000_OM_Delete("job_picture", sb.ToString());  // Remove the records from the DB in one go //
                    }
                    catch (Exception) { }
                }
                Load_Data();

                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Load_Data();
        }


        public void UpdateFormRefreshStatus(int status, string newIds)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            i_str_AddedRecordIDsPicture = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsPicture : newIds);
        }

        private void beiImageSize_EditValueChanged(object sender, EventArgs e)
        {
            int intZoom = Convert.ToInt32(beiImageSize.EditValue);
            if (intZoom > 0 && intZoom <= 10)
            {
                galleryControl1.Gallery.ImageSize = new Size(intThumbNailWidth * intZoom, intThumbNailHeight * intZoom);
            }

            if (intZoom <= 3)
            {
                galleryControl1.Gallery.HoverImageSize = new Size(intZoomedWidth * intZoom, intZoomedHeight * intZoom);
            }
            else
            {
                galleryControl1.Gallery.HoverImageSize = galleryControl1.Gallery.ImageSize;
            }
        }






    }
}

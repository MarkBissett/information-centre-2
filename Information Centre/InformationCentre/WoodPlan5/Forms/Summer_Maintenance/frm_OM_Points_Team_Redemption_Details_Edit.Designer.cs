﻿namespace WoodPlan5
{
    partial class frm_OM_Points_Team_Redemption_Details_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Points_Team_Redemption_Details_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.tableAdapterManager = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager();
            this.barStatus = new DevExpress.XtraBars.Bar();
            this.bsiFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.editFormLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.editFormDataNavigator = new DevExpress.XtraEditors.DataNavigator();
            this.sp06804OMTeamPointsRedemptionDetailItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_Teams = new WoodPlan5.DataSet_Teams();
            this.editFormDataLayoutControlGroup = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.PointsRedemptionHeaderIDLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.sp06802OMTeamPointsRedemptionHeaderItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.PointsRedemptionListIDLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.sp06800OMTeamRedemptionListItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ItemTypeIDLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.sp06808OMTeamRedemptionItemTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.OrderQuantitySpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.UnitNumberOfPointsSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.UnitPriceSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.UnitVATSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.LineTotalPointsSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.LineTotalPriceSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.LineVATSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.PurchaseOrderNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CommentsMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForPointsRedemptionHeaderID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOrderQuantity = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLineTotalPrice = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLineVAT = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForPointsRedemptionListID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForItemTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPurchaseOrderNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUnitNumberOfPoints = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLineTotalPoints = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUnitPrice = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUnitVAT = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForComments = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp06804_OM_Team_PointsRedemptionDetailItemTableAdapter = new WoodPlan5.DataSet_TeamsTableAdapters.sp06804_OM_Team_PointsRedemptionDetailItemTableAdapter();
            this.sp06802_OM_Team_PointsRedemptionHeaderItemTableAdapter = new WoodPlan5.DataSet_TeamsTableAdapters.sp06802_OM_Team_PointsRedemptionHeaderItemTableAdapter();
            this.sp06808_OM_Team_RedemptionItemTypeTableAdapter = new WoodPlan5.DataSet_TeamsTableAdapters.sp06808_OM_Team_RedemptionItemTypeTableAdapter();
            this.sp06800_OM_Team_RedemptionListItemTableAdapter = new WoodPlan5.DataSet_TeamsTableAdapters.sp06800_OM_Team_RedemptionListItemTableAdapter();
            this.sp06810OMTeamRedemptionVATRateBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp06810_OM_Team_RedemptionVATRateTableAdapter = new WoodPlan5.DataSet_TeamsTableAdapters.sp06810_OM_Team_RedemptionVATRateTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06804OMTeamPointsRedemptionDetailItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Teams)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).BeginInit();
            this.editFormDataLayoutControlGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PointsRedemptionHeaderIDLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06802OMTeamPointsRedemptionHeaderItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PointsRedemptionListIDLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06800OMTeamRedemptionListItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemTypeIDLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06808OMTeamRedemptionItemTypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OrderQuantitySpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnitNumberOfPointsSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnitPriceSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnitVATSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LineTotalPointsSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LineTotalPriceSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LineVATSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PurchaseOrderNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CommentsMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPointsRedemptionHeaderID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOrderQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLineTotalPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLineVAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPointsRedemptionListID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForItemTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPurchaseOrderNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUnitNumberOfPoints)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLineTotalPoints)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUnitPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUnitVAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForComments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06810OMTeamRedemptionVATRateBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1049, 31);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 520);
            this.barDockControlBottom.Size = new System.Drawing.Size(1049, 27);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 31);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 489);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1049, 31);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 489);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barStatus,
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.bsiFormMode,
            this.barStaticItem1,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemChangesPending,
            this.barStaticItemInformation,
            this.bbiRefresh});
            this.barManager1.MaxItemId = 36;
            this.barManager1.StatusBar = this.barStatus;
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.sp_AS_11002_Equipment_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11005_Vehicle_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11008_Plant_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11011_Equipment_Category_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11014_Make_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11017_Model_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11020_Supplier_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Tracker_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Verilocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11026_Gadget_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11029_Office_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11032_Hardware_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11035_Software_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11041_Transaction_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11047_Equipment_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11050_Depreciation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Data_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Interval_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11057_Company_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11060_Department_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11063_Cost_Centre_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11066_Depreciation_Settings_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11075_Cover_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11078_Work_Detail_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11081_Incident_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11084_Purpose_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11087_Equipment_Category_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11089_Fuel_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11093_Initial_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11096_Road_Tax_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11099_Improvement_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11102_Rental_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11105_Speeding_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11108_Rental_Details_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11112_Keeper_Email_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11117_Sim_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11120_Additional_FollowUps_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11123_Sim_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_AlertTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11129_Software_Assign_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11133_Picklist_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_AS_11141_Hardware_Specifications_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11157_Email_Schedule_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11163_Service_ScheduleTableAdapter = null;
            this.tableAdapterManager.sp_AS_11166_P11D_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11169_NetBookValue_ItemTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // barStatus
            // 
            this.barStatus.BarName = "Status Bar";
            this.barStatus.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.barStatus.DockCol = 0;
            this.barStatus.DockRow = 0;
            this.barStatus.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.barStatus.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiFormMode, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barStaticItemInformation, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barStatus.OptionsBar.AllowQuickCustomization = false;
            this.barStatus.OptionsBar.DrawDragBorder = false;
            this.barStatus.OptionsBar.UseWholeRow = true;
            this.barStatus.Text = "Status bar";
            // 
            // bsiFormMode
            // 
            this.bsiFormMode.Caption = "Form Mode: Editing";
            this.bsiFormMode.Id = 29;
            this.bsiFormMode.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.ImageOptions.Image")));
            this.bsiFormMode.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.ImageOptions.LargeImage")));
            this.bsiFormMode.Name = "bsiFormMode";
            toolTipTitleItem1.Text = "Form Mode - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bsiFormMode.SuperTip = superToolTip1;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 31;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 32;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 33;
            this.barStaticItemInformation.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.ImageOptions.Image")));
            this.barStaticItemInformation.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.ImageOptions.LargeImage")));
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiRefresh, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 27;
            this.bbiFormSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.ImageOptions.Image")));
            this.bbiFormSave.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.ImageOptions.LargeImage")));
            this.bbiFormSave.Name = "bbiFormSave";
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Text = "Save Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormSave.SuperTip = superToolTip2;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 28;
            this.bbiFormCancel.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.ImageOptions.Image")));
            this.bbiFormCancel.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.ImageOptions.LargeImage")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Text = "Cancel Button - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiFormCancel.SuperTip = superToolTip3;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Caption = "Refresh Picklists";
            this.bbiRefresh.Id = 35;
            this.bbiRefresh.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRefresh.ImageOptions.Image")));
            this.bbiRefresh.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiRefresh.ImageOptions.LargeImage")));
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "barStaticItem1";
            this.barStaticItem1.Id = 30;
            this.barStaticItem1.Name = "barStaticItem1";
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // editFormLayoutControlGroup
            // 
            this.editFormLayoutControlGroup.CustomizationFormText = "Main Layout Control Group";
            this.editFormLayoutControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.editFormLayoutControlGroup.GroupBordersVisible = false;
            this.editFormLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlGroup1,
            this.emptySpaceItem1,
            this.emptySpaceItem2});
            this.editFormLayoutControlGroup.Name = "Root";
            this.editFormLayoutControlGroup.Size = new System.Drawing.Size(1049, 489);
            this.editFormLayoutControlGroup.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.editFormDataNavigator;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(331, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(221, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // editFormDataNavigator
            // 
            this.editFormDataNavigator.Buttons.Append.Visible = false;
            this.editFormDataNavigator.Buttons.CancelEdit.Visible = false;
            this.editFormDataNavigator.Buttons.EndEdit.Visible = false;
            this.editFormDataNavigator.Buttons.Remove.Visible = false;
            this.editFormDataNavigator.DataSource = this.sp06804OMTeamPointsRedemptionDetailItemBindingSource;
            this.editFormDataNavigator.Location = new System.Drawing.Point(343, 12);
            this.editFormDataNavigator.Name = "editFormDataNavigator";
            this.editFormDataNavigator.Size = new System.Drawing.Size(217, 19);
            this.editFormDataNavigator.StyleController = this.editFormDataLayoutControlGroup;
            this.editFormDataNavigator.TabIndex = 122;
            this.editFormDataNavigator.Text = "depreciationDataNavigator";
            this.editFormDataNavigator.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            // 
            // sp06804OMTeamPointsRedemptionDetailItemBindingSource
            // 
            this.sp06804OMTeamPointsRedemptionDetailItemBindingSource.DataMember = "sp06804_OM_Team_PointsRedemptionDetailItem";
            this.sp06804OMTeamPointsRedemptionDetailItemBindingSource.DataSource = this.dataSet_Teams;
            // 
            // dataSet_Teams
            // 
            this.dataSet_Teams.DataSetName = "DataSet_Teams";
            this.dataSet_Teams.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // editFormDataLayoutControlGroup
            // 
            this.editFormDataLayoutControlGroup.Controls.Add(this.editFormDataNavigator);
            this.editFormDataLayoutControlGroup.Controls.Add(this.PointsRedemptionHeaderIDLookUpEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.PointsRedemptionListIDLookUpEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.ItemTypeIDLookUpEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.OrderQuantitySpinEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.UnitNumberOfPointsSpinEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.UnitPriceSpinEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.UnitVATSpinEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.LineTotalPointsSpinEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.LineTotalPriceSpinEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.LineVATSpinEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.PurchaseOrderNumberTextEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.CommentsMemoEdit);
            this.editFormDataLayoutControlGroup.DataSource = this.sp06804OMTeamPointsRedemptionDetailItemBindingSource;
            this.editFormDataLayoutControlGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editFormDataLayoutControlGroup.Location = new System.Drawing.Point(0, 31);
            this.editFormDataLayoutControlGroup.Name = "editFormDataLayoutControlGroup";
            this.editFormDataLayoutControlGroup.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(761, 206, 250, 350);
            this.editFormDataLayoutControlGroup.Root = this.editFormLayoutControlGroup;
            this.editFormDataLayoutControlGroup.Size = new System.Drawing.Size(1049, 489);
            this.editFormDataLayoutControlGroup.TabIndex = 4;
            this.editFormDataLayoutControlGroup.Text = "dataLayoutControl1";
            // 
            // PointsRedemptionHeaderIDLookUpEdit
            // 
            this.PointsRedemptionHeaderIDLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06804OMTeamPointsRedemptionDetailItemBindingSource, "PointsRedemptionHeaderID", true));
            this.PointsRedemptionHeaderIDLookUpEdit.Enabled = false;
            this.PointsRedemptionHeaderIDLookUpEdit.Location = new System.Drawing.Point(130, 59);
            this.PointsRedemptionHeaderIDLookUpEdit.MenuManager = this.barManager1;
            this.PointsRedemptionHeaderIDLookUpEdit.Name = "PointsRedemptionHeaderIDLookUpEdit";
            this.PointsRedemptionHeaderIDLookUpEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.PointsRedemptionHeaderIDLookUpEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.PointsRedemptionHeaderIDLookUpEdit.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.PointsRedemptionHeaderIDLookUpEdit.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.PointsRedemptionHeaderIDLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PointsRedemptionHeaderIDLookUpEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("BSCode", "BS Code", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TeamName", "Team Name", 66, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("RedemptionDate", "Redemption Date", 93, DevExpress.Utils.FormatType.DateTime, "dd/MM/yyyy", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ContactMethod", "Contact Method", 87, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TotalPointsOrder", "Total Points Order", 97, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TotalPrice", "Total Price", 60, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TotalVAT", "Total VAT", 56, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TotalOrderLines", "Total Order Lines", 92, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.PointsRedemptionHeaderIDLookUpEdit.Properties.DataSource = this.sp06802OMTeamPointsRedemptionHeaderItemBindingSource;
            this.PointsRedemptionHeaderIDLookUpEdit.Properties.DisplayMember = "BSCode";
            this.PointsRedemptionHeaderIDLookUpEdit.Properties.NullText = "";
            this.PointsRedemptionHeaderIDLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.PointsRedemptionHeaderIDLookUpEdit.Properties.ValueMember = "PointsRedemptionHeaderID";
            this.PointsRedemptionHeaderIDLookUpEdit.Size = new System.Drawing.Size(392, 20);
            this.PointsRedemptionHeaderIDLookUpEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.PointsRedemptionHeaderIDLookUpEdit.TabIndex = 144;
            this.PointsRedemptionHeaderIDLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.lookupedit_Validating);
            // 
            // sp06802OMTeamPointsRedemptionHeaderItemBindingSource
            // 
            this.sp06802OMTeamPointsRedemptionHeaderItemBindingSource.DataMember = "sp06802_OM_Team_PointsRedemptionHeaderItem";
            this.sp06802OMTeamPointsRedemptionHeaderItemBindingSource.DataSource = this.dataSet_Teams;
            // 
            // PointsRedemptionListIDLookUpEdit
            // 
            this.PointsRedemptionListIDLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06804OMTeamPointsRedemptionDetailItemBindingSource, "PointsRedemptionListID", true));
            this.PointsRedemptionListIDLookUpEdit.Location = new System.Drawing.Point(130, 35);
            this.PointsRedemptionListIDLookUpEdit.MenuManager = this.barManager1;
            this.PointsRedemptionListIDLookUpEdit.Name = "PointsRedemptionListIDLookUpEdit";
            this.PointsRedemptionListIDLookUpEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.PointsRedemptionListIDLookUpEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.PointsRedemptionListIDLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PointsRedemptionListIDLookUpEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ReferenceCode", "Reference Code", 88, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ItemDescription", "Item Description", 150, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("NumberOfPoints", "Number Of Points", 94, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PointsRatio", "Points Ratio", 67, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.PointsRedemptionListIDLookUpEdit.Properties.DataSource = this.sp06800OMTeamRedemptionListItemBindingSource;
            this.PointsRedemptionListIDLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.PointsRedemptionListIDLookUpEdit.Properties.NullText = "";
            this.PointsRedemptionListIDLookUpEdit.Properties.ValueMember = "PointsRedemptionListID";
            this.PointsRedemptionListIDLookUpEdit.Size = new System.Drawing.Size(907, 20);
            this.PointsRedemptionListIDLookUpEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.PointsRedemptionListIDLookUpEdit.TabIndex = 146;
            this.PointsRedemptionListIDLookUpEdit.EditValueChanged += new System.EventHandler(this.PointsRedemptionListIDLookUpEdit_EditValueChanged);
            this.PointsRedemptionListIDLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.lookupedit_Validating);
            // 
            // sp06800OMTeamRedemptionListItemBindingSource
            // 
            this.sp06800OMTeamRedemptionListItemBindingSource.DataMember = "sp06800_OM_Team_RedemptionListItem";
            this.sp06800OMTeamRedemptionListItemBindingSource.DataSource = this.dataSet_Teams;
            // 
            // ItemTypeIDLookUpEdit
            // 
            this.ItemTypeIDLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06804OMTeamPointsRedemptionDetailItemBindingSource, "ItemTypeID", true));
            this.ItemTypeIDLookUpEdit.Enabled = false;
            this.ItemTypeIDLookUpEdit.Location = new System.Drawing.Point(644, 59);
            this.ItemTypeIDLookUpEdit.MenuManager = this.barManager1;
            this.ItemTypeIDLookUpEdit.Name = "ItemTypeIDLookUpEdit";
            this.ItemTypeIDLookUpEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.ItemTypeIDLookUpEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ItemTypeIDLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ItemTypeIDLookUpEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ItemType", "Item Type", 59, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code", "Code", 35, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("intOrder", "int Order", 53, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.Default)});
            this.ItemTypeIDLookUpEdit.Properties.DataSource = this.sp06808OMTeamRedemptionItemTypeBindingSource;
            this.ItemTypeIDLookUpEdit.Properties.DisplayMember = "ItemType";
            this.ItemTypeIDLookUpEdit.Properties.NullText = "";
            this.ItemTypeIDLookUpEdit.Properties.ValueMember = "ItemTypeID";
            this.ItemTypeIDLookUpEdit.Size = new System.Drawing.Size(393, 20);
            this.ItemTypeIDLookUpEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.ItemTypeIDLookUpEdit.TabIndex = 147;
            this.ItemTypeIDLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.lookupedit_Validating);
            // 
            // sp06808OMTeamRedemptionItemTypeBindingSource
            // 
            this.sp06808OMTeamRedemptionItemTypeBindingSource.DataMember = "sp06808_OM_Team_RedemptionItemType";
            this.sp06808OMTeamRedemptionItemTypeBindingSource.DataSource = this.dataSet_Teams;
            // 
            // OrderQuantitySpinEdit
            // 
            this.OrderQuantitySpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06804OMTeamPointsRedemptionDetailItemBindingSource, "OrderQuantity", true));
            this.OrderQuantitySpinEdit.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.OrderQuantitySpinEdit.Location = new System.Drawing.Point(130, 83);
            this.OrderQuantitySpinEdit.MenuManager = this.barManager1;
            this.OrderQuantitySpinEdit.Name = "OrderQuantitySpinEdit";
            this.OrderQuantitySpinEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.OrderQuantitySpinEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.OrderQuantitySpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.OrderQuantitySpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.OrderQuantitySpinEdit.Properties.Mask.EditMask = "N0";
            this.OrderQuantitySpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.OrderQuantitySpinEdit.Properties.MaxValue = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.OrderQuantitySpinEdit.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.OrderQuantitySpinEdit.Size = new System.Drawing.Size(392, 20);
            this.OrderQuantitySpinEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.OrderQuantitySpinEdit.TabIndex = 149;
            this.OrderQuantitySpinEdit.EditValueChanged += new System.EventHandler(this.OrderQuantitySpinEdit_EditValueChanged);
            this.OrderQuantitySpinEdit.Validating += new System.ComponentModel.CancelEventHandler(this.SpinEdit_Validating);
            // 
            // UnitNumberOfPointsSpinEdit
            // 
            this.UnitNumberOfPointsSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06804OMTeamPointsRedemptionDetailItemBindingSource, "UnitNumberOfPoints", true));
            this.UnitNumberOfPointsSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UnitNumberOfPointsSpinEdit.Enabled = false;
            this.UnitNumberOfPointsSpinEdit.Location = new System.Drawing.Point(130, 107);
            this.UnitNumberOfPointsSpinEdit.MenuManager = this.barManager1;
            this.UnitNumberOfPointsSpinEdit.Name = "UnitNumberOfPointsSpinEdit";
            this.UnitNumberOfPointsSpinEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.UnitNumberOfPointsSpinEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.UnitNumberOfPointsSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.UnitNumberOfPointsSpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.UnitNumberOfPointsSpinEdit.Properties.Mask.EditMask = "N0";
            this.UnitNumberOfPointsSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.UnitNumberOfPointsSpinEdit.Size = new System.Drawing.Size(392, 20);
            this.UnitNumberOfPointsSpinEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.UnitNumberOfPointsSpinEdit.TabIndex = 150;
            this.UnitNumberOfPointsSpinEdit.Tag = "Number";
            this.UnitNumberOfPointsSpinEdit.Validating += new System.ComponentModel.CancelEventHandler(this.SpinEdit_Validating);
            // 
            // UnitPriceSpinEdit
            // 
            this.UnitPriceSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06804OMTeamPointsRedemptionDetailItemBindingSource, "UnitPrice", true));
            this.UnitPriceSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UnitPriceSpinEdit.Enabled = false;
            this.UnitPriceSpinEdit.Location = new System.Drawing.Point(130, 131);
            this.UnitPriceSpinEdit.MenuManager = this.barManager1;
            this.UnitPriceSpinEdit.Name = "UnitPriceSpinEdit";
            this.UnitPriceSpinEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.UnitPriceSpinEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.UnitPriceSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.UnitPriceSpinEdit.Properties.DisplayFormat.FormatString = "c2";
            this.UnitPriceSpinEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.UnitPriceSpinEdit.Properties.Mask.EditMask = "c";
            this.UnitPriceSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.UnitPriceSpinEdit.Properties.MaxValue = new decimal(new int[] {
            -1530494976,
            232830,
            0,
            0});
            this.UnitPriceSpinEdit.Properties.NullText = "0.00";
            this.UnitPriceSpinEdit.Size = new System.Drawing.Size(392, 20);
            this.UnitPriceSpinEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.UnitPriceSpinEdit.TabIndex = 151;
            this.UnitPriceSpinEdit.Tag = "Amount";
            this.UnitPriceSpinEdit.Validating += new System.ComponentModel.CancelEventHandler(this.SpinEdit_Validating);
            // 
            // UnitVATSpinEdit
            // 
            this.UnitVATSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06804OMTeamPointsRedemptionDetailItemBindingSource, "UnitVAT", true));
            this.UnitVATSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UnitVATSpinEdit.Enabled = false;
            this.UnitVATSpinEdit.Location = new System.Drawing.Point(644, 131);
            this.UnitVATSpinEdit.MenuManager = this.barManager1;
            this.UnitVATSpinEdit.Name = "UnitVATSpinEdit";
            this.UnitVATSpinEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.UnitVATSpinEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.UnitVATSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.UnitVATSpinEdit.Properties.DisplayFormat.FormatString = "c2";
            this.UnitVATSpinEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.UnitVATSpinEdit.Properties.Mask.EditMask = "c2";
            this.UnitVATSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.UnitVATSpinEdit.Properties.MaxValue = new decimal(new int[] {
            -1530494976,
            232830,
            0,
            0});
            this.UnitVATSpinEdit.Properties.NullText = "0.00";
            this.UnitVATSpinEdit.Size = new System.Drawing.Size(393, 20);
            this.UnitVATSpinEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.UnitVATSpinEdit.TabIndex = 152;
            this.UnitVATSpinEdit.Tag = "Amount";
            this.UnitVATSpinEdit.Validating += new System.ComponentModel.CancelEventHandler(this.SpinEdit_Validating);
            // 
            // LineTotalPointsSpinEdit
            // 
            this.LineTotalPointsSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06804OMTeamPointsRedemptionDetailItemBindingSource, "LineTotalPoints", true));
            this.LineTotalPointsSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.LineTotalPointsSpinEdit.Enabled = false;
            this.LineTotalPointsSpinEdit.Location = new System.Drawing.Point(644, 107);
            this.LineTotalPointsSpinEdit.MenuManager = this.barManager1;
            this.LineTotalPointsSpinEdit.Name = "LineTotalPointsSpinEdit";
            this.LineTotalPointsSpinEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.LineTotalPointsSpinEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.LineTotalPointsSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LineTotalPointsSpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.LineTotalPointsSpinEdit.Properties.Mask.EditMask = "N0";
            this.LineTotalPointsSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.LineTotalPointsSpinEdit.Size = new System.Drawing.Size(393, 20);
            this.LineTotalPointsSpinEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.LineTotalPointsSpinEdit.TabIndex = 153;
            this.LineTotalPointsSpinEdit.Tag = "Number";
            // 
            // LineTotalPriceSpinEdit
            // 
            this.LineTotalPriceSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06804OMTeamPointsRedemptionDetailItemBindingSource, "LineTotalPrice", true));
            this.LineTotalPriceSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.LineTotalPriceSpinEdit.Enabled = false;
            this.LineTotalPriceSpinEdit.Location = new System.Drawing.Point(130, 155);
            this.LineTotalPriceSpinEdit.MenuManager = this.barManager1;
            this.LineTotalPriceSpinEdit.Name = "LineTotalPriceSpinEdit";
            this.LineTotalPriceSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LineTotalPriceSpinEdit.Properties.DisplayFormat.FormatString = "c2";
            this.LineTotalPriceSpinEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.LineTotalPriceSpinEdit.Properties.Mask.EditMask = "c2";
            this.LineTotalPriceSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.LineTotalPriceSpinEdit.Properties.MaxValue = new decimal(new int[] {
            -1530494976,
            232830,
            0,
            0});
            this.LineTotalPriceSpinEdit.Properties.NullText = "0.00";
            this.LineTotalPriceSpinEdit.Size = new System.Drawing.Size(392, 20);
            this.LineTotalPriceSpinEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.LineTotalPriceSpinEdit.TabIndex = 154;
            this.LineTotalPriceSpinEdit.Tag = "Amount";
            // 
            // LineVATSpinEdit
            // 
            this.LineVATSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06804OMTeamPointsRedemptionDetailItemBindingSource, "LineVAT", true));
            this.LineVATSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.LineVATSpinEdit.Enabled = false;
            this.LineVATSpinEdit.Location = new System.Drawing.Point(644, 155);
            this.LineVATSpinEdit.MenuManager = this.barManager1;
            this.LineVATSpinEdit.Name = "LineVATSpinEdit";
            this.LineVATSpinEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.LineVATSpinEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.LineVATSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LineVATSpinEdit.Properties.DisplayFormat.FormatString = "c2";
            this.LineVATSpinEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.LineVATSpinEdit.Properties.Mask.EditMask = "c2";
            this.LineVATSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.LineVATSpinEdit.Properties.MaxValue = new decimal(new int[] {
            -1530494976,
            232830,
            0,
            0});
            this.LineVATSpinEdit.Properties.NullText = "0.00";
            this.LineVATSpinEdit.Size = new System.Drawing.Size(393, 20);
            this.LineVATSpinEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.LineVATSpinEdit.TabIndex = 155;
            this.LineVATSpinEdit.Tag = "Amount";
            // 
            // PurchaseOrderNumberTextEdit
            // 
            this.PurchaseOrderNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06804OMTeamPointsRedemptionDetailItemBindingSource, "PurchaseOrderNumber", true));
            this.PurchaseOrderNumberTextEdit.Location = new System.Drawing.Point(644, 83);
            this.PurchaseOrderNumberTextEdit.MenuManager = this.barManager1;
            this.PurchaseOrderNumberTextEdit.Name = "PurchaseOrderNumberTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.PurchaseOrderNumberTextEdit, true);
            this.PurchaseOrderNumberTextEdit.Size = new System.Drawing.Size(393, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PurchaseOrderNumberTextEdit, optionsSpelling1);
            this.PurchaseOrderNumberTextEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.PurchaseOrderNumberTextEdit.TabIndex = 156;
            this.PurchaseOrderNumberTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.textBox_Validating);
            // 
            // CommentsMemoEdit
            // 
            this.CommentsMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sp06804OMTeamPointsRedemptionDetailItemBindingSource, "Comments", true));
            this.CommentsMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06804OMTeamPointsRedemptionDetailItemBindingSource, "Comments", true));
            this.CommentsMemoEdit.Location = new System.Drawing.Point(24, 213);
            this.CommentsMemoEdit.MenuManager = this.barManager1;
            this.CommentsMemoEdit.Name = "CommentsMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.CommentsMemoEdit, true);
            this.CommentsMemoEdit.Size = new System.Drawing.Size(1001, 240);
            this.scSpellChecker.SetSpellCheckerOptions(this.CommentsMemoEdit, optionsSpelling2);
            this.CommentsMemoEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.CommentsMemoEdit.TabIndex = 157;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AllowDrawBackground = false;
            this.layoutControlGroup1.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForPointsRedemptionHeaderID,
            this.ItemForOrderQuantity,
            this.ItemForLineTotalPrice,
            this.ItemForLineVAT,
            this.emptySpaceItem3,
            this.ItemForPointsRedemptionListID,
            this.ItemForItemTypeID,
            this.ItemForPurchaseOrderNumber,
            this.ItemForUnitNumberOfPoints,
            this.ItemForLineTotalPoints,
            this.ItemForUnitPrice,
            this.ItemForUnitVAT,
            this.tabbedControlGroup1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 23);
            this.layoutControlGroup1.Name = "autoGeneratedGroup0";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1029, 446);
            // 
            // ItemForPointsRedemptionHeaderID
            // 
            this.ItemForPointsRedemptionHeaderID.Control = this.PointsRedemptionHeaderIDLookUpEdit;
            this.ItemForPointsRedemptionHeaderID.Location = new System.Drawing.Point(0, 24);
            this.ItemForPointsRedemptionHeaderID.Name = "ItemForPointsRedemptionHeaderID";
            this.ItemForPointsRedemptionHeaderID.Size = new System.Drawing.Size(514, 24);
            this.ItemForPointsRedemptionHeaderID.Text = "BS Code";
            this.ItemForPointsRedemptionHeaderID.TextSize = new System.Drawing.Size(115, 13);
            // 
            // ItemForOrderQuantity
            // 
            this.ItemForOrderQuantity.Control = this.OrderQuantitySpinEdit;
            this.ItemForOrderQuantity.Location = new System.Drawing.Point(0, 48);
            this.ItemForOrderQuantity.Name = "ItemForOrderQuantity";
            this.ItemForOrderQuantity.Size = new System.Drawing.Size(514, 24);
            this.ItemForOrderQuantity.Text = "Order Quantity";
            this.ItemForOrderQuantity.TextSize = new System.Drawing.Size(115, 13);
            // 
            // ItemForLineTotalPrice
            // 
            this.ItemForLineTotalPrice.Control = this.LineTotalPriceSpinEdit;
            this.ItemForLineTotalPrice.Location = new System.Drawing.Point(0, 120);
            this.ItemForLineTotalPrice.Name = "ItemForLineTotalPrice";
            this.ItemForLineTotalPrice.Size = new System.Drawing.Size(514, 24);
            this.ItemForLineTotalPrice.Text = "Line Total Price";
            this.ItemForLineTotalPrice.TextSize = new System.Drawing.Size(115, 13);
            // 
            // ItemForLineVAT
            // 
            this.ItemForLineVAT.Control = this.LineVATSpinEdit;
            this.ItemForLineVAT.Location = new System.Drawing.Point(514, 120);
            this.ItemForLineVAT.Name = "ItemForLineVAT";
            this.ItemForLineVAT.Size = new System.Drawing.Size(515, 24);
            this.ItemForLineVAT.Text = "Line VAT";
            this.ItemForLineVAT.TextSize = new System.Drawing.Size(115, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 434);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(1029, 12);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForPointsRedemptionListID
            // 
            this.ItemForPointsRedemptionListID.Control = this.PointsRedemptionListIDLookUpEdit;
            this.ItemForPointsRedemptionListID.Location = new System.Drawing.Point(0, 0);
            this.ItemForPointsRedemptionListID.Name = "ItemForPointsRedemptionListID";
            this.ItemForPointsRedemptionListID.Size = new System.Drawing.Size(1029, 24);
            this.ItemForPointsRedemptionListID.Text = "Redemable Item";
            this.ItemForPointsRedemptionListID.TextSize = new System.Drawing.Size(115, 13);
            // 
            // ItemForItemTypeID
            // 
            this.ItemForItemTypeID.Control = this.ItemTypeIDLookUpEdit;
            this.ItemForItemTypeID.Enabled = false;
            this.ItemForItemTypeID.Location = new System.Drawing.Point(514, 24);
            this.ItemForItemTypeID.Name = "ItemForItemTypeID";
            this.ItemForItemTypeID.Size = new System.Drawing.Size(515, 24);
            this.ItemForItemTypeID.Text = "Item Type";
            this.ItemForItemTypeID.TextSize = new System.Drawing.Size(115, 13);
            // 
            // ItemForPurchaseOrderNumber
            // 
            this.ItemForPurchaseOrderNumber.Control = this.PurchaseOrderNumberTextEdit;
            this.ItemForPurchaseOrderNumber.Location = new System.Drawing.Point(514, 48);
            this.ItemForPurchaseOrderNumber.Name = "ItemForPurchaseOrderNumber";
            this.ItemForPurchaseOrderNumber.Size = new System.Drawing.Size(515, 24);
            this.ItemForPurchaseOrderNumber.Text = "Purchase Order Number";
            this.ItemForPurchaseOrderNumber.TextSize = new System.Drawing.Size(115, 13);
            // 
            // ItemForUnitNumberOfPoints
            // 
            this.ItemForUnitNumberOfPoints.Control = this.UnitNumberOfPointsSpinEdit;
            this.ItemForUnitNumberOfPoints.Location = new System.Drawing.Point(0, 72);
            this.ItemForUnitNumberOfPoints.Name = "ItemForUnitNumberOfPoints";
            this.ItemForUnitNumberOfPoints.Size = new System.Drawing.Size(514, 24);
            this.ItemForUnitNumberOfPoints.Text = "Unit Number Of Points";
            this.ItemForUnitNumberOfPoints.TextSize = new System.Drawing.Size(115, 13);
            // 
            // ItemForLineTotalPoints
            // 
            this.ItemForLineTotalPoints.Control = this.LineTotalPointsSpinEdit;
            this.ItemForLineTotalPoints.Location = new System.Drawing.Point(514, 72);
            this.ItemForLineTotalPoints.Name = "ItemForLineTotalPoints";
            this.ItemForLineTotalPoints.Size = new System.Drawing.Size(515, 24);
            this.ItemForLineTotalPoints.Text = "Line Total Points";
            this.ItemForLineTotalPoints.TextSize = new System.Drawing.Size(115, 13);
            // 
            // ItemForUnitPrice
            // 
            this.ItemForUnitPrice.Control = this.UnitPriceSpinEdit;
            this.ItemForUnitPrice.Location = new System.Drawing.Point(0, 96);
            this.ItemForUnitPrice.Name = "ItemForUnitPrice";
            this.ItemForUnitPrice.Size = new System.Drawing.Size(514, 24);
            this.ItemForUnitPrice.Text = "Unit Price";
            this.ItemForUnitPrice.TextSize = new System.Drawing.Size(115, 13);
            // 
            // ItemForUnitVAT
            // 
            this.ItemForUnitVAT.Control = this.UnitVATSpinEdit;
            this.ItemForUnitVAT.Location = new System.Drawing.Point(514, 96);
            this.ItemForUnitVAT.Name = "ItemForUnitVAT";
            this.ItemForUnitVAT.Size = new System.Drawing.Size(515, 24);
            this.ItemForUnitVAT.Text = "Unit VAT";
            this.ItemForUnitVAT.TextSize = new System.Drawing.Size(115, 13);
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 144);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup2;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(1029, 290);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForComments});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1005, 244);
            this.layoutControlGroup2.Text = "Comments";
            // 
            // ItemForComments
            // 
            this.ItemForComments.Control = this.CommentsMemoEdit;
            this.ItemForComments.Location = new System.Drawing.Point(0, 0);
            this.ItemForComments.Name = "ItemForComments";
            this.ItemForComments.Size = new System.Drawing.Size(1005, 244);
            this.ItemForComments.StartNewLine = true;
            this.ItemForComments.Text = "Comments";
            this.ItemForComments.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForComments.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(331, 23);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(552, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(477, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp06804_OM_Team_PointsRedemptionDetailItemTableAdapter
            // 
            this.sp06804_OM_Team_PointsRedemptionDetailItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp06802_OM_Team_PointsRedemptionHeaderItemTableAdapter
            // 
            this.sp06802_OM_Team_PointsRedemptionHeaderItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp06808_OM_Team_RedemptionItemTypeTableAdapter
            // 
            this.sp06808_OM_Team_RedemptionItemTypeTableAdapter.ClearBeforeFill = true;
            // 
            // sp06800_OM_Team_RedemptionListItemTableAdapter
            // 
            this.sp06800_OM_Team_RedemptionListItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp06810OMTeamRedemptionVATRateBindingSource
            // 
            this.sp06810OMTeamRedemptionVATRateBindingSource.DataMember = "sp06810_OM_Team_RedemptionVATRate";
            this.sp06810OMTeamRedemptionVATRateBindingSource.DataSource = this.dataSet_Teams;
            // 
            // sp06810_OM_Team_RedemptionVATRateTableAdapter
            // 
            this.sp06810_OM_Team_RedemptionVATRateTableAdapter.ClearBeforeFill = true;
            // 
            // frm_OM_Points_Team_Redemption_Details_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1049, 547);
            this.Controls.Add(this.editFormDataLayoutControlGroup);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1065, 586);
            this.Name = "frm_OM_Points_Team_Redemption_Details_Edit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Manage Team Points Redemption Details";
            this.Activated += new System.EventHandler(this.frm_OM_Points_Team_Redemption_Details_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_OM_Points_Team_Redemption_Details_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_OM_Points_Team_Redemption_Details_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.editFormDataLayoutControlGroup, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06804OMTeamPointsRedemptionDetailItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Teams)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).EndInit();
            this.editFormDataLayoutControlGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PointsRedemptionHeaderIDLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06802OMTeamPointsRedemptionHeaderItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PointsRedemptionListIDLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06800OMTeamRedemptionListItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemTypeIDLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06808OMTeamRedemptionItemTypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OrderQuantitySpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnitNumberOfPointsSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnitPriceSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnitVATSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LineTotalPointsSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LineTotalPriceSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LineVATSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PurchaseOrderNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CommentsMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPointsRedemptionHeaderID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOrderQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLineTotalPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLineVAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPointsRedemptionListID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForItemTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPurchaseOrderNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUnitNumberOfPoints)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLineTotalPoints)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUnitPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUnitVAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForComments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06810OMTeamRedemptionVATRateBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DataSet_AS_DataEntryTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraBars.Bar barStatus;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.BarStaticItem bsiFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private DevExpress.XtraDataLayout.DataLayoutControl editFormDataLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlGroup editFormLayoutControlGroup;
        private DevExpress.XtraEditors.DataNavigator editFormDataNavigator;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraEditors.LookUpEdit PointsRedemptionHeaderIDLookUpEdit;
        private DataSet_Teams dataSet_Teams;
        private System.Windows.Forms.BindingSource sp06802OMTeamPointsRedemptionHeaderItemBindingSource;
        private DevExpress.XtraEditors.LookUpEdit PointsRedemptionListIDLookUpEdit;
        private DevExpress.XtraEditors.LookUpEdit ItemTypeIDLookUpEdit;
        private DevExpress.XtraEditors.SpinEdit OrderQuantitySpinEdit;
        private DevExpress.XtraEditors.SpinEdit UnitNumberOfPointsSpinEdit;
        private DevExpress.XtraEditors.SpinEdit UnitPriceSpinEdit;
        private DevExpress.XtraEditors.SpinEdit UnitVATSpinEdit;
        private DevExpress.XtraEditors.SpinEdit LineTotalPointsSpinEdit;
        private DevExpress.XtraEditors.SpinEdit LineTotalPriceSpinEdit;
        private DevExpress.XtraEditors.SpinEdit LineVATSpinEdit;
        private DevExpress.XtraEditors.TextEdit PurchaseOrderNumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPointsRedemptionHeaderID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPointsRedemptionListID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForItemTypeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOrderQuantity;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUnitNumberOfPoints;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUnitPrice;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUnitVAT;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLineTotalPoints;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLineTotalPrice;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLineVAT;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPurchaseOrderNumber;
        private DataSet_TeamsTableAdapters.sp06804_OM_Team_PointsRedemptionDetailItemTableAdapter sp06804_OM_Team_PointsRedemptionDetailItemTableAdapter;
        private DataSet_TeamsTableAdapters.sp06802_OM_Team_PointsRedemptionHeaderItemTableAdapter sp06802_OM_Team_PointsRedemptionHeaderItemTableAdapter;
        private System.Windows.Forms.BindingSource sp06808OMTeamRedemptionItemTypeBindingSource;
        private DataSet_TeamsTableAdapters.sp06808_OM_Team_RedemptionItemTypeTableAdapter sp06808_OM_Team_RedemptionItemTypeTableAdapter;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private System.Windows.Forms.BindingSource sp06804OMTeamPointsRedemptionDetailItemBindingSource;
        private System.Windows.Forms.BindingSource sp06800OMTeamRedemptionListItemBindingSource;
        private DataSet_TeamsTableAdapters.sp06800_OM_Team_RedemptionListItemTableAdapter sp06800_OM_Team_RedemptionListItemTableAdapter;
        private System.Windows.Forms.BindingSource sp06810OMTeamRedemptionVATRateBindingSource;
        private DataSet_TeamsTableAdapters.sp06810_OM_Team_RedemptionVATRateTableAdapter sp06810_OM_Team_RedemptionVATRateTableAdapter;
        private DevExpress.XtraEditors.MemoEdit CommentsMemoEdit;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForComments;
    }
}

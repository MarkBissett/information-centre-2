using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.IO;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;
using WoodPlan5.Properties;
using WoodPlan5.Classes.Operations;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_OM_Job_Waste_Edit : BaseObjects.frmBase
    {
        #region Instance Variables
        
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //
        private bool ibool_FormStillLoading = true;

        public int _ClientID = 0;
        public int _ClientContractID = 0;
        public int _SiteID = 0;
        public int _SiteContractID = 0;
        public int _VisitID = 0;
        public int _VisitNumber = 0;
        public int _LinkedToRecordID = 0;
        public int _LinkedToRecordTypeID = 0;
        public int _LinkedToRecordSubTypeID = 0;  // 0 = Visit, 1 = Job //
        public int _JobTypeID = 0;
        public int _JobSubTypeID = 0;
        public string _ClientName = "";
        public string _ContractDescription = "";
        public string _SiteName = "";
        public string _JobTypeDescription = "";
        public string _JobSubTypeDescription = "";
        public string _LinkedToParentDescription = "";
        
        private string strWasteDocumentPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //

        #endregion

        public frm_OM_Job_Waste_Edit()
        {
            InitializeComponent();
        }
        
        private void frm_OM_Job_Waste_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 500184;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strWasteDocumentPath = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_WasteDocumentFolder").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for Waste Documents (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Waste Document Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            
            sp06108_OM_Unit_Descriptors_PicklistTableAdapter.Connection.ConnectionString = strConnectionString;
            try
            {
                sp06108_OM_Unit_Descriptors_PicklistTableAdapter.Fill(dataSet_OM_Contract.sp06108_OM_Unit_Descriptors_Picklist, 1);
            }
            catch (Exception) { }

            // Populate Main Dataset //    
            sp06213_OM_Job_Waste_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();

            switch (strFormMode)
            {
                case "add":
                case "blockadd":
                    try
                    {
                        DataRow drNewRow = dataSet_OM_Job.sp06213_OM_Job_Waste_Edit.NewRow();
                        drNewRow["strMode"] = strFormMode;
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["WasteID"] = 0;
                        drNewRow["WasteTypeID"] = 0;
                        drNewRow["WasteType"] = "";
                        drNewRow["LinkedToRecordID"] = _LinkedToRecordID;
                        drNewRow["LinkedToRecordTypeID"] = _LinkedToRecordTypeID;
                        drNewRow["WasteDisposalCentreID"] = 0;
                        drNewRow["WasteDisposalCentreName"] = "";
                        drNewRow["LinkedToParentDescription"] = (strFormMode == "blockadd" ? "N\\A - Block Adding" : _LinkedToParentDescription);
                        drNewRow["ClientNameContractDescription"] =  (strFormMode == "blockadd" ? "N\\A - Block Adding" : (string.IsNullOrWhiteSpace(_ClientName) ? "" : _ClientName + ", Contract: " ) + (string.IsNullOrEmpty(_ContractDescription) ? "" : _ContractDescription));
                        drNewRow["WasteAmount"] = (decimal)0.00;
                        drNewRow["WasteAmountUnitDescriptorID"] = 0;
                        drNewRow["ClientID"] = _ClientID;
                        drNewRow["ClientName"] = (strFormMode == "blockadd" ? "N\\A - Block Adding" : _ClientName);
                        drNewRow["SiteID"] = _SiteID;
                        drNewRow["SiteName"] = (strFormMode == "blockadd" ? "N\\A - Block Adding" : _SiteName);
                        drNewRow["VisitID"] = _VisitID;
                        drNewRow["VisitNumber"] = (strFormMode == "blockadd" ? 0 : _VisitNumber);
                        drNewRow["JobTypeID"] = _JobTypeID;
                        drNewRow["JobSubTypeID"] = _JobSubTypeID;
                        drNewRow["SiteContractID"] = _SiteContractID;
                        drNewRow["ClientContractID"] = _ClientContractID;
                        drNewRow["ContractDescription"] = (strFormMode == "blockadd" ? "N\\A - Block Adding" : _ContractDescription);
                        drNewRow["JobTypeDescription"] = (strFormMode == "blockadd" ? "N\\A - Block Adding" : _JobTypeDescription);
                        drNewRow["JobSubTypeDescription"] = (strFormMode == "blockadd" ? "N\\A - Block Adding" : _JobSubTypeDescription);
                        drNewRow["WasteDocumentID"] = 0;
                        drNewRow["WasteDocumentPath"] = "";
                        drNewRow["WasteDocumentPathExtension"] = "";
                        dataSet_OM_Job.sp06213_OM_Job_Waste_Edit.Rows.Add(drNewRow);
                    }
                    catch (Exception ex)
                    {
                    }
                    break;
                case "blockedit":
                    try
                    {
                        //DataRow drNewRow;
                        DataRow drNewRow = dataSet_OM_Job.sp06213_OM_Job_Waste_Edit.NewRow();
                        drNewRow["strMode"] = strFormMode;
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        dataSet_OM_Job.sp06213_OM_Job_Waste_Edit.Rows.Add(drNewRow);
                        drNewRow.AcceptChanges();
                    }
                    catch (Exception ex)
                    {
                    }
                    break;
                case "edit":
                case "view":
                    try
                    {
                        sp06213_OM_Job_Waste_EditTableAdapter.Fill(this.dataSet_OM_Job.sp06213_OM_Job_Waste_Edit, strRecordIDs, strFormMode, strWasteDocumentPath);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);

                    }
                    break;
            }
            dataLayoutControl1.EndUpdate();

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
        }
       
        private void frm_OM_Job_Waste_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_OM_Job_Waste_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strFormMode == "view") return;
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }

 
        private void Attach_EditValueChanged_To_Children(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (this.dataSet_OM_Job.sp06213_OM_Job_Waste_Edit.Rows.Count == 0)
            {
                _KeepWaitFormOpen = false;
                splashScreenManager.CloseWaitForm();
                XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Job / Visit Waste", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        LinkedToParentDescriptionButtonEdit.Focus();

                        LinkedToParentDescriptionButtonEdit.Properties.ReadOnly = false;
                        LinkedToParentDescriptionButtonEdit.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        WasteTypeButtonEdit.Focus();

                        LinkedToParentDescriptionButtonEdit.Properties.ReadOnly = true;
                        LinkedToParentDescriptionButtonEdit.Properties.Buttons[0].Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        LinkedToParentDescriptionButtonEdit.Focus();

                        LinkedToParentDescriptionButtonEdit.Properties.ReadOnly = false;
                        LinkedToParentDescriptionButtonEdit.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        LinkedToParentDescriptionButtonEdit.Focus();

                        LinkedToParentDescriptionButtonEdit.Properties.ReadOnly = false;
                        LinkedToParentDescriptionButtonEdit.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Viewing";
                        barStaticItemFormMode.ImageIndex = 4;  // View //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();
            SetEditorButtons();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }
            ibool_ignoreValidation = false;
            ibool_FormStillLoading = false;
        }

        private void SetEditorButtons()
        {
            // Get Form Permissions //
        }
        
        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_OM_Job.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            int intID = 0;  // Check if the current record has been saved at some point //
            DataRowView currentRow = (DataRowView)sp06213OMJobWasteEditBindingSource.Current;
            if (currentRow != null)
            {
                intID = (currentRow["WasteID"] == null ? 0 : Convert.ToInt32(currentRow["WasteID"]));
            }
            bbiLinkedDocuments.Enabled = intID > 0;  // Set status of button //
            
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Saving Changes...");

            this.sp06213OMJobWasteEditBindingSource.EndEdit();
            try
            {
                sp06213_OM_Job_Waste_EditTableAdapter.Update(dataSet_OM_Job);  // Insert and Update queries defined in Table Adapter //
            }
            catch (Exception ex)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                var currentRowView = (DataRowView)sp06213OMJobWasteEditBindingSource.Current;
                var currentRow = (DataSet_OM_Job.sp06213_OM_Job_Waste_EditRow)currentRowView.Row;
                this.strFormMode = "edit";  // Switch mode to Edit so than any subsequent changes update this record //
                if (currentRow != null)
                {
                    currentRow.strMode = "edit";
                    currentRow.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                    strNewIDs = currentRow.WasteID + ";";
                }
            }
            else if (this.strFormMode.ToLower() == "blockadd")
            {
                var currentRowView = (DataRowView)sp06213OMJobWasteEditBindingSource.Current;
                var currentRow = (DataSet_OM_Job.sp06213_OM_Job_Waste_EditRow)currentRowView.Row;
                this.strFormMode = "blockedit";  // Switch mode to BlockEdit so than any subsequent changes update these record //
                if (currentRow != null)
                {
                    currentRow.strMode = "blockedit";
                    currentRow.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                    strNewIDs = Convert.ToString(currentRow["strRecordIDs"]);  // Values returned from Update SP //
                }
            }

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (_LinkedToRecordTypeID == 0)
                    {
                        if (frmChild.Name == "frm_OM_Visit_Manager")
                        {
                            var fParentForm = (frm_OM_Visit_Manager)frmChild;
                            fParentForm.UpdateFormRefreshStatus(3, Utils.enmFocusedGrid.Waste, strNewIDs);
                        }
                        /*else if (frmChild.Name == "frm_OM_Visit_Edit")
                       {
                           var fParentForm = (frm_OM_Visit_Edit)frmChild;
                           fParentForm.UpdateFormRefreshStatus(1, Utils.enmFocusedGrid.Materials, strNewIDs);
                       }*/
                    }
                    else if (_LinkedToRecordTypeID == 1)
                    {
                        if (frmChild.Name == "frm_OM_Job_Manager")
                        {
                            var fParentForm = (frm_OM_Job_Manager)frmChild;
                            fParentForm.UpdateFormRefreshStatus(2, Utils.enmFocusedGrid.Waste, strNewIDs);
                        }
                        /*else if (frmChild.Name == "frm_OM_Job_Edit")
                       {
                           var fParentForm = (frm_OM_Job_Edit)frmChild;
                           fParentForm.UpdateFormRefreshStatus(1, Utils.enmFocusedGrid.Materials, strNewIDs);
                       }*/
                        else if (frmChild.Name == "frm_OM_Visit_Manager")
                        {
                            var fParentForm = (frm_OM_Visit_Manager)frmChild;
                            fParentForm.UpdateFormRefreshStatus(57, Utils.enmFocusedGrid.WasteJob, strNewIDs);
                        }
                    }
                }
            }

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < this.dataSet_OM_Job.sp06213_OM_Job_Waste_Edit.Rows.Count; i++)
            {
                switch (this.dataSet_OM_Job.sp06213_OM_Job_Waste_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }

        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void LinkedToParentDescriptionButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view" || strFormMode == "blockadd") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var currentRowView = (DataRowView)sp06213OMJobWasteEditBindingSource.Current;
                var currentRow = (DataSet_OM_Job.sp06213_OM_Job_Waste_EditRow)currentRowView.Row;
                if (currentRow == null) return;

                int intClientContractID = 0;
                try { intClientContractID = (string.IsNullOrWhiteSpace(currentRow.ClientContractID.ToString()) ? 0 : Convert.ToInt32(currentRow.ClientContractID)); }
                catch (Exception) { }

                int intSiteContractID = 0;
                try { intSiteContractID = (string.IsNullOrWhiteSpace(currentRow.SiteContractID.ToString()) ? 0 : Convert.ToInt32(currentRow.SiteContractID)); }
                catch (Exception) { }

                int intVisitID = 0;
                try { intVisitID = (string.IsNullOrWhiteSpace(currentRow.VisitID.ToString()) ? 0 : Convert.ToInt32(currentRow.VisitID)); }
                catch (Exception) { }

                int intJobID = 0;
                try { intJobID = (string.IsNullOrWhiteSpace(currentRow.LinkedToRecordID.ToString()) ? 0 : Convert.ToInt32(currentRow.LinkedToRecordID)); }
                catch (Exception) { }

                int intLinkedToRecordTypeID = 0;
                try { intLinkedToRecordTypeID = (string.IsNullOrWhiteSpace(currentRow.LinkedToRecordTypeID.ToString()) ? 0 : Convert.ToInt32(currentRow.LinkedToRecordTypeID)); }
                catch (Exception) { }

                if (intLinkedToRecordTypeID == 0)  // Visit //
                {
                    var fChildForm = new frm_OM_Select_Visit();
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.intOriginalClientContractID = intClientContractID;
                    fChildForm.intOriginalSiteContractID = intSiteContractID;
                    fChildForm.intOriginalVisitID = intVisitID;
                    if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                    {
                        currentRow.ClientID = fChildForm.intSelectedClientID;
                        currentRow.ClientContractID = fChildForm.intSelectedClientContractID;
                        currentRow.SiteID = fChildForm.intSelectedSiteID;
                        currentRow.SiteContractID = fChildForm.intSelectedSiteContractID;
                        currentRow.VisitID = fChildForm.intSelectedVisitID;
                        currentRow.VisitNumber = fChildForm.intSelectedVisitNumber;
                        currentRow.LinkedToRecordID = fChildForm.intSelectedVisitID;
                        currentRow.ClientName = fChildForm.strSelectedClientName;
                        currentRow.ContractDescription = fChildForm.strSelectedContractDescription;
                        currentRow.SiteName = fChildForm.strSelectedSiteName;
                        currentRow.LinkedToParentDescription = fChildForm.intSelectedVisitNumber.ToString();
                        sp06213OMJobWasteEditBindingSource.EndEdit();
                    }
                }
                else  // Job //
                {
                    var fChildForm = new frm_OM_Select_Job();
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.intOriginalClientContractID = intClientContractID;
                    fChildForm.intOriginalSiteContractID = intSiteContractID;
                    fChildForm.intOriginalVisitID = intVisitID;
                    fChildForm.intOriginalJobID = intJobID;
                    if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                    {
                        currentRow.ClientID = fChildForm.intSelectedClientID;
                        currentRow.ClientContractID = fChildForm.intSelectedClientContractID;
                        currentRow.SiteID = fChildForm.intSelectedSiteID;
                        currentRow.SiteContractID = fChildForm.intSelectedSiteContractID;
                        currentRow.VisitID = fChildForm.intSelectedVisitID;
                        currentRow.VisitNumber = fChildForm.intSelectedVisitNumber;
                        currentRow.LinkedToRecordID = fChildForm.intSelectedJobID;
                        currentRow.JobTypeID = fChildForm.intSelectedJobTypeID;
                        currentRow.JobSubTypeID = fChildForm.intSelectedJobSubTypeID;
                        currentRow.ClientName = fChildForm.strSelectedClientName;
                        currentRow.ContractDescription = fChildForm.strSelectedContractDescription;
                        currentRow.SiteName = fChildForm.strSelectedSiteName;
                        currentRow.JobTypeDescription = fChildForm.strSelectedJobTypeDescription;
                        currentRow.JobSubTypeDescription = fChildForm.strSelectedJobSubTypeDescription;
                        currentRow.LinkedToParentDescription = fChildForm.strSelectedJobTypeDescription + " - " + fChildForm.strSelectedJobSubTypeDescription;
                        currentRow.ExpectedStartDate = fChildForm.dtSelectedJobExpectedStartDate;
                        sp06213OMJobWasteEditBindingSource.EndEdit();
                    }
                }
            }
        }
        private void LinkedToParentDescriptionButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if ((this.strFormMode == "add" || this.strFormMode == "edit") && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(LinkedToParentDescriptionButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(LinkedToParentDescriptionButtonEdit, "");
            }
        }

        private void WasteTypeButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var currentRowView = (DataRowView)sp06213OMJobWasteEditBindingSource.Current;
                var currentRow = (DataSet_OM_Job.sp06213_OM_Job_Waste_EditRow)currentRowView.Row;
                if (currentRow == null) return;

                int intWasteTypeID = 0;
                try { intWasteTypeID = (string.IsNullOrWhiteSpace(currentRow.WasteTypeID.ToString()) ? 0 : Convert.ToInt32(currentRow.WasteTypeID)); }
                catch (Exception) { }
                
                var fChildForm = new frm_OM_Select_Waste_Type();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalID = intWasteTypeID;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow.WasteTypeID = fChildForm.intSelectedID;
                    currentRow.WasteType = fChildForm.strSelectedDescription1;
                    sp06213OMJobWasteEditBindingSource.EndEdit();
                }
            }
        }
        private void WasteTypeButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if ((this.strFormMode == "add" || this.strFormMode == "blockadd" || this.strFormMode == "edit") && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(WasteTypeButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(WasteTypeButtonEdit, "");
            }
        }

        private void WasteDisposalCentreNameButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var currentRowView = (DataRowView)sp06213OMJobWasteEditBindingSource.Current;
                var currentRow = (DataSet_OM_Job.sp06213_OM_Job_Waste_EditRow)currentRowView.Row;
                if (currentRow == null) return;

                int intWasteDisposalCentreID = 0;
                try { intWasteDisposalCentreID = (string.IsNullOrWhiteSpace(currentRow.WasteDisposalCentreID.ToString()) ? 0 : Convert.ToInt32(currentRow.WasteDisposalCentreID)); }
                catch (Exception) { }
                
                string strWasteTypeIDs = "";
                try { strWasteTypeIDs = (string.IsNullOrWhiteSpace(currentRow.WasteTypeID.ToString()) ? "" : currentRow.WasteTypeID.ToString() + ","); }
                catch (Exception) { }

                var fChildForm = new frm_OM_Select_Waste_Disposal_Centre();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalID = intWasteDisposalCentreID;
                fChildForm.strPassedInWasteTypeIDs = strWasteTypeIDs;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow.WasteDisposalCentreID = fChildForm.intSelectedID;
                    currentRow.WasteDisposalCentreName = fChildForm.strSelectedDescription1;
                    sp06213OMJobWasteEditBindingSource.EndEdit();
                }
            }
        }
        private void WasteDisposalCentreNameButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if ((this.strFormMode == "add" || this.strFormMode == "blockadd" || this.strFormMode == "edit") && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(WasteDisposalCentreNameButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(WasteDisposalCentreNameButtonEdit, "");
            }
        }

        private void WasteAmountSpinEdit_Validating(object sender, CancelEventArgs e)
        {
            SpinEdit se = (SpinEdit)sender;
            if ((this.strFormMode == "add" || this.strFormMode == "edit") && (string.IsNullOrEmpty(se.EditValue.ToString()) || se.EditValue.ToString() == "0"))
            {
                dxErrorProvider1.SetError(WasteAmountSpinEdit, "Enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(WasteAmountSpinEdit, "");
            }
        }

        private void WasteAmountUnitDescriptorIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if ((this.strFormMode == "add" || this.strFormMode == "edit") && (string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "0"))
            {
                dxErrorProvider1.SetError(WasteAmountUnitDescriptorIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(WasteAmountUnitDescriptorIDGridLookUpEdit, "");
            }
        }

        private void WasteDocumentPathButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view" || strFormMode == "blockadd") return;
            var currentRowView = (DataRowView)sp06213OMJobWasteEditBindingSource.Current;
            var currentRow = (DataSet_OM_Job.sp06213_OM_Job_Waste_EditRow)currentRowView.Row;
            if (currentRow == null) return;
            switch (e.Button.Tag.ToString())
            {
                case "select file":
                    {
                        int intWasteDocumentID = 0;
                        try { intWasteDocumentID = (string.IsNullOrWhiteSpace(currentRow.WasteDocumentID.ToString()) ? 0 : Convert.ToInt32(currentRow.WasteDocumentID)); }
                        catch (Exception) { }

                        var fChildForm = new frm_OM_Select_Waste_Document();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.intOriginalSelectedID = intWasteDocumentID;
                        fChildForm.i_dt_FromDate = DateTime.Today.AddMonths(-3);
                        fChildForm.i_dt_ToDate = DateTime.Today.AddDays(1);
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                        {
                            currentRow.WasteDocumentID = fChildForm.intSelectedID;
                            currentRow.WasteDocumentPath = fChildForm.strSelectedWasteDocumentPath;
                            currentRow.WasteDocumentPathExtension = fChildForm.strSelectedWasteDocumentExtension;
                            sp06213OMJobWasteEditBindingSource.EndEdit();
                        }
                    }
                    break;
                case "view file":
                    {
                        if (currentRow == null) return;
                        string strFile = currentRow["WasteDocumentPath"].ToString();
                        string strExtension = currentRow["WasteDocumentPathExtension"].ToString();

                        if (string.IsNullOrEmpty(strFile))
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Link the File to be Viewed before proceeding.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        try
                        {
                            if (strExtension.ToLower() != "pdf")
                            {
                                System.Diagnostics.Process.Start(strFile);
                            }
                            else
                            {
                                if (!BaseObjects.Keyboard.IsKeyDown(Keys.A))  // Only open in DevExpress PDF viewer if user is not holding down A key (CTRL Key doesn't work when fired from a grid as it's used for row selection) //
                                {
                                    frm_Core_PDF_Viewer fChildForm = new frm_Core_PDF_Viewer();
                                    fChildForm.strPDFFile = strFile;
                                    fChildForm.MdiParent = this.MdiParent;
                                    fChildForm.GlobalSettings = this.GlobalSettings;
                                    fChildForm.Show();
                                }
                                else
                                {
                                    System.Diagnostics.Process.Start(strFile);
                                }
                            }
                        }
                        catch
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view file: " + strFile + ".\n\nThe file may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                    break;
            }
        }

        #endregion


        private void bbiLinkedDocuments_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (strFormMode == "blockedit" || strFormMode == "blockadd") return;
            DataRowView currentRow = (DataRowView)sp06213OMJobWasteEditBindingSource.Current;
            if (currentRow == null) return;
            int intRecordID = (string.IsNullOrEmpty(currentRow["WasteID"].ToString()) ? 0 : Convert.ToInt32(currentRow["WasteID"]));
            if (intRecordID <= 0) return;

            // Drilldown to Linked Document Manager //
            int intRecordType = 29;  // Waste //
            string strRecordDescription = currentRow["ClientName"].ToString() + ", Contract: " + currentRow["ContractDescription"].ToString() + ", Site: " + currentRow["SiteName"].ToString() + ", Visit #: " + currentRow["VisitNumber"].ToString() + ", Waste: " + currentRow["WasteType"].ToString();
            Linked_Document_Drill_Down(intRecordType, _LinkedToRecordSubTypeID, intRecordID, strRecordDescription);
        }

        private void Linked_Document_Drill_Down(int intRecordType, int intRecordSubType, int intRecordID, string strRecordDescription)
        {
            // Drilldown to Linked Document Manager //
            string strRecordIDs = "";
            DataSet_GC_Summer_CoreTableAdapters.QueriesTableAdapter GetSetting = new DataSet_GC_Summer_CoreTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            try
            {
                strRecordIDs = GetSetting.sp06049_OM_Linked_Docs_Drilldown_Get_IDs(intRecordID, intRecordType, intRecordSubType).ToString();
            }
            catch (Exception) { }
            if (string.IsNullOrWhiteSpace(strRecordIDs)) strRecordIDs = "-1,";  // Make sure the form starts in drilldown mode [use a dummy id] //
            Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "om_linked_documents", intRecordType, intRecordSubType, intRecordID, strRecordDescription);

        }







    }
}


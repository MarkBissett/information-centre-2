namespace WoodPlan5
{
    partial class frm_OM_Job_Labour_Reassign
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Job_Labour_Reassign));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            this.gridControlJob = new DevExpress.XtraGrid.GridControl();
            this.sp06269OMJobLabourReassignAvailableJobsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Job = new WoodPlan5.DataSet_OM_Job();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridViewJob = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colJobID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReactive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colJobNoLongerRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRequiresAccessPermit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccessPermitID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPOID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colExpectedEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedDurationUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNumeric2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colExpectedDurationUnitsDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScheduleSentDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualDurationUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualDurationUnitsDescriptionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotPayContractor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotInvoiceClient = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colJobStatusDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedDurationUnitsDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualDurationUnitsDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelected = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFullDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaximumDaysFromLastVisit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinimumDaysFromLastVisit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDaysLeeway = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSitePostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToVisit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientAndContract = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRework = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReworkOriginalJobID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedLabourCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.repositoryItemCheckEditShowActiveEmployeesOnly = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemPopupContainerEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControlLabour = new DevExpress.XtraGrid.GridControl();
            this.sp06270OMJobLabourReassignLinkedLabourBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewLabour = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLabourUsedID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostUnitsUsed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNumeric2DP2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCostUnitDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellUnitsUsed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellUnitDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostPerUnitExVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCostPerUnitVatRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSellPerUnitExVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellPerUnitVatRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostValueExVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostValueVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostValueTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellValueExVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellValueVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellValueTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubTypeDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedStartDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colVisitNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelected1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colCostUnitDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellUnitDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLabourType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCISPercentage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCISValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPurchaseOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEndDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPDA_JobID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditChangeTeam = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.sp06269_OM_Job_Labour_Reassign_Available_JobsTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06269_OM_Job_Labour_Reassign_Available_JobsTableAdapter();
            this.sp06270_OM_Job_Labour_Reassign_Linked_LabourTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06270_OM_Job_Labour_Reassign_Linked_LabourTableAdapter();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bbiPivotLayout = new DevExpress.XtraBars.BarButtonItem();
            this.bbiChangeTeam = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlJob)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06269OMJobLabourReassignAvailableJobsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Job)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewJob)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric2DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditShowActiveEmployeesOnly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlLabour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06270OMJobLabourReassignLinkedLabourBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLabour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric2DP2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditChangeTeam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(990, 42);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 527);
            this.barDockControlBottom.Size = new System.Drawing.Size(990, 30);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 42);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 485);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(990, 42);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 485);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar2});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barStaticItemInformation,
            this.bbiRefresh,
            this.bbiPivotLayout,
            this.bbiChangeTeam});
            this.barManager1.MaxItemId = 37;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEdit2,
            this.repositoryItemCheckEditShowActiveEmployeesOnly});
            this.barManager1.StatusBar = this.bar1;
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // gridControlJob
            // 
            this.gridControlJob.DataSource = this.sp06269OMJobLabourReassignAvailableJobsBindingSource;
            this.gridControlJob.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlJob.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlJob.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlJob.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlJob.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlJob.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlJob.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlJob.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlJob.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlJob.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlJob.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlJob.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlJob.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "Reload Data", "reload")});
            this.gridControlJob.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlJob_EmbeddedNavigator_ButtonClick);
            this.gridControlJob.Location = new System.Drawing.Point(0, 0);
            this.gridControlJob.MainView = this.gridViewJob;
            this.gridControlJob.MenuManager = this.barManager1;
            this.gridControlJob.Name = "gridControlJob";
            this.gridControlJob.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemTextEditNumeric2DP});
            this.gridControlJob.Size = new System.Drawing.Size(488, 459);
            this.gridControlJob.TabIndex = 0;
            this.gridControlJob.UseEmbeddedNavigator = true;
            this.gridControlJob.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewJob});
            // 
            // sp06269OMJobLabourReassignAvailableJobsBindingSource
            // 
            this.sp06269OMJobLabourReassignAvailableJobsBindingSource.DataMember = "sp06269_OM_Job_Labour_Reassign_Available_Jobs";
            this.sp06269OMJobLabourReassignAvailableJobsBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // dataSet_OM_Job
            // 
            this.dataSet_OM_Job.DataSetName = "DataSet_OM_Job";
            this.dataSet_OM_Job.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "Preview_16x16.png");
            this.imageCollection1.InsertGalleryImage("refresh_16x16.png", "images/actions/refresh_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/refresh_16x16.png"), 4);
            this.imageCollection1.Images.SetKeyName(4, "refresh_16x16.png");
            // 
            // gridViewJob
            // 
            this.gridViewJob.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colJobID,
            this.colVisitID,
            this.colVisitNumber,
            this.colJobStatusID,
            this.colJobSubTypeID,
            this.colReactive,
            this.colJobNoLongerRequired,
            this.colRequiresAccessPermit,
            this.colAccessPermitID,
            this.colClientPONumber,
            this.colClientPOID,
            this.colExpectedStartDate,
            this.colExpectedEndDate,
            this.colExpectedDurationUnits,
            this.colExpectedDurationUnitsDescriptorID,
            this.colScheduleSentDate,
            this.colActualStartDate,
            this.colActualEndDate,
            this.colActualDurationUnits,
            this.colActualDurationUnitsDescriptionID,
            this.colDoNotPayContractor,
            this.colDoNotInvoiceClient,
            this.colRemarks1,
            this.colJobStatusDescription,
            this.colSiteName,
            this.colClientName,
            this.colCreatedByStaffName,
            this.colExpectedDurationUnitsDescriptor,
            this.colActualDurationUnitsDescriptor,
            this.colJobTypeID,
            this.colJobSubTypeDescription,
            this.colJobTypeDescription,
            this.colRecordType,
            this.colSelected,
            this.colFullDescription,
            this.colMaximumDaysFromLastVisit,
            this.colMinimumDaysFromLastVisit,
            this.colDaysLeeway,
            this.colContractDescription,
            this.colClientID,
            this.colClientContractID,
            this.colSiteID,
            this.colSiteContractID,
            this.colSitePostcode,
            this.colLocationX,
            this.colLocationY,
            this.colLinkedToVisit,
            this.colClientAndContract,
            this.colRework,
            this.colReworkOriginalJobID,
            this.colLinkedLabourCount});
            this.gridViewJob.GridControl = this.gridControlJob;
            this.gridViewJob.Name = "gridViewJob";
            this.gridViewJob.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridViewJob.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewJob.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewJob.OptionsLayout.StoreAppearance = true;
            this.gridViewJob.OptionsLayout.StoreFormatRules = true;
            this.gridViewJob.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewJob.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewJob.OptionsSelection.MultiSelect = true;
            this.gridViewJob.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewJob.OptionsView.ColumnAutoWidth = false;
            this.gridViewJob.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewJob.OptionsView.ShowGroupPanel = false;
            this.gridViewJob.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colVisitNumber, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colJobTypeDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colJobSubTypeDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewJob.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridViewJob_CustomDrawCell);
            this.gridViewJob.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewJob_CustomRowCellEdit);
            this.gridViewJob.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewJob.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewJob.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewJob.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewJob_ShowingEditor);
            this.gridViewJob.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewJob.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewJob.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewJob_MouseUp);
            this.gridViewJob.DoubleClick += new System.EventHandler(this.gridViewJob_DoubleClick);
            this.gridViewJob.GotFocus += new System.EventHandler(this.gridViewJob_GotFocus);
            // 
            // colJobID
            // 
            this.colJobID.Caption = "Job ID";
            this.colJobID.FieldName = "JobID";
            this.colJobID.Name = "colJobID";
            this.colJobID.OptionsColumn.AllowEdit = false;
            this.colJobID.OptionsColumn.AllowFocus = false;
            this.colJobID.OptionsColumn.ReadOnly = true;
            // 
            // colVisitID
            // 
            this.colVisitID.Caption = "Visit ID";
            this.colVisitID.FieldName = "VisitID";
            this.colVisitID.Name = "colVisitID";
            this.colVisitID.OptionsColumn.AllowEdit = false;
            this.colVisitID.OptionsColumn.AllowFocus = false;
            this.colVisitID.OptionsColumn.ReadOnly = true;
            // 
            // colVisitNumber
            // 
            this.colVisitNumber.Caption = "Visit #";
            this.colVisitNumber.FieldName = "VisitNumber";
            this.colVisitNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colVisitNumber.Name = "colVisitNumber";
            this.colVisitNumber.OptionsColumn.AllowEdit = false;
            this.colVisitNumber.OptionsColumn.AllowFocus = false;
            this.colVisitNumber.OptionsColumn.ReadOnly = true;
            this.colVisitNumber.Visible = true;
            this.colVisitNumber.VisibleIndex = 2;
            this.colVisitNumber.Width = 64;
            // 
            // colJobStatusID
            // 
            this.colJobStatusID.Caption = "Job Status ID";
            this.colJobStatusID.FieldName = "JobStatusID";
            this.colJobStatusID.Name = "colJobStatusID";
            this.colJobStatusID.OptionsColumn.AllowEdit = false;
            this.colJobStatusID.OptionsColumn.AllowFocus = false;
            this.colJobStatusID.OptionsColumn.ReadOnly = true;
            this.colJobStatusID.Width = 86;
            // 
            // colJobSubTypeID
            // 
            this.colJobSubTypeID.Caption = "Sub-Type ID";
            this.colJobSubTypeID.FieldName = "JobSubTypeID";
            this.colJobSubTypeID.Name = "colJobSubTypeID";
            this.colJobSubTypeID.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeID.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeID.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeID.Width = 81;
            // 
            // colReactive
            // 
            this.colReactive.Caption = "Reactive";
            this.colReactive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colReactive.FieldName = "Reactive";
            this.colReactive.Name = "colReactive";
            this.colReactive.OptionsColumn.AllowEdit = false;
            this.colReactive.OptionsColumn.AllowFocus = false;
            this.colReactive.OptionsColumn.ReadOnly = true;
            this.colReactive.Visible = true;
            this.colReactive.VisibleIndex = 15;
            this.colReactive.Width = 63;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colJobNoLongerRequired
            // 
            this.colJobNoLongerRequired.Caption = "No Longer Required";
            this.colJobNoLongerRequired.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colJobNoLongerRequired.FieldName = "JobNoLongerRequired";
            this.colJobNoLongerRequired.Name = "colJobNoLongerRequired";
            this.colJobNoLongerRequired.OptionsColumn.AllowEdit = false;
            this.colJobNoLongerRequired.OptionsColumn.AllowFocus = false;
            this.colJobNoLongerRequired.OptionsColumn.ReadOnly = true;
            this.colJobNoLongerRequired.Visible = true;
            this.colJobNoLongerRequired.VisibleIndex = 18;
            this.colJobNoLongerRequired.Width = 116;
            // 
            // colRequiresAccessPermit
            // 
            this.colRequiresAccessPermit.Caption = "Access Permit Required";
            this.colRequiresAccessPermit.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colRequiresAccessPermit.FieldName = "RequiresAccessPermit";
            this.colRequiresAccessPermit.Name = "colRequiresAccessPermit";
            this.colRequiresAccessPermit.OptionsColumn.AllowEdit = false;
            this.colRequiresAccessPermit.OptionsColumn.AllowFocus = false;
            this.colRequiresAccessPermit.OptionsColumn.ReadOnly = true;
            this.colRequiresAccessPermit.Visible = true;
            this.colRequiresAccessPermit.VisibleIndex = 17;
            this.colRequiresAccessPermit.Width = 133;
            // 
            // colAccessPermitID
            // 
            this.colAccessPermitID.Caption = "Access Permit ID";
            this.colAccessPermitID.FieldName = "AccessPermitID";
            this.colAccessPermitID.Name = "colAccessPermitID";
            this.colAccessPermitID.OptionsColumn.AllowEdit = false;
            this.colAccessPermitID.OptionsColumn.AllowFocus = false;
            this.colAccessPermitID.OptionsColumn.ReadOnly = true;
            this.colAccessPermitID.Width = 101;
            // 
            // colClientPONumber
            // 
            this.colClientPONumber.Caption = "Client PO #";
            this.colClientPONumber.FieldName = "ClientPONumber";
            this.colClientPONumber.Name = "colClientPONumber";
            this.colClientPONumber.OptionsColumn.AllowEdit = false;
            this.colClientPONumber.OptionsColumn.AllowFocus = false;
            this.colClientPONumber.OptionsColumn.ReadOnly = true;
            this.colClientPONumber.Visible = true;
            this.colClientPONumber.VisibleIndex = 19;
            this.colClientPONumber.Width = 76;
            // 
            // colClientPOID
            // 
            this.colClientPOID.Caption = "Client PO ID";
            this.colClientPOID.FieldName = "ClientPOID";
            this.colClientPOID.Name = "colClientPOID";
            this.colClientPOID.OptionsColumn.AllowEdit = false;
            this.colClientPOID.OptionsColumn.AllowFocus = false;
            this.colClientPOID.OptionsColumn.ReadOnly = true;
            this.colClientPOID.Width = 79;
            // 
            // colExpectedStartDate
            // 
            this.colExpectedStartDate.Caption = "Expected Start";
            this.colExpectedStartDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colExpectedStartDate.FieldName = "ExpectedStartDate";
            this.colExpectedStartDate.Name = "colExpectedStartDate";
            this.colExpectedStartDate.OptionsColumn.AllowEdit = false;
            this.colExpectedStartDate.OptionsColumn.AllowFocus = false;
            this.colExpectedStartDate.OptionsColumn.ReadOnly = true;
            this.colExpectedStartDate.Visible = true;
            this.colExpectedStartDate.VisibleIndex = 7;
            this.colExpectedStartDate.Width = 100;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colExpectedEndDate
            // 
            this.colExpectedEndDate.Caption = "Expected End";
            this.colExpectedEndDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colExpectedEndDate.FieldName = "ExpectedEndDate";
            this.colExpectedEndDate.Name = "colExpectedEndDate";
            this.colExpectedEndDate.OptionsColumn.AllowEdit = false;
            this.colExpectedEndDate.OptionsColumn.AllowFocus = false;
            this.colExpectedEndDate.OptionsColumn.ReadOnly = true;
            this.colExpectedEndDate.Visible = true;
            this.colExpectedEndDate.VisibleIndex = 8;
            this.colExpectedEndDate.Width = 100;
            // 
            // colExpectedDurationUnits
            // 
            this.colExpectedDurationUnits.Caption = "Expected Duration";
            this.colExpectedDurationUnits.ColumnEdit = this.repositoryItemTextEditNumeric2DP;
            this.colExpectedDurationUnits.FieldName = "ExpectedDurationUnits";
            this.colExpectedDurationUnits.Name = "colExpectedDurationUnits";
            this.colExpectedDurationUnits.OptionsColumn.AllowEdit = false;
            this.colExpectedDurationUnits.OptionsColumn.AllowFocus = false;
            this.colExpectedDurationUnits.OptionsColumn.ReadOnly = true;
            this.colExpectedDurationUnits.Visible = true;
            this.colExpectedDurationUnits.VisibleIndex = 9;
            this.colExpectedDurationUnits.Width = 110;
            // 
            // repositoryItemTextEditNumeric2DP
            // 
            this.repositoryItemTextEditNumeric2DP.AutoHeight = false;
            this.repositoryItemTextEditNumeric2DP.Mask.EditMask = "f2";
            this.repositoryItemTextEditNumeric2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumeric2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumeric2DP.Name = "repositoryItemTextEditNumeric2DP";
            // 
            // colExpectedDurationUnitsDescriptorID
            // 
            this.colExpectedDurationUnitsDescriptorID.Caption = "Expected Duration Decriptor ID";
            this.colExpectedDurationUnitsDescriptorID.FieldName = "ExpectedDurationUnitsDescriptorID";
            this.colExpectedDurationUnitsDescriptorID.Name = "colExpectedDurationUnitsDescriptorID";
            this.colExpectedDurationUnitsDescriptorID.OptionsColumn.AllowEdit = false;
            this.colExpectedDurationUnitsDescriptorID.OptionsColumn.AllowFocus = false;
            this.colExpectedDurationUnitsDescriptorID.OptionsColumn.ReadOnly = true;
            this.colExpectedDurationUnitsDescriptorID.Width = 171;
            // 
            // colScheduleSentDate
            // 
            this.colScheduleSentDate.Caption = "Schedule Sent";
            this.colScheduleSentDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colScheduleSentDate.FieldName = "ScheduleSentDate";
            this.colScheduleSentDate.Name = "colScheduleSentDate";
            this.colScheduleSentDate.OptionsColumn.AllowEdit = false;
            this.colScheduleSentDate.OptionsColumn.AllowFocus = false;
            this.colScheduleSentDate.OptionsColumn.ReadOnly = true;
            this.colScheduleSentDate.Visible = true;
            this.colScheduleSentDate.VisibleIndex = 20;
            this.colScheduleSentDate.Width = 89;
            // 
            // colActualStartDate
            // 
            this.colActualStartDate.Caption = "Actual Start";
            this.colActualStartDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colActualStartDate.FieldName = "ActualStartDate";
            this.colActualStartDate.Name = "colActualStartDate";
            this.colActualStartDate.OptionsColumn.AllowEdit = false;
            this.colActualStartDate.OptionsColumn.AllowFocus = false;
            this.colActualStartDate.OptionsColumn.ReadOnly = true;
            this.colActualStartDate.Visible = true;
            this.colActualStartDate.VisibleIndex = 11;
            this.colActualStartDate.Width = 100;
            // 
            // colActualEndDate
            // 
            this.colActualEndDate.Caption = "Actual End";
            this.colActualEndDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colActualEndDate.FieldName = "ActualEndDate";
            this.colActualEndDate.Name = "colActualEndDate";
            this.colActualEndDate.OptionsColumn.AllowEdit = false;
            this.colActualEndDate.OptionsColumn.AllowFocus = false;
            this.colActualEndDate.OptionsColumn.ReadOnly = true;
            this.colActualEndDate.Visible = true;
            this.colActualEndDate.VisibleIndex = 12;
            this.colActualEndDate.Width = 100;
            // 
            // colActualDurationUnits
            // 
            this.colActualDurationUnits.Caption = "Actual Duration";
            this.colActualDurationUnits.ColumnEdit = this.repositoryItemTextEditNumeric2DP;
            this.colActualDurationUnits.FieldName = "ActualDurationUnits";
            this.colActualDurationUnits.Name = "colActualDurationUnits";
            this.colActualDurationUnits.OptionsColumn.AllowEdit = false;
            this.colActualDurationUnits.OptionsColumn.AllowFocus = false;
            this.colActualDurationUnits.OptionsColumn.ReadOnly = true;
            this.colActualDurationUnits.Visible = true;
            this.colActualDurationUnits.VisibleIndex = 13;
            this.colActualDurationUnits.Width = 95;
            // 
            // colActualDurationUnitsDescriptionID
            // 
            this.colActualDurationUnitsDescriptionID.Caption = "Actual Duration Descriptor ID";
            this.colActualDurationUnitsDescriptionID.FieldName = "ActualDurationUnitsDescriptionID";
            this.colActualDurationUnitsDescriptionID.Name = "colActualDurationUnitsDescriptionID";
            this.colActualDurationUnitsDescriptionID.OptionsColumn.AllowEdit = false;
            this.colActualDurationUnitsDescriptionID.OptionsColumn.AllowFocus = false;
            this.colActualDurationUnitsDescriptionID.OptionsColumn.ReadOnly = true;
            this.colActualDurationUnitsDescriptionID.Width = 161;
            // 
            // colDoNotPayContractor
            // 
            this.colDoNotPayContractor.Caption = "Do Not Pay Team";
            this.colDoNotPayContractor.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDoNotPayContractor.FieldName = "DoNotPayContractor";
            this.colDoNotPayContractor.Name = "colDoNotPayContractor";
            this.colDoNotPayContractor.OptionsColumn.AllowEdit = false;
            this.colDoNotPayContractor.OptionsColumn.AllowFocus = false;
            this.colDoNotPayContractor.OptionsColumn.ReadOnly = true;
            this.colDoNotPayContractor.Width = 104;
            // 
            // colDoNotInvoiceClient
            // 
            this.colDoNotInvoiceClient.Caption = "Do Not Invoice Client";
            this.colDoNotInvoiceClient.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDoNotInvoiceClient.FieldName = "DoNotInvoiceClient";
            this.colDoNotInvoiceClient.Name = "colDoNotInvoiceClient";
            this.colDoNotInvoiceClient.OptionsColumn.AllowEdit = false;
            this.colDoNotInvoiceClient.OptionsColumn.AllowFocus = false;
            this.colDoNotInvoiceClient.OptionsColumn.ReadOnly = true;
            this.colDoNotInvoiceClient.Width = 122;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 25;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colJobStatusDescription
            // 
            this.colJobStatusDescription.Caption = "Job Status";
            this.colJobStatusDescription.FieldName = "JobStatusDescription";
            this.colJobStatusDescription.Name = "colJobStatusDescription";
            this.colJobStatusDescription.OptionsColumn.AllowEdit = false;
            this.colJobStatusDescription.OptionsColumn.AllowFocus = false;
            this.colJobStatusDescription.OptionsColumn.ReadOnly = true;
            this.colJobStatusDescription.Visible = true;
            this.colJobStatusDescription.VisibleIndex = 6;
            this.colJobStatusDescription.Width = 103;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.Visible = true;
            this.colSiteName.VisibleIndex = 1;
            this.colSiteName.Width = 107;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 0;
            this.colClientName.Width = 103;
            // 
            // colCreatedByStaffName
            // 
            this.colCreatedByStaffName.Caption = "Created By";
            this.colCreatedByStaffName.FieldName = "CreatedByStaffName";
            this.colCreatedByStaffName.Name = "colCreatedByStaffName";
            this.colCreatedByStaffName.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffName.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffName.OptionsColumn.ReadOnly = true;
            // 
            // colExpectedDurationUnitsDescriptor
            // 
            this.colExpectedDurationUnitsDescriptor.Caption = "Expected Duration Descriptor";
            this.colExpectedDurationUnitsDescriptor.FieldName = "ExpectedDurationUnitsDescriptor";
            this.colExpectedDurationUnitsDescriptor.Name = "colExpectedDurationUnitsDescriptor";
            this.colExpectedDurationUnitsDescriptor.OptionsColumn.AllowEdit = false;
            this.colExpectedDurationUnitsDescriptor.OptionsColumn.AllowFocus = false;
            this.colExpectedDurationUnitsDescriptor.OptionsColumn.ReadOnly = true;
            this.colExpectedDurationUnitsDescriptor.Visible = true;
            this.colExpectedDurationUnitsDescriptor.VisibleIndex = 10;
            this.colExpectedDurationUnitsDescriptor.Width = 162;
            // 
            // colActualDurationUnitsDescriptor
            // 
            this.colActualDurationUnitsDescriptor.Caption = "Actual Duration Descriptor";
            this.colActualDurationUnitsDescriptor.FieldName = "ActualDurationUnitsDescriptor";
            this.colActualDurationUnitsDescriptor.Name = "colActualDurationUnitsDescriptor";
            this.colActualDurationUnitsDescriptor.OptionsColumn.AllowEdit = false;
            this.colActualDurationUnitsDescriptor.OptionsColumn.AllowFocus = false;
            this.colActualDurationUnitsDescriptor.OptionsColumn.ReadOnly = true;
            this.colActualDurationUnitsDescriptor.Visible = true;
            this.colActualDurationUnitsDescriptor.VisibleIndex = 14;
            this.colActualDurationUnitsDescriptor.Width = 147;
            // 
            // colJobTypeID
            // 
            this.colJobTypeID.Caption = "Job Type ID";
            this.colJobTypeID.FieldName = "JobTypeID";
            this.colJobTypeID.Name = "colJobTypeID";
            this.colJobTypeID.OptionsColumn.AllowEdit = false;
            this.colJobTypeID.OptionsColumn.AllowFocus = false;
            this.colJobTypeID.OptionsColumn.ReadOnly = true;
            this.colJobTypeID.Width = 79;
            // 
            // colJobSubTypeDescription
            // 
            this.colJobSubTypeDescription.Caption = "Job Sub-Type";
            this.colJobSubTypeDescription.FieldName = "JobSubTypeDescription";
            this.colJobSubTypeDescription.Name = "colJobSubTypeDescription";
            this.colJobSubTypeDescription.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeDescription.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeDescription.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeDescription.Visible = true;
            this.colJobSubTypeDescription.VisibleIndex = 5;
            this.colJobSubTypeDescription.Width = 99;
            // 
            // colJobTypeDescription
            // 
            this.colJobTypeDescription.Caption = "Job Type";
            this.colJobTypeDescription.FieldName = "JobTypeDescription";
            this.colJobTypeDescription.Name = "colJobTypeDescription";
            this.colJobTypeDescription.OptionsColumn.AllowEdit = false;
            this.colJobTypeDescription.OptionsColumn.AllowFocus = false;
            this.colJobTypeDescription.OptionsColumn.ReadOnly = true;
            this.colJobTypeDescription.Visible = true;
            this.colJobTypeDescription.VisibleIndex = 4;
            this.colJobTypeDescription.Width = 89;
            // 
            // colRecordType
            // 
            this.colRecordType.Caption = "Record Type";
            this.colRecordType.FieldName = "RecordType";
            this.colRecordType.Name = "colRecordType";
            this.colRecordType.OptionsColumn.AllowEdit = false;
            this.colRecordType.OptionsColumn.AllowFocus = false;
            this.colRecordType.OptionsColumn.ReadOnly = true;
            this.colRecordType.Width = 82;
            // 
            // colSelected
            // 
            this.colSelected.Caption = "Selected";
            this.colSelected.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSelected.FieldName = "Selected";
            this.colSelected.Name = "colSelected";
            this.colSelected.OptionsColumn.AllowEdit = false;
            this.colSelected.OptionsColumn.AllowFocus = false;
            this.colSelected.OptionsColumn.ReadOnly = true;
            // 
            // colFullDescription
            // 
            this.colFullDescription.Caption = "Full Description";
            this.colFullDescription.FieldName = "FullDescription";
            this.colFullDescription.Name = "colFullDescription";
            this.colFullDescription.OptionsColumn.AllowEdit = false;
            this.colFullDescription.OptionsColumn.AllowFocus = false;
            this.colFullDescription.OptionsColumn.ReadOnly = true;
            this.colFullDescription.Width = 93;
            // 
            // colMaximumDaysFromLastVisit
            // 
            this.colMaximumDaysFromLastVisit.Caption = "Max Days From Last Visit";
            this.colMaximumDaysFromLastVisit.FieldName = "MaximumDaysFromLastVisit";
            this.colMaximumDaysFromLastVisit.Name = "colMaximumDaysFromLastVisit";
            this.colMaximumDaysFromLastVisit.OptionsColumn.AllowEdit = false;
            this.colMaximumDaysFromLastVisit.OptionsColumn.AllowFocus = false;
            this.colMaximumDaysFromLastVisit.OptionsColumn.ReadOnly = true;
            this.colMaximumDaysFromLastVisit.Visible = true;
            this.colMaximumDaysFromLastVisit.VisibleIndex = 21;
            this.colMaximumDaysFromLastVisit.Width = 140;
            // 
            // colMinimumDaysFromLastVisit
            // 
            this.colMinimumDaysFromLastVisit.Caption = "Min Days From Last Visit";
            this.colMinimumDaysFromLastVisit.FieldName = "MinimumDaysFromLastVisit";
            this.colMinimumDaysFromLastVisit.Name = "colMinimumDaysFromLastVisit";
            this.colMinimumDaysFromLastVisit.OptionsColumn.AllowEdit = false;
            this.colMinimumDaysFromLastVisit.OptionsColumn.AllowFocus = false;
            this.colMinimumDaysFromLastVisit.OptionsColumn.ReadOnly = true;
            this.colMinimumDaysFromLastVisit.Visible = true;
            this.colMinimumDaysFromLastVisit.VisibleIndex = 22;
            this.colMinimumDaysFromLastVisit.Width = 136;
            // 
            // colDaysLeeway
            // 
            this.colDaysLeeway.Caption = "Days Leeway";
            this.colDaysLeeway.FieldName = "DaysLeeway";
            this.colDaysLeeway.Name = "colDaysLeeway";
            this.colDaysLeeway.OptionsColumn.AllowEdit = false;
            this.colDaysLeeway.OptionsColumn.AllowFocus = false;
            this.colDaysLeeway.OptionsColumn.ReadOnly = true;
            this.colDaysLeeway.Visible = true;
            this.colDaysLeeway.VisibleIndex = 23;
            this.colDaysLeeway.Width = 85;
            // 
            // colContractDescription
            // 
            this.colContractDescription.Caption = "Contract Descrition";
            this.colContractDescription.FieldName = "ContractDescription";
            this.colContractDescription.Name = "colContractDescription";
            this.colContractDescription.OptionsColumn.AllowEdit = false;
            this.colContractDescription.OptionsColumn.AllowFocus = false;
            this.colContractDescription.OptionsColumn.ReadOnly = true;
            this.colContractDescription.Visible = true;
            this.colContractDescription.VisibleIndex = 3;
            this.colContractDescription.Width = 125;
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // colClientContractID
            // 
            this.colClientContractID.Caption = "Client Contract ID";
            this.colClientContractID.FieldName = "ClientContractID";
            this.colClientContractID.Name = "colClientContractID";
            this.colClientContractID.OptionsColumn.AllowEdit = false;
            this.colClientContractID.OptionsColumn.AllowFocus = false;
            this.colClientContractID.OptionsColumn.ReadOnly = true;
            this.colClientContractID.Width = 107;
            // 
            // colSiteID
            // 
            this.colSiteID.Caption = "Site ID";
            this.colSiteID.FieldName = "SiteID";
            this.colSiteID.Name = "colSiteID";
            this.colSiteID.OptionsColumn.AllowEdit = false;
            this.colSiteID.OptionsColumn.AllowFocus = false;
            this.colSiteID.OptionsColumn.ReadOnly = true;
            // 
            // colSiteContractID
            // 
            this.colSiteContractID.Caption = "Site Contract ID";
            this.colSiteContractID.FieldName = "SiteContractID";
            this.colSiteContractID.Name = "colSiteContractID";
            this.colSiteContractID.OptionsColumn.AllowEdit = false;
            this.colSiteContractID.OptionsColumn.AllowFocus = false;
            this.colSiteContractID.OptionsColumn.ReadOnly = true;
            this.colSiteContractID.Width = 98;
            // 
            // colSitePostcode
            // 
            this.colSitePostcode.Caption = "Site Postcode";
            this.colSitePostcode.FieldName = "SitePostcode";
            this.colSitePostcode.Name = "colSitePostcode";
            this.colSitePostcode.OptionsColumn.AllowEdit = false;
            this.colSitePostcode.OptionsColumn.AllowFocus = false;
            this.colSitePostcode.OptionsColumn.ReadOnly = true;
            this.colSitePostcode.Width = 86;
            // 
            // colLocationX
            // 
            this.colLocationX.Caption = "Latitude";
            this.colLocationX.FieldName = "LocationX";
            this.colLocationX.Name = "colLocationX";
            this.colLocationX.OptionsColumn.AllowEdit = false;
            this.colLocationX.OptionsColumn.AllowFocus = false;
            this.colLocationX.OptionsColumn.ReadOnly = true;
            // 
            // colLocationY
            // 
            this.colLocationY.Caption = "Longitude";
            this.colLocationY.FieldName = "LocationY";
            this.colLocationY.Name = "colLocationY";
            this.colLocationY.OptionsColumn.AllowEdit = false;
            this.colLocationY.OptionsColumn.AllowFocus = false;
            this.colLocationY.OptionsColumn.ReadOnly = true;
            // 
            // colLinkedToVisit
            // 
            this.colLinkedToVisit.Caption = "Linked To Visit";
            this.colLinkedToVisit.FieldName = "LinkedToVisit";
            this.colLinkedToVisit.Name = "colLinkedToVisit";
            this.colLinkedToVisit.OptionsColumn.AllowEdit = false;
            this.colLinkedToVisit.OptionsColumn.AllowFocus = false;
            this.colLinkedToVisit.OptionsColumn.ReadOnly = true;
            this.colLinkedToVisit.Width = 88;
            // 
            // colClientAndContract
            // 
            this.colClientAndContract.Caption = "Client and Contract";
            this.colClientAndContract.FieldName = "ClientAndContract";
            this.colClientAndContract.Name = "colClientAndContract";
            this.colClientAndContract.OptionsColumn.AllowEdit = false;
            this.colClientAndContract.OptionsColumn.AllowFocus = false;
            this.colClientAndContract.OptionsColumn.ReadOnly = true;
            this.colClientAndContract.Width = 114;
            // 
            // colRework
            // 
            this.colRework.Caption = "Rework";
            this.colRework.FieldName = "Rework";
            this.colRework.Name = "colRework";
            this.colRework.OptionsColumn.AllowEdit = false;
            this.colRework.OptionsColumn.AllowFocus = false;
            this.colRework.OptionsColumn.ReadOnly = true;
            this.colRework.Visible = true;
            this.colRework.VisibleIndex = 24;
            // 
            // colReworkOriginalJobID
            // 
            this.colReworkOriginalJobID.Caption = "Rework Original Job ID";
            this.colReworkOriginalJobID.FieldName = "ReworkOriginalJobID";
            this.colReworkOriginalJobID.Name = "colReworkOriginalJobID";
            this.colReworkOriginalJobID.OptionsColumn.AllowEdit = false;
            this.colReworkOriginalJobID.OptionsColumn.AllowFocus = false;
            this.colReworkOriginalJobID.OptionsColumn.ReadOnly = true;
            this.colReworkOriginalJobID.Width = 130;
            // 
            // colLinkedLabourCount
            // 
            this.colLinkedLabourCount.Caption = "Linked Labour";
            this.colLinkedLabourCount.FieldName = "LinkedLabourCount";
            this.colLinkedLabourCount.Name = "colLinkedLabourCount";
            this.colLinkedLabourCount.OptionsColumn.AllowEdit = false;
            this.colLinkedLabourCount.OptionsColumn.AllowFocus = false;
            this.colLinkedLabourCount.OptionsColumn.ReadOnly = true;
            this.colLinkedLabourCount.Visible = true;
            this.colLinkedLabourCount.VisibleIndex = 16;
            this.colLinkedLabourCount.Width = 87;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.dataSet_AT;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // repositoryItemCheckEditShowActiveEmployeesOnly
            // 
            this.repositoryItemCheckEditShowActiveEmployeesOnly.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.Appearance.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.AutoHeight = false;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.Caption = "";
            this.repositoryItemCheckEditShowActiveEmployeesOnly.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.Name = "repositoryItemCheckEditShowActiveEmployeesOnly";
            this.repositoryItemCheckEditShowActiveEmployeesOnly.ValueChecked = 1;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.ValueUnchecked = 0;
            // 
            // repositoryItemPopupContainerEdit2
            // 
            this.repositoryItemPopupContainerEdit2.AutoHeight = false;
            this.repositoryItemPopupContainerEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit2.Name = "repositoryItemPopupContainerEdit2";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 43);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Jobs for Labour Reassignment";
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel2.Controls.Add(this.gridSplitContainer2);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "Labour Linked to Selected Jobs";
            this.splitContainerControl1.Size = new System.Drawing.Size(989, 483);
            this.splitContainerControl1.SplitterPosition = 491;
            this.splitContainerControl1.TabIndex = 4;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.gridControlJob;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControlJob);
            this.gridSplitContainer1.Size = new System.Drawing.Size(488, 459);
            this.gridSplitContainer1.TabIndex = 2;
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer2.Grid = this.gridControlLabour;
            this.gridSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.gridControlLabour);
            this.gridSplitContainer2.Size = new System.Drawing.Size(487, 459);
            this.gridSplitContainer2.TabIndex = 0;
            // 
            // gridControlLabour
            // 
            this.gridControlLabour.DataSource = this.sp06270OMJobLabourReassignLinkedLabourBindingSource;
            this.gridControlLabour.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlLabour.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlLabour.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlLabour.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Ad New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "Reload Data", "reload")});
            this.gridControlLabour.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlLabour_EmbeddedNavigator_ButtonClick);
            this.gridControlLabour.Location = new System.Drawing.Point(0, 0);
            this.gridControlLabour.MainView = this.gridViewLabour;
            this.gridControlLabour.MenuManager = this.barManager1;
            this.gridControlLabour.Name = "gridControlLabour";
            this.gridControlLabour.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEditNumeric2DP2,
            this.repositoryItemTextEditCurrency,
            this.repositoryItemTextEditPercentage,
            this.repositoryItemMemoExEdit2,
            this.repositoryItemTextEditDateEdit2,
            this.repositoryItemCheckEdit2,
            this.repositoryItemButtonEditChangeTeam});
            this.gridControlLabour.Size = new System.Drawing.Size(487, 459);
            this.gridControlLabour.TabIndex = 3;
            this.gridControlLabour.UseEmbeddedNavigator = true;
            this.gridControlLabour.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewLabour});
            // 
            // sp06270OMJobLabourReassignLinkedLabourBindingSource
            // 
            this.sp06270OMJobLabourReassignLinkedLabourBindingSource.DataMember = "sp06270_OM_Job_Labour_Reassign_Linked_Labour";
            this.sp06270OMJobLabourReassignLinkedLabourBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridViewLabour
            // 
            this.gridViewLabour.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLabourUsedID,
            this.colContractorID,
            this.colJobID1,
            this.colCostUnitsUsed,
            this.colCostUnitDescriptorID,
            this.colSellUnitsUsed,
            this.colSellUnitDescriptorID,
            this.colCostPerUnitExVat,
            this.colCostPerUnitVatRate,
            this.colSellPerUnitExVat,
            this.colSellPerUnitVatRate,
            this.colCostValueExVat,
            this.colCostValueVat,
            this.colCostValueTotal,
            this.colSellValueExVat,
            this.colSellValueVat,
            this.colSellValueTotal,
            this.colRemarks,
            this.gridColumn1,
            this.gridColumn2,
            this.colSiteName1,
            this.colClientName1,
            this.colJobSubTypeDescription1,
            this.colJobTypeDescription2,
            this.colExpectedStartDate1,
            this.colVisitNumber1,
            this.gridColumn3,
            this.colContractorName,
            this.colSelected1,
            this.colCostUnitDescriptor,
            this.colSellUnitDescriptor,
            this.colLabourType,
            this.colCISPercentage,
            this.colCISValue,
            this.colPurchaseOrderID,
            this.colStartDateTime,
            this.colEndDateTime,
            this.colVisitID1,
            this.colPDA_JobID,
            this.colLinkedToPersonType,
            this.colLinkedToPersonTypeID});
            this.gridViewLabour.GridControl = this.gridControlLabour;
            this.gridViewLabour.GroupCount = 1;
            this.gridViewLabour.Name = "gridViewLabour";
            this.gridViewLabour.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewLabour.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewLabour.OptionsLayout.StoreAppearance = true;
            this.gridViewLabour.OptionsLayout.StoreFormatRules = true;
            this.gridViewLabour.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewLabour.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewLabour.OptionsSelection.MultiSelect = true;
            this.gridViewLabour.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewLabour.OptionsView.ColumnAutoWidth = false;
            this.gridViewLabour.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewLabour.OptionsView.ShowGroupPanel = false;
            this.gridViewLabour.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToPersonType, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colContractorName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewLabour.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridViewLabour_CustomDrawCell);
            this.gridViewLabour.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewLabour_CustomRowCellEdit);
            this.gridViewLabour.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewLabour.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewLabour.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewLabour.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewLabour.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewLabour.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewLabour_MouseUp);
            this.gridViewLabour.DoubleClick += new System.EventHandler(this.gridViewLabour_DoubleClick);
            this.gridViewLabour.GotFocus += new System.EventHandler(this.gridViewLabour_GotFocus);
            // 
            // colLabourUsedID
            // 
            this.colLabourUsedID.Caption = "Labour Used ID";
            this.colLabourUsedID.FieldName = "LabourUsedID";
            this.colLabourUsedID.Name = "colLabourUsedID";
            this.colLabourUsedID.OptionsColumn.AllowEdit = false;
            this.colLabourUsedID.OptionsColumn.AllowFocus = false;
            this.colLabourUsedID.OptionsColumn.ReadOnly = true;
            this.colLabourUsedID.Width = 108;
            // 
            // colContractorID
            // 
            this.colContractorID.Caption = "Contractor ID";
            this.colContractorID.FieldName = "ContractorID";
            this.colContractorID.Name = "colContractorID";
            this.colContractorID.OptionsColumn.AllowEdit = false;
            this.colContractorID.OptionsColumn.AllowFocus = false;
            this.colContractorID.OptionsColumn.ReadOnly = true;
            // 
            // colJobID1
            // 
            this.colJobID1.Caption = "Job ID";
            this.colJobID1.FieldName = "JobID";
            this.colJobID1.Name = "colJobID1";
            this.colJobID1.OptionsColumn.AllowEdit = false;
            this.colJobID1.OptionsColumn.AllowFocus = false;
            this.colJobID1.OptionsColumn.ReadOnly = true;
            // 
            // colCostUnitsUsed
            // 
            this.colCostUnitsUsed.Caption = "Cost Units Used";
            this.colCostUnitsUsed.ColumnEdit = this.repositoryItemTextEditNumeric2DP2;
            this.colCostUnitsUsed.FieldName = "CostUnitsUsed";
            this.colCostUnitsUsed.Name = "colCostUnitsUsed";
            this.colCostUnitsUsed.OptionsColumn.AllowEdit = false;
            this.colCostUnitsUsed.OptionsColumn.AllowFocus = false;
            this.colCostUnitsUsed.OptionsColumn.ReadOnly = true;
            this.colCostUnitsUsed.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostUnitsUsed", "{0:n2}")});
            this.colCostUnitsUsed.Visible = true;
            this.colCostUnitsUsed.VisibleIndex = 6;
            this.colCostUnitsUsed.Width = 97;
            // 
            // repositoryItemTextEditNumeric2DP2
            // 
            this.repositoryItemTextEditNumeric2DP2.AutoHeight = false;
            this.repositoryItemTextEditNumeric2DP2.Mask.EditMask = "n2";
            this.repositoryItemTextEditNumeric2DP2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumeric2DP2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumeric2DP2.Name = "repositoryItemTextEditNumeric2DP2";
            // 
            // colCostUnitDescriptorID
            // 
            this.colCostUnitDescriptorID.Caption = "Cost Unit Descriptor ID";
            this.colCostUnitDescriptorID.FieldName = "CostUnitDescriptorID";
            this.colCostUnitDescriptorID.Name = "colCostUnitDescriptorID";
            this.colCostUnitDescriptorID.OptionsColumn.AllowEdit = false;
            this.colCostUnitDescriptorID.OptionsColumn.AllowFocus = false;
            this.colCostUnitDescriptorID.OptionsColumn.ReadOnly = true;
            this.colCostUnitDescriptorID.Width = 131;
            // 
            // colSellUnitsUsed
            // 
            this.colSellUnitsUsed.Caption = "Sell Units Used";
            this.colSellUnitsUsed.ColumnEdit = this.repositoryItemTextEditNumeric2DP2;
            this.colSellUnitsUsed.FieldName = "SellUnitsUsed";
            this.colSellUnitsUsed.Name = "colSellUnitsUsed";
            this.colSellUnitsUsed.OptionsColumn.AllowEdit = false;
            this.colSellUnitsUsed.OptionsColumn.AllowFocus = false;
            this.colSellUnitsUsed.OptionsColumn.ReadOnly = true;
            this.colSellUnitsUsed.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellUnitsUsed", "{0:n2}")});
            this.colSellUnitsUsed.Visible = true;
            this.colSellUnitsUsed.VisibleIndex = 12;
            this.colSellUnitsUsed.Width = 91;
            // 
            // colSellUnitDescriptorID
            // 
            this.colSellUnitDescriptorID.Caption = "Sell Unit Descriptor ID";
            this.colSellUnitDescriptorID.FieldName = "SellUnitDescriptorID";
            this.colSellUnitDescriptorID.Name = "colSellUnitDescriptorID";
            this.colSellUnitDescriptorID.OptionsColumn.AllowEdit = false;
            this.colSellUnitDescriptorID.OptionsColumn.AllowFocus = false;
            this.colSellUnitDescriptorID.OptionsColumn.ReadOnly = true;
            this.colSellUnitDescriptorID.Width = 125;
            // 
            // colCostPerUnitExVat
            // 
            this.colCostPerUnitExVat.Caption = "Cost Per Unit Ex VAT";
            this.colCostPerUnitExVat.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCostPerUnitExVat.FieldName = "CostPerUnitExVat";
            this.colCostPerUnitExVat.Name = "colCostPerUnitExVat";
            this.colCostPerUnitExVat.OptionsColumn.AllowEdit = false;
            this.colCostPerUnitExVat.OptionsColumn.AllowFocus = false;
            this.colCostPerUnitExVat.OptionsColumn.ReadOnly = true;
            this.colCostPerUnitExVat.Visible = true;
            this.colCostPerUnitExVat.VisibleIndex = 7;
            this.colCostPerUnitExVat.Width = 121;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // colCostPerUnitVatRate
            // 
            this.colCostPerUnitVatRate.Caption = "Cost VAT Rate";
            this.colCostPerUnitVatRate.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colCostPerUnitVatRate.FieldName = "CostPerUnitVatRate";
            this.colCostPerUnitVatRate.Name = "colCostPerUnitVatRate";
            this.colCostPerUnitVatRate.OptionsColumn.AllowEdit = false;
            this.colCostPerUnitVatRate.OptionsColumn.AllowFocus = false;
            this.colCostPerUnitVatRate.OptionsColumn.ReadOnly = true;
            this.colCostPerUnitVatRate.Visible = true;
            this.colCostPerUnitVatRate.VisibleIndex = 8;
            this.colCostPerUnitVatRate.Width = 91;
            // 
            // repositoryItemTextEditPercentage
            // 
            this.repositoryItemTextEditPercentage.AutoHeight = false;
            this.repositoryItemTextEditPercentage.Mask.EditMask = "P0";
            this.repositoryItemTextEditPercentage.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage.Name = "repositoryItemTextEditPercentage";
            // 
            // colSellPerUnitExVat
            // 
            this.colSellPerUnitExVat.Caption = "Sell Per Unit Ex VAT";
            this.colSellPerUnitExVat.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSellPerUnitExVat.FieldName = "SellPerUnitExVat";
            this.colSellPerUnitExVat.Name = "colSellPerUnitExVat";
            this.colSellPerUnitExVat.OptionsColumn.AllowEdit = false;
            this.colSellPerUnitExVat.OptionsColumn.AllowFocus = false;
            this.colSellPerUnitExVat.OptionsColumn.ReadOnly = true;
            this.colSellPerUnitExVat.Visible = true;
            this.colSellPerUnitExVat.VisibleIndex = 13;
            this.colSellPerUnitExVat.Width = 115;
            // 
            // colSellPerUnitVatRate
            // 
            this.colSellPerUnitVatRate.Caption = "Sell VAT Rate";
            this.colSellPerUnitVatRate.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colSellPerUnitVatRate.FieldName = "SellPerUnitVatRate";
            this.colSellPerUnitVatRate.Name = "colSellPerUnitVatRate";
            this.colSellPerUnitVatRate.OptionsColumn.AllowEdit = false;
            this.colSellPerUnitVatRate.OptionsColumn.AllowFocus = false;
            this.colSellPerUnitVatRate.OptionsColumn.ReadOnly = true;
            this.colSellPerUnitVatRate.Visible = true;
            this.colSellPerUnitVatRate.VisibleIndex = 14;
            this.colSellPerUnitVatRate.Width = 85;
            // 
            // colCostValueExVat
            // 
            this.colCostValueExVat.Caption = "Cost Ex VAT";
            this.colCostValueExVat.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCostValueExVat.FieldName = "CostValueExVat";
            this.colCostValueExVat.Name = "colCostValueExVat";
            this.colCostValueExVat.OptionsColumn.AllowEdit = false;
            this.colCostValueExVat.OptionsColumn.AllowFocus = false;
            this.colCostValueExVat.OptionsColumn.ReadOnly = true;
            this.colCostValueExVat.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostValueExVat", "{0:c}")});
            this.colCostValueExVat.Visible = true;
            this.colCostValueExVat.VisibleIndex = 9;
            this.colCostValueExVat.Width = 80;
            // 
            // colCostValueVat
            // 
            this.colCostValueVat.Caption = "Cost VAT";
            this.colCostValueVat.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCostValueVat.FieldName = "CostValueVat";
            this.colCostValueVat.Name = "colCostValueVat";
            this.colCostValueVat.OptionsColumn.AllowEdit = false;
            this.colCostValueVat.OptionsColumn.AllowFocus = false;
            this.colCostValueVat.OptionsColumn.ReadOnly = true;
            this.colCostValueVat.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostValueVat", "{0:c}")});
            this.colCostValueVat.Visible = true;
            this.colCostValueVat.VisibleIndex = 10;
            // 
            // colCostValueTotal
            // 
            this.colCostValueTotal.Caption = "Cost Total";
            this.colCostValueTotal.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCostValueTotal.FieldName = "CostValueTotal";
            this.colCostValueTotal.Name = "colCostValueTotal";
            this.colCostValueTotal.OptionsColumn.AllowEdit = false;
            this.colCostValueTotal.OptionsColumn.AllowFocus = false;
            this.colCostValueTotal.OptionsColumn.ReadOnly = true;
            this.colCostValueTotal.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostValueTotal", "{0:c}")});
            this.colCostValueTotal.Visible = true;
            this.colCostValueTotal.VisibleIndex = 11;
            // 
            // colSellValueExVat
            // 
            this.colSellValueExVat.Caption = "Sell Ex VAT";
            this.colSellValueExVat.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSellValueExVat.FieldName = "SellValueExVat";
            this.colSellValueExVat.Name = "colSellValueExVat";
            this.colSellValueExVat.OptionsColumn.AllowEdit = false;
            this.colSellValueExVat.OptionsColumn.AllowFocus = false;
            this.colSellValueExVat.OptionsColumn.ReadOnly = true;
            this.colSellValueExVat.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellValueExVat", "{0:c}")});
            this.colSellValueExVat.Visible = true;
            this.colSellValueExVat.VisibleIndex = 15;
            // 
            // colSellValueVat
            // 
            this.colSellValueVat.Caption = "Sell VAT";
            this.colSellValueVat.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSellValueVat.FieldName = "SellValueVat";
            this.colSellValueVat.Name = "colSellValueVat";
            this.colSellValueVat.OptionsColumn.AllowEdit = false;
            this.colSellValueVat.OptionsColumn.AllowFocus = false;
            this.colSellValueVat.OptionsColumn.ReadOnly = true;
            this.colSellValueVat.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellValueVat", "{0:c}")});
            this.colSellValueVat.Visible = true;
            this.colSellValueVat.VisibleIndex = 16;
            // 
            // colSellValueTotal
            // 
            this.colSellValueTotal.Caption = "Sell Total";
            this.colSellValueTotal.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSellValueTotal.FieldName = "SellValueTotal";
            this.colSellValueTotal.Name = "colSellValueTotal";
            this.colSellValueTotal.OptionsColumn.AllowEdit = false;
            this.colSellValueTotal.OptionsColumn.AllowFocus = false;
            this.colSellValueTotal.OptionsColumn.ReadOnly = true;
            this.colSellValueTotal.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellValueTotal", "{0:c}")});
            this.colSellValueTotal.Visible = true;
            this.colSellValueTotal.VisibleIndex = 17;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 20;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Client ID";
            this.gridColumn1.FieldName = "ClientID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Site ID";
            this.gridColumn2.FieldName = "SiteID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            // 
            // colSiteName1
            // 
            this.colSiteName1.Caption = "Site Name";
            this.colSiteName1.FieldName = "SiteName";
            this.colSiteName1.Name = "colSiteName1";
            this.colSiteName1.OptionsColumn.AllowEdit = false;
            this.colSiteName1.OptionsColumn.AllowFocus = false;
            this.colSiteName1.OptionsColumn.ReadOnly = true;
            this.colSiteName1.Width = 133;
            // 
            // colClientName1
            // 
            this.colClientName1.Caption = "Client Name";
            this.colClientName1.FieldName = "ClientName";
            this.colClientName1.Name = "colClientName1";
            this.colClientName1.OptionsColumn.AllowEdit = false;
            this.colClientName1.OptionsColumn.AllowFocus = false;
            this.colClientName1.OptionsColumn.ReadOnly = true;
            this.colClientName1.Width = 139;
            // 
            // colJobSubTypeDescription1
            // 
            this.colJobSubTypeDescription1.Caption = "Job Sub-Type";
            this.colJobSubTypeDescription1.FieldName = "JobSubTypeDescription";
            this.colJobSubTypeDescription1.Name = "colJobSubTypeDescription1";
            this.colJobSubTypeDescription1.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeDescription1.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeDescription1.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeDescription1.Width = 150;
            // 
            // colJobTypeDescription2
            // 
            this.colJobTypeDescription2.Caption = "Job Type";
            this.colJobTypeDescription2.FieldName = "JobTypeDescription";
            this.colJobTypeDescription2.Name = "colJobTypeDescription2";
            this.colJobTypeDescription2.OptionsColumn.AllowEdit = false;
            this.colJobTypeDescription2.OptionsColumn.AllowFocus = false;
            this.colJobTypeDescription2.OptionsColumn.ReadOnly = true;
            this.colJobTypeDescription2.Width = 169;
            // 
            // colExpectedStartDate1
            // 
            this.colExpectedStartDate1.Caption = "Expected Start Date";
            this.colExpectedStartDate1.ColumnEdit = this.repositoryItemTextEditDateEdit2;
            this.colExpectedStartDate1.FieldName = "ExpectedStartDate";
            this.colExpectedStartDate1.Name = "colExpectedStartDate1";
            this.colExpectedStartDate1.OptionsColumn.AllowEdit = false;
            this.colExpectedStartDate1.OptionsColumn.AllowFocus = false;
            this.colExpectedStartDate1.OptionsColumn.ReadOnly = true;
            this.colExpectedStartDate1.Visible = true;
            this.colExpectedStartDate1.VisibleIndex = 3;
            this.colExpectedStartDate1.Width = 119;
            // 
            // repositoryItemTextEditDateEdit2
            // 
            this.repositoryItemTextEditDateEdit2.AutoHeight = false;
            this.repositoryItemTextEditDateEdit2.Mask.EditMask = "g";
            this.repositoryItemTextEditDateEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateEdit2.Name = "repositoryItemTextEditDateEdit2";
            // 
            // colVisitNumber1
            // 
            this.colVisitNumber1.Caption = "Visit #";
            this.colVisitNumber1.FieldName = "VisitNumber";
            this.colVisitNumber1.Name = "colVisitNumber1";
            this.colVisitNumber1.OptionsColumn.AllowEdit = false;
            this.colVisitNumber1.OptionsColumn.AllowFocus = false;
            this.colVisitNumber1.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Linked To Job";
            this.gridColumn3.FieldName = "FullDescription";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 18;
            this.gridColumn3.Width = 415;
            // 
            // colContractorName
            // 
            this.colContractorName.Caption = "Labour Name";
            this.colContractorName.FieldName = "ContractorName";
            this.colContractorName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colContractorName.Name = "colContractorName";
            this.colContractorName.OptionsColumn.AllowEdit = false;
            this.colContractorName.OptionsColumn.AllowFocus = false;
            this.colContractorName.OptionsColumn.ReadOnly = true;
            this.colContractorName.Visible = true;
            this.colContractorName.VisibleIndex = 1;
            this.colContractorName.Width = 181;
            // 
            // colSelected1
            // 
            this.colSelected1.Caption = "Selected";
            this.colSelected1.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colSelected1.FieldName = "Selected";
            this.colSelected1.Name = "colSelected1";
            this.colSelected1.OptionsColumn.AllowEdit = false;
            this.colSelected1.OptionsColumn.AllowFocus = false;
            this.colSelected1.OptionsColumn.ReadOnly = true;
            this.colSelected1.Width = 62;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // colCostUnitDescriptor
            // 
            this.colCostUnitDescriptor.Caption = "Cost Unit Descriptor";
            this.colCostUnitDescriptor.FieldName = "CostUnitDescriptor";
            this.colCostUnitDescriptor.Name = "colCostUnitDescriptor";
            this.colCostUnitDescriptor.OptionsColumn.AllowEdit = false;
            this.colCostUnitDescriptor.OptionsColumn.AllowFocus = false;
            this.colCostUnitDescriptor.OptionsColumn.ReadOnly = true;
            this.colCostUnitDescriptor.Width = 117;
            // 
            // colSellUnitDescriptor
            // 
            this.colSellUnitDescriptor.Caption = "Sell Unit Descriptor";
            this.colSellUnitDescriptor.FieldName = "SellUnitDescriptor";
            this.colSellUnitDescriptor.Name = "colSellUnitDescriptor";
            this.colSellUnitDescriptor.OptionsColumn.AllowEdit = false;
            this.colSellUnitDescriptor.OptionsColumn.AllowFocus = false;
            this.colSellUnitDescriptor.OptionsColumn.ReadOnly = true;
            this.colSellUnitDescriptor.Width = 111;
            // 
            // colLabourType
            // 
            this.colLabourType.Caption = "Internal \\ External";
            this.colLabourType.FieldName = "LabourType";
            this.colLabourType.Name = "colLabourType";
            this.colLabourType.OptionsColumn.AllowEdit = false;
            this.colLabourType.OptionsColumn.AllowFocus = false;
            this.colLabourType.OptionsColumn.ReadOnly = true;
            this.colLabourType.Visible = true;
            this.colLabourType.VisibleIndex = 2;
            this.colLabourType.Width = 116;
            // 
            // colCISPercentage
            // 
            this.colCISPercentage.Caption = "CIS %";
            this.colCISPercentage.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colCISPercentage.FieldName = "CISPercentage";
            this.colCISPercentage.Name = "colCISPercentage";
            this.colCISPercentage.OptionsColumn.AllowEdit = false;
            this.colCISPercentage.OptionsColumn.AllowFocus = false;
            this.colCISPercentage.OptionsColumn.ReadOnly = true;
            this.colCISPercentage.Visible = true;
            this.colCISPercentage.VisibleIndex = 18;
            // 
            // colCISValue
            // 
            this.colCISValue.Caption = "CIS Value";
            this.colCISValue.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCISValue.FieldName = "CISValue";
            this.colCISValue.Name = "colCISValue";
            this.colCISValue.OptionsColumn.AllowEdit = false;
            this.colCISValue.OptionsColumn.AllowFocus = false;
            this.colCISValue.OptionsColumn.ReadOnly = true;
            this.colCISValue.Visible = true;
            this.colCISValue.VisibleIndex = 19;
            // 
            // colPurchaseOrderID
            // 
            this.colPurchaseOrderID.Caption = "PO ID";
            this.colPurchaseOrderID.FieldName = "PurchaseOrderID";
            this.colPurchaseOrderID.Name = "colPurchaseOrderID";
            this.colPurchaseOrderID.OptionsColumn.AllowEdit = false;
            this.colPurchaseOrderID.OptionsColumn.AllowFocus = false;
            this.colPurchaseOrderID.OptionsColumn.ReadOnly = true;
            this.colPurchaseOrderID.Width = 49;
            // 
            // colStartDateTime
            // 
            this.colStartDateTime.Caption = "Start Date\\Time";
            this.colStartDateTime.ColumnEdit = this.repositoryItemTextEditDateEdit2;
            this.colStartDateTime.FieldName = "StartDateTime";
            this.colStartDateTime.Name = "colStartDateTime";
            this.colStartDateTime.OptionsColumn.AllowEdit = false;
            this.colStartDateTime.OptionsColumn.AllowFocus = false;
            this.colStartDateTime.OptionsColumn.ReadOnly = true;
            this.colStartDateTime.Visible = true;
            this.colStartDateTime.VisibleIndex = 4;
            this.colStartDateTime.Width = 100;
            // 
            // colEndDateTime
            // 
            this.colEndDateTime.Caption = "End Date\\Time";
            this.colEndDateTime.ColumnEdit = this.repositoryItemTextEditDateEdit2;
            this.colEndDateTime.FieldName = "EndDateTime";
            this.colEndDateTime.Name = "colEndDateTime";
            this.colEndDateTime.OptionsColumn.AllowEdit = false;
            this.colEndDateTime.OptionsColumn.AllowFocus = false;
            this.colEndDateTime.OptionsColumn.ReadOnly = true;
            this.colEndDateTime.Visible = true;
            this.colEndDateTime.VisibleIndex = 5;
            this.colEndDateTime.Width = 100;
            // 
            // colVisitID1
            // 
            this.colVisitID1.Caption = "Visit ID";
            this.colVisitID1.FieldName = "VisitID";
            this.colVisitID1.Name = "colVisitID1";
            this.colVisitID1.OptionsColumn.AllowEdit = false;
            this.colVisitID1.OptionsColumn.AllowFocus = false;
            this.colVisitID1.OptionsColumn.ReadOnly = true;
            this.colVisitID1.Width = 54;
            // 
            // colPDA_JobID
            // 
            this.colPDA_JobID.Caption = "Device Job ID";
            this.colPDA_JobID.FieldName = "PDA_JobID";
            this.colPDA_JobID.Name = "colPDA_JobID";
            this.colPDA_JobID.OptionsColumn.AllowEdit = false;
            this.colPDA_JobID.OptionsColumn.AllowFocus = false;
            this.colPDA_JobID.OptionsColumn.ReadOnly = true;
            // 
            // colLinkedToPersonType
            // 
            this.colLinkedToPersonType.Caption = "Labour Type";
            this.colLinkedToPersonType.FieldName = "LinkedToPersonType";
            this.colLinkedToPersonType.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colLinkedToPersonType.Name = "colLinkedToPersonType";
            this.colLinkedToPersonType.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonType.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonType.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonType.Visible = true;
            this.colLinkedToPersonType.VisibleIndex = 0;
            this.colLinkedToPersonType.Width = 109;
            // 
            // colLinkedToPersonTypeID
            // 
            this.colLinkedToPersonTypeID.Caption = "Labour Type ID";
            this.colLinkedToPersonTypeID.FieldName = "LinkedToPersonTypeID";
            this.colLinkedToPersonTypeID.Name = "colLinkedToPersonTypeID";
            this.colLinkedToPersonTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonTypeID.Width = 93;
            // 
            // repositoryItemButtonEditChangeTeam
            // 
            this.repositoryItemButtonEditChangeTeam.AutoHeight = false;
            this.repositoryItemButtonEditChangeTeam.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemButtonEditChangeTeam.Name = "repositoryItemButtonEditChangeTeam";
            // 
            // gridView3
            // 
            this.gridView3.Name = "gridView3";
            // 
            // sp06269_OM_Job_Labour_Reassign_Available_JobsTableAdapter
            // 
            this.sp06269_OM_Job_Labour_Reassign_Available_JobsTableAdapter.ClearBeforeFill = true;
            // 
            // sp06270_OM_Job_Labour_Reassign_Linked_LabourTableAdapter
            // 
            this.sp06270_OM_Job_Labour_Reassign_Linked_LabourTableAdapter.ClearBeforeFill = true;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 2";
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Select one or more jobs from the left list to view the labour for these jobs in t" +
    "he right list. Use the buttons within the lists to adjust the labour.";
            this.barStaticItemInformation.Id = 33;
            this.barStaticItemInformation.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.ImageOptions.Image")));
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bar2
            // 
            this.bar2.BarName = "Custom 3";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.FloatLocation = new System.Drawing.Point(500, 148);
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefresh),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiPivotLayout, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiChangeTeam, true)});
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Custom 3";
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Caption = "Refresh";
            this.bbiRefresh.Id = 34;
            this.bbiRefresh.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRefresh.ImageOptions.Image")));
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Refresh Data - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to reload the list of selected jobs.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiRefresh.SuperTip = superToolTip1;
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // bbiPivotLayout
            // 
            this.bbiPivotLayout.Caption = "Rotate Layout";
            this.bbiPivotLayout.Id = 35;
            this.bbiPivotLayout.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPivotLayout.ImageOptions.Image")));
            this.bbiPivotLayout.Name = "bbiPivotLayout";
            this.bbiPivotLayout.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem2.Text = "Rotate Layout - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to switch the screen panels between horizontally split and vertically sp" +
    "lit.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiPivotLayout.SuperTip = superToolTip2;
            this.bbiPivotLayout.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiPivotLayout_ItemClick);
            // 
            // bbiChangeTeam
            // 
            this.bbiChangeTeam.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiChangeTeam.Caption = "Change Team";
            this.bbiChangeTeam.Id = 36;
            this.bbiChangeTeam.ImageOptions.Image = global::WoodPlan5.Properties.Resources.team_reassign_32;
            this.bbiChangeTeam.Name = "bbiChangeTeam";
            this.bbiChangeTeam.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image4")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image5")));
            toolTipTitleItem3.Text = "Change Team - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to change the Team for the selected Job Labour records.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiChangeTeam.SuperTip = superToolTip3;
            this.bbiChangeTeam.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiChangeTeam_ItemClick);
            // 
            // frm_OM_Job_Labour_Reassign
            // 
            this.ClientSize = new System.Drawing.Size(990, 557);
            this.Controls.Add(this.splitContainerControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_OM_Job_Labour_Reassign";
            this.Text = "Reassign Labour to Jobs";
            this.Activated += new System.EventHandler(this.frm_OM_Job_Labour_Reassign_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_OM_Job_Labour_Reassign_FormClosing);
            this.Load += new System.EventHandler(this.frm_OM_Job_Labour_Reassign_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlJob)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06269OMJobLabourReassignAvailableJobsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Job)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewJob)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric2DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditShowActiveEmployeesOnly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlLabour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06270OMJobLabourReassignLinkedLabourBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLabour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric2DP2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditChangeTeam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControlJob;
        private DataSet_AT dataSet_AT;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewJob;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit2;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private DevExpress.XtraGrid.GridControl gridControlLabour;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewLabour;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditShowActiveEmployeesOnly;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private System.Windows.Forms.BindingSource sp06269OMJobLabourReassignAvailableJobsBindingSource;
        private DataSet_OM_Job dataSet_OM_Job;
        private DevExpress.XtraGrid.Columns.GridColumn colJobID;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitID;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colJobStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colReactive;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobNoLongerRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colRequiresAccessPermit;
        private DevExpress.XtraGrid.Columns.GridColumn colAccessPermitID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPONumber;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPOID;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedStartDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedDurationUnits;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumeric2DP;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedDurationUnitsDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colScheduleSentDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActualStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActualEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActualDurationUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colActualDurationUnitsDescriptionID;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotPayContractor;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotInvoiceClient;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobStatusDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedDurationUnitsDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colActualDurationUnitsDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordType;
        private DevExpress.XtraGrid.Columns.GridColumn colSelected;
        private DevExpress.XtraGrid.Columns.GridColumn colFullDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colMaximumDaysFromLastVisit;
        private DevExpress.XtraGrid.Columns.GridColumn colMinimumDaysFromLastVisit;
        private DevExpress.XtraGrid.Columns.GridColumn colDaysLeeway;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colSitePostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationX;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationY;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToVisit;
        private DevExpress.XtraGrid.Columns.GridColumn colClientAndContract;
        private DevExpress.XtraGrid.Columns.GridColumn colRework;
        private DevExpress.XtraGrid.Columns.GridColumn colReworkOriginalJobID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedLabourCount;
        private DataSet_OM_JobTableAdapters.sp06269_OM_Job_Labour_Reassign_Available_JobsTableAdapter sp06269_OM_Job_Labour_Reassign_Available_JobsTableAdapter;
        private System.Windows.Forms.BindingSource sp06270OMJobLabourReassignLinkedLabourBindingSource;
        private DataSet_OM_JobTableAdapters.sp06270_OM_Job_Labour_Reassign_Linked_LabourTableAdapter sp06270_OM_Job_Labour_Reassign_Linked_LabourTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourUsedID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobID1;
        private DevExpress.XtraGrid.Columns.GridColumn colCostUnitsUsed;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumeric2DP2;
        private DevExpress.XtraGrid.Columns.GridColumn colCostUnitDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colSellUnitsUsed;
        private DevExpress.XtraGrid.Columns.GridColumn colSellUnitDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colCostPerUnitExVat;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraGrid.Columns.GridColumn colCostPerUnitVatRate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage;
        private DevExpress.XtraGrid.Columns.GridColumn colSellPerUnitExVat;
        private DevExpress.XtraGrid.Columns.GridColumn colSellPerUnitVatRate;
        private DevExpress.XtraGrid.Columns.GridColumn colCostValueExVat;
        private DevExpress.XtraGrid.Columns.GridColumn colCostValueVat;
        private DevExpress.XtraGrid.Columns.GridColumn colCostValueTotal;
        private DevExpress.XtraGrid.Columns.GridColumn colSellValueExVat;
        private DevExpress.XtraGrid.Columns.GridColumn colSellValueVat;
        private DevExpress.XtraGrid.Columns.GridColumn colSellValueTotal;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedStartDate1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colSelected1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colCostUnitDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colSellUnitDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourType;
        private DevExpress.XtraGrid.Columns.GridColumn colCISPercentage;
        private DevExpress.XtraGrid.Columns.GridColumn colCISValue;
        private DevExpress.XtraGrid.Columns.GridColumn colPurchaseOrderID;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitID1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraBars.BarButtonItem bbiPivotLayout;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditChangeTeam;
        private DevExpress.XtraBars.BarButtonItem bbiChangeTeam;
        private DevExpress.XtraGrid.Columns.GridColumn colPDA_JobID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonType;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonTypeID;
    }
}

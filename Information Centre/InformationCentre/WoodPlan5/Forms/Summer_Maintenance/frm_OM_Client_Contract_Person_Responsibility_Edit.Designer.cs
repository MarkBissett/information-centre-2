namespace WoodPlan5
{
    partial class frm_OM_Client_Contract_Person_Responsibility_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Client_Contract_Person_Responsibility_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.ResponsibilityTypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.sp06081OMClientContractLinkedResponsibilitiesEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Contract = new WoodPlan5.DataSet_OM_Contract();
            this.ResponsibilityTypeButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.PersonTypeDescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PersonTypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.StaffIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.StaffNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.ResponsibleForRecordTypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ResponsibleForRecordIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.LinkedToParentButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.PersonResponsibilityIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ContractStartDateTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ContractEndDateTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ItemForPersonResponsibilityID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForResponsibleForRecordID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForContractEndDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForContractStartDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForResponsibleForRecordTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStaffID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPersonTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForResponsibilityTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForLinkedToParent = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layGrpAddress = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForStaffName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPersonTypeDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForResponsibilityType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp06067OMClientContractYearEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.sp06067_OM_Client_Contract_Year_EditTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06067_OM_Client_Contract_Year_EditTableAdapter();
            this.sp06081_OM_Client_Contract_Linked_Responsibilities_EditTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06081_OM_Client_Contract_Linked_Responsibilities_EditTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ResponsibilityTypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06081OMClientContractLinkedResponsibilitiesEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResponsibilityTypeButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PersonTypeDescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PersonTypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StaffIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StaffNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResponsibleForRecordTypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResponsibleForRecordIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToParentButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PersonResponsibilityIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractStartDateTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractEndDateTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPersonResponsibilityID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForResponsibleForRecordID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForResponsibleForRecordTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStaffID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPersonTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForResponsibilityTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToParent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStaffName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPersonTypeDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForResponsibilityType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06067OMClientContractYearEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(686, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 370);
            this.barDockControlBottom.Size = new System.Drawing.Size(686, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 344);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(686, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 344);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.ImageOptions.Image")));
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.ImageOptions.Image")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageOptions.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageOptions.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(686, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 370);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(686, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 344);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(686, 26);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 344);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.ResponsibilityTypeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ResponsibilityTypeButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.PersonTypeDescriptionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PersonTypeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.StaffIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.StaffNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.ResponsibleForRecordTypeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ResponsibleForRecordIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.LinkedToParentButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.PersonResponsibilityIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ContractStartDateTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ContractEndDateTextEdit);
            this.dataLayoutControl1.DataSource = this.sp06081OMClientContractLinkedResponsibilitiesEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForResponsibleForRecordID,
            this.ItemForContractEndDate,
            this.ItemForContractStartDate,
            this.ItemForStaffID,
            this.ItemForResponsibleForRecordTypeID,
            this.ItemForPersonTypeID,
            this.ItemForPersonResponsibilityID,
            this.ItemForResponsibilityTypeID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1234, 189, 439, 593);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(686, 344);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // ResponsibilityTypeIDTextEdit
            // 
            this.ResponsibilityTypeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06081OMClientContractLinkedResponsibilitiesEditBindingSource, "ResponsibilityTypeID", true));
            this.ResponsibilityTypeIDTextEdit.Location = new System.Drawing.Point(125, 312);
            this.ResponsibilityTypeIDTextEdit.MenuManager = this.barManager1;
            this.ResponsibilityTypeIDTextEdit.Name = "ResponsibilityTypeIDTextEdit";
            this.ResponsibilityTypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ResponsibilityTypeIDTextEdit, true);
            this.ResponsibilityTypeIDTextEdit.Size = new System.Drawing.Size(549, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ResponsibilityTypeIDTextEdit, optionsSpelling1);
            this.ResponsibilityTypeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ResponsibilityTypeIDTextEdit.TabIndex = 54;
            // 
            // sp06081OMClientContractLinkedResponsibilitiesEditBindingSource
            // 
            this.sp06081OMClientContractLinkedResponsibilitiesEditBindingSource.DataMember = "sp06081_OM_Client_Contract_Linked_Responsibilities_Edit";
            this.sp06081OMClientContractLinkedResponsibilitiesEditBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // dataSet_OM_Contract
            // 
            this.dataSet_OM_Contract.DataSetName = "DataSet_OM_Contract";
            this.dataSet_OM_Contract.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ResponsibilityTypeButtonEdit
            // 
            this.ResponsibilityTypeButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06081OMClientContractLinkedResponsibilitiesEditBindingSource, "ResponsibilityType", true));
            this.ResponsibilityTypeButtonEdit.EditValue = "";
            this.ResponsibilityTypeButtonEdit.Location = new System.Drawing.Point(135, 187);
            this.ResponsibilityTypeButtonEdit.MenuManager = this.barManager1;
            this.ResponsibilityTypeButtonEdit.Name = "ResponsibilityTypeButtonEdit";
            this.ResponsibilityTypeButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click me to open the Select Person Resonsibility Type screen.", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ResponsibilityTypeButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ResponsibilityTypeButtonEdit.Size = new System.Drawing.Size(515, 20);
            this.ResponsibilityTypeButtonEdit.StyleController = this.dataLayoutControl1;
            this.ResponsibilityTypeButtonEdit.TabIndex = 14;
            this.ResponsibilityTypeButtonEdit.TabStop = false;
            this.ResponsibilityTypeButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ResponsibilityTypeButtonEdit_ButtonClick);
            this.ResponsibilityTypeButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ResponsibilityTypeButtonEdit_Validating);
            // 
            // PersonTypeDescriptionTextEdit
            // 
            this.PersonTypeDescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06081OMClientContractLinkedResponsibilitiesEditBindingSource, "PersonTypeDescription", true));
            this.PersonTypeDescriptionTextEdit.Location = new System.Drawing.Point(135, 139);
            this.PersonTypeDescriptionTextEdit.MenuManager = this.barManager1;
            this.PersonTypeDescriptionTextEdit.Name = "PersonTypeDescriptionTextEdit";
            this.PersonTypeDescriptionTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PersonTypeDescriptionTextEdit, true);
            this.PersonTypeDescriptionTextEdit.Size = new System.Drawing.Size(515, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PersonTypeDescriptionTextEdit, optionsSpelling2);
            this.PersonTypeDescriptionTextEdit.StyleController = this.dataLayoutControl1;
            this.PersonTypeDescriptionTextEdit.TabIndex = 53;
            // 
            // PersonTypeIDTextEdit
            // 
            this.PersonTypeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06081OMClientContractLinkedResponsibilitiesEditBindingSource, "PersonTypeID", true));
            this.PersonTypeIDTextEdit.Location = new System.Drawing.Point(134, 312);
            this.PersonTypeIDTextEdit.MenuManager = this.barManager1;
            this.PersonTypeIDTextEdit.Name = "PersonTypeIDTextEdit";
            this.PersonTypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PersonTypeIDTextEdit, true);
            this.PersonTypeIDTextEdit.Size = new System.Drawing.Size(540, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PersonTypeIDTextEdit, optionsSpelling3);
            this.PersonTypeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.PersonTypeIDTextEdit.TabIndex = 52;
            // 
            // StaffIDTextEdit
            // 
            this.StaffIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06081OMClientContractLinkedResponsibilitiesEditBindingSource, "StaffID", true));
            this.StaffIDTextEdit.Location = new System.Drawing.Point(173, 312);
            this.StaffIDTextEdit.MenuManager = this.barManager1;
            this.StaffIDTextEdit.Name = "StaffIDTextEdit";
            this.StaffIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.StaffIDTextEdit, true);
            this.StaffIDTextEdit.Size = new System.Drawing.Size(484, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.StaffIDTextEdit, optionsSpelling4);
            this.StaffIDTextEdit.StyleController = this.dataLayoutControl1;
            this.StaffIDTextEdit.TabIndex = 51;
            // 
            // StaffNameButtonEdit
            // 
            this.StaffNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06081OMClientContractLinkedResponsibilitiesEditBindingSource, "StaffName", true));
            this.StaffNameButtonEdit.EditValue = "";
            this.StaffNameButtonEdit.Location = new System.Drawing.Point(135, 163);
            this.StaffNameButtonEdit.MenuManager = this.barManager1;
            this.StaffNameButtonEdit.Name = "StaffNameButtonEdit";
            this.StaffNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Click me to open the Select Employee screen.", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.StaffNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.StaffNameButtonEdit.Size = new System.Drawing.Size(515, 20);
            this.StaffNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.StaffNameButtonEdit.TabIndex = 13;
            this.StaffNameButtonEdit.TabStop = false;
            this.StaffNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.StaffNameButtonEdit_ButtonClick);
            this.StaffNameButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.StaffNameButtonEdit_Validating);
            // 
            // ResponsibleForRecordTypeIDTextEdit
            // 
            this.ResponsibleForRecordTypeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06081OMClientContractLinkedResponsibilitiesEditBindingSource, "ResponsibleForRecordTypeID", true));
            this.ResponsibleForRecordTypeIDTextEdit.Location = new System.Drawing.Point(173, 312);
            this.ResponsibleForRecordTypeIDTextEdit.MenuManager = this.barManager1;
            this.ResponsibleForRecordTypeIDTextEdit.Name = "ResponsibleForRecordTypeIDTextEdit";
            this.ResponsibleForRecordTypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ResponsibleForRecordTypeIDTextEdit, true);
            this.ResponsibleForRecordTypeIDTextEdit.Size = new System.Drawing.Size(484, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ResponsibleForRecordTypeIDTextEdit, optionsSpelling5);
            this.ResponsibleForRecordTypeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ResponsibleForRecordTypeIDTextEdit.TabIndex = 49;
            // 
            // ResponsibleForRecordIDTextEdit
            // 
            this.ResponsibleForRecordIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06081OMClientContractLinkedResponsibilitiesEditBindingSource, "ResponsibleForRecordID", true));
            this.ResponsibleForRecordIDTextEdit.Location = new System.Drawing.Point(114, 59);
            this.ResponsibleForRecordIDTextEdit.MenuManager = this.barManager1;
            this.ResponsibleForRecordIDTextEdit.Name = "ResponsibleForRecordIDTextEdit";
            this.ResponsibleForRecordIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ResponsibleForRecordIDTextEdit, true);
            this.ResponsibleForRecordIDTextEdit.Size = new System.Drawing.Size(543, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ResponsibleForRecordIDTextEdit, optionsSpelling6);
            this.ResponsibleForRecordIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ResponsibleForRecordIDTextEdit.TabIndex = 44;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06081OMClientContractLinkedResponsibilitiesEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, 139);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(614, 78);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling7);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 35;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp06081OMClientContractLinkedResponsibilitiesEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(111, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(173, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 24;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // LinkedToParentButtonEdit
            // 
            this.LinkedToParentButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06081OMClientContractLinkedResponsibilitiesEditBindingSource, "LinkedToParent", true));
            this.LinkedToParentButtonEdit.EditValue = "";
            this.LinkedToParentButtonEdit.Location = new System.Drawing.Point(111, 35);
            this.LinkedToParentButtonEdit.MenuManager = this.barManager1;
            this.LinkedToParentButtonEdit.Name = "LinkedToParentButtonEdit";
            this.LinkedToParentButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Click me to open the Select Employee screen.", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.LinkedToParentButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.LinkedToParentButtonEdit.Size = new System.Drawing.Size(563, 20);
            this.LinkedToParentButtonEdit.StyleController = this.dataLayoutControl1;
            this.LinkedToParentButtonEdit.TabIndex = 6;
            this.LinkedToParentButtonEdit.TabStop = false;
            this.LinkedToParentButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.LinkedToParentButtonEdit_ButtonClick);
            this.LinkedToParentButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.LinkedToParentButtonEdit_Validating);
            // 
            // PersonResponsibilityIDTextEdit
            // 
            this.PersonResponsibilityIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06081OMClientContractLinkedResponsibilitiesEditBindingSource, "PersonResponsibilityID", true));
            this.PersonResponsibilityIDTextEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.PersonResponsibilityIDTextEdit.Location = new System.Drawing.Point(134, 312);
            this.PersonResponsibilityIDTextEdit.MenuManager = this.barManager1;
            this.PersonResponsibilityIDTextEdit.Name = "PersonResponsibilityIDTextEdit";
            this.PersonResponsibilityIDTextEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.PersonResponsibilityIDTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.PersonResponsibilityIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PersonResponsibilityIDTextEdit, true);
            this.PersonResponsibilityIDTextEdit.Size = new System.Drawing.Size(540, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PersonResponsibilityIDTextEdit, optionsSpelling8);
            this.PersonResponsibilityIDTextEdit.StyleController = this.dataLayoutControl1;
            this.PersonResponsibilityIDTextEdit.TabIndex = 27;
            // 
            // ContractStartDateTextEdit
            // 
            this.ContractStartDateTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06081OMClientContractLinkedResponsibilitiesEditBindingSource, "ContractStartDate", true));
            this.ContractStartDateTextEdit.Location = new System.Drawing.Point(114, 59);
            this.ContractStartDateTextEdit.MenuManager = this.barManager1;
            this.ContractStartDateTextEdit.Name = "ContractStartDateTextEdit";
            this.ContractStartDateTextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.ContractStartDateTextEdit.Properties.Mask.EditMask = "g";
            this.ContractStartDateTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.ContractStartDateTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ContractStartDateTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ContractStartDateTextEdit, true);
            this.ContractStartDateTextEdit.Size = new System.Drawing.Size(543, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ContractStartDateTextEdit, optionsSpelling9);
            this.ContractStartDateTextEdit.StyleController = this.dataLayoutControl1;
            this.ContractStartDateTextEdit.TabIndex = 14;
            // 
            // ContractEndDateTextEdit
            // 
            this.ContractEndDateTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06081OMClientContractLinkedResponsibilitiesEditBindingSource, "ContractEndDate", true));
            this.ContractEndDateTextEdit.Location = new System.Drawing.Point(114, 59);
            this.ContractEndDateTextEdit.MenuManager = this.barManager1;
            this.ContractEndDateTextEdit.Name = "ContractEndDateTextEdit";
            this.ContractEndDateTextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.ContractEndDateTextEdit.Properties.Mask.EditMask = "g";
            this.ContractEndDateTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.ContractEndDateTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ContractEndDateTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ContractEndDateTextEdit, true);
            this.ContractEndDateTextEdit.Size = new System.Drawing.Size(543, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ContractEndDateTextEdit, optionsSpelling10);
            this.ContractEndDateTextEdit.StyleController = this.dataLayoutControl1;
            this.ContractEndDateTextEdit.TabIndex = 15;
            // 
            // ItemForPersonResponsibilityID
            // 
            this.ItemForPersonResponsibilityID.Control = this.PersonResponsibilityIDTextEdit;
            this.ItemForPersonResponsibilityID.CustomizationFormText = "Person Responsibility ID:";
            this.ItemForPersonResponsibilityID.Location = new System.Drawing.Point(0, 67);
            this.ItemForPersonResponsibilityID.Name = "ItemForPersonResponsibilityID";
            this.ItemForPersonResponsibilityID.Size = new System.Drawing.Size(666, 24);
            this.ItemForPersonResponsibilityID.Text = "Person Responsibility ID:";
            this.ItemForPersonResponsibilityID.TextSize = new System.Drawing.Size(84, 13);
            // 
            // ItemForResponsibleForRecordID
            // 
            this.ItemForResponsibleForRecordID.Control = this.ResponsibleForRecordIDTextEdit;
            this.ItemForResponsibleForRecordID.CustomizationFormText = "Client Contract ID:";
            this.ItemForResponsibleForRecordID.Location = new System.Drawing.Point(0, 47);
            this.ItemForResponsibleForRecordID.Name = "ItemForResponsibleForRecordID";
            this.ItemForResponsibleForRecordID.Size = new System.Drawing.Size(649, 24);
            this.ItemForResponsibleForRecordID.Text = "Client Contract ID:";
            this.ItemForResponsibleForRecordID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForContractEndDate
            // 
            this.ItemForContractEndDate.Control = this.ContractEndDateTextEdit;
            this.ItemForContractEndDate.CustomizationFormText = "Contract End Date:";
            this.ItemForContractEndDate.Location = new System.Drawing.Point(0, 47);
            this.ItemForContractEndDate.Name = "ItemForContractEndDate";
            this.ItemForContractEndDate.Size = new System.Drawing.Size(649, 24);
            this.ItemForContractEndDate.Text = "Contract End Date:";
            this.ItemForContractEndDate.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForContractStartDate
            // 
            this.ItemForContractStartDate.Control = this.ContractStartDateTextEdit;
            this.ItemForContractStartDate.CustomizationFormText = "Contract Start Date:";
            this.ItemForContractStartDate.Location = new System.Drawing.Point(0, 47);
            this.ItemForContractStartDate.Name = "ItemForContractStartDate";
            this.ItemForContractStartDate.Size = new System.Drawing.Size(649, 24);
            this.ItemForContractStartDate.Text = "Contract Start Date:";
            this.ItemForContractStartDate.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForResponsibleForRecordTypeID
            // 
            this.ItemForResponsibleForRecordTypeID.Control = this.ResponsibleForRecordTypeIDTextEdit;
            this.ItemForResponsibleForRecordTypeID.CustomizationFormText = "Responsible For Record Type ID:";
            this.ItemForResponsibleForRecordTypeID.Location = new System.Drawing.Point(0, 82);
            this.ItemForResponsibleForRecordTypeID.Name = "ItemForResponsibleForRecordTypeID";
            this.ItemForResponsibleForRecordTypeID.Size = new System.Drawing.Size(649, 24);
            this.ItemForResponsibleForRecordTypeID.Text = "Responsible For Record Type ID:";
            this.ItemForResponsibleForRecordTypeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForStaffID
            // 
            this.ItemForStaffID.Control = this.StaffIDTextEdit;
            this.ItemForStaffID.CustomizationFormText = "Staff ID:";
            this.ItemForStaffID.Location = new System.Drawing.Point(0, 106);
            this.ItemForStaffID.Name = "ItemForStaffID";
            this.ItemForStaffID.Size = new System.Drawing.Size(649, 24);
            this.ItemForStaffID.Text = "Staff ID:";
            this.ItemForStaffID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForPersonTypeID
            // 
            this.ItemForPersonTypeID.Control = this.PersonTypeIDTextEdit;
            this.ItemForPersonTypeID.Location = new System.Drawing.Point(0, 67);
            this.ItemForPersonTypeID.Name = "ItemForPersonTypeID";
            this.ItemForPersonTypeID.Size = new System.Drawing.Size(666, 24);
            this.ItemForPersonTypeID.Text = "Person Type ID:";
            this.ItemForPersonTypeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForResponsibilityTypeID
            // 
            this.ItemForResponsibilityTypeID.Control = this.ResponsibilityTypeIDTextEdit;
            this.ItemForResponsibilityTypeID.Location = new System.Drawing.Point(0, 67);
            this.ItemForResponsibilityTypeID.Name = "ItemForResponsibilityTypeID";
            this.ItemForResponsibilityTypeID.Size = new System.Drawing.Size(666, 24);
            this.ItemForResponsibilityTypeID.Text = "Responsibility Type ID:";
            this.ItemForResponsibilityTypeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(686, 344);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForLinkedToParent,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.emptySpaceItem4,
            this.layoutControlGroup6});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(666, 233);
            // 
            // ItemForLinkedToParent
            // 
            this.ItemForLinkedToParent.AllowHide = false;
            this.ItemForLinkedToParent.Control = this.LinkedToParentButtonEdit;
            this.ItemForLinkedToParent.CustomizationFormText = "Linked To:";
            this.ItemForLinkedToParent.Location = new System.Drawing.Point(0, 23);
            this.ItemForLinkedToParent.Name = "ItemForLinkedToParent";
            this.ItemForLinkedToParent.Size = new System.Drawing.Size(666, 24);
            this.ItemForLinkedToParent.Text = "Linked to:";
            this.ItemForLinkedToParent.TextSize = new System.Drawing.Size(96, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(99, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(99, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(99, 23);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(276, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(390, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(99, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(177, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 47);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(666, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Details";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 57);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(666, 176);
            this.layoutControlGroup6.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layGrpAddress;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(642, 130);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layGrpAddress,
            this.layoutControlGroup4});
            // 
            // layGrpAddress
            // 
            this.layGrpAddress.CustomizationFormText = "Address";
            this.layGrpAddress.ExpandButtonVisible = true;
            this.layGrpAddress.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layGrpAddress.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem5,
            this.ItemForStaffName,
            this.ItemForPersonTypeDescription,
            this.ItemForResponsibilityType});
            this.layGrpAddress.Location = new System.Drawing.Point(0, 0);
            this.layGrpAddress.Name = "layGrpAddress";
            this.layGrpAddress.Size = new System.Drawing.Size(618, 82);
            this.layGrpAddress.Text = "Details";
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 72);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(618, 10);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForStaffName
            // 
            this.ItemForStaffName.Control = this.StaffNameButtonEdit;
            this.ItemForStaffName.CustomizationFormText = "Person Name:";
            this.ItemForStaffName.Location = new System.Drawing.Point(0, 24);
            this.ItemForStaffName.Name = "ItemForStaffName";
            this.ItemForStaffName.Size = new System.Drawing.Size(618, 24);
            this.ItemForStaffName.Text = "Person Name:";
            this.ItemForStaffName.TextSize = new System.Drawing.Size(96, 13);
            // 
            // ItemForPersonTypeDescription
            // 
            this.ItemForPersonTypeDescription.Control = this.PersonTypeDescriptionTextEdit;
            this.ItemForPersonTypeDescription.Location = new System.Drawing.Point(0, 0);
            this.ItemForPersonTypeDescription.Name = "ItemForPersonTypeDescription";
            this.ItemForPersonTypeDescription.Size = new System.Drawing.Size(618, 24);
            this.ItemForPersonTypeDescription.Text = "Person Type:";
            this.ItemForPersonTypeDescription.TextSize = new System.Drawing.Size(96, 13);
            // 
            // ItemForResponsibilityType
            // 
            this.ItemForResponsibilityType.Control = this.ResponsibilityTypeButtonEdit;
            this.ItemForResponsibilityType.Location = new System.Drawing.Point(0, 48);
            this.ItemForResponsibilityType.Name = "ItemForResponsibilityType";
            this.ItemForResponsibilityType.Size = new System.Drawing.Size(618, 24);
            this.ItemForResponsibilityType.Text = "Responsibility Type:";
            this.ItemForResponsibilityType.TextSize = new System.Drawing.Size(96, 13);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup4.CaptionImageOptions.Image")));
            this.layoutControlGroup4.CustomizationFormText = "Remarks";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(618, 82);
            this.layoutControlGroup4.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Comments:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(618, 82);
            this.ItemForRemarks.Text = "Comments:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AllowDrawBackground = false;
            this.layoutControlGroup3.CustomizationFormText = "autoGeneratedGroup1";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem3});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 233);
            this.layoutControlGroup3.Name = "autoGeneratedGroup1";
            this.layoutControlGroup3.Size = new System.Drawing.Size(666, 91);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(666, 91);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp06067OMClientContractYearEditBindingSource
            // 
            this.sp06067OMClientContractYearEditBindingSource.DataMember = "sp06067_OM_Client_Contract_Year_Edit";
            this.sp06067OMClientContractYearEditBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp06067_OM_Client_Contract_Year_EditTableAdapter
            // 
            this.sp06067_OM_Client_Contract_Year_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp06081_OM_Client_Contract_Linked_Responsibilities_EditTableAdapter
            // 
            this.sp06081_OM_Client_Contract_Linked_Responsibilities_EditTableAdapter.ClearBeforeFill = true;
            // 
            // frm_OM_Client_Contract_Person_Responsibility_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(686, 400);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_OM_Client_Contract_Person_Responsibility_Edit";
            this.Text = "Edit Client Contract Person Responsibility";
            this.Activated += new System.EventHandler(this.frm_OM_Client_Contract_Person_Responsibility_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_OM_Client_Contract_Person_Responsibility_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_OM_Client_Contract_Person_Responsibility_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ResponsibilityTypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06081OMClientContractLinkedResponsibilitiesEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResponsibilityTypeButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PersonTypeDescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PersonTypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StaffIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StaffNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResponsibleForRecordTypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResponsibleForRecordIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToParentButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PersonResponsibilityIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractStartDateTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractEndDateTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPersonResponsibilityID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForResponsibleForRecordID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForResponsibleForRecordTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStaffID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPersonTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForResponsibilityTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToParent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStaffName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPersonTypeDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForResponsibilityType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06067OMClientContractYearEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedToParent;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.ButtonEdit LinkedToParentButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPersonResponsibilityID;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraEditors.TextEdit PersonResponsibilityIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layGrpAddress;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DataSet_OM_Contract dataSet_OM_Contract;
        private DevExpress.XtraEditors.TextEdit ResponsibleForRecordIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForResponsibleForRecordID;
        private System.Windows.Forms.BindingSource sp06067OMClientContractYearEditBindingSource;
        private DataSet_OM_ContractTableAdapters.sp06067_OM_Client_Contract_Year_EditTableAdapter sp06067_OM_Client_Contract_Year_EditTableAdapter;
        private DevExpress.XtraEditors.TextEdit ContractStartDateTextEdit;
        private DevExpress.XtraEditors.TextEdit ContractEndDateTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContractStartDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContractEndDate;
        private System.Windows.Forms.BindingSource sp06081OMClientContractLinkedResponsibilitiesEditBindingSource;
        private DataSet_OM_ContractTableAdapters.sp06081_OM_Client_Contract_Linked_Responsibilities_EditTableAdapter sp06081_OM_Client_Contract_Linked_Responsibilities_EditTableAdapter;
        private DevExpress.XtraEditors.TextEdit ResponsibleForRecordTypeIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForResponsibleForRecordTypeID;
        private DevExpress.XtraEditors.ButtonEdit StaffNameButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStaffName;
        private DevExpress.XtraEditors.TextEdit StaffIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStaffID;
        private DevExpress.XtraEditors.TextEdit PersonTypeIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPersonTypeID;
        private DevExpress.XtraEditors.TextEdit PersonTypeDescriptionTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPersonTypeDescription;
        private DevExpress.XtraEditors.ButtonEdit ResponsibilityTypeButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForResponsibilityType;
        private DevExpress.XtraEditors.TextEdit ResponsibilityTypeIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForResponsibilityTypeID;
    }
}

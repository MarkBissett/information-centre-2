﻿namespace WoodPlan5
{
    partial class frm_OM_Site_Contract_Wizard_Block_Edit_Responsible_Person
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Site_Contract_Wizard_Block_Edit_Responsible_Person));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.PersonTypeDescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ResponsibilityTypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ResponsibilityTypeButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.PersonTypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.StaffIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.StaffNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ItemForStaffID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPersonTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForResponsibilityTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStaffName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForResponsibilityType = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPersonTypeDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bsiInformation = new DevExpress.XtraBars.BarStaticItem();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PersonTypeDescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResponsibilityTypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResponsibilityTypeButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PersonTypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StaffIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StaffNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStaffID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPersonTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForResponsibilityTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStaffName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForResponsibilityType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPersonTypeDescription)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(515, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 333);
            this.barDockControlBottom.Size = new System.Drawing.Size(515, 30);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 307);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(515, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 307);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bsiInformation});
            this.barManager1.MaxItemId = 31;
            this.barManager1.StatusBar = this.bar1;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutControl1.Controls.Add(this.PersonTypeDescriptionTextEdit);
            this.layoutControl1.Controls.Add(this.ResponsibilityTypeIDTextEdit);
            this.layoutControl1.Controls.Add(this.ResponsibilityTypeButtonEdit);
            this.layoutControl1.Controls.Add(this.PersonTypeIDTextEdit);
            this.layoutControl1.Controls.Add(this.StaffIDTextEdit);
            this.layoutControl1.Controls.Add(this.StaffNameButtonEdit);
            this.layoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.layoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForStaffID,
            this.ItemForPersonTypeID,
            this.ItemForResponsibilityTypeID});
            this.layoutControl1.Location = new System.Drawing.Point(0, 28);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1213, 293, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(514, 275);
            this.layoutControl1.TabIndex = 4;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // PersonTypeDescriptionTextEdit
            // 
            this.PersonTypeDescriptionTextEdit.Location = new System.Drawing.Point(112, 12);
            this.PersonTypeDescriptionTextEdit.MenuManager = this.barManager1;
            this.PersonTypeDescriptionTextEdit.Name = "PersonTypeDescriptionTextEdit";
            this.PersonTypeDescriptionTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PersonTypeDescriptionTextEdit, true);
            this.PersonTypeDescriptionTextEdit.Size = new System.Drawing.Size(390, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PersonTypeDescriptionTextEdit, optionsSpelling6);
            this.PersonTypeDescriptionTextEdit.StyleController = this.layoutControl1;
            this.PersonTypeDescriptionTextEdit.TabIndex = 26;
            // 
            // ResponsibilityTypeIDTextEdit
            // 
            this.ResponsibilityTypeIDTextEdit.Location = new System.Drawing.Point(125, 243);
            this.ResponsibilityTypeIDTextEdit.MenuManager = this.barManager1;
            this.ResponsibilityTypeIDTextEdit.Name = "ResponsibilityTypeIDTextEdit";
            this.ResponsibilityTypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ResponsibilityTypeIDTextEdit, true);
            this.ResponsibilityTypeIDTextEdit.Size = new System.Drawing.Size(377, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ResponsibilityTypeIDTextEdit, optionsSpelling1);
            this.ResponsibilityTypeIDTextEdit.StyleController = this.layoutControl1;
            this.ResponsibilityTypeIDTextEdit.TabIndex = 25;
            // 
            // ResponsibilityTypeButtonEdit
            // 
            this.ResponsibilityTypeButtonEdit.Location = new System.Drawing.Point(112, 60);
            this.ResponsibilityTypeButtonEdit.MenuManager = this.barManager1;
            this.ResponsibilityTypeButtonEdit.Name = "ResponsibilityTypeButtonEdit";
            this.ResponsibilityTypeButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, "Click me to open the Select  Person Responsibility Type screen.", "choose", null, true)});
            this.ResponsibilityTypeButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ResponsibilityTypeButtonEdit.Size = new System.Drawing.Size(390, 20);
            this.ResponsibilityTypeButtonEdit.StyleController = this.layoutControl1;
            this.ResponsibilityTypeButtonEdit.TabIndex = 15;
            this.ResponsibilityTypeButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ResponsibilityTypeIDButtonEdit_ButtonClick);
            // 
            // PersonTypeIDTextEdit
            // 
            this.PersonTypeIDTextEdit.Location = new System.Drawing.Point(112, 243);
            this.PersonTypeIDTextEdit.MenuManager = this.barManager1;
            this.PersonTypeIDTextEdit.Name = "PersonTypeIDTextEdit";
            this.PersonTypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PersonTypeIDTextEdit, true);
            this.PersonTypeIDTextEdit.Size = new System.Drawing.Size(390, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PersonTypeIDTextEdit, optionsSpelling2);
            this.PersonTypeIDTextEdit.StyleController = this.layoutControl1;
            this.PersonTypeIDTextEdit.TabIndex = 24;
            // 
            // StaffIDTextEdit
            // 
            this.StaffIDTextEdit.Location = new System.Drawing.Point(130, 12);
            this.StaffIDTextEdit.MenuManager = this.barManager1;
            this.StaffIDTextEdit.Name = "StaffIDTextEdit";
            this.StaffIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.StaffIDTextEdit, true);
            this.StaffIDTextEdit.Size = new System.Drawing.Size(372, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.StaffIDTextEdit, optionsSpelling3);
            this.StaffIDTextEdit.StyleController = this.layoutControl1;
            this.StaffIDTextEdit.TabIndex = 23;
            // 
            // StaffNameButtonEdit
            // 
            this.StaffNameButtonEdit.Location = new System.Drawing.Point(112, 36);
            this.StaffNameButtonEdit.MenuManager = this.barManager1;
            this.StaffNameButtonEdit.Name = "StaffNameButtonEdit";
            this.StaffNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "Click me to open the Select Material screen", "choose", null, true)});
            this.StaffNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.StaffNameButtonEdit.Size = new System.Drawing.Size(390, 20);
            this.StaffNameButtonEdit.StyleController = this.layoutControl1;
            this.StaffNameButtonEdit.TabIndex = 0;
            this.StaffNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.StaffNameButtonEdit_ButtonClick);
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.Location = new System.Drawing.Point(112, 84);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.RemarksMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(390, 179);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling4);
            this.RemarksMemoEdit.StyleController = this.layoutControl1;
            this.RemarksMemoEdit.TabIndex = 11;
            // 
            // ItemForStaffID
            // 
            this.ItemForStaffID.Control = this.StaffIDTextEdit;
            this.ItemForStaffID.CustomizationFormText = "Staff ID:";
            this.ItemForStaffID.Location = new System.Drawing.Point(0, 0);
            this.ItemForStaffID.Name = "ItemForStaffID";
            this.ItemForStaffID.Size = new System.Drawing.Size(494, 24);
            this.ItemForStaffID.Text = "Staff ID:";
            this.ItemForStaffID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForPersonTypeID
            // 
            this.ItemForPersonTypeID.Control = this.PersonTypeIDTextEdit;
            this.ItemForPersonTypeID.Location = new System.Drawing.Point(0, 231);
            this.ItemForPersonTypeID.Name = "ItemForPersonTypeID";
            this.ItemForPersonTypeID.Size = new System.Drawing.Size(494, 24);
            this.ItemForPersonTypeID.Text = "Person Type ID:";
            this.ItemForPersonTypeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForResponsibilityTypeID
            // 
            this.ItemForResponsibilityTypeID.Control = this.ResponsibilityTypeIDTextEdit;
            this.ItemForResponsibilityTypeID.Location = new System.Drawing.Point(0, 231);
            this.ItemForResponsibilityTypeID.Name = "ItemForResponsibilityTypeID";
            this.ItemForResponsibilityTypeID.Size = new System.Drawing.Size(494, 24);
            this.ItemForResponsibilityTypeID.Text = "Responsibility Type ID:";
            this.ItemForResponsibilityTypeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks,
            this.ItemForStaffName,
            this.ItemForResponsibilityType,
            this.ItemForPersonTypeDescription});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(514, 275);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 72);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(494, 183);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(97, 13);
            // 
            // ItemForStaffName
            // 
            this.ItemForStaffName.Control = this.StaffNameButtonEdit;
            this.ItemForStaffName.CustomizationFormText = "Responsible Person:";
            this.ItemForStaffName.Location = new System.Drawing.Point(0, 24);
            this.ItemForStaffName.Name = "ItemForStaffName";
            this.ItemForStaffName.Size = new System.Drawing.Size(494, 24);
            this.ItemForStaffName.Text = "Responsible Person:";
            this.ItemForStaffName.TextSize = new System.Drawing.Size(97, 13);
            // 
            // ItemForResponsibilityType
            // 
            this.ItemForResponsibilityType.Control = this.ResponsibilityTypeButtonEdit;
            this.ItemForResponsibilityType.Location = new System.Drawing.Point(0, 48);
            this.ItemForResponsibilityType.Name = "ItemForResponsibilityType";
            this.ItemForResponsibilityType.Size = new System.Drawing.Size(494, 24);
            this.ItemForResponsibilityType.Text = "Responsibility Type:";
            this.ItemForResponsibilityType.TextSize = new System.Drawing.Size(97, 13);
            // 
            // ItemForPersonTypeDescription
            // 
            this.ItemForPersonTypeDescription.Control = this.PersonTypeDescriptionTextEdit;
            this.ItemForPersonTypeDescription.Location = new System.Drawing.Point(0, 0);
            this.ItemForPersonTypeDescription.Name = "ItemForPersonTypeDescription";
            this.ItemForPersonTypeDescription.Size = new System.Drawing.Size(494, 24);
            this.ItemForPersonTypeDescription.Text = "Person Type:";
            this.ItemForPersonTypeDescription.TextSize = new System.Drawing.Size(97, 13);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(295, 303);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(96, 22);
            this.btnOK.TabIndex = 13;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(406, 303);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(96, 22);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 3";
            this.bar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiInformation)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 3";
            // 
            // bsiInformation
            // 
            this.bsiInformation.Caption = "Complete the fields displayed. Any fields left blank will not be overwritten.";
            this.bsiInformation.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiInformation.Glyph")));
            this.bsiInformation.Id = 30;
            this.bsiInformation.Name = "bsiInformation";
            this.bsiInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bsiInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // frm_OM_Site_Contract_Wizard_Block_Edit_Responsible_Person
            // 
            this.ClientSize = new System.Drawing.Size(515, 363);
            this.ControlBox = false;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_OM_Site_Contract_Wizard_Block_Edit_Responsible_Person";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Contract Wizard - Block Edit Person Responsibilities";
            this.Load += new System.EventHandler(this.frm_OM_Site_Contract_Wizard_Block_Edit_Responsible_Person_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PersonTypeDescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResponsibilityTypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResponsibilityTypeButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PersonTypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StaffIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StaffNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStaffID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPersonTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForResponsibilityTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStaffName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForResponsibilityType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPersonTypeDescription)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraEditors.TextEdit StaffIDTextEdit;
        private DevExpress.XtraEditors.ButtonEdit StaffNameButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStaffID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStaffName;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarStaticItem bsiInformation;
        private DevExpress.XtraEditors.TextEdit PersonTypeIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPersonTypeID;
        private DevExpress.XtraEditors.ButtonEdit ResponsibilityTypeButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForResponsibilityType;
        private DevExpress.XtraEditors.TextEdit PersonTypeDescriptionTextEdit;
        private DevExpress.XtraEditors.TextEdit ResponsibilityTypeIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForResponsibilityTypeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPersonTypeDescription;
    }
}

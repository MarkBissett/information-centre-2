using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;  // Required for Path Statement //
using System.Xml;
using System.Linq;
using System.Xml.Linq;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraBars;
using DevExpress.XtraTab;
using DevExpress.XtraBars.Docking;

using DevExpress.Utils.Menu;  // Required to disable Column Chooser on Grids //
using DevExpress.XtraGrid.Localization;  // Required to disable Column Chooser on Grids //
using DevExpress.XtraScheduler;

using DevExpress.XtraReports;
using DevExpress.XtraReports.UserDesigner;
using DevExpress.XtraReports.Design;
using DevExpress.XtraReports.UI;
using System.ComponentModel.Design;  // Used by process to auto link event for custom sorting when a object is dropped onto the design panel of the report //

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;
using WoodPlan5.Reports;
using WoodPlan5.Classes.Operations;
using LocusEffects;
using Utilities;  // Used by Datasets //

namespace WoodPlan5
{
    public partial class frm_OM_Team_Self_Billing_Manager : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        private string strConnectionStringREADONLY = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;
        bool iBool_TemplateManager = false;
        bool iBool_BillingRequirementTemplateManager = false;
        bool iBool_InvoiceLayourDesigner = false;
        bool iBool_AllowDeleteInvoice = false;

        private Utils.enmFocusedGrid _enmFocusedGrid = Utils.enmFocusedGrid.Jobs;

        bool iBool_EnableGridColumnChooser = true;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        public RefreshGridState RefreshGridViewStateLabour;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewStateVisit;
        public RefreshGridState RefreshGridViewStateJob;
        public RefreshGridState RefreshGridViewStateBillingRequirement;
        public RefreshGridState RefreshGridViewStateBillingException;
        public RefreshGridState RefreshGridViewStateLabour2;
        public RefreshGridState RefreshGridViewStateInvoice;

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //
        RepositoryItemMemoExEdit disabledEditor;  // Used to conditionally disable editors in GridControlVisit //
        RepositoryItemMemoExEdit enabledEditor;  // Used to conditionally enable editors in GridControlVisit //

        string i_str_AddedRecordIDsVisit = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDsJob = "";
        string i_str_AddedRecordIDsBillingRequirement = "";
        string i_str_AddedRecordIDsBillingException = "";

        string i_str_selected_Labour_Team_ids = "";
        string i_str_selected_Labour_Team_descriptions = "";
        BaseObjects.GridCheckMarksSelection selection12;

        string i_str_selected_VisitType_ids = "";
        string i_str_selected_VisitTypes = "";
        BaseObjects.GridCheckMarksSelection selection2;

        string i_str_selected_VisitCategory_ids = "";
        string i_str_selected_VisitCategories = "";
        BaseObjects.GridCheckMarksSelection selection4;

        string i_str_selected_Warning_ids = "";
        string i_str_selected_Warnings = "";
        BaseObjects.GridCheckMarksSelection selection1;

        string i_str_selected_invoice_numbers = "";
        BaseObjects.GridCheckMarksSelection selection3;

        string i_str_selected_client_ids = "";
        string i_str_selected_client_names = "";
        string i_str_selected_site_ids = "";
        string i_str_selected_site_names = "";
        string i_str_selected_exclude_client_ids = "";
        string i_str_selected_exclude_client_names = "";

        private string strSignaturePath = "";  // Holds the first part of the path, retrieved from the System_Settings table //
        private string strDefaultPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        public string strPassedInDrillDownIDs = "";
        //private string strVisitIDsToWorkWith = "";

        // Holds all the visits to work with while the screen is open //
        SqlDataAdapter sdaVisitsToWorkWith= null;
        DataSet dsVisitsToWorkWith = null;

        private DateTime i_dtStart = DateTime.MinValue;  // Used if Panel is Date Range //
        private DateTime i_dtEnd = DateTime.MaxValue;  // Used if Panel is Date Range //

        private LocusEffects.LocusEffectsProvider locusEffectsProvider1;
        private ArrowLocusEffect m_customArrowLocusEffect1 = null;

        SuperToolTip superToolTipSiteContractFilter = null;
        SuperToolTipSetupArgs superToolTipSetupArgsSiteContractFilter = null;

        SuperToolTip superToolTipClientContractFilter = null;
        SuperToolTipSetupArgs superToolTipSetupArgsClientContractFilter = null;

        SuperToolTip superToolTipExcludedClientContractFilter = null;
        SuperToolTipSetupArgs superToolTipSetupArgsExcludedClientContractFilter = null;

        SuperToolTip superToolTipInvoicingParameters = null;
        SuperToolTipSetupArgs superToolTipSetupArgsInvoicingParameters = null;

        private DataSet_Selection DS_Selection1;

        WaitDialogForm loadingForm;
        rpt_OM_Team_Self_Billing_Invoice rptReport;
        public string i_str_SavedDirectoryName = "";
        public string i_str_SavedSelfBillingInvoiceFolder = "";

        private string strEditedVisitIDs = "";  // Used to merge updated changes back into the original dataset without reloading the whole dataset //

        private int intBillingRequirementLinkedRecordTypeID = 3;  // 3 = Visits //

        private DateTime i_dtHistoricalStart = DateTime.MinValue;
        private DateTime i_dtHistoricalEnd = DateTime.MaxValue;

        bool boolFirstLoadOfTeamsGrid = true;
        bool boolFirstLoadOfHistoricalTeamsGrid = true;

        #endregion

        public frm_OM_Team_Self_Billing_Manager()
        {
            InitializeComponent();
        }

        private void frm_OM_Team_Self_Billing_Manager_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 7020;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;
            strConnectionStringREADONLY = GlobalSettings.ConnectionStringREADONLY;

            Set_Grid_Highlighter_Transparent(this.Controls);

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, FormID, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);
            ProcessPermissionsForForm();

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                i_str_SavedDirectoryName = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_ReportLayouts").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for the Team Self-Billing Layouts (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Report Layouts Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (!i_str_SavedDirectoryName.EndsWith("\\")) i_str_SavedDirectoryName += "\\";  // Add Backslash to end //

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                i_str_SavedSelfBillingInvoiceFolder = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_TeamSelfBillingInvoicesSavedFolder").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for the Saved Team Self-Billing Invoices (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Saved Team Self-Billing Invoices Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (!i_str_SavedSelfBillingInvoiceFolder.EndsWith("\\")) i_str_SavedSelfBillingInvoiceFolder += "\\";  // Add Backslash to end //

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionStringREADONLY);
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_PictureFilesFolder").ToString();
                strSignaturePath = strDefaultPath;
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for Client Pictures and Job Pictures (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Linked Job Pictures Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            sdaVisitsToWorkWith = new SqlDataAdapter();
            dsVisitsToWorkWith = new DataSet("NewDataSet");

            DS_Selection1 = new Utilities.DataSet_Selection((GridView)gridViewJob, strConnectionString);  // Used by DataSets //
            
            xtraTabPageInvoiceDesigner.PageEnabled = iBool_InvoiceLayourDesigner;  // Controlled by Permissions //
            bbiDeleteInvoice.Enabled = iBool_AllowDeleteInvoice;  // Controlled by Permissions //


            sp06452_OM_Billing_Manager_LabourTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY + "Connect Timeout=3600;";
            RefreshGridViewStateLabour = new RefreshGridState(gridViewLabour, "LabourID");
            gridControlLabour.ForceInitialize();

            sp06445_OM_Team_Self_Billing_Manager_VisitsTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            RefreshGridViewStateVisit = new RefreshGridState(gridViewVisit, "VisitID");
            gridControlVisit.ForceInitialize();

            #region Filter Panel

            buttonEditClientFilter.EditValue = "";

            checkEditWeekNumber.Checked = true;
            spinEditWeekNumber.EditValue = BaseObjects.DateFunctions.GetIso8601WeekOfYear(DateTime.Today);
            spinEditYear.EditValue = DateTime.Today.Year;
            Set_Date_Filter_Controls_ReadOnly(false);

            Set_Historical_Date_Range(DateTime.Today.AddMonths(-3), DateTime.Today.AddDays(2).AddMilliseconds(-5));

            sp06028_OM_Labour_FilterTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            sp06028_OM_Labour_FilterTableAdapter.Fill(dataSet_GC_Summer_Core.sp06028_OM_Labour_Filter, (checkEditTeam.Checked ? 1 : 0));
            gridControl12.ForceInitialize();
            selection12 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl12.MainView);
            selection12.CheckMarkColumn.VisibleIndex = 0;
            selection12.CheckMarkColumn.Width = 30;

            sp06308_OM_Visit_TypesTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            sp06308_OM_Visit_TypesTableAdapter.Fill(dataSet_OM_Visit.sp06308_OM_Visit_Types, 0);
            gridControl2.ForceInitialize();
            selection2 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl2.MainView);
            selection2.CheckMarkColumn.VisibleIndex = 0;
            selection2.CheckMarkColumn.Width = 30;

            sp06528_OM_Visit_Categories_FilterTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            sp06528_OM_Visit_Categories_FilterTableAdapter.Fill(dataSet_OM_Core.sp06528_OM_Visit_Categories_Filter, 0);
            gridControl4.ForceInitialize();
            selection4 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl4.MainView);
            selection4.CheckMarkColumn.VisibleIndex = 0;
            selection4.CheckMarkColumn.Width = 30;

            sp06451_OM_Self_Billing_Billing_Warnings_FilterTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            sp06451_OM_Self_Billing_Billing_Warnings_FilterTableAdapter.Fill(dataSet_OM_Billing.sp06451_OM_Self_Billing_Billing_Warnings_Filter);
            gridControl1.ForceInitialize();
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;

            sp06477_OM_Get_Unique_Self_Billing_Invoice_ListTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            sp06477_OM_Get_Unique_Self_Billing_Invoice_ListTableAdapter.Fill(dataSet_OM_Billing.sp06477_OM_Get_Unique_Self_Billing_Invoice_List, null, null);
            gridControl3.ForceInitialize();
            selection3 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl3.MainView);
            selection3.CheckMarkColumn.VisibleIndex = 0;
            selection3.CheckMarkColumn.Width = 30;

            #endregion

            sp06454_OM_Self_Billing_Jobs_Linked_to_VisitsTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            RefreshGridViewStateJob = new RefreshGridState(gridViewJob, "JobID");

            sp06446_OM_Team_Self_Billing_Billing_RequirementsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateBillingRequirement = new RefreshGridState(gridViewBillingRequirement, "BillingRequirementID");

            sp06448_OM_Team_Self_Billing_Billing_ExceptionsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateBillingException = new RefreshGridState(gridViewBillingException, "BillingWarningID");

            sp06463_OM_Billing_Manager_Invoiced_LabourTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateLabour2 = new RefreshGridState(gridViewLabour2, "LabourID");

            sp06464_OM_Billing_Manager_Invoices_For_LabourTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateInvoice = new RefreshGridState(gridViewInvoice, "SelfBillingInvoiceHeaderID");


            if (strPassedInDrillDownIDs != "")  // Opened in drill-down mode //
            {
                popupContainerEditDateRange.Text = "Custom Filter";
                buttonEditClientFilter.Text = "Custom Filter";
                buttonEditSiteFilter.Text = "Custom Filter";
                popupContainerEditLabourFilter.Text = "Custom Filter";

                Load_Data();  // Load records //
            }
            popupContainerControlLabourFilter.Size = new System.Drawing.Size(270, 500);
            popupContainerControlWarningFilter.Size = new System.Drawing.Size(270, 150);
            popupContainerControlVisitTypeFilter.Size = new System.Drawing.Size(270, 400);
            popupContainerControlVisitCategoryFilter.Size = new System.Drawing.Size(270, 400);

            // Create a SuperToolTip //
            superToolTipSiteContractFilter = new SuperToolTip();
            superToolTipSetupArgsSiteContractFilter = new SuperToolTipSetupArgs();
            superToolTipSetupArgsSiteContractFilter.Title.Text = "Site Contract Filter - Information";
            superToolTipSetupArgsSiteContractFilter.Title.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            superToolTipSetupArgsSiteContractFilter.Contents.Text = "I store the currently applied Site Filter.\n\nNo Filter Set.";
            superToolTipSetupArgsSiteContractFilter.ShowFooterSeparator = false;
            superToolTipSetupArgsSiteContractFilter.Footer.Text = "";
            superToolTipSiteContractFilter.Setup(superToolTipSetupArgsSiteContractFilter);
            superToolTipSiteContractFilter.AllowHtmlText = DefaultBoolean.True;
            buttonEditSiteFilter.SuperTip = superToolTipSiteContractFilter;

            // Create a SuperToolTip //
            superToolTipClientContractFilter = new SuperToolTip();
            superToolTipSetupArgsClientContractFilter = new SuperToolTipSetupArgs();
            superToolTipSetupArgsClientContractFilter.Title.Text = "Client Contract Filter - Information";
            superToolTipSetupArgsClientContractFilter.Title.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            superToolTipSetupArgsClientContractFilter.Contents.Text = "I store the currently applied Client Filter.\n\nNo Filter Set.";
            superToolTipSetupArgsClientContractFilter.ShowFooterSeparator = false;
            superToolTipSetupArgsClientContractFilter.Footer.Text = "";
            superToolTipClientContractFilter.Setup(superToolTipSetupArgsClientContractFilter);
            superToolTipClientContractFilter.AllowHtmlText = DefaultBoolean.True;
            buttonEditClientFilter.SuperTip = superToolTipClientContractFilter;

            // Create a SuperToolTip //
            superToolTipInvoicingParameters = new SuperToolTip();
            superToolTipSetupArgsInvoicingParameters = new SuperToolTipSetupArgs();
            superToolTipSetupArgsInvoicingParameters.Title.Text = "Invoicing Parameters - Information";
            superToolTipSetupArgsInvoicingParameters.Title.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            superToolTipSetupArgsInvoicingParameters.Contents.Text = "I control if Teams can be invoiced for Visits with outstanding <b>Requirements</b> and <b>Warnings</b>.\n\nTick to disallow or un-tick to allow.";
            superToolTipSetupArgsInvoicingParameters.ShowFooterSeparator = false;
            superToolTipSetupArgsInvoicingParameters.Footer.Text = "";
            superToolTipInvoicingParameters.Setup(superToolTipSetupArgsInvoicingParameters);
            superToolTipInvoicingParameters.AllowHtmlText = DefaultBoolean.True;
            barEditItemInvoicingParameters.SuperTip = superToolTipInvoicingParameters;

            // Create a SuperToolTip //
            superToolTipExcludedClientContractFilter = new SuperToolTip();
            superToolTipSetupArgsExcludedClientContractFilter = new SuperToolTipSetupArgs();
            superToolTipSetupArgsClientContractFilter.Title.Text = "Excluded Client Contract Filter - Information";
            superToolTipSetupArgsExcludedClientContractFilter.Title.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            superToolTipSetupArgsExcludedClientContractFilter.Contents.Text = "I store the currently applied Excluded Client Filter.\n\nNo Filter Set.";
            superToolTipSetupArgsExcludedClientContractFilter.ShowFooterSeparator = false;
            superToolTipSetupArgsExcludedClientContractFilter.Footer.Text = "";
            superToolTipExcludedClientContractFilter.Setup(superToolTipSetupArgsExcludedClientContractFilter);
            superToolTipExcludedClientContractFilter.AllowHtmlText = DefaultBoolean.True;
            buttonEditExcludeClientFilter.SuperTip = superToolTipExcludedClientContractFilter;

            disabledEditor = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            disabledEditor.ShowIcon = false;
            disabledEditor.ScrollBars = ScrollBars.Vertical;
            disabledEditor.ReadOnly = true;

            enabledEditor = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            enabledEditor.ShowIcon = false;
            enabledEditor.ScrollBars = ScrollBars.Vertical;
            enabledEditor.ReadOnly = false;

            emptyEditor = new RepositoryItem();

            checkEditAutoSendCreatedInvoices.Checked = false;
            checkEditExcludeTeamsWithOutstandingRequirements.Checked = true;
            checkEditExcludeTeamsWithOutstandingWarnings.Checked = true;
            checkEditExcludeVisitWithRequirements.Checked = true;
            checkEditExcludeVisitsWithWarnings.Checked = true;
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionStringREADONLY, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            _KeepWaitFormOpen = false;
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();

            Application.DoEvents();  // Allow Form time to repaint itself //

            //splitContainerControl2.SplitterPosition = splitContainerControl2.Height / 2;

            //if (!splitContainerControl4.Collapsed)
            //{
            //    splitContainerControl4.SplitterPosition = splitContainerControl4.Width / 3;
            //}

            //if (!splitContainerControl3.Collapsed)
            //{
            //    splitContainerControl3.SplitterPosition = splitContainerControl3.Width / 2;
            //}

            if (strPassedInDrillDownIDs == "")  // Not opened in drill-down mode so load last saved screen settings for current user //
            {
                LoadLastSavedUserScreenSettings();
            }
            gridControlJob.Focus();  // Need this to make tooltip on column start working as soon as form opens otherwise it doesn't start until user clicks into a grid //
        }

        private void frm_OM_Team_Self_Billing_Manager_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedRecordIDsVisit))
                {
                    Load_Data();
                }
                if (UpdateRefreshStatus == 2 || !string.IsNullOrEmpty(i_str_AddedRecordIDsJob))
                {
                    //LoadLinkedRecords();
                    Load_Data();  // Need to update Parent Visits to refresh visits agregated column values which will automatically reload linked Jobs //
                }
                if (UpdateRefreshStatus == 13 || !string.IsNullOrEmpty(i_str_AddedRecordIDsBillingRequirement))
                {
                    LoadLinkedRecords();
                }
                if (UpdateRefreshStatus == 14 || !string.IsNullOrEmpty(i_str_AddedRecordIDsBillingException))
                {
                    LoadLinkedRecords();
                }

            }
            SetMenuStatus();
        }

        private void frm_OM_Team_Self_Billing_Manager_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strPassedInDrillDownIDs == "")  // Not opened in drill-down mode so save screen settings for current user //
            {
                if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
                {
                    // Store last used screen settings for current user //
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ClientFilter", i_str_selected_client_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "SiteFilter", i_str_selected_site_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ExcludeClientFilter", i_str_selected_exclude_client_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "LabourFilter", i_str_selected_Labour_Team_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "VisitTypeFilter", i_str_selected_VisitType_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "VisitCategoryFilter", i_str_selected_VisitCategory_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "LabourTypeFilter", (checkEditTeam.Checked ? "Team" : "Staff"));
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ShowInvoiced", (checkEditShowInvoiced.Checked ? "1" : "0"));
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "SiteID", spinEditSiteID.EditValue.ToString());
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "WarningFilter", i_str_selected_Warning_ids);                    
                    default_screen_settings.SaveDefaultScreenSettings();
                }
            }
        }


        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionStringREADONLY) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                string strItemFilter = "";
                int intFoundRow = 0;

                
                // Client Filter //
                strItemFilter = default_screen_settings.RetrieveSetting("ClientFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    i_str_selected_client_ids = strItemFilter;
                    var ResolveIDs = new DataSet_GC_Summer_CoreTableAdapters.QueriesTableAdapter();
                    ResolveIDs.ChangeConnectionString(strConnectionStringREADONLY);
                    try
                    {
                        buttonEditClientFilter.Text = ResolveIDs.sp06023_OM_Get_Clients_From_ClientIDs(strItemFilter).ToString();
                        i_str_selected_client_names = buttonEditClientFilter.Text;
                        
                        // Update Filter control's tooltip //
                        string strTooltipText = i_str_selected_client_names.Replace(", ", "\n");
                        superToolTipSetupArgsClientContractFilter.Contents.Text = "I store the currently applied Client Filter.\n\n" + strTooltipText;
                    }
                    catch (Exception) { }
                }

                // Site Filter //
                strItemFilter = default_screen_settings.RetrieveSetting("SiteFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    i_str_selected_site_ids = strItemFilter;
                    var ResolveIDs = new DataSet_GC_Summer_CoreTableAdapters.QueriesTableAdapter();
                    ResolveIDs.ChangeConnectionString(strConnectionStringREADONLY);
                    try
                    {
                        buttonEditSiteFilter.Text = ResolveIDs.sp06024_OM_Get_Sites_From_SiteIDs(strItemFilter).ToString();
                        i_str_selected_site_names = buttonEditSiteFilter.Text;
                        
                        // Update Filter control's tooltip //
                        string strTooltipText = i_str_selected_site_names.Replace(", ", "\n");
                        superToolTipSetupArgsSiteContractFilter.Contents.Text = "I store the currently applied Site Filter.\n\n" + strTooltipText;
                    }
                    catch (Exception) { }
                }

                // Exclude Client Filter //
                strItemFilter = default_screen_settings.RetrieveSetting("ExcludeClientFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    i_str_selected_exclude_client_ids = strItemFilter;
                    var ResolveIDs = new DataSet_GC_Summer_CoreTableAdapters.QueriesTableAdapter();
                    ResolveIDs.ChangeConnectionString(strConnectionStringREADONLY);
                    try
                    {
                        buttonEditExcludeClientFilter.Text = ResolveIDs.sp06023_OM_Get_Clients_From_ClientIDs(strItemFilter).ToString();
                        i_str_selected_exclude_client_names = buttonEditClientFilter.Text;

                        // Update Filter control's tooltip //
                        string strTooltipText = i_str_selected_exclude_client_names.Replace(", ", "\n");
                        superToolTipSetupArgsExcludedClientContractFilter.Contents.Text = "I store the currently applied Excluded Client Filter.\n\n" + strTooltipText;
                    }
                    catch (Exception) { }
                }
              
                // Site ID //
                strItemFilter = default_screen_settings.RetrieveSetting("SiteID");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    try
                    {
                        spinEditSiteID.EditValue = Convert.ToInt32(strItemFilter);
                    }
                    catch (Exception) { }
                }

                // Labour Type Check Edit //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("LabourTypeFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    if (strItemFilter == "Team")
                    {
                        checkEditTeam.Checked = true;
                    }
                    else  // Staff //
                    {
                        checkEditStaff.Checked = true;
                    }
                }

                // Labour Filter //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("LabourFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl12.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["ID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEditLabourFilter.Text = PopupContainerEditLabourFilter_Get_Selected();
                }


                // Visit Type Filter //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("VisitTypeFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl2.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["ID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEditVisitTypeFilter.Text = PopupContainerEditVisitTypeFilter_Get_Selected();
                }


                // Visit Category Filter //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("VisitCategoryFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl4.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["ID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEditVisitCategoryFilter.Text = PopupContainerEditVisitCategoryFilter_Get_Selected();
                }


                // Warning Filter //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("WarningFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl1.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["ID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEditWarningsFilter.Text = PopupContainerEditWarningFilter_Get_Selected();
                }

                PopupContainerEditInvoicingParameters_Get_Value();  // Construct text of the tooltip according to selected values //

                // Show Invoiced //
                //intFoundRow = 0;
                //strItemFilter = default_screen_settings.RetrieveSetting("ShowInvoiced");
                //if (!string.IsNullOrEmpty(strItemFilter))
                //{
                //    checkEditShowInvoiced.Checked = (strItemFilter == "1");
                //}

                // Following commented out - asked for by Laura Cain 28/11/2017 - JIRA item: ICE-469 //
                //Load_Data();

                // Prepare LocusEffects and add custom effect //
                locusEffectsProvider1 = new LocusEffectsProvider();
                locusEffectsProvider1.Initialize();
                locusEffectsProvider1.FramesPerSecond = 30;
                m_customArrowLocusEffect1 = new ArrowLocusEffect();
                m_customArrowLocusEffect1.Name = "CustomeArrow1";
                m_customArrowLocusEffect1.AnimationStartColor = Color.Orange;
                m_customArrowLocusEffect1.AnimationEndColor = Color.Red;
                m_customArrowLocusEffect1.MovementMode = MovementMode.OneWayAlongVector;
                m_customArrowLocusEffect1.MovementCycles = 20;
                m_customArrowLocusEffect1.MovementAmplitude = 200;
                m_customArrowLocusEffect1.MovementVectorAngle = 45; //degrees
                m_customArrowLocusEffect1.LeadInTime = 0; //msec
                m_customArrowLocusEffect1.LeadOutTime = 1000; //msec
                m_customArrowLocusEffect1.AnimationTime = 2000; //msec
                locusEffectsProvider1.AddLocusEffect(m_customArrowLocusEffect1);

                Point location = dockPanelFilters.PointToScreen(Point.Empty);

                System.Drawing.Point screenPoint = new System.Drawing.Point(location.X + 10, location.Y + 5);
                locusEffectsProvider1.ShowLocusEffect(this, screenPoint, m_customArrowLocusEffect1.Name);
            }
        }

        private void xtraTabControl1_SelectedPageChanged(object sender, TabPageChangedEventArgs e)
        {
            switch (xtraTabControl1.SelectedTabPage.Name)
            {
                case "xtraTabPageChecking":
                    {
                        tabbedControlGroupFilters.SelectedTabPage = layoutControlGroupChecking;
                    }
                    break;
                case "xtraTabPageHistoricalInvoices":
                case "xtraTabPagePreview":
                    {
                        tabbedControlGroupFilters.SelectedTabPage = layoutControlGroupHistorical;
                    }
                    break;
            }
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                    case 1:
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_InvoiceLayourDesigner = true;
                        }
                        break;
                    case 2:
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteInvoice = true;
                        }
                        break;
                }
            }
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, 7008, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);  // 7008 //
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        iBool_TemplateManager = sfpPermissions.blRead;
                        break;
                }
            }
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, 7018, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);  // 7018 //
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        iBool_BillingRequirementTemplateManager = sfpPermissions.blRead;
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bbiDatasetSelectionInverted.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            bsiAuditTrail.Enabled = false;
            bbiViewAuditTrail.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = (GridView)gridControlVisit.MainView;
            GridView ParentView = (GridView)gridControlVisit.MainView;

            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Labour:
                    view = (GridView)gridControlLabour.MainView;
                    break;
                case Utils.enmFocusedGrid.Visit:
                    view = (GridView)gridControlVisit.MainView;
                    break;
                case Utils.enmFocusedGrid.Jobs:
                    view = (GridView)gridControlJob.MainView;
                    break;
                case Utils.enmFocusedGrid.BillingRequirement:
                    view = (GridView)gridControlBillingRequirement.MainView;
                    break;
                case Utils.enmFocusedGrid.BillingException:
                    view = (GridView)gridControlBillingException.MainView;
                    break;
                case Utils.enmFocusedGrid.Labour2:
                    view = (GridView)gridControlLabour2.MainView;
                    break;
                case Utils.enmFocusedGrid.Invoice:
                    view = (GridView)gridControlInvoice.MainView;
                    break;
                default:
                    break;
            }
            int[] intRowHandles = view.GetSelectedRows();
            int[] intParentRowHandles = ParentView.GetSelectedRows();

            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Visit:
                case Utils.enmFocusedGrid.Jobs:
                    {
                        if (iBool_AllowEdit && intRowHandles.Length >= 1)
                        {
                            alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                            bsiEdit.Enabled = true;
                            bbiSingleEdit.Enabled = true;

                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.BillingRequirement:
                    {
                        if (iBool_AllowEdit && intRowHandles.Length >= 1)
                        {
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                        if (iBool_AllowDelete && intRowHandles.Length >= 1)
                        {
                            alItems.Add("iDelete");
                            bbiDelete.Enabled = true;
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.BillingException:
                    {
                        if (iBool_AllowEdit && intRowHandles.Length >= 1)
                        {
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                    }
                    break;
                default:
                    break;
            }
            bsiAuditTrail.Enabled = (intRowHandles.Length > 0);
            bbiViewAuditTrail.Enabled = (intRowHandles.Length > 0);

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of navigator custom buttons //
            gridControlVisit.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControlVisit.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControlVisit.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intParentRowHandles.Length > 0);
            gridControlVisit.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = true;

            view = (GridView)gridControlJob.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlJob.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControlJob.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0);
            gridControlJob.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = true;

            view = (GridView)gridControlBillingRequirement.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlBillingRequirement.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControlBillingRequirement.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControlBillingRequirement.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = true;
            gridControlBillingRequirement.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (iBool_AllowAdd);
            gridControlBillingRequirement.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (iBool_BillingRequirementTemplateManager);
            gridControlBillingRequirement.EmbeddedNavigator.Buttons.CustomButtons[5].Enabled = true;
            gridControlBillingRequirement.EmbeddedNavigator.Buttons.CustomButtons[6].Enabled = (intRowHandles.Length > 0);
            gridControlBillingRequirement.EmbeddedNavigator.Buttons.CustomButtons[7].Enabled = (intRowHandles.Length > 0);

            view = (GridView)gridControlBillingException.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlBillingException.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControlBillingException.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = true;
            gridControlBillingException.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowAdd && intParentRowHandles.Length > 0);
            gridControlBillingException.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);
            gridControlBillingException.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length > 0);
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridControlLabour":
                    message = "No Labour Available - Adjust any filters and click Refresh button";
                    break;
                case "gridViewVisit":
                    message = "No Visits Available - Adjust any filters and click Refresh button";
                    break;
                case "gridViewJob":
                    message = "No Linked Jobs Available - Select one or more Visits to view Linked Jobs";
                    break;
                case "gridControlBillingRequirement":
                    message = "No Linked Billing Requirements Available - Select one or more Visits to view Linked Billing Requirements";
                    break;
                case "gridControlBillingException":
                    message = "No Linked Billing Warnings Available - Select one or more Visits to view Linked Billing Warnings";
                    break;
                case "gridViewLabour2":
                    message = "No Labour Available - Adjust any filters and click Refresh button";
                    break;
                case "gridViewInvoice":
                    message = "No Invoices Available - Select one or more Labour records to view linked Invoices";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_MouseDown_Standard(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_Standard(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridViewLabour":
                    Load_Data_Visits();
                    view = (GridView)gridControlVisit.MainView;
                    view.ExpandAllGroups();
                    Set_Selected_Count0();
                    break;
                case "gridViewVisit":
                    LoadLinkedRecords();
                    view = (GridView)gridControlJob.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControlBillingRequirement.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControlBillingException.MainView;
                    view.ExpandAllGroups();
                    Set_Selected_Count1();
                    Set_Selected_Count2();
                    break;
                case "gridViewJob":
                    Set_Selected_Count1();
                    break;
                case "gridControlBillingRequirement":
                case "gridControlBillingException":
                    Set_Selected_Count2();
                    break;
                case "gridViewLabour2":
                    Load_Data_Invoices();
                    view = (GridView)gridControlInvoice.MainView;
                    view.ExpandAllGroups();
                    Set_Selected_Count3();
                    break;
                case "gridViewInvoice":
                    Set_Selected_Count3();
                    break;
                default:
                    break;
            }
            SetMenuStatus();
        }

        #endregion


        #region Checking \ Invoicing Page

        private void btnLoad_Click(object sender, EventArgs e)
        {
            Load_Data();
        }

        private void Load_Data(int CreateWarnings = 1)
        {
            if (!splashScreenManager.IsSplashFormVisible)
            {
                this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Getting Visits...");
            }

            if (i_str_selected_site_ids == null) i_str_selected_site_ids = "";
            int intShowInvoiced = (checkEditShowInvoiced.Checked ? 1 : 0);

            int intSiteID = Convert.ToInt32(spinEditSiteID.EditValue);
            DateTime dtStart = i_dtStart;
            DateTime dtEnd = i_dtEnd;
            //strVisitIDsToWorkWith = "";
            dsVisitsToWorkWith.Clear(); ;

            using (var GetVisitIDs = new DataSet_OM_BillingTableAdapters.QueriesTableAdapter())
            {
                GetVisitIDs.ChangeConnectionString(strConnectionString + "Connect Timeout=3600;");  // Timeout on SQL Connection to 1 hour //
                GetVisitIDs.ChangeCommandTimeout("dbo.sp06453_OM_Billing_Manager_Get_Visits_And_Create_Warnings", 3600);  // Need following command also to update the timeout to 1 hour //
                try
                {
                    if (buttonEditClientFilter.EditValue.ToString() == "Custom Filter" && strPassedInDrillDownIDs != "")  // Load passed in Visits //
                    {
                        using (var conn = new SqlConnection(strConnectionString + "Connect Timeout=3600;"))  // Timeout on SQL Connection to 1 hour - need line further down too. //
                        {
                            conn.Open();
                            using (var cmd2 = new SqlCommand())
                            {
                                cmd2.CommandText = "sp06453_OM_Billing_Manager_Get_Visits_And_Create_Warnings";
                                cmd2.CommandType = CommandType.StoredProcedure;
                                cmd2.Parameters.Add(new SqlParameter("@DrillDownIDs", strPassedInDrillDownIDs));
                                cmd2.Parameters.Add(new SqlParameter("@StartDate", dtStart));
                                cmd2.Parameters.Add(new SqlParameter("@EndDate", dtEnd));
                                cmd2.Parameters.Add(new SqlParameter("@ClientFilter", ""));
                                cmd2.Parameters.Add(new SqlParameter("@SiteFilter", ""));
                                cmd2.Parameters.Add(new SqlParameter("@LabourTeamFilter", ""));
                                cmd2.Parameters.Add(new SqlParameter("@VisitTypeFilter", ""));
                                cmd2.Parameters.Add(new SqlParameter("@VisitCategoryFilter", ""));
                                cmd2.Parameters.Add(new SqlParameter("@LabourTypeID", 0));
                                cmd2.Parameters.Add(new SqlParameter("@ShowInvoiced", 0));
                                cmd2.Parameters.Add(new SqlParameter("@SiteID", 0));
                                cmd2.Parameters.Add(new SqlParameter("@WarningsFilter", ""));
                                cmd2.Parameters.Add(new SqlParameter("@IncludeUnpaidHistorical", 0));
                                cmd2.Parameters.Add(new SqlParameter("@ExcludeClientIDs", ""));
                                cmd2.Parameters.Add(new SqlParameter("@IncludeDoNotPay", 0));
                                cmd2.Connection = conn;
                                cmd2.CommandTimeout = 3600; // Timeout on SQL Connection to 1 hour - need line further up too. //
                                sdaVisitsToWorkWith = new SqlDataAdapter(cmd2);
                                sdaVisitsToWorkWith.Fill(dsVisitsToWorkWith, "Table");
                            }
                            conn.Close();
                        }
                    }
                    else // Load based on users filters //
                    {
                        using (var conn = new SqlConnection(strConnectionString + "Connect Timeout=3600;"))  // Timeout on SQL Connection to 1 hour - need line further down too. //
                        {
                            int intLabourTypeID = (checkEditTeam.Checked ? 1 : 0);
                            int intIncludeUnpaidHistorical = (checkEditIncludeUnpaidHistorical.Checked ? 1 : 0);
                            int intIncludeDoNotPay = (checkEditDoNotPay.Checked ? 1 : 0);
                            conn.Open();
                            using (var cmd2 = new SqlCommand())
                            {
                                cmd2.CommandText = "sp06453_OM_Billing_Manager_Get_Visits_And_Create_Warnings";
                                cmd2.CommandType = CommandType.StoredProcedure;
                                cmd2.Parameters.Add(new SqlParameter("@DrillDownIDs", ""));
                                cmd2.Parameters.Add(new SqlParameter("@StartDate", dtStart));
                                cmd2.Parameters.Add(new SqlParameter("@EndDate", dtEnd));
                                cmd2.Parameters.Add(new SqlParameter("@ClientFilter", i_str_selected_client_ids));
                                cmd2.Parameters.Add(new SqlParameter("@SiteFilter", i_str_selected_site_ids));
                                cmd2.Parameters.Add(new SqlParameter("@LabourTeamFilter", i_str_selected_Labour_Team_ids));
                                cmd2.Parameters.Add(new SqlParameter("@VisitTypeFilter", i_str_selected_VisitType_ids));
                                cmd2.Parameters.Add(new SqlParameter("@VisitCategoryFilter", i_str_selected_VisitCategory_ids));
                                cmd2.Parameters.Add(new SqlParameter("@LabourTypeID", intLabourTypeID));
                                cmd2.Parameters.Add(new SqlParameter("@ShowInvoiced", intShowInvoiced));
                                cmd2.Parameters.Add(new SqlParameter("@SiteID", intSiteID));
                                cmd2.Parameters.Add(new SqlParameter("@WarningsFilter", i_str_selected_Warning_ids));
                                cmd2.Parameters.Add(new SqlParameter("@IncludeUnpaidHistorical", intIncludeUnpaidHistorical));
                                cmd2.Parameters.Add(new SqlParameter("@ExcludeClientIDs", i_str_selected_exclude_client_ids));
                                cmd2.Parameters.Add(new SqlParameter("@IncludeDoNotPay", intIncludeDoNotPay));
                                cmd2.Connection = conn;
                                cmd2.CommandTimeout = 3600; // Timeout on SQL Connection to 1 hour - need line further up too. //
                                sdaVisitsToWorkWith = new SqlDataAdapter(cmd2);
                                sdaVisitsToWorkWith.Fill(dsVisitsToWorkWith, "Table");
                            }
                            conn.Close();
                        }
                    }
                }
                catch (SqlException ex)
                {
                    if (ex.Number == -2)  // SQL Server Time-Out //
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("The screen has not been able to return your data due to the volume of records in your request. Please filter your request by using the data filter or contact Technical Support for further help.", "Load Visits", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Loading Visit Data.\n\nMessage = [" + ex.Message + "].\n\nIf the problem persists, contact Technical Support.", "Load Visits", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                    return;
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Loading Visit Data.\n\nMessage = [" + ex.Message + "].\n\nIf the error is a result of a query timeout, try adjusting the parameters at the top of the screen to reduce the amount of data returned then try again. If the problem persists, contact Technical Support.", "Load Visits", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                    return;
                }
                if (CreateWarnings == 1)
                {
                    //if (!string.IsNullOrWhiteSpace(strVisitIDsToWorkWith))
                    if (dsVisitsToWorkWith.Tables[0].Rows.Count > 0)
                    {
                        if (splashScreenManager.IsSplashFormVisible) splashScreenManager.SetWaitFormDescription("Updating Visit Costs...");
                        Recalculate_Visit_Costings();

                        if (splashScreenManager.IsSplashFormVisible) splashScreenManager.SetWaitFormDescription("Checking and Creating Warnings...");
                        try
                        {
                            GetVisitIDs.sp06450_OM_Self_Billing_Billing_Get_Warnings(dsVisitsToWorkWith.Tables[0]);
                        }
                        catch (SqlException ex)
                        {
                            if (ex.Number == -2)  // SQL Server Time-Out //
                            {
                                DevExpress.XtraEditors.XtraMessageBox.Show("The system has not been able to create warnings costs due to the volume of records in your request. Please filter your request by using the data filter or contact Technical Support for further help.", "Create Warnings", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                            else
                            {
                                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while create warnings.\n\nMessage = [" + ex.Message + "].\n\nIf the problem persists, contact Technical Support.", "Create Warnings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            }
                        }
                        catch (Exception ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while createing warnings.\n\nMessage = [" + ex.Message + "].\n\nIf the error is a result of a query timeout, try adjusting the parameters at the top of the screen to reduce the amount of data returned then try again. If the problem persists, contact Technical Support.", "Create Warnings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                }
            }
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.SetWaitFormDescription("Loading Labour...");
            Load_Data_Teams();
            GridView view = (GridView)gridControlLabour.MainView;
            view.ExpandAllGroups();
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }

        private void Recalculate_Visit_Costings()
        {
            bool boolSplashScreenShownByThisEvent = false;
            if (!splashScreenManager.IsSplashFormVisible)
            {
                boolSplashScreenShownByThisEvent = true;
                this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Updating Costing Data...");
            }

            using (var Update_Costs = new DataSet_OM_BillingTableAdapters.QueriesTableAdapter())
            {
                Update_Costs.ChangeConnectionString(strConnectionString);
                try
                {
                    Update_Costs.sp06456_OM_Self_Billing_Recalculate_Values(dsVisitsToWorkWith.Tables[0]);
                }
                catch (SqlException ex)
                {
                    if (ex.Number == -2)  // SQL Server Time-Out //
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("The system has not been able to re-calculate costs due to the volume of records in your request. Please filter your request by using the data filter or contact Technical Support for further help.", "Re-Calculate Costs", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while re-calculating costs.\n\nMessage = [" + ex.Message + "].\n\nIf the problem persists, contact Technical Support.", "Re-Calculate Costs", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while re-calculating costs.\n\nMessage = [" + ex.Message + "].\n\nIf the error is a result of a query timeout, try adjusting the parameters at the top of the screen to reduce the amount of data returned then try again. If the problem persists, contact Technical Support.", "Re-Calculate Costs", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }

            if (splashScreenManager.IsSplashFormVisible && boolSplashScreenShownByThisEvent)
            {
                splashScreenManager.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager.CloseWaitForm();
            }
        }

        private void Load_Data_Teams()
        {
            if (!splashScreenManager.IsSplashFormVisible)
            {
                this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Loading Labour...");
            }
            int intShowInvoiced = (checkEditShowInvoiced.Checked ? 1 : 0);

            RefreshGridViewStateLabour.SaveViewInfo();
            GridView view = (GridView)gridControlLabour.MainView;
            view.BeginUpdate();
            try
            {
                // Timeout on SQL Connection to 1 hour - need line further up where the connection string for it is declared. //
                DataSet_OM_BillingTableAdapters.QueriesTableAdapter configureDataTable = new DataSet_OM_BillingTableAdapters.QueriesTableAdapter();
                configureDataTable.ChangeTableAdapterTimeout(sp06452_OM_Billing_Manager_LabourTableAdapter, 3600);

                sp06452_OM_Billing_Manager_LabourTableAdapter.Fill(this.dataSet_OM_Billing.sp06452_OM_Billing_Manager_Labour, i_str_selected_Labour_Team_ids, (checkEditTeam.Checked ? 1 : 0), intShowInvoiced, "", dsVisitsToWorkWith.Tables[0]);
                if (boolFirstLoadOfTeamsGrid)
                {
                    view.ExpandAllGroups();
                    boolFirstLoadOfTeamsGrid = false;
                }
                else
                {
                    this.RefreshGridViewStateLabour.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
            }
            catch (SqlException ex)
            {
                if (ex.Number == -2)  // SQL Server Time-Out //
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("The screen has not been able to return your data due to the volume of records in your request. Please filter your request by using the data filter or contact Technical Support for further help.", "Load Labour", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Loading Labour Data.\n\nMessage = [" + ex.Message + "].\n\nIf the problem persists, contact Technical Support.", "Load Labour", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Loading Labour Data.\n\nMessage = [" + ex.Message + "].\n\nIf the problem persists, contact Technical Support.", "Load Labour", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            view.EndUpdate();
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }
        private void Refresh_Updated_Team_Rows(string strRecordIDs, DataTable dtVisits)
        {
            int intShowInvoiced = (checkEditShowInvoiced.Checked ? 1 : 0);
            try
            {
                SqlDataAdapter sdaData = new SqlDataAdapter();
                DataSet dsData = new DataSet("NewDataSet");

                using (var conn = new SqlConnection(strConnectionString))
                {
                    conn.Open();
                    using (var cmd = new SqlCommand("sp06452_OM_Billing_Manager_Labour", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@LabourTeamFilter", i_str_selected_Labour_Team_ids));
                        cmd.Parameters.Add(new SqlParameter("@LabourTypeID", (checkEditTeam.Checked ? 1 : 0)));
                        cmd.Parameters.Add(new SqlParameter("@ShowInvoiced", intShowInvoiced));
                        cmd.Parameters.Add(new SqlParameter("@DrillDownIDs", strRecordIDs));
                        cmd.Parameters.Add(new SqlParameter("@PassedInVisitIDsTable", dtVisits));
                        sdaData = new SqlDataAdapter(cmd);
                        sdaData.Fill(dsData, "Table");
                    }
                    conn.Close();
                }
                if (dsData.Tables[0].Rows.Count > 0) dataSet_OM_Billing.sp06452_OM_Billing_Manager_Labour.Merge(dsData.Tables[0]);
            }
            catch (Exception ex) { }
        }

        private void Load_Data_Visits()
        {
            GridView view = (GridView)gridControlVisit.MainView;
            if (UpdateRefreshStatus > 0)
            {
                UpdateRefreshStatus = 0;

                // Merge just changed rows back in to dataset - no need to reload the full dataset //
                if (!splashScreenManager.IsSplashFormVisible)
                {
                    this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    splashScreenManager.ShowWaitForm();
                    splashScreenManager.SetWaitFormDescription("Loading Visits...");
                }
                RefreshGridViewStateVisit.SaveViewInfo();
                view.BeginUpdate();
                Refresh_Updated_Visit_Rows(string.IsNullOrWhiteSpace(i_str_AddedRecordIDsVisit) ? strEditedVisitIDs : i_str_AddedRecordIDsVisit.Replace(';', ','), dsVisitsToWorkWith.Tables[0]);
                RefreshGridViewStateVisit.LoadViewInfo();  // Reload any expanded groups and selected rows //
                view.EndUpdate();
            }
            else
            {
                view = (GridView)gridControlLabour.MainView;
                int[] intRowHandles = view.GetSelectedRows();
                int intCount = intRowHandles.Length;
                if (intCount == 0)
                {
                    view.BeginUpdate();
                    this.dataSet_OM_Billing.sp06445_OM_Team_Self_Billing_Manager_Visits.Rows.Clear();
                    view.EndUpdate();
                    return;
                }

                if (!splashScreenManager.IsSplashFormVisible)
                {
                    this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    splashScreenManager.ShowWaitForm();
                    splashScreenManager.SetWaitFormDescription("Loading Visits...");
                }

                StringBuilder sbTeams = new StringBuilder();
                StringBuilder sbStaff = new StringBuilder();
                foreach (int intRowHandle in intRowHandles)
                {
                    if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, view.Columns["LabourTypeID"])) == 1)
                    {
                        sbTeams.Append(view.GetRowCellValue(intRowHandle, view.Columns["LabourID"]).ToString() + ",");
                    }
                    else
                    {
                        sbStaff.Append(view.GetRowCellValue(intRowHandle, view.Columns["LabourID"]).ToString() + ",");
                    }
                }
                string strSelectedTeamIDs = sbTeams.ToString();
                string strSelectedStaffIDs = sbStaff.ToString();
                int intShowInvoiced = (checkEditShowInvoiced.Checked ? 1 : 0);
               
                view = (GridView)gridControlVisit.MainView;
                RefreshGridViewStateVisit.SaveViewInfo();
                view.BeginUpdate();
                try
                {
                    if (buttonEditClientFilter.EditValue.ToString() == "Custom Filter" && strPassedInDrillDownIDs != "")  // Load passed in Callouts //
                    {
                        sp06445_OM_Team_Self_Billing_Manager_VisitsTableAdapter.Fill(this.dataSet_OM_Billing.sp06445_OM_Team_Self_Billing_Manager_Visits, strPassedInDrillDownIDs, "", "", 0, null);
                        this.RefreshGridViewStateVisit.LoadViewInfo();  // Reload any expanded groups and selected rows //
                        view.ExpandAllGroups();
                    }
                    else // Load users selection //
                    {
                        sp06445_OM_Team_Self_Billing_Manager_VisitsTableAdapter.Fill(this.dataSet_OM_Billing.sp06445_OM_Team_Self_Billing_Manager_Visits, "", strSelectedTeamIDs, strSelectedStaffIDs, intShowInvoiced, dsVisitsToWorkWith.Tables[0]);
                        this.RefreshGridViewStateVisit.LoadViewInfo();  // Reload any expanded groups and selected rows //
                    }
                }
                catch (SqlException ex)
                {
                    if (ex.Number == -2)  // SQL Server Time-Out //
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("The screen has not been able to return your data due to the volume of records in your request. Please filter your request by using the data filter or contact Technical Support for further help.", "Load Visits", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Loading Visit Data.\n\nMessage = [" + ex.Message + "].\n\nIf the problem persists, contact Technical Support.", "Load Visits", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Loading Visit Data.\n\nMessage = [" + ex.Message + "].\n\nIf the problem persists, contact Technical Support.", "Load Visits", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                view.EndUpdate();
            }

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDsVisit != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDsVisit.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                view.BeginSelection();
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["VisitID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                        view.SetRowCellValue(intRowHandle, "CheckMarkSelection", 1);  // Tick the new record so it is picked up by the calendar if it is active //
                    }
                }
                i_str_AddedRecordIDsVisit = "";
                view.EndSelection();
            }

            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }
        private void Refresh_Updated_Visit_Rows(string strRecordIDs, DataTable dtVisits)
        {
            try
            {
                SqlDataAdapter sdaData = new SqlDataAdapter();
                DataSet dsData = new DataSet("NewDataSet");

                using (var conn = new SqlConnection(strConnectionString))
                {
                    conn.Open();
                    using (var cmd = new SqlCommand("sp06445_OM_Team_Self_Billing_Manager_Visits", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@DrillDownIDs", strRecordIDs));
                        cmd.Parameters.Add(new SqlParameter("@ContractorIDsFilter", null));
                        cmd.Parameters.Add(new SqlParameter("@StaffIDsFilter", null));
                        cmd.Parameters.Add(new SqlParameter("@PassedInVisitIDsTable", dtVisits));
                        sdaData = new SqlDataAdapter(cmd);
                        sdaData.Fill(dsData, "Table");
                    }
                    conn.Close();
                }
                if (dsData.Tables[0].Rows.Count > 0) dataSet_OM_Billing.sp06445_OM_Team_Self_Billing_Manager_Visits.Merge(dsData.Tables[0]);
            }
            catch (Exception ex) { }
        }

        private void LoadLinkedRecords()
        {
            if (splitContainerControl1.Collapsed) return;  // Don't bother loading related data as the parent panel is collapsed so the grids are invisible //

            bool boolSplashScreenVisibile = splashScreenManager.IsSplashFormVisible;
            if (!boolSplashScreenVisibile)
            {
                this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Loading Linked Data...");
            }

            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControlVisit.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;

            StringBuilder sb = new StringBuilder();
            foreach (int intRowHandle in intRowHandles)
            {
                if (intRowHandle < 0) continue;
                sb.Append(view.GetRowCellValue(intRowHandle, view.Columns["VisitID"]).ToString() + ",");
            }
            string strSelectedIDs = sb.ToString();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;

            gridControlJob.MainView.BeginUpdate();
            RefreshGridViewStateJob.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                dataSet_OM_Billing.sp06454_OM_Self_Billing_Jobs_Linked_to_Visits.Clear();
            }
            else
            {
                try
                {
                    sp06454_OM_Self_Billing_Jobs_Linked_to_VisitsTableAdapter.Fill(dataSet_OM_Billing.sp06454_OM_Self_Billing_Jobs_Linked_to_Visits, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                    RefreshGridViewStateJob.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                catch (Exception) { }
            }
            gridControlJob.MainView.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDsJob != "")
            {
                strArray = i_str_AddedRecordIDsJob.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControlJob.MainView;
                view.BeginSelection();
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["JobID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDsJob = "";
                view.EndSelection();
            }


            gridControlBillingRequirement.MainView.BeginUpdate();
            RefreshGridViewStateBillingRequirement.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                dataSet_OM_Billing.sp06446_OM_Team_Self_Billing_Billing_Requirements.Clear();
            }
            else
            {
                sp06446_OM_Team_Self_Billing_Billing_RequirementsTableAdapter.Fill(dataSet_OM_Billing.sp06446_OM_Team_Self_Billing_Billing_Requirements, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), intBillingRequirementLinkedRecordTypeID);
                RefreshGridViewStateBillingRequirement.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControlBillingRequirement.MainView.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDsBillingRequirement != "")
            {
                strArray = i_str_AddedRecordIDsBillingRequirement.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControlBillingRequirement.MainView;
                view.BeginSelection();
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["BillingRequirementID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDsBillingRequirement = "";
                view.EndSelection();
            }


            gridControlBillingException.MainView.BeginUpdate();
            RefreshGridViewStateBillingException.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                dataSet_OM_Billing.sp06448_OM_Team_Self_Billing_Billing_Exceptions.Clear();
            }
            else
            {
                sp06448_OM_Team_Self_Billing_Billing_ExceptionsTableAdapter.Fill(dataSet_OM_Billing.sp06448_OM_Team_Self_Billing_Billing_Exceptions, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), intBillingRequirementLinkedRecordTypeID);
                RefreshGridViewStateBillingException.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControlBillingException.MainView.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDsBillingException != "")
            {
                strArray = i_str_AddedRecordIDsBillingException.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControlBillingException.MainView;
                view.BeginSelection();
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["BillingWarningID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDsBillingException = "";
                view.EndSelection();
            }

            if (!boolSplashScreenVisibile && splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }

        private void bbiRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            Load_Data();
        }

        public void UpdateFormRefreshStatus(int status, Utils.enmFocusedGrid grid, string newIds)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            switch (grid)
            {
                case Utils.enmFocusedGrid.Visit:
                    i_str_AddedRecordIDsVisit = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsVisit : newIds);
                    break;
                case Utils.enmFocusedGrid.Jobs:
                    i_str_AddedRecordIDsJob = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsJob : newIds);
                    break;
                case Utils.enmFocusedGrid.BillingRequirement:
                    i_str_AddedRecordIDsBillingRequirement = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsBillingRequirement : newIds);
                    break;
                case Utils.enmFocusedGrid.BillingException:
                    i_str_AddedRecordIDsBillingException = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsBillingException : newIds);
                    break;
            }
        }


        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }


        private void Block_Edit()
        {
            if (!iBool_AllowAdd) return;
                        
            int[] intRowHandles;
            int intRecordCount = 0;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Visit:
                    {
                        GridView view = (GridView)gridControlVisit.MainView;
                        int intRowCount = view.DataRowCount;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();

                        view.BeginSelection();
                        foreach (int intRowHandle in intRowHandles)
                        {
                            if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "SelfBillingInvoiceHeaderID")) > 0) view.UnselectRow(intRowHandle);
                        }
                        view.EndSelection();

                        intRowHandles = view.GetSelectedRows();
                        intRecordCount = intRowHandles.Length;
                        if (intRecordCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Visits to block edit the Self-Billing Comments then try again.\n\nNote: Only Visits not already billed against a team can be selected. All other records will automatically be de-selected.", "Block Edit Self-Billing Comments", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        var fChildForm = new frm_OM_Team_Self_Billing_Manager_Block_Edit_Self_Billing_Comment();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                        {
                            splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager.ShowWaitForm();
                            splashScreenManager.SetWaitFormDescription("Updating...");

                            string strComment = fChildForm.SelfBillingComment;
                            view.BeginUpdate();
                            view.BeginSort();
                            try
                            {
                                StringBuilder sb = new StringBuilder();
                                foreach (int intRowHandle in intRowHandles)
                                {
                                    sb.Append(view.GetRowCellValue(intRowHandle, "VisitLabourCalculatedID").ToString() + ",");
                                    view.SetRowCellValue(intRowHandle, "SelfBillingComment", strComment);
                                }
                                Update_Billing_Comments(strComment, sb.ToString());
                                //Load_Data_Visits();
                            }
                            catch (Exception) { }
                            view.EndSort();
                            view.EndUpdate();
                        }
                        if (splashScreenManager.IsSplashFormVisible)
                        {
                            splashScreenManager.SetWaitFormDescription("Loading...");  // Reset caption //
                            splashScreenManager.CloseWaitForm();
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.BillingRequirement:
                    {
                        GridView view = (GridView)gridControlBillingRequirement.MainView;
                        int intRowCount = view.DataRowCount;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intRecordCount = intRowHandles.Length;
                        if (intRowHandles.Length <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Billing Requirements to block edit then try again.", "Block Edit Billing Requirements", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        var fChildForm = new frm_Core_Billing_Requirement_Block_Edit_Fulfilled();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                        {
                            splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager.ShowWaitForm();
                            splashScreenManager.SetWaitFormDescription("Updating...");

                            intRowHandles = view.GetSelectedRows();
                            intRecordCount = intRowHandles.Length;
                            if (intRowHandles.Length <= 0) return;
                            view.BeginUpdate();
                            view.BeginSort();
                            try
                            {
                                StringBuilder sb = new StringBuilder();
                                foreach (int intRowHandle in intRowHandles)
                                {
                                    sb.Append(view.GetRowCellValue(intRowHandle, "BillingRequirementID").ToString() + ",");
                                }
                                int? intChecked = fChildForm.intRequirementFulfilled;
                                string strRemarks = fChildForm.strRemarks;
                                int intChangedByStaffID = (intChecked == 1 ? GlobalSettings.UserID : 0);
                                string strChangedByName = (intChecked == 1 ? GlobalSettings.UserForename + " " + GlobalSettings.UserSurname : "");
                                string strRequirementIDs = sb.ToString();
                                Update_Billing_Requirement(view, intRowHandles, intChecked, intChangedByStaffID, strChangedByName, strRemarks, strRequirementIDs);
                                //LoadLinkedRecords();
                            }
                            catch (Exception) { }
                            view.EndSort();
                            view.EndUpdate();
                        }
                        if (splashScreenManager.IsSplashFormVisible)
                        {
                            splashScreenManager.SetWaitFormDescription("Loading...");  // Reset caption //
                            splashScreenManager.CloseWaitForm();
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.BillingException:
                    {
                        GridView view = (GridView)gridControlBillingException.MainView;
                        int intRowCount = view.DataRowCount;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intRecordCount = intRowHandles.Length;
                        if (intRowHandles.Length <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Billing Exceptions to block edit then try again.", "Block Edit Billing Exceptions", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        var fChildForm = new frm_Core_Billing_Warning_Block_Edit_Authorised();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                        {
                            splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager.ShowWaitForm();
                            splashScreenManager.SetWaitFormDescription("Updating...");

                            intRowHandles = view.GetSelectedRows();
                            intRecordCount = intRowHandles.Length;
                            if (intRowHandles.Length <= 0) return;
                            view.BeginUpdate();
                            view.BeginSort();
                            try
                            {
                                StringBuilder sb = new StringBuilder();
                                foreach (int intRowHandle in intRowHandles)
                                {
                                    sb.Append(view.GetRowCellValue(intRowHandle, "BillingWarningID").ToString() + ",");
                                }
                                int? intAuthorised = fChildForm.intAuthorised;
                                string strRemarks = fChildForm.strRemarks;
                                int intAuthorisedByStaffID = (intAuthorised == 1 ? GlobalSettings.UserID : 0);
                                string strAuthorisedByName = (intAuthorised == 1 ? GlobalSettings.UserForename + " " + GlobalSettings.UserSurname : "");
                                string strBillingWarningIDs = sb.ToString();
                                Update_Billing_Warning(view, intRowHandles, intAuthorised, intAuthorisedByStaffID, strAuthorisedByName, strRemarks, strBillingWarningIDs);
                                //LoadLinkedRecords();
                            }
                            catch (Exception) { }
                            view.EndSort();
                            view.EndUpdate();
                        }
                        if (splashScreenManager.IsSplashFormVisible)
                        {
                            splashScreenManager.SetWaitFormDescription("Loading...");  // Reset caption //
                            splashScreenManager.CloseWaitForm();
                        }
                    }
                    break;
            }
        }

        private void Edit_Record()
        {
            GridView view = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (_enmFocusedGrid)
            {
                #region Visit
                case Utils.enmFocusedGrid.Visit:
                    {
                        view = (GridView)gridControlVisit.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "VisitID")) + ',';
                        }
                        var fChildForm = new frm_OM_Visit_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                #endregion

                #region Jobs
                case Utils.enmFocusedGrid.Jobs:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlJob.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        StringBuilder sb = new StringBuilder();
                        foreach (int intRowHandle in intRowHandles)
                        {
                            sb.Append(Convert.ToString(view.GetRowCellValue(intRowHandle, "JobID")) + ',');
                        }
                        Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, sb.ToString(), "operations_job");
                    }
                    break;
                #endregion

                #region Billing Requirement
                case Utils.enmFocusedGrid.BillingRequirement:
                    {
                        view = (GridView)gridControlBillingRequirement.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view parent Visits before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        StringBuilder sb = new StringBuilder();
                        foreach (int intRowHandle in intRowHandles)
                        {
                            sb.Append(Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedToRecordID")) + ',');
                        }
                        strEditedVisitIDs = sb.ToString();  // Put IDs into the instance var so we can merge changes if any are made without reloading the whole screen //
                        Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strEditedVisitIDs, "operations_visit");
                    }
                    break;
                #endregion

                #region Billing Warning
                case Utils.enmFocusedGrid.BillingException:
                    {
                        view = (GridView)gridControlBillingException.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view parent Visits before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        StringBuilder sb = new StringBuilder();
                        foreach (int intRowHandle in intRowHandles)
                        {
                            sb.Append(Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedToRecordID")) + ',');
                        }
                        strEditedVisitIDs = sb.ToString();  // Put IDs into the instance var so we can merge changes if any are made without reloading the whole screen //
                        Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strEditedVisitIDs, "operations_visit");
                    }
                    break;
                #endregion

                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            StringBuilder sbRecords = new StringBuilder();
            switch (_enmFocusedGrid)
            {
                #region Billing Requirement
                case Utils.enmFocusedGrid.BillingRequirement:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlBillingRequirement.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Billing Requirements to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Billing Requirement" : Convert.ToString(intRowHandles.Length) + " Linked Billing Requirements") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Billing Requirement" : "these Linked Billing Requirements") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strVisitIDs = ",";
                            string strCurrentVisitID = "";

                            Array.Reverse(intRowHandles);  // Reverse the order so the last record is deleted first //
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strCurrentVisitID = Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedToRecordID"));
                                if (!strVisitIDs.Contains("," + strCurrentVisitID + ",")) strVisitIDs += strCurrentVisitID + ",";
                                
                                sbRecords.Append(Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "BillingRequirementID")) + ",");
                                view.DeleteRow(intRowHandle);
                            }
                            if (strVisitIDs.StartsWith(",")) strVisitIDs.Remove(0, 1);

                            using (var RemoveRecords = new DataSet_Common_FunctionalityTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp01000_Core_Delete("billing_requirement", sbRecords.ToString());  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            Refresh_Updated_Team_Rows(null, dsVisitsToWorkWith.Tables[0]);
                            Refresh_Updated_Visit_Rows(strVisitIDs, null);

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                #endregion

                default:
                    break;
            }
        }

        private void View_Record()
        {
            GridView view = null;
            MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            //switch (i_int_FocusedGrid)
            switch (_enmFocusedGrid)
            {
                #region Visit
                case Utils.enmFocusedGrid.Visit:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlVisit.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Visits to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        StringBuilder sb = new StringBuilder();
                        foreach (int intRowHandle in intRowHandles)
                        {
                            sb.Append(Convert.ToString(view.GetRowCellValue(intRowHandle, "VisitID")) + ',');
                        }
                        strEditedVisitIDs = sb.ToString();  // Put IDs into the instance var so we can merge changes if any are made without reloading the whole screen //
                        Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strEditedVisitIDs, "operations_visit");
                    }
                    break;
                #endregion

                #region Jobs
                case Utils.enmFocusedGrid.Jobs:
                    {
                        view = (GridView)gridControlJob.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "JobID")) + ',';
                        }
                        var fChildForm = new frm_OM_Job_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                #endregion

                #region Billing Requirement
                    // Not Used //
                case Utils.enmFocusedGrid.BillingRequirement:
                    {
                        view = (GridView)gridControlBillingRequirement.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view parent Visits before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedToRecordID")) + ',';
                        }
                        var fChildForm = new frm_OM_Visit_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                #endregion

                #region Billing Warning
                // Not Used //
                case Utils.enmFocusedGrid.BillingException:
                    {
                        view = (GridView)gridControlBillingException.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view parent Visits before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedToRecordID")) + ',';
                        }
                        var fChildForm = new frm_OM_Visit_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                #endregion

                default:
                    break;
            }
        }


        public override void OnViewAuditTrail(object sender, EventArgs e)
        {
            View_Audit_Trail();
        }
        private void View_Audit_Trail()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Visit:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlVisit.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view the audit trail for before proceeding.", "View Audit Trail", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "VisitID")) + ',';
                        }

                        var fChildForm = new frm_Core_Audit_Trail_Viewer();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInDrillDownIDs = strRecordIDs;
                        fChildForm.strPassedInDrillDownTypeID = "dbo.OM_Visit";
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Jobs:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlJob.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view the audit trail for before proceeding.", "View Audit Trail", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "JobID")) + ',';
                        }

                        var fChildForm = new frm_Core_Audit_Trail_Viewer();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInDrillDownIDs = strRecordIDs;
                        fChildForm.strPassedInDrillDownTypeID = "dbo.OM_Job";
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
            }
        }

        private void Set_Selected_Count0()
        {
            GridView view = (GridView)gridControlLabour.MainView;
            int intSelectedCount = view.GetSelectedRows().Length;
            if (intSelectedCount == 0)
            {
                bsiSelectedCount0.Visibility = BarItemVisibility.Never;
                bbiInvoice.Enabled = false;
            }
            else
            {
                bsiSelectedCount0.Visibility = BarItemVisibility.Always;
                string strText = "";
                if (intSelectedCount > 0)
                {
                    strText = (intSelectedCount == 1 ? "1 Team Selected" : "<color=red>" + intSelectedCount.ToString() + " Teams</Color> Selected");
                }
                bsiSelectedCount0.Caption = strText;
                bbiInvoice.Enabled = true;
            }
        }
        private void Set_Selected_Count1()
        {
            GridView view = (GridView)gridControlVisit.MainView;
            int intSelectedCount = view.GetSelectedRows().Length;
            view = (GridView)gridControlJob.MainView;
            int intSelectedCount2 = view.GetSelectedRows().Length;
            if (intSelectedCount == 0 && intSelectedCount2 == 0)
            {
                bsiSelectedCount1.Visibility = BarItemVisibility.Never;
            }
            else
            {
                bsiSelectedCount1.Visibility = BarItemVisibility.Always;
                string strText = "";
                if (intSelectedCount > 0)
                {
                    strText = (intSelectedCount == 1 ? "1 Visit Selected" : "<color=red>" + intSelectedCount.ToString() + " Visits</Color> Selected");
                }
                if (intSelectedCount > 0 && intSelectedCount2 > 0) strText += "<br>";
                if (intSelectedCount2 > 0)
                {
                    strText += (intSelectedCount2 == 1 ? "1 Job Selected" : "<color=red>" + intSelectedCount2.ToString() + " Jobs</Color> Selected");
                }
                bsiSelectedCount1.Caption = strText;
            }
        }
        private void Set_Selected_Count2()
        {
            GridView view = (GridView)gridControlBillingRequirement.MainView;
            int intSelectedCount = view.GetSelectedRows().Length;
            view = (GridView)gridControlBillingException.MainView;
            int intSelectedCount2 = view.GetSelectedRows().Length;
            if (intSelectedCount == 0 && intSelectedCount2 == 0)
            {
                bsiSelectedCount2.Visibility = BarItemVisibility.Never;
            }
            else
            {
                bsiSelectedCount2.Visibility = BarItemVisibility.Always;
                string strText = "";
                if (intSelectedCount > 0)
                {
                    strText = (intSelectedCount == 1 ? "1 Billing Requirement Selected" : "<color=red>" + intSelectedCount.ToString() + " Billing Requirements</Color> Selected");
                }
                if (intSelectedCount > 0 && intSelectedCount2 > 0) strText += "<br>";
                if (intSelectedCount2 > 0)
                {
                    strText += (intSelectedCount2 == 1 ? "1 Billing Exception Selected" : "<color=red>" + intSelectedCount2.ToString() + " Billing Exceptions</Color> Selected");
                }
                bsiSelectedCount2.Caption = strText;
            }
        }


        #region GridView - Labour

        private void gridControlLabour_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("reload".Equals(e.Button.Tag))
                    {
                        Load_Data_Teams();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewLabour_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            /*GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            switch (e.Column.FieldName)
            {
                case "LinkedJobCount":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedJobCount")) <= 0)
                        {
                            //e.Appearance.BackColor = Color.LightCoral;
                            //e.Appearance.BackColor2 = Color.Red;
                            e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                            e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                            e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        }
                    }
                    break;
                case "IssueFound":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "IssueFound")) != 0)
                        {
                            e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                            e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                            e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        }
                    }
                    break;
                case "Rework":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "Rework")) >= 1)
                        {
                            e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                            e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                            e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        }
                    }
                    break;
                case "AccidentOnSite":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "AccidentOnSite")) >= 1)
                        {
                            e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                            e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                            e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        }
                    }
                    break;
                case "SuspendedJobCount":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "SuspendedJobCount")) >= 1)
                        {
                            e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                            e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                            e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        }
                    }
                    break;
            }*/
        }

        private void gridViewLabour_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewLabour_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.Labour;
            SetMenuStatus();
        }

        private void gridViewLabour_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Labour;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                int[] intRowHandles = view.GetSelectedRows();
                int intCount = intRowHandles.Length;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewLabour_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedJobCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedJobCount")) <= 0) e.Cancel = true;
                    break;
                case "LinkedOutstandingJobCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedOutstandingJobCount")) <= 0) e.Cancel = true;
                    break;
                case "LinkedCompletedJobCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedCompletedJobCount")) <= 0) e.Cancel = true;
                    break;
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) <= 0) e.Cancel = true;
                    break;
                case "LabourCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LabourCount")) <= 0) e.Cancel = true;
                    break;
                case "LinkedVisitCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedVisitCount")) <= 0) e.Cancel = true;
                    break;
                case "SuspendedJobCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("SuspendedJobCount")) <= 0) e.Cancel = true;
                    break;
                case "ClientSignaturePath":
                    if (string.IsNullOrWhiteSpace(view.GetFocusedRowCellValue("ClientSignaturePath").ToString())) e.Cancel = true;
                    break;
                case "LinkedPictureCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedPictureCount")) <= 0) e.Cancel = true;
                    break;
                case "SelfBillingInvoiceID":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("SelfBillingInvoiceID")) <= 0) e.Cancel = true;
                    break;
                case "ClientInvoiceID":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("ClientInvoiceID")) <= 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView - Visit

        private void gridControlVisit_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("blockedit".Equals(e.Button.Tag))
                    {
                        Block_Edit();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        Load_Data_Visits();
                    }
                    break;                
                default:
                    break;
            }
        }

        private void gridViewVisit_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            switch (e.Column.FieldName)
            {
                case "LinkedJobCount":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedJobCount")) <= 0)
                        {
                            //e.Appearance.BackColor = Color.LightCoral;
                            //e.Appearance.BackColor2 = Color.Red;
                            e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                            //e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                            //e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        }
                    }
                    break;
                case "IssueFound":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "IssueFound")) != 0)
                        {
                            e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                            //e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                            //e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        }
                    }
                    break;
                case "Rework":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "Rework")) >= 1)
                        {
                            e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                            //e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                            //e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        }
                    }
                    break;
                case "AccidentOnSite":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "AccidentOnSite")) >= 1)
                        {
                            e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                            //e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                            //e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        }
                    }
                    break;
                case "SuspendedJobCount":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "SuspendedJobCount")) >= 1)
                        {
                            e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                            //e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                            //e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        }
                    }
                    break;
                case "DoNotInvoiceClient":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "DoNotInvoiceClient")) == 1)
                        {
                            e.Appearance.BackColor = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                            //e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                            //e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                            //e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        }
                    }
                    break;
                case "DoNotPayContractor":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "DoNotPayContractor")) == 1)
                        {
                            e.Appearance.BackColor = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                            //e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                            //e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                            //e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        }
                    }
                    break;
            }            
        }

        private void gridViewVisit_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            try
            {
                GridView view = (GridView)sender;
                switch (e.Column.FieldName)
                {
                    case "LinkedJobCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedJobCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "LinkedOutstandingJobCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedOutstandingJobCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "LinkedCompletedJobCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedCompletedJobCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "LinkedDocumentCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "LabourCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LabourCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "LinkedVisitCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedVisitCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "SuspendedJobCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "SuspendedJobCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "ClientSignaturePath":
                        if (string.IsNullOrWhiteSpace(view.GetRowCellValue(e.RowHandle, "ClientSignaturePath").ToString())) e.RepositoryItem = emptyEditor;
                        break;
                    case "LinkedPictureCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedPictureCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "SelfBillingInvoice":
                        if (view.GetRowCellValue(e.RowHandle, "SelfBillingInvoice").ToString() != "Yes") e.RepositoryItem = emptyEditor;
                        break;
                    case "ClientInvoiceID":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "ClientInvoiceID")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "SelfBillingComment":
                        {
                            int intSelfBillingInvoiceHeaderID = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "SelfBillingInvoiceHeaderID"));
                            e.RepositoryItem = (intSelfBillingInvoiceHeaderID > 0 ? disabledEditor : enabledEditor);
                        }
                        break;
                    default:
                        break;
                }
            }
            catch (Exception) { }
        }

        private void gridViewVisit_CustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
        }

        private void gridViewVisit_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewVisit_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.Visit;
            SetMenuStatus();
        }

        private void gridViewVisit_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Visit;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                int[] intRowHandles = view.GetSelectedRows();
                int intCount = intRowHandles.Length;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewVisit_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedJobCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedJobCount")) <= 0) e.Cancel = true;
                    break;
                case "LinkedOutstandingJobCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedOutstandingJobCount")) <= 0) e.Cancel = true;
                    break;
                case "LinkedCompletedJobCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedCompletedJobCount")) <= 0) e.Cancel = true;
                    break;
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) <= 0) e.Cancel = true;
                    break;
                case "LabourCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LabourCount")) <= 0) e.Cancel = true;
                    break;
                case "LinkedVisitCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedVisitCount")) <= 0) e.Cancel = true;
                    break;
                case "SuspendedJobCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("SuspendedJobCount")) <= 0) e.Cancel = true;
                    break;
                case "ClientSignaturePath":
                    if (string.IsNullOrWhiteSpace(view.GetFocusedRowCellValue("ClientSignaturePath").ToString())) e.Cancel = true;
                    break;
                case "LinkedPictureCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedPictureCount")) <= 0) e.Cancel = true;
                    break;
                case "SelfBillingInvoice":
                    if (view.GetFocusedRowCellValue("SelfBillingInvoice").ToString() != "Yes") e.Cancel = true;
                    break;
                case "ClientInvoiceID":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("ClientInvoiceID")) <= 0) e.Cancel = true;
                    break;
                case "OverdueDays":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("OverdueDays")) <= 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void gridViewVisit_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            GridView view = (GridView)sender;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            switch (view.FocusedColumn.Name)
            {
                case "colSelfBillingComment":
                    try
                    {
                        string strSelfBillingComment = (view.FocusedColumn.Name == "colSelfBillingComment" ? e.Value.ToString() : view.GetFocusedRowCellValue("SelfBillingComment").ToString());
                        string strVisitLabourCalculatedIDs = view.GetFocusedRowCellValue("VisitLabourCalculatedID").ToString() + ",";
                        int[] intRowHandles = new int[1];
                        intRowHandles[0] = view.FocusedRowHandle;
                        Update_Billing_Comments(strSelfBillingComment, strVisitLabourCalculatedIDs);
                    }
                    catch (Exception) { };
                    break;
                default:
                    break;
            }
        }
        private void Update_Billing_Comments(string strSelfBillingComment, string strVisitLabourCalculatedIDs)
        {
            // Update DB with changes //
            try
            {
                using (DataSet_OM_BillingTableAdapters.QueriesTableAdapter UpdateDatabase = new DataSet_OM_BillingTableAdapters.QueriesTableAdapter())
                {
                    UpdateDatabase.ChangeConnectionString(strConnectionString);
                    UpdateDatabase.sp06458_OM_Self_Billing_Update_Billing_Comment(strVisitLabourCalculatedIDs, strSelfBillingComment);
                }
            }
            catch (Exception) { };
        }

       
        private void repositoryItemHyperLinkEditGrid1_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            if (view.FocusedColumn.FieldName == "LinkedPictureCount")
            {
                int intVisitID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "VisitID"));
                string strJobIDs = "";
                try
                {
                    var GetValue = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter();
                    GetValue.ChangeConnectionString(strConnectionString);
                    strJobIDs = GetValue.sp06130_OM_Visit_Manager_Get_Linked_Job_IDs(intVisitID.ToString() + ",", 0).ToString();
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view linked pictures [" + ex.Message + "].\n\nPlease try again. If the problem persists contact Technical Support.", "View Linked Pictures", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }

                Open_Picture_Viewer(strJobIDs, Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "ClientID")), 1);
                return;
            }
            else
            {
                int intVisitID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "VisitID"));
                if (intVisitID <= 0) return;
                string strRecordIDs = "";

                int intRecordType = -1;
                switch (view.FocusedColumn.FieldName)
                {
                    case "LinkedJobCount":
                        intRecordType = 0;
                        break;
                    case "LinkedOutstandingJobCount":
                        intRecordType = 1;
                        break;
                    case "LinkedCompletedJobCount":
                        intRecordType = 2;
                        break;
                    case "SuspendedJobCount":
                        intRecordType = 3;
                        break;
                    default:
                        break;
                }
                if (intRecordType == -1) return;

                try
                {
                    string strParameter = intVisitID.ToString() + ",";
                    var GetValue = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter();
                    GetValue.ChangeConnectionString(strConnectionStringREADONLY);
                    strRecordIDs = GetValue.sp06130_OM_Visit_Manager_Get_Linked_Job_IDs(intVisitID.ToString() + ",", intRecordType).ToString();
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view linked jobs [" + ex.Message + "].\n\nPlease try again. If the problem persists contact Technical Support.", "View Linked Jobs", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "operations_job");
            }
        }

        private void repositoryItemHyperLinkEditClientSignature_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFirstPartOfPathWithBackslash = strSignaturePath + "\\";
            string strClientPath = view.GetRowCellValue(view.FocusedRowHandle, "ImagesFolderOM").ToString() + "\\Signatures";
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "ClientSignaturePath").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Client Signature Linked - unable to proceed.", "View Client Signature", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(Path.Combine(strFirstPartOfPathWithBackslash + strClientPath + strFile));
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view Client Signature: " + strFile + ".\n\nThe file may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Client Signature", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void repositoryItemHyperLinkEditSelfBillingInvoice_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strSelfBillingInvoice = view.GetRowCellValue(view.FocusedRowHandle, "SelfBillingInvoice").ToString();
            if (strSelfBillingInvoice != "Yes")
            {
                XtraMessageBox.Show("This visit has not been invoiced yet - Unable to View Self-Billing Invoice.", "View Self-Billing Invoice.", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            int intVisitID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "VisitID"));
            if (intVisitID <= 0) return;
            OM_View_Self_Billing_Invoice viewInvoice = new OM_View_Self_Billing_Invoice();
            viewInvoice.View_Self_Billing_Invoice(this, this.GlobalSettings, strConnectionStringREADONLY, intVisitID.ToString() + ",");
        }

        private void repositoryItemHyperLinkEditClientInvoiceID_OpenLink(object sender, OpenLinkEventArgs e)
        {

        }

        #endregion


        #region GridView - Job

        private void gridControlJob_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                   if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        LoadLinkedRecords();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewJob_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            switch (e.Column.FieldName)
            {
                case "DoNotInvoiceClient":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "DoNotInvoiceClient")) == 1)
                        {
                            //e.Appearance.BackColor = Color.LightCoral;
                            //e.Appearance.BackColor2 = Color.Red;
                            e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                            e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                            e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        }
                    }
                    break;
                case "DoNotPayContractor":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "DoNotPayContractor")) == 1)
                        {
                            e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                            e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                            e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        }
                    }
                    break;
                case "LinkedLabourCount":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedLabourCount")) <= 0)
                        {
                            e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                            e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                            e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        }
                    }
                    break;
            }
        }

        private void gridViewJob_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            try
            {
                GridView view = (GridView)sender;
                switch (e.Column.FieldName)
                {
                    case "LinkedJobCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedJobCount")) <= 1) e.RepositoryItem = emptyEditor;
                        break;
                    case "LinkedDocumentCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "DaysUntilDue":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "DaysUntilDue")) >= 99999) e.RepositoryItem = emptyEditor;
                        break;
                    case "LinkedPictureCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedPictureCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception) { }
        }

        private void gridViewJob_CustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
        }

        private void gridViewJob_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewJob_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.Jobs;
            SetMenuStatus();
        }

        private void gridViewJob_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Jobs;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bbiDatasetCreate.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                bbiDatasetSelection.Enabled = (view.RowCount > 0);
                bsiDataset.Enabled = true;
                bbiDatasetSelectionInverted.Enabled = true;
                bbiDatasetManager.Enabled = true;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewJob_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedJobCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedJobCount")) <= 1) e.Cancel = true;
                    break;
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) <= 0) e.Cancel = true;
                    break;
                case "DaysUntilDue":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("DaysUntilDue")) >= 99999) e.Cancel = true;
                    break;
                case "LinkedPictureCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedPictureCount")) <= 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }
       
        private void repositoryItemHyperLinkEditLinkedDocumentsJob_OpenLink(object sender, OpenLinkEventArgs e)
        {
        }

        private void repositoryItemHyperLinkEditViewJob_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intSiteContractID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "SiteContractID"));
            int intJobSubTypeID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "JobSubTypeID"));
            if (intSiteContractID <= 0 || intJobSubTypeID <= 0) return;
            string strRecordIDs = "";
            try
            {
                var GetValue = new DataSet_OM_JobTableAdapters.QueriesTableAdapter();
                GetValue.ChangeConnectionString(strConnectionStringREADONLY);
                strRecordIDs = GetValue.sp06032_OM_Job_Manager_Get_Linked_Job_IDs(intSiteContractID.ToString() + ",", intJobSubTypeID.ToString() + ",").ToString();
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view linked jobs [" + ex.Message + "].\n\nPlease try again. If the problem persists contact Technical Support.", "View Linked Jobs", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "operations_job");
        }

        private void repositoryItemHyperLinkEditPictures_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedPictureCount":
                    {
                        Open_Picture_Viewer(view.GetRowCellValue(view.FocusedRowHandle, "JobID").ToString() + ",", Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "ClientID")), 1);
                    }
                    break;
            }
        }


        #endregion


        #region GridView - Billing Requirement

        private void gridControlBillingRequirement_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("blockedit".Equals(e.Button.Tag))
                    {
                        Block_Edit();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        LoadLinkedRecords();
                    }
                    else if ("add_from_template".Equals(e.Button.Tag))
                    {
                        Add_From_Billing_Requirement_Template();
                    }
                    else if ("template_manager".Equals(e.Button.Tag))
                    {
                        if (!iBool_BillingRequirementTemplateManager) return;
                        var fChildForm = new frm_Core_Billing_Requirement_Template_Manager();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();
                        System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    else if ("add_from_parent".Equals(e.Button.Tag))
                    {
                        GridView view = (GridView)gridControlVisit.MainView;
                        view.PostEditor();
                        int[] intRowHandles;
                        if (!iBool_AllowEdit) return;
                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length <= 0)
                        {
                            XtraMessageBox.Show("Select at least one Visit to re-create the Billing Requirements for before proceeding.", "Re-Create Billing Requirements", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        if (XtraMessageBox.Show("WARNING... You are about to re-create the Billing Requirements for " + intRowHandles.Length.ToString() + " Visit(s).\n\nIf you proceed, any existing Billing Requirements for the selected Visit(s) will be deleted first.", "Re-Create Billing Requirements", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;

                        if (!splashScreenManager.IsSplashFormVisible)
                        {
                            this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager.ShowWaitForm();
                            splashScreenManager.SetWaitFormDescription("Re-Creating Billing Requirements...");
                        }

                        StringBuilder sb = new StringBuilder();
                        try
                        {
                            using (var ReCreateRecords = new DataSet_Common_FunctionalityTableAdapters.QueriesTableAdapter())
                            {
                                ReCreateRecords.ChangeConnectionString(strConnectionString);
                                foreach (int intRowHandle in intRowHandles)
                                {
                                    int intSiteContractID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "SiteContractID"));
                                    int intVisitID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "VisitID"));
                                    sb.Append(intVisitID.ToString() + ",");
                                    i_str_AddedRecordIDsBillingRequirement += ReCreateRecords.sp01043_Core_Billing_Requirement_Re_Add_From_Parent(intSiteContractID, intBillingRequirementLinkedRecordTypeID-1, intVisitID, intBillingRequirementLinkedRecordTypeID).ToString() + ";";
                                }
                            }
                        }
                        catch (Exception) { }
                        LoadLinkedRecords();

                        Refresh_Updated_Team_Rows(null, dsVisitsToWorkWith.Tables[0]);
                        Refresh_Updated_Visit_Rows(sb.ToString(), null);

                        if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                    }
                    else if ("drill_site_contract".Equals(e.Button.Tag))
                    {
                        GridView view = (GridView)gridControlBillingRequirement.MainView;
                        Drill_To_Site_Contract(view);
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridControlBillingRequirement_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridControlBillingRequirement_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.BillingRequirement;
            SetMenuStatus();
        }

        private void gridControlBillingRequirement_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.BillingRequirement;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewBillingRequirement_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            GridView view = (GridView)sender;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            switch (view.FocusedColumn.Name)
            {
                case "colRequirementFulfilled":
                case "colRemarks3":
                    try
                    {
                        int? intChecked = (view.FocusedColumn.Name == "colRequirementFulfilled" ? Convert.ToInt32(e.Value) : Convert.ToInt32(view.GetFocusedRowCellValue("RequirementFulfilled")));
                        string strRemarks = (view.FocusedColumn.Name == "colRemarks3" ? e.Value.ToString() : view.GetFocusedRowCellValue("Remarks").ToString());
                        int intChangedByStaffID = (intChecked == 1 ? GlobalSettings.UserID : 0);      
                        string strChangedByName = (intChecked == 1 ? GlobalSettings.UserForename + " " + GlobalSettings.UserSurname : "");
                        string strRequirementIDs = view.GetFocusedRowCellValue("BillingRequirementID").ToString() + ",";
                        int[] intRowHandles = new int[1] ;
                        intRowHandles[0] = view.FocusedRowHandle;
                        Update_Billing_Requirement(view, intRowHandles, intChecked, intChangedByStaffID, strChangedByName, strRemarks, strRequirementIDs);
                    }
                    catch (Exception) { };
                    break;
                default:
                    break;
            }
        }


        private void Add_From_Billing_Requirement_Template()
        {
            GridView parentView = (GridView)gridControlVisit.MainView;
            int[] intRowHandles = parentView.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Visits to add the Billing Requirement Template to by clicking on them then try again.", "Add Billing Requirements From Template", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else if (intCount > 1)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + intCount.ToString() + " Visits selected. If you proceed new Billing Requirements will be created for each of these records from the chosen Template.\n\nProceed?", "Add Billing Requirements From Template", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;
            }
            GridView view = (GridView)gridControlBillingRequirement.MainView;
            view.PostEditor();

            var fChildForm = new frm_Core_Select_Billing_Requirement_Template();
            fChildForm.GlobalSettings = this.GlobalSettings;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                int intTemplateID = fChildForm.intSelectedTemplateHeaderID;
                if (intTemplateID <= 0) return;

                StringBuilder sb = new StringBuilder();
                foreach (int i in intRowHandles)  // Selected Contracts //
                {
                    sb.Append(parentView.GetRowCellValue(i, "VisitID").ToString() + ",");
                }
                if (string.IsNullOrWhiteSpace(sb.ToString())) return;

                i_str_AddedRecordIDsBillingRequirement = "";
                using (var AddRecords = new DataSet_Common_FunctionalityTableAdapters.QueriesTableAdapter())
                {
                    AddRecords.ChangeConnectionString(strConnectionString);
                    try
                    {
                        i_str_AddedRecordIDsBillingRequirement += AddRecords.sp01042_Core_Billing_Requirement_Item_Add_From_Template(sb.ToString(), intBillingRequirementLinkedRecordTypeID, intTemplateID).ToString();
                    }
                    catch (Exception) { }
                }
                LoadLinkedRecords();
                Refresh_Updated_Team_Rows(null, dsVisitsToWorkWith.Tables[0]);
                Refresh_Updated_Visit_Rows(sb.ToString(), null);
            }
        }

        private void Update_Billing_Requirement(GridView view, int[] intRowHandles, int? intChecked, int intChangedByStaffID, string strChangedByName, string strRemarks, string strRequirementIDs)
        {
            // Update DB with changes //
            try
            {
                using (DataSet_OM_BillingTableAdapters.QueriesTableAdapter UpdateDatabase = new DataSet_OM_BillingTableAdapters.QueriesTableAdapter())
                {
                    UpdateDatabase.ChangeConnectionString(strConnectionString);
                    UpdateDatabase.sp06447_OM_Self_Billing_Billing_Requirements_Update(strRequirementIDs, intChecked, strRemarks, intChangedByStaffID);
                }
                foreach (int rowHandle in intRowHandles)
                {
                    if (intChecked != null) view.SetRowCellValue(rowHandle, "CheckedByStaff", strChangedByName);
                    if (intChecked != null) view.SetRowCellValue(rowHandle, "CheckedByStaffID", intChangedByStaffID);
                    if (intChecked != null) view.SetRowCellValue(rowHandle, "RequirementFulfilled", intChecked);
                    if (strRemarks != null) view.SetRowCellValue(rowHandle, "Remarks", strRemarks);
                }
                // Get list of Visits so we can refresh parent teams and visits with updated metrics //
                StringBuilder sb = new StringBuilder();
                foreach (int intRowHandle in intRowHandles)
                {
                    sb.Append(view.GetRowCellValue(intRowHandle, view.Columns["LinkedToRecordID"]).ToString() + ",");
                }
                Refresh_Updated_Team_Rows(null, dsVisitsToWorkWith.Tables[0]);
                Refresh_Updated_Visit_Rows(sb.ToString(), null);
            }
            catch (Exception) { };
        }

        #endregion


        #region GridView - Billing Warning

        private void gridControlBillingException_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("blockedit".Equals(e.Button.Tag))
                    {
                        Block_Edit();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        LoadLinkedRecords();
                    }
                    else if ("recreate".Equals(e.Button.Tag))
                    {
                        ReCreateWarnings();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("drill_site_contract".Equals(e.Button.Tag))
                    {
                        GridView view = (GridView)gridControlBillingException.MainView;
                        Drill_To_Site_Contract(view);
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridControlBillingException_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridControlBillingException_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.BillingException;
            SetMenuStatus();
        }

        private void gridControlBillingException_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.BillingException;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewBillingException_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            GridView view = (GridView)sender;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            switch (view.FocusedColumn.Name)
            {
                case "colAuthorised":
                case "colRemarks4":
                    try
                    {
                        int intAuthorised = (view.FocusedColumn.Name == "colAuthorised" ? Convert.ToInt32(e.Value) : Convert.ToInt32(view.GetFocusedRowCellValue("Authorised")));
                        string strRemarks = (view.FocusedColumn.Name == "colRemarks4" ? e.Value.ToString() : view.GetFocusedRowCellValue("Remarks").ToString());
                        int intAuthorisedByStaffID = (intAuthorised == 1 ? GlobalSettings.UserID : 0);
                        string strAuthorisedByName = (intAuthorised == 1 ? GlobalSettings.UserForename + " " + GlobalSettings.UserSurname : "");
                        string strBillingWarningIDs = view.GetFocusedRowCellValue("BillingWarningID").ToString() + ",";
                        int[] intRowHandles = new int[1];
                        intRowHandles[0] = view.FocusedRowHandle;
                        Update_Billing_Warning(view, intRowHandles, intAuthorised, intAuthorisedByStaffID, strAuthorisedByName, strRemarks, strBillingWarningIDs);
                    }
                    catch (Exception) { };
                    break;
                default:
                    break;
            }
        }

        private void Update_Billing_Warning(GridView view, int[] intRowHandles, int? intAuthorised, int intAuthorisedByStaffID, string strAuthorisedByName, string strRemarks, string strBillingWarningIDs)
        {
            // Update DB with changes //
            try
            {
                using (DataSet_OM_BillingTableAdapters.QueriesTableAdapter UpdateDatabase = new DataSet_OM_BillingTableAdapters.QueriesTableAdapter())
                {
                    UpdateDatabase.ChangeConnectionString(strConnectionString);
                    UpdateDatabase.sp06449_OM_Self_Billing_Billing_Warning_Update(strBillingWarningIDs, intAuthorised, strRemarks, intAuthorisedByStaffID);
                }
                foreach (int rowHandle in intRowHandles)
                {
                    if (intAuthorised != null) view.SetRowCellValue(rowHandle, "AuthorisedByStaff", strAuthorisedByName);
                    if (intAuthorised != null) view.SetRowCellValue(rowHandle, "AuthorisedByStaffID", intAuthorisedByStaffID);
                    if (intAuthorised != null) view.SetRowCellValue(rowHandle, "Authorised", intAuthorised);
                    if (strRemarks != null) view.SetRowCellValue(rowHandle, "Remarks", strRemarks);
                }
                // Get list of Visits so we can refresh parent teams and visits with updated metrics //
                StringBuilder sb = new StringBuilder();
                foreach (int intRowHandle in intRowHandles)
                {
                    sb.Append(view.GetRowCellValue(intRowHandle, view.Columns["LinkedToRecordID"]).ToString() + ",");
                }
                Refresh_Updated_Team_Rows(null, dsVisitsToWorkWith.Tables[0]);
                Refresh_Updated_Visit_Rows(sb.ToString(), null);
            }
            catch (Exception) { };
        }

        private void ReCreateWarnings()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = (GridView)gridControlVisit.MainView;
            if (!iBool_AllowAdd) return;
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more Linked Billing Requirements to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Checks passed so delete selected record(s) //
            string strMessage = "You have " + (intCount == 1 ? "1 Visit" : Convert.ToString(intRowHandles.Length) + " Visits") + " selected for re-creating Warnings!\n\nProceed?\n\n<color=red>Warning: If you proceed any existing warnings will be deleted and then re-created.</color>";
            if (XtraMessageBox.Show(strMessage, "Re-Create Warnings", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
            {
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager1.ShowWaitForm();
                splashScreenManager1.SetWaitFormDescription("Creating Warnings...");

                DataTable dtRecreateWarningsVisits = new DataTable("Visits");
                dtRecreateWarningsVisits.Columns.Add("VisitID", typeof(int));
                dtRecreateWarningsVisits.Rows.Clear();

                StringBuilder sb = new StringBuilder();
                foreach (int intRowHandle in intRowHandles)
                {
                    sb.Append(view.GetRowCellValue(intRowHandle, "VisitID").ToString() + ",");
                    dtRecreateWarningsVisits.Rows.Add(Convert.ToInt32(view.GetRowCellValue(intRowHandle, "VisitID")));
                }

                try
                {
                    using (var RemoveRecords = new DataSet_Common_FunctionalityTableAdapters.QueriesTableAdapter())
                    {
                        RemoveRecords.ChangeConnectionString(strConnectionString);
                        RemoveRecords.sp01000_Core_Delete("billing_warning_from_visit", sb.ToString());  // Remove the records from the DB in one go // 
                        }
                }
                catch (Exception) { } 
                try
                {
                    using (DataSet_OM_BillingTableAdapters.QueriesTableAdapter UpdateDatabase = new DataSet_OM_BillingTableAdapters.QueriesTableAdapter())
                    {
                        UpdateDatabase.ChangeConnectionString(strConnectionString);
                        UpdateDatabase.sp06450_OM_Self_Billing_Billing_Get_Warnings(dtRecreateWarningsVisits);
                    }
                }
                catch (Exception) { };
                
                LoadLinkedRecords();

                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
            }
        }

        #endregion


        #region Date Range \ From Today Filter Panel

        private void spinEditWeekNumber_EditValueChanged(object sender, EventArgs e)
        {
            SpinEdit se = (SpinEdit)sender;
            int intWeek = Convert.ToInt32(se.EditValue);
            int intYear = Convert.ToInt32(spinEditYear.EditValue);
            Calculate_Date_Range(intWeek, intYear);
        }
        private void spinEditYear_EditValueChanged(object sender, EventArgs e)
        {
            int intWeek = Convert.ToInt32(spinEditWeekNumber.EditValue);
            SpinEdit se = (SpinEdit)sender;
            int intYear = Convert.ToInt32(se.EditValue);
            Calculate_Date_Range(intWeek, intYear);
        }
        private void Calculate_Date_Range(int intWeek, int intYear)
        {
            DateTime dtStart = BaseObjects.DateFunctions.FirstDateOfWeekISO8601(intYear, intWeek);
            dateEditFromDate.DateTime = dtStart;
            dateEditToDate.DateTime = dtStart.AddDays(7).AddMilliseconds(-5);
            popupContainerEditDateRange.EditValue = PopupContainerEditDateRange_Get_Selected();

            // Calculate default Self-Biller Number //
            string strShortYear = intYear.ToString().Substring(intYear.ToString().Length - 2);  // Last 2 digits only //
            textEditSBNumber.Text = "SB" + intWeek.ToString().PadLeft(2, '0') + strShortYear;
        }

        private void checkEditWeekNumber_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                Set_Date_Filter_Controls_ReadOnly(false);
                Calculate_Date_Range(Convert.ToInt32(spinEditWeekNumber.EditValue), Convert.ToInt32(spinEditYear.EditValue));
            }
        }
        private void checkEditDateRange_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked) Set_Date_Filter_Controls_ReadOnly(true);
        }
        private void Set_Date_Filter_Controls_ReadOnly(bool WeekReadOnly)
        {
            spinEditWeekNumber.Properties.ReadOnly = (WeekReadOnly ? true : false);
            spinEditYear.Properties.ReadOnly = (WeekReadOnly ? true : false);
            popupContainerEditDateRange.Properties.ReadOnly = (!WeekReadOnly ? true : false);
        }

        private void btnDateRangeOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }
        private void popupContainerEditDateRange_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditDateRange_Get_Selected();
        }
        private string PopupContainerEditDateRange_Get_Selected()
        {
            string strValue = "";
            if (dateEditFromDate.DateTime == DateTime.MinValue)
            {
                i_dtStart = new DateTime(1900, 1, 1);
            }
            else
            {
                i_dtStart = dateEditFromDate.DateTime;
            }
            if (dateEditToDate.DateTime == DateTime.MinValue)
            {
                i_dtEnd = new DateTime(1900, 1, 1);
            }
            else
            {
                i_dtEnd = dateEditToDate.DateTime;
            }
            if (dateEditFromDate.DateTime == DateTime.MinValue && dateEditToDate.DateTime == DateTime.MinValue)
            {
                strValue = "No Date Range Filter";
            }
            else
            {
                strValue = (i_dtStart == new DateTime(1900, 1, 1) ? "No Start" : i_dtStart.ToString("dd/MM/yyyy HH:mm")) + " - " + (i_dtEnd == new DateTime(1900, 1, 1) ? "No End" : i_dtEnd.ToString("dd/MM/yyyy HH:mm"));
            }
            return strValue;
        }
        private void btnDateRangeFromTodayOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        #endregion


        private void buttonEditClientFilter_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "clear")
            {
                i_str_selected_client_ids = "";
                i_str_selected_client_names = "";
                buttonEditClientFilter.Text = "";
                
                // Update Filter control's tooltip //
                superToolTipSetupArgsClientContractFilter.Contents.Text = "I store the currently applied Client Filter.\n\nNo Filter Set.";
            }
            else if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var fChildForm = new frm_Core_Select_Client_Site();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intAllowChildSelection = 0;
                fChildForm.intMustSelectChildren = 0;
                fChildForm.strPassedInParentIDs = i_str_selected_client_ids;
                fChildForm.i_dtStart = DateTime.Today.AddMonths(-12);  // back a year //
                fChildForm.i_dtEnd = DateTime.Today.AddMonths(12);  // forward a year //
                fChildForm.intFilterSummerMaintenanceClient = 1;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    i_str_selected_client_ids = fChildForm.strSelectedParentIDs;
                    i_str_selected_client_names = fChildForm.strSelectedParentDescriptions;
                    buttonEditClientFilter.Text = i_str_selected_client_names;
                   
                    // Update Filter control's tooltip //
                    string strTooltipText = i_str_selected_client_names.Replace(", ", "\n");
                    superToolTipSetupArgsClientContractFilter.Contents.Text = "I store the currently applied Client Filter.\n\n" + strTooltipText;

                    if (!string.IsNullOrWhiteSpace(fChildForm.strSelectedChildIDs))
                    {
                        i_str_selected_site_ids = fChildForm.strSelectedChildIDs;
                        i_str_selected_site_names = fChildForm.strSelectedChildDescriptions;
                        buttonEditSiteFilter.Text = i_str_selected_site_names;
                        
                        // Update Filter control's tooltip //
                        strTooltipText = i_str_selected_site_names.Replace(", ", "\n");
                        superToolTipSetupArgsSiteContractFilter.Contents.Text = "I store the currently applied Site Filter.\n\n" + strTooltipText;
                    }
                    else
                    {
                        // Update Filter control's tooltip //
                        strTooltipText = i_str_selected_site_names.Replace(", ", "\n");
                        superToolTipSetupArgsSiteContractFilter.Contents.Text = "I store the currently applied Site Filter.\n\nNo Filter Set.";
                    }
                }
            }
        }

        private void buttonEditSiteFilter_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "clear")
            {
                i_str_selected_site_ids = "";
                i_str_selected_site_names = "";
                buttonEditSiteFilter.Text = "";
                
                // Update Filter control's tooltip //
                superToolTipSetupArgsSiteContractFilter.Contents.Text = "I store the currently applied Site Filter.\n\nNo Filter Set.";
            }
            else if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var fChildForm = new frm_Core_Select_Client_Site();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intAllowChildSelection = 1;
                fChildForm.intMustSelectChildren = 1;
                fChildForm.strPassedInParentIDs = i_str_selected_client_ids;
                fChildForm.strPassedInChildIDs = i_str_selected_site_ids;
                fChildForm.i_dtStart = DateTime.Today.AddMonths(-12);  // back a year //
                fChildForm.i_dtEnd = DateTime.Today.AddMonths(12);  // forward a year //
                fChildForm.intFilterSummerMaintenanceClient = 1;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    i_str_selected_client_ids = fChildForm.strSelectedParentIDs;
                    i_str_selected_client_names = fChildForm.strSelectedParentDescriptions;
                    buttonEditClientFilter.Text = i_str_selected_client_names;

                    // Update Filter control's tooltip //
                    string strTooltipText = i_str_selected_client_names.Replace(", ", "\n");
                    superToolTipSetupArgsClientContractFilter.Contents.Text = "I store the currently applied Client Filter.\n\n" + strTooltipText;

                    i_str_selected_site_ids = fChildForm.strSelectedChildIDs;
                    i_str_selected_site_names = fChildForm.strSelectedChildDescriptions;
                    buttonEditSiteFilter.Text = i_str_selected_site_names;

                    // Update Filter control's tooltip //
                    strTooltipText = i_str_selected_site_names.Replace(", ", "\n");
                    superToolTipSetupArgsSiteContractFilter.Contents.Text = "I store the currently applied Site Filter.\n\n" + strTooltipText;
                }
            }
        }

        private void buttonEditExcludeClientFilter_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "clear")
            {
                i_str_selected_exclude_client_ids = "";
                i_str_selected_exclude_client_names = "";
                buttonEditExcludeClientFilter.Text = "";

                // Update Filter control's tooltip //
                superToolTipSetupArgsExcludedClientContractFilter.Contents.Text = "I store the currently applied Excluded Client Filter.\n\nNo Filter Set.";
            }
            else if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var fChildForm = new frm_Core_Select_Client_Site();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intAllowChildSelection = 0;
                fChildForm.intMustSelectChildren = 0;
                fChildForm.strPassedInParentIDs = i_str_selected_exclude_client_ids;
                fChildForm.i_dtStart = DateTime.Today.AddMonths(-12);  // back a year //
                fChildForm.i_dtEnd = DateTime.Today.AddMonths(12);  // forward a year //
                fChildForm.intFilterSummerMaintenanceClient = 1;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    i_str_selected_exclude_client_ids = fChildForm.strSelectedParentIDs;
                    i_str_selected_exclude_client_names = fChildForm.strSelectedParentDescriptions;
                    buttonEditExcludeClientFilter.Text = i_str_selected_exclude_client_names;

                    // Update Filter control's tooltip //
                    string strTooltipText = i_str_selected_exclude_client_names.Replace(", ", "\n");
                    superToolTipSetupArgsExcludedClientContractFilter.Contents.Text = "I store the currently applied Excluded Client Filter.\n\n" + strTooltipText;
                }
            }
        }

        private void Drill_To_Site_Contract(GridView view)
        {
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0) return;

            string strRecordIDs = "";
            StringBuilder sb = new StringBuilder();
            try
            {
                foreach (int intRowHandle in intRowHandles)
                {
                    sb.Append(view.GetRowCellValue(intRowHandle, "LinkedToRecordID").ToString() + ",");
                }
                using (var GetValue = new DataSet_OM_BillingTableAdapters.QueriesTableAdapter())
                {
                    GetValue.ChangeConnectionString(strConnectionString + "Connect Timeout=3600;");
                    GetValue.ChangeCommandTimeout("dbo.sp06526_OM_Get_SiteContractIDs_From_VisitIDs", 3600);  // Timeout on SQL Connection to 1 hour - need line above too. //               
                    strRecordIDs = GetValue.sp06526_OM_Get_SiteContractIDs_From_VisitIDs(sb.ToString()).ToString();
                }
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view visits linked to the parent Site Contract(s) [" + ex.Message + "].\n\nPlease try again. If the problem persists contact Technical Support.", "View Parent Site Contracts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "operations_site_contracts");
        }


        #region Labour Filter Panel

        private void btnLabourFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void popupContainerEditLabourFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditLabourFilter_Get_Selected();
        }

        private string PopupContainerEditLabourFilter_Get_Selected()
        {
            i_str_selected_Labour_Team_ids = "";    // Reset any prior values first //
            i_str_selected_Labour_Team_descriptions = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl12.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_Labour_Team_ids = "";
                return "No Labour Filter";

            }
            else if (selection12.SelectedCount <= 0)
            {
                i_str_selected_Labour_Team_ids = "";
                return "No Labour Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_Labour_Team_ids += Convert.ToInt32(view.GetRowCellValue(i, "ID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_Labour_Team_descriptions = Convert.ToString(view.GetRowCellValue(i, "Name"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_Labour_Team_descriptions += ", " + Convert.ToString(view.GetRowCellValue(i, "Name"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_Labour_Team_descriptions;
        }

        private void popupContainerEditLabourFilter_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("reload".Equals(e.Button.Tag))
                {
                    i_str_selected_Labour_Team_ids = "";
                    i_str_selected_Labour_Team_descriptions = "";
                    selection12.ClearSelection();
                    gridControl12.BeginUpdate();
                    sp06028_OM_Labour_FilterTableAdapter.Fill(dataSet_GC_Summer_Core.sp06028_OM_Labour_Filter, (checkEditTeam.Checked ? 1 : 0));
                    gridControl12.EndUpdate();
                    popupContainerEditLabourFilter.Text = PopupContainerEditLabourFilter_Get_Selected();
                }
            }
            else if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                i_str_selected_Labour_Team_ids = "";
                i_str_selected_Labour_Team_descriptions = "";
                selection12.ClearSelection();
                popupContainerEditLabourFilter.Text = PopupContainerEditLabourFilter_Get_Selected();
            }
        }

        #endregion


        #region Visit Type Filter Panel

        private void btnVisitTypeFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void popupContainerEditVisitTypeFilter_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                i_str_selected_VisitType_ids = "";
                i_str_selected_VisitTypes = "";
                selection2.ClearSelection();
                popupContainerEditVisitTypeFilter.Text = PopupContainerEditVisitTypeFilter_Get_Selected();
            }
        }

        private void popupContainerEditVisitTypeFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditVisitTypeFilter_Get_Selected();
        }

        private string PopupContainerEditVisitTypeFilter_Get_Selected()
        {
            i_str_selected_VisitType_ids = "";    // Reset any prior values first //
            i_str_selected_VisitTypes = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl2.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_VisitType_ids = "";
                return "No Visit Type Filter";

            }
            else if (selection2.SelectedCount <= 0)
            {
                i_str_selected_VisitType_ids = "";
                return "No Visit Type Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_VisitType_ids += Convert.ToString(view.GetRowCellValue(i, "ID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_VisitTypes = Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_VisitTypes += ", " + Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_VisitTypes;
        }

        #endregion


        #region Visit Category Filter Panel

        private void btnVisitCategoryFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void popupContainerEditVisitCategoryFilter_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                i_str_selected_VisitCategory_ids = "";
                i_str_selected_VisitCategories = "";
                selection4.ClearSelection();
                popupContainerEditVisitCategoryFilter.Text = PopupContainerEditVisitCategoryFilter_Get_Selected();
            }
        }

        private void popupContainerEditVisitCategoryFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditVisitCategoryFilter_Get_Selected();
        }

        private string PopupContainerEditVisitCategoryFilter_Get_Selected()
        {
            i_str_selected_VisitCategory_ids = "";    // Reset any prior values first //
            i_str_selected_VisitCategories = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl4.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_VisitCategory_ids = "";
                return "No Visit Category Filter";

            }
            else if (selection4.SelectedCount <= 0)
            {
                i_str_selected_VisitCategory_ids = "";
                return "No Visit Category Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_VisitCategory_ids += Convert.ToString(view.GetRowCellValue(i, "ID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_VisitCategories = Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_VisitCategories += ", " + Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_VisitCategories;
        }

        #endregion



        #region Warning Filter Panel

        private void btnWarningFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void popupContainerEditWarningsFilter_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                i_str_selected_Warning_ids = "";
                i_str_selected_Warnings = "";
                selection1.ClearSelection();
                popupContainerEditWarningsFilter.Text = PopupContainerEditWarningFilter_Get_Selected();
            }
        }

        private void popupContainerEditWarnings_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditWarningFilter_Get_Selected();
        }

        private string PopupContainerEditWarningFilter_Get_Selected()
        {
            i_str_selected_Warning_ids = "";    // Reset any prior values first //
            i_str_selected_Warnings = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl1.MainView;
            if (view.DataRowCount <= 0 || selection1.SelectedCount <= 0)
            {
                return "No Warnings Filter";
            }
            else
            {
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_Warning_ids += Convert.ToString(view.GetRowCellValue(i, "ID")) + ",";
                        i_str_selected_Warnings += ", " + Convert.ToString(view.GetRowCellValue(i, "Description"));
                    }
                    if (i_str_selected_Warnings.StartsWith(", ")) i_str_selected_Warnings = i_str_selected_Warnings.Substring(2, i_str_selected_Warnings.Length - 2);
                }
            }
            return i_str_selected_Warnings;
        }

        #endregion


        #region Invoicing Parameters

        private void simpleButtonInvoicingParameters_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }
        
        private void repositoryItemPopupContainerEditInvoicingParameters_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditInvoicingParameters_Get_Value();
        }

        private string PopupContainerEditInvoicingParameters_Get_Value()
        {
            string strValue = "";
            strValue = "Auto-Send Created Invoices " + (checkEditAutoSendCreatedInvoices.Checked ? "On" : "Off");
            strValue += ", Exclude Teams with Outstanding Requirements " + (checkEditExcludeTeamsWithOutstandingRequirements.Checked ? "On" : "Off");
            strValue += ", Exclude Teams with Outstanding Warnings " + (checkEditExcludeTeamsWithOutstandingWarnings.Checked ? "On" : "Off");
            strValue += ", Exclude Visits with Outstanding Requirements " + (checkEditExcludeVisitWithRequirements.Checked ? "On" : "Off");
            strValue += ", Exclude Visits with Outstanding Warnings " + (checkEditExcludeVisitsWithWarnings.Checked ? "On" : "Off");

            // Update Filter control's tooltip //
            string strTooltipValue = "";
            strTooltipValue = "Auto-Send Created Invoices " + (checkEditAutoSendCreatedInvoices.Checked ? "<b>On</b>" : "<b>Off</b>");
            strTooltipValue += "\nExclude Teams with Outstanding Requirements " + (checkEditExcludeTeamsWithOutstandingRequirements.Checked ? "<b>On</b>" : "<b>Off</b>");
            strTooltipValue += "\nExclude Teams with Outstanding Warnings " + (checkEditExcludeTeamsWithOutstandingWarnings.Checked ? "<b>On</b>" : "<b>Off</b>");
            strTooltipValue += "\nExclude Visits with Outstanding Requirements " + (checkEditExcludeVisitWithRequirements.Checked ? "<b>On</b>" : "<b>Off</b>");
            strTooltipValue += "\nExclude Visits with Outstanding Warnings " + (checkEditExcludeVisitsWithWarnings.Checked ? "<b>On</b>" : "<b>Off</b>");
            superToolTipSetupArgsInvoicingParameters.Contents.Text = "I control if Teams can be invoiced for Visits with outstanding <b>Requirements</b> and <b>Warnings</b>.\n\nTick to disallow or un-tick to allow.\n\n" + strTooltipValue;

            return strValue;
        }

        #endregion


        #region DataSet

        public override void OnDatasetCreateEvent(object sender, EventArgs e)
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strCurrentID = "";
            string strSelectedIDs = ",";
            string strColumnName = "";
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Jobs:
                    view = (GridView)gridControlJob.MainView;
                    strColumnName = "JobID";
                    break;
                default:
                    return;
            }
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to add to the dataset before proceeding!", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            intCount = 0;
            foreach (int intRowHandle in intRowHandles)
            {
                strCurrentID = view.GetRowCellValue(intRowHandle, view.Columns[strColumnName]).ToString() + ',';
                if (!strSelectedIDs.Contains("," + strCurrentID))  // Put leading comma in temporarily for checking purposes //
                {
                    strSelectedIDs += strCurrentID;
                    intCount++;
                }
            }
            strSelectedIDs = strSelectedIDs.Remove(0, 1);  // Remove leading ',' //
            CreateDataset("Job", intCount, strSelectedIDs);
        }

        private void CreateDataset(string strType, int intRecordCount, string strSelectedRecordIDs)
        {
            frmDatasetCreate fChildForm = new frmDatasetCreate();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.i_str_dataset_type = strType;
            fChildForm.i_int_selected_job_count = intRecordCount;

            if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Dataset Created.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            else
            {
                int intAction = fChildForm.i_int_returned_action;
                string strName = fChildForm.i_str_dataset_name;
                strType = fChildForm.i_str_dataset_type;
                int intDatasetID = fChildForm.i_int_selected_dataset;
                string strDatasetType = fChildForm.i_str_dataset_type;
                int intReturnValue = 0;
                switch (intAction)
                {
                    case 1:  // Create New dataset and dataset members //
                        Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter AddDataset = new Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter();
                        AddDataset.ChangeConnectionString(strConnectionString);

                        intReturnValue = Convert.ToInt32(AddDataset.sp01224_AT_Dataset_create(strDatasetType, strName, this.GlobalSettings.UserID, strSelectedRecordIDs));
                        if (intReturnValue <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while attempting to create the dataset!\n\nNo Dataset Created.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        return;
                    case 2:  // Append to dataset members //
                        Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter AppendDataset = new Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter();
                        AppendDataset.ChangeConnectionString(strConnectionString);
                        intReturnValue = Convert.ToInt32(AppendDataset.sp01226_AT_Dataset_append_dataset_members(intDatasetID, strSelectedRecordIDs));
                        if (intReturnValue <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while attempting to append the selected records to the dataset!\n\nNo Records Append to the Dataset.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        return;
                    case 3: // Replace dataset members //
                        Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter ReplaceDataset = new Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter();
                        ReplaceDataset.ChangeConnectionString(strConnectionString);
                        intReturnValue = Convert.ToInt32(ReplaceDataset.sp01227_AT_Dataset_replace_dataset_members(intDatasetID, strSelectedRecordIDs));
                        if (intReturnValue <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while attempting to replace the records in the selected dataset!\n\nNo Records Replaced in the Dataset.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        return;
                    default:
                        return;
                }
            }
        }

        public override void OnDatasetSelectionEvent(object sender, EventArgs e)
        {
            frmDatasetSelection fChildForm = new frmDatasetSelection();
            Form frmMain = this.MdiParent;
            int intSelectedDataset;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Jobs:
                    frmMain.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.i_str_dataset_types = "Job,"; // Comma seperated list needs to be passed //

                    if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
                    {
                        return;
                    }
                    intSelectedDataset = fChildForm.i_int_selected_dataset;
                    DS_Selection1.SelectRecordsFromDataset(intSelectedDataset, "JobID");
                    break;
                default:
                    break;
            }
        }

        public override void OnDatasetSelectionInvertedEvent(object sender, EventArgs e)
        {
            frmDatasetSelection fChildForm = new frmDatasetSelection();
            Form frmMain = this.MdiParent;
            int intSelectedDataset;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Jobs:
                    frmMain.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.i_str_dataset_types = "Job,"; // Comma seperated list needs to be passed //

                    if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
                    {
                        return;
                    }
                    intSelectedDataset = fChildForm.i_int_selected_dataset;
                    DS_Selection1.SelectRecordsFromDatasetInverted(intSelectedDataset, "JobID");
                    break;
                default:
                    break;
            }
        }

        public override void OnDatasetManagerEvent(object sender, EventArgs e)
        {
            var fChildForm = new frm_DatasetManager();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.FormPermissions = this.FormPermissions;
            fChildForm._i_PassedInFilterTypes = "Job,";
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();
            fChildForm.Show();

            MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
        }

        #endregion


        private void splitContainerControl1_SplitGroupPanelCollapsed(object sender, SplitGroupPanelCollapsedEventArgs e)
        {
            if (!e.Collapsed)
            {
                Application.DoEvents();  // Allow Form time to repaint itself //
                LoadLinkedRecords();
            }
        }

        private void barManager1_HighlightedLinkChanged(object sender, DevExpress.XtraBars.HighlightedLinkChangedEventArgs e)
        {
            // This event is required by the ToolTipControl object [it handles firing the Tooltips on BarSubItem objects on the toolbar because they don't show Tooltips] //
            toolTipController1.HideHint();
            if (e.Link == null) return;

            BarSubItemLink link = e.PrevLink as BarSubItemLink;
            if (link != null) link.CloseMenu();

            if (e.Link.Item is BarLargeButtonItem) return;

            var Info = new ToolTipControlInfo {Object = e.Link.Item, SuperTip = e.Link.Item.SuperTip};

            toolTipController1.ShowHint(Info);
        }


        private void toolTipController1_GetActiveObjectInfo(object sender, ToolTipControllerGetActiveObjectInfoEventArgs e)
        {
            try
            {
                switch (e.SelectedControl.Name)
                {
                    case "gridControlVisit":
                        {
                            GridView view = gridControlVisit.GetViewAt(e.ControlMousePosition) as GridView;
                            if (view == null) return;
                            GridHitInfo hi = view.CalcHitInfo(e.ControlMousePosition);
                            if (hi.InRowCell && hi.Column.Name == "Alert2")
                            {
                                if (view.GetRowCellValue(hi.RowHandle, "Timeliness").ToString() == "Overdue" || Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "LinkedJobCount")) <= 0 || Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "IssueFound")) != 0 || Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "Rework")) >= 1 || Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "AccidentOnSite")) >= 1)
                                {
                                    string strMessage = "The Following Warnings are present for this Visit:";
                                    if (view.GetRowCellValue(hi.RowHandle, "Timeliness").ToString() == "Overdue") strMessage += "\n--> Visit Overdue.";
                                    if (Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "LinkedJobCount")) <= 0) strMessage += "\n--> No Linked Jobs.";
                                    //if (Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "LinkedOutstandingJobCount")) > 0) strMessage += "\n--> Outstanding Jobs.";
                                    if (Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "IssueFound")) != 0) strMessage += "\n--> Issue(s) Found.";
                                    if (Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "Rework")) > 0) strMessage += "\n--> Rework Required.";
                                    if (Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "AccidentOnSite")) > 0) strMessage += "\n--> Accident On Site.";

                                    SuperToolTip st = new SuperToolTip();
                                    SuperToolTipSetupArgs args = new SuperToolTipSetupArgs(st);
                                    args.Title.Text = "Warning Icons - Information";
                                    args.Title.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
                                    args.Contents.Text = strMessage;
                                    args.ShowFooterSeparator = false;
                                    args.Footer.Text = "";
                                    st.Setup(args);
                                    ToolTipControlInfo info = new ToolTipControlInfo(new CellToolTipInfo(hi.RowHandle, hi.Column, "cell"), args.Contents.Text);
                                    info.SuperTip = st;
                                    info.ToolTipType = ToolTipType.SuperTip;
                                    e.Info = info;
                                    return;
                                }
                                else return;  // No Tooltip //
                            }
                            if (hi.InColumnPanel && hi.Column != null && hi.Column.Name == "Alert2")
                            {
                                SuperToolTip st = new SuperToolTip();
                                SuperToolTipSetupArgs args = new SuperToolTipSetupArgs(st);
                                args.Title.Text = "Warning Icons - Information";
                                args.Title.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
                                args.Contents.Text = "This column displays warning icons for Visits when certain conditions are met. Hover over the warning icon on visits to see the warning(s).";
                                args.ShowFooterSeparator = false;
                                args.Footer.Text = "";
                                st.Setup(args);
                                ToolTipControlInfo info = new ToolTipControlInfo(new CellToolTipInfo(hi.RowHandle, hi.Column, "cell"), args.Contents.Text);
                                info.SuperTip = st;
                                info.ToolTipType = ToolTipType.SuperTip;
                                e.Info = info;
                                return;
                            }
                        }
                        return;
                    case "gridControlJob":
                        {
                            GridView view = gridControlJob.GetViewAt(e.ControlMousePosition) as GridView;
                            if (view == null) return;
                            GridHitInfo hi = view.CalcHitInfo(e.ControlMousePosition);
                            if (hi.InRowCell && hi.Column.Name == "Alert")
                            {
                                if (Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "LinkedLabourCount")) <= 0 || Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "JobStatusID")) < 20 || Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "DoNotPayContractor")) == 1 || Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "DoNotInvoiceClient")) == 1)
                                {
                                    string strMessage = "The Following Warnings are present for this Job:";
                                    if (Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "DoNotPayContractor")) == 1) strMessage += "\n--> Do Not Pay Team.";
                                    if (Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "DoNotInvoiceClient")) == 1) strMessage += "\n--> Do Not Invoice Client.";
                                    if (Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "LinkedLabourCount")) <= 0) strMessage += "\n--> Unable to Send - At least one Labour Team must be linked.";
                                    if (Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "JobStatusID")) < 20) strMessage += "\n--> Unable to Send - Job Status must be >= Job Creation Completed - Ready To Send.";

                                    SuperToolTip st = new SuperToolTip();
                                    SuperToolTipSetupArgs args = new SuperToolTipSetupArgs(st);
                                    args.Title.Text = "Warning Icons - Information";
                                    args.Title.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
                                    args.Contents.Text = strMessage;
                                    args.ShowFooterSeparator = false;
                                    args.Footer.Text = "";
                                    st.Setup(args);
                                    ToolTipControlInfo info = new ToolTipControlInfo(new CellToolTipInfo(hi.RowHandle, hi.Column, "cell"), args.Contents.Text);
                                    info.SuperTip = st;
                                    info.ToolTipType = ToolTipType.SuperTip;
                                    e.Info = info;
                                    return;
                                }
                                else return;  // No Tooltip //
                            }
                            if (hi.InColumnPanel && hi.Column != null && hi.Column.Name == "Alert")
                            {
                                SuperToolTip st = new SuperToolTip();
                                SuperToolTipSetupArgs args = new SuperToolTipSetupArgs(st);
                                args.Title.Text = "Warning Icons - Information";
                                args.Title.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
                                args.Contents.Text = "This column displays warning icons for jobs when certain conditions are met. Hover over the warning icon on jobs to see the warning(s).";
                                args.ShowFooterSeparator = false;
                                args.Footer.Text = "";
                                st.Setup(args);
                                ToolTipControlInfo info = new ToolTipControlInfo(new CellToolTipInfo(hi.RowHandle, hi.Column, "cell"), args.Contents.Text);
                                info.SuperTip = st;
                                info.ToolTipType = ToolTipType.SuperTip;
                                e.Info = info;
                                return;
                            }
                            /*if (hi.HitTest == GridHitTest.GroupPanel)
                            {
                                info = new ToolTipControlInfo(hi.HitTest, "Group panel");
                                return;
                            }*/

                            /*if (hi.HitTest == GridHitTest.RowIndicator)
                            {
                                info = new ToolTipControlInfo(GridHitTest.RowIndicator.ToString() + hi.RowHandle.ToString(), "Row Handle: " + hi.RowHandle.ToString());
                                ToolTipTitleItem titleItem1 = new ToolTipTitleItem();
                                byte[] cellIm = view.GetRowCellValue(hi.RowHandle, "Alert") as byte[];
                                Image im = null;
                                if (cellIm != null)
                                {
                                    MemoryStream ms = new MemoryStream(cellIm);

                                    im = Image.FromStream(ms);
                                }

                                ToolTipItem item1 = new ToolTipItem();
                                item1.Image = im;
                                sTooltip1.Items.Add(item1);
                            }*/
                            //info = new ToolTipControlInfo(hi.HitTest, "");
                            //info.SuperTip = superToolTipSiteContractFilter;
                        }
                        return;
                    default:
                        break;
                }
            }
            catch (Exception) { }
        }

        private void hideContainerLeft_Click(object sender, EventArgs e)
        {

        }

        private void checkEditTeam_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                ItemForLabourFilter.CustomizationFormText = "Team:";
                ItemForLabourFilter.Text = "Team:";
                i_str_selected_Labour_Team_ids = "";
                i_str_selected_Labour_Team_descriptions = "";
                selection12.ClearSelection();
                gridControl12.BeginUpdate();
                sp06028_OM_Labour_FilterTableAdapter.Fill(dataSet_GC_Summer_Core.sp06028_OM_Labour_Filter, (checkEditTeam.Checked ? 1 : 0));
                gridControl12.EndUpdate();
                popupContainerEditLabourFilter.Text = PopupContainerEditLabourFilter_Get_Selected();
            }
        }

        private void checkEditStaff_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                ItemForLabourFilter.CustomizationFormText = "Staff:";
                ItemForLabourFilter.Text = "Staff:";
                i_str_selected_Labour_Team_ids = "";
                i_str_selected_Labour_Team_descriptions = "";
                selection12.ClearSelection();
                gridControl12.BeginUpdate();
                sp06028_OM_Labour_FilterTableAdapter.Fill(dataSet_GC_Summer_Core.sp06028_OM_Labour_Filter, (checkEditTeam.Checked ? 1 : 0));
                gridControl12.EndUpdate();
                popupContainerEditLabourFilter.Text = PopupContainerEditLabourFilter_Get_Selected();
            }
        }

        private void bciFilterTeamsSelected_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlLabour.MainView;
            if (bciFilterTeamsSelected.Checked)  // Filter Selected rows //
            {
                try
                {
                    int[] intRowHandles = view.GetSelectedRows();
                    int intCount = intRowHandles.Length;
                    DataRow dr = null;
                    if (intCount <= 0)
                    {
                        XtraMessageBox.Show("Select one or more Teams records to filter by before proceeding.", "Filter Selected Team Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    gridControlLabour.BeginUpdate();
                    foreach (int intRowHandle in intRowHandles)
                    {
                        dr = view.GetDataRow(intRowHandle);
                        if (dr != null) dr["Selected"] = 1;
                    }
                }
                catch (Exception) { }
                view.ActiveFilter.Clear();
                view.ActiveFilter.NonColumnFilter = "[Selected] = 1";
                gridControlLabour.EndUpdate();
            }
            else  // Clear Filter //
            {
                gridControlLabour.BeginUpdate();
                try
                {
                    view.ActiveFilter.Clear();
                    foreach (DataRow dr in dataSet_OM_Billing.sp06452_OM_Billing_Manager_Labour.Rows)
                    {
                        dr["Selected"] = 0;
                    }
                }
                catch (Exception) { }
            }
            gridControlLabour.EndUpdate();
        }

        private void bciFilterVisitsSelected_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlVisit.MainView;
            if (bciFilterVisitsSelected.Checked)  // Filter Selected rows //
            {
                try
                {
                    int[] intRowHandles = view.GetSelectedRows();
                    int intCount = intRowHandles.Length;
                    DataRow dr = null;
                    if (intCount <= 0)
                    {
                        XtraMessageBox.Show("Select one or more Visit records to filter by before proceeding.", "Filter Selected Visit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    gridControlVisit.BeginUpdate();
                    foreach (int intRowHandle in intRowHandles)
                    {
                        dr = view.GetDataRow(intRowHandle);
                        if (dr != null) dr["Selected"] = 1;
                    }
                }
                catch (Exception) { }
                view.ActiveFilter.Clear();
                view.ActiveFilter.NonColumnFilter = "[Selected] = 1";
                gridControlVisit.EndUpdate();
            }
            else  // Clear Filter //
            {
                gridControlVisit.BeginUpdate();
                try
                {
                    view.ActiveFilter.Clear();
                    foreach (DataRow dr in dataSet_OM_Billing.sp06445_OM_Team_Self_Billing_Manager_Visits.Rows)
                    {
                        dr["Selected"] = 0;
                    }
                }
                catch (Exception) { }
            }
            gridControlVisit.EndUpdate();
        }

        private void bciFilterJobsSelected_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlJob.MainView;
            if (bciFilterJobsSelected.Checked)  // Filter Selected rows //
            {
                try
                {
                    int[] intRowHandles = view.GetSelectedRows();
                    int intCount = intRowHandles.Length;
                    DataRow dr = null;
                    if (intCount <= 0)
                    {
                        XtraMessageBox.Show("Select one or more Job records to filter by before proceeding.", "Filter Selected Job Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    gridControlJob.BeginUpdate();
                    foreach (int intRowHandle in intRowHandles)
                    {
                        dr = view.GetDataRow(intRowHandle);
                        if (dr != null) dr["Selected"] = 1;
                    }
                }
                catch (Exception) { }
                view.ActiveFilter.Clear();
                view.ActiveFilter.NonColumnFilter = "[Selected] = 1";
                gridControlJob.EndUpdate();
            }
            else  // Clear Filter //
            {
                gridControlJob.BeginUpdate();
                try
                {
                    view.ActiveFilter.Clear();
                    foreach (DataRow dr in dataSet_OM_Billing.sp06454_OM_Self_Billing_Jobs_Linked_to_Visits.Rows)
                    {
                        dr["Selected"] = 0;
                    }
                }
                catch (Exception) { }
            }
            gridControlJob.EndUpdate();

        }

        private void spinEditSiteID_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "clear")
            {
                spinEditSiteID.EditValue = 0;
            }
        }

        private void bbiRecalculateTotal_ItemClick(object sender, ItemClickEventArgs e)
        {
            Recalculate_Visit_Costings();
        }

        private void bbiInvoice_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlVisit.MainView;
            view.PostEditor();
            view = (GridView)gridControlBillingRequirement.MainView;
            view.PostEditor();
            view = (GridView)gridControlBillingException.MainView;
            view.PostEditor();

            //Load_Data_Teams();  // Make sure totals of Requirements and Warning on each line is up-to-date //

            view = (GridView)gridControlLabour.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intFoundRow = 0;
            if (checkEditExcludeTeamsWithOutstandingRequirements.Checked || checkEditExcludeTeamsWithOutstandingWarnings.Checked)  // Need to de-select any rows that have requirements or warnings outstanding //
            {
                view.BeginUpdate();
                view.BeginSelection();
                foreach (int intRowHandle in intRowHandles)
                {
                    if ((checkEditExcludeTeamsWithOutstandingRequirements.Checked && Convert.ToInt32(view.GetRowCellValue(intRowHandle, "RequirementUnfulfilledCount")) > 0) || (checkEditExcludeTeamsWithOutstandingWarnings.Checked && Convert.ToInt32(view.GetRowCellValue(intRowHandle, "WarningUnauthorisedCount")) > 0))
                    {
                        view.UnselectRow(intRowHandle);
                    }
                }
                view.EndSelection();
                view.EndUpdate();
            }
            intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("No Teams are selected for Editing.\n\nNote: This process will de-select Teams with unfulfilled Requirement Counts or Warning Counts greater than 0 if this is not disabled in the Invoicing Parameters.", "Self-Bill Selected Teams", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // ***** SELECT VISITS ***** - Only select visits that don't have warnings - unless the exclude visits with warnings is un-ticked //
            DataTable dtVisitsToInvoice = new DataTable("Visits");
            dtVisitsToInvoice.Columns.Add("VisitID", typeof(int));
            dtVisitsToInvoice.Rows.Clear();

            GridView viewVisits = (GridView)gridControlVisit.MainView;
            viewVisits.PostEditor();
            if (!ReferenceEquals(viewVisits.ActiveFilter.Criteria, null)) viewVisits.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(viewVisits.FindFilterText)) viewVisits.ApplyFindFilter("");  // Clear any Find in place //  
            if (viewVisits.DataRowCount <= 0)
            {
                XtraMessageBox.Show("No visits to invoice - select one ore more teams with one or more visits before proceeding.", "Self-Bill Selected Teams", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            int intViewGridRowCount = viewVisits.DataRowCount;
            for (int i = 0; i < intViewGridRowCount; i++)
            {
                if ((!checkEditExcludeVisitWithRequirements.Checked || Convert.ToInt32(viewVisits.GetRowCellValue(i, "BillingRequirmentsUnfulfilled")) <= 0) && (!checkEditExcludeVisitsWithWarnings.Checked || Convert.ToInt32(viewVisits.GetRowCellValue(i, "BillingWarningUnauthorised")) <= 0))
                {
                    dtVisitsToInvoice.Rows.Add(Convert.ToInt32(viewVisits.GetRowCellValue(i, "VisitID")));
                }
            }
            dtVisitsToInvoice = dtVisitsToInvoice.DefaultView.ToTable(true, "VisitID");  // Remove any duplicate IDs //

            if (dtVisitsToInvoice.Rows.Count <= 0)
            {
                XtraMessageBox.Show("No visits to invoice - select one or more teams with one or more visits before proceeding.\n\nNote: If the Exclude Visits With Outstanding Requirements \\ Warnings tickboxes are ticked and these are present for the visits, they will be excluded by the process.", "Self-Bill Selected Teams", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }



            string strMessage = "You have " + (intCount == 1 ? "1 Team" : Convert.ToString(intRowHandles.Length) + " Teams") + " selected for Self-Billing Invoicing.\n\n<color=red>Proceed with creating the invoice(s) and update the data?</color>";
            if (XtraMessageBox.Show(strMessage, "Self-Billing Invoice", MessageBoxButtons.YesNo, MessageBoxIcon.Question, DefaultBoolean.True) == DialogResult.No) return;

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Updating Data...");

            int intExcludeVisitWithRequirements = (checkEditExcludeVisitWithRequirements.Checked ? 1 : 0);
            int intExcludeVisitsWithWarnings = (checkEditExcludeVisitsWithWarnings.Checked ? 1 : 0);

            StringBuilder sbTeams = new StringBuilder();
            StringBuilder sbStaff = new StringBuilder();
            StringBuilder sbSelfBillingInvoiceHeaderID = new StringBuilder();
            foreach (int intRowHandle in intRowHandles)
            {
                if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, view.Columns["LabourTypeID"])) == 1)
                {
                    sbTeams.Append(view.GetRowCellValue(intRowHandle, view.Columns["LabourID"]).ToString() + ",");
                }
                else
                {
                    sbStaff.Append(view.GetRowCellValue(intRowHandle, view.Columns["LabourID"]).ToString() + ",");
                }
            }
            string strSelectedTeamIDs = sbTeams.ToString();
            string strSelectedStaffIDs = sbStaff.ToString();
            int intWeekNumber = BaseObjects.DateFunctions.GetIso8601WeekOfYear(i_dtStart);
            int intYearNumber = Convert.ToInt32(i_dtStart.Year.ToString().Substring(i_dtStart.Year.ToString().Length - 2));
            
            string strSelfBillingNumber = (!string.IsNullOrWhiteSpace(textEditSBNumber.Text.ToString()) ? textEditSBNumber.Text.ToString() : "SB" + intWeekNumber.ToString().PadLeft(2, '0') + i_dtStart.ToString("yy"));
            
            try
            {
                using (SqlConnection conn = new SqlConnection(strConnectionString + "Connect Timeout=3600;"))  // Timeout on SQL Connection to 1 hour - need line further down too. //
                {
                    conn.Open();
                    SqlDataAdapter sdaData = new SqlDataAdapter();
                    DataSet dsData = new DataSet("NewDataSet");

                    SqlCommand cmd = new SqlCommand("sp06459_OM_Self_Billing_Create_Bills", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@TeamIDs", strSelectedTeamIDs));
                    cmd.Parameters.Add(new SqlParameter("@StaffIDs", strSelectedStaffIDs));
                    cmd.Parameters.Add(new SqlParameter("@ExcludeVisitWithRequirements", intExcludeVisitWithRequirements));
                    cmd.Parameters.Add(new SqlParameter("@ExcludeVisitsWithWarnings", intExcludeVisitsWithWarnings));
                    cmd.Parameters.Add(new SqlParameter("@WeekNumber", intWeekNumber));
                    cmd.Parameters.Add(new SqlParameter("@YearNumber", intYearNumber));
                    cmd.Parameters.Add(new SqlParameter("@SBNumber", strSelfBillingNumber));
                    //cmd.Parameters.Add(new SqlParameter("@PassedInVisitIDsTable", dsVisitsToWorkWith.Tables[0]));             
                    cmd.Parameters.Add(new SqlParameter("@PassedInVisitIDsTable", dtVisitsToInvoice));
                    cmd.CommandTimeout = 3600; // Timeout on SQL Connection to 1 hour - need line further up too. //

                    sdaData = new SqlDataAdapter(cmd);
                    sdaData.Fill(dsData, "Table");

                    if (dsData.Tables[0].Rows.Count > 0)
                    {
                        splashScreenManager1.SetWaitFormDescription("Creating Self-Billing Invoice Files...");

                        // Create a folder for the invoices if it doesn't exist //
                        string strSubFolderName = DateTime.Today.ToString("yyyy-MM-dd") + "_" + strSelfBillingNumber;
                        string strFolderPath = Path.Combine(i_str_SavedSelfBillingInvoiceFolder, strSubFolderName);
                        if (!Directory.Exists(strFolderPath)) Directory.CreateDirectory(strFolderPath);

                        int intPDFCreationErrorCount = 0;
                        int intPDFSendingErrorCount = 0;
                        int intPDFCreatedCount = 0;
                        int intPDFSendCount = 0;
                        string strReturn1 = "";
                        string strReturn2 = "";
                        string strFileName1 = "";
                        string strFileName2 = "";
                        string strSelfBillingInvoiceNumber = "";
                        string strPDFPassword = "";
                        string strTeamEmail = "";
                        string strContractManagerEmail = "";
                        string strLabourName = "";
                        DateTime dtTodayWithTime = DateTime.Now;
                        DataRow drEmail = Get_Email_Settings();
                        DataRow drEmail2 = Get_Email_Settings2();
                        foreach (DataRow dr in dsData.Tables[0].Rows)
                        {
                            strLabourName = dr["LabourName"].ToString();
                            int intSelfBillingInvoiceHeaderID = Convert.ToInt32(dr["SelfBillingInvoiceHeaderID"]);
                            sbSelfBillingInvoiceHeaderID.Append(intSelfBillingInvoiceHeaderID.ToString() + ",");

                            strSelfBillingInvoiceNumber = dr["SelfBillingInvoiceNumber"].ToString();
                            strPDFPassword = dr["EmailPassword"].ToString();
                            //strFileName1 = strSelfBillingInvoiceNumber + "_" + intSelfBillingInvoiceHeaderID.ToString().PadLeft(7, '0') + "_" + dtTodayWithTime.ToString("yyyy-MM-dd_HH_mm_ss") + ".PDF";
                            //strFileName2 = "CM_" + strSelfBillingInvoiceNumber + "_" + intSelfBillingInvoiceHeaderID.ToString().PadLeft(7, '0') + "_" + dtTodayWithTime.ToString("yyyy-MM-dd_HH_mm_ss") + ".PDF";
                            strFileName1 = strSubFolderName + "\\" + dr["LabourName"].ToString() +"_"+ dr["LabourID"].ToString() + "_" + intSelfBillingInvoiceHeaderID.ToString().PadLeft(7, '0') + ".PDF";
                            strFileName2 = strSubFolderName + "\\" + "CM_" + dr["LabourName"].ToString() + "_" + dr["LabourID"].ToString() + "_" + intSelfBillingInvoiceHeaderID.ToString().PadLeft(7, '0') + ".PDF";

                            strReturn1 = Create_Physical_PDF_Invoice(intSelfBillingInvoiceHeaderID, strFileName1, strPDFPassword);  // Create physical Self-Billing Invoice PDF file //
                            strReturn2 = Create_Physical_PDF_CM_Report(intSelfBillingInvoiceHeaderID, strFileName2, i_dtEnd);  // Create physical CM Self-Billing Report PDF file //

                            if (!string.IsNullOrWhiteSpace(strReturn1) || !string.IsNullOrWhiteSpace(strReturn2))  // Creation of a file failed so don't send //
                            {
                                intPDFCreationErrorCount++;
                            }
                            else
                            {
                                intPDFCreatedCount++;
                                Update_Invoice_Header(intSelfBillingInvoiceHeaderID, strFileName1, strFileName2, dtTodayWithTime, null, null);  // Update the DB with the filenames and date created //

                                // Check if we should auto-send the invoice - if yes, send it //
                                if (checkEditAutoSendCreatedInvoices.Checked)
                                {
                                    strTeamEmail = dr["Email"].ToString();
                                    strContractManagerEmail = dr["ContractManagerEmail"].ToString();

                                    strReturn1 = Send_Physical_PDF_Invoice(intSelfBillingInvoiceHeaderID, strFileName1, strTeamEmail, strContractManagerEmail, strSelfBillingInvoiceNumber, drEmail, strLabourName);
                                    strReturn2 = Send_Physical_PDF_CM_Report(intSelfBillingInvoiceHeaderID, strFileName2, strContractManagerEmail, strSelfBillingInvoiceNumber, drEmail2, strLabourName);
                                    if (!string.IsNullOrWhiteSpace(strReturn1) || !string.IsNullOrWhiteSpace(strReturn2))  // Sending of file failed //
                                    {
                                        intPDFCreationErrorCount++;
                                        if (string.IsNullOrWhiteSpace(strReturn1)) Update_Invoice_Header(intSelfBillingInvoiceHeaderID, null, null, null, dtTodayWithTime, null);  // Invoice file sent successfully //
                                        if (string.IsNullOrWhiteSpace(strReturn2)) Update_Invoice_Header(intSelfBillingInvoiceHeaderID, null, null, null, null, dtTodayWithTime);  // CM report sent successfully //
                                    }
                                    else
                                    {
                                        intPDFSendCount++;
                                        Update_Invoice_Header(intSelfBillingInvoiceHeaderID, null, null, null, dtTodayWithTime, dtTodayWithTime);  // Update the DB with the last sent date - both files sent successfully //
                                    }
                                }
                            }
                        }

                        if (splashScreenManager1.IsSplashFormVisible)
                        {
                            splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                            splashScreenManager1.CloseWaitForm();
                        }

                        strMessage = "Self-Billing Process completed.\n\n" +
                                    "--> PDF Files Created: " + intPDFCreatedCount.ToString() + "\n" +
                                    (intPDFCreationErrorCount > 0 ? "<color=red>--> PDF Files Not Created: " + intPDFCreationErrorCount.ToString() + "</color>\n" : "") +
                                    "--> PDF Files Sent: " + intPDFSendCount.ToString() + "\n" +
                                    (intPDFSendingErrorCount > 0 ? "<color=red>--> PDF Files Not Sent: " + intPDFSendingErrorCount.ToString() + "</color>\n" : "");
                        if (intPDFCreationErrorCount > 0) strMessage += "\nErrors occurred when creating one or more of the PDF invoice files. You will need to select these records from the historical grid and click the Re-Create PDF button then the Send button to create and send these invoices.";
                        if (intPDFSendingErrorCount > 0) strMessage += "\nErrors occurred when sending one or more of the PDF invoice files. You will need to select these records from the historical grid and click the Send button.";
                        XtraMessageBox.Show(strMessage, "Create Team Self-Billing Invoices", MessageBoxButtons.OK, MessageBoxIcon.Information, DefaultBoolean.True);
                    }
                    else
                    {
                        XtraMessageBox.Show("No Self-Billing Invoices created - adjust any filters or change the teams selected for self-billing invoicing.", "Create Team Self-Billing Invoices", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    conn.Close();
                }

                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
            }
            catch (Exception ex)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }

                XtraMessageBox.Show("An error occurred while creating the Team Self-Billing Invoices.\n\nError: " + ex.Message + "\n\nNo Team Self-Billing Invoices Created. Please try again. If the problem persists contact Technical Support.", "Create Team Self-Billing Invoices", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            Load_Data(0);  // Reload data on checking page regardless if whole process succeeded as some invoices may have been generated - 0 passed as paramater to suppress creation of warnings as they have already been created. //
            Refresh_Updated_Team2_Rows(sbTeams.ToString(), sbStaff.ToString());  // Get updated rows and merge back in //
            Refresh_Updated_Invoice_Rows(sbSelfBillingInvoiceHeaderID.ToString());  // Get added rows and merge back in //

            Set_Historical_Date_Range(DateTime.Today, DateTime.Today.AddDays(1).AddMilliseconds(-5));  // Adjust DateTimes for viewing created invoices //
            xtraTabControl1.SelectedTabPage = xtraTabPageHistoricalInvoices;

            // Update the SelfBillingInvoiceNumbers filter panel grid - reload in case this if the first run of a new week to add the new number into the list then select it //
            selection3.ClearSelection();
            sp06477_OM_Get_Unique_Self_Billing_Invoice_ListTableAdapter.Fill(dataSet_OM_Billing.sp06477_OM_Get_Unique_Self_Billing_Invoice_List, null, null);
            GridView gvInvoiceNumbers = (GridView)gridControl3.MainView;
            intFoundRow = gvInvoiceNumbers.LocateByValue(0, gvInvoiceNumbers.Columns["SelfBillingInvoiceNumber"], strSelfBillingNumber);
            if (intFoundRow != GridControl.InvalidRowHandle)
            {
                gvInvoiceNumbers.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                gvInvoiceNumbers.MakeRowVisible(intFoundRow, false);
            }
            popupContainerEditInvoiceNumber.Text = popupContainerEditInvoiceNumber_Get_Selected();  //  should be none as line above will have deselected everything //
            
            Load_Data_Teams2();
           
            // Attempt to select teams \ staff who have just had new invoices created for them //
            view = (GridView)gridControlLabour2.MainView;
            view.BeginSelection();
            view.ClearSelection();
            if (!string.IsNullOrWhiteSpace(strSelectedTeamIDs))
            {
                Array arrayItems = strSelectedTeamIDs.Split(',');  // Single quotes because char expected for delimeter //
                DevExpress.XtraGrid.Columns.GridColumn[] cols = new DevExpress.XtraGrid.Columns.GridColumn[] { gridColumnLabourTypeID, gridColumnLabourID };  // Array of columns to search for a match on //
                ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
                foreach (string strElement in arrayItems)
                {
                    if (strElement == "") break;
                    object[] values = new object[] { 1, Convert.ToInt32(strElement) };  // Array of objects [values] to look for //
                    intFoundRow = extGridViewFunction.LocateRowByMultipleValues(view, cols, values, 0);  // Function declared in BaseObjects... Classes... ExtendedGridViewFunctions //
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intFoundRow);
                        view.MakeRowVisible(intFoundRow, false);
                    }
                }
            }
            if (!string.IsNullOrWhiteSpace(strSelectedStaffIDs))
            {
                Array arrayItems = strSelectedStaffIDs.Split(',');  // Single quotes because char expected for delimeter //
                DevExpress.XtraGrid.Columns.GridColumn[] cols = new DevExpress.XtraGrid.Columns.GridColumn[] { gridColumnLabourTypeID, gridColumnLabourID };  // Array of columns to search for a match on //
                ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
                foreach (string strElement in arrayItems)
                {
                    if (strElement == "") break;
                    object[] values = new object[] { 0, Convert.ToInt32(strElement) };  // Array of objects [values] to look for //
                    intFoundRow = extGridViewFunction.LocateRowByMultipleValues(view, cols, values, 0);  // Function declared in BaseObjects... Classes... ExtendedGridViewFunctions //
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intFoundRow);
                        view.MakeRowVisible(intFoundRow, false);
                    }
                }

            }
            view.EndSelection();
        }

        #endregion


        #region Historical Page

        private void Load_Data_Teams2()
        {
            if (!splashScreenManager.IsSplashFormVisible)
            {
                this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Loading Invoiced Labour...");
            }

            RefreshGridViewStateLabour2.SaveViewInfo();
            GridView view = (GridView)gridControlLabour2.MainView;
            view.BeginUpdate();
            try
            {

                sp06463_OM_Billing_Manager_Invoiced_LabourTableAdapter.Fill(this.dataSet_OM_Billing.sp06463_OM_Billing_Manager_Invoiced_Labour, "", "", i_dtHistoricalStart, i_dtHistoricalEnd, i_str_selected_invoice_numbers.Replace(" ", ""));
                if (boolFirstLoadOfHistoricalTeamsGrid)
                {
                    view.ExpandAllGroups();
                    boolFirstLoadOfHistoricalTeamsGrid = false;
                }
                else
                {
                    this.RefreshGridViewStateLabour2.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
            }
            catch (SqlException ex)
            {
                if (ex.Number == -2)  // SQL Server Time-Out //
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("The screen has not been able to return your data due to the volume of records in your request. Please filter your request by using the data filter or contact Technical Support for further help.", "Load Invoiced Labour", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Loading Invoiced Labour Data.\n\nMessage = [" + ex.Message + "].\n\nIf the problem persists, contact Technical Support.", "Load Invoiced Labour", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Loading Invoiced Labour Data.\n\nMessage = [" + ex.Message + "].\n\nIf the problem persists, contact Technical Support.", "Load Invoiced Labour", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            view.EndUpdate();

            if (!splashScreenManager.IsSplashFormVisible)
            {
                this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Loading Invoiced Labour...");
            }

            if (splashScreenManager.IsSplashFormVisible)
            {
                splashScreenManager.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager.CloseWaitForm();
            }
        }
        private void Refresh_Updated_Team2_Rows(string strTeamIDs, string strStaffIDs)
        {
            try
            {
                SqlDataAdapter sdaData = new SqlDataAdapter();
                DataSet dsData = new DataSet("NewDataSet");

                using (var conn = new SqlConnection(strConnectionStringREADONLY))
                {
                    conn.Open();
                    using (var cmd = new SqlCommand("sp06463_OM_Billing_Manager_Invoiced_Labour", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@DrillDownIDTeams", strTeamIDs));
                        cmd.Parameters.Add(new SqlParameter("@DrillDownIDStaff", strStaffIDs));
                        cmd.Parameters.Add(new SqlParameter("@StartDate", i_dtHistoricalStart));
                        cmd.Parameters.Add(new SqlParameter("@EndDate", i_dtHistoricalEnd));
                        cmd.Parameters.Add(new SqlParameter("@SelfBillingInvoiceNumbers", i_str_selected_invoice_numbers.Replace(" ", "")));
                        sdaData = new SqlDataAdapter(cmd);
                        sdaData.Fill(dsData, "Table");
                    }
                    conn.Close();
                }
                if (dsData.Tables[0].Rows.Count > 0) dataSet_OM_Billing.sp06463_OM_Billing_Manager_Invoiced_Labour.Merge(dsData.Tables[0]);
            }
            catch (Exception ex) { }
        }

        private void Load_Data_Invoices()
        {
            GridView view = (GridView)gridControlLabour2.MainView;
            if (!splashScreenManager.IsSplashFormVisible)
            {
                this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Loading Invoices...");
            }

            view = (GridView)gridControlLabour2.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;

            StringBuilder sbTeams = new StringBuilder();
            StringBuilder sbStaff = new StringBuilder();
            foreach (int intRowHandle in intRowHandles)
            {
                if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, view.Columns["LabourTypeID"])) == 1)
                {
                    sbTeams.Append(view.GetRowCellValue(intRowHandle, view.Columns["LabourID"]).ToString() + ",");
                }
                else
                {
                    sbStaff.Append(view.GetRowCellValue(intRowHandle, view.Columns["LabourID"]).ToString() + ",");
                }
            }
            string strSelectedTeamIDs = sbTeams.ToString();
            string strSelectedStaffIDs = sbStaff.ToString();

            view = (GridView)gridControlInvoice.MainView;

            if (string.IsNullOrWhiteSpace(strSelectedTeamIDs) && string.IsNullOrWhiteSpace(strSelectedStaffIDs))
            {
                view.BeginUpdate();
                dataSet_OM_Billing.sp06464_OM_Billing_Manager_Invoices_For_Labour.Clear();
                view.EndUpdate();
            }
            else
            {
                RefreshGridViewStateInvoice.SaveViewInfo();
                view.BeginUpdate();
                try
                {
                    sp06464_OM_Billing_Manager_Invoices_For_LabourTableAdapter.Fill(this.dataSet_OM_Billing.sp06464_OM_Billing_Manager_Invoices_For_Labour, "", strSelectedTeamIDs, strSelectedStaffIDs, i_dtHistoricalStart, i_dtHistoricalEnd, i_str_selected_invoice_numbers.Replace(" ", ""));
                    this.RefreshGridViewStateInvoice.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                catch (SqlException ex)
                {
                    if (ex.Number == -2)  // SQL Server Time-Out //
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("The screen has not been able to return your data due to the volume of records in your request. Please filter your request by using the data filter or contact Technical Support for further help.", "Load Invoices", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Loading Invoices Data.\n\nMessage = [" + ex.Message + "].\n\nIf the problem persists, contact Technical Support.", "Load Invoices", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Loading Invoices Data.\n\nMessage = [" + ex.Message + "].\n\nIf the problem persists, contact Technical Support.", "Load Invoices", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                view.EndUpdate();
            }

            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }
        private void Refresh_Updated_Invoice_Rows(string strSelfBillingInvoiceHeaderID)
        {
            string strEmpty = "";
            try
            {
                SqlDataAdapter sdaData = new SqlDataAdapter();
                DataSet dsData = new DataSet("NewDataSet");
                using (var conn = new SqlConnection(strConnectionStringREADONLY))
                {
                    conn.Open();
                    using (var cmd = new SqlCommand("sp06464_OM_Billing_Manager_Invoices_For_Labour", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@DrillDownIDs", strSelfBillingInvoiceHeaderID));
                        cmd.Parameters.Add(new SqlParameter("@TeamIDs", strEmpty));
                        cmd.Parameters.Add(new SqlParameter("@StaffIDs", strEmpty));
                        cmd.Parameters.Add(new SqlParameter("@StartDate", i_dtHistoricalStart));
                        cmd.Parameters.Add(new SqlParameter("@EndDate", i_dtHistoricalEnd));
                        cmd.Parameters.Add(new SqlParameter("@SelfBillingInvoiceNumbers", i_str_selected_invoice_numbers.Replace(" ", "")));
                        sdaData = new SqlDataAdapter(cmd);
                        sdaData.Fill(dsData, "Table");
                    }
                    conn.Close();
                }
                if (dsData.Tables[0].Rows.Count > 0) dataSet_OM_Billing.sp06464_OM_Billing_Manager_Invoices_For_Labour.Merge(dsData.Tables[0]);
            }
            catch (Exception ex) { }
        }

        private void bbiRefresh2_ItemClick(object sender, ItemClickEventArgs e)
        {
            Load_Data_Teams2();
        }

        private void bciFilterTeamsSelected2_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlLabour2.MainView;
            if (bciFilterTeamsSelected2.Checked)  // Filter Selected rows //
            {
                try
                {
                    int[] intRowHandles = view.GetSelectedRows();
                    int intCount = intRowHandles.Length;
                    DataRow dr = null;
                    if (intCount <= 0)
                    {
                        XtraMessageBox.Show("Select one or more Teams records to filter by before proceeding.", "Filter Selected Team Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    gridControlLabour2.BeginUpdate();
                    foreach (int intRowHandle in intRowHandles)
                    {
                        dr = view.GetDataRow(intRowHandle);
                        if (dr != null) dr["Selected"] = 1;
                    }
                }
                catch (Exception) { }
                view.ActiveFilter.Clear();
                view.ActiveFilter.NonColumnFilter = "[Selected] = 1";
                gridControlLabour2.EndUpdate();
            }
            else  // Clear Filter //
            {
                gridControlLabour2.BeginUpdate();
                try
                {
                    view.ActiveFilter.Clear();
                    foreach (DataRow dr in dataSet_OM_Billing.sp06463_OM_Billing_Manager_Invoiced_Labour.Rows)
                    {
                        dr["Selected"] = 0;
                    }
                }
                catch (Exception) { }
            }
            gridControlLabour2.EndUpdate();
        }

        private void bciFilterInvoicesSelected_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlInvoice.MainView;
            if (bciFilterInvoicesSelected.Checked)  // Filter Selected rows //
            {
                try
                {
                    int[] intRowHandles = view.GetSelectedRows();
                    int intCount = intRowHandles.Length;
                    DataRow dr = null;
                    if (intCount <= 0)
                    {
                        XtraMessageBox.Show("Select one or more Invoice records to filter by before proceeding.", "Filter Selected Invoice Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    gridControlInvoice.BeginUpdate();
                    foreach (int intRowHandle in intRowHandles)
                    {
                        dr = view.GetDataRow(intRowHandle);
                        if (dr != null) dr["Selected"] = 1;
                    }
                }
                catch (Exception) { }
                view.ActiveFilter.Clear();
                view.ActiveFilter.NonColumnFilter = "[Selected] = 1";
                gridControlInvoice.EndUpdate();
            }
            else  // Clear Filter //
            {
                gridControlInvoice.BeginUpdate();
                try
                {
                    view.ActiveFilter.Clear();
                    foreach (DataRow dr in dataSet_OM_Billing.sp06464_OM_Billing_Manager_Invoices_For_Labour.Rows)
                    {
                        dr["Selected"] = 0;
                    }
                }
                catch (Exception) { }
            }
            gridControlInvoice.EndUpdate();
        }

        private void Set_Historical_Date_Range(DateTime StartDate, DateTime EndDate)
        {
            i_dtHistoricalStart = StartDate;
            i_dtHistoricalEnd = EndDate;
            dateEditHistoricalFromDate.DateTime = i_dtHistoricalStart;
            dateEditHistoricalToDate.DateTime = i_dtHistoricalEnd;
            popupContainerEditHistoricalDateRange.EditValue = PopupContainerEditHistoricalDateRange_Get_Selected();
        }


        #region GridView - Labour2

        private void gridControlLabour2_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("reload".Equals(e.Button.Tag))
                    {
                        Load_Data_Teams2();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewLabour2_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.Labour2;
            SetMenuStatus();
        }

        private void gridViewLabour2_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Labour2;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                int[] intRowHandles = view.GetSelectedRows();
                int intCount = intRowHandles.Length;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView - Invoice

        private void gridControlInvoice_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {

        }

        private void gridViewInvoice_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            try
            {
                GridView view = (GridView)sender;
                switch (e.Column.FieldName)
                {
                    case "InvoicePDF":
                        if (string.IsNullOrWhiteSpace(view.GetRowCellValue(e.RowHandle, "InvoicePDF").ToString())) e.RepositoryItem = emptyEditor;
                        break;
                    case "CMReportPDF":
                        if (string.IsNullOrWhiteSpace(view.GetRowCellValue(e.RowHandle, "CMReportPDF").ToString())) e.RepositoryItem = emptyEditor;
                        break;
                    case "LinkedVisitCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedVisitCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception) { }
        }

        private void gridViewInvoice_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.Invoice;
            SetMenuStatus();
        }

        private void gridViewInvoice_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Invoice;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                int[] intRowHandles = view.GetSelectedRows();
                int intCount = intRowHandles.Length;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewInvoice_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "InvoicePDF":
                    if (string.IsNullOrWhiteSpace(view.GetFocusedRowCellValue("InvoicePDF").ToString())) e.Cancel = true;
                    break;
                case "CMReportPDF":
                    if (string.IsNullOrWhiteSpace(view.GetFocusedRowCellValue("CMReportPDF").ToString())) e.Cancel = true;
                    break;
                case "LinkedVisitCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedVisitCount")) <= 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        
        private void repositoryItemHyperLinkEditInvoicePDF_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "InvoicePDF").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Team Self-Billing Invoice Linked - unable to proceed. Click the Re-Create PDF button.", "View Team Self-Billing Invoice", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                if (!BaseObjects.Keyboard.IsKeyDown(Keys.A))  // Only open in DevExpress PDF viewer if user is not holding down A key (CTRL Key doesn't work when fired from a grid as it's used for row selection) //
                {
                    frm_Core_PDF_Viewer fChildForm = new frm_Core_PDF_Viewer();
                    fChildForm.strPDFFile = Path.Combine(i_str_SavedSelfBillingInvoiceFolder + strFile);
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.Show();
                }
                else
                {
                    System.Diagnostics.Process.Start(Path.Combine(i_str_SavedSelfBillingInvoiceFolder + strFile));
                }
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view the Team Self-Billing Invoice: " + strFile + ".\n\nThe File may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Team Self-Billing Invoice", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void repositoryItemHyperLinkEditCMReportPDF_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "CMReportPDF").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No CM Invoice Report Linked - unable to proceed. Click the Re-Create PDF button.", "View CM Invoice Report", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                if (!BaseObjects.Keyboard.IsKeyDown(Keys.A))  // Only open in DevExpress PDF viewer if user is not holding down A key (CTRL Key doesn't work when fired from a grid as it's used for row selection) //
                {
                    frm_Core_PDF_Viewer fChildForm = new frm_Core_PDF_Viewer();
                    fChildForm.strPDFFile = Path.Combine(i_str_SavedSelfBillingInvoiceFolder + strFile);
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.Show();
                }
                else
                {
                    System.Diagnostics.Process.Start(Path.Combine(i_str_SavedSelfBillingInvoiceFolder + strFile));
                }
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view the CM Invoice Report: " + strFile + ".\n\nThe File may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View CM Invoice Report", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void repositoryItemHyperLinkEditVisitCount_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedVisitCount":
                    {
                        string strRecordIDs = ""; 
                        try
                        {
                            int intSelfBillingInvoiceHeaderID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "SelfBillingInvoiceHeaderID"));
                            if (intSelfBillingInvoiceHeaderID <= 0) return;

                            using (var GetValue = new DataSet_OM_BillingTableAdapters.QueriesTableAdapter())
                            {
                                GetValue.ChangeConnectionString(strConnectionString);
                                strRecordIDs = GetValue.sp06467_OM_Get_Visits_From_SelfBillingInvoiceHeaderIDs(intSelfBillingInvoiceHeaderID.ToString() + ",").ToString();
                            }
                        }
                        catch (Exception ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view visits linked to this self-billing invoice [" + ex.Message + "].\n\nPlease try again. If the problem persists contact Technical Support.", "View Linked Visits", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                        Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "operations_visit");
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region Historical Date Range Panel

        private void btnHistoricalDateRange_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void popupContainerEditHistoricalDateRange_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditHistoricalDateRange_Get_Selected();
        }
        
        private string PopupContainerEditHistoricalDateRange_Get_Selected()
        {
            string strValue = "";
            if (dateEditHistoricalFromDate.DateTime == DateTime.MinValue)
            {
                i_dtHistoricalStart = new DateTime(1900, 1, 1);
            }
            else
            {
                i_dtHistoricalStart = dateEditHistoricalFromDate.DateTime;
            }
            if (dateEditHistoricalToDate.DateTime == DateTime.MinValue)
            {
                i_dtHistoricalEnd = new DateTime(1900, 1, 1);
            }
            else
            {
                i_dtHistoricalEnd = dateEditHistoricalToDate.DateTime;
            }
            if (dateEditHistoricalFromDate.DateTime == DateTime.MinValue && dateEditHistoricalToDate.DateTime == DateTime.MinValue)
            {
                strValue = "No Date Range Filter";
            }
            else
            {
                strValue = (i_dtHistoricalStart == new DateTime(1900, 1, 1) ? "No Start" : i_dtHistoricalStart.ToString("dd/MM/yyyy HH:mm")) + " - " + (i_dtHistoricalEnd == new DateTime(1900, 1, 1) ? "No End" : i_dtHistoricalEnd.ToString("dd/MM/yyyy HH:mm"));
            }
            return strValue;
        }
        
        private void btnLoadHistorical_Click(object sender, EventArgs e)
        {
            Load_Data_Teams2();
        }

        #endregion


        #region Invoice Number Filter Panel

        private void btnInvoiceNumber_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void popupContainerEditInvoiceNumber_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                i_str_selected_invoice_numbers = "";
                selection3.ClearSelection();
                popupContainerEditInvoiceNumber.Text = popupContainerEditInvoiceNumber_Get_Selected();
            }
        }

        private void popupContainerEditInvoiceNumber_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = popupContainerEditInvoiceNumber_Get_Selected();
        }

        private string popupContainerEditInvoiceNumber_Get_Selected()
        {
            i_str_selected_invoice_numbers = "";
            GridView view = (GridView)gridControl3.MainView;
            if (view.DataRowCount <= 0 || selection3.SelectedCount <= 0)
            {
                return "No Invoice Number Filter";
            }
            else
            {
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_invoice_numbers += ", " + Convert.ToString(view.GetRowCellValue(i, "SelfBillingInvoiceNumber"));
                    }
                    if (i_str_selected_invoice_numbers.StartsWith(", ")) i_str_selected_invoice_numbers = i_str_selected_invoice_numbers.Substring(2, i_str_selected_invoice_numbers.Length - 2);
                }
            }
            return i_str_selected_invoice_numbers;
        }

        #endregion


        private void Set_Selected_Count3()
        {
            GridView view = (GridView)gridControlLabour2.MainView;
            int intSelectedCount = view.GetSelectedRows().Length;
            view = (GridView)gridControlInvoice.MainView;
            int intRowCount = view.DataRowCount;
            int intSelectedCount2 = view.GetSelectedRows().Length;
            if (intSelectedCount == 0 && intSelectedCount2 == 0)
            {
                bsiSelectedCount3.Visibility = BarItemVisibility.Never;
            }
            else
            {
                bsiSelectedCount3.Visibility = BarItemVisibility.Always;
                string strText = "";
                if (intSelectedCount > 0)
                {
                    strText = (intSelectedCount == 1 ? "1 Team Selected" : "<color=red>" + intSelectedCount.ToString() + " Teams</Color> Selected");
                }
                if (intSelectedCount > 0 && intSelectedCount2 > 0) strText += "<br>";
                if (intSelectedCount2 > 0)
                {
                    strText += (intSelectedCount2 == 1 ? "1 Invoice Selected" : "<color=red>" + intSelectedCount2.ToString() + " Invoices</Color> Selected");
                }
                bsiSelectedCount3.Caption = strText;
            }
            bsiRecreatePDF.Enabled = intRowCount > 0;
            bbiSelectInvoicesMissingPDFs.Enabled = intRowCount > 0;
            bbiRecreatePDF.Enabled = intSelectedCount2 > 0;

            bsiSendToTeam.Enabled = intRowCount > 0;
            bbiSelectUnsentInvoices.Enabled = intRowCount > 0;
            bbiResend.Enabled = intSelectedCount2 > 0;
            bbiResendCM.Enabled = intSelectedCount2 > 0;
            bbiDeleteInvoice.Enabled = (iBool_AllowDeleteInvoice && intSelectedCount2 > 0);
        }

        private void bbiSelectInvoicesMissingPDFs_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlInvoice.MainView;
            int intRowCount = view.DataRowCount;
            view.BeginSelection();
            view.ClearSelection();
            view.EndSelection();

            view.BeginSelection();
            for (int i = 0; i < intRowCount; i++)
            {
                if (string.IsNullOrWhiteSpace(view.GetRowCellValue(i, "InvoicePDF").ToString()) || string.IsNullOrWhiteSpace(view.GetRowCellValue(i, "CMReportPDF").ToString())) view.SelectRow(i);
            }
            view.EndSelection();
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                XtraMessageBox.Show("No Invoices are missing their PDF File.", "Select Invoices Missing PDF Files", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else
            {
                XtraMessageBox.Show(intRowHandles.Length + " Invoices(s) selected. Click the Re-create PDF button to create the missing file(s).", "Select Invoices Missing PDF Files", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return;
        }
        private void bbiRecreatePDF_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlInvoice.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                XtraMessageBox.Show("Select at least one Team Self-Billing Invoice to create before proceeding.", "Re-create Team Self-Billing Invoice PDF Files", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strMessage = "You have " + (intRowHandles.Length == 1 ? "1 Team Self-Billing Invoice" : Convert.ToString(intRowHandles.Length) + " Team Self-Billing Invoices") + " selected for Re-creating.\n\n<color=red>Proceed?</color>";
            if (XtraMessageBox.Show(strMessage, "Re-create Self-Billing Invoice", MessageBoxButtons.YesNo, MessageBoxIcon.Question, DefaultBoolean.True) == DialogResult.No) return;

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Creating Self-Billing Invoice Files...");

            StringBuilder sbTeams = new StringBuilder();
            StringBuilder sbStaff = new StringBuilder();
            StringBuilder sbSelfBillingInvoiceHeaderID = new StringBuilder();

            try
            {
                string strSubFolderName = "";
                string strFolderPath = "";

                int intPDFCreationErrorCount = 0;
                int intPDFCreatedCount = 0;
                string strReturn1 = "";
                string strReturn2 = "";
                string strOldFileName1 = "";
                string strOldFileName2 = "";
                string strFileName1 = "";
                string strFileName2 = "";
                string strSelfBillingInvoiceNumber = "";
                string strPDFPassword = "";
                DateTime dtTodayWithTime = DateTime.Now;
                DateTime dtSecondReportEndDate = DateTime.MinValue;
                foreach (int intRowHandle in intRowHandles)
                {
                    dtSecondReportEndDate = Convert.ToDateTime(view.GetRowCellValue(intRowHandle, "DateRaised"));
                    
                    // Attempt to remove any existing saved Invoice PDF file //
                    strOldFileName1 = view.GetRowCellDisplayText(intRowHandle, "InvoicePDF").ToString();
                    if (!string.IsNullOrWhiteSpace(strOldFileName1))
                    {
                        try
                        {
                            strOldFileName1 = Path.Combine(i_str_SavedSelfBillingInvoiceFolder + strOldFileName1);
                            System.IO.File.Delete(strOldFileName1);
                        }
                        catch (Exception)
                        {
                        }
                    }
                    // Attempt to remove any existing saved CM Report PDF file //
                    strOldFileName2 = view.GetRowCellDisplayText(intRowHandle, "CMReportPDF").ToString();
                    if (!string.IsNullOrWhiteSpace(strOldFileName2))
                    {
                        try
                        {
                            strOldFileName2 = Path.Combine(i_str_SavedSelfBillingInvoiceFolder + strOldFileName2);
                            System.IO.File.Delete(strOldFileName2);
                        }
                        catch (Exception)
                        {
                        }
                    }
                    if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, view.Columns["LinkedToLabourTypeID"])) == 1)
                    {
                        sbTeams.Append(view.GetRowCellValue(intRowHandle, view.Columns["LinkedToLabourID"]).ToString() + ",");
                    }
                    else
                    {
                        sbStaff.Append(view.GetRowCellValue(intRowHandle, view.Columns["LinkedToLabourID"]).ToString() + ",");
                    }

                    int intSelfBillingInvoiceHeaderID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "SelfBillingInvoiceHeaderID"));
                    sbSelfBillingInvoiceHeaderID.Append(intSelfBillingInvoiceHeaderID.ToString() + ",");

                    strSelfBillingInvoiceNumber = view.GetRowCellValue(intRowHandle, "SelfBillingInvoiceNumber").ToString();
                    strPDFPassword = view.GetRowCellDisplayText(intRowHandle, "EmailPassword").ToString();

                    // Create a folder for the invoices if it doesn't exist //
                    strSubFolderName = DateTime.Today.ToString("yyyy-MM-dd") + "_" + strSelfBillingInvoiceNumber;
                    strFolderPath = Path.Combine(i_str_SavedSelfBillingInvoiceFolder, strSubFolderName);
                    if (!Directory.Exists(strFolderPath)) Directory.CreateDirectory(strFolderPath);

                    //strFileName1 = strSelfBillingInvoiceNumber + "_" + intSelfBillingInvoiceHeaderID.ToString().PadLeft(7, '0') + "_" + dtTodayWithTime.ToString("yyyy-MM-dd_HH_mm_ss") + ".PDF";
                    //strFileName2 = "CM_" + strSelfBillingInvoiceNumber + "_" + intSelfBillingInvoiceHeaderID.ToString().PadLeft(7, '0') + "_" + dtTodayWithTime.ToString("yyyy-MM-dd_HH_mm_ss") + ".PDF";
                    strFileName1 = strSubFolderName + "\\" + view.GetRowCellValue(intRowHandle, "LabourName").ToString() + "_" + view.GetRowCellValue(intRowHandle, "LinkedToLabourID").ToString() + "_" + intSelfBillingInvoiceHeaderID.ToString().PadLeft(7, '0') + ".PDF";
                    strFileName2 = strSubFolderName + "\\" + "CM_" + view.GetRowCellValue(intRowHandle, "LabourName").ToString() + "_" + view.GetRowCellValue(intRowHandle, "LinkedToLabourID").ToString() + "_" + intSelfBillingInvoiceHeaderID.ToString().PadLeft(7, '0') + ".PDF";

                    strReturn1 = Create_Physical_PDF_Invoice(intSelfBillingInvoiceHeaderID, strFileName1, strPDFPassword);  // Create physical Self-Billing Invoice PDF file //
                    strReturn2 = Create_Physical_PDF_CM_Report(intSelfBillingInvoiceHeaderID, strFileName2, dtSecondReportEndDate);  // Create physical CM Self-Billing Report PDF file //
                    
                    if (!string.IsNullOrWhiteSpace(strReturn1) || !string.IsNullOrWhiteSpace(strReturn2))  // Creation of a file failed so don't send //
                    {
                        intPDFCreationErrorCount++;
                    }
                    else  // PDF file created //
                    {
                        intPDFCreatedCount++;
                        Update_Invoice_Header(intSelfBillingInvoiceHeaderID, strFileName1, strFileName2, dtTodayWithTime, null, null);  // Update the DB with the filename and date created //
                    }
                }
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                strMessage = "Self-Billing Process completed.\n\n" +
                            "--> PDF Files Created: " + intPDFCreatedCount.ToString() + "\n" +
                            (intPDFCreationErrorCount > 0 ? "<color=red>--> PDF Files Not Created: " + intPDFCreationErrorCount.ToString() + "</color>\n" : "");
                if (intPDFCreationErrorCount > 0) strMessage += "\nErrors occurred when creating one or more of the PDF invoice files. You will need to select these records from the historical grid and click the Re-Create PDF button then the Send PDF button to create and send these invoices.";
                XtraMessageBox.Show(strMessage, "Re-Create Team Self-Billing Invoices", MessageBoxButtons.OK, MessageBoxIcon.Information, DefaultBoolean.True);
            }
            catch (Exception ex)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                XtraMessageBox.Show("An error occurred while Re-creating the Team Self-Billing Invoice PDF Files.\n\nError: " + ex.Message + "\n\nNo Team Self-Billing Invoices Re-created. Please try again. If the problem persists contact Technical Support.", "Re-create Team Self-Billing Invoice PDF Files", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            Refresh_Updated_Team2_Rows(sbTeams.ToString(), sbStaff.ToString());  // Get updated rows and merge back in //
            Refresh_Updated_Invoice_Rows(sbSelfBillingInvoiceHeaderID.ToString());  // Get updated rows and merge back in //
        }

        private void bbiSelectUnsentInvoices_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlInvoice.MainView;
            int intRowCount = view.DataRowCount;
            view.BeginSelection();
            view.ClearSelection();
            view.EndSelection();

            view.BeginSelection();
            for (int i = 0; i < intRowCount; i++)
            {
                if (string.IsNullOrWhiteSpace(view.GetRowCellValue(i, "LastSentDate").ToString())) view.SelectRow(i);
            }
            view.EndSelection();
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                XtraMessageBox.Show("No Invoices are Un-sent.", "Select Un-sent Invoices", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else
            {
                XtraMessageBox.Show(intRowHandles.Length + " Un-sent Invoice(s) selected. Click the Send Un-sent Invoices To Team button to send the invoice(s).", "Select Un-sent Invoices", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return;
        }
        private void bbiResend_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlInvoice.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            
            string strMessage = "You have " + (intRowHandles.Length == 1 ? "1 Team Self-Billing Invoice" : Convert.ToString(intRowHandles.Length) + " Team Self-Billing Invoices") + " selected for Sending to the team(s).\n\n<color=red>Proceed?</color>";
            if (XtraMessageBox.Show(strMessage, "Send Self-Billing Invoice", MessageBoxButtons.YesNo, MessageBoxIcon.Question, DefaultBoolean.True) == DialogResult.No) return;
            
            view.BeginUpdate();
            view.BeginSelection();
            foreach (int intRowHandle in intRowHandles)
            {
                if (string.IsNullOrWhiteSpace(view.GetRowCellDisplayText(intRowHandle, "InvoicePDF").ToString()) || string.IsNullOrWhiteSpace(view.GetRowCellDisplayText(intRowHandle, "Email").ToString()))
                {
                    view.UnselectRow(intRowHandle);
                }
            }
            view.EndSelection();
            view.EndUpdate();
            intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select at least one Team Self-Billing Invoice to Send before proceeding.\n\nNote: This process will de-select Invoices where the Team being sent the invoice has no Email address or the physical PDF Invoice file has not been generated.", "Send Team Self-Billing Invoice PDF Files", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Sending Self-Billing Invoice Files...");

            StringBuilder sbTeams = new StringBuilder();
            StringBuilder sbStaff = new StringBuilder();
            StringBuilder sbSelfBillingInvoiceHeaderID = new StringBuilder();

            try
            {

                int intPDFSendingErrorCount = 0;
                int intPDFSendCount = 0;
                string strReturn = "";
                int intSelfBillingInvoiceHeaderID = 0;
                string strFileName = "";
                string strSelfBillingInvoiceNumber = "";
                string strPDFPassword = "";
                string strTeamEmail = "";
                string strContractManagerEmail = "";
                string strLabourName = "";
                DateTime dtTodayWithTime = DateTime.Now;
                DataRow drEmail = Get_Email_Settings();

                foreach (int intRowHandle in intRowHandles)
                {
                    if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, view.Columns["LinkedToLabourTypeID"])) == 1)
                    {
                        sbTeams.Append(view.GetRowCellValue(intRowHandle, view.Columns["LinkedToLabourID"]).ToString() + ",");
                    }
                    else
                    {
                        sbStaff.Append(view.GetRowCellValue(intRowHandle, view.Columns["LinkedToLabourID"]).ToString() + ",");
                    }

                    intSelfBillingInvoiceHeaderID = Convert.ToInt32(view.GetRowCellDisplayText(intRowHandle, "SelfBillingInvoiceHeaderID"));
                    sbSelfBillingInvoiceHeaderID.Append(intSelfBillingInvoiceHeaderID.ToString() + ",");
                    
                    strSelfBillingInvoiceNumber = view.GetRowCellValue(intRowHandle, "SelfBillingInvoiceNumber").ToString();
                    strPDFPassword = view.GetRowCellValue(intRowHandle, "EmailPassword").ToString();
                    strFileName = view.GetRowCellValue(intRowHandle, "InvoicePDF").ToString();
                    strTeamEmail = view.GetRowCellValue(intRowHandle, "Email").ToString();
                    strContractManagerEmail = view.GetRowCellValue(intRowHandle, "ContractManagerEmail").ToString();
                    strLabourName = view.GetRowCellValue(intRowHandle, "LabourName").ToString();

                    strReturn = Send_Physical_PDF_Invoice(intSelfBillingInvoiceHeaderID, strFileName, strTeamEmail, strContractManagerEmail, strSelfBillingInvoiceNumber, drEmail, strLabourName);
                    if (!string.IsNullOrWhiteSpace(strReturn))  // Creation of file failed so don't send //
                    {
                        intPDFSendingErrorCount++;
                    }
                    else
                    {
                        intPDFSendCount++;
                        Update_Invoice_Header(intSelfBillingInvoiceHeaderID, null, null, null, dtTodayWithTime, null);  // Update the DB with the last sent date //
                    }
                }
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                strMessage = "Self-Billing Invoice Sending Completed.\n\n" +
                            "--> PDF Files Sent: " + intPDFSendCount.ToString() + "\n" +
                            (intPDFSendingErrorCount > 0 ? "<color=red>--> PDF Files Not Sent: " + intPDFSendingErrorCount.ToString() + "</color>\n" : "");
                if (intPDFSendingErrorCount > 0) strMessage += "\nErrors occurred when sending one or more of the PDF invoice files. You will need to select these records from the historical grid and click the Send PDF button.";
                XtraMessageBox.Show(strMessage, "Send Team Self-Billing Invoices", MessageBoxButtons.OK, MessageBoxIcon.Information, DefaultBoolean.True);
            }
            catch (Exception ex)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                XtraMessageBox.Show("An error occurred while Sending the Team Self-Billing Invoice PDF Files.\n\nError: " + ex.Message + "\n\nCheck the Last send dates of the invoices to see which records if any, successfully sent.", "Send Team Self-Billing Invoice PDF Files", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            Refresh_Updated_Team2_Rows(sbTeams.ToString(), sbStaff.ToString());  // Get updated rows and merge back in //
            Refresh_Updated_Invoice_Rows(sbSelfBillingInvoiceHeaderID.ToString());  // Get updated rows and merge back in //
        }

        private void bbiSelectUnsentInvoiceReports_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlInvoice.MainView;
            int intRowCount = view.DataRowCount;
            view.BeginSelection();
            view.ClearSelection();
            view.EndSelection();

            view.BeginSelection();
            for (int i = 0; i < intRowCount; i++)
            {
                if (string.IsNullOrWhiteSpace(view.GetRowCellValue(i, "LastSentCMReportDate").ToString())) view.SelectRow(i);
            }
            view.EndSelection();
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                XtraMessageBox.Show("No CM Reports are Un-sent.", "Select Un-sent CM Reports", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else
            {
                XtraMessageBox.Show(intRowHandles.Length + " Un-sent CM Report(s) selected. Click the Send Un-sent CM Reports To Contract Managers button to send the CM Report(s).", "Select Un-sent CM Reports", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return;
        }
        private void bbiResendCM_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlInvoice.MainView;
            int[] intRowHandles = view.GetSelectedRows();

            string strMessage = "You have " + (intRowHandles.Length == 1 ? "1 Team Self-Billing CM Report" : Convert.ToString(intRowHandles.Length) + " Team Self-Billing CM Reports") + " selected for Sending to the Contract Manager(s).\n\n<color=red>Proceed?</color>";
            if (XtraMessageBox.Show(strMessage, "Send CM Report", MessageBoxButtons.YesNo, MessageBoxIcon.Question, DefaultBoolean.True) == DialogResult.No) return;

            view.BeginUpdate();
            view.BeginSelection();
            foreach (int intRowHandle in intRowHandles)
            {
                if (string.IsNullOrWhiteSpace(view.GetRowCellDisplayText(intRowHandle, "CMReportPDF").ToString()) || string.IsNullOrWhiteSpace(view.GetRowCellDisplayText(intRowHandle, "ContractManagerEmail").ToString()))
                {
                    view.UnselectRow(intRowHandle);
                }
            }
            view.EndSelection();
            view.EndUpdate();
            intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select at least one Team Self-Billing Invoice to send the CM Report before proceeding.\n\nNote: This process will de-select Invoices where the CM being sent the report has no Email address or the physical PDF CM Report file has not been generated.", "Send CM Report", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Sending CM Report Files...");

            StringBuilder sbTeams = new StringBuilder();
            StringBuilder sbStaff = new StringBuilder();
            StringBuilder sbSelfBillingInvoiceHeaderID = new StringBuilder();

            try
            {

                int intPDFSendingErrorCount = 0;
                int intPDFSendCount = 0;
                string strReturn = "";
                int intSelfBillingInvoiceHeaderID = 0;
                string strFileName = "";
                string strSelfBillingInvoiceNumber = "";
                string strPDFPassword = "";
                string strContractManagerEmail = "";
                string strLabourName = "";
                DateTime dtTodayWithTime = DateTime.Now;
                DataRow drEmail = Get_Email_Settings2();

                foreach (int intRowHandle in intRowHandles)
                {
                    if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, view.Columns["LinkedToLabourTypeID"])) == 1)
                    {
                        sbTeams.Append(view.GetRowCellValue(intRowHandle, view.Columns["LinkedToLabourID"]).ToString() + ",");
                    }
                    else
                    {
                        sbStaff.Append(view.GetRowCellValue(intRowHandle, view.Columns["LinkedToLabourID"]).ToString() + ",");
                    }

                    intSelfBillingInvoiceHeaderID = Convert.ToInt32(view.GetRowCellDisplayText(intRowHandle, "SelfBillingInvoiceHeaderID"));
                    sbSelfBillingInvoiceHeaderID.Append(intSelfBillingInvoiceHeaderID.ToString() + ",");

                    strSelfBillingInvoiceNumber = view.GetRowCellValue(intRowHandle, "SelfBillingInvoiceNumber").ToString();
                    strPDFPassword = view.GetRowCellValue(intRowHandle, "EmailPassword").ToString();
                    strFileName = view.GetRowCellValue(intRowHandle, "CMReportPDF").ToString();
                    strContractManagerEmail = view.GetRowCellValue(intRowHandle, "ContractManagerEmail").ToString();
                    strLabourName = view.GetRowCellValue(intRowHandle, "LabourName").ToString();

                    strReturn = Send_Physical_PDF_CM_Report(intSelfBillingInvoiceHeaderID, strFileName, strContractManagerEmail, strSelfBillingInvoiceNumber, drEmail, strLabourName);
                    if (!string.IsNullOrWhiteSpace(strReturn))  // Creation of file failed so don't send //
                    {
                        intPDFSendingErrorCount++;
                    }
                    else
                    {
                        intPDFSendCount++;
                        Update_Invoice_Header(intSelfBillingInvoiceHeaderID, null, null, null, null, dtTodayWithTime);  // Update the DB with the last sent date //
                    }
                }
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                strMessage = "CM Report Sending Completed.\n\n" +
                            "--> CM Report PDF Files Sent: " + intPDFSendCount.ToString() + "\n" +
                            (intPDFSendingErrorCount > 0 ? "<color=red>--> CM Report PDF Files Not Sent: " + intPDFSendingErrorCount.ToString() + "</color>\n" : "");
                if (intPDFSendingErrorCount > 0) strMessage += "\nErrors occurred when sending one or more of the PDF invoice files. You will need to select these records from the historical grid and click the Send CM Report button.";
                XtraMessageBox.Show(strMessage, "Send CM Reports", MessageBoxButtons.OK, MessageBoxIcon.Information, DefaultBoolean.True);
            }
            catch (Exception ex)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                XtraMessageBox.Show("An error occurred while Sending the CM Report PDF Files.\n\nError: " + ex.Message + "\n\nCheck the Last send dates of the invoices to see which records if any, successfully sent.", "Send CM Report PDF Files", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            Refresh_Updated_Team2_Rows(sbTeams.ToString(), sbStaff.ToString());  // Get updated rows and merge back in //
            Refresh_Updated_Invoice_Rows(sbSelfBillingInvoiceHeaderID.ToString());  // Get updated rows and merge back in //
        }

        private void bbiDeleteInvoice_ItemClick(object sender, ItemClickEventArgs e)
        {
            Load_Data_Invoices();  // Reload the grid just in case user has been in Finance Self-Billing and done an export - don't want to allow invoices with exported visits to be deleted //

            GridView view = (GridView)gridControlInvoice.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;

            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more Team Self-Billing Invoices to delete by clicking on them then try again.", "Delete Self-Billing Invoice", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Check if any visits on the invoice have been exported to Finance - if yes, de-select the Invoice so it can't be deleted //
            view.BeginUpdate();
            view.BeginSelection();
            foreach (int intRowHandle in intRowHandles)
            {
                if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "LinkedFinanceExportedCount")) > 0) view.UnselectRow(intRowHandle);
            }
            view.EndSelection();
            view.EndUpdate();
 
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("No Team Self-Billing Invoices are selected for Deleting.\n\nNote: This process will de-select Invoices with any attached visits which have been exported by Finance.", "Delete Self-Billing Invoice", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            string strMessage = "You have " + (intCount == 1 ? "1 Team Self-Billing Invoice" : intCount.ToString() + " Team Self-Billing Invoices") + " selected for Deleting.\n\n<color=red>If you proceed these invoices will be deleted and the jobs will no longer be linked to the invoice. These jobs will then be available for linking to new invoices. Proceed?</color>";
            if (XtraMessageBox.Show(strMessage, "Delete Self-Billing Invoice", MessageBoxButtons.YesNo, MessageBoxIcon.Question, DefaultBoolean.True) == DialogResult.No) return;

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Deleting Self-Billing Invoices...");

            try
            {
                DataTable dtInvoicesToDelete = new DataTable("Visits");
                dtInvoicesToDelete.Columns.Add("VisitID", typeof(int));
                dtInvoicesToDelete.Rows.Clear();

                foreach (int intRowHandle in intRowHandles)
                {
                    dtInvoicesToDelete.Rows.Add(Convert.ToInt32(view.GetRowCellValue(intRowHandle, "SelfBillingInvoiceHeaderID")));
                }

                bool boolRecordDeletionSuccessful = true;
                using (var RemoveRecords = new DataSet_OM_BillingTableAdapters.QueriesTableAdapter())
                {
                    RemoveRecords.ChangeConnectionString(strConnectionString + "Connect Timeout=3600;");
                    RemoveRecords.ChangeCommandTimeout("dbo.sp06000_OM_Delete_Self_Billing_Invoice", 3600);  // Timeout on SQL Connection to 1 hour - need line above too. //               
                    try
                    {
                        RemoveRecords.sp06000_OM_Delete_Self_Billing_Invoice(dtInvoicesToDelete);  // Remove the records from the DB in one go //
                    }
                    catch (Exception ex) 
                    {
                        boolRecordDeletionSuccessful = false;
                    }
                }
                if (boolRecordDeletionSuccessful)
                {
                    // Remove physical PDF files //
                    string strOldFileName = "";
                    foreach (int intRowHandle in intRowHandles)
                    {
                        strOldFileName = view.GetRowCellDisplayText(intRowHandle, "InvoicePDF").ToString();
                        if (!string.IsNullOrWhiteSpace(strOldFileName))
                        {
                            try
                            {
                                strOldFileName = Path.Combine(i_str_SavedSelfBillingInvoiceFolder + strOldFileName);
                                System.IO.File.Delete(strOldFileName);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        strOldFileName = view.GetRowCellDisplayText(intRowHandle, "CMReportPDF").ToString();
                        if (!string.IsNullOrWhiteSpace(strOldFileName))
                        {
                            try
                            {
                                strOldFileName = Path.Combine(i_str_SavedSelfBillingInvoiceFolder + strOldFileName);
                                System.IO.File.Delete(strOldFileName);
                            }
                            catch (Exception)
                            {
                            }
                        }
                    }
                }
                
                Load_Data_Teams2();
                Load_Data_Invoices();

                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete Self-Billing Invoices", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                XtraMessageBox.Show("An error occurred while Deleting the selected Team Self-Billing Invoices.\n\nError: " + ex.Message + "\n\nPlease try again. If the problem persists please contact Technical Support.", "Delete Team Self-Billing Invoice", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
        }

        #endregion


        #region Preview Page

        private void bbiBack_ItemClick(object sender, ItemClickEventArgs e)
        {
            xtraTabControl1.SelectedTabPage = xtraTabPageChecking;
        }

        private void bbiEditReportLayout_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string strReportFileName = "TeamSelfBillingInvoice.repx";

            if (DevExpress.XtraEditors.XtraMessageBox.Show("Are you sure you wish to edit the selected layout?", "Edit Report Layout", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                // Get some test data to feed the report in case user tries to Preview it from the designer //
                string strSelfBillingInvoiceIDs = "0,";
                using (var GetTestData = new DataSet_OM_BillingTableAdapters.QueriesTableAdapter())
                {
                    GetTestData.ChangeConnectionString(strConnectionString);
                    try
                    {
                        strSelfBillingInvoiceIDs = GetTestData.sp06462_OM_Self_Billing_Report_Test_Data().ToString();
                    }
                    catch (Exception) { }
                }

                rpt_OM_Team_Self_Billing_Invoice rptReport = new rpt_OM_Team_Self_Billing_Invoice(this.GlobalSettings, strSelfBillingInvoiceIDs);
                try
                {
                    rptReport.LoadLayout(i_str_SavedDirectoryName + strReportFileName);
                    rptReport.DisplayName = "Team Self-Billing Invoice";
                    rptReport.Bookmark = "Team Self-Billing Invoice";

                }
                catch (Exception Ex)
                {
                    Console.WriteLine(Ex.Message);
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to load layout, it may no longer exist or you may not have the necessary permissions to access it - contact Technical Support.", "Load Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                this.splashScreenManager.ShowWaitForm();
                this.splashScreenManager.SetWaitFormDescription("Loading Report Builder...");

                // Open the report in the Report Builder - Create a design form and get its panel //
                XRDesignRibbonFormEx form = new XRDesignRibbonFormEx();  // Create an End-User Designer Form with a Ribbon //
                XRDesignPanel panel = form.DesignPanel;
                panel.SetCommandVisibility(ReportCommand.NewReport, CommandVisibility.None);
                panel.SetCommandVisibility(ReportCommand.OpenFile, CommandVisibility.None);
                panel.SetCommandVisibility(ReportCommand.SaveFileAs, CommandVisibility.None);

                // Add a new command handler to the Report Designer which saves the report in a custom way.
                panel.AddCommandHandler(new SaveCommandHandler(panel, i_str_SavedDirectoryName, strReportFileName));

                // Add a new command handler to the report panel to fire code on adding a new object to the report (Not currently used but could be used to preset values on new object for user) //
                panel.ComponentAdded += new ComponentEventHandler(panel_ComponentAdded);

                panel.OpenReport(rptReport);
                form.Shown += new EventHandler(ReportBuilder_Shown);  // Fires event after report builder is shown //
                form.WindowState = FormWindowState.Maximized;

                if (this.splashScreenManager.IsSplashFormVisible)
                {
                    this.splashScreenManager.SetWaitFormDescription("Loading...");  // Reset caption //
                    this.splashScreenManager.CloseWaitForm();
                }
                               
                form.ShowDialog();
                panel.CloseReport();
            }
        }

        private void bbiEditCMReportLayout_ItemClick(object sender, ItemClickEventArgs e)
        {
            string strReportFileName = "CMSelfBillingInvoiceReport.repx";

            if (DevExpress.XtraEditors.XtraMessageBox.Show("Are you sure you wish to edit the selected layout?", "Edit Report Layout", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                // Get some test data to feed the report in case user tries to Preview it from the designer //
                string strSelfBillingInvoiceIDs = "0,";
                DateTime dtReportStartDate = DateTime.Now;
                using (var GetTestData = new DataSet_OM_BillingTableAdapters.QueriesTableAdapter())
                {
                    GetTestData.ChangeConnectionString(strConnectionString);
                    try
                    {
                        strSelfBillingInvoiceIDs = GetTestData.sp06462_OM_Self_Billing_Report_Test_Data().ToString();
                    }
                    catch (Exception) { }
                }

                rpt_OM_CM_Self_Billing_Invoice_Report rptReport = new rpt_OM_CM_Self_Billing_Invoice_Report(this.GlobalSettings, strSelfBillingInvoiceIDs, dtReportStartDate);
                try
                {
                    rptReport.LoadLayout(i_str_SavedDirectoryName + strReportFileName);
                    rptReport.DisplayName = "CM Self-Billing Report";
                    rptReport.Bookmark = "CM Self-Billing Report";

                }
                catch (Exception Ex)
                {
                    Console.WriteLine(Ex.Message);
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to load layout, it may no longer exist or you may not have the necessary permissions to access it - contact Technical Support.", "Load Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                this.splashScreenManager.ShowWaitForm();
                this.splashScreenManager.SetWaitFormDescription("Loading Report Builder...");

                // Open the report in the Report Builder - Create a design form and get its panel //
                XRDesignRibbonFormEx form = new XRDesignRibbonFormEx();  // Create an End-User Designer Form with a Ribbon //
                XRDesignPanel panel = form.DesignPanel;
                panel.SetCommandVisibility(ReportCommand.NewReport, CommandVisibility.None);
                panel.SetCommandVisibility(ReportCommand.OpenFile, CommandVisibility.None);
                panel.SetCommandVisibility(ReportCommand.SaveFileAs, CommandVisibility.None);

                // Add a new command handler to the Report Designer which saves the report in a custom way.
                panel.AddCommandHandler(new SaveCommandHandler(panel, i_str_SavedDirectoryName, strReportFileName));

                // Add a new command handler to the report panel to fire code on adding a new object to the report (Not currently used but could be used to preset values on new object for user) //
                panel.ComponentAdded += new ComponentEventHandler(panel_ComponentAdded);

                panel.OpenReport(rptReport);
                form.Shown += new EventHandler(ReportBuilder_Shown);  // Fires event after report builder is shown //
                form.WindowState = FormWindowState.Maximized;

                if (this.splashScreenManager.IsSplashFormVisible)
                {
                    this.splashScreenManager.SetWaitFormDescription("Loading...");  // Reset caption //
                    this.splashScreenManager.CloseWaitForm();
                }

                form.ShowDialog();
                panel.CloseReport();
            }
        }



        #region Report Builder

        void ReportBuilder_Shown(object sender, EventArgs e)
        {
            if (this.splashScreenManager.IsSplashFormVisible)
            {
                this.splashScreenManager.SetWaitFormDescription("Loading...");  // Reset caption //
                this.splashScreenManager.CloseWaitForm();
            }
        }

        void panel_ComponentAdded(object sender, ComponentEventArgs e)
        {
            if (e.Component is XRTableCell)// && FormLoaded)
            {
                //((XRTableCell)e.Component).Font = new Font("Arial", 12, FontStyle.Bold);
            }
            if (e.Component is XRLabel)
            {
                //((XRLabel)e.Component).PreviewClick += new PreviewMouseEventHandler(My_PreviewClick);
            }
        }

        private void AdjustEventHandlers(XtraReport ActiveReport)
        {
            foreach (Band b in ActiveReport.Bands)
            {
                foreach (XRControl c in b.Controls)
                {
                    if (c is XRTable)
                    {
                        XRTable t = (XRTable)c;
                        foreach (XRControl row in t.Controls)
                        {
                            foreach (XRControl cell in row.Controls)
                            {
                                if (cell.Tag.ToString() != "")
                                {
                                    cell.PreviewClick += new PreviewMouseEventHandler(My_PreviewClick);
                                }
                            }
                        }
                    }
                }
            }
        }

        private XRTableCell currentSortCell;  // Used for sorting on-the-fly //
        private Boolean SortAscending = false;
        private void My_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            // ***** Used for on-the-fly sorting ***** //
            // Turn off sorting //
            DetailBand db = (DetailBand)rptReport.Bands.GetBandByType(typeof(DetailBand));
            if (db != null)
            {
                if (((XRControl)sender).Tag.ToString() == null) return;

                //printingSystem1.Begin();  // Switch redraw off //
                loadingForm = new WaitDialogForm("Sorting Report...", "Reporting");
                loadingForm.Show();

                db.SortFields.Clear();
                if (currentSortCell != null)
                {
                    currentSortCell.Text = currentSortCell.Text.Remove(currentSortCell.Text.Length - 1, 1);
                }
                // Create a new field to sort //
                GroupField grField = new GroupField();
                grField.FieldName = ((XRControl)sender).Tag.ToString();
                if (currentSortCell != null)
                {
                    if (currentSortCell.Text == ((XRLabel)sender).Text)
                    {
                        if (SortAscending)
                        {
                            grField.SortOrder = XRColumnSortOrder.Descending;
                            SortAscending = !SortAscending;
                        }
                        else
                        {
                            grField.SortOrder = XRColumnSortOrder.Ascending;
                            SortAscending = !SortAscending;
                        }
                    }
                    else
                    {
                        grField.SortOrder = XRColumnSortOrder.Ascending;
                        SortAscending = true;
                    }
                }
                else
                {
                    grField.SortOrder = XRColumnSortOrder.Ascending;
                    SortAscending = true;
                }
                // Add sorting //
                db.SortFields.Add(grField);
                ((XRLabel)sender).Text = ((XRLabel)sender).Text + "*";
                currentSortCell = (XRTableCell)sender;
                // Recreate the report document.
                rptReport.CreateDocument();
                loadingForm.Close();
                //printingSystem1.End();  // Switch redraw back on //
            }
        }

        public class SaveCommandHandler : DevExpress.XtraReports.UserDesigner.ICommandHandler
        {
            XRDesignPanel panel;
            public string strFullPath = "";
            public string strFileName = "";

            public SaveCommandHandler(XRDesignPanel panel, string strFullPath, string strFileName)
            {
                this.panel = panel;
                this.strFullPath = strFullPath;
                this.strFileName = strFileName;
            }

            public void HandleCommand(DevExpress.XtraReports.UserDesigner.ReportCommand command, object[] args)
            {
                //if (!CanHandleCommand(command)) return;
                Save();  // Save report //
                //handled = true;  // Set handled to true to avoid the standard saving procedure to be called.
            }

            public bool CanHandleCommand(DevExpress.XtraReports.UserDesigner.ReportCommand command, ref bool useNextHandler)
            {
                useNextHandler = !(command == ReportCommand.SaveFile ||
                    command == ReportCommand.SaveFileAs ||
                    command == ReportCommand.Closing);
                return !useNextHandler;
            }

            void Save()
            {
                Boolean blSaved = false;
                panel.ReportState = ReportState.Saved;  // Prevent the "Report has been changed" dialog from being shown //

                // Update existing file layout //
                panel.Report.DataSource = null;
                panel.Report.DataMember = null;
                panel.Report.DataAdapter = null;
                try
                {
                    panel.Report.SaveLayout(strFullPath + strFileName);
                    blSaved = true;
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save Report Layout... there is a problem with the default path!\n\nContact Technical Support.", "Save Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    Console.WriteLine(ex.Message);
                    blSaved = false;
                }
                if (blSaved)
                {
                    panel.ReportState = ReportState.Saved;   // Prevent the "Report has been changed" dialog from being shown //
                }
            }
        }

        #endregion

        #endregion


        public string Create_Physical_PDF_Invoice(int intSelfBillingInvoiceHeaderID, string strFileName, string strPassword)
        {
            if (intSelfBillingInvoiceHeaderID <= 0) return "No Self-Billing Invoice Header ID";

            try
            {
                string strPDFName = "";
                // Create report //
                string strReportFileName = "TeamSelfBillingInvoice.repx";
                rpt_OM_Team_Self_Billing_Invoice rptReport = new rpt_OM_Team_Self_Billing_Invoice(this.GlobalSettings, intSelfBillingInvoiceHeaderID.ToString() + ",");
                rptReport.LoadLayout(Path.Combine(i_str_SavedDirectoryName + strReportFileName));

                // Set security options of report so when it is exported, it can't be edited and is password protected //
                rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsOptions.EnableCopying = false;
                rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsOptions.ChangingPermissions = DevExpress.XtraPrinting.ChangingPermissions.None;
                rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsOptions.PrintingPermissions = DevExpress.XtraPrinting.PrintingPermissions.HighResolution;
                rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsPassword = "GroundControlXX";
                if (!string.IsNullOrEmpty(strPassword)) rptReport.ExportOptions.Pdf.PasswordSecurityOptions.OpenPassword = strPassword;

                // Save report to PDF //
                strPDFName = Path.Combine(i_str_SavedSelfBillingInvoiceFolder + strFileName);  // Put path onto start of filename //
                rptReport.ExportToPdf(strPDFName);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return "";
        }

        public string Send_Physical_PDF_Invoice(int intSelfBillingInvoiceHeaderID, string strFileName, string strEmailTeam, string strEmailContractManagers, string strSelfBillingInvoiceNumber, DataRow drEmail, string strLabourName)
        {
            // Get DB Settings for sending emails //
            string strEmailBodyFile = "";
            string strEmailFrom = "";
            string strEmailSubjectLine = "";
            string strCCToEmailAddress = "";

            string strSMTPMailServerAddress = "";
            string strSMTPMailServerUsername = "";
            string strSMTPMailServerPassword = "";
            string strSMTPMailServerPort = "";

            try
            {
                strEmailBodyFile = drEmail["BodyFileName"].ToString();
                strEmailFrom = drEmail["EmailFrom"].ToString();
                strEmailSubjectLine = (string.IsNullOrWhiteSpace(drEmail["SubjectLine"].ToString()) ? "Ground Control Self-Billing Invoice" : drEmail["SubjectLine"].ToString()) + ": " + strSelfBillingInvoiceNumber + " - " + strLabourName;
                strCCToEmailAddress = drEmail["CCToName"].ToString();

                strSMTPMailServerAddress = drEmail["SMTPMailServerAddress"].ToString();
                strSMTPMailServerUsername = drEmail["SMTPMailServerUsername"].ToString();
                strSMTPMailServerPassword = drEmail["SMTPMailServerPassword"].ToString();
                strSMTPMailServerPort = drEmail["SMTPMailServerPort"].ToString();
                if (string.IsNullOrEmpty(strSMTPMailServerPort) || !CheckingFunctions.IsNumeric(strSMTPMailServerPort)) strSMTPMailServerPort = "0";
                int intSMTPMailServerPort = Convert.ToInt32(strSMTPMailServerPort);

                if (string.IsNullOrEmpty(strEmailBodyFile) || string.IsNullOrEmpty(strEmailFrom) || string.IsNullOrEmpty(strSMTPMailServerAddress)) return "One or more of the email settings (Email Layout File, From Email Address and SMTP Mail Server Name) missing from the System Configuration Screen.";

                bool boolNoInternet = BaseObjects.PingTest.Ping("www.google.com");
                if (!boolNoInternet) boolNoInternet = BaseObjects.PingTest.Ping("www.microsoft.com");  // try another site just in case that one is down //
                if (!boolNoInternet) boolNoInternet = BaseObjects.PingTest.Ping("www.yahoo.com");  // try another site just in case that one is down //
                if (!boolNoInternet) return "Unable to connect to the internet - Check Internet Connection.";

                char[] delimiters = new char[] { ',' };

                string strBody = System.IO.File.ReadAllText(strEmailBodyFile);

                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                msg.From = new System.Net.Mail.MailAddress(strEmailFrom);
                string[] strEmailTo = strEmailTeam.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                if (strEmailTo.Length > 0)
                {
                    foreach (string strEmailAddress in strEmailTo)
                    {
                        msg.To.Add(new System.Net.Mail.MailAddress(strEmailAddress));
                    }
                }
                else
                {
                    msg.To.Add(new System.Net.Mail.MailAddress(strEmailTeam));  // Original value wouldn't split as no commas so it's just one email address so use it //
                }
                msg.Subject = strEmailSubjectLine;

                // Add any passed in Contract Manager email addresses to the CC of the email //
                string[] strEmailCC = strEmailContractManagers.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                if (strEmailCC.Length > 0)
                {
                    foreach (string strEmailAddress in strEmailCC)
                    {
                        msg.CC.Add(new System.Net.Mail.MailAddress(strEmailAddress));
                    }
                }
                else
                {
                    msg.CC.Add(new System.Net.Mail.MailAddress(strEmailContractManagers));  // Original value wouldn't split as no commas so it's just one email address so use it //
                }

                if (!string.IsNullOrEmpty(strCCToEmailAddress)) msg.CC.Add(strCCToEmailAddress);
                msg.Priority = System.Net.Mail.MailPriority.High;
                msg.IsBodyHtml = true;

                System.Net.Mail.AlternateView plainView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(System.Text.RegularExpressions.Regex.Replace(strBody, @"<(.|\n)*?>", string.Empty), null, "text/plain");
                System.Net.Mail.AlternateView htmlView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(strBody, null, "text/html");

                // Create a new attachment //
                System.Net.Mail.Attachment mailAttachment = new System.Net.Mail.Attachment(Path.Combine(i_str_SavedSelfBillingInvoiceFolder + strFileName));  // Attach PDF file //
                msg.Attachments.Add(mailAttachment);

                /*        // Create the LinkedResource (embedded image) //
                        System.Net.Mail.LinkedResource logo = new System.Net.Mail.LinkedResource(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\" + "Company_Logo.jpg");
                        logo.ContentId = "companylogo";
                        //add the LinkedResource to the appropriate view
                        htmlView.LinkedResources.Add(logo);

                        // Create the LinkedResource (embedded image) //
                        System.Net.Mail.LinkedResource logo2 = new System.Net.Mail.LinkedResource(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\" + "Company_Footer.gif");
                        logo2.ContentId = "companyfooter";
                       
                        // Add the LinkedResource to the appropriate view //
                        htmlView.LinkedResources.Add(logo2);
                */
                msg.AlternateViews.Add(plainView);
                msg.AlternateViews.Add(htmlView);

                object userState = msg;
                System.Net.Mail.SmtpClient emailClient = null;
                if (intSMTPMailServerPort != 0)
                {
                    emailClient = new System.Net.Mail.SmtpClient(strSMTPMailServerAddress, intSMTPMailServerPort);
                }
                else
                {
                    emailClient = new System.Net.Mail.SmtpClient(strSMTPMailServerAddress);
                }
                if (!string.IsNullOrEmpty(strSMTPMailServerUsername) && !string.IsNullOrEmpty(strSMTPMailServerPassword))
                {
                    System.Net.NetworkCredential basicCredential = new System.Net.NetworkCredential(strSMTPMailServerUsername, strSMTPMailServerPassword);
                    emailClient.UseDefaultCredentials = false;
                    emailClient.Credentials = basicCredential;
                }
                emailClient.SendAsync(msg, userState);
                //Application.DoEvents();  // Give Application time to catch up in case we run into a timing issue - MIGHT NEED TO SWITCH THIS ON //
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return "";
        }

        public string Send_Physical_PDF_CM_Report(int intSelfBillingInvoiceHeaderID, string strFileName, string strEmailContractManagers, string strSelfBillingInvoiceNumber, DataRow drEmail, string strLabourName)
        {
            // Get DB Settings for sending emails //
            string strEmailBodyFile = "";
            string strEmailFrom = "";
            string strEmailSubjectLine = "";
            string strCCToEmailAddress = "";

            string strSMTPMailServerAddress = "";
            string strSMTPMailServerUsername = "";
            string strSMTPMailServerPassword = "";
            string strSMTPMailServerPort = "";

            try
            {
                strEmailBodyFile = drEmail["BodyFileName"].ToString();
                strEmailFrom = drEmail["EmailFrom"].ToString();
                strEmailSubjectLine = (string.IsNullOrWhiteSpace(drEmail["SubjectLine"].ToString()) ? "Ground Control CM Report" : drEmail["SubjectLine"].ToString()) + ": " + strSelfBillingInvoiceNumber + " - " + strLabourName;
                strCCToEmailAddress = drEmail["CCToName"].ToString();

                strSMTPMailServerAddress = drEmail["SMTPMailServerAddress"].ToString();
                strSMTPMailServerUsername = drEmail["SMTPMailServerUsername"].ToString();
                strSMTPMailServerPassword = drEmail["SMTPMailServerPassword"].ToString();
                strSMTPMailServerPort = drEmail["SMTPMailServerPort"].ToString();
                if (string.IsNullOrEmpty(strSMTPMailServerPort) || !CheckingFunctions.IsNumeric(strSMTPMailServerPort)) strSMTPMailServerPort = "0";
                int intSMTPMailServerPort = Convert.ToInt32(strSMTPMailServerPort);

                if (string.IsNullOrEmpty(strEmailBodyFile) || string.IsNullOrEmpty(strEmailFrom) || string.IsNullOrEmpty(strSMTPMailServerAddress)) return "One or more of the email settings (Email Layout File, From Email Address and SMTP Mail Server Name) missing from the System Configuration Screen.";

                bool boolNoInternet = BaseObjects.PingTest.Ping("www.google.com");
                if (!boolNoInternet) boolNoInternet = BaseObjects.PingTest.Ping("www.microsoft.com");  // try another site just in case that one is down //
                if (!boolNoInternet) boolNoInternet = BaseObjects.PingTest.Ping("www.yahoo.com");  // try another site just in case that one is down //
                if (!boolNoInternet) return "Unable to connect to the internet - Check Internet Connection.";

                char[] delimiters = new char[] { ',' };

                string strBody = System.IO.File.ReadAllText(strEmailBodyFile);

                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                msg.From = new System.Net.Mail.MailAddress(strEmailFrom);
                string[] strEmailTo = strEmailContractManagers.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                if (strEmailTo.Length > 0)
                {
                    foreach (string strEmailAddress in strEmailTo)
                    {
                        msg.To.Add(new System.Net.Mail.MailAddress(strEmailAddress));
                    }
                }
                else
                {
                    msg.To.Add(new System.Net.Mail.MailAddress(strEmailContractManagers));  // Original value wouldn't split as no commas so it's just one email address so use it //
                }
                msg.Subject = strEmailSubjectLine;

                if (!string.IsNullOrEmpty(strCCToEmailAddress)) msg.CC.Add(strCCToEmailAddress);
                msg.Priority = System.Net.Mail.MailPriority.High;
                msg.IsBodyHtml = true;

                System.Net.Mail.AlternateView plainView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(System.Text.RegularExpressions.Regex.Replace(strBody, @"<(.|\n)*?>", string.Empty), null, "text/plain");
                System.Net.Mail.AlternateView htmlView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(strBody, null, "text/html");

                // Create a new attachment //
                System.Net.Mail.Attachment mailAttachment = new System.Net.Mail.Attachment(Path.Combine(i_str_SavedSelfBillingInvoiceFolder + strFileName));  // Attach PDF file //
                msg.Attachments.Add(mailAttachment);

                /*        // Create the LinkedResource (embedded image) //
                        System.Net.Mail.LinkedResource logo = new System.Net.Mail.LinkedResource(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\" + "Company_Logo.jpg");
                        logo.ContentId = "companylogo";
                        //add the LinkedResource to the appropriate view
                        htmlView.LinkedResources.Add(logo);

                        // Create the LinkedResource (embedded image) //
                        System.Net.Mail.LinkedResource logo2 = new System.Net.Mail.LinkedResource(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\" + "Company_Footer.gif");
                        logo2.ContentId = "companyfooter";
                       
                        // Add the LinkedResource to the appropriate view //
                        htmlView.LinkedResources.Add(logo2);
                */
                msg.AlternateViews.Add(plainView);
                msg.AlternateViews.Add(htmlView);

                object userState = msg;
                System.Net.Mail.SmtpClient emailClient = null;
                if (intSMTPMailServerPort != 0)
                {
                    emailClient = new System.Net.Mail.SmtpClient(strSMTPMailServerAddress, intSMTPMailServerPort);
                }
                else
                {
                    emailClient = new System.Net.Mail.SmtpClient(strSMTPMailServerAddress);
                }
                if (!string.IsNullOrEmpty(strSMTPMailServerUsername) && !string.IsNullOrEmpty(strSMTPMailServerPassword))
                {
                    System.Net.NetworkCredential basicCredential = new System.Net.NetworkCredential(strSMTPMailServerUsername, strSMTPMailServerPassword);
                    emailClient.UseDefaultCredentials = false;
                    emailClient.Credentials = basicCredential;
                }
                emailClient.SendAsync(msg, userState);
                //Application.DoEvents();  // Give Application time to catch up in case we run into a timing issue - MIGHT NEED TO SWITCH THIS ON //
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return "";
        }

        public string Create_Physical_PDF_CM_Report(int intSelfBillingInvoiceHeaderID, string strFileName, DateTime EndDate)
        {
            if (intSelfBillingInvoiceHeaderID <= 0) return "No Self-Billing Invoice Header ID";
            try
            {
                string strPDFName = "";
                // Create report //
                string strReportFileName = "CMSelfBillingInvoiceReport.repx";
                rpt_OM_CM_Self_Billing_Invoice_Report rptReport = new rpt_OM_CM_Self_Billing_Invoice_Report(this.GlobalSettings, intSelfBillingInvoiceHeaderID.ToString() + ",", EndDate);
                rptReport.LoadLayout(Path.Combine(i_str_SavedDirectoryName + strReportFileName));

                // Set security options of report so when it is exported, it can't be edited and is password protected //
                rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsOptions.EnableCopying = false;
                rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsOptions.ChangingPermissions = DevExpress.XtraPrinting.ChangingPermissions.None;
                rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsOptions.PrintingPermissions = DevExpress.XtraPrinting.PrintingPermissions.HighResolution;
                rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsPassword = "GroundControlXX";
                //if (!string.IsNullOrEmpty(strPassword)) rptReport.ExportOptions.Pdf.PasswordSecurityOptions.OpenPassword = strPassword;

                // Save report to PDF //
                strPDFName = Path.Combine(i_str_SavedSelfBillingInvoiceFolder + strFileName);  // Put path onto start of filename //
                rptReport.ExportToPdf(strPDFName);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return "";
        }


        public string Update_Invoice_Header(int intSelfBillingInvoiceHeaderID, string strFileName1, string strFileName2, DateTime? dtDateRaised, DateTime? dtLastSentDate1, DateTime? dtLastSentDate2)
        {
            using (var UpdateRecord = new DataSet_OM_BillingTableAdapters.QueriesTableAdapter())
            {
                UpdateRecord.ChangeConnectionString(strConnectionString);
                try
                {
                    UpdateRecord.sp06466_OM_Self_Billing_Invoice_Update_Invoice_Header(intSelfBillingInvoiceHeaderID, strFileName1, strFileName2, dtDateRaised, dtLastSentDate1, dtLastSentDate2);
                }
                catch (Exception ex) 
                {
                    return ex.Message;
                }
            }
            return "";  // No text so success //
        }

        private DataRow Get_Email_Settings()
        {
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[]
            { 
                new DataColumn("BodyFileName", typeof(string)),
                new DataColumn("EmailFrom", typeof(string)),
                new DataColumn("SubjectLine", typeof(string)),
                new DataColumn("CCToName",typeof(string)),                    
                new DataColumn("SMTPMailServerAddress",typeof(string)),                    
                new DataColumn("SMTPMailServerUsername",typeof(string)),                    
                new DataColumn("SMTPMailServerPassword",typeof(string)),                    
                new DataColumn("SMTPMailServerPort",typeof(string))           
            });
            DataRow drEmpty = dt.NewRow();
            drEmpty["BodyFileName"] = string.Empty;
            drEmpty["EmailFrom"] = string.Empty;
            drEmpty["SubjectLine"] = string.Empty;
            drEmpty["CCToName"] = string.Empty;
            drEmpty["SMTPMailServerAddress"] = string.Empty;
            drEmpty["SMTPMailServerUsername"] = string.Empty;
            drEmpty["SMTPMailServerPassword"] = string.Empty;
            drEmpty["SMTPMailServerPort"] = string.Empty;
            dt.Rows.Add(drEmpty);


            using (SqlConnection conn = new SqlConnection(strConnectionStringREADONLY))
            {
                conn.Open();
                SqlCommand cmd = null;
                cmd = new SqlCommand("sp06465_OM_Self_Billing_Invoice_Email_Settings", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter sdaSettings = new SqlDataAdapter(cmd);
                DataSet dsSettings = new DataSet("NewDataSet");
                sdaSettings.Fill(dsSettings, "Table");

                return (dsSettings.Tables[0].Rows.Count != 1 ? drEmpty : dsSettings.Tables[0].Rows[0]);
            }
        }

        private DataRow Get_Email_Settings2()
        {
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[]
            { 
                new DataColumn("BodyFileName", typeof(string)),
                new DataColumn("EmailFrom", typeof(string)),
                new DataColumn("SubjectLine", typeof(string)),
                new DataColumn("CCToName",typeof(string)),                    
                new DataColumn("SMTPMailServerAddress",typeof(string)),                    
                new DataColumn("SMTPMailServerUsername",typeof(string)),                    
                new DataColumn("SMTPMailServerPassword",typeof(string)),                    
                new DataColumn("SMTPMailServerPort",typeof(string))           
            });
            DataRow drEmpty = dt.NewRow();
            drEmpty["BodyFileName"] = string.Empty;
            drEmpty["EmailFrom"] = string.Empty;
            drEmpty["SubjectLine"] = string.Empty;
            drEmpty["CCToName"] = string.Empty;
            drEmpty["SMTPMailServerAddress"] = string.Empty;
            drEmpty["SMTPMailServerUsername"] = string.Empty;
            drEmpty["SMTPMailServerPassword"] = string.Empty;
            drEmpty["SMTPMailServerPort"] = string.Empty;
            dt.Rows.Add(drEmpty);


            using (SqlConnection conn = new SqlConnection(strConnectionStringREADONLY))
            {
                conn.Open();
                SqlCommand cmd = null;
                cmd = new SqlCommand("sp06480_OM_CM_Report_Self_Billing_Invoice_Email_Settings", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter sdaSettings = new SqlDataAdapter(cmd);
                DataSet dsSettings = new DataSet("NewDataSet");
                sdaSettings.Fill(dsSettings, "Table");

                return (dsSettings.Tables[0].Rows.Count != 1 ? drEmpty : dsSettings.Tables[0].Rows[0]);
            }
        }


        private void Open_Picture_Viewer(string RecordIDs, int ClientID, int RecordTypeID)
        {
            var fChildForm = new frm_OM_Picture_Viewer();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm._PassedInRecordIDs = RecordIDs;
            fChildForm._PassedInRecordTypeID = RecordTypeID;
            fChildForm._ClientID = ClientID;
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();
            fChildForm.Show();
            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
            return;
        }






    }
}



namespace WoodPlan5
{
    partial class frm_OM_Select_Contractor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Select_Contractor));
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            this.colDisabled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp06100OMContractorSelectBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Contract = new WoodPlan5.DataSet_OM_Contract();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMobileTelephone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVetReg = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUser1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUser2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUser3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colInternalContractor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTextNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsWoodPlan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsGritter = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsSnowClearer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsVatRegistered = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsUtilityArbTeam = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsOperationsTeam = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinanceSystemCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLabourType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalculatedDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditMiles = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colActiveTeamMemberCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.sp06100_OM_Contractor_SelectTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06100_OM_Contractor_SelectTableAdapter();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bciPreferredOnly = new DevExpress.XtraBars.BarCheckItem();
            this.barEditItemSearchRadius = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditSearchRadius = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerControlSearchRadius = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnOKSearchRadius = new DevExpress.XtraEditors.SimpleButton();
            this.radiusLabel = new DevExpress.XtraEditors.LabelControl();
            this.trackBarControlRadius = new DevExpress.XtraEditors.TrackBarControl();
            this.barEditItemJobRequirements = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditJobRequirements = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerControlJobRequirements = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnJobRequirementsOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp06185OMSelectContractorJobSubTypeCompetenciesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Job = new WoodPlan5.DataSet_OM_Job();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colJobCompetencyID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colJobTypeID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompetencyDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRequirementLevelDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationSubType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationSubTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bciShowWorkLoad = new DevExpress.XtraBars.BarCheckItem();
            this.bciAuthorisedWorkTypes = new DevExpress.XtraBars.BarCheckItem();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControlLinkedData = new DevExpress.XtraEditors.SplitContainerControl();
            this.popupContainerControlDateRange = new DevExpress.XtraEditors.PopupContainerControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.btnDateRangeOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp06186OMContractorWorkLoadBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLabourUsedID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReactive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobNoLongerRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRequiresAccessPermit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colExpectedEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedDurationUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNumeric2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colScheduleSentDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colJobStatusDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedDurationUnitsDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubTypeDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientNameContractDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaximumDaysFromLastVisit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinimumDaysFromLastVisit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDaysLeeway = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp09119HRQualificationsForContractorBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_Common_Functionality = new WoodPlan5.DataSet_Common_Functionality();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colQualificationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationFromID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCourseNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCourseName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostToCompany = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationFrom = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDaysUntilExpiry = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedDocumentCount7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colArchived = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colAssessmentDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCourseEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCourseStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barEditItemDateRange = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditDateRange = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.bbiRefreshWorkLoad = new DevExpress.XtraBars.BarButtonItem();
            this.sp06185_OM_Select_Contractor_Job_Sub_Type_CompetenciesTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06185_OM_Select_Contractor_Job_Sub_Type_CompetenciesTableAdapter();
            this.sp06186_OM_Contractor_Work_LoadTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06186_OM_Contractor_Work_LoadTableAdapter();
            this.bbiShowHide = new DevExpress.XtraBars.BarButtonItem();
            this.sp09119_HR_Qualifications_For_ContractorTableAdapter = new WoodPlan5.DataSet_Common_FunctionalityTableAdapters.sp09119_HR_Qualifications_For_ContractorTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06100OMContractorSelectBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMiles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditSearchRadius)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlSearchRadius)).BeginInit();
            this.popupContainerControlSearchRadius.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControlRadius)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControlRadius.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditJobRequirements)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlJobRequirements)).BeginInit();
            this.popupContainerControlJobRequirements.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06185OMSelectContractorJobSubTypeCompetenciesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Job)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControlLinkedData)).BeginInit();
            this.splitContainerControlLinkedData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).BeginInit();
            this.popupContainerControlDateRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06186OMContractorWorkLoadBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric2DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp09119HRQualificationsForContractorBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Common_Functionality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateRange)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1080, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 551);
            this.barDockControlBottom.Size = new System.Drawing.Size(1080, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 525);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1080, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 525);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar2});
            this.barManager1.DockControls.Add(this.standaloneBarDockControl1);
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barEditItemSearchRadius,
            this.barEditItemJobRequirements,
            this.barEditItemDateRange,
            this.bbiRefreshWorkLoad,
            this.bbiShowHide,
            this.bciShowWorkLoad,
            this.bciAuthorisedWorkTypes,
            this.bciPreferredOnly});
            this.barManager1.MaxItemId = 38;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEditSearchRadius,
            this.repositoryItemPopupContainerEditJobRequirements,
            this.repositoryItemPopupContainerEditDateRange});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colDisabled
            // 
            this.colDisabled.Caption = "Disabled";
            this.colDisabled.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDisabled.FieldName = "Disabled";
            this.colDisabled.Name = "colDisabled";
            this.colDisabled.OptionsColumn.AllowEdit = false;
            this.colDisabled.OptionsColumn.AllowFocus = false;
            this.colDisabled.OptionsColumn.ReadOnly = true;
            this.colDisabled.Visible = true;
            this.colDisabled.VisibleIndex = 16;
            this.colDisabled.Width = 61;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp06100OMContractorSelectBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemMemoExEdit1,
            this.repositoryItemTextEditMiles});
            this.gridControl1.Size = new System.Drawing.Size(408, 466);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp06100OMContractorSelectBindingSource
            // 
            this.sp06100OMContractorSelectBindingSource.DataMember = "sp06100_OM_Contractor_Select";
            this.sp06100OMContractorSelectBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // dataSet_OM_Contract
            // 
            this.dataSet_OM_Contract.DataSetName = "DataSet_OM_Contract";
            this.dataSet_OM_Contract.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colContractorID,
            this.colTypeID,
            this.colContractorName,
            this.colAddressLine1,
            this.colAddressLine2,
            this.colAddressLine3,
            this.colAddressLine4,
            this.colAddressLine5,
            this.colPostcode,
            this.colTelephone1,
            this.colTelephone2,
            this.colMobileTelephone,
            this.colVetReg,
            this.colUser1,
            this.colUser2,
            this.colUser3,
            this.colRemarks,
            this.colDisabled,
            this.colInternalContractor,
            this.colTextNumber,
            this.colLatitude,
            this.colLongitude,
            this.colIsWoodPlan,
            this.colIsGritter,
            this.colIsSnowClearer,
            this.colIsVatRegistered,
            this.colIsUtilityArbTeam,
            this.colIsOperationsTeam,
            this.colFinanceSystemCode,
            this.colLabourType,
            this.colCalculatedDistance,
            this.colActiveTeamMemberCount});
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.colDisabled;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Gray;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 1;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView1.FormatRules.Add(gridFormatRule1);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsFind.FindDelay = 2000;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLabourType, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCalculatedDistance, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colContractorName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.GridView_FocusedRowChanged_NoGroupSelection);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_NoGroupSelection);
            // 
            // colContractorID
            // 
            this.colContractorID.Caption = "Contractor ID";
            this.colContractorID.FieldName = "ContractorID";
            this.colContractorID.Name = "colContractorID";
            this.colContractorID.OptionsColumn.AllowEdit = false;
            this.colContractorID.OptionsColumn.AllowFocus = false;
            this.colContractorID.OptionsColumn.ReadOnly = true;
            this.colContractorID.Width = 87;
            // 
            // colTypeID
            // 
            this.colTypeID.Caption = "Type ID";
            this.colTypeID.FieldName = "TypeID";
            this.colTypeID.Name = "colTypeID";
            this.colTypeID.OptionsColumn.AllowEdit = false;
            this.colTypeID.OptionsColumn.AllowFocus = false;
            this.colTypeID.OptionsColumn.ReadOnly = true;
            // 
            // colContractorName
            // 
            this.colContractorName.Caption = "Contractor Name";
            this.colContractorName.FieldName = "ContractorName";
            this.colContractorName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colContractorName.Name = "colContractorName";
            this.colContractorName.OptionsColumn.AllowEdit = false;
            this.colContractorName.OptionsColumn.AllowFocus = false;
            this.colContractorName.OptionsColumn.ReadOnly = true;
            this.colContractorName.Visible = true;
            this.colContractorName.VisibleIndex = 0;
            this.colContractorName.Width = 219;
            // 
            // colAddressLine1
            // 
            this.colAddressLine1.Caption = "Address Line 1";
            this.colAddressLine1.FieldName = "AddressLine1";
            this.colAddressLine1.Name = "colAddressLine1";
            this.colAddressLine1.OptionsColumn.AllowEdit = false;
            this.colAddressLine1.OptionsColumn.AllowFocus = false;
            this.colAddressLine1.OptionsColumn.ReadOnly = true;
            this.colAddressLine1.Visible = true;
            this.colAddressLine1.VisibleIndex = 4;
            this.colAddressLine1.Width = 91;
            // 
            // colAddressLine2
            // 
            this.colAddressLine2.Caption = "Address Line 2";
            this.colAddressLine2.FieldName = "AddressLine2";
            this.colAddressLine2.Name = "colAddressLine2";
            this.colAddressLine2.OptionsColumn.AllowEdit = false;
            this.colAddressLine2.OptionsColumn.AllowFocus = false;
            this.colAddressLine2.OptionsColumn.ReadOnly = true;
            this.colAddressLine2.Visible = true;
            this.colAddressLine2.VisibleIndex = 5;
            this.colAddressLine2.Width = 91;
            // 
            // colAddressLine3
            // 
            this.colAddressLine3.Caption = "Address Line 3";
            this.colAddressLine3.FieldName = "AddressLine3";
            this.colAddressLine3.Name = "colAddressLine3";
            this.colAddressLine3.OptionsColumn.AllowEdit = false;
            this.colAddressLine3.OptionsColumn.AllowFocus = false;
            this.colAddressLine3.OptionsColumn.ReadOnly = true;
            this.colAddressLine3.Visible = true;
            this.colAddressLine3.VisibleIndex = 6;
            this.colAddressLine3.Width = 91;
            // 
            // colAddressLine4
            // 
            this.colAddressLine4.Caption = "Address Line 4";
            this.colAddressLine4.FieldName = "AddressLine4";
            this.colAddressLine4.Name = "colAddressLine4";
            this.colAddressLine4.OptionsColumn.AllowEdit = false;
            this.colAddressLine4.OptionsColumn.AllowFocus = false;
            this.colAddressLine4.OptionsColumn.ReadOnly = true;
            this.colAddressLine4.Visible = true;
            this.colAddressLine4.VisibleIndex = 7;
            this.colAddressLine4.Width = 91;
            // 
            // colAddressLine5
            // 
            this.colAddressLine5.Caption = "Address Line 5";
            this.colAddressLine5.FieldName = "AddressLine5";
            this.colAddressLine5.Name = "colAddressLine5";
            this.colAddressLine5.OptionsColumn.AllowEdit = false;
            this.colAddressLine5.OptionsColumn.AllowFocus = false;
            this.colAddressLine5.OptionsColumn.ReadOnly = true;
            this.colAddressLine5.Visible = true;
            this.colAddressLine5.VisibleIndex = 8;
            this.colAddressLine5.Width = 91;
            // 
            // colPostcode
            // 
            this.colPostcode.Caption = "Postcode";
            this.colPostcode.FieldName = "Postcode";
            this.colPostcode.Name = "colPostcode";
            this.colPostcode.OptionsColumn.AllowEdit = false;
            this.colPostcode.OptionsColumn.AllowFocus = false;
            this.colPostcode.OptionsColumn.ReadOnly = true;
            this.colPostcode.Visible = true;
            this.colPostcode.VisibleIndex = 3;
            // 
            // colTelephone1
            // 
            this.colTelephone1.Caption = "Telephone 1";
            this.colTelephone1.FieldName = "Telephone1";
            this.colTelephone1.Name = "colTelephone1";
            this.colTelephone1.OptionsColumn.AllowEdit = false;
            this.colTelephone1.OptionsColumn.AllowFocus = false;
            this.colTelephone1.OptionsColumn.ReadOnly = true;
            this.colTelephone1.Visible = true;
            this.colTelephone1.VisibleIndex = 9;
            this.colTelephone1.Width = 80;
            // 
            // colTelephone2
            // 
            this.colTelephone2.Caption = "Telephone 2";
            this.colTelephone2.FieldName = "Telephone2";
            this.colTelephone2.Name = "colTelephone2";
            this.colTelephone2.OptionsColumn.AllowEdit = false;
            this.colTelephone2.OptionsColumn.AllowFocus = false;
            this.colTelephone2.OptionsColumn.ReadOnly = true;
            this.colTelephone2.Visible = true;
            this.colTelephone2.VisibleIndex = 10;
            this.colTelephone2.Width = 80;
            // 
            // colMobileTelephone
            // 
            this.colMobileTelephone.Caption = "Mobile Tel";
            this.colMobileTelephone.FieldName = "MobileTelephone";
            this.colMobileTelephone.Name = "colMobileTelephone";
            this.colMobileTelephone.OptionsColumn.AllowEdit = false;
            this.colMobileTelephone.OptionsColumn.AllowFocus = false;
            this.colMobileTelephone.OptionsColumn.ReadOnly = true;
            this.colMobileTelephone.Visible = true;
            this.colMobileTelephone.VisibleIndex = 11;
            this.colMobileTelephone.Width = 78;
            // 
            // colVetReg
            // 
            this.colVetReg.Caption = "Vat Reg #";
            this.colVetReg.FieldName = "VetReg";
            this.colVetReg.Name = "colVetReg";
            this.colVetReg.OptionsColumn.AllowEdit = false;
            this.colVetReg.OptionsColumn.AllowFocus = false;
            this.colVetReg.OptionsColumn.ReadOnly = true;
            this.colVetReg.Visible = true;
            this.colVetReg.VisibleIndex = 12;
            this.colVetReg.Width = 97;
            // 
            // colUser1
            // 
            this.colUser1.Caption = "User Defined 1";
            this.colUser1.FieldName = "User1";
            this.colUser1.Name = "colUser1";
            this.colUser1.OptionsColumn.AllowEdit = false;
            this.colUser1.OptionsColumn.AllowFocus = false;
            this.colUser1.OptionsColumn.ReadOnly = true;
            this.colUser1.Visible = true;
            this.colUser1.VisibleIndex = 13;
            this.colUser1.Width = 92;
            // 
            // colUser2
            // 
            this.colUser2.Caption = "User Defined 2";
            this.colUser2.FieldName = "User2";
            this.colUser2.Name = "colUser2";
            this.colUser2.OptionsColumn.AllowEdit = false;
            this.colUser2.OptionsColumn.AllowFocus = false;
            this.colUser2.OptionsColumn.ReadOnly = true;
            this.colUser2.Visible = true;
            this.colUser2.VisibleIndex = 14;
            this.colUser2.Width = 92;
            // 
            // colUser3
            // 
            this.colUser3.Caption = "User Defined 3";
            this.colUser3.FieldName = "User3";
            this.colUser3.Name = "colUser3";
            this.colUser3.OptionsColumn.AllowEdit = false;
            this.colUser3.OptionsColumn.AllowFocus = false;
            this.colUser3.OptionsColumn.ReadOnly = true;
            this.colUser3.Visible = true;
            this.colUser3.VisibleIndex = 15;
            this.colUser3.Width = 92;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 17;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colInternalContractor
            // 
            this.colInternalContractor.Caption = "Internal Contractor";
            this.colInternalContractor.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colInternalContractor.FieldName = "InternalContractor";
            this.colInternalContractor.Name = "colInternalContractor";
            this.colInternalContractor.OptionsColumn.AllowEdit = false;
            this.colInternalContractor.OptionsColumn.AllowFocus = false;
            this.colInternalContractor.OptionsColumn.ReadOnly = true;
            this.colInternalContractor.Width = 114;
            // 
            // colTextNumber
            // 
            this.colTextNumber.Caption = "Text #";
            this.colTextNumber.FieldName = "TextNumber";
            this.colTextNumber.Name = "colTextNumber";
            this.colTextNumber.OptionsColumn.AllowEdit = false;
            this.colTextNumber.OptionsColumn.AllowFocus = false;
            this.colTextNumber.OptionsColumn.ReadOnly = true;
            // 
            // colLatitude
            // 
            this.colLatitude.Caption = "Latitude";
            this.colLatitude.FieldName = "Latitude";
            this.colLatitude.Name = "colLatitude";
            this.colLatitude.OptionsColumn.AllowEdit = false;
            this.colLatitude.OptionsColumn.AllowFocus = false;
            this.colLatitude.OptionsColumn.ReadOnly = true;
            // 
            // colLongitude
            // 
            this.colLongitude.Caption = "Longitude";
            this.colLongitude.FieldName = "Longitude";
            this.colLongitude.Name = "colLongitude";
            this.colLongitude.OptionsColumn.AllowEdit = false;
            this.colLongitude.OptionsColumn.AllowFocus = false;
            this.colLongitude.OptionsColumn.ReadOnly = true;
            // 
            // colIsWoodPlan
            // 
            this.colIsWoodPlan.Caption = "WoodPlan";
            this.colIsWoodPlan.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsWoodPlan.FieldName = "IsWoodPlan";
            this.colIsWoodPlan.Name = "colIsWoodPlan";
            this.colIsWoodPlan.OptionsColumn.AllowEdit = false;
            this.colIsWoodPlan.OptionsColumn.AllowFocus = false;
            this.colIsWoodPlan.OptionsColumn.ReadOnly = true;
            // 
            // colIsGritter
            // 
            this.colIsGritter.Caption = "Gritter";
            this.colIsGritter.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsGritter.FieldName = "IsGritter";
            this.colIsGritter.Name = "colIsGritter";
            this.colIsGritter.OptionsColumn.AllowEdit = false;
            this.colIsGritter.OptionsColumn.AllowFocus = false;
            this.colIsGritter.OptionsColumn.ReadOnly = true;
            // 
            // colIsSnowClearer
            // 
            this.colIsSnowClearer.Caption = "Snow Clearer";
            this.colIsSnowClearer.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsSnowClearer.FieldName = "IsSnowClearer";
            this.colIsSnowClearer.Name = "colIsSnowClearer";
            this.colIsSnowClearer.OptionsColumn.AllowEdit = false;
            this.colIsSnowClearer.OptionsColumn.AllowFocus = false;
            this.colIsSnowClearer.OptionsColumn.ReadOnly = true;
            this.colIsSnowClearer.Width = 85;
            // 
            // colIsVatRegistered
            // 
            this.colIsVatRegistered.Caption = "VAT Registered";
            this.colIsVatRegistered.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsVatRegistered.FieldName = "IsVatRegistered";
            this.colIsVatRegistered.Name = "colIsVatRegistered";
            this.colIsVatRegistered.OptionsColumn.AllowEdit = false;
            this.colIsVatRegistered.OptionsColumn.AllowFocus = false;
            this.colIsVatRegistered.OptionsColumn.ReadOnly = true;
            this.colIsVatRegistered.Width = 95;
            // 
            // colIsUtilityArbTeam
            // 
            this.colIsUtilityArbTeam.Caption = "Utility ARB";
            this.colIsUtilityArbTeam.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsUtilityArbTeam.FieldName = "IsUtilityArbTeam";
            this.colIsUtilityArbTeam.Name = "colIsUtilityArbTeam";
            this.colIsUtilityArbTeam.OptionsColumn.AllowEdit = false;
            this.colIsUtilityArbTeam.OptionsColumn.AllowFocus = false;
            this.colIsUtilityArbTeam.OptionsColumn.ReadOnly = true;
            // 
            // colIsOperationsTeam
            // 
            this.colIsOperationsTeam.Caption = "Operations";
            this.colIsOperationsTeam.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsOperationsTeam.FieldName = "IsOperationsTeam";
            this.colIsOperationsTeam.Name = "colIsOperationsTeam";
            this.colIsOperationsTeam.OptionsColumn.AllowEdit = false;
            this.colIsOperationsTeam.OptionsColumn.AllowFocus = false;
            this.colIsOperationsTeam.OptionsColumn.ReadOnly = true;
            // 
            // colFinanceSystemCode
            // 
            this.colFinanceSystemCode.Caption = "Finance System Code";
            this.colFinanceSystemCode.FieldName = "FinanceSystemCode";
            this.colFinanceSystemCode.Name = "colFinanceSystemCode";
            this.colFinanceSystemCode.OptionsColumn.AllowEdit = false;
            this.colFinanceSystemCode.OptionsColumn.AllowFocus = false;
            this.colFinanceSystemCode.OptionsColumn.ReadOnly = true;
            this.colFinanceSystemCode.Width = 124;
            // 
            // colLabourType
            // 
            this.colLabourType.Caption = "Labour Type";
            this.colLabourType.FieldName = "LabourType";
            this.colLabourType.Name = "colLabourType";
            this.colLabourType.OptionsColumn.AllowEdit = false;
            this.colLabourType.OptionsColumn.AllowFocus = false;
            this.colLabourType.OptionsColumn.ReadOnly = true;
            this.colLabourType.Visible = true;
            this.colLabourType.VisibleIndex = 16;
            this.colLabourType.Width = 147;
            // 
            // colCalculatedDistance
            // 
            this.colCalculatedDistance.Caption = "Distance";
            this.colCalculatedDistance.ColumnEdit = this.repositoryItemTextEditMiles;
            this.colCalculatedDistance.FieldName = "CalculatedDistance";
            this.colCalculatedDistance.Name = "colCalculatedDistance";
            this.colCalculatedDistance.OptionsColumn.AllowEdit = false;
            this.colCalculatedDistance.OptionsColumn.AllowFocus = false;
            this.colCalculatedDistance.OptionsColumn.ReadOnly = true;
            this.colCalculatedDistance.Visible = true;
            this.colCalculatedDistance.VisibleIndex = 1;
            this.colCalculatedDistance.Width = 97;
            // 
            // repositoryItemTextEditMiles
            // 
            this.repositoryItemTextEditMiles.Appearance.Options.UseTextOptions = true;
            this.repositoryItemTextEditMiles.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.repositoryItemTextEditMiles.AutoHeight = false;
            this.repositoryItemTextEditMiles.Mask.EditMask = "#######0.00 Miles";
            this.repositoryItemTextEditMiles.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditMiles.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditMiles.Name = "repositoryItemTextEditMiles";
            // 
            // colActiveTeamMemberCount
            // 
            this.colActiveTeamMemberCount.Caption = "Active Team Members";
            this.colActiveTeamMemberCount.FieldName = "ActiveTeamMemberCount";
            this.colActiveTeamMemberCount.Name = "colActiveTeamMemberCount";
            this.colActiveTeamMemberCount.OptionsColumn.AllowEdit = false;
            this.colActiveTeamMemberCount.OptionsColumn.AllowFocus = false;
            this.colActiveTeamMemberCount.OptionsColumn.ReadOnly = true;
            this.colActiveTeamMemberCount.Visible = true;
            this.colActiveTeamMemberCount.VisibleIndex = 2;
            this.colActiveTeamMemberCount.Width = 124;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(912, 523);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(993, 523);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // sp06100_OM_Contractor_SelectTableAdapter
            // 
            this.sp06100_OM_Contractor_SelectTableAdapter.ClearBeforeFill = true;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 3";
            this.bar1.DockCol = 1;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(604, 170);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bciPreferredOnly),
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemSearchRadius, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemJobRequirements),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciShowWorkLoad, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciAuthorisedWorkTypes, true)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.Text = "Custom 3";
            // 
            // bciPreferredOnly
            // 
            this.bciPreferredOnly.Caption = "Preferred Only";
            this.bciPreferredOnly.Id = 37;
            this.bciPreferredOnly.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bciPreferredOnly.ImageOptions.Image")));
            this.bciPreferredOnly.Name = "bciPreferredOnly";
            this.bciPreferredOnly.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem1.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Preferred Only - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Check me to load only teams for the current Site Contract\'s preferred Labour List" +
    ".\r\nLeave un-checked for all.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bciPreferredOnly.SuperTip = superToolTip1;
            this.bciPreferredOnly.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciPreferredOnly_CheckedChanged);
            // 
            // barEditItemSearchRadius
            // 
            this.barEditItemSearchRadius.Caption = "Search Radius:";
            this.barEditItemSearchRadius.Edit = this.repositoryItemPopupContainerEditSearchRadius;
            this.barEditItemSearchRadius.EditValue = "Disabled";
            this.barEditItemSearchRadius.EditWidth = 135;
            this.barEditItemSearchRadius.Id = 30;
            this.barEditItemSearchRadius.Name = "barEditItemSearchRadius";
            this.barEditItemSearchRadius.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // repositoryItemPopupContainerEditSearchRadius
            // 
            this.repositoryItemPopupContainerEditSearchRadius.AutoHeight = false;
            this.repositoryItemPopupContainerEditSearchRadius.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditSearchRadius.Name = "repositoryItemPopupContainerEditSearchRadius";
            this.repositoryItemPopupContainerEditSearchRadius.PopupControl = this.popupContainerControlSearchRadius;
            this.repositoryItemPopupContainerEditSearchRadius.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEditSearchRadius.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditSearchRadius_QueryResultValue);
            // 
            // popupContainerControlSearchRadius
            // 
            this.popupContainerControlSearchRadius.Controls.Add(this.btnOKSearchRadius);
            this.popupContainerControlSearchRadius.Controls.Add(this.radiusLabel);
            this.popupContainerControlSearchRadius.Controls.Add(this.trackBarControlRadius);
            this.popupContainerControlSearchRadius.Location = new System.Drawing.Point(11, 113);
            this.popupContainerControlSearchRadius.Name = "popupContainerControlSearchRadius";
            this.popupContainerControlSearchRadius.Size = new System.Drawing.Size(564, 74);
            this.popupContainerControlSearchRadius.TabIndex = 18;
            // 
            // btnOKSearchRadius
            // 
            this.btnOKSearchRadius.Location = new System.Drawing.Point(489, 45);
            this.btnOKSearchRadius.Name = "btnOKSearchRadius";
            this.btnOKSearchRadius.Size = new System.Drawing.Size(69, 23);
            this.btnOKSearchRadius.TabIndex = 18;
            this.btnOKSearchRadius.Text = "OK";
            this.btnOKSearchRadius.Click += new System.EventHandler(this.btnOKSearchRadius_Click);
            // 
            // radiusLabel
            // 
            this.radiusLabel.Location = new System.Drawing.Point(182, 45);
            this.radiusLabel.Name = "radiusLabel";
            this.radiusLabel.Size = new System.Drawing.Size(201, 13);
            this.radiusLabel.TabIndex = 17;
            this.radiusLabel.Text = "Postcode: XXXX - Seach Radius: XX miles. ";
            // 
            // trackBarControlRadius
            // 
            this.trackBarControlRadius.EditValue = null;
            this.trackBarControlRadius.Location = new System.Drawing.Point(3, 3);
            this.trackBarControlRadius.MenuManager = this.barManager1;
            this.trackBarControlRadius.Name = "trackBarControlRadius";
            this.trackBarControlRadius.Properties.Maximum = 250;
            this.trackBarControlRadius.Properties.ShowValueToolTip = true;
            this.trackBarControlRadius.Properties.TickFrequency = 5;
            this.trackBarControlRadius.Size = new System.Drawing.Size(558, 45);
            this.trackBarControlRadius.TabIndex = 16;
            this.trackBarControlRadius.ValueChanged += new System.EventHandler(this.trackBarControlRadius_ValueChanged);
            this.trackBarControlRadius.EditValueChanged += new System.EventHandler(this.trackBarControlRadius_EditValueChanged);
            // 
            // barEditItemJobRequirements
            // 
            this.barEditItemJobRequirements.Caption = "Job Requirements:";
            this.barEditItemJobRequirements.Edit = this.repositoryItemPopupContainerEditJobRequirements;
            this.barEditItemJobRequirements.EditValue = "No Job Requirements";
            this.barEditItemJobRequirements.EditWidth = 131;
            this.barEditItemJobRequirements.Id = 31;
            this.barEditItemJobRequirements.Name = "barEditItemJobRequirements";
            this.barEditItemJobRequirements.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // repositoryItemPopupContainerEditJobRequirements
            // 
            this.repositoryItemPopupContainerEditJobRequirements.AutoHeight = false;
            this.repositoryItemPopupContainerEditJobRequirements.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditJobRequirements.Name = "repositoryItemPopupContainerEditJobRequirements";
            this.repositoryItemPopupContainerEditJobRequirements.PopupControl = this.popupContainerControlJobRequirements;
            this.repositoryItemPopupContainerEditJobRequirements.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEditJobRequirements.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditJobRequirements_QueryResultValue);
            // 
            // popupContainerControlJobRequirements
            // 
            this.popupContainerControlJobRequirements.Controls.Add(this.btnJobRequirementsOK);
            this.popupContainerControlJobRequirements.Controls.Add(this.gridControl3);
            this.popupContainerControlJobRequirements.Location = new System.Drawing.Point(12, 190);
            this.popupContainerControlJobRequirements.Name = "popupContainerControlJobRequirements";
            this.popupContainerControlJobRequirements.Size = new System.Drawing.Size(382, 197);
            this.popupContainerControlJobRequirements.TabIndex = 20;
            // 
            // btnJobRequirementsOK
            // 
            this.btnJobRequirementsOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnJobRequirementsOK.Location = new System.Drawing.Point(3, 174);
            this.btnJobRequirementsOK.Name = "btnJobRequirementsOK";
            this.btnJobRequirementsOK.Size = new System.Drawing.Size(50, 20);
            this.btnJobRequirementsOK.TabIndex = 12;
            this.btnJobRequirementsOK.Text = "OK";
            this.btnJobRequirementsOK.Click += new System.EventHandler(this.btnJobRequirementsOK_Click);
            // 
            // gridControl3
            // 
            this.gridControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl3.DataSource = this.sp06185OMSelectContractorJobSubTypeCompetenciesBindingSource;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl3.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl3.Location = new System.Drawing.Point(3, 3);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3});
            this.gridControl3.Size = new System.Drawing.Size(376, 169);
            this.gridControl3.TabIndex = 4;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp06185OMSelectContractorJobSubTypeCompetenciesBindingSource
            // 
            this.sp06185OMSelectContractorJobSubTypeCompetenciesBindingSource.DataMember = "sp06185_OM_Select_Contractor_Job_Sub_Type_Competencies";
            this.sp06185OMSelectContractorJobSubTypeCompetenciesBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // dataSet_OM_Job
            // 
            this.dataSet_OM_Job.DataSetName = "DataSet_OM_Job";
            this.dataSet_OM_Job.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colJobCompetencyID,
            this.colJobSubTypeID1,
            this.colQualificationTypeID,
            this.colRemarks4,
            this.colJobTypeID3,
            this.colJobSubTypeDescription,
            this.colJobTypeDescription,
            this.colCompetencyDescription,
            this.colRequirementLevelDescription,
            this.colQualificationSubType,
            this.colQualificationSubTypeID,
            this.colQualificationType});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRequirementLevelDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCompetencyDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colJobCompetencyID
            // 
            this.colJobCompetencyID.Caption = "Job Competency ID";
            this.colJobCompetencyID.FieldName = "JobCompetencyID";
            this.colJobCompetencyID.Name = "colJobCompetencyID";
            this.colJobCompetencyID.OptionsColumn.AllowEdit = false;
            this.colJobCompetencyID.OptionsColumn.AllowFocus = false;
            this.colJobCompetencyID.OptionsColumn.ReadOnly = true;
            this.colJobCompetencyID.Width = 115;
            // 
            // colJobSubTypeID1
            // 
            this.colJobSubTypeID1.Caption = "Job Sub-Type ID";
            this.colJobSubTypeID1.FieldName = "JobSubTypeID";
            this.colJobSubTypeID1.Name = "colJobSubTypeID1";
            this.colJobSubTypeID1.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeID1.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeID1.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeID1.Width = 101;
            // 
            // colQualificationTypeID
            // 
            this.colQualificationTypeID.Caption = "Qualification Type ID";
            this.colQualificationTypeID.FieldName = "QualificationTypeID";
            this.colQualificationTypeID.Name = "colQualificationTypeID";
            this.colQualificationTypeID.OptionsColumn.AllowEdit = false;
            this.colQualificationTypeID.OptionsColumn.AllowFocus = false;
            this.colQualificationTypeID.OptionsColumn.ReadOnly = true;
            this.colQualificationTypeID.Width = 121;
            // 
            // colRemarks4
            // 
            this.colRemarks4.Caption = "Remarks";
            this.colRemarks4.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colRemarks4.FieldName = "Remarks";
            this.colRemarks4.Name = "colRemarks4";
            this.colRemarks4.OptionsColumn.ReadOnly = true;
            this.colRemarks4.Visible = true;
            this.colRemarks4.VisibleIndex = 4;
            this.colRemarks4.Width = 63;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colJobTypeID3
            // 
            this.colJobTypeID3.Caption = "Job Type ID";
            this.colJobTypeID3.FieldName = "JobTypeID";
            this.colJobTypeID3.Name = "colJobTypeID3";
            this.colJobTypeID3.OptionsColumn.AllowEdit = false;
            this.colJobTypeID3.OptionsColumn.AllowFocus = false;
            this.colJobTypeID3.OptionsColumn.ReadOnly = true;
            this.colJobTypeID3.Width = 79;
            // 
            // colJobSubTypeDescription
            // 
            this.colJobSubTypeDescription.Caption = "Job Sub-Type Description";
            this.colJobSubTypeDescription.FieldName = "JobSubTypeDescription";
            this.colJobSubTypeDescription.Name = "colJobSubTypeDescription";
            this.colJobSubTypeDescription.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeDescription.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeDescription.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeDescription.Visible = true;
            this.colJobSubTypeDescription.VisibleIndex = 3;
            this.colJobSubTypeDescription.Width = 174;
            // 
            // colJobTypeDescription
            // 
            this.colJobTypeDescription.Caption = "Job Type Description";
            this.colJobTypeDescription.FieldName = "JobTypeDescription";
            this.colJobTypeDescription.Name = "colJobTypeDescription";
            this.colJobTypeDescription.OptionsColumn.AllowEdit = false;
            this.colJobTypeDescription.OptionsColumn.AllowFocus = false;
            this.colJobTypeDescription.OptionsColumn.ReadOnly = true;
            this.colJobTypeDescription.Visible = true;
            this.colJobTypeDescription.VisibleIndex = 2;
            this.colJobTypeDescription.Width = 161;
            // 
            // colCompetencyDescription
            // 
            this.colCompetencyDescription.Caption = "Competency Description";
            this.colCompetencyDescription.FieldName = "CompetencyDescription";
            this.colCompetencyDescription.Name = "colCompetencyDescription";
            this.colCompetencyDescription.OptionsColumn.AllowEdit = false;
            this.colCompetencyDescription.OptionsColumn.AllowFocus = false;
            this.colCompetencyDescription.OptionsColumn.ReadOnly = true;
            this.colCompetencyDescription.Visible = true;
            this.colCompetencyDescription.VisibleIndex = 0;
            this.colCompetencyDescription.Width = 229;
            // 
            // colRequirementLevelDescription
            // 
            this.colRequirementLevelDescription.Caption = "Requirement";
            this.colRequirementLevelDescription.FieldName = "RequirementLevelDescription";
            this.colRequirementLevelDescription.Name = "colRequirementLevelDescription";
            this.colRequirementLevelDescription.OptionsColumn.AllowEdit = false;
            this.colRequirementLevelDescription.OptionsColumn.AllowFocus = false;
            this.colRequirementLevelDescription.OptionsColumn.ReadOnly = true;
            this.colRequirementLevelDescription.Visible = true;
            this.colRequirementLevelDescription.VisibleIndex = 1;
            this.colRequirementLevelDescription.Width = 95;
            // 
            // colQualificationSubType
            // 
            this.colQualificationSubType.Caption = "Qualification Sub-Type";
            this.colQualificationSubType.FieldName = "QualificationSubType";
            this.colQualificationSubType.Name = "colQualificationSubType";
            this.colQualificationSubType.OptionsColumn.AllowEdit = false;
            this.colQualificationSubType.OptionsColumn.AllowFocus = false;
            this.colQualificationSubType.OptionsColumn.ReadOnly = true;
            this.colQualificationSubType.Width = 129;
            // 
            // colQualificationSubTypeID
            // 
            this.colQualificationSubTypeID.Caption = "Qualification Type";
            this.colQualificationSubTypeID.FieldName = "QualificationSubTypeID";
            this.colQualificationSubTypeID.Name = "colQualificationSubTypeID";
            this.colQualificationSubTypeID.OptionsColumn.AllowEdit = false;
            this.colQualificationSubTypeID.OptionsColumn.AllowFocus = false;
            this.colQualificationSubTypeID.OptionsColumn.ReadOnly = true;
            this.colQualificationSubTypeID.Width = 107;
            // 
            // colQualificationType
            // 
            this.colQualificationType.Caption = "Qualification Type ID";
            this.colQualificationType.FieldName = "QualificationType";
            this.colQualificationType.Name = "colQualificationType";
            this.colQualificationType.OptionsColumn.AllowEdit = false;
            this.colQualificationType.OptionsColumn.AllowFocus = false;
            this.colQualificationType.OptionsColumn.ReadOnly = true;
            this.colQualificationType.Width = 121;
            // 
            // bciShowWorkLoad
            // 
            this.bciShowWorkLoad.Caption = "Workload";
            this.bciShowWorkLoad.Id = 35;
            this.bciShowWorkLoad.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bciShowWorkLoad.ImageOptions.Image")));
            this.bciShowWorkLoad.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bciShowWorkLoad.ImageOptions.LargeImage")));
            this.bciShowWorkLoad.Name = "bciShowWorkLoad";
            this.bciShowWorkLoad.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Text = "Show \\ Hide Workload - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to Toggle on and off the display of linked Contractor Workload.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bciShowWorkLoad.SuperTip = superToolTip2;
            this.bciShowWorkLoad.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciShowWorkLoad_CheckedChanged);
            // 
            // bciAuthorisedWorkTypes
            // 
            this.bciAuthorisedWorkTypes.Caption = "Authorised Work Types";
            this.bciAuthorisedWorkTypes.Id = 36;
            this.bciAuthorisedWorkTypes.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bciAuthorisedWorkTypes.ImageOptions.Image")));
            this.bciAuthorisedWorkTypes.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bciAuthorisedWorkTypes.ImageOptions.LargeImage")));
            this.bciAuthorisedWorkTypes.Name = "bciAuthorisedWorkTypes";
            this.bciAuthorisedWorkTypes.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Show \\ Hide Authorised Work Types - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to Toggle on and off the display of linked Contractor Authorised Work Ty" +
    "pes.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bciAuthorisedWorkTypes.SuperTip = superToolTip3;
            this.bciAuthorisedWorkTypes.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciAuthorisedWorkTypes_CheckedChanged);
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 27);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.Controls.Add(this.gridControl1);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Available Contractors";
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel2.Controls.Add(this.splitContainerControlLinkedData);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "Workload for Selected Contractor(s)  [ Read Only]";
            this.splitContainerControl1.Size = new System.Drawing.Size(1080, 490);
            this.splitContainerControl1.SplitterPosition = 412;
            this.splitContainerControl1.TabIndex = 19;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // splitContainerControlLinkedData
            // 
            this.splitContainerControlLinkedData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControlLinkedData.Horizontal = false;
            this.splitContainerControlLinkedData.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControlLinkedData.Name = "splitContainerControlLinkedData";
            this.splitContainerControlLinkedData.Panel1.Controls.Add(this.popupContainerControlDateRange);
            this.splitContainerControlLinkedData.Panel1.Controls.Add(this.gridControl2);
            this.splitContainerControlLinkedData.Panel1.Controls.Add(this.standaloneBarDockControl1);
            this.splitContainerControlLinkedData.Panel1.Text = "Panel1";
            this.splitContainerControlLinkedData.Panel2.Controls.Add(this.gridControl4);
            this.splitContainerControlLinkedData.Panel2.Text = "Panel2";
            this.splitContainerControlLinkedData.Size = new System.Drawing.Size(658, 466);
            this.splitContainerControlLinkedData.SplitterPosition = 240;
            this.splitContainerControlLinkedData.TabIndex = 23;
            this.splitContainerControlLinkedData.Text = "splitContainerControl2";
            // 
            // popupContainerControlDateRange
            // 
            this.popupContainerControlDateRange.Controls.Add(this.groupControl1);
            this.popupContainerControlDateRange.Controls.Add(this.btnDateRangeOK);
            this.popupContainerControlDateRange.Location = new System.Drawing.Point(161, 67);
            this.popupContainerControlDateRange.Name = "popupContainerControlDateRange";
            this.popupContainerControlDateRange.Size = new System.Drawing.Size(200, 107);
            this.popupContainerControlDateRange.TabIndex = 21;
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.dateEditFromDate);
            this.groupControl1.Controls.Add(this.dateEditToDate);
            this.groupControl1.Location = new System.Drawing.Point(3, 5);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(194, 76);
            this.groupControl1.TabIndex = 20;
            this.groupControl1.Text = "Date Range  [Expected Start Date]";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(6, 53);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(16, 13);
            this.labelControl2.TabIndex = 13;
            this.labelControl2.Text = "To:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(6, 27);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(28, 13);
            this.labelControl1.TabIndex = 12;
            this.labelControl1.Text = "From:";
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(40, 24);
            this.dateEditFromDate.MenuManager = this.barManager1;
            this.dateEditFromDate.Name = "dateEditFromDate";
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditFromDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditFromDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullValuePrompt = "No Date";
            this.dateEditFromDate.Size = new System.Drawing.Size(149, 20);
            this.dateEditFromDate.TabIndex = 10;
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(40, 50);
            this.dateEditToDate.MenuManager = this.barManager1;
            this.dateEditToDate.Name = "dateEditToDate";
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditToDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditToDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullValuePrompt = "No Date";
            this.dateEditToDate.Size = new System.Drawing.Size(149, 20);
            this.dateEditToDate.TabIndex = 11;
            // 
            // btnDateRangeOK
            // 
            this.btnDateRangeOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDateRangeOK.Location = new System.Drawing.Point(3, 84);
            this.btnDateRangeOK.Name = "btnDateRangeOK";
            this.btnDateRangeOK.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnDateRangeOK.Size = new System.Drawing.Size(50, 20);
            this.btnDateRangeOK.TabIndex = 19;
            this.btnDateRangeOK.Text = "OK";
            this.btnDateRangeOK.Click += new System.EventHandler(this.btnDateRangeOK_Click);
            // 
            // gridControl2
            // 
            this.gridControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl2.DataSource = this.sp06186OMContractorWorkLoadBindingSource;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.Location = new System.Drawing.Point(0, 26);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit2,
            this.repositoryItemMemoExEdit2,
            this.repositoryItemTextEditDateTime2,
            this.repositoryItemTextEditNumeric2DP});
            this.gridControl2.Size = new System.Drawing.Size(660, 214);
            this.gridControl2.TabIndex = 5;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp06186OMContractorWorkLoadBindingSource
            // 
            this.sp06186OMContractorWorkLoadBindingSource.DataMember = "sp06186_OM_Contractor_Work_Load";
            this.sp06186OMContractorWorkLoadBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLabourUsedID,
            this.colContractorID1,
            this.colContractorName1,
            this.colJobID,
            this.colVisitID,
            this.colVisitNumber,
            this.colJobSubTypeID,
            this.colReactive,
            this.colJobNoLongerRequired,
            this.colRequiresAccessPermit,
            this.colExpectedStartDate,
            this.colExpectedEndDate,
            this.colExpectedDurationUnits,
            this.colScheduleSentDate,
            this.colRemarks1,
            this.colJobStatusDescription,
            this.colSiteName,
            this.colClientName,
            this.colCreatedByStaffName,
            this.colExpectedDurationUnitsDescriptor,
            this.colJobTypeID,
            this.colJobSubTypeDescription1,
            this.colJobTypeDescription1,
            this.colClientNameContractDescription,
            this.colMaximumDaysFromLastVisit,
            this.colMinimumDaysFromLastVisit,
            this.colDaysLeeway,
            this.colContractDescription,
            this.colRecordType});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupCount = 1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFind.FindDelay = 2000;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsLayout.StoreFormatRules = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colContractorName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colExpectedStartDate, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colExpectedEndDate, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView2_SelectionChanged);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            // 
            // colLabourUsedID
            // 
            this.colLabourUsedID.Caption = "Labour Used ID";
            this.colLabourUsedID.FieldName = "LabourUsedID";
            this.colLabourUsedID.Name = "colLabourUsedID";
            this.colLabourUsedID.OptionsColumn.AllowEdit = false;
            this.colLabourUsedID.OptionsColumn.AllowFocus = false;
            this.colLabourUsedID.OptionsColumn.ReadOnly = true;
            this.colLabourUsedID.Width = 96;
            // 
            // colContractorID1
            // 
            this.colContractorID1.Caption = "Contractor ID";
            this.colContractorID1.FieldName = "ContractorID";
            this.colContractorID1.Name = "colContractorID1";
            this.colContractorID1.OptionsColumn.AllowEdit = false;
            this.colContractorID1.OptionsColumn.AllowFocus = false;
            this.colContractorID1.OptionsColumn.ReadOnly = true;
            this.colContractorID1.Width = 88;
            // 
            // colContractorName1
            // 
            this.colContractorName1.Caption = "Contractor Name";
            this.colContractorName1.FieldName = "ContractorName";
            this.colContractorName1.Name = "colContractorName1";
            this.colContractorName1.OptionsColumn.AllowEdit = false;
            this.colContractorName1.OptionsColumn.AllowFocus = false;
            this.colContractorName1.OptionsColumn.ReadOnly = true;
            this.colContractorName1.Visible = true;
            this.colContractorName1.VisibleIndex = 0;
            this.colContractorName1.Width = 280;
            // 
            // colJobID
            // 
            this.colJobID.Caption = "Job ID";
            this.colJobID.FieldName = "JobID";
            this.colJobID.Name = "colJobID";
            this.colJobID.OptionsColumn.AllowEdit = false;
            this.colJobID.OptionsColumn.AllowFocus = false;
            this.colJobID.OptionsColumn.ReadOnly = true;
            // 
            // colVisitID
            // 
            this.colVisitID.Caption = "Visit ID";
            this.colVisitID.FieldName = "VisitID";
            this.colVisitID.Name = "colVisitID";
            this.colVisitID.OptionsColumn.AllowEdit = false;
            this.colVisitID.OptionsColumn.AllowFocus = false;
            this.colVisitID.OptionsColumn.ReadOnly = true;
            // 
            // colVisitNumber
            // 
            this.colVisitNumber.Caption = "Visit #";
            this.colVisitNumber.FieldName = "VisitNumber";
            this.colVisitNumber.Name = "colVisitNumber";
            this.colVisitNumber.OptionsColumn.AllowEdit = false;
            this.colVisitNumber.OptionsColumn.AllowFocus = false;
            this.colVisitNumber.OptionsColumn.ReadOnly = true;
            this.colVisitNumber.Visible = true;
            this.colVisitNumber.VisibleIndex = 7;
            this.colVisitNumber.Width = 52;
            // 
            // colJobSubTypeID
            // 
            this.colJobSubTypeID.Caption = "Job Sub-Type ID";
            this.colJobSubTypeID.FieldName = "JobSubTypeID";
            this.colJobSubTypeID.Name = "colJobSubTypeID";
            this.colJobSubTypeID.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeID.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeID.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeID.Width = 102;
            // 
            // colReactive
            // 
            this.colReactive.Caption = "Reactive";
            this.colReactive.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colReactive.FieldName = "Reactive";
            this.colReactive.Name = "colReactive";
            this.colReactive.OptionsColumn.AllowEdit = false;
            this.colReactive.OptionsColumn.AllowFocus = false;
            this.colReactive.OptionsColumn.ReadOnly = true;
            this.colReactive.Width = 64;
            // 
            // colJobNoLongerRequired
            // 
            this.colJobNoLongerRequired.Caption = "No Longer Required";
            this.colJobNoLongerRequired.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colJobNoLongerRequired.FieldName = "JobNoLongerRequired";
            this.colJobNoLongerRequired.Name = "colJobNoLongerRequired";
            this.colJobNoLongerRequired.OptionsColumn.AllowEdit = false;
            this.colJobNoLongerRequired.OptionsColumn.AllowFocus = false;
            this.colJobNoLongerRequired.OptionsColumn.ReadOnly = true;
            this.colJobNoLongerRequired.Visible = true;
            this.colJobNoLongerRequired.VisibleIndex = 9;
            this.colJobNoLongerRequired.Width = 117;
            // 
            // colRequiresAccessPermit
            // 
            this.colRequiresAccessPermit.Caption = "Acess Permit Required";
            this.colRequiresAccessPermit.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colRequiresAccessPermit.FieldName = "RequiresAccessPermit";
            this.colRequiresAccessPermit.Name = "colRequiresAccessPermit";
            this.colRequiresAccessPermit.OptionsColumn.AllowEdit = false;
            this.colRequiresAccessPermit.OptionsColumn.AllowFocus = false;
            this.colRequiresAccessPermit.OptionsColumn.ReadOnly = true;
            this.colRequiresAccessPermit.Visible = true;
            this.colRequiresAccessPermit.VisibleIndex = 10;
            this.colRequiresAccessPermit.Width = 129;
            // 
            // colExpectedStartDate
            // 
            this.colExpectedStartDate.Caption = "Expected Start";
            this.colExpectedStartDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colExpectedStartDate.FieldName = "ExpectedStartDate";
            this.colExpectedStartDate.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colExpectedStartDate.Name = "colExpectedStartDate";
            this.colExpectedStartDate.OptionsColumn.AllowEdit = false;
            this.colExpectedStartDate.OptionsColumn.AllowFocus = false;
            this.colExpectedStartDate.OptionsColumn.ReadOnly = true;
            this.colExpectedStartDate.Visible = true;
            this.colExpectedStartDate.VisibleIndex = 0;
            this.colExpectedStartDate.Width = 120;
            // 
            // repositoryItemTextEditDateTime2
            // 
            this.repositoryItemTextEditDateTime2.Appearance.Options.UseTextOptions = true;
            this.repositoryItemTextEditDateTime2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.repositoryItemTextEditDateTime2.AutoHeight = false;
            this.repositoryItemTextEditDateTime2.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime2.Name = "repositoryItemTextEditDateTime2";
            // 
            // colExpectedEndDate
            // 
            this.colExpectedEndDate.Caption = "Expected End";
            this.colExpectedEndDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colExpectedEndDate.FieldName = "ExpectedEndDate";
            this.colExpectedEndDate.Name = "colExpectedEndDate";
            this.colExpectedEndDate.OptionsColumn.AllowEdit = false;
            this.colExpectedEndDate.OptionsColumn.AllowFocus = false;
            this.colExpectedEndDate.OptionsColumn.ReadOnly = true;
            this.colExpectedEndDate.Visible = true;
            this.colExpectedEndDate.VisibleIndex = 1;
            this.colExpectedEndDate.Width = 101;
            // 
            // colExpectedDurationUnits
            // 
            this.colExpectedDurationUnits.Caption = "Duration Units";
            this.colExpectedDurationUnits.ColumnEdit = this.repositoryItemTextEditNumeric2DP;
            this.colExpectedDurationUnits.FieldName = "ExpectedDurationUnits";
            this.colExpectedDurationUnits.Name = "colExpectedDurationUnits";
            this.colExpectedDurationUnits.OptionsColumn.AllowEdit = false;
            this.colExpectedDurationUnits.OptionsColumn.AllowFocus = false;
            this.colExpectedDurationUnits.OptionsColumn.ReadOnly = true;
            this.colExpectedDurationUnits.Width = 90;
            // 
            // repositoryItemTextEditNumeric2DP
            // 
            this.repositoryItemTextEditNumeric2DP.AutoHeight = false;
            this.repositoryItemTextEditNumeric2DP.Mask.EditMask = "n2";
            this.repositoryItemTextEditNumeric2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumeric2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumeric2DP.Name = "repositoryItemTextEditNumeric2DP";
            // 
            // colScheduleSentDate
            // 
            this.colScheduleSentDate.Caption = "Schedule Sent";
            this.colScheduleSentDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colScheduleSentDate.FieldName = "ScheduleSentDate";
            this.colScheduleSentDate.Name = "colScheduleSentDate";
            this.colScheduleSentDate.OptionsColumn.AllowEdit = false;
            this.colScheduleSentDate.OptionsColumn.AllowFocus = false;
            this.colScheduleSentDate.OptionsColumn.ReadOnly = true;
            this.colScheduleSentDate.Visible = true;
            this.colScheduleSentDate.VisibleIndex = 11;
            this.colScheduleSentDate.Width = 90;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 15;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colJobStatusDescription
            // 
            this.colJobStatusDescription.Caption = "Job Status";
            this.colJobStatusDescription.FieldName = "JobStatusDescription";
            this.colJobStatusDescription.Name = "colJobStatusDescription";
            this.colJobStatusDescription.OptionsColumn.AllowEdit = false;
            this.colJobStatusDescription.OptionsColumn.AllowFocus = false;
            this.colJobStatusDescription.OptionsColumn.ReadOnly = true;
            this.colJobStatusDescription.Visible = true;
            this.colJobStatusDescription.VisibleIndex = 8;
            this.colJobStatusDescription.Width = 105;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.Visible = true;
            this.colSiteName.VisibleIndex = 6;
            this.colSiteName.Width = 120;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 4;
            this.colClientName.Width = 129;
            // 
            // colCreatedByStaffName
            // 
            this.colCreatedByStaffName.Caption = "Created By";
            this.colCreatedByStaffName.FieldName = "CreatedByStaffName";
            this.colCreatedByStaffName.Name = "colCreatedByStaffName";
            this.colCreatedByStaffName.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffName.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffName.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffName.Width = 136;
            // 
            // colExpectedDurationUnitsDescriptor
            // 
            this.colExpectedDurationUnitsDescriptor.Caption = "Expected Duration Descriptor";
            this.colExpectedDurationUnitsDescriptor.FieldName = "ExpectedDurationUnitsDescriptor";
            this.colExpectedDurationUnitsDescriptor.Name = "colExpectedDurationUnitsDescriptor";
            this.colExpectedDurationUnitsDescriptor.OptionsColumn.AllowEdit = false;
            this.colExpectedDurationUnitsDescriptor.OptionsColumn.AllowFocus = false;
            this.colExpectedDurationUnitsDescriptor.OptionsColumn.ReadOnly = true;
            this.colExpectedDurationUnitsDescriptor.Width = 163;
            // 
            // colJobTypeID
            // 
            this.colJobTypeID.Caption = "Job Type ID";
            this.colJobTypeID.FieldName = "JobTypeID";
            this.colJobTypeID.Name = "colJobTypeID";
            this.colJobTypeID.OptionsColumn.AllowEdit = false;
            this.colJobTypeID.OptionsColumn.AllowFocus = false;
            this.colJobTypeID.OptionsColumn.ReadOnly = true;
            // 
            // colJobSubTypeDescription1
            // 
            this.colJobSubTypeDescription1.Caption = "Job Sub-Type";
            this.colJobSubTypeDescription1.FieldName = "JobSubTypeDescription";
            this.colJobSubTypeDescription1.Name = "colJobSubTypeDescription1";
            this.colJobSubTypeDescription1.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeDescription1.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeDescription1.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeDescription1.Visible = true;
            this.colJobSubTypeDescription1.VisibleIndex = 3;
            this.colJobSubTypeDescription1.Width = 140;
            // 
            // colJobTypeDescription1
            // 
            this.colJobTypeDescription1.Caption = "Job Type";
            this.colJobTypeDescription1.FieldName = "JobTypeDescription";
            this.colJobTypeDescription1.Name = "colJobTypeDescription1";
            this.colJobTypeDescription1.OptionsColumn.AllowEdit = false;
            this.colJobTypeDescription1.OptionsColumn.AllowFocus = false;
            this.colJobTypeDescription1.OptionsColumn.ReadOnly = true;
            this.colJobTypeDescription1.Visible = true;
            this.colJobTypeDescription1.VisibleIndex = 2;
            this.colJobTypeDescription1.Width = 129;
            // 
            // colClientNameContractDescription
            // 
            this.colClientNameContractDescription.Caption = "Client Name \\ Contract";
            this.colClientNameContractDescription.FieldName = "ClientNameContractDescription";
            this.colClientNameContractDescription.Name = "colClientNameContractDescription";
            this.colClientNameContractDescription.OptionsColumn.AllowEdit = false;
            this.colClientNameContractDescription.OptionsColumn.AllowFocus = false;
            this.colClientNameContractDescription.OptionsColumn.ReadOnly = true;
            this.colClientNameContractDescription.Width = 131;
            // 
            // colMaximumDaysFromLastVisit
            // 
            this.colMaximumDaysFromLastVisit.Caption = "Max Days from Last Visit";
            this.colMaximumDaysFromLastVisit.FieldName = "MaximumDaysFromLastVisit";
            this.colMaximumDaysFromLastVisit.Name = "colMaximumDaysFromLastVisit";
            this.colMaximumDaysFromLastVisit.OptionsColumn.AllowEdit = false;
            this.colMaximumDaysFromLastVisit.OptionsColumn.AllowFocus = false;
            this.colMaximumDaysFromLastVisit.OptionsColumn.ReadOnly = true;
            this.colMaximumDaysFromLastVisit.Visible = true;
            this.colMaximumDaysFromLastVisit.VisibleIndex = 12;
            this.colMaximumDaysFromLastVisit.Width = 139;
            // 
            // colMinimumDaysFromLastVisit
            // 
            this.colMinimumDaysFromLastVisit.Caption = "Min Days from Last Visit";
            this.colMinimumDaysFromLastVisit.FieldName = "MinimumDaysFromLastVisit";
            this.colMinimumDaysFromLastVisit.Name = "colMinimumDaysFromLastVisit";
            this.colMinimumDaysFromLastVisit.OptionsColumn.AllowEdit = false;
            this.colMinimumDaysFromLastVisit.OptionsColumn.AllowFocus = false;
            this.colMinimumDaysFromLastVisit.OptionsColumn.ReadOnly = true;
            this.colMinimumDaysFromLastVisit.Visible = true;
            this.colMinimumDaysFromLastVisit.VisibleIndex = 13;
            this.colMinimumDaysFromLastVisit.Width = 135;
            // 
            // colDaysLeeway
            // 
            this.colDaysLeeway.Caption = "Days Leeway";
            this.colDaysLeeway.FieldName = "DaysLeeway";
            this.colDaysLeeway.Name = "colDaysLeeway";
            this.colDaysLeeway.OptionsColumn.AllowEdit = false;
            this.colDaysLeeway.OptionsColumn.AllowFocus = false;
            this.colDaysLeeway.OptionsColumn.ReadOnly = true;
            this.colDaysLeeway.Visible = true;
            this.colDaysLeeway.VisibleIndex = 14;
            this.colDaysLeeway.Width = 86;
            // 
            // colContractDescription
            // 
            this.colContractDescription.Caption = "Contract Description";
            this.colContractDescription.FieldName = "ContractDescription";
            this.colContractDescription.Name = "colContractDescription";
            this.colContractDescription.OptionsColumn.AllowEdit = false;
            this.colContractDescription.OptionsColumn.AllowFocus = false;
            this.colContractDescription.OptionsColumn.ReadOnly = true;
            this.colContractDescription.Visible = true;
            this.colContractDescription.VisibleIndex = 5;
            this.colContractDescription.Width = 142;
            // 
            // colRecordType
            // 
            this.colRecordType.Caption = "Record Type";
            this.colRecordType.FieldName = "RecordType";
            this.colRecordType.Name = "colRecordType";
            this.colRecordType.OptionsColumn.AllowEdit = false;
            this.colRecordType.OptionsColumn.AllowFocus = false;
            this.colRecordType.OptionsColumn.ReadOnly = true;
            this.colRecordType.Width = 83;
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl1.Manager = this.barManager1;
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(660, 24);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // gridControl4
            // 
            this.gridControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl4.DataSource = this.sp09119HRQualificationsForContractorBindingSource;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl4.Location = new System.Drawing.Point(0, 0);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit3,
            this.repositoryItemMemoExEdit4,
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit2});
            this.gridControl4.Size = new System.Drawing.Size(657, 219);
            this.gridControl4.TabIndex = 21;
            this.gridControl4.UseEmbeddedNavigator = true;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp09119HRQualificationsForContractorBindingSource
            // 
            this.sp09119HRQualificationsForContractorBindingSource.DataMember = "sp09119_HR_Qualifications_For_Contractor";
            this.sp09119HRQualificationsForContractorBindingSource.DataSource = this.dataSet_Common_Functionality;
            // 
            // dataSet_Common_Functionality
            // 
            this.dataSet_Common_Functionality.DataSetName = "DataSet_Common_Functionality";
            this.dataSet_Common_Functionality.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colQualificationID,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn1,
            this.colQualificationFromID,
            this.colCourseNumber,
            this.colCourseName,
            this.colCostToCompany,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn7,
            this.colGUID,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn2,
            this.colQualificationFrom,
            this.colDaysUntilExpiry,
            this.colLinkedDocumentCount7,
            this.gridColumn3,
            this.gridColumn4,
            this.colArchived,
            this.colAssessmentDate,
            this.colCourseEndDate,
            this.colCourseStartDate});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.GroupCount = 1;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsLayout.StoreFormatRules = true;
            this.gridView4.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.MultiSelect = true;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn12, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDaysUntilExpiry, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn11, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView4.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView4_CustomDrawCell);
            this.gridView4.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView4_CustomRowCellEdit);
            this.gridView4.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView4.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView4_SelectionChanged);
            this.gridView4.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView4.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView4.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            // 
            // colQualificationID
            // 
            this.colQualificationID.Caption = "Qualification ID";
            this.colQualificationID.FieldName = "QualificationID";
            this.colQualificationID.Name = "colQualificationID";
            this.colQualificationID.OptionsColumn.AllowEdit = false;
            this.colQualificationID.OptionsColumn.AllowFocus = false;
            this.colQualificationID.OptionsColumn.ReadOnly = true;
            this.colQualificationID.Width = 94;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Linked To Person ID";
            this.gridColumn5.FieldName = "LinkedToPersonID";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Width = 116;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Linked To Person Type ID";
            this.gridColumn6.FieldName = "LinkedToPersonTypeID";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Width = 143;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Qualification Type ID";
            this.gridColumn1.FieldName = "QualificationTypeID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Width = 121;
            // 
            // colQualificationFromID
            // 
            this.colQualificationFromID.Caption = "Qualification From ID";
            this.colQualificationFromID.FieldName = "QualificationFromID";
            this.colQualificationFromID.Name = "colQualificationFromID";
            this.colQualificationFromID.OptionsColumn.AllowEdit = false;
            this.colQualificationFromID.OptionsColumn.AllowFocus = false;
            this.colQualificationFromID.OptionsColumn.ReadOnly = true;
            this.colQualificationFromID.Width = 121;
            // 
            // colCourseNumber
            // 
            this.colCourseNumber.Caption = "Course #";
            this.colCourseNumber.FieldName = "CourseNumber";
            this.colCourseNumber.Name = "colCourseNumber";
            this.colCourseNumber.OptionsColumn.AllowEdit = false;
            this.colCourseNumber.OptionsColumn.AllowFocus = false;
            this.colCourseNumber.OptionsColumn.ReadOnly = true;
            this.colCourseNumber.Visible = true;
            this.colCourseNumber.VisibleIndex = 11;
            // 
            // colCourseName
            // 
            this.colCourseName.Caption = "Course Name";
            this.colCourseName.FieldName = "CourseName";
            this.colCourseName.Name = "colCourseName";
            this.colCourseName.OptionsColumn.AllowEdit = false;
            this.colCourseName.OptionsColumn.AllowFocus = false;
            this.colCourseName.OptionsColumn.ReadOnly = true;
            this.colCourseName.Visible = true;
            this.colCourseName.VisibleIndex = 10;
            this.colCourseName.Width = 120;
            // 
            // colCostToCompany
            // 
            this.colCostToCompany.Caption = "Cost to Company";
            this.colCostToCompany.ColumnEdit = this.repositoryItemTextEdit2;
            this.colCostToCompany.FieldName = "CostToCompany";
            this.colCostToCompany.Name = "colCostToCompany";
            this.colCostToCompany.OptionsColumn.AllowEdit = false;
            this.colCostToCompany.OptionsColumn.AllowFocus = false;
            this.colCostToCompany.OptionsColumn.ReadOnly = true;
            this.colCostToCompany.Visible = true;
            this.colCostToCompany.VisibleIndex = 13;
            this.colCostToCompany.Width = 104;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Mask.EditMask = "c";
            this.repositoryItemTextEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Qualification Start";
            this.gridColumn8.ColumnEdit = this.repositoryItemTextEdit1;
            this.gridColumn8.FieldName = "StartDate";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 5;
            this.gridColumn8.Width = 107;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.Appearance.Options.UseTextOptions = true;
            this.repositoryItemTextEdit1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "d";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Qualification End";
            this.gridColumn9.ColumnEdit = this.repositoryItemTextEdit1;
            this.gridColumn9.FieldName = "EndDate";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 6;
            this.gridColumn9.Width = 101;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Qualification Expiry";
            this.gridColumn10.FieldName = "ExpiryDate";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 4;
            this.gridColumn10.Width = 113;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Remarks";
            this.gridColumn7.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.gridColumn7.FieldName = "Remarks";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 14;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // colGUID
            // 
            this.colGUID.Caption = "GUID";
            this.colGUID.FieldName = "GUID";
            this.colGUID.Name = "colGUID";
            this.colGUID.OptionsColumn.AllowEdit = false;
            this.colGUID.OptionsColumn.AllowFocus = false;
            this.colGUID.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Owner Type";
            this.gridColumn11.FieldName = "LinkedToPersonType";
            this.gridColumn11.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 0;
            this.gridColumn11.Width = 102;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Owner";
            this.gridColumn12.FieldName = "LinkedToPersonName";
            this.gridColumn12.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 1;
            this.gridColumn12.Width = 151;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Qualification Type";
            this.gridColumn2.FieldName = "QualificationType";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 133;
            // 
            // colQualificationFrom
            // 
            this.colQualificationFrom.Caption = "Qualification Source";
            this.colQualificationFrom.FieldName = "QualificationFrom";
            this.colQualificationFrom.Name = "colQualificationFrom";
            this.colQualificationFrom.OptionsColumn.AllowEdit = false;
            this.colQualificationFrom.OptionsColumn.AllowFocus = false;
            this.colQualificationFrom.OptionsColumn.ReadOnly = true;
            this.colQualificationFrom.Visible = true;
            this.colQualificationFrom.VisibleIndex = 12;
            this.colQualificationFrom.Width = 147;
            // 
            // colDaysUntilExpiry
            // 
            this.colDaysUntilExpiry.Caption = "Expires In";
            this.colDaysUntilExpiry.FieldName = "DaysUntilExpiry";
            this.colDaysUntilExpiry.Name = "colDaysUntilExpiry";
            this.colDaysUntilExpiry.OptionsColumn.AllowEdit = false;
            this.colDaysUntilExpiry.OptionsColumn.AllowFocus = false;
            this.colDaysUntilExpiry.OptionsColumn.ReadOnly = true;
            this.colDaysUntilExpiry.Visible = true;
            this.colDaysUntilExpiry.VisibleIndex = 3;
            this.colDaysUntilExpiry.Width = 82;
            // 
            // colLinkedDocumentCount7
            // 
            this.colLinkedDocumentCount7.Caption = "Linked Documents";
            this.colLinkedDocumentCount7.FieldName = "LinkedDocumentCount";
            this.colLinkedDocumentCount7.Name = "colLinkedDocumentCount7";
            this.colLinkedDocumentCount7.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentCount7.Visible = true;
            this.colLinkedDocumentCount7.VisibleIndex = 15;
            this.colLinkedDocumentCount7.Width = 107;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Qualification Sub Type";
            this.gridColumn3.FieldName = "QualificationSubType";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            this.gridColumn3.Width = 128;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Qualification Sub Type ID";
            this.gridColumn4.FieldName = "QualificationSubTypeID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Width = 142;
            // 
            // colArchived
            // 
            this.colArchived.Caption = "Archived";
            this.colArchived.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colArchived.FieldName = "Archived";
            this.colArchived.Name = "colArchived";
            this.colArchived.OptionsColumn.AllowEdit = false;
            this.colArchived.OptionsColumn.AllowFocus = false;
            this.colArchived.OptionsColumn.ReadOnly = true;
            this.colArchived.Visible = true;
            this.colArchived.VisibleIndex = 16;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Caption = "Check";
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = 1;
            this.repositoryItemCheckEdit3.ValueUnchecked = 0;
            // 
            // colAssessmentDate
            // 
            this.colAssessmentDate.Caption = "Assessment Date";
            this.colAssessmentDate.ColumnEdit = this.repositoryItemTextEdit1;
            this.colAssessmentDate.FieldName = "AssessmentDate";
            this.colAssessmentDate.Name = "colAssessmentDate";
            this.colAssessmentDate.OptionsColumn.AllowEdit = false;
            this.colAssessmentDate.OptionsColumn.AllowFocus = false;
            this.colAssessmentDate.OptionsColumn.ReadOnly = true;
            this.colAssessmentDate.Visible = true;
            this.colAssessmentDate.VisibleIndex = 9;
            this.colAssessmentDate.Width = 104;
            // 
            // colCourseEndDate
            // 
            this.colCourseEndDate.Caption = "Course End";
            this.colCourseEndDate.ColumnEdit = this.repositoryItemTextEdit1;
            this.colCourseEndDate.FieldName = "CourseEndDate";
            this.colCourseEndDate.Name = "colCourseEndDate";
            this.colCourseEndDate.OptionsColumn.AllowEdit = false;
            this.colCourseEndDate.OptionsColumn.AllowFocus = false;
            this.colCourseEndDate.OptionsColumn.ReadOnly = true;
            this.colCourseEndDate.Visible = true;
            this.colCourseEndDate.VisibleIndex = 8;
            this.colCourseEndDate.Width = 100;
            // 
            // colCourseStartDate
            // 
            this.colCourseStartDate.Caption = "Course Start";
            this.colCourseStartDate.ColumnEdit = this.repositoryItemTextEdit1;
            this.colCourseStartDate.FieldName = "CourseStartDate";
            this.colCourseStartDate.Name = "colCourseStartDate";
            this.colCourseStartDate.OptionsColumn.AllowEdit = false;
            this.colCourseStartDate.OptionsColumn.AllowFocus = false;
            this.colCourseStartDate.OptionsColumn.ReadOnly = true;
            this.colCourseStartDate.Visible = true;
            this.colCourseStartDate.VisibleIndex = 7;
            this.colCourseStartDate.Width = 100;
            // 
            // bar2
            // 
            this.bar2.BarName = "Custom 4";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar2.FloatLocation = new System.Drawing.Point(892, 230);
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemDateRange),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefreshWorkLoad)});
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar2.Text = "Custom 4";
            // 
            // barEditItemDateRange
            // 
            this.barEditItemDateRange.Caption = "Date Range:";
            this.barEditItemDateRange.Edit = this.repositoryItemPopupContainerEditDateRange;
            this.barEditItemDateRange.EditValue = "No Date Range Filter";
            this.barEditItemDateRange.EditWidth = 217;
            this.barEditItemDateRange.Id = 32;
            this.barEditItemDateRange.Name = "barEditItemDateRange";
            this.barEditItemDateRange.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // repositoryItemPopupContainerEditDateRange
            // 
            this.repositoryItemPopupContainerEditDateRange.AutoHeight = false;
            this.repositoryItemPopupContainerEditDateRange.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditDateRange.Name = "repositoryItemPopupContainerEditDateRange";
            this.repositoryItemPopupContainerEditDateRange.PopupControl = this.popupContainerControlDateRange;
            this.repositoryItemPopupContainerEditDateRange.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEditDateRange.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditDateRange_QueryResultValue);
            // 
            // bbiRefreshWorkLoad
            // 
            this.bbiRefreshWorkLoad.Caption = "Refresh";
            this.bbiRefreshWorkLoad.Id = 33;
            this.bbiRefreshWorkLoad.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRefreshWorkLoad.ImageOptions.Image")));
            this.bbiRefreshWorkLoad.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiRefreshWorkLoad.ImageOptions.LargeImage")));
            this.bbiRefreshWorkLoad.Name = "bbiRefreshWorkLoad";
            this.bbiRefreshWorkLoad.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefreshWorkLoad.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefreshWorkLoad_ItemClick);
            // 
            // sp06185_OM_Select_Contractor_Job_Sub_Type_CompetenciesTableAdapter
            // 
            this.sp06185_OM_Select_Contractor_Job_Sub_Type_CompetenciesTableAdapter.ClearBeforeFill = true;
            // 
            // sp06186_OM_Contractor_Work_LoadTableAdapter
            // 
            this.sp06186_OM_Contractor_Work_LoadTableAdapter.ClearBeforeFill = true;
            // 
            // bbiShowHide
            // 
            this.bbiShowHide.Caption = "barButtonItem1";
            this.bbiShowHide.Id = 34;
            this.bbiShowHide.Name = "bbiShowHide";
            // 
            // sp09119_HR_Qualifications_For_ContractorTableAdapter
            // 
            this.sp09119_HR_Qualifications_For_ContractorTableAdapter.ClearBeforeFill = true;
            // 
            // frm_OM_Select_Contractor
            // 
            this.ClientSize = new System.Drawing.Size(1080, 551);
            this.Controls.Add(this.popupContainerControlJobRequirements);
            this.Controls.Add(this.popupContainerControlSearchRadius);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.splitContainerControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_OM_Select_Contractor";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select Contractor";
            this.Load += new System.EventHandler(this.frm_OM_Select_Contractor_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.popupContainerControlSearchRadius, 0);
            this.Controls.SetChildIndex(this.popupContainerControlJobRequirements, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06100OMContractorSelectBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMiles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditSearchRadius)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlSearchRadius)).EndInit();
            this.popupContainerControlSearchRadius.ResumeLayout(false);
            this.popupContainerControlSearchRadius.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControlRadius.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControlRadius)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditJobRequirements)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlJobRequirements)).EndInit();
            this.popupContainerControlJobRequirements.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06185OMSelectContractorJobSubTypeCompetenciesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Job)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControlLinkedData)).EndInit();
            this.splitContainerControlLinkedData.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).EndInit();
            this.popupContainerControlDateRange.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06186OMContractorWorkLoadBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric2DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp09119HRQualificationsForContractorBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Common_Functionality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateRange)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private System.Windows.Forms.BindingSource sp06100OMContractorSelectBindingSource;
        private DataSet_OM_Contract dataSet_OM_Contract;
        private DataSet_OM_ContractTableAdapters.sp06100_OM_Contractor_SelectTableAdapter sp06100_OM_Contractor_SelectTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine2;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine3;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine4;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine5;
        private DevExpress.XtraGrid.Columns.GridColumn colPostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone1;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone2;
        private DevExpress.XtraGrid.Columns.GridColumn colMobileTelephone;
        private DevExpress.XtraGrid.Columns.GridColumn colVetReg;
        private DevExpress.XtraGrid.Columns.GridColumn colUser1;
        private DevExpress.XtraGrid.Columns.GridColumn colUser2;
        private DevExpress.XtraGrid.Columns.GridColumn colUser3;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colDisabled;
        private DevExpress.XtraGrid.Columns.GridColumn colInternalContractor;
        private DevExpress.XtraGrid.Columns.GridColumn colTextNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colIsWoodPlan;
        private DevExpress.XtraGrid.Columns.GridColumn colIsGritter;
        private DevExpress.XtraGrid.Columns.GridColumn colIsSnowClearer;
        private DevExpress.XtraGrid.Columns.GridColumn colIsVatRegistered;
        private DevExpress.XtraGrid.Columns.GridColumn colIsUtilityArbTeam;
        private DevExpress.XtraGrid.Columns.GridColumn colIsOperationsTeam;
        private DevExpress.XtraGrid.Columns.GridColumn colFinanceSystemCode;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourType;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarEditItem barEditItemSearchRadius;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditSearchRadius;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlSearchRadius;
        private DevExpress.XtraEditors.SimpleButton btnOKSearchRadius;
        private DevExpress.XtraEditors.LabelControl radiusLabel;
        private DevExpress.XtraEditors.TrackBarControl trackBarControlRadius;
        private DevExpress.XtraGrid.Columns.GridColumn colCalculatedDistance;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditMiles;
        public DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        public DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime2;
        private DevExpress.XtraBars.BarEditItem barEditItemJobRequirements;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditJobRequirements;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlJobRequirements;
        private DevExpress.XtraEditors.SimpleButton btnJobRequirementsOK;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarEditItem barEditItemDateRange;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditDateRange;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlDateRange;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraEditors.SimpleButton btnDateRangeOK;
        private System.Windows.Forms.BindingSource sp06185OMSelectContractorJobSubTypeCompetenciesBindingSource;
        private DataSet_OM_Job dataSet_OM_Job;
        private DataSet_OM_JobTableAdapters.sp06185_OM_Select_Contractor_Job_Sub_Type_CompetenciesTableAdapter sp06185_OM_Select_Contractor_Job_Sub_Type_CompetenciesTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colJobCompetencyID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks4;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeID3;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colCompetencyDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colRequirementLevelDescription;
        private System.Windows.Forms.BindingSource sp06186OMContractorWorkLoadBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourUsedID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorID1;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorName1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobID;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitID;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colReactive;
        private DevExpress.XtraGrid.Columns.GridColumn colJobNoLongerRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colRequiresAccessPermit;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedDurationUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colScheduleSentDate;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobStatusDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedDurationUnitsDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientNameContractDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colMaximumDaysFromLastVisit;
        private DevExpress.XtraGrid.Columns.GridColumn colMinimumDaysFromLastVisit;
        private DevExpress.XtraGrid.Columns.GridColumn colDaysLeeway;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordType;
        private DataSet_OM_JobTableAdapters.sp06186_OM_Contractor_Work_LoadTableAdapter sp06186_OM_Contractor_Work_LoadTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumeric2DP;
        private DevExpress.XtraBars.BarButtonItem bbiRefreshWorkLoad;
        private DevExpress.XtraBars.BarCheckItem bciShowWorkLoad;
        private DevExpress.XtraBars.BarButtonItem bbiShowHide;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationSubType;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationSubTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationType;
        private DevExpress.XtraGrid.Columns.GridColumn colActiveTeamMemberCount;
        private DevExpress.XtraBars.BarCheckItem bciAuthorisedWorkTypes;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControlLinkedData;
        public DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private System.Windows.Forms.BindingSource sp09119HRQualificationsForContractorBindingSource;
        private DataSet_Common_Functionality dataSet_Common_Functionality;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationFromID;
        private DevExpress.XtraGrid.Columns.GridColumn colCourseNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colCourseName;
        private DevExpress.XtraGrid.Columns.GridColumn colCostToCompany;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationFrom;
        private DevExpress.XtraGrid.Columns.GridColumn colDaysUntilExpiry;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentCount7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn colArchived;
        private DevExpress.XtraGrid.Columns.GridColumn colAssessmentDate;
        private DevExpress.XtraGrid.Columns.GridColumn colCourseEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colCourseStartDate;
        private DataSet_Common_FunctionalityTableAdapters.sp09119_HR_Qualifications_For_ContractorTableAdapter sp09119_HR_Qualifications_For_ContractorTableAdapter;
        private DevExpress.XtraBars.BarCheckItem bciPreferredOnly;
    }
}

﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Linq;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraEditors.Controls;
using DevExpress.Skins;
using DevExpress.XtraScheduler;
using DevExpress.Utils.Animation;  // Required by Transition Effects //
using DevExpress.XtraScheduler;  // Required by Recurrence //
using DevExpress.XtraScheduler.UI;
using DevExpress.XtraScheduler.Compatibility;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;
using WoodPlan5.Classes.Operations;

namespace WoodPlan5
{
    public partial class frm_OM_Tender_Visit_Wizard : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        private string strConnectionStringREADONLY = "";
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        bool internalRowFocusing;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //
        RepositoryItemTextEdit disabledTextCurrencyEditor;  // Used to conditionally disable editors in GridControl6 //

        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //
        private bool ibool_FormStillLoading = true;


        public int i_intPassedInTenderID = 0;
        public int i_intPassedInClientID = 0;
        public int i_intPassedInClientContractID = 0;
        public int i_intPassedInSiteID = 0;
        public int i_intPassedInSiteContractID = 0;
        public string i_strPassedInClientName = "";
        
        public string i_strPassedInClientContractDescription = "";
        public string i_strPassedInTenderRevisionNumber = "";
        public string i_strPassedInSiteName = "";
        public string i_strPassedInWorkDescription = "";
        public DateTime? i_dtPassedInWorkCompletedBy = null;

        public decimal i_decPassedInTenderLabourCost = (decimal)0.00;
        public decimal i_decPassedInTenderMaterialCost = (decimal)0.00;
        public decimal i_decPassedInTenderEquipmentCost = (decimal)0.00;
        public decimal i_decPassedInTenderTotalCost = (decimal)0.00;

        public decimal i_decPassedInTenderLabourSell = (decimal)0.00;
        public decimal i_decPassedInTenderMaterialSell = (decimal)0.00;
        public decimal i_decPassedInTenderEquipmentSell = (decimal)0.00;
        public decimal i_decPassedInTenderTotalSell = (decimal)0.00;

        // Following are passed through this screen (not used) to the Job Wizard for pre-selecting data //
        public int i_intPassedInJobSubTypeID = 0;
        public int i_intPassedInLabourTypeID = 0;
        public int i_intPassedInLabourID = 0;
        public string i_strPassedInLabourName = "";
        public double i_dbPassedInLabourLatitude = (double)0.00;
        public double i_dbPassedInLabourLongitude = (double)0.00;
        public string i_strPassedInLabourPostcode = "";

        private int i_intMaxVisitNumber = 0;
        private int i_intExistingVisitCount = 0;
        private decimal i_decSiteContractDaysSeparationPercent = (decimal)0.00;
        private int i_intExtraWorksID = 0;
        private int i_intDefaultVisitCategoryID = 0;

        private Utils.enmFocusedGrid _enmFocusedGrid = Utils.enmFocusedGrid.Visit;

        public frm_OM_Tender_Edit frmCaller = null;

        #endregion

        public frm_OM_Tender_Visit_Wizard()
        {
            InitializeComponent();
        }

        private void frm_OM_Tender_Visit_Wizard_Load(object sender, EventArgs e)
        {
            this.FormID = 500290;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            strConnectionStringREADONLY = GlobalSettings.ConnectionStringREADONLY;

            Set_Grid_Highlighter_Transparent(this.Controls);

            // Get Form Permissions (For Edit Templates Button) //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, 7008, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);  // 7008 //
            ProcessPermissionsForForm();

            xtraTabControl1.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;  // Hide Table Page headers so it doesn't look like a pageframe //
            xtraTabControl1.SelectedTabPage = this.xtraTabPageWelcome;
 
            try
            {
                sp06250_OM_Visit_Categories_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06250_OM_Visit_Categories_With_BlankTableAdapter.Fill(dataSet_OM_Visit.sp06250_OM_Visit_Categories_With_Blank, 0);

                sp06133_OM_Visit_Gap_Unit_DescriptorsTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06133_OM_Visit_Gap_Unit_DescriptorsTableAdapter.Fill(dataSet_OM_Visit.sp06133_OM_Visit_Gap_Unit_Descriptors, 0);

                sp06134_OM_Job_Calculation_Level_DescriptorsTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06134_OM_Job_Calculation_Level_DescriptorsTableAdapter.Fill(dataSet_OM_Visit.sp06134_OM_Job_Calculation_Level_Descriptors, 0);

                sp06308_OM_Visit_TypesTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06308_OM_Visit_TypesTableAdapter.Fill(dataSet_OM_Visit.sp06308_OM_Visit_Types, 0);

                sp06141_OM_Visit_Template_Headers_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06141_OM_Visit_Template_Headers_With_BlankTableAdapter.Fill(dataSet_OM_Visit.sp06141_OM_Visit_Template_Headers_With_Blank);

                sp06538_OM_Visit_Statuses_Not_Ready_And_Ready_No_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06538_OM_Visit_Statuses_Not_Ready_And_Ready_No_BlankTableAdapter.Fill(dataSet_OM_Tender.sp06538_OM_Visit_Statuses_Not_Ready_And_Ready_No_Blank);
            }
            catch (Exception) { }

            TenderDetailsLabelControl.Text = "<b>Tender ID: </b>" + i_intPassedInTenderID.ToString() + (string.IsNullOrWhiteSpace(i_strPassedInTenderRevisionNumber) ? "" : " \\ " + i_strPassedInTenderRevisionNumber)
                + "  <b>Client Name: </b>" + (string.IsNullOrWhiteSpace(i_strPassedInClientName) ? "" : i_strPassedInClientName)
                + "  <b>Contract: </b>" + (string.IsNullOrWhiteSpace(i_strPassedInClientContractDescription) ? "" : i_strPassedInClientContractDescription)
                + "  <b>Site Name: </b>" + (string.IsNullOrWhiteSpace(i_strPassedInSiteName) ? "" : i_strPassedInSiteName)
                + "  <b>Work: </b>" + (string.IsNullOrWhiteSpace(i_strPassedInWorkDescription) ? "" : i_strPassedInWorkDescription)
                + "  -  <b>Work Completed By: </b>" + ((i_dtPassedInWorkCompletedBy == null || i_dtPassedInWorkCompletedBy == DateTime.MinValue) ? "Not Specified" : "<color=red>" + i_dtPassedInWorkCompletedBy.ToString() + "</color>");
            TenderDetailsLabelControl2.Text = TenderDetailsLabelControl.Text;
            TenderDetailsLabelControl3.Text = TenderDetailsLabelControl.Text;

            textEditTenderLabourCost.EditValue = i_decPassedInTenderLabourCost;
            textEditTenderMaterialCost.EditValue = i_decPassedInTenderMaterialCost;
            textEditTenderEquipmentCost.EditValue = i_decPassedInTenderEquipmentCost;
            textEditTenderTotalCost.EditValue = i_decPassedInTenderTotalCost;
            textEditTenderLabourSell.EditValue = i_decPassedInTenderLabourSell;
            textEditTenderMaterialSell.EditValue = i_decPassedInTenderMaterialSell;
            textEditTenderEquipmentSell.EditValue = i_decPassedInTenderEquipmentSell;
            textEditTenderTotalSell.EditValue = i_decPassedInTenderTotalSell;

            dateEditStartDate.Properties.MinValue = new DateTime(DateTime.Now.Year, 1, 1);
            dateEditStartDate.DateTime = DateTime.Today;
            
            // Check if there are already visits set up for this tender. If yes, get the highest date and use for starting point for new visits. //
            try
            {
                DataSet_OM_TenderTableAdapters.QueriesTableAdapter GetLastDateTime = new DataSet_OM_TenderTableAdapters.QueriesTableAdapter();
                GetLastDateTime.ChangeConnectionString(strConnectionStringREADONLY);
                dateEditStartDate.DateTime = Convert.ToDateTime(GetLastDateTime.sp06540_OM_Tender_Wizard_Get_Highest_Last_Visit_Date(i_intPassedInTenderID));
            }
            catch (Exception) { }

            string strTenderID = i_intPassedInTenderID.ToString() + ",";
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                DataSet ds = new DataSet();
                using (var conn = new SqlConnection(strConnectionString))
                {
                    conn.Open();
                    using (var cmd = new SqlCommand())
                    {
                        cmd.CommandText = "sp06521_OM_Tender_Visit_Wizard_Site_Contract_Info";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("TenderIDs", strTenderID));
                        cmd.Connection = conn;
                        sda = new SqlDataAdapter(cmd);
                        sda.Fill(ds, "Table");
                    }
                    conn.Close();
                }
                if (ds.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    i_intMaxVisitNumber = Convert.ToInt32(dr["VisitNumberMax"]);
                    i_intExistingVisitCount = Convert.ToInt32(dr["VisitCount"]);
                    i_decSiteContractDaysSeparationPercent = Convert.ToDecimal(dr["SiteContractDaysSeparationPercent"]);
                    i_intExtraWorksID = Convert.ToInt32(dr["ExtraWorksID"]);
                    i_intDefaultVisitCategoryID = Convert.ToInt32(dr["DefaultVisitCategoryID"]);
                }
            }
            catch (Exception) { }

            sp06145_OM_Visit_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            gridControl6.ForceInitialize();

            xtraTabControl1.SelectedTabPage = this.xtraTabPageWelcome;
            btnWelcomeNext.Focus();

            timerWelcomePage.Start();  // allow wizard to move from the Welcome page after 2 seconds. //
            
            emptyEditor = new RepositoryItem();

            disabledTextCurrencyEditor = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            disabledTextCurrencyEditor.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            disabledTextCurrencyEditor.Mask.EditMask = "c";
            disabledTextCurrencyEditor.Mask.UseMaskAsDisplayFormat = true;
            disabledTextCurrencyEditor.ReadOnly = true;

            ibool_FormStillLoading = false;
        }
        private void timerWelcomePage_Tick(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep1;
            timerWelcomePage.Stop();
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            ConfigureFormAccordingToMode();
            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            switch (strFormMode)
            {
                case "add":
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    break;
                default:
                    break;
            }
            ibool_FormStillLoading = false;

            //Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            //Activate_Pages.Activate(xtraTabPageStep2.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //
        }


        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        //iBool_TemplateManager = sfpPermissions.blRead;
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            GridView view = null;
            int[] intRowHandles;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Visit:
                    view = (GridView)gridControl6.MainView;
                    intRowHandles = view.GetSelectedRows();
                     if (intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                   if (intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    break;
                default:
                    break;
            }
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            view = (GridView)gridControl6.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl6.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (strFormMode != "view" && intRowHandles.Length > 0);
            btnApplyVisitDaysSeparation.Enabled = (strFormMode != "view" && intRowHandles.Length > 0);
            btnCalculateVisitDaysSeparation.Enabled = (strFormMode != "view" && intRowHandles.Length > 0);

            VisitDurationSpinEdit.Enabled = (strFormMode != "view" && intRowHandles.Length > 0);
            VisitDescriptorComboBoxEdit.Enabled = (strFormMode != "view" && intRowHandles.Length > 0);
            VisitTimeEdit.Enabled = (strFormMode != "view" && intRowHandles.Length > 0);
            btnApplyVisitDuration.Enabled = (strFormMode != "view" && intRowHandles.Length > 0);
            string strValue = VisitDescriptorComboBoxEdit.EditValue.ToString();
            Set_Visit_Hours_Edit_Status(strValue);
        }

        private void frm_OM_Tender_Visit_Wizard_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

            }
        }

        private void frm_OM_Tender_Visit_Wizard_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
            {
                // Store last used screen settings for current user //
                //default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "CompanyFilter", i_str_selected_Company_ids);
                //default_screen_settings.SaveDefaultScreenSettings();
            }

            // Clear timer in the off chance it is still running //
            timerWelcomePage.Stop();
            timerWelcomePage = null;
        }


        private void xtraTabControl1_SelectedPageChanging(object sender, DevExpress.XtraTab.TabPageChangingEventArgs e)
        {
            // Remember to set each pages Tag property to the page number in the designer //
            if (Convert.ToInt32(e.Page.Tag) > Convert.ToInt32(e.PrevPage.Tag))
            {
                ((PushTransition)transitionManager1.Transitions[xtraTabControl1].TransitionType).Parameters.EffectOptions = DevExpress.Utils.Animation.PushEffectOptions.FromRight;
            }
            else
            {
                ((PushTransition)transitionManager1.Transitions[xtraTabControl1].TransitionType).Parameters.EffectOptions = DevExpress.Utils.Animation.PushEffectOptions.FromLeft;
            }
            transitionManager1.StartTransition(xtraTabControl1);
        }
        private void xtraTabControl1_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            // Remember to set each pages Tag property to the page number in the designer //
            transitionManager1.EndTransition();

            switch (xtraTabControl1.SelectedTabPage.Name)
            {
                case "xtraTabPagWelcome":
                    {
                        this.Text = "Tender Visit Wizard - Welcome";
                    }
                    break;
                case "xtraTabPageStep1":
                    {
                        this.Text = "Tender Visit Wizard - Step 1";
                    }
                    break;
                case "xtraTabPageFinish":
                    {
                        this.Text = "Tender Visit Wizard - Finish";
                    }
                    break;
            }
        }

        public override void PostViewClick(object sender, EventArgs e, int row)
        {
            GridView view = (GridView)sender;
            switch (view.GridControl.MainView.Name)
            {
                case "gridView2":
                    {
                        if (row == GridControl.InvalidRowHandle) return;
                        //Set_Selected_Site_Count_Label();
                    }
                    break;
                default:
                    break;
            }
        }


        #region Welcome Page

        private void btnWelcomeNext_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep1;
            GridView view = (GridView)gridControl6.MainView;
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //  
            //if (view.DataRowCount <= 0)
            //{
            //    btnAddVisits.PerformClick();
            //}         
        }

        #endregion


        #region Step 1 Page

        private void btnStep5Previous_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageWelcome;
        }
        private void btnStep5Next_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl6.MainView;
            view.PostEditor();
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //  
            if (view.DataRowCount <= 0)
            {
                XtraMessageBox.Show("No visit set up to create. Set up one or more visits using the preceeding step before proceeding.", "Create Visit(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            string strMessage = "";
            int intErrors = CheckRows(view);
            if (intErrors > 0)
            {
                strMessage = (intErrors == 1 ? "1 Row" : intErrors.ToString() + " Rows") + " with errors - correct the errors before proceeding.\n\nColumns with invalid values have warning icons in them.";
                XtraMessageBox.Show(strMessage, "Check Data", MessageBoxButtons.OK, MessageBoxIcon.Warning, DefaultBoolean.True);
                return;
            }

            if (Convert.ToDecimal(textEditCalculatedTotalCost.EditValue) > Convert.ToDecimal(textEditTenderTotalCost.EditValue)) strMessage = "The Calculated Total Cost price for the Visit(s) has exceeded the Tender Cost price.\n\n";
            if (Convert.ToDecimal(textEditCalculatedTotalSell.EditValue) > Convert.ToDecimal(textEditTenderTotalSell.EditValue)) strMessage += "The Calculated Total Sell price for the Visit(s) has exceeded the Tender Sell price.\n\n";
            if (!string.IsNullOrWhiteSpace(strMessage))
            {
                if (XtraMessageBox.Show(strMessage + "Are you sure you wish to proceed?", "Create Visits", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, DefaultBoolean.True) == DialogResult.No) return;

            }

            xtraTabControl1.SelectedTabPage = this.xtraTabPageFinish;
        }


        #region GridView6

        private void gridControl6_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView6_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            switch (e.Column.FieldName)
            {
                case "ExpectedStartDate":
                    {
                        if (Convert.ToDateTime(view.GetRowCellValue(e.RowHandle, "ExpectedStartDate")) < DateTime.Today)
                        {
                            e.Appearance.BackColor = Color.FromArgb(248, 196, 196);
                            e.Appearance.ForeColor = Color.FromArgb(198, 16, 61);
                        }
                    }
                    break;
                case "ExpectedEndDate":
                    {
                        if (i_dtPassedInWorkCompletedBy != null)
                        {
                            if (Convert.ToDateTime(view.GetRowCellValue(e.RowHandle, "ExpectedEndDate")) > i_dtPassedInWorkCompletedBy)
                            {
                                e.Appearance.BackColor = Color.FromArgb(248, 196, 196);
                                e.Appearance.ForeColor = Color.FromArgb(198, 16, 61);
                            }
                        }
                    }
                    break;
            }
        }

        private void gridView6_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Visits Available - Select Site Contracts in Step 2 of the Wizard");
        }

        private void gridView6_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "VisitLabourCost":
                case "VisitMaterialCost":
                case "VisitEquipmentCost":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "CostCalculationLevel")) > 0) e.RepositoryItem = disabledTextCurrencyEditor;  // Read Only //
                    }
                    break;
                case "VisitLabourSell":
                case "VisitMaterialSell":
                case "VisitEquipmentSell":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "SellCalculationLevel")) > 0) e.RepositoryItem = disabledTextCurrencyEditor;  // Read Only //
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView6_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Visit;
            SetMenuStatus();
        }

        private void gridView6_MouseDown(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_Standard(sender, e);
        }
        private void gridView6_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Visit;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView6_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void gridView6_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);
            SetMenuStatus();
            Set_Selected_Visits_Text();
        }
        private void Set_Selected_Visits_Text()
        {
            GridView view = (GridView)gridControl6.MainView;
            int intSelectedCount = view.GetSelectedRows().Length;
            groupControlBlockEditVisits.Text = "<b>Block Edit</b>  -  Use this panel to block edit selected Visits  -  <b>" + intSelectedCount.ToString() + (intSelectedCount == 1 ? " Visit" : " Visits") + " Selected</b>";
        }

        private void gridView6_ShowingEditor(object sender, CancelEventArgs e)
        {
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "VisitLabourCost":
                case "VisitMaterialCost":
                case "VisitEquipmentCost":
                    {
                        if (Convert.ToInt32(view.GetFocusedRowCellValue("CostCalculationLevel")) > 0) e.Cancel = true;
                    }
                    break;
                case "VisitLabourSell":
                case "VisitMaterialSell":
                case "VisitEquipmentSell":
                    {
                        if (Convert.ToInt32(view.GetFocusedRowCellValue("SellCalculationLevel")) > 0) e.Cancel = true;
                    }
                    break;
                default:
                    break;
            }

        }
       
        private void gridView6_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            // Code also in bntNext2_Click event to validate all the rows initially //
            GridView view = (GridView)sender;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            int intFocusedRow = view.FocusedRowHandle;
            DataRow dr = view.GetDataRow(intFocusedRow);
            if (dr == null) return;
            switch (view.FocusedColumn.Name)
            {
                case "colExpectedStartDate":
                case "colExpectedEndDate":
                    {
                        bool boolValidFromDate = true;
                        bool boolValidToDate = true;
                        DateTime? dtFromDate = null;
                        DateTime? dtToDate = null;
                        if (view.FocusedColumn.Name == "colExpectedStartDate")
                        {
                            if (e.Value != DBNull.Value) dtFromDate = Convert.ToDateTime(e.Value);
                            if (view.GetFocusedRowCellValue("ExpectedEndDate") != DBNull.Value) dtToDate = Convert.ToDateTime(view.GetFocusedRowCellValue("ExpectedEndDate"));
                        }
                        else
                        {
                            if (e.Value != DBNull.Value) dtToDate = Convert.ToDateTime(e.Value);
                            if (view.GetFocusedRowCellValue("ExpectedStartDate") != DBNull.Value) dtFromDate = Convert.ToDateTime(view.GetFocusedRowCellValue("ExpectedStartDate"));
                        }
                        if (dtFromDate == null || dtFromDate == DateTime.MinValue)
                        {
                            dr.SetColumnError("ExpectedStartDate", String.Format("{0} requires a value.", view.Columns["ExpectedStartDate"].Caption));
                            boolValidFromDate = false;
                        }
                        if (dtToDate == null || dtToDate == DateTime.MinValue)
                        {
                            dr.SetColumnError("ExpectedEndDate", String.Format("{0} requires a value.", view.Columns["ExpectedEndDate"].Caption));
                            boolValidToDate = false;
                        }
                        if (dtFromDate != null && dtToDate != null && dtFromDate > dtToDate)
                        {
                            dr.SetColumnError("ExpectedStartDate", String.Format("{0} should be less than {1}.", view.Columns["ExpectedStartDate"].Caption, view.Columns["ExpectedEndDate"].Caption));
                            boolValidFromDate = false;

                            dr.SetColumnError("ExpectedEndDate", String.Format("{0} should be greater than {1}.", view.Columns["ExpectedEndDate"].Caption, view.Columns["ExpectedStartDate"].Caption));
                            boolValidToDate = false;
                        }
                        if (boolValidFromDate) dr.SetColumnError("ExpectedStartDate", "");  // Clear Error Text //
                        if (boolValidToDate) dr.SetColumnError("ExpectedEndDate", "");  // Clear Error Text //
                    }
                    break;
                case "colVisitNumber":
                    {
                        bool boolValidVisitNumber = true;
                        int? intVisitNumber = null;
                        if (e.Value != DBNull.Value) intVisitNumber = Convert.ToInt32(e.Value);
                        if (intVisitNumber == null || intVisitNumber <= 0)
                        {
                            dr.SetColumnError("VisitNumber", String.Format("{0} requires a value.", view.Columns["VisitNumber"].Caption));
                            boolValidVisitNumber = false;
                            //dr.RowError = "Error(s) Present - correct before proceeding.";
                        }
                        if (boolValidVisitNumber) dr.SetColumnError("VisitNumber", "");  // Clear Error Text //
                    }
                    break;
                case "colVisitLabourCost":
                case "colVisitMaterialCost":
                case "colVisitEquipmentCost":
                case "colVisitLabourSell":
                case "colVisitMaterialSell":
                case "colVisitEquipmentSell":
                    {
                        int intVisitID = Convert.ToInt32(view.GetFocusedRowCellValue("VisitID"));
                        decimal decValue = (decimal)0.00;
                        if (e.Value != DBNull.Value) decValue = Convert.ToDecimal(e.Value);

                        // Update Row Total //
                        switch (view.FocusedColumn.Name)
                        {

                            case "colVisitLabourCost":
                                {
                                    dr["VisitCost"] = decValue + Convert.ToDecimal(dr["VisitMaterialCost"]) + Convert.ToDecimal(dr["VisitEquipmentCost"]);
                                }
                                break;
                            case "colVisitMaterialCost":
                                {
                                    dr["VisitCost"] = Convert.ToDecimal(dr["VisitLabourCost"]) + decValue + Convert.ToDecimal(dr["VisitEquipmentCost"]);
                                }
                                break;
                            case "colVisitEquipmentCost":
                                {
                                    dr["VisitCost"] = Convert.ToDecimal(dr["VisitLabourCost"]) + Convert.ToDecimal(dr["VisitMaterialCost"]) + decValue;
                                }
                                break;
                            case "colVisitLabourSell":
                                {
                                    dr["VisitSell"] = decValue + Convert.ToDecimal(dr["VisitMaterialSell"]) + Convert.ToDecimal(dr["VisitEquipmentSell"]);
                                }
                                break;
                            case "colVisitMaterialSell":
                                {
                                    dr["VisitSell"] = Convert.ToDecimal(dr["VisitLabourSell"]) + decValue + Convert.ToDecimal(dr["VisitEquipmentSell"]);
                                }
                                break;
                            case "colVisitEquipmentSell":
                                 {
                                    dr["VisitSell"] = Convert.ToDecimal(dr["VisitLabourSell"]) + Convert.ToDecimal(dr["VisitMaterialSell"]) + decValue;
                                }
                               break;
                        }
                        UpdateCalculatedTotals(view.FocusedColumn.Name, decValue, intVisitID);  // Update Calculated Totals at top of screen //
                    }
                    break;
                case "colCostCalculationLevel":
                    {
                        int intValue = 0;
                        if (e.Value != DBNull.Value) intValue = Convert.ToInt32(e.Value);
                        if (intValue > 0)
                        {
                            dr["VisitLabourCost"] = (decimal)0.00;
                            dr["VisitMaterialCost"] = (decimal)0.00;
                            dr["VisitEquipmentCost"] = (decimal)0.00;
                            dr["VisitCost"] = (decimal)0.00;
                            UpdateCalculatedTotalsRow();
                        }
                    }
                    break;
                case "colSellCalculationLevel":
                    {
                        int intValue = 0;
                        if (e.Value != DBNull.Value) intValue = Convert.ToInt32(e.Value);
                        if (intValue > 0)
                        {
                            dr["VisitLabourSell"] = (decimal)0.00;
                            dr["VisitMaterialSell"] = (decimal)0.00;
                            dr["VisitEquipmentSell"] = (decimal)0.00;
                            dr["VisitSell"] = (decimal)0.00;
                            UpdateCalculatedTotalsRow();
                        }
                    }
                    break;
            }
        }
        private void UpdateCalculatedTotals(string ColumnName, decimal Value, int intVisitID)
        {
            decimal decTotal = (decimal)0.00;
            switch (ColumnName)
            {
                case "colVisitLabourCost":
                    {
                        // Use LINQ to get total for column from datatable - had to add a reference to System.Data.DataSetExtensions for the project then a reference to link at the top of this form //
                        decTotal = dataSet_OM_Visit.sp06145_OM_Visit_Edit
                                    .Where(x => x.VisitID != intVisitID)
                                    .Sum(x => x.VisitLabourCost) + Value;
                        textEditCalculatedLabourCost.EditValue = decTotal;
                        textEditCalculatedTotalCost.EditValue = decTotal + Convert.ToDecimal(textEditCalculatedEquipmentCost.EditValue) + Convert.ToDecimal(textEditCalculatedMaterialCost.EditValue);
                    }
                    break;
                case "colVisitMaterialCost":
                    {
                        decTotal = dataSet_OM_Visit.sp06145_OM_Visit_Edit
                                    .Where(x => x.VisitID != intVisitID)
                                    .Sum(x => x.VisitMaterialCost) + Value;
                        textEditCalculatedMaterialCost.EditValue = decTotal;
                        textEditCalculatedTotalCost.EditValue = Convert.ToDecimal(textEditCalculatedLabourCost.EditValue) + Convert.ToDecimal(textEditCalculatedEquipmentCost.EditValue) + decTotal;
                    }
                    break;
                case "colVisitEquipmentCost":
                    {
                        decTotal = dataSet_OM_Visit.sp06145_OM_Visit_Edit
                                    .Where(x => x.VisitID != intVisitID)
                                    .Sum(x => x.VisitEquipmentCost) + Value;
                        textEditCalculatedEquipmentCost.EditValue = decTotal;
                        textEditCalculatedTotalCost.EditValue = Convert.ToDecimal(textEditCalculatedLabourCost.EditValue) + decTotal + Convert.ToDecimal(textEditCalculatedMaterialCost.EditValue);
                    }
                    break;
                case "colVisitLabourSell":
                    {
                        decTotal = dataSet_OM_Visit.sp06145_OM_Visit_Edit
                                    .Where(x => x.VisitID != intVisitID)
                                    .Sum(x => x.VisitLabourSell) + Value;
                        textEditCalculatedLabourSell.EditValue = decTotal;
                        textEditCalculatedTotalSell.EditValue = decTotal + Convert.ToDecimal(textEditCalculatedEquipmentSell.EditValue) + Convert.ToDecimal(textEditCalculatedMaterialSell.EditValue);
                    }
                    break;
                case "colVisitMaterialSell":
                    {
                        decTotal = dataSet_OM_Visit.sp06145_OM_Visit_Edit
                                    .Where(x => x.VisitID != intVisitID)
                                    .Sum(x => x.VisitMaterialSell) + Value;
                        textEditCalculatedMaterialSell.EditValue = decTotal;
                        textEditCalculatedTotalSell.EditValue = Convert.ToDecimal(textEditCalculatedLabourSell.EditValue) + Convert.ToDecimal(textEditCalculatedEquipmentSell.EditValue) + decTotal;
                    }
                    break;
                case "colVisitEquipmentSell":
                    {
                        decTotal = dataSet_OM_Visit.sp06145_OM_Visit_Edit
                                    .Where(x => x.VisitID != intVisitID)
                                    .Sum(x => x.VisitEquipmentSell) + Value;
                        textEditCalculatedEquipmentSell.EditValue = decTotal;
                        textEditCalculatedTotalSell.EditValue = Convert.ToDecimal(textEditCalculatedLabourSell.EditValue) + decTotal + Convert.ToDecimal(textEditCalculatedMaterialSell.EditValue);
                    }
                    break;
                default:
                    break;
            }
            SetCalculatedFieldsTextColour(ColumnName);
        }
        private void UpdateCalculatedTotalsRow()
        {
            UpdateCalculatedTotals("colVisitLabourCost", (decimal)0.00, 0);
            UpdateCalculatedTotals("colVisitMaterialCost", (decimal)0.00, 0);
            UpdateCalculatedTotals("colVisitEquipmentCost", (decimal)0.00, 0);
            UpdateCalculatedTotals("colVisitLabourSell", (decimal)0.00, 0);
            UpdateCalculatedTotals("colVisitMaterialSell", (decimal)0.00, 0);
            UpdateCalculatedTotals("colVisitEquipmentSell", (decimal)0.00, 0);
        }
        private void SetCalculatedFieldsTextColour(string ColumnName)
        {
            decimal decTenderValue = (decimal)0.00;
            decimal decCalculatedValue = (decimal)0.00;
            decimal decTotalTenderValue = (decimal)0.00;
            decimal decTotalCalculatedValue = (decimal)0.00;
            switch (ColumnName)
            {
                case "colVisitLabourCost":
                    {
                        decTenderValue = Convert.ToDecimal(textEditTenderLabourCost.EditValue);
                        decCalculatedValue = Convert.ToDecimal(textEditCalculatedLabourCost.EditValue);
                        textEditCalculatedLabourCost.ForeColor = (decTenderValue == decCalculatedValue ? Color.Black : decTenderValue > decCalculatedValue ? Color.Green : Color.Red);

                        decTotalTenderValue = Convert.ToDecimal(textEditTenderTotalCost.EditValue);
                        decTotalCalculatedValue = Convert.ToDecimal(textEditCalculatedTotalCost.EditValue);
                        textEditCalculatedTotalCost.ForeColor = (decTotalTenderValue == decTotalCalculatedValue ? Color.Black : decTotalTenderValue > decTotalCalculatedValue ? Color.Green : Color.Red);
                    }
                    break;
                case "colVisitMaterialCost":
                    {
                        decTenderValue = Convert.ToDecimal(textEditTenderMaterialCost.EditValue);
                        decCalculatedValue = Convert.ToDecimal(textEditCalculatedMaterialCost.EditValue);
                        textEditCalculatedMaterialCost.ForeColor = (decTenderValue == decCalculatedValue ? Color.Black : decTenderValue > decCalculatedValue ? Color.Green : Color.Red);

                        decTotalTenderValue = Convert.ToDecimal(textEditTenderTotalCost.EditValue);
                        decTotalCalculatedValue = Convert.ToDecimal(textEditCalculatedTotalCost.EditValue);
                        textEditCalculatedTotalCost.ForeColor = (decTotalTenderValue == decTotalCalculatedValue ? Color.Black : decTotalTenderValue >decTotalCalculatedValue ? Color.Green : Color.Red);
                    }
                    break;
                case "colVisitEquipmentCost":
                    {
                        decTenderValue = Convert.ToDecimal(textEditTenderEquipmentCost.EditValue);
                        decCalculatedValue = Convert.ToDecimal(textEditCalculatedEquipmentCost.EditValue);
                        textEditCalculatedEquipmentCost.ForeColor = (decTenderValue == decCalculatedValue ? Color.Black : decTenderValue > decCalculatedValue ? Color.Green : Color.Red);

                        decTotalTenderValue = Convert.ToDecimal(textEditTenderTotalCost.EditValue);
                        decTotalCalculatedValue = Convert.ToDecimal(textEditCalculatedTotalCost.EditValue);
                        textEditCalculatedTotalCost.ForeColor = (decTotalTenderValue == decTotalCalculatedValue ? Color.Black : decTotalTenderValue > decTotalCalculatedValue ? Color.Green : Color.Red);
                    }
                    break;
                case "colVisitLabourSell":
                    {
                        decTenderValue = Convert.ToDecimal(textEditTenderLabourSell.EditValue);
                        decCalculatedValue = Convert.ToDecimal(textEditCalculatedLabourSell.EditValue);
                        textEditCalculatedLabourSell.ForeColor = (decTenderValue == decCalculatedValue ? Color.Black : decTenderValue > decCalculatedValue ? Color.Green : Color.Red);

                        decTotalTenderValue = Convert.ToDecimal(textEditTenderTotalSell.EditValue);
                        decTotalCalculatedValue = Convert.ToDecimal(textEditCalculatedTotalSell.EditValue);
                        textEditCalculatedTotalSell.ForeColor = (decTotalTenderValue == decTotalCalculatedValue ? Color.Black : decTotalTenderValue > decTotalCalculatedValue ? Color.Green : Color.Red);
                    }
                    break;
                case "colVisitMaterialSell":
                    {
                        decTenderValue = Convert.ToDecimal(textEditTenderMaterialSell.EditValue);
                        decCalculatedValue = Convert.ToDecimal(textEditCalculatedMaterialSell.EditValue);
                        textEditCalculatedMaterialSell.ForeColor = (decTenderValue == decCalculatedValue ? Color.Black : decTenderValue > decCalculatedValue ? Color.Green : Color.Red);

                        decTotalTenderValue = Convert.ToDecimal(textEditTenderTotalSell.EditValue);
                        decTotalCalculatedValue = Convert.ToDecimal(textEditCalculatedTotalSell.EditValue);
                        textEditCalculatedTotalSell.ForeColor = (decTotalTenderValue == decTotalCalculatedValue ? Color.Black : decTotalTenderValue > decTotalCalculatedValue ? Color.Green : Color.Red);
                    }
                    break;
                case "colVisitEquipmentSell":
                    {
                        decTenderValue = Convert.ToDecimal(textEditTenderEquipmentSell.EditValue);
                        decCalculatedValue = Convert.ToDecimal(textEditCalculatedEquipmentSell.EditValue);
                        textEditCalculatedEquipmentSell.ForeColor = (decTenderValue == decCalculatedValue ? Color.Black : decTenderValue > decCalculatedValue ? Color.Green : Color.Red);

                        decTotalTenderValue = Convert.ToDecimal(textEditTenderTotalSell.EditValue);
                        decTotalCalculatedValue = Convert.ToDecimal(textEditCalculatedTotalSell.EditValue);
                        textEditCalculatedTotalSell.ForeColor = (decTotalTenderValue == decTotalCalculatedValue ? Color.Black : decTotalTenderValue > decTotalCalculatedValue ? Color.Green : Color.Red);
                    }
                    break;
            }
        }

        private void repositoryItemButtonEditClientPONumber_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var currentRowView = (DataRowView)sp06145OMVisitEditBindingSource.Current;
                var currentRow = (DataSet_OM_Visit.sp06145_OM_Visit_EditRow)currentRowView.Row;
                if (currentRow == null) return;
                int intClientContractID = 0;
                try { intClientContractID = (string.IsNullOrEmpty(currentRow.ClientContractID.ToString()) ? 0 : Convert.ToInt32(currentRow.ClientContractID)); }
                catch (Exception) { }

                int intSiteContractID = 0;
                try { intSiteContractID = (string.IsNullOrEmpty(currentRow.SiteContractID.ToString()) ? 0 : Convert.ToInt32(currentRow.SiteContractID)); }
                catch (Exception) { }

                int intClientPOID = 0;
                try { intClientPOID = (string.IsNullOrEmpty(currentRow.ClientPOID.ToString()) ? 0 : Convert.ToInt32(currentRow.ClientPOID)); }
                catch (Exception) { }

                var fChildForm = new frm_OM_Select_Client_PO_Multi_Page();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.boolPage1Enabled = true;
                fChildForm.boolPage2Enabled = true;
                fChildForm.boolPage3Enabled = false; // Disable Visit page //
                fChildForm.intActivePage = 2;
                fChildForm._PassedInClientContractIDs = intClientContractID.ToString() + ",";
                fChildForm._PassedInSiteContractIDs = intSiteContractID.ToString() + ",";
                fChildForm._PassedInVisitIDs = "";
                fChildForm.intOriginalPOID = intClientPOID;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow.ClientPOID = fChildForm.intSelectedPOID;
                    currentRow.ClientPONumber = fChildForm.strSelectedPONumbers;
                    sp06145OMVisitEditBindingSource.EndEdit();
                }
            }
            else if (e.Button.Tag.ToString() == "clear")
            {
                var currentRowView = (DataRowView)sp06145OMVisitEditBindingSource.Current;
                var currentRow = (DataSet_OM_Visit.sp06145_OM_Visit_EditRow)currentRowView.Row;
                if (currentRow == null) return;
                currentRow.ClientPOID = 0;
                currentRow.ClientPONumber = "";
                sp06145OMVisitEditBindingSource.EndEdit();
            }
        }

        private void repositoryItemDateEdit1_EditValueChanged(object sender, EventArgs e)
        {
            DateEdit de = (DateEdit)sender;
            GridView view = (GridView)gridControl6.MainView;
            if (view.FocusedColumn.Name == "colExpectedStartDate")
            {
                Calculate_Min_Days_Between_Visits(view.FocusedRowHandle, false, de.DateTime, null);
                //view.FocusedColumn = view.GetVisibleColumn(view.FocusedColumn.VisibleIndex - 1);
                
                // Get visit in grid with closest higher date for same Site Contract if there is one and re-calculate the Days Separation since this is the record proceeding it and it's start date has changed effecting the Days Separation to the next visit. //
                // Get existing rows visit ID first so we can ignore this record. //
                int intOriginalVisitID = 0;
                try { intOriginalVisitID = (string.IsNullOrEmpty(view.GetFocusedRowCellValue("VisitID").ToString()) ? 0 : Convert.ToInt32(view.GetFocusedRowCellValue("VisitID"))); }
                catch (Exception) { }
                int intSiteContractID = 0;
                try { intSiteContractID = (string.IsNullOrEmpty(view.GetFocusedRowCellValue("SiteContractID").ToString()) ? 0 : Convert.ToInt32(view.GetFocusedRowCellValue("SiteContractID"))); }
                catch (Exception) { }
                if (intOriginalVisitID != 0 && intSiteContractID != 0)
                {
                    DateTime dtRowStartDate = DateTime.MinValue;
                    DateTime dtNearestHigherStartDate = DateTime.MaxValue;
                    int intRowSiteContractID = 0;
                    int intVisitID = 0;
                    int intNextRowID = -1;
                    try
                    {
                        for (int i = 0; i < view.DataRowCount; i++)  // Find next row //
                        {
                            dtRowStartDate = Convert.ToDateTime(view.GetRowCellValue(i, "ExpectedStartDate"));
                            intRowSiteContractID = Convert.ToInt32(view.GetRowCellValue(i, "SiteContractID"));
                            intVisitID = Convert.ToInt32(view.GetRowCellValue(i, "VisitID"));
                            if (intRowSiteContractID == intSiteContractID && intVisitID != intOriginalVisitID && dtRowStartDate > de.DateTime && dtRowStartDate < dtNearestHigherStartDate)
                            {
                                dtNearestHigherStartDate = dtRowStartDate;
                                intNextRowID = i;
                            }
                        }
                        if (intNextRowID >= 0) Calculate_Min_Days_Between_Visits(intNextRowID, false, null, de.DateTime);  // Update next rows Days Separation //
                    }
                    catch (Exception) { }
                }
            }
        }

        private void repositoryItemSpinEditDaysSeparation_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "calculate")
            {
                GridView view = (GridView)gridControl6.MainView;               
                Calculate_Min_Days_Between_Visits(view.FocusedRowHandle, true, null, null);
            }
        }
        private void Calculate_Min_Days_Between_Visits(int intRow, bool ShowErrorMessages, DateTime? PassedInStartDate, DateTime? PassedInPreviousDate)
        {
            // PassedInStartDate is optional can be null //
            GridView view = (GridView)gridControl6.MainView;
            view.PostEditor();

            int intSiteContractID = 0;
            try { intSiteContractID = (string.IsNullOrEmpty(view.GetRowCellValue(intRow, "SiteContractID").ToString()) ? 0 : Convert.ToInt32(view.GetRowCellValue(intRow, "SiteContractID"))); }
            catch (Exception) { }
            if (intSiteContractID <= 0 && ShowErrorMessages)
            {
                XtraMessageBox.Show("Select the Site Contract first before clicking the Re-Calculate Minimum Days Between Visits button.", "Re-Calculate Minimum Days Between Visits", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            DateTime dtExpectedStartDate = DateTime.MinValue;
            if (PassedInStartDate != null)
            {
                dtExpectedStartDate = Convert.ToDateTime(PassedInStartDate);
            }
            else
            {
                try { dtExpectedStartDate = (string.IsNullOrEmpty(view.GetRowCellValue(intRow, "ExpectedStartDate").ToString()) ? DateTime.MinValue : Convert.ToDateTime(view.GetRowCellValue(intRow, "ExpectedStartDate"))); }
                catch (Exception) { }
            }
            if (dtExpectedStartDate <= DateTime.MinValue && ShowErrorMessages)
            {
                XtraMessageBox.Show("Enter the Visit Expected Start Date first before clicking the Re-Calculate Minimum Days Between Visits button.", "Re-Calculate Minimum Days Between Visits", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            int intOriginalVisitID = 0;
            try { intOriginalVisitID = (string.IsNullOrEmpty(view.GetRowCellValue(intRow, "VisitID").ToString()) ? 0 : Convert.ToInt32(view.GetRowCellValue(intRow, "VisitID"))); }
            catch (Exception) { }

            decimal decDaysSeparationPercentage = (decimal)0.00;
            try { decDaysSeparationPercentage = (string.IsNullOrEmpty(view.GetRowCellValue(intRow, "SiteContractDaysSeparationPercent").ToString()) ? (decimal)0.00 : Convert.ToDecimal(view.GetRowCellValue(intRow, "SiteContractDaysSeparationPercent"))); }
            catch (Exception) { }
            if (decDaysSeparationPercentage <= (decimal)0.00 && ShowErrorMessages)
            {
                XtraMessageBox.Show("The selected Site Contract has no value set for the Days Separation %. Unable to calculate Minimum Days Between Visits.", "Re-Calculate Minimum Days Between Visits", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            int intCurrentValue = 0;
            try { intCurrentValue = (string.IsNullOrEmpty(view.GetRowCellValue(intRow, "DaysSeparation").ToString()) ? 0 : Convert.ToInt32(view.GetRowCellValue(intRow, "DaysSeparation"))); }
            catch (Exception) { }

            int intDaysSeparation = 0;
            DateTime dtPreviousDate = DateTime.MaxValue;
            int intRowSiteContractID = 0;
            int intVisitID = 0;
            //DateTime NoNextDate = new DateTime(2500, 1, 1);
            DateTime NoPreviousDate = new DateTime(1900, 1, 1);
            DateTime dtRowStartDate = NoPreviousDate;
            using (var GetValue = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter())
            {
                GetValue.ChangeConnectionString(strConnectionString);
                try
                {
                    if (PassedInPreviousDate == null)
                    {
                        dtPreviousDate = Convert.ToDateTime(GetValue.sp06438_OM_Get_Next_Scheduled_Visit_Date(intSiteContractID, dtExpectedStartDate));

                        foreach (DataRow dr in dataSet_OM_Visit.sp06145_OM_Visit_Edit.Rows)
                        {
                            dtRowStartDate = Convert.ToDateTime(dr["ExpectedStartDate"]);
                            intRowSiteContractID = Convert.ToInt32(dr["SiteContractID"]);
                            intVisitID = Convert.ToInt32(dr["VisitID"]);
                            //if (intVisitID != intOriginalVisitID && dtRowStartDate > dtExpectedStartDate && dtRowStartDate < dtNextDate && intRowSiteContractID == intSiteContractID) dtNextDate = dtRowStartDate;
                            if (intVisitID != intOriginalVisitID && dtRowStartDate < dtExpectedStartDate && dtRowStartDate > dtPreviousDate && intRowSiteContractID == intSiteContractID) dtPreviousDate = dtRowStartDate;
                        }
                    }
                    else
                    {
                        dtPreviousDate = Convert.ToDateTime(PassedInPreviousDate);
                    }
                    //intDaysSeparation = (dtNextDate == NoNextDate ? 0 : Convert.ToInt32((Convert.ToDecimal((dtNextDate - dtExpectedStartDate).TotalDays) * decDaysSeparationPercentage) / (decimal)100.00));
                    intDaysSeparation = (dtPreviousDate == NoPreviousDate ? 0 : Convert.ToInt32((Convert.ToDecimal((dtExpectedStartDate - dtPreviousDate).TotalDays) * decDaysSeparationPercentage) / (decimal)100.00));
                    if (intDaysSeparation != intCurrentValue)
                    {
                        view.SetRowCellValue(intRow, "DaysSeparation", (intDaysSeparation < 0 ? 0 : intDaysSeparation));
                        sp06145OMVisitEditBindingSource.EndEdit();
                    }
                }
                catch (Exception) { }
            }

        }

        #endregion


        private int CheckRows(GridView view)
        {
            int intInvalidRowCount = 0;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                intInvalidRowCount += CheckRow(view, i);
            }
            return intInvalidRowCount;
        }
        private int CheckRow(GridView view, int RowHandle)
        {
            try
            {
                DataRow dr = view.GetDataRow(RowHandle);
                DateTime? dtFromDate = null;
                DateTime? dtToDate = null;
                int? intVisitNumber = null;

                bool boolValidFromDate = true;
                bool boolValidToDate = true;
                bool boolValidVisitNumber = true;

                if (view.GetRowCellValue(RowHandle, "ExpectedStartDate") != DBNull.Value) dtFromDate = Convert.ToDateTime(view.GetRowCellValue(RowHandle, "ExpectedStartDate"));
                if (view.GetRowCellValue(RowHandle, "ExpectedEndDate") != DBNull.Value) dtToDate = Convert.ToDateTime(view.GetRowCellValue(RowHandle, "ExpectedEndDate"));
                if (view.GetRowCellValue(RowHandle, "VisitNumber") != DBNull.Value) intVisitNumber = Convert.ToInt32(view.GetRowCellValue(RowHandle, "VisitNumber"));

                if (dtFromDate == null || dtFromDate == DateTime.MinValue)
                {
                    dr.SetColumnError("ExpectedStartDate", String.Format("{0} requires a value.", view.Columns["ExpectedStartDate"].Caption));
                    boolValidFromDate = false;
                }
                else
                {
                    dr.SetColumnError("ExpectedStartDate", "");
                }
                if (dtToDate == null || dtToDate == DateTime.MinValue)
                {
                    dr.SetColumnError("ExpectedEndDate", String.Format("{0} requires a value.", view.Columns["ExpectedEndDate"].Caption));
                    boolValidToDate = false;
                }
                else
                {
                    dr.SetColumnError("ExpectedEndDate", "");
                }
                if (dtFromDate != null && dtToDate != null && dtFromDate > dtToDate)
                {
                    dr.SetColumnError("ExpectedStartDate", String.Format("{0} should be less than {1}.", view.Columns["ExpectedStartDate"].Caption, view.Columns["ExpectedEndDate"].Caption));
                    dr.SetColumnError("ExpectedEndDate", String.Format("{0} should be greater than {1}.", view.Columns["ExpectedEndDate"].Caption, view.Columns["ExpectedStartDate"].Caption));
                    boolValidFromDate = false;
                    boolValidToDate = false;
                }
                else if (boolValidFromDate && boolValidToDate)
                {
                    dr.SetColumnError("ExpectedStartDate", "");
                    dr.SetColumnError("ExpectedEndDate", "");
                }

                if (intVisitNumber == null || intVisitNumber <= 0)
                {
                    dr.SetColumnError("VisitNumber", String.Format("{0} requires a value.", view.Columns["VisitNumber"].Caption));
                    boolValidVisitNumber = false;
                }
                else
                {
                    dr.SetColumnError("VisitNumber", "");
                }

                return (!boolValidFromDate || !boolValidToDate || !boolValidVisitNumber ? 1 : 0);
            }
            catch (Exception)
            {
            }
            return 0;
        }

        private void btnAddVisits_Click(object sender, EventArgs e)
        {
            GridView viewNew = (GridView)gridControl6.MainView;
            int intCount = viewNew.DataRowCount;

            int intClientContractID = 0;
            int intSiteContractID = 0;
            string strClientName = "";
            string strContractDescription = "";
            string strSiteName = "";
            int intCreatedByID = GlobalSettings.UserID;
            string strCreatedByName = GlobalSettings.UserSurname + ": " + GlobalSettings.UserForename;
            DateTime dtStartDate = DateTime.MinValue;
            DateTime dtCalculatedDate = DateTime.MinValue;
            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();
            try
            {
                int intMax = i_intMaxVisitNumber;  // Max number from saved visits  //

                // Check grid to see if we have any higher numbers... //
                int intGridRowCount = viewNew.DataRowCount;
                int intTempMax = 0;
                for (int i = 0; i < intCount; i++)
                {
                    intTempMax = Convert.ToInt32(viewNew.GetRowCellValue(i, "VisitNumber"));
                    if (intTempMax > intMax) intMax = intTempMax;
                }

                intClientContractID = i_intPassedInClientContractID;
                intSiteContractID = i_intPassedInSiteContractID;
                strClientName = i_strPassedInClientName;
                strContractDescription = i_strPassedInClientContractDescription;
                strSiteName = i_strPassedInSiteName;

                int intVisitCount = Convert.ToInt32(spinEditNumberOfVisits.EditValue);
                int intGapBetweenVisitsUnits = Convert.ToInt32(spinEditGapBetweenVisitsUnits.EditValue);
                int intGapBetweenVisitsDescriptor = Convert.ToInt32(gridLookUpEditGapBetweenVisitsDescriptor.EditValue);
                int intVisitsStatusID = Convert.ToInt32(gridLookUpEditVisitStatusID.EditValue);
                int intCostCalculationLevel = Convert.ToInt32(gridLookUpEditCostCalculationLevel.EditValue);
                int intSellCalculationLevel = Convert.ToInt32(gridLookUpEditSellCalculationLevel.EditValue);
                dtStartDate = dateEditStartDate.DateTime;

                int intModifier = (GapfirstVisitCheckEdit.Checked ? 1 : 0);
                for (int j = 0; j < intVisitCount; j++)
                {
                    switch (intGapBetweenVisitsDescriptor)
                    {
                        case 1:  // Days //
                            dtCalculatedDate = dtStartDate.AddDays((j + intModifier) * intGapBetweenVisitsUnits);
                            break;
                        case 2:  // Weeks //
                            dtCalculatedDate = dtStartDate.AddDays((j + intModifier) * (7 * intGapBetweenVisitsUnits));
                            break;
                        case 3:  // Months //
                            dtCalculatedDate = dtStartDate.AddMonths((j + intModifier) * intGapBetweenVisitsUnits);
                            break;
                        case 4:  // Years //
                            dtCalculatedDate = dtStartDate.AddYears((j + intModifier) * intGapBetweenVisitsUnits);
                            break;
                        default:
                            break;
                    }
                    intMax++;
                    DataRow drNewRow;
                    drNewRow = dataSet_OM_Visit.sp06145_OM_Visit_Edit.NewRow();
                    drNewRow["strMode"] = "add";
                    drNewRow["strRecordIDs"] = "";
                    drNewRow["TenderID"] = i_intPassedInTenderID;
                    drNewRow["SiteContractID"] = intSiteContractID;
                    drNewRow["VisitNumber"] = intMax;
                    drNewRow["CreatedByStaffID"] = intCreatedByID;
                    drNewRow["ExpectedStartDate"] = dtCalculatedDate;
                    drNewRow["ExpectedEndDate"] = dtCalculatedDate.AddDays(1).AddMilliseconds(-5); //.AddHours((double)1);
                    drNewRow["CostCalculationLevel"] = intCostCalculationLevel;
                    drNewRow["SellCalculationLevel"] = intSellCalculationLevel;
                    drNewRow["StartLatitude"] = (decimal)0.00;
                    drNewRow["StartLongitude"] = (decimal)0.00;
                    drNewRow["FinishLatitude"] = (decimal)0.00;
                    drNewRow["FinishLongitude"] = (decimal)0.00;
                    drNewRow["ClientContractID"] = intClientContractID;
                    drNewRow["ClientName"] = strClientName;
                    drNewRow["ContractDescription"] = strContractDescription;
                    drNewRow["SiteName"] = strSiteName;
                    drNewRow["CreatedByStaffName"] = strCreatedByName;
                    drNewRow["VisitStatusID"] = intVisitsStatusID;
                    drNewRow["VisitCategoryID"] = i_intDefaultVisitCategoryID;
                    drNewRow["IssueFound"] = 0;
                    drNewRow["IssueTypeID"] = 0;
                    drNewRow["VisitTypeID"] = i_intExtraWorksID;
                    drNewRow["CreatedFromVisitTemplateID"] = 0;
                    drNewRow["DaysSeparation"] = 0;
                    drNewRow["SiteContractDaysSeparationPercent"] = i_decSiteContractDaysSeparationPercent;
                    drNewRow["VisitLabourCost"] = (intCostCalculationLevel == 0 ? i_decPassedInTenderLabourCost : (decimal)0.00);
                    drNewRow["VisitMaterialCost"] = (intCostCalculationLevel == 0 ? i_decPassedInTenderMaterialCost : (decimal)0.00);
                    drNewRow["VisitEquipmentCost"] = (intCostCalculationLevel == 0 ? i_decPassedInTenderEquipmentCost : (decimal)0.00);
                    drNewRow["VisitLabourSell"] = (intSellCalculationLevel == 0 ? i_decPassedInTenderLabourSell : (decimal)0.00);
                    drNewRow["VisitMaterialSell"] = (intSellCalculationLevel == 0 ? i_decPassedInTenderMaterialSell : (decimal)0.00);
                    drNewRow["VisitEquipmentSell"] = (intSellCalculationLevel == 0 ? i_decPassedInTenderEquipmentSell : (decimal)0.00);
                    drNewRow["VisitCost"] = (intCostCalculationLevel == 0 ? i_decPassedInTenderLabourCost + i_decPassedInTenderMaterialCost + i_decPassedInTenderEquipmentCost : (decimal)0.00);
                    drNewRow["VisitSell"] = (intSellCalculationLevel == 0 ? i_decPassedInTenderLabourSell + i_decPassedInTenderMaterialSell + i_decPassedInTenderEquipmentSell : (decimal)0.00);
                    drNewRow["DummyMaxDate"] = (i_dtPassedInWorkCompletedBy == DateTime.MinValue ? new DateTime(2500, 12, 31) : Convert.ToDateTime(i_dtPassedInWorkCompletedBy));
                    dataSet_OM_Visit.sp06145_OM_Visit_Edit.Rows.Add(drNewRow);
                    CheckRows(viewNew);
                }
                UpdateCalculatedTotalsRow();

                // Prepare defaults for any further added records to be added //
                GapfirstVisitCheckEdit.Checked = true;
                dateEditStartDate.DateTime = dtCalculatedDate;

                // Update Visit Days Separation //
                viewNew.BeginDataUpdate();
                try
                {
                    for (int i = 0; i < viewNew.DataRowCount; i++)
                    {
                        Calculate_Min_Days_Between_Visits(i, false, null, null);
                    }
                }
                catch (Exception) { }
                viewNew.EndDataUpdate();

            }
            catch (Exception) { }
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }

        private void btnApplyVisitDaysSeparation_Click(object sender, EventArgs e)
        {
            int intRecordCount = 0;
            GridView view = (GridView)gridControl6.MainView;
            view.PostEditor();
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            intRecordCount = intRowHandles.Length;
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Visits to Block Edit Visit Days Separation then try again.", "Block Edit Visit Days Separation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            int intDaysSeparation = Convert.ToInt32(VisitDaysSeparationSpinEdit.EditValue);
            view.BeginDataUpdate();
            try
            {
                foreach (int intRowHandle in intRowHandles)
                {
                    view.SetRowCellValue(intRowHandle, "DaysSeparation", intDaysSeparation);
                }
                sp06145OMVisitEditBindingSource.EndEdit();
            }
            catch (Exception) { }
            view.EndDataUpdate();
            DevExpress.XtraEditors.XtraMessageBox.Show("Visit(s) Days Separation Block Edited Successfully.", "Block Edit Visit Days Separation", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void btnCalculateVisitDaysSeparation_Click(object sender, EventArgs e)
        {
            int intRecordCount = 0;
            GridView view = (GridView)gridControl6.MainView;
            view.PostEditor();
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            intRecordCount = intRowHandles.Length;
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Visits to Calculate Visit Days Separation then try again.", "Calculate Visit Days Separation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            view.BeginDataUpdate();
            try
            {
                foreach (int intRowHandle in intRowHandles)
                {
                    Calculate_Min_Days_Between_Visits(intRowHandle, false, null, null);
                }
            }
            catch (Exception) { }
            view.EndDataUpdate();
            DevExpress.XtraEditors.XtraMessageBox.Show("Visit(s) Days Separation Calculated Successfully.", "Calculate Visit Days Separation", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }


        private void btnApplyVisitDuration_Click(object sender, EventArgs e)
        {
            int intRecordCount = 0;
            GridView view = (GridView)gridControl6.MainView;
            view.PostEditor();
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            intRecordCount = intRowHandles.Length;
            if (intRowHandles.Length < 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Visits to Block Edit Visit Duration then try again.", "Block Edit Visit Duration", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            int intDaysSeparation = Convert.ToInt32(VisitDaysSeparationSpinEdit.EditValue);
            view.BeginDataUpdate();
            DateTime dtExpectedStartDate = new DateTime();
            DateTime dtExpectedEndDate = new DateTime();
            int intVisitDuration = Convert.ToInt32(VisitDurationSpinEdit.EditValue);
            string strVisitDurationDescriptor = VisitDescriptorComboBoxEdit.EditValue.ToString();
            TimeSpan ts = VisitTimeEdit.TimeSpan;
            if (intVisitDuration < 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Enter Duration Units and select Unit Descriptor and optionally set the Time then try again.", "Block Edit Visit Duration", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                foreach (int intRowHandle in intRowHandles)
                {
                    try { if (!string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "ExpectedStartDate").ToString())) dtExpectedStartDate = Convert.ToDateTime(view.GetRowCellValue(intRowHandle, "ExpectedStartDate")); }
                    catch (Exception) { }

                    if (dtExpectedStartDate != DateTime.MinValue)
                    {
                        switch (strVisitDurationDescriptor)
	                        {
                                case "Minutes":
                                    {
                                        dtExpectedEndDate = dtExpectedStartDate.AddMinutes(intVisitDuration);
                                    }
                                    break;
                                case "Hours":
                                    {
                                        dtExpectedEndDate = dtExpectedStartDate.AddHours(intVisitDuration);
                                    }
                                    break;
                                case "Days":
                                    {
                                        dtExpectedEndDate = dtExpectedStartDate.AddDays(intVisitDuration);
                                        dtExpectedEndDate = dtExpectedEndDate.Add(ts);
                                    }
                                    break;
                                case "Weeks":
                                    {
                                        dtExpectedEndDate = dtExpectedStartDate.AddDays(intVisitDuration * 7);
                                        dtExpectedEndDate = dtExpectedEndDate.Add(ts);
                                    }
                                    break;
                                case "Months":
                                    {
                                        dtExpectedEndDate = dtExpectedStartDate.AddMonths(intVisitDuration);
                                        dtExpectedEndDate = dtExpectedEndDate.Add(ts);
                                    }
                                    break;
                                case "Years":
                                    {
                                        dtExpectedEndDate = dtExpectedEndDate.Add(ts);
                                        dtExpectedEndDate = dtExpectedStartDate.AddYears(intVisitDuration);
                                    }
                                    break;
                                default:
                                    break;
                        }
                        view.SetRowCellValue(intRowHandle, "ExpectedEndDate", dtExpectedEndDate);                                               
                    }

                }
                sp06145OMVisitEditBindingSource.EndEdit();
            }
            catch (Exception) { }
            view.EndDataUpdate();
            DevExpress.XtraEditors.XtraMessageBox.Show("Visit(s) Duration Block Edited Successfully.", "Block Edit Visit Duration", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void VisitDescriptorComboBoxEdit_EditValueChanged(object sender, EventArgs e)
        {
            ComboBoxEdit cbe = (ComboBoxEdit)sender;
            string strValue = cbe.EditValue.ToString();
            Set_Visit_Hours_Edit_Status(strValue);
        }
        private void Set_Visit_Hours_Edit_Status(string strValue)
        {
            VisitTimeEdit.Enabled = (strValue == "Minutes" || strValue == "Hours" ? false : true);
        }

        private void btnSplitCosts_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl6.MainView;
            int intCount = view.DataRowCount;
            if (intCount <= 0) return;

            // Use LINQ to get count for column from datatable - had to add a reference to System.Data.DataSetExtensions for the project then a reference to link at the top of this form //
            int intCountCost = dataSet_OM_Visit.sp06145_OM_Visit_Edit
                                .Where(x => x.CostCalculationLevel == 0)
                                .Count();

            int intCountSell = dataSet_OM_Visit.sp06145_OM_Visit_Edit
                                .Where(x => x.SellCalculationLevel == 0)
                                .Count();

            if (intCountCost <= 0 && intCountSell < 0)
            {
                XtraMessageBox.Show("Values can only be split across visits with cost and \\ or sell calculation methods equal to Manual.", "Split Tender Costs Across Visits", MessageBoxButtons.OK, MessageBoxIcon.Stop, DefaultBoolean.True);
                return;
            }

            if (XtraMessageBox.Show("You are about to split the costs evenly across the created visit(s).\n\nAre you sure you wish to proceed?", "Split Tender Costs Across Visits", MessageBoxButtons.YesNo, MessageBoxIcon.Question, DefaultBoolean.True) == DialogResult.No) return;

            decimal decLabourCost = (decimal)0.00;
            decimal decMaterialCost = (decimal)0.00;
            decimal decEquipmentCost = (decimal)0.00;
            decimal decTotalCost = (decimal)0.00;
            if (intCountCost > 0)
            {
                decLabourCost = ((i_decPassedInTenderLabourCost > (decimal)0.00) ? Math.Round((i_decPassedInTenderLabourCost / intCountCost), 2) : (decimal)0.00);
                decMaterialCost = ((i_decPassedInTenderMaterialCost > (decimal)0.00) ? Math.Round((i_decPassedInTenderMaterialCost / intCountCost), 2) : (decimal)0.00);
                decEquipmentCost = ((i_decPassedInTenderEquipmentCost > (decimal)0.00) ? Math.Round((i_decPassedInTenderEquipmentCost / intCountCost), 2) : (decimal)0.00);
                decTotalCost = decLabourCost + decMaterialCost + decEquipmentCost;
            }

            decimal decLabourSell = (decimal)0.00;
            decimal decMaterialSell = (decimal)0.00;
            decimal decEquipmentSell = (decimal)0.00;
            decimal decTotalSell = (decimal)0.00;
            if (intCountCost > 0)
            {
                decLabourSell = ((i_decPassedInTenderLabourSell > (decimal)0.00) ? Math.Round((i_decPassedInTenderLabourSell / intCountSell), 2) : (decimal)0.00);
                decMaterialSell = ((i_decPassedInTenderMaterialSell > (decimal)0.00) ? Math.Round((i_decPassedInTenderMaterialSell / intCountSell), 2) : (decimal)0.00);
                decEquipmentSell = ((i_decPassedInTenderEquipmentSell > (decimal)0.00) ? Math.Round((i_decPassedInTenderEquipmentSell / intCountSell), 2) : (decimal)0.00);
                decTotalSell = decLabourSell + decMaterialSell + decEquipmentSell;
            }
            view.BeginUpdate();
            for (int i = 0; i < intCount; i++)
            {
                if (intCountCost > 0)
                {
                    if (Convert.ToInt32(view.GetRowCellValue(i, "CostCalculationLevel")) == 0)
                    {
                        view.SetRowCellValue(i, "VisitLabourCost", decLabourCost);
                        view.SetRowCellValue(i, "VisitMaterialCost", decMaterialCost);
                        view.SetRowCellValue(i, "VisitEquipmentCost", decEquipmentCost);
                        view.SetRowCellValue(i, "VisitCost", decTotalCost);
                    }
                }

                if (intCountCost > 0)
                {
                    if (Convert.ToInt32(view.GetRowCellValue(i, "SellCalculationLevel")) == 0)
                    {
                        view.SetRowCellValue(i, "VisitLabourSell", decLabourSell);
                        view.SetRowCellValue(i, "VisitMaterialSell", decMaterialSell);
                        view.SetRowCellValue(i, "VisitEquipmentSell", decEquipmentSell);
                        view.SetRowCellValue(i, "VisitSell", decTotalSell);
                    }
                }
            }
             view.EndUpdate();
             UpdateCalculatedTotalsRow();
        }

        private void btnApplyVisitStatus_Click(object sender, EventArgs e)
        {
            int intRecordCount = 0;
            GridView view = (GridView)gridControl6.MainView;
            view.PostEditor();
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            intRecordCount = intRowHandles.Length;
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Visits to Block Edit Visit Status then try again.", "Block Edit Visit Status", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            int intVisitStatusID = Convert.ToInt32(gridLookUpEditVisitStatusBlockEdit.EditValue);
            view.BeginDataUpdate();
            try
            {
                foreach (int intRowHandle in intRowHandles)
                {
                    view.SetRowCellValue(intRowHandle, "VisitStatusID", intVisitStatusID);
                }
                sp06145OMVisitEditBindingSource.EndEdit();
            }
            catch (Exception) { }
            view.EndDataUpdate();
        }



        #endregion


        #region Finish Page

        private void btnFinishPrevious_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep1;
        }
        private void btnFinish_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl6.MainView;  // Linked Site Contracts  //
            view.PostEditor();
            int intVisitCount = view.DataRowCount;

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Saving Changes...");

            try 
            {
                sp06145_OM_Visit_EditTableAdapter.Update(dataSet_OM_Visit);  // Insert, Update and Delete queries defined in Table Adapter //
                foreach (DataRow dr in dataSet_OM_Visit.sp06145_OM_Visit_Edit.Rows)
                {
                    dr["strMode"] = "edit";
                }
                sp06145OMVisitEditBindingSource.EndEdit();  // Commit any changes from grid to underlying datasource //
            }
            catch (System.Exception ex)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                XtraMessageBox.Show("An error occurred while saving the Visit changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
              
            // Pick up the new IDs so they can be passed back to the parent manager screen so the new records can be highlighted //
            string strNewIDs = "";
            int intVisitID = 0;
            string strVisitIDs = "";
            string strTempSiteContractID = "";
            string strTempClientContractID = "";

            StringBuilder sbSiteContractIDs = new StringBuilder(",");  // Initialised with a preceeding comma to be used when checking if build string contains next value //
            StringBuilder sbClientContractIDs = new StringBuilder(",");  // Initialised with a preceeding comma to be used when checking if build string contains next value //

            foreach (DataRow dr in dataSet_OM_Visit.sp06145_OM_Visit_Edit.Rows)
            {
                intVisitID = Convert.ToInt32(dr["VisitID"]);
                strNewIDs += intVisitID.ToString() + ";";
                strVisitIDs += intVisitID.ToString() + ",";

                strTempSiteContractID = dr["SiteContractID"].ToString() + ",";
                if (!sbSiteContractIDs.ToString().Contains("," + strTempSiteContractID)) sbSiteContractIDs.Append(strTempSiteContractID);

                strTempClientContractID = dr["ClientContractID"].ToString() + ",";
                if (!sbClientContractIDs.ToString().Contains("," + strTempClientContractID)) sbClientContractIDs.Append(strTempClientContractID);
            }
            string strSiteContractIDs = sbSiteContractIDs.ToString();
            string strClientContractIDs = sbClientContractIDs.ToString();

            if (strSiteContractIDs.StartsWith(",")) strSiteContractIDs = strSiteContractIDs.Remove(0, 1);  // Remove preceeding comma //
            if (strClientContractIDs.StartsWith(",")) strClientContractIDs = strClientContractIDs.Remove(0, 1);  // Remove preceeding comma //

            // Notify any open instances of Parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                if (Application.OpenForms[strCaller] != null)
                {
                    if (strCaller == "frm_OM_Tender_Manager") (Application.OpenForms[strCaller] as frm_OM_Tender_Manager).UpdateFormRefreshStatus(2, Utils.enmFocusedGrid.Visit, strNewIDs);
                    else if (strCaller == "frm_OM_Visit_Manager") (Application.OpenForms[strCaller] as frm_OM_Visit_Manager).UpdateFormRefreshStatus(1, Utils.enmFocusedGrid.Visit, strNewIDs);
                }
            }
            try
            {
                if (frmCaller != null) frmCaller.SetTenderVisitCount(i_intPassedInTenderID, intVisitCount);  // Update Edit Tender screen //
            }
            catch (Exception) { }


            this.dataSet_OM_Contract.AcceptChanges();

            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }

            if (checkEdit2.Checked)  // Open Job Wizard //
            {
                if (this.ParentForm != null)
                {
                    var fChildForm = new frm_OM_Job_Wizard();
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strRecordIDs = "";
                    fChildForm.strFormMode = "add";
                    fChildForm.strCaller = this.strCaller;
                    fChildForm.intRecordCount = 0;
                    fChildForm.FormPermissions = this.FormPermissions;
                    fChildForm.i_str_PassedInClientContractIDs = strClientContractIDs;
                    fChildForm.i_str_PassedInSiteContractIDs = strSiteContractIDs;
                    fChildForm.i_str_PassedInVisitIDs = strVisitIDs;

                    fChildForm.i_intPassedInTenderID = i_intPassedInTenderID;
                    fChildForm.i_strPassedInTenderDescription = TenderDetailsLabelControl.Text;
                    fChildForm.i_intPassedInJobSubTypeID = i_intPassedInJobSubTypeID;
                    fChildForm.i_intPassedInLabourTypeID = i_intPassedInLabourTypeID;
                    fChildForm.i_intPassedInLabourID = i_intPassedInLabourID;
                    fChildForm.i_strPassedInLabourName = i_strPassedInLabourName;
                    fChildForm.i_dbPassedInLabourLatitude = i_dbPassedInLabourLatitude;
                    fChildForm.i_dbPassedInLabourLongitude = i_dbPassedInLabourLongitude;
                    fChildForm.i_strPassedInLabourPostcode = i_strPassedInLabourPostcode;
                    fChildForm.i_dtPassedInWorkCompletedBy = i_dtPassedInWorkCompletedBy;
                    fChildForm.frmCaller = frmCaller;

                    DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager2 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    fChildForm.splashScreenManager = splashScreenManager2;
                    fChildForm.splashScreenManager.ShowWaitForm();
                    fChildForm.Show();

                    System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm, new object[] { null });
                }
                else
                {
                    XtraMessageBox.Show("Unable to open another form from this form... The form must be docked inside the main container form first.", "Open Job Wizard", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
          
            this.Close();
        }

        #endregion


        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }


        private void Block_Edit_Record()
        {
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Visit:
                    {
                        Block_Edit_Visits();
                    }
                    break;
                default:
                    break;
            }
        }

        private void Block_Edit_Visits()
        {
            int intRecordCount = 0;
            GridView view = (GridView)gridControl6.MainView;
            int intRowCount = view.DataRowCount;
            view.PostEditor();
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            intRecordCount = intRowHandles.Length;
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Visits to block edit then try again.", "Block Edit Visits", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            var fChildForm = new frm_OM_Tender_Visit_Wizard_Block_Edit_Visit_Info();
            fChildForm.GlobalSettings = this.GlobalSettings;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                bool boolRecalulateValue = false;
                splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Updating...");
                
                intRowHandles = view.GetSelectedRows();
                intRecordCount = intRowHandles.Length;
                if (intRowHandles.Length <= 0) return;
                view.BeginUpdate();
                view.BeginSort();
                try
                {
                    foreach (int intRowHandle in intRowHandles)
                    {
                        int intCascadeDates = fChildForm.intCascadeDates;
                        double dblDaysDifferenceStart = (double)0;
                        double dblDaysDifferenceEnd = (double)0;
                        int intSiteContractID = 0;
                        int intVisitNumber = 0;
                        if (intCascadeDates == 1 && (fChildForm.dtVisitStartDate != null || fChildForm.dtVisitEndDate != null))
                        {
                            // Find any related Visits after this one and add the number of days to these //
                            if (fChildForm.dtVisitStartDate != null)
                            {
                                DateTime d1 = Convert.ToDateTime(fChildForm.dtVisitStartDate);
                                DateTime d2 = Convert.ToDateTime(view.GetRowCellValue(intRowHandle, "ExpectedStartDate"));
                                TimeSpan t = d1 - d2;
                                dblDaysDifferenceStart = t.TotalDays;
                            }
                            if (fChildForm.dtVisitEndDate != null)
                            {
                                DateTime d1 = Convert.ToDateTime(fChildForm.dtVisitEndDate);
                                DateTime d2 = Convert.ToDateTime(view.GetRowCellValue(intRowHandle, "ExpectedEndDate"));
                                TimeSpan t = d1 - d2;
                                dblDaysDifferenceEnd = t.TotalDays;
                            }
                            intSiteContractID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "SiteContractID"));
                            intVisitNumber = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "VisitNumber"));

                            for (int i = 0; i < intRowCount; i++)
                            {
                                if (Convert.ToInt32(view.GetRowCellValue(i, "SiteContractID")) == intSiteContractID && Convert.ToInt32(view.GetRowCellValue(i, "VisitNumber")) > intVisitNumber)
                                {
                                    view.SetRowCellValue(i, "ExpectedStartDate", Convert.ToDateTime(view.GetRowCellValue(i, "ExpectedStartDate")).AddDays(dblDaysDifferenceStart));
                                    view.SetRowCellValue(i, "ExpectedEndDate", Convert.ToDateTime(view.GetRowCellValue(i, "ExpectedEndDate")).AddDays(dblDaysDifferenceEnd));
                                }
                            }
                        }
                        if (fChildForm.dtVisitStartDate != null) view.SetRowCellValue(intRowHandle, "ExpectedStartDate", fChildForm.dtVisitStartDate);
                        if (fChildForm.dtVisitEndDate != null) view.SetRowCellValue(intRowHandle, "ExpectedEndDate", fChildForm.dtVisitEndDate);
                        
                        if (fChildForm.intCostCalculationLevel != null)
                        {
                            boolRecalulateValue = true;
                            view.SetRowCellValue(intRowHandle, "CostCalculationLevel", fChildForm.intCostCalculationLevel);
                            if (fChildForm.intCostCalculationLevel == 0)
                            {
                                if (fChildForm.decTenderLabourCost != null) view.SetRowCellValue(intRowHandle, "VisitLabourCost", fChildForm.decTenderLabourCost);
                                if (fChildForm.decTenderMaterialCost != null) view.SetRowCellValue(intRowHandle, "VisitMaterialCost", fChildForm.decTenderMaterialCost);
                                if (fChildForm.decTenderEquipmentCost != null) view.SetRowCellValue(intRowHandle, "VisitEquipmentCost", fChildForm.decTenderEquipmentCost);
                            }
                            else
                            {
                                view.SetRowCellValue(intRowHandle, "VisitLabourCost", (decimal)0.00);
                                view.SetRowCellValue(intRowHandle, "VisitMaterialCost", (decimal)0.00);
                                view.SetRowCellValue(intRowHandle, "VisitEquipmentCost", (decimal)0.00);
                            }
                            view.SetRowCellValue(intRowHandle, "VisitCost", Convert.ToDecimal(view.GetRowCellValue(intRowHandle, "VisitLabourCost")) + Convert.ToDecimal(view.GetRowCellValue(intRowHandle, "VisitMaterialCost")) + Convert.ToDecimal(view.GetRowCellValue(intRowHandle, "VisitEquipmentCost")));
                        }
                        if (fChildForm.intSellCalculationLevel != null)
                        {
                            boolRecalulateValue = true;
                            view.SetRowCellValue(intRowHandle, "SellCalculationLevel", fChildForm.intSellCalculationLevel);
                            if (fChildForm.intSellCalculationLevel == 0)
                            {
                                if (fChildForm.decTenderLabourSell != null) view.SetRowCellValue(intRowHandle, "VisitLabourSell", fChildForm.decTenderLabourSell);
                                if (fChildForm.decTenderMaterialSell != null) view.SetRowCellValue(intRowHandle, "VisitMaterialSell", fChildForm.decTenderMaterialSell);
                                if (fChildForm.decTenderEquipmentSell != null) view.SetRowCellValue(intRowHandle, "VisitEquipmentSell", fChildForm.decTenderEquipmentSell);
                            }
                            else
                            {
                                view.SetRowCellValue(intRowHandle, "VisitLabourSell", (decimal)0.00);
                                view.SetRowCellValue(intRowHandle, "VisitMaterialSell", (decimal)0.00);
                                view.SetRowCellValue(intRowHandle, "VisitEquipmentSell", (decimal)0.00);
                            }
                            view.SetRowCellValue(intRowHandle, "VisitSell", Convert.ToDecimal(view.GetRowCellValue(intRowHandle, "VisitLabourSell")) + Convert.ToDecimal(view.GetRowCellValue(intRowHandle, "VisitMaterialSell")) + Convert.ToDecimal(view.GetRowCellValue(intRowHandle, "VisitEquipmentSell")));
                        }

                        if (fChildForm.intVisitCategoryID != null) view.SetRowCellValue(intRowHandle, "VisitCategoryID", fChildForm.intVisitCategoryID);
                        if (fChildForm.intVisitTypeID != null) view.SetRowCellValue(intRowHandle, "VisitTypeID", fChildForm.intVisitTypeID);
                        if (fChildForm.strWorkNumber != null) view.SetRowCellValue(intRowHandle, "WorkNumber", fChildForm.strWorkNumber);
                        if (fChildForm.intClientPOID != null) view.SetRowCellValue(intRowHandle, "ClientPOID", fChildForm.intClientPOID);
                        if (fChildForm.strClientPONumber != null) view.SetRowCellValue(intRowHandle, "ClientPONumber", fChildForm.strClientPONumber);
                        if (fChildForm.intDaysSeparation != null) view.SetRowCellValue(intRowHandle, "DaysSeparation", fChildForm.intDaysSeparation);
                    }
                }
                catch (Exception) { }

                CheckRows(view);
                if (boolRecalulateValue) UpdateCalculatedTotalsRow();

                view.EndSort();
                view.EndUpdate();

                if (splashScreenManager.IsSplashFormVisible)
                {
                    splashScreenManager.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager.CloseWaitForm();
                }
            }
        }

        private void Delete_Record()
        {
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Visit:
                    {
                        if (strFormMode == "view") return;
                        int[] intRowHandles;
                        int intCount = 0;
                        GridView view = null;
                        string strMessage = "";

                        view = (GridView)gridControl6.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more new Visits to delete by clicking on them then try again.", "Delete New Visits", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 new Visit" : Convert.ToString(intRowHandles.Length) + " new Visits") + " selected for deleting!\n\nProceed?";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete New Visits", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            for (int i = intRowHandles.Length - 1; i >= 0; i--)
                            {
                                int intVisitID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "VisitID"));
                                //ResolveVisitNumbers(intVisitID);
                                view.DeleteRow(intRowHandles[i]);
                            }

                            UpdateCalculatedTotalsRow();

                            // Update Visit Days Separation //
                            view.BeginDataUpdate();
                            try
                            {
                                for (int i = 0; i < view.DataRowCount; i++)
                                {
                                    Calculate_Min_Days_Between_Visits(i, false, null, null);
                                }
                            }
                            catch (Exception) { }
                            view.EndDataUpdate();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            //view.EndUpdate();
                            SetMenuStatus();
                        }
                    }
                    break;
                default:
                    break;
            }
        }

 





 


 

  




    }
}

namespace WoodPlan5
{
    partial class frm_OM_Team_Self_Billing_Finance_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Team_Self_Billing_Finance_Manager));
            DevExpress.XtraGrid.GridFormatRule gridFormatRule5 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue5 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule6 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue6 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule7 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue7 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule8 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue8 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule9 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue9 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule2 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue2 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule3 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue3 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule4 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue4 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            this.colCompanyName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn87 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn86 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn89 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn88 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingRequirmentsUnfulfilled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditInteger2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colBillingRequirmentsFulfilled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingWarningUnauthorised = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingWarningAuthorised = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingInvoiceHeaderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControlHistoricalExports = new DevExpress.XtraGrid.GridControl();
            this.sp06472OMFinanceSelfBillingHistoricalExportsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Billing = new WoodPlan5.DataSet_OM_Billing();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridViewHistoricalExports = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSelfBillingFinanceExportID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateExported = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colExportedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExportFile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditExportFile = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colCompanyID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamsIncluded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colStaffIncluded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExportedByStaff = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedVisits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelected1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataSet_OM_Job = new WoodPlan5.DataSet_OM_Job();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.repositoryItemCheckEditShowActiveEmployeesOnly = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemPopupContainerEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControlExportVisits = new DevExpress.XtraGrid.GridControl();
            this.sp06473OMFinanceSelfBillingManagerVisitsOnExportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewExportVisits = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn40 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn41 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn42 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn43 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn44 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn45 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn46 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn47 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn48 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn49 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn50 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn51 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn52 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn53 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn54 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditClientSignature2 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn55 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn56 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn57 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn58 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn59 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn60 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn61 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn62 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn63 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn64 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn65 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn66 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn67 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn68 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn69 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn70 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn71 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn72 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn73 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditClientInvoiceID2 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn74 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn75 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn76 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn77 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn78 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn79 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn80 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn81 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn82 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn83 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn84 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn85 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn90 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn91 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn92 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn93 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn94 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn95 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn96 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn97 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn98 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn99 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn100 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn101 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditSelfBillingInvoicePDF = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.sp06469OMFinanceSelfBillingManagerVisitsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp06219OMWasteDisposalCentresAcceptedWasteTypesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridControlInvoices = new DevExpress.XtraGrid.GridControl();
            this.sp06468OMFinanceSelfRecordsClientsToBillBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewInvoices = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSubContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvoiceDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDate = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colInvoiceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAnalysisCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCentre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditMoney = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClient = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDirectEmployed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPeriod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYear = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelected = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colSelfBillingInvoiceHeaderID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUniqueID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGCCompanyID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingInvoiceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataSet_OM_Client_PO = new WoodPlan5.DataSet_OM_Client_PO();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.sp06219_OM_Waste_Disposal_Centres_Accepted_Waste_TypesTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06219_OM_Waste_Disposal_Centres_Accepted_Waste_TypesTableAdapter();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageTeamSelfBillingInvoices = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.popupContainerControlInvoiceNumber = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnInvoiceNumber = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp06477OMGetUniqueSelfBillingInvoiceListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSelfBillingInvoiceNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.popupContainerControlDateRange = new DevExpress.XtraEditors.PopupContainerControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.btnDateRangeOK = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerControlCompanyFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControlCompanyFilter = new DevExpress.XtraGrid.GridControl();
            this.sp01044CoreGCCompanyListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_Common_Functionality = new WoodPlan5.DataSet_Common_Functionality();
            this.gridViewCompanyFilter = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOMSelfBillingFinanceExportNamePrefix = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit21 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.btnCompanyFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControlVisit = new DevExpress.XtraGrid.GridControl();
            this.gridViewVisit = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colVisitID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colExpectedEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCalculationLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colVisitSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colStartLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colStartLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinishLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinishLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCalculationLevelDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDirectorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colClientContractValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinanceClientCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDirector = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToParent1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHTML0 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colContractDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSitePostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteLocationX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteLocationY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedJobCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditGrid1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colLinkedOutstandingJobCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedCompletedJobCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeliness = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitCategoryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssueFound = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssueType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssueTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssueRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientSignaturePath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditClientSignature = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colImagesFolderOM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRework = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExtraWorkComments = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExtraWorkRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExtraWorkType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccidentOnSite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccidentComment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSuspendedJobCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedPictureCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFriendlyVisitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPOID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellCalculationLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellCalculationLevelDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelected4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientInvoiceID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditClientInvoiceID = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colCMName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDaysAllowed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colManagerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colManagerNotes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoOnetoSign = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExternalCustomerStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompletionSheetNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusIssueID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusIssue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDaysSeparation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractDaysSeparationPercent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLabourName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotInvoiceClient1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotPayContractor1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingComment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitLabourCalculatedID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUniqueID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvoicePDF = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditSelfBillingInvoice = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.repositoryItemPictureEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.xtraTabPageHistoricalDataExtracts = new DevExpress.XtraTab.XtraTabPage();
            this.standaloneBarDockControl2 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.hideContainerLeft = new DevExpress.XtraBars.Docking.AutoHideContainer();
            this.dockPanelFilters = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.popupContainerEditInvoiceNumber = new DevExpress.XtraEditors.PopupContainerEdit();
            this.checkEditSelfEmployed = new DevExpress.XtraEditors.CheckEdit();
            this.btnLoadHistorical = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerEditDateRange = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerEditCompanyFilter = new DevExpress.XtraEditors.PopupContainerEdit();
            this.checkEditStaff = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditTeams = new DevExpress.XtraEditors.CheckEdit();
            this.btnLoad = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.tabbedControlGroupFilters = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroupDataExport = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupHistoricalExports = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.imageCollectionTooltips = new DevExpress.Utils.ImageCollection(this.components);
            this.sp01044_Core_GC_Company_ListTableAdapter = new WoodPlan5.DataSet_Common_FunctionalityTableAdapters.sp01044_Core_GC_Company_ListTableAdapter();
            this.sp06468_OM_Finance_Self_Records_Clients_To_BillTableAdapter = new WoodPlan5.DataSet_OM_BillingTableAdapters.sp06468_OM_Finance_Self_Records_Clients_To_BillTableAdapter();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bsiFilterSelected = new DevExpress.XtraBars.BarSubItem();
            this.bciFilterInvoicesSelected = new DevExpress.XtraBars.BarCheckItem();
            this.bciFilterVisitsSelected = new DevExpress.XtraBars.BarCheckItem();
            this.bsiSelectedCount0 = new DevExpress.XtraBars.BarStaticItem();
            this.bbiExport = new DevExpress.XtraBars.BarButtonItem();
            this.sp06469_OM_Finance_Self_Billing_Manager_VisitsTableAdapter = new WoodPlan5.DataSet_OM_BillingTableAdapters.sp06469_OM_Finance_Self_Billing_Manager_VisitsTableAdapter();
            this.imageCollection2 = new DevExpress.Utils.ImageCollection(this.components);
            this.sp06472_OM_Finance_Self_Billing_Historical_ExportsTableAdapter = new WoodPlan5.DataSet_OM_BillingTableAdapters.sp06472_OM_Finance_Self_Billing_Historical_ExportsTableAdapter();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bbiRefresh2 = new DevExpress.XtraBars.BarButtonItem();
            this.bsiFilterSelected2 = new DevExpress.XtraBars.BarSubItem();
            this.bciFilterExportsSelected = new DevExpress.XtraBars.BarCheckItem();
            this.bciFilterVisitsSelected2 = new DevExpress.XtraBars.BarCheckItem();
            this.bsiSelectedCount1 = new DevExpress.XtraBars.BarStaticItem();
            this.sp06473_OM_Finance_Self_Billing_Manager_Visits_On_ExportTableAdapter = new WoodPlan5.DataSet_OM_BillingTableAdapters.sp06473_OM_Finance_Self_Billing_Manager_Visits_On_ExportTableAdapter();
            this.sp06477_OM_Get_Unique_Self_Billing_Invoice_ListTableAdapter = new WoodPlan5.DataSet_OM_BillingTableAdapters.sp06477_OM_Get_Unique_Self_Billing_Invoice_ListTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlHistoricalExports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06472OMFinanceSelfBillingHistoricalExportsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Billing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewHistoricalExports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditExportFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Job)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditShowActiveEmployeesOnly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExportVisits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06473OMFinanceSelfBillingManagerVisitsOnExportBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExportVisits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditClientSignature2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditClientInvoiceID2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditSelfBillingInvoicePDF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06469OMFinanceSelfBillingManagerVisitsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06219OMWasteDisposalCentresAcceptedWasteTypesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlInvoices)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06468OMFinanceSelfRecordsClientsToBillBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewInvoices)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMoney)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Client_PO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPageTeamSelfBillingInvoices.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlInvoiceNumber)).BeginInit();
            this.popupContainerControlInvoiceNumber.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06477OMGetUniqueSelfBillingInvoiceListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).BeginInit();
            this.popupContainerControlDateRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlCompanyFilter)).BeginInit();
            this.popupContainerControlCompanyFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCompanyFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01044CoreGCCompanyListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Common_Functionality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCompanyFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlVisit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewVisit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditClientSignature)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditClientInvoiceID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditSelfBillingInvoice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit2)).BeginInit();
            this.xtraTabPageHistoricalDataExtracts.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.hideContainerLeft.SuspendLayout();
            this.dockPanelFilters.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditInvoiceNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSelfEmployed.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditDateRange.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditCompanyFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditStaff.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditTeams.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroupFilters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupDataExport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupHistoricalExports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollectionTooltips)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection2)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1243, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 597);
            this.barDockControlBottom.Size = new System.Drawing.Size(1243, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 597);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1243, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 597);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar2});
            this.barManager1.DockControls.Add(this.standaloneBarDockControl1);
            this.barManager1.DockControls.Add(this.standaloneBarDockControl2);
            this.barManager1.DockManager = this.dockManager1;
            this.barManager1.Images = this.imageCollection2;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiRefresh,
            this.bsiSelectedCount0,
            this.bsiFilterSelected,
            this.bciFilterInvoicesSelected,
            this.bciFilterVisitsSelected,
            this.bbiExport,
            this.bbiRefresh2,
            this.bsiSelectedCount1,
            this.bsiFilterSelected2,
            this.bciFilterExportsSelected,
            this.bciFilterVisitsSelected2});
            this.barManager1.MaxItemId = 47;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEdit2,
            this.repositoryItemCheckEditShowActiveEmployeesOnly});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colCompanyName2
            // 
            this.colCompanyName2.Caption = "Company Name";
            this.colCompanyName2.FieldName = "CompanyName";
            this.colCompanyName2.Name = "colCompanyName2";
            this.colCompanyName2.OptionsColumn.AllowEdit = false;
            this.colCompanyName2.OptionsColumn.AllowFocus = false;
            this.colCompanyName2.OptionsColumn.ReadOnly = true;
            this.colCompanyName2.Visible = true;
            this.colCompanyName2.VisibleIndex = 5;
            this.colCompanyName2.Width = 171;
            // 
            // gridColumn87
            // 
            this.gridColumn87.Caption = "Billing Requirements Unfulfilled";
            this.gridColumn87.ColumnEdit = this.repositoryItemTextEdit2;
            this.gridColumn87.FieldName = "BillingRequirmentsUnfulfilled";
            this.gridColumn87.Name = "gridColumn87";
            this.gridColumn87.OptionsColumn.AllowEdit = false;
            this.gridColumn87.OptionsColumn.AllowFocus = false;
            this.gridColumn87.OptionsColumn.ReadOnly = true;
            this.gridColumn87.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "BillingRequirmentsUnfulfilled", "{0:0}")});
            this.gridColumn87.Visible = true;
            this.gridColumn87.VisibleIndex = 13;
            this.gridColumn87.Width = 164;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.LookAndFeel.SkinName = "Blue";
            this.repositoryItemTextEdit2.Mask.EditMask = "n0";
            this.repositoryItemTextEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // gridColumn86
            // 
            this.gridColumn86.Caption = "Billing Requirements Fullfilled";
            this.gridColumn86.ColumnEdit = this.repositoryItemTextEdit2;
            this.gridColumn86.FieldName = "BillingRequirmentsFulfilled";
            this.gridColumn86.Name = "gridColumn86";
            this.gridColumn86.OptionsColumn.AllowEdit = false;
            this.gridColumn86.OptionsColumn.AllowFocus = false;
            this.gridColumn86.OptionsColumn.ReadOnly = true;
            this.gridColumn86.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "BillingRequirmentsFulfilled", "{0:0}")});
            this.gridColumn86.Visible = true;
            this.gridColumn86.VisibleIndex = 12;
            this.gridColumn86.Width = 155;
            // 
            // gridColumn89
            // 
            this.gridColumn89.Caption = "Billing Warnings Unauthorised";
            this.gridColumn89.ColumnEdit = this.repositoryItemTextEdit2;
            this.gridColumn89.FieldName = "BillingWarningUnauthorised";
            this.gridColumn89.Name = "gridColumn89";
            this.gridColumn89.OptionsColumn.AllowEdit = false;
            this.gridColumn89.OptionsColumn.AllowFocus = false;
            this.gridColumn89.OptionsColumn.ReadOnly = true;
            this.gridColumn89.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "BillingWarningUnauthorised", "{0:0}")});
            this.gridColumn89.Visible = true;
            this.gridColumn89.VisibleIndex = 15;
            this.gridColumn89.Width = 160;
            // 
            // gridColumn88
            // 
            this.gridColumn88.Caption = "Billing Warnings Authorised";
            this.gridColumn88.ColumnEdit = this.repositoryItemTextEdit2;
            this.gridColumn88.FieldName = "BillingWarningAuthorised";
            this.gridColumn88.Name = "gridColumn88";
            this.gridColumn88.OptionsColumn.AllowEdit = false;
            this.gridColumn88.OptionsColumn.AllowFocus = false;
            this.gridColumn88.OptionsColumn.ReadOnly = true;
            this.gridColumn88.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "BillingWarningAuthorised", "{0:0}")});
            this.gridColumn88.Visible = true;
            this.gridColumn88.VisibleIndex = 14;
            this.gridColumn88.Width = 148;
            // 
            // colBillingRequirmentsUnfulfilled
            // 
            this.colBillingRequirmentsUnfulfilled.Caption = "Billing Requirements Unfulfilled";
            this.colBillingRequirmentsUnfulfilled.ColumnEdit = this.repositoryItemTextEditInteger2;
            this.colBillingRequirmentsUnfulfilled.FieldName = "BillingRequirmentsUnfulfilled";
            this.colBillingRequirmentsUnfulfilled.Name = "colBillingRequirmentsUnfulfilled";
            this.colBillingRequirmentsUnfulfilled.OptionsColumn.AllowEdit = false;
            this.colBillingRequirmentsUnfulfilled.OptionsColumn.AllowFocus = false;
            this.colBillingRequirmentsUnfulfilled.OptionsColumn.ReadOnly = true;
            this.colBillingRequirmentsUnfulfilled.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "BillingRequirmentsUnfulfilled", "{0:0}")});
            this.colBillingRequirmentsUnfulfilled.Visible = true;
            this.colBillingRequirmentsUnfulfilled.VisibleIndex = 13;
            this.colBillingRequirmentsUnfulfilled.Width = 164;
            // 
            // repositoryItemTextEditInteger2
            // 
            this.repositoryItemTextEditInteger2.AutoHeight = false;
            this.repositoryItemTextEditInteger2.LookAndFeel.SkinName = "Blue";
            this.repositoryItemTextEditInteger2.Mask.EditMask = "n0";
            this.repositoryItemTextEditInteger2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditInteger2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditInteger2.Name = "repositoryItemTextEditInteger2";
            // 
            // colBillingRequirmentsFulfilled
            // 
            this.colBillingRequirmentsFulfilled.Caption = "Billing Requirements Fullfilled";
            this.colBillingRequirmentsFulfilled.ColumnEdit = this.repositoryItemTextEditInteger2;
            this.colBillingRequirmentsFulfilled.FieldName = "BillingRequirmentsFulfilled";
            this.colBillingRequirmentsFulfilled.Name = "colBillingRequirmentsFulfilled";
            this.colBillingRequirmentsFulfilled.OptionsColumn.AllowEdit = false;
            this.colBillingRequirmentsFulfilled.OptionsColumn.AllowFocus = false;
            this.colBillingRequirmentsFulfilled.OptionsColumn.ReadOnly = true;
            this.colBillingRequirmentsFulfilled.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "BillingRequirmentsFulfilled", "{0:0}")});
            this.colBillingRequirmentsFulfilled.Visible = true;
            this.colBillingRequirmentsFulfilled.VisibleIndex = 12;
            this.colBillingRequirmentsFulfilled.Width = 155;
            // 
            // colBillingWarningUnauthorised
            // 
            this.colBillingWarningUnauthorised.Caption = "Billing Warnings Unauthorised";
            this.colBillingWarningUnauthorised.ColumnEdit = this.repositoryItemTextEditInteger2;
            this.colBillingWarningUnauthorised.FieldName = "BillingWarningUnauthorised";
            this.colBillingWarningUnauthorised.Name = "colBillingWarningUnauthorised";
            this.colBillingWarningUnauthorised.OptionsColumn.AllowEdit = false;
            this.colBillingWarningUnauthorised.OptionsColumn.AllowFocus = false;
            this.colBillingWarningUnauthorised.OptionsColumn.ReadOnly = true;
            this.colBillingWarningUnauthorised.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "BillingWarningUnauthorised", "{0:0}")});
            this.colBillingWarningUnauthorised.Visible = true;
            this.colBillingWarningUnauthorised.VisibleIndex = 15;
            this.colBillingWarningUnauthorised.Width = 160;
            // 
            // colBillingWarningAuthorised
            // 
            this.colBillingWarningAuthorised.Caption = "Billing Warnings Authorised";
            this.colBillingWarningAuthorised.ColumnEdit = this.repositoryItemTextEditInteger2;
            this.colBillingWarningAuthorised.FieldName = "BillingWarningAuthorised";
            this.colBillingWarningAuthorised.Name = "colBillingWarningAuthorised";
            this.colBillingWarningAuthorised.OptionsColumn.AllowEdit = false;
            this.colBillingWarningAuthorised.OptionsColumn.AllowFocus = false;
            this.colBillingWarningAuthorised.OptionsColumn.ReadOnly = true;
            this.colBillingWarningAuthorised.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "BillingWarningAuthorised", "{0:0}")});
            this.colBillingWarningAuthorised.Visible = true;
            this.colBillingWarningAuthorised.VisibleIndex = 14;
            this.colBillingWarningAuthorised.Width = 148;
            // 
            // colSelfBillingInvoiceHeaderID
            // 
            this.colSelfBillingInvoiceHeaderID.Caption = "Self-Billing Invoice ID";
            this.colSelfBillingInvoiceHeaderID.FieldName = "SelfBillingInvoiceHeaderID";
            this.colSelfBillingInvoiceHeaderID.Name = "colSelfBillingInvoiceHeaderID";
            this.colSelfBillingInvoiceHeaderID.OptionsColumn.AllowEdit = false;
            this.colSelfBillingInvoiceHeaderID.OptionsColumn.AllowFocus = false;
            this.colSelfBillingInvoiceHeaderID.OptionsColumn.ReadOnly = true;
            this.colSelfBillingInvoiceHeaderID.Visible = true;
            this.colSelfBillingInvoiceHeaderID.VisibleIndex = 30;
            this.colSelfBillingInvoiceHeaderID.Width = 119;
            // 
            // gridControlHistoricalExports
            // 
            this.gridControlHistoricalExports.DataSource = this.sp06472OMFinanceSelfBillingHistoricalExportsBindingSource;
            this.gridControlHistoricalExports.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlHistoricalExports.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlHistoricalExports.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlHistoricalExports.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlHistoricalExports.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlHistoricalExports.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlHistoricalExports.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlHistoricalExports.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlHistoricalExports.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlHistoricalExports.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlHistoricalExports.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlHistoricalExports.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlHistoricalExports.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Reload Data", "reload")});
            this.gridControlHistoricalExports.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlHistoricalExports_EmbeddedNavigator_ButtonClick);
            this.gridControlHistoricalExports.Location = new System.Drawing.Point(0, 0);
            this.gridControlHistoricalExports.MainView = this.gridViewHistoricalExports;
            this.gridControlHistoricalExports.MenuManager = this.barManager1;
            this.gridControlHistoricalExports.Name = "gridControlHistoricalExports";
            this.gridControlHistoricalExports.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEditExportFile,
            this.repositoryItemCheckEdit3,
            this.repositoryItemTextEditDateTime3});
            this.gridControlHistoricalExports.Size = new System.Drawing.Size(1183, 521);
            this.gridControlHistoricalExports.TabIndex = 0;
            this.gridControlHistoricalExports.UseEmbeddedNavigator = true;
            this.gridControlHistoricalExports.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewHistoricalExports});
            // 
            // sp06472OMFinanceSelfBillingHistoricalExportsBindingSource
            // 
            this.sp06472OMFinanceSelfBillingHistoricalExportsBindingSource.DataMember = "sp06472_OM_Finance_Self_Billing_Historical_Exports";
            this.sp06472OMFinanceSelfBillingHistoricalExportsBindingSource.DataSource = this.dataSet_OM_Billing;
            // 
            // dataSet_OM_Billing
            // 
            this.dataSet_OM_Billing.DataSetName = "DataSet_OM_Billing";
            this.dataSet_OM_Billing.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "Preview_16x16.png");
            this.imageCollection1.InsertGalleryImage("find_16x16.png", "images/find/find_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/find/find_16x16.png"), 4);
            this.imageCollection1.Images.SetKeyName(4, "find_16x16.png");
            this.imageCollection1.InsertGalleryImage("refresh_16x16.png", "images/actions/refresh_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/refresh_16x16.png"), 5);
            this.imageCollection1.Images.SetKeyName(5, "refresh_16x16.png");
            // 
            // gridViewHistoricalExports
            // 
            this.gridViewHistoricalExports.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSelfBillingFinanceExportID,
            this.colDateExported,
            this.colExportedByStaffID,
            this.colExportFile,
            this.colCompanyID,
            this.colTeamsIncluded,
            this.colStaffIncluded,
            this.colExportedByStaff,
            this.colCompanyName2,
            this.colLinkedVisits,
            this.colSelected1});
            gridFormatRule5.Column = this.colCompanyName2;
            gridFormatRule5.ColumnApplyTo = this.colCompanyName2;
            gridFormatRule5.Name = "FormatMultipleCompanies";
            formatConditionRuleValue5.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue5.PredefinedName = "Red Fill, Red Text";
            formatConditionRuleValue5.Value1 = "Multiple Companies";
            gridFormatRule5.Rule = formatConditionRuleValue5;
            this.gridViewHistoricalExports.FormatRules.Add(gridFormatRule5);
            this.gridViewHistoricalExports.GridControl = this.gridControlHistoricalExports;
            this.gridViewHistoricalExports.Name = "gridViewHistoricalExports";
            this.gridViewHistoricalExports.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridViewHistoricalExports.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewHistoricalExports.OptionsFind.AlwaysVisible = true;
            this.gridViewHistoricalExports.OptionsFind.FindDelay = 2000;
            this.gridViewHistoricalExports.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewHistoricalExports.OptionsLayout.StoreAppearance = true;
            this.gridViewHistoricalExports.OptionsLayout.StoreFormatRules = true;
            this.gridViewHistoricalExports.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewHistoricalExports.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewHistoricalExports.OptionsSelection.MultiSelect = true;
            this.gridViewHistoricalExports.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewHistoricalExports.OptionsView.ColumnAutoWidth = false;
            this.gridViewHistoricalExports.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewHistoricalExports.OptionsView.ShowGroupPanel = false;
            this.gridViewHistoricalExports.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateExported, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridViewHistoricalExports.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewHistoricalExports.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewHistoricalExports.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewHistoricalExports.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewHistoricalExports.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewHistoricalExports.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewHistoricalExports.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewHistoricalExports_MouseUp);
            this.gridViewHistoricalExports.DoubleClick += new System.EventHandler(this.gridViewHistoricalExports_DoubleClick);
            this.gridViewHistoricalExports.GotFocus += new System.EventHandler(this.gridViewHistoricalExports_GotFocus);
            // 
            // colSelfBillingFinanceExportID
            // 
            this.colSelfBillingFinanceExportID.Caption = "Self-Billing Finance Export ID";
            this.colSelfBillingFinanceExportID.FieldName = "SelfBillingFinanceExportID";
            this.colSelfBillingFinanceExportID.Name = "colSelfBillingFinanceExportID";
            this.colSelfBillingFinanceExportID.OptionsColumn.AllowEdit = false;
            this.colSelfBillingFinanceExportID.OptionsColumn.AllowFocus = false;
            this.colSelfBillingFinanceExportID.OptionsColumn.ReadOnly = true;
            this.colSelfBillingFinanceExportID.Width = 156;
            // 
            // colDateExported
            // 
            this.colDateExported.Caption = "Date Exported";
            this.colDateExported.ColumnEdit = this.repositoryItemTextEditDateTime3;
            this.colDateExported.FieldName = "DateExported";
            this.colDateExported.Name = "colDateExported";
            this.colDateExported.OptionsColumn.AllowEdit = false;
            this.colDateExported.OptionsColumn.AllowFocus = false;
            this.colDateExported.OptionsColumn.ReadOnly = true;
            this.colDateExported.Visible = true;
            this.colDateExported.VisibleIndex = 0;
            this.colDateExported.Width = 120;
            // 
            // repositoryItemTextEditDateTime3
            // 
            this.repositoryItemTextEditDateTime3.AutoHeight = false;
            this.repositoryItemTextEditDateTime3.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime3.Name = "repositoryItemTextEditDateTime3";
            // 
            // colExportedByStaffID
            // 
            this.colExportedByStaffID.Caption = "Exported By Staff ID";
            this.colExportedByStaffID.FieldName = "ExportedByStaffID";
            this.colExportedByStaffID.Name = "colExportedByStaffID";
            this.colExportedByStaffID.OptionsColumn.AllowEdit = false;
            this.colExportedByStaffID.OptionsColumn.AllowFocus = false;
            this.colExportedByStaffID.OptionsColumn.ReadOnly = true;
            this.colExportedByStaffID.Width = 119;
            // 
            // colExportFile
            // 
            this.colExportFile.Caption = "Export File";
            this.colExportFile.ColumnEdit = this.repositoryItemHyperLinkEditExportFile;
            this.colExportFile.FieldName = "ExportFile";
            this.colExportFile.Name = "colExportFile";
            this.colExportFile.OptionsColumn.ReadOnly = true;
            this.colExportFile.Visible = true;
            this.colExportFile.VisibleIndex = 2;
            this.colExportFile.Width = 354;
            // 
            // repositoryItemHyperLinkEditExportFile
            // 
            this.repositoryItemHyperLinkEditExportFile.AutoHeight = false;
            this.repositoryItemHyperLinkEditExportFile.Name = "repositoryItemHyperLinkEditExportFile";
            this.repositoryItemHyperLinkEditExportFile.SingleClick = true;
            this.repositoryItemHyperLinkEditExportFile.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditExportFile_OpenLink);
            // 
            // colCompanyID
            // 
            this.colCompanyID.Caption = "Company ID";
            this.colCompanyID.FieldName = "CompanyID";
            this.colCompanyID.Name = "colCompanyID";
            this.colCompanyID.OptionsColumn.AllowEdit = false;
            this.colCompanyID.OptionsColumn.AllowFocus = false;
            this.colCompanyID.OptionsColumn.ReadOnly = true;
            this.colCompanyID.Width = 78;
            // 
            // colTeamsIncluded
            // 
            this.colTeamsIncluded.Caption = "Teams Included";
            this.colTeamsIncluded.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colTeamsIncluded.FieldName = "TeamsIncluded";
            this.colTeamsIncluded.Name = "colTeamsIncluded";
            this.colTeamsIncluded.OptionsColumn.AllowEdit = false;
            this.colTeamsIncluded.OptionsColumn.AllowFocus = false;
            this.colTeamsIncluded.OptionsColumn.ReadOnly = true;
            this.colTeamsIncluded.Visible = true;
            this.colTeamsIncluded.VisibleIndex = 3;
            this.colTeamsIncluded.Width = 94;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = 1;
            this.repositoryItemCheckEdit3.ValueUnchecked = 0;
            // 
            // colStaffIncluded
            // 
            this.colStaffIncluded.Caption = "Directly Employed Included";
            this.colStaffIncluded.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colStaffIncluded.FieldName = "StaffIncluded";
            this.colStaffIncluded.Name = "colStaffIncluded";
            this.colStaffIncluded.OptionsColumn.AllowEdit = false;
            this.colStaffIncluded.OptionsColumn.AllowFocus = false;
            this.colStaffIncluded.OptionsColumn.ReadOnly = true;
            this.colStaffIncluded.Visible = true;
            this.colStaffIncluded.VisibleIndex = 4;
            this.colStaffIncluded.Width = 148;
            // 
            // colExportedByStaff
            // 
            this.colExportedByStaff.Caption = "Exported By";
            this.colExportedByStaff.FieldName = "ExportedByStaff";
            this.colExportedByStaff.Name = "colExportedByStaff";
            this.colExportedByStaff.OptionsColumn.AllowEdit = false;
            this.colExportedByStaff.OptionsColumn.AllowFocus = false;
            this.colExportedByStaff.OptionsColumn.ReadOnly = true;
            this.colExportedByStaff.Visible = true;
            this.colExportedByStaff.VisibleIndex = 1;
            this.colExportedByStaff.Width = 119;
            // 
            // colLinkedVisits
            // 
            this.colLinkedVisits.Caption = "Linked Visits";
            this.colLinkedVisits.FieldName = "LinkedVisits";
            this.colLinkedVisits.Name = "colLinkedVisits";
            this.colLinkedVisits.OptionsColumn.AllowEdit = false;
            this.colLinkedVisits.OptionsColumn.AllowFocus = false;
            this.colLinkedVisits.OptionsColumn.ReadOnly = true;
            this.colLinkedVisits.Visible = true;
            this.colLinkedVisits.VisibleIndex = 6;
            this.colLinkedVisits.Width = 76;
            // 
            // colSelected1
            // 
            this.colSelected1.Caption = "Selected";
            this.colSelected1.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colSelected1.FieldName = "Selected";
            this.colSelected1.Name = "colSelected1";
            this.colSelected1.Width = 60;
            // 
            // dataSet_OM_Job
            // 
            this.dataSet_OM_Job.DataSetName = "DataSet_OM_Job";
            this.dataSet_OM_Job.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.dataSet_AT;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // repositoryItemCheckEditShowActiveEmployeesOnly
            // 
            this.repositoryItemCheckEditShowActiveEmployeesOnly.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.Appearance.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.AutoHeight = false;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.Caption = "";
            this.repositoryItemCheckEditShowActiveEmployeesOnly.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.Name = "repositoryItemCheckEditShowActiveEmployeesOnly";
            this.repositoryItemCheckEditShowActiveEmployeesOnly.ValueChecked = 1;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.ValueUnchecked = 0;
            // 
            // repositoryItemPopupContainerEdit2
            // 
            this.repositoryItemPopupContainerEdit2.AutoHeight = false;
            this.repositoryItemPopupContainerEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit2.Name = "repositoryItemPopupContainerEdit2";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl1.Collapsed = true;
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 44);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl1.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl1.Panel1.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Historical Exports";
            this.splitContainerControl1.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl1.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel2.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl1.Panel2.Controls.Add(this.gridControlExportVisits);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "Visits Linked To Export";
            this.splitContainerControl1.Size = new System.Drawing.Size(1214, 524);
            this.splitContainerControl1.SplitterPosition = 571;
            this.splitContainerControl1.TabIndex = 4;
            this.splitContainerControl1.Text = "splitContainerControl1";
            this.splitContainerControl1.SplitGroupPanelCollapsed += new DevExpress.XtraEditors.SplitGroupPanelCollapsedEventHandler(this.splitContainerControl1_SplitGroupPanelCollapsed);
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.gridControlHistoricalExports;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControlHistoricalExports);
            this.gridSplitContainer1.Size = new System.Drawing.Size(1183, 521);
            this.gridSplitContainer1.TabIndex = 2;
            // 
            // gridControlExportVisits
            // 
            this.gridControlExportVisits.DataSource = this.sp06473OMFinanceSelfBillingManagerVisitsOnExportBindingSource;
            this.gridControlExportVisits.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlExportVisits.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlExportVisits.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlExportVisits.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlExportVisits.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlExportVisits.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlExportVisits.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlExportVisits.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlExportVisits.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlExportVisits.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlExportVisits.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlExportVisits.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlExportVisits.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Reload Data", "reload")});
            this.gridControlExportVisits.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlExportVisits_EmbeddedNavigator_ButtonClick);
            this.gridControlExportVisits.Location = new System.Drawing.Point(0, 0);
            this.gridControlExportVisits.MainView = this.gridViewExportVisits;
            this.gridControlExportVisits.MenuManager = this.barManager1;
            this.gridControlExportVisits.Name = "gridControlExportVisits";
            this.gridControlExportVisits.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit2,
            this.repositoryItemTextEdit3,
            this.repositoryItemTextEdit4,
            this.repositoryItemMemoExEdit1,
            this.repositoryItemTextEdit5,
            this.repositoryItemCheckEdit4,
            this.repositoryItemTextEdit6,
            this.repositoryItemHyperLinkEdit1,
            this.repositoryItemHyperLinkEditClientSignature2,
            this.repositoryItemHyperLinkEditSelfBillingInvoicePDF,
            this.repositoryItemHyperLinkEditClientInvoiceID2,
            this.repositoryItemTextEdit7});
            this.gridControlExportVisits.Size = new System.Drawing.Size(0, 0);
            this.gridControlExportVisits.TabIndex = 6;
            this.gridControlExportVisits.UseEmbeddedNavigator = true;
            this.gridControlExportVisits.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExportVisits});
            // 
            // sp06473OMFinanceSelfBillingManagerVisitsOnExportBindingSource
            // 
            this.sp06473OMFinanceSelfBillingManagerVisitsOnExportBindingSource.DataMember = "sp06473_OM_Finance_Self_Billing_Manager_Visits_On_Export";
            this.sp06473OMFinanceSelfBillingManagerVisitsOnExportBindingSource.DataSource = this.dataSet_OM_Billing;
            // 
            // gridViewExportVisits
            // 
            this.gridViewExportVisits.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27,
            this.gridColumn28,
            this.gridColumn29,
            this.gridColumn30,
            this.gridColumn31,
            this.gridColumn32,
            this.gridColumn33,
            this.gridColumn34,
            this.gridColumn35,
            this.gridColumn36,
            this.gridColumn37,
            this.gridColumn38,
            this.gridColumn39,
            this.gridColumn40,
            this.gridColumn41,
            this.gridColumn42,
            this.gridColumn43,
            this.gridColumn44,
            this.gridColumn45,
            this.gridColumn46,
            this.gridColumn47,
            this.gridColumn48,
            this.gridColumn49,
            this.gridColumn50,
            this.gridColumn51,
            this.gridColumn52,
            this.gridColumn53,
            this.gridColumn54,
            this.gridColumn55,
            this.gridColumn56,
            this.gridColumn57,
            this.gridColumn58,
            this.gridColumn59,
            this.gridColumn60,
            this.gridColumn61,
            this.gridColumn62,
            this.gridColumn63,
            this.gridColumn64,
            this.gridColumn65,
            this.gridColumn66,
            this.gridColumn67,
            this.gridColumn68,
            this.gridColumn69,
            this.gridColumn70,
            this.gridColumn71,
            this.gridColumn72,
            this.gridColumn73,
            this.gridColumn74,
            this.gridColumn75,
            this.gridColumn76,
            this.gridColumn77,
            this.gridColumn78,
            this.gridColumn79,
            this.gridColumn80,
            this.gridColumn81,
            this.gridColumn82,
            this.gridColumn83,
            this.gridColumn84,
            this.gridColumn85,
            this.gridColumn86,
            this.gridColumn87,
            this.gridColumn88,
            this.gridColumn89,
            this.gridColumn90,
            this.gridColumn91,
            this.gridColumn92,
            this.gridColumn93,
            this.gridColumn94,
            this.gridColumn95,
            this.gridColumn96,
            this.gridColumn97,
            this.gridColumn98,
            this.gridColumn99,
            this.gridColumn100,
            this.gridColumn101});
            gridFormatRule6.Column = this.gridColumn87;
            gridFormatRule6.ColumnApplyTo = this.gridColumn86;
            gridFormatRule6.Name = "FormatBillingRequirementFulfilled";
            formatConditionRuleValue6.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue6.PredefinedName = "Green Fill, Green Text";
            formatConditionRuleValue6.Value1 = 0;
            gridFormatRule6.Rule = formatConditionRuleValue6;
            gridFormatRule7.Column = this.gridColumn87;
            gridFormatRule7.ColumnApplyTo = this.gridColumn87;
            gridFormatRule7.Name = "FormatBillingRequirementUnfulfilled";
            formatConditionRuleValue7.Condition = DevExpress.XtraEditors.FormatCondition.Greater;
            formatConditionRuleValue7.PredefinedName = "Red Fill, Red Text";
            formatConditionRuleValue7.Value1 = 0;
            gridFormatRule7.Rule = formatConditionRuleValue7;
            gridFormatRule8.Column = this.gridColumn89;
            gridFormatRule8.ColumnApplyTo = this.gridColumn88;
            gridFormatRule8.Name = "FormatBillingWarningAuthorised";
            formatConditionRuleValue8.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue8.PredefinedName = "Green Fill, Green Text";
            formatConditionRuleValue8.Value1 = 0;
            gridFormatRule8.Rule = formatConditionRuleValue8;
            gridFormatRule9.Column = this.gridColumn89;
            gridFormatRule9.ColumnApplyTo = this.gridColumn89;
            gridFormatRule9.Name = "FormatBillingWarningUnauthorised";
            formatConditionRuleValue9.Condition = DevExpress.XtraEditors.FormatCondition.Greater;
            formatConditionRuleValue9.PredefinedName = "Red Fill, Red Text";
            formatConditionRuleValue9.Value1 = 0;
            gridFormatRule9.Rule = formatConditionRuleValue9;
            this.gridViewExportVisits.FormatRules.Add(gridFormatRule6);
            this.gridViewExportVisits.FormatRules.Add(gridFormatRule7);
            this.gridViewExportVisits.FormatRules.Add(gridFormatRule8);
            this.gridViewExportVisits.FormatRules.Add(gridFormatRule9);
            this.gridViewExportVisits.GridControl = this.gridControlExportVisits;
            this.gridViewExportVisits.GroupCount = 4;
            this.gridViewExportVisits.Name = "gridViewExportVisits";
            this.gridViewExportVisits.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewExportVisits.OptionsFind.AlwaysVisible = true;
            this.gridViewExportVisits.OptionsFind.FindDelay = 2000;
            this.gridViewExportVisits.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewExportVisits.OptionsLayout.StoreAppearance = true;
            this.gridViewExportVisits.OptionsLayout.StoreFormatRules = true;
            this.gridViewExportVisits.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewExportVisits.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewExportVisits.OptionsSelection.MultiSelect = true;
            this.gridViewExportVisits.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewExportVisits.OptionsView.ColumnAutoWidth = false;
            this.gridViewExportVisits.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewExportVisits.OptionsView.ShowGroupPanel = false;
            this.gridViewExportVisits.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn100, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn90, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn29, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn97, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn33, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn4, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewExportVisits.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewExportVisits_CustomRowCellEdit);
            this.gridViewExportVisits.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewExportVisits.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewExportVisits.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewExportVisits.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewExportVisits_ShowingEditor);
            this.gridViewExportVisits.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridViewExportVisits_CustomFilterDialog);
            this.gridViewExportVisits.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridViewExportVisits_FilterEditorCreated);
            this.gridViewExportVisits.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewExportVisits.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewExportVisits_MouseUp);
            this.gridViewExportVisits.DoubleClick += new System.EventHandler(this.gridViewExportVisits_DoubleClick);
            this.gridViewExportVisits.GotFocus += new System.EventHandler(this.gridViewExportVisits_GotFocus);
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Visit ID";
            this.gridColumn2.FieldName = "VisitID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Site Contract ID";
            this.gridColumn3.FieldName = "SiteContractID";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Width = 98;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Visit #";
            this.gridColumn4.ColumnEdit = this.repositoryItemTextEdit2;
            this.gridColumn4.FieldName = "VisitNumber";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 1;
            this.gridColumn4.Width = 95;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Created By Staff ID";
            this.gridColumn5.FieldName = "CreatedByStaffID";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Width = 116;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Expected Start";
            this.gridColumn6.ColumnEdit = this.repositoryItemTextEdit3;
            this.gridColumn6.FieldName = "ExpectedStartDate";
            this.gridColumn6.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 8;
            this.gridColumn6.Width = 104;
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEdit3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Expected End";
            this.gridColumn7.ColumnEdit = this.repositoryItemTextEdit3;
            this.gridColumn7.FieldName = "ExpectedEndDate";
            this.gridColumn7.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 9;
            this.gridColumn7.Width = 100;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Start";
            this.gridColumn8.ColumnEdit = this.repositoryItemTextEdit3;
            this.gridColumn8.FieldName = "StartDate";
            this.gridColumn8.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 3;
            this.gridColumn8.Width = 100;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "End";
            this.gridColumn9.ColumnEdit = this.repositoryItemTextEdit3;
            this.gridColumn9.FieldName = "EndDate";
            this.gridColumn9.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 4;
            this.gridColumn9.Width = 100;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Cost Calculation ID";
            this.gridColumn10.FieldName = "CostCalculationLevel";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Width = 140;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Visit Cost";
            this.gridColumn11.ColumnEdit = this.repositoryItemTextEdit4;
            this.gridColumn11.FieldName = "VisitCost";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VisitCost", "{0:c}")});
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 26;
            // 
            // repositoryItemTextEdit4
            // 
            this.repositoryItemTextEdit4.AutoHeight = false;
            this.repositoryItemTextEdit4.Mask.EditMask = "c";
            this.repositoryItemTextEdit4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit4.Name = "repositoryItemTextEdit4";
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Visit Sell";
            this.gridColumn12.ColumnEdit = this.repositoryItemTextEdit4;
            this.gridColumn12.FieldName = "VisitSell";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VisitSell", "{0:c}")});
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 27;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Remarks";
            this.gridColumn13.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.gridColumn13.FieldName = "Remarks";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 64;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Start Latitude";
            this.gridColumn14.ColumnEdit = this.repositoryItemTextEdit5;
            this.gridColumn14.FieldName = "StartLatitude";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 35;
            this.gridColumn14.Width = 87;
            // 
            // repositoryItemTextEdit5
            // 
            this.repositoryItemTextEdit5.AutoHeight = false;
            this.repositoryItemTextEdit5.Mask.EditMask = "f8";
            this.repositoryItemTextEdit5.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit5.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit5.Name = "repositoryItemTextEdit5";
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Start Longitude";
            this.gridColumn15.ColumnEdit = this.repositoryItemTextEdit5;
            this.gridColumn15.FieldName = "StartLongitude";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 36;
            this.gridColumn15.Width = 95;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Finish Latitude";
            this.gridColumn16.ColumnEdit = this.repositoryItemTextEdit5;
            this.gridColumn16.FieldName = "FinishLatitude";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowFocus = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 37;
            this.gridColumn16.Width = 90;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Finish Longitude";
            this.gridColumn17.ColumnEdit = this.repositoryItemTextEdit5;
            this.gridColumn17.FieldName = "FinishLongitude";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.AllowFocus = false;
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 38;
            this.gridColumn17.Width = 98;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Cost Calculation";
            this.gridColumn18.FieldName = "CostCalculationLevelDescription";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.AllowFocus = false;
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 24;
            this.gridColumn18.Width = 100;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Client Contract ID";
            this.gridColumn19.FieldName = "ClientContractID";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.OptionsColumn.AllowFocus = false;
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            this.gridColumn19.Width = 107;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Site ID";
            this.gridColumn20.FieldName = "SiteID";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.AllowEdit = false;
            this.gridColumn20.OptionsColumn.AllowFocus = false;
            this.gridColumn20.OptionsColumn.ReadOnly = true;
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 31;
            this.gridColumn20.Width = 51;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Site Contract Value";
            this.gridColumn21.ColumnEdit = this.repositoryItemTextEdit4;
            this.gridColumn21.FieldName = "SiteContractValue";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.AllowEdit = false;
            this.gridColumn21.OptionsColumn.AllowFocus = false;
            this.gridColumn21.OptionsColumn.ReadOnly = true;
            this.gridColumn21.Width = 113;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Client ID";
            this.gridColumn22.FieldName = "ClientID";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.AllowEdit = false;
            this.gridColumn22.OptionsColumn.AllowFocus = false;
            this.gridColumn22.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Contract Director ID";
            this.gridColumn23.FieldName = "ContractDirectorID";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.AllowEdit = false;
            this.gridColumn23.OptionsColumn.AllowFocus = false;
            this.gridColumn23.OptionsColumn.ReadOnly = true;
            this.gridColumn23.Width = 118;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Client Contract Start";
            this.gridColumn24.ColumnEdit = this.repositoryItemTextEdit3;
            this.gridColumn24.FieldName = "ClientContractStartDate";
            this.gridColumn24.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.AllowEdit = false;
            this.gridColumn24.OptionsColumn.AllowFocus = false;
            this.gridColumn24.OptionsColumn.ReadOnly = true;
            this.gridColumn24.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn24.Width = 120;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Client Contract End";
            this.gridColumn25.ColumnEdit = this.repositoryItemTextEdit3;
            this.gridColumn25.FieldName = "ClientContractEndDate";
            this.gridColumn25.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.OptionsColumn.AllowEdit = false;
            this.gridColumn25.OptionsColumn.AllowFocus = false;
            this.gridColumn25.OptionsColumn.ReadOnly = true;
            this.gridColumn25.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn25.Width = 114;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Client Contract Active";
            this.gridColumn26.ColumnEdit = this.repositoryItemCheckEdit4;
            this.gridColumn26.FieldName = "ClientContractActive";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.OptionsColumn.AllowEdit = false;
            this.gridColumn26.OptionsColumn.AllowFocus = false;
            this.gridColumn26.OptionsColumn.ReadOnly = true;
            this.gridColumn26.Width = 126;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Caption = "Check";
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            this.repositoryItemCheckEdit4.ValueChecked = 1;
            this.repositoryItemCheckEdit4.ValueUnchecked = 0;
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Client Contract Value";
            this.gridColumn27.ColumnEdit = this.repositoryItemTextEdit4;
            this.gridColumn27.FieldName = "ClientContractValue";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.OptionsColumn.AllowEdit = false;
            this.gridColumn27.OptionsColumn.AllowFocus = false;
            this.gridColumn27.OptionsColumn.ReadOnly = true;
            this.gridColumn27.Width = 122;
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "Finance Client Code";
            this.gridColumn28.FieldName = "FinanceClientCode";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.OptionsColumn.AllowEdit = false;
            this.gridColumn28.OptionsColumn.AllowFocus = false;
            this.gridColumn28.OptionsColumn.ReadOnly = true;
            this.gridColumn28.Width = 116;
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "Client Name";
            this.gridColumn29.FieldName = "ClientName";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.OptionsColumn.AllowEdit = false;
            this.gridColumn29.OptionsColumn.AllowFocus = false;
            this.gridColumn29.OptionsColumn.ReadOnly = true;
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 1;
            this.gridColumn29.Width = 174;
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "Contract Type";
            this.gridColumn30.FieldName = "ContractType";
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.OptionsColumn.AllowEdit = false;
            this.gridColumn30.OptionsColumn.AllowFocus = false;
            this.gridColumn30.OptionsColumn.ReadOnly = true;
            this.gridColumn30.Visible = true;
            this.gridColumn30.VisibleIndex = 40;
            this.gridColumn30.Width = 117;
            // 
            // gridColumn31
            // 
            this.gridColumn31.Caption = "Contract Status";
            this.gridColumn31.FieldName = "ContractStatus";
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.OptionsColumn.AllowEdit = false;
            this.gridColumn31.OptionsColumn.AllowFocus = false;
            this.gridColumn31.OptionsColumn.ReadOnly = true;
            this.gridColumn31.Visible = true;
            this.gridColumn31.VisibleIndex = 41;
            this.gridColumn31.Width = 117;
            // 
            // gridColumn32
            // 
            this.gridColumn32.Caption = "Contract Director";
            this.gridColumn32.FieldName = "ContractDirector";
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.OptionsColumn.AllowEdit = false;
            this.gridColumn32.OptionsColumn.AllowFocus = false;
            this.gridColumn32.OptionsColumn.ReadOnly = true;
            this.gridColumn32.Visible = true;
            this.gridColumn32.VisibleIndex = 42;
            this.gridColumn32.Width = 122;
            // 
            // gridColumn33
            // 
            this.gridColumn33.Caption = "Site Name";
            this.gridColumn33.FieldName = "SiteName";
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.OptionsColumn.AllowEdit = false;
            this.gridColumn33.OptionsColumn.AllowFocus = false;
            this.gridColumn33.OptionsColumn.ReadOnly = true;
            this.gridColumn33.Visible = true;
            this.gridColumn33.VisibleIndex = 0;
            this.gridColumn33.Width = 270;
            // 
            // gridColumn34
            // 
            this.gridColumn34.Caption = "Linked To Site Contract";
            this.gridColumn34.ColumnEdit = this.repositoryItemTextEdit6;
            this.gridColumn34.FieldName = "LinkedToParent";
            this.gridColumn34.Name = "gridColumn34";
            this.gridColumn34.OptionsColumn.AllowEdit = false;
            this.gridColumn34.OptionsColumn.AllowFocus = false;
            this.gridColumn34.OptionsColumn.ReadOnly = true;
            this.gridColumn34.Width = 333;
            // 
            // repositoryItemTextEdit6
            // 
            this.repositoryItemTextEdit6.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTextEdit6.AutoHeight = false;
            this.repositoryItemTextEdit6.Name = "repositoryItemTextEdit6";
            // 
            // gridColumn35
            // 
            this.gridColumn35.Caption = "Contract Description";
            this.gridColumn35.FieldName = "ContractDescription";
            this.gridColumn35.Name = "gridColumn35";
            this.gridColumn35.OptionsColumn.AllowEdit = false;
            this.gridColumn35.OptionsColumn.AllowFocus = false;
            this.gridColumn35.OptionsColumn.ReadOnly = true;
            this.gridColumn35.Visible = true;
            this.gridColumn35.VisibleIndex = 11;
            this.gridColumn35.Width = 225;
            // 
            // gridColumn36
            // 
            this.gridColumn36.Caption = "Site Postcode";
            this.gridColumn36.FieldName = "SitePostcode";
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.OptionsColumn.AllowEdit = false;
            this.gridColumn36.OptionsColumn.AllowFocus = false;
            this.gridColumn36.OptionsColumn.ReadOnly = true;
            this.gridColumn36.Width = 86;
            // 
            // gridColumn37
            // 
            this.gridColumn37.Caption = "Site Latitude";
            this.gridColumn37.ColumnEdit = this.repositoryItemTextEdit5;
            this.gridColumn37.FieldName = "SiteLocationX";
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.OptionsColumn.AllowEdit = false;
            this.gridColumn37.OptionsColumn.AllowFocus = false;
            this.gridColumn37.OptionsColumn.ReadOnly = true;
            this.gridColumn37.Width = 81;
            // 
            // gridColumn38
            // 
            this.gridColumn38.Caption = "Site Longitude";
            this.gridColumn38.ColumnEdit = this.repositoryItemTextEdit5;
            this.gridColumn38.FieldName = "SiteLocationY";
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.OptionsColumn.AllowEdit = false;
            this.gridColumn38.OptionsColumn.AllowFocus = false;
            this.gridColumn38.OptionsColumn.ReadOnly = true;
            this.gridColumn38.Width = 89;
            // 
            // gridColumn39
            // 
            this.gridColumn39.Caption = "Linked Jobs Total";
            this.gridColumn39.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.gridColumn39.FieldName = "LinkedJobCount";
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.OptionsColumn.ReadOnly = true;
            this.gridColumn39.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "LinkedJobCount", "{0:n}")});
            this.gridColumn39.Width = 103;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            this.repositoryItemHyperLinkEdit1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit1_OpenLink);
            // 
            // gridColumn40
            // 
            this.gridColumn40.Caption = "Linked Jobs Outstanding";
            this.gridColumn40.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.gridColumn40.FieldName = "LinkedOutstandingJobCount";
            this.gridColumn40.Name = "gridColumn40";
            this.gridColumn40.OptionsColumn.ReadOnly = true;
            this.gridColumn40.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "LinkedOutstandingJobCount", "{0:n}")});
            this.gridColumn40.Visible = true;
            this.gridColumn40.VisibleIndex = 43;
            this.gridColumn40.Width = 138;
            // 
            // gridColumn41
            // 
            this.gridColumn41.Caption = "Linked Jobs Completed";
            this.gridColumn41.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.gridColumn41.FieldName = "LinkedCompletedJobCount";
            this.gridColumn41.Name = "gridColumn41";
            this.gridColumn41.OptionsColumn.ReadOnly = true;
            this.gridColumn41.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "LinkedCompletedJobCount", "{0:n}")});
            this.gridColumn41.Visible = true;
            this.gridColumn41.VisibleIndex = 44;
            this.gridColumn41.Width = 130;
            // 
            // gridColumn42
            // 
            this.gridColumn42.Caption = "Created By";
            this.gridColumn42.FieldName = "CreatedByStaffName";
            this.gridColumn42.Name = "gridColumn42";
            this.gridColumn42.OptionsColumn.AllowEdit = false;
            this.gridColumn42.OptionsColumn.AllowFocus = false;
            this.gridColumn42.OptionsColumn.ReadOnly = true;
            this.gridColumn42.Visible = true;
            this.gridColumn42.VisibleIndex = 29;
            this.gridColumn42.Width = 122;
            // 
            // gridColumn43
            // 
            this.gridColumn43.Caption = "Site Address";
            this.gridColumn43.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.gridColumn43.FieldName = "SiteAddress";
            this.gridColumn43.Name = "gridColumn43";
            this.gridColumn43.OptionsColumn.ReadOnly = true;
            this.gridColumn43.Visible = true;
            this.gridColumn43.VisibleIndex = 34;
            this.gridColumn43.Width = 125;
            // 
            // gridColumn44
            // 
            this.gridColumn44.Caption = "System Status";
            this.gridColumn44.FieldName = "VisitStatus";
            this.gridColumn44.Name = "gridColumn44";
            this.gridColumn44.OptionsColumn.AllowEdit = false;
            this.gridColumn44.OptionsColumn.AllowFocus = false;
            this.gridColumn44.OptionsColumn.ReadOnly = true;
            this.gridColumn44.Visible = true;
            this.gridColumn44.VisibleIndex = 19;
            this.gridColumn44.Width = 130;
            // 
            // gridColumn45
            // 
            this.gridColumn45.Caption = "System Status ID";
            this.gridColumn45.FieldName = "VisitStatusID";
            this.gridColumn45.Name = "gridColumn45";
            this.gridColumn45.OptionsColumn.AllowEdit = false;
            this.gridColumn45.OptionsColumn.AllowFocus = false;
            this.gridColumn45.OptionsColumn.ReadOnly = true;
            this.gridColumn45.Width = 88;
            // 
            // gridColumn46
            // 
            this.gridColumn46.Caption = "Visit Status";
            this.gridColumn46.FieldName = "Timeliness";
            this.gridColumn46.Name = "gridColumn46";
            this.gridColumn46.OptionsColumn.AllowEdit = false;
            this.gridColumn46.OptionsColumn.AllowFocus = false;
            this.gridColumn46.OptionsColumn.ReadOnly = true;
            this.gridColumn46.Visible = true;
            this.gridColumn46.VisibleIndex = 20;
            this.gridColumn46.Width = 89;
            // 
            // gridColumn47
            // 
            this.gridColumn47.Caption = "Work Number";
            this.gridColumn47.FieldName = "WorkNumber";
            this.gridColumn47.Name = "gridColumn47";
            this.gridColumn47.OptionsColumn.AllowEdit = false;
            this.gridColumn47.OptionsColumn.AllowFocus = false;
            this.gridColumn47.OptionsColumn.ReadOnly = true;
            this.gridColumn47.Visible = true;
            this.gridColumn47.VisibleIndex = 23;
            this.gridColumn47.Width = 86;
            // 
            // gridColumn48
            // 
            this.gridColumn48.Caption = "Visit Category";
            this.gridColumn48.FieldName = "VisitCategory";
            this.gridColumn48.Name = "gridColumn48";
            this.gridColumn48.OptionsColumn.AllowEdit = false;
            this.gridColumn48.OptionsColumn.AllowFocus = false;
            this.gridColumn48.OptionsColumn.ReadOnly = true;
            this.gridColumn48.Visible = true;
            this.gridColumn48.VisibleIndex = 17;
            this.gridColumn48.Width = 130;
            // 
            // gridColumn49
            // 
            this.gridColumn49.Caption = "Visit Category ID";
            this.gridColumn49.FieldName = "VisitCategoryID";
            this.gridColumn49.Name = "gridColumn49";
            this.gridColumn49.OptionsColumn.AllowEdit = false;
            this.gridColumn49.OptionsColumn.AllowFocus = false;
            this.gridColumn49.OptionsColumn.ReadOnly = true;
            this.gridColumn49.Width = 102;
            // 
            // gridColumn50
            // 
            this.gridColumn50.Caption = "Issue Found";
            this.gridColumn50.ColumnEdit = this.repositoryItemCheckEdit4;
            this.gridColumn50.FieldName = "IssueFound";
            this.gridColumn50.Name = "gridColumn50";
            this.gridColumn50.OptionsColumn.AllowEdit = false;
            this.gridColumn50.OptionsColumn.AllowFocus = false;
            this.gridColumn50.OptionsColumn.ReadOnly = true;
            this.gridColumn50.Visible = true;
            this.gridColumn50.VisibleIndex = 51;
            this.gridColumn50.Width = 80;
            // 
            // gridColumn51
            // 
            this.gridColumn51.Caption = "Issue Type";
            this.gridColumn51.FieldName = "IssueType";
            this.gridColumn51.Name = "gridColumn51";
            this.gridColumn51.OptionsColumn.AllowEdit = false;
            this.gridColumn51.OptionsColumn.AllowFocus = false;
            this.gridColumn51.OptionsColumn.ReadOnly = true;
            this.gridColumn51.Visible = true;
            this.gridColumn51.VisibleIndex = 52;
            // 
            // gridColumn52
            // 
            this.gridColumn52.Caption = "Issue Type ID";
            this.gridColumn52.FieldName = "IssueTypeID";
            this.gridColumn52.Name = "gridColumn52";
            this.gridColumn52.OptionsColumn.AllowEdit = false;
            this.gridColumn52.OptionsColumn.AllowFocus = false;
            this.gridColumn52.OptionsColumn.ReadOnly = true;
            this.gridColumn52.Width = 88;
            // 
            // gridColumn53
            // 
            this.gridColumn53.Caption = "Issue Remarks";
            this.gridColumn53.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.gridColumn53.FieldName = "IssueRemarks";
            this.gridColumn53.Name = "gridColumn53";
            this.gridColumn53.OptionsColumn.ReadOnly = true;
            this.gridColumn53.Visible = true;
            this.gridColumn53.VisibleIndex = 53;
            this.gridColumn53.Width = 91;
            // 
            // gridColumn54
            // 
            this.gridColumn54.Caption = "Client Signature";
            this.gridColumn54.ColumnEdit = this.repositoryItemHyperLinkEditClientSignature2;
            this.gridColumn54.FieldName = "ClientSignaturePath";
            this.gridColumn54.Name = "gridColumn54";
            this.gridColumn54.OptionsColumn.ReadOnly = true;
            this.gridColumn54.Visible = true;
            this.gridColumn54.VisibleIndex = 47;
            this.gridColumn54.Width = 97;
            // 
            // repositoryItemHyperLinkEditClientSignature2
            // 
            this.repositoryItemHyperLinkEditClientSignature2.AutoHeight = false;
            this.repositoryItemHyperLinkEditClientSignature2.Name = "repositoryItemHyperLinkEditClientSignature2";
            this.repositoryItemHyperLinkEditClientSignature2.SingleClick = true;
            this.repositoryItemHyperLinkEditClientSignature2.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditClientSignature2_OpenLink);
            // 
            // gridColumn55
            // 
            this.gridColumn55.Caption = "Client Images Folder";
            this.gridColumn55.FieldName = "ImagesFolderOM";
            this.gridColumn55.Name = "gridColumn55";
            this.gridColumn55.OptionsColumn.AllowEdit = false;
            this.gridColumn55.OptionsColumn.AllowFocus = false;
            this.gridColumn55.OptionsColumn.ReadOnly = true;
            this.gridColumn55.Width = 119;
            // 
            // gridColumn56
            // 
            this.gridColumn56.Caption = "Rework";
            this.gridColumn56.ColumnEdit = this.repositoryItemCheckEdit4;
            this.gridColumn56.FieldName = "Rework";
            this.gridColumn56.Name = "gridColumn56";
            this.gridColumn56.OptionsColumn.AllowEdit = false;
            this.gridColumn56.OptionsColumn.AllowFocus = false;
            this.gridColumn56.OptionsColumn.ReadOnly = true;
            this.gridColumn56.Visible = true;
            this.gridColumn56.VisibleIndex = 54;
            this.gridColumn56.Width = 57;
            // 
            // gridColumn57
            // 
            this.gridColumn57.Caption = "Extra Work Remarks";
            this.gridColumn57.FieldName = "ExtraWorkComments";
            this.gridColumn57.Name = "gridColumn57";
            this.gridColumn57.OptionsColumn.AllowEdit = false;
            this.gridColumn57.OptionsColumn.AllowFocus = false;
            this.gridColumn57.OptionsColumn.ReadOnly = true;
            this.gridColumn57.Visible = true;
            this.gridColumn57.VisibleIndex = 57;
            this.gridColumn57.Width = 119;
            // 
            // gridColumn58
            // 
            this.gridColumn58.Caption = "Extra Work Required";
            this.gridColumn58.ColumnEdit = this.repositoryItemCheckEdit4;
            this.gridColumn58.FieldName = "ExtraWorkRequired";
            this.gridColumn58.Name = "gridColumn58";
            this.gridColumn58.OptionsColumn.AllowEdit = false;
            this.gridColumn58.OptionsColumn.AllowFocus = false;
            this.gridColumn58.OptionsColumn.ReadOnly = true;
            this.gridColumn58.Visible = true;
            this.gridColumn58.VisibleIndex = 55;
            this.gridColumn58.Width = 121;
            // 
            // gridColumn59
            // 
            this.gridColumn59.Caption = "Extra Work Type";
            this.gridColumn59.FieldName = "ExtraWorkType";
            this.gridColumn59.Name = "gridColumn59";
            this.gridColumn59.OptionsColumn.AllowEdit = false;
            this.gridColumn59.OptionsColumn.AllowFocus = false;
            this.gridColumn59.OptionsColumn.ReadOnly = true;
            this.gridColumn59.Visible = true;
            this.gridColumn59.VisibleIndex = 56;
            this.gridColumn59.Width = 102;
            // 
            // gridColumn60
            // 
            this.gridColumn60.Caption = "Accident on Site";
            this.gridColumn60.ColumnEdit = this.repositoryItemCheckEdit4;
            this.gridColumn60.FieldName = "AccidentOnSite";
            this.gridColumn60.Name = "gridColumn60";
            this.gridColumn60.OptionsColumn.AllowEdit = false;
            this.gridColumn60.OptionsColumn.AllowFocus = false;
            this.gridColumn60.OptionsColumn.ReadOnly = true;
            this.gridColumn60.Visible = true;
            this.gridColumn60.VisibleIndex = 58;
            this.gridColumn60.Width = 98;
            // 
            // gridColumn61
            // 
            this.gridColumn61.Caption = "Accident Comments";
            this.gridColumn61.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.gridColumn61.FieldName = "AccidentComment";
            this.gridColumn61.Name = "gridColumn61";
            this.gridColumn61.OptionsColumn.ReadOnly = true;
            this.gridColumn61.Visible = true;
            this.gridColumn61.VisibleIndex = 59;
            this.gridColumn61.Width = 115;
            // 
            // gridColumn62
            // 
            this.gridColumn62.Caption = "Suspended Jobs";
            this.gridColumn62.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.gridColumn62.FieldName = "SuspendedJobCount";
            this.gridColumn62.Name = "gridColumn62";
            this.gridColumn62.OptionsColumn.ReadOnly = true;
            this.gridColumn62.Visible = true;
            this.gridColumn62.VisibleIndex = 46;
            this.gridColumn62.Width = 97;
            // 
            // gridColumn63
            // 
            this.gridColumn63.Caption = "Linked Pictures";
            this.gridColumn63.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.gridColumn63.FieldName = "LinkedPictureCount";
            this.gridColumn63.Name = "gridColumn63";
            this.gridColumn63.OptionsColumn.ReadOnly = true;
            this.gridColumn63.Visible = true;
            this.gridColumn63.VisibleIndex = 45;
            this.gridColumn63.Width = 90;
            // 
            // gridColumn64
            // 
            this.gridColumn64.Caption = "Friendly Visit #";
            this.gridColumn64.FieldName = "FriendlyVisitNumber";
            this.gridColumn64.Name = "gridColumn64";
            this.gridColumn64.OptionsColumn.AllowEdit = false;
            this.gridColumn64.OptionsColumn.AllowFocus = false;
            this.gridColumn64.OptionsColumn.ReadOnly = true;
            this.gridColumn64.Visible = true;
            this.gridColumn64.VisibleIndex = 2;
            this.gridColumn64.Width = 103;
            // 
            // gridColumn65
            // 
            this.gridColumn65.Caption = "Visit Type";
            this.gridColumn65.FieldName = "VisitType";
            this.gridColumn65.Name = "gridColumn65";
            this.gridColumn65.OptionsColumn.AllowEdit = false;
            this.gridColumn65.OptionsColumn.AllowFocus = false;
            this.gridColumn65.OptionsColumn.ReadOnly = true;
            this.gridColumn65.Visible = true;
            this.gridColumn65.VisibleIndex = 18;
            this.gridColumn65.Width = 99;
            // 
            // gridColumn66
            // 
            this.gridColumn66.Caption = "Visit Type ID";
            this.gridColumn66.FieldName = "VisitTypeID";
            this.gridColumn66.Name = "gridColumn66";
            this.gridColumn66.OptionsColumn.AllowEdit = false;
            this.gridColumn66.OptionsColumn.AllowFocus = false;
            this.gridColumn66.OptionsColumn.ReadOnly = true;
            this.gridColumn66.Width = 79;
            // 
            // gridColumn67
            // 
            this.gridColumn67.Caption = "Client PO #";
            this.gridColumn67.FieldName = "ClientPO";
            this.gridColumn67.Name = "gridColumn67";
            this.gridColumn67.OptionsColumn.AllowEdit = false;
            this.gridColumn67.OptionsColumn.AllowFocus = false;
            this.gridColumn67.OptionsColumn.ReadOnly = true;
            this.gridColumn67.Visible = true;
            this.gridColumn67.VisibleIndex = 30;
            this.gridColumn67.Width = 112;
            // 
            // gridColumn68
            // 
            this.gridColumn68.Caption = "Client PO ID";
            this.gridColumn68.FieldName = "ClientPOID";
            this.gridColumn68.Name = "gridColumn68";
            this.gridColumn68.OptionsColumn.AllowEdit = false;
            this.gridColumn68.OptionsColumn.AllowFocus = false;
            this.gridColumn68.OptionsColumn.ReadOnly = true;
            this.gridColumn68.Width = 77;
            // 
            // gridColumn69
            // 
            this.gridColumn69.Caption = "Site Code";
            this.gridColumn69.FieldName = "SiteCode";
            this.gridColumn69.Name = "gridColumn69";
            this.gridColumn69.OptionsColumn.AllowEdit = false;
            this.gridColumn69.OptionsColumn.AllowFocus = false;
            this.gridColumn69.OptionsColumn.ReadOnly = true;
            this.gridColumn69.Visible = true;
            this.gridColumn69.VisibleIndex = 32;
            // 
            // gridColumn70
            // 
            this.gridColumn70.Caption = "Sell Calculation ID";
            this.gridColumn70.FieldName = "SellCalculationLevel";
            this.gridColumn70.Name = "gridColumn70";
            this.gridColumn70.OptionsColumn.AllowEdit = false;
            this.gridColumn70.OptionsColumn.AllowFocus = false;
            this.gridColumn70.OptionsColumn.ReadOnly = true;
            this.gridColumn70.Width = 104;
            // 
            // gridColumn71
            // 
            this.gridColumn71.Caption = "Sell Calculation";
            this.gridColumn71.FieldName = "SellCalculationLevelDescription";
            this.gridColumn71.Name = "gridColumn71";
            this.gridColumn71.OptionsColumn.AllowEdit = false;
            this.gridColumn71.OptionsColumn.AllowFocus = false;
            this.gridColumn71.OptionsColumn.ReadOnly = true;
            this.gridColumn71.Visible = true;
            this.gridColumn71.VisibleIndex = 25;
            this.gridColumn71.Width = 100;
            // 
            // gridColumn72
            // 
            this.gridColumn72.Caption = "Selected";
            this.gridColumn72.ColumnEdit = this.repositoryItemCheckEdit4;
            this.gridColumn72.FieldName = "Selected";
            this.gridColumn72.Name = "gridColumn72";
            this.gridColumn72.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Selected", "{0:0}")});
            this.gridColumn72.Width = 60;
            // 
            // gridColumn73
            // 
            this.gridColumn73.Caption = "Client Invoice ID";
            this.gridColumn73.ColumnEdit = this.repositoryItemHyperLinkEditClientInvoiceID2;
            this.gridColumn73.FieldName = "ClientInvoiceID";
            this.gridColumn73.Name = "gridColumn73";
            this.gridColumn73.OptionsColumn.ReadOnly = true;
            this.gridColumn73.Visible = true;
            this.gridColumn73.VisibleIndex = 62;
            this.gridColumn73.Width = 98;
            // 
            // repositoryItemHyperLinkEditClientInvoiceID2
            // 
            this.repositoryItemHyperLinkEditClientInvoiceID2.AutoHeight = false;
            this.repositoryItemHyperLinkEditClientInvoiceID2.Name = "repositoryItemHyperLinkEditClientInvoiceID2";
            this.repositoryItemHyperLinkEditClientInvoiceID2.SingleClick = true;
            this.repositoryItemHyperLinkEditClientInvoiceID2.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditClientInvoiceID2_OpenLink);
            // 
            // gridColumn74
            // 
            this.gridColumn74.Caption = "Contract Manager";
            this.gridColumn74.FieldName = "CMName";
            this.gridColumn74.Name = "gridColumn74";
            this.gridColumn74.OptionsColumn.AllowEdit = false;
            this.gridColumn74.OptionsColumn.AllowFocus = false;
            this.gridColumn74.OptionsColumn.ReadOnly = true;
            this.gridColumn74.Visible = true;
            this.gridColumn74.VisibleIndex = 22;
            this.gridColumn74.Width = 106;
            // 
            // gridColumn75
            // 
            this.gridColumn75.Caption = "Days Allowed";
            this.gridColumn75.FieldName = "DaysAllowed";
            this.gridColumn75.Name = "gridColumn75";
            this.gridColumn75.OptionsColumn.AllowEdit = false;
            this.gridColumn75.OptionsColumn.AllowFocus = false;
            this.gridColumn75.OptionsColumn.ReadOnly = true;
            this.gridColumn75.Visible = true;
            this.gridColumn75.VisibleIndex = 6;
            this.gridColumn75.Width = 83;
            // 
            // gridColumn76
            // 
            this.gridColumn76.Caption = "Signed By Manager";
            this.gridColumn76.FieldName = "ManagerName";
            this.gridColumn76.Name = "gridColumn76";
            this.gridColumn76.OptionsColumn.AllowEdit = false;
            this.gridColumn76.OptionsColumn.AllowFocus = false;
            this.gridColumn76.OptionsColumn.ReadOnly = true;
            this.gridColumn76.Visible = true;
            this.gridColumn76.VisibleIndex = 48;
            this.gridColumn76.Width = 111;
            // 
            // gridColumn77
            // 
            this.gridColumn77.Caption = "Signed By Notes";
            this.gridColumn77.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.gridColumn77.FieldName = "ManagerNotes";
            this.gridColumn77.Name = "gridColumn77";
            this.gridColumn77.OptionsColumn.ReadOnly = true;
            this.gridColumn77.Visible = true;
            this.gridColumn77.VisibleIndex = 49;
            this.gridColumn77.Width = 97;
            // 
            // gridColumn78
            // 
            this.gridColumn78.Caption = "No One To Sign ";
            this.gridColumn78.ColumnEdit = this.repositoryItemCheckEdit4;
            this.gridColumn78.FieldName = "NoOnetoSign";
            this.gridColumn78.Name = "gridColumn78";
            this.gridColumn78.OptionsColumn.AllowEdit = false;
            this.gridColumn78.OptionsColumn.AllowFocus = false;
            this.gridColumn78.OptionsColumn.ReadOnly = true;
            this.gridColumn78.Visible = true;
            this.gridColumn78.VisibleIndex = 50;
            this.gridColumn78.Width = 96;
            // 
            // gridColumn79
            // 
            this.gridColumn79.Caption = "Site Category";
            this.gridColumn79.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.gridColumn79.FieldName = "SiteCategory";
            this.gridColumn79.Name = "gridColumn79";
            this.gridColumn79.OptionsColumn.ReadOnly = true;
            this.gridColumn79.Visible = true;
            this.gridColumn79.VisibleIndex = 33;
            this.gridColumn79.Width = 124;
            // 
            // gridColumn80
            // 
            this.gridColumn80.Caption = "External Customer Status";
            this.gridColumn80.FieldName = "ExternalCustomerStatus";
            this.gridColumn80.Name = "gridColumn80";
            this.gridColumn80.OptionsColumn.AllowEdit = false;
            this.gridColumn80.OptionsColumn.AllowFocus = false;
            this.gridColumn80.OptionsColumn.ReadOnly = true;
            this.gridColumn80.Visible = true;
            this.gridColumn80.VisibleIndex = 63;
            this.gridColumn80.Width = 142;
            // 
            // gridColumn81
            // 
            this.gridColumn81.Caption = "Completion Sheet #";
            this.gridColumn81.FieldName = "CompletionSheetNumber";
            this.gridColumn81.Name = "gridColumn81";
            this.gridColumn81.OptionsColumn.AllowEdit = false;
            this.gridColumn81.OptionsColumn.AllowFocus = false;
            this.gridColumn81.OptionsColumn.ReadOnly = true;
            this.gridColumn81.Visible = true;
            this.gridColumn81.VisibleIndex = 39;
            this.gridColumn81.Width = 114;
            // 
            // gridColumn82
            // 
            this.gridColumn82.Caption = "Status Issue ID";
            this.gridColumn82.FieldName = "StatusIssueID";
            this.gridColumn82.Name = "gridColumn82";
            this.gridColumn82.OptionsColumn.AllowEdit = false;
            this.gridColumn82.OptionsColumn.AllowFocus = false;
            this.gridColumn82.OptionsColumn.ReadOnly = true;
            this.gridColumn82.Width = 93;
            // 
            // gridColumn83
            // 
            this.gridColumn83.Caption = "Status Issue";
            this.gridColumn83.FieldName = "StatusIssue";
            this.gridColumn83.Name = "gridColumn83";
            this.gridColumn83.OptionsColumn.AllowEdit = false;
            this.gridColumn83.OptionsColumn.AllowFocus = false;
            this.gridColumn83.OptionsColumn.ReadOnly = true;
            this.gridColumn83.Visible = true;
            this.gridColumn83.VisibleIndex = 21;
            this.gridColumn83.Width = 110;
            // 
            // gridColumn84
            // 
            this.gridColumn84.Caption = "Days Separation";
            this.gridColumn84.FieldName = "DaysSeparation";
            this.gridColumn84.Name = "gridColumn84";
            this.gridColumn84.OptionsColumn.AllowEdit = false;
            this.gridColumn84.OptionsColumn.AllowFocus = false;
            this.gridColumn84.OptionsColumn.ReadOnly = true;
            this.gridColumn84.Visible = true;
            this.gridColumn84.VisibleIndex = 7;
            this.gridColumn84.Width = 98;
            // 
            // gridColumn85
            // 
            this.gridColumn85.Caption = "Site Contract Days Separation %";
            this.gridColumn85.ColumnEdit = this.repositoryItemTextEdit7;
            this.gridColumn85.FieldName = "SiteContractDaysSeparationPercent";
            this.gridColumn85.Name = "gridColumn85";
            this.gridColumn85.OptionsColumn.AllowEdit = false;
            this.gridColumn85.OptionsColumn.AllowFocus = false;
            this.gridColumn85.OptionsColumn.ReadOnly = true;
            this.gridColumn85.Width = 178;
            // 
            // repositoryItemTextEdit7
            // 
            this.repositoryItemTextEdit7.AutoHeight = false;
            this.repositoryItemTextEdit7.Mask.EditMask = "P";
            this.repositoryItemTextEdit7.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit7.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit7.Name = "repositoryItemTextEdit7";
            // 
            // gridColumn90
            // 
            this.gridColumn90.Caption = "Labour Name";
            this.gridColumn90.FieldName = "LabourName";
            this.gridColumn90.Name = "gridColumn90";
            this.gridColumn90.OptionsColumn.AllowEdit = false;
            this.gridColumn90.OptionsColumn.AllowFocus = false;
            this.gridColumn90.OptionsColumn.ReadOnly = true;
            this.gridColumn90.Visible = true;
            this.gridColumn90.VisibleIndex = 0;
            this.gridColumn90.Width = 190;
            // 
            // gridColumn91
            // 
            this.gridColumn91.Caption = "Labour Type";
            this.gridColumn91.FieldName = "LinkedToPersonType";
            this.gridColumn91.Name = "gridColumn91";
            this.gridColumn91.OptionsColumn.AllowEdit = false;
            this.gridColumn91.OptionsColumn.AllowFocus = false;
            this.gridColumn91.OptionsColumn.ReadOnly = true;
            this.gridColumn91.Visible = true;
            this.gridColumn91.VisibleIndex = 5;
            this.gridColumn91.Width = 79;
            // 
            // gridColumn92
            // 
            this.gridColumn92.Caption = "Labour Type ID";
            this.gridColumn92.FieldName = "LinkedToPersonTypeID";
            this.gridColumn92.Name = "gridColumn92";
            this.gridColumn92.OptionsColumn.AllowEdit = false;
            this.gridColumn92.OptionsColumn.AllowFocus = false;
            this.gridColumn92.OptionsColumn.ReadOnly = true;
            this.gridColumn92.Width = 93;
            // 
            // gridColumn93
            // 
            this.gridColumn93.Caption = "Do Not Invoice Client";
            this.gridColumn93.ColumnEdit = this.repositoryItemCheckEdit4;
            this.gridColumn93.FieldName = "DoNotInvoiceClient";
            this.gridColumn93.Name = "gridColumn93";
            this.gridColumn93.OptionsColumn.AllowEdit = false;
            this.gridColumn93.OptionsColumn.AllowFocus = false;
            this.gridColumn93.OptionsColumn.ReadOnly = true;
            this.gridColumn93.Visible = true;
            this.gridColumn93.VisibleIndex = 61;
            this.gridColumn93.Width = 120;
            // 
            // gridColumn94
            // 
            this.gridColumn94.Caption = "Do Not Pay Team";
            this.gridColumn94.ColumnEdit = this.repositoryItemCheckEdit4;
            this.gridColumn94.FieldName = "DoNotPayContractor";
            this.gridColumn94.Name = "gridColumn94";
            this.gridColumn94.OptionsColumn.AllowEdit = false;
            this.gridColumn94.OptionsColumn.AllowFocus = false;
            this.gridColumn94.OptionsColumn.ReadOnly = true;
            this.gridColumn94.Visible = true;
            this.gridColumn94.VisibleIndex = 60;
            this.gridColumn94.Width = 102;
            // 
            // gridColumn95
            // 
            this.gridColumn95.Caption = "Self-Billing Amount";
            this.gridColumn95.ColumnEdit = this.repositoryItemTextEdit4;
            this.gridColumn95.FieldName = "SelfBillingAmount";
            this.gridColumn95.Name = "gridColumn95";
            this.gridColumn95.OptionsColumn.AllowEdit = false;
            this.gridColumn95.OptionsColumn.AllowFocus = false;
            this.gridColumn95.OptionsColumn.ReadOnly = true;
            this.gridColumn95.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SelfBillingAmount", "{0:c}")});
            this.gridColumn95.Visible = true;
            this.gridColumn95.VisibleIndex = 28;
            this.gridColumn95.Width = 107;
            // 
            // gridColumn96
            // 
            this.gridColumn96.Caption = "Self-Billing Comment";
            this.gridColumn96.FieldName = "SelfBillingComment";
            this.gridColumn96.Name = "gridColumn96";
            this.gridColumn96.OptionsColumn.AllowEdit = false;
            this.gridColumn96.OptionsColumn.AllowFocus = false;
            this.gridColumn96.OptionsColumn.ReadOnly = true;
            this.gridColumn96.Visible = true;
            this.gridColumn96.VisibleIndex = 16;
            this.gridColumn96.Width = 136;
            // 
            // gridColumn97
            // 
            this.gridColumn97.Caption = "Self-Billing Invoice ID";
            this.gridColumn97.FieldName = "SelfBillingInvoiceHeaderID";
            this.gridColumn97.Name = "gridColumn97";
            this.gridColumn97.OptionsColumn.AllowEdit = false;
            this.gridColumn97.OptionsColumn.AllowFocus = false;
            this.gridColumn97.OptionsColumn.ReadOnly = true;
            this.gridColumn97.Visible = true;
            this.gridColumn97.VisibleIndex = 30;
            this.gridColumn97.Width = 119;
            // 
            // gridColumn98
            // 
            this.gridColumn98.Caption = "Visit Labour Calculated ID";
            this.gridColumn98.FieldName = "VisitLabourCalculatedID";
            this.gridColumn98.Name = "gridColumn98";
            this.gridColumn98.OptionsColumn.AllowEdit = false;
            this.gridColumn98.OptionsColumn.AllowFocus = false;
            this.gridColumn98.OptionsColumn.ReadOnly = true;
            this.gridColumn98.Width = 141;
            // 
            // gridColumn99
            // 
            this.gridColumn99.Caption = "Unique ID";
            this.gridColumn99.FieldName = "UniqueID";
            this.gridColumn99.Name = "gridColumn99";
            this.gridColumn99.OptionsColumn.AllowEdit = false;
            this.gridColumn99.OptionsColumn.AllowFocus = false;
            this.gridColumn99.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn100
            // 
            this.gridColumn100.Caption = "Company Name";
            this.gridColumn100.FieldName = "CompanyName";
            this.gridColumn100.Name = "gridColumn100";
            this.gridColumn100.OptionsColumn.AllowEdit = false;
            this.gridColumn100.OptionsColumn.AllowFocus = false;
            this.gridColumn100.OptionsColumn.ReadOnly = true;
            this.gridColumn100.Visible = true;
            this.gridColumn100.VisibleIndex = 0;
            this.gridColumn100.Width = 150;
            // 
            // gridColumn101
            // 
            this.gridColumn101.Caption = "Self-Billing Invoice PDF";
            this.gridColumn101.ColumnEdit = this.repositoryItemHyperLinkEditSelfBillingInvoicePDF;
            this.gridColumn101.FieldName = "InvoicePDF";
            this.gridColumn101.Name = "gridColumn101";
            this.gridColumn101.OptionsColumn.ReadOnly = true;
            this.gridColumn101.Visible = true;
            this.gridColumn101.VisibleIndex = 10;
            this.gridColumn101.Width = 267;
            // 
            // repositoryItemHyperLinkEditSelfBillingInvoicePDF
            // 
            this.repositoryItemHyperLinkEditSelfBillingInvoicePDF.AutoHeight = false;
            this.repositoryItemHyperLinkEditSelfBillingInvoicePDF.Name = "repositoryItemHyperLinkEditSelfBillingInvoicePDF";
            this.repositoryItemHyperLinkEditSelfBillingInvoicePDF.SingleClick = true;
            this.repositoryItemHyperLinkEditSelfBillingInvoicePDF.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditSelfBillingInvoicePDF_OpenLink);
            // 
            // sp06469OMFinanceSelfBillingManagerVisitsBindingSource
            // 
            this.sp06469OMFinanceSelfBillingManagerVisitsBindingSource.DataMember = "sp06469_OM_Finance_Self_Billing_Manager_Visits";
            this.sp06469OMFinanceSelfBillingManagerVisitsBindingSource.DataSource = this.dataSet_OM_Billing;
            // 
            // sp06219OMWasteDisposalCentresAcceptedWasteTypesBindingSource
            // 
            this.sp06219OMWasteDisposalCentresAcceptedWasteTypesBindingSource.DataMember = "sp06219_OM_Waste_Disposal_Centres_Accepted_Waste_Types";
            this.sp06219OMWasteDisposalCentresAcceptedWasteTypesBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridControlInvoices
            // 
            this.gridControlInvoices.DataSource = this.sp06468OMFinanceSelfRecordsClientsToBillBindingSource;
            this.gridControlInvoices.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlInvoices.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlInvoices.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlInvoices.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlInvoices.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlInvoices.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlInvoices.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlInvoices.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlInvoices.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlInvoices.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlInvoices.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlInvoices.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlInvoices.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Reload Data", "reload")});
            this.gridControlInvoices.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlInvoices_EmbeddedNavigator_ButtonClick);
            this.gridControlInvoices.Location = new System.Drawing.Point(0, 0);
            this.gridControlInvoices.MainView = this.gridViewInvoices;
            this.gridControlInvoices.MenuManager = this.barManager1;
            this.gridControlInvoices.Name = "gridControlInvoices";
            this.gridControlInvoices.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEditMoney,
            this.repositoryItemTextEditDate,
            this.repositoryItemCheckEdit2});
            this.gridControlInvoices.Size = new System.Drawing.Size(1204, 500);
            this.gridControlInvoices.TabIndex = 1;
            this.gridControlInvoices.UseEmbeddedNavigator = true;
            this.gridControlInvoices.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewInvoices});
            // 
            // sp06468OMFinanceSelfRecordsClientsToBillBindingSource
            // 
            this.sp06468OMFinanceSelfRecordsClientsToBillBindingSource.DataMember = "sp06468_OM_Finance_Self_Records_Clients_To_Bill";
            this.sp06468OMFinanceSelfRecordsClientsToBillBindingSource.DataSource = this.dataSet_OM_Billing;
            // 
            // gridViewInvoices
            // 
            this.gridViewInvoices.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSubContractorID,
            this.colEmployeeCode,
            this.colInvoiceDate,
            this.colInvoiceID,
            this.colJobCode,
            this.colAnalysisCode,
            this.colCostCentre,
            this.colDepartment,
            this.colPaid,
            this.colName,
            this.colClient,
            this.colDirectEmployed,
            this.colVAT,
            this.colPeriod,
            this.colYear,
            this.colCompanyName,
            this.colSelected,
            this.colSelfBillingInvoiceHeaderID1,
            this.colUniqueID,
            this.colClientID1,
            this.colGCCompanyID,
            this.colSelfBillingInvoiceNumber});
            this.gridViewInvoices.GridControl = this.gridControlInvoices;
            this.gridViewInvoices.GroupCount = 3;
            this.gridViewInvoices.Name = "gridViewInvoices";
            this.gridViewInvoices.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridViewInvoices.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewInvoices.OptionsFind.AlwaysVisible = true;
            this.gridViewInvoices.OptionsFind.FindDelay = 2000;
            this.gridViewInvoices.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewInvoices.OptionsLayout.StoreAppearance = true;
            this.gridViewInvoices.OptionsLayout.StoreFormatRules = true;
            this.gridViewInvoices.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewInvoices.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewInvoices.OptionsSelection.MultiSelect = true;
            this.gridViewInvoices.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewInvoices.OptionsView.ColumnAutoWidth = false;
            this.gridViewInvoices.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewInvoices.OptionsView.ShowGroupPanel = false;
            this.gridViewInvoices.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCompanyName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClient, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colInvoiceDate, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewInvoices.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewInvoices_CustomRowCellEdit);
            this.gridViewInvoices.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewInvoices.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewInvoices.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewInvoices.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewInvoices_ShowingEditor);
            this.gridViewInvoices.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewInvoices.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewInvoices.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewInvoices.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewInvoices_MouseUp);
            this.gridViewInvoices.DoubleClick += new System.EventHandler(this.gridViewInvoices_DoubleClick);
            this.gridViewInvoices.GotFocus += new System.EventHandler(this.gridViewInvoices_GotFocus);
            // 
            // colSubContractorID
            // 
            this.colSubContractorID.Caption = "Sub-Contractor ID";
            this.colSubContractorID.FieldName = "SubContractorID";
            this.colSubContractorID.Name = "colSubContractorID";
            this.colSubContractorID.OptionsColumn.AllowEdit = false;
            this.colSubContractorID.OptionsColumn.AllowFocus = false;
            this.colSubContractorID.OptionsColumn.ReadOnly = true;
            this.colSubContractorID.Visible = true;
            this.colSubContractorID.VisibleIndex = 0;
            this.colSubContractorID.Width = 107;
            // 
            // colEmployeeCode
            // 
            this.colEmployeeCode.Caption = "Employee Code";
            this.colEmployeeCode.FieldName = "EmployeeCode";
            this.colEmployeeCode.Name = "colEmployeeCode";
            this.colEmployeeCode.OptionsColumn.AllowEdit = false;
            this.colEmployeeCode.OptionsColumn.AllowFocus = false;
            this.colEmployeeCode.OptionsColumn.ReadOnly = true;
            this.colEmployeeCode.Visible = true;
            this.colEmployeeCode.VisibleIndex = 1;
            this.colEmployeeCode.Width = 93;
            // 
            // colInvoiceDate
            // 
            this.colInvoiceDate.Caption = "Invoice Date";
            this.colInvoiceDate.ColumnEdit = this.repositoryItemTextEditDate;
            this.colInvoiceDate.FieldName = "InvoiceDate";
            this.colInvoiceDate.Name = "colInvoiceDate";
            this.colInvoiceDate.OptionsColumn.AllowEdit = false;
            this.colInvoiceDate.OptionsColumn.AllowFocus = false;
            this.colInvoiceDate.OptionsColumn.ReadOnly = true;
            this.colInvoiceDate.Visible = true;
            this.colInvoiceDate.VisibleIndex = 2;
            this.colInvoiceDate.Width = 93;
            // 
            // repositoryItemTextEditDate
            // 
            this.repositoryItemTextEditDate.AutoHeight = false;
            this.repositoryItemTextEditDate.Mask.EditMask = "d";
            this.repositoryItemTextEditDate.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDate.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDate.Name = "repositoryItemTextEditDate";
            // 
            // colInvoiceID
            // 
            this.colInvoiceID.Caption = "Invoice ID";
            this.colInvoiceID.FieldName = "InvoiceID";
            this.colInvoiceID.Name = "colInvoiceID";
            this.colInvoiceID.OptionsColumn.AllowEdit = false;
            this.colInvoiceID.OptionsColumn.AllowFocus = false;
            this.colInvoiceID.OptionsColumn.ReadOnly = true;
            this.colInvoiceID.Visible = true;
            this.colInvoiceID.VisibleIndex = 3;
            // 
            // colJobCode
            // 
            this.colJobCode.Caption = "Job code";
            this.colJobCode.FieldName = "JobCode";
            this.colJobCode.Name = "colJobCode";
            this.colJobCode.OptionsColumn.AllowEdit = false;
            this.colJobCode.OptionsColumn.AllowFocus = false;
            this.colJobCode.OptionsColumn.ReadOnly = true;
            this.colJobCode.Visible = true;
            this.colJobCode.VisibleIndex = 4;
            this.colJobCode.Width = 92;
            // 
            // colAnalysisCode
            // 
            this.colAnalysisCode.Caption = "Analysis Code";
            this.colAnalysisCode.FieldName = "AnalysisCode";
            this.colAnalysisCode.Name = "colAnalysisCode";
            this.colAnalysisCode.OptionsColumn.AllowEdit = false;
            this.colAnalysisCode.OptionsColumn.AllowFocus = false;
            this.colAnalysisCode.OptionsColumn.ReadOnly = true;
            this.colAnalysisCode.Visible = true;
            this.colAnalysisCode.VisibleIndex = 5;
            this.colAnalysisCode.Width = 98;
            // 
            // colCostCentre
            // 
            this.colCostCentre.Caption = "Cost Centre";
            this.colCostCentre.FieldName = "CostCentre";
            this.colCostCentre.Name = "colCostCentre";
            this.colCostCentre.OptionsColumn.AllowEdit = false;
            this.colCostCentre.OptionsColumn.AllowFocus = false;
            this.colCostCentre.OptionsColumn.ReadOnly = true;
            this.colCostCentre.Visible = true;
            this.colCostCentre.VisibleIndex = 6;
            this.colCostCentre.Width = 93;
            // 
            // colDepartment
            // 
            this.colDepartment.Caption = "Department";
            this.colDepartment.FieldName = "Department";
            this.colDepartment.Name = "colDepartment";
            this.colDepartment.OptionsColumn.AllowEdit = false;
            this.colDepartment.OptionsColumn.AllowFocus = false;
            this.colDepartment.OptionsColumn.ReadOnly = true;
            this.colDepartment.Visible = true;
            this.colDepartment.VisibleIndex = 7;
            this.colDepartment.Width = 109;
            // 
            // colPaid
            // 
            this.colPaid.Caption = "Paid";
            this.colPaid.ColumnEdit = this.repositoryItemTextEditMoney;
            this.colPaid.FieldName = "Paid";
            this.colPaid.Name = "colPaid";
            this.colPaid.OptionsColumn.AllowEdit = false;
            this.colPaid.OptionsColumn.AllowFocus = false;
            this.colPaid.OptionsColumn.ReadOnly = true;
            this.colPaid.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Paid", "{0:c}")});
            this.colPaid.Visible = true;
            this.colPaid.VisibleIndex = 8;
            this.colPaid.Width = 70;
            // 
            // repositoryItemTextEditMoney
            // 
            this.repositoryItemTextEditMoney.AutoHeight = false;
            this.repositoryItemTextEditMoney.Mask.EditMask = "c";
            this.repositoryItemTextEditMoney.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditMoney.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditMoney.Name = "repositoryItemTextEditMoney";
            // 
            // colName
            // 
            this.colName.Caption = "Name";
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.OptionsColumn.AllowEdit = false;
            this.colName.OptionsColumn.AllowFocus = false;
            this.colName.OptionsColumn.ReadOnly = true;
            this.colName.Visible = true;
            this.colName.VisibleIndex = 9;
            this.colName.Width = 272;
            // 
            // colClient
            // 
            this.colClient.Caption = "Client";
            this.colClient.FieldName = "Client";
            this.colClient.Name = "colClient";
            this.colClient.OptionsColumn.AllowEdit = false;
            this.colClient.OptionsColumn.AllowFocus = false;
            this.colClient.OptionsColumn.ReadOnly = true;
            this.colClient.Visible = true;
            this.colClient.VisibleIndex = 9;
            this.colClient.Width = 253;
            // 
            // colDirectEmployed
            // 
            this.colDirectEmployed.Caption = "Direct Employed";
            this.colDirectEmployed.FieldName = "DirectEmployed";
            this.colDirectEmployed.Name = "colDirectEmployed";
            this.colDirectEmployed.OptionsColumn.AllowEdit = false;
            this.colDirectEmployed.OptionsColumn.AllowFocus = false;
            this.colDirectEmployed.OptionsColumn.ReadOnly = true;
            this.colDirectEmployed.Visible = true;
            this.colDirectEmployed.VisibleIndex = 9;
            this.colDirectEmployed.Width = 96;
            // 
            // colVAT
            // 
            this.colVAT.Caption = "VAT";
            this.colVAT.FieldName = "VAT";
            this.colVAT.Name = "colVAT";
            this.colVAT.OptionsColumn.AllowEdit = false;
            this.colVAT.OptionsColumn.AllowFocus = false;
            this.colVAT.OptionsColumn.ReadOnly = true;
            this.colVAT.Visible = true;
            this.colVAT.VisibleIndex = 10;
            this.colVAT.Width = 39;
            // 
            // colPeriod
            // 
            this.colPeriod.Caption = "Period";
            this.colPeriod.FieldName = "Period";
            this.colPeriod.Name = "colPeriod";
            this.colPeriod.OptionsColumn.AllowEdit = false;
            this.colPeriod.OptionsColumn.AllowFocus = false;
            this.colPeriod.OptionsColumn.ReadOnly = true;
            this.colPeriod.Visible = true;
            this.colPeriod.VisibleIndex = 11;
            this.colPeriod.Width = 50;
            // 
            // colYear
            // 
            this.colYear.Caption = "Year";
            this.colYear.FieldName = "Year";
            this.colYear.Name = "colYear";
            this.colYear.OptionsColumn.AllowEdit = false;
            this.colYear.OptionsColumn.AllowFocus = false;
            this.colYear.OptionsColumn.ReadOnly = true;
            this.colYear.Visible = true;
            this.colYear.VisibleIndex = 12;
            this.colYear.Width = 40;
            // 
            // colCompanyName
            // 
            this.colCompanyName.Caption = "Company Name";
            this.colCompanyName.FieldName = "CompanyName";
            this.colCompanyName.Name = "colCompanyName";
            this.colCompanyName.OptionsColumn.AllowEdit = false;
            this.colCompanyName.OptionsColumn.AllowFocus = false;
            this.colCompanyName.OptionsColumn.ReadOnly = true;
            this.colCompanyName.Visible = true;
            this.colCompanyName.VisibleIndex = 15;
            this.colCompanyName.Width = 222;
            // 
            // colSelected
            // 
            this.colSelected.Caption = "Selected";
            this.colSelected.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colSelected.FieldName = "Selected";
            this.colSelected.Name = "colSelected";
            this.colSelected.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Selected", "{0:0}")});
            this.colSelected.Width = 60;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // colSelfBillingInvoiceHeaderID1
            // 
            this.colSelfBillingInvoiceHeaderID1.Caption = "Self-Billing Invoice ID";
            this.colSelfBillingInvoiceHeaderID1.FieldName = "SelfBillingInvoiceHeaderID";
            this.colSelfBillingInvoiceHeaderID1.Name = "colSelfBillingInvoiceHeaderID1";
            this.colSelfBillingInvoiceHeaderID1.OptionsColumn.AllowEdit = false;
            this.colSelfBillingInvoiceHeaderID1.OptionsColumn.AllowFocus = false;
            this.colSelfBillingInvoiceHeaderID1.OptionsColumn.ReadOnly = true;
            this.colSelfBillingInvoiceHeaderID1.Width = 119;
            // 
            // colUniqueID
            // 
            this.colUniqueID.Caption = "Unique ID";
            this.colUniqueID.FieldName = "UniqueID";
            this.colUniqueID.Name = "colUniqueID";
            this.colUniqueID.OptionsColumn.AllowEdit = false;
            this.colUniqueID.OptionsColumn.AllowFocus = false;
            this.colUniqueID.OptionsColumn.ReadOnly = true;
            this.colUniqueID.Width = 99;
            // 
            // colClientID1
            // 
            this.colClientID1.Caption = "Client ID";
            this.colClientID1.FieldName = "ClientID";
            this.colClientID1.Name = "colClientID1";
            this.colClientID1.OptionsColumn.AllowEdit = false;
            this.colClientID1.OptionsColumn.AllowFocus = false;
            this.colClientID1.OptionsColumn.ReadOnly = true;
            // 
            // colGCCompanyID
            // 
            this.colGCCompanyID.Caption = "Company ID";
            this.colGCCompanyID.FieldName = "GCCompanyID";
            this.colGCCompanyID.Name = "colGCCompanyID";
            this.colGCCompanyID.OptionsColumn.AllowEdit = false;
            this.colGCCompanyID.OptionsColumn.AllowFocus = false;
            this.colGCCompanyID.OptionsColumn.ReadOnly = true;
            // 
            // colSelfBillingInvoiceNumber
            // 
            this.colSelfBillingInvoiceNumber.FieldName = "SelfBillingInvoiceNumber";
            this.colSelfBillingInvoiceNumber.Name = "colSelfBillingInvoiceNumber";
            this.colSelfBillingInvoiceNumber.OptionsColumn.AllowEdit = false;
            this.colSelfBillingInvoiceNumber.OptionsColumn.AllowFocus = false;
            this.colSelfBillingInvoiceNumber.OptionsColumn.ReadOnly = true;
            this.colSelfBillingInvoiceNumber.Width = 144;
            // 
            // dataSet_OM_Client_PO
            // 
            this.dataSet_OM_Client_PO.DataSetName = "DataSet_OM_Client_PO";
            this.dataSet_OM_Client_PO.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView3
            // 
            this.gridView3.Name = "gridView3";
            // 
            // sp06219_OM_Waste_Disposal_Centres_Accepted_Waste_TypesTableAdapter
            // 
            this.sp06219_OM_Waste_Disposal_Centres_Accepted_Waste_TypesTableAdapter.ClearBeforeFill = true;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(23, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPageTeamSelfBillingInvoices;
            this.xtraTabControl1.Size = new System.Drawing.Size(1220, 597);
            this.xtraTabControl1.TabIndex = 5;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageTeamSelfBillingInvoices,
            this.xtraTabPageHistoricalDataExtracts});
            this.xtraTabControl1.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl1_SelectedPageChanged);
            // 
            // xtraTabPageTeamSelfBillingInvoices
            // 
            this.xtraTabPageTeamSelfBillingInvoices.Controls.Add(this.splitContainerControl2);
            this.xtraTabPageTeamSelfBillingInvoices.Controls.Add(this.standaloneBarDockControl1);
            this.xtraTabPageTeamSelfBillingInvoices.Name = "xtraTabPageTeamSelfBillingInvoices";
            this.xtraTabPageTeamSelfBillingInvoices.Size = new System.Drawing.Size(1215, 571);
            this.xtraTabPageTeamSelfBillingInvoices.Text = "Team Self-Billing Invoices";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl2.Collapsed = true;
            this.splitContainerControl2.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl2.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 44);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl2.Panel1.Controls.Add(this.popupContainerControlInvoiceNumber);
            this.splitContainerControl2.Panel1.Controls.Add(this.popupContainerControlDateRange);
            this.splitContainerControl2.Panel1.Controls.Add(this.popupContainerControlCompanyFilter);
            this.splitContainerControl2.Panel1.Controls.Add(this.gridControlInvoices);
            this.splitContainerControl2.Panel1.ShowCaption = true;
            this.splitContainerControl2.Panel1.Text = "Self-Billing Invoices";
            this.splitContainerControl2.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl2.Panel2.Controls.Add(this.gridControlVisit);
            this.splitContainerControl2.Panel2.ShowCaption = true;
            this.splitContainerControl2.Panel2.Text = "Visits Linked To Invoice / Client";
            this.splitContainerControl2.Size = new System.Drawing.Size(1214, 524);
            this.splitContainerControl2.SplitterPosition = 567;
            this.splitContainerControl2.TabIndex = 25;
            this.splitContainerControl2.Text = "splitContainerControl2";
            this.splitContainerControl2.SplitGroupPanelCollapsed += new DevExpress.XtraEditors.SplitGroupPanelCollapsedEventHandler(this.splitContainerControl2_SplitGroupPanelCollapsed);
            // 
            // popupContainerControlInvoiceNumber
            // 
            this.popupContainerControlInvoiceNumber.Controls.Add(this.btnInvoiceNumber);
            this.popupContainerControlInvoiceNumber.Controls.Add(this.gridControl3);
            this.popupContainerControlInvoiceNumber.Location = new System.Drawing.Point(266, 119);
            this.popupContainerControlInvoiceNumber.Name = "popupContainerControlInvoiceNumber";
            this.popupContainerControlInvoiceNumber.Size = new System.Drawing.Size(208, 267);
            this.popupContainerControlInvoiceNumber.TabIndex = 23;
            // 
            // btnInvoiceNumber
            // 
            this.btnInvoiceNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnInvoiceNumber.Location = new System.Drawing.Point(3, 244);
            this.btnInvoiceNumber.Name = "btnInvoiceNumber";
            this.btnInvoiceNumber.Size = new System.Drawing.Size(50, 20);
            this.btnInvoiceNumber.TabIndex = 12;
            this.btnInvoiceNumber.Text = "OK";
            this.btnInvoiceNumber.Click += new System.EventHandler(this.btnInvoiceNumber_Click);
            // 
            // gridControl3
            // 
            this.gridControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl3.DataSource = this.sp06477OMGetUniqueSelfBillingInvoiceListBindingSource;
            this.gridControl3.Location = new System.Drawing.Point(3, 3);
            this.gridControl3.MainView = this.gridView1;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit8});
            this.gridControl3.Size = new System.Drawing.Size(202, 239);
            this.gridControl3.TabIndex = 4;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp06477OMGetUniqueSelfBillingInvoiceListBindingSource
            // 
            this.sp06477OMGetUniqueSelfBillingInvoiceListBindingSource.DataMember = "sp06477_OM_Get_Unique_Self_Billing_Invoice_List";
            this.sp06477OMGetUniqueSelfBillingInvoiceListBindingSource.DataSource = this.dataSet_OM_Billing;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSelfBillingInvoiceNumber1});
            this.gridView1.GridControl = this.gridControl3;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSelfBillingInvoiceNumber1, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colSelfBillingInvoiceNumber1
            // 
            this.colSelfBillingInvoiceNumber1.Caption = "Invoice Number";
            this.colSelfBillingInvoiceNumber1.FieldName = "SelfBillingInvoiceNumber";
            this.colSelfBillingInvoiceNumber1.Name = "colSelfBillingInvoiceNumber1";
            this.colSelfBillingInvoiceNumber1.OptionsColumn.AllowEdit = false;
            this.colSelfBillingInvoiceNumber1.OptionsColumn.AllowFocus = false;
            this.colSelfBillingInvoiceNumber1.OptionsColumn.ReadOnly = true;
            this.colSelfBillingInvoiceNumber1.Visible = true;
            this.colSelfBillingInvoiceNumber1.VisibleIndex = 0;
            this.colSelfBillingInvoiceNumber1.Width = 124;
            // 
            // repositoryItemCheckEdit8
            // 
            this.repositoryItemCheckEdit8.AutoHeight = false;
            this.repositoryItemCheckEdit8.Caption = "Check";
            this.repositoryItemCheckEdit8.Name = "repositoryItemCheckEdit8";
            this.repositoryItemCheckEdit8.ValueChecked = 1;
            this.repositoryItemCheckEdit8.ValueUnchecked = 0;
            // 
            // popupContainerControlDateRange
            // 
            this.popupContainerControlDateRange.Controls.Add(this.groupControl1);
            this.popupContainerControlDateRange.Controls.Add(this.btnDateRangeOK);
            this.popupContainerControlDateRange.Location = new System.Drawing.Point(480, 119);
            this.popupContainerControlDateRange.Name = "popupContainerControlDateRange";
            this.popupContainerControlDateRange.Size = new System.Drawing.Size(205, 108);
            this.popupContainerControlDateRange.TabIndex = 18;
            // 
            // groupControl1
            // 
            this.groupControl1.AllowHtmlText = true;
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.dateEditFromDate);
            this.groupControl1.Controls.Add(this.dateEditToDate);
            this.groupControl1.Location = new System.Drawing.Point(3, 5);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(199, 77);
            this.groupControl1.TabIndex = 20;
            this.groupControl1.Text = "Date Range  [Date Exported]";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(6, 54);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(16, 13);
            this.labelControl2.TabIndex = 13;
            this.labelControl2.Text = "To:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(6, 28);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(28, 13);
            this.labelControl1.TabIndex = 12;
            this.labelControl1.Text = "From:";
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(40, 25);
            this.dateEditFromDate.MenuManager = this.barManager1;
            this.dateEditFromDate.Name = "dateEditFromDate";
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditFromDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditFromDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullValuePrompt = "No Date";
            this.dateEditFromDate.Size = new System.Drawing.Size(154, 20);
            this.dateEditFromDate.TabIndex = 10;
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(40, 51);
            this.dateEditToDate.MenuManager = this.barManager1;
            this.dateEditToDate.Name = "dateEditToDate";
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditToDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditToDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullValuePrompt = "No Date";
            this.dateEditToDate.Size = new System.Drawing.Size(154, 20);
            this.dateEditToDate.TabIndex = 11;
            // 
            // btnDateRangeOK
            // 
            this.btnDateRangeOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDateRangeOK.Location = new System.Drawing.Point(3, 85);
            this.btnDateRangeOK.Name = "btnDateRangeOK";
            this.btnDateRangeOK.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnDateRangeOK.Size = new System.Drawing.Size(50, 20);
            this.btnDateRangeOK.TabIndex = 19;
            this.btnDateRangeOK.Text = "OK";
            this.btnDateRangeOK.Click += new System.EventHandler(this.btnDateRangeOK_Click);
            // 
            // popupContainerControlCompanyFilter
            // 
            this.popupContainerControlCompanyFilter.Controls.Add(this.gridControlCompanyFilter);
            this.popupContainerControlCompanyFilter.Controls.Add(this.btnCompanyFilterOK);
            this.popupContainerControlCompanyFilter.Location = new System.Drawing.Point(3, 119);
            this.popupContainerControlCompanyFilter.Name = "popupContainerControlCompanyFilter";
            this.popupContainerControlCompanyFilter.Size = new System.Drawing.Size(257, 180);
            this.popupContainerControlCompanyFilter.TabIndex = 24;
            // 
            // gridControlCompanyFilter
            // 
            this.gridControlCompanyFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControlCompanyFilter.DataSource = this.sp01044CoreGCCompanyListBindingSource;
            this.gridControlCompanyFilter.Location = new System.Drawing.Point(3, 3);
            this.gridControlCompanyFilter.MainView = this.gridViewCompanyFilter;
            this.gridControlCompanyFilter.MenuManager = this.barManager1;
            this.gridControlCompanyFilter.Name = "gridControlCompanyFilter";
            this.gridControlCompanyFilter.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit21});
            this.gridControlCompanyFilter.Size = new System.Drawing.Size(251, 153);
            this.gridControlCompanyFilter.TabIndex = 1;
            this.gridControlCompanyFilter.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewCompanyFilter});
            // 
            // sp01044CoreGCCompanyListBindingSource
            // 
            this.sp01044CoreGCCompanyListBindingSource.DataMember = "sp01044_Core_GC_Company_List";
            this.sp01044CoreGCCompanyListBindingSource.DataSource = this.dataSet_Common_Functionality;
            // 
            // dataSet_Common_Functionality
            // 
            this.dataSet_Common_Functionality.DataSetName = "DataSet_Common_Functionality";
            this.dataSet_Common_Functionality.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridViewCompanyFilter
            // 
            this.gridViewCompanyFilter.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID,
            this.colDescription,
            this.colRecordOrder,
            this.colOMSelfBillingFinanceExportNamePrefix});
            this.gridViewCompanyFilter.GridControl = this.gridControlCompanyFilter;
            this.gridViewCompanyFilter.Name = "gridViewCompanyFilter";
            this.gridViewCompanyFilter.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewCompanyFilter.OptionsLayout.StoreAppearance = true;
            this.gridViewCompanyFilter.OptionsView.AutoCalcPreviewLineCount = true;
            this.gridViewCompanyFilter.OptionsView.ColumnAutoWidth = false;
            this.gridViewCompanyFilter.OptionsView.ShowGroupPanel = false;
            this.gridViewCompanyFilter.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewCompanyFilter.OptionsView.ShowIndicator = false;
            this.gridViewCompanyFilter.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colID
            // 
            this.colID.Caption = "Company ID";
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.OptionsColumn.AllowEdit = false;
            this.colID.OptionsColumn.AllowFocus = false;
            this.colID.OptionsColumn.ReadOnly = true;
            this.colID.Width = 79;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Company Name";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 190;
            // 
            // colRecordOrder
            // 
            this.colRecordOrder.Caption = "Order";
            this.colRecordOrder.FieldName = "RecordOrder";
            this.colRecordOrder.Name = "colRecordOrder";
            this.colRecordOrder.OptionsColumn.AllowEdit = false;
            this.colRecordOrder.OptionsColumn.AllowFocus = false;
            this.colRecordOrder.OptionsColumn.ReadOnly = true;
            // 
            // colOMSelfBillingFinanceExportNamePrefix
            // 
            this.colOMSelfBillingFinanceExportNamePrefix.Caption = "Finance Self-Billing Export Filename Prefix";
            this.colOMSelfBillingFinanceExportNamePrefix.FieldName = "OMSelfBillingFinanceExportNamePrefix";
            this.colOMSelfBillingFinanceExportNamePrefix.Name = "colOMSelfBillingFinanceExportNamePrefix";
            this.colOMSelfBillingFinanceExportNamePrefix.OptionsColumn.AllowEdit = false;
            this.colOMSelfBillingFinanceExportNamePrefix.OptionsColumn.AllowFocus = false;
            this.colOMSelfBillingFinanceExportNamePrefix.OptionsColumn.ReadOnly = true;
            this.colOMSelfBillingFinanceExportNamePrefix.Width = 219;
            // 
            // repositoryItemCheckEdit21
            // 
            this.repositoryItemCheckEdit21.AutoHeight = false;
            this.repositoryItemCheckEdit21.Caption = "Check";
            this.repositoryItemCheckEdit21.Name = "repositoryItemCheckEdit21";
            this.repositoryItemCheckEdit21.ValueChecked = 1;
            this.repositoryItemCheckEdit21.ValueUnchecked = 0;
            // 
            // btnCompanyFilterOK
            // 
            this.btnCompanyFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCompanyFilterOK.Location = new System.Drawing.Point(3, 158);
            this.btnCompanyFilterOK.Name = "btnCompanyFilterOK";
            this.btnCompanyFilterOK.Size = new System.Drawing.Size(50, 20);
            this.btnCompanyFilterOK.TabIndex = 0;
            this.btnCompanyFilterOK.Text = "OK";
            this.btnCompanyFilterOK.Click += new System.EventHandler(this.btnCompanyFilterOK_Click);
            // 
            // gridControlVisit
            // 
            this.gridControlVisit.DataSource = this.sp06469OMFinanceSelfBillingManagerVisitsBindingSource;
            this.gridControlVisit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlVisit.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlVisit.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlVisit.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlVisit.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlVisit.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlVisit.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlVisit.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlVisit.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlVisit.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlVisit.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlVisit.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlVisit.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Reload Data", "reload")});
            this.gridControlVisit.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlVisit_EmbeddedNavigator_ButtonClick);
            this.gridControlVisit.Location = new System.Drawing.Point(0, 0);
            this.gridControlVisit.MainView = this.gridViewVisit;
            this.gridControlVisit.MenuManager = this.barManager1;
            this.gridControlVisit.Name = "gridControlVisit";
            this.gridControlVisit.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEditInteger2,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemTextEditCurrency,
            this.repositoryItemMemoExEdit2,
            this.repositoryItemTextEdit1,
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEditHTML0,
            this.repositoryItemHyperLinkEditGrid1,
            this.repositoryItemHyperLinkEditClientSignature,
            this.repositoryItemPictureEdit2,
            this.repositoryItemHyperLinkEditSelfBillingInvoice,
            this.repositoryItemHyperLinkEditClientInvoiceID,
            this.repositoryItemTextEditPercentage});
            this.gridControlVisit.Size = new System.Drawing.Size(0, 0);
            this.gridControlVisit.TabIndex = 5;
            this.gridControlVisit.UseEmbeddedNavigator = true;
            this.gridControlVisit.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewVisit});
            // 
            // gridViewVisit
            // 
            this.gridViewVisit.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colVisitID,
            this.colSiteContractID,
            this.colVisitNumber,
            this.colCreatedByStaffID,
            this.colExpectedStartDate,
            this.colExpectedEndDate,
            this.colStartDate,
            this.colEndDate,
            this.colCostCalculationLevel,
            this.colVisitCost,
            this.colVisitSell,
            this.gridColumn1,
            this.colStartLatitude,
            this.colStartLongitude,
            this.colFinishLatitude,
            this.colFinishLongitude,
            this.colCostCalculationLevelDescription,
            this.colClientContractID,
            this.colSiteID,
            this.colSiteContractValue,
            this.colClientID,
            this.colContractDirectorID,
            this.colClientContractStartDate,
            this.colClientContractEndDate,
            this.colClientContractActive,
            this.colClientContractValue,
            this.colFinanceClientCode,
            this.colClientName,
            this.colContractType,
            this.colContractStatus,
            this.colContractDirector,
            this.colSiteName,
            this.colLinkedToParent1,
            this.colContractDescription,
            this.colSitePostcode,
            this.colSiteLocationX,
            this.colSiteLocationY,
            this.colLinkedJobCount,
            this.colLinkedOutstandingJobCount,
            this.colLinkedCompletedJobCount,
            this.colCreatedByStaffName,
            this.colSiteAddress,
            this.colVisitStatus,
            this.colVisitStatusID,
            this.colTimeliness,
            this.colWorkNumber,
            this.colVisitCategory,
            this.colVisitCategoryID,
            this.colIssueFound,
            this.colIssueType,
            this.colIssueTypeID,
            this.colIssueRemarks,
            this.colClientSignaturePath,
            this.colImagesFolderOM,
            this.colRework,
            this.colExtraWorkComments,
            this.colExtraWorkRequired,
            this.colExtraWorkType,
            this.colAccidentOnSite,
            this.colAccidentComment,
            this.colSuspendedJobCount,
            this.colLinkedPictureCount,
            this.colFriendlyVisitNumber,
            this.colVisitType,
            this.colVisitTypeID,
            this.colClientPO,
            this.colClientPOID1,
            this.colSiteCode,
            this.colSellCalculationLevel,
            this.colSellCalculationLevelDescription,
            this.colSelected4,
            this.colClientInvoiceID1,
            this.colCMName,
            this.colDaysAllowed,
            this.colManagerName,
            this.colManagerNotes,
            this.colNoOnetoSign,
            this.colSiteCategory,
            this.colExternalCustomerStatus,
            this.colCompletionSheetNumber,
            this.colStatusIssueID,
            this.colStatusIssue,
            this.colDaysSeparation,
            this.colSiteContractDaysSeparationPercent,
            this.colBillingRequirmentsFulfilled,
            this.colBillingRequirmentsUnfulfilled,
            this.colBillingWarningAuthorised,
            this.colBillingWarningUnauthorised,
            this.colLabourName1,
            this.colLinkedToPersonType,
            this.colLinkedToPersonTypeID,
            this.colDoNotInvoiceClient1,
            this.colDoNotPayContractor1,
            this.colSelfBillingAmount,
            this.colSelfBillingComment,
            this.colSelfBillingInvoiceHeaderID,
            this.colVisitLabourCalculatedID,
            this.colUniqueID1,
            this.colCompanyName1,
            this.colInvoicePDF});
            gridFormatRule1.Column = this.colBillingRequirmentsUnfulfilled;
            gridFormatRule1.ColumnApplyTo = this.colBillingRequirmentsFulfilled;
            gridFormatRule1.Name = "FormatBillingRequirementFulfilled";
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.PredefinedName = "Green Fill, Green Text";
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            gridFormatRule2.Column = this.colBillingRequirmentsUnfulfilled;
            gridFormatRule2.ColumnApplyTo = this.colBillingRequirmentsUnfulfilled;
            gridFormatRule2.Name = "FormatBillingRequirementUnfulfilled";
            formatConditionRuleValue2.Condition = DevExpress.XtraEditors.FormatCondition.Greater;
            formatConditionRuleValue2.PredefinedName = "Red Fill, Red Text";
            formatConditionRuleValue2.Value1 = 0;
            gridFormatRule2.Rule = formatConditionRuleValue2;
            gridFormatRule3.Column = this.colBillingWarningUnauthorised;
            gridFormatRule3.ColumnApplyTo = this.colBillingWarningAuthorised;
            gridFormatRule3.Name = "FormatBillingWarningAuthorised";
            formatConditionRuleValue3.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue3.PredefinedName = "Green Fill, Green Text";
            formatConditionRuleValue3.Value1 = 0;
            gridFormatRule3.Rule = formatConditionRuleValue3;
            gridFormatRule4.Column = this.colBillingWarningUnauthorised;
            gridFormatRule4.ColumnApplyTo = this.colBillingWarningUnauthorised;
            gridFormatRule4.Name = "FormatBillingWarningUnauthorised";
            formatConditionRuleValue4.Condition = DevExpress.XtraEditors.FormatCondition.Greater;
            formatConditionRuleValue4.PredefinedName = "Red Fill, Red Text";
            formatConditionRuleValue4.Value1 = 0;
            gridFormatRule4.Rule = formatConditionRuleValue4;
            this.gridViewVisit.FormatRules.Add(gridFormatRule1);
            this.gridViewVisit.FormatRules.Add(gridFormatRule2);
            this.gridViewVisit.FormatRules.Add(gridFormatRule3);
            this.gridViewVisit.FormatRules.Add(gridFormatRule4);
            this.gridViewVisit.GridControl = this.gridControlVisit;
            this.gridViewVisit.GroupCount = 4;
            this.gridViewVisit.Name = "gridViewVisit";
            this.gridViewVisit.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewVisit.OptionsFind.AlwaysVisible = true;
            this.gridViewVisit.OptionsFind.FindDelay = 2000;
            this.gridViewVisit.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewVisit.OptionsLayout.StoreAppearance = true;
            this.gridViewVisit.OptionsLayout.StoreFormatRules = true;
            this.gridViewVisit.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewVisit.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewVisit.OptionsSelection.MultiSelect = true;
            this.gridViewVisit.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewVisit.OptionsView.ColumnAutoWidth = false;
            this.gridViewVisit.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewVisit.OptionsView.ShowGroupPanel = false;
            this.gridViewVisit.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCompanyName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLabourName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSelfBillingInvoiceHeaderID, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colVisitNumber, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewVisit.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewVisit_CustomRowCellEdit);
            this.gridViewVisit.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewVisit.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewVisit.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewVisit.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewVisit_ShowingEditor);
            this.gridViewVisit.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewVisit.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewVisit.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewVisit.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewVisit_MouseUp);
            this.gridViewVisit.DoubleClick += new System.EventHandler(this.gridViewVisit_DoubleClick);
            this.gridViewVisit.GotFocus += new System.EventHandler(this.gridViewVisit_GotFocus);
            // 
            // colVisitID
            // 
            this.colVisitID.Caption = "Visit ID";
            this.colVisitID.FieldName = "VisitID";
            this.colVisitID.Name = "colVisitID";
            this.colVisitID.OptionsColumn.AllowEdit = false;
            this.colVisitID.OptionsColumn.AllowFocus = false;
            this.colVisitID.OptionsColumn.ReadOnly = true;
            // 
            // colSiteContractID
            // 
            this.colSiteContractID.Caption = "Site Contract ID";
            this.colSiteContractID.FieldName = "SiteContractID";
            this.colSiteContractID.Name = "colSiteContractID";
            this.colSiteContractID.OptionsColumn.AllowEdit = false;
            this.colSiteContractID.OptionsColumn.AllowFocus = false;
            this.colSiteContractID.OptionsColumn.ReadOnly = true;
            this.colSiteContractID.Width = 98;
            // 
            // colVisitNumber
            // 
            this.colVisitNumber.Caption = "Visit #";
            this.colVisitNumber.ColumnEdit = this.repositoryItemTextEditInteger2;
            this.colVisitNumber.FieldName = "VisitNumber";
            this.colVisitNumber.Name = "colVisitNumber";
            this.colVisitNumber.OptionsColumn.AllowEdit = false;
            this.colVisitNumber.OptionsColumn.AllowFocus = false;
            this.colVisitNumber.OptionsColumn.ReadOnly = true;
            this.colVisitNumber.Visible = true;
            this.colVisitNumber.VisibleIndex = 1;
            this.colVisitNumber.Width = 95;
            // 
            // colCreatedByStaffID
            // 
            this.colCreatedByStaffID.Caption = "Created By Staff ID";
            this.colCreatedByStaffID.FieldName = "CreatedByStaffID";
            this.colCreatedByStaffID.Name = "colCreatedByStaffID";
            this.colCreatedByStaffID.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffID.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffID.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffID.Width = 116;
            // 
            // colExpectedStartDate
            // 
            this.colExpectedStartDate.Caption = "Expected Start";
            this.colExpectedStartDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colExpectedStartDate.FieldName = "ExpectedStartDate";
            this.colExpectedStartDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colExpectedStartDate.Name = "colExpectedStartDate";
            this.colExpectedStartDate.OptionsColumn.AllowEdit = false;
            this.colExpectedStartDate.OptionsColumn.AllowFocus = false;
            this.colExpectedStartDate.OptionsColumn.ReadOnly = true;
            this.colExpectedStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colExpectedStartDate.Visible = true;
            this.colExpectedStartDate.VisibleIndex = 8;
            this.colExpectedStartDate.Width = 104;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colExpectedEndDate
            // 
            this.colExpectedEndDate.Caption = "Expected End";
            this.colExpectedEndDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colExpectedEndDate.FieldName = "ExpectedEndDate";
            this.colExpectedEndDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colExpectedEndDate.Name = "colExpectedEndDate";
            this.colExpectedEndDate.OptionsColumn.AllowEdit = false;
            this.colExpectedEndDate.OptionsColumn.AllowFocus = false;
            this.colExpectedEndDate.OptionsColumn.ReadOnly = true;
            this.colExpectedEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colExpectedEndDate.Visible = true;
            this.colExpectedEndDate.VisibleIndex = 9;
            this.colExpectedEndDate.Width = 100;
            // 
            // colStartDate
            // 
            this.colStartDate.Caption = "Start";
            this.colStartDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colStartDate.FieldName = "StartDate";
            this.colStartDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colStartDate.Name = "colStartDate";
            this.colStartDate.OptionsColumn.AllowEdit = false;
            this.colStartDate.OptionsColumn.AllowFocus = false;
            this.colStartDate.OptionsColumn.ReadOnly = true;
            this.colStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colStartDate.Visible = true;
            this.colStartDate.VisibleIndex = 3;
            this.colStartDate.Width = 100;
            // 
            // colEndDate
            // 
            this.colEndDate.Caption = "End";
            this.colEndDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colEndDate.FieldName = "EndDate";
            this.colEndDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colEndDate.Name = "colEndDate";
            this.colEndDate.OptionsColumn.AllowEdit = false;
            this.colEndDate.OptionsColumn.AllowFocus = false;
            this.colEndDate.OptionsColumn.ReadOnly = true;
            this.colEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colEndDate.Visible = true;
            this.colEndDate.VisibleIndex = 4;
            this.colEndDate.Width = 100;
            // 
            // colCostCalculationLevel
            // 
            this.colCostCalculationLevel.Caption = "Cost Calculation ID";
            this.colCostCalculationLevel.FieldName = "CostCalculationLevel";
            this.colCostCalculationLevel.Name = "colCostCalculationLevel";
            this.colCostCalculationLevel.OptionsColumn.AllowEdit = false;
            this.colCostCalculationLevel.OptionsColumn.AllowFocus = false;
            this.colCostCalculationLevel.OptionsColumn.ReadOnly = true;
            this.colCostCalculationLevel.Width = 140;
            // 
            // colVisitCost
            // 
            this.colVisitCost.Caption = "Visit Cost";
            this.colVisitCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colVisitCost.FieldName = "VisitCost";
            this.colVisitCost.Name = "colVisitCost";
            this.colVisitCost.OptionsColumn.AllowEdit = false;
            this.colVisitCost.OptionsColumn.AllowFocus = false;
            this.colVisitCost.OptionsColumn.ReadOnly = true;
            this.colVisitCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VisitCost", "{0:c}")});
            this.colVisitCost.Visible = true;
            this.colVisitCost.VisibleIndex = 26;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // colVisitSell
            // 
            this.colVisitSell.Caption = "Visit Sell";
            this.colVisitSell.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colVisitSell.FieldName = "VisitSell";
            this.colVisitSell.Name = "colVisitSell";
            this.colVisitSell.OptionsColumn.AllowEdit = false;
            this.colVisitSell.OptionsColumn.AllowFocus = false;
            this.colVisitSell.OptionsColumn.ReadOnly = true;
            this.colVisitSell.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VisitSell", "{0:c}")});
            this.colVisitSell.Visible = true;
            this.colVisitSell.VisibleIndex = 27;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Remarks";
            this.gridColumn1.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.gridColumn1.FieldName = "Remarks";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 64;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colStartLatitude
            // 
            this.colStartLatitude.Caption = "Start Latitude";
            this.colStartLatitude.ColumnEdit = this.repositoryItemTextEdit1;
            this.colStartLatitude.FieldName = "StartLatitude";
            this.colStartLatitude.Name = "colStartLatitude";
            this.colStartLatitude.OptionsColumn.AllowEdit = false;
            this.colStartLatitude.OptionsColumn.AllowFocus = false;
            this.colStartLatitude.OptionsColumn.ReadOnly = true;
            this.colStartLatitude.Visible = true;
            this.colStartLatitude.VisibleIndex = 35;
            this.colStartLatitude.Width = 87;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "f8";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colStartLongitude
            // 
            this.colStartLongitude.Caption = "Start Longitude";
            this.colStartLongitude.ColumnEdit = this.repositoryItemTextEdit1;
            this.colStartLongitude.FieldName = "StartLongitude";
            this.colStartLongitude.Name = "colStartLongitude";
            this.colStartLongitude.OptionsColumn.AllowEdit = false;
            this.colStartLongitude.OptionsColumn.AllowFocus = false;
            this.colStartLongitude.OptionsColumn.ReadOnly = true;
            this.colStartLongitude.Visible = true;
            this.colStartLongitude.VisibleIndex = 36;
            this.colStartLongitude.Width = 95;
            // 
            // colFinishLatitude
            // 
            this.colFinishLatitude.Caption = "Finish Latitude";
            this.colFinishLatitude.ColumnEdit = this.repositoryItemTextEdit1;
            this.colFinishLatitude.FieldName = "FinishLatitude";
            this.colFinishLatitude.Name = "colFinishLatitude";
            this.colFinishLatitude.OptionsColumn.AllowEdit = false;
            this.colFinishLatitude.OptionsColumn.AllowFocus = false;
            this.colFinishLatitude.OptionsColumn.ReadOnly = true;
            this.colFinishLatitude.Visible = true;
            this.colFinishLatitude.VisibleIndex = 37;
            this.colFinishLatitude.Width = 90;
            // 
            // colFinishLongitude
            // 
            this.colFinishLongitude.Caption = "Finish Longitude";
            this.colFinishLongitude.ColumnEdit = this.repositoryItemTextEdit1;
            this.colFinishLongitude.FieldName = "FinishLongitude";
            this.colFinishLongitude.Name = "colFinishLongitude";
            this.colFinishLongitude.OptionsColumn.AllowEdit = false;
            this.colFinishLongitude.OptionsColumn.AllowFocus = false;
            this.colFinishLongitude.OptionsColumn.ReadOnly = true;
            this.colFinishLongitude.Visible = true;
            this.colFinishLongitude.VisibleIndex = 38;
            this.colFinishLongitude.Width = 98;
            // 
            // colCostCalculationLevelDescription
            // 
            this.colCostCalculationLevelDescription.Caption = "Cost Calculation";
            this.colCostCalculationLevelDescription.FieldName = "CostCalculationLevelDescription";
            this.colCostCalculationLevelDescription.Name = "colCostCalculationLevelDescription";
            this.colCostCalculationLevelDescription.OptionsColumn.AllowEdit = false;
            this.colCostCalculationLevelDescription.OptionsColumn.AllowFocus = false;
            this.colCostCalculationLevelDescription.OptionsColumn.ReadOnly = true;
            this.colCostCalculationLevelDescription.Visible = true;
            this.colCostCalculationLevelDescription.VisibleIndex = 24;
            this.colCostCalculationLevelDescription.Width = 100;
            // 
            // colClientContractID
            // 
            this.colClientContractID.Caption = "Client Contract ID";
            this.colClientContractID.FieldName = "ClientContractID";
            this.colClientContractID.Name = "colClientContractID";
            this.colClientContractID.OptionsColumn.AllowEdit = false;
            this.colClientContractID.OptionsColumn.AllowFocus = false;
            this.colClientContractID.OptionsColumn.ReadOnly = true;
            this.colClientContractID.Width = 107;
            // 
            // colSiteID
            // 
            this.colSiteID.Caption = "Site ID";
            this.colSiteID.FieldName = "SiteID";
            this.colSiteID.Name = "colSiteID";
            this.colSiteID.OptionsColumn.AllowEdit = false;
            this.colSiteID.OptionsColumn.AllowFocus = false;
            this.colSiteID.OptionsColumn.ReadOnly = true;
            this.colSiteID.Visible = true;
            this.colSiteID.VisibleIndex = 31;
            this.colSiteID.Width = 51;
            // 
            // colSiteContractValue
            // 
            this.colSiteContractValue.Caption = "Site Contract Value";
            this.colSiteContractValue.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSiteContractValue.FieldName = "SiteContractValue";
            this.colSiteContractValue.Name = "colSiteContractValue";
            this.colSiteContractValue.OptionsColumn.AllowEdit = false;
            this.colSiteContractValue.OptionsColumn.AllowFocus = false;
            this.colSiteContractValue.OptionsColumn.ReadOnly = true;
            this.colSiteContractValue.Width = 113;
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // colContractDirectorID
            // 
            this.colContractDirectorID.Caption = "Contract Director ID";
            this.colContractDirectorID.FieldName = "ContractDirectorID";
            this.colContractDirectorID.Name = "colContractDirectorID";
            this.colContractDirectorID.OptionsColumn.AllowEdit = false;
            this.colContractDirectorID.OptionsColumn.AllowFocus = false;
            this.colContractDirectorID.OptionsColumn.ReadOnly = true;
            this.colContractDirectorID.Width = 118;
            // 
            // colClientContractStartDate
            // 
            this.colClientContractStartDate.Caption = "Client Contract Start";
            this.colClientContractStartDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colClientContractStartDate.FieldName = "ClientContractStartDate";
            this.colClientContractStartDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colClientContractStartDate.Name = "colClientContractStartDate";
            this.colClientContractStartDate.OptionsColumn.AllowEdit = false;
            this.colClientContractStartDate.OptionsColumn.AllowFocus = false;
            this.colClientContractStartDate.OptionsColumn.ReadOnly = true;
            this.colClientContractStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colClientContractStartDate.Width = 120;
            // 
            // colClientContractEndDate
            // 
            this.colClientContractEndDate.Caption = "Client Contract End";
            this.colClientContractEndDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colClientContractEndDate.FieldName = "ClientContractEndDate";
            this.colClientContractEndDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colClientContractEndDate.Name = "colClientContractEndDate";
            this.colClientContractEndDate.OptionsColumn.AllowEdit = false;
            this.colClientContractEndDate.OptionsColumn.AllowFocus = false;
            this.colClientContractEndDate.OptionsColumn.ReadOnly = true;
            this.colClientContractEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colClientContractEndDate.Width = 114;
            // 
            // colClientContractActive
            // 
            this.colClientContractActive.Caption = "Client Contract Active";
            this.colClientContractActive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colClientContractActive.FieldName = "ClientContractActive";
            this.colClientContractActive.Name = "colClientContractActive";
            this.colClientContractActive.OptionsColumn.AllowEdit = false;
            this.colClientContractActive.OptionsColumn.AllowFocus = false;
            this.colClientContractActive.OptionsColumn.ReadOnly = true;
            this.colClientContractActive.Width = 126;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colClientContractValue
            // 
            this.colClientContractValue.Caption = "Client Contract Value";
            this.colClientContractValue.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colClientContractValue.FieldName = "ClientContractValue";
            this.colClientContractValue.Name = "colClientContractValue";
            this.colClientContractValue.OptionsColumn.AllowEdit = false;
            this.colClientContractValue.OptionsColumn.AllowFocus = false;
            this.colClientContractValue.OptionsColumn.ReadOnly = true;
            this.colClientContractValue.Width = 122;
            // 
            // colFinanceClientCode
            // 
            this.colFinanceClientCode.Caption = "Finance Client Code";
            this.colFinanceClientCode.FieldName = "FinanceClientCode";
            this.colFinanceClientCode.Name = "colFinanceClientCode";
            this.colFinanceClientCode.OptionsColumn.AllowEdit = false;
            this.colFinanceClientCode.OptionsColumn.AllowFocus = false;
            this.colFinanceClientCode.OptionsColumn.ReadOnly = true;
            this.colFinanceClientCode.Width = 116;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 1;
            this.colClientName.Width = 174;
            // 
            // colContractType
            // 
            this.colContractType.Caption = "Contract Type";
            this.colContractType.FieldName = "ContractType";
            this.colContractType.Name = "colContractType";
            this.colContractType.OptionsColumn.AllowEdit = false;
            this.colContractType.OptionsColumn.AllowFocus = false;
            this.colContractType.OptionsColumn.ReadOnly = true;
            this.colContractType.Visible = true;
            this.colContractType.VisibleIndex = 40;
            this.colContractType.Width = 117;
            // 
            // colContractStatus
            // 
            this.colContractStatus.Caption = "Contract Status";
            this.colContractStatus.FieldName = "ContractStatus";
            this.colContractStatus.Name = "colContractStatus";
            this.colContractStatus.OptionsColumn.AllowEdit = false;
            this.colContractStatus.OptionsColumn.AllowFocus = false;
            this.colContractStatus.OptionsColumn.ReadOnly = true;
            this.colContractStatus.Visible = true;
            this.colContractStatus.VisibleIndex = 41;
            this.colContractStatus.Width = 117;
            // 
            // colContractDirector
            // 
            this.colContractDirector.Caption = "Contract Director";
            this.colContractDirector.FieldName = "ContractDirector";
            this.colContractDirector.Name = "colContractDirector";
            this.colContractDirector.OptionsColumn.AllowEdit = false;
            this.colContractDirector.OptionsColumn.AllowFocus = false;
            this.colContractDirector.OptionsColumn.ReadOnly = true;
            this.colContractDirector.Visible = true;
            this.colContractDirector.VisibleIndex = 42;
            this.colContractDirector.Width = 122;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.Visible = true;
            this.colSiteName.VisibleIndex = 0;
            this.colSiteName.Width = 270;
            // 
            // colLinkedToParent1
            // 
            this.colLinkedToParent1.Caption = "Linked To Site Contract";
            this.colLinkedToParent1.ColumnEdit = this.repositoryItemTextEditHTML0;
            this.colLinkedToParent1.FieldName = "LinkedToParent";
            this.colLinkedToParent1.Name = "colLinkedToParent1";
            this.colLinkedToParent1.OptionsColumn.AllowEdit = false;
            this.colLinkedToParent1.OptionsColumn.AllowFocus = false;
            this.colLinkedToParent1.OptionsColumn.ReadOnly = true;
            this.colLinkedToParent1.Width = 333;
            // 
            // repositoryItemTextEditHTML0
            // 
            this.repositoryItemTextEditHTML0.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTextEditHTML0.AutoHeight = false;
            this.repositoryItemTextEditHTML0.Name = "repositoryItemTextEditHTML0";
            // 
            // colContractDescription
            // 
            this.colContractDescription.Caption = "Contract Description";
            this.colContractDescription.FieldName = "ContractDescription";
            this.colContractDescription.Name = "colContractDescription";
            this.colContractDescription.OptionsColumn.AllowEdit = false;
            this.colContractDescription.OptionsColumn.AllowFocus = false;
            this.colContractDescription.OptionsColumn.ReadOnly = true;
            this.colContractDescription.Visible = true;
            this.colContractDescription.VisibleIndex = 11;
            this.colContractDescription.Width = 225;
            // 
            // colSitePostcode
            // 
            this.colSitePostcode.Caption = "Site Postcode";
            this.colSitePostcode.FieldName = "SitePostcode";
            this.colSitePostcode.Name = "colSitePostcode";
            this.colSitePostcode.OptionsColumn.AllowEdit = false;
            this.colSitePostcode.OptionsColumn.AllowFocus = false;
            this.colSitePostcode.OptionsColumn.ReadOnly = true;
            this.colSitePostcode.Width = 86;
            // 
            // colSiteLocationX
            // 
            this.colSiteLocationX.Caption = "Site Latitude";
            this.colSiteLocationX.ColumnEdit = this.repositoryItemTextEdit1;
            this.colSiteLocationX.FieldName = "SiteLocationX";
            this.colSiteLocationX.Name = "colSiteLocationX";
            this.colSiteLocationX.OptionsColumn.AllowEdit = false;
            this.colSiteLocationX.OptionsColumn.AllowFocus = false;
            this.colSiteLocationX.OptionsColumn.ReadOnly = true;
            this.colSiteLocationX.Width = 81;
            // 
            // colSiteLocationY
            // 
            this.colSiteLocationY.Caption = "Site Longitude";
            this.colSiteLocationY.ColumnEdit = this.repositoryItemTextEdit1;
            this.colSiteLocationY.FieldName = "SiteLocationY";
            this.colSiteLocationY.Name = "colSiteLocationY";
            this.colSiteLocationY.OptionsColumn.AllowEdit = false;
            this.colSiteLocationY.OptionsColumn.AllowFocus = false;
            this.colSiteLocationY.OptionsColumn.ReadOnly = true;
            this.colSiteLocationY.Width = 89;
            // 
            // colLinkedJobCount
            // 
            this.colLinkedJobCount.Caption = "Linked Jobs Total";
            this.colLinkedJobCount.ColumnEdit = this.repositoryItemHyperLinkEditGrid1;
            this.colLinkedJobCount.FieldName = "LinkedJobCount";
            this.colLinkedJobCount.Name = "colLinkedJobCount";
            this.colLinkedJobCount.OptionsColumn.ReadOnly = true;
            this.colLinkedJobCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "LinkedJobCount", "{0:n}")});
            this.colLinkedJobCount.Width = 103;
            // 
            // repositoryItemHyperLinkEditGrid1
            // 
            this.repositoryItemHyperLinkEditGrid1.AutoHeight = false;
            this.repositoryItemHyperLinkEditGrid1.Name = "repositoryItemHyperLinkEditGrid1";
            this.repositoryItemHyperLinkEditGrid1.SingleClick = true;
            this.repositoryItemHyperLinkEditGrid1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditGrid1_OpenLink);
            // 
            // colLinkedOutstandingJobCount
            // 
            this.colLinkedOutstandingJobCount.Caption = "Linked Jobs Outstanding";
            this.colLinkedOutstandingJobCount.ColumnEdit = this.repositoryItemHyperLinkEditGrid1;
            this.colLinkedOutstandingJobCount.FieldName = "LinkedOutstandingJobCount";
            this.colLinkedOutstandingJobCount.Name = "colLinkedOutstandingJobCount";
            this.colLinkedOutstandingJobCount.OptionsColumn.ReadOnly = true;
            this.colLinkedOutstandingJobCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "LinkedOutstandingJobCount", "{0:n}")});
            this.colLinkedOutstandingJobCount.Visible = true;
            this.colLinkedOutstandingJobCount.VisibleIndex = 43;
            this.colLinkedOutstandingJobCount.Width = 138;
            // 
            // colLinkedCompletedJobCount
            // 
            this.colLinkedCompletedJobCount.Caption = "Linked Jobs Completed";
            this.colLinkedCompletedJobCount.ColumnEdit = this.repositoryItemHyperLinkEditGrid1;
            this.colLinkedCompletedJobCount.FieldName = "LinkedCompletedJobCount";
            this.colLinkedCompletedJobCount.Name = "colLinkedCompletedJobCount";
            this.colLinkedCompletedJobCount.OptionsColumn.ReadOnly = true;
            this.colLinkedCompletedJobCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "LinkedCompletedJobCount", "{0:n}")});
            this.colLinkedCompletedJobCount.Visible = true;
            this.colLinkedCompletedJobCount.VisibleIndex = 44;
            this.colLinkedCompletedJobCount.Width = 130;
            // 
            // colCreatedByStaffName
            // 
            this.colCreatedByStaffName.Caption = "Created By";
            this.colCreatedByStaffName.FieldName = "CreatedByStaffName";
            this.colCreatedByStaffName.Name = "colCreatedByStaffName";
            this.colCreatedByStaffName.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffName.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffName.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffName.Visible = true;
            this.colCreatedByStaffName.VisibleIndex = 29;
            this.colCreatedByStaffName.Width = 122;
            // 
            // colSiteAddress
            // 
            this.colSiteAddress.Caption = "Site Address";
            this.colSiteAddress.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colSiteAddress.FieldName = "SiteAddress";
            this.colSiteAddress.Name = "colSiteAddress";
            this.colSiteAddress.OptionsColumn.ReadOnly = true;
            this.colSiteAddress.Visible = true;
            this.colSiteAddress.VisibleIndex = 34;
            this.colSiteAddress.Width = 125;
            // 
            // colVisitStatus
            // 
            this.colVisitStatus.Caption = "System Status";
            this.colVisitStatus.FieldName = "VisitStatus";
            this.colVisitStatus.Name = "colVisitStatus";
            this.colVisitStatus.OptionsColumn.AllowEdit = false;
            this.colVisitStatus.OptionsColumn.AllowFocus = false;
            this.colVisitStatus.OptionsColumn.ReadOnly = true;
            this.colVisitStatus.Visible = true;
            this.colVisitStatus.VisibleIndex = 19;
            this.colVisitStatus.Width = 130;
            // 
            // colVisitStatusID
            // 
            this.colVisitStatusID.Caption = "System Status ID";
            this.colVisitStatusID.FieldName = "VisitStatusID";
            this.colVisitStatusID.Name = "colVisitStatusID";
            this.colVisitStatusID.OptionsColumn.AllowEdit = false;
            this.colVisitStatusID.OptionsColumn.AllowFocus = false;
            this.colVisitStatusID.OptionsColumn.ReadOnly = true;
            this.colVisitStatusID.Width = 88;
            // 
            // colTimeliness
            // 
            this.colTimeliness.Caption = "Visit Status";
            this.colTimeliness.FieldName = "Timeliness";
            this.colTimeliness.Name = "colTimeliness";
            this.colTimeliness.OptionsColumn.AllowEdit = false;
            this.colTimeliness.OptionsColumn.AllowFocus = false;
            this.colTimeliness.OptionsColumn.ReadOnly = true;
            this.colTimeliness.Visible = true;
            this.colTimeliness.VisibleIndex = 20;
            this.colTimeliness.Width = 89;
            // 
            // colWorkNumber
            // 
            this.colWorkNumber.Caption = "Work Number";
            this.colWorkNumber.FieldName = "WorkNumber";
            this.colWorkNumber.Name = "colWorkNumber";
            this.colWorkNumber.OptionsColumn.AllowEdit = false;
            this.colWorkNumber.OptionsColumn.AllowFocus = false;
            this.colWorkNumber.OptionsColumn.ReadOnly = true;
            this.colWorkNumber.Visible = true;
            this.colWorkNumber.VisibleIndex = 23;
            this.colWorkNumber.Width = 86;
            // 
            // colVisitCategory
            // 
            this.colVisitCategory.Caption = "Visit Category";
            this.colVisitCategory.FieldName = "VisitCategory";
            this.colVisitCategory.Name = "colVisitCategory";
            this.colVisitCategory.OptionsColumn.AllowEdit = false;
            this.colVisitCategory.OptionsColumn.AllowFocus = false;
            this.colVisitCategory.OptionsColumn.ReadOnly = true;
            this.colVisitCategory.Visible = true;
            this.colVisitCategory.VisibleIndex = 17;
            this.colVisitCategory.Width = 130;
            // 
            // colVisitCategoryID
            // 
            this.colVisitCategoryID.Caption = "Visit Category ID";
            this.colVisitCategoryID.FieldName = "VisitCategoryID";
            this.colVisitCategoryID.Name = "colVisitCategoryID";
            this.colVisitCategoryID.OptionsColumn.AllowEdit = false;
            this.colVisitCategoryID.OptionsColumn.AllowFocus = false;
            this.colVisitCategoryID.OptionsColumn.ReadOnly = true;
            this.colVisitCategoryID.Width = 102;
            // 
            // colIssueFound
            // 
            this.colIssueFound.Caption = "Issue Found";
            this.colIssueFound.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIssueFound.FieldName = "IssueFound";
            this.colIssueFound.Name = "colIssueFound";
            this.colIssueFound.OptionsColumn.AllowEdit = false;
            this.colIssueFound.OptionsColumn.AllowFocus = false;
            this.colIssueFound.OptionsColumn.ReadOnly = true;
            this.colIssueFound.Visible = true;
            this.colIssueFound.VisibleIndex = 51;
            this.colIssueFound.Width = 80;
            // 
            // colIssueType
            // 
            this.colIssueType.Caption = "Issue Type";
            this.colIssueType.FieldName = "IssueType";
            this.colIssueType.Name = "colIssueType";
            this.colIssueType.OptionsColumn.AllowEdit = false;
            this.colIssueType.OptionsColumn.AllowFocus = false;
            this.colIssueType.OptionsColumn.ReadOnly = true;
            this.colIssueType.Visible = true;
            this.colIssueType.VisibleIndex = 52;
            // 
            // colIssueTypeID
            // 
            this.colIssueTypeID.Caption = "Issue Type ID";
            this.colIssueTypeID.FieldName = "IssueTypeID";
            this.colIssueTypeID.Name = "colIssueTypeID";
            this.colIssueTypeID.OptionsColumn.AllowEdit = false;
            this.colIssueTypeID.OptionsColumn.AllowFocus = false;
            this.colIssueTypeID.OptionsColumn.ReadOnly = true;
            this.colIssueTypeID.Width = 88;
            // 
            // colIssueRemarks
            // 
            this.colIssueRemarks.Caption = "Issue Remarks";
            this.colIssueRemarks.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colIssueRemarks.FieldName = "IssueRemarks";
            this.colIssueRemarks.Name = "colIssueRemarks";
            this.colIssueRemarks.OptionsColumn.ReadOnly = true;
            this.colIssueRemarks.Visible = true;
            this.colIssueRemarks.VisibleIndex = 53;
            this.colIssueRemarks.Width = 91;
            // 
            // colClientSignaturePath
            // 
            this.colClientSignaturePath.Caption = "Client Signature";
            this.colClientSignaturePath.ColumnEdit = this.repositoryItemHyperLinkEditClientSignature;
            this.colClientSignaturePath.FieldName = "ClientSignaturePath";
            this.colClientSignaturePath.Name = "colClientSignaturePath";
            this.colClientSignaturePath.OptionsColumn.ReadOnly = true;
            this.colClientSignaturePath.Visible = true;
            this.colClientSignaturePath.VisibleIndex = 47;
            this.colClientSignaturePath.Width = 97;
            // 
            // repositoryItemHyperLinkEditClientSignature
            // 
            this.repositoryItemHyperLinkEditClientSignature.AutoHeight = false;
            this.repositoryItemHyperLinkEditClientSignature.Name = "repositoryItemHyperLinkEditClientSignature";
            this.repositoryItemHyperLinkEditClientSignature.SingleClick = true;
            this.repositoryItemHyperLinkEditClientSignature.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditClientSignature_OpenLink);
            // 
            // colImagesFolderOM
            // 
            this.colImagesFolderOM.Caption = "Client Images Folder";
            this.colImagesFolderOM.FieldName = "ImagesFolderOM";
            this.colImagesFolderOM.Name = "colImagesFolderOM";
            this.colImagesFolderOM.OptionsColumn.AllowEdit = false;
            this.colImagesFolderOM.OptionsColumn.AllowFocus = false;
            this.colImagesFolderOM.OptionsColumn.ReadOnly = true;
            this.colImagesFolderOM.Width = 119;
            // 
            // colRework
            // 
            this.colRework.Caption = "Rework";
            this.colRework.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colRework.FieldName = "Rework";
            this.colRework.Name = "colRework";
            this.colRework.OptionsColumn.AllowEdit = false;
            this.colRework.OptionsColumn.AllowFocus = false;
            this.colRework.OptionsColumn.ReadOnly = true;
            this.colRework.Visible = true;
            this.colRework.VisibleIndex = 54;
            this.colRework.Width = 57;
            // 
            // colExtraWorkComments
            // 
            this.colExtraWorkComments.Caption = "Extra Work Remarks";
            this.colExtraWorkComments.FieldName = "ExtraWorkComments";
            this.colExtraWorkComments.Name = "colExtraWorkComments";
            this.colExtraWorkComments.OptionsColumn.AllowEdit = false;
            this.colExtraWorkComments.OptionsColumn.AllowFocus = false;
            this.colExtraWorkComments.OptionsColumn.ReadOnly = true;
            this.colExtraWorkComments.Visible = true;
            this.colExtraWorkComments.VisibleIndex = 57;
            this.colExtraWorkComments.Width = 119;
            // 
            // colExtraWorkRequired
            // 
            this.colExtraWorkRequired.Caption = "Extra Work Required";
            this.colExtraWorkRequired.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colExtraWorkRequired.FieldName = "ExtraWorkRequired";
            this.colExtraWorkRequired.Name = "colExtraWorkRequired";
            this.colExtraWorkRequired.OptionsColumn.AllowEdit = false;
            this.colExtraWorkRequired.OptionsColumn.AllowFocus = false;
            this.colExtraWorkRequired.OptionsColumn.ReadOnly = true;
            this.colExtraWorkRequired.Visible = true;
            this.colExtraWorkRequired.VisibleIndex = 55;
            this.colExtraWorkRequired.Width = 121;
            // 
            // colExtraWorkType
            // 
            this.colExtraWorkType.Caption = "Extra Work Type";
            this.colExtraWorkType.FieldName = "ExtraWorkType";
            this.colExtraWorkType.Name = "colExtraWorkType";
            this.colExtraWorkType.OptionsColumn.AllowEdit = false;
            this.colExtraWorkType.OptionsColumn.AllowFocus = false;
            this.colExtraWorkType.OptionsColumn.ReadOnly = true;
            this.colExtraWorkType.Visible = true;
            this.colExtraWorkType.VisibleIndex = 56;
            this.colExtraWorkType.Width = 102;
            // 
            // colAccidentOnSite
            // 
            this.colAccidentOnSite.Caption = "Accident on Site";
            this.colAccidentOnSite.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colAccidentOnSite.FieldName = "AccidentOnSite";
            this.colAccidentOnSite.Name = "colAccidentOnSite";
            this.colAccidentOnSite.OptionsColumn.AllowEdit = false;
            this.colAccidentOnSite.OptionsColumn.AllowFocus = false;
            this.colAccidentOnSite.OptionsColumn.ReadOnly = true;
            this.colAccidentOnSite.Visible = true;
            this.colAccidentOnSite.VisibleIndex = 58;
            this.colAccidentOnSite.Width = 98;
            // 
            // colAccidentComment
            // 
            this.colAccidentComment.Caption = "Accident Comments";
            this.colAccidentComment.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colAccidentComment.FieldName = "AccidentComment";
            this.colAccidentComment.Name = "colAccidentComment";
            this.colAccidentComment.OptionsColumn.ReadOnly = true;
            this.colAccidentComment.Visible = true;
            this.colAccidentComment.VisibleIndex = 59;
            this.colAccidentComment.Width = 115;
            // 
            // colSuspendedJobCount
            // 
            this.colSuspendedJobCount.Caption = "Suspended Jobs";
            this.colSuspendedJobCount.ColumnEdit = this.repositoryItemHyperLinkEditGrid1;
            this.colSuspendedJobCount.FieldName = "SuspendedJobCount";
            this.colSuspendedJobCount.Name = "colSuspendedJobCount";
            this.colSuspendedJobCount.OptionsColumn.ReadOnly = true;
            this.colSuspendedJobCount.Visible = true;
            this.colSuspendedJobCount.VisibleIndex = 46;
            this.colSuspendedJobCount.Width = 97;
            // 
            // colLinkedPictureCount
            // 
            this.colLinkedPictureCount.Caption = "Linked Pictures";
            this.colLinkedPictureCount.ColumnEdit = this.repositoryItemHyperLinkEditGrid1;
            this.colLinkedPictureCount.FieldName = "LinkedPictureCount";
            this.colLinkedPictureCount.Name = "colLinkedPictureCount";
            this.colLinkedPictureCount.OptionsColumn.ReadOnly = true;
            this.colLinkedPictureCount.Visible = true;
            this.colLinkedPictureCount.VisibleIndex = 45;
            this.colLinkedPictureCount.Width = 90;
            // 
            // colFriendlyVisitNumber
            // 
            this.colFriendlyVisitNumber.Caption = "Friendly Visit #";
            this.colFriendlyVisitNumber.FieldName = "FriendlyVisitNumber";
            this.colFriendlyVisitNumber.Name = "colFriendlyVisitNumber";
            this.colFriendlyVisitNumber.OptionsColumn.AllowEdit = false;
            this.colFriendlyVisitNumber.OptionsColumn.AllowFocus = false;
            this.colFriendlyVisitNumber.OptionsColumn.ReadOnly = true;
            this.colFriendlyVisitNumber.Visible = true;
            this.colFriendlyVisitNumber.VisibleIndex = 2;
            this.colFriendlyVisitNumber.Width = 103;
            // 
            // colVisitType
            // 
            this.colVisitType.Caption = "Visit Type";
            this.colVisitType.FieldName = "VisitType";
            this.colVisitType.Name = "colVisitType";
            this.colVisitType.OptionsColumn.AllowEdit = false;
            this.colVisitType.OptionsColumn.AllowFocus = false;
            this.colVisitType.OptionsColumn.ReadOnly = true;
            this.colVisitType.Visible = true;
            this.colVisitType.VisibleIndex = 18;
            this.colVisitType.Width = 99;
            // 
            // colVisitTypeID
            // 
            this.colVisitTypeID.Caption = "Visit Type ID";
            this.colVisitTypeID.FieldName = "VisitTypeID";
            this.colVisitTypeID.Name = "colVisitTypeID";
            this.colVisitTypeID.OptionsColumn.AllowEdit = false;
            this.colVisitTypeID.OptionsColumn.AllowFocus = false;
            this.colVisitTypeID.OptionsColumn.ReadOnly = true;
            this.colVisitTypeID.Width = 79;
            // 
            // colClientPO
            // 
            this.colClientPO.Caption = "Client PO #";
            this.colClientPO.FieldName = "ClientPO";
            this.colClientPO.Name = "colClientPO";
            this.colClientPO.OptionsColumn.AllowEdit = false;
            this.colClientPO.OptionsColumn.AllowFocus = false;
            this.colClientPO.OptionsColumn.ReadOnly = true;
            this.colClientPO.Visible = true;
            this.colClientPO.VisibleIndex = 30;
            this.colClientPO.Width = 112;
            // 
            // colClientPOID1
            // 
            this.colClientPOID1.Caption = "Client PO ID";
            this.colClientPOID1.FieldName = "ClientPOID";
            this.colClientPOID1.Name = "colClientPOID1";
            this.colClientPOID1.OptionsColumn.AllowEdit = false;
            this.colClientPOID1.OptionsColumn.AllowFocus = false;
            this.colClientPOID1.OptionsColumn.ReadOnly = true;
            this.colClientPOID1.Width = 77;
            // 
            // colSiteCode
            // 
            this.colSiteCode.Caption = "Site Code";
            this.colSiteCode.FieldName = "SiteCode";
            this.colSiteCode.Name = "colSiteCode";
            this.colSiteCode.OptionsColumn.AllowEdit = false;
            this.colSiteCode.OptionsColumn.AllowFocus = false;
            this.colSiteCode.OptionsColumn.ReadOnly = true;
            this.colSiteCode.Visible = true;
            this.colSiteCode.VisibleIndex = 32;
            // 
            // colSellCalculationLevel
            // 
            this.colSellCalculationLevel.Caption = "Sell Calculation ID";
            this.colSellCalculationLevel.FieldName = "SellCalculationLevel";
            this.colSellCalculationLevel.Name = "colSellCalculationLevel";
            this.colSellCalculationLevel.OptionsColumn.AllowEdit = false;
            this.colSellCalculationLevel.OptionsColumn.AllowFocus = false;
            this.colSellCalculationLevel.OptionsColumn.ReadOnly = true;
            this.colSellCalculationLevel.Width = 104;
            // 
            // colSellCalculationLevelDescription
            // 
            this.colSellCalculationLevelDescription.Caption = "Sell Calculation";
            this.colSellCalculationLevelDescription.FieldName = "SellCalculationLevelDescription";
            this.colSellCalculationLevelDescription.Name = "colSellCalculationLevelDescription";
            this.colSellCalculationLevelDescription.OptionsColumn.AllowEdit = false;
            this.colSellCalculationLevelDescription.OptionsColumn.AllowFocus = false;
            this.colSellCalculationLevelDescription.OptionsColumn.ReadOnly = true;
            this.colSellCalculationLevelDescription.Visible = true;
            this.colSellCalculationLevelDescription.VisibleIndex = 25;
            this.colSellCalculationLevelDescription.Width = 100;
            // 
            // colSelected4
            // 
            this.colSelected4.Caption = "Selected";
            this.colSelected4.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSelected4.FieldName = "Selected";
            this.colSelected4.Name = "colSelected4";
            this.colSelected4.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Selected", "{0:0}")});
            this.colSelected4.Width = 60;
            // 
            // colClientInvoiceID1
            // 
            this.colClientInvoiceID1.Caption = "Client Invoice ID";
            this.colClientInvoiceID1.ColumnEdit = this.repositoryItemHyperLinkEditClientInvoiceID;
            this.colClientInvoiceID1.FieldName = "ClientInvoiceID";
            this.colClientInvoiceID1.Name = "colClientInvoiceID1";
            this.colClientInvoiceID1.OptionsColumn.ReadOnly = true;
            this.colClientInvoiceID1.Visible = true;
            this.colClientInvoiceID1.VisibleIndex = 62;
            this.colClientInvoiceID1.Width = 98;
            // 
            // repositoryItemHyperLinkEditClientInvoiceID
            // 
            this.repositoryItemHyperLinkEditClientInvoiceID.AutoHeight = false;
            this.repositoryItemHyperLinkEditClientInvoiceID.Name = "repositoryItemHyperLinkEditClientInvoiceID";
            this.repositoryItemHyperLinkEditClientInvoiceID.SingleClick = true;
            this.repositoryItemHyperLinkEditClientInvoiceID.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditClientInvoiceID_OpenLink);
            // 
            // colCMName
            // 
            this.colCMName.Caption = "Contract Manager";
            this.colCMName.FieldName = "CMName";
            this.colCMName.Name = "colCMName";
            this.colCMName.OptionsColumn.AllowEdit = false;
            this.colCMName.OptionsColumn.AllowFocus = false;
            this.colCMName.OptionsColumn.ReadOnly = true;
            this.colCMName.Visible = true;
            this.colCMName.VisibleIndex = 22;
            this.colCMName.Width = 106;
            // 
            // colDaysAllowed
            // 
            this.colDaysAllowed.Caption = "Days Allowed";
            this.colDaysAllowed.FieldName = "DaysAllowed";
            this.colDaysAllowed.Name = "colDaysAllowed";
            this.colDaysAllowed.OptionsColumn.AllowEdit = false;
            this.colDaysAllowed.OptionsColumn.AllowFocus = false;
            this.colDaysAllowed.OptionsColumn.ReadOnly = true;
            this.colDaysAllowed.Visible = true;
            this.colDaysAllowed.VisibleIndex = 6;
            this.colDaysAllowed.Width = 83;
            // 
            // colManagerName
            // 
            this.colManagerName.Caption = "Signed By Manager";
            this.colManagerName.FieldName = "ManagerName";
            this.colManagerName.Name = "colManagerName";
            this.colManagerName.OptionsColumn.AllowEdit = false;
            this.colManagerName.OptionsColumn.AllowFocus = false;
            this.colManagerName.OptionsColumn.ReadOnly = true;
            this.colManagerName.Visible = true;
            this.colManagerName.VisibleIndex = 48;
            this.colManagerName.Width = 111;
            // 
            // colManagerNotes
            // 
            this.colManagerNotes.Caption = "Signed By Notes";
            this.colManagerNotes.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colManagerNotes.FieldName = "ManagerNotes";
            this.colManagerNotes.Name = "colManagerNotes";
            this.colManagerNotes.OptionsColumn.ReadOnly = true;
            this.colManagerNotes.Visible = true;
            this.colManagerNotes.VisibleIndex = 49;
            this.colManagerNotes.Width = 97;
            // 
            // colNoOnetoSign
            // 
            this.colNoOnetoSign.Caption = "No One To Sign ";
            this.colNoOnetoSign.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colNoOnetoSign.FieldName = "NoOnetoSign";
            this.colNoOnetoSign.Name = "colNoOnetoSign";
            this.colNoOnetoSign.OptionsColumn.AllowEdit = false;
            this.colNoOnetoSign.OptionsColumn.AllowFocus = false;
            this.colNoOnetoSign.OptionsColumn.ReadOnly = true;
            this.colNoOnetoSign.Visible = true;
            this.colNoOnetoSign.VisibleIndex = 50;
            this.colNoOnetoSign.Width = 96;
            // 
            // colSiteCategory
            // 
            this.colSiteCategory.Caption = "Site Category";
            this.colSiteCategory.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colSiteCategory.FieldName = "SiteCategory";
            this.colSiteCategory.Name = "colSiteCategory";
            this.colSiteCategory.OptionsColumn.ReadOnly = true;
            this.colSiteCategory.Visible = true;
            this.colSiteCategory.VisibleIndex = 33;
            this.colSiteCategory.Width = 124;
            // 
            // colExternalCustomerStatus
            // 
            this.colExternalCustomerStatus.Caption = "External Customer Status";
            this.colExternalCustomerStatus.FieldName = "ExternalCustomerStatus";
            this.colExternalCustomerStatus.Name = "colExternalCustomerStatus";
            this.colExternalCustomerStatus.OptionsColumn.AllowEdit = false;
            this.colExternalCustomerStatus.OptionsColumn.AllowFocus = false;
            this.colExternalCustomerStatus.OptionsColumn.ReadOnly = true;
            this.colExternalCustomerStatus.Visible = true;
            this.colExternalCustomerStatus.VisibleIndex = 63;
            this.colExternalCustomerStatus.Width = 142;
            // 
            // colCompletionSheetNumber
            // 
            this.colCompletionSheetNumber.Caption = "Completion Sheet #";
            this.colCompletionSheetNumber.FieldName = "CompletionSheetNumber";
            this.colCompletionSheetNumber.Name = "colCompletionSheetNumber";
            this.colCompletionSheetNumber.OptionsColumn.AllowEdit = false;
            this.colCompletionSheetNumber.OptionsColumn.AllowFocus = false;
            this.colCompletionSheetNumber.OptionsColumn.ReadOnly = true;
            this.colCompletionSheetNumber.Visible = true;
            this.colCompletionSheetNumber.VisibleIndex = 39;
            this.colCompletionSheetNumber.Width = 114;
            // 
            // colStatusIssueID
            // 
            this.colStatusIssueID.Caption = "Status Issue ID";
            this.colStatusIssueID.FieldName = "StatusIssueID";
            this.colStatusIssueID.Name = "colStatusIssueID";
            this.colStatusIssueID.OptionsColumn.AllowEdit = false;
            this.colStatusIssueID.OptionsColumn.AllowFocus = false;
            this.colStatusIssueID.OptionsColumn.ReadOnly = true;
            this.colStatusIssueID.Width = 93;
            // 
            // colStatusIssue
            // 
            this.colStatusIssue.Caption = "Status Issue";
            this.colStatusIssue.FieldName = "StatusIssue";
            this.colStatusIssue.Name = "colStatusIssue";
            this.colStatusIssue.OptionsColumn.AllowEdit = false;
            this.colStatusIssue.OptionsColumn.AllowFocus = false;
            this.colStatusIssue.OptionsColumn.ReadOnly = true;
            this.colStatusIssue.Visible = true;
            this.colStatusIssue.VisibleIndex = 21;
            this.colStatusIssue.Width = 110;
            // 
            // colDaysSeparation
            // 
            this.colDaysSeparation.Caption = "Days Separation";
            this.colDaysSeparation.FieldName = "DaysSeparation";
            this.colDaysSeparation.Name = "colDaysSeparation";
            this.colDaysSeparation.OptionsColumn.AllowEdit = false;
            this.colDaysSeparation.OptionsColumn.AllowFocus = false;
            this.colDaysSeparation.OptionsColumn.ReadOnly = true;
            this.colDaysSeparation.Visible = true;
            this.colDaysSeparation.VisibleIndex = 7;
            this.colDaysSeparation.Width = 98;
            // 
            // colSiteContractDaysSeparationPercent
            // 
            this.colSiteContractDaysSeparationPercent.Caption = "Site Contract Days Separation %";
            this.colSiteContractDaysSeparationPercent.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colSiteContractDaysSeparationPercent.FieldName = "SiteContractDaysSeparationPercent";
            this.colSiteContractDaysSeparationPercent.Name = "colSiteContractDaysSeparationPercent";
            this.colSiteContractDaysSeparationPercent.OptionsColumn.AllowEdit = false;
            this.colSiteContractDaysSeparationPercent.OptionsColumn.AllowFocus = false;
            this.colSiteContractDaysSeparationPercent.OptionsColumn.ReadOnly = true;
            this.colSiteContractDaysSeparationPercent.Width = 178;
            // 
            // repositoryItemTextEditPercentage
            // 
            this.repositoryItemTextEditPercentage.AutoHeight = false;
            this.repositoryItemTextEditPercentage.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage.Name = "repositoryItemTextEditPercentage";
            // 
            // colLabourName1
            // 
            this.colLabourName1.Caption = "Labour Name";
            this.colLabourName1.FieldName = "LabourName";
            this.colLabourName1.Name = "colLabourName1";
            this.colLabourName1.OptionsColumn.AllowEdit = false;
            this.colLabourName1.OptionsColumn.AllowFocus = false;
            this.colLabourName1.OptionsColumn.ReadOnly = true;
            this.colLabourName1.Visible = true;
            this.colLabourName1.VisibleIndex = 0;
            this.colLabourName1.Width = 190;
            // 
            // colLinkedToPersonType
            // 
            this.colLinkedToPersonType.Caption = "Labour Type";
            this.colLinkedToPersonType.FieldName = "LinkedToPersonType";
            this.colLinkedToPersonType.Name = "colLinkedToPersonType";
            this.colLinkedToPersonType.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonType.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonType.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonType.Visible = true;
            this.colLinkedToPersonType.VisibleIndex = 5;
            this.colLinkedToPersonType.Width = 79;
            // 
            // colLinkedToPersonTypeID
            // 
            this.colLinkedToPersonTypeID.Caption = "Labour Type ID";
            this.colLinkedToPersonTypeID.FieldName = "LinkedToPersonTypeID";
            this.colLinkedToPersonTypeID.Name = "colLinkedToPersonTypeID";
            this.colLinkedToPersonTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonTypeID.Width = 93;
            // 
            // colDoNotInvoiceClient1
            // 
            this.colDoNotInvoiceClient1.Caption = "Do Not Invoice Client";
            this.colDoNotInvoiceClient1.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDoNotInvoiceClient1.FieldName = "DoNotInvoiceClient";
            this.colDoNotInvoiceClient1.Name = "colDoNotInvoiceClient1";
            this.colDoNotInvoiceClient1.OptionsColumn.AllowEdit = false;
            this.colDoNotInvoiceClient1.OptionsColumn.AllowFocus = false;
            this.colDoNotInvoiceClient1.OptionsColumn.ReadOnly = true;
            this.colDoNotInvoiceClient1.Visible = true;
            this.colDoNotInvoiceClient1.VisibleIndex = 61;
            this.colDoNotInvoiceClient1.Width = 120;
            // 
            // colDoNotPayContractor1
            // 
            this.colDoNotPayContractor1.Caption = "Do Not Pay Team";
            this.colDoNotPayContractor1.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDoNotPayContractor1.FieldName = "DoNotPayContractor";
            this.colDoNotPayContractor1.Name = "colDoNotPayContractor1";
            this.colDoNotPayContractor1.OptionsColumn.AllowEdit = false;
            this.colDoNotPayContractor1.OptionsColumn.AllowFocus = false;
            this.colDoNotPayContractor1.OptionsColumn.ReadOnly = true;
            this.colDoNotPayContractor1.Visible = true;
            this.colDoNotPayContractor1.VisibleIndex = 60;
            this.colDoNotPayContractor1.Width = 102;
            // 
            // colSelfBillingAmount
            // 
            this.colSelfBillingAmount.Caption = "Self-Billing Amount";
            this.colSelfBillingAmount.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSelfBillingAmount.FieldName = "SelfBillingAmount";
            this.colSelfBillingAmount.Name = "colSelfBillingAmount";
            this.colSelfBillingAmount.OptionsColumn.AllowEdit = false;
            this.colSelfBillingAmount.OptionsColumn.AllowFocus = false;
            this.colSelfBillingAmount.OptionsColumn.ReadOnly = true;
            this.colSelfBillingAmount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SelfBillingAmount", "{0:c}")});
            this.colSelfBillingAmount.Visible = true;
            this.colSelfBillingAmount.VisibleIndex = 28;
            this.colSelfBillingAmount.Width = 107;
            // 
            // colSelfBillingComment
            // 
            this.colSelfBillingComment.Caption = "Self-Billing Comment";
            this.colSelfBillingComment.FieldName = "SelfBillingComment";
            this.colSelfBillingComment.Name = "colSelfBillingComment";
            this.colSelfBillingComment.OptionsColumn.AllowEdit = false;
            this.colSelfBillingComment.OptionsColumn.AllowFocus = false;
            this.colSelfBillingComment.OptionsColumn.ReadOnly = true;
            this.colSelfBillingComment.Visible = true;
            this.colSelfBillingComment.VisibleIndex = 16;
            this.colSelfBillingComment.Width = 136;
            // 
            // colVisitLabourCalculatedID
            // 
            this.colVisitLabourCalculatedID.Caption = "Visit Labour Calculated ID";
            this.colVisitLabourCalculatedID.FieldName = "VisitLabourCalculatedID";
            this.colVisitLabourCalculatedID.Name = "colVisitLabourCalculatedID";
            this.colVisitLabourCalculatedID.OptionsColumn.AllowEdit = false;
            this.colVisitLabourCalculatedID.OptionsColumn.AllowFocus = false;
            this.colVisitLabourCalculatedID.OptionsColumn.ReadOnly = true;
            this.colVisitLabourCalculatedID.Width = 141;
            // 
            // colUniqueID1
            // 
            this.colUniqueID1.Caption = "Unique ID";
            this.colUniqueID1.FieldName = "UniqueID";
            this.colUniqueID1.Name = "colUniqueID1";
            this.colUniqueID1.OptionsColumn.AllowEdit = false;
            this.colUniqueID1.OptionsColumn.AllowFocus = false;
            this.colUniqueID1.OptionsColumn.ReadOnly = true;
            // 
            // colCompanyName1
            // 
            this.colCompanyName1.Caption = "Company Name";
            this.colCompanyName1.FieldName = "CompanyName";
            this.colCompanyName1.Name = "colCompanyName1";
            this.colCompanyName1.OptionsColumn.AllowEdit = false;
            this.colCompanyName1.OptionsColumn.AllowFocus = false;
            this.colCompanyName1.OptionsColumn.ReadOnly = true;
            this.colCompanyName1.Visible = true;
            this.colCompanyName1.VisibleIndex = 0;
            this.colCompanyName1.Width = 150;
            // 
            // colInvoicePDF
            // 
            this.colInvoicePDF.Caption = "Self-Billing Invoice PDF";
            this.colInvoicePDF.ColumnEdit = this.repositoryItemHyperLinkEditSelfBillingInvoice;
            this.colInvoicePDF.FieldName = "InvoicePDF";
            this.colInvoicePDF.Name = "colInvoicePDF";
            this.colInvoicePDF.OptionsColumn.ReadOnly = true;
            this.colInvoicePDF.Visible = true;
            this.colInvoicePDF.VisibleIndex = 10;
            this.colInvoicePDF.Width = 267;
            // 
            // repositoryItemHyperLinkEditSelfBillingInvoice
            // 
            this.repositoryItemHyperLinkEditSelfBillingInvoice.AutoHeight = false;
            this.repositoryItemHyperLinkEditSelfBillingInvoice.Name = "repositoryItemHyperLinkEditSelfBillingInvoice";
            this.repositoryItemHyperLinkEditSelfBillingInvoice.SingleClick = true;
            this.repositoryItemHyperLinkEditSelfBillingInvoice.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditSelfBillingInvoice_OpenLink);
            // 
            // repositoryItemPictureEdit2
            // 
            this.repositoryItemPictureEdit2.Name = "repositoryItemPictureEdit2";
            this.repositoryItemPictureEdit2.ReadOnly = true;
            this.repositoryItemPictureEdit2.ShowMenu = false;
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl1.Manager = this.barManager1;
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(1215, 42);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // xtraTabPageHistoricalDataExtracts
            // 
            this.xtraTabPageHistoricalDataExtracts.Controls.Add(this.standaloneBarDockControl2);
            this.xtraTabPageHistoricalDataExtracts.Controls.Add(this.splitContainerControl1);
            this.xtraTabPageHistoricalDataExtracts.Name = "xtraTabPageHistoricalDataExtracts";
            this.xtraTabPageHistoricalDataExtracts.Size = new System.Drawing.Size(1215, 571);
            this.xtraTabPageHistoricalDataExtracts.Text = "Historical Data Extracts";
            // 
            // standaloneBarDockControl2
            // 
            this.standaloneBarDockControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl2.CausesValidation = false;
            this.standaloneBarDockControl2.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl2.Manager = this.barManager1;
            this.standaloneBarDockControl2.Name = "standaloneBarDockControl2";
            this.standaloneBarDockControl2.Size = new System.Drawing.Size(1215, 42);
            this.standaloneBarDockControl2.Text = "standaloneBarDockControl2";
            // 
            // dockManager1
            // 
            this.dockManager1.AutoHideContainers.AddRange(new DevExpress.XtraBars.Docking.AutoHideContainer[] {
            this.hideContainerLeft});
            this.dockManager1.Form = this;
            this.dockManager1.MenuManager = this.barManager1;
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane"});
            // 
            // hideContainerLeft
            // 
            this.hideContainerLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.hideContainerLeft.Controls.Add(this.dockPanelFilters);
            this.hideContainerLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.hideContainerLeft.Location = new System.Drawing.Point(0, 0);
            this.hideContainerLeft.Name = "hideContainerLeft";
            this.hideContainerLeft.Size = new System.Drawing.Size(23, 597);
            // 
            // dockPanelFilters
            // 
            this.dockPanelFilters.Controls.Add(this.dockPanel1_Container);
            this.dockPanelFilters.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelFilters.ID = new System.Guid("891f675c-6df6-457a-9d87-45006bd66ebf");
            this.dockPanelFilters.Location = new System.Drawing.Point(0, 0);
            this.dockPanelFilters.Name = "dockPanelFilters";
            this.dockPanelFilters.Options.ShowCloseButton = false;
            this.dockPanelFilters.Options.ShowMaximizeButton = false;
            this.dockPanelFilters.OriginalSize = new System.Drawing.Size(312, 200);
            this.dockPanelFilters.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelFilters.SavedIndex = 0;
            this.dockPanelFilters.Size = new System.Drawing.Size(312, 597);
            this.dockPanelFilters.Text = "Data Filter";
            this.dockPanelFilters.Visibility = DevExpress.XtraBars.Docking.DockVisibility.AutoHide;
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.layoutControl1);
            this.dockPanel1_Container.Location = new System.Drawing.Point(3, 29);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(306, 565);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.popupContainerEditInvoiceNumber);
            this.layoutControl1.Controls.Add(this.checkEditSelfEmployed);
            this.layoutControl1.Controls.Add(this.btnLoadHistorical);
            this.layoutControl1.Controls.Add(this.popupContainerEditDateRange);
            this.layoutControl1.Controls.Add(this.popupContainerEditCompanyFilter);
            this.layoutControl1.Controls.Add(this.checkEditStaff);
            this.layoutControl1.Controls.Add(this.checkEditTeams);
            this.layoutControl1.Controls.Add(this.btnLoad);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1067, 233, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(306, 565);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // popupContainerEditInvoiceNumber
            // 
            this.popupContainerEditInvoiceNumber.EditValue = "No Invoice Number Filter";
            this.popupContainerEditInvoiceNumber.Location = new System.Drawing.Point(83, 110);
            this.popupContainerEditInvoiceNumber.MenuManager = this.barManager1;
            this.popupContainerEditInvoiceNumber.Name = "popupContainerEditInvoiceNumber";
            this.popupContainerEditInvoiceNumber.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Clear Filter", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.popupContainerEditInvoiceNumber.Properties.PopupControl = this.popupContainerControlInvoiceNumber;
            this.popupContainerEditInvoiceNumber.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditInvoiceNumber.Size = new System.Drawing.Size(204, 20);
            this.popupContainerEditInvoiceNumber.StyleController = this.layoutControl1;
            this.popupContainerEditInvoiceNumber.TabIndex = 12;
            this.popupContainerEditInvoiceNumber.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditInvoiceNumber_QueryResultValue);
            this.popupContainerEditInvoiceNumber.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.popupContainerEditInvoiceNumber_ButtonClick);
            // 
            // checkEditSelfEmployed
            // 
            this.checkEditSelfEmployed.EditValue = 1;
            this.checkEditSelfEmployed.Location = new System.Drawing.Point(243, 64);
            this.checkEditSelfEmployed.MenuManager = this.barManager1;
            this.checkEditSelfEmployed.Name = "checkEditSelfEmployed";
            this.checkEditSelfEmployed.Properties.Caption = "";
            this.checkEditSelfEmployed.Properties.ValueChecked = 1;
            this.checkEditSelfEmployed.Properties.ValueUnchecked = 0;
            this.checkEditSelfEmployed.Size = new System.Drawing.Size(44, 19);
            this.checkEditSelfEmployed.StyleController = this.layoutControl1;
            this.checkEditSelfEmployed.TabIndex = 19;
            // 
            // btnLoadHistorical
            // 
            this.btnLoadHistorical.ImageOptions.ImageIndex = 5;
            this.btnLoadHistorical.ImageOptions.ImageList = this.imageCollection1;
            this.btnLoadHistorical.Location = new System.Drawing.Point(208, 64);
            this.btnLoadHistorical.Name = "btnLoadHistorical";
            this.btnLoadHistorical.Size = new System.Drawing.Size(79, 22);
            this.btnLoadHistorical.StyleController = this.layoutControl1;
            toolTipTitleItem6.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Appearance.Options.UseImage = true;
            toolTipTitleItem6.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Text = "Load Data - Information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Click me to load Team Historical Data.\r\n\r\nOnly data matching the specified Filter" +
    " Criteria (From and To Dates) will be loaded.";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this.btnLoadHistorical.SuperTip = superToolTip6;
            this.btnLoadHistorical.TabIndex = 18;
            this.btnLoadHistorical.Text = "Load Data";
            this.btnLoadHistorical.Click += new System.EventHandler(this.btnLoadHistorical_Click);
            // 
            // popupContainerEditDateRange
            // 
            this.popupContainerEditDateRange.EditValue = "No Date Range Filter";
            this.popupContainerEditDateRange.Location = new System.Drawing.Point(83, 40);
            this.popupContainerEditDateRange.MenuManager = this.barManager1;
            this.popupContainerEditDateRange.Name = "popupContainerEditDateRange";
            this.popupContainerEditDateRange.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditDateRange.Properties.PopupControl = this.popupContainerControlDateRange;
            this.popupContainerEditDateRange.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditDateRange.Size = new System.Drawing.Size(204, 20);
            this.popupContainerEditDateRange.StyleController = this.layoutControl1;
            this.popupContainerEditDateRange.TabIndex = 6;
            this.popupContainerEditDateRange.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditDateRange_QueryResultValue);
            // 
            // popupContainerEditCompanyFilter
            // 
            this.popupContainerEditCompanyFilter.EditValue = "No Company Filter";
            this.popupContainerEditCompanyFilter.Location = new System.Drawing.Point(83, 40);
            this.popupContainerEditCompanyFilter.MenuManager = this.barManager1;
            this.popupContainerEditCompanyFilter.Name = "popupContainerEditCompanyFilter";
            this.popupContainerEditCompanyFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditCompanyFilter.Properties.PopupControl = this.popupContainerControlCompanyFilter;
            this.popupContainerEditCompanyFilter.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditCompanyFilter.Size = new System.Drawing.Size(204, 20);
            this.popupContainerEditCompanyFilter.StyleController = this.layoutControl1;
            this.popupContainerEditCompanyFilter.TabIndex = 7;
            this.popupContainerEditCompanyFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditCompanyFilter_QueryResultValue);
            // 
            // checkEditStaff
            // 
            this.checkEditStaff.EditValue = 1;
            this.checkEditStaff.Location = new System.Drawing.Point(83, 87);
            this.checkEditStaff.MenuManager = this.barManager1;
            this.checkEditStaff.Name = "checkEditStaff";
            this.checkEditStaff.Properties.Caption = "[Tick if Yes]";
            this.checkEditStaff.Properties.ValueChecked = 1;
            this.checkEditStaff.Properties.ValueUnchecked = 0;
            this.checkEditStaff.Size = new System.Drawing.Size(204, 19);
            this.checkEditStaff.StyleController = this.layoutControl1;
            this.checkEditStaff.TabIndex = 19;
            // 
            // checkEditTeams
            // 
            this.checkEditTeams.EditValue = 1;
            this.checkEditTeams.Location = new System.Drawing.Point(83, 64);
            this.checkEditTeams.MenuManager = this.barManager1;
            this.checkEditTeams.Name = "checkEditTeams";
            this.checkEditTeams.Properties.Caption = "[Tick if Yes]";
            this.checkEditTeams.Properties.ValueChecked = 1;
            this.checkEditTeams.Properties.ValueUnchecked = 0;
            this.checkEditTeams.Size = new System.Drawing.Size(83, 19);
            this.checkEditTeams.StyleController = this.layoutControl1;
            this.checkEditTeams.TabIndex = 18;
            this.checkEditTeams.CheckedChanged += new System.EventHandler(this.checkEditTeams_CheckedChanged);
            // 
            // btnLoad
            // 
            this.btnLoad.ImageOptions.ImageIndex = 5;
            this.btnLoad.ImageOptions.ImageList = this.imageCollection1;
            this.btnLoad.Location = new System.Drawing.Point(208, 134);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(79, 22);
            this.btnLoad.StyleController = this.layoutControl1;
            toolTipTitleItem7.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem7.Appearance.Options.UseImage = true;
            toolTipTitleItem7.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem7.Text = "Load Data - Information";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Click me to Load Callout Client Invoice Data\r\n\r\nOnly data matching the specified " +
    "Filter Criteria (Date, Client and Site) will be loaded.";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            this.btnLoad.SuperTip = superToolTip7;
            this.btnLoad.TabIndex = 17;
            this.btnLoad.Text = "Load Data";
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.tabbedControlGroupFilters,
            this.emptySpaceItem14});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Size = new System.Drawing.Size(306, 565);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 165);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(296, 353);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // tabbedControlGroupFilters
            // 
            this.tabbedControlGroupFilters.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroupFilters.Name = "tabbedControlGroupFilters";
            this.tabbedControlGroupFilters.SelectedTabPage = this.layoutControlGroupDataExport;
            this.tabbedControlGroupFilters.SelectedTabPageIndex = 0;
            this.tabbedControlGroupFilters.Size = new System.Drawing.Size(296, 165);
            this.tabbedControlGroupFilters.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupDataExport,
            this.layoutControlGroupHistoricalExports});
            // 
            // layoutControlGroupDataExport
            // 
            this.layoutControlGroupDataExport.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.emptySpaceItem2,
            this.layoutControlItem2,
            this.layoutControlItem7,
            this.layoutControlItem8});
            this.layoutControlGroupDataExport.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupDataExport.Name = "layoutControlGroupDataExport";
            this.layoutControlGroupDataExport.Size = new System.Drawing.Size(272, 120);
            this.layoutControlGroupDataExport.Text = "Data Export";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.popupContainerEditCompanyFilter;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(272, 24);
            this.layoutControlItem1.Text = "Company:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(61, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.checkEditTeams;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(151, 23);
            this.layoutControlItem3.Text = "Teams:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(61, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.checkEditStaff;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 47);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(272, 23);
            this.layoutControlItem4.Text = "Staff:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(61, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 94);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(189, 26);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.btnLoad;
            this.layoutControlItem2.Location = new System.Drawing.Point(189, 94);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(83, 26);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(83, 26);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(83, 26);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.checkEditSelfEmployed;
            this.layoutControlItem7.Location = new System.Drawing.Point(151, 24);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(121, 23);
            this.layoutControlItem7.Text = "Self-Employed";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(68, 13);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.popupContainerEditInvoiceNumber;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 70);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(272, 24);
            this.layoutControlItem8.Text = "Invoice #:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(61, 13);
            // 
            // layoutControlGroupHistoricalExports
            // 
            this.layoutControlGroupHistoricalExports.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.emptySpaceItem3,
            this.emptySpaceItem4});
            this.layoutControlGroupHistoricalExports.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupHistoricalExports.Name = "layoutControlGroupHistoricalExports";
            this.layoutControlGroupHistoricalExports.Size = new System.Drawing.Size(272, 120);
            this.layoutControlGroupHistoricalExports.Text = "Historical Exports";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.popupContainerEditDateRange;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(272, 24);
            this.layoutControlItem5.Text = "Date Range:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(61, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.btnLoadHistorical;
            this.layoutControlItem6.Location = new System.Drawing.Point(189, 24);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(83, 26);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(83, 26);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(83, 26);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            this.layoutControlItem6.Click += new System.EventHandler(this.btnLoadHistorical_Click);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 24);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(189, 26);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 50);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(272, 70);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.CustomizationFormText = "emptySpaceItem14";
            this.emptySpaceItem14.Location = new System.Drawing.Point(0, 518);
            this.emptySpaceItem14.Name = "emptySpaceItem14";
            this.emptySpaceItem14.Size = new System.Drawing.Size(296, 37);
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(0, 0);
            // 
            // imageCollectionTooltips
            // 
            this.imageCollectionTooltips.ImageSize = new System.Drawing.Size(32, 32);
            this.imageCollectionTooltips.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollectionTooltips.ImageStream")));
            this.imageCollectionTooltips.InsertGalleryImage("info_32x32.png", "images/support/info_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/support/info_32x32.png"), 0);
            this.imageCollectionTooltips.Images.SetKeyName(0, "info_32x32.png");
            // 
            // sp01044_Core_GC_Company_ListTableAdapter
            // 
            this.sp01044_Core_GC_Company_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp06468_OM_Finance_Self_Records_Clients_To_BillTableAdapter
            // 
            this.sp06468_OM_Finance_Self_Records_Clients_To_BillTableAdapter.ClearBeforeFill = true;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.FloatLocation = new System.Drawing.Point(550, 190);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefresh),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiFilterSelected),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiSelectedCount0, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiExport, true)});
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.DisableCustomization = true;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar1.Text = "Custom 2";
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiRefresh.Caption = "Refresh";
            this.bbiRefresh.Id = 34;
            this.bbiRefresh.ImageOptions.ImageIndex = 1;
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // bsiFilterSelected
            // 
            this.bsiFilterSelected.Caption = "Filter Selected";
            this.bsiFilterSelected.Id = 36;
            this.bsiFilterSelected.ImageOptions.ImageIndex = 0;
            this.bsiFilterSelected.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bciFilterInvoicesSelected),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciFilterVisitsSelected)});
            this.bsiFilterSelected.Name = "bsiFilterSelected";
            this.bsiFilterSelected.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionInMenu;
            // 
            // bciFilterInvoicesSelected
            // 
            this.bciFilterInvoicesSelected.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bciFilterInvoicesSelected.Caption = "Filter Selected <b>Invoices</b>";
            this.bciFilterInvoicesSelected.Id = 39;
            this.bciFilterInvoicesSelected.ImageOptions.ImageIndex = 0;
            this.bciFilterInvoicesSelected.Name = "bciFilterInvoicesSelected";
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Filter Selected Invoices - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = resources.GetString("toolTipItem1.Text");
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bciFilterInvoicesSelected.SuperTip = superToolTip1;
            this.bciFilterInvoicesSelected.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciFilterInvoicesSelected_CheckedChanged);
            // 
            // bciFilterVisitsSelected
            // 
            this.bciFilterVisitsSelected.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bciFilterVisitsSelected.Caption = "Filter Selected <b>Visits</b>";
            this.bciFilterVisitsSelected.Id = 40;
            this.bciFilterVisitsSelected.ImageOptions.ImageIndex = 0;
            this.bciFilterVisitsSelected.Name = "bciFilterVisitsSelected";
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Filter Selected Visits - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = resources.GetString("toolTipItem2.Text");
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bciFilterVisitsSelected.SuperTip = superToolTip2;
            this.bciFilterVisitsSelected.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciFilterVisitsSelected_CheckedChanged);
            // 
            // bsiSelectedCount0
            // 
            this.bsiSelectedCount0.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bsiSelectedCount0.Caption = "0 Selected Invoices";
            this.bsiSelectedCount0.Id = 35;
            this.bsiSelectedCount0.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_16x16;
            this.bsiSelectedCount0.ItemAppearance.Disabled.Options.UseTextOptions = true;
            this.bsiSelectedCount0.ItemAppearance.Disabled.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bsiSelectedCount0.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.bsiSelectedCount0.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bsiSelectedCount0.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.bsiSelectedCount0.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bsiSelectedCount0.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.bsiSelectedCount0.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bsiSelectedCount0.Name = "bsiSelectedCount0";
            this.bsiSelectedCount0.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bsiSelectedCount0.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // bbiExport
            // 
            this.bbiExport.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiExport.Caption = "Export";
            this.bbiExport.Enabled = false;
            this.bbiExport.Id = 41;
            this.bbiExport.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiExport.ImageOptions.Image")));
            this.bbiExport.Name = "bbiExport";
            this.bbiExport.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Export To Spreadsheet - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = resources.GetString("toolTipItem3.Text");
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiExport.SuperTip = superToolTip3;
            this.bbiExport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiExport_ItemClick);
            // 
            // sp06469_OM_Finance_Self_Billing_Manager_VisitsTableAdapter
            // 
            this.sp06469_OM_Finance_Self_Billing_Manager_VisitsTableAdapter.ClearBeforeFill = true;
            // 
            // imageCollection2
            // 
            this.imageCollection2.ImageSize = new System.Drawing.Size(32, 32);
            this.imageCollection2.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection2.ImageStream")));
            this.imageCollection2.InsertImage(global::WoodPlan5.Properties.Resources.Filter_32x32, "Filter_32x32", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection2.Images.SetKeyName(0, "Filter_32x32");
            this.imageCollection2.InsertImage(global::WoodPlan5.Properties.Resources.refresh_32x32, "refresh_32x32", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection2.Images.SetKeyName(1, "refresh_32x32");
            this.imageCollection2.InsertImage(global::WoodPlan5.Properties.Resources.Cost_Calculate_32x32, "Cost_Calculate_32x32", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection2.Images.SetKeyName(2, "Cost_Calculate_32x32");
            // 
            // sp06472_OM_Finance_Self_Billing_Historical_ExportsTableAdapter
            // 
            this.sp06472_OM_Finance_Self_Billing_Historical_ExportsTableAdapter.ClearBeforeFill = true;
            // 
            // bar2
            // 
            this.bar2.BarName = "Custom 3";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar2.FloatLocation = new System.Drawing.Point(544, 196);
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefresh2),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiFilterSelected2),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiSelectedCount1, true)});
            this.bar2.OptionsBar.DisableClose = true;
            this.bar2.OptionsBar.DisableCustomization = true;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.StandaloneBarDockControl = this.standaloneBarDockControl2;
            this.bar2.Text = "Custom 3";
            // 
            // bbiRefresh2
            // 
            this.bbiRefresh2.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiRefresh2.Caption = "Refresh";
            this.bbiRefresh2.Id = 42;
            this.bbiRefresh2.ImageOptions.ImageIndex = 1;
            this.bbiRefresh2.Name = "bbiRefresh2";
            this.bbiRefresh2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefresh2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh2_ItemClick);
            // 
            // bsiFilterSelected2
            // 
            this.bsiFilterSelected2.Caption = "Filter Selected";
            this.bsiFilterSelected2.Id = 44;
            this.bsiFilterSelected2.ImageOptions.ImageIndex = 0;
            this.bsiFilterSelected2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bciFilterExportsSelected),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciFilterVisitsSelected2)});
            this.bsiFilterSelected2.Name = "bsiFilterSelected2";
            this.bsiFilterSelected2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionInMenu;
            // 
            // bciFilterExportsSelected
            // 
            this.bciFilterExportsSelected.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bciFilterExportsSelected.Caption = "Filter Selected <b>Exports</b>";
            this.bciFilterExportsSelected.Id = 45;
            this.bciFilterExportsSelected.ImageOptions.ImageIndex = 0;
            this.bciFilterExportsSelected.Name = "bciFilterExportsSelected";
            toolTipTitleItem4.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Text = "Filter Selected Exports - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = resources.GetString("toolTipItem4.Text");
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.bciFilterExportsSelected.SuperTip = superToolTip4;
            this.bciFilterExportsSelected.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciFilterExportsSelected_CheckedChanged);
            // 
            // bciFilterVisitsSelected2
            // 
            this.bciFilterVisitsSelected2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bciFilterVisitsSelected2.Caption = "Filter Selectecd <b>Visits</b>";
            this.bciFilterVisitsSelected2.Id = 46;
            this.bciFilterVisitsSelected2.ImageOptions.ImageIndex = 0;
            this.bciFilterVisitsSelected2.Name = "bciFilterVisitsSelected2";
            toolTipTitleItem5.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Text = "Filter Selected Visits - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = resources.GetString("toolTipItem5.Text");
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.bciFilterVisitsSelected2.SuperTip = superToolTip5;
            this.bciFilterVisitsSelected2.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciFilterVisitsSelected2_CheckedChanged);
            // 
            // bsiSelectedCount1
            // 
            this.bsiSelectedCount1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bsiSelectedCount1.Caption = "0 Selected Historical Extracts";
            this.bsiSelectedCount1.Id = 43;
            this.bsiSelectedCount1.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_16x16;
            this.bsiSelectedCount1.ItemAppearance.Disabled.Options.UseTextOptions = true;
            this.bsiSelectedCount1.ItemAppearance.Disabled.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bsiSelectedCount1.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.bsiSelectedCount1.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bsiSelectedCount1.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.bsiSelectedCount1.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bsiSelectedCount1.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.bsiSelectedCount1.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bsiSelectedCount1.Name = "bsiSelectedCount1";
            this.bsiSelectedCount1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bsiSelectedCount1.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // sp06473_OM_Finance_Self_Billing_Manager_Visits_On_ExportTableAdapter
            // 
            this.sp06473_OM_Finance_Self_Billing_Manager_Visits_On_ExportTableAdapter.ClearBeforeFill = true;
            // 
            // sp06477_OM_Get_Unique_Self_Billing_Invoice_ListTableAdapter
            // 
            this.sp06477_OM_Get_Unique_Self_Billing_Invoice_ListTableAdapter.ClearBeforeFill = true;
            // 
            // frm_OM_Team_Self_Billing_Finance_Manager
            // 
            this.ClientSize = new System.Drawing.Size(1243, 597);
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.hideContainerLeft);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_OM_Team_Self_Billing_Finance_Manager";
            this.Text = "Finance Self-Billing Manager";
            this.Activated += new System.EventHandler(this.frm_OM_Team_Self_Billing_Finance_Manager_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_OM_Team_Self_Billing_Finance_Manager_FormClosing);
            this.Load += new System.EventHandler(this.frm_OM_Team_Self_Billing_Finance_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.hideContainerLeft, 0);
            this.Controls.SetChildIndex(this.xtraTabControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlHistoricalExports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06472OMFinanceSelfBillingHistoricalExportsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Billing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewHistoricalExports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditExportFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Job)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditShowActiveEmployeesOnly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExportVisits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06473OMFinanceSelfBillingManagerVisitsOnExportBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExportVisits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditClientSignature2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditClientInvoiceID2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditSelfBillingInvoicePDF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06469OMFinanceSelfBillingManagerVisitsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06219OMWasteDisposalCentresAcceptedWasteTypesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlInvoices)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06468OMFinanceSelfRecordsClientsToBillBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewInvoices)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMoney)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Client_PO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPageTeamSelfBillingInvoices.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlInvoiceNumber)).EndInit();
            this.popupContainerControlInvoiceNumber.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06477OMGetUniqueSelfBillingInvoiceListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).EndInit();
            this.popupContainerControlDateRange.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlCompanyFilter)).EndInit();
            this.popupContainerControlCompanyFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCompanyFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01044CoreGCCompanyListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Common_Functionality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCompanyFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlVisit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewVisit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditClientSignature)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditClientInvoiceID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditSelfBillingInvoice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit2)).EndInit();
            this.xtraTabPageHistoricalDataExtracts.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.hideContainerLeft.ResumeLayout(false);
            this.dockPanelFilters.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditInvoiceNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSelfEmployed.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditDateRange.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditCompanyFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditStaff.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditTeams.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroupFilters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupDataExport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupHistoricalExports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollectionTooltips)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControlHistoricalExports;
        private DataSet_AT dataSet_AT;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewHistoricalExports;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit2;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditShowActiveEmployeesOnly;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DataSet_OM_Job dataSet_OM_Job;
        private System.Windows.Forms.BindingSource sp06219OMWasteDisposalCentresAcceptedWasteTypesBindingSource;
        private DataSet_OM_JobTableAdapters.sp06219_OM_Waste_Disposal_Centres_Accepted_Waste_TypesTableAdapter sp06219_OM_Waste_Disposal_Centres_Accepted_Waste_TypesTableAdapter;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageHistoricalDataExtracts;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageTeamSelfBillingInvoices;
        private DevExpress.XtraGrid.GridControl gridControlInvoices;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewInvoices;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelFilters;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.SimpleButton btnLoad;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DataSet_OM_Client_PO dataSet_OM_Client_PO;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditMoney;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDate;
        private DevExpress.Utils.ImageCollection imageCollectionTooltips;
        private DevExpress.XtraEditors.CheckEdit checkEditStaff;
        private DevExpress.XtraEditors.CheckEdit checkEditTeams;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditCompanyFilter;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlCompanyFilter;
        private DevExpress.XtraGrid.GridControl gridControlCompanyFilter;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewCompanyFilter;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit21;
        private DevExpress.XtraEditors.SimpleButton btnCompanyFilterOK;
        private System.Windows.Forms.BindingSource sp01044CoreGCCompanyListBindingSource;
        private DataSet_Common_Functionality dataSet_Common_Functionality;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder;
        private DataSet_Common_FunctionalityTableAdapters.sp01044_Core_GC_Company_ListTableAdapter sp01044_Core_GC_Company_ListTableAdapter;
        private System.Windows.Forms.BindingSource sp06468OMFinanceSelfRecordsClientsToBillBindingSource;
        private DataSet_OM_Billing dataSet_OM_Billing;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeCode;
        private DevExpress.XtraGrid.Columns.GridColumn colInvoiceDate;
        private DevExpress.XtraGrid.Columns.GridColumn colInvoiceID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobCode;
        private DevExpress.XtraGrid.Columns.GridColumn colAnalysisCode;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCentre;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartment;
        private DevExpress.XtraGrid.Columns.GridColumn colPaid;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colClient;
        private DevExpress.XtraGrid.Columns.GridColumn colDirectEmployed;
        private DevExpress.XtraGrid.Columns.GridColumn colVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colPeriod;
        private DevExpress.XtraGrid.Columns.GridColumn colYear;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyName;
        private DataSet_OM_BillingTableAdapters.sp06468_OM_Finance_Self_Records_Clients_To_BillTableAdapter sp06468_OM_Finance_Self_Records_Clients_To_BillTableAdapter;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraBars.BarStaticItem bsiSelectedCount0;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraGrid.GridControl gridControlVisit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewVisit;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitNumber;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditInteger2;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedStartDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCalculationLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitCost;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitSell;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colStartLatitude;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colStartLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colFinishLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colFinishLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCalculationLevelDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractValue;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDirectorID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractActive;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractValue;
        private DevExpress.XtraGrid.Columns.GridColumn colFinanceClientCode;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colContractType;
        private DevExpress.XtraGrid.Columns.GridColumn colContractStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDirector;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToParent1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHTML0;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colSitePostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLocationX;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLocationY;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedJobCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditGrid1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedOutstandingJobCount;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedCompletedJobCount;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddress;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeliness;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitCategoryID;
        private DevExpress.XtraGrid.Columns.GridColumn colIssueFound;
        private DevExpress.XtraGrid.Columns.GridColumn colIssueType;
        private DevExpress.XtraGrid.Columns.GridColumn colIssueTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colIssueRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colClientSignaturePath;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditClientSignature;
        private DevExpress.XtraGrid.Columns.GridColumn colImagesFolderOM;
        private DevExpress.XtraGrid.Columns.GridColumn colRework;
        private DevExpress.XtraGrid.Columns.GridColumn colExtraWorkComments;
        private DevExpress.XtraGrid.Columns.GridColumn colExtraWorkRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colExtraWorkType;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentOnSite;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentComment;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colSuspendedJobCount;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedPictureCount;
        private DevExpress.XtraGrid.Columns.GridColumn colFriendlyVisitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitType;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPO;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPOID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCode;
        private DevExpress.XtraGrid.Columns.GridColumn colSellCalculationLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colSellCalculationLevelDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colSelected4;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditSelfBillingInvoice;
        private DevExpress.XtraGrid.Columns.GridColumn colClientInvoiceID1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditClientInvoiceID;
        private DevExpress.XtraGrid.Columns.GridColumn colCMName;
        private DevExpress.XtraGrid.Columns.GridColumn colDaysAllowed;
        private DevExpress.XtraGrid.Columns.GridColumn colManagerName;
        private DevExpress.XtraGrid.Columns.GridColumn colManagerNotes;
        private DevExpress.XtraGrid.Columns.GridColumn colNoOnetoSign;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colExternalCustomerStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colCompletionSheetNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusIssueID;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusIssue;
        private DevExpress.XtraGrid.Columns.GridColumn colDaysSeparation;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractDaysSeparationPercent;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingRequirmentsFulfilled;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingRequirmentsUnfulfilled;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingWarningAuthorised;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingWarningUnauthorised;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourName1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonType;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotInvoiceClient1;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotPayContractor1;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingComment;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoiceHeaderID;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitLabourCalculatedID;
        private DevExpress.XtraGrid.Columns.GridColumn colSelected;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoiceHeaderID1;
        private DevExpress.XtraGrid.Columns.GridColumn colUniqueID;
        private System.Windows.Forms.BindingSource sp06469OMFinanceSelfBillingManagerVisitsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colUniqueID1;
        private DataSet_OM_BillingTableAdapters.sp06469_OM_Finance_Self_Billing_Manager_VisitsTableAdapter sp06469_OM_Finance_Self_Billing_Manager_VisitsTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyName1;
        private DevExpress.XtraGrid.Columns.GridColumn colInvoicePDF;
        private DevExpress.XtraBars.BarSubItem bsiFilterSelected;
        private DevExpress.Utils.ImageCollection imageCollection2;
        private DevExpress.XtraBars.BarCheckItem bciFilterInvoicesSelected;
        private DevExpress.XtraBars.BarCheckItem bciFilterVisitsSelected;
        private DevExpress.XtraBars.BarButtonItem bbiExport;
        private DevExpress.XtraGrid.Columns.GridColumn colOMSelfBillingFinanceExportNamePrefix;
        private System.Windows.Forms.BindingSource sp06472OMFinanceSelfBillingHistoricalExportsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingFinanceExportID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateExported;
        private DevExpress.XtraGrid.Columns.GridColumn colExportedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colExportFile;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditExportFile;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyID;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamsIncluded;
        private DevExpress.XtraGrid.Columns.GridColumn colStaffIncluded;
        private DevExpress.XtraGrid.Columns.GridColumn colExportedByStaff;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyName2;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedVisits;
        private DataSet_OM_BillingTableAdapters.sp06472_OM_Finance_Self_Billing_Historical_ExportsTableAdapter sp06472_OM_Finance_Self_Billing_Historical_ExportsTableAdapter;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh2;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroupFilters;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupDataExport;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupHistoricalExports;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditDateRange;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
        private DevExpress.XtraEditors.SimpleButton btnLoadHistorical;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlDateRange;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraEditors.SimpleButton btnDateRangeOK;
        private DevExpress.XtraBars.BarStaticItem bsiSelectedCount1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime3;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID1;
        private DevExpress.XtraGrid.Columns.GridColumn colGCCompanyID;
        private DevExpress.XtraGrid.GridControl gridControlExportVisits;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewExportVisits;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn37;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn39;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn40;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn41;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn42;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn43;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn44;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn45;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn46;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn47;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn48;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn49;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn50;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn51;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn52;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn53;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn54;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditClientSignature2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn55;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn56;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn57;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn58;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn59;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn60;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn61;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn62;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn63;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn64;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn65;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn66;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn67;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn68;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn69;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn70;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn71;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn72;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn73;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditClientInvoiceID2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn74;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn75;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn76;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn77;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn78;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn79;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn80;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn81;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn82;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn83;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn84;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn85;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn86;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn87;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn88;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn89;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn90;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn91;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn92;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn93;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn94;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn95;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn96;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn97;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn98;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn99;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn100;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn101;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditSelfBillingInvoicePDF;
        private System.Windows.Forms.BindingSource sp06473OMFinanceSelfBillingManagerVisitsOnExportBindingSource;
        private DataSet_OM_BillingTableAdapters.sp06473_OM_Finance_Self_Billing_Manager_Visits_On_ExportTableAdapter sp06473_OM_Finance_Self_Billing_Manager_Visits_On_ExportTableAdapter;
        private DevExpress.XtraBars.BarSubItem bsiFilterSelected2;
        private DevExpress.XtraBars.BarCheckItem bciFilterExportsSelected;
        private DevExpress.XtraBars.BarCheckItem bciFilterVisitsSelected2;
        private DevExpress.XtraGrid.Columns.GridColumn colSelected1;
        private DevExpress.XtraEditors.CheckEdit checkEditSelfEmployed;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditInvoiceNumber;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlInvoiceNumber;
        private DevExpress.XtraEditors.SimpleButton btnInvoiceNumber;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoiceNumber1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit8;
        private System.Windows.Forms.BindingSource sp06477OMGetUniqueSelfBillingInvoiceListBindingSource;
        private DataSet_OM_BillingTableAdapters.sp06477_OM_Get_Unique_Self_Billing_Invoice_ListTableAdapter sp06477_OM_Get_Unique_Self_Billing_Invoice_ListTableAdapter;
        private DevExpress.XtraBars.Docking.AutoHideContainer hideContainerLeft;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoiceNumber;
    }
}

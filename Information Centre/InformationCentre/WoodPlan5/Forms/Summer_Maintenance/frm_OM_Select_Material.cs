using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Linq;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_OM_Select_Material : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        GridHitInfo downHitInfo = null;

        public string _Mode = "single";  // single or multiple //
        public int intOriginalID = 0;
        public string strOriginalIDs = "";
        public int intSelectedID = 0;
        public string strSelectedIDs = "";
        public string strSelectedDescription1 = "";
        public int intSelectedDescription2 = 0;
        public string strSelectedDescription3 = "";
        public int _SelectedCount = 0;
        public decimal decSelectedCostExVat = (decimal)0.00;
        public decimal decSelectedCostVatRate = (decimal)0.00;
        public decimal decSelectedSellExVat = (decimal)0.00;
        public decimal decSelectedSellVatRate = (decimal)0.00;
        BaseObjects.GridCheckMarksSelection selection1;
       
        private Dictionary<string, RepositoryItemTextEdit> dictionary;

        #endregion

        public frm_OM_Select_Material()
        {
            InitializeComponent();
        }

        private void frm_OM_Select_Material_Load(object sender, EventArgs e)
        {
            this.LockThisWindow();
            this.FormID = 500140;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            dictionary = new Dictionary<String, RepositoryItemTextEdit>();

            sp06109_OM_Material_SelectTableAdapter.Connection.ConnectionString = strConnectionString;
            GridView view = (GridView)gridControl1.MainView;

            LoadData();
            gridControl1.ForceInitialize();

            if (_Mode != "single")
            {
                // Add record selection checkboxes to popup grid control //
                selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
                selection1.CheckMarkColumn.Width = 30;
                selection1.CheckMarkColumn.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
                selection1.CheckMarkColumn.VisibleIndex = 0;

                Array arrayRecords = strOriginalIDs.Split(',');  // Single quotes because char expected for delimeter //
                view.BeginUpdate();
                int intFoundRow = 0;
                foreach (string strElement in arrayRecords)
                {
                    if (strElement == "") break;
                    intFoundRow = view.LocateByValue(0, view.Columns["MaterialID"], Convert.ToInt32(strElement));
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                        view.MakeRowVisible(intFoundRow, false);
                    }
                }
                view.EndUpdate();
            }
            else  // Single selection mode //
            {
                if (intOriginalID != 0)  // Record selected so try to find and highlight //
                {
                    int intFoundRow = view.LocateByValue(0, view.Columns["MaterialID"], intOriginalID);
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.FocusedRowHandle = intFoundRow;
                        view.MakeRowVisible(intFoundRow, false);
                    }
                }
            }
            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
        }

        public override void PostLoadView(object objParameter)
        {
        }

        private void LoadData()
        {
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            sp06109_OM_Material_SelectTableAdapter.Fill(dataSet_OM_Contract.sp06109_OM_Material_Select, "");            
            view.EndUpdate();
        }


        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Materials Available");
        }

        bool internalRowFocusing;
        private void gridView1_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void gridView1_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void gridView1_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            if (e.ListSourceRowIndex < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "StartingUnits":
                case "UnitsInStock":
                case "ReorderLevel":
                case "WarningLevel":
                    {
                        string strMask = (string.IsNullOrWhiteSpace(view.GetRowCellValue(e.ListSourceRowIndex, "UnitDescriptor").ToString()) ? "" : " " + view.GetRowCellValue(e.ListSourceRowIndex, "UnitDescriptor"));
                        if (!string.IsNullOrWhiteSpace(strMask))
                        {
                            e.DisplayText = string.Format("{0:N2}" + strMask, e.Value);
                        }
                        break;
                    }
            }
        }

        private void gridView1_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "UnitsInStock")
            {
                if (Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "UnitsInStock")) < Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "ReorderLevel")))
                {
                    e.Appearance.BackColor = Color.LightCoral;
                    e.Appearance.BackColor2 = Color.Red;
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                    return;
                }
                else if (Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "UnitsInStock")) < Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "WarningLevel")))
                {
                    e.Appearance.BackColor = Color.Khaki;
                    e.Appearance.BackColor2 = Color.DarkOrange;
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                    return;
                }
                else
                {
                    //e.Appearance.BackColor = Color.Transparent;
                    //e.Appearance.BackColor2 = Color.Transparent;
                    //e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                    return;
                }
            }
        }

        private void gridView1_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            /*
            if (e.RowHandle < 0) return; 
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "StartingUnits":
                case "UnitsInStock":
                case "ReorderLevel":
                case "WarningLevel":
                    string strMask = (string.IsNullOrWhiteSpace(view.GetRowCellValue(e.RowHandle, "UnitDescriptor").ToString()) ? "" : " " + view.GetRowCellValue(e.RowHandle, "UnitDescriptor"));
                    string strItemName = "ri_" + strMask.Replace(" ", "");
                    var ri = FindRepositoryItem(strItemName);
                    if (ri == null)
                    {
                        ri = new RepositoryItemTextEdit();
                        ri.Mask.EditMask = "#######0.00" + strMask;
                        ri.Name = strItemName;
                        e.RepositoryItem = ri;
                    
                        dictionary.Add(strItemName, ri);
                    }
                    else
                    {
                        e.RepositoryItem = ri;
                    }
                    break;
                default:
                    break;
            }*/
        }
        private RepositoryItemTextEdit FindRepositoryItem(string NameToFind)
        {
            if (!dictionary.ContainsKey(NameToFind))
            {
                return null;
            }
            else
            {
                return dictionary[NameToFind];
            }
        }
     
 
        #endregion


        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (_Mode != "single")
            {
                if (string.IsNullOrEmpty(strSelectedIDs))
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records by ticking them before proceeding.", "Select Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            else  // Single selection mode //
            {
                if (intSelectedID == 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Select a record before proceeding.", "Select Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }


        private void GetSelectedDetails()
        {
            GridView view = (GridView)gridControl1.MainView;
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //  
            if (_Mode != "single")
            {
                strSelectedIDs = "";    // Reset any prior values first //
                strSelectedDescription1 = "";  // Reset any prior values first //
                intSelectedDescription2 = 0;  // Reset any prior values first //
                strSelectedDescription3 = "";  // Reset any prior values first //
                string strDescriptor1 = "";
                int intDescriptor2 = 0;
                string strDescriptor3 = "";
                if (selection1.SelectedCount <= 0) return;

                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        strSelectedIDs += Convert.ToString(view.GetRowCellValue(i, "MaterialID")) + ",";
                        strDescriptor1 = Convert.ToString(view.GetRowCellValue(i, "MaterialDescription"));
                        intDescriptor2 = Convert.ToInt32(view.GetRowCellValue(i, "UnitDescriptorID"));
                        strDescriptor3 = Convert.ToString(view.GetRowCellValue(i, "UnitDescriptor"));
                        
                        if (intCount == 0)
                        {
                            strSelectedDescription1 = strDescriptor1;
                            intSelectedDescription2 = intDescriptor2;
                            strSelectedDescription3 = strDescriptor3;
                        }
                        else if (intCount >= 1)
                        {
                            strSelectedDescription1 += ", " + strDescriptor1;
                            intSelectedDescription2 = intDescriptor2;
                            strSelectedDescription3 += ", " + strDescriptor3;
                        }
                        intCount++;
                    }
                }
                _SelectedCount = intCount;
            }
            else  // Single record selection //
            {
                if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
                {
                    var currentRowView = (DataRowView)sp06109OMMaterialSelectBindingSource.Current;
                    var currentRow = (DataSet_OM_Contract.sp06109_OM_Material_SelectRow)currentRowView.Row;
                    if (currentRow == null) return;
                    intSelectedID = (string.IsNullOrEmpty(currentRow.MaterialID.ToString()) ? 0 : currentRow.MaterialID);
                    strSelectedDescription1 = (string.IsNullOrEmpty(currentRow.MaterialDescription.ToString()) ? "" : currentRow.MaterialDescription);
                    intSelectedDescription2 = (string.IsNullOrEmpty(currentRow.UnitDescriptorID.ToString()) ? 0 : currentRow.UnitDescriptorID);
                    strSelectedDescription3 = (string.IsNullOrEmpty(currentRow.UnitDescriptor.ToString()) ? "" : currentRow.UnitDescriptor);
                    decSelectedCostExVat = (string.IsNullOrEmpty(currentRow.CostPerUnitExVat.ToString()) ? (decimal)0.00 : currentRow.CostPerUnitExVat);
                    decSelectedCostVatRate = (string.IsNullOrEmpty(currentRow.CostPerUnitVatRate.ToString()) ? (decimal)0.00 : currentRow.CostPerUnitVatRate);
                    decSelectedSellExVat = (string.IsNullOrEmpty(currentRow.SellPerUnitExVat.ToString()) ? (decimal)0.00 : currentRow.SellPerUnitExVat);
                    decSelectedSellVatRate = (string.IsNullOrEmpty(currentRow.SellPerUnitVatRate.ToString()) ? (decimal)0.00 : currentRow.SellPerUnitVatRate);
               }
            }
        }




    }
}


namespace WoodPlan5
{
    partial class frm_OM_Select_Tender
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Select_Tender));
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            this.colActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp06508OMSelectExtraWorksClientContractBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Tender = new WoodPlan5.DataSet_OM_Tender();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDatetime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSectorTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGCCompanyName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDirector = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSectorType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReactive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sp07194UTPermissionDocumentSelectBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_WorkOrder = new WoodPlan5.DataSet_UT_WorkOrder();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerControlDateRange = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnDateRangeFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.beiDateFilter = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.sp07194_UT_Permission_Document_SelectTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07194_UT_Permission_Document_SelectTableAdapter();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp06497OMTendersForClientContractBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTenderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientReferenceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTenderDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colCreatedByStaffID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContactID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKAM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTenderGroupID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuoteNotRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRequestReceivedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colReturnToClientByDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCMInitialAttendanceDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuoteSubmittedToClientDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuoteAcceptedByClientDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRequiredWorkCompletedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKAMQuoteRejectionReasonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOurInternalComments = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCMComments = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientComments = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTPORequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlanningAuthorityID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTPOCheckedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeProtected = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlanningAuthorityNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlanningAuthoritySubmittedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlanningAuthorityOutcomeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlanningAuthorityOkToProceed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGCCompanyName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSectorType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddress1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToParent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSitePostcode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteLocationX1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteLocationY1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedVisitCount1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTenderGroupDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKAMName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCMName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTenderStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactPersonName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactPersonPosition = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactPersonTitle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlanningAuthorityOutcome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlanningAuthority = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKAMQuoteRejectionReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRevisionNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusIsssue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusIssueID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubmittedToCMDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProposedLabourID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProposedLabour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProposedLabourTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProposedLabourType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTenderType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTenderTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEditPercentage = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.sp06497_OM_Tenders_For_Client_ContractTableAdapter = new WoodPlan5.DataSet_OM_TenderTableAdapters.sp06497_OM_Tenders_For_Client_ContractTableAdapter();
            this.sp06508_OM_Select_Extra_Works_Client_ContractTableAdapter = new WoodPlan5.DataSet_OM_TenderTableAdapters.sp06508_OM_Select_Extra_Works_Client_ContractTableAdapter();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bbiRefreshChildData = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06508OMSelectExtraWorksClientContractBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Tender)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDatetime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07194UTPermissionDocumentSelectBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_WorkOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).BeginInit();
            this.popupContainerControlDateRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06497OMTendersForClientContractBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(843, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 665);
            this.barDockControlBottom.Size = new System.Drawing.Size(843, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 639);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(843, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 639);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar2});
            this.barManager1.DockControls.Add(this.standaloneBarDockControl1);
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.beiDateFilter,
            this.bbiRefresh,
            this.bbiRefreshChildData});
            this.barManager1.MaxItemId = 33;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEdit1});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colActive
            // 
            this.colActive.Caption = "Active";
            this.colActive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colActive.FieldName = "Active";
            this.colActive.Name = "colActive";
            this.colActive.OptionsColumn.AllowEdit = false;
            this.colActive.OptionsColumn.AllowFocus = false;
            this.colActive.OptionsColumn.ReadOnly = true;
            this.colActive.Visible = true;
            this.colActive.VisibleIndex = 4;
            this.colActive.Width = 51;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp06508OMSelectExtraWorksClientContractBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemTextEditDatetime,
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEditCurrency});
            this.gridControl1.Size = new System.Drawing.Size(818, 295);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp06508OMSelectExtraWorksClientContractBindingSource
            // 
            this.sp06508OMSelectExtraWorksClientContractBindingSource.DataMember = "sp06508_OM_Select_Extra_Works_Client_Contract";
            this.sp06508OMSelectExtraWorksClientContractBindingSource.DataSource = this.dataSet_OM_Tender;
            // 
            // dataSet_OM_Tender
            // 
            this.dataSet_OM_Tender.DataSetName = "DataSet_OM_Tender";
            this.dataSet_OM_Tender.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientContractID,
            this.colClientID,
            this.colStartDate,
            this.colEndDate,
            this.colActive,
            this.colSectorTypeID,
            this.colContractValue,
            this.colRemarks,
            this.colClientName,
            this.colGCCompanyName,
            this.colContractType,
            this.colContractStatus,
            this.colContractDirector,
            this.colSectorType,
            this.colContractDescription,
            this.colReactive});
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.colActive;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Gray;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView1.FormatRules.Add(gridFormatRule1);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsFind.FindDelay = 2000;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colStartDate, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.GridView_FocusedRowChanged_NoGroupSelection);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_NoGroupSelection);
            // 
            // colClientContractID
            // 
            this.colClientContractID.Caption = "Client Contract ID";
            this.colClientContractID.FieldName = "ClientContractID";
            this.colClientContractID.Name = "colClientContractID";
            this.colClientContractID.OptionsColumn.AllowEdit = false;
            this.colClientContractID.OptionsColumn.AllowFocus = false;
            this.colClientContractID.OptionsColumn.ReadOnly = true;
            this.colClientContractID.Width = 107;
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // colStartDate
            // 
            this.colStartDate.Caption = "Start Date";
            this.colStartDate.ColumnEdit = this.repositoryItemTextEditDatetime;
            this.colStartDate.FieldName = "StartDate";
            this.colStartDate.Name = "colStartDate";
            this.colStartDate.OptionsColumn.AllowEdit = false;
            this.colStartDate.OptionsColumn.AllowFocus = false;
            this.colStartDate.OptionsColumn.ReadOnly = true;
            this.colStartDate.Visible = true;
            this.colStartDate.VisibleIndex = 2;
            this.colStartDate.Width = 100;
            // 
            // repositoryItemTextEditDatetime
            // 
            this.repositoryItemTextEditDatetime.AutoHeight = false;
            this.repositoryItemTextEditDatetime.Mask.EditMask = "g";
            this.repositoryItemTextEditDatetime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDatetime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDatetime.Name = "repositoryItemTextEditDatetime";
            // 
            // colEndDate
            // 
            this.colEndDate.Caption = "End Date";
            this.colEndDate.ColumnEdit = this.repositoryItemTextEditDatetime;
            this.colEndDate.FieldName = "EndDate";
            this.colEndDate.Name = "colEndDate";
            this.colEndDate.OptionsColumn.AllowEdit = false;
            this.colEndDate.OptionsColumn.AllowFocus = false;
            this.colEndDate.OptionsColumn.ReadOnly = true;
            this.colEndDate.Visible = true;
            this.colEndDate.VisibleIndex = 3;
            this.colEndDate.Width = 100;
            // 
            // colSectorTypeID
            // 
            this.colSectorTypeID.Caption = "Sector Type ID";
            this.colSectorTypeID.FieldName = "SectorTypeID";
            this.colSectorTypeID.Name = "colSectorTypeID";
            this.colSectorTypeID.OptionsColumn.AllowEdit = false;
            this.colSectorTypeID.OptionsColumn.AllowFocus = false;
            this.colSectorTypeID.OptionsColumn.ReadOnly = true;
            this.colSectorTypeID.Width = 93;
            // 
            // colContractValue
            // 
            this.colContractValue.Caption = "Contract Value";
            this.colContractValue.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colContractValue.FieldName = "ContractValue";
            this.colContractValue.Name = "colContractValue";
            this.colContractValue.OptionsColumn.AllowEdit = false;
            this.colContractValue.OptionsColumn.AllowFocus = false;
            this.colContractValue.OptionsColumn.ReadOnly = true;
            this.colContractValue.Visible = true;
            this.colContractValue.VisibleIndex = 11;
            this.colContractValue.Width = 92;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 12;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 0;
            this.colClientName.Width = 181;
            // 
            // colGCCompanyName
            // 
            this.colGCCompanyName.Caption = "GC Company Name";
            this.colGCCompanyName.FieldName = "GCCompanyName";
            this.colGCCompanyName.Name = "colGCCompanyName";
            this.colGCCompanyName.OptionsColumn.AllowEdit = false;
            this.colGCCompanyName.OptionsColumn.AllowFocus = false;
            this.colGCCompanyName.OptionsColumn.ReadOnly = true;
            this.colGCCompanyName.Visible = true;
            this.colGCCompanyName.VisibleIndex = 6;
            this.colGCCompanyName.Width = 113;
            // 
            // colContractType
            // 
            this.colContractType.Caption = "Contract Type";
            this.colContractType.FieldName = "ContractType";
            this.colContractType.Name = "colContractType";
            this.colContractType.OptionsColumn.AllowEdit = false;
            this.colContractType.OptionsColumn.AllowFocus = false;
            this.colContractType.OptionsColumn.ReadOnly = true;
            this.colContractType.Visible = true;
            this.colContractType.VisibleIndex = 7;
            this.colContractType.Width = 105;
            // 
            // colContractStatus
            // 
            this.colContractStatus.Caption = "Contract Status";
            this.colContractStatus.FieldName = "ContractStatus";
            this.colContractStatus.Name = "colContractStatus";
            this.colContractStatus.OptionsColumn.AllowEdit = false;
            this.colContractStatus.OptionsColumn.AllowFocus = false;
            this.colContractStatus.OptionsColumn.ReadOnly = true;
            this.colContractStatus.Visible = true;
            this.colContractStatus.VisibleIndex = 9;
            this.colContractStatus.Width = 118;
            // 
            // colContractDirector
            // 
            this.colContractDirector.Caption = "Contract Director";
            this.colContractDirector.FieldName = "ContractDirector";
            this.colContractDirector.Name = "colContractDirector";
            this.colContractDirector.OptionsColumn.AllowEdit = false;
            this.colContractDirector.OptionsColumn.AllowFocus = false;
            this.colContractDirector.OptionsColumn.ReadOnly = true;
            this.colContractDirector.Visible = true;
            this.colContractDirector.VisibleIndex = 10;
            this.colContractDirector.Width = 117;
            // 
            // colSectorType
            // 
            this.colSectorType.Caption = "Sector Type";
            this.colSectorType.FieldName = "SectorType";
            this.colSectorType.Name = "colSectorType";
            this.colSectorType.OptionsColumn.AllowEdit = false;
            this.colSectorType.OptionsColumn.AllowFocus = false;
            this.colSectorType.OptionsColumn.ReadOnly = true;
            this.colSectorType.Visible = true;
            this.colSectorType.VisibleIndex = 8;
            this.colSectorType.Width = 79;
            // 
            // colContractDescription
            // 
            this.colContractDescription.Caption = "Contract Description";
            this.colContractDescription.FieldName = "ContractDescription";
            this.colContractDescription.Name = "colContractDescription";
            this.colContractDescription.OptionsColumn.AllowEdit = false;
            this.colContractDescription.OptionsColumn.AllowFocus = false;
            this.colContractDescription.OptionsColumn.ReadOnly = true;
            this.colContractDescription.Visible = true;
            this.colContractDescription.VisibleIndex = 1;
            this.colContractDescription.Width = 285;
            // 
            // colReactive
            // 
            this.colReactive.Caption = "Reactive";
            this.colReactive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colReactive.FieldName = "Reactive";
            this.colReactive.Name = "colReactive";
            this.colReactive.OptionsColumn.AllowEdit = false;
            this.colReactive.OptionsColumn.AllowFocus = false;
            this.colReactive.OptionsColumn.ReadOnly = true;
            this.colReactive.Visible = true;
            this.colReactive.VisibleIndex = 5;
            this.colReactive.Width = 63;
            // 
            // sp07194UTPermissionDocumentSelectBindingSource
            // 
            this.sp07194UTPermissionDocumentSelectBindingSource.DataMember = "sp07194_UT_Permission_Document_Select";
            this.sp07194UTPermissionDocumentSelectBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // dataSet_UT_WorkOrder
            // 
            this.dataSet_UT_WorkOrder.DataSetName = "DataSet_UT_WorkOrder";
            this.dataSet_UT_WorkOrder.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(675, 637);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(756, 637);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // popupContainerControlDateRange
            // 
            this.popupContainerControlDateRange.Controls.Add(this.btnDateRangeFilterOK);
            this.popupContainerControlDateRange.Controls.Add(this.groupControl1);
            this.popupContainerControlDateRange.Location = new System.Drawing.Point(13, 112);
            this.popupContainerControlDateRange.Name = "popupContainerControlDateRange";
            this.popupContainerControlDateRange.Size = new System.Drawing.Size(180, 109);
            this.popupContainerControlDateRange.TabIndex = 5;
            // 
            // btnDateRangeFilterOK
            // 
            this.btnDateRangeFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDateRangeFilterOK.Location = new System.Drawing.Point(3, 83);
            this.btnDateRangeFilterOK.Name = "btnDateRangeFilterOK";
            this.btnDateRangeFilterOK.Size = new System.Drawing.Size(34, 23);
            this.btnDateRangeFilterOK.TabIndex = 3;
            this.btnDateRangeFilterOK.Text = "OK";
            this.btnDateRangeFilterOK.Click += new System.EventHandler(this.btnDateRangeFilterOK_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.dateEditToDate);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.dateEditFromDate);
            this.groupControl1.Location = new System.Drawing.Point(3, 3);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(174, 76);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "Date Range";
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(40, 50);
            this.dateEditToDate.MenuManager = this.barManager1;
            this.dateEditToDate.Name = "dateEditToDate";
            this.dateEditToDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Size = new System.Drawing.Size(129, 20);
            this.dateEditToDate.TabIndex = 3;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(6, 53);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(16, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "To:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(6, 27);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(28, 13);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "From:";
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(40, 24);
            this.dateEditFromDate.MenuManager = this.barManager1;
            this.dateEditFromDate.Name = "dateEditFromDate";
            this.dateEditFromDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Size = new System.Drawing.Size(129, 20);
            this.dateEditFromDate.TabIndex = 0;
            // 
            // beiDateFilter
            // 
            this.beiDateFilter.Caption = "No Date Filter:";
            this.beiDateFilter.Description = "Date Filter:";
            this.beiDateFilter.Edit = this.repositoryItemPopupContainerEdit1;
            this.beiDateFilter.EditValue = "No Date Filter";
            this.beiDateFilter.EditWidth = 168;
            this.beiDateFilter.Id = 30;
            this.beiDateFilter.Name = "beiDateFilter";
            this.beiDateFilter.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // repositoryItemPopupContainerEdit1
            // 
            this.repositoryItemPopupContainerEdit1.AutoHeight = false;
            this.repositoryItemPopupContainerEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit1.Name = "repositoryItemPopupContainerEdit1";
            this.repositoryItemPopupContainerEdit1.PopupControl = this.popupContainerControlDateRange;
            this.repositoryItemPopupContainerEdit1.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditDateRange_QueryResultValue);
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Caption = "Refresh Data";
            this.bbiRefresh.Id = 31;
            this.bbiRefresh.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRefresh.ImageOptions.Image")));
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 1;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.beiDateFilter),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefresh)});
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.Text = "Filter";
            // 
            // sp07194_UT_Permission_Document_SelectTableAdapter
            // 
            this.sp07194_UT_Permission_Document_SelectTableAdapter.ClearBeforeFill = true;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 26);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl1.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControlDateRange);
            this.splitContainerControl1.Panel1.Controls.Add(this.gridControl1);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Client Contracts";
            this.splitContainerControl1.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl1.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel2.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl1.Panel2.Controls.Add(this.standaloneBarDockControl1);
            this.splitContainerControl1.Panel2.Controls.Add(this.gridControl2);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "Linked Tenders";
            this.splitContainerControl1.Size = new System.Drawing.Size(843, 605);
            this.splitContainerControl1.SplitterPosition = 298;
            this.splitContainerControl1.TabIndex = 7;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, -2);
            this.standaloneBarDockControl1.Manager = this.barManager1;
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(820, 26);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // gridControl2
            // 
            this.gridControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl2.DataSource = this.sp06497OMTendersForClientContractBindingSource;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.Location = new System.Drawing.Point(0, 24);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2,
            this.repositoryItemTextEditDateTime2,
            this.repositoryItemCheckEdit2,
            this.repositoryItemTextEditCurrency2,
            this.repositoryItemTextEditPercentage});
            this.gridControl2.Size = new System.Drawing.Size(818, 273);
            this.gridControl2.TabIndex = 5;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp06497OMTendersForClientContractBindingSource
            // 
            this.sp06497OMTendersForClientContractBindingSource.DataMember = "sp06497_OM_Tenders_For_Client_Contract";
            this.sp06497OMTendersForClientContractBindingSource.DataSource = this.dataSet_OM_Tender;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTenderID,
            this.colClientContractID1,
            this.colSiteContractID1,
            this.colClientReferenceNumber,
            this.colStatusID1,
            this.colTenderDescription,
            this.colCreatedByStaffID1,
            this.colClientContactID1,
            this.colKAM,
            this.colCM,
            this.colWorkTypeID,
            this.colTenderGroupID,
            this.gridColumn1,
            this.colQuoteNotRequired,
            this.colRequestReceivedDate,
            this.colReturnToClientByDate,
            this.colCMInitialAttendanceDate,
            this.colQuoteSubmittedToClientDate,
            this.colQuoteAcceptedByClientDate,
            this.colRequiredWorkCompletedDate,
            this.colClientPONumber,
            this.colKAMQuoteRejectionReasonID,
            this.colOurInternalComments,
            this.colCMComments,
            this.colClientComments,
            this.colTPORequired,
            this.colPlanningAuthorityID,
            this.colTPOCheckedDate,
            this.colTreeProtected,
            this.colPlanningAuthorityNumber,
            this.colPlanningAuthoritySubmittedDate,
            this.colPlanningAuthorityOutcomeID,
            this.colPlanningAuthorityOkToProceed,
            this.colClientName1,
            this.colGCCompanyName1,
            this.colSectorType1,
            this.colSiteName1,
            this.colSiteAddress1,
            this.colLinkedToParent,
            this.colContractDescription1,
            this.colSitePostcode1,
            this.colSiteLocationX1,
            this.colSiteLocationY1,
            this.colLinkedVisitCount1,
            this.colTenderGroupDescription,
            this.colCreatedByStaffName1,
            this.colKAMName1,
            this.colCMName1,
            this.colTenderStatus,
            this.colContactPersonName,
            this.colContactPersonPosition,
            this.colContactPersonTitle,
            this.colPlanningAuthorityOutcome,
            this.colPlanningAuthority,
            this.colJobSubType,
            this.colJobType,
            this.colKAMQuoteRejectionReason,
            this.colWorkType,
            this.colSiteCode1,
            this.colSiteID1,
            this.colClientID1,
            this.colRevisionNumber,
            this.colStatusIsssue,
            this.colStatusIssueID,
            this.colSubmittedToCMDate,
            this.colProposedLabourID,
            this.colProposedLabour,
            this.colProposedLabourTypeID,
            this.colProposedLabourType,
            this.colTenderType,
            this.colTenderTypeID});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupCount = 2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsFind.AlwaysVisible = true;
            this.gridView2.OptionsFind.FindDelay = 2000;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsLayout.StoreFormatRules = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTenderID, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.GridView_FocusedRowChanged_NoGroupSelection);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_NoGroupSelection);
            // 
            // colTenderID
            // 
            this.colTenderID.Caption = "Tender ID";
            this.colTenderID.FieldName = "TenderID";
            this.colTenderID.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colTenderID.Name = "colTenderID";
            this.colTenderID.OptionsColumn.AllowEdit = false;
            this.colTenderID.OptionsColumn.AllowFocus = false;
            this.colTenderID.OptionsColumn.ReadOnly = true;
            this.colTenderID.Visible = true;
            this.colTenderID.VisibleIndex = 0;
            this.colTenderID.Width = 122;
            // 
            // colClientContractID1
            // 
            this.colClientContractID1.Caption = "Client Contract ID";
            this.colClientContractID1.FieldName = "ClientContractID";
            this.colClientContractID1.Name = "colClientContractID1";
            this.colClientContractID1.OptionsColumn.AllowEdit = false;
            this.colClientContractID1.OptionsColumn.AllowFocus = false;
            this.colClientContractID1.OptionsColumn.ReadOnly = true;
            this.colClientContractID1.Width = 105;
            // 
            // colSiteContractID1
            // 
            this.colSiteContractID1.Caption = "Site Contract ID";
            this.colSiteContractID1.FieldName = "SiteContractID";
            this.colSiteContractID1.Name = "colSiteContractID1";
            this.colSiteContractID1.OptionsColumn.AllowEdit = false;
            this.colSiteContractID1.OptionsColumn.AllowFocus = false;
            this.colSiteContractID1.OptionsColumn.ReadOnly = true;
            this.colSiteContractID1.Width = 96;
            // 
            // colClientReferenceNumber
            // 
            this.colClientReferenceNumber.Caption = "Client Reference #";
            this.colClientReferenceNumber.FieldName = "ClientReferenceNumber";
            this.colClientReferenceNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colClientReferenceNumber.Name = "colClientReferenceNumber";
            this.colClientReferenceNumber.OptionsColumn.AllowEdit = false;
            this.colClientReferenceNumber.OptionsColumn.AllowFocus = false;
            this.colClientReferenceNumber.OptionsColumn.ReadOnly = true;
            this.colClientReferenceNumber.Visible = true;
            this.colClientReferenceNumber.VisibleIndex = 1;
            this.colClientReferenceNumber.Width = 110;
            // 
            // colStatusID1
            // 
            this.colStatusID1.Caption = "Status ID";
            this.colStatusID1.FieldName = "StatusID";
            this.colStatusID1.Name = "colStatusID1";
            this.colStatusID1.OptionsColumn.AllowEdit = false;
            this.colStatusID1.OptionsColumn.AllowFocus = false;
            this.colStatusID1.OptionsColumn.ReadOnly = true;
            // 
            // colTenderDescription
            // 
            this.colTenderDescription.Caption = "Tender Description";
            this.colTenderDescription.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colTenderDescription.FieldName = "TenderDescription";
            this.colTenderDescription.Name = "colTenderDescription";
            this.colTenderDescription.OptionsColumn.ReadOnly = true;
            this.colTenderDescription.Visible = true;
            this.colTenderDescription.VisibleIndex = 4;
            this.colTenderDescription.Width = 224;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colCreatedByStaffID1
            // 
            this.colCreatedByStaffID1.Caption = "Created By Staff ID";
            this.colCreatedByStaffID1.FieldName = "CreatedByStaffID";
            this.colCreatedByStaffID1.Name = "colCreatedByStaffID1";
            this.colCreatedByStaffID1.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffID1.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffID1.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffID1.Width = 127;
            // 
            // colClientContactID1
            // 
            this.colClientContactID1.Caption = "Client Contact ID";
            this.colClientContactID1.FieldName = "ClientContactID";
            this.colClientContactID1.Name = "colClientContactID1";
            this.colClientContactID1.OptionsColumn.AllowEdit = false;
            this.colClientContactID1.OptionsColumn.AllowFocus = false;
            this.colClientContactID1.OptionsColumn.ReadOnly = true;
            this.colClientContactID1.Width = 101;
            // 
            // colKAM
            // 
            this.colKAM.Caption = "KAM";
            this.colKAM.FieldName = "KAM";
            this.colKAM.Name = "colKAM";
            this.colKAM.OptionsColumn.AllowEdit = false;
            this.colKAM.OptionsColumn.AllowFocus = false;
            this.colKAM.OptionsColumn.ReadOnly = true;
            this.colKAM.Width = 40;
            // 
            // colCM
            // 
            this.colCM.Caption = "CM";
            this.colCM.FieldName = "CM";
            this.colCM.Name = "colCM";
            this.colCM.OptionsColumn.AllowEdit = false;
            this.colCM.OptionsColumn.AllowFocus = false;
            this.colCM.OptionsColumn.ReadOnly = true;
            this.colCM.Width = 34;
            // 
            // colWorkTypeID
            // 
            this.colWorkTypeID.Caption = "Work Sub-Type ID";
            this.colWorkTypeID.FieldName = "WorkSubTypeID";
            this.colWorkTypeID.Name = "colWorkTypeID";
            this.colWorkTypeID.OptionsColumn.AllowEdit = false;
            this.colWorkTypeID.OptionsColumn.AllowFocus = false;
            this.colWorkTypeID.OptionsColumn.ReadOnly = true;
            this.colWorkTypeID.Width = 85;
            // 
            // colTenderGroupID
            // 
            this.colTenderGroupID.Caption = "Tender Group ID";
            this.colTenderGroupID.FieldName = "TenderGroupID";
            this.colTenderGroupID.Name = "colTenderGroupID";
            this.colTenderGroupID.OptionsColumn.AllowEdit = false;
            this.colTenderGroupID.OptionsColumn.AllowFocus = false;
            this.colTenderGroupID.OptionsColumn.ReadOnly = true;
            this.colTenderGroupID.Width = 99;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Reactive";
            this.gridColumn1.ColumnEdit = this.repositoryItemCheckEdit2;
            this.gridColumn1.FieldName = "Reactive";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 10;
            // 
            // colQuoteNotRequired
            // 
            this.colQuoteNotRequired.Caption = "Quote Not Required";
            this.colQuoteNotRequired.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colQuoteNotRequired.FieldName = "QuoteNotRequired";
            this.colQuoteNotRequired.Name = "colQuoteNotRequired";
            this.colQuoteNotRequired.OptionsColumn.AllowEdit = false;
            this.colQuoteNotRequired.OptionsColumn.AllowFocus = false;
            this.colQuoteNotRequired.OptionsColumn.ReadOnly = true;
            this.colQuoteNotRequired.Visible = true;
            this.colQuoteNotRequired.VisibleIndex = 11;
            this.colQuoteNotRequired.Width = 115;
            // 
            // colRequestReceivedDate
            // 
            this.colRequestReceivedDate.Caption = "Request Received From Client";
            this.colRequestReceivedDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colRequestReceivedDate.FieldName = "RequestReceivedDate";
            this.colRequestReceivedDate.Name = "colRequestReceivedDate";
            this.colRequestReceivedDate.OptionsColumn.AllowEdit = false;
            this.colRequestReceivedDate.OptionsColumn.AllowFocus = false;
            this.colRequestReceivedDate.OptionsColumn.ReadOnly = true;
            this.colRequestReceivedDate.Visible = true;
            this.colRequestReceivedDate.VisibleIndex = 12;
            this.colRequestReceivedDate.Width = 163;
            // 
            // repositoryItemTextEditDateTime2
            // 
            this.repositoryItemTextEditDateTime2.AutoHeight = false;
            this.repositoryItemTextEditDateTime2.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime2.Name = "repositoryItemTextEditDateTime2";
            // 
            // colReturnToClientByDate
            // 
            this.colReturnToClientByDate.Caption = "Return To Client By";
            this.colReturnToClientByDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colReturnToClientByDate.FieldName = "ReturnToClientByDate";
            this.colReturnToClientByDate.Name = "colReturnToClientByDate";
            this.colReturnToClientByDate.OptionsColumn.AllowEdit = false;
            this.colReturnToClientByDate.OptionsColumn.AllowFocus = false;
            this.colReturnToClientByDate.OptionsColumn.ReadOnly = true;
            this.colReturnToClientByDate.Visible = true;
            this.colReturnToClientByDate.VisibleIndex = 13;
            this.colReturnToClientByDate.Width = 112;
            // 
            // colCMInitialAttendanceDate
            // 
            this.colCMInitialAttendanceDate.Caption = "CM Initial Attendance";
            this.colCMInitialAttendanceDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colCMInitialAttendanceDate.FieldName = "CMInitialAttendanceDate";
            this.colCMInitialAttendanceDate.Name = "colCMInitialAttendanceDate";
            this.colCMInitialAttendanceDate.OptionsColumn.AllowEdit = false;
            this.colCMInitialAttendanceDate.OptionsColumn.AllowFocus = false;
            this.colCMInitialAttendanceDate.OptionsColumn.ReadOnly = true;
            this.colCMInitialAttendanceDate.Visible = true;
            this.colCMInitialAttendanceDate.VisibleIndex = 15;
            this.colCMInitialAttendanceDate.Width = 122;
            // 
            // colQuoteSubmittedToClientDate
            // 
            this.colQuoteSubmittedToClientDate.Caption = "Quote Submitted To Client";
            this.colQuoteSubmittedToClientDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colQuoteSubmittedToClientDate.FieldName = "QuoteSubmittedToClientDate";
            this.colQuoteSubmittedToClientDate.Name = "colQuoteSubmittedToClientDate";
            this.colQuoteSubmittedToClientDate.OptionsColumn.AllowEdit = false;
            this.colQuoteSubmittedToClientDate.OptionsColumn.AllowFocus = false;
            this.colQuoteSubmittedToClientDate.OptionsColumn.ReadOnly = true;
            this.colQuoteSubmittedToClientDate.Visible = true;
            this.colQuoteSubmittedToClientDate.VisibleIndex = 17;
            this.colQuoteSubmittedToClientDate.Width = 145;
            // 
            // colQuoteAcceptedByClientDate
            // 
            this.colQuoteAcceptedByClientDate.Caption = "Quote Accepted By Client";
            this.colQuoteAcceptedByClientDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colQuoteAcceptedByClientDate.FieldName = "QuoteAcceptedByClientDate";
            this.colQuoteAcceptedByClientDate.Name = "colQuoteAcceptedByClientDate";
            this.colQuoteAcceptedByClientDate.OptionsColumn.AllowEdit = false;
            this.colQuoteAcceptedByClientDate.OptionsColumn.AllowFocus = false;
            this.colQuoteAcceptedByClientDate.OptionsColumn.ReadOnly = true;
            this.colQuoteAcceptedByClientDate.Visible = true;
            this.colQuoteAcceptedByClientDate.VisibleIndex = 18;
            this.colQuoteAcceptedByClientDate.Width = 142;
            // 
            // colRequiredWorkCompletedDate
            // 
            this.colRequiredWorkCompletedDate.Caption = "Required Work Completed By";
            this.colRequiredWorkCompletedDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colRequiredWorkCompletedDate.FieldName = "RequiredWorkCompletedDate";
            this.colRequiredWorkCompletedDate.Name = "colRequiredWorkCompletedDate";
            this.colRequiredWorkCompletedDate.OptionsColumn.AllowEdit = false;
            this.colRequiredWorkCompletedDate.OptionsColumn.AllowFocus = false;
            this.colRequiredWorkCompletedDate.OptionsColumn.ReadOnly = true;
            this.colRequiredWorkCompletedDate.Visible = true;
            this.colRequiredWorkCompletedDate.VisibleIndex = 19;
            this.colRequiredWorkCompletedDate.Width = 159;
            // 
            // colClientPONumber
            // 
            this.colClientPONumber.Caption = "Client PO #";
            this.colClientPONumber.FieldName = "ClientPONumber";
            this.colClientPONumber.Name = "colClientPONumber";
            this.colClientPONumber.OptionsColumn.AllowEdit = false;
            this.colClientPONumber.OptionsColumn.AllowFocus = false;
            this.colClientPONumber.OptionsColumn.ReadOnly = true;
            this.colClientPONumber.Visible = true;
            this.colClientPONumber.VisibleIndex = 22;
            this.colClientPONumber.Width = 91;
            // 
            // colKAMQuoteRejectionReasonID
            // 
            this.colKAMQuoteRejectionReasonID.Caption = "KAm Quote Rejection Reason ID";
            this.colKAMQuoteRejectionReasonID.FieldName = "KAMQuoteRejectionReasonID";
            this.colKAMQuoteRejectionReasonID.Name = "colKAMQuoteRejectionReasonID";
            this.colKAMQuoteRejectionReasonID.OptionsColumn.AllowEdit = false;
            this.colKAMQuoteRejectionReasonID.OptionsColumn.AllowFocus = false;
            this.colKAMQuoteRejectionReasonID.OptionsColumn.ReadOnly = true;
            this.colKAMQuoteRejectionReasonID.Width = 174;
            // 
            // colOurInternalComments
            // 
            this.colOurInternalComments.Caption = "Our Internal Comments";
            this.colOurInternalComments.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colOurInternalComments.FieldName = "OurInternalComments";
            this.colOurInternalComments.Name = "colOurInternalComments";
            this.colOurInternalComments.OptionsColumn.ReadOnly = true;
            this.colOurInternalComments.Visible = true;
            this.colOurInternalComments.VisibleIndex = 25;
            this.colOurInternalComments.Width = 131;
            // 
            // colCMComments
            // 
            this.colCMComments.Caption = "CM Comments";
            this.colCMComments.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colCMComments.FieldName = "CMComments";
            this.colCMComments.Name = "colCMComments";
            this.colCMComments.OptionsColumn.ReadOnly = true;
            this.colCMComments.Visible = true;
            this.colCMComments.VisibleIndex = 26;
            this.colCMComments.Width = 120;
            // 
            // colClientComments
            // 
            this.colClientComments.Caption = "Client Comments";
            this.colClientComments.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colClientComments.FieldName = "ClientComments";
            this.colClientComments.Name = "colClientComments";
            this.colClientComments.OptionsColumn.ReadOnly = true;
            this.colClientComments.Visible = true;
            this.colClientComments.VisibleIndex = 27;
            this.colClientComments.Width = 122;
            // 
            // colTPORequired
            // 
            this.colTPORequired.Caption = "TPO Required";
            this.colTPORequired.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colTPORequired.FieldName = "TPORequired";
            this.colTPORequired.Name = "colTPORequired";
            this.colTPORequired.OptionsColumn.AllowEdit = false;
            this.colTPORequired.OptionsColumn.AllowFocus = false;
            this.colTPORequired.OptionsColumn.ReadOnly = true;
            this.colTPORequired.Visible = true;
            this.colTPORequired.VisibleIndex = 28;
            this.colTPORequired.Width = 85;
            // 
            // colPlanningAuthorityID
            // 
            this.colPlanningAuthorityID.Caption = "Planning Authority ID";
            this.colPlanningAuthorityID.FieldName = "PlanningAuthorityID";
            this.colPlanningAuthorityID.Name = "colPlanningAuthorityID";
            this.colPlanningAuthorityID.OptionsColumn.AllowEdit = false;
            this.colPlanningAuthorityID.OptionsColumn.AllowFocus = false;
            this.colPlanningAuthorityID.OptionsColumn.ReadOnly = true;
            this.colPlanningAuthorityID.Visible = true;
            this.colPlanningAuthorityID.VisibleIndex = 29;
            this.colPlanningAuthorityID.Width = 121;
            // 
            // colTPOCheckedDate
            // 
            this.colTPOCheckedDate.Caption = "TPO Checked Date";
            this.colTPOCheckedDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colTPOCheckedDate.FieldName = "TPOCheckedDate";
            this.colTPOCheckedDate.Name = "colTPOCheckedDate";
            this.colTPOCheckedDate.OptionsColumn.AllowEdit = false;
            this.colTPOCheckedDate.OptionsColumn.AllowFocus = false;
            this.colTPOCheckedDate.OptionsColumn.ReadOnly = true;
            this.colTPOCheckedDate.Visible = true;
            this.colTPOCheckedDate.VisibleIndex = 30;
            this.colTPOCheckedDate.Width = 109;
            // 
            // colTreeProtected
            // 
            this.colTreeProtected.Caption = "Tree Protected";
            this.colTreeProtected.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colTreeProtected.FieldName = "TreeProtected";
            this.colTreeProtected.Name = "colTreeProtected";
            this.colTreeProtected.OptionsColumn.AllowEdit = false;
            this.colTreeProtected.OptionsColumn.AllowFocus = false;
            this.colTreeProtected.OptionsColumn.ReadOnly = true;
            this.colTreeProtected.Visible = true;
            this.colTreeProtected.VisibleIndex = 31;
            this.colTreeProtected.Width = 91;
            // 
            // colPlanningAuthorityNumber
            // 
            this.colPlanningAuthorityNumber.Caption = "Planning Authority #";
            this.colPlanningAuthorityNumber.FieldName = "PlanningAuthorityNumber";
            this.colPlanningAuthorityNumber.Name = "colPlanningAuthorityNumber";
            this.colPlanningAuthorityNumber.OptionsColumn.AllowEdit = false;
            this.colPlanningAuthorityNumber.OptionsColumn.AllowFocus = false;
            this.colPlanningAuthorityNumber.OptionsColumn.ReadOnly = true;
            this.colPlanningAuthorityNumber.Visible = true;
            this.colPlanningAuthorityNumber.VisibleIndex = 32;
            this.colPlanningAuthorityNumber.Width = 118;
            // 
            // colPlanningAuthoritySubmittedDate
            // 
            this.colPlanningAuthoritySubmittedDate.Caption = "Planning Submitted Date";
            this.colPlanningAuthoritySubmittedDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colPlanningAuthoritySubmittedDate.FieldName = "PlanningAuthoritySubmittedDate";
            this.colPlanningAuthoritySubmittedDate.Name = "colPlanningAuthoritySubmittedDate";
            this.colPlanningAuthoritySubmittedDate.OptionsColumn.AllowEdit = false;
            this.colPlanningAuthoritySubmittedDate.OptionsColumn.AllowFocus = false;
            this.colPlanningAuthoritySubmittedDate.OptionsColumn.ReadOnly = true;
            this.colPlanningAuthoritySubmittedDate.Visible = true;
            this.colPlanningAuthoritySubmittedDate.VisibleIndex = 33;
            this.colPlanningAuthoritySubmittedDate.Width = 136;
            // 
            // colPlanningAuthorityOutcomeID
            // 
            this.colPlanningAuthorityOutcomeID.Caption = "Planning Outcome ID";
            this.colPlanningAuthorityOutcomeID.FieldName = "PlanningAuthorityOutcomeID";
            this.colPlanningAuthorityOutcomeID.Name = "colPlanningAuthorityOutcomeID";
            this.colPlanningAuthorityOutcomeID.OptionsColumn.AllowEdit = false;
            this.colPlanningAuthorityOutcomeID.OptionsColumn.AllowFocus = false;
            this.colPlanningAuthorityOutcomeID.OptionsColumn.ReadOnly = true;
            this.colPlanningAuthorityOutcomeID.Visible = true;
            this.colPlanningAuthorityOutcomeID.VisibleIndex = 34;
            this.colPlanningAuthorityOutcomeID.Width = 119;
            // 
            // colPlanningAuthorityOkToProceed
            // 
            this.colPlanningAuthorityOkToProceed.Caption = "Planning OK To Proceed";
            this.colPlanningAuthorityOkToProceed.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colPlanningAuthorityOkToProceed.FieldName = "PlanningAuthorityOkToProceed";
            this.colPlanningAuthorityOkToProceed.Name = "colPlanningAuthorityOkToProceed";
            this.colPlanningAuthorityOkToProceed.OptionsColumn.AllowEdit = false;
            this.colPlanningAuthorityOkToProceed.OptionsColumn.AllowFocus = false;
            this.colPlanningAuthorityOkToProceed.OptionsColumn.ReadOnly = true;
            this.colPlanningAuthorityOkToProceed.Visible = true;
            this.colPlanningAuthorityOkToProceed.VisibleIndex = 35;
            this.colPlanningAuthorityOkToProceed.Width = 133;
            // 
            // colClientName1
            // 
            this.colClientName1.Caption = "Client Name";
            this.colClientName1.FieldName = "ClientName";
            this.colClientName1.Name = "colClientName1";
            this.colClientName1.OptionsColumn.AllowEdit = false;
            this.colClientName1.OptionsColumn.AllowFocus = false;
            this.colClientName1.OptionsColumn.ReadOnly = true;
            this.colClientName1.Visible = true;
            this.colClientName1.VisibleIndex = 5;
            this.colClientName1.Width = 238;
            // 
            // colGCCompanyName1
            // 
            this.colGCCompanyName1.Caption = "GC Company Name";
            this.colGCCompanyName1.FieldName = "GCCompanyName";
            this.colGCCompanyName1.Name = "colGCCompanyName1";
            this.colGCCompanyName1.OptionsColumn.AllowEdit = false;
            this.colGCCompanyName1.OptionsColumn.AllowFocus = false;
            this.colGCCompanyName1.OptionsColumn.ReadOnly = true;
            this.colGCCompanyName1.Width = 111;
            // 
            // colSectorType1
            // 
            this.colSectorType1.Caption = "Sector Type";
            this.colSectorType1.FieldName = "SectorType";
            this.colSectorType1.Name = "colSectorType1";
            this.colSectorType1.OptionsColumn.AllowEdit = false;
            this.colSectorType1.OptionsColumn.AllowFocus = false;
            this.colSectorType1.OptionsColumn.ReadOnly = true;
            this.colSectorType1.Width = 77;
            // 
            // colSiteName1
            // 
            this.colSiteName1.Caption = "Site Name";
            this.colSiteName1.FieldName = "SiteName";
            this.colSiteName1.Name = "colSiteName1";
            this.colSiteName1.OptionsColumn.AllowEdit = false;
            this.colSiteName1.OptionsColumn.AllowFocus = false;
            this.colSiteName1.OptionsColumn.ReadOnly = true;
            this.colSiteName1.Visible = true;
            this.colSiteName1.VisibleIndex = 4;
            this.colSiteName1.Width = 263;
            // 
            // colSiteAddress1
            // 
            this.colSiteAddress1.Caption = "Site Address";
            this.colSiteAddress1.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colSiteAddress1.FieldName = "SiteAddress";
            this.colSiteAddress1.Name = "colSiteAddress1";
            this.colSiteAddress1.OptionsColumn.ReadOnly = true;
            this.colSiteAddress1.Visible = true;
            this.colSiteAddress1.VisibleIndex = 23;
            this.colSiteAddress1.Width = 208;
            // 
            // colLinkedToParent
            // 
            this.colLinkedToParent.Caption = "Linked To Parent";
            this.colLinkedToParent.FieldName = "LinkedToParent";
            this.colLinkedToParent.Name = "colLinkedToParent";
            this.colLinkedToParent.OptionsColumn.AllowEdit = false;
            this.colLinkedToParent.OptionsColumn.AllowFocus = false;
            this.colLinkedToParent.OptionsColumn.ReadOnly = true;
            this.colLinkedToParent.Width = 276;
            // 
            // colContractDescription1
            // 
            this.colContractDescription1.Caption = "Contract Description";
            this.colContractDescription1.FieldName = "ContractDescription";
            this.colContractDescription1.Name = "colContractDescription1";
            this.colContractDescription1.OptionsColumn.AllowEdit = false;
            this.colContractDescription1.OptionsColumn.AllowFocus = false;
            this.colContractDescription1.OptionsColumn.ReadOnly = true;
            this.colContractDescription1.Width = 155;
            // 
            // colSitePostcode1
            // 
            this.colSitePostcode1.Caption = "Site Postcode";
            this.colSitePostcode1.FieldName = "SitePostcode";
            this.colSitePostcode1.Name = "colSitePostcode1";
            this.colSitePostcode1.OptionsColumn.AllowEdit = false;
            this.colSitePostcode1.OptionsColumn.AllowFocus = false;
            this.colSitePostcode1.OptionsColumn.ReadOnly = true;
            this.colSitePostcode1.Visible = true;
            this.colSitePostcode1.VisibleIndex = 24;
            this.colSitePostcode1.Width = 84;
            // 
            // colSiteLocationX1
            // 
            this.colSiteLocationX1.Caption = "Site Location X";
            this.colSiteLocationX1.FieldName = "SiteLocationX";
            this.colSiteLocationX1.Name = "colSiteLocationX1";
            this.colSiteLocationX1.OptionsColumn.AllowEdit = false;
            this.colSiteLocationX1.OptionsColumn.AllowFocus = false;
            this.colSiteLocationX1.OptionsColumn.ReadOnly = true;
            this.colSiteLocationX1.Width = 89;
            // 
            // colSiteLocationY1
            // 
            this.colSiteLocationY1.Caption = "Site Location Y";
            this.colSiteLocationY1.FieldName = "SiteLocationY";
            this.colSiteLocationY1.Name = "colSiteLocationY1";
            this.colSiteLocationY1.OptionsColumn.AllowEdit = false;
            this.colSiteLocationY1.OptionsColumn.AllowFocus = false;
            this.colSiteLocationY1.OptionsColumn.ReadOnly = true;
            this.colSiteLocationY1.Width = 89;
            // 
            // colLinkedVisitCount1
            // 
            this.colLinkedVisitCount1.Caption = "Linked Visits";
            this.colLinkedVisitCount1.FieldName = "LinkedVisitCount";
            this.colLinkedVisitCount1.Name = "colLinkedVisitCount1";
            this.colLinkedVisitCount1.OptionsColumn.ReadOnly = true;
            this.colLinkedVisitCount1.Visible = true;
            this.colLinkedVisitCount1.VisibleIndex = 38;
            this.colLinkedVisitCount1.Width = 76;
            // 
            // colTenderGroupDescription
            // 
            this.colTenderGroupDescription.Caption = "Tender Group";
            this.colTenderGroupDescription.FieldName = "TenderGroupDescription";
            this.colTenderGroupDescription.Name = "colTenderGroupDescription";
            this.colTenderGroupDescription.OptionsColumn.AllowEdit = false;
            this.colTenderGroupDescription.OptionsColumn.AllowFocus = false;
            this.colTenderGroupDescription.OptionsColumn.ReadOnly = true;
            this.colTenderGroupDescription.Width = 85;
            // 
            // colCreatedByStaffName1
            // 
            this.colCreatedByStaffName1.Caption = "Created By Name";
            this.colCreatedByStaffName1.FieldName = "CreatedByStaffName";
            this.colCreatedByStaffName1.Name = "colCreatedByStaffName1";
            this.colCreatedByStaffName1.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffName1.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffName1.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffName1.Visible = true;
            this.colCreatedByStaffName1.VisibleIndex = 9;
            this.colCreatedByStaffName1.Width = 112;
            // 
            // colKAMName1
            // 
            this.colKAMName1.Caption = "KAM Name";
            this.colKAMName1.FieldName = "KAMName";
            this.colKAMName1.Name = "colKAMName1";
            this.colKAMName1.OptionsColumn.AllowEdit = false;
            this.colKAMName1.OptionsColumn.AllowFocus = false;
            this.colKAMName1.OptionsColumn.ReadOnly = true;
            this.colKAMName1.Visible = true;
            this.colKAMName1.VisibleIndex = 7;
            this.colKAMName1.Width = 116;
            // 
            // colCMName1
            // 
            this.colCMName1.Caption = "CM Name";
            this.colCMName1.FieldName = "CMName";
            this.colCMName1.Name = "colCMName1";
            this.colCMName1.OptionsColumn.AllowEdit = false;
            this.colCMName1.OptionsColumn.AllowFocus = false;
            this.colCMName1.OptionsColumn.ReadOnly = true;
            this.colCMName1.Visible = true;
            this.colCMName1.VisibleIndex = 8;
            this.colCMName1.Width = 113;
            // 
            // colTenderStatus
            // 
            this.colTenderStatus.Caption = "Tender Status";
            this.colTenderStatus.FieldName = "TenderStatus";
            this.colTenderStatus.Name = "colTenderStatus";
            this.colTenderStatus.OptionsColumn.AllowEdit = false;
            this.colTenderStatus.OptionsColumn.AllowFocus = false;
            this.colTenderStatus.OptionsColumn.ReadOnly = true;
            this.colTenderStatus.Visible = true;
            this.colTenderStatus.VisibleIndex = 5;
            this.colTenderStatus.Width = 182;
            // 
            // colContactPersonName
            // 
            this.colContactPersonName.Caption = "Client Contact Person";
            this.colContactPersonName.FieldName = "ContactPersonName";
            this.colContactPersonName.Name = "colContactPersonName";
            this.colContactPersonName.OptionsColumn.AllowEdit = false;
            this.colContactPersonName.OptionsColumn.AllowFocus = false;
            this.colContactPersonName.OptionsColumn.ReadOnly = true;
            this.colContactPersonName.Width = 123;
            // 
            // colContactPersonPosition
            // 
            this.colContactPersonPosition.Caption = "Client Contact Position";
            this.colContactPersonPosition.FieldName = "ContactPersonPosition";
            this.colContactPersonPosition.Name = "colContactPersonPosition";
            this.colContactPersonPosition.OptionsColumn.AllowEdit = false;
            this.colContactPersonPosition.OptionsColumn.AllowFocus = false;
            this.colContactPersonPosition.OptionsColumn.ReadOnly = true;
            this.colContactPersonPosition.Width = 127;
            // 
            // colContactPersonTitle
            // 
            this.colContactPersonTitle.Caption = "Client Contact Title";
            this.colContactPersonTitle.FieldName = "ContactPersonTitle";
            this.colContactPersonTitle.Name = "colContactPersonTitle";
            this.colContactPersonTitle.OptionsColumn.AllowEdit = false;
            this.colContactPersonTitle.OptionsColumn.AllowFocus = false;
            this.colContactPersonTitle.OptionsColumn.ReadOnly = true;
            this.colContactPersonTitle.Width = 110;
            // 
            // colPlanningAuthorityOutcome
            // 
            this.colPlanningAuthorityOutcome.Caption = "Planning Outcome";
            this.colPlanningAuthorityOutcome.FieldName = "PlanningAuthorityOutcome";
            this.colPlanningAuthorityOutcome.Name = "colPlanningAuthorityOutcome";
            this.colPlanningAuthorityOutcome.OptionsColumn.AllowEdit = false;
            this.colPlanningAuthorityOutcome.OptionsColumn.AllowFocus = false;
            this.colPlanningAuthorityOutcome.OptionsColumn.ReadOnly = true;
            this.colPlanningAuthorityOutcome.Width = 105;
            // 
            // colPlanningAuthority
            // 
            this.colPlanningAuthority.Caption = "Planning Authority";
            this.colPlanningAuthority.FieldName = "PlanningAuthority";
            this.colPlanningAuthority.Name = "colPlanningAuthority";
            this.colPlanningAuthority.OptionsColumn.AllowEdit = false;
            this.colPlanningAuthority.OptionsColumn.AllowFocus = false;
            this.colPlanningAuthority.OptionsColumn.ReadOnly = true;
            this.colPlanningAuthority.Width = 107;
            // 
            // colJobSubType
            // 
            this.colJobSubType.Caption = "Work Sub-Type";
            this.colJobSubType.FieldName = "JobSubType";
            this.colJobSubType.Name = "colJobSubType";
            this.colJobSubType.OptionsColumn.AllowEdit = false;
            this.colJobSubType.OptionsColumn.AllowFocus = false;
            this.colJobSubType.OptionsColumn.ReadOnly = true;
            this.colJobSubType.Visible = true;
            this.colJobSubType.VisibleIndex = 21;
            this.colJobSubType.Width = 158;
            // 
            // colJobType
            // 
            this.colJobType.Caption = "Work Type";
            this.colJobType.FieldName = "JobType";
            this.colJobType.Name = "colJobType";
            this.colJobType.OptionsColumn.AllowEdit = false;
            this.colJobType.OptionsColumn.AllowFocus = false;
            this.colJobType.OptionsColumn.ReadOnly = true;
            this.colJobType.Visible = true;
            this.colJobType.VisibleIndex = 20;
            this.colJobType.Width = 166;
            // 
            // colKAMQuoteRejectionReason
            // 
            this.colKAMQuoteRejectionReason.FieldName = "KAMQuoteRejectionReason";
            this.colKAMQuoteRejectionReason.Name = "colKAMQuoteRejectionReason";
            this.colKAMQuoteRejectionReason.OptionsColumn.AllowEdit = false;
            this.colKAMQuoteRejectionReason.OptionsColumn.AllowFocus = false;
            this.colKAMQuoteRejectionReason.OptionsColumn.ReadOnly = true;
            this.colKAMQuoteRejectionReason.Visible = true;
            this.colKAMQuoteRejectionReason.VisibleIndex = 16;
            this.colKAMQuoteRejectionReason.Width = 160;
            // 
            // colWorkType
            // 
            this.colWorkType.Caption = "Work Type : Sub-Type";
            this.colWorkType.FieldName = "WorkType";
            this.colWorkType.Name = "colWorkType";
            this.colWorkType.OptionsColumn.AllowEdit = false;
            this.colWorkType.OptionsColumn.AllowFocus = false;
            this.colWorkType.OptionsColumn.ReadOnly = true;
            // 
            // colSiteCode1
            // 
            this.colSiteCode1.Caption = "Site Code";
            this.colSiteCode1.FieldName = "SiteCode";
            this.colSiteCode1.Name = "colSiteCode1";
            this.colSiteCode1.OptionsColumn.AllowEdit = false;
            this.colSiteCode1.OptionsColumn.AllowFocus = false;
            this.colSiteCode1.OptionsColumn.ReadOnly = true;
            // 
            // colSiteID1
            // 
            this.colSiteID1.Caption = "Site ID";
            this.colSiteID1.FieldName = "SiteID";
            this.colSiteID1.Name = "colSiteID1";
            this.colSiteID1.OptionsColumn.AllowEdit = false;
            this.colSiteID1.OptionsColumn.AllowFocus = false;
            this.colSiteID1.OptionsColumn.ReadOnly = true;
            this.colSiteID1.Width = 74;
            // 
            // colClientID1
            // 
            this.colClientID1.Caption = "Client ID";
            this.colClientID1.FieldName = "ClientID";
            this.colClientID1.Name = "colClientID1";
            this.colClientID1.OptionsColumn.AllowEdit = false;
            this.colClientID1.OptionsColumn.AllowFocus = false;
            this.colClientID1.OptionsColumn.ReadOnly = true;
            // 
            // colRevisionNumber
            // 
            this.colRevisionNumber.Caption = "Revision #";
            this.colRevisionNumber.FieldName = "RevisionNumber";
            this.colRevisionNumber.Name = "colRevisionNumber";
            this.colRevisionNumber.OptionsColumn.AllowEdit = false;
            this.colRevisionNumber.OptionsColumn.AllowFocus = false;
            this.colRevisionNumber.OptionsColumn.ReadOnly = true;
            this.colRevisionNumber.Visible = true;
            this.colRevisionNumber.VisibleIndex = 2;
            this.colRevisionNumber.Width = 70;
            // 
            // colStatusIsssue
            // 
            this.colStatusIsssue.Caption = "Status Issue";
            this.colStatusIsssue.FieldName = "StatusIsssue";
            this.colStatusIsssue.Name = "colStatusIsssue";
            this.colStatusIsssue.OptionsColumn.AllowEdit = false;
            this.colStatusIsssue.OptionsColumn.AllowFocus = false;
            this.colStatusIsssue.OptionsColumn.ReadOnly = true;
            this.colStatusIsssue.Visible = true;
            this.colStatusIsssue.VisibleIndex = 6;
            this.colStatusIsssue.Width = 154;
            // 
            // colStatusIssueID
            // 
            this.colStatusIssueID.Caption = "Status Issue ID";
            this.colStatusIssueID.FieldName = "StatusIssueID";
            this.colStatusIssueID.Name = "colStatusIssueID";
            this.colStatusIssueID.OptionsColumn.AllowEdit = false;
            this.colStatusIssueID.OptionsColumn.AllowFocus = false;
            this.colStatusIssueID.OptionsColumn.ReadOnly = true;
            this.colStatusIssueID.Width = 93;
            // 
            // colSubmittedToCMDate
            // 
            this.colSubmittedToCMDate.Caption = "Sent To CM";
            this.colSubmittedToCMDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colSubmittedToCMDate.FieldName = "SubmittedToCMDate";
            this.colSubmittedToCMDate.Name = "colSubmittedToCMDate";
            this.colSubmittedToCMDate.OptionsColumn.AllowEdit = false;
            this.colSubmittedToCMDate.OptionsColumn.AllowFocus = false;
            this.colSubmittedToCMDate.OptionsColumn.ReadOnly = true;
            this.colSubmittedToCMDate.Visible = true;
            this.colSubmittedToCMDate.VisibleIndex = 14;
            this.colSubmittedToCMDate.Width = 84;
            // 
            // colProposedLabourID
            // 
            this.colProposedLabourID.Caption = "Proposed Labour ID";
            this.colProposedLabourID.FieldName = "ProposedLabourID";
            this.colProposedLabourID.Name = "colProposedLabourID";
            this.colProposedLabourID.OptionsColumn.AllowEdit = false;
            this.colProposedLabourID.OptionsColumn.AllowFocus = false;
            this.colProposedLabourID.OptionsColumn.ReadOnly = true;
            this.colProposedLabourID.Width = 114;
            // 
            // colProposedLabour
            // 
            this.colProposedLabour.Caption = "Proposed Labour";
            this.colProposedLabour.FieldName = "ProposedLabour";
            this.colProposedLabour.Name = "colProposedLabour";
            this.colProposedLabour.OptionsColumn.AllowEdit = false;
            this.colProposedLabour.OptionsColumn.AllowFocus = false;
            this.colProposedLabour.OptionsColumn.ReadOnly = true;
            this.colProposedLabour.Visible = true;
            this.colProposedLabour.VisibleIndex = 37;
            this.colProposedLabour.Width = 160;
            // 
            // colProposedLabourTypeID
            // 
            this.colProposedLabourTypeID.Caption = "Proposed Labour Type ID";
            this.colProposedLabourTypeID.FieldName = "ProposedLabourTypeID";
            this.colProposedLabourTypeID.Name = "colProposedLabourTypeID";
            this.colProposedLabourTypeID.OptionsColumn.AllowEdit = false;
            this.colProposedLabourTypeID.OptionsColumn.AllowFocus = false;
            this.colProposedLabourTypeID.OptionsColumn.ReadOnly = true;
            this.colProposedLabourTypeID.Width = 141;
            // 
            // colProposedLabourType
            // 
            this.colProposedLabourType.FieldName = "ProposedLabourType";
            this.colProposedLabourType.Name = "colProposedLabourType";
            this.colProposedLabourType.OptionsColumn.AllowEdit = false;
            this.colProposedLabourType.OptionsColumn.AllowFocus = false;
            this.colProposedLabourType.OptionsColumn.ReadOnly = true;
            this.colProposedLabourType.Visible = true;
            this.colProposedLabourType.VisibleIndex = 36;
            this.colProposedLabourType.Width = 127;
            // 
            // colTenderType
            // 
            this.colTenderType.Caption = "Tender Type";
            this.colTenderType.FieldName = "TenderType";
            this.colTenderType.Name = "colTenderType";
            this.colTenderType.OptionsColumn.AllowEdit = false;
            this.colTenderType.OptionsColumn.AllowFocus = false;
            this.colTenderType.OptionsColumn.ReadOnly = true;
            this.colTenderType.Visible = true;
            this.colTenderType.VisibleIndex = 3;
            this.colTenderType.Width = 131;
            // 
            // colTenderTypeID
            // 
            this.colTenderTypeID.Caption = "Tender Type ID";
            this.colTenderTypeID.FieldName = "TenderTypeID";
            this.colTenderTypeID.Name = "colTenderTypeID";
            this.colTenderTypeID.OptionsColumn.AllowEdit = false;
            this.colTenderTypeID.OptionsColumn.AllowFocus = false;
            this.colTenderTypeID.OptionsColumn.ReadOnly = true;
            this.colTenderTypeID.Width = 94;
            // 
            // repositoryItemTextEditCurrency2
            // 
            this.repositoryItemTextEditCurrency2.AutoHeight = false;
            this.repositoryItemTextEditCurrency2.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency2.Name = "repositoryItemTextEditCurrency2";
            // 
            // repositoryItemTextEditPercentage
            // 
            this.repositoryItemTextEditPercentage.AutoHeight = false;
            this.repositoryItemTextEditPercentage.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage.Name = "repositoryItemTextEditPercentage";
            // 
            // sp06497_OM_Tenders_For_Client_ContractTableAdapter
            // 
            this.sp06497_OM_Tenders_For_Client_ContractTableAdapter.ClearBeforeFill = true;
            // 
            // sp06508_OM_Select_Extra_Works_Client_ContractTableAdapter
            // 
            this.sp06508_OM_Select_Extra_Works_Client_ContractTableAdapter.ClearBeforeFill = true;
            // 
            // bar2
            // 
            this.bar2.BarName = "Custom 4";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar2.FloatLocation = new System.Drawing.Point(594, 490);
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefreshChildData)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DisableClose = true;
            this.bar2.OptionsBar.DisableCustomization = true;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar2.Text = "Custom 4";
            // 
            // bbiRefreshChildData
            // 
            this.bbiRefreshChildData.Caption = "Refresh Linked Tenders";
            this.bbiRefreshChildData.Id = 32;
            this.bbiRefreshChildData.ImageOptions.Image = global::WoodPlan5.Properties.Resources.refresh_16x16;
            this.bbiRefreshChildData.Name = "bbiRefreshChildData";
            this.bbiRefreshChildData.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefreshChildData.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefreshChildData_ItemClick);
            // 
            // frm_OM_Select_Tender
            // 
            this.ClientSize = new System.Drawing.Size(843, 665);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_OM_Select_Tender";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select Tender";
            this.Load += new System.EventHandler(this.frm_OM_Select_Tender_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06508OMSelectExtraWorksClientContractBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Tender)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDatetime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07194UTPermissionDocumentSelectBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_WorkOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).EndInit();
            this.popupContainerControlDateRange.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06497OMTendersForClientContractBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDatetime;
        private DevExpress.XtraBars.BarEditItem beiDateFilter;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit1;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlDateRange;
        private DevExpress.XtraEditors.SimpleButton btnDateRangeFilterOK;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraBars.Bar bar1;
        private System.Windows.Forms.BindingSource sp07194UTPermissionDocumentSelectBindingSource;
        private DataSet_UT_WorkOrder dataSet_UT_WorkOrder;
        private DataSet_UT_WorkOrderTableAdapters.sp07194_UT_Permission_Document_SelectTableAdapter sp07194_UT_Permission_Document_SelectTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActive;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colSectorTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractValue;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colGCCompanyName;
        private DevExpress.XtraGrid.Columns.GridColumn colContractType;
        private DevExpress.XtraGrid.Columns.GridColumn colContractStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDirector;
        private DevExpress.XtraGrid.Columns.GridColumn colSectorType;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colReactive;
        private System.Windows.Forms.BindingSource sp06497OMTendersForClientContractBindingSource;
        private DataSet_OM_Tender dataSet_OM_Tender;
        private DataSet_OM_TenderTableAdapters.sp06497_OM_Tenders_For_Client_ContractTableAdapter sp06497_OM_Tenders_For_Client_ContractTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colTenderID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientReferenceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colTenderDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colKAM;
        private DevExpress.XtraGrid.Columns.GridColumn colCM;
        private DevExpress.XtraGrid.Columns.GridColumn colTenderGroupID;
        private DevExpress.XtraGrid.Columns.GridColumn colQuoteNotRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colRequestReceivedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colReturnToClientByDate;
        private DevExpress.XtraGrid.Columns.GridColumn colCMInitialAttendanceDate;
        private DevExpress.XtraGrid.Columns.GridColumn colQuoteSubmittedToClientDate;
        private DevExpress.XtraGrid.Columns.GridColumn colQuoteAcceptedByClientDate;
        private DevExpress.XtraGrid.Columns.GridColumn colRequiredWorkCompletedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPONumber;
        private DevExpress.XtraGrid.Columns.GridColumn colKAMQuoteRejectionReasonID;
        private DevExpress.XtraGrid.Columns.GridColumn colOurInternalComments;
        private DevExpress.XtraGrid.Columns.GridColumn colCMComments;
        private DevExpress.XtraGrid.Columns.GridColumn colClientComments;
        private DevExpress.XtraGrid.Columns.GridColumn colTPORequired;
        private DevExpress.XtraGrid.Columns.GridColumn colPlanningAuthorityID;
        private DevExpress.XtraGrid.Columns.GridColumn colTPOCheckedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeProtected;
        private DevExpress.XtraGrid.Columns.GridColumn colPlanningAuthorityNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPlanningAuthoritySubmittedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colPlanningAuthorityOutcomeID;
        private DevExpress.XtraGrid.Columns.GridColumn colPlanningAuthorityOkToProceed;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName1;
        private DevExpress.XtraGrid.Columns.GridColumn colGCCompanyName1;
        private DevExpress.XtraGrid.Columns.GridColumn colSectorType1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToParent;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colTenderGroupDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colTenderStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colContactPersonName;
        private DevExpress.XtraGrid.Columns.GridColumn colContactPersonPosition;
        private DevExpress.XtraGrid.Columns.GridColumn colContactPersonTitle;
        private DevExpress.XtraGrid.Columns.GridColumn colPlanningAuthorityOutcome;
        private DevExpress.XtraGrid.Columns.GridColumn colPlanningAuthority;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubType;
        private DevExpress.XtraGrid.Columns.GridColumn colJobType;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkType;
        private DevExpress.XtraGrid.Columns.GridColumn colKAMQuoteRejectionReason;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractID1;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusID1;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContactID1;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddress1;
        private DevExpress.XtraGrid.Columns.GridColumn colSitePostcode1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLocationX1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLocationY1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedVisitCount1;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffName1;
        private DevExpress.XtraGrid.Columns.GridColumn colKAMName1;
        private DevExpress.XtraGrid.Columns.GridColumn colCMName1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID1;
        private System.Windows.Forms.BindingSource sp06508OMSelectExtraWorksClientContractBindingSource;
        private DataSet_OM_TenderTableAdapters.sp06508_OM_Select_Extra_Works_Client_ContractTableAdapter sp06508_OM_Select_Extra_Works_Client_ContractTableAdapter;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraBars.BarButtonItem bbiRefreshChildData;
        private DevExpress.XtraGrid.Columns.GridColumn colRevisionNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusIsssue;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusIssueID;
        private DevExpress.XtraGrid.Columns.GridColumn colSubmittedToCMDate;
        private DevExpress.XtraGrid.Columns.GridColumn colProposedLabourID;
        private DevExpress.XtraGrid.Columns.GridColumn colProposedLabour;
        private DevExpress.XtraGrid.Columns.GridColumn colProposedLabourTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colProposedLabourType;
        private DevExpress.XtraGrid.Columns.GridColumn colTenderType;
        private DevExpress.XtraGrid.Columns.GridColumn colTenderTypeID;
    }
}

﻿namespace WoodPlan5
{
    partial class frm_OM_Job_Wizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.Utils.SuperToolTip superToolTip10 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem10 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem10 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip11 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem11 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem11 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule2 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue2 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule3 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue3 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule4 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue4 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule5 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue5 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions7 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject25 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject26 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject27 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject28 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions8 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject29 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject30 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject31 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject32 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition3 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions9 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject33 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject34 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject35 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject36 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions10 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject37 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject38 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject39 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject40 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Job_Wizard));
            DevExpress.Utils.Animation.Transition transition1 = new DevExpress.Utils.Animation.Transition();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem9 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem9 = new DevExpress.Utils.ToolTipItem();
            this.colActive1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colActive3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOnHoldReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn356 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn53 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn223 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn361 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageWelcome = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit4 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.btnWelcomeNext = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.xtraTabPageStep1 = new DevExpress.XtraTab.XtraTabPage();
            this.labelControlSelectedClientCount = new DevExpress.XtraEditors.LabelControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.popupContainerControlDateRange = new DevExpress.XtraEditors.PopupContainerControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.btnDateRangeOK = new DevExpress.XtraEditors.SimpleButton();
            this.standaloneBarDockControl3 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit3 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.btnStep1Previous = new DevExpress.XtraEditors.SimpleButton();
            this.btnStep1Next = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp06156OMJobWizardClientContractsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Job = new WoodPlan5.DataSet_OM_Job();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientContractID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGCCompanyID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDirectorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEndDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSectorTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colYearlyPercentageIncrease = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCheckRPIDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastClientPaymentDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPreviousContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinanceClientCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingCentreCodeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGCCompanyName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDirector = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSectorType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientYearCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditInteger = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colContractDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReactive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageStep2 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.StoreInDBCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.DefaultJobCollectionTemplateButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.simpleButtonApplyDefaultJobCollectionTemplate = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.labelControlSelectedSiteCount = new DevExpress.XtraEditors.LabelControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp06157OMJobWizardSiteContractsForClientContractBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSiteContractID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEndDate3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractValue2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colYearlyPercentageIncrease3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLastClientPaymentDate2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colClientID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToParent3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditInteger2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colVisitNumberMax = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOnHold = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colOnHoldReasonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOnHoldStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOnHoldEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDefaultJobCollectionTemplate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDefaultJobCollectionTemplateID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit5 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.btnStep2Next = new DevExpress.XtraEditors.SimpleButton();
            this.btnStep2Previous = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPageStep3 = new DevExpress.XtraTab.XtraTabPage();
            this.standaloneBarDockControl6 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.labelControlSelectedVisitCount = new DevExpress.XtraEditors.LabelControl();
            this.btnStep3Next = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp06158OMJobWizardVisitsForSiteContractBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colVisitID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedStartDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colExpectedEndDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEndDate2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCalculationLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitCost1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colVisitSell1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartLatitude1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditLatLong = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colStartLongitude1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinishLatitude1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinishLongitude1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colCostCalculationLevelDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToParent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditInteger4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLocationX1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationY1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSitePostcode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitCategoryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPOID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPONumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellCalculationLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellCalculationLevelDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedFromVisitTemplate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedFromVisitTemplateID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastVisitStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnStep3Previous = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.SelectedSiteContractCountTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.pictureEdit10 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPageStep4 = new DevExpress.XtraTab.XtraTabPage();
            this.labelControlSelectedJobCount = new DevExpress.XtraEditors.LabelControl();
            this.standaloneBarDockControl2 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp06162OMJobWizardMasterJobSubTypesAvailableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colJobSubTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubTypeRecordOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colJobSubTypeRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colJobTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnStep4Next = new DevExpress.XtraEditors.SimpleButton();
            this.btnStep4Previous = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.SelectedVisitCountTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.pictureEdit7 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPageStep5 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp06176OMJobWizardVisitsSelectedBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLocationX2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationY2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSitePostcode2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPOID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPONumber2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitCategory1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitCategoryID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellCalculationLevel1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellCalculationLevelDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl6 = new DevExpress.XtraGrid.GridControl();
            this.sp06174OMJobEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colstrMode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrRecordIDs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDummyJobID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReactive1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colJobNoLongerRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRequiresAccessPermit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccessPermitID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditClientPONumber = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colClientPOID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinanceSystemPONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colExpectedEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedDurationUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit2DP = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colExpectedDurationUnitsDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditEditDurationUnitDescriptor1 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp06177OMJobDurationDescriptorsWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemGridLookUpEdit4View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn357 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn359 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScheduleSentDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colActualStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualDurationUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualDurationUnitsDescriptionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalMaterialExVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCostTotalMaterialVATRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCostTotalMaterialVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalMaterialCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalMaterialExVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalMaterialVATRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalMaterialVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalMaterialCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalEquipmentExVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalEquipmentVATRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalEquipmentVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalEquipmentCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalEquipmentExVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalEquipmentVATRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalEquipmentVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalEquipmentCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalLabourExVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalLabourVATRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalLabourVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalLabourCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalLabourExVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalLabourVATRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalLabourVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalLabourCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalCostExVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalCostVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalCostExVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalCostVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientInvoiceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateClientInvoiced = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingInvoiceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingInvoiceReceivedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingInvoicePaidDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingInvoiceAmountPaid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotPayContractor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotInvoiceClient = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colRouteOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditInteger3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colStartLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditFloat = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colStartLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinishLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinishLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTenderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTenderJobID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinimumDaysFromLastVisit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditInteger = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colMaximumDaysFromLastVisit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDaysLeeway = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingCentreCodeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientNameContractDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSitePostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubTypeDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeJobSubTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditJobTypeJobSubType = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colJobTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLabourCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaterialCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDisplayOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colMandatory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnStep5Next = new DevExpress.XtraEditors.SimpleButton();
            this.btnStep5Previous = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit8 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPageStep6 = new DevExpress.XtraTab.XtraTabPage();
            this.checkEditSkipEquipmentAndMaterials = new DevExpress.XtraEditors.CheckEdit();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl7 = new DevExpress.XtraGrid.GridControl();
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn40 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn41 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn42 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn43 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn44 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DP6 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn45 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn46 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn47 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn48 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn49 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn50 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn51 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency6 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn52 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage6 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn147 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn148 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn149 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn150 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn151 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn152 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn153 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn154 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn155 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn156 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn157 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn158 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn159 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn160 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn161 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn162 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn163 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn164 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn165 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn166 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn167 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn168 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn169 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn170 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn171 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn172 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn173 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn174 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn175 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn176 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn177 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn178 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn179 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn180 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn181 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn182 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn183 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn184 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn185 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn186 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn187 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn188 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn189 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn190 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn191 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditInteger6 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn192 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn193 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn194 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn195 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn196 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn197 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn198 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn199 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn200 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn201 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn202 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn203 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn204 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn205 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn206 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn207 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn208 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn209 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn210 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn211 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLabourCount1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentCount1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaterialCount1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitCostCalculationLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitCostCalculationLevel1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.gridControl8 = new DevExpress.XtraGrid.GridControl();
            this.sp06182OMJobLabourEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colstrMode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrRecordIDs1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDummyJobID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLabourUsedID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostUnitsUsed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit2DP8 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colCostUnitDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditUnitDescriptor = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp06101OMWorkUnitTypesPicklistBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Contract = new WoodPlan5.DataSet_OM_Contract();
            this.gridView9x = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn54 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn55 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellUnitsUsed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellUnitDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostPerUnitExVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditCurrency8 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colCostPerUnitVatRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditPercentage8 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colSellPerUnitExVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellPerUnitVatRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostValueExVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostValueVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostValueTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellValueExVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellValueVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellValueTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colClientID4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobExpectedStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitNumber2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubTypeDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditContractorName = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colCostUnitDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellUnitDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLabourType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCISPercentage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCISValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPurchaseOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime8 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEndDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDescription3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientNameContractDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeJobSubTypeDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLatLongSiteDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colPostcodeSiteDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditLinkedToPersonType = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colLinkedToPersonTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnStep6Next = new DevExpress.XtraEditors.SimpleButton();
            this.btnStep6Previous = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl10 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit9 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPageStep7 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl3 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl9 = new DevExpress.XtraGrid.GridControl();
            this.gridView9 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn56 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn57 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn58 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn59 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn60 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn61 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn62 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn63 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn64 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn65 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn66 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn67 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn68 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn69 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn70 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn71 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime9 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn72 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn73 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNumeric2DP9 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn74 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn75 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn76 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn77 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn78 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn79 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn80 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency9 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn81 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage9 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn82 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn83 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn84 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn85 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn86 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn87 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn88 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn89 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn90 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn91 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn92 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn93 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn94 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn95 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn96 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn97 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn98 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn99 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn100 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn101 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn102 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn103 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn104 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn105 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn106 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn107 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn108 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn109 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn110 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn111 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn112 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn113 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn114 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn115 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn116 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn117 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn118 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit10 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn119 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn120 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn121 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn122 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn123 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn124 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn125 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn126 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNumeric0DP9 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn127 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn128 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn129 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn130 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn131 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn132 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn133 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn134 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn135 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn136 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn137 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn138 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn139 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn140 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn141 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn142 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn143 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn144 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn145 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn146 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn212 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn213 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn214 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.standaloneBarDockControl4 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.gridControl10 = new DevExpress.XtraGrid.GridControl();
            this.sp06187OMJobEquipmentEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView10 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn215 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn216 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn217 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn218 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn219 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn220 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn221 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.epositoryItemSpinEdit2DP10 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumn222 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView13 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn224 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn225 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn226 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn227 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn228 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.epositoryItemSpinEditCurrency10 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumn229 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.epositoryItemSpinEditPercentage10 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumn230 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn231 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn232 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn233 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn234 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn235 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn236 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn237 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn238 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit11 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn239 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn240 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn241 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn242 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn243 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn244 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn245 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn246 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn247 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn248 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditEquipmentName = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridColumn249 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn250 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn254 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn255 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit12 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn256 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn257 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn258 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn259 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn260 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit13 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn261 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubTypeID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnStep7Next = new DevExpress.XtraEditors.SimpleButton();
            this.btnStep7Previous = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl11 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit11 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPageStep8 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl4 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl11 = new DevExpress.XtraGrid.GridControl();
            this.gridView11 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn251 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn252 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn253 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn262 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn263 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn264 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn265 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn266 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn267 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn268 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn269 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn270 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn271 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn272 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn273 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn274 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn275 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn276 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn277 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn278 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn279 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn280 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn281 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn282 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn283 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn284 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit10 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn285 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn286 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn287 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn288 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn289 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn290 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn291 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn292 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn293 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn294 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn295 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn296 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn297 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn298 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn299 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn300 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn301 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn302 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn303 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn304 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn305 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn306 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn307 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn308 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn309 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn310 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn311 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn312 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn313 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn314 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn315 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn316 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn317 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn318 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn319 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn320 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn321 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit12 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn322 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn323 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn324 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn325 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn326 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn327 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn328 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn329 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit11 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn330 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn331 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn332 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn333 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn334 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn335 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn336 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn337 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn338 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn339 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn340 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn341 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn342 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn343 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn344 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn345 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn346 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn347 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn348 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn349 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn350 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn351 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn352 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.standaloneBarDockControl5 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.gridControl12 = new DevExpress.XtraGrid.GridControl();
            this.sp06191OMJobMaterialEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView12 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn353 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn354 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn355 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaterialUsedID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaterialID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn358 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostUnitsUsed3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit2DP12 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumn360 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp06108OMUnitDescriptorsPicklistBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView14 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn362 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn363 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn364 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn365 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn366 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditCurrency12 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumn367 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditPercentage12 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumn368 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn369 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn370 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn371 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn372 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn373 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn374 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn375 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn376 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit13 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn377 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn378 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn379 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn380 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn381 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn382 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn383 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn384 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn385 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaterialName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditMaterialName = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridColumn387 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn388 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn390 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn391 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit14 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn392 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn393 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn394 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn395 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn396 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit15 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn397 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn399 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn400 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnStep8Next = new DevExpress.XtraEditors.SimpleButton();
            this.btnStep8Previous = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl12 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit12 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPageFinish = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit6 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.checkEdit4 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit3 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.btnFinish = new DevExpress.XtraEditors.SimpleButton();
            this.btnFinishPrevious = new DevExpress.XtraEditors.SimpleButton();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.repositoryItemGridLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemButtonEditChooseStaff = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.sp06145OMVisitEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Visit = new WoodPlan5.DataSet_OM_Visit();
            this.sp06144OMVisitWizardSiteContractStartDatesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.barStep1 = new DevExpress.XtraBars.Bar();
            this.barEditItemDateRange = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditDateRange = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.barEditItemActive = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.bbiRefreshClientContracts = new DevExpress.XtraBars.BarButtonItem();
            this.sp06144_OM_Visit_Wizard_Site_Contract_Start_DatesTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06144_OM_Visit_Wizard_Site_Contract_Start_DatesTableAdapter();
            this.sp06145_OM_Visit_EditTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06145_OM_Visit_EditTableAdapter();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemDuration1 = new DevExpress.XtraScheduler.UI.RepositoryItemDuration();
            this.sp06156_OM_Job_Wizard_Client_ContractsTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06156_OM_Job_Wizard_Client_ContractsTableAdapter();
            this.sp06157_OM_Job_Wizard_Site_Contracts_For_Client_ContractTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06157_OM_Job_Wizard_Site_Contracts_For_Client_ContractTableAdapter();
            this.sp06158_OM_Job_Wizard_Visits_For_Site_ContractTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06158_OM_Job_Wizard_Visits_For_Site_ContractTableAdapter();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.beiJobsFromSiteTemplate = new DevExpress.XtraBars.BarEditItem();
            this.checkEditJobsFromSiteTemplate = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.bbiSelectJobCollection = new DevExpress.XtraBars.BarButtonItem();
            this.bbiJobCollectionManager = new DevExpress.XtraBars.BarButtonItem();
            this.bbiMasterJobManager = new DevExpress.XtraBars.BarButtonItem();
            this.beiJobsFromLastVisit = new DevExpress.XtraBars.BarEditItem();
            this.checkEditJobsFromLastVisit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.sp06162_OM_Job_Wizard_Master_Job_Sub_Types_AvailableTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06162_OM_Job_Wizard_Master_Job_Sub_Types_AvailableTableAdapter();
            this.sp06176_OM_Job_Wizard_Visits_SelectedTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06176_OM_Job_Wizard_Visits_SelectedTableAdapter();
            this.sp06182_OM_Job_Labour_EditTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06182_OM_Job_Labour_EditTableAdapter();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bbiAddLabourFromContract = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAddLabourFromContractAll = new DevExpress.XtraBars.BarButtonItem();
            this.sp06101_OM_Work_Unit_Types_PicklistTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06101_OM_Work_Unit_Types_PicklistTableAdapter();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.bbiAddEquipmentFromContract = new DevExpress.XtraBars.BarButtonItem();
            this.sp06187_OM_Job_Equipment_EditTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06187_OM_Job_Equipment_EditTableAdapter();
            this.bar4 = new DevExpress.XtraBars.Bar();
            this.bbiAddMaterialFromContract = new DevExpress.XtraBars.BarButtonItem();
            this.sp06191_OM_Job_Material_EditTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06191_OM_Job_Material_EditTableAdapter();
            this.sp06108_OM_Unit_Descriptors_PicklistTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06108_OM_Unit_Descriptors_PicklistTableAdapter();
            this.sp06174_OM_Job_EditTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06174_OM_Job_EditTableAdapter();
            this.bsiRecordTicking = new DevExpress.XtraBars.BarSubItem();
            this.bbiTick = new DevExpress.XtraBars.BarButtonItem();
            this.bbiUntick = new DevExpress.XtraBars.BarButtonItem();
            this.transitionManager1 = new DevExpress.Utils.Animation.TransitionManager(this.components);
            this.bar5 = new DevExpress.XtraBars.Bar();
            this.bbiSelectVisitsFromCriteria = new DevExpress.XtraBars.BarButtonItem();
            this.sp06177_OM_Job_Duration_Descriptors_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06177_OM_Job_Duration_Descriptors_With_BlankTableAdapter();
            this.TenderDetailsLabelControl = new DevExpress.XtraEditors.LabelControl();
            this.timerWelcomePage = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPageWelcome.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            this.xtraTabPageStep1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).BeginInit();
            this.popupContainerControlDateRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06156OMJobWizardClientContractsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Job)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger)).BeginInit();
            this.xtraTabPageStep2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StoreInDBCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DefaultJobCollectionTemplateButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06157OMJobWizardSiteContractsForClientContractBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).BeginInit();
            this.xtraTabPageStep3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06158OMJobWizardVisitsForSiteContractBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditLatLong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SelectedSiteContractCountTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit10.Properties)).BeginInit();
            this.xtraTabPageStep4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06162OMJobWizardMasterJobSubTypesAvailableBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SelectedVisitCountTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit7.Properties)).BeginInit();
            this.xtraTabPageStep5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06176OMJobWizardVisitsSelectedBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06174OMJobEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditClientPONumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditEditDurationUnitDescriptor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06177OMJobDurationDescriptorsWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit4View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditFloat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditInteger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditJobTypeJobSubType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit8.Properties)).BeginInit();
            this.xtraTabPageStep6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSkipEquipmentAndMaterials.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06182OMJobLabourEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2DP8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditUnitDescriptor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06101OMWorkUnitTypesPicklistBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9x)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditCurrency8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditPercentage8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditContractorName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditLinkedToPersonType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).BeginInit();
            this.panelControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit9.Properties)).BeginInit();
            this.xtraTabPageStep7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).BeginInit();
            this.splitContainerControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric2DP9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric0DP9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06187OMJobEquipmentEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.epositoryItemSpinEdit2DP10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.epositoryItemSpinEditCurrency10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.epositoryItemSpinEditPercentage10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditEquipmentName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).BeginInit();
            this.panelControl11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit11.Properties)).BeginInit();
            this.xtraTabPageStep8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).BeginInit();
            this.splitContainerControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06191OMJobMaterialEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2DP12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06108OMUnitDescriptorsPicklistBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditCurrency12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditPercentage12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditMaterialName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).BeginInit();
            this.panelControl12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit12.Properties)).BeginInit();
            this.xtraTabPageFinish.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditChooseStaff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06145OMVisitEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Visit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06144OMVisitWizardSiteContractStartDatesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDuration1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditJobsFromSiteTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditJobsFromLastVisit)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiRecordTicking, true)});
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1236, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Size = new System.Drawing.Size(1236, 0);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1236, 0);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barStep1,
            this.bar1,
            this.bar2,
            this.bar3,
            this.bar4,
            this.bar5});
            this.barManager1.DockControls.Add(this.standaloneBarDockControl3);
            this.barManager1.DockControls.Add(this.standaloneBarDockControl6);
            this.barManager1.DockControls.Add(this.standaloneBarDockControl2);
            this.barManager1.DockControls.Add(this.standaloneBarDockControl1);
            this.barManager1.DockControls.Add(this.standaloneBarDockControl4);
            this.barManager1.DockControls.Add(this.standaloneBarDockControl5);
            this.barManager1.Images = this.imageCollection1;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barEditItemDateRange,
            this.bbiRefreshClientContracts,
            this.barEditItemActive,
            this.bbiSelectJobCollection,
            this.bbiJobCollectionManager,
            this.bbiMasterJobManager,
            this.bbiAddLabourFromContract,
            this.bbiAddEquipmentFromContract,
            this.bbiAddMaterialFromContract,
            this.bbiAddLabourFromContractAll,
            this.bsiRecordTicking,
            this.bbiTick,
            this.bbiUntick,
            this.beiJobsFromLastVisit,
            this.bbiSelectVisitsFromCriteria,
            this.beiJobsFromSiteTemplate});
            this.barManager1.MaxItemId = 106;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEditDateRange,
            this.repositoryItemCheckEdit4,
            this.repositoryItemSpinEdit1,
            this.repositoryItemDuration1,
            this.checkEditJobsFromLastVisit,
            this.checkEditJobsFromSiteTemplate});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colActive1
            // 
            this.colActive1.Caption = "Active";
            this.colActive1.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colActive1.FieldName = "Active";
            this.colActive1.Name = "colActive1";
            this.colActive1.OptionsColumn.AllowEdit = false;
            this.colActive1.OptionsColumn.AllowFocus = false;
            this.colActive1.OptionsColumn.ReadOnly = true;
            this.colActive1.Visible = true;
            this.colActive1.VisibleIndex = 4;
            this.colActive1.Width = 51;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colActive3
            // 
            this.colActive3.Caption = "Active";
            this.colActive3.FieldName = "Active";
            this.colActive3.Name = "colActive3";
            this.colActive3.OptionsColumn.AllowEdit = false;
            this.colActive3.OptionsColumn.AllowFocus = false;
            this.colActive3.OptionsColumn.ReadOnly = true;
            this.colActive3.Visible = true;
            this.colActive3.VisibleIndex = 4;
            this.colActive3.Width = 51;
            // 
            // colOnHoldReason
            // 
            this.colOnHoldReason.Caption = "On-Hold Reason";
            this.colOnHoldReason.FieldName = "OnHoldReason";
            this.colOnHoldReason.Name = "colOnHoldReason";
            this.colOnHoldReason.OptionsColumn.AllowEdit = false;
            this.colOnHoldReason.OptionsColumn.AllowFocus = false;
            this.colOnHoldReason.OptionsColumn.ReadOnly = true;
            this.colOnHoldReason.Visible = true;
            this.colOnHoldReason.VisibleIndex = 6;
            this.colOnHoldReason.Width = 147;
            // 
            // colVisitStatusID
            // 
            this.colVisitStatusID.Caption = "Visit Status ID";
            this.colVisitStatusID.FieldName = "VisitStatusID";
            this.colVisitStatusID.Name = "colVisitStatusID";
            this.colVisitStatusID.OptionsColumn.AllowEdit = false;
            this.colVisitStatusID.OptionsColumn.AllowFocus = false;
            this.colVisitStatusID.OptionsColumn.ReadOnly = true;
            this.colVisitStatusID.Width = 86;
            // 
            // gridColumn356
            // 
            this.gridColumn356.Caption = "ID";
            this.gridColumn356.FieldName = "ID";
            this.gridColumn356.Name = "gridColumn356";
            this.gridColumn356.OptionsColumn.AllowEdit = false;
            this.gridColumn356.OptionsColumn.AllowFocus = false;
            this.gridColumn356.OptionsColumn.ReadOnly = true;
            this.gridColumn356.Width = 53;
            // 
            // gridColumn53
            // 
            this.gridColumn53.Caption = "ID";
            this.gridColumn53.FieldName = "ID";
            this.gridColumn53.Name = "gridColumn53";
            this.gridColumn53.OptionsColumn.AllowEdit = false;
            this.gridColumn53.OptionsColumn.AllowFocus = false;
            this.gridColumn53.OptionsColumn.ReadOnly = true;
            this.gridColumn53.Width = 53;
            // 
            // gridColumn223
            // 
            this.gridColumn223.Caption = "ID";
            this.gridColumn223.FieldName = "ID";
            this.gridColumn223.Name = "gridColumn223";
            this.gridColumn223.OptionsColumn.AllowEdit = false;
            this.gridColumn223.OptionsColumn.AllowFocus = false;
            this.gridColumn223.OptionsColumn.ReadOnly = true;
            this.gridColumn223.Width = 53;
            // 
            // gridColumn361
            // 
            this.gridColumn361.Caption = "ID";
            this.gridColumn361.FieldName = "ID";
            this.gridColumn361.Name = "gridColumn361";
            this.gridColumn361.OptionsColumn.AllowEdit = false;
            this.gridColumn361.OptionsColumn.AllowFocus = false;
            this.gridColumn361.OptionsColumn.ReadOnly = true;
            this.gridColumn361.Width = 53;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.xtraTabControl1.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Left;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, -1);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPageWelcome;
            this.xtraTabControl1.Size = new System.Drawing.Size(1240, 542);
            this.xtraTabControl1.TabIndex = 6;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageWelcome,
            this.xtraTabPageStep1,
            this.xtraTabPageStep2,
            this.xtraTabPageStep3,
            this.xtraTabPageStep4,
            this.xtraTabPageStep5,
            this.xtraTabPageStep6,
            this.xtraTabPageStep7,
            this.xtraTabPageStep8,
            this.xtraTabPageFinish});
            this.xtraTabControl1.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl1_SelectedPageChanged);
            this.xtraTabControl1.SelectedPageChanging += new DevExpress.XtraTab.TabPageChangingEventHandler(this.xtraTabControl1_SelectedPageChanging);
            // 
            // xtraTabPageWelcome
            // 
            this.xtraTabPageWelcome.Controls.Add(this.panelControl1);
            this.xtraTabPageWelcome.Controls.Add(this.btnWelcomeNext);
            this.xtraTabPageWelcome.Controls.Add(this.labelControl2);
            this.xtraTabPageWelcome.Controls.Add(this.pictureEdit1);
            this.xtraTabPageWelcome.Name = "xtraTabPageWelcome";
            this.xtraTabPageWelcome.Size = new System.Drawing.Size(1213, 537);
            this.xtraTabPageWelcome.Tag = "0";
            this.xtraTabPageWelcome.Text = "Welcome";
            // 
            // panelControl1
            // 
            this.panelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl1.Controls.Add(this.pictureEdit4);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.labelControl9);
            this.panelControl1.Location = new System.Drawing.Point(211, 6);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(996, 48);
            this.panelControl1.TabIndex = 17;
            // 
            // pictureEdit4
            // 
            this.pictureEdit4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit4.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit4.Location = new System.Drawing.Point(952, 4);
            this.pictureEdit4.MenuManager = this.barManager1;
            this.pictureEdit4.Name = "pictureEdit4";
            this.pictureEdit4.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit4.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit4.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit4.Properties.ReadOnly = true;
            this.pictureEdit4.Properties.ShowMenu = false;
            this.pictureEdit4.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit4.TabIndex = 16;
            // 
            // labelControl1
            // 
            this.labelControl1.AllowHtmlString = true;
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.labelControl1.Appearance.Options.UseBackColor = true;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(5, 1);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(309, 29);
            this.labelControl1.TabIndex = 5;
            this.labelControl1.Text = "Welcome to the <b>Job Wizard</b>";
            // 
            // labelControl9
            // 
            this.labelControl9.AllowHtmlString = true;
            this.labelControl9.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl9.Appearance.Options.UseBackColor = true;
            this.labelControl9.Location = new System.Drawing.Point(9, 31);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(400, 13);
            this.labelControl9.TabIndex = 15;
            this.labelControl9.Text = "This wizard will allow you to create one or more new Jobs for one or more Site Vi" +
    "sits\r\n";
            // 
            // btnWelcomeNext
            // 
            this.btnWelcomeNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnWelcomeNext.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.btnWelcomeNext.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnWelcomeNext.Location = new System.Drawing.Point(1119, 501);
            this.btnWelcomeNext.Name = "btnWelcomeNext";
            this.btnWelcomeNext.Size = new System.Drawing.Size(88, 30);
            this.btnWelcomeNext.TabIndex = 7;
            this.btnWelcomeNext.Text = "Next";
            this.btnWelcomeNext.Click += new System.EventHandler(this.btnWelcomeNext_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.AllowHtmlString = true;
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(220, 104);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(220, 25);
            this.labelControl2.TabIndex = 6;
            this.labelControl2.Text = "Click <b>Next</b> To Continue";
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureEdit1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit1.EditValue = global::WoodPlan5.Properties.Resources.wizard_image;
            this.pictureEdit1.Location = new System.Drawing.Point(0, 0);
            this.pictureEdit1.MenuManager = this.barManager1;
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.ShowMenu = false;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit1.Size = new System.Drawing.Size(205, 537);
            this.pictureEdit1.TabIndex = 4;
            // 
            // xtraTabPageStep1
            // 
            this.xtraTabPageStep1.Controls.Add(this.labelControlSelectedClientCount);
            this.xtraTabPageStep1.Controls.Add(this.popupContainerControlDateRange);
            this.xtraTabPageStep1.Controls.Add(this.standaloneBarDockControl3);
            this.xtraTabPageStep1.Controls.Add(this.panelControl3);
            this.xtraTabPageStep1.Controls.Add(this.btnStep1Previous);
            this.xtraTabPageStep1.Controls.Add(this.btnStep1Next);
            this.xtraTabPageStep1.Controls.Add(this.gridControl1);
            this.xtraTabPageStep1.Name = "xtraTabPageStep1";
            this.xtraTabPageStep1.Size = new System.Drawing.Size(1213, 537);
            this.xtraTabPageStep1.Tag = "1";
            this.xtraTabPageStep1.Text = "Step 1";
            // 
            // labelControlSelectedClientCount
            // 
            this.labelControlSelectedClientCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControlSelectedClientCount.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControlSelectedClientCount.Appearance.ImageIndex = 11;
            this.labelControlSelectedClientCount.Appearance.ImageList = this.imageCollection1;
            this.labelControlSelectedClientCount.Appearance.Options.UseImageAlign = true;
            this.labelControlSelectedClientCount.Appearance.Options.UseImageIndex = true;
            this.labelControlSelectedClientCount.Appearance.Options.UseImageList = true;
            this.labelControlSelectedClientCount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControlSelectedClientCount.Location = new System.Drawing.Point(7, 503);
            this.labelControlSelectedClientCount.Name = "labelControlSelectedClientCount";
            this.labelControlSelectedClientCount.Size = new System.Drawing.Size(188, 17);
            this.labelControlSelectedClientCount.TabIndex = 24;
            this.labelControlSelectedClientCount.Text = "       0 Selected Client Contract(s)";
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16");
            this.imageCollection1.Images.SetKeyName(3, "arrow_up_blue_round.png");
            this.imageCollection1.Images.SetKeyName(4, "arrow_down_blue_round.png");
            this.imageCollection1.Images.SetKeyName(5, "Sort_16x16.png");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.refresh_16x16, "refresh_16x16", typeof(global::WoodPlan5.Properties.Resources), 6);
            this.imageCollection1.Images.SetKeyName(6, "refresh_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.BlockEdit_16x16, "BlockEdit_16x16", typeof(global::WoodPlan5.Properties.Resources), 7);
            this.imageCollection1.Images.SetKeyName(7, "BlockEdit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Preview_16x16, "Preview_16x16", typeof(global::WoodPlan5.Properties.Resources), 8);
            this.imageCollection1.Images.SetKeyName(8, "Preview_16x16");
            this.imageCollection1.InsertGalleryImage("groupheader_16x16.png", "images/reports/groupheader_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/reports/groupheader_16x16.png"), 9);
            this.imageCollection1.Images.SetKeyName(9, "groupheader_16x16.png");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.BlockAdd_16x16, "BlockAdd_16x16", typeof(global::WoodPlan5.Properties.Resources), 10);
            this.imageCollection1.Images.SetKeyName(10, "BlockAdd_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 11);
            this.imageCollection1.Images.SetKeyName(11, "Info_16x16");
            this.imageCollection1.InsertGalleryImage("touchmode_16x16.png", "images/mode/touchmode_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/mode/touchmode_16x16.png"), 12);
            this.imageCollection1.Images.SetKeyName(12, "touchmode_16x16.png");
            // 
            // popupContainerControlDateRange
            // 
            this.popupContainerControlDateRange.Controls.Add(this.groupControl2);
            this.popupContainerControlDateRange.Controls.Add(this.btnDateRangeOK);
            this.popupContainerControlDateRange.Location = new System.Drawing.Point(34, 222);
            this.popupContainerControlDateRange.Name = "popupContainerControlDateRange";
            this.popupContainerControlDateRange.Size = new System.Drawing.Size(199, 107);
            this.popupContainerControlDateRange.TabIndex = 18;
            // 
            // groupControl2
            // 
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl2.Controls.Add(this.labelControl17);
            this.groupControl2.Controls.Add(this.labelControl18);
            this.groupControl2.Controls.Add(this.dateEditFromDate);
            this.groupControl2.Controls.Add(this.dateEditToDate);
            this.groupControl2.Location = new System.Drawing.Point(3, 5);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(193, 76);
            this.groupControl2.TabIndex = 20;
            this.groupControl2.Text = "Date Range  [Contract Start Date]";
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(6, 53);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(16, 13);
            this.labelControl17.TabIndex = 13;
            this.labelControl17.Text = "To:";
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(6, 27);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(28, 13);
            this.labelControl18.TabIndex = 12;
            this.labelControl18.Text = "From:";
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(40, 24);
            this.dateEditFromDate.MenuManager = this.barManager1;
            this.dateEditFromDate.Name = "dateEditFromDate";
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditFromDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditFromDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullValuePrompt = "No Date";
            this.dateEditFromDate.Size = new System.Drawing.Size(148, 20);
            this.dateEditFromDate.TabIndex = 10;
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(40, 50);
            this.dateEditToDate.MenuManager = this.barManager1;
            this.dateEditToDate.Name = "dateEditToDate";
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditToDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditToDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullValuePrompt = "No Date";
            this.dateEditToDate.Size = new System.Drawing.Size(148, 20);
            this.dateEditToDate.TabIndex = 11;
            // 
            // btnDateRangeOK
            // 
            this.btnDateRangeOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDateRangeOK.Location = new System.Drawing.Point(3, 84);
            this.btnDateRangeOK.Name = "btnDateRangeOK";
            this.btnDateRangeOK.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnDateRangeOK.Size = new System.Drawing.Size(50, 20);
            this.btnDateRangeOK.TabIndex = 19;
            this.btnDateRangeOK.Text = "OK";
            this.btnDateRangeOK.Click += new System.EventHandler(this.btnDateRangeOK_Click);
            // 
            // standaloneBarDockControl3
            // 
            this.standaloneBarDockControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl3.CausesValidation = false;
            this.standaloneBarDockControl3.Location = new System.Drawing.Point(7, 62);
            this.standaloneBarDockControl3.Manager = this.barManager1;
            this.standaloneBarDockControl3.Name = "standaloneBarDockControl3";
            this.standaloneBarDockControl3.Size = new System.Drawing.Size(1200, 26);
            this.standaloneBarDockControl3.Text = "standaloneBarDockControl3";
            // 
            // panelControl3
            // 
            this.panelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl3.Controls.Add(this.labelControl3);
            this.panelControl3.Controls.Add(this.pictureEdit3);
            this.panelControl3.Controls.Add(this.labelControl5);
            this.panelControl3.Location = new System.Drawing.Point(7, 6);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(1200, 48);
            this.panelControl3.TabIndex = 10;
            // 
            // labelControl3
            // 
            this.labelControl3.AllowHtmlString = true;
            this.labelControl3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseBackColor = true;
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(5, 5);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(348, 16);
            this.labelControl3.TabIndex = 6;
            this.labelControl3.Text = "<b>Step 1:</b> Select the Client Contract(s) to create the Job(s) for";
            // 
            // pictureEdit3
            // 
            this.pictureEdit3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit3.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit3.Location = new System.Drawing.Point(1156, 4);
            this.pictureEdit3.MenuManager = this.barManager1;
            this.pictureEdit3.Name = "pictureEdit3";
            this.pictureEdit3.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit3.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit3.Properties.ReadOnly = true;
            this.pictureEdit3.Properties.ShowMenu = false;
            this.pictureEdit3.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit3.TabIndex = 8;
            // 
            // labelControl5
            // 
            this.labelControl5.AllowHtmlString = true;
            this.labelControl5.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl5.Appearance.Options.UseBackColor = true;
            this.labelControl5.Location = new System.Drawing.Point(57, 29);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(456, 13);
            this.labelControl5.TabIndex = 7;
            this.labelControl5.Text = "Select one or more Client Contracts to create Jobs for by <b>ticking</b> them. Cl" +
    "ick Next when done.\r\n";
            // 
            // btnStep1Previous
            // 
            this.btnStep1Previous.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep1Previous.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnStep1Previous.Location = new System.Drawing.Point(1025, 501);
            this.btnStep1Previous.Name = "btnStep1Previous";
            this.btnStep1Previous.Size = new System.Drawing.Size(88, 30);
            this.btnStep1Previous.TabIndex = 9;
            this.btnStep1Previous.Text = "Previous";
            this.btnStep1Previous.Click += new System.EventHandler(this.btnStep1Previous_Click);
            // 
            // btnStep1Next
            // 
            this.btnStep1Next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep1Next.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.btnStep1Next.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnStep1Next.Location = new System.Drawing.Point(1119, 501);
            this.btnStep1Next.Name = "btnStep1Next";
            this.btnStep1Next.Size = new System.Drawing.Size(88, 30);
            this.btnStep1Next.TabIndex = 8;
            this.btnStep1Next.Text = "Next";
            this.btnStep1Next.Click += new System.EventHandler(this.btnStep1Next_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.DataSource = this.sp06156OMJobWizardClientContractsBindingSource;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Reload Data", "reload")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(7, 88);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEditDateTime3,
            this.repositoryItemTextEditCurrency3,
            this.repositoryItemTextEditPercentage3,
            this.repositoryItemTextEditInteger});
            this.gridControl1.Size = new System.Drawing.Size(1200, 407);
            this.gridControl1.TabIndex = 7;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp06156OMJobWizardClientContractsBindingSource
            // 
            this.sp06156OMJobWizardClientContractsBindingSource.DataMember = "sp06156_OM_Job_Wizard_Client_Contracts";
            this.sp06156OMJobWizardClientContractsBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // dataSet_OM_Job
            // 
            this.dataSet_OM_Job.DataSetName = "DataSet_OM_Job";
            this.dataSet_OM_Job.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientContractID1,
            this.colClientID,
            this.colGCCompanyID,
            this.colContractTypeID,
            this.colContractStatusID,
            this.colContractDirectorID,
            this.colStartDate1,
            this.colEndDate1,
            this.colActive1,
            this.colSectorTypeID,
            this.colContractValue,
            this.colYearlyPercentageIncrease,
            this.colCheckRPIDate,
            this.colLastClientPaymentDate,
            this.colLinkedToPreviousContractID,
            this.colFinanceClientCode,
            this.colBillingCentreCodeID,
            this.colRemarks,
            this.colClientName,
            this.colGCCompanyName,
            this.colContractType,
            this.colContractStatus,
            this.colContractDirector,
            this.colSectorType,
            this.colClientYearCount,
            this.colContractDescription,
            this.colReactive});
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.colActive1;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Gray;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView1.FormatRules.Add(gridFormatRule1);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsFind.FindDelay = 2000;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colStartDate1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colClientContractID1
            // 
            this.colClientContractID1.Caption = "Client Contract ID";
            this.colClientContractID1.FieldName = "ClientContractID";
            this.colClientContractID1.Name = "colClientContractID1";
            this.colClientContractID1.OptionsColumn.AllowEdit = false;
            this.colClientContractID1.OptionsColumn.AllowFocus = false;
            this.colClientContractID1.OptionsColumn.ReadOnly = true;
            this.colClientContractID1.Width = 107;
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // colGCCompanyID
            // 
            this.colGCCompanyID.Caption = "GC Company ID";
            this.colGCCompanyID.FieldName = "GCCompanyID";
            this.colGCCompanyID.Name = "colGCCompanyID";
            this.colGCCompanyID.OptionsColumn.AllowEdit = false;
            this.colGCCompanyID.OptionsColumn.AllowFocus = false;
            this.colGCCompanyID.OptionsColumn.ReadOnly = true;
            this.colGCCompanyID.Width = 97;
            // 
            // colContractTypeID
            // 
            this.colContractTypeID.Caption = "Contract Type ID";
            this.colContractTypeID.FieldName = "ContractTypeID";
            this.colContractTypeID.Name = "colContractTypeID";
            this.colContractTypeID.OptionsColumn.AllowEdit = false;
            this.colContractTypeID.OptionsColumn.AllowFocus = false;
            this.colContractTypeID.OptionsColumn.ReadOnly = true;
            this.colContractTypeID.Width = 104;
            // 
            // colContractStatusID
            // 
            this.colContractStatusID.Caption = "Contract Status ID";
            this.colContractStatusID.FieldName = "ContractStatusID";
            this.colContractStatusID.Name = "colContractStatusID";
            this.colContractStatusID.OptionsColumn.AllowEdit = false;
            this.colContractStatusID.OptionsColumn.AllowFocus = false;
            this.colContractStatusID.OptionsColumn.ReadOnly = true;
            this.colContractStatusID.Width = 111;
            // 
            // colContractDirectorID
            // 
            this.colContractDirectorID.Caption = "Contract Director ID";
            this.colContractDirectorID.FieldName = "ContractDirectorID";
            this.colContractDirectorID.Name = "colContractDirectorID";
            this.colContractDirectorID.OptionsColumn.AllowEdit = false;
            this.colContractDirectorID.OptionsColumn.AllowFocus = false;
            this.colContractDirectorID.OptionsColumn.ReadOnly = true;
            this.colContractDirectorID.Width = 118;
            // 
            // colStartDate1
            // 
            this.colStartDate1.Caption = "Start Date";
            this.colStartDate1.ColumnEdit = this.repositoryItemTextEditDateTime3;
            this.colStartDate1.FieldName = "StartDate";
            this.colStartDate1.Name = "colStartDate1";
            this.colStartDate1.OptionsColumn.AllowEdit = false;
            this.colStartDate1.OptionsColumn.AllowFocus = false;
            this.colStartDate1.OptionsColumn.ReadOnly = true;
            this.colStartDate1.Visible = true;
            this.colStartDate1.VisibleIndex = 2;
            this.colStartDate1.Width = 100;
            // 
            // repositoryItemTextEditDateTime3
            // 
            this.repositoryItemTextEditDateTime3.AutoHeight = false;
            this.repositoryItemTextEditDateTime3.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime3.Name = "repositoryItemTextEditDateTime3";
            // 
            // colEndDate1
            // 
            this.colEndDate1.Caption = "End Date";
            this.colEndDate1.ColumnEdit = this.repositoryItemTextEditDateTime3;
            this.colEndDate1.FieldName = "EndDate";
            this.colEndDate1.Name = "colEndDate1";
            this.colEndDate1.OptionsColumn.AllowEdit = false;
            this.colEndDate1.OptionsColumn.AllowFocus = false;
            this.colEndDate1.OptionsColumn.ReadOnly = true;
            this.colEndDate1.Visible = true;
            this.colEndDate1.VisibleIndex = 3;
            this.colEndDate1.Width = 100;
            // 
            // colSectorTypeID
            // 
            this.colSectorTypeID.Caption = "Sector Type ID";
            this.colSectorTypeID.FieldName = "SectorTypeID";
            this.colSectorTypeID.Name = "colSectorTypeID";
            this.colSectorTypeID.OptionsColumn.AllowEdit = false;
            this.colSectorTypeID.OptionsColumn.AllowFocus = false;
            this.colSectorTypeID.OptionsColumn.ReadOnly = true;
            this.colSectorTypeID.Width = 93;
            // 
            // colContractValue
            // 
            this.colContractValue.Caption = "Contract Value";
            this.colContractValue.ColumnEdit = this.repositoryItemTextEditCurrency3;
            this.colContractValue.FieldName = "ContractValue";
            this.colContractValue.Name = "colContractValue";
            this.colContractValue.OptionsColumn.AllowEdit = false;
            this.colContractValue.OptionsColumn.AllowFocus = false;
            this.colContractValue.OptionsColumn.ReadOnly = true;
            this.colContractValue.Visible = true;
            this.colContractValue.VisibleIndex = 10;
            this.colContractValue.Width = 92;
            // 
            // repositoryItemTextEditCurrency3
            // 
            this.repositoryItemTextEditCurrency3.AutoHeight = false;
            this.repositoryItemTextEditCurrency3.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency3.Name = "repositoryItemTextEditCurrency3";
            // 
            // colYearlyPercentageIncrease
            // 
            this.colYearlyPercentageIncrease.Caption = "Yearly % Increase";
            this.colYearlyPercentageIncrease.ColumnEdit = this.repositoryItemTextEditPercentage3;
            this.colYearlyPercentageIncrease.FieldName = "YearlyPercentageIncrease";
            this.colYearlyPercentageIncrease.Name = "colYearlyPercentageIncrease";
            this.colYearlyPercentageIncrease.OptionsColumn.AllowEdit = false;
            this.colYearlyPercentageIncrease.OptionsColumn.AllowFocus = false;
            this.colYearlyPercentageIncrease.OptionsColumn.ReadOnly = true;
            this.colYearlyPercentageIncrease.Visible = true;
            this.colYearlyPercentageIncrease.VisibleIndex = 11;
            this.colYearlyPercentageIncrease.Width = 110;
            // 
            // repositoryItemTextEditPercentage3
            // 
            this.repositoryItemTextEditPercentage3.AutoHeight = false;
            this.repositoryItemTextEditPercentage3.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage3.Name = "repositoryItemTextEditPercentage3";
            // 
            // colCheckRPIDate
            // 
            this.colCheckRPIDate.Caption = "Check RPI Date";
            this.colCheckRPIDate.ColumnEdit = this.repositoryItemTextEditDateTime3;
            this.colCheckRPIDate.FieldName = "CheckRPIDate";
            this.colCheckRPIDate.Name = "colCheckRPIDate";
            this.colCheckRPIDate.OptionsColumn.AllowEdit = false;
            this.colCheckRPIDate.OptionsColumn.AllowFocus = false;
            this.colCheckRPIDate.OptionsColumn.ReadOnly = true;
            this.colCheckRPIDate.Width = 96;
            // 
            // colLastClientPaymentDate
            // 
            this.colLastClientPaymentDate.Caption = "Last Client Payment Date";
            this.colLastClientPaymentDate.ColumnEdit = this.repositoryItemTextEditDateTime3;
            this.colLastClientPaymentDate.FieldName = "LastClientPaymentDate";
            this.colLastClientPaymentDate.Name = "colLastClientPaymentDate";
            this.colLastClientPaymentDate.OptionsColumn.AllowEdit = false;
            this.colLastClientPaymentDate.OptionsColumn.AllowFocus = false;
            this.colLastClientPaymentDate.OptionsColumn.ReadOnly = true;
            this.colLastClientPaymentDate.Width = 142;
            // 
            // colLinkedToPreviousContractID
            // 
            this.colLinkedToPreviousContractID.Caption = "Linked To Previous Contract ID";
            this.colLinkedToPreviousContractID.FieldName = "LinkedToPreviousContractID";
            this.colLinkedToPreviousContractID.Name = "colLinkedToPreviousContractID";
            this.colLinkedToPreviousContractID.OptionsColumn.AllowEdit = false;
            this.colLinkedToPreviousContractID.OptionsColumn.AllowFocus = false;
            this.colLinkedToPreviousContractID.OptionsColumn.ReadOnly = true;
            this.colLinkedToPreviousContractID.Width = 169;
            // 
            // colFinanceClientCode
            // 
            this.colFinanceClientCode.Caption = "Finance Client Code";
            this.colFinanceClientCode.FieldName = "FinanceClientCode";
            this.colFinanceClientCode.Name = "colFinanceClientCode";
            this.colFinanceClientCode.OptionsColumn.AllowEdit = false;
            this.colFinanceClientCode.OptionsColumn.AllowFocus = false;
            this.colFinanceClientCode.OptionsColumn.ReadOnly = true;
            this.colFinanceClientCode.Width = 116;
            // 
            // colBillingCentreCodeID
            // 
            this.colBillingCentreCodeID.Caption = "Billing Centre Code ID";
            this.colBillingCentreCodeID.FieldName = "BillingCentreCodeID";
            this.colBillingCentreCodeID.Name = "colBillingCentreCodeID";
            this.colBillingCentreCodeID.OptionsColumn.AllowEdit = false;
            this.colBillingCentreCodeID.OptionsColumn.AllowFocus = false;
            this.colBillingCentreCodeID.OptionsColumn.ReadOnly = true;
            this.colBillingCentreCodeID.Width = 125;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.AllowEdit = false;
            this.colRemarks.OptionsColumn.AllowFocus = false;
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 13;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 0;
            this.colClientName.Width = 195;
            // 
            // colGCCompanyName
            // 
            this.colGCCompanyName.Caption = "Gc Company Name";
            this.colGCCompanyName.FieldName = "GCCompanyName";
            this.colGCCompanyName.Name = "colGCCompanyName";
            this.colGCCompanyName.OptionsColumn.AllowEdit = false;
            this.colGCCompanyName.OptionsColumn.AllowFocus = false;
            this.colGCCompanyName.OptionsColumn.ReadOnly = true;
            this.colGCCompanyName.Width = 111;
            // 
            // colContractType
            // 
            this.colContractType.Caption = "Contract Type";
            this.colContractType.FieldName = "ContractType";
            this.colContractType.Name = "colContractType";
            this.colContractType.OptionsColumn.AllowEdit = false;
            this.colContractType.OptionsColumn.AllowFocus = false;
            this.colContractType.OptionsColumn.ReadOnly = true;
            this.colContractType.Visible = true;
            this.colContractType.VisibleIndex = 6;
            this.colContractType.Width = 147;
            // 
            // colContractStatus
            // 
            this.colContractStatus.Caption = "Contract Status";
            this.colContractStatus.FieldName = "ContractStatus";
            this.colContractStatus.Name = "colContractStatus";
            this.colContractStatus.OptionsColumn.AllowEdit = false;
            this.colContractStatus.OptionsColumn.AllowFocus = false;
            this.colContractStatus.OptionsColumn.ReadOnly = true;
            this.colContractStatus.Visible = true;
            this.colContractStatus.VisibleIndex = 8;
            this.colContractStatus.Width = 136;
            // 
            // colContractDirector
            // 
            this.colContractDirector.Caption = "Contract Director";
            this.colContractDirector.FieldName = "ContractDirector";
            this.colContractDirector.Name = "colContractDirector";
            this.colContractDirector.OptionsColumn.AllowEdit = false;
            this.colContractDirector.OptionsColumn.AllowFocus = false;
            this.colContractDirector.OptionsColumn.ReadOnly = true;
            this.colContractDirector.Visible = true;
            this.colContractDirector.VisibleIndex = 9;
            this.colContractDirector.Width = 120;
            // 
            // colSectorType
            // 
            this.colSectorType.Caption = "Sector Type";
            this.colSectorType.FieldName = "SectorType";
            this.colSectorType.Name = "colSectorType";
            this.colSectorType.OptionsColumn.AllowEdit = false;
            this.colSectorType.OptionsColumn.AllowFocus = false;
            this.colSectorType.OptionsColumn.ReadOnly = true;
            this.colSectorType.Visible = true;
            this.colSectorType.VisibleIndex = 7;
            this.colSectorType.Width = 146;
            // 
            // colClientYearCount
            // 
            this.colClientYearCount.Caption = "Linked Visits";
            this.colClientYearCount.ColumnEdit = this.repositoryItemTextEditInteger;
            this.colClientYearCount.FieldName = "VisitCount";
            this.colClientYearCount.Name = "colClientYearCount";
            this.colClientYearCount.OptionsColumn.AllowEdit = false;
            this.colClientYearCount.OptionsColumn.AllowFocus = false;
            this.colClientYearCount.OptionsColumn.ReadOnly = true;
            this.colClientYearCount.Visible = true;
            this.colClientYearCount.VisibleIndex = 12;
            this.colClientYearCount.Width = 78;
            // 
            // repositoryItemTextEditInteger
            // 
            this.repositoryItemTextEditInteger.AutoHeight = false;
            this.repositoryItemTextEditInteger.Mask.EditMask = "f0";
            this.repositoryItemTextEditInteger.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditInteger.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditInteger.Name = "repositoryItemTextEditInteger";
            // 
            // colContractDescription
            // 
            this.colContractDescription.Caption = "Contract Description";
            this.colContractDescription.FieldName = "ContractDescription";
            this.colContractDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colContractDescription.Name = "colContractDescription";
            this.colContractDescription.OptionsColumn.AllowEdit = false;
            this.colContractDescription.OptionsColumn.AllowFocus = false;
            this.colContractDescription.OptionsColumn.ReadOnly = true;
            this.colContractDescription.Visible = true;
            this.colContractDescription.VisibleIndex = 1;
            this.colContractDescription.Width = 285;
            // 
            // colReactive
            // 
            this.colReactive.Caption = "Reactive";
            this.colReactive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colReactive.FieldName = "Reactive";
            this.colReactive.Name = "colReactive";
            this.colReactive.OptionsColumn.AllowEdit = false;
            this.colReactive.OptionsColumn.AllowFocus = false;
            this.colReactive.OptionsColumn.ReadOnly = true;
            this.colReactive.Visible = true;
            this.colReactive.VisibleIndex = 5;
            this.colReactive.Width = 63;
            // 
            // xtraTabPageStep2
            // 
            this.xtraTabPageStep2.Controls.Add(this.panelControl7);
            this.xtraTabPageStep2.Controls.Add(this.labelControlSelectedSiteCount);
            this.xtraTabPageStep2.Controls.Add(this.gridControl2);
            this.xtraTabPageStep2.Controls.Add(this.panelControl4);
            this.xtraTabPageStep2.Controls.Add(this.btnStep2Next);
            this.xtraTabPageStep2.Controls.Add(this.btnStep2Previous);
            this.xtraTabPageStep2.Name = "xtraTabPageStep2";
            this.xtraTabPageStep2.Size = new System.Drawing.Size(1213, 537);
            this.xtraTabPageStep2.Tag = "2";
            this.xtraTabPageStep2.Text = "Step 2";
            // 
            // panelControl7
            // 
            this.panelControl7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panelControl7.Controls.Add(this.StoreInDBCheckEdit);
            this.panelControl7.Controls.Add(this.DefaultJobCollectionTemplateButtonEdit);
            this.panelControl7.Controls.Add(this.simpleButtonApplyDefaultJobCollectionTemplate);
            this.panelControl7.Controls.Add(this.labelControl28);
            this.panelControl7.Location = new System.Drawing.Point(201, 501);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(563, 30);
            this.panelControl7.TabIndex = 25;
            // 
            // StoreInDBCheckEdit
            // 
            this.StoreInDBCheckEdit.EditValue = 1;
            this.StoreInDBCheckEdit.Location = new System.Drawing.Point(417, 4);
            this.StoreInDBCheckEdit.MenuManager = this.barManager1;
            this.StoreInDBCheckEdit.Name = "StoreInDBCheckEdit";
            this.StoreInDBCheckEdit.Properties.Caption = "Store In DB";
            this.StoreInDBCheckEdit.Properties.ValueChecked = 1;
            this.StoreInDBCheckEdit.Properties.ValueUnchecked = 0;
            this.StoreInDBCheckEdit.Size = new System.Drawing.Size(75, 19);
            toolTipTitleItem10.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem10.Text = "Store in DB - Information";
            toolTipItem10.LeftIndent = 6;
            toolTipItem10.Text = resources.GetString("toolTipItem10.Text");
            superToolTip10.Items.Add(toolTipTitleItem10);
            superToolTip10.Items.Add(toolTipItem10);
            this.StoreInDBCheckEdit.SuperTip = superToolTip10;
            this.StoreInDBCheckEdit.TabIndex = 27;
            // 
            // DefaultJobCollectionTemplateButtonEdit
            // 
            this.DefaultJobCollectionTemplateButtonEdit.EditValue = "";
            this.DefaultJobCollectionTemplateButtonEdit.Location = new System.Drawing.Point(186, 4);
            this.DefaultJobCollectionTemplateButtonEdit.MenuManager = this.barManager1;
            this.DefaultJobCollectionTemplateButtonEdit.Name = "DefaultJobCollectionTemplateButtonEdit";
            this.DefaultJobCollectionTemplateButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click me to open the Select Job Collection Template screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Click me to Clear the selected Default Job Collection Template", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.DefaultJobCollectionTemplateButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.DefaultJobCollectionTemplateButtonEdit.Size = new System.Drawing.Size(225, 20);
            this.DefaultJobCollectionTemplateButtonEdit.TabIndex = 26;
            this.DefaultJobCollectionTemplateButtonEdit.TabStop = false;
            this.DefaultJobCollectionTemplateButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.DefaultJobCollectionTemplateButtonEdit_ButtonClick);
            // 
            // simpleButtonApplyDefaultJobCollectionTemplate
            // 
            this.simpleButtonApplyDefaultJobCollectionTemplate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonApplyDefaultJobCollectionTemplate.ImageOptions.Image")));
            this.simpleButtonApplyDefaultJobCollectionTemplate.Location = new System.Drawing.Point(500, 4);
            this.simpleButtonApplyDefaultJobCollectionTemplate.Name = "simpleButtonApplyDefaultJobCollectionTemplate";
            this.simpleButtonApplyDefaultJobCollectionTemplate.Size = new System.Drawing.Size(59, 22);
            superToolTip11.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem11.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem11.Text = "Apply Default Template Job Collection - Information";
            toolTipItem11.LeftIndent = 6;
            toolTipItem11.Text = resources.GetString("toolTipItem11.Text");
            superToolTip11.Items.Add(toolTipTitleItem11);
            superToolTip11.Items.Add(toolTipItem11);
            this.simpleButtonApplyDefaultJobCollectionTemplate.SuperTip = superToolTip11;
            this.simpleButtonApplyDefaultJobCollectionTemplate.TabIndex = 2;
            this.simpleButtonApplyDefaultJobCollectionTemplate.Text = "Apply";
            this.simpleButtonApplyDefaultJobCollectionTemplate.Click += new System.EventHandler(this.simpleButtonApplyDefaultJobCollectionTemplate_Click);
            // 
            // labelControl28
            // 
            this.labelControl28.Location = new System.Drawing.Point(6, 8);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(174, 13);
            this.labelControl28.TabIndex = 0;
            this.labelControl28.Text = "Set Default Template Job Collection:";
            // 
            // labelControlSelectedSiteCount
            // 
            this.labelControlSelectedSiteCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControlSelectedSiteCount.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControlSelectedSiteCount.Appearance.ImageIndex = 11;
            this.labelControlSelectedSiteCount.Appearance.ImageList = this.imageCollection1;
            this.labelControlSelectedSiteCount.Appearance.Options.UseImageAlign = true;
            this.labelControlSelectedSiteCount.Appearance.Options.UseImageIndex = true;
            this.labelControlSelectedSiteCount.Appearance.Options.UseImageList = true;
            this.labelControlSelectedSiteCount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControlSelectedSiteCount.Location = new System.Drawing.Point(7, 503);
            this.labelControlSelectedSiteCount.Name = "labelControlSelectedSiteCount";
            this.labelControlSelectedSiteCount.Size = new System.Drawing.Size(188, 17);
            this.labelControlSelectedSiteCount.TabIndex = 23;
            this.labelControlSelectedSiteCount.Text = "       0 Selected Site Contract(s)";
            // 
            // gridControl2
            // 
            this.gridControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl2.DataSource = this.sp06157OMJobWizardSiteContractsForClientContractBindingSource;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.Location = new System.Drawing.Point(7, 62);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit6,
            this.repositoryItemTextEditDateTime2,
            this.repositoryItemTextEditCurrency2,
            this.repositoryItemTextEditPercentage2,
            this.repositoryItemTextEditInteger2,
            this.repositoryItemCheckEdit7});
            this.gridControl2.Size = new System.Drawing.Size(1200, 433);
            this.gridControl2.TabIndex = 22;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp06157OMJobWizardSiteContractsForClientContractBindingSource
            // 
            this.sp06157OMJobWizardSiteContractsForClientContractBindingSource.DataMember = "sp06157_OM_Job_Wizard_Site_Contracts_For_Client_Contract";
            this.sp06157OMJobWizardSiteContractsForClientContractBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSiteContractID3,
            this.colClientContractID3,
            this.colSiteID,
            this.colStartDate3,
            this.colEndDate3,
            this.colActive3,
            this.colContractValue2,
            this.colYearlyPercentageIncrease3,
            this.colLastClientPaymentDate2,
            this.colRemarks4,
            this.colClientID2,
            this.colSiteName,
            this.colLinkedToParent3,
            this.colVisitCount,
            this.colVisitNumberMax,
            this.colOnHold,
            this.colOnHoldReasonID,
            this.colOnHoldReason,
            this.colOnHoldStartDate,
            this.colOnHoldEndDate,
            this.colSiteCategory,
            this.colDefaultJobCollectionTemplate,
            this.colDefaultJobCollectionTemplateID});
            gridFormatRule2.ApplyToRow = true;
            gridFormatRule2.Column = this.colActive3;
            gridFormatRule2.Name = "FormatActive";
            formatConditionRuleValue2.Appearance.ForeColor = System.Drawing.Color.Gray;
            formatConditionRuleValue2.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue2.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue2.Value1 = 0;
            gridFormatRule2.Rule = formatConditionRuleValue2;
            gridFormatRule3.ApplyToRow = true;
            gridFormatRule3.Column = this.colOnHoldReason;
            gridFormatRule3.Name = "Format0";
            formatConditionRuleValue3.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue3.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue3.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue3.Value1 = 1;
            gridFormatRule3.Rule = formatConditionRuleValue3;
            this.gridView2.FormatRules.Add(gridFormatRule2);
            this.gridView2.FormatRules.Add(gridFormatRule3);
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupCount = 1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsFind.AlwaysVisible = true;
            this.gridView2.OptionsFind.FindDelay = 2000;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsLayout.StoreFormatRules = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToParent3, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView2_MouseUp);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // colSiteContractID3
            // 
            this.colSiteContractID3.Caption = "Site Contract ID";
            this.colSiteContractID3.FieldName = "SiteContractID";
            this.colSiteContractID3.Name = "colSiteContractID3";
            this.colSiteContractID3.OptionsColumn.AllowEdit = false;
            this.colSiteContractID3.OptionsColumn.AllowFocus = false;
            this.colSiteContractID3.OptionsColumn.ReadOnly = true;
            this.colSiteContractID3.Width = 98;
            // 
            // colClientContractID3
            // 
            this.colClientContractID3.Caption = "Client Contract ID";
            this.colClientContractID3.FieldName = "ClientContractID";
            this.colClientContractID3.Name = "colClientContractID3";
            this.colClientContractID3.OptionsColumn.AllowEdit = false;
            this.colClientContractID3.OptionsColumn.AllowFocus = false;
            this.colClientContractID3.OptionsColumn.ReadOnly = true;
            this.colClientContractID3.Width = 107;
            // 
            // colSiteID
            // 
            this.colSiteID.Caption = "Site ID";
            this.colSiteID.FieldName = "SiteID";
            this.colSiteID.Name = "colSiteID";
            this.colSiteID.OptionsColumn.AllowEdit = false;
            this.colSiteID.OptionsColumn.AllowFocus = false;
            this.colSiteID.OptionsColumn.ReadOnly = true;
            this.colSiteID.Visible = true;
            this.colSiteID.VisibleIndex = 0;
            this.colSiteID.Width = 51;
            // 
            // colStartDate3
            // 
            this.colStartDate3.Caption = "Start Date";
            this.colStartDate3.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colStartDate3.FieldName = "StartDate";
            this.colStartDate3.Name = "colStartDate3";
            this.colStartDate3.OptionsColumn.AllowEdit = false;
            this.colStartDate3.OptionsColumn.AllowFocus = false;
            this.colStartDate3.OptionsColumn.ReadOnly = true;
            this.colStartDate3.Visible = true;
            this.colStartDate3.VisibleIndex = 2;
            this.colStartDate3.Width = 100;
            // 
            // repositoryItemTextEditDateTime2
            // 
            this.repositoryItemTextEditDateTime2.AutoHeight = false;
            this.repositoryItemTextEditDateTime2.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime2.Name = "repositoryItemTextEditDateTime2";
            // 
            // colEndDate3
            // 
            this.colEndDate3.Caption = "End Date";
            this.colEndDate3.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colEndDate3.FieldName = "EndDate";
            this.colEndDate3.Name = "colEndDate3";
            this.colEndDate3.OptionsColumn.AllowEdit = false;
            this.colEndDate3.OptionsColumn.AllowFocus = false;
            this.colEndDate3.OptionsColumn.ReadOnly = true;
            this.colEndDate3.Visible = true;
            this.colEndDate3.VisibleIndex = 3;
            this.colEndDate3.Width = 100;
            // 
            // colContractValue2
            // 
            this.colContractValue2.Caption = "Value";
            this.colContractValue2.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colContractValue2.FieldName = "ContractValue";
            this.colContractValue2.Name = "colContractValue2";
            this.colContractValue2.OptionsColumn.AllowEdit = false;
            this.colContractValue2.OptionsColumn.AllowFocus = false;
            this.colContractValue2.OptionsColumn.ReadOnly = true;
            this.colContractValue2.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ContractValue", "{0:c}")});
            this.colContractValue2.Visible = true;
            this.colContractValue2.VisibleIndex = 10;
            // 
            // repositoryItemTextEditCurrency2
            // 
            this.repositoryItemTextEditCurrency2.AutoHeight = false;
            this.repositoryItemTextEditCurrency2.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency2.Name = "repositoryItemTextEditCurrency2";
            // 
            // colYearlyPercentageIncrease3
            // 
            this.colYearlyPercentageIncrease3.Caption = "Yearly % Increase";
            this.colYearlyPercentageIncrease3.ColumnEdit = this.repositoryItemTextEditPercentage2;
            this.colYearlyPercentageIncrease3.FieldName = "YearlyPercentageIncrease";
            this.colYearlyPercentageIncrease3.Name = "colYearlyPercentageIncrease3";
            this.colYearlyPercentageIncrease3.OptionsColumn.AllowEdit = false;
            this.colYearlyPercentageIncrease3.OptionsColumn.AllowFocus = false;
            this.colYearlyPercentageIncrease3.OptionsColumn.ReadOnly = true;
            this.colYearlyPercentageIncrease3.Visible = true;
            this.colYearlyPercentageIncrease3.VisibleIndex = 11;
            this.colYearlyPercentageIncrease3.Width = 110;
            // 
            // repositoryItemTextEditPercentage2
            // 
            this.repositoryItemTextEditPercentage2.AutoHeight = false;
            this.repositoryItemTextEditPercentage2.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage2.Name = "repositoryItemTextEditPercentage2";
            // 
            // colLastClientPaymentDate2
            // 
            this.colLastClientPaymentDate2.Caption = "Last Client Payment";
            this.colLastClientPaymentDate2.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colLastClientPaymentDate2.FieldName = "LastClientPaymentDate";
            this.colLastClientPaymentDate2.Name = "colLastClientPaymentDate2";
            this.colLastClientPaymentDate2.OptionsColumn.AllowEdit = false;
            this.colLastClientPaymentDate2.OptionsColumn.AllowFocus = false;
            this.colLastClientPaymentDate2.OptionsColumn.ReadOnly = true;
            this.colLastClientPaymentDate2.Width = 116;
            // 
            // colRemarks4
            // 
            this.colRemarks4.Caption = "Remarks";
            this.colRemarks4.ColumnEdit = this.repositoryItemMemoExEdit6;
            this.colRemarks4.FieldName = "Remarks";
            this.colRemarks4.Name = "colRemarks4";
            this.colRemarks4.OptionsColumn.ReadOnly = true;
            this.colRemarks4.Visible = true;
            this.colRemarks4.VisibleIndex = 12;
            // 
            // repositoryItemMemoExEdit6
            // 
            this.repositoryItemMemoExEdit6.AutoHeight = false;
            this.repositoryItemMemoExEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit6.Name = "repositoryItemMemoExEdit6";
            this.repositoryItemMemoExEdit6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit6.ShowIcon = false;
            // 
            // colClientID2
            // 
            this.colClientID2.Caption = "Client ID";
            this.colClientID2.FieldName = "ClientID";
            this.colClientID2.Name = "colClientID2";
            this.colClientID2.OptionsColumn.AllowEdit = false;
            this.colClientID2.OptionsColumn.AllowFocus = false;
            this.colClientID2.OptionsColumn.ReadOnly = true;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.Visible = true;
            this.colSiteName.VisibleIndex = 1;
            this.colSiteName.Width = 255;
            // 
            // colLinkedToParent3
            // 
            this.colLinkedToParent3.Caption = "Linked To Contract";
            this.colLinkedToParent3.FieldName = "LinkedToParent";
            this.colLinkedToParent3.Name = "colLinkedToParent3";
            this.colLinkedToParent3.OptionsColumn.AllowEdit = false;
            this.colLinkedToParent3.OptionsColumn.AllowFocus = false;
            this.colLinkedToParent3.OptionsColumn.ReadOnly = true;
            this.colLinkedToParent3.Visible = true;
            this.colLinkedToParent3.VisibleIndex = 10;
            this.colLinkedToParent3.Width = 273;
            // 
            // colVisitCount
            // 
            this.colVisitCount.Caption = "Existing Visit Count";
            this.colVisitCount.ColumnEdit = this.repositoryItemTextEditInteger2;
            this.colVisitCount.FieldName = "VisitCount";
            this.colVisitCount.Name = "colVisitCount";
            this.colVisitCount.OptionsColumn.AllowEdit = false;
            this.colVisitCount.OptionsColumn.AllowFocus = false;
            this.colVisitCount.OptionsColumn.ReadOnly = true;
            this.colVisitCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VisitCount", "{0:n0}")});
            this.colVisitCount.Visible = true;
            this.colVisitCount.VisibleIndex = 13;
            this.colVisitCount.Width = 112;
            // 
            // repositoryItemTextEditInteger2
            // 
            this.repositoryItemTextEditInteger2.AutoHeight = false;
            this.repositoryItemTextEditInteger2.Mask.EditMask = "n0";
            this.repositoryItemTextEditInteger2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditInteger2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditInteger2.Name = "repositoryItemTextEditInteger2";
            // 
            // colVisitNumberMax
            // 
            this.colVisitNumberMax.Caption = "Current Max Visit #";
            this.colVisitNumberMax.ColumnEdit = this.repositoryItemTextEditInteger2;
            this.colVisitNumberMax.FieldName = "VisitNumberMax";
            this.colVisitNumberMax.Name = "colVisitNumberMax";
            this.colVisitNumberMax.OptionsColumn.AllowEdit = false;
            this.colVisitNumberMax.OptionsColumn.AllowFocus = false;
            this.colVisitNumberMax.OptionsColumn.ReadOnly = true;
            this.colVisitNumberMax.Visible = true;
            this.colVisitNumberMax.VisibleIndex = 14;
            this.colVisitNumberMax.Width = 114;
            // 
            // colOnHold
            // 
            this.colOnHold.Caption = "On-Hold";
            this.colOnHold.ColumnEdit = this.repositoryItemCheckEdit7;
            this.colOnHold.FieldName = "OnHold";
            this.colOnHold.Name = "colOnHold";
            this.colOnHold.OptionsColumn.AllowEdit = false;
            this.colOnHold.OptionsColumn.AllowFocus = false;
            this.colOnHold.OptionsColumn.ReadOnly = true;
            this.colOnHold.Visible = true;
            this.colOnHold.VisibleIndex = 5;
            this.colOnHold.Width = 58;
            // 
            // repositoryItemCheckEdit7
            // 
            this.repositoryItemCheckEdit7.AutoHeight = false;
            this.repositoryItemCheckEdit7.Name = "repositoryItemCheckEdit7";
            this.repositoryItemCheckEdit7.ValueChecked = 1;
            this.repositoryItemCheckEdit7.ValueUnchecked = 0;
            // 
            // colOnHoldReasonID
            // 
            this.colOnHoldReasonID.Caption = "On-Hold Reason ID";
            this.colOnHoldReasonID.FieldName = "OnHoldReasonID";
            this.colOnHoldReasonID.Name = "colOnHoldReasonID";
            this.colOnHoldReasonID.OptionsColumn.AllowEdit = false;
            this.colOnHoldReasonID.OptionsColumn.AllowFocus = false;
            this.colOnHoldReasonID.OptionsColumn.ReadOnly = true;
            this.colOnHoldReasonID.Width = 111;
            // 
            // colOnHoldStartDate
            // 
            this.colOnHoldStartDate.Caption = "On-Hold Start";
            this.colOnHoldStartDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colOnHoldStartDate.FieldName = "OnHoldStartDate";
            this.colOnHoldStartDate.Name = "colOnHoldStartDate";
            this.colOnHoldStartDate.OptionsColumn.AllowEdit = false;
            this.colOnHoldStartDate.OptionsColumn.AllowFocus = false;
            this.colOnHoldStartDate.OptionsColumn.ReadOnly = true;
            this.colOnHoldStartDate.Visible = true;
            this.colOnHoldStartDate.VisibleIndex = 7;
            this.colOnHoldStartDate.Width = 100;
            // 
            // colOnHoldEndDate
            // 
            this.colOnHoldEndDate.Caption = "On-Hold End";
            this.colOnHoldEndDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colOnHoldEndDate.FieldName = "OnHoldEndDate";
            this.colOnHoldEndDate.Name = "colOnHoldEndDate";
            this.colOnHoldEndDate.OptionsColumn.AllowEdit = false;
            this.colOnHoldEndDate.OptionsColumn.AllowFocus = false;
            this.colOnHoldEndDate.OptionsColumn.ReadOnly = true;
            this.colOnHoldEndDate.Visible = true;
            this.colOnHoldEndDate.VisibleIndex = 8;
            this.colOnHoldEndDate.Width = 100;
            // 
            // colSiteCategory
            // 
            this.colSiteCategory.Caption = "Site Category";
            this.colSiteCategory.FieldName = "SiteCategory";
            this.colSiteCategory.Name = "colSiteCategory";
            this.colSiteCategory.OptionsColumn.AllowEdit = false;
            this.colSiteCategory.OptionsColumn.AllowFocus = false;
            this.colSiteCategory.OptionsColumn.ReadOnly = true;
            this.colSiteCategory.Visible = true;
            this.colSiteCategory.VisibleIndex = 9;
            this.colSiteCategory.Width = 111;
            // 
            // colDefaultJobCollectionTemplate
            // 
            this.colDefaultJobCollectionTemplate.Caption = "Default Job Collection Template";
            this.colDefaultJobCollectionTemplate.FieldName = "DefaultJobCollectionTemplate";
            this.colDefaultJobCollectionTemplate.Name = "colDefaultJobCollectionTemplate";
            this.colDefaultJobCollectionTemplate.OptionsColumn.AllowEdit = false;
            this.colDefaultJobCollectionTemplate.OptionsColumn.AllowFocus = false;
            this.colDefaultJobCollectionTemplate.OptionsColumn.ReadOnly = true;
            this.colDefaultJobCollectionTemplate.Visible = true;
            this.colDefaultJobCollectionTemplate.VisibleIndex = 15;
            this.colDefaultJobCollectionTemplate.Width = 170;
            // 
            // colDefaultJobCollectionTemplateID
            // 
            this.colDefaultJobCollectionTemplateID.Caption = "Default Job Collection Template ID";
            this.colDefaultJobCollectionTemplateID.FieldName = "DefaultJobCollectionTemplateID";
            this.colDefaultJobCollectionTemplateID.Name = "colDefaultJobCollectionTemplateID";
            this.colDefaultJobCollectionTemplateID.OptionsColumn.AllowEdit = false;
            this.colDefaultJobCollectionTemplateID.OptionsColumn.AllowFocus = false;
            this.colDefaultJobCollectionTemplateID.OptionsColumn.ReadOnly = true;
            this.colDefaultJobCollectionTemplateID.Width = 184;
            // 
            // panelControl4
            // 
            this.panelControl4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl4.Controls.Add(this.pictureEdit5);
            this.panelControl4.Controls.Add(this.labelControl7);
            this.panelControl4.Controls.Add(this.labelControl8);
            this.panelControl4.Location = new System.Drawing.Point(7, 6);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(1200, 48);
            this.panelControl4.TabIndex = 15;
            // 
            // pictureEdit5
            // 
            this.pictureEdit5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit5.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit5.Location = new System.Drawing.Point(1156, 4);
            this.pictureEdit5.MenuManager = this.barManager1;
            this.pictureEdit5.Name = "pictureEdit5";
            this.pictureEdit5.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit5.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit5.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit5.Properties.ReadOnly = true;
            this.pictureEdit5.Properties.ShowMenu = false;
            this.pictureEdit5.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit5.TabIndex = 9;
            // 
            // labelControl7
            // 
            this.labelControl7.AllowHtmlString = true;
            this.labelControl7.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Appearance.Options.UseBackColor = true;
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Location = new System.Drawing.Point(5, 5);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(316, 16);
            this.labelControl7.TabIndex = 6;
            this.labelControl7.Text = "<b>Step 2:</b> Select the Site Contract(s) to create Job(s) for";
            // 
            // labelControl8
            // 
            this.labelControl8.AllowHtmlString = true;
            this.labelControl8.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl8.Appearance.Options.UseBackColor = true;
            this.labelControl8.Location = new System.Drawing.Point(57, 29);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(447, 13);
            this.labelControl8.TabIndex = 7;
            this.labelControl8.Text = "Select one or more Site Contracts to create Jobs for by <b>ticking</b> them. Clic" +
    "k Next when done.";
            // 
            // btnStep2Next
            // 
            this.btnStep2Next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep2Next.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.btnStep2Next.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnStep2Next.Location = new System.Drawing.Point(1119, 501);
            this.btnStep2Next.Name = "btnStep2Next";
            this.btnStep2Next.Size = new System.Drawing.Size(88, 30);
            this.btnStep2Next.TabIndex = 0;
            this.btnStep2Next.Text = "Next";
            this.btnStep2Next.Click += new System.EventHandler(this.btnStep2Next_Click);
            // 
            // btnStep2Previous
            // 
            this.btnStep2Previous.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep2Previous.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnStep2Previous.Location = new System.Drawing.Point(1025, 501);
            this.btnStep2Previous.Name = "btnStep2Previous";
            this.btnStep2Previous.Size = new System.Drawing.Size(88, 30);
            this.btnStep2Previous.TabIndex = 10;
            this.btnStep2Previous.Text = "Previous";
            this.btnStep2Previous.Click += new System.EventHandler(this.btnStep2Previous_Click);
            // 
            // xtraTabPageStep3
            // 
            this.xtraTabPageStep3.Controls.Add(this.standaloneBarDockControl6);
            this.xtraTabPageStep3.Controls.Add(this.labelControlSelectedVisitCount);
            this.xtraTabPageStep3.Controls.Add(this.btnStep3Next);
            this.xtraTabPageStep3.Controls.Add(this.gridControl3);
            this.xtraTabPageStep3.Controls.Add(this.btnStep3Previous);
            this.xtraTabPageStep3.Controls.Add(this.panelControl8);
            this.xtraTabPageStep3.Name = "xtraTabPageStep3";
            this.xtraTabPageStep3.Size = new System.Drawing.Size(1213, 537);
            this.xtraTabPageStep3.Tag = "3";
            this.xtraTabPageStep3.Text = "Step 3";
            // 
            // standaloneBarDockControl6
            // 
            this.standaloneBarDockControl6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl6.CausesValidation = false;
            this.standaloneBarDockControl6.Location = new System.Drawing.Point(7, 62);
            this.standaloneBarDockControl6.Manager = this.barManager1;
            this.standaloneBarDockControl6.Name = "standaloneBarDockControl6";
            this.standaloneBarDockControl6.Size = new System.Drawing.Size(1200, 26);
            this.standaloneBarDockControl6.Text = "standaloneBarDockControl6";
            // 
            // labelControlSelectedVisitCount
            // 
            this.labelControlSelectedVisitCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControlSelectedVisitCount.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControlSelectedVisitCount.Appearance.ImageIndex = 11;
            this.labelControlSelectedVisitCount.Appearance.ImageList = this.imageCollection1;
            this.labelControlSelectedVisitCount.Appearance.Options.UseImageAlign = true;
            this.labelControlSelectedVisitCount.Appearance.Options.UseImageIndex = true;
            this.labelControlSelectedVisitCount.Appearance.Options.UseImageList = true;
            this.labelControlSelectedVisitCount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControlSelectedVisitCount.Location = new System.Drawing.Point(7, 503);
            this.labelControlSelectedVisitCount.Name = "labelControlSelectedVisitCount";
            this.labelControlSelectedVisitCount.Size = new System.Drawing.Size(188, 17);
            this.labelControlSelectedVisitCount.TabIndex = 24;
            this.labelControlSelectedVisitCount.Text = "       0 Selected Visit(s)";
            // 
            // btnStep3Next
            // 
            this.btnStep3Next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep3Next.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.btnStep3Next.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnStep3Next.Location = new System.Drawing.Point(1119, 501);
            this.btnStep3Next.Name = "btnStep3Next";
            this.btnStep3Next.Size = new System.Drawing.Size(88, 30);
            this.btnStep3Next.TabIndex = 17;
            this.btnStep3Next.Text = "Next";
            this.btnStep3Next.Click += new System.EventHandler(this.btnStep3Next_Click);
            // 
            // gridControl3
            // 
            this.gridControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl3.DataSource = this.sp06158OMJobWizardVisitsForSiteContractBindingSource;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl3.Location = new System.Drawing.Point(7, 88);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit8,
            this.repositoryItemTextEditDateTime4,
            this.repositoryItemTextEditCurrency4,
            this.repositoryItemTextEditLatLong,
            this.repositoryItemTextEditInteger4});
            this.gridControl3.Size = new System.Drawing.Size(1200, 407);
            this.gridControl3.TabIndex = 23;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp06158OMJobWizardVisitsForSiteContractBindingSource
            // 
            this.sp06158OMJobWizardVisitsForSiteContractBindingSource.DataMember = "sp06158_OM_Job_Wizard_Visits_For_Site_Contract";
            this.sp06158OMJobWizardVisitsForSiteContractBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colVisitID1,
            this.colSiteContractID1,
            this.colVisitNumber1,
            this.colCreatedByStaffID1,
            this.colExpectedStartDate1,
            this.colExpectedEndDate1,
            this.colStartDate2,
            this.colEndDate2,
            this.colCostCalculationLevel,
            this.colVisitCost1,
            this.colVisitSell1,
            this.colStartLatitude1,
            this.colStartLongitude1,
            this.colFinishLatitude1,
            this.colFinishLongitude1,
            this.colRemarks2,
            this.colCostCalculationLevelDescription,
            this.colCreatedByStaffName1,
            this.colClientContractID2,
            this.colSiteID1,
            this.colClientID1,
            this.colSiteName2,
            this.colClientName2,
            this.colContractDescription2,
            this.colLinkedToParent,
            this.colJobCount,
            this.colLocationX1,
            this.colLocationY1,
            this.colSitePostcode1,
            this.colVisitCategory,
            this.colVisitCategoryID,
            this.colClientPOID1,
            this.colClientPONumber1,
            this.colSellCalculationLevel,
            this.colSellCalculationLevelDescription,
            this.colCreatedFromVisitTemplate,
            this.colCreatedFromVisitTemplateID,
            this.colLastVisitStartDate,
            this.colVisitStatus,
            this.colVisitStatusID,
            this.colVisitType,
            this.colVisitTypeID});
            gridFormatRule4.ApplyToRow = true;
            gridFormatRule4.Column = this.colVisitStatusID;
            gridFormatRule4.Name = "FormatOnHold";
            formatConditionRuleValue4.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue4.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue4.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue4.Value1 = 55;
            gridFormatRule4.Rule = formatConditionRuleValue4;
            this.gridView3.FormatRules.Add(gridFormatRule4);
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.GroupCount = 2;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsFind.AlwaysVisible = true;
            this.gridView3.OptionsFind.FindDelay = 2000;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsLayout.StoreFormatRules = true;
            this.gridView3.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.MultiSelect = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToParent, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colVisitNumber1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView3.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView3.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView3.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView3.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView3_MouseUp);
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colVisitID1
            // 
            this.colVisitID1.Caption = "Visit ID";
            this.colVisitID1.FieldName = "VisitID";
            this.colVisitID1.Name = "colVisitID1";
            this.colVisitID1.OptionsColumn.AllowEdit = false;
            this.colVisitID1.OptionsColumn.AllowFocus = false;
            this.colVisitID1.OptionsColumn.ReadOnly = true;
            // 
            // colSiteContractID1
            // 
            this.colSiteContractID1.Caption = "Site Contract ID";
            this.colSiteContractID1.FieldName = "SiteContractID";
            this.colSiteContractID1.Name = "colSiteContractID1";
            this.colSiteContractID1.OptionsColumn.AllowEdit = false;
            this.colSiteContractID1.OptionsColumn.AllowFocus = false;
            this.colSiteContractID1.OptionsColumn.ReadOnly = true;
            this.colSiteContractID1.Width = 98;
            // 
            // colVisitNumber1
            // 
            this.colVisitNumber1.Caption = "Visit #";
            this.colVisitNumber1.FieldName = "VisitNumber";
            this.colVisitNumber1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colVisitNumber1.Name = "colVisitNumber1";
            this.colVisitNumber1.OptionsColumn.AllowEdit = false;
            this.colVisitNumber1.OptionsColumn.AllowFocus = false;
            this.colVisitNumber1.OptionsColumn.ReadOnly = true;
            this.colVisitNumber1.Visible = true;
            this.colVisitNumber1.VisibleIndex = 0;
            this.colVisitNumber1.Width = 74;
            // 
            // colCreatedByStaffID1
            // 
            this.colCreatedByStaffID1.Caption = "Created By Staff ID";
            this.colCreatedByStaffID1.FieldName = "CreatedByStaffID";
            this.colCreatedByStaffID1.Name = "colCreatedByStaffID1";
            this.colCreatedByStaffID1.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffID1.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffID1.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffID1.Width = 116;
            // 
            // colExpectedStartDate1
            // 
            this.colExpectedStartDate1.Caption = "Expected Start";
            this.colExpectedStartDate1.ColumnEdit = this.repositoryItemTextEditDateTime4;
            this.colExpectedStartDate1.FieldName = "ExpectedStartDate";
            this.colExpectedStartDate1.Name = "colExpectedStartDate1";
            this.colExpectedStartDate1.OptionsColumn.AllowEdit = false;
            this.colExpectedStartDate1.OptionsColumn.AllowFocus = false;
            this.colExpectedStartDate1.OptionsColumn.ReadOnly = true;
            this.colExpectedStartDate1.Visible = true;
            this.colExpectedStartDate1.VisibleIndex = 2;
            this.colExpectedStartDate1.Width = 100;
            // 
            // repositoryItemTextEditDateTime4
            // 
            this.repositoryItemTextEditDateTime4.AutoHeight = false;
            this.repositoryItemTextEditDateTime4.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime4.Name = "repositoryItemTextEditDateTime4";
            // 
            // colExpectedEndDate1
            // 
            this.colExpectedEndDate1.Caption = "Expected End";
            this.colExpectedEndDate1.ColumnEdit = this.repositoryItemTextEditDateTime4;
            this.colExpectedEndDate1.FieldName = "ExpectedEndDate";
            this.colExpectedEndDate1.Name = "colExpectedEndDate1";
            this.colExpectedEndDate1.OptionsColumn.AllowEdit = false;
            this.colExpectedEndDate1.OptionsColumn.AllowFocus = false;
            this.colExpectedEndDate1.OptionsColumn.ReadOnly = true;
            this.colExpectedEndDate1.Visible = true;
            this.colExpectedEndDate1.VisibleIndex = 3;
            this.colExpectedEndDate1.Width = 100;
            // 
            // colStartDate2
            // 
            this.colStartDate2.Caption = "Start";
            this.colStartDate2.ColumnEdit = this.repositoryItemTextEditDateTime4;
            this.colStartDate2.FieldName = "StartDate";
            this.colStartDate2.Name = "colStartDate2";
            this.colStartDate2.OptionsColumn.AllowEdit = false;
            this.colStartDate2.OptionsColumn.AllowFocus = false;
            this.colStartDate2.OptionsColumn.ReadOnly = true;
            this.colStartDate2.Visible = true;
            this.colStartDate2.VisibleIndex = 4;
            this.colStartDate2.Width = 100;
            // 
            // colEndDate2
            // 
            this.colEndDate2.Caption = "End";
            this.colEndDate2.ColumnEdit = this.repositoryItemTextEditDateTime4;
            this.colEndDate2.FieldName = "EndDate";
            this.colEndDate2.Name = "colEndDate2";
            this.colEndDate2.OptionsColumn.AllowEdit = false;
            this.colEndDate2.OptionsColumn.AllowFocus = false;
            this.colEndDate2.OptionsColumn.ReadOnly = true;
            this.colEndDate2.Visible = true;
            this.colEndDate2.VisibleIndex = 5;
            this.colEndDate2.Width = 100;
            // 
            // colCostCalculationLevel
            // 
            this.colCostCalculationLevel.Caption = "Cost Calculation ID";
            this.colCostCalculationLevel.FieldName = "CostCalculationLevel";
            this.colCostCalculationLevel.Name = "colCostCalculationLevel";
            this.colCostCalculationLevel.OptionsColumn.AllowEdit = false;
            this.colCostCalculationLevel.OptionsColumn.AllowFocus = false;
            this.colCostCalculationLevel.OptionsColumn.ReadOnly = true;
            this.colCostCalculationLevel.Width = 140;
            // 
            // colVisitCost1
            // 
            this.colVisitCost1.Caption = "Visit Cost";
            this.colVisitCost1.ColumnEdit = this.repositoryItemTextEditCurrency4;
            this.colVisitCost1.FieldName = "VisitCost";
            this.colVisitCost1.Name = "colVisitCost1";
            this.colVisitCost1.OptionsColumn.AllowEdit = false;
            this.colVisitCost1.OptionsColumn.AllowFocus = false;
            this.colVisitCost1.OptionsColumn.ReadOnly = true;
            this.colVisitCost1.Visible = true;
            this.colVisitCost1.VisibleIndex = 9;
            // 
            // repositoryItemTextEditCurrency4
            // 
            this.repositoryItemTextEditCurrency4.AutoHeight = false;
            this.repositoryItemTextEditCurrency4.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency4.Name = "repositoryItemTextEditCurrency4";
            // 
            // colVisitSell1
            // 
            this.colVisitSell1.Caption = "Visit Sell";
            this.colVisitSell1.ColumnEdit = this.repositoryItemTextEditCurrency4;
            this.colVisitSell1.FieldName = "VisitSell";
            this.colVisitSell1.Name = "colVisitSell1";
            this.colVisitSell1.OptionsColumn.AllowEdit = false;
            this.colVisitSell1.OptionsColumn.AllowFocus = false;
            this.colVisitSell1.OptionsColumn.ReadOnly = true;
            this.colVisitSell1.Visible = true;
            this.colVisitSell1.VisibleIndex = 10;
            // 
            // colStartLatitude1
            // 
            this.colStartLatitude1.Caption = "Start Latitude";
            this.colStartLatitude1.ColumnEdit = this.repositoryItemTextEditLatLong;
            this.colStartLatitude1.FieldName = "StartLatitude";
            this.colStartLatitude1.Name = "colStartLatitude1";
            this.colStartLatitude1.OptionsColumn.AllowEdit = false;
            this.colStartLatitude1.OptionsColumn.AllowFocus = false;
            this.colStartLatitude1.OptionsColumn.ReadOnly = true;
            this.colStartLatitude1.Width = 87;
            // 
            // repositoryItemTextEditLatLong
            // 
            this.repositoryItemTextEditLatLong.AutoHeight = false;
            this.repositoryItemTextEditLatLong.Mask.EditMask = "n8";
            this.repositoryItemTextEditLatLong.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditLatLong.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditLatLong.Name = "repositoryItemTextEditLatLong";
            // 
            // colStartLongitude1
            // 
            this.colStartLongitude1.Caption = "Start Longitude";
            this.colStartLongitude1.ColumnEdit = this.repositoryItemTextEditLatLong;
            this.colStartLongitude1.FieldName = "StartLongitude";
            this.colStartLongitude1.Name = "colStartLongitude1";
            this.colStartLongitude1.OptionsColumn.AllowEdit = false;
            this.colStartLongitude1.OptionsColumn.AllowFocus = false;
            this.colStartLongitude1.OptionsColumn.ReadOnly = true;
            this.colStartLongitude1.Width = 95;
            // 
            // colFinishLatitude1
            // 
            this.colFinishLatitude1.Caption = "Finish Latitude";
            this.colFinishLatitude1.ColumnEdit = this.repositoryItemTextEditLatLong;
            this.colFinishLatitude1.FieldName = "FinishLatitude";
            this.colFinishLatitude1.Name = "colFinishLatitude1";
            this.colFinishLatitude1.OptionsColumn.AllowEdit = false;
            this.colFinishLatitude1.OptionsColumn.AllowFocus = false;
            this.colFinishLatitude1.OptionsColumn.ReadOnly = true;
            this.colFinishLatitude1.Width = 90;
            // 
            // colFinishLongitude1
            // 
            this.colFinishLongitude1.Caption = "Finish Longitude";
            this.colFinishLongitude1.ColumnEdit = this.repositoryItemTextEditLatLong;
            this.colFinishLongitude1.FieldName = "FinishLongitude";
            this.colFinishLongitude1.Name = "colFinishLongitude1";
            this.colFinishLongitude1.OptionsColumn.AllowEdit = false;
            this.colFinishLongitude1.OptionsColumn.AllowFocus = false;
            this.colFinishLongitude1.OptionsColumn.ReadOnly = true;
            this.colFinishLongitude1.Width = 98;
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit8;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.ReadOnly = true;
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 11;
            // 
            // repositoryItemMemoExEdit8
            // 
            this.repositoryItemMemoExEdit8.AutoHeight = false;
            this.repositoryItemMemoExEdit8.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit8.Name = "repositoryItemMemoExEdit8";
            this.repositoryItemMemoExEdit8.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit8.ShowIcon = false;
            // 
            // colCostCalculationLevelDescription
            // 
            this.colCostCalculationLevelDescription.Caption = "Cost Calculation";
            this.colCostCalculationLevelDescription.FieldName = "CostCalculationLevelDescription";
            this.colCostCalculationLevelDescription.Name = "colCostCalculationLevelDescription";
            this.colCostCalculationLevelDescription.OptionsColumn.AllowEdit = false;
            this.colCostCalculationLevelDescription.OptionsColumn.AllowFocus = false;
            this.colCostCalculationLevelDescription.OptionsColumn.ReadOnly = true;
            this.colCostCalculationLevelDescription.Width = 100;
            // 
            // colCreatedByStaffName1
            // 
            this.colCreatedByStaffName1.Caption = "Created By";
            this.colCreatedByStaffName1.FieldName = "CreatedByStaffName";
            this.colCreatedByStaffName1.Name = "colCreatedByStaffName1";
            this.colCreatedByStaffName1.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffName1.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffName1.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffName1.Visible = true;
            this.colCreatedByStaffName1.VisibleIndex = 12;
            this.colCreatedByStaffName1.Width = 109;
            // 
            // colClientContractID2
            // 
            this.colClientContractID2.Caption = "Client Contract ID";
            this.colClientContractID2.FieldName = "ClientContractID";
            this.colClientContractID2.Name = "colClientContractID2";
            this.colClientContractID2.OptionsColumn.AllowEdit = false;
            this.colClientContractID2.OptionsColumn.AllowFocus = false;
            this.colClientContractID2.OptionsColumn.ReadOnly = true;
            this.colClientContractID2.Width = 107;
            // 
            // colSiteID1
            // 
            this.colSiteID1.Caption = "Site ID";
            this.colSiteID1.FieldName = "SiteID";
            this.colSiteID1.Name = "colSiteID1";
            this.colSiteID1.OptionsColumn.AllowEdit = false;
            this.colSiteID1.OptionsColumn.AllowFocus = false;
            this.colSiteID1.OptionsColumn.ReadOnly = true;
            this.colSiteID1.Visible = true;
            this.colSiteID1.VisibleIndex = 1;
            this.colSiteID1.Width = 51;
            // 
            // colClientID1
            // 
            this.colClientID1.Caption = "Client ID";
            this.colClientID1.FieldName = "ClientID";
            this.colClientID1.Name = "colClientID1";
            this.colClientID1.OptionsColumn.AllowEdit = false;
            this.colClientID1.OptionsColumn.AllowFocus = false;
            this.colClientID1.OptionsColumn.ReadOnly = true;
            // 
            // colSiteName2
            // 
            this.colSiteName2.Caption = "Site Name";
            this.colSiteName2.FieldName = "SiteName";
            this.colSiteName2.Name = "colSiteName2";
            this.colSiteName2.OptionsColumn.AllowEdit = false;
            this.colSiteName2.OptionsColumn.AllowFocus = false;
            this.colSiteName2.OptionsColumn.ReadOnly = true;
            this.colSiteName2.Visible = true;
            this.colSiteName2.VisibleIndex = 16;
            this.colSiteName2.Width = 242;
            // 
            // colClientName2
            // 
            this.colClientName2.Caption = "Client Name";
            this.colClientName2.FieldName = "ClientName";
            this.colClientName2.Name = "colClientName2";
            this.colClientName2.OptionsColumn.AllowEdit = false;
            this.colClientName2.OptionsColumn.AllowFocus = false;
            this.colClientName2.OptionsColumn.ReadOnly = true;
            this.colClientName2.Width = 153;
            // 
            // colContractDescription2
            // 
            this.colContractDescription2.Caption = "Contract Description";
            this.colContractDescription2.FieldName = "ContractDescription";
            this.colContractDescription2.Name = "colContractDescription2";
            this.colContractDescription2.OptionsColumn.AllowEdit = false;
            this.colContractDescription2.OptionsColumn.AllowFocus = false;
            this.colContractDescription2.OptionsColumn.ReadOnly = true;
            this.colContractDescription2.Width = 210;
            // 
            // colLinkedToParent
            // 
            this.colLinkedToParent.Caption = "Client Contract";
            this.colLinkedToParent.FieldName = "LinkedToParent";
            this.colLinkedToParent.Name = "colLinkedToParent";
            this.colLinkedToParent.OptionsColumn.AllowEdit = false;
            this.colLinkedToParent.OptionsColumn.AllowFocus = false;
            this.colLinkedToParent.OptionsColumn.ReadOnly = true;
            this.colLinkedToParent.Visible = true;
            this.colLinkedToParent.VisibleIndex = 12;
            this.colLinkedToParent.Width = 415;
            // 
            // colJobCount
            // 
            this.colJobCount.Caption = "Existing Job Count";
            this.colJobCount.ColumnEdit = this.repositoryItemTextEditInteger4;
            this.colJobCount.FieldName = "JobCount";
            this.colJobCount.Name = "colJobCount";
            this.colJobCount.OptionsColumn.AllowEdit = false;
            this.colJobCount.OptionsColumn.AllowFocus = false;
            this.colJobCount.OptionsColumn.ReadOnly = true;
            this.colJobCount.Visible = true;
            this.colJobCount.VisibleIndex = 13;
            this.colJobCount.Width = 110;
            // 
            // repositoryItemTextEditInteger4
            // 
            this.repositoryItemTextEditInteger4.AutoHeight = false;
            this.repositoryItemTextEditInteger4.Mask.EditMask = "n0";
            this.repositoryItemTextEditInteger4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditInteger4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditInteger4.Name = "repositoryItemTextEditInteger4";
            // 
            // colLocationX1
            // 
            this.colLocationX1.Caption = "Site Latitude";
            this.colLocationX1.ColumnEdit = this.repositoryItemTextEditLatLong;
            this.colLocationX1.FieldName = "LocationX";
            this.colLocationX1.Name = "colLocationX1";
            this.colLocationX1.OptionsColumn.AllowEdit = false;
            this.colLocationX1.OptionsColumn.AllowFocus = false;
            this.colLocationX1.OptionsColumn.ReadOnly = true;
            this.colLocationX1.Width = 81;
            // 
            // colLocationY1
            // 
            this.colLocationY1.Caption = "Site Longitude";
            this.colLocationY1.ColumnEdit = this.repositoryItemTextEditLatLong;
            this.colLocationY1.FieldName = "LocationY";
            this.colLocationY1.Name = "colLocationY1";
            this.colLocationY1.OptionsColumn.AllowEdit = false;
            this.colLocationY1.OptionsColumn.AllowFocus = false;
            this.colLocationY1.OptionsColumn.ReadOnly = true;
            this.colLocationY1.Width = 89;
            // 
            // colSitePostcode1
            // 
            this.colSitePostcode1.Caption = "Site Postcode";
            this.colSitePostcode1.FieldName = "SitePostcode";
            this.colSitePostcode1.Name = "colSitePostcode1";
            this.colSitePostcode1.OptionsColumn.AllowEdit = false;
            this.colSitePostcode1.OptionsColumn.AllowFocus = false;
            this.colSitePostcode1.OptionsColumn.ReadOnly = true;
            this.colSitePostcode1.Width = 86;
            // 
            // colVisitCategory
            // 
            this.colVisitCategory.Caption = "Visit Category";
            this.colVisitCategory.FieldName = "VisitCategory";
            this.colVisitCategory.Name = "colVisitCategory";
            this.colVisitCategory.OptionsColumn.AllowEdit = false;
            this.colVisitCategory.OptionsColumn.AllowFocus = false;
            this.colVisitCategory.OptionsColumn.ReadOnly = true;
            this.colVisitCategory.Visible = true;
            this.colVisitCategory.VisibleIndex = 8;
            this.colVisitCategory.Width = 108;
            // 
            // colVisitCategoryID
            // 
            this.colVisitCategoryID.Caption = "Visit Category ID";
            this.colVisitCategoryID.FieldName = "VisitCategoryID";
            this.colVisitCategoryID.Name = "colVisitCategoryID";
            this.colVisitCategoryID.OptionsColumn.AllowEdit = false;
            this.colVisitCategoryID.OptionsColumn.AllowFocus = false;
            this.colVisitCategoryID.OptionsColumn.ReadOnly = true;
            this.colVisitCategoryID.Width = 102;
            // 
            // colClientPOID1
            // 
            this.colClientPOID1.Caption = "Client PO ID";
            this.colClientPOID1.FieldName = "ClientPOID";
            this.colClientPOID1.Name = "colClientPOID1";
            this.colClientPOID1.OptionsColumn.AllowEdit = false;
            this.colClientPOID1.OptionsColumn.AllowFocus = false;
            this.colClientPOID1.OptionsColumn.ReadOnly = true;
            this.colClientPOID1.Width = 77;
            // 
            // colClientPONumber1
            // 
            this.colClientPONumber1.Caption = "Client PO #";
            this.colClientPONumber1.FieldName = "ClientPONumber";
            this.colClientPONumber1.Name = "colClientPONumber1";
            this.colClientPONumber1.OptionsColumn.AllowEdit = false;
            this.colClientPONumber1.OptionsColumn.AllowFocus = false;
            this.colClientPONumber1.OptionsColumn.ReadOnly = true;
            this.colClientPONumber1.Visible = true;
            this.colClientPONumber1.VisibleIndex = 14;
            this.colClientPONumber1.Width = 105;
            // 
            // colSellCalculationLevel
            // 
            this.colSellCalculationLevel.Caption = "Sell Calculation ID";
            this.colSellCalculationLevel.FieldName = "SellCalculationLevel";
            this.colSellCalculationLevel.Name = "colSellCalculationLevel";
            this.colSellCalculationLevel.OptionsColumn.AllowEdit = false;
            this.colSellCalculationLevel.OptionsColumn.AllowFocus = false;
            this.colSellCalculationLevel.OptionsColumn.ReadOnly = true;
            this.colSellCalculationLevel.Width = 104;
            // 
            // colSellCalculationLevelDescription
            // 
            this.colSellCalculationLevelDescription.Caption = "Sell Calculation";
            this.colSellCalculationLevelDescription.FieldName = "SellCalculationLevelDescription";
            this.colSellCalculationLevelDescription.Name = "colSellCalculationLevelDescription";
            this.colSellCalculationLevelDescription.OptionsColumn.AllowEdit = false;
            this.colSellCalculationLevelDescription.OptionsColumn.AllowFocus = false;
            this.colSellCalculationLevelDescription.OptionsColumn.ReadOnly = true;
            this.colSellCalculationLevelDescription.Width = 100;
            // 
            // colCreatedFromVisitTemplate
            // 
            this.colCreatedFromVisitTemplate.Caption = "Created From Visit Template";
            this.colCreatedFromVisitTemplate.FieldName = "CreatedFromVisitTemplate";
            this.colCreatedFromVisitTemplate.Name = "colCreatedFromVisitTemplate";
            this.colCreatedFromVisitTemplate.OptionsColumn.AllowEdit = false;
            this.colCreatedFromVisitTemplate.OptionsColumn.AllowFocus = false;
            this.colCreatedFromVisitTemplate.OptionsColumn.ReadOnly = true;
            this.colCreatedFromVisitTemplate.Visible = true;
            this.colCreatedFromVisitTemplate.VisibleIndex = 15;
            this.colCreatedFromVisitTemplate.Width = 195;
            // 
            // colCreatedFromVisitTemplateID
            // 
            this.colCreatedFromVisitTemplateID.Caption = "Created From Visit Template ID";
            this.colCreatedFromVisitTemplateID.FieldName = "CreatedFromVisitTemplateID";
            this.colCreatedFromVisitTemplateID.Name = "colCreatedFromVisitTemplateID";
            this.colCreatedFromVisitTemplateID.OptionsColumn.AllowEdit = false;
            this.colCreatedFromVisitTemplateID.OptionsColumn.AllowFocus = false;
            this.colCreatedFromVisitTemplateID.OptionsColumn.ReadOnly = true;
            this.colCreatedFromVisitTemplateID.Width = 168;
            // 
            // colLastVisitStartDate
            // 
            this.colLastVisitStartDate.Caption = "Last Visit Start Date";
            this.colLastVisitStartDate.ColumnEdit = this.repositoryItemTextEditDateTime4;
            this.colLastVisitStartDate.FieldName = "LastVisitStartDate";
            this.colLastVisitStartDate.Name = "colLastVisitStartDate";
            this.colLastVisitStartDate.OptionsColumn.AllowEdit = false;
            this.colLastVisitStartDate.OptionsColumn.AllowFocus = false;
            this.colLastVisitStartDate.OptionsColumn.ReadOnly = true;
            this.colLastVisitStartDate.Width = 114;
            // 
            // colVisitStatus
            // 
            this.colVisitStatus.Caption = "Visit Status";
            this.colVisitStatus.FieldName = "VisitStatus";
            this.colVisitStatus.Name = "colVisitStatus";
            this.colVisitStatus.OptionsColumn.AllowEdit = false;
            this.colVisitStatus.OptionsColumn.AllowFocus = false;
            this.colVisitStatus.OptionsColumn.ReadOnly = true;
            this.colVisitStatus.Visible = true;
            this.colVisitStatus.VisibleIndex = 6;
            this.colVisitStatus.Width = 129;
            // 
            // colVisitType
            // 
            this.colVisitType.Caption = "Visit Type";
            this.colVisitType.FieldName = "VisitType";
            this.colVisitType.Name = "colVisitType";
            this.colVisitType.OptionsColumn.AllowEdit = false;
            this.colVisitType.OptionsColumn.AllowFocus = false;
            this.colVisitType.OptionsColumn.ReadOnly = true;
            this.colVisitType.Visible = true;
            this.colVisitType.VisibleIndex = 7;
            this.colVisitType.Width = 108;
            // 
            // colVisitTypeID
            // 
            this.colVisitTypeID.Caption = "Visit Type ID";
            this.colVisitTypeID.FieldName = "VisitTypeID";
            this.colVisitTypeID.Name = "colVisitTypeID";
            this.colVisitTypeID.OptionsColumn.AllowEdit = false;
            this.colVisitTypeID.OptionsColumn.AllowFocus = false;
            this.colVisitTypeID.OptionsColumn.ReadOnly = true;
            // 
            // btnStep3Previous
            // 
            this.btnStep3Previous.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep3Previous.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnStep3Previous.Location = new System.Drawing.Point(1025, 501);
            this.btnStep3Previous.Name = "btnStep3Previous";
            this.btnStep3Previous.Size = new System.Drawing.Size(88, 30);
            this.btnStep3Previous.TabIndex = 18;
            this.btnStep3Previous.Text = "Previous";
            this.btnStep3Previous.Click += new System.EventHandler(this.btnStep3Previous_Click);
            // 
            // panelControl8
            // 
            this.panelControl8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl8.Controls.Add(this.SelectedSiteContractCountTextEdit);
            this.panelControl8.Controls.Add(this.pictureEdit10);
            this.panelControl8.Controls.Add(this.labelControl19);
            this.panelControl8.Controls.Add(this.labelControl20);
            this.panelControl8.Location = new System.Drawing.Point(7, 6);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(1200, 48);
            this.panelControl8.TabIndex = 16;
            // 
            // SelectedSiteContractCountTextEdit
            // 
            this.SelectedSiteContractCountTextEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.SelectedSiteContractCountTextEdit.Location = new System.Drawing.Point(608, 25);
            this.SelectedSiteContractCountTextEdit.MenuManager = this.barManager1;
            this.SelectedSiteContractCountTextEdit.Name = "SelectedSiteContractCountTextEdit";
            this.SelectedSiteContractCountTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.SelectedSiteContractCountTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SelectedSiteContractCountTextEdit.Properties.Mask.EditMask = "n0";
            this.SelectedSiteContractCountTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.SelectedSiteContractCountTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SelectedSiteContractCountTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SelectedSiteContractCountTextEdit, true);
            this.SelectedSiteContractCountTextEdit.Size = new System.Drawing.Size(48, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SelectedSiteContractCountTextEdit, optionsSpelling1);
            this.SelectedSiteContractCountTextEdit.TabIndex = 10;
            // 
            // pictureEdit10
            // 
            this.pictureEdit10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit10.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit10.Location = new System.Drawing.Point(1156, 4);
            this.pictureEdit10.MenuManager = this.barManager1;
            this.pictureEdit10.Name = "pictureEdit10";
            this.pictureEdit10.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit10.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit10.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit10.Properties.ReadOnly = true;
            this.pictureEdit10.Properties.ShowMenu = false;
            this.pictureEdit10.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit10.TabIndex = 9;
            // 
            // labelControl19
            // 
            this.labelControl19.AllowHtmlString = true;
            this.labelControl19.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl19.Appearance.Options.UseBackColor = true;
            this.labelControl19.Appearance.Options.UseFont = true;
            this.labelControl19.Location = new System.Drawing.Point(5, 5);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(266, 16);
            this.labelControl19.TabIndex = 6;
            this.labelControl19.Text = "<b>Step 3:</b> Select the Visit(s) to create Job(s) for\r\n";
            // 
            // labelControl20
            // 
            this.labelControl20.AllowHtmlString = true;
            this.labelControl20.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl20.Appearance.Options.UseBackColor = true;
            this.labelControl20.Location = new System.Drawing.Point(57, 29);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(544, 13);
            this.labelControl20.TabIndex = 7;
            this.labelControl20.Text = "Select one or more Visits to create Jobs for by <b>ticking</b> them. Click Next w" +
    "hen done.  <b>Selected Site Contracts:</b>";
            // 
            // xtraTabPageStep4
            // 
            this.xtraTabPageStep4.Controls.Add(this.labelControlSelectedJobCount);
            this.xtraTabPageStep4.Controls.Add(this.standaloneBarDockControl2);
            this.xtraTabPageStep4.Controls.Add(this.gridControl4);
            this.xtraTabPageStep4.Controls.Add(this.btnStep4Next);
            this.xtraTabPageStep4.Controls.Add(this.btnStep4Previous);
            this.xtraTabPageStep4.Controls.Add(this.panelControl5);
            this.xtraTabPageStep4.Name = "xtraTabPageStep4";
            this.xtraTabPageStep4.Size = new System.Drawing.Size(1213, 537);
            this.xtraTabPageStep4.Tag = "4";
            this.xtraTabPageStep4.Text = "Step 4";
            // 
            // labelControlSelectedJobCount
            // 
            this.labelControlSelectedJobCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControlSelectedJobCount.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControlSelectedJobCount.Appearance.ImageIndex = 11;
            this.labelControlSelectedJobCount.Appearance.ImageList = this.imageCollection1;
            this.labelControlSelectedJobCount.Appearance.Options.UseImageAlign = true;
            this.labelControlSelectedJobCount.Appearance.Options.UseImageIndex = true;
            this.labelControlSelectedJobCount.Appearance.Options.UseImageList = true;
            this.labelControlSelectedJobCount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControlSelectedJobCount.Location = new System.Drawing.Point(7, 503);
            this.labelControlSelectedJobCount.Name = "labelControlSelectedJobCount";
            this.labelControlSelectedJobCount.Size = new System.Drawing.Size(188, 17);
            this.labelControlSelectedJobCount.TabIndex = 25;
            this.labelControlSelectedJobCount.Text = "       0 Selected Job(s)";
            // 
            // standaloneBarDockControl2
            // 
            this.standaloneBarDockControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl2.CausesValidation = false;
            this.standaloneBarDockControl2.Location = new System.Drawing.Point(7, 62);
            this.standaloneBarDockControl2.Manager = this.barManager1;
            this.standaloneBarDockControl2.Name = "standaloneBarDockControl2";
            this.standaloneBarDockControl2.Size = new System.Drawing.Size(1200, 26);
            this.standaloneBarDockControl2.Text = "standaloneBarDockControl2";
            // 
            // gridControl4
            // 
            this.gridControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl4.DataSource = this.sp06162OMJobWizardMasterJobSubTypesAvailableBindingSource;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl4.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Reload Data", "reload")});
            this.gridControl4.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl4_EmbeddedNavigator_ButtonClick);
            this.gridControl4.Location = new System.Drawing.Point(7, 88);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2,
            this.repositoryItemTextEdit1});
            this.gridControl4.Size = new System.Drawing.Size(1200, 407);
            this.gridControl4.TabIndex = 24;
            this.gridControl4.UseEmbeddedNavigator = true;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp06162OMJobWizardMasterJobSubTypesAvailableBindingSource
            // 
            this.sp06162OMJobWizardMasterJobSubTypesAvailableBindingSource.DataMember = "sp06162_OM_Job_Wizard_Master_Job_Sub_Types_Available";
            this.sp06162OMJobWizardMasterJobSubTypesAvailableBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colJobSubTypeID,
            this.colJobTypeID,
            this.colJobSubTypeDescription,
            this.colJobSubTypeRecordOrder,
            this.colJobSubTypeRemarks,
            this.colJobTypeDescription,
            this.colJobTypeRemarks});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.GroupCount = 1;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsFind.AlwaysVisible = true;
            this.gridView4.OptionsFind.FindDelay = 2000;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsLayout.StoreFormatRules = true;
            this.gridView4.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.MultiSelect = true;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colJobTypeDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colJobSubTypeRecordOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView4.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView4.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView4.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView4.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView4.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView4_MouseUp);
            this.gridView4.GotFocus += new System.EventHandler(this.gridView4_GotFocus);
            // 
            // colJobSubTypeID
            // 
            this.colJobSubTypeID.Caption = "Job Sub-Type ID";
            this.colJobSubTypeID.FieldName = "JobSubTypeID";
            this.colJobSubTypeID.Name = "colJobSubTypeID";
            this.colJobSubTypeID.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeID.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeID.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeID.Width = 101;
            // 
            // colJobTypeID
            // 
            this.colJobTypeID.Caption = "Job Type ID";
            this.colJobTypeID.FieldName = "JobTypeID";
            this.colJobTypeID.Name = "colJobTypeID";
            this.colJobTypeID.OptionsColumn.AllowEdit = false;
            this.colJobTypeID.OptionsColumn.AllowFocus = false;
            this.colJobTypeID.OptionsColumn.ReadOnly = true;
            this.colJobTypeID.Width = 79;
            // 
            // colJobSubTypeDescription
            // 
            this.colJobSubTypeDescription.Caption = "Job Sub-Type Description";
            this.colJobSubTypeDescription.FieldName = "JobSubTypeDescription";
            this.colJobSubTypeDescription.Name = "colJobSubTypeDescription";
            this.colJobSubTypeDescription.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeDescription.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeDescription.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeDescription.Visible = true;
            this.colJobSubTypeDescription.VisibleIndex = 0;
            this.colJobSubTypeDescription.Width = 479;
            // 
            // colJobSubTypeRecordOrder
            // 
            this.colJobSubTypeRecordOrder.Caption = "Job Sub-Type Order";
            this.colJobSubTypeRecordOrder.ColumnEdit = this.repositoryItemTextEdit1;
            this.colJobSubTypeRecordOrder.FieldName = "JobSubTypeRecordOrder";
            this.colJobSubTypeRecordOrder.Name = "colJobSubTypeRecordOrder";
            this.colJobSubTypeRecordOrder.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeRecordOrder.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeRecordOrder.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeRecordOrder.Width = 131;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "n0";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colJobSubTypeRemarks
            // 
            this.colJobSubTypeRemarks.Caption = "Job Sub-Type Remarks";
            this.colJobSubTypeRemarks.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colJobSubTypeRemarks.FieldName = "JobSubTypeRemarks";
            this.colJobSubTypeRemarks.Name = "colJobSubTypeRemarks";
            this.colJobSubTypeRemarks.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeRemarks.Visible = true;
            this.colJobSubTypeRemarks.VisibleIndex = 1;
            this.colJobSubTypeRemarks.Width = 211;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colJobTypeDescription
            // 
            this.colJobTypeDescription.Caption = "Job Type Description";
            this.colJobTypeDescription.FieldName = "JobTypeDescription";
            this.colJobTypeDescription.Name = "colJobTypeDescription";
            this.colJobTypeDescription.OptionsColumn.AllowEdit = false;
            this.colJobTypeDescription.OptionsColumn.AllowFocus = false;
            this.colJobTypeDescription.OptionsColumn.ReadOnly = true;
            this.colJobTypeDescription.Visible = true;
            this.colJobTypeDescription.VisibleIndex = 2;
            this.colJobTypeDescription.Width = 362;
            // 
            // colJobTypeRemarks
            // 
            this.colJobTypeRemarks.Caption = "Job Type Remarks";
            this.colJobTypeRemarks.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colJobTypeRemarks.FieldName = "JobTypeRemarks";
            this.colJobTypeRemarks.Name = "colJobTypeRemarks";
            this.colJobTypeRemarks.OptionsColumn.ReadOnly = true;
            this.colJobTypeRemarks.Width = 109;
            // 
            // btnStep4Next
            // 
            this.btnStep4Next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep4Next.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.btnStep4Next.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnStep4Next.Location = new System.Drawing.Point(1119, 501);
            this.btnStep4Next.Name = "btnStep4Next";
            this.btnStep4Next.Size = new System.Drawing.Size(88, 30);
            this.btnStep4Next.TabIndex = 17;
            this.btnStep4Next.Text = "Next";
            this.btnStep4Next.Click += new System.EventHandler(this.btnStep4Next_Click);
            // 
            // btnStep4Previous
            // 
            this.btnStep4Previous.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep4Previous.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnStep4Previous.Location = new System.Drawing.Point(1025, 501);
            this.btnStep4Previous.Name = "btnStep4Previous";
            this.btnStep4Previous.Size = new System.Drawing.Size(88, 30);
            this.btnStep4Previous.TabIndex = 18;
            this.btnStep4Previous.Text = "Previous";
            this.btnStep4Previous.Click += new System.EventHandler(this.btnStep4Previous_Click);
            // 
            // panelControl5
            // 
            this.panelControl5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl5.Controls.Add(this.SelectedVisitCountTextEdit);
            this.panelControl5.Controls.Add(this.pictureEdit7);
            this.panelControl5.Controls.Add(this.labelControl4);
            this.panelControl5.Controls.Add(this.labelControl6);
            this.panelControl5.Location = new System.Drawing.Point(7, 6);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(1200, 48);
            this.panelControl5.TabIndex = 16;
            // 
            // SelectedVisitCountTextEdit
            // 
            this.SelectedVisitCountTextEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.SelectedVisitCountTextEdit.Location = new System.Drawing.Point(577, 25);
            this.SelectedVisitCountTextEdit.MenuManager = this.barManager1;
            this.SelectedVisitCountTextEdit.Name = "SelectedVisitCountTextEdit";
            this.SelectedVisitCountTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.SelectedVisitCountTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SelectedVisitCountTextEdit.Properties.Mask.EditMask = "n0";
            this.SelectedVisitCountTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.SelectedVisitCountTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SelectedVisitCountTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SelectedVisitCountTextEdit, true);
            this.SelectedVisitCountTextEdit.Size = new System.Drawing.Size(48, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SelectedVisitCountTextEdit, optionsSpelling2);
            this.SelectedVisitCountTextEdit.TabIndex = 11;
            // 
            // pictureEdit7
            // 
            this.pictureEdit7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit7.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit7.Location = new System.Drawing.Point(1156, 4);
            this.pictureEdit7.MenuManager = this.barManager1;
            this.pictureEdit7.Name = "pictureEdit7";
            this.pictureEdit7.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit7.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit7.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit7.Properties.ReadOnly = true;
            this.pictureEdit7.Properties.ShowMenu = false;
            this.pictureEdit7.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit7.TabIndex = 9;
            // 
            // labelControl4
            // 
            this.labelControl4.AllowHtmlString = true;
            this.labelControl4.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.Options.UseBackColor = true;
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(5, 5);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(197, 16);
            this.labelControl4.TabIndex = 6;
            this.labelControl4.Text = "<b>Step 4:</b> Select Job Type(s) to add";
            // 
            // labelControl6
            // 
            this.labelControl6.AllowHtmlString = true;
            this.labelControl6.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl6.Appearance.Options.UseBackColor = true;
            this.labelControl6.Location = new System.Drawing.Point(57, 29);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(515, 13);
            this.labelControl6.TabIndex = 7;
            this.labelControl6.Text = "Select one or more Jobs to create for each visit by <b>ticking</b> them. Click Ne" +
    "xt when done.  <b>Selected Visits:</b>\r\n";
            // 
            // xtraTabPageStep5
            // 
            this.xtraTabPageStep5.Controls.Add(this.splitContainerControl1);
            this.xtraTabPageStep5.Controls.Add(this.btnStep5Next);
            this.xtraTabPageStep5.Controls.Add(this.btnStep5Previous);
            this.xtraTabPageStep5.Controls.Add(this.panelControl6);
            this.xtraTabPageStep5.Name = "xtraTabPageStep5";
            this.xtraTabPageStep5.Size = new System.Drawing.Size(1213, 537);
            this.xtraTabPageStep5.Tag = "5";
            this.xtraTabPageStep5.Text = "Step 5";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl1.Location = new System.Drawing.Point(6, 62);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.gridControl5);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.gridControl6);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1201, 433);
            this.splitContainerControl1.SplitterPosition = 296;
            this.splitContainerControl1.TabIndex = 27;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gridControl5
            // 
            this.gridControl5.DataSource = this.sp06176OMJobWizardVisitsSelectedBindingSource;
            this.gridControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl5.Location = new System.Drawing.Point(0, 0);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4,
            this.repositoryItemTextEdit2,
            this.repositoryItemTextEdit3,
            this.repositoryItemTextEdit4,
            this.repositoryItemTextEdit5});
            this.gridControl5.Size = new System.Drawing.Size(296, 433);
            this.gridControl5.TabIndex = 24;
            this.gridControl5.UseEmbeddedNavigator = true;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp06176OMJobWizardVisitsSelectedBindingSource
            // 
            this.sp06176OMJobWizardVisitsSelectedBindingSource.DataMember = "sp06176_OM_Job_Wizard_Visits_Selected";
            this.sp06176OMJobWizardVisitsSelectedBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27,
            this.gridColumn28,
            this.gridColumn29,
            this.colLocationX2,
            this.colLocationY2,
            this.colSitePostcode2,
            this.colClientPOID2,
            this.colClientPONumber2,
            this.colVisitCategory1,
            this.colVisitCategoryID1,
            this.colSellCalculationLevel1,
            this.colSellCalculationLevelDescription1});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.GroupCount = 2;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsFind.AlwaysVisible = true;
            this.gridView5.OptionsFind.FindDelay = 2000;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsLayout.StoreFormatRules = true;
            this.gridView5.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.MultiSelect = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn28, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn25, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView5.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView5.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView5.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView5.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView5.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView5.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView5_MouseUp);
            this.gridView5.GotFocus += new System.EventHandler(this.gridView5_GotFocus);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Visit ID";
            this.gridColumn1.FieldName = "VisitID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Site Contract ID";
            this.gridColumn2.FieldName = "SiteContractID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Width = 98;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Visit #";
            this.gridColumn3.FieldName = "VisitNumber";
            this.gridColumn3.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 0;
            this.gridColumn3.Width = 74;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Created By Staff ID";
            this.gridColumn4.FieldName = "CreatedByStaffID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Width = 116;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Expected Start";
            this.gridColumn5.ColumnEdit = this.repositoryItemTextEdit2;
            this.gridColumn5.FieldName = "ExpectedStartDate";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 2;
            this.gridColumn5.Width = 100;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Mask.EditMask = "g";
            this.repositoryItemTextEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Expected End";
            this.gridColumn6.ColumnEdit = this.repositoryItemTextEdit2;
            this.gridColumn6.FieldName = "ExpectedEndDate";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 3;
            this.gridColumn6.Width = 100;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Start";
            this.gridColumn7.ColumnEdit = this.repositoryItemTextEdit2;
            this.gridColumn7.FieldName = "StartDate";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Width = 100;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "End";
            this.gridColumn8.ColumnEdit = this.repositoryItemTextEdit2;
            this.gridColumn8.FieldName = "EndDate";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Width = 100;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Cost Calculation ID";
            this.gridColumn9.FieldName = "CostCalculationLevel";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Width = 140;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Visit Cost";
            this.gridColumn13.ColumnEdit = this.repositoryItemTextEdit3;
            this.gridColumn13.FieldName = "VisitCost";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Mask.EditMask = "c";
            this.repositoryItemTextEdit3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Visit Sell";
            this.gridColumn14.ColumnEdit = this.repositoryItemTextEdit3;
            this.gridColumn14.FieldName = "VisitSell";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Start Latitude";
            this.gridColumn15.ColumnEdit = this.repositoryItemTextEdit4;
            this.gridColumn15.FieldName = "StartLatitude";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.Width = 87;
            // 
            // repositoryItemTextEdit4
            // 
            this.repositoryItemTextEdit4.AutoHeight = false;
            this.repositoryItemTextEdit4.Mask.EditMask = "n8";
            this.repositoryItemTextEdit4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit4.Name = "repositoryItemTextEdit4";
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Start Longitude";
            this.gridColumn16.ColumnEdit = this.repositoryItemTextEdit4;
            this.gridColumn16.FieldName = "StartLongitude";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowFocus = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.Width = 95;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Finish Latitude";
            this.gridColumn17.ColumnEdit = this.repositoryItemTextEdit4;
            this.gridColumn17.FieldName = "FinishLatitude";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.AllowFocus = false;
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.Width = 90;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Finish Longitude";
            this.gridColumn18.ColumnEdit = this.repositoryItemTextEdit4;
            this.gridColumn18.FieldName = "FinishLongitude";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.AllowFocus = false;
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            this.gridColumn18.Width = 98;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Remarks";
            this.gridColumn19.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.gridColumn19.FieldName = "Remarks";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 4;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Cost Calculation";
            this.gridColumn20.FieldName = "CostCalculationLevelDescription";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.AllowEdit = false;
            this.gridColumn20.OptionsColumn.AllowFocus = false;
            this.gridColumn20.OptionsColumn.ReadOnly = true;
            this.gridColumn20.Width = 100;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Created By";
            this.gridColumn21.FieldName = "CreatedByStaffName";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.AllowEdit = false;
            this.gridColumn21.OptionsColumn.AllowFocus = false;
            this.gridColumn21.OptionsColumn.ReadOnly = true;
            this.gridColumn21.Width = 109;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Client Contract ID";
            this.gridColumn22.FieldName = "ClientContractID";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.AllowEdit = false;
            this.gridColumn22.OptionsColumn.AllowFocus = false;
            this.gridColumn22.OptionsColumn.ReadOnly = true;
            this.gridColumn22.Width = 107;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Site ID";
            this.gridColumn23.FieldName = "SiteID";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.AllowEdit = false;
            this.gridColumn23.OptionsColumn.AllowFocus = false;
            this.gridColumn23.OptionsColumn.ReadOnly = true;
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 1;
            this.gridColumn23.Width = 51;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Client ID";
            this.gridColumn24.FieldName = "ClientID";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.AllowEdit = false;
            this.gridColumn24.OptionsColumn.AllowFocus = false;
            this.gridColumn24.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Site Name";
            this.gridColumn25.FieldName = "SiteName";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.OptionsColumn.AllowEdit = false;
            this.gridColumn25.OptionsColumn.AllowFocus = false;
            this.gridColumn25.OptionsColumn.ReadOnly = true;
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 6;
            this.gridColumn25.Width = 242;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Client Name";
            this.gridColumn26.FieldName = "ClientName";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.OptionsColumn.AllowEdit = false;
            this.gridColumn26.OptionsColumn.AllowFocus = false;
            this.gridColumn26.OptionsColumn.ReadOnly = true;
            this.gridColumn26.Width = 153;
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Contract Description";
            this.gridColumn27.FieldName = "ContractDescription";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.OptionsColumn.AllowEdit = false;
            this.gridColumn27.OptionsColumn.AllowFocus = false;
            this.gridColumn27.OptionsColumn.ReadOnly = true;
            this.gridColumn27.Width = 210;
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "Client Contract";
            this.gridColumn28.FieldName = "LinkedToParent";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.OptionsColumn.AllowEdit = false;
            this.gridColumn28.OptionsColumn.AllowFocus = false;
            this.gridColumn28.OptionsColumn.ReadOnly = true;
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 5;
            this.gridColumn28.Width = 415;
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "Existing Job Count";
            this.gridColumn29.ColumnEdit = this.repositoryItemTextEdit5;
            this.gridColumn29.FieldName = "JobCount";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.OptionsColumn.AllowEdit = false;
            this.gridColumn29.OptionsColumn.AllowFocus = false;
            this.gridColumn29.OptionsColumn.ReadOnly = true;
            this.gridColumn29.Width = 110;
            // 
            // repositoryItemTextEdit5
            // 
            this.repositoryItemTextEdit5.AutoHeight = false;
            this.repositoryItemTextEdit5.Mask.EditMask = "n0";
            this.repositoryItemTextEdit5.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit5.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit5.Name = "repositoryItemTextEdit5";
            // 
            // colLocationX2
            // 
            this.colLocationX2.Caption = "Site Latitude";
            this.colLocationX2.ColumnEdit = this.repositoryItemTextEdit4;
            this.colLocationX2.FieldName = "LocationX";
            this.colLocationX2.Name = "colLocationX2";
            this.colLocationX2.OptionsColumn.AllowEdit = false;
            this.colLocationX2.OptionsColumn.AllowFocus = false;
            this.colLocationX2.OptionsColumn.ReadOnly = true;
            this.colLocationX2.Width = 81;
            // 
            // colLocationY2
            // 
            this.colLocationY2.Caption = "Site Longitude";
            this.colLocationY2.ColumnEdit = this.repositoryItemTextEdit4;
            this.colLocationY2.FieldName = "LocationY";
            this.colLocationY2.Name = "colLocationY2";
            this.colLocationY2.OptionsColumn.AllowEdit = false;
            this.colLocationY2.OptionsColumn.AllowFocus = false;
            this.colLocationY2.OptionsColumn.ReadOnly = true;
            this.colLocationY2.Width = 89;
            // 
            // colSitePostcode2
            // 
            this.colSitePostcode2.Caption = "Site Postcode";
            this.colSitePostcode2.FieldName = "SitePostcode";
            this.colSitePostcode2.Name = "colSitePostcode2";
            this.colSitePostcode2.OptionsColumn.AllowEdit = false;
            this.colSitePostcode2.OptionsColumn.AllowFocus = false;
            this.colSitePostcode2.OptionsColumn.ReadOnly = true;
            this.colSitePostcode2.Width = 86;
            // 
            // colClientPOID2
            // 
            this.colClientPOID2.Caption = "Client PO ID";
            this.colClientPOID2.FieldName = "ClientPOID";
            this.colClientPOID2.Name = "colClientPOID2";
            this.colClientPOID2.OptionsColumn.AllowEdit = false;
            this.colClientPOID2.OptionsColumn.AllowFocus = false;
            this.colClientPOID2.OptionsColumn.ReadOnly = true;
            this.colClientPOID2.Width = 77;
            // 
            // colClientPONumber2
            // 
            this.colClientPONumber2.Caption = "Client PO #";
            this.colClientPONumber2.FieldName = "ClientPONumber";
            this.colClientPONumber2.Name = "colClientPONumber2";
            this.colClientPONumber2.OptionsColumn.AllowEdit = false;
            this.colClientPONumber2.OptionsColumn.AllowFocus = false;
            this.colClientPONumber2.OptionsColumn.ReadOnly = true;
            this.colClientPONumber2.Width = 149;
            // 
            // colVisitCategory1
            // 
            this.colVisitCategory1.Caption = "Visit Category";
            this.colVisitCategory1.FieldName = "VisitCategory";
            this.colVisitCategory1.Name = "colVisitCategory1";
            this.colVisitCategory1.OptionsColumn.AllowEdit = false;
            this.colVisitCategory1.OptionsColumn.AllowFocus = false;
            this.colVisitCategory1.OptionsColumn.ReadOnly = true;
            this.colVisitCategory1.Width = 130;
            // 
            // colVisitCategoryID1
            // 
            this.colVisitCategoryID1.Caption = "Visit Category ID";
            this.colVisitCategoryID1.FieldName = "VisitCategoryID";
            this.colVisitCategoryID1.Name = "colVisitCategoryID1";
            this.colVisitCategoryID1.OptionsColumn.AllowEdit = false;
            this.colVisitCategoryID1.OptionsColumn.AllowFocus = false;
            this.colVisitCategoryID1.OptionsColumn.ReadOnly = true;
            this.colVisitCategoryID1.Width = 100;
            // 
            // colSellCalculationLevel1
            // 
            this.colSellCalculationLevel1.Caption = "Sell Calculation ID";
            this.colSellCalculationLevel1.FieldName = "SellCalculationLevel";
            this.colSellCalculationLevel1.Name = "colSellCalculationLevel1";
            this.colSellCalculationLevel1.OptionsColumn.AllowEdit = false;
            this.colSellCalculationLevel1.OptionsColumn.AllowFocus = false;
            this.colSellCalculationLevel1.OptionsColumn.ReadOnly = true;
            this.colSellCalculationLevel1.Width = 104;
            // 
            // colSellCalculationLevelDescription1
            // 
            this.colSellCalculationLevelDescription1.Caption = "Sell Calculation";
            this.colSellCalculationLevelDescription1.FieldName = "SellCalculationLevelDescription";
            this.colSellCalculationLevelDescription1.Name = "colSellCalculationLevelDescription1";
            this.colSellCalculationLevelDescription1.OptionsColumn.AllowEdit = false;
            this.colSellCalculationLevelDescription1.OptionsColumn.AllowFocus = false;
            this.colSellCalculationLevelDescription1.OptionsColumn.ReadOnly = true;
            this.colSellCalculationLevelDescription1.Width = 100;
            // 
            // gridControl6
            // 
            this.gridControl6.DataSource = this.sp06174OMJobEditBindingSource;
            this.gridControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl6.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 10, true, true, "Block Add New Records", "block add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Block Edit Selected Records", "block edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl6.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl6_EmbeddedNavigator_ButtonClick);
            this.gridControl6.Location = new System.Drawing.Point(0, 0);
            this.gridControl6.MainView = this.gridView6;
            this.gridControl6.MenuManager = this.barManager1;
            this.gridControl6.Name = "gridControl6";
            this.gridControl6.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEditClientPONumber,
            this.repositoryItemDateEdit1,
            this.repositoryItemSpinEdit2DP,
            this.repositoryItemTextEditCurrency,
            this.repositoryItemTextEditPercentage,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemCheckEdit2,
            this.repositoryItemMemoExEdit3,
            this.repositoryItemTextEditInteger,
            this.repositoryItemTextEditFloat,
            this.repositoryItemButtonEditJobTypeJobSubType,
            this.repositoryItemGridLookUpEditEditDurationUnitDescriptor1,
            this.repositoryItemSpinEdit2});
            this.gridControl6.Size = new System.Drawing.Size(899, 433);
            this.gridControl6.TabIndex = 21;
            this.gridControl6.UseEmbeddedNavigator = true;
            this.gridControl6.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView6});
            // 
            // sp06174OMJobEditBindingSource
            // 
            this.sp06174OMJobEditBindingSource.DataMember = "sp06174_OM_Job_Edit";
            this.sp06174OMJobEditBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colstrMode,
            this.colstrRecordIDs,
            this.colDummyJobID,
            this.colJobID,
            this.colVisitID,
            this.colJobStatusID,
            this.colJobSubTypeID1,
            this.colCreatedByStaffID,
            this.colReactive1,
            this.colJobNoLongerRequired,
            this.colRequiresAccessPermit,
            this.colAccessPermitID,
            this.colClientPONumber,
            this.colClientPOID,
            this.colFinanceSystemPONumber,
            this.colExpectedStartDate,
            this.colExpectedEndDate,
            this.colExpectedDurationUnits,
            this.colExpectedDurationUnitsDescriptorID,
            this.colScheduleSentDate,
            this.colActualStartDate,
            this.colActualEndDate,
            this.colActualDurationUnits,
            this.colActualDurationUnitsDescriptionID,
            this.colCostTotalMaterialExVAT,
            this.colCostTotalMaterialVATRate,
            this.colCostTotalMaterialVAT,
            this.colCostTotalMaterialCost,
            this.colSellTotalMaterialExVAT,
            this.colSellTotalMaterialVATRate,
            this.colSellTotalMaterialVAT,
            this.colSellTotalMaterialCost,
            this.colCostTotalEquipmentExVAT,
            this.colCostTotalEquipmentVATRate,
            this.colCostTotalEquipmentVAT,
            this.colCostTotalEquipmentCost,
            this.colSellTotalEquipmentExVAT,
            this.colSellTotalEquipmentVATRate,
            this.colSellTotalEquipmentVAT,
            this.colSellTotalEquipmentCost,
            this.colCostTotalLabourExVAT,
            this.colCostTotalLabourVATRate,
            this.colCostTotalLabourVAT,
            this.colCostTotalLabourCost,
            this.colSellTotalLabourExVAT,
            this.colSellTotalLabourVATRate,
            this.colSellTotalLabourVAT,
            this.colSellTotalLabourCost,
            this.colCostTotalCostExVAT,
            this.colCostTotalCostVAT,
            this.colCostTotalCost,
            this.colSellTotalCostExVAT,
            this.colSellTotalCostVAT,
            this.colSellTotalCost,
            this.colClientInvoiceID,
            this.colDateClientInvoiced,
            this.colSelfBillingInvoiceID,
            this.colSelfBillingInvoiceReceivedDate,
            this.colSelfBillingInvoicePaidDate,
            this.colSelfBillingInvoiceAmountPaid,
            this.colDoNotPayContractor,
            this.colDoNotInvoiceClient,
            this.colRemarks1,
            this.colRouteOrder,
            this.colStartLatitude,
            this.colStartLongitude,
            this.colFinishLatitude,
            this.colFinishLongitude,
            this.colTenderID,
            this.colTenderJobID,
            this.colMinimumDaysFromLastVisit,
            this.colMaximumDaysFromLastVisit,
            this.colDaysLeeway,
            this.colBillingCentreCodeID1,
            this.colSiteContractID,
            this.colClientContractID,
            this.colSiteID2,
            this.colClientID3,
            this.colContractDescription1,
            this.colClientNameContractDescription,
            this.colClientName1,
            this.colSiteName1,
            this.colSitePostcode,
            this.colLocationX,
            this.colLocationY,
            this.colCreatedByStaffName,
            this.colJobSubTypeDescription1,
            this.colJobTypeDescription1,
            this.colJobTypeJobSubTypeDescription,
            this.colJobTypeID1,
            this.colVisitNumber,
            this.colLabourCount,
            this.colEquipmentCount,
            this.colMaterialCount,
            this.colDisplayOrder,
            this.colMandatory});
            this.gridView6.GridControl = this.gridControl6;
            this.gridView6.GroupCount = 3;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView6.OptionsFilter.AllowFilterEditor = false;
            this.gridView6.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView6.OptionsFilter.AllowMRUFilterList = false;
            this.gridView6.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView6.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView6.OptionsFind.FindDelay = 2000;
            this.gridView6.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView6.OptionsLayout.StoreAppearance = true;
            this.gridView6.OptionsLayout.StoreFormatRules = true;
            this.gridView6.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView6.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView6.OptionsSelection.MultiSelect = true;
            this.gridView6.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientNameContractDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colVisitNumber, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDisplayOrder, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colJobTypeJobSubTypeDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView6.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView6_CustomDrawCell);
            this.gridView6.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView6.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView6.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView6.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView6_MouseUp);
            this.gridView6.GotFocus += new System.EventHandler(this.gridView6_GotFocus);
            // 
            // colstrMode
            // 
            this.colstrMode.Caption = "Mode";
            this.colstrMode.FieldName = "strMode";
            this.colstrMode.Name = "colstrMode";
            this.colstrMode.OptionsColumn.AllowEdit = false;
            this.colstrMode.OptionsColumn.AllowFocus = false;
            this.colstrMode.OptionsColumn.ReadOnly = true;
            // 
            // colstrRecordIDs
            // 
            this.colstrRecordIDs.Caption = "Record IDs";
            this.colstrRecordIDs.FieldName = "strRecordIDs";
            this.colstrRecordIDs.Name = "colstrRecordIDs";
            this.colstrRecordIDs.OptionsColumn.AllowEdit = false;
            this.colstrRecordIDs.OptionsColumn.AllowFocus = false;
            this.colstrRecordIDs.OptionsColumn.ReadOnly = true;
            // 
            // colDummyJobID
            // 
            this.colDummyJobID.Caption = "Dummy Job ID";
            this.colDummyJobID.FieldName = "DummyJobID";
            this.colDummyJobID.Name = "colDummyJobID";
            this.colDummyJobID.OptionsColumn.AllowEdit = false;
            this.colDummyJobID.OptionsColumn.AllowFocus = false;
            this.colDummyJobID.OptionsColumn.ReadOnly = true;
            // 
            // colJobID
            // 
            this.colJobID.Caption = "Job ID";
            this.colJobID.FieldName = "JobID";
            this.colJobID.Name = "colJobID";
            this.colJobID.OptionsColumn.AllowEdit = false;
            this.colJobID.OptionsColumn.AllowFocus = false;
            this.colJobID.OptionsColumn.ReadOnly = true;
            // 
            // colVisitID
            // 
            this.colVisitID.Caption = "Visit ID";
            this.colVisitID.FieldName = "VisitID";
            this.colVisitID.Name = "colVisitID";
            this.colVisitID.OptionsColumn.AllowEdit = false;
            this.colVisitID.OptionsColumn.AllowFocus = false;
            this.colVisitID.OptionsColumn.ReadOnly = true;
            // 
            // colJobStatusID
            // 
            this.colJobStatusID.Caption = "Status ID";
            this.colJobStatusID.FieldName = "JobStatusID";
            this.colJobStatusID.Name = "colJobStatusID";
            this.colJobStatusID.OptionsColumn.AllowEdit = false;
            this.colJobStatusID.OptionsColumn.AllowFocus = false;
            this.colJobStatusID.OptionsColumn.ReadOnly = true;
            // 
            // colJobSubTypeID1
            // 
            this.colJobSubTypeID1.Caption = "Job Sub-Type ID";
            this.colJobSubTypeID1.FieldName = "JobSubTypeID";
            this.colJobSubTypeID1.Name = "colJobSubTypeID1";
            this.colJobSubTypeID1.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeID1.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeID1.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeID1.Width = 101;
            // 
            // colCreatedByStaffID
            // 
            this.colCreatedByStaffID.Caption = "Created By Staff ID";
            this.colCreatedByStaffID.FieldName = "CreatedByStaffID";
            this.colCreatedByStaffID.Name = "colCreatedByStaffID";
            this.colCreatedByStaffID.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffID.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffID.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffID.Width = 116;
            // 
            // colReactive1
            // 
            this.colReactive1.Caption = "Reactive";
            this.colReactive1.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colReactive1.FieldName = "Reactive";
            this.colReactive1.Name = "colReactive1";
            this.colReactive1.Visible = true;
            this.colReactive1.VisibleIndex = 10;
            this.colReactive1.Width = 63;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.LookAndFeel.SkinName = "Blue";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // colJobNoLongerRequired
            // 
            this.colJobNoLongerRequired.Caption = "Job No Longer Required";
            this.colJobNoLongerRequired.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colJobNoLongerRequired.FieldName = "JobNoLongerRequired";
            this.colJobNoLongerRequired.Name = "colJobNoLongerRequired";
            this.colJobNoLongerRequired.OptionsColumn.AllowEdit = false;
            this.colJobNoLongerRequired.OptionsColumn.AllowFocus = false;
            this.colJobNoLongerRequired.OptionsColumn.ReadOnly = true;
            this.colJobNoLongerRequired.Width = 136;
            // 
            // colRequiresAccessPermit
            // 
            this.colRequiresAccessPermit.Caption = "Requires Access Permit";
            this.colRequiresAccessPermit.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colRequiresAccessPermit.FieldName = "RequiresAccessPermit";
            this.colRequiresAccessPermit.Name = "colRequiresAccessPermit";
            this.colRequiresAccessPermit.Visible = true;
            this.colRequiresAccessPermit.VisibleIndex = 11;
            this.colRequiresAccessPermit.Width = 132;
            // 
            // colAccessPermitID
            // 
            this.colAccessPermitID.Caption = "Access Permit ID";
            this.colAccessPermitID.FieldName = "AccessPermitID";
            this.colAccessPermitID.Name = "colAccessPermitID";
            this.colAccessPermitID.OptionsColumn.AllowEdit = false;
            this.colAccessPermitID.OptionsColumn.AllowFocus = false;
            this.colAccessPermitID.OptionsColumn.ReadOnly = true;
            this.colAccessPermitID.Width = 101;
            // 
            // colClientPONumber
            // 
            this.colClientPONumber.Caption = "Client PO #";
            this.colClientPONumber.ColumnEdit = this.repositoryItemButtonEditClientPONumber;
            this.colClientPONumber.FieldName = "ClientPONumber";
            this.colClientPONumber.Name = "colClientPONumber";
            this.colClientPONumber.Visible = true;
            this.colClientPONumber.VisibleIndex = 12;
            this.colClientPONumber.Width = 162;
            // 
            // repositoryItemButtonEditClientPONumber
            // 
            this.repositoryItemButtonEditClientPONumber.AutoHeight = false;
            this.repositoryItemButtonEditClientPONumber.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Click me to open the Select Client Purchase Order screen.", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Close, "Clear", -1, true, true, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Click me to Clear the selected Client PO Number.", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditClientPONumber.Name = "repositoryItemButtonEditClientPONumber";
            this.repositoryItemButtonEditClientPONumber.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemButtonEditClientPONumber.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditClientPONumber_ButtonClick);
            // 
            // colClientPOID
            // 
            this.colClientPOID.Caption = "Client PO ID";
            this.colClientPOID.FieldName = "ClientPOID";
            this.colClientPOID.Name = "colClientPOID";
            this.colClientPOID.OptionsColumn.AllowEdit = false;
            this.colClientPOID.OptionsColumn.AllowFocus = false;
            this.colClientPOID.OptionsColumn.ReadOnly = true;
            this.colClientPOID.Width = 90;
            // 
            // colFinanceSystemPONumber
            // 
            this.colFinanceSystemPONumber.Caption = "Finance System P.O. #";
            this.colFinanceSystemPONumber.FieldName = "FinanceSystemPONumber";
            this.colFinanceSystemPONumber.Name = "colFinanceSystemPONumber";
            this.colFinanceSystemPONumber.OptionsColumn.AllowEdit = false;
            this.colFinanceSystemPONumber.OptionsColumn.AllowFocus = false;
            this.colFinanceSystemPONumber.OptionsColumn.ReadOnly = true;
            this.colFinanceSystemPONumber.Width = 132;
            // 
            // colExpectedStartDate
            // 
            this.colExpectedStartDate.Caption = "Expected Start Date";
            this.colExpectedStartDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.colExpectedStartDate.FieldName = "ExpectedStartDate";
            this.colExpectedStartDate.Name = "colExpectedStartDate";
            this.colExpectedStartDate.Visible = true;
            this.colExpectedStartDate.VisibleIndex = 3;
            this.colExpectedStartDate.Width = 120;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.Mask.EditMask = "G";
            this.repositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            this.repositoryItemDateEdit1.EditValueChanged += new System.EventHandler(this.repositoryItemDateEdit1_EditValueChanged);
            // 
            // colExpectedEndDate
            // 
            this.colExpectedEndDate.Caption = "Expected End Date";
            this.colExpectedEndDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.colExpectedEndDate.FieldName = "ExpectedEndDate";
            this.colExpectedEndDate.Name = "colExpectedEndDate";
            this.colExpectedEndDate.Visible = true;
            this.colExpectedEndDate.VisibleIndex = 6;
            this.colExpectedEndDate.Width = 120;
            // 
            // colExpectedDurationUnits
            // 
            this.colExpectedDurationUnits.Caption = "Expected Duraction";
            this.colExpectedDurationUnits.ColumnEdit = this.repositoryItemSpinEdit2DP;
            this.colExpectedDurationUnits.FieldName = "ExpectedDurationUnits";
            this.colExpectedDurationUnits.Name = "colExpectedDurationUnits";
            this.colExpectedDurationUnits.Visible = true;
            this.colExpectedDurationUnits.VisibleIndex = 4;
            this.colExpectedDurationUnits.Width = 115;
            // 
            // repositoryItemSpinEdit2DP
            // 
            this.repositoryItemSpinEdit2DP.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemSpinEdit2DP.AutoHeight = false;
            this.repositoryItemSpinEdit2DP.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit2DP.Mask.EditMask = "f2";
            this.repositoryItemSpinEdit2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEdit2DP.MaxValue = new decimal(new int[] {
            1410065407,
            2,
            0,
            131072});
            this.repositoryItemSpinEdit2DP.Name = "repositoryItemSpinEdit2DP";
            this.repositoryItemSpinEdit2DP.EditValueChanged += new System.EventHandler(this.repositoryItemSpinEdit2DP_EditValueChanged);
            // 
            // colExpectedDurationUnitsDescriptorID
            // 
            this.colExpectedDurationUnitsDescriptorID.Caption = "Expected Duration Descriptor";
            this.colExpectedDurationUnitsDescriptorID.ColumnEdit = this.repositoryItemGridLookUpEditEditDurationUnitDescriptor1;
            this.colExpectedDurationUnitsDescriptorID.FieldName = "ExpectedDurationUnitsDescriptorID";
            this.colExpectedDurationUnitsDescriptorID.Name = "colExpectedDurationUnitsDescriptorID";
            this.colExpectedDurationUnitsDescriptorID.Visible = true;
            this.colExpectedDurationUnitsDescriptorID.VisibleIndex = 5;
            this.colExpectedDurationUnitsDescriptorID.Width = 162;
            // 
            // repositoryItemGridLookUpEditEditDurationUnitDescriptor1
            // 
            this.repositoryItemGridLookUpEditEditDurationUnitDescriptor1.AutoHeight = false;
            this.repositoryItemGridLookUpEditEditDurationUnitDescriptor1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditEditDurationUnitDescriptor1.DataSource = this.sp06177OMJobDurationDescriptorsWithBlankBindingSource;
            this.repositoryItemGridLookUpEditEditDurationUnitDescriptor1.DisplayMember = "Description";
            this.repositoryItemGridLookUpEditEditDurationUnitDescriptor1.Name = "repositoryItemGridLookUpEditEditDurationUnitDescriptor1";
            this.repositoryItemGridLookUpEditEditDurationUnitDescriptor1.PopupView = this.repositoryItemGridLookUpEdit4View;
            this.repositoryItemGridLookUpEditEditDurationUnitDescriptor1.ValueMember = "ID";
            this.repositoryItemGridLookUpEditEditDurationUnitDescriptor1.EditValueChanged += new System.EventHandler(this.repositoryItemGridLookUpEditDurationUnitDescriptor_EditValueChanged);
            // 
            // sp06177OMJobDurationDescriptorsWithBlankBindingSource
            // 
            this.sp06177OMJobDurationDescriptorsWithBlankBindingSource.DataMember = "sp06177_OM_Job_Duration_Descriptors_With_Blank";
            this.sp06177OMJobDurationDescriptorsWithBlankBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // repositoryItemGridLookUpEdit4View
            // 
            this.repositoryItemGridLookUpEdit4View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn356,
            this.gridColumn357,
            this.gridColumn359});
            this.repositoryItemGridLookUpEdit4View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule5.ApplyToRow = true;
            gridFormatRule5.Column = this.gridColumn356;
            gridFormatRule5.Name = "Format0";
            formatConditionRuleValue5.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue5.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue5.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue5.Value1 = 0;
            gridFormatRule5.Rule = formatConditionRuleValue5;
            this.repositoryItemGridLookUpEdit4View.FormatRules.Add(gridFormatRule5);
            this.repositoryItemGridLookUpEdit4View.Name = "repositoryItemGridLookUpEdit4View";
            this.repositoryItemGridLookUpEdit4View.OptionsLayout.Columns.StoreAllOptions = true;
            this.repositoryItemGridLookUpEdit4View.OptionsLayout.StoreAppearance = true;
            this.repositoryItemGridLookUpEdit4View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.repositoryItemGridLookUpEdit4View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit4View.OptionsView.AllowHtmlDrawHeaders = true;
            this.repositoryItemGridLookUpEdit4View.OptionsView.ColumnAutoWidth = false;
            this.repositoryItemGridLookUpEdit4View.OptionsView.EnableAppearanceEvenRow = true;
            this.repositoryItemGridLookUpEdit4View.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEdit4View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemGridLookUpEdit4View.OptionsView.ShowIndicator = false;
            this.repositoryItemGridLookUpEdit4View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn359, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn357
            // 
            this.gridColumn357.Caption = "Duration Description";
            this.gridColumn357.FieldName = "Description";
            this.gridColumn357.Name = "gridColumn357";
            this.gridColumn357.OptionsColumn.AllowEdit = false;
            this.gridColumn357.OptionsColumn.AllowFocus = false;
            this.gridColumn357.OptionsColumn.ReadOnly = true;
            this.gridColumn357.Visible = true;
            this.gridColumn357.VisibleIndex = 0;
            this.gridColumn357.Width = 220;
            // 
            // gridColumn359
            // 
            this.gridColumn359.Caption = "Order";
            this.gridColumn359.FieldName = "RecordOrder";
            this.gridColumn359.Name = "gridColumn359";
            this.gridColumn359.OptionsColumn.AllowEdit = false;
            this.gridColumn359.OptionsColumn.AllowFocus = false;
            this.gridColumn359.OptionsColumn.ReadOnly = true;
            // 
            // colScheduleSentDate
            // 
            this.colScheduleSentDate.Caption = "Schedule Sent Date";
            this.colScheduleSentDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colScheduleSentDate.FieldName = "ScheduleSentDate";
            this.colScheduleSentDate.Name = "colScheduleSentDate";
            this.colScheduleSentDate.OptionsColumn.AllowEdit = false;
            this.colScheduleSentDate.OptionsColumn.AllowFocus = false;
            this.colScheduleSentDate.OptionsColumn.ReadOnly = true;
            this.colScheduleSentDate.Width = 115;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colActualStartDate
            // 
            this.colActualStartDate.Caption = "Actual Start Date";
            this.colActualStartDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colActualStartDate.FieldName = "ActualStartDate";
            this.colActualStartDate.Name = "colActualStartDate";
            this.colActualStartDate.OptionsColumn.AllowEdit = false;
            this.colActualStartDate.OptionsColumn.AllowFocus = false;
            this.colActualStartDate.OptionsColumn.ReadOnly = true;
            this.colActualStartDate.Width = 104;
            // 
            // colActualEndDate
            // 
            this.colActualEndDate.Caption = "Actual End Date";
            this.colActualEndDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colActualEndDate.FieldName = "ActualEndDate";
            this.colActualEndDate.Name = "colActualEndDate";
            this.colActualEndDate.OptionsColumn.AllowEdit = false;
            this.colActualEndDate.OptionsColumn.AllowFocus = false;
            this.colActualEndDate.OptionsColumn.ReadOnly = true;
            this.colActualEndDate.Width = 98;
            // 
            // colActualDurationUnits
            // 
            this.colActualDurationUnits.Caption = "Actual Duration Units";
            this.colActualDurationUnits.ColumnEdit = this.repositoryItemSpinEdit2DP;
            this.colActualDurationUnits.FieldName = "ActualDurationUnits";
            this.colActualDurationUnits.Name = "colActualDurationUnits";
            this.colActualDurationUnits.OptionsColumn.AllowEdit = false;
            this.colActualDurationUnits.OptionsColumn.AllowFocus = false;
            this.colActualDurationUnits.OptionsColumn.ReadOnly = true;
            this.colActualDurationUnits.Width = 122;
            // 
            // colActualDurationUnitsDescriptionID
            // 
            this.colActualDurationUnitsDescriptionID.Caption = "Actual Duration Unit Descriptor";
            this.colActualDurationUnitsDescriptionID.ColumnEdit = this.repositoryItemGridLookUpEditEditDurationUnitDescriptor1;
            this.colActualDurationUnitsDescriptionID.FieldName = "ActualDurationUnitsDescriptionID";
            this.colActualDurationUnitsDescriptionID.Name = "colActualDurationUnitsDescriptionID";
            this.colActualDurationUnitsDescriptionID.OptionsColumn.AllowEdit = false;
            this.colActualDurationUnitsDescriptionID.OptionsColumn.AllowFocus = false;
            this.colActualDurationUnitsDescriptionID.OptionsColumn.ReadOnly = true;
            this.colActualDurationUnitsDescriptionID.Width = 169;
            // 
            // colCostTotalMaterialExVAT
            // 
            this.colCostTotalMaterialExVAT.Caption = "Cost - Total Material Ex VAT";
            this.colCostTotalMaterialExVAT.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCostTotalMaterialExVAT.FieldName = "CostTotalMaterialExVAT";
            this.colCostTotalMaterialExVAT.Name = "colCostTotalMaterialExVAT";
            this.colCostTotalMaterialExVAT.OptionsColumn.AllowEdit = false;
            this.colCostTotalMaterialExVAT.OptionsColumn.AllowFocus = false;
            this.colCostTotalMaterialExVAT.OptionsColumn.ReadOnly = true;
            this.colCostTotalMaterialExVAT.Width = 155;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // colCostTotalMaterialVATRate
            // 
            this.colCostTotalMaterialVATRate.Caption = "Cost - Total Material VAT Rate";
            this.colCostTotalMaterialVATRate.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colCostTotalMaterialVATRate.FieldName = "CostTotalMaterialVATRate";
            this.colCostTotalMaterialVATRate.Name = "colCostTotalMaterialVATRate";
            this.colCostTotalMaterialVATRate.OptionsColumn.AllowEdit = false;
            this.colCostTotalMaterialVATRate.OptionsColumn.AllowFocus = false;
            this.colCostTotalMaterialVATRate.OptionsColumn.ReadOnly = true;
            this.colCostTotalMaterialVATRate.Width = 166;
            // 
            // repositoryItemTextEditPercentage
            // 
            this.repositoryItemTextEditPercentage.AutoHeight = false;
            this.repositoryItemTextEditPercentage.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage.Name = "repositoryItemTextEditPercentage";
            // 
            // colCostTotalMaterialVAT
            // 
            this.colCostTotalMaterialVAT.Caption = "Cost - Total Material VAT";
            this.colCostTotalMaterialVAT.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCostTotalMaterialVAT.FieldName = "CostTotalMaterialVAT";
            this.colCostTotalMaterialVAT.Name = "colCostTotalMaterialVAT";
            this.colCostTotalMaterialVAT.OptionsColumn.AllowEdit = false;
            this.colCostTotalMaterialVAT.OptionsColumn.AllowFocus = false;
            this.colCostTotalMaterialVAT.OptionsColumn.ReadOnly = true;
            this.colCostTotalMaterialVAT.Width = 140;
            // 
            // colCostTotalMaterialCost
            // 
            this.colCostTotalMaterialCost.Caption = "Cost - Total Material Value";
            this.colCostTotalMaterialCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCostTotalMaterialCost.FieldName = "CostTotalMaterialCost";
            this.colCostTotalMaterialCost.Name = "colCostTotalMaterialCost";
            this.colCostTotalMaterialCost.OptionsColumn.AllowEdit = false;
            this.colCostTotalMaterialCost.OptionsColumn.AllowFocus = false;
            this.colCostTotalMaterialCost.OptionsColumn.ReadOnly = true;
            this.colCostTotalMaterialCost.Width = 147;
            // 
            // colSellTotalMaterialExVAT
            // 
            this.colSellTotalMaterialExVAT.Caption = "Sell - Total Material Ex VAT";
            this.colSellTotalMaterialExVAT.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSellTotalMaterialExVAT.FieldName = "SellTotalMaterialExVAT";
            this.colSellTotalMaterialExVAT.Name = "colSellTotalMaterialExVAT";
            this.colSellTotalMaterialExVAT.OptionsColumn.AllowEdit = false;
            this.colSellTotalMaterialExVAT.OptionsColumn.AllowFocus = false;
            this.colSellTotalMaterialExVAT.OptionsColumn.ReadOnly = true;
            this.colSellTotalMaterialExVAT.Width = 149;
            // 
            // colSellTotalMaterialVATRate
            // 
            this.colSellTotalMaterialVATRate.Caption = "Sell - Total Material VAT Rate";
            this.colSellTotalMaterialVATRate.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colSellTotalMaterialVATRate.FieldName = "SellTotalMaterialVATRate";
            this.colSellTotalMaterialVATRate.Name = "colSellTotalMaterialVATRate";
            this.colSellTotalMaterialVATRate.OptionsColumn.AllowEdit = false;
            this.colSellTotalMaterialVATRate.OptionsColumn.AllowFocus = false;
            this.colSellTotalMaterialVATRate.OptionsColumn.ReadOnly = true;
            this.colSellTotalMaterialVATRate.Width = 160;
            // 
            // colSellTotalMaterialVAT
            // 
            this.colSellTotalMaterialVAT.Caption = "Sell - Total Material VAT";
            this.colSellTotalMaterialVAT.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSellTotalMaterialVAT.FieldName = "SellTotalMaterialVAT";
            this.colSellTotalMaterialVAT.Name = "colSellTotalMaterialVAT";
            this.colSellTotalMaterialVAT.OptionsColumn.AllowEdit = false;
            this.colSellTotalMaterialVAT.OptionsColumn.AllowFocus = false;
            this.colSellTotalMaterialVAT.OptionsColumn.ReadOnly = true;
            this.colSellTotalMaterialVAT.Width = 134;
            // 
            // colSellTotalMaterialCost
            // 
            this.colSellTotalMaterialCost.Caption = "Sell - Total Material Value";
            this.colSellTotalMaterialCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSellTotalMaterialCost.FieldName = "SellTotalMaterialCost";
            this.colSellTotalMaterialCost.Name = "colSellTotalMaterialCost";
            this.colSellTotalMaterialCost.OptionsColumn.AllowEdit = false;
            this.colSellTotalMaterialCost.OptionsColumn.AllowFocus = false;
            this.colSellTotalMaterialCost.OptionsColumn.ReadOnly = true;
            this.colSellTotalMaterialCost.Width = 141;
            // 
            // colCostTotalEquipmentExVAT
            // 
            this.colCostTotalEquipmentExVAT.Caption = "Cost - Total Equipment Value";
            this.colCostTotalEquipmentExVAT.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCostTotalEquipmentExVAT.FieldName = "CostTotalEquipmentExVAT";
            this.colCostTotalEquipmentExVAT.Name = "colCostTotalEquipmentExVAT";
            this.colCostTotalEquipmentExVAT.OptionsColumn.AllowEdit = false;
            this.colCostTotalEquipmentExVAT.OptionsColumn.AllowFocus = false;
            this.colCostTotalEquipmentExVAT.OptionsColumn.ReadOnly = true;
            this.colCostTotalEquipmentExVAT.Width = 159;
            // 
            // colCostTotalEquipmentVATRate
            // 
            this.colCostTotalEquipmentVATRate.Caption = "Cost - Total Equipment VAT Rate";
            this.colCostTotalEquipmentVATRate.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colCostTotalEquipmentVATRate.FieldName = "CostTotalEquipmentVATRate";
            this.colCostTotalEquipmentVATRate.Name = "colCostTotalEquipmentVATRate";
            this.colCostTotalEquipmentVATRate.OptionsColumn.AllowEdit = false;
            this.colCostTotalEquipmentVATRate.OptionsColumn.AllowFocus = false;
            this.colCostTotalEquipmentVATRate.OptionsColumn.ReadOnly = true;
            this.colCostTotalEquipmentVATRate.Width = 178;
            // 
            // colCostTotalEquipmentVAT
            // 
            this.colCostTotalEquipmentVAT.Caption = "Cost - Total Equipment VAT";
            this.colCostTotalEquipmentVAT.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCostTotalEquipmentVAT.FieldName = "CostTotalEquipmentVAT";
            this.colCostTotalEquipmentVAT.Name = "colCostTotalEquipmentVAT";
            this.colCostTotalEquipmentVAT.OptionsColumn.AllowEdit = false;
            this.colCostTotalEquipmentVAT.OptionsColumn.AllowFocus = false;
            this.colCostTotalEquipmentVAT.OptionsColumn.ReadOnly = true;
            this.colCostTotalEquipmentVAT.Width = 154;
            // 
            // colCostTotalEquipmentCost
            // 
            this.colCostTotalEquipmentCost.Caption = "Cost - Total Equipment Value";
            this.colCostTotalEquipmentCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCostTotalEquipmentCost.FieldName = "CostTotalEquipmentCost";
            this.colCostTotalEquipmentCost.Name = "colCostTotalEquipmentCost";
            this.colCostTotalEquipmentCost.OptionsColumn.AllowEdit = false;
            this.colCostTotalEquipmentCost.OptionsColumn.AllowFocus = false;
            this.colCostTotalEquipmentCost.OptionsColumn.ReadOnly = true;
            this.colCostTotalEquipmentCost.Width = 159;
            // 
            // colSellTotalEquipmentExVAT
            // 
            this.colSellTotalEquipmentExVAT.Caption = "Sell - Total Equipment Ex VAT";
            this.colSellTotalEquipmentExVAT.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSellTotalEquipmentExVAT.FieldName = "SellTotalEquipmentExVAT";
            this.colSellTotalEquipmentExVAT.Name = "colSellTotalEquipmentExVAT";
            this.colSellTotalEquipmentExVAT.OptionsColumn.AllowEdit = false;
            this.colSellTotalEquipmentExVAT.OptionsColumn.AllowFocus = false;
            this.colSellTotalEquipmentExVAT.OptionsColumn.ReadOnly = true;
            this.colSellTotalEquipmentExVAT.Width = 161;
            // 
            // colSellTotalEquipmentVATRate
            // 
            this.colSellTotalEquipmentVATRate.Caption = "Sell - Total Equipment VAT Rate";
            this.colSellTotalEquipmentVATRate.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colSellTotalEquipmentVATRate.FieldName = "SellTotalEquipmentVATRate";
            this.colSellTotalEquipmentVATRate.Name = "colSellTotalEquipmentVATRate";
            this.colSellTotalEquipmentVATRate.OptionsColumn.AllowEdit = false;
            this.colSellTotalEquipmentVATRate.OptionsColumn.AllowFocus = false;
            this.colSellTotalEquipmentVATRate.OptionsColumn.ReadOnly = true;
            this.colSellTotalEquipmentVATRate.Width = 172;
            // 
            // colSellTotalEquipmentVAT
            // 
            this.colSellTotalEquipmentVAT.Caption = "Sell - Total Equipment VAT";
            this.colSellTotalEquipmentVAT.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSellTotalEquipmentVAT.FieldName = "SellTotalEquipmentVAT";
            this.colSellTotalEquipmentVAT.Name = "colSellTotalEquipmentVAT";
            this.colSellTotalEquipmentVAT.OptionsColumn.AllowEdit = false;
            this.colSellTotalEquipmentVAT.OptionsColumn.AllowFocus = false;
            this.colSellTotalEquipmentVAT.OptionsColumn.ReadOnly = true;
            this.colSellTotalEquipmentVAT.Width = 146;
            // 
            // colSellTotalEquipmentCost
            // 
            this.colSellTotalEquipmentCost.Caption = "Sell - Total Equipment Value";
            this.colSellTotalEquipmentCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSellTotalEquipmentCost.FieldName = "SellTotalEquipmentCost";
            this.colSellTotalEquipmentCost.Name = "colSellTotalEquipmentCost";
            this.colSellTotalEquipmentCost.OptionsColumn.AllowEdit = false;
            this.colSellTotalEquipmentCost.OptionsColumn.AllowFocus = false;
            this.colSellTotalEquipmentCost.OptionsColumn.ReadOnly = true;
            this.colSellTotalEquipmentCost.Width = 153;
            // 
            // colCostTotalLabourExVAT
            // 
            this.colCostTotalLabourExVAT.Caption = "Cost - Total Labour Ex VAT";
            this.colCostTotalLabourExVAT.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCostTotalLabourExVAT.FieldName = "CostTotalLabourExVAT";
            this.colCostTotalLabourExVAT.Name = "colCostTotalLabourExVAT";
            this.colCostTotalLabourExVAT.OptionsColumn.AllowEdit = false;
            this.colCostTotalLabourExVAT.OptionsColumn.AllowFocus = false;
            this.colCostTotalLabourExVAT.OptionsColumn.ReadOnly = true;
            this.colCostTotalLabourExVAT.Width = 150;
            // 
            // colCostTotalLabourVATRate
            // 
            this.colCostTotalLabourVATRate.Caption = "Cost - Total Labour VAT Rate";
            this.colCostTotalLabourVATRate.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colCostTotalLabourVATRate.FieldName = "CostTotalLabourVATRate";
            this.colCostTotalLabourVATRate.Name = "colCostTotalLabourVATRate";
            this.colCostTotalLabourVATRate.OptionsColumn.AllowEdit = false;
            this.colCostTotalLabourVATRate.OptionsColumn.AllowFocus = false;
            this.colCostTotalLabourVATRate.OptionsColumn.ReadOnly = true;
            this.colCostTotalLabourVATRate.Width = 161;
            // 
            // colCostTotalLabourVAT
            // 
            this.colCostTotalLabourVAT.Caption = "Cost - Total Labour VAT";
            this.colCostTotalLabourVAT.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCostTotalLabourVAT.FieldName = "CostTotalLabourVAT";
            this.colCostTotalLabourVAT.Name = "colCostTotalLabourVAT";
            this.colCostTotalLabourVAT.OptionsColumn.AllowEdit = false;
            this.colCostTotalLabourVAT.OptionsColumn.AllowFocus = false;
            this.colCostTotalLabourVAT.OptionsColumn.ReadOnly = true;
            this.colCostTotalLabourVAT.Width = 135;
            // 
            // colCostTotalLabourCost
            // 
            this.colCostTotalLabourCost.Caption = "Cost - Total Labour Value";
            this.colCostTotalLabourCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCostTotalLabourCost.FieldName = "CostTotalLabourCost";
            this.colCostTotalLabourCost.Name = "colCostTotalLabourCost";
            this.colCostTotalLabourCost.OptionsColumn.AllowEdit = false;
            this.colCostTotalLabourCost.OptionsColumn.AllowFocus = false;
            this.colCostTotalLabourCost.OptionsColumn.ReadOnly = true;
            this.colCostTotalLabourCost.Width = 142;
            // 
            // colSellTotalLabourExVAT
            // 
            this.colSellTotalLabourExVAT.Caption = "Sell - Total Labour Ex VAT";
            this.colSellTotalLabourExVAT.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSellTotalLabourExVAT.FieldName = "SellTotalLabourExVAT";
            this.colSellTotalLabourExVAT.Name = "colSellTotalLabourExVAT";
            this.colSellTotalLabourExVAT.OptionsColumn.AllowEdit = false;
            this.colSellTotalLabourExVAT.OptionsColumn.AllowFocus = false;
            this.colSellTotalLabourExVAT.OptionsColumn.ReadOnly = true;
            this.colSellTotalLabourExVAT.Width = 144;
            // 
            // colSellTotalLabourVATRate
            // 
            this.colSellTotalLabourVATRate.Caption = "Sell - Total Labour VAT Rate";
            this.colSellTotalLabourVATRate.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colSellTotalLabourVATRate.FieldName = "SellTotalLabourVATRate";
            this.colSellTotalLabourVATRate.Name = "colSellTotalLabourVATRate";
            this.colSellTotalLabourVATRate.OptionsColumn.AllowEdit = false;
            this.colSellTotalLabourVATRate.OptionsColumn.AllowFocus = false;
            this.colSellTotalLabourVATRate.OptionsColumn.ReadOnly = true;
            this.colSellTotalLabourVATRate.Width = 155;
            // 
            // colSellTotalLabourVAT
            // 
            this.colSellTotalLabourVAT.Caption = "Sell - Total Labour VAT";
            this.colSellTotalLabourVAT.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSellTotalLabourVAT.FieldName = "SellTotalLabourVAT";
            this.colSellTotalLabourVAT.Name = "colSellTotalLabourVAT";
            this.colSellTotalLabourVAT.OptionsColumn.AllowEdit = false;
            this.colSellTotalLabourVAT.OptionsColumn.AllowFocus = false;
            this.colSellTotalLabourVAT.OptionsColumn.ReadOnly = true;
            this.colSellTotalLabourVAT.Width = 129;
            // 
            // colSellTotalLabourCost
            // 
            this.colSellTotalLabourCost.Caption = "Sell - Total Labour Value";
            this.colSellTotalLabourCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSellTotalLabourCost.FieldName = "SellTotalLabourCost";
            this.colSellTotalLabourCost.Name = "colSellTotalLabourCost";
            this.colSellTotalLabourCost.OptionsColumn.AllowEdit = false;
            this.colSellTotalLabourCost.OptionsColumn.AllowFocus = false;
            this.colSellTotalLabourCost.OptionsColumn.ReadOnly = true;
            this.colSellTotalLabourCost.Width = 136;
            // 
            // colCostTotalCostExVAT
            // 
            this.colCostTotalCostExVAT.Caption = "Cost - Total Ex VAT";
            this.colCostTotalCostExVAT.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCostTotalCostExVAT.FieldName = "CostTotalCostExVAT";
            this.colCostTotalCostExVAT.Name = "colCostTotalCostExVAT";
            this.colCostTotalCostExVAT.OptionsColumn.AllowEdit = false;
            this.colCostTotalCostExVAT.OptionsColumn.AllowFocus = false;
            this.colCostTotalCostExVAT.OptionsColumn.ReadOnly = true;
            this.colCostTotalCostExVAT.Width = 114;
            // 
            // colCostTotalCostVAT
            // 
            this.colCostTotalCostVAT.Caption = "Cost - Total VAT";
            this.colCostTotalCostVAT.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCostTotalCostVAT.FieldName = "CostTotalCostVAT";
            this.colCostTotalCostVAT.Name = "colCostTotalCostVAT";
            this.colCostTotalCostVAT.OptionsColumn.AllowEdit = false;
            this.colCostTotalCostVAT.OptionsColumn.AllowFocus = false;
            this.colCostTotalCostVAT.OptionsColumn.ReadOnly = true;
            this.colCostTotalCostVAT.Width = 99;
            // 
            // colCostTotalCost
            // 
            this.colCostTotalCost.Caption = "Cost - Total Value";
            this.colCostTotalCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCostTotalCost.FieldName = "CostTotalCost";
            this.colCostTotalCost.Name = "colCostTotalCost";
            this.colCostTotalCost.OptionsColumn.AllowEdit = false;
            this.colCostTotalCost.OptionsColumn.AllowFocus = false;
            this.colCostTotalCost.OptionsColumn.ReadOnly = true;
            this.colCostTotalCost.Width = 106;
            // 
            // colSellTotalCostExVAT
            // 
            this.colSellTotalCostExVAT.Caption = "Sell - Total Ex VAT";
            this.colSellTotalCostExVAT.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSellTotalCostExVAT.FieldName = "SellTotalCostExVAT";
            this.colSellTotalCostExVAT.Name = "colSellTotalCostExVAT";
            this.colSellTotalCostExVAT.OptionsColumn.AllowEdit = false;
            this.colSellTotalCostExVAT.OptionsColumn.AllowFocus = false;
            this.colSellTotalCostExVAT.OptionsColumn.ReadOnly = true;
            this.colSellTotalCostExVAT.Width = 108;
            // 
            // colSellTotalCostVAT
            // 
            this.colSellTotalCostVAT.Caption = "Sell - Total VAT";
            this.colSellTotalCostVAT.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSellTotalCostVAT.FieldName = "SellTotalCostVAT";
            this.colSellTotalCostVAT.Name = "colSellTotalCostVAT";
            this.colSellTotalCostVAT.OptionsColumn.AllowEdit = false;
            this.colSellTotalCostVAT.OptionsColumn.AllowFocus = false;
            this.colSellTotalCostVAT.OptionsColumn.ReadOnly = true;
            this.colSellTotalCostVAT.Width = 93;
            // 
            // colSellTotalCost
            // 
            this.colSellTotalCost.Caption = "Sell - Total Value";
            this.colSellTotalCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSellTotalCost.FieldName = "SellTotalCost";
            this.colSellTotalCost.Name = "colSellTotalCost";
            this.colSellTotalCost.OptionsColumn.AllowEdit = false;
            this.colSellTotalCost.OptionsColumn.AllowFocus = false;
            this.colSellTotalCost.OptionsColumn.ReadOnly = true;
            this.colSellTotalCost.Width = 100;
            // 
            // colClientInvoiceID
            // 
            this.colClientInvoiceID.Caption = "Client Invoice ID";
            this.colClientInvoiceID.FieldName = "ClientInvoiceID";
            this.colClientInvoiceID.Name = "colClientInvoiceID";
            this.colClientInvoiceID.OptionsColumn.AllowEdit = false;
            this.colClientInvoiceID.OptionsColumn.AllowFocus = false;
            this.colClientInvoiceID.OptionsColumn.ReadOnly = true;
            this.colClientInvoiceID.Width = 100;
            // 
            // colDateClientInvoiced
            // 
            this.colDateClientInvoiced.Caption = "Client Invoiced Date";
            this.colDateClientInvoiced.FieldName = "DateClientInvoiced";
            this.colDateClientInvoiced.Name = "colDateClientInvoiced";
            this.colDateClientInvoiced.OptionsColumn.AllowEdit = false;
            this.colDateClientInvoiced.OptionsColumn.AllowFocus = false;
            this.colDateClientInvoiced.OptionsColumn.ReadOnly = true;
            this.colDateClientInvoiced.Width = 118;
            // 
            // colSelfBillingInvoiceID
            // 
            this.colSelfBillingInvoiceID.Caption = "Self-Billing Invoice ID";
            this.colSelfBillingInvoiceID.FieldName = "SelfBillingInvoiceID";
            this.colSelfBillingInvoiceID.Name = "colSelfBillingInvoiceID";
            this.colSelfBillingInvoiceID.OptionsColumn.AllowEdit = false;
            this.colSelfBillingInvoiceID.OptionsColumn.AllowFocus = false;
            this.colSelfBillingInvoiceID.OptionsColumn.ReadOnly = true;
            this.colSelfBillingInvoiceID.Width = 121;
            // 
            // colSelfBillingInvoiceReceivedDate
            // 
            this.colSelfBillingInvoiceReceivedDate.Caption = "Self-Billing Invoice Received Date";
            this.colSelfBillingInvoiceReceivedDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colSelfBillingInvoiceReceivedDate.FieldName = "SelfBillingInvoiceReceivedDate";
            this.colSelfBillingInvoiceReceivedDate.Name = "colSelfBillingInvoiceReceivedDate";
            this.colSelfBillingInvoiceReceivedDate.OptionsColumn.AllowEdit = false;
            this.colSelfBillingInvoiceReceivedDate.OptionsColumn.AllowFocus = false;
            this.colSelfBillingInvoiceReceivedDate.OptionsColumn.ReadOnly = true;
            this.colSelfBillingInvoiceReceivedDate.Width = 180;
            // 
            // colSelfBillingInvoicePaidDate
            // 
            this.colSelfBillingInvoicePaidDate.Caption = "Self-Billing Invoice Paid Date";
            this.colSelfBillingInvoicePaidDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colSelfBillingInvoicePaidDate.FieldName = "SelfBillingInvoicePaidDate";
            this.colSelfBillingInvoicePaidDate.Name = "colSelfBillingInvoicePaidDate";
            this.colSelfBillingInvoicePaidDate.OptionsColumn.AllowEdit = false;
            this.colSelfBillingInvoicePaidDate.OptionsColumn.AllowFocus = false;
            this.colSelfBillingInvoicePaidDate.OptionsColumn.ReadOnly = true;
            this.colSelfBillingInvoicePaidDate.Width = 156;
            // 
            // colSelfBillingInvoiceAmountPaid
            // 
            this.colSelfBillingInvoiceAmountPaid.Caption = "Self-Billing Invoice Amount Paid";
            this.colSelfBillingInvoiceAmountPaid.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colSelfBillingInvoiceAmountPaid.FieldName = "SelfBillingInvoiceAmountPaid";
            this.colSelfBillingInvoiceAmountPaid.Name = "colSelfBillingInvoiceAmountPaid";
            this.colSelfBillingInvoiceAmountPaid.OptionsColumn.AllowEdit = false;
            this.colSelfBillingInvoiceAmountPaid.OptionsColumn.AllowFocus = false;
            this.colSelfBillingInvoiceAmountPaid.OptionsColumn.ReadOnly = true;
            this.colSelfBillingInvoiceAmountPaid.Width = 170;
            // 
            // colDoNotPayContractor
            // 
            this.colDoNotPayContractor.Caption = "Do Not Pay Contractor";
            this.colDoNotPayContractor.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colDoNotPayContractor.FieldName = "DoNotPayContractor";
            this.colDoNotPayContractor.Name = "colDoNotPayContractor";
            this.colDoNotPayContractor.OptionsColumn.AllowEdit = false;
            this.colDoNotPayContractor.OptionsColumn.AllowFocus = false;
            this.colDoNotPayContractor.OptionsColumn.ReadOnly = true;
            this.colDoNotPayContractor.Width = 130;
            // 
            // colDoNotInvoiceClient
            // 
            this.colDoNotInvoiceClient.Caption = "Do Not Invoice Client";
            this.colDoNotInvoiceClient.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colDoNotInvoiceClient.FieldName = "DoNotInvoiceClient";
            this.colDoNotInvoiceClient.Name = "colDoNotInvoiceClient";
            this.colDoNotInvoiceClient.OptionsColumn.AllowEdit = false;
            this.colDoNotInvoiceClient.OptionsColumn.AllowFocus = false;
            this.colDoNotInvoiceClient.OptionsColumn.ReadOnly = true;
            this.colDoNotInvoiceClient.Width = 122;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 14;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colRouteOrder
            // 
            this.colRouteOrder.Caption = "Route Order";
            this.colRouteOrder.ColumnEdit = this.repositoryItemTextEditInteger3;
            this.colRouteOrder.FieldName = "RouteOrder";
            this.colRouteOrder.Name = "colRouteOrder";
            this.colRouteOrder.OptionsColumn.AllowEdit = false;
            this.colRouteOrder.OptionsColumn.AllowFocus = false;
            this.colRouteOrder.OptionsColumn.ReadOnly = true;
            this.colRouteOrder.Width = 81;
            // 
            // repositoryItemTextEditInteger3
            // 
            this.repositoryItemTextEditInteger3.AutoHeight = false;
            this.repositoryItemTextEditInteger3.Mask.EditMask = "n0";
            this.repositoryItemTextEditInteger3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditInteger3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditInteger3.Name = "repositoryItemTextEditInteger3";
            // 
            // colStartLatitude
            // 
            this.colStartLatitude.Caption = "Start Latitude";
            this.colStartLatitude.ColumnEdit = this.repositoryItemTextEditFloat;
            this.colStartLatitude.FieldName = "StartLatitude";
            this.colStartLatitude.Name = "colStartLatitude";
            this.colStartLatitude.OptionsColumn.AllowEdit = false;
            this.colStartLatitude.OptionsColumn.AllowFocus = false;
            this.colStartLatitude.OptionsColumn.ReadOnly = true;
            this.colStartLatitude.Width = 87;
            // 
            // repositoryItemTextEditFloat
            // 
            this.repositoryItemTextEditFloat.AutoHeight = false;
            this.repositoryItemTextEditFloat.Mask.EditMask = "n8";
            this.repositoryItemTextEditFloat.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditFloat.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditFloat.Name = "repositoryItemTextEditFloat";
            // 
            // colStartLongitude
            // 
            this.colStartLongitude.Caption = "Start Longitude";
            this.colStartLongitude.ColumnEdit = this.repositoryItemTextEditFloat;
            this.colStartLongitude.FieldName = "StartLongitude";
            this.colStartLongitude.Name = "colStartLongitude";
            this.colStartLongitude.OptionsColumn.AllowEdit = false;
            this.colStartLongitude.OptionsColumn.AllowFocus = false;
            this.colStartLongitude.OptionsColumn.ReadOnly = true;
            this.colStartLongitude.Width = 95;
            // 
            // colFinishLatitude
            // 
            this.colFinishLatitude.Caption = "Finish Latitude";
            this.colFinishLatitude.ColumnEdit = this.repositoryItemTextEditFloat;
            this.colFinishLatitude.FieldName = "FinishLatitude";
            this.colFinishLatitude.Name = "colFinishLatitude";
            this.colFinishLatitude.OptionsColumn.AllowEdit = false;
            this.colFinishLatitude.OptionsColumn.AllowFocus = false;
            this.colFinishLatitude.OptionsColumn.ReadOnly = true;
            this.colFinishLatitude.Width = 90;
            // 
            // colFinishLongitude
            // 
            this.colFinishLongitude.Caption = "Finish Longitude";
            this.colFinishLongitude.ColumnEdit = this.repositoryItemTextEditFloat;
            this.colFinishLongitude.FieldName = "FinishLongitude";
            this.colFinishLongitude.Name = "colFinishLongitude";
            this.colFinishLongitude.OptionsColumn.AllowEdit = false;
            this.colFinishLongitude.OptionsColumn.AllowFocus = false;
            this.colFinishLongitude.OptionsColumn.ReadOnly = true;
            this.colFinishLongitude.Width = 98;
            // 
            // colTenderID
            // 
            this.colTenderID.Caption = "Tender ID";
            this.colTenderID.FieldName = "TenderID";
            this.colTenderID.Name = "colTenderID";
            this.colTenderID.OptionsColumn.AllowEdit = false;
            this.colTenderID.OptionsColumn.AllowFocus = false;
            this.colTenderID.OptionsColumn.ReadOnly = true;
            // 
            // colTenderJobID
            // 
            this.colTenderJobID.Caption = "Tender Job ID";
            this.colTenderJobID.FieldName = "TenderJobID";
            this.colTenderJobID.Name = "colTenderJobID";
            this.colTenderJobID.OptionsColumn.AllowEdit = false;
            this.colTenderJobID.OptionsColumn.AllowFocus = false;
            this.colTenderJobID.OptionsColumn.ReadOnly = true;
            this.colTenderJobID.Width = 89;
            // 
            // colMinimumDaysFromLastVisit
            // 
            this.colMinimumDaysFromLastVisit.Caption = "Min Days From Last Visit";
            this.colMinimumDaysFromLastVisit.ColumnEdit = this.repositoryItemSpinEditInteger;
            this.colMinimumDaysFromLastVisit.FieldName = "MinimumDaysFromLastVisit";
            this.colMinimumDaysFromLastVisit.Name = "colMinimumDaysFromLastVisit";
            this.colMinimumDaysFromLastVisit.Visible = true;
            this.colMinimumDaysFromLastVisit.VisibleIndex = 7;
            this.colMinimumDaysFromLastVisit.Width = 136;
            // 
            // repositoryItemSpinEditInteger
            // 
            this.repositoryItemSpinEditInteger.AutoHeight = false;
            this.repositoryItemSpinEditInteger.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEditInteger.Mask.EditMask = "n0";
            this.repositoryItemSpinEditInteger.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditInteger.MaxValue = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.repositoryItemSpinEditInteger.Name = "repositoryItemSpinEditInteger";
            // 
            // colMaximumDaysFromLastVisit
            // 
            this.colMaximumDaysFromLastVisit.Caption = "Max Days From Last Visit";
            this.colMaximumDaysFromLastVisit.ColumnEdit = this.repositoryItemSpinEditInteger;
            this.colMaximumDaysFromLastVisit.FieldName = "MaximumDaysFromLastVisit";
            this.colMaximumDaysFromLastVisit.Name = "colMaximumDaysFromLastVisit";
            this.colMaximumDaysFromLastVisit.Visible = true;
            this.colMaximumDaysFromLastVisit.VisibleIndex = 8;
            this.colMaximumDaysFromLastVisit.Width = 140;
            // 
            // colDaysLeeway
            // 
            this.colDaysLeeway.Caption = "Days Leeway";
            this.colDaysLeeway.ColumnEdit = this.repositoryItemSpinEditInteger;
            this.colDaysLeeway.FieldName = "DaysLeeway";
            this.colDaysLeeway.Name = "colDaysLeeway";
            this.colDaysLeeway.Visible = true;
            this.colDaysLeeway.VisibleIndex = 9;
            this.colDaysLeeway.Width = 85;
            // 
            // colBillingCentreCodeID1
            // 
            this.colBillingCentreCodeID1.Caption = "Billing Centre Code";
            this.colBillingCentreCodeID1.FieldName = "BillingCentreCodeID";
            this.colBillingCentreCodeID1.Name = "colBillingCentreCodeID1";
            this.colBillingCentreCodeID1.OptionsColumn.AllowEdit = false;
            this.colBillingCentreCodeID1.OptionsColumn.AllowFocus = false;
            this.colBillingCentreCodeID1.OptionsColumn.ReadOnly = true;
            this.colBillingCentreCodeID1.Width = 111;
            // 
            // colSiteContractID
            // 
            this.colSiteContractID.Caption = "Site Contract ID";
            this.colSiteContractID.FieldName = "SiteContractID";
            this.colSiteContractID.Name = "colSiteContractID";
            this.colSiteContractID.OptionsColumn.AllowEdit = false;
            this.colSiteContractID.OptionsColumn.AllowFocus = false;
            this.colSiteContractID.OptionsColumn.ReadOnly = true;
            this.colSiteContractID.Width = 98;
            // 
            // colClientContractID
            // 
            this.colClientContractID.Caption = "Client Contract ID";
            this.colClientContractID.FieldName = "ClientContractID";
            this.colClientContractID.Name = "colClientContractID";
            this.colClientContractID.OptionsColumn.AllowEdit = false;
            this.colClientContractID.OptionsColumn.AllowFocus = false;
            this.colClientContractID.OptionsColumn.ReadOnly = true;
            this.colClientContractID.Width = 107;
            // 
            // colSiteID2
            // 
            this.colSiteID2.Caption = "Site ID";
            this.colSiteID2.FieldName = "SiteID";
            this.colSiteID2.Name = "colSiteID2";
            this.colSiteID2.OptionsColumn.AllowEdit = false;
            this.colSiteID2.OptionsColumn.AllowFocus = false;
            this.colSiteID2.OptionsColumn.ReadOnly = true;
            this.colSiteID2.Visible = true;
            this.colSiteID2.VisibleIndex = 13;
            this.colSiteID2.Width = 51;
            // 
            // colClientID3
            // 
            this.colClientID3.Caption = "Client ID";
            this.colClientID3.FieldName = "ClientID";
            this.colClientID3.Name = "colClientID3";
            this.colClientID3.OptionsColumn.AllowEdit = false;
            this.colClientID3.OptionsColumn.AllowFocus = false;
            this.colClientID3.OptionsColumn.ReadOnly = true;
            // 
            // colContractDescription1
            // 
            this.colContractDescription1.Caption = "Contract Description";
            this.colContractDescription1.FieldName = "ContractDescription";
            this.colContractDescription1.Name = "colContractDescription1";
            this.colContractDescription1.OptionsColumn.AllowEdit = false;
            this.colContractDescription1.OptionsColumn.AllowFocus = false;
            this.colContractDescription1.OptionsColumn.ReadOnly = true;
            this.colContractDescription1.Width = 119;
            // 
            // colClientNameContractDescription
            // 
            this.colClientNameContractDescription.Caption = "Client \\ Contract";
            this.colClientNameContractDescription.FieldName = "ClientNameContractDescription";
            this.colClientNameContractDescription.Name = "colClientNameContractDescription";
            this.colClientNameContractDescription.OptionsColumn.AllowEdit = false;
            this.colClientNameContractDescription.OptionsColumn.AllowFocus = false;
            this.colClientNameContractDescription.OptionsColumn.ReadOnly = true;
            this.colClientNameContractDescription.Visible = true;
            this.colClientNameContractDescription.VisibleIndex = 14;
            this.colClientNameContractDescription.Width = 416;
            // 
            // colClientName1
            // 
            this.colClientName1.Caption = "Client Name";
            this.colClientName1.FieldName = "ClientName";
            this.colClientName1.Name = "colClientName1";
            this.colClientName1.OptionsColumn.AllowEdit = false;
            this.colClientName1.OptionsColumn.AllowFocus = false;
            this.colClientName1.OptionsColumn.ReadOnly = true;
            this.colClientName1.Width = 199;
            // 
            // colSiteName1
            // 
            this.colSiteName1.Caption = "Site Name";
            this.colSiteName1.FieldName = "SiteName";
            this.colSiteName1.Name = "colSiteName1";
            this.colSiteName1.OptionsColumn.AllowEdit = false;
            this.colSiteName1.OptionsColumn.AllowFocus = false;
            this.colSiteName1.OptionsColumn.ReadOnly = true;
            this.colSiteName1.Visible = true;
            this.colSiteName1.VisibleIndex = 15;
            this.colSiteName1.Width = 276;
            // 
            // colSitePostcode
            // 
            this.colSitePostcode.Caption = "Site Postcode";
            this.colSitePostcode.FieldName = "SitePostcode";
            this.colSitePostcode.Name = "colSitePostcode";
            this.colSitePostcode.OptionsColumn.AllowEdit = false;
            this.colSitePostcode.OptionsColumn.AllowFocus = false;
            this.colSitePostcode.OptionsColumn.ReadOnly = true;
            this.colSitePostcode.Width = 86;
            // 
            // colLocationX
            // 
            this.colLocationX.Caption = "Site Latitude";
            this.colLocationX.ColumnEdit = this.repositoryItemTextEditFloat;
            this.colLocationX.FieldName = "LocationX";
            this.colLocationX.Name = "colLocationX";
            this.colLocationX.OptionsColumn.AllowEdit = false;
            this.colLocationX.OptionsColumn.AllowFocus = false;
            this.colLocationX.OptionsColumn.ReadOnly = true;
            this.colLocationX.Width = 81;
            // 
            // colLocationY
            // 
            this.colLocationY.Caption = "Site Longitude";
            this.colLocationY.ColumnEdit = this.repositoryItemTextEditFloat;
            this.colLocationY.FieldName = "LocationY";
            this.colLocationY.Name = "colLocationY";
            this.colLocationY.OptionsColumn.AllowEdit = false;
            this.colLocationY.OptionsColumn.AllowFocus = false;
            this.colLocationY.OptionsColumn.ReadOnly = true;
            this.colLocationY.Width = 89;
            // 
            // colCreatedByStaffName
            // 
            this.colCreatedByStaffName.Caption = "Created By";
            this.colCreatedByStaffName.FieldName = "CreatedByStaffName";
            this.colCreatedByStaffName.Name = "colCreatedByStaffName";
            this.colCreatedByStaffName.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffName.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffName.OptionsColumn.ReadOnly = true;
            // 
            // colJobSubTypeDescription1
            // 
            this.colJobSubTypeDescription1.Caption = "Job Sub-Type";
            this.colJobSubTypeDescription1.FieldName = "JobSubTypeDescription";
            this.colJobSubTypeDescription1.Name = "colJobSubTypeDescription1";
            this.colJobSubTypeDescription1.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeDescription1.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeDescription1.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeDescription1.Width = 87;
            // 
            // colJobTypeDescription1
            // 
            this.colJobTypeDescription1.Caption = "Job Type";
            this.colJobTypeDescription1.FieldName = "JobTypeDescription";
            this.colJobTypeDescription1.Name = "colJobTypeDescription1";
            this.colJobTypeDescription1.OptionsColumn.AllowEdit = false;
            this.colJobTypeDescription1.OptionsColumn.AllowFocus = false;
            this.colJobTypeDescription1.OptionsColumn.ReadOnly = true;
            // 
            // colJobTypeJobSubTypeDescription
            // 
            this.colJobTypeJobSubTypeDescription.Caption = "Job Type \\ Sub-Type";
            this.colJobTypeJobSubTypeDescription.ColumnEdit = this.repositoryItemButtonEditJobTypeJobSubType;
            this.colJobTypeJobSubTypeDescription.FieldName = "JobTypeJobSubTypeDescription";
            this.colJobTypeJobSubTypeDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colJobTypeJobSubTypeDescription.Name = "colJobTypeJobSubTypeDescription";
            this.colJobTypeJobSubTypeDescription.Visible = true;
            this.colJobTypeJobSubTypeDescription.VisibleIndex = 0;
            this.colJobTypeJobSubTypeDescription.Width = 290;
            // 
            // repositoryItemButtonEditJobTypeJobSubType
            // 
            this.repositoryItemButtonEditJobTypeJobSubType.AutoHeight = false;
            this.repositoryItemButtonEditJobTypeJobSubType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "Click me to open the Select Job Type \\ Sub-Type screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditJobTypeJobSubType.Name = "repositoryItemButtonEditJobTypeJobSubType";
            this.repositoryItemButtonEditJobTypeJobSubType.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemButtonEditJobTypeJobSubType.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditJobTypeJobSubType_ButtonClick);
            // 
            // colJobTypeID1
            // 
            this.colJobTypeID1.Caption = "Job Type ID";
            this.colJobTypeID1.FieldName = "JobTypeID";
            this.colJobTypeID1.Name = "colJobTypeID1";
            this.colJobTypeID1.OptionsColumn.AllowEdit = false;
            this.colJobTypeID1.OptionsColumn.AllowFocus = false;
            this.colJobTypeID1.OptionsColumn.ReadOnly = true;
            this.colJobTypeID1.Width = 79;
            // 
            // colVisitNumber
            // 
            this.colVisitNumber.Caption = "Visit #";
            this.colVisitNumber.ColumnEdit = this.repositoryItemTextEditInteger3;
            this.colVisitNumber.FieldName = "VisitNumber";
            this.colVisitNumber.Name = "colVisitNumber";
            this.colVisitNumber.OptionsColumn.AllowEdit = false;
            this.colVisitNumber.OptionsColumn.AllowFocus = false;
            this.colVisitNumber.OptionsColumn.ReadOnly = true;
            this.colVisitNumber.Visible = true;
            this.colVisitNumber.VisibleIndex = 13;
            // 
            // colLabourCount
            // 
            this.colLabourCount.Caption = "Labour Count";
            this.colLabourCount.ColumnEdit = this.repositoryItemTextEditInteger3;
            this.colLabourCount.FieldName = "LabourCount";
            this.colLabourCount.Name = "colLabourCount";
            this.colLabourCount.OptionsColumn.AllowEdit = false;
            this.colLabourCount.OptionsColumn.AllowFocus = false;
            this.colLabourCount.OptionsColumn.ReadOnly = true;
            this.colLabourCount.Width = 86;
            // 
            // colEquipmentCount
            // 
            this.colEquipmentCount.Caption = "Equipment Count";
            this.colEquipmentCount.ColumnEdit = this.repositoryItemTextEditInteger3;
            this.colEquipmentCount.FieldName = "EquipmentCount";
            this.colEquipmentCount.Name = "colEquipmentCount";
            this.colEquipmentCount.OptionsColumn.AllowEdit = false;
            this.colEquipmentCount.OptionsColumn.AllowFocus = false;
            this.colEquipmentCount.OptionsColumn.ReadOnly = true;
            this.colEquipmentCount.Width = 103;
            // 
            // colMaterialCount
            // 
            this.colMaterialCount.Caption = "Material Count";
            this.colMaterialCount.ColumnEdit = this.repositoryItemTextEditInteger3;
            this.colMaterialCount.FieldName = "MaterialCount";
            this.colMaterialCount.Name = "colMaterialCount";
            this.colMaterialCount.OptionsColumn.AllowEdit = false;
            this.colMaterialCount.OptionsColumn.AllowFocus = false;
            this.colMaterialCount.OptionsColumn.ReadOnly = true;
            this.colMaterialCount.Width = 91;
            // 
            // colDisplayOrder
            // 
            this.colDisplayOrder.Caption = "App Display Order";
            this.colDisplayOrder.ColumnEdit = this.repositoryItemSpinEdit2;
            this.colDisplayOrder.FieldName = "DisplayOrder";
            this.colDisplayOrder.Name = "colDisplayOrder";
            this.colDisplayOrder.Visible = true;
            this.colDisplayOrder.VisibleIndex = 1;
            this.colDisplayOrder.Width = 119;
            // 
            // repositoryItemSpinEdit2
            // 
            this.repositoryItemSpinEdit2.AutoHeight = false;
            this.repositoryItemSpinEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit2.IsFloatValue = false;
            this.repositoryItemSpinEdit2.Mask.EditMask = "n0";
            this.repositoryItemSpinEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEdit2.MaxValue = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.repositoryItemSpinEdit2.Name = "repositoryItemSpinEdit2";
            // 
            // colMandatory
            // 
            this.colMandatory.Caption = "Mandatory";
            this.colMandatory.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colMandatory.FieldName = "Mandatory";
            this.colMandatory.Name = "colMandatory";
            this.colMandatory.Visible = true;
            this.colMandatory.VisibleIndex = 2;
            // 
            // btnStep5Next
            // 
            this.btnStep5Next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep5Next.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.btnStep5Next.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnStep5Next.Location = new System.Drawing.Point(1119, 501);
            this.btnStep5Next.Name = "btnStep5Next";
            this.btnStep5Next.Size = new System.Drawing.Size(88, 30);
            this.btnStep5Next.TabIndex = 19;
            this.btnStep5Next.Text = "Next";
            this.btnStep5Next.Click += new System.EventHandler(this.btnStep5Next_Click);
            // 
            // btnStep5Previous
            // 
            this.btnStep5Previous.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep5Previous.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnStep5Previous.Location = new System.Drawing.Point(1025, 501);
            this.btnStep5Previous.Name = "btnStep5Previous";
            this.btnStep5Previous.Size = new System.Drawing.Size(88, 30);
            this.btnStep5Previous.TabIndex = 20;
            this.btnStep5Previous.Text = "Previous";
            this.btnStep5Previous.Click += new System.EventHandler(this.btnStep5Previous_Click);
            // 
            // panelControl6
            // 
            this.panelControl6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl6.Controls.Add(this.pictureEdit8);
            this.panelControl6.Controls.Add(this.labelControl13);
            this.panelControl6.Controls.Add(this.labelControl14);
            this.panelControl6.Location = new System.Drawing.Point(7, 6);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(1200, 48);
            this.panelControl6.TabIndex = 17;
            // 
            // pictureEdit8
            // 
            this.pictureEdit8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit8.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit8.Location = new System.Drawing.Point(1156, 4);
            this.pictureEdit8.MenuManager = this.barManager1;
            this.pictureEdit8.Name = "pictureEdit8";
            this.pictureEdit8.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit8.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit8.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit8.Properties.ReadOnly = true;
            this.pictureEdit8.Properties.ShowMenu = false;
            this.pictureEdit8.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit8.TabIndex = 9;
            // 
            // labelControl13
            // 
            this.labelControl13.AllowHtmlString = true;
            this.labelControl13.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Appearance.Options.UseBackColor = true;
            this.labelControl13.Appearance.Options.UseFont = true;
            this.labelControl13.Location = new System.Drawing.Point(5, 5);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(194, 16);
            this.labelControl13.TabIndex = 6;
            this.labelControl13.Text = "<b>Step 5:</b> Refine Work  <b>[Optional]</b>";
            // 
            // labelControl14
            // 
            this.labelControl14.AllowHtmlString = true;
            this.labelControl14.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl14.Appearance.Options.UseBackColor = true;
            this.labelControl14.Location = new System.Drawing.Point(57, 29);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(447, 13);
            this.labelControl14.TabIndex = 7;
            this.labelControl14.Text = "Make any adjustments to the work by adjusting the values in the grid. Click Next " +
    "when Done.";
            // 
            // xtraTabPageStep6
            // 
            this.xtraTabPageStep6.Controls.Add(this.checkEditSkipEquipmentAndMaterials);
            this.xtraTabPageStep6.Controls.Add(this.splitContainerControl2);
            this.xtraTabPageStep6.Controls.Add(this.btnStep6Next);
            this.xtraTabPageStep6.Controls.Add(this.btnStep6Previous);
            this.xtraTabPageStep6.Controls.Add(this.panelControl10);
            this.xtraTabPageStep6.Name = "xtraTabPageStep6";
            this.xtraTabPageStep6.Size = new System.Drawing.Size(1213, 537);
            this.xtraTabPageStep6.Tag = "6";
            this.xtraTabPageStep6.Text = "Step 6";
            // 
            // checkEditSkipEquipmentAndMaterials
            // 
            this.checkEditSkipEquipmentAndMaterials.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkEditSkipEquipmentAndMaterials.EditValue = true;
            this.checkEditSkipEquipmentAndMaterials.Location = new System.Drawing.Point(6, 506);
            this.checkEditSkipEquipmentAndMaterials.MenuManager = this.barManager1;
            this.checkEditSkipEquipmentAndMaterials.Name = "checkEditSkipEquipmentAndMaterials";
            this.checkEditSkipEquipmentAndMaterials.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.checkEditSkipEquipmentAndMaterials.Properties.Appearance.Options.UseFont = true;
            this.checkEditSkipEquipmentAndMaterials.Properties.Caption = "Skip Equipment and Materials";
            this.checkEditSkipEquipmentAndMaterials.Size = new System.Drawing.Size(212, 19);
            this.checkEditSkipEquipmentAndMaterials.TabIndex = 29;
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl2.Location = new System.Drawing.Point(6, 62);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.gridControl7);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.standaloneBarDockControl1);
            this.splitContainerControl2.Panel2.Controls.Add(this.gridControl8);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(1201, 433);
            this.splitContainerControl2.SplitterPosition = 471;
            this.splitContainerControl2.TabIndex = 28;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // gridControl7
            // 
            this.gridControl7.DataSource = this.sp06174OMJobEditBindingSource;
            this.gridControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl7.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl7.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl7.Location = new System.Drawing.Point(0, 0);
            this.gridControl7.MainView = this.gridView7;
            this.gridControl7.MenuManager = this.barManager1;
            this.gridControl7.Name = "gridControl7";
            this.gridControl7.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit7,
            this.repositoryItemTextEditDateTime5,
            this.repositoryItemTextEditCurrency6,
            this.repositoryItemTextEdit2DP6,
            this.repositoryItemTextEditPercentage6,
            this.repositoryItemTextEditInteger6,
            this.repositoryItemCheckEdit5});
            this.gridControl7.Size = new System.Drawing.Size(471, 433);
            this.gridControl7.TabIndex = 24;
            this.gridControl7.UseEmbeddedNavigator = true;
            this.gridControl7.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView7});
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn30,
            this.gridColumn31,
            this.gridColumn32,
            this.gridColumn33,
            this.gridColumn34,
            this.gridColumn35,
            this.gridColumn36,
            this.gridColumn37,
            this.gridColumn38,
            this.gridColumn39,
            this.gridColumn40,
            this.gridColumn41,
            this.gridColumn42,
            this.gridColumn43,
            this.gridColumn44,
            this.gridColumn45,
            this.gridColumn46,
            this.gridColumn47,
            this.gridColumn48,
            this.gridColumn49,
            this.gridColumn50,
            this.gridColumn51,
            this.gridColumn52,
            this.gridColumn147,
            this.gridColumn148,
            this.gridColumn149,
            this.gridColumn150,
            this.gridColumn151,
            this.gridColumn152,
            this.gridColumn153,
            this.gridColumn154,
            this.gridColumn155,
            this.gridColumn156,
            this.gridColumn157,
            this.gridColumn158,
            this.gridColumn159,
            this.gridColumn160,
            this.gridColumn161,
            this.gridColumn162,
            this.gridColumn163,
            this.gridColumn164,
            this.gridColumn165,
            this.gridColumn166,
            this.gridColumn167,
            this.gridColumn168,
            this.gridColumn169,
            this.gridColumn170,
            this.gridColumn171,
            this.gridColumn172,
            this.gridColumn173,
            this.gridColumn174,
            this.gridColumn175,
            this.gridColumn176,
            this.gridColumn177,
            this.gridColumn178,
            this.gridColumn179,
            this.gridColumn180,
            this.gridColumn181,
            this.gridColumn182,
            this.gridColumn183,
            this.gridColumn184,
            this.gridColumn185,
            this.gridColumn186,
            this.gridColumn187,
            this.gridColumn188,
            this.gridColumn189,
            this.gridColumn190,
            this.gridColumn191,
            this.gridColumn192,
            this.gridColumn193,
            this.gridColumn194,
            this.gridColumn195,
            this.gridColumn196,
            this.gridColumn197,
            this.gridColumn198,
            this.gridColumn199,
            this.gridColumn200,
            this.gridColumn201,
            this.gridColumn202,
            this.gridColumn203,
            this.gridColumn204,
            this.gridColumn205,
            this.gridColumn206,
            this.gridColumn207,
            this.gridColumn208,
            this.gridColumn209,
            this.gridColumn210,
            this.gridColumn211,
            this.colLabourCount1,
            this.colEquipmentCount1,
            this.colMaterialCount1,
            this.colVisitCostCalculationLevel,
            this.colVisitCostCalculationLevel1});
            this.gridView7.GridControl = this.gridControl7;
            this.gridView7.GroupCount = 3;
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView7.OptionsFilter.AllowFilterEditor = false;
            this.gridView7.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView7.OptionsFilter.AllowMRUFilterList = false;
            this.gridView7.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView7.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView7.OptionsFind.FindDelay = 2000;
            this.gridView7.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView7.OptionsLayout.StoreAppearance = true;
            this.gridView7.OptionsLayout.StoreFormatRules = true;
            this.gridView7.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView7.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView7.OptionsSelection.MultiSelect = true;
            this.gridView7.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView7.OptionsView.ColumnAutoWidth = false;
            this.gridView7.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView7.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            this.gridView7.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn200, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn202, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn211, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn209, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView7.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView7_CustomDrawCell);
            this.gridView7.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView7.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView7.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView7.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView7.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView7.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView7.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView7_MouseUp);
            this.gridView7.GotFocus += new System.EventHandler(this.gridView7_GotFocus);
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Mode";
            this.gridColumn10.FieldName = "strMode";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Record IDs";
            this.gridColumn11.FieldName = "strRecordIDs";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Dummy Job ID";
            this.gridColumn12.FieldName = "DummyJobID";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "Job ID";
            this.gridColumn30.FieldName = "JobID";
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.OptionsColumn.AllowEdit = false;
            this.gridColumn30.OptionsColumn.AllowFocus = false;
            this.gridColumn30.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn31
            // 
            this.gridColumn31.Caption = "Visit ID";
            this.gridColumn31.FieldName = "VisitID";
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.OptionsColumn.AllowEdit = false;
            this.gridColumn31.OptionsColumn.AllowFocus = false;
            this.gridColumn31.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn32
            // 
            this.gridColumn32.Caption = "Status ID";
            this.gridColumn32.FieldName = "JobStatusID";
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.OptionsColumn.AllowEdit = false;
            this.gridColumn32.OptionsColumn.AllowFocus = false;
            this.gridColumn32.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn33
            // 
            this.gridColumn33.Caption = "Job Sub-Type ID";
            this.gridColumn33.FieldName = "JobSubTypeID";
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.OptionsColumn.AllowEdit = false;
            this.gridColumn33.OptionsColumn.AllowFocus = false;
            this.gridColumn33.OptionsColumn.ReadOnly = true;
            this.gridColumn33.Width = 101;
            // 
            // gridColumn34
            // 
            this.gridColumn34.Caption = "Created By Staff ID";
            this.gridColumn34.FieldName = "CreatedByStaffID";
            this.gridColumn34.Name = "gridColumn34";
            this.gridColumn34.OptionsColumn.AllowEdit = false;
            this.gridColumn34.OptionsColumn.AllowFocus = false;
            this.gridColumn34.OptionsColumn.ReadOnly = true;
            this.gridColumn34.Width = 116;
            // 
            // gridColumn35
            // 
            this.gridColumn35.Caption = "Reactive";
            this.gridColumn35.FieldName = "Reactive";
            this.gridColumn35.Name = "gridColumn35";
            this.gridColumn35.OptionsColumn.AllowEdit = false;
            this.gridColumn35.OptionsColumn.AllowFocus = false;
            this.gridColumn35.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn36
            // 
            this.gridColumn36.Caption = "Job No Longer Required";
            this.gridColumn36.FieldName = "JobNoLongerRequired";
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.OptionsColumn.AllowEdit = false;
            this.gridColumn36.OptionsColumn.AllowFocus = false;
            this.gridColumn36.OptionsColumn.ReadOnly = true;
            this.gridColumn36.Width = 136;
            // 
            // gridColumn37
            // 
            this.gridColumn37.Caption = "Requires Access Permit";
            this.gridColumn37.ColumnEdit = this.repositoryItemCheckEdit5;
            this.gridColumn37.FieldName = "RequiresAccessPermit";
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.OptionsColumn.AllowEdit = false;
            this.gridColumn37.OptionsColumn.AllowFocus = false;
            this.gridColumn37.OptionsColumn.ReadOnly = true;
            this.gridColumn37.Visible = true;
            this.gridColumn37.VisibleIndex = 9;
            this.gridColumn37.Width = 132;
            // 
            // repositoryItemCheckEdit5
            // 
            this.repositoryItemCheckEdit5.AutoHeight = false;
            this.repositoryItemCheckEdit5.Caption = "Check";
            this.repositoryItemCheckEdit5.Name = "repositoryItemCheckEdit5";
            this.repositoryItemCheckEdit5.ValueChecked = 1;
            this.repositoryItemCheckEdit5.ValueUnchecked = 0;
            // 
            // gridColumn38
            // 
            this.gridColumn38.Caption = "Access Permit ID";
            this.gridColumn38.FieldName = "AccessPermitID";
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.OptionsColumn.AllowEdit = false;
            this.gridColumn38.OptionsColumn.AllowFocus = false;
            this.gridColumn38.OptionsColumn.ReadOnly = true;
            this.gridColumn38.Width = 101;
            // 
            // gridColumn39
            // 
            this.gridColumn39.Caption = "Client P.O. #";
            this.gridColumn39.FieldName = "ClientPONumber";
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.OptionsColumn.AllowEdit = false;
            this.gridColumn39.OptionsColumn.AllowFocus = false;
            this.gridColumn39.OptionsColumn.ReadOnly = true;
            this.gridColumn39.Width = 87;
            // 
            // gridColumn40
            // 
            this.gridColumn40.Caption = "Client P.O. ID";
            this.gridColumn40.FieldName = "ClientPOID";
            this.gridColumn40.Name = "gridColumn40";
            this.gridColumn40.OptionsColumn.AllowEdit = false;
            this.gridColumn40.OptionsColumn.AllowFocus = false;
            this.gridColumn40.OptionsColumn.ReadOnly = true;
            this.gridColumn40.Width = 90;
            // 
            // gridColumn41
            // 
            this.gridColumn41.Caption = "Finance System P.O. #";
            this.gridColumn41.FieldName = "FinanceSystemPONumber";
            this.gridColumn41.Name = "gridColumn41";
            this.gridColumn41.OptionsColumn.AllowEdit = false;
            this.gridColumn41.OptionsColumn.AllowFocus = false;
            this.gridColumn41.OptionsColumn.ReadOnly = true;
            this.gridColumn41.Width = 132;
            // 
            // gridColumn42
            // 
            this.gridColumn42.Caption = "Expected Start Date";
            this.gridColumn42.ColumnEdit = this.repositoryItemTextEditDateTime5;
            this.gridColumn42.FieldName = "ExpectedStartDate";
            this.gridColumn42.Name = "gridColumn42";
            this.gridColumn42.OptionsColumn.AllowEdit = false;
            this.gridColumn42.OptionsColumn.AllowFocus = false;
            this.gridColumn42.OptionsColumn.ReadOnly = true;
            this.gridColumn42.Visible = true;
            this.gridColumn42.VisibleIndex = 2;
            this.gridColumn42.Width = 119;
            // 
            // repositoryItemTextEditDateTime5
            // 
            this.repositoryItemTextEditDateTime5.AutoHeight = false;
            this.repositoryItemTextEditDateTime5.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime5.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime5.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime5.Name = "repositoryItemTextEditDateTime5";
            // 
            // gridColumn43
            // 
            this.gridColumn43.Caption = "Expected End Date";
            this.gridColumn43.ColumnEdit = this.repositoryItemTextEditDateTime5;
            this.gridColumn43.FieldName = "ExpectedEndDate";
            this.gridColumn43.Name = "gridColumn43";
            this.gridColumn43.OptionsColumn.AllowEdit = false;
            this.gridColumn43.OptionsColumn.AllowFocus = false;
            this.gridColumn43.OptionsColumn.ReadOnly = true;
            this.gridColumn43.Visible = true;
            this.gridColumn43.VisibleIndex = 5;
            this.gridColumn43.Width = 113;
            // 
            // gridColumn44
            // 
            this.gridColumn44.Caption = "Expected Duraction";
            this.gridColumn44.ColumnEdit = this.repositoryItemTextEdit2DP6;
            this.gridColumn44.FieldName = "ExpectedDurationUnits";
            this.gridColumn44.Name = "gridColumn44";
            this.gridColumn44.OptionsColumn.AllowEdit = false;
            this.gridColumn44.OptionsColumn.AllowFocus = false;
            this.gridColumn44.OptionsColumn.ReadOnly = true;
            this.gridColumn44.Visible = true;
            this.gridColumn44.VisibleIndex = 3;
            this.gridColumn44.Width = 115;
            // 
            // repositoryItemTextEdit2DP6
            // 
            this.repositoryItemTextEdit2DP6.AutoHeight = false;
            this.repositoryItemTextEdit2DP6.Mask.EditMask = "n2";
            this.repositoryItemTextEdit2DP6.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP6.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP6.Name = "repositoryItemTextEdit2DP6";
            // 
            // gridColumn45
            // 
            this.gridColumn45.Caption = "Expected Duration Descriptor";
            this.gridColumn45.FieldName = "ExpectedDurationUnitsDescriptorID";
            this.gridColumn45.Name = "gridColumn45";
            this.gridColumn45.OptionsColumn.AllowEdit = false;
            this.gridColumn45.OptionsColumn.AllowFocus = false;
            this.gridColumn45.OptionsColumn.ReadOnly = true;
            this.gridColumn45.Visible = true;
            this.gridColumn45.VisibleIndex = 4;
            this.gridColumn45.Width = 162;
            // 
            // gridColumn46
            // 
            this.gridColumn46.Caption = "Schedule Sent Date";
            this.gridColumn46.FieldName = "ScheduleSentDate";
            this.gridColumn46.Name = "gridColumn46";
            this.gridColumn46.OptionsColumn.AllowEdit = false;
            this.gridColumn46.OptionsColumn.AllowFocus = false;
            this.gridColumn46.OptionsColumn.ReadOnly = true;
            this.gridColumn46.Width = 115;
            // 
            // gridColumn47
            // 
            this.gridColumn47.Caption = "Actual Start Date";
            this.gridColumn47.FieldName = "ActualStartDate";
            this.gridColumn47.Name = "gridColumn47";
            this.gridColumn47.OptionsColumn.AllowEdit = false;
            this.gridColumn47.OptionsColumn.AllowFocus = false;
            this.gridColumn47.OptionsColumn.ReadOnly = true;
            this.gridColumn47.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn47.Width = 104;
            // 
            // gridColumn48
            // 
            this.gridColumn48.Caption = "Actual End Date";
            this.gridColumn48.FieldName = "ActualEndDate";
            this.gridColumn48.Name = "gridColumn48";
            this.gridColumn48.OptionsColumn.AllowEdit = false;
            this.gridColumn48.OptionsColumn.AllowFocus = false;
            this.gridColumn48.OptionsColumn.ReadOnly = true;
            this.gridColumn48.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn48.Width = 98;
            // 
            // gridColumn49
            // 
            this.gridColumn49.Caption = "Actual Duration Units";
            this.gridColumn49.FieldName = "ActualDurationUnits";
            this.gridColumn49.Name = "gridColumn49";
            this.gridColumn49.OptionsColumn.AllowEdit = false;
            this.gridColumn49.OptionsColumn.AllowFocus = false;
            this.gridColumn49.OptionsColumn.ReadOnly = true;
            this.gridColumn49.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn49.Width = 122;
            // 
            // gridColumn50
            // 
            this.gridColumn50.Caption = "Actual Duration Unit Descriptor";
            this.gridColumn50.FieldName = "ActualDurationUnitsDescriptionID";
            this.gridColumn50.Name = "gridColumn50";
            this.gridColumn50.OptionsColumn.AllowEdit = false;
            this.gridColumn50.OptionsColumn.AllowFocus = false;
            this.gridColumn50.OptionsColumn.ReadOnly = true;
            this.gridColumn50.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn50.Width = 169;
            // 
            // gridColumn51
            // 
            this.gridColumn51.Caption = "Cost - Total Material Ex VAT";
            this.gridColumn51.ColumnEdit = this.repositoryItemTextEditCurrency6;
            this.gridColumn51.FieldName = "CostTotalMaterialExVAT";
            this.gridColumn51.Name = "gridColumn51";
            this.gridColumn51.OptionsColumn.AllowEdit = false;
            this.gridColumn51.OptionsColumn.AllowFocus = false;
            this.gridColumn51.OptionsColumn.ReadOnly = true;
            this.gridColumn51.Width = 155;
            // 
            // repositoryItemTextEditCurrency6
            // 
            this.repositoryItemTextEditCurrency6.AutoHeight = false;
            this.repositoryItemTextEditCurrency6.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency6.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency6.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency6.Name = "repositoryItemTextEditCurrency6";
            // 
            // gridColumn52
            // 
            this.gridColumn52.Caption = "Cost - Total Material VAT Rate";
            this.gridColumn52.ColumnEdit = this.repositoryItemTextEditPercentage6;
            this.gridColumn52.FieldName = "CostTotalMaterialVATRate";
            this.gridColumn52.Name = "gridColumn52";
            this.gridColumn52.OptionsColumn.AllowEdit = false;
            this.gridColumn52.OptionsColumn.AllowFocus = false;
            this.gridColumn52.OptionsColumn.ReadOnly = true;
            this.gridColumn52.Width = 166;
            // 
            // repositoryItemTextEditPercentage6
            // 
            this.repositoryItemTextEditPercentage6.AutoHeight = false;
            this.repositoryItemTextEditPercentage6.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage6.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage6.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage6.Name = "repositoryItemTextEditPercentage6";
            // 
            // gridColumn147
            // 
            this.gridColumn147.Caption = "Cost - Total Material VAT";
            this.gridColumn147.ColumnEdit = this.repositoryItemTextEditCurrency6;
            this.gridColumn147.FieldName = "CostTotalMaterialVAT";
            this.gridColumn147.Name = "gridColumn147";
            this.gridColumn147.OptionsColumn.AllowEdit = false;
            this.gridColumn147.OptionsColumn.AllowFocus = false;
            this.gridColumn147.OptionsColumn.ReadOnly = true;
            this.gridColumn147.Width = 140;
            // 
            // gridColumn148
            // 
            this.gridColumn148.Caption = "Cost - Total Material Value";
            this.gridColumn148.ColumnEdit = this.repositoryItemTextEditCurrency6;
            this.gridColumn148.FieldName = "CostTotalMaterialCost";
            this.gridColumn148.Name = "gridColumn148";
            this.gridColumn148.OptionsColumn.AllowEdit = false;
            this.gridColumn148.OptionsColumn.AllowFocus = false;
            this.gridColumn148.OptionsColumn.ReadOnly = true;
            this.gridColumn148.Width = 147;
            // 
            // gridColumn149
            // 
            this.gridColumn149.Caption = "Sell - Total Material Ex VAT";
            this.gridColumn149.FieldName = "SellTotalMaterialExVAT";
            this.gridColumn149.Name = "gridColumn149";
            this.gridColumn149.OptionsColumn.AllowEdit = false;
            this.gridColumn149.OptionsColumn.AllowFocus = false;
            this.gridColumn149.OptionsColumn.ReadOnly = true;
            this.gridColumn149.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn149.Width = 149;
            // 
            // gridColumn150
            // 
            this.gridColumn150.Caption = "Sell - Total Material VAT Rate";
            this.gridColumn150.FieldName = "SellTotalMaterialVATRate";
            this.gridColumn150.Name = "gridColumn150";
            this.gridColumn150.OptionsColumn.AllowEdit = false;
            this.gridColumn150.OptionsColumn.AllowFocus = false;
            this.gridColumn150.OptionsColumn.ReadOnly = true;
            this.gridColumn150.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn150.Width = 160;
            // 
            // gridColumn151
            // 
            this.gridColumn151.Caption = "Sell - Total Material VAT";
            this.gridColumn151.FieldName = "SellTotalMaterialVAT";
            this.gridColumn151.Name = "gridColumn151";
            this.gridColumn151.OptionsColumn.AllowEdit = false;
            this.gridColumn151.OptionsColumn.AllowFocus = false;
            this.gridColumn151.OptionsColumn.ReadOnly = true;
            this.gridColumn151.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn151.Width = 134;
            // 
            // gridColumn152
            // 
            this.gridColumn152.Caption = "Sell - Total Material Value";
            this.gridColumn152.FieldName = "SellTotalMaterialCost";
            this.gridColumn152.Name = "gridColumn152";
            this.gridColumn152.OptionsColumn.AllowEdit = false;
            this.gridColumn152.OptionsColumn.AllowFocus = false;
            this.gridColumn152.OptionsColumn.ReadOnly = true;
            this.gridColumn152.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn152.Width = 141;
            // 
            // gridColumn153
            // 
            this.gridColumn153.Caption = "Cost - Total Equipment Value";
            this.gridColumn153.ColumnEdit = this.repositoryItemTextEditCurrency6;
            this.gridColumn153.FieldName = "CostTotalEquipmentExVAT";
            this.gridColumn153.Name = "gridColumn153";
            this.gridColumn153.OptionsColumn.AllowEdit = false;
            this.gridColumn153.OptionsColumn.AllowFocus = false;
            this.gridColumn153.OptionsColumn.ReadOnly = true;
            this.gridColumn153.Width = 159;
            // 
            // gridColumn154
            // 
            this.gridColumn154.Caption = "Cost - Total Equipment VAT Rate";
            this.gridColumn154.ColumnEdit = this.repositoryItemTextEditPercentage6;
            this.gridColumn154.FieldName = "CostTotalEquipmentVATRate";
            this.gridColumn154.Name = "gridColumn154";
            this.gridColumn154.OptionsColumn.AllowEdit = false;
            this.gridColumn154.OptionsColumn.AllowFocus = false;
            this.gridColumn154.OptionsColumn.ReadOnly = true;
            this.gridColumn154.Width = 178;
            // 
            // gridColumn155
            // 
            this.gridColumn155.Caption = "Cost - Total Equipment VAT";
            this.gridColumn155.ColumnEdit = this.repositoryItemTextEditCurrency6;
            this.gridColumn155.FieldName = "CostTotalEquipmentVAT";
            this.gridColumn155.Name = "gridColumn155";
            this.gridColumn155.OptionsColumn.AllowEdit = false;
            this.gridColumn155.OptionsColumn.AllowFocus = false;
            this.gridColumn155.OptionsColumn.ReadOnly = true;
            this.gridColumn155.Width = 154;
            // 
            // gridColumn156
            // 
            this.gridColumn156.Caption = "Cost - Total Equipment Value";
            this.gridColumn156.ColumnEdit = this.repositoryItemTextEditCurrency6;
            this.gridColumn156.FieldName = "CostTotalEquipmentCost";
            this.gridColumn156.Name = "gridColumn156";
            this.gridColumn156.OptionsColumn.AllowEdit = false;
            this.gridColumn156.OptionsColumn.AllowFocus = false;
            this.gridColumn156.OptionsColumn.ReadOnly = true;
            this.gridColumn156.Width = 159;
            // 
            // gridColumn157
            // 
            this.gridColumn157.Caption = "Sell - Total Equipment Ex VAT";
            this.gridColumn157.FieldName = "SellTotalEquipmentExVAT";
            this.gridColumn157.Name = "gridColumn157";
            this.gridColumn157.OptionsColumn.AllowEdit = false;
            this.gridColumn157.OptionsColumn.AllowFocus = false;
            this.gridColumn157.OptionsColumn.ReadOnly = true;
            this.gridColumn157.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn157.Width = 161;
            // 
            // gridColumn158
            // 
            this.gridColumn158.Caption = "Sell - Total Equipment VAT Rate";
            this.gridColumn158.FieldName = "SellTotalEquipmentVATRate";
            this.gridColumn158.Name = "gridColumn158";
            this.gridColumn158.OptionsColumn.AllowEdit = false;
            this.gridColumn158.OptionsColumn.AllowFocus = false;
            this.gridColumn158.OptionsColumn.ReadOnly = true;
            this.gridColumn158.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn158.Width = 172;
            // 
            // gridColumn159
            // 
            this.gridColumn159.Caption = "Sell - Total Equipment VAT";
            this.gridColumn159.FieldName = "SellTotalEquipmentVAT";
            this.gridColumn159.Name = "gridColumn159";
            this.gridColumn159.OptionsColumn.AllowEdit = false;
            this.gridColumn159.OptionsColumn.AllowFocus = false;
            this.gridColumn159.OptionsColumn.ReadOnly = true;
            this.gridColumn159.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn159.Width = 146;
            // 
            // gridColumn160
            // 
            this.gridColumn160.Caption = "Sell - Total Equipment Value";
            this.gridColumn160.FieldName = "SellTotalEquipmentCost";
            this.gridColumn160.Name = "gridColumn160";
            this.gridColumn160.OptionsColumn.AllowEdit = false;
            this.gridColumn160.OptionsColumn.AllowFocus = false;
            this.gridColumn160.OptionsColumn.ReadOnly = true;
            this.gridColumn160.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn160.Width = 153;
            // 
            // gridColumn161
            // 
            this.gridColumn161.Caption = "Cost - Total Labour Ex VAT";
            this.gridColumn161.ColumnEdit = this.repositoryItemTextEditCurrency6;
            this.gridColumn161.FieldName = "CostTotalLabourExVAT";
            this.gridColumn161.Name = "gridColumn161";
            this.gridColumn161.OptionsColumn.AllowEdit = false;
            this.gridColumn161.OptionsColumn.AllowFocus = false;
            this.gridColumn161.OptionsColumn.ReadOnly = true;
            this.gridColumn161.Width = 150;
            // 
            // gridColumn162
            // 
            this.gridColumn162.Caption = "Cost - Total Labour VAT Rate";
            this.gridColumn162.ColumnEdit = this.repositoryItemTextEditPercentage6;
            this.gridColumn162.FieldName = "CostTotalLabourVATRate";
            this.gridColumn162.Name = "gridColumn162";
            this.gridColumn162.OptionsColumn.AllowEdit = false;
            this.gridColumn162.OptionsColumn.AllowFocus = false;
            this.gridColumn162.OptionsColumn.ReadOnly = true;
            this.gridColumn162.Width = 161;
            // 
            // gridColumn163
            // 
            this.gridColumn163.Caption = "Cost - Total Labour VAT";
            this.gridColumn163.ColumnEdit = this.repositoryItemTextEditCurrency6;
            this.gridColumn163.FieldName = "CostTotalLabourVAT";
            this.gridColumn163.Name = "gridColumn163";
            this.gridColumn163.OptionsColumn.AllowEdit = false;
            this.gridColumn163.OptionsColumn.AllowFocus = false;
            this.gridColumn163.OptionsColumn.ReadOnly = true;
            this.gridColumn163.Width = 135;
            // 
            // gridColumn164
            // 
            this.gridColumn164.Caption = "Cost - Total Labour Value";
            this.gridColumn164.ColumnEdit = this.repositoryItemTextEditCurrency6;
            this.gridColumn164.FieldName = "CostTotalLabourCost";
            this.gridColumn164.Name = "gridColumn164";
            this.gridColumn164.OptionsColumn.AllowEdit = false;
            this.gridColumn164.OptionsColumn.AllowFocus = false;
            this.gridColumn164.OptionsColumn.ReadOnly = true;
            this.gridColumn164.Width = 142;
            // 
            // gridColumn165
            // 
            this.gridColumn165.Caption = "Sell - Total Labour Ex VAT";
            this.gridColumn165.FieldName = "SellTotalLabourExVAT";
            this.gridColumn165.Name = "gridColumn165";
            this.gridColumn165.OptionsColumn.AllowEdit = false;
            this.gridColumn165.OptionsColumn.AllowFocus = false;
            this.gridColumn165.OptionsColumn.ReadOnly = true;
            this.gridColumn165.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn165.Width = 144;
            // 
            // gridColumn166
            // 
            this.gridColumn166.Caption = "Sell - Total Labour VAT Rate";
            this.gridColumn166.FieldName = "SellTotalLabourVATRate";
            this.gridColumn166.Name = "gridColumn166";
            this.gridColumn166.OptionsColumn.AllowEdit = false;
            this.gridColumn166.OptionsColumn.AllowFocus = false;
            this.gridColumn166.OptionsColumn.ReadOnly = true;
            this.gridColumn166.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn166.Width = 155;
            // 
            // gridColumn167
            // 
            this.gridColumn167.Caption = "Sell - Total Labour VAT";
            this.gridColumn167.FieldName = "SellTotalLabourVAT";
            this.gridColumn167.Name = "gridColumn167";
            this.gridColumn167.OptionsColumn.AllowEdit = false;
            this.gridColumn167.OptionsColumn.AllowFocus = false;
            this.gridColumn167.OptionsColumn.ReadOnly = true;
            this.gridColumn167.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn167.Width = 129;
            // 
            // gridColumn168
            // 
            this.gridColumn168.Caption = "Sell - Total Labour Value";
            this.gridColumn168.FieldName = "SellTotalLabourCost";
            this.gridColumn168.Name = "gridColumn168";
            this.gridColumn168.OptionsColumn.AllowEdit = false;
            this.gridColumn168.OptionsColumn.AllowFocus = false;
            this.gridColumn168.OptionsColumn.ReadOnly = true;
            this.gridColumn168.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn168.Width = 136;
            // 
            // gridColumn169
            // 
            this.gridColumn169.Caption = "Cost - Total Ex VAT";
            this.gridColumn169.ColumnEdit = this.repositoryItemTextEditCurrency6;
            this.gridColumn169.FieldName = "CostTotalCostExVAT";
            this.gridColumn169.Name = "gridColumn169";
            this.gridColumn169.OptionsColumn.AllowEdit = false;
            this.gridColumn169.OptionsColumn.AllowFocus = false;
            this.gridColumn169.OptionsColumn.ReadOnly = true;
            this.gridColumn169.Width = 114;
            // 
            // gridColumn170
            // 
            this.gridColumn170.Caption = "Cost - Total VAT";
            this.gridColumn170.ColumnEdit = this.repositoryItemTextEditCurrency6;
            this.gridColumn170.FieldName = "CostTotalCostVAT";
            this.gridColumn170.Name = "gridColumn170";
            this.gridColumn170.OptionsColumn.AllowEdit = false;
            this.gridColumn170.OptionsColumn.AllowFocus = false;
            this.gridColumn170.OptionsColumn.ReadOnly = true;
            this.gridColumn170.Width = 99;
            // 
            // gridColumn171
            // 
            this.gridColumn171.Caption = "Cost - Total Value";
            this.gridColumn171.ColumnEdit = this.repositoryItemTextEditCurrency6;
            this.gridColumn171.FieldName = "CostTotalCost";
            this.gridColumn171.Name = "gridColumn171";
            this.gridColumn171.OptionsColumn.AllowEdit = false;
            this.gridColumn171.OptionsColumn.AllowFocus = false;
            this.gridColumn171.OptionsColumn.ReadOnly = true;
            this.gridColumn171.Width = 106;
            // 
            // gridColumn172
            // 
            this.gridColumn172.Caption = "Sell - Total Ex VAT";
            this.gridColumn172.FieldName = "SellTotalCostExVAT";
            this.gridColumn172.Name = "gridColumn172";
            this.gridColumn172.OptionsColumn.AllowEdit = false;
            this.gridColumn172.OptionsColumn.AllowFocus = false;
            this.gridColumn172.OptionsColumn.ReadOnly = true;
            this.gridColumn172.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn172.Width = 108;
            // 
            // gridColumn173
            // 
            this.gridColumn173.Caption = "Sell - Total VAT";
            this.gridColumn173.FieldName = "SellTotalCostVAT";
            this.gridColumn173.Name = "gridColumn173";
            this.gridColumn173.OptionsColumn.AllowEdit = false;
            this.gridColumn173.OptionsColumn.AllowFocus = false;
            this.gridColumn173.OptionsColumn.ReadOnly = true;
            this.gridColumn173.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn173.Width = 93;
            // 
            // gridColumn174
            // 
            this.gridColumn174.Caption = "Sell - Total Value";
            this.gridColumn174.FieldName = "SellTotalCost";
            this.gridColumn174.Name = "gridColumn174";
            this.gridColumn174.OptionsColumn.AllowEdit = false;
            this.gridColumn174.OptionsColumn.AllowFocus = false;
            this.gridColumn174.OptionsColumn.ReadOnly = true;
            this.gridColumn174.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn174.Width = 100;
            // 
            // gridColumn175
            // 
            this.gridColumn175.Caption = "Client Invoice ID";
            this.gridColumn175.FieldName = "ClientInvoiceID";
            this.gridColumn175.Name = "gridColumn175";
            this.gridColumn175.OptionsColumn.AllowEdit = false;
            this.gridColumn175.OptionsColumn.AllowFocus = false;
            this.gridColumn175.OptionsColumn.ReadOnly = true;
            this.gridColumn175.Width = 100;
            // 
            // gridColumn176
            // 
            this.gridColumn176.Caption = "Client Invoiced Date";
            this.gridColumn176.FieldName = "DateClientInvoiced";
            this.gridColumn176.Name = "gridColumn176";
            this.gridColumn176.OptionsColumn.AllowEdit = false;
            this.gridColumn176.OptionsColumn.AllowFocus = false;
            this.gridColumn176.OptionsColumn.ReadOnly = true;
            this.gridColumn176.Width = 118;
            // 
            // gridColumn177
            // 
            this.gridColumn177.Caption = "Self-Billing Invoice ID";
            this.gridColumn177.FieldName = "SelfBillingInvoiceID";
            this.gridColumn177.Name = "gridColumn177";
            this.gridColumn177.OptionsColumn.AllowEdit = false;
            this.gridColumn177.OptionsColumn.AllowFocus = false;
            this.gridColumn177.OptionsColumn.ReadOnly = true;
            this.gridColumn177.Width = 121;
            // 
            // gridColumn178
            // 
            this.gridColumn178.Caption = "Self-Billing Invoice Received Date";
            this.gridColumn178.FieldName = "SelfBillingInvoiceReceivedDate";
            this.gridColumn178.Name = "gridColumn178";
            this.gridColumn178.OptionsColumn.AllowEdit = false;
            this.gridColumn178.OptionsColumn.AllowFocus = false;
            this.gridColumn178.OptionsColumn.ReadOnly = true;
            this.gridColumn178.Width = 180;
            // 
            // gridColumn179
            // 
            this.gridColumn179.Caption = "Self-Billing Invoice Paid Date";
            this.gridColumn179.FieldName = "SelfBillingInvoicePaidDate";
            this.gridColumn179.Name = "gridColumn179";
            this.gridColumn179.OptionsColumn.AllowEdit = false;
            this.gridColumn179.OptionsColumn.AllowFocus = false;
            this.gridColumn179.OptionsColumn.ReadOnly = true;
            this.gridColumn179.Width = 156;
            // 
            // gridColumn180
            // 
            this.gridColumn180.Caption = "Self-Billing Invoice Amount Paid";
            this.gridColumn180.FieldName = "SelfBillingInvoiceAmountPaid";
            this.gridColumn180.Name = "gridColumn180";
            this.gridColumn180.OptionsColumn.AllowEdit = false;
            this.gridColumn180.OptionsColumn.AllowFocus = false;
            this.gridColumn180.OptionsColumn.ReadOnly = true;
            this.gridColumn180.Width = 170;
            // 
            // gridColumn181
            // 
            this.gridColumn181.Caption = "Do Not Pay Contractor";
            this.gridColumn181.FieldName = "DoNotPayContractor";
            this.gridColumn181.Name = "gridColumn181";
            this.gridColumn181.OptionsColumn.AllowEdit = false;
            this.gridColumn181.OptionsColumn.AllowFocus = false;
            this.gridColumn181.OptionsColumn.ReadOnly = true;
            this.gridColumn181.Width = 130;
            // 
            // gridColumn182
            // 
            this.gridColumn182.Caption = "Do Not Invoice Client";
            this.gridColumn182.FieldName = "DoNotInvoiceClient";
            this.gridColumn182.Name = "gridColumn182";
            this.gridColumn182.OptionsColumn.AllowEdit = false;
            this.gridColumn182.OptionsColumn.AllowFocus = false;
            this.gridColumn182.OptionsColumn.ReadOnly = true;
            this.gridColumn182.Width = 122;
            // 
            // gridColumn183
            // 
            this.gridColumn183.Caption = "Remarks";
            this.gridColumn183.ColumnEdit = this.repositoryItemMemoExEdit7;
            this.gridColumn183.FieldName = "Remarks";
            this.gridColumn183.Name = "gridColumn183";
            this.gridColumn183.OptionsColumn.ReadOnly = true;
            this.gridColumn183.Visible = true;
            this.gridColumn183.VisibleIndex = 10;
            // 
            // repositoryItemMemoExEdit7
            // 
            this.repositoryItemMemoExEdit7.AutoHeight = false;
            this.repositoryItemMemoExEdit7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit7.Name = "repositoryItemMemoExEdit7";
            this.repositoryItemMemoExEdit7.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit7.ShowIcon = false;
            // 
            // gridColumn184
            // 
            this.gridColumn184.Caption = "Route Order";
            this.gridColumn184.FieldName = "RouteOrder";
            this.gridColumn184.Name = "gridColumn184";
            this.gridColumn184.OptionsColumn.AllowEdit = false;
            this.gridColumn184.OptionsColumn.AllowFocus = false;
            this.gridColumn184.OptionsColumn.ReadOnly = true;
            this.gridColumn184.Width = 81;
            // 
            // gridColumn185
            // 
            this.gridColumn185.Caption = "Start Latitude";
            this.gridColumn185.FieldName = "StartLatitude";
            this.gridColumn185.Name = "gridColumn185";
            this.gridColumn185.OptionsColumn.AllowEdit = false;
            this.gridColumn185.OptionsColumn.AllowFocus = false;
            this.gridColumn185.OptionsColumn.ReadOnly = true;
            this.gridColumn185.Width = 87;
            // 
            // gridColumn186
            // 
            this.gridColumn186.Caption = "Start Longitude";
            this.gridColumn186.FieldName = "StartLongitude";
            this.gridColumn186.Name = "gridColumn186";
            this.gridColumn186.OptionsColumn.AllowEdit = false;
            this.gridColumn186.OptionsColumn.AllowFocus = false;
            this.gridColumn186.OptionsColumn.ReadOnly = true;
            this.gridColumn186.Width = 95;
            // 
            // gridColumn187
            // 
            this.gridColumn187.Caption = "Finish Latitude";
            this.gridColumn187.FieldName = "FinishLatitude";
            this.gridColumn187.Name = "gridColumn187";
            this.gridColumn187.OptionsColumn.AllowEdit = false;
            this.gridColumn187.OptionsColumn.AllowFocus = false;
            this.gridColumn187.OptionsColumn.ReadOnly = true;
            this.gridColumn187.Width = 90;
            // 
            // gridColumn188
            // 
            this.gridColumn188.Caption = "Finish Longitude";
            this.gridColumn188.FieldName = "FinishLongitude";
            this.gridColumn188.Name = "gridColumn188";
            this.gridColumn188.OptionsColumn.AllowEdit = false;
            this.gridColumn188.OptionsColumn.AllowFocus = false;
            this.gridColumn188.OptionsColumn.ReadOnly = true;
            this.gridColumn188.Width = 98;
            // 
            // gridColumn189
            // 
            this.gridColumn189.Caption = "Tender ID";
            this.gridColumn189.FieldName = "TenderID";
            this.gridColumn189.Name = "gridColumn189";
            this.gridColumn189.OptionsColumn.AllowEdit = false;
            this.gridColumn189.OptionsColumn.AllowFocus = false;
            this.gridColumn189.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn190
            // 
            this.gridColumn190.Caption = "Tender Job ID";
            this.gridColumn190.FieldName = "TenderJobID";
            this.gridColumn190.Name = "gridColumn190";
            this.gridColumn190.OptionsColumn.AllowEdit = false;
            this.gridColumn190.OptionsColumn.AllowFocus = false;
            this.gridColumn190.OptionsColumn.ReadOnly = true;
            this.gridColumn190.Width = 89;
            // 
            // gridColumn191
            // 
            this.gridColumn191.Caption = "Min Days From Last Visit";
            this.gridColumn191.ColumnEdit = this.repositoryItemTextEditInteger6;
            this.gridColumn191.FieldName = "MinimumDaysFromLastVisit";
            this.gridColumn191.Name = "gridColumn191";
            this.gridColumn191.OptionsColumn.AllowEdit = false;
            this.gridColumn191.OptionsColumn.AllowFocus = false;
            this.gridColumn191.OptionsColumn.ReadOnly = true;
            this.gridColumn191.Visible = true;
            this.gridColumn191.VisibleIndex = 6;
            this.gridColumn191.Width = 136;
            // 
            // repositoryItemTextEditInteger6
            // 
            this.repositoryItemTextEditInteger6.AutoHeight = false;
            this.repositoryItemTextEditInteger6.Mask.EditMask = "n0";
            this.repositoryItemTextEditInteger6.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditInteger6.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditInteger6.Name = "repositoryItemTextEditInteger6";
            // 
            // gridColumn192
            // 
            this.gridColumn192.Caption = "Max Days From Last Visit";
            this.gridColumn192.ColumnEdit = this.repositoryItemTextEditInteger6;
            this.gridColumn192.FieldName = "MaximumDaysFromLastVisit";
            this.gridColumn192.Name = "gridColumn192";
            this.gridColumn192.OptionsColumn.AllowEdit = false;
            this.gridColumn192.OptionsColumn.AllowFocus = false;
            this.gridColumn192.OptionsColumn.ReadOnly = true;
            this.gridColumn192.Visible = true;
            this.gridColumn192.VisibleIndex = 7;
            this.gridColumn192.Width = 140;
            // 
            // gridColumn193
            // 
            this.gridColumn193.Caption = "Days Leeway";
            this.gridColumn193.ColumnEdit = this.repositoryItemTextEditInteger6;
            this.gridColumn193.FieldName = "DaysLeeway";
            this.gridColumn193.Name = "gridColumn193";
            this.gridColumn193.OptionsColumn.AllowEdit = false;
            this.gridColumn193.OptionsColumn.AllowFocus = false;
            this.gridColumn193.OptionsColumn.ReadOnly = true;
            this.gridColumn193.Visible = true;
            this.gridColumn193.VisibleIndex = 8;
            this.gridColumn193.Width = 85;
            // 
            // gridColumn194
            // 
            this.gridColumn194.Caption = "Billing Centre Code";
            this.gridColumn194.FieldName = "BillingCentreCodeID";
            this.gridColumn194.Name = "gridColumn194";
            this.gridColumn194.OptionsColumn.AllowEdit = false;
            this.gridColumn194.OptionsColumn.AllowFocus = false;
            this.gridColumn194.OptionsColumn.ReadOnly = true;
            this.gridColumn194.Width = 111;
            // 
            // gridColumn195
            // 
            this.gridColumn195.Caption = "Site Contract ID";
            this.gridColumn195.FieldName = "SiteContractID";
            this.gridColumn195.Name = "gridColumn195";
            this.gridColumn195.OptionsColumn.AllowEdit = false;
            this.gridColumn195.OptionsColumn.AllowFocus = false;
            this.gridColumn195.OptionsColumn.ReadOnly = true;
            this.gridColumn195.Width = 98;
            // 
            // gridColumn196
            // 
            this.gridColumn196.Caption = "Client Contract ID";
            this.gridColumn196.FieldName = "ClientContractID";
            this.gridColumn196.Name = "gridColumn196";
            this.gridColumn196.OptionsColumn.AllowEdit = false;
            this.gridColumn196.OptionsColumn.AllowFocus = false;
            this.gridColumn196.OptionsColumn.ReadOnly = true;
            this.gridColumn196.Width = 107;
            // 
            // gridColumn197
            // 
            this.gridColumn197.Caption = "Site ID";
            this.gridColumn197.FieldName = "SiteID";
            this.gridColumn197.Name = "gridColumn197";
            this.gridColumn197.OptionsColumn.AllowEdit = false;
            this.gridColumn197.OptionsColumn.AllowFocus = false;
            this.gridColumn197.OptionsColumn.ReadOnly = true;
            this.gridColumn197.Visible = true;
            this.gridColumn197.VisibleIndex = 1;
            this.gridColumn197.Width = 51;
            // 
            // gridColumn198
            // 
            this.gridColumn198.Caption = "Client ID";
            this.gridColumn198.FieldName = "ClientID";
            this.gridColumn198.Name = "gridColumn198";
            this.gridColumn198.OptionsColumn.AllowEdit = false;
            this.gridColumn198.OptionsColumn.AllowFocus = false;
            this.gridColumn198.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn199
            // 
            this.gridColumn199.Caption = "Contract Description";
            this.gridColumn199.FieldName = "ContractDescription";
            this.gridColumn199.Name = "gridColumn199";
            this.gridColumn199.OptionsColumn.AllowEdit = false;
            this.gridColumn199.OptionsColumn.AllowFocus = false;
            this.gridColumn199.OptionsColumn.ReadOnly = true;
            this.gridColumn199.Width = 119;
            // 
            // gridColumn200
            // 
            this.gridColumn200.Caption = "Client \\ Contract";
            this.gridColumn200.FieldName = "ClientNameContractDescription";
            this.gridColumn200.Name = "gridColumn200";
            this.gridColumn200.OptionsColumn.AllowEdit = false;
            this.gridColumn200.OptionsColumn.AllowFocus = false;
            this.gridColumn200.OptionsColumn.ReadOnly = true;
            this.gridColumn200.Visible = true;
            this.gridColumn200.VisibleIndex = 14;
            this.gridColumn200.Width = 416;
            // 
            // gridColumn201
            // 
            this.gridColumn201.Caption = "Client Name";
            this.gridColumn201.FieldName = "ClientName";
            this.gridColumn201.Name = "gridColumn201";
            this.gridColumn201.OptionsColumn.AllowEdit = false;
            this.gridColumn201.OptionsColumn.AllowFocus = false;
            this.gridColumn201.OptionsColumn.ReadOnly = true;
            this.gridColumn201.Width = 199;
            // 
            // gridColumn202
            // 
            this.gridColumn202.Caption = "Site Name";
            this.gridColumn202.FieldName = "SiteName";
            this.gridColumn202.Name = "gridColumn202";
            this.gridColumn202.OptionsColumn.AllowEdit = false;
            this.gridColumn202.OptionsColumn.AllowFocus = false;
            this.gridColumn202.OptionsColumn.ReadOnly = true;
            this.gridColumn202.Visible = true;
            this.gridColumn202.VisibleIndex = 15;
            this.gridColumn202.Width = 276;
            // 
            // gridColumn203
            // 
            this.gridColumn203.Caption = "Site Postcode";
            this.gridColumn203.FieldName = "SitePostcode";
            this.gridColumn203.Name = "gridColumn203";
            this.gridColumn203.OptionsColumn.AllowEdit = false;
            this.gridColumn203.OptionsColumn.AllowFocus = false;
            this.gridColumn203.OptionsColumn.ReadOnly = true;
            this.gridColumn203.Width = 86;
            // 
            // gridColumn204
            // 
            this.gridColumn204.Caption = "Site Latitude";
            this.gridColumn204.FieldName = "LocationX";
            this.gridColumn204.Name = "gridColumn204";
            this.gridColumn204.OptionsColumn.AllowEdit = false;
            this.gridColumn204.OptionsColumn.AllowFocus = false;
            this.gridColumn204.OptionsColumn.ReadOnly = true;
            this.gridColumn204.Width = 81;
            // 
            // gridColumn205
            // 
            this.gridColumn205.Caption = "Site Longitude";
            this.gridColumn205.FieldName = "LocationY";
            this.gridColumn205.Name = "gridColumn205";
            this.gridColumn205.OptionsColumn.AllowEdit = false;
            this.gridColumn205.OptionsColumn.AllowFocus = false;
            this.gridColumn205.OptionsColumn.ReadOnly = true;
            this.gridColumn205.Width = 89;
            // 
            // gridColumn206
            // 
            this.gridColumn206.Caption = "Created By";
            this.gridColumn206.FieldName = "CreatedByStaffName";
            this.gridColumn206.Name = "gridColumn206";
            this.gridColumn206.OptionsColumn.AllowEdit = false;
            this.gridColumn206.OptionsColumn.AllowFocus = false;
            this.gridColumn206.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn207
            // 
            this.gridColumn207.Caption = "Job Sub-Type";
            this.gridColumn207.FieldName = "JobSubTypeDescription";
            this.gridColumn207.Name = "gridColumn207";
            this.gridColumn207.OptionsColumn.AllowEdit = false;
            this.gridColumn207.OptionsColumn.AllowFocus = false;
            this.gridColumn207.OptionsColumn.ReadOnly = true;
            this.gridColumn207.Width = 87;
            // 
            // gridColumn208
            // 
            this.gridColumn208.Caption = "Job Type";
            this.gridColumn208.FieldName = "JobTypeDescription";
            this.gridColumn208.Name = "gridColumn208";
            this.gridColumn208.OptionsColumn.AllowEdit = false;
            this.gridColumn208.OptionsColumn.AllowFocus = false;
            this.gridColumn208.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn209
            // 
            this.gridColumn209.Caption = "Job Type \\ Sub-Type";
            this.gridColumn209.FieldName = "JobTypeJobSubTypeDescription";
            this.gridColumn209.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn209.Name = "gridColumn209";
            this.gridColumn209.OptionsColumn.AllowEdit = false;
            this.gridColumn209.OptionsColumn.AllowFocus = false;
            this.gridColumn209.OptionsColumn.ReadOnly = true;
            this.gridColumn209.Visible = true;
            this.gridColumn209.VisibleIndex = 0;
            this.gridColumn209.Width = 290;
            // 
            // gridColumn210
            // 
            this.gridColumn210.Caption = "Job Type ID";
            this.gridColumn210.FieldName = "JobTypeID";
            this.gridColumn210.Name = "gridColumn210";
            this.gridColumn210.OptionsColumn.AllowEdit = false;
            this.gridColumn210.OptionsColumn.AllowFocus = false;
            this.gridColumn210.OptionsColumn.ReadOnly = true;
            this.gridColumn210.Width = 79;
            // 
            // gridColumn211
            // 
            this.gridColumn211.Caption = "Visit #";
            this.gridColumn211.FieldName = "VisitNumber";
            this.gridColumn211.Name = "gridColumn211";
            this.gridColumn211.OptionsColumn.AllowEdit = false;
            this.gridColumn211.OptionsColumn.AllowFocus = false;
            this.gridColumn211.OptionsColumn.ReadOnly = true;
            this.gridColumn211.Visible = true;
            this.gridColumn211.VisibleIndex = 11;
            // 
            // colLabourCount1
            // 
            this.colLabourCount1.Caption = "Labour Count";
            this.colLabourCount1.ColumnEdit = this.repositoryItemTextEditInteger6;
            this.colLabourCount1.FieldName = "LabourCount";
            this.colLabourCount1.Name = "colLabourCount1";
            this.colLabourCount1.OptionsColumn.AllowEdit = false;
            this.colLabourCount1.OptionsColumn.AllowFocus = false;
            this.colLabourCount1.OptionsColumn.ReadOnly = true;
            this.colLabourCount1.Visible = true;
            this.colLabourCount1.VisibleIndex = 11;
            this.colLabourCount1.Width = 86;
            // 
            // colEquipmentCount1
            // 
            this.colEquipmentCount1.Caption = "Equipment Count";
            this.colEquipmentCount1.ColumnEdit = this.repositoryItemTextEditInteger6;
            this.colEquipmentCount1.FieldName = "EquipmentCount";
            this.colEquipmentCount1.Name = "colEquipmentCount1";
            this.colEquipmentCount1.OptionsColumn.AllowEdit = false;
            this.colEquipmentCount1.OptionsColumn.AllowFocus = false;
            this.colEquipmentCount1.OptionsColumn.ReadOnly = true;
            this.colEquipmentCount1.Width = 103;
            // 
            // colMaterialCount1
            // 
            this.colMaterialCount1.Caption = "Material Count";
            this.colMaterialCount1.ColumnEdit = this.repositoryItemTextEditInteger6;
            this.colMaterialCount1.FieldName = "MaterialCount";
            this.colMaterialCount1.Name = "colMaterialCount1";
            this.colMaterialCount1.OptionsColumn.AllowEdit = false;
            this.colMaterialCount1.OptionsColumn.AllowFocus = false;
            this.colMaterialCount1.OptionsColumn.ReadOnly = true;
            this.colMaterialCount1.Width = 91;
            // 
            // colVisitCostCalculationLevel
            // 
            this.colVisitCostCalculationLevel.Caption = "Cost Calculation Level ID";
            this.colVisitCostCalculationLevel.FieldName = "VisitCostCalculationLevelID";
            this.colVisitCostCalculationLevel.Name = "colVisitCostCalculationLevel";
            this.colVisitCostCalculationLevel.OptionsColumn.AllowEdit = false;
            this.colVisitCostCalculationLevel.OptionsColumn.AllowFocus = false;
            this.colVisitCostCalculationLevel.OptionsColumn.ReadOnly = true;
            this.colVisitCostCalculationLevel.Width = 138;
            // 
            // colVisitCostCalculationLevel1
            // 
            this.colVisitCostCalculationLevel1.Caption = "Cost Calculation Level";
            this.colVisitCostCalculationLevel1.FieldName = "VisitCostCalculationLevel";
            this.colVisitCostCalculationLevel1.Name = "colVisitCostCalculationLevel1";
            this.colVisitCostCalculationLevel1.OptionsColumn.AllowEdit = false;
            this.colVisitCostCalculationLevel1.OptionsColumn.AllowFocus = false;
            this.colVisitCostCalculationLevel1.OptionsColumn.ReadOnly = true;
            this.colVisitCostCalculationLevel1.Visible = true;
            this.colVisitCostCalculationLevel1.VisibleIndex = 12;
            this.colVisitCostCalculationLevel1.Width = 124;
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl1.Manager = this.barManager1;
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(724, 26);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // gridControl8
            // 
            this.gridControl8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl8.DataSource = this.sp06182OMJobLabourEditBindingSource;
            this.gridControl8.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl8.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl8.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 10, true, true, "Block Add New Records", "block add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Block Edit Selected Records", "block edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl8.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl8_EmbeddedNavigator_ButtonClick);
            this.gridControl8.Location = new System.Drawing.Point(0, 26);
            this.gridControl8.MainView = this.gridView8;
            this.gridControl8.MenuManager = this.barManager1;
            this.gridControl8.Name = "gridControl8";
            this.gridControl8.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEditContractorName,
            this.repositoryItemSpinEdit2DP8,
            this.repositoryItemGridLookUpEditUnitDescriptor,
            this.repositoryItemSpinEditCurrency8,
            this.repositoryItemSpinEditPercentage8,
            this.repositoryItemMemoExEdit9,
            this.repositoryItemTextEditDateTime8,
            this.repositoryItemTextEdit6,
            this.repositoryItemButtonEditLinkedToPersonType});
            this.gridControl8.Size = new System.Drawing.Size(724, 407);
            this.gridControl8.TabIndex = 21;
            this.gridControl8.UseEmbeddedNavigator = true;
            this.gridControl8.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView8});
            // 
            // sp06182OMJobLabourEditBindingSource
            // 
            this.sp06182OMJobLabourEditBindingSource.DataMember = "sp06182_OM_Job_Labour_Edit";
            this.sp06182OMJobLabourEditBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridView8
            // 
            this.gridView8.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colstrMode1,
            this.colstrRecordIDs1,
            this.colDummyJobID1,
            this.colLabourUsedID,
            this.colContractorID,
            this.colJobID1,
            this.colCostUnitsUsed,
            this.colCostUnitDescriptorID,
            this.colSellUnitsUsed,
            this.colSellUnitDescriptorID,
            this.colCostPerUnitExVat,
            this.colCostPerUnitVatRate,
            this.colSellPerUnitExVat,
            this.colSellPerUnitVatRate,
            this.colCostValueExVat,
            this.colCostValueVat,
            this.colCostValueTotal,
            this.colSellValueExVat,
            this.colSellValueVat,
            this.colSellValueTotal,
            this.colRemarks3,
            this.colClientID4,
            this.colSiteID3,
            this.colVisitID2,
            this.colJobExpectedStartDate,
            this.colClientName3,
            this.colSiteName3,
            this.colVisitNumber2,
            this.colJobTypeDescription2,
            this.colJobSubTypeDescription2,
            this.colContractorName,
            this.colCostUnitDescriptor,
            this.colSellUnitDescriptor,
            this.colLabourType,
            this.colCISPercentage,
            this.colCISValue,
            this.colPurchaseOrderID,
            this.colStartDateTime,
            this.colEndDateTime,
            this.colContractDescription3,
            this.colClientNameContractDescription1,
            this.colJobTypeJobSubTypeDescription1,
            this.colLatLongSiteDistance,
            this.colPostcodeSiteDistance,
            this.colLinkedToPersonType,
            this.colLinkedToPersonTypeID});
            this.gridView8.GridControl = this.gridControl8;
            this.gridView8.GroupCount = 4;
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView8.OptionsFilter.AllowFilterEditor = false;
            this.gridView8.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView8.OptionsFilter.AllowMRUFilterList = false;
            this.gridView8.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView8.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView8.OptionsFind.FindDelay = 2000;
            this.gridView8.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView8.OptionsLayout.StoreAppearance = true;
            this.gridView8.OptionsLayout.StoreFormatRules = true;
            this.gridView8.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView8.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView8.OptionsNavigation.AutoFocusNewRow = true;
            this.gridView8.OptionsSelection.MultiSelect = true;
            this.gridView8.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView8.OptionsView.ColumnAutoWidth = false;
            this.gridView8.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView8.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView8.OptionsView.ShowFooter = true;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            this.gridView8.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientNameContractDescription1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName3, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colVisitNumber2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colJobTypeJobSubTypeDescription1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView8.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView8.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView8.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView8.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView8.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView8.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView8.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView8_MouseUp);
            this.gridView8.GotFocus += new System.EventHandler(this.gridView8_GotFocus);
            // 
            // colstrMode1
            // 
            this.colstrMode1.FieldName = "strMode";
            this.colstrMode1.Name = "colstrMode1";
            this.colstrMode1.OptionsColumn.AllowEdit = false;
            this.colstrMode1.OptionsColumn.AllowFocus = false;
            this.colstrMode1.OptionsColumn.ReadOnly = true;
            this.colstrMode1.OptionsColumn.ShowInCustomizationForm = false;
            this.colstrMode1.OptionsColumn.ShowInExpressionEditor = false;
            // 
            // colstrRecordIDs1
            // 
            this.colstrRecordIDs1.FieldName = "strRecordIDs";
            this.colstrRecordIDs1.Name = "colstrRecordIDs1";
            this.colstrRecordIDs1.OptionsColumn.AllowEdit = false;
            this.colstrRecordIDs1.OptionsColumn.AllowFocus = false;
            this.colstrRecordIDs1.OptionsColumn.ReadOnly = true;
            this.colstrRecordIDs1.OptionsColumn.ShowInCustomizationForm = false;
            this.colstrRecordIDs1.OptionsColumn.ShowInExpressionEditor = false;
            this.colstrRecordIDs1.Width = 90;
            // 
            // colDummyJobID1
            // 
            this.colDummyJobID1.Caption = "Dummy Job ID";
            this.colDummyJobID1.FieldName = "DummyJobID";
            this.colDummyJobID1.Name = "colDummyJobID1";
            this.colDummyJobID1.OptionsColumn.AllowEdit = false;
            this.colDummyJobID1.OptionsColumn.AllowFocus = false;
            this.colDummyJobID1.OptionsColumn.ReadOnly = true;
            this.colDummyJobID1.Width = 90;
            // 
            // colLabourUsedID
            // 
            this.colLabourUsedID.Caption = "Labour Used ID ";
            this.colLabourUsedID.FieldName = "LabourUsedID";
            this.colLabourUsedID.Name = "colLabourUsedID";
            this.colLabourUsedID.OptionsColumn.AllowEdit = false;
            this.colLabourUsedID.OptionsColumn.AllowFocus = false;
            this.colLabourUsedID.OptionsColumn.ReadOnly = true;
            this.colLabourUsedID.Width = 98;
            // 
            // colContractorID
            // 
            this.colContractorID.Caption = "ContractorID";
            this.colContractorID.FieldName = "ContractorID";
            this.colContractorID.Name = "colContractorID";
            this.colContractorID.OptionsColumn.AllowEdit = false;
            this.colContractorID.OptionsColumn.AllowFocus = false;
            this.colContractorID.OptionsColumn.ReadOnly = true;
            this.colContractorID.Width = 84;
            // 
            // colJobID1
            // 
            this.colJobID1.Caption = "Job ID";
            this.colJobID1.FieldName = "JobID";
            this.colJobID1.Name = "colJobID1";
            this.colJobID1.OptionsColumn.AllowEdit = false;
            this.colJobID1.OptionsColumn.AllowFocus = false;
            this.colJobID1.OptionsColumn.ReadOnly = true;
            // 
            // colCostUnitsUsed
            // 
            this.colCostUnitsUsed.Caption = "Cost Units Used";
            this.colCostUnitsUsed.ColumnEdit = this.repositoryItemSpinEdit2DP8;
            this.colCostUnitsUsed.FieldName = "CostUnitsUsed";
            this.colCostUnitsUsed.Name = "colCostUnitsUsed";
            this.colCostUnitsUsed.Visible = true;
            this.colCostUnitsUsed.VisibleIndex = 3;
            this.colCostUnitsUsed.Width = 97;
            // 
            // repositoryItemSpinEdit2DP8
            // 
            this.repositoryItemSpinEdit2DP8.AutoHeight = false;
            this.repositoryItemSpinEdit2DP8.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit2DP8.Mask.EditMask = "n2";
            this.repositoryItemSpinEdit2DP8.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEdit2DP8.Name = "repositoryItemSpinEdit2DP8";
            this.repositoryItemSpinEdit2DP8.EditValueChanged += new System.EventHandler(this.repositoryItemSpinEdit2DP8_EditValueChanged);
            // 
            // colCostUnitDescriptorID
            // 
            this.colCostUnitDescriptorID.Caption = "Cost Unit Descriptor";
            this.colCostUnitDescriptorID.ColumnEdit = this.repositoryItemGridLookUpEditUnitDescriptor;
            this.colCostUnitDescriptorID.FieldName = "CostUnitDescriptorID";
            this.colCostUnitDescriptorID.Name = "colCostUnitDescriptorID";
            this.colCostUnitDescriptorID.Visible = true;
            this.colCostUnitDescriptorID.VisibleIndex = 4;
            this.colCostUnitDescriptorID.Width = 117;
            // 
            // repositoryItemGridLookUpEditUnitDescriptor
            // 
            this.repositoryItemGridLookUpEditUnitDescriptor.AutoHeight = false;
            this.repositoryItemGridLookUpEditUnitDescriptor.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditUnitDescriptor.DataSource = this.sp06101OMWorkUnitTypesPicklistBindingSource;
            this.repositoryItemGridLookUpEditUnitDescriptor.DisplayMember = "Description";
            this.repositoryItemGridLookUpEditUnitDescriptor.Name = "repositoryItemGridLookUpEditUnitDescriptor";
            this.repositoryItemGridLookUpEditUnitDescriptor.NullText = "";
            this.repositoryItemGridLookUpEditUnitDescriptor.PopupView = this.gridView9x;
            this.repositoryItemGridLookUpEditUnitDescriptor.ValueMember = "ID";
            // 
            // sp06101OMWorkUnitTypesPicklistBindingSource
            // 
            this.sp06101OMWorkUnitTypesPicklistBindingSource.DataMember = "sp06101_OM_Work_Unit_Types_Picklist";
            this.sp06101OMWorkUnitTypesPicklistBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // dataSet_OM_Contract
            // 
            this.dataSet_OM_Contract.DataSetName = "DataSet_OM_Contract";
            this.dataSet_OM_Contract.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView9x
            // 
            this.gridView9x.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn53,
            this.gridColumn54,
            this.gridColumn55});
            this.gridView9x.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.gridColumn53;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView9x.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView9x.Name = "gridView9x";
            this.gridView9x.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView9x.OptionsLayout.StoreAppearance = true;
            this.gridView9x.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView9x.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView9x.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView9x.OptionsView.ColumnAutoWidth = false;
            this.gridView9x.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView9x.OptionsView.ShowGroupPanel = false;
            this.gridView9x.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView9x.OptionsView.ShowIndicator = false;
            this.gridView9x.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn55, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn54
            // 
            this.gridColumn54.Caption = "Unit Descriptor";
            this.gridColumn54.FieldName = "Description";
            this.gridColumn54.Name = "gridColumn54";
            this.gridColumn54.OptionsColumn.AllowEdit = false;
            this.gridColumn54.OptionsColumn.AllowFocus = false;
            this.gridColumn54.OptionsColumn.ReadOnly = true;
            this.gridColumn54.Visible = true;
            this.gridColumn54.VisibleIndex = 0;
            this.gridColumn54.Width = 220;
            // 
            // gridColumn55
            // 
            this.gridColumn55.Caption = "Order";
            this.gridColumn55.FieldName = "RecordOrder";
            this.gridColumn55.Name = "gridColumn55";
            this.gridColumn55.OptionsColumn.AllowEdit = false;
            this.gridColumn55.OptionsColumn.AllowFocus = false;
            this.gridColumn55.OptionsColumn.ReadOnly = true;
            // 
            // colSellUnitsUsed
            // 
            this.colSellUnitsUsed.Caption = "Sell Units Used";
            this.colSellUnitsUsed.ColumnEdit = this.repositoryItemSpinEdit2DP8;
            this.colSellUnitsUsed.FieldName = "SellUnitsUsed";
            this.colSellUnitsUsed.Name = "colSellUnitsUsed";
            this.colSellUnitsUsed.Visible = true;
            this.colSellUnitsUsed.VisibleIndex = 10;
            this.colSellUnitsUsed.Width = 91;
            // 
            // colSellUnitDescriptorID
            // 
            this.colSellUnitDescriptorID.Caption = "Sell Unit Descriptor";
            this.colSellUnitDescriptorID.ColumnEdit = this.repositoryItemGridLookUpEditUnitDescriptor;
            this.colSellUnitDescriptorID.FieldName = "SellUnitDescriptorID";
            this.colSellUnitDescriptorID.Name = "colSellUnitDescriptorID";
            this.colSellUnitDescriptorID.Visible = true;
            this.colSellUnitDescriptorID.VisibleIndex = 11;
            this.colSellUnitDescriptorID.Width = 111;
            // 
            // colCostPerUnitExVat
            // 
            this.colCostPerUnitExVat.Caption = "Unit Cost Ex VAT";
            this.colCostPerUnitExVat.ColumnEdit = this.repositoryItemSpinEditCurrency8;
            this.colCostPerUnitExVat.FieldName = "CostPerUnitExVat";
            this.colCostPerUnitExVat.Name = "colCostPerUnitExVat";
            this.colCostPerUnitExVat.Visible = true;
            this.colCostPerUnitExVat.VisibleIndex = 5;
            this.colCostPerUnitExVat.Width = 102;
            // 
            // repositoryItemSpinEditCurrency8
            // 
            this.repositoryItemSpinEditCurrency8.AutoHeight = false;
            this.repositoryItemSpinEditCurrency8.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEditCurrency8.Mask.EditMask = "c";
            this.repositoryItemSpinEditCurrency8.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditCurrency8.Name = "repositoryItemSpinEditCurrency8";
            this.repositoryItemSpinEditCurrency8.EditValueChanged += new System.EventHandler(this.repositoryItemSpinEditCurrency8_EditValueChanged);
            // 
            // colCostPerUnitVatRate
            // 
            this.colCostPerUnitVatRate.Caption = "Unit Cost VAT Rate";
            this.colCostPerUnitVatRate.ColumnEdit = this.repositoryItemSpinEditPercentage8;
            this.colCostPerUnitVatRate.FieldName = "CostPerUnitVatRate";
            this.colCostPerUnitVatRate.Name = "colCostPerUnitVatRate";
            this.colCostPerUnitVatRate.Visible = true;
            this.colCostPerUnitVatRate.VisibleIndex = 6;
            this.colCostPerUnitVatRate.Width = 113;
            // 
            // repositoryItemSpinEditPercentage8
            // 
            this.repositoryItemSpinEditPercentage8.AutoHeight = false;
            this.repositoryItemSpinEditPercentage8.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEditPercentage8.Mask.EditMask = "P";
            this.repositoryItemSpinEditPercentage8.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditPercentage8.Name = "repositoryItemSpinEditPercentage8";
            this.repositoryItemSpinEditPercentage8.EditValueChanged += new System.EventHandler(this.repositoryItemSpinEditPercentage8_EditValueChanged);
            // 
            // colSellPerUnitExVat
            // 
            this.colSellPerUnitExVat.Caption = "Unit Sell Ex VAT";
            this.colSellPerUnitExVat.ColumnEdit = this.repositoryItemSpinEditCurrency8;
            this.colSellPerUnitExVat.FieldName = "SellPerUnitExVat";
            this.colSellPerUnitExVat.Name = "colSellPerUnitExVat";
            this.colSellPerUnitExVat.Visible = true;
            this.colSellPerUnitExVat.VisibleIndex = 12;
            this.colSellPerUnitExVat.Width = 96;
            // 
            // colSellPerUnitVatRate
            // 
            this.colSellPerUnitVatRate.Caption = "Unit Sell VAT Rate";
            this.colSellPerUnitVatRate.ColumnEdit = this.repositoryItemSpinEditPercentage8;
            this.colSellPerUnitVatRate.FieldName = "SellPerUnitVatRate";
            this.colSellPerUnitVatRate.Name = "colSellPerUnitVatRate";
            this.colSellPerUnitVatRate.Visible = true;
            this.colSellPerUnitVatRate.VisibleIndex = 13;
            this.colSellPerUnitVatRate.Width = 107;
            // 
            // colCostValueExVat
            // 
            this.colCostValueExVat.Caption = "Total Cost Ex VAT";
            this.colCostValueExVat.ColumnEdit = this.repositoryItemSpinEditCurrency8;
            this.colCostValueExVat.FieldName = "CostValueExVat";
            this.colCostValueExVat.Name = "colCostValueExVat";
            this.colCostValueExVat.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostValueExVat", "{0:c}")});
            this.colCostValueExVat.Visible = true;
            this.colCostValueExVat.VisibleIndex = 7;
            this.colCostValueExVat.Width = 107;
            // 
            // colCostValueVat
            // 
            this.colCostValueVat.Caption = "Total Cost VAT";
            this.colCostValueVat.ColumnEdit = this.repositoryItemSpinEditCurrency8;
            this.colCostValueVat.FieldName = "CostValueVat";
            this.colCostValueVat.Name = "colCostValueVat";
            this.colCostValueVat.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostValueVat", "{0:c}")});
            this.colCostValueVat.Visible = true;
            this.colCostValueVat.VisibleIndex = 8;
            this.colCostValueVat.Width = 92;
            // 
            // colCostValueTotal
            // 
            this.colCostValueTotal.Caption = "Total Cost";
            this.colCostValueTotal.ColumnEdit = this.repositoryItemSpinEditCurrency8;
            this.colCostValueTotal.FieldName = "CostValueTotal";
            this.colCostValueTotal.Name = "colCostValueTotal";
            this.colCostValueTotal.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostValueTotal", "{0:c}")});
            this.colCostValueTotal.Visible = true;
            this.colCostValueTotal.VisibleIndex = 9;
            // 
            // colSellValueExVat
            // 
            this.colSellValueExVat.Caption = "Total Sell Ex VAT";
            this.colSellValueExVat.ColumnEdit = this.repositoryItemSpinEditCurrency8;
            this.colSellValueExVat.FieldName = "SellValueExVat";
            this.colSellValueExVat.Name = "colSellValueExVat";
            this.colSellValueExVat.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellValueExVat", "{0:c}")});
            this.colSellValueExVat.Visible = true;
            this.colSellValueExVat.VisibleIndex = 14;
            this.colSellValueExVat.Width = 101;
            // 
            // colSellValueVat
            // 
            this.colSellValueVat.Caption = "Total Sell VAT";
            this.colSellValueVat.ColumnEdit = this.repositoryItemSpinEditCurrency8;
            this.colSellValueVat.FieldName = "SellValueVat";
            this.colSellValueVat.Name = "colSellValueVat";
            this.colSellValueVat.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellValueVat", "{0:c}")});
            this.colSellValueVat.Visible = true;
            this.colSellValueVat.VisibleIndex = 15;
            this.colSellValueVat.Width = 86;
            // 
            // colSellValueTotal
            // 
            this.colSellValueTotal.Caption = "Total Sell";
            this.colSellValueTotal.ColumnEdit = this.repositoryItemSpinEditCurrency8;
            this.colSellValueTotal.FieldName = "SellValueTotal";
            this.colSellValueTotal.Name = "colSellValueTotal";
            this.colSellValueTotal.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellValueTotal", "{0:c}")});
            this.colSellValueTotal.Visible = true;
            this.colSellValueTotal.VisibleIndex = 16;
            // 
            // colRemarks3
            // 
            this.colRemarks3.Caption = "Remarks";
            this.colRemarks3.ColumnEdit = this.repositoryItemMemoExEdit9;
            this.colRemarks3.FieldName = "Remarks";
            this.colRemarks3.Name = "colRemarks3";
            this.colRemarks3.Visible = true;
            this.colRemarks3.VisibleIndex = 23;
            // 
            // repositoryItemMemoExEdit9
            // 
            this.repositoryItemMemoExEdit9.AutoHeight = false;
            this.repositoryItemMemoExEdit9.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit9.Name = "repositoryItemMemoExEdit9";
            this.repositoryItemMemoExEdit9.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit9.ShowIcon = false;
            // 
            // colClientID4
            // 
            this.colClientID4.Caption = "Client ID";
            this.colClientID4.FieldName = "ClientID";
            this.colClientID4.Name = "colClientID4";
            this.colClientID4.OptionsColumn.AllowEdit = false;
            this.colClientID4.OptionsColumn.AllowFocus = false;
            this.colClientID4.OptionsColumn.ReadOnly = true;
            // 
            // colSiteID3
            // 
            this.colSiteID3.Caption = "Site ID";
            this.colSiteID3.FieldName = "SiteID";
            this.colSiteID3.Name = "colSiteID3";
            this.colSiteID3.OptionsColumn.AllowEdit = false;
            this.colSiteID3.OptionsColumn.AllowFocus = false;
            this.colSiteID3.OptionsColumn.ReadOnly = true;
            this.colSiteID3.Visible = true;
            this.colSiteID3.VisibleIndex = 22;
            this.colSiteID3.Width = 51;
            // 
            // colVisitID2
            // 
            this.colVisitID2.Caption = "Visit ID";
            this.colVisitID2.FieldName = "VisitID";
            this.colVisitID2.Name = "colVisitID2";
            this.colVisitID2.OptionsColumn.AllowEdit = false;
            this.colVisitID2.OptionsColumn.AllowFocus = false;
            this.colVisitID2.OptionsColumn.ReadOnly = true;
            // 
            // colJobExpectedStartDate
            // 
            this.colJobExpectedStartDate.Caption = "Expected Start Date";
            this.colJobExpectedStartDate.FieldName = "JobExpectedStartDate";
            this.colJobExpectedStartDate.Name = "colJobExpectedStartDate";
            this.colJobExpectedStartDate.OptionsColumn.AllowEdit = false;
            this.colJobExpectedStartDate.OptionsColumn.AllowFocus = false;
            this.colJobExpectedStartDate.OptionsColumn.ReadOnly = true;
            this.colJobExpectedStartDate.Visible = true;
            this.colJobExpectedStartDate.VisibleIndex = 19;
            this.colJobExpectedStartDate.Width = 119;
            // 
            // colClientName3
            // 
            this.colClientName3.Caption = "Client Name";
            this.colClientName3.FieldName = "ClientName";
            this.colClientName3.Name = "colClientName3";
            this.colClientName3.OptionsColumn.AllowEdit = false;
            this.colClientName3.OptionsColumn.AllowFocus = false;
            this.colClientName3.OptionsColumn.ReadOnly = true;
            this.colClientName3.Width = 304;
            // 
            // colSiteName3
            // 
            this.colSiteName3.Caption = "Site Name";
            this.colSiteName3.FieldName = "SiteName";
            this.colSiteName3.Name = "colSiteName3";
            this.colSiteName3.OptionsColumn.AllowEdit = false;
            this.colSiteName3.OptionsColumn.AllowFocus = false;
            this.colSiteName3.OptionsColumn.ReadOnly = true;
            this.colSiteName3.Visible = true;
            this.colSiteName3.VisibleIndex = 25;
            this.colSiteName3.Width = 275;
            // 
            // colVisitNumber2
            // 
            this.colVisitNumber2.Caption = "Visit #";
            this.colVisitNumber2.FieldName = "VisitNumber";
            this.colVisitNumber2.Name = "colVisitNumber2";
            this.colVisitNumber2.OptionsColumn.AllowEdit = false;
            this.colVisitNumber2.OptionsColumn.AllowFocus = false;
            this.colVisitNumber2.OptionsColumn.ReadOnly = true;
            this.colVisitNumber2.Visible = true;
            this.colVisitNumber2.VisibleIndex = 14;
            this.colVisitNumber2.Width = 63;
            // 
            // colJobTypeDescription2
            // 
            this.colJobTypeDescription2.Caption = "Job Type";
            this.colJobTypeDescription2.FieldName = "JobTypeDescription";
            this.colJobTypeDescription2.Name = "colJobTypeDescription2";
            this.colJobTypeDescription2.OptionsColumn.AllowEdit = false;
            this.colJobTypeDescription2.OptionsColumn.AllowFocus = false;
            this.colJobTypeDescription2.OptionsColumn.ReadOnly = true;
            this.colJobTypeDescription2.Width = 192;
            // 
            // colJobSubTypeDescription2
            // 
            this.colJobSubTypeDescription2.Caption = "Job Sub-Type";
            this.colJobSubTypeDescription2.FieldName = "JobSubTypeDescription";
            this.colJobSubTypeDescription2.Name = "colJobSubTypeDescription2";
            this.colJobSubTypeDescription2.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeDescription2.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeDescription2.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeDescription2.Width = 190;
            // 
            // colContractorName
            // 
            this.colContractorName.Caption = "Labour Name";
            this.colContractorName.ColumnEdit = this.repositoryItemButtonEditContractorName;
            this.colContractorName.FieldName = "ContractorName";
            this.colContractorName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colContractorName.Name = "colContractorName";
            this.colContractorName.Visible = true;
            this.colContractorName.VisibleIndex = 1;
            this.colContractorName.Width = 213;
            // 
            // repositoryItemButtonEditContractorName
            // 
            this.repositoryItemButtonEditContractorName.AutoHeight = false;
            this.repositoryItemButtonEditContractorName.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "Click me to open the Select Contractor screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditContractorName.Name = "repositoryItemButtonEditContractorName";
            this.repositoryItemButtonEditContractorName.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemButtonEditContractorName.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditContractorName_ButtonClick);
            // 
            // colCostUnitDescriptor
            // 
            this.colCostUnitDescriptor.Caption = "Cost Unit Description";
            this.colCostUnitDescriptor.FieldName = "CostUnitDescriptor";
            this.colCostUnitDescriptor.Name = "colCostUnitDescriptor";
            this.colCostUnitDescriptor.OptionsColumn.AllowEdit = false;
            this.colCostUnitDescriptor.OptionsColumn.AllowFocus = false;
            this.colCostUnitDescriptor.OptionsColumn.ReadOnly = true;
            this.colCostUnitDescriptor.Width = 121;
            // 
            // colSellUnitDescriptor
            // 
            this.colSellUnitDescriptor.Caption = "Sell Unit Description";
            this.colSellUnitDescriptor.FieldName = "SellUnitDescriptor";
            this.colSellUnitDescriptor.Name = "colSellUnitDescriptor";
            this.colSellUnitDescriptor.OptionsColumn.AllowEdit = false;
            this.colSellUnitDescriptor.OptionsColumn.AllowFocus = false;
            this.colSellUnitDescriptor.OptionsColumn.ReadOnly = true;
            this.colSellUnitDescriptor.Width = 115;
            // 
            // colLabourType
            // 
            this.colLabourType.AppearanceCell.ForeColor = System.Drawing.Color.Gray;
            this.colLabourType.AppearanceCell.Options.UseForeColor = true;
            this.colLabourType.Caption = "Internal \\ External";
            this.colLabourType.FieldName = "LabourType";
            this.colLabourType.Name = "colLabourType";
            this.colLabourType.OptionsColumn.AllowEdit = false;
            this.colLabourType.OptionsColumn.AllowFocus = false;
            this.colLabourType.OptionsColumn.ReadOnly = true;
            this.colLabourType.Visible = true;
            this.colLabourType.VisibleIndex = 2;
            this.colLabourType.Width = 117;
            // 
            // colCISPercentage
            // 
            this.colCISPercentage.Caption = "CIS %";
            this.colCISPercentage.ColumnEdit = this.repositoryItemSpinEditPercentage8;
            this.colCISPercentage.FieldName = "CISPercentage";
            this.colCISPercentage.Name = "colCISPercentage";
            this.colCISPercentage.Visible = true;
            this.colCISPercentage.VisibleIndex = 17;
            // 
            // colCISValue
            // 
            this.colCISValue.Caption = "CIS Value";
            this.colCISValue.ColumnEdit = this.repositoryItemSpinEditCurrency8;
            this.colCISValue.FieldName = "CISValue";
            this.colCISValue.Name = "colCISValue";
            this.colCISValue.Visible = true;
            this.colCISValue.VisibleIndex = 18;
            // 
            // colPurchaseOrderID
            // 
            this.colPurchaseOrderID.Caption = "PO ID";
            this.colPurchaseOrderID.FieldName = "PurchaseOrderID";
            this.colPurchaseOrderID.Name = "colPurchaseOrderID";
            this.colPurchaseOrderID.OptionsColumn.AllowEdit = false;
            this.colPurchaseOrderID.OptionsColumn.AllowFocus = false;
            this.colPurchaseOrderID.OptionsColumn.ReadOnly = true;
            // 
            // colStartDateTime
            // 
            this.colStartDateTime.Caption = "Start Date";
            this.colStartDateTime.ColumnEdit = this.repositoryItemTextEditDateTime8;
            this.colStartDateTime.FieldName = "StartDateTime";
            this.colStartDateTime.Name = "colStartDateTime";
            this.colStartDateTime.OptionsColumn.AllowEdit = false;
            this.colStartDateTime.OptionsColumn.AllowFocus = false;
            this.colStartDateTime.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemTextEditDateTime8
            // 
            this.repositoryItemTextEditDateTime8.AutoHeight = false;
            this.repositoryItemTextEditDateTime8.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime8.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime8.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime8.Name = "repositoryItemTextEditDateTime8";
            // 
            // colEndDateTime
            // 
            this.colEndDateTime.Caption = "End Date";
            this.colEndDateTime.ColumnEdit = this.repositoryItemTextEditDateTime8;
            this.colEndDateTime.FieldName = "EndDateTime";
            this.colEndDateTime.Name = "colEndDateTime";
            this.colEndDateTime.OptionsColumn.AllowEdit = false;
            this.colEndDateTime.OptionsColumn.AllowFocus = false;
            this.colEndDateTime.OptionsColumn.ReadOnly = true;
            // 
            // colContractDescription3
            // 
            this.colContractDescription3.Caption = "Contract Description";
            this.colContractDescription3.FieldName = "ContractDescription";
            this.colContractDescription3.Name = "colContractDescription3";
            this.colContractDescription3.OptionsColumn.AllowEdit = false;
            this.colContractDescription3.OptionsColumn.AllowFocus = false;
            this.colContractDescription3.OptionsColumn.ReadOnly = true;
            this.colContractDescription3.Width = 119;
            // 
            // colClientNameContractDescription1
            // 
            this.colClientNameContractDescription1.Caption = "Client \\ Contract Description";
            this.colClientNameContractDescription1.FieldName = "ClientNameContractDescription";
            this.colClientNameContractDescription1.Name = "colClientNameContractDescription1";
            this.colClientNameContractDescription1.OptionsColumn.AllowEdit = false;
            this.colClientNameContractDescription1.OptionsColumn.AllowFocus = false;
            this.colClientNameContractDescription1.OptionsColumn.ReadOnly = true;
            this.colClientNameContractDescription1.Visible = true;
            this.colClientNameContractDescription1.VisibleIndex = 26;
            this.colClientNameContractDescription1.Width = 361;
            // 
            // colJobTypeJobSubTypeDescription1
            // 
            this.colJobTypeJobSubTypeDescription1.Caption = "Job Type \\ Job Sub-Type";
            this.colJobTypeJobSubTypeDescription1.FieldName = "JobTypeJobSubTypeDescription";
            this.colJobTypeJobSubTypeDescription1.Name = "colJobTypeJobSubTypeDescription1";
            this.colJobTypeJobSubTypeDescription1.OptionsColumn.AllowEdit = false;
            this.colJobTypeJobSubTypeDescription1.OptionsColumn.AllowFocus = false;
            this.colJobTypeJobSubTypeDescription1.OptionsColumn.ReadOnly = true;
            this.colJobTypeJobSubTypeDescription1.Visible = true;
            this.colJobTypeJobSubTypeDescription1.VisibleIndex = 27;
            this.colJobTypeJobSubTypeDescription1.Width = 396;
            // 
            // colLatLongSiteDistance
            // 
            this.colLatLongSiteDistance.AppearanceCell.ForeColor = System.Drawing.Color.Gray;
            this.colLatLongSiteDistance.AppearanceCell.Options.UseForeColor = true;
            this.colLatLongSiteDistance.Caption = "Distance From Site Lat\\Long";
            this.colLatLongSiteDistance.ColumnEdit = this.repositoryItemTextEdit6;
            this.colLatLongSiteDistance.FieldName = "LatLongSiteDistance";
            this.colLatLongSiteDistance.Name = "colLatLongSiteDistance";
            this.colLatLongSiteDistance.OptionsColumn.AllowEdit = false;
            this.colLatLongSiteDistance.OptionsColumn.AllowFocus = false;
            this.colLatLongSiteDistance.OptionsColumn.ReadOnly = true;
            this.colLatLongSiteDistance.Visible = true;
            this.colLatLongSiteDistance.VisibleIndex = 20;
            this.colLatLongSiteDistance.Width = 155;
            // 
            // repositoryItemTextEdit6
            // 
            this.repositoryItemTextEdit6.AutoHeight = false;
            this.repositoryItemTextEdit6.Mask.EditMask = "########0.00 Miles";
            this.repositoryItemTextEdit6.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit6.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit6.Name = "repositoryItemTextEdit6";
            // 
            // colPostcodeSiteDistance
            // 
            this.colPostcodeSiteDistance.AppearanceCell.ForeColor = System.Drawing.Color.Gray;
            this.colPostcodeSiteDistance.AppearanceCell.Options.UseForeColor = true;
            this.colPostcodeSiteDistance.Caption = "Distance From Site Postcode";
            this.colPostcodeSiteDistance.ColumnEdit = this.repositoryItemTextEdit6;
            this.colPostcodeSiteDistance.FieldName = "PostcodeSiteDistance";
            this.colPostcodeSiteDistance.Name = "colPostcodeSiteDistance";
            this.colPostcodeSiteDistance.OptionsColumn.AllowEdit = false;
            this.colPostcodeSiteDistance.OptionsColumn.AllowFocus = false;
            this.colPostcodeSiteDistance.OptionsColumn.ReadOnly = true;
            this.colPostcodeSiteDistance.Visible = true;
            this.colPostcodeSiteDistance.VisibleIndex = 21;
            this.colPostcodeSiteDistance.Width = 157;
            // 
            // colLinkedToPersonType
            // 
            this.colLinkedToPersonType.Caption = "Labour Type";
            this.colLinkedToPersonType.ColumnEdit = this.repositoryItemButtonEditLinkedToPersonType;
            this.colLinkedToPersonType.FieldName = "LinkedToPersonType";
            this.colLinkedToPersonType.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colLinkedToPersonType.Name = "colLinkedToPersonType";
            this.colLinkedToPersonType.Visible = true;
            this.colLinkedToPersonType.VisibleIndex = 0;
            this.colLinkedToPersonType.Width = 156;
            // 
            // repositoryItemButtonEditLinkedToPersonType
            // 
            this.repositoryItemButtonEditLinkedToPersonType.AutoHeight = false;
            this.repositoryItemButtonEditLinkedToPersonType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions7, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject25, serializableAppearanceObject26, serializableAppearanceObject27, serializableAppearanceObject28, "Click me to Open the Select Labour Type screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditLinkedToPersonType.Name = "repositoryItemButtonEditLinkedToPersonType";
            this.repositoryItemButtonEditLinkedToPersonType.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemButtonEditLinkedToPersonType.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditLinkedToPersonType_ButtonClick);
            // 
            // colLinkedToPersonTypeID
            // 
            this.colLinkedToPersonTypeID.Caption = "Labour Type ID";
            this.colLinkedToPersonTypeID.FieldName = "LinkedToPersonTypeID";
            this.colLinkedToPersonTypeID.Name = "colLinkedToPersonTypeID";
            this.colLinkedToPersonTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonTypeID.Width = 93;
            // 
            // btnStep6Next
            // 
            this.btnStep6Next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep6Next.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.btnStep6Next.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnStep6Next.Location = new System.Drawing.Point(1119, 501);
            this.btnStep6Next.Name = "btnStep6Next";
            this.btnStep6Next.Size = new System.Drawing.Size(88, 30);
            this.btnStep6Next.TabIndex = 21;
            this.btnStep6Next.Text = "Next";
            this.btnStep6Next.Click += new System.EventHandler(this.btnStep6Next_Click);
            // 
            // btnStep6Previous
            // 
            this.btnStep6Previous.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep6Previous.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnStep6Previous.Location = new System.Drawing.Point(1025, 501);
            this.btnStep6Previous.Name = "btnStep6Previous";
            this.btnStep6Previous.Size = new System.Drawing.Size(88, 30);
            this.btnStep6Previous.TabIndex = 22;
            this.btnStep6Previous.Text = "Previous";
            this.btnStep6Previous.Click += new System.EventHandler(this.btnStep6Previous_Click);
            // 
            // panelControl10
            // 
            this.panelControl10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl10.Controls.Add(this.pictureEdit9);
            this.panelControl10.Controls.Add(this.labelControl21);
            this.panelControl10.Controls.Add(this.labelControl22);
            this.panelControl10.Location = new System.Drawing.Point(7, 6);
            this.panelControl10.Name = "panelControl10";
            this.panelControl10.Size = new System.Drawing.Size(1200, 48);
            this.panelControl10.TabIndex = 18;
            // 
            // pictureEdit9
            // 
            this.pictureEdit9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit9.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit9.Location = new System.Drawing.Point(1156, 4);
            this.pictureEdit9.MenuManager = this.barManager1;
            this.pictureEdit9.Name = "pictureEdit9";
            this.pictureEdit9.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit9.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit9.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit9.Properties.ReadOnly = true;
            this.pictureEdit9.Properties.ShowMenu = false;
            this.pictureEdit9.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit9.TabIndex = 9;
            // 
            // labelControl21
            // 
            this.labelControl21.AllowHtmlString = true;
            this.labelControl21.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl21.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl21.Appearance.Options.UseBackColor = true;
            this.labelControl21.Appearance.Options.UseFont = true;
            this.labelControl21.Location = new System.Drawing.Point(5, 5);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(189, 16);
            this.labelControl21.TabIndex = 6;
            this.labelControl21.Text = "<b>Step 6:</b> Add Labour  <b>[Optional]</b>";
            // 
            // labelControl22
            // 
            this.labelControl22.AllowHtmlString = true;
            this.labelControl22.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl22.Appearance.Options.UseBackColor = true;
            this.labelControl22.Location = new System.Drawing.Point(57, 29);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(654, 13);
            this.labelControl22.TabIndex = 7;
            this.labelControl22.Text = "Select one or more Jobs by clicking on them then click the required Add Button to" +
    " add labour to the selected jobs. Click Next when Done.";
            // 
            // xtraTabPageStep7
            // 
            this.xtraTabPageStep7.Controls.Add(this.splitContainerControl3);
            this.xtraTabPageStep7.Controls.Add(this.btnStep7Next);
            this.xtraTabPageStep7.Controls.Add(this.btnStep7Previous);
            this.xtraTabPageStep7.Controls.Add(this.panelControl11);
            this.xtraTabPageStep7.Name = "xtraTabPageStep7";
            this.xtraTabPageStep7.Size = new System.Drawing.Size(1213, 537);
            this.xtraTabPageStep7.Tag = "7";
            this.xtraTabPageStep7.Text = "Step 7";
            // 
            // splitContainerControl3
            // 
            this.splitContainerControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl3.Location = new System.Drawing.Point(6, 62);
            this.splitContainerControl3.Name = "splitContainerControl3";
            this.splitContainerControl3.Panel1.Controls.Add(this.gridControl9);
            this.splitContainerControl3.Panel1.Text = "Panel1";
            this.splitContainerControl3.Panel2.Controls.Add(this.standaloneBarDockControl4);
            this.splitContainerControl3.Panel2.Controls.Add(this.gridControl10);
            this.splitContainerControl3.Panel2.Text = "Panel2";
            this.splitContainerControl3.Size = new System.Drawing.Size(1201, 433);
            this.splitContainerControl3.SplitterPosition = 471;
            this.splitContainerControl3.TabIndex = 29;
            this.splitContainerControl3.Text = "splitContainerControl3";
            // 
            // gridControl9
            // 
            this.gridControl9.DataSource = this.sp06174OMJobEditBindingSource;
            this.gridControl9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl9.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl9.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl9.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl9.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl9.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl9.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl9.Location = new System.Drawing.Point(0, 0);
            this.gridControl9.MainView = this.gridView9;
            this.gridControl9.MenuManager = this.barManager1;
            this.gridControl9.Name = "gridControl9";
            this.gridControl9.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit10,
            this.repositoryItemTextEditDateTime9,
            this.repositoryItemTextEditCurrency9,
            this.repositoryItemTextEditNumeric2DP9,
            this.repositoryItemTextEditPercentage9,
            this.repositoryItemTextEditNumeric0DP9,
            this.repositoryItemCheckEdit3});
            this.gridControl9.Size = new System.Drawing.Size(471, 433);
            this.gridControl9.TabIndex = 24;
            this.gridControl9.UseEmbeddedNavigator = true;
            this.gridControl9.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView9});
            // 
            // gridView9
            // 
            this.gridView9.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn56,
            this.gridColumn57,
            this.gridColumn58,
            this.gridColumn59,
            this.gridColumn60,
            this.gridColumn61,
            this.gridColumn62,
            this.gridColumn63,
            this.gridColumn64,
            this.gridColumn65,
            this.gridColumn66,
            this.gridColumn67,
            this.gridColumn68,
            this.gridColumn69,
            this.gridColumn70,
            this.gridColumn71,
            this.gridColumn72,
            this.gridColumn73,
            this.gridColumn74,
            this.gridColumn75,
            this.gridColumn76,
            this.gridColumn77,
            this.gridColumn78,
            this.gridColumn79,
            this.gridColumn80,
            this.gridColumn81,
            this.gridColumn82,
            this.gridColumn83,
            this.gridColumn84,
            this.gridColumn85,
            this.gridColumn86,
            this.gridColumn87,
            this.gridColumn88,
            this.gridColumn89,
            this.gridColumn90,
            this.gridColumn91,
            this.gridColumn92,
            this.gridColumn93,
            this.gridColumn94,
            this.gridColumn95,
            this.gridColumn96,
            this.gridColumn97,
            this.gridColumn98,
            this.gridColumn99,
            this.gridColumn100,
            this.gridColumn101,
            this.gridColumn102,
            this.gridColumn103,
            this.gridColumn104,
            this.gridColumn105,
            this.gridColumn106,
            this.gridColumn107,
            this.gridColumn108,
            this.gridColumn109,
            this.gridColumn110,
            this.gridColumn111,
            this.gridColumn112,
            this.gridColumn113,
            this.gridColumn114,
            this.gridColumn115,
            this.gridColumn116,
            this.gridColumn117,
            this.gridColumn118,
            this.gridColumn119,
            this.gridColumn120,
            this.gridColumn121,
            this.gridColumn122,
            this.gridColumn123,
            this.gridColumn124,
            this.gridColumn125,
            this.gridColumn126,
            this.gridColumn127,
            this.gridColumn128,
            this.gridColumn129,
            this.gridColumn130,
            this.gridColumn131,
            this.gridColumn132,
            this.gridColumn133,
            this.gridColumn134,
            this.gridColumn135,
            this.gridColumn136,
            this.gridColumn137,
            this.gridColumn138,
            this.gridColumn139,
            this.gridColumn140,
            this.gridColumn141,
            this.gridColumn142,
            this.gridColumn143,
            this.gridColumn144,
            this.gridColumn145,
            this.gridColumn146,
            this.gridColumn212,
            this.gridColumn213,
            this.gridColumn214});
            this.gridView9.GridControl = this.gridControl9;
            this.gridView9.GroupCount = 3;
            this.gridView9.Name = "gridView9";
            this.gridView9.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView9.OptionsFilter.AllowFilterEditor = false;
            this.gridView9.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView9.OptionsFilter.AllowMRUFilterList = false;
            this.gridView9.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView9.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView9.OptionsFind.FindDelay = 2000;
            this.gridView9.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView9.OptionsLayout.StoreAppearance = true;
            this.gridView9.OptionsLayout.StoreFormatRules = true;
            this.gridView9.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView9.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView9.OptionsSelection.MultiSelect = true;
            this.gridView9.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView9.OptionsView.ColumnAutoWidth = false;
            this.gridView9.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView9.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView9.OptionsView.ShowGroupPanel = false;
            this.gridView9.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn135, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn137, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn146, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn144, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView9.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView9_CustomDrawCell);
            this.gridView9.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView9.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView9.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView9.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView9.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView9.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView9.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView9_MouseUp);
            this.gridView9.GotFocus += new System.EventHandler(this.gridView9_GotFocus);
            // 
            // gridColumn56
            // 
            this.gridColumn56.Caption = "Mode";
            this.gridColumn56.FieldName = "strMode";
            this.gridColumn56.Name = "gridColumn56";
            this.gridColumn56.OptionsColumn.AllowEdit = false;
            this.gridColumn56.OptionsColumn.AllowFocus = false;
            this.gridColumn56.OptionsColumn.ReadOnly = true;
            this.gridColumn56.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // gridColumn57
            // 
            this.gridColumn57.Caption = "Record IDs";
            this.gridColumn57.FieldName = "strRecordIDs";
            this.gridColumn57.Name = "gridColumn57";
            this.gridColumn57.OptionsColumn.AllowEdit = false;
            this.gridColumn57.OptionsColumn.AllowFocus = false;
            this.gridColumn57.OptionsColumn.ReadOnly = true;
            this.gridColumn57.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // gridColumn58
            // 
            this.gridColumn58.Caption = "Dummy Job ID";
            this.gridColumn58.FieldName = "DummyJobID";
            this.gridColumn58.Name = "gridColumn58";
            this.gridColumn58.OptionsColumn.AllowEdit = false;
            this.gridColumn58.OptionsColumn.AllowFocus = false;
            this.gridColumn58.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn59
            // 
            this.gridColumn59.Caption = "Job ID";
            this.gridColumn59.FieldName = "JobID";
            this.gridColumn59.Name = "gridColumn59";
            this.gridColumn59.OptionsColumn.AllowEdit = false;
            this.gridColumn59.OptionsColumn.AllowFocus = false;
            this.gridColumn59.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn60
            // 
            this.gridColumn60.Caption = "Visit ID";
            this.gridColumn60.FieldName = "VisitID";
            this.gridColumn60.Name = "gridColumn60";
            this.gridColumn60.OptionsColumn.AllowEdit = false;
            this.gridColumn60.OptionsColumn.AllowFocus = false;
            this.gridColumn60.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn61
            // 
            this.gridColumn61.Caption = "Status ID";
            this.gridColumn61.FieldName = "JobStatusID";
            this.gridColumn61.Name = "gridColumn61";
            this.gridColumn61.OptionsColumn.AllowEdit = false;
            this.gridColumn61.OptionsColumn.AllowFocus = false;
            this.gridColumn61.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn62
            // 
            this.gridColumn62.Caption = "Job Sub-Type ID";
            this.gridColumn62.FieldName = "JobSubTypeID";
            this.gridColumn62.Name = "gridColumn62";
            this.gridColumn62.OptionsColumn.AllowEdit = false;
            this.gridColumn62.OptionsColumn.AllowFocus = false;
            this.gridColumn62.OptionsColumn.ReadOnly = true;
            this.gridColumn62.Width = 101;
            // 
            // gridColumn63
            // 
            this.gridColumn63.Caption = "Created By Staff ID";
            this.gridColumn63.FieldName = "CreatedByStaffID";
            this.gridColumn63.Name = "gridColumn63";
            this.gridColumn63.OptionsColumn.AllowEdit = false;
            this.gridColumn63.OptionsColumn.AllowFocus = false;
            this.gridColumn63.OptionsColumn.ReadOnly = true;
            this.gridColumn63.Width = 116;
            // 
            // gridColumn64
            // 
            this.gridColumn64.Caption = "Reactive";
            this.gridColumn64.ColumnEdit = this.repositoryItemCheckEdit3;
            this.gridColumn64.FieldName = "Reactive";
            this.gridColumn64.Name = "gridColumn64";
            this.gridColumn64.OptionsColumn.AllowEdit = false;
            this.gridColumn64.OptionsColumn.AllowFocus = false;
            this.gridColumn64.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Caption = "Check";
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = 1;
            this.repositoryItemCheckEdit3.ValueUnchecked = 0;
            // 
            // gridColumn65
            // 
            this.gridColumn65.Caption = "Job No Longer Required";
            this.gridColumn65.ColumnEdit = this.repositoryItemCheckEdit3;
            this.gridColumn65.FieldName = "JobNoLongerRequired";
            this.gridColumn65.Name = "gridColumn65";
            this.gridColumn65.OptionsColumn.AllowEdit = false;
            this.gridColumn65.OptionsColumn.AllowFocus = false;
            this.gridColumn65.OptionsColumn.ReadOnly = true;
            this.gridColumn65.Width = 136;
            // 
            // gridColumn66
            // 
            this.gridColumn66.Caption = "Requires Access Permit";
            this.gridColumn66.ColumnEdit = this.repositoryItemCheckEdit3;
            this.gridColumn66.FieldName = "RequiresAccessPermit";
            this.gridColumn66.Name = "gridColumn66";
            this.gridColumn66.OptionsColumn.AllowEdit = false;
            this.gridColumn66.OptionsColumn.AllowFocus = false;
            this.gridColumn66.OptionsColumn.ReadOnly = true;
            this.gridColumn66.Visible = true;
            this.gridColumn66.VisibleIndex = 9;
            this.gridColumn66.Width = 132;
            // 
            // gridColumn67
            // 
            this.gridColumn67.Caption = "Access Permit ID";
            this.gridColumn67.FieldName = "AccessPermitID";
            this.gridColumn67.Name = "gridColumn67";
            this.gridColumn67.OptionsColumn.AllowEdit = false;
            this.gridColumn67.OptionsColumn.AllowFocus = false;
            this.gridColumn67.OptionsColumn.ReadOnly = true;
            this.gridColumn67.Width = 101;
            // 
            // gridColumn68
            // 
            this.gridColumn68.Caption = "CLient P.O. #";
            this.gridColumn68.FieldName = "ClientPONumber";
            this.gridColumn68.Name = "gridColumn68";
            this.gridColumn68.OptionsColumn.AllowEdit = false;
            this.gridColumn68.OptionsColumn.AllowFocus = false;
            this.gridColumn68.OptionsColumn.ReadOnly = true;
            this.gridColumn68.Width = 87;
            // 
            // gridColumn69
            // 
            this.gridColumn69.Caption = "CLient P.O. ID";
            this.gridColumn69.FieldName = "ClientPOID";
            this.gridColumn69.Name = "gridColumn69";
            this.gridColumn69.OptionsColumn.AllowEdit = false;
            this.gridColumn69.OptionsColumn.AllowFocus = false;
            this.gridColumn69.OptionsColumn.ReadOnly = true;
            this.gridColumn69.Width = 90;
            // 
            // gridColumn70
            // 
            this.gridColumn70.Caption = "Finance System P.O. #";
            this.gridColumn70.FieldName = "FinanceSystemPONumber";
            this.gridColumn70.Name = "gridColumn70";
            this.gridColumn70.OptionsColumn.AllowEdit = false;
            this.gridColumn70.OptionsColumn.AllowFocus = false;
            this.gridColumn70.OptionsColumn.ReadOnly = true;
            this.gridColumn70.Width = 132;
            // 
            // gridColumn71
            // 
            this.gridColumn71.Caption = "Expected Start Date";
            this.gridColumn71.ColumnEdit = this.repositoryItemTextEditDateTime9;
            this.gridColumn71.FieldName = "ExpectedStartDate";
            this.gridColumn71.Name = "gridColumn71";
            this.gridColumn71.OptionsColumn.AllowEdit = false;
            this.gridColumn71.OptionsColumn.AllowFocus = false;
            this.gridColumn71.OptionsColumn.ReadOnly = true;
            this.gridColumn71.Visible = true;
            this.gridColumn71.VisibleIndex = 2;
            this.gridColumn71.Width = 119;
            // 
            // repositoryItemTextEditDateTime9
            // 
            this.repositoryItemTextEditDateTime9.AutoHeight = false;
            this.repositoryItemTextEditDateTime9.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime9.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime9.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime9.Name = "repositoryItemTextEditDateTime9";
            // 
            // gridColumn72
            // 
            this.gridColumn72.Caption = "Expected End Date";
            this.gridColumn72.ColumnEdit = this.repositoryItemTextEditDateTime9;
            this.gridColumn72.FieldName = "ExpectedEndDate";
            this.gridColumn72.Name = "gridColumn72";
            this.gridColumn72.OptionsColumn.AllowEdit = false;
            this.gridColumn72.OptionsColumn.AllowFocus = false;
            this.gridColumn72.OptionsColumn.ReadOnly = true;
            this.gridColumn72.Visible = true;
            this.gridColumn72.VisibleIndex = 5;
            this.gridColumn72.Width = 113;
            // 
            // gridColumn73
            // 
            this.gridColumn73.Caption = "Expected Duraction";
            this.gridColumn73.ColumnEdit = this.repositoryItemTextEditNumeric2DP9;
            this.gridColumn73.FieldName = "ExpectedDurationUnits";
            this.gridColumn73.Name = "gridColumn73";
            this.gridColumn73.OptionsColumn.AllowEdit = false;
            this.gridColumn73.OptionsColumn.AllowFocus = false;
            this.gridColumn73.OptionsColumn.ReadOnly = true;
            this.gridColumn73.Visible = true;
            this.gridColumn73.VisibleIndex = 3;
            this.gridColumn73.Width = 115;
            // 
            // repositoryItemTextEditNumeric2DP9
            // 
            this.repositoryItemTextEditNumeric2DP9.AutoHeight = false;
            this.repositoryItemTextEditNumeric2DP9.Mask.EditMask = "n2";
            this.repositoryItemTextEditNumeric2DP9.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumeric2DP9.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumeric2DP9.Name = "repositoryItemTextEditNumeric2DP9";
            // 
            // gridColumn74
            // 
            this.gridColumn74.Caption = "Expected Duration Descriptor";
            this.gridColumn74.FieldName = "ExpectedDurationUnitsDescriptorID";
            this.gridColumn74.Name = "gridColumn74";
            this.gridColumn74.OptionsColumn.AllowEdit = false;
            this.gridColumn74.OptionsColumn.AllowFocus = false;
            this.gridColumn74.OptionsColumn.ReadOnly = true;
            this.gridColumn74.Visible = true;
            this.gridColumn74.VisibleIndex = 4;
            this.gridColumn74.Width = 162;
            // 
            // gridColumn75
            // 
            this.gridColumn75.Caption = "Schedule Sent Date";
            this.gridColumn75.ColumnEdit = this.repositoryItemTextEditDateTime9;
            this.gridColumn75.FieldName = "ScheduleSentDate";
            this.gridColumn75.Name = "gridColumn75";
            this.gridColumn75.OptionsColumn.AllowEdit = false;
            this.gridColumn75.OptionsColumn.AllowFocus = false;
            this.gridColumn75.OptionsColumn.ReadOnly = true;
            this.gridColumn75.Width = 115;
            // 
            // gridColumn76
            // 
            this.gridColumn76.Caption = "Actual Start Date";
            this.gridColumn76.ColumnEdit = this.repositoryItemTextEditDateTime9;
            this.gridColumn76.FieldName = "ActualStartDate";
            this.gridColumn76.Name = "gridColumn76";
            this.gridColumn76.OptionsColumn.AllowEdit = false;
            this.gridColumn76.OptionsColumn.AllowFocus = false;
            this.gridColumn76.OptionsColumn.ReadOnly = true;
            this.gridColumn76.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn76.Width = 104;
            // 
            // gridColumn77
            // 
            this.gridColumn77.Caption = "Actual End Date";
            this.gridColumn77.ColumnEdit = this.repositoryItemTextEditDateTime9;
            this.gridColumn77.FieldName = "ActualEndDate";
            this.gridColumn77.Name = "gridColumn77";
            this.gridColumn77.OptionsColumn.AllowEdit = false;
            this.gridColumn77.OptionsColumn.AllowFocus = false;
            this.gridColumn77.OptionsColumn.ReadOnly = true;
            this.gridColumn77.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn77.Width = 98;
            // 
            // gridColumn78
            // 
            this.gridColumn78.Caption = "Actual Duration Units";
            this.gridColumn78.ColumnEdit = this.repositoryItemTextEditNumeric2DP9;
            this.gridColumn78.FieldName = "ActualDurationUnits";
            this.gridColumn78.Name = "gridColumn78";
            this.gridColumn78.OptionsColumn.AllowEdit = false;
            this.gridColumn78.OptionsColumn.AllowFocus = false;
            this.gridColumn78.OptionsColumn.ReadOnly = true;
            this.gridColumn78.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn78.Width = 122;
            // 
            // gridColumn79
            // 
            this.gridColumn79.Caption = "Actual Duration Unit Descriptor";
            this.gridColumn79.FieldName = "ActualDurationUnitsDescriptionID";
            this.gridColumn79.Name = "gridColumn79";
            this.gridColumn79.OptionsColumn.AllowEdit = false;
            this.gridColumn79.OptionsColumn.AllowFocus = false;
            this.gridColumn79.OptionsColumn.ReadOnly = true;
            this.gridColumn79.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn79.Width = 169;
            // 
            // gridColumn80
            // 
            this.gridColumn80.Caption = "Cost - Total Material Ex VAT";
            this.gridColumn80.ColumnEdit = this.repositoryItemTextEditCurrency9;
            this.gridColumn80.FieldName = "CostTotalMaterialExVAT";
            this.gridColumn80.Name = "gridColumn80";
            this.gridColumn80.OptionsColumn.AllowEdit = false;
            this.gridColumn80.OptionsColumn.AllowFocus = false;
            this.gridColumn80.OptionsColumn.ReadOnly = true;
            this.gridColumn80.Width = 155;
            // 
            // repositoryItemTextEditCurrency9
            // 
            this.repositoryItemTextEditCurrency9.AutoHeight = false;
            this.repositoryItemTextEditCurrency9.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency9.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency9.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency9.Name = "repositoryItemTextEditCurrency9";
            // 
            // gridColumn81
            // 
            this.gridColumn81.Caption = "Cost - Total Material VAT Rate";
            this.gridColumn81.ColumnEdit = this.repositoryItemTextEditPercentage9;
            this.gridColumn81.FieldName = "CostTotalMaterialVATRate";
            this.gridColumn81.Name = "gridColumn81";
            this.gridColumn81.OptionsColumn.AllowEdit = false;
            this.gridColumn81.OptionsColumn.AllowFocus = false;
            this.gridColumn81.OptionsColumn.ReadOnly = true;
            this.gridColumn81.Width = 166;
            // 
            // repositoryItemTextEditPercentage9
            // 
            this.repositoryItemTextEditPercentage9.AutoHeight = false;
            this.repositoryItemTextEditPercentage9.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage9.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage9.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage9.Name = "repositoryItemTextEditPercentage9";
            // 
            // gridColumn82
            // 
            this.gridColumn82.Caption = "Cost - Total Material VAT";
            this.gridColumn82.ColumnEdit = this.repositoryItemTextEditCurrency9;
            this.gridColumn82.FieldName = "CostTotalMaterialVAT";
            this.gridColumn82.Name = "gridColumn82";
            this.gridColumn82.OptionsColumn.AllowEdit = false;
            this.gridColumn82.OptionsColumn.AllowFocus = false;
            this.gridColumn82.OptionsColumn.ReadOnly = true;
            this.gridColumn82.Width = 140;
            // 
            // gridColumn83
            // 
            this.gridColumn83.Caption = "Cost - Total Material Value";
            this.gridColumn83.ColumnEdit = this.repositoryItemTextEditCurrency9;
            this.gridColumn83.FieldName = "CostTotalMaterialCost";
            this.gridColumn83.Name = "gridColumn83";
            this.gridColumn83.OptionsColumn.AllowEdit = false;
            this.gridColumn83.OptionsColumn.AllowFocus = false;
            this.gridColumn83.OptionsColumn.ReadOnly = true;
            this.gridColumn83.Width = 147;
            // 
            // gridColumn84
            // 
            this.gridColumn84.Caption = "Sell - Total Material Ex VAT";
            this.gridColumn84.ColumnEdit = this.repositoryItemTextEditCurrency9;
            this.gridColumn84.FieldName = "SellTotalMaterialExVAT";
            this.gridColumn84.Name = "gridColumn84";
            this.gridColumn84.OptionsColumn.AllowEdit = false;
            this.gridColumn84.OptionsColumn.AllowFocus = false;
            this.gridColumn84.OptionsColumn.ReadOnly = true;
            this.gridColumn84.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn84.Width = 149;
            // 
            // gridColumn85
            // 
            this.gridColumn85.Caption = "Sell - Total Material VAT Rate";
            this.gridColumn85.ColumnEdit = this.repositoryItemTextEditPercentage9;
            this.gridColumn85.FieldName = "SellTotalMaterialVATRate";
            this.gridColumn85.Name = "gridColumn85";
            this.gridColumn85.OptionsColumn.AllowEdit = false;
            this.gridColumn85.OptionsColumn.AllowFocus = false;
            this.gridColumn85.OptionsColumn.ReadOnly = true;
            this.gridColumn85.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn85.Width = 160;
            // 
            // gridColumn86
            // 
            this.gridColumn86.Caption = "Sell - Total Material VAT";
            this.gridColumn86.ColumnEdit = this.repositoryItemTextEditCurrency9;
            this.gridColumn86.FieldName = "SellTotalMaterialVAT";
            this.gridColumn86.Name = "gridColumn86";
            this.gridColumn86.OptionsColumn.AllowEdit = false;
            this.gridColumn86.OptionsColumn.AllowFocus = false;
            this.gridColumn86.OptionsColumn.ReadOnly = true;
            this.gridColumn86.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn86.Width = 134;
            // 
            // gridColumn87
            // 
            this.gridColumn87.Caption = "Sell - Total Material Value";
            this.gridColumn87.ColumnEdit = this.repositoryItemTextEditCurrency9;
            this.gridColumn87.FieldName = "SellTotalMaterialCost";
            this.gridColumn87.Name = "gridColumn87";
            this.gridColumn87.OptionsColumn.AllowEdit = false;
            this.gridColumn87.OptionsColumn.AllowFocus = false;
            this.gridColumn87.OptionsColumn.ReadOnly = true;
            this.gridColumn87.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn87.Width = 141;
            // 
            // gridColumn88
            // 
            this.gridColumn88.Caption = "Cost - Total Equipment Value";
            this.gridColumn88.ColumnEdit = this.repositoryItemTextEditCurrency9;
            this.gridColumn88.FieldName = "CostTotalEquipmentExVAT";
            this.gridColumn88.Name = "gridColumn88";
            this.gridColumn88.OptionsColumn.AllowEdit = false;
            this.gridColumn88.OptionsColumn.AllowFocus = false;
            this.gridColumn88.OptionsColumn.ReadOnly = true;
            this.gridColumn88.Width = 159;
            // 
            // gridColumn89
            // 
            this.gridColumn89.Caption = "Cost - Total Equipment VAT Rate";
            this.gridColumn89.ColumnEdit = this.repositoryItemTextEditPercentage9;
            this.gridColumn89.FieldName = "CostTotalEquipmentVATRate";
            this.gridColumn89.Name = "gridColumn89";
            this.gridColumn89.OptionsColumn.AllowEdit = false;
            this.gridColumn89.OptionsColumn.AllowFocus = false;
            this.gridColumn89.OptionsColumn.ReadOnly = true;
            this.gridColumn89.Width = 178;
            // 
            // gridColumn90
            // 
            this.gridColumn90.Caption = "Cost - Total Equipment VAT";
            this.gridColumn90.ColumnEdit = this.repositoryItemTextEditCurrency9;
            this.gridColumn90.FieldName = "CostTotalEquipmentVAT";
            this.gridColumn90.Name = "gridColumn90";
            this.gridColumn90.OptionsColumn.AllowEdit = false;
            this.gridColumn90.OptionsColumn.AllowFocus = false;
            this.gridColumn90.OptionsColumn.ReadOnly = true;
            this.gridColumn90.Width = 154;
            // 
            // gridColumn91
            // 
            this.gridColumn91.Caption = "Cost - Total Equipment Value";
            this.gridColumn91.ColumnEdit = this.repositoryItemTextEditCurrency9;
            this.gridColumn91.FieldName = "CostTotalEquipmentCost";
            this.gridColumn91.Name = "gridColumn91";
            this.gridColumn91.OptionsColumn.AllowEdit = false;
            this.gridColumn91.OptionsColumn.AllowFocus = false;
            this.gridColumn91.OptionsColumn.ReadOnly = true;
            this.gridColumn91.Width = 159;
            // 
            // gridColumn92
            // 
            this.gridColumn92.Caption = "Sell - Total Equipment Ex VAT";
            this.gridColumn92.ColumnEdit = this.repositoryItemTextEditCurrency9;
            this.gridColumn92.FieldName = "SellTotalEquipmentExVAT";
            this.gridColumn92.Name = "gridColumn92";
            this.gridColumn92.OptionsColumn.AllowEdit = false;
            this.gridColumn92.OptionsColumn.AllowFocus = false;
            this.gridColumn92.OptionsColumn.ReadOnly = true;
            this.gridColumn92.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn92.Width = 161;
            // 
            // gridColumn93
            // 
            this.gridColumn93.Caption = "Sell - Total Equipment VAT Rate";
            this.gridColumn93.ColumnEdit = this.repositoryItemTextEditPercentage9;
            this.gridColumn93.FieldName = "SellTotalEquipmentVATRate";
            this.gridColumn93.Name = "gridColumn93";
            this.gridColumn93.OptionsColumn.AllowEdit = false;
            this.gridColumn93.OptionsColumn.AllowFocus = false;
            this.gridColumn93.OptionsColumn.ReadOnly = true;
            this.gridColumn93.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn93.Width = 172;
            // 
            // gridColumn94
            // 
            this.gridColumn94.Caption = "Sell - Total Equipment VAT";
            this.gridColumn94.ColumnEdit = this.repositoryItemTextEditCurrency9;
            this.gridColumn94.FieldName = "SellTotalEquipmentVAT";
            this.gridColumn94.Name = "gridColumn94";
            this.gridColumn94.OptionsColumn.AllowEdit = false;
            this.gridColumn94.OptionsColumn.AllowFocus = false;
            this.gridColumn94.OptionsColumn.ReadOnly = true;
            this.gridColumn94.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn94.Width = 146;
            // 
            // gridColumn95
            // 
            this.gridColumn95.Caption = "Sell - Total Equipment Value";
            this.gridColumn95.ColumnEdit = this.repositoryItemTextEditCurrency9;
            this.gridColumn95.FieldName = "SellTotalEquipmentCost";
            this.gridColumn95.Name = "gridColumn95";
            this.gridColumn95.OptionsColumn.AllowEdit = false;
            this.gridColumn95.OptionsColumn.AllowFocus = false;
            this.gridColumn95.OptionsColumn.ReadOnly = true;
            this.gridColumn95.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn95.Width = 153;
            // 
            // gridColumn96
            // 
            this.gridColumn96.Caption = "Cost - Total Labour Ex VAT";
            this.gridColumn96.ColumnEdit = this.repositoryItemTextEditCurrency9;
            this.gridColumn96.FieldName = "CostTotalLabourExVAT";
            this.gridColumn96.Name = "gridColumn96";
            this.gridColumn96.OptionsColumn.AllowEdit = false;
            this.gridColumn96.OptionsColumn.AllowFocus = false;
            this.gridColumn96.OptionsColumn.ReadOnly = true;
            this.gridColumn96.Width = 150;
            // 
            // gridColumn97
            // 
            this.gridColumn97.Caption = "Cost - Total Labour VAT Rate";
            this.gridColumn97.ColumnEdit = this.repositoryItemTextEditPercentage9;
            this.gridColumn97.FieldName = "CostTotalLabourVATRate";
            this.gridColumn97.Name = "gridColumn97";
            this.gridColumn97.OptionsColumn.AllowEdit = false;
            this.gridColumn97.OptionsColumn.AllowFocus = false;
            this.gridColumn97.OptionsColumn.ReadOnly = true;
            this.gridColumn97.Width = 161;
            // 
            // gridColumn98
            // 
            this.gridColumn98.Caption = "Cost - Total Labour VAT";
            this.gridColumn98.ColumnEdit = this.repositoryItemTextEditCurrency9;
            this.gridColumn98.FieldName = "CostTotalLabourVAT";
            this.gridColumn98.Name = "gridColumn98";
            this.gridColumn98.OptionsColumn.AllowEdit = false;
            this.gridColumn98.OptionsColumn.AllowFocus = false;
            this.gridColumn98.OptionsColumn.ReadOnly = true;
            this.gridColumn98.Width = 135;
            // 
            // gridColumn99
            // 
            this.gridColumn99.Caption = "Cost - Total Labour Value";
            this.gridColumn99.ColumnEdit = this.repositoryItemTextEditCurrency9;
            this.gridColumn99.FieldName = "CostTotalLabourCost";
            this.gridColumn99.Name = "gridColumn99";
            this.gridColumn99.OptionsColumn.AllowEdit = false;
            this.gridColumn99.OptionsColumn.AllowFocus = false;
            this.gridColumn99.OptionsColumn.ReadOnly = true;
            this.gridColumn99.Width = 142;
            // 
            // gridColumn100
            // 
            this.gridColumn100.Caption = "Sell - Total Labour Ex VAT";
            this.gridColumn100.ColumnEdit = this.repositoryItemTextEditCurrency9;
            this.gridColumn100.FieldName = "SellTotalLabourExVAT";
            this.gridColumn100.Name = "gridColumn100";
            this.gridColumn100.OptionsColumn.AllowEdit = false;
            this.gridColumn100.OptionsColumn.AllowFocus = false;
            this.gridColumn100.OptionsColumn.ReadOnly = true;
            this.gridColumn100.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn100.Width = 144;
            // 
            // gridColumn101
            // 
            this.gridColumn101.Caption = "Sell - Total Labour VAT Rate";
            this.gridColumn101.ColumnEdit = this.repositoryItemTextEditPercentage9;
            this.gridColumn101.FieldName = "SellTotalLabourVATRate";
            this.gridColumn101.Name = "gridColumn101";
            this.gridColumn101.OptionsColumn.AllowEdit = false;
            this.gridColumn101.OptionsColumn.AllowFocus = false;
            this.gridColumn101.OptionsColumn.ReadOnly = true;
            this.gridColumn101.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn101.Width = 155;
            // 
            // gridColumn102
            // 
            this.gridColumn102.Caption = "Sell - Total Labour VAT";
            this.gridColumn102.ColumnEdit = this.repositoryItemTextEditCurrency9;
            this.gridColumn102.FieldName = "SellTotalLabourVAT";
            this.gridColumn102.Name = "gridColumn102";
            this.gridColumn102.OptionsColumn.AllowEdit = false;
            this.gridColumn102.OptionsColumn.AllowFocus = false;
            this.gridColumn102.OptionsColumn.ReadOnly = true;
            this.gridColumn102.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn102.Width = 129;
            // 
            // gridColumn103
            // 
            this.gridColumn103.Caption = "Sell - Total Labour Value";
            this.gridColumn103.ColumnEdit = this.repositoryItemTextEditCurrency9;
            this.gridColumn103.FieldName = "SellTotalLabourCost";
            this.gridColumn103.Name = "gridColumn103";
            this.gridColumn103.OptionsColumn.AllowEdit = false;
            this.gridColumn103.OptionsColumn.AllowFocus = false;
            this.gridColumn103.OptionsColumn.ReadOnly = true;
            this.gridColumn103.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn103.Width = 136;
            // 
            // gridColumn104
            // 
            this.gridColumn104.Caption = "Cost - Total Ex VAT";
            this.gridColumn104.ColumnEdit = this.repositoryItemTextEditCurrency9;
            this.gridColumn104.FieldName = "CostTotalCostExVAT";
            this.gridColumn104.Name = "gridColumn104";
            this.gridColumn104.OptionsColumn.AllowEdit = false;
            this.gridColumn104.OptionsColumn.AllowFocus = false;
            this.gridColumn104.OptionsColumn.ReadOnly = true;
            this.gridColumn104.Width = 114;
            // 
            // gridColumn105
            // 
            this.gridColumn105.Caption = "Cost - Total VAT";
            this.gridColumn105.ColumnEdit = this.repositoryItemTextEditCurrency9;
            this.gridColumn105.FieldName = "CostTotalCostVAT";
            this.gridColumn105.Name = "gridColumn105";
            this.gridColumn105.OptionsColumn.AllowEdit = false;
            this.gridColumn105.OptionsColumn.AllowFocus = false;
            this.gridColumn105.OptionsColumn.ReadOnly = true;
            this.gridColumn105.Width = 99;
            // 
            // gridColumn106
            // 
            this.gridColumn106.Caption = "Cost - Total Value";
            this.gridColumn106.ColumnEdit = this.repositoryItemTextEditCurrency9;
            this.gridColumn106.FieldName = "CostTotalCost";
            this.gridColumn106.Name = "gridColumn106";
            this.gridColumn106.OptionsColumn.AllowEdit = false;
            this.gridColumn106.OptionsColumn.AllowFocus = false;
            this.gridColumn106.OptionsColumn.ReadOnly = true;
            this.gridColumn106.Width = 106;
            // 
            // gridColumn107
            // 
            this.gridColumn107.Caption = "Sell - Total Ex VAT";
            this.gridColumn107.ColumnEdit = this.repositoryItemTextEditCurrency9;
            this.gridColumn107.FieldName = "SellTotalCostExVAT";
            this.gridColumn107.Name = "gridColumn107";
            this.gridColumn107.OptionsColumn.AllowEdit = false;
            this.gridColumn107.OptionsColumn.AllowFocus = false;
            this.gridColumn107.OptionsColumn.ReadOnly = true;
            this.gridColumn107.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn107.Width = 108;
            // 
            // gridColumn108
            // 
            this.gridColumn108.Caption = "Sell - Total VAT";
            this.gridColumn108.ColumnEdit = this.repositoryItemTextEditCurrency9;
            this.gridColumn108.FieldName = "SellTotalCostVAT";
            this.gridColumn108.Name = "gridColumn108";
            this.gridColumn108.OptionsColumn.AllowEdit = false;
            this.gridColumn108.OptionsColumn.AllowFocus = false;
            this.gridColumn108.OptionsColumn.ReadOnly = true;
            this.gridColumn108.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn108.Width = 93;
            // 
            // gridColumn109
            // 
            this.gridColumn109.Caption = "Sell - Total Value";
            this.gridColumn109.ColumnEdit = this.repositoryItemTextEditCurrency9;
            this.gridColumn109.FieldName = "SellTotalCost";
            this.gridColumn109.Name = "gridColumn109";
            this.gridColumn109.OptionsColumn.AllowEdit = false;
            this.gridColumn109.OptionsColumn.AllowFocus = false;
            this.gridColumn109.OptionsColumn.ReadOnly = true;
            this.gridColumn109.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn109.Width = 100;
            // 
            // gridColumn110
            // 
            this.gridColumn110.Caption = "Client Invoice ID";
            this.gridColumn110.FieldName = "ClientInvoiceID";
            this.gridColumn110.Name = "gridColumn110";
            this.gridColumn110.OptionsColumn.AllowEdit = false;
            this.gridColumn110.OptionsColumn.AllowFocus = false;
            this.gridColumn110.OptionsColumn.ReadOnly = true;
            this.gridColumn110.Width = 100;
            // 
            // gridColumn111
            // 
            this.gridColumn111.Caption = "Client Invoiced Date";
            this.gridColumn111.FieldName = "DateClientInvoiced";
            this.gridColumn111.Name = "gridColumn111";
            this.gridColumn111.OptionsColumn.AllowEdit = false;
            this.gridColumn111.OptionsColumn.AllowFocus = false;
            this.gridColumn111.OptionsColumn.ReadOnly = true;
            this.gridColumn111.Width = 118;
            // 
            // gridColumn112
            // 
            this.gridColumn112.Caption = "Self-Billing Invoice ID";
            this.gridColumn112.FieldName = "SelfBillingInvoiceID";
            this.gridColumn112.Name = "gridColumn112";
            this.gridColumn112.OptionsColumn.AllowEdit = false;
            this.gridColumn112.OptionsColumn.AllowFocus = false;
            this.gridColumn112.OptionsColumn.ReadOnly = true;
            this.gridColumn112.Width = 121;
            // 
            // gridColumn113
            // 
            this.gridColumn113.Caption = "Self-Billing Invoice Received Date";
            this.gridColumn113.ColumnEdit = this.repositoryItemTextEditDateTime9;
            this.gridColumn113.FieldName = "SelfBillingInvoiceReceivedDate";
            this.gridColumn113.Name = "gridColumn113";
            this.gridColumn113.OptionsColumn.AllowEdit = false;
            this.gridColumn113.OptionsColumn.AllowFocus = false;
            this.gridColumn113.OptionsColumn.ReadOnly = true;
            this.gridColumn113.Width = 180;
            // 
            // gridColumn114
            // 
            this.gridColumn114.Caption = "Self-Billing Invoice Paid Date";
            this.gridColumn114.ColumnEdit = this.repositoryItemTextEditDateTime9;
            this.gridColumn114.FieldName = "SelfBillingInvoicePaidDate";
            this.gridColumn114.Name = "gridColumn114";
            this.gridColumn114.OptionsColumn.AllowEdit = false;
            this.gridColumn114.OptionsColumn.AllowFocus = false;
            this.gridColumn114.OptionsColumn.ReadOnly = true;
            this.gridColumn114.Width = 156;
            // 
            // gridColumn115
            // 
            this.gridColumn115.Caption = "Self-Billing Invoice Amount Paid";
            this.gridColumn115.ColumnEdit = this.repositoryItemTextEditCurrency9;
            this.gridColumn115.FieldName = "SelfBillingInvoiceAmountPaid";
            this.gridColumn115.Name = "gridColumn115";
            this.gridColumn115.OptionsColumn.AllowEdit = false;
            this.gridColumn115.OptionsColumn.AllowFocus = false;
            this.gridColumn115.OptionsColumn.ReadOnly = true;
            this.gridColumn115.Width = 170;
            // 
            // gridColumn116
            // 
            this.gridColumn116.Caption = "Do Not Pay Contractor";
            this.gridColumn116.ColumnEdit = this.repositoryItemCheckEdit3;
            this.gridColumn116.FieldName = "DoNotPayContractor";
            this.gridColumn116.Name = "gridColumn116";
            this.gridColumn116.OptionsColumn.AllowEdit = false;
            this.gridColumn116.OptionsColumn.AllowFocus = false;
            this.gridColumn116.OptionsColumn.ReadOnly = true;
            this.gridColumn116.Width = 130;
            // 
            // gridColumn117
            // 
            this.gridColumn117.Caption = "Do Not Invoice Client";
            this.gridColumn117.ColumnEdit = this.repositoryItemCheckEdit3;
            this.gridColumn117.FieldName = "DoNotInvoiceClient";
            this.gridColumn117.Name = "gridColumn117";
            this.gridColumn117.OptionsColumn.AllowEdit = false;
            this.gridColumn117.OptionsColumn.AllowFocus = false;
            this.gridColumn117.OptionsColumn.ReadOnly = true;
            this.gridColumn117.Width = 122;
            // 
            // gridColumn118
            // 
            this.gridColumn118.Caption = "Remarks";
            this.gridColumn118.ColumnEdit = this.repositoryItemMemoExEdit10;
            this.gridColumn118.FieldName = "Remarks";
            this.gridColumn118.Name = "gridColumn118";
            this.gridColumn118.OptionsColumn.ReadOnly = true;
            this.gridColumn118.Visible = true;
            this.gridColumn118.VisibleIndex = 10;
            // 
            // repositoryItemMemoExEdit10
            // 
            this.repositoryItemMemoExEdit10.AutoHeight = false;
            this.repositoryItemMemoExEdit10.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit10.Name = "repositoryItemMemoExEdit10";
            this.repositoryItemMemoExEdit10.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit10.ShowIcon = false;
            // 
            // gridColumn119
            // 
            this.gridColumn119.Caption = "Route Order";
            this.gridColumn119.FieldName = "RouteOrder";
            this.gridColumn119.Name = "gridColumn119";
            this.gridColumn119.OptionsColumn.AllowEdit = false;
            this.gridColumn119.OptionsColumn.AllowFocus = false;
            this.gridColumn119.OptionsColumn.ReadOnly = true;
            this.gridColumn119.Width = 81;
            // 
            // gridColumn120
            // 
            this.gridColumn120.Caption = "Start Latitude";
            this.gridColumn120.FieldName = "StartLatitude";
            this.gridColumn120.Name = "gridColumn120";
            this.gridColumn120.OptionsColumn.AllowEdit = false;
            this.gridColumn120.OptionsColumn.AllowFocus = false;
            this.gridColumn120.OptionsColumn.ReadOnly = true;
            this.gridColumn120.Width = 87;
            // 
            // gridColumn121
            // 
            this.gridColumn121.Caption = "Start Longitude";
            this.gridColumn121.FieldName = "StartLongitude";
            this.gridColumn121.Name = "gridColumn121";
            this.gridColumn121.OptionsColumn.AllowEdit = false;
            this.gridColumn121.OptionsColumn.AllowFocus = false;
            this.gridColumn121.OptionsColumn.ReadOnly = true;
            this.gridColumn121.Width = 95;
            // 
            // gridColumn122
            // 
            this.gridColumn122.Caption = "Finish Latitude";
            this.gridColumn122.FieldName = "FinishLatitude";
            this.gridColumn122.Name = "gridColumn122";
            this.gridColumn122.OptionsColumn.AllowEdit = false;
            this.gridColumn122.OptionsColumn.AllowFocus = false;
            this.gridColumn122.OptionsColumn.ReadOnly = true;
            this.gridColumn122.Width = 90;
            // 
            // gridColumn123
            // 
            this.gridColumn123.Caption = "Finish Longitude";
            this.gridColumn123.FieldName = "FinishLongitude";
            this.gridColumn123.Name = "gridColumn123";
            this.gridColumn123.OptionsColumn.AllowEdit = false;
            this.gridColumn123.OptionsColumn.AllowFocus = false;
            this.gridColumn123.OptionsColumn.ReadOnly = true;
            this.gridColumn123.Width = 98;
            // 
            // gridColumn124
            // 
            this.gridColumn124.Caption = "Tender ID";
            this.gridColumn124.FieldName = "TenderID";
            this.gridColumn124.Name = "gridColumn124";
            this.gridColumn124.OptionsColumn.AllowEdit = false;
            this.gridColumn124.OptionsColumn.AllowFocus = false;
            this.gridColumn124.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn125
            // 
            this.gridColumn125.Caption = "Tender Job ID";
            this.gridColumn125.FieldName = "TenderJobID";
            this.gridColumn125.Name = "gridColumn125";
            this.gridColumn125.OptionsColumn.AllowEdit = false;
            this.gridColumn125.OptionsColumn.AllowFocus = false;
            this.gridColumn125.OptionsColumn.ReadOnly = true;
            this.gridColumn125.Width = 89;
            // 
            // gridColumn126
            // 
            this.gridColumn126.Caption = "Min Days From Last Visit";
            this.gridColumn126.ColumnEdit = this.repositoryItemTextEditNumeric0DP9;
            this.gridColumn126.FieldName = "MinimumDaysFromLastVisit";
            this.gridColumn126.Name = "gridColumn126";
            this.gridColumn126.OptionsColumn.AllowEdit = false;
            this.gridColumn126.OptionsColumn.AllowFocus = false;
            this.gridColumn126.OptionsColumn.ReadOnly = true;
            this.gridColumn126.Visible = true;
            this.gridColumn126.VisibleIndex = 6;
            this.gridColumn126.Width = 136;
            // 
            // repositoryItemTextEditNumeric0DP9
            // 
            this.repositoryItemTextEditNumeric0DP9.AutoHeight = false;
            this.repositoryItemTextEditNumeric0DP9.Mask.EditMask = "n0";
            this.repositoryItemTextEditNumeric0DP9.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumeric0DP9.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumeric0DP9.Name = "repositoryItemTextEditNumeric0DP9";
            // 
            // gridColumn127
            // 
            this.gridColumn127.Caption = "Max Days From Last Visit";
            this.gridColumn127.ColumnEdit = this.repositoryItemTextEditNumeric0DP9;
            this.gridColumn127.FieldName = "MaximumDaysFromLastVisit";
            this.gridColumn127.Name = "gridColumn127";
            this.gridColumn127.OptionsColumn.AllowEdit = false;
            this.gridColumn127.OptionsColumn.AllowFocus = false;
            this.gridColumn127.OptionsColumn.ReadOnly = true;
            this.gridColumn127.Visible = true;
            this.gridColumn127.VisibleIndex = 7;
            this.gridColumn127.Width = 140;
            // 
            // gridColumn128
            // 
            this.gridColumn128.Caption = "Days Leeway";
            this.gridColumn128.ColumnEdit = this.repositoryItemTextEditNumeric0DP9;
            this.gridColumn128.FieldName = "DaysLeeway";
            this.gridColumn128.Name = "gridColumn128";
            this.gridColumn128.OptionsColumn.AllowEdit = false;
            this.gridColumn128.OptionsColumn.AllowFocus = false;
            this.gridColumn128.OptionsColumn.ReadOnly = true;
            this.gridColumn128.Visible = true;
            this.gridColumn128.VisibleIndex = 8;
            this.gridColumn128.Width = 85;
            // 
            // gridColumn129
            // 
            this.gridColumn129.Caption = "Billing Centre Code";
            this.gridColumn129.FieldName = "BillingCentreCodeID";
            this.gridColumn129.Name = "gridColumn129";
            this.gridColumn129.OptionsColumn.AllowEdit = false;
            this.gridColumn129.OptionsColumn.AllowFocus = false;
            this.gridColumn129.OptionsColumn.ReadOnly = true;
            this.gridColumn129.Width = 111;
            // 
            // gridColumn130
            // 
            this.gridColumn130.Caption = "Site Contract ID";
            this.gridColumn130.FieldName = "SiteContractID";
            this.gridColumn130.Name = "gridColumn130";
            this.gridColumn130.OptionsColumn.AllowEdit = false;
            this.gridColumn130.OptionsColumn.AllowFocus = false;
            this.gridColumn130.OptionsColumn.ReadOnly = true;
            this.gridColumn130.Width = 98;
            // 
            // gridColumn131
            // 
            this.gridColumn131.Caption = "Client Contract ID";
            this.gridColumn131.FieldName = "ClientContractID";
            this.gridColumn131.Name = "gridColumn131";
            this.gridColumn131.OptionsColumn.AllowEdit = false;
            this.gridColumn131.OptionsColumn.AllowFocus = false;
            this.gridColumn131.OptionsColumn.ReadOnly = true;
            this.gridColumn131.Width = 107;
            // 
            // gridColumn132
            // 
            this.gridColumn132.Caption = "Site ID";
            this.gridColumn132.FieldName = "SiteID";
            this.gridColumn132.Name = "gridColumn132";
            this.gridColumn132.OptionsColumn.AllowEdit = false;
            this.gridColumn132.OptionsColumn.AllowFocus = false;
            this.gridColumn132.OptionsColumn.ReadOnly = true;
            this.gridColumn132.Visible = true;
            this.gridColumn132.VisibleIndex = 1;
            this.gridColumn132.Width = 51;
            // 
            // gridColumn133
            // 
            this.gridColumn133.Caption = "Client ID";
            this.gridColumn133.FieldName = "ClientID";
            this.gridColumn133.Name = "gridColumn133";
            this.gridColumn133.OptionsColumn.AllowEdit = false;
            this.gridColumn133.OptionsColumn.AllowFocus = false;
            this.gridColumn133.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn134
            // 
            this.gridColumn134.Caption = "Contract Description";
            this.gridColumn134.FieldName = "ContractDescription";
            this.gridColumn134.Name = "gridColumn134";
            this.gridColumn134.OptionsColumn.AllowEdit = false;
            this.gridColumn134.OptionsColumn.AllowFocus = false;
            this.gridColumn134.OptionsColumn.ReadOnly = true;
            this.gridColumn134.Width = 119;
            // 
            // gridColumn135
            // 
            this.gridColumn135.Caption = "Client \\ Contract";
            this.gridColumn135.FieldName = "ClientNameContractDescription";
            this.gridColumn135.Name = "gridColumn135";
            this.gridColumn135.OptionsColumn.AllowEdit = false;
            this.gridColumn135.OptionsColumn.AllowFocus = false;
            this.gridColumn135.OptionsColumn.ReadOnly = true;
            this.gridColumn135.Visible = true;
            this.gridColumn135.VisibleIndex = 13;
            this.gridColumn135.Width = 416;
            // 
            // gridColumn136
            // 
            this.gridColumn136.Caption = "Client Name";
            this.gridColumn136.FieldName = "ClientName";
            this.gridColumn136.Name = "gridColumn136";
            this.gridColumn136.OptionsColumn.AllowEdit = false;
            this.gridColumn136.OptionsColumn.AllowFocus = false;
            this.gridColumn136.OptionsColumn.ReadOnly = true;
            this.gridColumn136.Width = 199;
            // 
            // gridColumn137
            // 
            this.gridColumn137.Caption = "Site Name";
            this.gridColumn137.FieldName = "SiteName";
            this.gridColumn137.Name = "gridColumn137";
            this.gridColumn137.OptionsColumn.AllowEdit = false;
            this.gridColumn137.OptionsColumn.AllowFocus = false;
            this.gridColumn137.OptionsColumn.ReadOnly = true;
            this.gridColumn137.Visible = true;
            this.gridColumn137.VisibleIndex = 14;
            this.gridColumn137.Width = 276;
            // 
            // gridColumn138
            // 
            this.gridColumn138.Caption = "Site Postcode";
            this.gridColumn138.FieldName = "SitePostcode";
            this.gridColumn138.Name = "gridColumn138";
            this.gridColumn138.OptionsColumn.AllowEdit = false;
            this.gridColumn138.OptionsColumn.AllowFocus = false;
            this.gridColumn138.OptionsColumn.ReadOnly = true;
            this.gridColumn138.Width = 86;
            // 
            // gridColumn139
            // 
            this.gridColumn139.Caption = "Site Latitude";
            this.gridColumn139.FieldName = "LocationX";
            this.gridColumn139.Name = "gridColumn139";
            this.gridColumn139.OptionsColumn.AllowEdit = false;
            this.gridColumn139.OptionsColumn.AllowFocus = false;
            this.gridColumn139.OptionsColumn.ReadOnly = true;
            this.gridColumn139.Width = 81;
            // 
            // gridColumn140
            // 
            this.gridColumn140.Caption = "Site Longitude";
            this.gridColumn140.FieldName = "LocationY";
            this.gridColumn140.Name = "gridColumn140";
            this.gridColumn140.OptionsColumn.AllowEdit = false;
            this.gridColumn140.OptionsColumn.AllowFocus = false;
            this.gridColumn140.OptionsColumn.ReadOnly = true;
            this.gridColumn140.Width = 89;
            // 
            // gridColumn141
            // 
            this.gridColumn141.Caption = "Created By";
            this.gridColumn141.FieldName = "CreatedByStaffName";
            this.gridColumn141.Name = "gridColumn141";
            this.gridColumn141.OptionsColumn.AllowEdit = false;
            this.gridColumn141.OptionsColumn.AllowFocus = false;
            this.gridColumn141.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn142
            // 
            this.gridColumn142.Caption = "Job Sub-Type";
            this.gridColumn142.FieldName = "JobSubTypeDescription";
            this.gridColumn142.Name = "gridColumn142";
            this.gridColumn142.OptionsColumn.AllowEdit = false;
            this.gridColumn142.OptionsColumn.AllowFocus = false;
            this.gridColumn142.OptionsColumn.ReadOnly = true;
            this.gridColumn142.Width = 87;
            // 
            // gridColumn143
            // 
            this.gridColumn143.Caption = "Job Type";
            this.gridColumn143.FieldName = "JobTypeDescription";
            this.gridColumn143.Name = "gridColumn143";
            this.gridColumn143.OptionsColumn.AllowEdit = false;
            this.gridColumn143.OptionsColumn.AllowFocus = false;
            this.gridColumn143.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn144
            // 
            this.gridColumn144.Caption = "Job Type \\ Sub-Type";
            this.gridColumn144.FieldName = "JobTypeJobSubTypeDescription";
            this.gridColumn144.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn144.Name = "gridColumn144";
            this.gridColumn144.OptionsColumn.AllowEdit = false;
            this.gridColumn144.OptionsColumn.AllowFocus = false;
            this.gridColumn144.OptionsColumn.ReadOnly = true;
            this.gridColumn144.Visible = true;
            this.gridColumn144.VisibleIndex = 0;
            this.gridColumn144.Width = 290;
            // 
            // gridColumn145
            // 
            this.gridColumn145.Caption = "Job Type ID";
            this.gridColumn145.FieldName = "JobTypeID";
            this.gridColumn145.Name = "gridColumn145";
            this.gridColumn145.OptionsColumn.AllowEdit = false;
            this.gridColumn145.OptionsColumn.AllowFocus = false;
            this.gridColumn145.OptionsColumn.ReadOnly = true;
            this.gridColumn145.Width = 79;
            // 
            // gridColumn146
            // 
            this.gridColumn146.Caption = "Visit #";
            this.gridColumn146.FieldName = "VisitNumber";
            this.gridColumn146.Name = "gridColumn146";
            this.gridColumn146.OptionsColumn.AllowEdit = false;
            this.gridColumn146.OptionsColumn.AllowFocus = false;
            this.gridColumn146.OptionsColumn.ReadOnly = true;
            this.gridColumn146.Visible = true;
            this.gridColumn146.VisibleIndex = 11;
            // 
            // gridColumn212
            // 
            this.gridColumn212.Caption = "Labour Count";
            this.gridColumn212.ColumnEdit = this.repositoryItemTextEditNumeric0DP9;
            this.gridColumn212.FieldName = "LabourCount";
            this.gridColumn212.Name = "gridColumn212";
            this.gridColumn212.OptionsColumn.AllowEdit = false;
            this.gridColumn212.OptionsColumn.AllowFocus = false;
            this.gridColumn212.OptionsColumn.ReadOnly = true;
            this.gridColumn212.Width = 86;
            // 
            // gridColumn213
            // 
            this.gridColumn213.Caption = "Equipment Count";
            this.gridColumn213.ColumnEdit = this.repositoryItemTextEditNumeric0DP9;
            this.gridColumn213.FieldName = "EquipmentCount";
            this.gridColumn213.Name = "gridColumn213";
            this.gridColumn213.OptionsColumn.AllowEdit = false;
            this.gridColumn213.OptionsColumn.AllowFocus = false;
            this.gridColumn213.OptionsColumn.ReadOnly = true;
            this.gridColumn213.Visible = true;
            this.gridColumn213.VisibleIndex = 11;
            this.gridColumn213.Width = 103;
            // 
            // gridColumn214
            // 
            this.gridColumn214.Caption = "Material Count";
            this.gridColumn214.ColumnEdit = this.repositoryItemTextEditNumeric0DP9;
            this.gridColumn214.FieldName = "MaterialCount";
            this.gridColumn214.Name = "gridColumn214";
            this.gridColumn214.OptionsColumn.AllowEdit = false;
            this.gridColumn214.OptionsColumn.AllowFocus = false;
            this.gridColumn214.OptionsColumn.ReadOnly = true;
            this.gridColumn214.Width = 91;
            // 
            // standaloneBarDockControl4
            // 
            this.standaloneBarDockControl4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl4.CausesValidation = false;
            this.standaloneBarDockControl4.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl4.Manager = this.barManager1;
            this.standaloneBarDockControl4.Name = "standaloneBarDockControl4";
            this.standaloneBarDockControl4.Size = new System.Drawing.Size(724, 26);
            this.standaloneBarDockControl4.Text = "standaloneBarDockControl4";
            // 
            // gridControl10
            // 
            this.gridControl10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl10.DataSource = this.sp06187OMJobEquipmentEditBindingSource;
            this.gridControl10.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl10.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl10.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl10.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl10.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl10.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl10.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 10, true, true, "Block Add New Records", "block add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Block Edit Selected Records", "block edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl10.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl10_EmbeddedNavigator_ButtonClick);
            this.gridControl10.Location = new System.Drawing.Point(0, 26);
            this.gridControl10.MainView = this.gridView10;
            this.gridControl10.MenuManager = this.barManager1;
            this.gridControl10.Name = "gridControl10";
            this.gridControl10.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEditEquipmentName,
            this.epositoryItemSpinEdit2DP10,
            this.repositoryItemGridLookUpEdit2,
            this.epositoryItemSpinEditCurrency10,
            this.epositoryItemSpinEditPercentage10,
            this.repositoryItemMemoExEdit11,
            this.repositoryItemTextEdit12,
            this.repositoryItemTextEdit13});
            this.gridControl10.Size = new System.Drawing.Size(724, 407);
            this.gridControl10.TabIndex = 21;
            this.gridControl10.UseEmbeddedNavigator = true;
            this.gridControl10.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView10});
            // 
            // sp06187OMJobEquipmentEditBindingSource
            // 
            this.sp06187OMJobEquipmentEditBindingSource.DataMember = "sp06187_OM_Job_Equipment_Edit";
            this.sp06187OMJobEquipmentEditBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridView10
            // 
            this.gridView10.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn215,
            this.gridColumn216,
            this.gridColumn217,
            this.gridColumn218,
            this.gridColumn219,
            this.gridColumn220,
            this.gridColumn221,
            this.gridColumn222,
            this.gridColumn226,
            this.gridColumn227,
            this.gridColumn228,
            this.gridColumn229,
            this.gridColumn230,
            this.gridColumn231,
            this.gridColumn232,
            this.gridColumn233,
            this.gridColumn234,
            this.gridColumn235,
            this.gridColumn236,
            this.gridColumn237,
            this.gridColumn238,
            this.gridColumn239,
            this.gridColumn240,
            this.gridColumn241,
            this.gridColumn242,
            this.gridColumn243,
            this.gridColumn244,
            this.gridColumn245,
            this.gridColumn246,
            this.gridColumn247,
            this.gridColumn248,
            this.gridColumn249,
            this.gridColumn250,
            this.colOwnerType,
            this.gridColumn254,
            this.gridColumn255,
            this.gridColumn256,
            this.gridColumn257,
            this.gridColumn258,
            this.gridColumn259,
            this.gridColumn260,
            this.gridColumn261,
            this.colOwnerName,
            this.colJobSubTypeID2,
            this.colJobTypeID2});
            this.gridView10.GridControl = this.gridControl10;
            this.gridView10.GroupCount = 4;
            this.gridView10.Name = "gridView10";
            this.gridView10.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView10.OptionsFilter.AllowFilterEditor = false;
            this.gridView10.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView10.OptionsFilter.AllowMRUFilterList = false;
            this.gridView10.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView10.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView10.OptionsFind.FindDelay = 2000;
            this.gridView10.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView10.OptionsLayout.StoreAppearance = true;
            this.gridView10.OptionsLayout.StoreFormatRules = true;
            this.gridView10.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView10.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView10.OptionsNavigation.AutoFocusNewRow = true;
            this.gridView10.OptionsSelection.MultiSelect = true;
            this.gridView10.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView10.OptionsView.ColumnAutoWidth = false;
            this.gridView10.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView10.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView10.OptionsView.ShowFooter = true;
            this.gridView10.OptionsView.ShowGroupPanel = false;
            this.gridView10.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn258, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn244, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn245, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn259, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView10.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView10.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView10.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView10.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView10.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView10_MouseUp);
            this.gridView10.GotFocus += new System.EventHandler(this.gridView10_GotFocus);
            // 
            // gridColumn215
            // 
            this.gridColumn215.FieldName = "strMode";
            this.gridColumn215.Name = "gridColumn215";
            this.gridColumn215.OptionsColumn.AllowEdit = false;
            this.gridColumn215.OptionsColumn.AllowFocus = false;
            this.gridColumn215.OptionsColumn.ReadOnly = true;
            this.gridColumn215.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn215.OptionsColumn.ShowInExpressionEditor = false;
            // 
            // gridColumn216
            // 
            this.gridColumn216.FieldName = "strRecordIDs";
            this.gridColumn216.Name = "gridColumn216";
            this.gridColumn216.OptionsColumn.AllowEdit = false;
            this.gridColumn216.OptionsColumn.AllowFocus = false;
            this.gridColumn216.OptionsColumn.ReadOnly = true;
            this.gridColumn216.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn216.OptionsColumn.ShowInExpressionEditor = false;
            this.gridColumn216.Width = 90;
            // 
            // gridColumn217
            // 
            this.gridColumn217.Caption = "Dummy Job ID";
            this.gridColumn217.FieldName = "DummyJobID";
            this.gridColumn217.Name = "gridColumn217";
            this.gridColumn217.OptionsColumn.AllowEdit = false;
            this.gridColumn217.OptionsColumn.AllowFocus = false;
            this.gridColumn217.OptionsColumn.ReadOnly = true;
            this.gridColumn217.Width = 90;
            // 
            // gridColumn218
            // 
            this.gridColumn218.Caption = "Equipment Used ID ";
            this.gridColumn218.FieldName = "EquipmentUsedID";
            this.gridColumn218.Name = "gridColumn218";
            this.gridColumn218.OptionsColumn.AllowEdit = false;
            this.gridColumn218.OptionsColumn.AllowFocus = false;
            this.gridColumn218.OptionsColumn.ReadOnly = true;
            this.gridColumn218.Width = 98;
            // 
            // gridColumn219
            // 
            this.gridColumn219.Caption = "Equipment ID";
            this.gridColumn219.FieldName = "EquipmentID";
            this.gridColumn219.Name = "gridColumn219";
            this.gridColumn219.OptionsColumn.AllowEdit = false;
            this.gridColumn219.OptionsColumn.AllowFocus = false;
            this.gridColumn219.OptionsColumn.ReadOnly = true;
            this.gridColumn219.Width = 84;
            // 
            // gridColumn220
            // 
            this.gridColumn220.Caption = "Job ID";
            this.gridColumn220.FieldName = "JobID";
            this.gridColumn220.Name = "gridColumn220";
            this.gridColumn220.OptionsColumn.AllowEdit = false;
            this.gridColumn220.OptionsColumn.AllowFocus = false;
            this.gridColumn220.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn221
            // 
            this.gridColumn221.Caption = "Cost Units Used";
            this.gridColumn221.ColumnEdit = this.epositoryItemSpinEdit2DP10;
            this.gridColumn221.FieldName = "CostUnitsUsed";
            this.gridColumn221.Name = "gridColumn221";
            this.gridColumn221.Visible = true;
            this.gridColumn221.VisibleIndex = 3;
            this.gridColumn221.Width = 97;
            // 
            // epositoryItemSpinEdit2DP10
            // 
            this.epositoryItemSpinEdit2DP10.AutoHeight = false;
            this.epositoryItemSpinEdit2DP10.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.epositoryItemSpinEdit2DP10.Mask.EditMask = "n2";
            this.epositoryItemSpinEdit2DP10.Mask.UseMaskAsDisplayFormat = true;
            this.epositoryItemSpinEdit2DP10.Name = "epositoryItemSpinEdit2DP10";
            this.epositoryItemSpinEdit2DP10.EditValueChanged += new System.EventHandler(this.repositoryItemSpinEdit2DP10_EditValueChanged);
            // 
            // gridColumn222
            // 
            this.gridColumn222.Caption = "Cost Unit Descriptor";
            this.gridColumn222.ColumnEdit = this.repositoryItemGridLookUpEdit2;
            this.gridColumn222.FieldName = "CostUnitDescriptorID";
            this.gridColumn222.Name = "gridColumn222";
            this.gridColumn222.Visible = true;
            this.gridColumn222.VisibleIndex = 4;
            this.gridColumn222.Width = 117;
            // 
            // repositoryItemGridLookUpEdit2
            // 
            this.repositoryItemGridLookUpEdit2.AutoHeight = false;
            this.repositoryItemGridLookUpEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit2.DataSource = this.sp06101OMWorkUnitTypesPicklistBindingSource;
            this.repositoryItemGridLookUpEdit2.DisplayMember = "Description";
            this.repositoryItemGridLookUpEdit2.Name = "repositoryItemGridLookUpEdit2";
            this.repositoryItemGridLookUpEdit2.NullText = "";
            this.repositoryItemGridLookUpEdit2.PopupView = this.gridView13;
            this.repositoryItemGridLookUpEdit2.ValueMember = "ID";
            // 
            // gridView13
            // 
            this.gridView13.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn223,
            this.gridColumn224,
            this.gridColumn225});
            this.gridView13.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.gridColumn223;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridView13.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridView13.Name = "gridView13";
            this.gridView13.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView13.OptionsLayout.StoreAppearance = true;
            this.gridView13.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView13.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView13.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView13.OptionsView.ColumnAutoWidth = false;
            this.gridView13.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView13.OptionsView.ShowGroupPanel = false;
            this.gridView13.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView13.OptionsView.ShowIndicator = false;
            this.gridView13.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn225, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn224
            // 
            this.gridColumn224.Caption = "Unit Descriptor";
            this.gridColumn224.FieldName = "Description";
            this.gridColumn224.Name = "gridColumn224";
            this.gridColumn224.OptionsColumn.AllowEdit = false;
            this.gridColumn224.OptionsColumn.AllowFocus = false;
            this.gridColumn224.OptionsColumn.ReadOnly = true;
            this.gridColumn224.Visible = true;
            this.gridColumn224.VisibleIndex = 0;
            this.gridColumn224.Width = 220;
            // 
            // gridColumn225
            // 
            this.gridColumn225.Caption = "Order";
            this.gridColumn225.FieldName = "RecordOrder";
            this.gridColumn225.Name = "gridColumn225";
            this.gridColumn225.OptionsColumn.AllowEdit = false;
            this.gridColumn225.OptionsColumn.AllowFocus = false;
            this.gridColumn225.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn226
            // 
            this.gridColumn226.Caption = "Sell Units Used";
            this.gridColumn226.ColumnEdit = this.epositoryItemSpinEdit2DP10;
            this.gridColumn226.FieldName = "SellUnitsUsed";
            this.gridColumn226.Name = "gridColumn226";
            this.gridColumn226.Visible = true;
            this.gridColumn226.VisibleIndex = 10;
            this.gridColumn226.Width = 91;
            // 
            // gridColumn227
            // 
            this.gridColumn227.Caption = "Sell Unit Descriptor";
            this.gridColumn227.ColumnEdit = this.repositoryItemGridLookUpEdit2;
            this.gridColumn227.FieldName = "SellUnitDescriptorID";
            this.gridColumn227.Name = "gridColumn227";
            this.gridColumn227.Visible = true;
            this.gridColumn227.VisibleIndex = 11;
            this.gridColumn227.Width = 111;
            // 
            // gridColumn228
            // 
            this.gridColumn228.Caption = "Unit Cost Ex VAT";
            this.gridColumn228.ColumnEdit = this.epositoryItemSpinEditCurrency10;
            this.gridColumn228.FieldName = "CostPerUnitExVat";
            this.gridColumn228.Name = "gridColumn228";
            this.gridColumn228.Visible = true;
            this.gridColumn228.VisibleIndex = 5;
            this.gridColumn228.Width = 102;
            // 
            // epositoryItemSpinEditCurrency10
            // 
            this.epositoryItemSpinEditCurrency10.AutoHeight = false;
            this.epositoryItemSpinEditCurrency10.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.epositoryItemSpinEditCurrency10.Mask.EditMask = "c";
            this.epositoryItemSpinEditCurrency10.Mask.UseMaskAsDisplayFormat = true;
            this.epositoryItemSpinEditCurrency10.Name = "epositoryItemSpinEditCurrency10";
            this.epositoryItemSpinEditCurrency10.EditValueChanged += new System.EventHandler(this.repositoryItemSpinEditCurrency10_EditValueChanged);
            // 
            // gridColumn229
            // 
            this.gridColumn229.Caption = "Unit Cost VAT Rate";
            this.gridColumn229.ColumnEdit = this.epositoryItemSpinEditPercentage10;
            this.gridColumn229.FieldName = "CostPerUnitVatRate";
            this.gridColumn229.Name = "gridColumn229";
            this.gridColumn229.Visible = true;
            this.gridColumn229.VisibleIndex = 6;
            this.gridColumn229.Width = 113;
            // 
            // epositoryItemSpinEditPercentage10
            // 
            this.epositoryItemSpinEditPercentage10.AutoHeight = false;
            this.epositoryItemSpinEditPercentage10.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.epositoryItemSpinEditPercentage10.Mask.EditMask = "P";
            this.epositoryItemSpinEditPercentage10.Mask.UseMaskAsDisplayFormat = true;
            this.epositoryItemSpinEditPercentage10.Name = "epositoryItemSpinEditPercentage10";
            this.epositoryItemSpinEditPercentage10.EditValueChanged += new System.EventHandler(this.repositoryItemSpinEditPercentage10_EditValueChanged);
            // 
            // gridColumn230
            // 
            this.gridColumn230.Caption = "Unit Sell Ex VAT";
            this.gridColumn230.ColumnEdit = this.epositoryItemSpinEditCurrency10;
            this.gridColumn230.FieldName = "SellPerUnitExVat";
            this.gridColumn230.Name = "gridColumn230";
            this.gridColumn230.Visible = true;
            this.gridColumn230.VisibleIndex = 12;
            this.gridColumn230.Width = 96;
            // 
            // gridColumn231
            // 
            this.gridColumn231.Caption = "Unit Sell VAT Rate";
            this.gridColumn231.ColumnEdit = this.epositoryItemSpinEditPercentage10;
            this.gridColumn231.FieldName = "SellPerUnitVatRate";
            this.gridColumn231.Name = "gridColumn231";
            this.gridColumn231.Visible = true;
            this.gridColumn231.VisibleIndex = 13;
            this.gridColumn231.Width = 107;
            // 
            // gridColumn232
            // 
            this.gridColumn232.Caption = "Total Cost Ex VAT";
            this.gridColumn232.ColumnEdit = this.epositoryItemSpinEditCurrency10;
            this.gridColumn232.FieldName = "CostValueExVat";
            this.gridColumn232.Name = "gridColumn232";
            this.gridColumn232.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostValueExVat", "{0:c}")});
            this.gridColumn232.Visible = true;
            this.gridColumn232.VisibleIndex = 7;
            this.gridColumn232.Width = 107;
            // 
            // gridColumn233
            // 
            this.gridColumn233.Caption = "Total Cost VAT";
            this.gridColumn233.ColumnEdit = this.epositoryItemSpinEditCurrency10;
            this.gridColumn233.FieldName = "CostValueVat";
            this.gridColumn233.Name = "gridColumn233";
            this.gridColumn233.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostValueVat", "{0:c}")});
            this.gridColumn233.Visible = true;
            this.gridColumn233.VisibleIndex = 8;
            this.gridColumn233.Width = 92;
            // 
            // gridColumn234
            // 
            this.gridColumn234.Caption = "Total Cost";
            this.gridColumn234.ColumnEdit = this.epositoryItemSpinEditCurrency10;
            this.gridColumn234.FieldName = "CostValueTotal";
            this.gridColumn234.Name = "gridColumn234";
            this.gridColumn234.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostValueTotal", "{0:c}")});
            this.gridColumn234.Visible = true;
            this.gridColumn234.VisibleIndex = 9;
            // 
            // gridColumn235
            // 
            this.gridColumn235.Caption = "Total Sell Ex VAT";
            this.gridColumn235.ColumnEdit = this.epositoryItemSpinEditCurrency10;
            this.gridColumn235.FieldName = "SellValueExVat";
            this.gridColumn235.Name = "gridColumn235";
            this.gridColumn235.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellValueExVat", "{0:c}")});
            this.gridColumn235.Visible = true;
            this.gridColumn235.VisibleIndex = 14;
            this.gridColumn235.Width = 101;
            // 
            // gridColumn236
            // 
            this.gridColumn236.Caption = "Total Sell VAT";
            this.gridColumn236.ColumnEdit = this.epositoryItemSpinEditCurrency10;
            this.gridColumn236.FieldName = "SellValueVat";
            this.gridColumn236.Name = "gridColumn236";
            this.gridColumn236.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellValueVat", "{0:c}")});
            this.gridColumn236.Visible = true;
            this.gridColumn236.VisibleIndex = 15;
            this.gridColumn236.Width = 86;
            // 
            // gridColumn237
            // 
            this.gridColumn237.Caption = "Total Sell";
            this.gridColumn237.ColumnEdit = this.epositoryItemSpinEditCurrency10;
            this.gridColumn237.FieldName = "SellValueTotal";
            this.gridColumn237.Name = "gridColumn237";
            this.gridColumn237.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellValueTotal", "{0:c}")});
            this.gridColumn237.Visible = true;
            this.gridColumn237.VisibleIndex = 16;
            // 
            // gridColumn238
            // 
            this.gridColumn238.Caption = "Remarks";
            this.gridColumn238.ColumnEdit = this.repositoryItemMemoExEdit11;
            this.gridColumn238.FieldName = "Remarks";
            this.gridColumn238.Name = "gridColumn238";
            this.gridColumn238.Visible = true;
            this.gridColumn238.VisibleIndex = 19;
            // 
            // repositoryItemMemoExEdit11
            // 
            this.repositoryItemMemoExEdit11.AutoHeight = false;
            this.repositoryItemMemoExEdit11.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit11.Name = "repositoryItemMemoExEdit11";
            this.repositoryItemMemoExEdit11.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit11.ShowIcon = false;
            // 
            // gridColumn239
            // 
            this.gridColumn239.Caption = "Client ID";
            this.gridColumn239.FieldName = "ClientID";
            this.gridColumn239.Name = "gridColumn239";
            this.gridColumn239.OptionsColumn.AllowEdit = false;
            this.gridColumn239.OptionsColumn.AllowFocus = false;
            this.gridColumn239.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn240
            // 
            this.gridColumn240.Caption = "Site ID";
            this.gridColumn240.FieldName = "SiteID";
            this.gridColumn240.Name = "gridColumn240";
            this.gridColumn240.OptionsColumn.AllowEdit = false;
            this.gridColumn240.OptionsColumn.AllowFocus = false;
            this.gridColumn240.OptionsColumn.ReadOnly = true;
            this.gridColumn240.Visible = true;
            this.gridColumn240.VisibleIndex = 18;
            this.gridColumn240.Width = 51;
            // 
            // gridColumn241
            // 
            this.gridColumn241.Caption = "Visit ID";
            this.gridColumn241.FieldName = "VisitID";
            this.gridColumn241.Name = "gridColumn241";
            this.gridColumn241.OptionsColumn.AllowEdit = false;
            this.gridColumn241.OptionsColumn.AllowFocus = false;
            this.gridColumn241.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn242
            // 
            this.gridColumn242.Caption = "Expected Start Date";
            this.gridColumn242.FieldName = "JobExpectedStartDate";
            this.gridColumn242.Name = "gridColumn242";
            this.gridColumn242.OptionsColumn.AllowEdit = false;
            this.gridColumn242.OptionsColumn.AllowFocus = false;
            this.gridColumn242.OptionsColumn.ReadOnly = true;
            this.gridColumn242.Visible = true;
            this.gridColumn242.VisibleIndex = 17;
            this.gridColumn242.Width = 119;
            // 
            // gridColumn243
            // 
            this.gridColumn243.Caption = "Client Name";
            this.gridColumn243.FieldName = "ClientName";
            this.gridColumn243.Name = "gridColumn243";
            this.gridColumn243.OptionsColumn.AllowEdit = false;
            this.gridColumn243.OptionsColumn.AllowFocus = false;
            this.gridColumn243.OptionsColumn.ReadOnly = true;
            this.gridColumn243.Width = 304;
            // 
            // gridColumn244
            // 
            this.gridColumn244.Caption = "Site Name";
            this.gridColumn244.FieldName = "SiteName";
            this.gridColumn244.Name = "gridColumn244";
            this.gridColumn244.OptionsColumn.AllowEdit = false;
            this.gridColumn244.OptionsColumn.AllowFocus = false;
            this.gridColumn244.OptionsColumn.ReadOnly = true;
            this.gridColumn244.Visible = true;
            this.gridColumn244.VisibleIndex = 21;
            this.gridColumn244.Width = 275;
            // 
            // gridColumn245
            // 
            this.gridColumn245.Caption = "Visit #";
            this.gridColumn245.FieldName = "VisitNumber";
            this.gridColumn245.Name = "gridColumn245";
            this.gridColumn245.OptionsColumn.AllowEdit = false;
            this.gridColumn245.OptionsColumn.AllowFocus = false;
            this.gridColumn245.OptionsColumn.ReadOnly = true;
            this.gridColumn245.Visible = true;
            this.gridColumn245.VisibleIndex = 14;
            this.gridColumn245.Width = 63;
            // 
            // gridColumn246
            // 
            this.gridColumn246.Caption = "Job Type";
            this.gridColumn246.FieldName = "JobTypeDescription";
            this.gridColumn246.Name = "gridColumn246";
            this.gridColumn246.OptionsColumn.AllowEdit = false;
            this.gridColumn246.OptionsColumn.AllowFocus = false;
            this.gridColumn246.OptionsColumn.ReadOnly = true;
            this.gridColumn246.Width = 192;
            // 
            // gridColumn247
            // 
            this.gridColumn247.Caption = "Job Sub-Type";
            this.gridColumn247.FieldName = "JobSubTypeDescription";
            this.gridColumn247.Name = "gridColumn247";
            this.gridColumn247.OptionsColumn.AllowEdit = false;
            this.gridColumn247.OptionsColumn.AllowFocus = false;
            this.gridColumn247.OptionsColumn.ReadOnly = true;
            this.gridColumn247.Width = 190;
            // 
            // gridColumn248
            // 
            this.gridColumn248.Caption = "Equipment Type";
            this.gridColumn248.ColumnEdit = this.repositoryItemButtonEditEquipmentName;
            this.gridColumn248.FieldName = "EquipmentName";
            this.gridColumn248.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn248.Name = "gridColumn248";
            this.gridColumn248.Visible = true;
            this.gridColumn248.VisibleIndex = 0;
            this.gridColumn248.Width = 211;
            // 
            // repositoryItemButtonEditEquipmentName
            // 
            this.repositoryItemButtonEditEquipmentName.AutoHeight = false;
            this.repositoryItemButtonEditEquipmentName.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions8, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject29, serializableAppearanceObject30, serializableAppearanceObject31, serializableAppearanceObject32, "Click me to open the Select Equipment screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditEquipmentName.Name = "repositoryItemButtonEditEquipmentName";
            this.repositoryItemButtonEditEquipmentName.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemButtonEditEquipmentName.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditEquipmentName_ButtonClick);
            // 
            // gridColumn249
            // 
            this.gridColumn249.Caption = "Cost Unit Description";
            this.gridColumn249.FieldName = "CostUnitDescriptor";
            this.gridColumn249.Name = "gridColumn249";
            this.gridColumn249.OptionsColumn.AllowEdit = false;
            this.gridColumn249.OptionsColumn.AllowFocus = false;
            this.gridColumn249.OptionsColumn.ReadOnly = true;
            this.gridColumn249.Width = 121;
            // 
            // gridColumn250
            // 
            this.gridColumn250.Caption = "Sell Unit Description";
            this.gridColumn250.FieldName = "SellUnitDescriptor";
            this.gridColumn250.Name = "gridColumn250";
            this.gridColumn250.OptionsColumn.AllowEdit = false;
            this.gridColumn250.OptionsColumn.AllowFocus = false;
            this.gridColumn250.OptionsColumn.ReadOnly = true;
            this.gridColumn250.Width = 115;
            // 
            // colOwnerType
            // 
            this.colOwnerType.AppearanceCell.ForeColor = System.Drawing.Color.Gray;
            this.colOwnerType.AppearanceCell.Options.UseForeColor = true;
            this.colOwnerType.Caption = "Owner Type";
            this.colOwnerType.FieldName = "OwnerType";
            this.colOwnerType.Name = "colOwnerType";
            this.colOwnerType.OptionsColumn.AllowEdit = false;
            this.colOwnerType.OptionsColumn.AllowFocus = false;
            this.colOwnerType.OptionsColumn.ReadOnly = true;
            this.colOwnerType.Visible = true;
            this.colOwnerType.VisibleIndex = 1;
            this.colOwnerType.Width = 80;
            // 
            // gridColumn254
            // 
            this.gridColumn254.Caption = "PO ID";
            this.gridColumn254.FieldName = "PurchaseOrderID";
            this.gridColumn254.Name = "gridColumn254";
            this.gridColumn254.OptionsColumn.AllowEdit = false;
            this.gridColumn254.OptionsColumn.AllowFocus = false;
            this.gridColumn254.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn255
            // 
            this.gridColumn255.Caption = "Start Date";
            this.gridColumn255.ColumnEdit = this.repositoryItemTextEdit12;
            this.gridColumn255.FieldName = "StartDateTime";
            this.gridColumn255.Name = "gridColumn255";
            this.gridColumn255.OptionsColumn.AllowEdit = false;
            this.gridColumn255.OptionsColumn.AllowFocus = false;
            this.gridColumn255.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemTextEdit12
            // 
            this.repositoryItemTextEdit12.AutoHeight = false;
            this.repositoryItemTextEdit12.Mask.EditMask = "g";
            this.repositoryItemTextEdit12.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit12.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit12.Name = "repositoryItemTextEdit12";
            // 
            // gridColumn256
            // 
            this.gridColumn256.Caption = "End Date";
            this.gridColumn256.ColumnEdit = this.repositoryItemTextEdit12;
            this.gridColumn256.FieldName = "EndDateTime";
            this.gridColumn256.Name = "gridColumn256";
            this.gridColumn256.OptionsColumn.AllowEdit = false;
            this.gridColumn256.OptionsColumn.AllowFocus = false;
            this.gridColumn256.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn257
            // 
            this.gridColumn257.Caption = "Contract Description";
            this.gridColumn257.FieldName = "ContractDescription";
            this.gridColumn257.Name = "gridColumn257";
            this.gridColumn257.OptionsColumn.AllowEdit = false;
            this.gridColumn257.OptionsColumn.AllowFocus = false;
            this.gridColumn257.OptionsColumn.ReadOnly = true;
            this.gridColumn257.Width = 119;
            // 
            // gridColumn258
            // 
            this.gridColumn258.Caption = "Client \\ Contract Description";
            this.gridColumn258.FieldName = "ClientNameContractDescription";
            this.gridColumn258.Name = "gridColumn258";
            this.gridColumn258.OptionsColumn.AllowEdit = false;
            this.gridColumn258.OptionsColumn.AllowFocus = false;
            this.gridColumn258.OptionsColumn.ReadOnly = true;
            this.gridColumn258.Visible = true;
            this.gridColumn258.VisibleIndex = 22;
            this.gridColumn258.Width = 361;
            // 
            // gridColumn259
            // 
            this.gridColumn259.Caption = "Job Type \\ Job Sub-Type";
            this.gridColumn259.FieldName = "JobTypeJobSubTypeDescription";
            this.gridColumn259.Name = "gridColumn259";
            this.gridColumn259.OptionsColumn.AllowEdit = false;
            this.gridColumn259.OptionsColumn.AllowFocus = false;
            this.gridColumn259.OptionsColumn.ReadOnly = true;
            this.gridColumn259.Visible = true;
            this.gridColumn259.VisibleIndex = 23;
            this.gridColumn259.Width = 396;
            // 
            // gridColumn260
            // 
            this.gridColumn260.AppearanceCell.ForeColor = System.Drawing.Color.Gray;
            this.gridColumn260.AppearanceCell.Options.UseForeColor = true;
            this.gridColumn260.Caption = "Distance From Site Lat\\Long";
            this.gridColumn260.ColumnEdit = this.repositoryItemTextEdit13;
            this.gridColumn260.FieldName = "LatLongSiteDistance";
            this.gridColumn260.Name = "gridColumn260";
            this.gridColumn260.OptionsColumn.AllowEdit = false;
            this.gridColumn260.OptionsColumn.AllowFocus = false;
            this.gridColumn260.OptionsColumn.ReadOnly = true;
            this.gridColumn260.Width = 155;
            // 
            // repositoryItemTextEdit13
            // 
            this.repositoryItemTextEdit13.AutoHeight = false;
            this.repositoryItemTextEdit13.Mask.EditMask = "########0.00 Miles";
            this.repositoryItemTextEdit13.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit13.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit13.Name = "repositoryItemTextEdit13";
            // 
            // gridColumn261
            // 
            this.gridColumn261.AppearanceCell.ForeColor = System.Drawing.Color.Gray;
            this.gridColumn261.AppearanceCell.Options.UseForeColor = true;
            this.gridColumn261.Caption = "Distance From Site Postcode";
            this.gridColumn261.ColumnEdit = this.repositoryItemTextEdit13;
            this.gridColumn261.FieldName = "PostcodeSiteDistance";
            this.gridColumn261.Name = "gridColumn261";
            this.gridColumn261.OptionsColumn.AllowEdit = false;
            this.gridColumn261.OptionsColumn.AllowFocus = false;
            this.gridColumn261.OptionsColumn.ReadOnly = true;
            this.gridColumn261.Width = 157;
            // 
            // colOwnerName
            // 
            this.colOwnerName.AppearanceCell.ForeColor = System.Drawing.Color.Gray;
            this.colOwnerName.AppearanceCell.Options.UseForeColor = true;
            this.colOwnerName.Caption = "Owner Name";
            this.colOwnerName.FieldName = "OwnerName";
            this.colOwnerName.Name = "colOwnerName";
            this.colOwnerName.OptionsColumn.AllowEdit = false;
            this.colOwnerName.OptionsColumn.AllowFocus = false;
            this.colOwnerName.OptionsColumn.ReadOnly = true;
            this.colOwnerName.Visible = true;
            this.colOwnerName.VisibleIndex = 2;
            this.colOwnerName.Width = 140;
            // 
            // colJobSubTypeID2
            // 
            this.colJobSubTypeID2.Caption = "Job Sub-Type ID";
            this.colJobSubTypeID2.FieldName = "JobSubTypeID";
            this.colJobSubTypeID2.Name = "colJobSubTypeID2";
            this.colJobSubTypeID2.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeID2.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeID2.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeID2.Width = 101;
            // 
            // colJobTypeID2
            // 
            this.colJobTypeID2.Caption = "Job Type ID";
            this.colJobTypeID2.FieldName = "JobTypeID";
            this.colJobTypeID2.Name = "colJobTypeID2";
            this.colJobTypeID2.OptionsColumn.AllowEdit = false;
            this.colJobTypeID2.OptionsColumn.AllowFocus = false;
            this.colJobTypeID2.OptionsColumn.ReadOnly = true;
            this.colJobTypeID2.Width = 81;
            // 
            // btnStep7Next
            // 
            this.btnStep7Next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep7Next.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.btnStep7Next.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnStep7Next.Location = new System.Drawing.Point(1119, 501);
            this.btnStep7Next.Name = "btnStep7Next";
            this.btnStep7Next.Size = new System.Drawing.Size(88, 30);
            this.btnStep7Next.TabIndex = 21;
            this.btnStep7Next.Text = "Next";
            this.btnStep7Next.Click += new System.EventHandler(this.btnStep7Next_Click);
            // 
            // btnStep7Previous
            // 
            this.btnStep7Previous.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep7Previous.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnStep7Previous.Location = new System.Drawing.Point(1025, 501);
            this.btnStep7Previous.Name = "btnStep7Previous";
            this.btnStep7Previous.Size = new System.Drawing.Size(88, 30);
            this.btnStep7Previous.TabIndex = 22;
            this.btnStep7Previous.Text = "Previous";
            this.btnStep7Previous.Click += new System.EventHandler(this.btnStep7Previous_Click);
            // 
            // panelControl11
            // 
            this.panelControl11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl11.Controls.Add(this.pictureEdit11);
            this.panelControl11.Controls.Add(this.labelControl23);
            this.panelControl11.Controls.Add(this.labelControl24);
            this.panelControl11.Location = new System.Drawing.Point(7, 6);
            this.panelControl11.Name = "panelControl11";
            this.panelControl11.Size = new System.Drawing.Size(1200, 48);
            this.panelControl11.TabIndex = 19;
            // 
            // pictureEdit11
            // 
            this.pictureEdit11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit11.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit11.Location = new System.Drawing.Point(1156, 4);
            this.pictureEdit11.MenuManager = this.barManager1;
            this.pictureEdit11.Name = "pictureEdit11";
            this.pictureEdit11.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit11.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit11.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit11.Properties.ReadOnly = true;
            this.pictureEdit11.Properties.ShowMenu = false;
            this.pictureEdit11.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit11.TabIndex = 9;
            // 
            // labelControl23
            // 
            this.labelControl23.AllowHtmlString = true;
            this.labelControl23.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl23.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl23.Appearance.Options.UseBackColor = true;
            this.labelControl23.Appearance.Options.UseFont = true;
            this.labelControl23.Location = new System.Drawing.Point(5, 5);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(210, 16);
            this.labelControl23.TabIndex = 6;
            this.labelControl23.Text = "<b>Step 7:</b> Add Equipment  <b>[Optional]</b>";
            // 
            // labelControl24
            // 
            this.labelControl24.AllowHtmlString = true;
            this.labelControl24.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl24.Appearance.Options.UseBackColor = true;
            this.labelControl24.Location = new System.Drawing.Point(57, 29);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(674, 13);
            this.labelControl24.TabIndex = 7;
            this.labelControl24.Text = "Select one or more Jobs by clicking on them then click the required Add Button to" +
    " add equipment to the selected jobs. Click Next when Done.";
            // 
            // xtraTabPageStep8
            // 
            this.xtraTabPageStep8.Controls.Add(this.splitContainerControl4);
            this.xtraTabPageStep8.Controls.Add(this.btnStep8Next);
            this.xtraTabPageStep8.Controls.Add(this.btnStep8Previous);
            this.xtraTabPageStep8.Controls.Add(this.panelControl12);
            this.xtraTabPageStep8.Name = "xtraTabPageStep8";
            this.xtraTabPageStep8.Size = new System.Drawing.Size(1213, 537);
            this.xtraTabPageStep8.Tag = "8";
            this.xtraTabPageStep8.Text = "Step 8";
            // 
            // splitContainerControl4
            // 
            this.splitContainerControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl4.Location = new System.Drawing.Point(6, 62);
            this.splitContainerControl4.Name = "splitContainerControl4";
            this.splitContainerControl4.Panel1.Controls.Add(this.gridControl11);
            this.splitContainerControl4.Panel1.Text = "Panel1";
            this.splitContainerControl4.Panel2.Controls.Add(this.standaloneBarDockControl5);
            this.splitContainerControl4.Panel2.Controls.Add(this.gridControl12);
            this.splitContainerControl4.Panel2.Text = "Panel2";
            this.splitContainerControl4.Size = new System.Drawing.Size(1201, 433);
            this.splitContainerControl4.SplitterPosition = 471;
            this.splitContainerControl4.TabIndex = 30;
            this.splitContainerControl4.Text = "splitContainerControl4";
            // 
            // gridControl11
            // 
            this.gridControl11.DataSource = this.sp06174OMJobEditBindingSource;
            this.gridControl11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl11.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl11.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl11.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl11.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl11.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl11.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl11.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl11.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl11.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl11.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl11.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl11.Location = new System.Drawing.Point(0, 0);
            this.gridControl11.MainView = this.gridView11;
            this.gridControl11.MenuManager = this.barManager1;
            this.gridControl11.Name = "gridControl11";
            this.gridControl11.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit12,
            this.repositoryItemTextEdit7,
            this.repositoryItemTextEdit9,
            this.repositoryItemTextEdit8,
            this.repositoryItemTextEdit10,
            this.repositoryItemTextEdit11,
            this.repositoryItemCheckEdit6});
            this.gridControl11.Size = new System.Drawing.Size(471, 433);
            this.gridControl11.TabIndex = 24;
            this.gridControl11.UseEmbeddedNavigator = true;
            this.gridControl11.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView11});
            // 
            // gridView11
            // 
            this.gridView11.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn251,
            this.gridColumn252,
            this.gridColumn253,
            this.gridColumn262,
            this.gridColumn263,
            this.gridColumn264,
            this.gridColumn265,
            this.gridColumn266,
            this.gridColumn267,
            this.gridColumn268,
            this.gridColumn269,
            this.gridColumn270,
            this.gridColumn271,
            this.gridColumn272,
            this.gridColumn273,
            this.gridColumn274,
            this.gridColumn275,
            this.gridColumn276,
            this.gridColumn277,
            this.gridColumn278,
            this.gridColumn279,
            this.gridColumn280,
            this.gridColumn281,
            this.gridColumn282,
            this.gridColumn283,
            this.gridColumn284,
            this.gridColumn285,
            this.gridColumn286,
            this.gridColumn287,
            this.gridColumn288,
            this.gridColumn289,
            this.gridColumn290,
            this.gridColumn291,
            this.gridColumn292,
            this.gridColumn293,
            this.gridColumn294,
            this.gridColumn295,
            this.gridColumn296,
            this.gridColumn297,
            this.gridColumn298,
            this.gridColumn299,
            this.gridColumn300,
            this.gridColumn301,
            this.gridColumn302,
            this.gridColumn303,
            this.gridColumn304,
            this.gridColumn305,
            this.gridColumn306,
            this.gridColumn307,
            this.gridColumn308,
            this.gridColumn309,
            this.gridColumn310,
            this.gridColumn311,
            this.gridColumn312,
            this.gridColumn313,
            this.gridColumn314,
            this.gridColumn315,
            this.gridColumn316,
            this.gridColumn317,
            this.gridColumn318,
            this.gridColumn319,
            this.gridColumn320,
            this.gridColumn321,
            this.gridColumn322,
            this.gridColumn323,
            this.gridColumn324,
            this.gridColumn325,
            this.gridColumn326,
            this.gridColumn327,
            this.gridColumn328,
            this.gridColumn329,
            this.gridColumn330,
            this.gridColumn331,
            this.gridColumn332,
            this.gridColumn333,
            this.gridColumn334,
            this.gridColumn335,
            this.gridColumn336,
            this.gridColumn337,
            this.gridColumn338,
            this.gridColumn339,
            this.gridColumn340,
            this.gridColumn341,
            this.gridColumn342,
            this.gridColumn343,
            this.gridColumn344,
            this.gridColumn345,
            this.gridColumn346,
            this.gridColumn347,
            this.gridColumn348,
            this.gridColumn349,
            this.gridColumn350,
            this.gridColumn351,
            this.gridColumn352});
            this.gridView11.GridControl = this.gridControl11;
            this.gridView11.GroupCount = 3;
            this.gridView11.Name = "gridView11";
            this.gridView11.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView11.OptionsFilter.AllowFilterEditor = false;
            this.gridView11.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView11.OptionsFilter.AllowMRUFilterList = false;
            this.gridView11.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView11.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView11.OptionsFind.FindDelay = 2000;
            this.gridView11.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView11.OptionsLayout.StoreAppearance = true;
            this.gridView11.OptionsLayout.StoreFormatRules = true;
            this.gridView11.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView11.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView11.OptionsSelection.MultiSelect = true;
            this.gridView11.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView11.OptionsView.ColumnAutoWidth = false;
            this.gridView11.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView11.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView11.OptionsView.ShowGroupPanel = false;
            this.gridView11.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn338, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn340, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn349, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn347, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView11.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView11_CustomDrawCell);
            this.gridView11.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView11.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView11.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView11.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView11.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView11.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView11.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView11_MouseUp);
            this.gridView11.GotFocus += new System.EventHandler(this.gridView11_GotFocus);
            // 
            // gridColumn251
            // 
            this.gridColumn251.Caption = "Mode";
            this.gridColumn251.FieldName = "strMode";
            this.gridColumn251.Name = "gridColumn251";
            this.gridColumn251.OptionsColumn.AllowEdit = false;
            this.gridColumn251.OptionsColumn.AllowFocus = false;
            this.gridColumn251.OptionsColumn.ReadOnly = true;
            this.gridColumn251.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // gridColumn252
            // 
            this.gridColumn252.Caption = "Record IDs";
            this.gridColumn252.FieldName = "strRecordIDs";
            this.gridColumn252.Name = "gridColumn252";
            this.gridColumn252.OptionsColumn.AllowEdit = false;
            this.gridColumn252.OptionsColumn.AllowFocus = false;
            this.gridColumn252.OptionsColumn.ReadOnly = true;
            this.gridColumn252.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // gridColumn253
            // 
            this.gridColumn253.Caption = "Dummy Job ID";
            this.gridColumn253.FieldName = "DummyJobID";
            this.gridColumn253.Name = "gridColumn253";
            this.gridColumn253.OptionsColumn.AllowEdit = false;
            this.gridColumn253.OptionsColumn.AllowFocus = false;
            this.gridColumn253.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn262
            // 
            this.gridColumn262.Caption = "Job ID";
            this.gridColumn262.FieldName = "JobID";
            this.gridColumn262.Name = "gridColumn262";
            this.gridColumn262.OptionsColumn.AllowEdit = false;
            this.gridColumn262.OptionsColumn.AllowFocus = false;
            this.gridColumn262.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn263
            // 
            this.gridColumn263.Caption = "Visit ID";
            this.gridColumn263.FieldName = "VisitID";
            this.gridColumn263.Name = "gridColumn263";
            this.gridColumn263.OptionsColumn.AllowEdit = false;
            this.gridColumn263.OptionsColumn.AllowFocus = false;
            this.gridColumn263.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn264
            // 
            this.gridColumn264.Caption = "Status ID";
            this.gridColumn264.FieldName = "JobStatusID";
            this.gridColumn264.Name = "gridColumn264";
            this.gridColumn264.OptionsColumn.AllowEdit = false;
            this.gridColumn264.OptionsColumn.AllowFocus = false;
            this.gridColumn264.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn265
            // 
            this.gridColumn265.Caption = "Job Sub-Type ID";
            this.gridColumn265.FieldName = "JobSubTypeID";
            this.gridColumn265.Name = "gridColumn265";
            this.gridColumn265.OptionsColumn.AllowEdit = false;
            this.gridColumn265.OptionsColumn.AllowFocus = false;
            this.gridColumn265.OptionsColumn.ReadOnly = true;
            this.gridColumn265.Width = 101;
            // 
            // gridColumn266
            // 
            this.gridColumn266.Caption = "Created By Staff ID";
            this.gridColumn266.FieldName = "CreatedByStaffID";
            this.gridColumn266.Name = "gridColumn266";
            this.gridColumn266.OptionsColumn.AllowEdit = false;
            this.gridColumn266.OptionsColumn.AllowFocus = false;
            this.gridColumn266.OptionsColumn.ReadOnly = true;
            this.gridColumn266.Width = 116;
            // 
            // gridColumn267
            // 
            this.gridColumn267.Caption = "Reactive";
            this.gridColumn267.ColumnEdit = this.repositoryItemCheckEdit6;
            this.gridColumn267.FieldName = "Reactive";
            this.gridColumn267.Name = "gridColumn267";
            this.gridColumn267.OptionsColumn.AllowEdit = false;
            this.gridColumn267.OptionsColumn.AllowFocus = false;
            this.gridColumn267.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemCheckEdit6
            // 
            this.repositoryItemCheckEdit6.AutoHeight = false;
            this.repositoryItemCheckEdit6.Caption = "Check";
            this.repositoryItemCheckEdit6.Name = "repositoryItemCheckEdit6";
            this.repositoryItemCheckEdit6.ValueChecked = 1;
            this.repositoryItemCheckEdit6.ValueUnchecked = 0;
            // 
            // gridColumn268
            // 
            this.gridColumn268.Caption = "Job No Longer Required";
            this.gridColumn268.ColumnEdit = this.repositoryItemCheckEdit6;
            this.gridColumn268.FieldName = "JobNoLongerRequired";
            this.gridColumn268.Name = "gridColumn268";
            this.gridColumn268.OptionsColumn.AllowEdit = false;
            this.gridColumn268.OptionsColumn.AllowFocus = false;
            this.gridColumn268.OptionsColumn.ReadOnly = true;
            this.gridColumn268.Width = 136;
            // 
            // gridColumn269
            // 
            this.gridColumn269.Caption = "Requires Access Permit";
            this.gridColumn269.ColumnEdit = this.repositoryItemCheckEdit6;
            this.gridColumn269.FieldName = "RequiresAccessPermit";
            this.gridColumn269.Name = "gridColumn269";
            this.gridColumn269.OptionsColumn.AllowEdit = false;
            this.gridColumn269.OptionsColumn.AllowFocus = false;
            this.gridColumn269.OptionsColumn.ReadOnly = true;
            this.gridColumn269.Visible = true;
            this.gridColumn269.VisibleIndex = 9;
            this.gridColumn269.Width = 132;
            // 
            // gridColumn270
            // 
            this.gridColumn270.Caption = "Access Permit ID";
            this.gridColumn270.FieldName = "AccessPermitID";
            this.gridColumn270.Name = "gridColumn270";
            this.gridColumn270.OptionsColumn.AllowEdit = false;
            this.gridColumn270.OptionsColumn.AllowFocus = false;
            this.gridColumn270.OptionsColumn.ReadOnly = true;
            this.gridColumn270.Width = 101;
            // 
            // gridColumn271
            // 
            this.gridColumn271.Caption = "CLient P.O. #";
            this.gridColumn271.FieldName = "ClientPONumber";
            this.gridColumn271.Name = "gridColumn271";
            this.gridColumn271.OptionsColumn.AllowEdit = false;
            this.gridColumn271.OptionsColumn.AllowFocus = false;
            this.gridColumn271.OptionsColumn.ReadOnly = true;
            this.gridColumn271.Width = 87;
            // 
            // gridColumn272
            // 
            this.gridColumn272.Caption = "CLient P.O. ID";
            this.gridColumn272.FieldName = "ClientPOID";
            this.gridColumn272.Name = "gridColumn272";
            this.gridColumn272.OptionsColumn.AllowEdit = false;
            this.gridColumn272.OptionsColumn.AllowFocus = false;
            this.gridColumn272.OptionsColumn.ReadOnly = true;
            this.gridColumn272.Width = 90;
            // 
            // gridColumn273
            // 
            this.gridColumn273.Caption = "Finance System P.O. #";
            this.gridColumn273.FieldName = "FinanceSystemPONumber";
            this.gridColumn273.Name = "gridColumn273";
            this.gridColumn273.OptionsColumn.AllowEdit = false;
            this.gridColumn273.OptionsColumn.AllowFocus = false;
            this.gridColumn273.OptionsColumn.ReadOnly = true;
            this.gridColumn273.Width = 132;
            // 
            // gridColumn274
            // 
            this.gridColumn274.Caption = "Expected Start Date";
            this.gridColumn274.ColumnEdit = this.repositoryItemTextEdit7;
            this.gridColumn274.FieldName = "ExpectedStartDate";
            this.gridColumn274.Name = "gridColumn274";
            this.gridColumn274.OptionsColumn.AllowEdit = false;
            this.gridColumn274.OptionsColumn.AllowFocus = false;
            this.gridColumn274.OptionsColumn.ReadOnly = true;
            this.gridColumn274.Visible = true;
            this.gridColumn274.VisibleIndex = 2;
            this.gridColumn274.Width = 119;
            // 
            // repositoryItemTextEdit7
            // 
            this.repositoryItemTextEdit7.AutoHeight = false;
            this.repositoryItemTextEdit7.Mask.EditMask = "g";
            this.repositoryItemTextEdit7.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit7.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit7.Name = "repositoryItemTextEdit7";
            // 
            // gridColumn275
            // 
            this.gridColumn275.Caption = "Expected End Date";
            this.gridColumn275.ColumnEdit = this.repositoryItemTextEdit7;
            this.gridColumn275.FieldName = "ExpectedEndDate";
            this.gridColumn275.Name = "gridColumn275";
            this.gridColumn275.OptionsColumn.AllowEdit = false;
            this.gridColumn275.OptionsColumn.AllowFocus = false;
            this.gridColumn275.OptionsColumn.ReadOnly = true;
            this.gridColumn275.Visible = true;
            this.gridColumn275.VisibleIndex = 5;
            this.gridColumn275.Width = 113;
            // 
            // gridColumn276
            // 
            this.gridColumn276.Caption = "Expected Duraction";
            this.gridColumn276.ColumnEdit = this.repositoryItemTextEdit8;
            this.gridColumn276.FieldName = "ExpectedDurationUnits";
            this.gridColumn276.Name = "gridColumn276";
            this.gridColumn276.OptionsColumn.AllowEdit = false;
            this.gridColumn276.OptionsColumn.AllowFocus = false;
            this.gridColumn276.OptionsColumn.ReadOnly = true;
            this.gridColumn276.Visible = true;
            this.gridColumn276.VisibleIndex = 3;
            this.gridColumn276.Width = 115;
            // 
            // repositoryItemTextEdit8
            // 
            this.repositoryItemTextEdit8.AutoHeight = false;
            this.repositoryItemTextEdit8.Mask.EditMask = "n2";
            this.repositoryItemTextEdit8.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit8.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit8.Name = "repositoryItemTextEdit8";
            // 
            // gridColumn277
            // 
            this.gridColumn277.Caption = "Expected Duration Descriptor";
            this.gridColumn277.FieldName = "ExpectedDurationUnitsDescriptorID";
            this.gridColumn277.Name = "gridColumn277";
            this.gridColumn277.OptionsColumn.AllowEdit = false;
            this.gridColumn277.OptionsColumn.AllowFocus = false;
            this.gridColumn277.OptionsColumn.ReadOnly = true;
            this.gridColumn277.Visible = true;
            this.gridColumn277.VisibleIndex = 4;
            this.gridColumn277.Width = 162;
            // 
            // gridColumn278
            // 
            this.gridColumn278.Caption = "Schedule Sent Date";
            this.gridColumn278.ColumnEdit = this.repositoryItemTextEdit7;
            this.gridColumn278.FieldName = "ScheduleSentDate";
            this.gridColumn278.Name = "gridColumn278";
            this.gridColumn278.OptionsColumn.AllowEdit = false;
            this.gridColumn278.OptionsColumn.AllowFocus = false;
            this.gridColumn278.OptionsColumn.ReadOnly = true;
            this.gridColumn278.Width = 115;
            // 
            // gridColumn279
            // 
            this.gridColumn279.Caption = "Actual Start Date";
            this.gridColumn279.ColumnEdit = this.repositoryItemTextEdit7;
            this.gridColumn279.FieldName = "ActualStartDate";
            this.gridColumn279.Name = "gridColumn279";
            this.gridColumn279.OptionsColumn.AllowEdit = false;
            this.gridColumn279.OptionsColumn.AllowFocus = false;
            this.gridColumn279.OptionsColumn.ReadOnly = true;
            this.gridColumn279.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn279.Width = 104;
            // 
            // gridColumn280
            // 
            this.gridColumn280.Caption = "Actual End Date";
            this.gridColumn280.ColumnEdit = this.repositoryItemTextEdit7;
            this.gridColumn280.FieldName = "ActualEndDate";
            this.gridColumn280.Name = "gridColumn280";
            this.gridColumn280.OptionsColumn.AllowEdit = false;
            this.gridColumn280.OptionsColumn.AllowFocus = false;
            this.gridColumn280.OptionsColumn.ReadOnly = true;
            this.gridColumn280.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn280.Width = 98;
            // 
            // gridColumn281
            // 
            this.gridColumn281.Caption = "Actual Duration Units";
            this.gridColumn281.ColumnEdit = this.repositoryItemTextEdit8;
            this.gridColumn281.FieldName = "ActualDurationUnits";
            this.gridColumn281.Name = "gridColumn281";
            this.gridColumn281.OptionsColumn.AllowEdit = false;
            this.gridColumn281.OptionsColumn.AllowFocus = false;
            this.gridColumn281.OptionsColumn.ReadOnly = true;
            this.gridColumn281.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn281.Width = 122;
            // 
            // gridColumn282
            // 
            this.gridColumn282.Caption = "Actual Duration Unit Descriptor";
            this.gridColumn282.FieldName = "ActualDurationUnitsDescriptionID";
            this.gridColumn282.Name = "gridColumn282";
            this.gridColumn282.OptionsColumn.AllowEdit = false;
            this.gridColumn282.OptionsColumn.AllowFocus = false;
            this.gridColumn282.OptionsColumn.ReadOnly = true;
            this.gridColumn282.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn282.Width = 169;
            // 
            // gridColumn283
            // 
            this.gridColumn283.Caption = "Cost - Total Material Ex VAT";
            this.gridColumn283.ColumnEdit = this.repositoryItemTextEdit9;
            this.gridColumn283.FieldName = "CostTotalMaterialExVAT";
            this.gridColumn283.Name = "gridColumn283";
            this.gridColumn283.OptionsColumn.AllowEdit = false;
            this.gridColumn283.OptionsColumn.AllowFocus = false;
            this.gridColumn283.OptionsColumn.ReadOnly = true;
            this.gridColumn283.Width = 155;
            // 
            // repositoryItemTextEdit9
            // 
            this.repositoryItemTextEdit9.AutoHeight = false;
            this.repositoryItemTextEdit9.Mask.EditMask = "c";
            this.repositoryItemTextEdit9.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit9.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit9.Name = "repositoryItemTextEdit9";
            // 
            // gridColumn284
            // 
            this.gridColumn284.Caption = "Cost - Total Material VAT Rate";
            this.gridColumn284.ColumnEdit = this.repositoryItemTextEdit10;
            this.gridColumn284.FieldName = "CostTotalMaterialVATRate";
            this.gridColumn284.Name = "gridColumn284";
            this.gridColumn284.OptionsColumn.AllowEdit = false;
            this.gridColumn284.OptionsColumn.AllowFocus = false;
            this.gridColumn284.OptionsColumn.ReadOnly = true;
            this.gridColumn284.Width = 166;
            // 
            // repositoryItemTextEdit10
            // 
            this.repositoryItemTextEdit10.AutoHeight = false;
            this.repositoryItemTextEdit10.Mask.EditMask = "P";
            this.repositoryItemTextEdit10.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit10.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit10.Name = "repositoryItemTextEdit10";
            // 
            // gridColumn285
            // 
            this.gridColumn285.Caption = "Cost - Total Material VAT";
            this.gridColumn285.ColumnEdit = this.repositoryItemTextEdit9;
            this.gridColumn285.FieldName = "CostTotalMaterialVAT";
            this.gridColumn285.Name = "gridColumn285";
            this.gridColumn285.OptionsColumn.AllowEdit = false;
            this.gridColumn285.OptionsColumn.AllowFocus = false;
            this.gridColumn285.OptionsColumn.ReadOnly = true;
            this.gridColumn285.Width = 140;
            // 
            // gridColumn286
            // 
            this.gridColumn286.Caption = "Cost - Total Material Value";
            this.gridColumn286.ColumnEdit = this.repositoryItemTextEdit9;
            this.gridColumn286.FieldName = "CostTotalMaterialCost";
            this.gridColumn286.Name = "gridColumn286";
            this.gridColumn286.OptionsColumn.AllowEdit = false;
            this.gridColumn286.OptionsColumn.AllowFocus = false;
            this.gridColumn286.OptionsColumn.ReadOnly = true;
            this.gridColumn286.Width = 147;
            // 
            // gridColumn287
            // 
            this.gridColumn287.Caption = "Sell - Total Material Ex VAT";
            this.gridColumn287.ColumnEdit = this.repositoryItemTextEdit9;
            this.gridColumn287.FieldName = "SellTotalMaterialExVAT";
            this.gridColumn287.Name = "gridColumn287";
            this.gridColumn287.OptionsColumn.AllowEdit = false;
            this.gridColumn287.OptionsColumn.AllowFocus = false;
            this.gridColumn287.OptionsColumn.ReadOnly = true;
            this.gridColumn287.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn287.Width = 149;
            // 
            // gridColumn288
            // 
            this.gridColumn288.Caption = "Sell - Total Material VAT Rate";
            this.gridColumn288.ColumnEdit = this.repositoryItemTextEdit10;
            this.gridColumn288.FieldName = "SellTotalMaterialVATRate";
            this.gridColumn288.Name = "gridColumn288";
            this.gridColumn288.OptionsColumn.AllowEdit = false;
            this.gridColumn288.OptionsColumn.AllowFocus = false;
            this.gridColumn288.OptionsColumn.ReadOnly = true;
            this.gridColumn288.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn288.Width = 160;
            // 
            // gridColumn289
            // 
            this.gridColumn289.Caption = "Sell - Total Material VAT";
            this.gridColumn289.ColumnEdit = this.repositoryItemTextEdit9;
            this.gridColumn289.FieldName = "SellTotalMaterialVAT";
            this.gridColumn289.Name = "gridColumn289";
            this.gridColumn289.OptionsColumn.AllowEdit = false;
            this.gridColumn289.OptionsColumn.AllowFocus = false;
            this.gridColumn289.OptionsColumn.ReadOnly = true;
            this.gridColumn289.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn289.Width = 134;
            // 
            // gridColumn290
            // 
            this.gridColumn290.Caption = "Sell - Total Material Value";
            this.gridColumn290.ColumnEdit = this.repositoryItemTextEdit9;
            this.gridColumn290.FieldName = "SellTotalMaterialCost";
            this.gridColumn290.Name = "gridColumn290";
            this.gridColumn290.OptionsColumn.AllowEdit = false;
            this.gridColumn290.OptionsColumn.AllowFocus = false;
            this.gridColumn290.OptionsColumn.ReadOnly = true;
            this.gridColumn290.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn290.Width = 141;
            // 
            // gridColumn291
            // 
            this.gridColumn291.Caption = "Cost - Total Equipment Value";
            this.gridColumn291.ColumnEdit = this.repositoryItemTextEdit9;
            this.gridColumn291.FieldName = "CostTotalEquipmentExVAT";
            this.gridColumn291.Name = "gridColumn291";
            this.gridColumn291.OptionsColumn.AllowEdit = false;
            this.gridColumn291.OptionsColumn.AllowFocus = false;
            this.gridColumn291.OptionsColumn.ReadOnly = true;
            this.gridColumn291.Width = 159;
            // 
            // gridColumn292
            // 
            this.gridColumn292.Caption = "Cost - Total Equipment VAT Rate";
            this.gridColumn292.ColumnEdit = this.repositoryItemTextEdit10;
            this.gridColumn292.FieldName = "CostTotalEquipmentVATRate";
            this.gridColumn292.Name = "gridColumn292";
            this.gridColumn292.OptionsColumn.AllowEdit = false;
            this.gridColumn292.OptionsColumn.AllowFocus = false;
            this.gridColumn292.OptionsColumn.ReadOnly = true;
            this.gridColumn292.Width = 178;
            // 
            // gridColumn293
            // 
            this.gridColumn293.Caption = "Cost - Total Equipment VAT";
            this.gridColumn293.ColumnEdit = this.repositoryItemTextEdit9;
            this.gridColumn293.FieldName = "CostTotalEquipmentVAT";
            this.gridColumn293.Name = "gridColumn293";
            this.gridColumn293.OptionsColumn.AllowEdit = false;
            this.gridColumn293.OptionsColumn.AllowFocus = false;
            this.gridColumn293.OptionsColumn.ReadOnly = true;
            this.gridColumn293.Width = 154;
            // 
            // gridColumn294
            // 
            this.gridColumn294.Caption = "Cost - Total Equipment Value";
            this.gridColumn294.ColumnEdit = this.repositoryItemTextEdit9;
            this.gridColumn294.FieldName = "CostTotalEquipmentCost";
            this.gridColumn294.Name = "gridColumn294";
            this.gridColumn294.OptionsColumn.AllowEdit = false;
            this.gridColumn294.OptionsColumn.AllowFocus = false;
            this.gridColumn294.OptionsColumn.ReadOnly = true;
            this.gridColumn294.Width = 159;
            // 
            // gridColumn295
            // 
            this.gridColumn295.Caption = "Sell - Total Equipment Ex VAT";
            this.gridColumn295.ColumnEdit = this.repositoryItemTextEdit9;
            this.gridColumn295.FieldName = "SellTotalEquipmentExVAT";
            this.gridColumn295.Name = "gridColumn295";
            this.gridColumn295.OptionsColumn.AllowEdit = false;
            this.gridColumn295.OptionsColumn.AllowFocus = false;
            this.gridColumn295.OptionsColumn.ReadOnly = true;
            this.gridColumn295.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn295.Width = 161;
            // 
            // gridColumn296
            // 
            this.gridColumn296.Caption = "Sell - Total Equipment VAT Rate";
            this.gridColumn296.ColumnEdit = this.repositoryItemTextEdit10;
            this.gridColumn296.FieldName = "SellTotalEquipmentVATRate";
            this.gridColumn296.Name = "gridColumn296";
            this.gridColumn296.OptionsColumn.AllowEdit = false;
            this.gridColumn296.OptionsColumn.AllowFocus = false;
            this.gridColumn296.OptionsColumn.ReadOnly = true;
            this.gridColumn296.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn296.Width = 172;
            // 
            // gridColumn297
            // 
            this.gridColumn297.Caption = "Sell - Total Equipment VAT";
            this.gridColumn297.ColumnEdit = this.repositoryItemTextEdit9;
            this.gridColumn297.FieldName = "SellTotalEquipmentVAT";
            this.gridColumn297.Name = "gridColumn297";
            this.gridColumn297.OptionsColumn.AllowEdit = false;
            this.gridColumn297.OptionsColumn.AllowFocus = false;
            this.gridColumn297.OptionsColumn.ReadOnly = true;
            this.gridColumn297.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn297.Width = 146;
            // 
            // gridColumn298
            // 
            this.gridColumn298.Caption = "Sell - Total Equipment Value";
            this.gridColumn298.ColumnEdit = this.repositoryItemTextEdit9;
            this.gridColumn298.FieldName = "SellTotalEquipmentCost";
            this.gridColumn298.Name = "gridColumn298";
            this.gridColumn298.OptionsColumn.AllowEdit = false;
            this.gridColumn298.OptionsColumn.AllowFocus = false;
            this.gridColumn298.OptionsColumn.ReadOnly = true;
            this.gridColumn298.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn298.Width = 153;
            // 
            // gridColumn299
            // 
            this.gridColumn299.Caption = "Cost - Total Labour Ex VAT";
            this.gridColumn299.ColumnEdit = this.repositoryItemTextEdit9;
            this.gridColumn299.FieldName = "CostTotalLabourExVAT";
            this.gridColumn299.Name = "gridColumn299";
            this.gridColumn299.OptionsColumn.AllowEdit = false;
            this.gridColumn299.OptionsColumn.AllowFocus = false;
            this.gridColumn299.OptionsColumn.ReadOnly = true;
            this.gridColumn299.Width = 150;
            // 
            // gridColumn300
            // 
            this.gridColumn300.Caption = "Cost - Total Labour VAT Rate";
            this.gridColumn300.ColumnEdit = this.repositoryItemTextEdit10;
            this.gridColumn300.FieldName = "CostTotalLabourVATRate";
            this.gridColumn300.Name = "gridColumn300";
            this.gridColumn300.OptionsColumn.AllowEdit = false;
            this.gridColumn300.OptionsColumn.AllowFocus = false;
            this.gridColumn300.OptionsColumn.ReadOnly = true;
            this.gridColumn300.Width = 161;
            // 
            // gridColumn301
            // 
            this.gridColumn301.Caption = "Cost - Total Labour VAT";
            this.gridColumn301.ColumnEdit = this.repositoryItemTextEdit9;
            this.gridColumn301.FieldName = "CostTotalLabourVAT";
            this.gridColumn301.Name = "gridColumn301";
            this.gridColumn301.OptionsColumn.AllowEdit = false;
            this.gridColumn301.OptionsColumn.AllowFocus = false;
            this.gridColumn301.OptionsColumn.ReadOnly = true;
            this.gridColumn301.Width = 135;
            // 
            // gridColumn302
            // 
            this.gridColumn302.Caption = "Cost - Total Labour Value";
            this.gridColumn302.ColumnEdit = this.repositoryItemTextEdit9;
            this.gridColumn302.FieldName = "CostTotalLabourCost";
            this.gridColumn302.Name = "gridColumn302";
            this.gridColumn302.OptionsColumn.AllowEdit = false;
            this.gridColumn302.OptionsColumn.AllowFocus = false;
            this.gridColumn302.OptionsColumn.ReadOnly = true;
            this.gridColumn302.Width = 142;
            // 
            // gridColumn303
            // 
            this.gridColumn303.Caption = "Sell - Total Labour Ex VAT";
            this.gridColumn303.ColumnEdit = this.repositoryItemTextEdit9;
            this.gridColumn303.FieldName = "SellTotalLabourExVAT";
            this.gridColumn303.Name = "gridColumn303";
            this.gridColumn303.OptionsColumn.AllowEdit = false;
            this.gridColumn303.OptionsColumn.AllowFocus = false;
            this.gridColumn303.OptionsColumn.ReadOnly = true;
            this.gridColumn303.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn303.Width = 144;
            // 
            // gridColumn304
            // 
            this.gridColumn304.Caption = "Sell - Total Labour VAT Rate";
            this.gridColumn304.ColumnEdit = this.repositoryItemTextEdit10;
            this.gridColumn304.FieldName = "SellTotalLabourVATRate";
            this.gridColumn304.Name = "gridColumn304";
            this.gridColumn304.OptionsColumn.AllowEdit = false;
            this.gridColumn304.OptionsColumn.AllowFocus = false;
            this.gridColumn304.OptionsColumn.ReadOnly = true;
            this.gridColumn304.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn304.Width = 155;
            // 
            // gridColumn305
            // 
            this.gridColumn305.Caption = "Sell - Total Labour VAT";
            this.gridColumn305.ColumnEdit = this.repositoryItemTextEdit9;
            this.gridColumn305.FieldName = "SellTotalLabourVAT";
            this.gridColumn305.Name = "gridColumn305";
            this.gridColumn305.OptionsColumn.AllowEdit = false;
            this.gridColumn305.OptionsColumn.AllowFocus = false;
            this.gridColumn305.OptionsColumn.ReadOnly = true;
            this.gridColumn305.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn305.Width = 129;
            // 
            // gridColumn306
            // 
            this.gridColumn306.Caption = "Sell - Total Labour Value";
            this.gridColumn306.ColumnEdit = this.repositoryItemTextEdit9;
            this.gridColumn306.FieldName = "SellTotalLabourCost";
            this.gridColumn306.Name = "gridColumn306";
            this.gridColumn306.OptionsColumn.AllowEdit = false;
            this.gridColumn306.OptionsColumn.AllowFocus = false;
            this.gridColumn306.OptionsColumn.ReadOnly = true;
            this.gridColumn306.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn306.Width = 136;
            // 
            // gridColumn307
            // 
            this.gridColumn307.Caption = "Cost - Total Ex VAT";
            this.gridColumn307.ColumnEdit = this.repositoryItemTextEdit9;
            this.gridColumn307.FieldName = "CostTotalCostExVAT";
            this.gridColumn307.Name = "gridColumn307";
            this.gridColumn307.OptionsColumn.AllowEdit = false;
            this.gridColumn307.OptionsColumn.AllowFocus = false;
            this.gridColumn307.OptionsColumn.ReadOnly = true;
            this.gridColumn307.Width = 114;
            // 
            // gridColumn308
            // 
            this.gridColumn308.Caption = "Cost - Total VAT";
            this.gridColumn308.ColumnEdit = this.repositoryItemTextEdit9;
            this.gridColumn308.FieldName = "CostTotalCostVAT";
            this.gridColumn308.Name = "gridColumn308";
            this.gridColumn308.OptionsColumn.AllowEdit = false;
            this.gridColumn308.OptionsColumn.AllowFocus = false;
            this.gridColumn308.OptionsColumn.ReadOnly = true;
            this.gridColumn308.Width = 99;
            // 
            // gridColumn309
            // 
            this.gridColumn309.Caption = "Cost - Total Value";
            this.gridColumn309.ColumnEdit = this.repositoryItemTextEdit9;
            this.gridColumn309.FieldName = "CostTotalCost";
            this.gridColumn309.Name = "gridColumn309";
            this.gridColumn309.OptionsColumn.AllowEdit = false;
            this.gridColumn309.OptionsColumn.AllowFocus = false;
            this.gridColumn309.OptionsColumn.ReadOnly = true;
            this.gridColumn309.Width = 106;
            // 
            // gridColumn310
            // 
            this.gridColumn310.Caption = "Sell - Total Ex VAT";
            this.gridColumn310.ColumnEdit = this.repositoryItemTextEdit9;
            this.gridColumn310.FieldName = "SellTotalCostExVAT";
            this.gridColumn310.Name = "gridColumn310";
            this.gridColumn310.OptionsColumn.AllowEdit = false;
            this.gridColumn310.OptionsColumn.AllowFocus = false;
            this.gridColumn310.OptionsColumn.ReadOnly = true;
            this.gridColumn310.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn310.Width = 108;
            // 
            // gridColumn311
            // 
            this.gridColumn311.Caption = "Sell - Total VAT";
            this.gridColumn311.ColumnEdit = this.repositoryItemTextEdit9;
            this.gridColumn311.FieldName = "SellTotalCostVAT";
            this.gridColumn311.Name = "gridColumn311";
            this.gridColumn311.OptionsColumn.AllowEdit = false;
            this.gridColumn311.OptionsColumn.AllowFocus = false;
            this.gridColumn311.OptionsColumn.ReadOnly = true;
            this.gridColumn311.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn311.Width = 93;
            // 
            // gridColumn312
            // 
            this.gridColumn312.Caption = "Sell - Total Value";
            this.gridColumn312.ColumnEdit = this.repositoryItemTextEdit9;
            this.gridColumn312.FieldName = "SellTotalCost";
            this.gridColumn312.Name = "gridColumn312";
            this.gridColumn312.OptionsColumn.AllowEdit = false;
            this.gridColumn312.OptionsColumn.AllowFocus = false;
            this.gridColumn312.OptionsColumn.ReadOnly = true;
            this.gridColumn312.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn312.Width = 100;
            // 
            // gridColumn313
            // 
            this.gridColumn313.Caption = "Client Invoice ID";
            this.gridColumn313.FieldName = "ClientInvoiceID";
            this.gridColumn313.Name = "gridColumn313";
            this.gridColumn313.OptionsColumn.AllowEdit = false;
            this.gridColumn313.OptionsColumn.AllowFocus = false;
            this.gridColumn313.OptionsColumn.ReadOnly = true;
            this.gridColumn313.Width = 100;
            // 
            // gridColumn314
            // 
            this.gridColumn314.Caption = "Client Invoiced Date";
            this.gridColumn314.FieldName = "DateClientInvoiced";
            this.gridColumn314.Name = "gridColumn314";
            this.gridColumn314.OptionsColumn.AllowEdit = false;
            this.gridColumn314.OptionsColumn.AllowFocus = false;
            this.gridColumn314.OptionsColumn.ReadOnly = true;
            this.gridColumn314.Width = 118;
            // 
            // gridColumn315
            // 
            this.gridColumn315.Caption = "Self-Billing Invoice ID";
            this.gridColumn315.FieldName = "SelfBillingInvoiceID";
            this.gridColumn315.Name = "gridColumn315";
            this.gridColumn315.OptionsColumn.AllowEdit = false;
            this.gridColumn315.OptionsColumn.AllowFocus = false;
            this.gridColumn315.OptionsColumn.ReadOnly = true;
            this.gridColumn315.Width = 121;
            // 
            // gridColumn316
            // 
            this.gridColumn316.Caption = "Self-Billing Invoice Received Date";
            this.gridColumn316.ColumnEdit = this.repositoryItemTextEdit7;
            this.gridColumn316.FieldName = "SelfBillingInvoiceReceivedDate";
            this.gridColumn316.Name = "gridColumn316";
            this.gridColumn316.OptionsColumn.AllowEdit = false;
            this.gridColumn316.OptionsColumn.AllowFocus = false;
            this.gridColumn316.OptionsColumn.ReadOnly = true;
            this.gridColumn316.Width = 180;
            // 
            // gridColumn317
            // 
            this.gridColumn317.Caption = "Self-Billing Invoice Paid Date";
            this.gridColumn317.ColumnEdit = this.repositoryItemTextEdit7;
            this.gridColumn317.FieldName = "SelfBillingInvoicePaidDate";
            this.gridColumn317.Name = "gridColumn317";
            this.gridColumn317.OptionsColumn.AllowEdit = false;
            this.gridColumn317.OptionsColumn.AllowFocus = false;
            this.gridColumn317.OptionsColumn.ReadOnly = true;
            this.gridColumn317.Width = 156;
            // 
            // gridColumn318
            // 
            this.gridColumn318.Caption = "Self-Billing Invoice Amount Paid";
            this.gridColumn318.ColumnEdit = this.repositoryItemTextEdit9;
            this.gridColumn318.FieldName = "SelfBillingInvoiceAmountPaid";
            this.gridColumn318.Name = "gridColumn318";
            this.gridColumn318.OptionsColumn.AllowEdit = false;
            this.gridColumn318.OptionsColumn.AllowFocus = false;
            this.gridColumn318.OptionsColumn.ReadOnly = true;
            this.gridColumn318.Width = 170;
            // 
            // gridColumn319
            // 
            this.gridColumn319.Caption = "Do Not Pay Contractor";
            this.gridColumn319.ColumnEdit = this.repositoryItemCheckEdit6;
            this.gridColumn319.FieldName = "DoNotPayContractor";
            this.gridColumn319.Name = "gridColumn319";
            this.gridColumn319.OptionsColumn.AllowEdit = false;
            this.gridColumn319.OptionsColumn.AllowFocus = false;
            this.gridColumn319.OptionsColumn.ReadOnly = true;
            this.gridColumn319.Width = 130;
            // 
            // gridColumn320
            // 
            this.gridColumn320.Caption = "Do Not Invoice Client";
            this.gridColumn320.ColumnEdit = this.repositoryItemCheckEdit6;
            this.gridColumn320.FieldName = "DoNotInvoiceClient";
            this.gridColumn320.Name = "gridColumn320";
            this.gridColumn320.OptionsColumn.AllowEdit = false;
            this.gridColumn320.OptionsColumn.AllowFocus = false;
            this.gridColumn320.OptionsColumn.ReadOnly = true;
            this.gridColumn320.Width = 122;
            // 
            // gridColumn321
            // 
            this.gridColumn321.Caption = "Remarks";
            this.gridColumn321.ColumnEdit = this.repositoryItemMemoExEdit12;
            this.gridColumn321.FieldName = "Remarks";
            this.gridColumn321.Name = "gridColumn321";
            this.gridColumn321.OptionsColumn.ReadOnly = true;
            this.gridColumn321.Visible = true;
            this.gridColumn321.VisibleIndex = 10;
            // 
            // repositoryItemMemoExEdit12
            // 
            this.repositoryItemMemoExEdit12.AutoHeight = false;
            this.repositoryItemMemoExEdit12.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit12.Name = "repositoryItemMemoExEdit12";
            this.repositoryItemMemoExEdit12.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit12.ShowIcon = false;
            // 
            // gridColumn322
            // 
            this.gridColumn322.Caption = "Route Order";
            this.gridColumn322.FieldName = "RouteOrder";
            this.gridColumn322.Name = "gridColumn322";
            this.gridColumn322.OptionsColumn.AllowEdit = false;
            this.gridColumn322.OptionsColumn.AllowFocus = false;
            this.gridColumn322.OptionsColumn.ReadOnly = true;
            this.gridColumn322.Width = 81;
            // 
            // gridColumn323
            // 
            this.gridColumn323.Caption = "Start Latitude";
            this.gridColumn323.FieldName = "StartLatitude";
            this.gridColumn323.Name = "gridColumn323";
            this.gridColumn323.OptionsColumn.AllowEdit = false;
            this.gridColumn323.OptionsColumn.AllowFocus = false;
            this.gridColumn323.OptionsColumn.ReadOnly = true;
            this.gridColumn323.Width = 87;
            // 
            // gridColumn324
            // 
            this.gridColumn324.Caption = "Start Longitude";
            this.gridColumn324.FieldName = "StartLongitude";
            this.gridColumn324.Name = "gridColumn324";
            this.gridColumn324.OptionsColumn.AllowEdit = false;
            this.gridColumn324.OptionsColumn.AllowFocus = false;
            this.gridColumn324.OptionsColumn.ReadOnly = true;
            this.gridColumn324.Width = 95;
            // 
            // gridColumn325
            // 
            this.gridColumn325.Caption = "Finish Latitude";
            this.gridColumn325.FieldName = "FinishLatitude";
            this.gridColumn325.Name = "gridColumn325";
            this.gridColumn325.OptionsColumn.AllowEdit = false;
            this.gridColumn325.OptionsColumn.AllowFocus = false;
            this.gridColumn325.OptionsColumn.ReadOnly = true;
            this.gridColumn325.Width = 90;
            // 
            // gridColumn326
            // 
            this.gridColumn326.Caption = "Finish Longitude";
            this.gridColumn326.FieldName = "FinishLongitude";
            this.gridColumn326.Name = "gridColumn326";
            this.gridColumn326.OptionsColumn.AllowEdit = false;
            this.gridColumn326.OptionsColumn.AllowFocus = false;
            this.gridColumn326.OptionsColumn.ReadOnly = true;
            this.gridColumn326.Width = 98;
            // 
            // gridColumn327
            // 
            this.gridColumn327.Caption = "Tender ID";
            this.gridColumn327.FieldName = "TenderID";
            this.gridColumn327.Name = "gridColumn327";
            this.gridColumn327.OptionsColumn.AllowEdit = false;
            this.gridColumn327.OptionsColumn.AllowFocus = false;
            this.gridColumn327.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn328
            // 
            this.gridColumn328.Caption = "Tender Job ID";
            this.gridColumn328.FieldName = "TenderJobID";
            this.gridColumn328.Name = "gridColumn328";
            this.gridColumn328.OptionsColumn.AllowEdit = false;
            this.gridColumn328.OptionsColumn.AllowFocus = false;
            this.gridColumn328.OptionsColumn.ReadOnly = true;
            this.gridColumn328.Width = 89;
            // 
            // gridColumn329
            // 
            this.gridColumn329.Caption = "Min Days From Last Visit";
            this.gridColumn329.ColumnEdit = this.repositoryItemTextEdit11;
            this.gridColumn329.FieldName = "MinimumDaysFromLastVisit";
            this.gridColumn329.Name = "gridColumn329";
            this.gridColumn329.OptionsColumn.AllowEdit = false;
            this.gridColumn329.OptionsColumn.AllowFocus = false;
            this.gridColumn329.OptionsColumn.ReadOnly = true;
            this.gridColumn329.Visible = true;
            this.gridColumn329.VisibleIndex = 6;
            this.gridColumn329.Width = 136;
            // 
            // repositoryItemTextEdit11
            // 
            this.repositoryItemTextEdit11.AutoHeight = false;
            this.repositoryItemTextEdit11.Mask.EditMask = "n0";
            this.repositoryItemTextEdit11.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit11.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit11.Name = "repositoryItemTextEdit11";
            // 
            // gridColumn330
            // 
            this.gridColumn330.Caption = "Max Days From Last Visit";
            this.gridColumn330.ColumnEdit = this.repositoryItemTextEdit11;
            this.gridColumn330.FieldName = "MaximumDaysFromLastVisit";
            this.gridColumn330.Name = "gridColumn330";
            this.gridColumn330.OptionsColumn.AllowEdit = false;
            this.gridColumn330.OptionsColumn.AllowFocus = false;
            this.gridColumn330.OptionsColumn.ReadOnly = true;
            this.gridColumn330.Visible = true;
            this.gridColumn330.VisibleIndex = 7;
            this.gridColumn330.Width = 140;
            // 
            // gridColumn331
            // 
            this.gridColumn331.Caption = "Days Leeway";
            this.gridColumn331.ColumnEdit = this.repositoryItemTextEdit11;
            this.gridColumn331.FieldName = "DaysLeeway";
            this.gridColumn331.Name = "gridColumn331";
            this.gridColumn331.OptionsColumn.AllowEdit = false;
            this.gridColumn331.OptionsColumn.AllowFocus = false;
            this.gridColumn331.OptionsColumn.ReadOnly = true;
            this.gridColumn331.Visible = true;
            this.gridColumn331.VisibleIndex = 8;
            this.gridColumn331.Width = 85;
            // 
            // gridColumn332
            // 
            this.gridColumn332.Caption = "Billing Centre Code";
            this.gridColumn332.FieldName = "BillingCentreCodeID";
            this.gridColumn332.Name = "gridColumn332";
            this.gridColumn332.OptionsColumn.AllowEdit = false;
            this.gridColumn332.OptionsColumn.AllowFocus = false;
            this.gridColumn332.OptionsColumn.ReadOnly = true;
            this.gridColumn332.Width = 111;
            // 
            // gridColumn333
            // 
            this.gridColumn333.Caption = "Site Contract ID";
            this.gridColumn333.FieldName = "SiteContractID";
            this.gridColumn333.Name = "gridColumn333";
            this.gridColumn333.OptionsColumn.AllowEdit = false;
            this.gridColumn333.OptionsColumn.AllowFocus = false;
            this.gridColumn333.OptionsColumn.ReadOnly = true;
            this.gridColumn333.Width = 98;
            // 
            // gridColumn334
            // 
            this.gridColumn334.Caption = "Client Contract ID";
            this.gridColumn334.FieldName = "ClientContractID";
            this.gridColumn334.Name = "gridColumn334";
            this.gridColumn334.OptionsColumn.AllowEdit = false;
            this.gridColumn334.OptionsColumn.AllowFocus = false;
            this.gridColumn334.OptionsColumn.ReadOnly = true;
            this.gridColumn334.Width = 107;
            // 
            // gridColumn335
            // 
            this.gridColumn335.Caption = "Site ID";
            this.gridColumn335.FieldName = "SiteID";
            this.gridColumn335.Name = "gridColumn335";
            this.gridColumn335.OptionsColumn.AllowEdit = false;
            this.gridColumn335.OptionsColumn.AllowFocus = false;
            this.gridColumn335.OptionsColumn.ReadOnly = true;
            this.gridColumn335.Visible = true;
            this.gridColumn335.VisibleIndex = 1;
            this.gridColumn335.Width = 51;
            // 
            // gridColumn336
            // 
            this.gridColumn336.Caption = "Client ID";
            this.gridColumn336.FieldName = "ClientID";
            this.gridColumn336.Name = "gridColumn336";
            this.gridColumn336.OptionsColumn.AllowEdit = false;
            this.gridColumn336.OptionsColumn.AllowFocus = false;
            this.gridColumn336.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn337
            // 
            this.gridColumn337.Caption = "Contract Description";
            this.gridColumn337.FieldName = "ContractDescription";
            this.gridColumn337.Name = "gridColumn337";
            this.gridColumn337.OptionsColumn.AllowEdit = false;
            this.gridColumn337.OptionsColumn.AllowFocus = false;
            this.gridColumn337.OptionsColumn.ReadOnly = true;
            this.gridColumn337.Width = 119;
            // 
            // gridColumn338
            // 
            this.gridColumn338.Caption = "Client \\ Contract";
            this.gridColumn338.FieldName = "ClientNameContractDescription";
            this.gridColumn338.Name = "gridColumn338";
            this.gridColumn338.OptionsColumn.AllowEdit = false;
            this.gridColumn338.OptionsColumn.AllowFocus = false;
            this.gridColumn338.OptionsColumn.ReadOnly = true;
            this.gridColumn338.Visible = true;
            this.gridColumn338.VisibleIndex = 13;
            this.gridColumn338.Width = 416;
            // 
            // gridColumn339
            // 
            this.gridColumn339.Caption = "Client Name";
            this.gridColumn339.FieldName = "ClientName";
            this.gridColumn339.Name = "gridColumn339";
            this.gridColumn339.OptionsColumn.AllowEdit = false;
            this.gridColumn339.OptionsColumn.AllowFocus = false;
            this.gridColumn339.OptionsColumn.ReadOnly = true;
            this.gridColumn339.Width = 199;
            // 
            // gridColumn340
            // 
            this.gridColumn340.Caption = "Site Name";
            this.gridColumn340.FieldName = "SiteName";
            this.gridColumn340.Name = "gridColumn340";
            this.gridColumn340.OptionsColumn.AllowEdit = false;
            this.gridColumn340.OptionsColumn.AllowFocus = false;
            this.gridColumn340.OptionsColumn.ReadOnly = true;
            this.gridColumn340.Visible = true;
            this.gridColumn340.VisibleIndex = 14;
            this.gridColumn340.Width = 276;
            // 
            // gridColumn341
            // 
            this.gridColumn341.Caption = "Site Postcode";
            this.gridColumn341.FieldName = "SitePostcode";
            this.gridColumn341.Name = "gridColumn341";
            this.gridColumn341.OptionsColumn.AllowEdit = false;
            this.gridColumn341.OptionsColumn.AllowFocus = false;
            this.gridColumn341.OptionsColumn.ReadOnly = true;
            this.gridColumn341.Width = 86;
            // 
            // gridColumn342
            // 
            this.gridColumn342.Caption = "Site Latitude";
            this.gridColumn342.FieldName = "LocationX";
            this.gridColumn342.Name = "gridColumn342";
            this.gridColumn342.OptionsColumn.AllowEdit = false;
            this.gridColumn342.OptionsColumn.AllowFocus = false;
            this.gridColumn342.OptionsColumn.ReadOnly = true;
            this.gridColumn342.Width = 81;
            // 
            // gridColumn343
            // 
            this.gridColumn343.Caption = "Site Longitude";
            this.gridColumn343.FieldName = "LocationY";
            this.gridColumn343.Name = "gridColumn343";
            this.gridColumn343.OptionsColumn.AllowEdit = false;
            this.gridColumn343.OptionsColumn.AllowFocus = false;
            this.gridColumn343.OptionsColumn.ReadOnly = true;
            this.gridColumn343.Width = 89;
            // 
            // gridColumn344
            // 
            this.gridColumn344.Caption = "Created By";
            this.gridColumn344.FieldName = "CreatedByStaffName";
            this.gridColumn344.Name = "gridColumn344";
            this.gridColumn344.OptionsColumn.AllowEdit = false;
            this.gridColumn344.OptionsColumn.AllowFocus = false;
            this.gridColumn344.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn345
            // 
            this.gridColumn345.Caption = "Job Sub-Type";
            this.gridColumn345.FieldName = "JobSubTypeDescription";
            this.gridColumn345.Name = "gridColumn345";
            this.gridColumn345.OptionsColumn.AllowEdit = false;
            this.gridColumn345.OptionsColumn.AllowFocus = false;
            this.gridColumn345.OptionsColumn.ReadOnly = true;
            this.gridColumn345.Width = 87;
            // 
            // gridColumn346
            // 
            this.gridColumn346.Caption = "Job Type";
            this.gridColumn346.FieldName = "JobTypeDescription";
            this.gridColumn346.Name = "gridColumn346";
            this.gridColumn346.OptionsColumn.AllowEdit = false;
            this.gridColumn346.OptionsColumn.AllowFocus = false;
            this.gridColumn346.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn347
            // 
            this.gridColumn347.Caption = "Job Type \\ Sub-Type";
            this.gridColumn347.FieldName = "JobTypeJobSubTypeDescription";
            this.gridColumn347.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn347.Name = "gridColumn347";
            this.gridColumn347.OptionsColumn.AllowEdit = false;
            this.gridColumn347.OptionsColumn.AllowFocus = false;
            this.gridColumn347.OptionsColumn.ReadOnly = true;
            this.gridColumn347.Visible = true;
            this.gridColumn347.VisibleIndex = 0;
            this.gridColumn347.Width = 290;
            // 
            // gridColumn348
            // 
            this.gridColumn348.Caption = "Job Type ID";
            this.gridColumn348.FieldName = "JobTypeID";
            this.gridColumn348.Name = "gridColumn348";
            this.gridColumn348.OptionsColumn.AllowEdit = false;
            this.gridColumn348.OptionsColumn.AllowFocus = false;
            this.gridColumn348.OptionsColumn.ReadOnly = true;
            this.gridColumn348.Width = 79;
            // 
            // gridColumn349
            // 
            this.gridColumn349.Caption = "Visit #";
            this.gridColumn349.FieldName = "VisitNumber";
            this.gridColumn349.Name = "gridColumn349";
            this.gridColumn349.OptionsColumn.AllowEdit = false;
            this.gridColumn349.OptionsColumn.AllowFocus = false;
            this.gridColumn349.OptionsColumn.ReadOnly = true;
            this.gridColumn349.Visible = true;
            this.gridColumn349.VisibleIndex = 11;
            // 
            // gridColumn350
            // 
            this.gridColumn350.Caption = "Labour Count";
            this.gridColumn350.ColumnEdit = this.repositoryItemTextEdit11;
            this.gridColumn350.FieldName = "LabourCount";
            this.gridColumn350.Name = "gridColumn350";
            this.gridColumn350.OptionsColumn.AllowEdit = false;
            this.gridColumn350.OptionsColumn.AllowFocus = false;
            this.gridColumn350.OptionsColumn.ReadOnly = true;
            this.gridColumn350.Width = 86;
            // 
            // gridColumn351
            // 
            this.gridColumn351.Caption = "Equipment Count";
            this.gridColumn351.ColumnEdit = this.repositoryItemTextEdit11;
            this.gridColumn351.FieldName = "EquipmentCount";
            this.gridColumn351.Name = "gridColumn351";
            this.gridColumn351.OptionsColumn.AllowEdit = false;
            this.gridColumn351.OptionsColumn.AllowFocus = false;
            this.gridColumn351.OptionsColumn.ReadOnly = true;
            this.gridColumn351.Width = 103;
            // 
            // gridColumn352
            // 
            this.gridColumn352.Caption = "Material Count";
            this.gridColumn352.ColumnEdit = this.repositoryItemTextEdit11;
            this.gridColumn352.FieldName = "MaterialCount";
            this.gridColumn352.Name = "gridColumn352";
            this.gridColumn352.OptionsColumn.AllowEdit = false;
            this.gridColumn352.OptionsColumn.AllowFocus = false;
            this.gridColumn352.OptionsColumn.ReadOnly = true;
            this.gridColumn352.Visible = true;
            this.gridColumn352.VisibleIndex = 11;
            this.gridColumn352.Width = 91;
            // 
            // standaloneBarDockControl5
            // 
            this.standaloneBarDockControl5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl5.CausesValidation = false;
            this.standaloneBarDockControl5.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl5.Manager = this.barManager1;
            this.standaloneBarDockControl5.Name = "standaloneBarDockControl5";
            this.standaloneBarDockControl5.Size = new System.Drawing.Size(724, 26);
            this.standaloneBarDockControl5.Text = "standaloneBarDockControl5";
            // 
            // gridControl12
            // 
            this.gridControl12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl12.DataSource = this.sp06191OMJobMaterialEditBindingSource;
            this.gridControl12.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl12.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl12.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl12.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl12.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl12.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl12.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl12.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl12.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl12.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl12.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl12.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 10, true, true, "Block Add New Records", "block add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Block Edit Selected Records", "block edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl12.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl12_EmbeddedNavigator_ButtonClick);
            this.gridControl12.Location = new System.Drawing.Point(0, 26);
            this.gridControl12.MainView = this.gridView12;
            this.gridControl12.MenuManager = this.barManager1;
            this.gridControl12.Name = "gridControl12";
            this.gridControl12.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEditMaterialName,
            this.repositoryItemSpinEdit2DP12,
            this.repositoryItemGridLookUpEdit3,
            this.repositoryItemSpinEditCurrency12,
            this.repositoryItemSpinEditPercentage12,
            this.repositoryItemMemoExEdit13,
            this.repositoryItemTextEdit14,
            this.repositoryItemTextEdit15});
            this.gridControl12.Size = new System.Drawing.Size(724, 407);
            this.gridControl12.TabIndex = 21;
            this.gridControl12.UseEmbeddedNavigator = true;
            this.gridControl12.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView12});
            // 
            // sp06191OMJobMaterialEditBindingSource
            // 
            this.sp06191OMJobMaterialEditBindingSource.DataMember = "sp06191_OM_Job_Material_Edit";
            this.sp06191OMJobMaterialEditBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridView12
            // 
            this.gridView12.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn353,
            this.gridColumn354,
            this.gridColumn355,
            this.colMaterialUsedID,
            this.colMaterialID,
            this.gridColumn358,
            this.colCostUnitsUsed3,
            this.gridColumn360,
            this.gridColumn364,
            this.gridColumn365,
            this.gridColumn366,
            this.gridColumn367,
            this.gridColumn368,
            this.gridColumn369,
            this.gridColumn370,
            this.gridColumn371,
            this.gridColumn372,
            this.gridColumn373,
            this.gridColumn374,
            this.gridColumn375,
            this.gridColumn376,
            this.gridColumn377,
            this.gridColumn378,
            this.gridColumn379,
            this.gridColumn380,
            this.gridColumn381,
            this.gridColumn382,
            this.gridColumn383,
            this.gridColumn384,
            this.gridColumn385,
            this.colMaterialName,
            this.gridColumn387,
            this.gridColumn388,
            this.gridColumn390,
            this.gridColumn391,
            this.gridColumn392,
            this.gridColumn393,
            this.gridColumn394,
            this.gridColumn395,
            this.gridColumn396,
            this.gridColumn397,
            this.gridColumn399,
            this.gridColumn400});
            this.gridView12.GridControl = this.gridControl12;
            this.gridView12.GroupCount = 4;
            this.gridView12.Name = "gridView12";
            this.gridView12.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView12.OptionsFilter.AllowFilterEditor = false;
            this.gridView12.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView12.OptionsFilter.AllowMRUFilterList = false;
            this.gridView12.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView12.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView12.OptionsFind.FindDelay = 2000;
            this.gridView12.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView12.OptionsLayout.StoreAppearance = true;
            this.gridView12.OptionsLayout.StoreFormatRules = true;
            this.gridView12.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView12.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView12.OptionsNavigation.AutoFocusNewRow = true;
            this.gridView12.OptionsSelection.MultiSelect = true;
            this.gridView12.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView12.OptionsView.ColumnAutoWidth = false;
            this.gridView12.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView12.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView12.OptionsView.ShowFooter = true;
            this.gridView12.OptionsView.ShowGroupPanel = false;
            this.gridView12.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn394, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn382, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn383, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn395, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView12.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView12.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView12.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView12.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView12.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView12.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView12_MouseUp);
            this.gridView12.GotFocus += new System.EventHandler(this.gridView12_GotFocus);
            // 
            // gridColumn353
            // 
            this.gridColumn353.FieldName = "strMode";
            this.gridColumn353.Name = "gridColumn353";
            this.gridColumn353.OptionsColumn.AllowEdit = false;
            this.gridColumn353.OptionsColumn.AllowFocus = false;
            this.gridColumn353.OptionsColumn.ReadOnly = true;
            this.gridColumn353.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn353.OptionsColumn.ShowInExpressionEditor = false;
            // 
            // gridColumn354
            // 
            this.gridColumn354.FieldName = "strRecordIDs";
            this.gridColumn354.Name = "gridColumn354";
            this.gridColumn354.OptionsColumn.AllowEdit = false;
            this.gridColumn354.OptionsColumn.AllowFocus = false;
            this.gridColumn354.OptionsColumn.ReadOnly = true;
            this.gridColumn354.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn354.OptionsColumn.ShowInExpressionEditor = false;
            this.gridColumn354.Width = 90;
            // 
            // gridColumn355
            // 
            this.gridColumn355.Caption = "Dummy Job ID";
            this.gridColumn355.FieldName = "DummyJobID";
            this.gridColumn355.Name = "gridColumn355";
            this.gridColumn355.OptionsColumn.AllowEdit = false;
            this.gridColumn355.OptionsColumn.AllowFocus = false;
            this.gridColumn355.OptionsColumn.ReadOnly = true;
            this.gridColumn355.Width = 90;
            // 
            // colMaterialUsedID
            // 
            this.colMaterialUsedID.Caption = "Material Used ID";
            this.colMaterialUsedID.FieldName = "MaterialUsedID";
            this.colMaterialUsedID.Name = "colMaterialUsedID";
            this.colMaterialUsedID.OptionsColumn.AllowEdit = false;
            this.colMaterialUsedID.OptionsColumn.AllowFocus = false;
            this.colMaterialUsedID.OptionsColumn.ReadOnly = true;
            this.colMaterialUsedID.Width = 98;
            // 
            // colMaterialID
            // 
            this.colMaterialID.Caption = "Material ID";
            this.colMaterialID.FieldName = "MaterialID";
            this.colMaterialID.Name = "colMaterialID";
            this.colMaterialID.OptionsColumn.AllowEdit = false;
            this.colMaterialID.OptionsColumn.AllowFocus = false;
            this.colMaterialID.OptionsColumn.ReadOnly = true;
            this.colMaterialID.Width = 84;
            // 
            // gridColumn358
            // 
            this.gridColumn358.Caption = "Job ID";
            this.gridColumn358.FieldName = "JobID";
            this.gridColumn358.Name = "gridColumn358";
            this.gridColumn358.OptionsColumn.AllowEdit = false;
            this.gridColumn358.OptionsColumn.AllowFocus = false;
            this.gridColumn358.OptionsColumn.ReadOnly = true;
            // 
            // colCostUnitsUsed3
            // 
            this.colCostUnitsUsed3.Caption = "Cost Units Used";
            this.colCostUnitsUsed3.ColumnEdit = this.repositoryItemSpinEdit2DP12;
            this.colCostUnitsUsed3.FieldName = "CostUnitsUsed";
            this.colCostUnitsUsed3.Name = "colCostUnitsUsed3";
            this.colCostUnitsUsed3.Visible = true;
            this.colCostUnitsUsed3.VisibleIndex = 1;
            this.colCostUnitsUsed3.Width = 97;
            // 
            // repositoryItemSpinEdit2DP12
            // 
            this.repositoryItemSpinEdit2DP12.AutoHeight = false;
            this.repositoryItemSpinEdit2DP12.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit2DP12.Mask.EditMask = "n2";
            this.repositoryItemSpinEdit2DP12.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEdit2DP12.Name = "repositoryItemSpinEdit2DP12";
            this.repositoryItemSpinEdit2DP12.EditValueChanged += new System.EventHandler(this.repositoryItemSpinEdit2DP12_EditValueChanged);
            // 
            // gridColumn360
            // 
            this.gridColumn360.Caption = "Cost Unit Descriptor";
            this.gridColumn360.ColumnEdit = this.repositoryItemGridLookUpEdit3;
            this.gridColumn360.FieldName = "CostUnitDescriptorID";
            this.gridColumn360.Name = "gridColumn360";
            this.gridColumn360.Visible = true;
            this.gridColumn360.VisibleIndex = 2;
            this.gridColumn360.Width = 117;
            // 
            // repositoryItemGridLookUpEdit3
            // 
            this.repositoryItemGridLookUpEdit3.AutoHeight = false;
            this.repositoryItemGridLookUpEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit3.DataSource = this.sp06108OMUnitDescriptorsPicklistBindingSource;
            this.repositoryItemGridLookUpEdit3.DisplayMember = "Description";
            this.repositoryItemGridLookUpEdit3.Name = "repositoryItemGridLookUpEdit3";
            this.repositoryItemGridLookUpEdit3.NullText = "";
            this.repositoryItemGridLookUpEdit3.PopupView = this.gridView14;
            this.repositoryItemGridLookUpEdit3.ValueMember = "ID";
            // 
            // sp06108OMUnitDescriptorsPicklistBindingSource
            // 
            this.sp06108OMUnitDescriptorsPicklistBindingSource.DataMember = "sp06108_OM_Unit_Descriptors_Picklist";
            this.sp06108OMUnitDescriptorsPicklistBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // gridView14
            // 
            this.gridView14.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn361,
            this.gridColumn362,
            this.gridColumn363});
            this.gridView14.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition3.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition3.Appearance.Options.UseForeColor = true;
            styleFormatCondition3.ApplyToRow = true;
            styleFormatCondition3.Column = this.gridColumn361;
            styleFormatCondition3.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition3.Value1 = 0;
            this.gridView14.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition3});
            this.gridView14.Name = "gridView14";
            this.gridView14.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView14.OptionsLayout.StoreAppearance = true;
            this.gridView14.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView14.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView14.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView14.OptionsView.ColumnAutoWidth = false;
            this.gridView14.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView14.OptionsView.ShowGroupPanel = false;
            this.gridView14.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView14.OptionsView.ShowIndicator = false;
            this.gridView14.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn363, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn362
            // 
            this.gridColumn362.Caption = "Unit Descriptor";
            this.gridColumn362.FieldName = "Description";
            this.gridColumn362.Name = "gridColumn362";
            this.gridColumn362.OptionsColumn.AllowEdit = false;
            this.gridColumn362.OptionsColumn.AllowFocus = false;
            this.gridColumn362.OptionsColumn.ReadOnly = true;
            this.gridColumn362.Visible = true;
            this.gridColumn362.VisibleIndex = 0;
            this.gridColumn362.Width = 220;
            // 
            // gridColumn363
            // 
            this.gridColumn363.Caption = "Order";
            this.gridColumn363.FieldName = "RecordOrder";
            this.gridColumn363.Name = "gridColumn363";
            this.gridColumn363.OptionsColumn.AllowEdit = false;
            this.gridColumn363.OptionsColumn.AllowFocus = false;
            this.gridColumn363.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn364
            // 
            this.gridColumn364.Caption = "Sell Units Used";
            this.gridColumn364.ColumnEdit = this.repositoryItemSpinEdit2DP12;
            this.gridColumn364.FieldName = "SellUnitsUsed";
            this.gridColumn364.Name = "gridColumn364";
            this.gridColumn364.Visible = true;
            this.gridColumn364.VisibleIndex = 8;
            this.gridColumn364.Width = 91;
            // 
            // gridColumn365
            // 
            this.gridColumn365.Caption = "Sell Unit Descriptor";
            this.gridColumn365.ColumnEdit = this.repositoryItemGridLookUpEdit3;
            this.gridColumn365.FieldName = "SellUnitDescriptorID";
            this.gridColumn365.Name = "gridColumn365";
            this.gridColumn365.Visible = true;
            this.gridColumn365.VisibleIndex = 9;
            this.gridColumn365.Width = 111;
            // 
            // gridColumn366
            // 
            this.gridColumn366.Caption = "Unit Cost Ex VAT";
            this.gridColumn366.ColumnEdit = this.repositoryItemSpinEditCurrency12;
            this.gridColumn366.FieldName = "CostPerUnitExVat";
            this.gridColumn366.Name = "gridColumn366";
            this.gridColumn366.Visible = true;
            this.gridColumn366.VisibleIndex = 3;
            this.gridColumn366.Width = 102;
            // 
            // repositoryItemSpinEditCurrency12
            // 
            this.repositoryItemSpinEditCurrency12.AutoHeight = false;
            this.repositoryItemSpinEditCurrency12.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEditCurrency12.Mask.EditMask = "c";
            this.repositoryItemSpinEditCurrency12.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditCurrency12.Name = "repositoryItemSpinEditCurrency12";
            this.repositoryItemSpinEditCurrency12.EditValueChanged += new System.EventHandler(this.repositoryItemSpinEditCurrency12_EditValueChanged);
            // 
            // gridColumn367
            // 
            this.gridColumn367.Caption = "Unit Cost VAT Rate";
            this.gridColumn367.ColumnEdit = this.repositoryItemSpinEditPercentage12;
            this.gridColumn367.FieldName = "CostPerUnitVatRate";
            this.gridColumn367.Name = "gridColumn367";
            this.gridColumn367.Visible = true;
            this.gridColumn367.VisibleIndex = 4;
            this.gridColumn367.Width = 113;
            // 
            // repositoryItemSpinEditPercentage12
            // 
            this.repositoryItemSpinEditPercentage12.AutoHeight = false;
            this.repositoryItemSpinEditPercentage12.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEditPercentage12.Mask.EditMask = "P";
            this.repositoryItemSpinEditPercentage12.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditPercentage12.Name = "repositoryItemSpinEditPercentage12";
            this.repositoryItemSpinEditPercentage12.EditValueChanged += new System.EventHandler(this.repositoryItemSpinEditPercentage12_EditValueChanged);
            // 
            // gridColumn368
            // 
            this.gridColumn368.Caption = "Unit Sell Ex VAT";
            this.gridColumn368.ColumnEdit = this.repositoryItemSpinEditCurrency12;
            this.gridColumn368.FieldName = "SellPerUnitExVat";
            this.gridColumn368.Name = "gridColumn368";
            this.gridColumn368.Visible = true;
            this.gridColumn368.VisibleIndex = 10;
            this.gridColumn368.Width = 96;
            // 
            // gridColumn369
            // 
            this.gridColumn369.Caption = "Unit Sell VAT Rate";
            this.gridColumn369.ColumnEdit = this.repositoryItemSpinEditPercentage12;
            this.gridColumn369.FieldName = "SellPerUnitVatRate";
            this.gridColumn369.Name = "gridColumn369";
            this.gridColumn369.Visible = true;
            this.gridColumn369.VisibleIndex = 11;
            this.gridColumn369.Width = 107;
            // 
            // gridColumn370
            // 
            this.gridColumn370.Caption = "Total Cost Ex VAT";
            this.gridColumn370.ColumnEdit = this.repositoryItemSpinEditCurrency12;
            this.gridColumn370.FieldName = "CostValueExVat";
            this.gridColumn370.Name = "gridColumn370";
            this.gridColumn370.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostValueExVat", "{0:c}")});
            this.gridColumn370.Visible = true;
            this.gridColumn370.VisibleIndex = 5;
            this.gridColumn370.Width = 107;
            // 
            // gridColumn371
            // 
            this.gridColumn371.Caption = "Total Cost VAT";
            this.gridColumn371.ColumnEdit = this.repositoryItemSpinEditCurrency12;
            this.gridColumn371.FieldName = "CostValueVat";
            this.gridColumn371.Name = "gridColumn371";
            this.gridColumn371.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostValueVat", "{0:c}")});
            this.gridColumn371.Visible = true;
            this.gridColumn371.VisibleIndex = 6;
            this.gridColumn371.Width = 92;
            // 
            // gridColumn372
            // 
            this.gridColumn372.Caption = "Total Cost";
            this.gridColumn372.ColumnEdit = this.repositoryItemSpinEditCurrency12;
            this.gridColumn372.FieldName = "CostValueTotal";
            this.gridColumn372.Name = "gridColumn372";
            this.gridColumn372.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostValueTotal", "{0:c}")});
            this.gridColumn372.Visible = true;
            this.gridColumn372.VisibleIndex = 7;
            // 
            // gridColumn373
            // 
            this.gridColumn373.Caption = "Total Sell Ex VAT";
            this.gridColumn373.ColumnEdit = this.repositoryItemSpinEditCurrency12;
            this.gridColumn373.FieldName = "SellValueExVat";
            this.gridColumn373.Name = "gridColumn373";
            this.gridColumn373.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellValueExVat", "{0:c}")});
            this.gridColumn373.Visible = true;
            this.gridColumn373.VisibleIndex = 12;
            this.gridColumn373.Width = 101;
            // 
            // gridColumn374
            // 
            this.gridColumn374.Caption = "Total Sell VAT";
            this.gridColumn374.ColumnEdit = this.repositoryItemSpinEditCurrency12;
            this.gridColumn374.FieldName = "SellValueVat";
            this.gridColumn374.Name = "gridColumn374";
            this.gridColumn374.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellValueVat", "{0:c}")});
            this.gridColumn374.Visible = true;
            this.gridColumn374.VisibleIndex = 13;
            this.gridColumn374.Width = 86;
            // 
            // gridColumn375
            // 
            this.gridColumn375.Caption = "Total Sell";
            this.gridColumn375.ColumnEdit = this.repositoryItemSpinEditCurrency12;
            this.gridColumn375.FieldName = "SellValueTotal";
            this.gridColumn375.Name = "gridColumn375";
            this.gridColumn375.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellValueTotal", "{0:c}")});
            this.gridColumn375.Visible = true;
            this.gridColumn375.VisibleIndex = 14;
            // 
            // gridColumn376
            // 
            this.gridColumn376.Caption = "Remarks";
            this.gridColumn376.ColumnEdit = this.repositoryItemMemoExEdit13;
            this.gridColumn376.FieldName = "Remarks";
            this.gridColumn376.Name = "gridColumn376";
            this.gridColumn376.Visible = true;
            this.gridColumn376.VisibleIndex = 15;
            // 
            // repositoryItemMemoExEdit13
            // 
            this.repositoryItemMemoExEdit13.AutoHeight = false;
            this.repositoryItemMemoExEdit13.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit13.Name = "repositoryItemMemoExEdit13";
            this.repositoryItemMemoExEdit13.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit13.ShowIcon = false;
            // 
            // gridColumn377
            // 
            this.gridColumn377.Caption = "Client ID";
            this.gridColumn377.FieldName = "ClientID";
            this.gridColumn377.Name = "gridColumn377";
            this.gridColumn377.OptionsColumn.AllowEdit = false;
            this.gridColumn377.OptionsColumn.AllowFocus = false;
            this.gridColumn377.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn378
            // 
            this.gridColumn378.Caption = "Site ID";
            this.gridColumn378.FieldName = "SiteID";
            this.gridColumn378.Name = "gridColumn378";
            this.gridColumn378.OptionsColumn.AllowEdit = false;
            this.gridColumn378.OptionsColumn.AllowFocus = false;
            this.gridColumn378.OptionsColumn.ReadOnly = true;
            this.gridColumn378.Visible = true;
            this.gridColumn378.VisibleIndex = 16;
            this.gridColumn378.Width = 51;
            // 
            // gridColumn379
            // 
            this.gridColumn379.Caption = "Visit ID";
            this.gridColumn379.FieldName = "VisitID";
            this.gridColumn379.Name = "gridColumn379";
            this.gridColumn379.OptionsColumn.AllowEdit = false;
            this.gridColumn379.OptionsColumn.AllowFocus = false;
            this.gridColumn379.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn380
            // 
            this.gridColumn380.Caption = "Expected Start Date";
            this.gridColumn380.FieldName = "JobExpectedStartDate";
            this.gridColumn380.Name = "gridColumn380";
            this.gridColumn380.OptionsColumn.AllowEdit = false;
            this.gridColumn380.OptionsColumn.AllowFocus = false;
            this.gridColumn380.OptionsColumn.ReadOnly = true;
            this.gridColumn380.Visible = true;
            this.gridColumn380.VisibleIndex = 17;
            this.gridColumn380.Width = 119;
            // 
            // gridColumn381
            // 
            this.gridColumn381.Caption = "Client Name";
            this.gridColumn381.FieldName = "ClientName";
            this.gridColumn381.Name = "gridColumn381";
            this.gridColumn381.OptionsColumn.AllowEdit = false;
            this.gridColumn381.OptionsColumn.AllowFocus = false;
            this.gridColumn381.OptionsColumn.ReadOnly = true;
            this.gridColumn381.Width = 304;
            // 
            // gridColumn382
            // 
            this.gridColumn382.Caption = "Site Name";
            this.gridColumn382.FieldName = "SiteName";
            this.gridColumn382.Name = "gridColumn382";
            this.gridColumn382.OptionsColumn.AllowEdit = false;
            this.gridColumn382.OptionsColumn.AllowFocus = false;
            this.gridColumn382.OptionsColumn.ReadOnly = true;
            this.gridColumn382.Visible = true;
            this.gridColumn382.VisibleIndex = 19;
            this.gridColumn382.Width = 275;
            // 
            // gridColumn383
            // 
            this.gridColumn383.Caption = "Visit #";
            this.gridColumn383.FieldName = "VisitNumber";
            this.gridColumn383.Name = "gridColumn383";
            this.gridColumn383.OptionsColumn.AllowEdit = false;
            this.gridColumn383.OptionsColumn.AllowFocus = false;
            this.gridColumn383.OptionsColumn.ReadOnly = true;
            this.gridColumn383.Visible = true;
            this.gridColumn383.VisibleIndex = 18;
            this.gridColumn383.Width = 63;
            // 
            // gridColumn384
            // 
            this.gridColumn384.Caption = "Job Type";
            this.gridColumn384.FieldName = "JobTypeDescription";
            this.gridColumn384.Name = "gridColumn384";
            this.gridColumn384.OptionsColumn.AllowEdit = false;
            this.gridColumn384.OptionsColumn.AllowFocus = false;
            this.gridColumn384.OptionsColumn.ReadOnly = true;
            this.gridColumn384.Width = 192;
            // 
            // gridColumn385
            // 
            this.gridColumn385.Caption = "Job Sub-Type";
            this.gridColumn385.FieldName = "JobSubTypeDescription";
            this.gridColumn385.Name = "gridColumn385";
            this.gridColumn385.OptionsColumn.AllowEdit = false;
            this.gridColumn385.OptionsColumn.AllowFocus = false;
            this.gridColumn385.OptionsColumn.ReadOnly = true;
            this.gridColumn385.Width = 190;
            // 
            // colMaterialName
            // 
            this.colMaterialName.Caption = "Material Name";
            this.colMaterialName.ColumnEdit = this.repositoryItemButtonEditMaterialName;
            this.colMaterialName.FieldName = "MaterialName";
            this.colMaterialName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colMaterialName.Name = "colMaterialName";
            this.colMaterialName.Visible = true;
            this.colMaterialName.VisibleIndex = 0;
            this.colMaterialName.Width = 211;
            // 
            // repositoryItemButtonEditMaterialName
            // 
            this.repositoryItemButtonEditMaterialName.AutoHeight = false;
            this.repositoryItemButtonEditMaterialName.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions9, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject33, serializableAppearanceObject34, serializableAppearanceObject35, serializableAppearanceObject36, "Click me to open the Select Materials screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditMaterialName.Name = "repositoryItemButtonEditMaterialName";
            this.repositoryItemButtonEditMaterialName.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemButtonEditMaterialName.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditMaterialName_ButtonClick);
            // 
            // gridColumn387
            // 
            this.gridColumn387.Caption = "Cost Unit Description";
            this.gridColumn387.FieldName = "CostUnitDescriptor";
            this.gridColumn387.Name = "gridColumn387";
            this.gridColumn387.OptionsColumn.AllowEdit = false;
            this.gridColumn387.OptionsColumn.AllowFocus = false;
            this.gridColumn387.OptionsColumn.ReadOnly = true;
            this.gridColumn387.Width = 121;
            // 
            // gridColumn388
            // 
            this.gridColumn388.Caption = "Sell Unit Description";
            this.gridColumn388.FieldName = "SellUnitDescriptor";
            this.gridColumn388.Name = "gridColumn388";
            this.gridColumn388.OptionsColumn.AllowEdit = false;
            this.gridColumn388.OptionsColumn.AllowFocus = false;
            this.gridColumn388.OptionsColumn.ReadOnly = true;
            this.gridColumn388.Width = 115;
            // 
            // gridColumn390
            // 
            this.gridColumn390.Caption = "PO ID";
            this.gridColumn390.FieldName = "PurchaseOrderID";
            this.gridColumn390.Name = "gridColumn390";
            this.gridColumn390.OptionsColumn.AllowEdit = false;
            this.gridColumn390.OptionsColumn.AllowFocus = false;
            this.gridColumn390.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn391
            // 
            this.gridColumn391.Caption = "Start Date";
            this.gridColumn391.ColumnEdit = this.repositoryItemTextEdit14;
            this.gridColumn391.FieldName = "StartDateTime";
            this.gridColumn391.Name = "gridColumn391";
            this.gridColumn391.OptionsColumn.AllowEdit = false;
            this.gridColumn391.OptionsColumn.AllowFocus = false;
            this.gridColumn391.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemTextEdit14
            // 
            this.repositoryItemTextEdit14.AutoHeight = false;
            this.repositoryItemTextEdit14.Mask.EditMask = "g";
            this.repositoryItemTextEdit14.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit14.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit14.Name = "repositoryItemTextEdit14";
            // 
            // gridColumn392
            // 
            this.gridColumn392.Caption = "End Date";
            this.gridColumn392.ColumnEdit = this.repositoryItemTextEdit14;
            this.gridColumn392.FieldName = "EndDateTime";
            this.gridColumn392.Name = "gridColumn392";
            this.gridColumn392.OptionsColumn.AllowEdit = false;
            this.gridColumn392.OptionsColumn.AllowFocus = false;
            this.gridColumn392.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn393
            // 
            this.gridColumn393.Caption = "Contract Description";
            this.gridColumn393.FieldName = "ContractDescription";
            this.gridColumn393.Name = "gridColumn393";
            this.gridColumn393.OptionsColumn.AllowEdit = false;
            this.gridColumn393.OptionsColumn.AllowFocus = false;
            this.gridColumn393.OptionsColumn.ReadOnly = true;
            this.gridColumn393.Width = 119;
            // 
            // gridColumn394
            // 
            this.gridColumn394.Caption = "Client \\ Contract Description";
            this.gridColumn394.FieldName = "ClientNameContractDescription";
            this.gridColumn394.Name = "gridColumn394";
            this.gridColumn394.OptionsColumn.AllowEdit = false;
            this.gridColumn394.OptionsColumn.AllowFocus = false;
            this.gridColumn394.OptionsColumn.ReadOnly = true;
            this.gridColumn394.Visible = true;
            this.gridColumn394.VisibleIndex = 20;
            this.gridColumn394.Width = 361;
            // 
            // gridColumn395
            // 
            this.gridColumn395.Caption = "Job Type \\ Job Sub-Type";
            this.gridColumn395.FieldName = "JobTypeJobSubTypeDescription";
            this.gridColumn395.Name = "gridColumn395";
            this.gridColumn395.OptionsColumn.AllowEdit = false;
            this.gridColumn395.OptionsColumn.AllowFocus = false;
            this.gridColumn395.OptionsColumn.ReadOnly = true;
            this.gridColumn395.Visible = true;
            this.gridColumn395.VisibleIndex = 21;
            this.gridColumn395.Width = 396;
            // 
            // gridColumn396
            // 
            this.gridColumn396.AppearanceCell.ForeColor = System.Drawing.Color.Gray;
            this.gridColumn396.AppearanceCell.Options.UseForeColor = true;
            this.gridColumn396.Caption = "Distance From Site Lat\\Long";
            this.gridColumn396.ColumnEdit = this.repositoryItemTextEdit15;
            this.gridColumn396.FieldName = "LatLongSiteDistance";
            this.gridColumn396.Name = "gridColumn396";
            this.gridColumn396.OptionsColumn.AllowEdit = false;
            this.gridColumn396.OptionsColumn.AllowFocus = false;
            this.gridColumn396.OptionsColumn.ReadOnly = true;
            this.gridColumn396.Width = 155;
            // 
            // repositoryItemTextEdit15
            // 
            this.repositoryItemTextEdit15.AutoHeight = false;
            this.repositoryItemTextEdit15.Mask.EditMask = "########0.00 Miles";
            this.repositoryItemTextEdit15.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit15.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit15.Name = "repositoryItemTextEdit15";
            // 
            // gridColumn397
            // 
            this.gridColumn397.AppearanceCell.ForeColor = System.Drawing.Color.Gray;
            this.gridColumn397.AppearanceCell.Options.UseForeColor = true;
            this.gridColumn397.Caption = "Distance From Site Postcode";
            this.gridColumn397.ColumnEdit = this.repositoryItemTextEdit15;
            this.gridColumn397.FieldName = "PostcodeSiteDistance";
            this.gridColumn397.Name = "gridColumn397";
            this.gridColumn397.OptionsColumn.AllowEdit = false;
            this.gridColumn397.OptionsColumn.AllowFocus = false;
            this.gridColumn397.OptionsColumn.ReadOnly = true;
            this.gridColumn397.Width = 157;
            // 
            // gridColumn399
            // 
            this.gridColumn399.Caption = "Job Sub-Type ID";
            this.gridColumn399.FieldName = "JobSubTypeID";
            this.gridColumn399.Name = "gridColumn399";
            this.gridColumn399.OptionsColumn.AllowEdit = false;
            this.gridColumn399.OptionsColumn.AllowFocus = false;
            this.gridColumn399.OptionsColumn.ReadOnly = true;
            this.gridColumn399.Width = 101;
            // 
            // gridColumn400
            // 
            this.gridColumn400.Caption = "Job Type ID";
            this.gridColumn400.FieldName = "JobTypeID";
            this.gridColumn400.Name = "gridColumn400";
            this.gridColumn400.OptionsColumn.AllowEdit = false;
            this.gridColumn400.OptionsColumn.AllowFocus = false;
            this.gridColumn400.OptionsColumn.ReadOnly = true;
            this.gridColumn400.Width = 81;
            // 
            // btnStep8Next
            // 
            this.btnStep8Next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep8Next.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.btnStep8Next.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnStep8Next.Location = new System.Drawing.Point(1119, 501);
            this.btnStep8Next.Name = "btnStep8Next";
            this.btnStep8Next.Size = new System.Drawing.Size(88, 30);
            this.btnStep8Next.TabIndex = 21;
            this.btnStep8Next.Text = "Next";
            this.btnStep8Next.Click += new System.EventHandler(this.btnStep8Next_Click);
            // 
            // btnStep8Previous
            // 
            this.btnStep8Previous.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep8Previous.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnStep8Previous.Location = new System.Drawing.Point(1025, 501);
            this.btnStep8Previous.Name = "btnStep8Previous";
            this.btnStep8Previous.Size = new System.Drawing.Size(88, 30);
            this.btnStep8Previous.TabIndex = 22;
            this.btnStep8Previous.Text = "Previous";
            this.btnStep8Previous.Click += new System.EventHandler(this.btnStep8Previous_Click);
            // 
            // panelControl12
            // 
            this.panelControl12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl12.Controls.Add(this.pictureEdit12);
            this.panelControl12.Controls.Add(this.labelControl25);
            this.panelControl12.Controls.Add(this.labelControl26);
            this.panelControl12.Location = new System.Drawing.Point(7, 6);
            this.panelControl12.Name = "panelControl12";
            this.panelControl12.Size = new System.Drawing.Size(1200, 48);
            this.panelControl12.TabIndex = 20;
            // 
            // pictureEdit12
            // 
            this.pictureEdit12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit12.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit12.Location = new System.Drawing.Point(1156, 4);
            this.pictureEdit12.MenuManager = this.barManager1;
            this.pictureEdit12.Name = "pictureEdit12";
            this.pictureEdit12.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit12.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit12.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit12.Properties.ReadOnly = true;
            this.pictureEdit12.Properties.ShowMenu = false;
            this.pictureEdit12.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit12.TabIndex = 9;
            // 
            // labelControl25
            // 
            this.labelControl25.AllowHtmlString = true;
            this.labelControl25.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl25.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl25.Appearance.Options.UseBackColor = true;
            this.labelControl25.Appearance.Options.UseFont = true;
            this.labelControl25.Location = new System.Drawing.Point(5, 5);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(202, 16);
            this.labelControl25.TabIndex = 6;
            this.labelControl25.Text = "<b>Step 8:</b> Add Materials  <b>[Optional]</b>";
            // 
            // labelControl26
            // 
            this.labelControl26.AllowHtmlString = true;
            this.labelControl26.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl26.Appearance.Options.UseBackColor = true;
            this.labelControl26.Location = new System.Drawing.Point(57, 29);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(667, 13);
            this.labelControl26.TabIndex = 7;
            this.labelControl26.Text = "Select one or more Jobs by clicking on them then click the required Add Button to" +
    " add materials to the selected jobs. Click Next when Done.";
            // 
            // xtraTabPageFinish
            // 
            this.xtraTabPageFinish.Controls.Add(this.panelControl2);
            this.xtraTabPageFinish.Controls.Add(this.groupControl1);
            this.xtraTabPageFinish.Controls.Add(this.labelControl10);
            this.xtraTabPageFinish.Controls.Add(this.btnFinish);
            this.xtraTabPageFinish.Controls.Add(this.btnFinishPrevious);
            this.xtraTabPageFinish.Controls.Add(this.pictureEdit2);
            this.xtraTabPageFinish.Name = "xtraTabPageFinish";
            this.xtraTabPageFinish.Size = new System.Drawing.Size(1213, 537);
            this.xtraTabPageFinish.Tag = "99";
            this.xtraTabPageFinish.Text = "Finished";
            // 
            // panelControl2
            // 
            this.panelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl2.Controls.Add(this.pictureEdit6);
            this.panelControl2.Controls.Add(this.labelControl11);
            this.panelControl2.Controls.Add(this.labelControl12);
            this.panelControl2.Location = new System.Drawing.Point(211, 6);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(996, 48);
            this.panelControl2.TabIndex = 18;
            // 
            // pictureEdit6
            // 
            this.pictureEdit6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit6.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit6.Location = new System.Drawing.Point(952, 4);
            this.pictureEdit6.MenuManager = this.barManager1;
            this.pictureEdit6.Name = "pictureEdit6";
            this.pictureEdit6.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit6.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit6.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit6.Properties.ReadOnly = true;
            this.pictureEdit6.Properties.ShowMenu = false;
            this.pictureEdit6.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit6.TabIndex = 19;
            // 
            // labelControl11
            // 
            this.labelControl11.AllowHtmlString = true;
            this.labelControl11.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.labelControl11.Appearance.Options.UseBackColor = true;
            this.labelControl11.Appearance.Options.UseFont = true;
            this.labelControl11.Location = new System.Drawing.Point(5, 1);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(240, 29);
            this.labelControl11.TabIndex = 5;
            this.labelControl11.Text = "Completing the Wizard";
            // 
            // labelControl12
            // 
            this.labelControl12.AllowHtmlString = true;
            this.labelControl12.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl12.Appearance.Options.UseBackColor = true;
            this.labelControl12.Location = new System.Drawing.Point(9, 31);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(502, 13);
            this.labelControl12.TabIndex = 15;
            this.labelControl12.Text = "You have successfully completed the Wizard. On clicking <b>Finish</b>, one or mor" +
    "e job records will be created.";
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.checkEdit4);
            this.groupControl1.Controls.Add(this.checkEdit2);
            this.groupControl1.Controls.Add(this.checkEdit3);
            this.groupControl1.Controls.Add(this.checkEdit1);
            this.groupControl1.Location = new System.Drawing.Point(211, 155);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(996, 159);
            this.groupControl1.TabIndex = 16;
            this.groupControl1.Text = "Available Choices:";
            // 
            // checkEdit4
            // 
            this.checkEdit4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEdit4.Location = new System.Drawing.Point(6, 76);
            this.checkEdit4.MenuManager = this.barManager1;
            this.checkEdit4.Name = "checkEdit4";
            this.checkEdit4.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEdit4.Properties.Caption = "Create the Job(s) then <b>open</b> the <b>Job Wizard</b> to create further <b>Job" +
    "s</b> and <b>remember currently selected Visits</b>.";
            this.checkEdit4.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit4.Properties.RadioGroupIndex = 1;
            this.checkEdit4.Size = new System.Drawing.Size(984, 19);
            this.checkEdit4.TabIndex = 5;
            this.checkEdit4.TabStop = false;
            // 
            // checkEdit2
            // 
            this.checkEdit2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEdit2.Location = new System.Drawing.Point(6, 51);
            this.checkEdit2.MenuManager = this.barManager1;
            this.checkEdit2.Name = "checkEdit2";
            this.checkEdit2.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEdit2.Properties.Caption = "Create the Job(s) then <b>open</b> the <b>Job Wizard</b> to create further <b>Job" +
    "s</b>.";
            this.checkEdit2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit2.Properties.RadioGroupIndex = 1;
            this.checkEdit2.Size = new System.Drawing.Size(984, 19);
            this.checkEdit2.TabIndex = 4;
            this.checkEdit2.TabStop = false;
            // 
            // checkEdit3
            // 
            this.checkEdit3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEdit3.Location = new System.Drawing.Point(5, 101);
            this.checkEdit3.MenuManager = this.barManager1;
            this.checkEdit3.Name = "checkEdit3";
            this.checkEdit3.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEdit3.Properties.Caption = "Create the Job(s) then <b>open</b> the <b>Visit Wizard</b> to create further <b>V" +
    "isits</b>.";
            this.checkEdit3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit3.Properties.RadioGroupIndex = 1;
            this.checkEdit3.Size = new System.Drawing.Size(984, 19);
            this.checkEdit3.TabIndex = 2;
            this.checkEdit3.TabStop = false;
            // 
            // checkEdit1
            // 
            this.checkEdit1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEdit1.EditValue = true;
            this.checkEdit1.Location = new System.Drawing.Point(6, 26);
            this.checkEdit1.MenuManager = this.barManager1;
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEdit1.Properties.Caption = "Create the Job(s) then <b>return</b> to the parent<b> Manager</b>.";
            this.checkEdit1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit1.Properties.RadioGroupIndex = 1;
            this.checkEdit1.Size = new System.Drawing.Size(984, 19);
            this.checkEdit1.TabIndex = 0;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(213, 132);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(154, 13);
            this.labelControl10.TabIndex = 15;
            this.labelControl10.Text = "What would you like to do next?";
            // 
            // btnFinish
            // 
            this.btnFinish.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFinish.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnFinish.Appearance.Options.UseFont = true;
            this.btnFinish.ImageOptions.Image = global::WoodPlan5.Properties.Resources.apply_32x32;
            this.btnFinish.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnFinish.Location = new System.Drawing.Point(1119, 501);
            this.btnFinish.Name = "btnFinish";
            this.btnFinish.Size = new System.Drawing.Size(88, 30);
            this.btnFinish.TabIndex = 12;
            this.btnFinish.Text = "Finish";
            this.btnFinish.Click += new System.EventHandler(this.btnFinish_Click);
            // 
            // btnFinishPrevious
            // 
            this.btnFinishPrevious.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFinishPrevious.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnFinishPrevious.Location = new System.Drawing.Point(1025, 501);
            this.btnFinishPrevious.Name = "btnFinishPrevious";
            this.btnFinishPrevious.Size = new System.Drawing.Size(88, 30);
            this.btnFinishPrevious.TabIndex = 11;
            this.btnFinishPrevious.Text = "Previous";
            this.btnFinishPrevious.Click += new System.EventHandler(this.btnFinishPrevious_Click);
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureEdit2.EditValue = global::WoodPlan5.Properties.Resources.wizard_finish1;
            this.pictureEdit2.Location = new System.Drawing.Point(0, 0);
            this.pictureEdit2.MenuManager = this.barManager1;
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.ShowMenu = false;
            this.pictureEdit2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit2.Size = new System.Drawing.Size(205, 537);
            this.pictureEdit2.TabIndex = 5;
            // 
            // repositoryItemGridLookUpEdit1
            // 
            this.repositoryItemGridLookUpEdit1.AutoHeight = false;
            this.repositoryItemGridLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit1.DisplayMember = "Description";
            this.repositoryItemGridLookUpEdit1.LookAndFeel.SkinName = "Blue";
            this.repositoryItemGridLookUpEdit1.Name = "repositoryItemGridLookUpEdit1";
            this.repositoryItemGridLookUpEdit1.NullText = "";
            this.repositoryItemGridLookUpEdit1.ValueMember = "ID";
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // repositoryItemButtonEditChooseStaff
            // 
            this.repositoryItemButtonEditChooseStaff.AutoHeight = false;
            this.repositoryItemButtonEditChooseStaff.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions10, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject37, serializableAppearanceObject38, serializableAppearanceObject39, serializableAppearanceObject40, "Click me to open the Select Staff screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditChooseStaff.Name = "repositoryItemButtonEditChooseStaff";
            this.repositoryItemButtonEditChooseStaff.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // sp06145OMVisitEditBindingSource
            // 
            this.sp06145OMVisitEditBindingSource.DataMember = "sp06145_OM_Visit_Edit";
            this.sp06145OMVisitEditBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // dataSet_OM_Visit
            // 
            this.dataSet_OM_Visit.DataSetName = "DataSet_OM_Visit";
            this.dataSet_OM_Visit.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp06144OMVisitWizardSiteContractStartDatesBindingSource
            // 
            this.sp06144OMVisitWizardSiteContractStartDatesBindingSource.DataMember = "sp06144_OM_Visit_Wizard_Site_Contract_Start_Dates";
            this.sp06144OMVisitWizardSiteContractStartDatesBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // barStep1
            // 
            this.barStep1.BarName = "Custom 4";
            this.barStep1.DockCol = 0;
            this.barStep1.DockRow = 0;
            this.barStep1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.barStep1.FloatLocation = new System.Drawing.Point(480, 247);
            this.barStep1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemDateRange),
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemActive),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefreshClientContracts, true)});
            this.barStep1.OptionsBar.AllowQuickCustomization = false;
            this.barStep1.OptionsBar.DrawDragBorder = false;
            this.barStep1.OptionsBar.UseWholeRow = true;
            this.barStep1.StandaloneBarDockControl = this.standaloneBarDockControl3;
            this.barStep1.Text = "Custom 4";
            // 
            // barEditItemDateRange
            // 
            this.barEditItemDateRange.Caption = "Date Range:";
            this.barEditItemDateRange.Edit = this.repositoryItemPopupContainerEditDateRange;
            this.barEditItemDateRange.EditValue = "No Date Range Filter";
            this.barEditItemDateRange.EditWidth = 238;
            this.barEditItemDateRange.Id = 41;
            this.barEditItemDateRange.Name = "barEditItemDateRange";
            this.barEditItemDateRange.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // repositoryItemPopupContainerEditDateRange
            // 
            this.repositoryItemPopupContainerEditDateRange.AutoHeight = false;
            this.repositoryItemPopupContainerEditDateRange.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditDateRange.Name = "repositoryItemPopupContainerEditDateRange";
            this.repositoryItemPopupContainerEditDateRange.PopupControl = this.popupContainerControlDateRange;
            this.repositoryItemPopupContainerEditDateRange.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEditDateRange.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditDateRange_QueryResultValue);
            // 
            // barEditItemActive
            // 
            this.barEditItemActive.Caption = "Active:";
            this.barEditItemActive.Edit = this.repositoryItemCheckEdit4;
            this.barEditItemActive.EditValue = 1;
            this.barEditItemActive.EditWidth = 20;
            this.barEditItemActive.Id = 43;
            this.barEditItemActive.Name = "barEditItemActive";
            this.barEditItemActive.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEdit4.Appearance.Options.UseBackColor = true;
            this.repositoryItemCheckEdit4.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEdit4.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemCheckEdit4.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEdit4.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemCheckEdit4.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEdit4.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Caption = "";
            this.repositoryItemCheckEdit4.GlyphAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            this.repositoryItemCheckEdit4.ValueChecked = 1;
            this.repositoryItemCheckEdit4.ValueUnchecked = 0;
            // 
            // bbiRefreshClientContracts
            // 
            this.bbiRefreshClientContracts.Caption = "Load Client Contracts";
            this.bbiRefreshClientContracts.Id = 42;
            this.bbiRefreshClientContracts.ImageOptions.ImageIndex = 6;
            this.bbiRefreshClientContracts.Name = "bbiRefreshClientContracts";
            this.bbiRefreshClientContracts.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefreshClientContracts.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefreshClientContracts_ItemClick);
            // 
            // sp06144_OM_Visit_Wizard_Site_Contract_Start_DatesTableAdapter
            // 
            this.sp06144_OM_Visit_Wizard_Site_Contract_Start_DatesTableAdapter.ClearBeforeFill = true;
            // 
            // sp06145_OM_Visit_EditTableAdapter
            // 
            this.sp06145_OM_Visit_EditTableAdapter.ClearBeforeFill = true;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.MinValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // repositoryItemDuration1
            // 
            this.repositoryItemDuration1.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemDuration1.AutoHeight = false;
            this.repositoryItemDuration1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDuration1.DisabledStateText = null;
            this.repositoryItemDuration1.Name = "repositoryItemDuration1";
            this.repositoryItemDuration1.NullValuePromptShowForEmptyValue = true;
            this.repositoryItemDuration1.ShowEmptyItem = true;
            this.repositoryItemDuration1.ValidateOnEnterKey = true;
            // 
            // sp06156_OM_Job_Wizard_Client_ContractsTableAdapter
            // 
            this.sp06156_OM_Job_Wizard_Client_ContractsTableAdapter.ClearBeforeFill = true;
            // 
            // sp06157_OM_Job_Wizard_Site_Contracts_For_Client_ContractTableAdapter
            // 
            this.sp06157_OM_Job_Wizard_Site_Contracts_For_Client_ContractTableAdapter.ClearBeforeFill = true;
            // 
            // sp06158_OM_Job_Wizard_Visits_For_Site_ContractTableAdapter
            // 
            this.sp06158_OM_Job_Wizard_Visits_For_Site_ContractTableAdapter.ClearBeforeFill = true;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 9";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.FloatLocation = new System.Drawing.Point(597, 213);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.beiJobsFromSiteTemplate, "", true, true, true, 62),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSelectJobCollection),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiJobCollectionManager, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiMasterJobManager, true),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.beiJobsFromLastVisit, "", true, true, true, 64)});
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl2;
            this.bar1.Text = "Custom 9";
            // 
            // beiJobsFromSiteTemplate
            // 
            this.beiJobsFromSiteTemplate.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.beiJobsFromSiteTemplate.Caption = "Select Jobs From Site Template:";
            this.beiJobsFromSiteTemplate.Edit = this.checkEditJobsFromSiteTemplate;
            this.beiJobsFromSiteTemplate.EditValue = 0;
            this.beiJobsFromSiteTemplate.Id = 105;
            this.beiJobsFromSiteTemplate.Name = "beiJobsFromSiteTemplate";
            this.beiJobsFromSiteTemplate.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // checkEditJobsFromSiteTemplate
            // 
            this.checkEditJobsFromSiteTemplate.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.checkEditJobsFromSiteTemplate.Appearance.Options.UseBackColor = true;
            this.checkEditJobsFromSiteTemplate.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.checkEditJobsFromSiteTemplate.AppearanceDisabled.Options.UseBackColor = true;
            this.checkEditJobsFromSiteTemplate.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.checkEditJobsFromSiteTemplate.AppearanceFocused.Options.UseBackColor = true;
            this.checkEditJobsFromSiteTemplate.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.checkEditJobsFromSiteTemplate.AppearanceReadOnly.Options.UseBackColor = true;
            this.checkEditJobsFromSiteTemplate.AutoHeight = false;
            this.checkEditJobsFromSiteTemplate.Caption = "0 / 0";
            this.checkEditJobsFromSiteTemplate.GlyphAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.checkEditJobsFromSiteTemplate.Name = "checkEditJobsFromSiteTemplate";
            this.checkEditJobsFromSiteTemplate.ValueChecked = 1;
            this.checkEditJobsFromSiteTemplate.ValueUnchecked = 0;
            this.checkEditJobsFromSiteTemplate.EditValueChanged += new System.EventHandler(this.checkEditJobsFromSiteTemplate_EditValueChanged);
            // 
            // bbiSelectJobCollection
            // 
            this.bbiSelectJobCollection.Caption = "Select Job Collection";
            this.bbiSelectJobCollection.Id = 91;
            this.bbiSelectJobCollection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectJobCollection.ImageOptions.Image")));
            this.bbiSelectJobCollection.Name = "bbiSelectJobCollection";
            this.bbiSelectJobCollection.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Select Job Collection - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = resources.GetString("toolTipItem1.Text");
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiSelectJobCollection.SuperTip = superToolTip1;
            this.bbiSelectJobCollection.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSelectJobCollection_ItemClick);
            // 
            // bbiJobCollectionManager
            // 
            this.bbiJobCollectionManager.Caption = "Job Collections Manager";
            this.bbiJobCollectionManager.Id = 92;
            this.bbiJobCollectionManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiJobCollectionManager.ImageOptions.Image")));
            this.bbiJobCollectionManager.Name = "bbiJobCollectionManager";
            this.bbiJobCollectionManager.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Job Collections Manager - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to open the Job Collection Manager screen from which the Job collections" +
    " can be added, edited and deleted.\r\n";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiJobCollectionManager.SuperTip = superToolTip2;
            this.bbiJobCollectionManager.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiJobCollectionManager_ItemClick);
            // 
            // bbiMasterJobManager
            // 
            this.bbiMasterJobManager.Caption = "Master Job Manager";
            this.bbiMasterJobManager.Id = 93;
            this.bbiMasterJobManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiMasterJobManager.ImageOptions.Image")));
            this.bbiMasterJobManager.Name = "bbiMasterJobManager";
            this.bbiMasterJobManager.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Master Job Manager - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to open the Master Job Manager.\r\n\r\nThe Master Job Manager allows adding," +
    " editing and deleting of Master Job Types and Sub-Types.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiMasterJobManager.SuperTip = superToolTip3;
            this.bbiMasterJobManager.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiMasterJobManager_ItemClick);
            // 
            // beiJobsFromLastVisit
            // 
            this.beiJobsFromLastVisit.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.beiJobsFromLastVisit.Caption = "Select Jobs From Last Visit:";
            this.beiJobsFromLastVisit.Edit = this.checkEditJobsFromLastVisit;
            this.beiJobsFromLastVisit.EditValue = 0;
            this.beiJobsFromLastVisit.EditWidth = 151;
            this.beiJobsFromLastVisit.Id = 103;
            this.beiJobsFromLastVisit.Name = "beiJobsFromLastVisit";
            this.beiJobsFromLastVisit.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem4.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Text = "Select Jobs From Last Visit - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = resources.GetString("toolTipItem4.Text");
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.beiJobsFromLastVisit.SuperTip = superToolTip4;
            // 
            // checkEditJobsFromLastVisit
            // 
            this.checkEditJobsFromLastVisit.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.checkEditJobsFromLastVisit.Appearance.Options.UseBackColor = true;
            this.checkEditJobsFromLastVisit.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.checkEditJobsFromLastVisit.AppearanceDisabled.Options.UseBackColor = true;
            this.checkEditJobsFromLastVisit.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.checkEditJobsFromLastVisit.AppearanceFocused.Options.UseBackColor = true;
            this.checkEditJobsFromLastVisit.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.checkEditJobsFromLastVisit.AppearanceReadOnly.Options.UseBackColor = true;
            this.checkEditJobsFromLastVisit.AutoHeight = false;
            this.checkEditJobsFromLastVisit.Caption = "0 / 0";
            this.checkEditJobsFromLastVisit.GlyphAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.checkEditJobsFromLastVisit.Name = "checkEditJobsFromLastVisit";
            this.checkEditJobsFromLastVisit.ValueChecked = 1;
            this.checkEditJobsFromLastVisit.ValueUnchecked = 0;
            this.checkEditJobsFromLastVisit.EditValueChanged += new System.EventHandler(this.checkEditJobsFromLastVisit_EditValueChanged);
            // 
            // sp06162_OM_Job_Wizard_Master_Job_Sub_Types_AvailableTableAdapter
            // 
            this.sp06162_OM_Job_Wizard_Master_Job_Sub_Types_AvailableTableAdapter.ClearBeforeFill = true;
            // 
            // sp06176_OM_Job_Wizard_Visits_SelectedTableAdapter
            // 
            this.sp06176_OM_Job_Wizard_Visits_SelectedTableAdapter.ClearBeforeFill = true;
            // 
            // sp06182_OM_Job_Labour_EditTableAdapter
            // 
            this.sp06182_OM_Job_Labour_EditTableAdapter.ClearBeforeFill = true;
            // 
            // bar2
            // 
            this.bar2.BarName = "Custom 5";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar2.FloatLocation = new System.Drawing.Point(955, 212);
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddLabourFromContract),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddLabourFromContractAll, true)});
            this.bar2.OptionsBar.DisableClose = true;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar2.Text = "Custom 5";
            // 
            // bbiAddLabourFromContract
            // 
            this.bbiAddLabourFromContract.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiAddLabourFromContract.Caption = "Add from Site Contract";
            this.bbiAddLabourFromContract.Id = 94;
            this.bbiAddLabourFromContract.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiAddLabourFromContract.ImageOptions.Image")));
            this.bbiAddLabourFromContract.Name = "bbiAddLabourFromContract";
            this.bbiAddLabourFromContract.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem5.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Text = "Add Labour From Site Contract - Information\r\n";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to select Labour from the parent Site Contract Default Labour.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.bbiAddLabourFromContract.SuperTip = superToolTip5;
            this.bbiAddLabourFromContract.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddLabourFromContract_ItemClick);
            // 
            // bbiAddLabourFromContractAll
            // 
            this.bbiAddLabourFromContractAll.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiAddLabourFromContractAll.Caption = "Add <b>All</b> From Site Contract";
            this.bbiAddLabourFromContractAll.Id = 97;
            this.bbiAddLabourFromContractAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiAddLabourFromContractAll.ImageOptions.Image")));
            this.bbiAddLabourFromContractAll.Name = "bbiAddLabourFromContractAll";
            this.bbiAddLabourFromContractAll.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip6.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem6.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Appearance.Options.UseImage = true;
            toolTipTitleItem6.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Text = "Add All Labour From Site Contract - Information\r\n";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = resources.GetString("toolTipItem6.Text");
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this.bbiAddLabourFromContractAll.SuperTip = superToolTip6;
            this.bbiAddLabourFromContractAll.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddLabourFromContractAll_ItemClick);
            // 
            // sp06101_OM_Work_Unit_Types_PicklistTableAdapter
            // 
            this.sp06101_OM_Work_Unit_Types_PicklistTableAdapter.ClearBeforeFill = true;
            // 
            // bar3
            // 
            this.bar3.BarName = "Custom 6";
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar3.FloatLocation = new System.Drawing.Point(966, 243);
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddEquipmentFromContract)});
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.StandaloneBarDockControl = this.standaloneBarDockControl4;
            this.bar3.Text = "Custom 6";
            // 
            // bbiAddEquipmentFromContract
            // 
            this.bbiAddEquipmentFromContract.Caption = "Add from Site Contract";
            this.bbiAddEquipmentFromContract.Id = 95;
            this.bbiAddEquipmentFromContract.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiAddEquipmentFromContract.ImageOptions.Image")));
            this.bbiAddEquipmentFromContract.Name = "bbiAddEquipmentFromContract";
            this.bbiAddEquipmentFromContract.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem7.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Appearance.Options.UseImage = true;
            toolTipTitleItem7.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Text = "Add Equipment From Site Contract - Information";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Click me to select Equipment from the parent Site Contract Default Equipment.";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            this.bbiAddEquipmentFromContract.SuperTip = superToolTip7;
            this.bbiAddEquipmentFromContract.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddEquipmentFromContract_ItemClick);
            // 
            // sp06187_OM_Job_Equipment_EditTableAdapter
            // 
            this.sp06187_OM_Job_Equipment_EditTableAdapter.ClearBeforeFill = true;
            // 
            // bar4
            // 
            this.bar4.BarName = "Custom 7";
            this.bar4.DockCol = 0;
            this.bar4.DockRow = 0;
            this.bar4.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar4.FloatLocation = new System.Drawing.Point(1017, 251);
            this.bar4.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddMaterialFromContract)});
            this.bar4.OptionsBar.DrawDragBorder = false;
            this.bar4.OptionsBar.UseWholeRow = true;
            this.bar4.StandaloneBarDockControl = this.standaloneBarDockControl5;
            this.bar4.Text = "Custom 7";
            // 
            // bbiAddMaterialFromContract
            // 
            this.bbiAddMaterialFromContract.Caption = "Add from Site Contract";
            this.bbiAddMaterialFromContract.Id = 96;
            this.bbiAddMaterialFromContract.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiAddMaterialFromContract.ImageOptions.Image")));
            this.bbiAddMaterialFromContract.Name = "bbiAddMaterialFromContract";
            this.bbiAddMaterialFromContract.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem8.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem8.Appearance.Options.UseImage = true;
            toolTipTitleItem8.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem8.Text = "Add Material From Site Contract - Information";
            toolTipItem8.LeftIndent = 6;
            toolTipItem8.Text = "Click me to select Material from the parent Site Contract Default Material.";
            superToolTip8.Items.Add(toolTipTitleItem8);
            superToolTip8.Items.Add(toolTipItem8);
            this.bbiAddMaterialFromContract.SuperTip = superToolTip8;
            this.bbiAddMaterialFromContract.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddMaterialFromContract_ItemClick);
            // 
            // sp06191_OM_Job_Material_EditTableAdapter
            // 
            this.sp06191_OM_Job_Material_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp06108_OM_Unit_Descriptors_PicklistTableAdapter
            // 
            this.sp06108_OM_Unit_Descriptors_PicklistTableAdapter.ClearBeforeFill = true;
            // 
            // sp06174_OM_Job_EditTableAdapter
            // 
            this.sp06174_OM_Job_EditTableAdapter.ClearBeforeFill = true;
            // 
            // bsiRecordTicking
            // 
            this.bsiRecordTicking.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bsiRecordTicking.Caption = "Record <b>Ticking</b>";
            this.bsiRecordTicking.Id = 98;
            this.bsiRecordTicking.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiRecordTicking.ImageOptions.Image")));
            this.bsiRecordTicking.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiTick),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiUntick)});
            this.bsiRecordTicking.Name = "bsiRecordTicking";
            // 
            // bbiTick
            // 
            this.bbiTick.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiTick.Caption = "<b>Tick</b> Selected Records";
            this.bbiTick.Id = 99;
            this.bbiTick.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiTick.ImageOptions.Image")));
            this.bbiTick.Name = "bbiTick";
            this.bbiTick.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiTick_ItemClick);
            // 
            // bbiUntick
            // 
            this.bbiUntick.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiUntick.Caption = "<b>Untick</b> Selected Records";
            this.bbiUntick.Id = 100;
            this.bbiUntick.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUntick.ImageOptions.Image")));
            this.bbiUntick.Name = "bbiUntick";
            this.bbiUntick.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiUntick_ItemClick);
            // 
            // transitionManager1
            // 
            this.transitionManager1.FrameInterval = 5000;
            transition1.Control = this.xtraTabControl1;
            transition1.EasingMode = DevExpress.Data.Utils.EasingMode.EaseInOut;
            transition1.WaitingIndicatorProperties.Caption = "Loading...";
            this.transitionManager1.Transitions.Add(transition1);
            // 
            // bar5
            // 
            this.bar5.BarName = "Custom 8";
            this.bar5.DockCol = 0;
            this.bar5.DockRow = 0;
            this.bar5.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar5.FloatLocation = new System.Drawing.Point(562, 305);
            this.bar5.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSelectVisitsFromCriteria)});
            this.bar5.OptionsBar.AllowRename = true;
            this.bar5.OptionsBar.DrawDragBorder = false;
            this.bar5.OptionsBar.UseWholeRow = true;
            this.bar5.StandaloneBarDockControl = this.standaloneBarDockControl6;
            this.bar5.Text = "Custom 8";
            // 
            // bbiSelectVisitsFromCriteria
            // 
            this.bbiSelectVisitsFromCriteria.Caption = "Select Visits based on Criteria";
            this.bbiSelectVisitsFromCriteria.Id = 104;
            this.bbiSelectVisitsFromCriteria.ImageOptions.ImageIndex = 12;
            this.bbiSelectVisitsFromCriteria.Name = "bbiSelectVisitsFromCriteria";
            this.bbiSelectVisitsFromCriteria.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem9.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem9.Appearance.Options.UseImage = true;
            toolTipTitleItem9.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem9.Text = "Select Visits based on Criteria - Information";
            toolTipItem9.LeftIndent = 6;
            toolTipItem9.Text = "Click me to select Visits based on Criteria. A list of job sub-types will be show" +
    "n to allow you pick visits either with or without the jobs you select.";
            superToolTip9.Items.Add(toolTipTitleItem9);
            superToolTip9.Items.Add(toolTipItem9);
            this.bbiSelectVisitsFromCriteria.SuperTip = superToolTip9;
            this.bbiSelectVisitsFromCriteria.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSelectVisitsFromCriteria_ItemClick);
            // 
            // sp06177_OM_Job_Duration_Descriptors_With_BlankTableAdapter
            // 
            this.sp06177_OM_Job_Duration_Descriptors_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // TenderDetailsLabelControl
            // 
            this.TenderDetailsLabelControl.AllowHtmlString = true;
            this.TenderDetailsLabelControl.Location = new System.Drawing.Point(566, 12);
            this.TenderDetailsLabelControl.Name = "TenderDetailsLabelControl";
            this.TenderDetailsLabelControl.Size = new System.Drawing.Size(82, 13);
            this.TenderDetailsLabelControl.TabIndex = 18;
            this.TenderDetailsLabelControl.Text = "Selected Tender:";
            this.TenderDetailsLabelControl.Visible = false;
            // 
            // timerWelcomePage
            // 
            this.timerWelcomePage.Interval = 2000;
            this.timerWelcomePage.Tick += new System.EventHandler(this.timerWelcomePage_Tick);
            // 
            // frm_OM_Job_Wizard
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1236, 537);
            this.Controls.Add(this.TenderDetailsLabelControl);
            this.Controls.Add(this.xtraTabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_OM_Job_Wizard";
            this.Text = "Job Wizard - Operations";
            this.Activated += new System.EventHandler(this.frm_OM_Job_Wizard_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_OM_Job_Wizard_FormClosing);
            this.Load += new System.EventHandler(this.frm_OM_Job_Wizard_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.xtraTabControl1, 0);
            this.Controls.SetChildIndex(this.TenderDetailsLabelControl, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPageWelcome.ResumeLayout(false);
            this.xtraTabPageWelcome.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            this.xtraTabPageStep1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).EndInit();
            this.popupContainerControlDateRange.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06156OMJobWizardClientContractsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Job)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger)).EndInit();
            this.xtraTabPageStep2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            this.panelControl7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StoreInDBCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DefaultJobCollectionTemplateButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06157OMJobWizardSiteContractsForClientContractBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.panelControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).EndInit();
            this.xtraTabPageStep3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06158OMJobWizardVisitsForSiteContractBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditLatLong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            this.panelControl8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SelectedSiteContractCountTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit10.Properties)).EndInit();
            this.xtraTabPageStep4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06162OMJobWizardMasterJobSubTypesAvailableBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            this.panelControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SelectedVisitCountTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit7.Properties)).EndInit();
            this.xtraTabPageStep5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06176OMJobWizardVisitsSelectedBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06174OMJobEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditClientPONumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditEditDurationUnitDescriptor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06177OMJobDurationDescriptorsWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit4View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditFloat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditInteger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditJobTypeJobSubType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            this.panelControl6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit8.Properties)).EndInit();
            this.xtraTabPageStep6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSkipEquipmentAndMaterials.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06182OMJobLabourEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2DP8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditUnitDescriptor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06101OMWorkUnitTypesPicklistBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9x)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditCurrency8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditPercentage8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditContractorName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditLinkedToPersonType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).EndInit();
            this.panelControl10.ResumeLayout(false);
            this.panelControl10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit9.Properties)).EndInit();
            this.xtraTabPageStep7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).EndInit();
            this.splitContainerControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric2DP9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric0DP9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06187OMJobEquipmentEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.epositoryItemSpinEdit2DP10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.epositoryItemSpinEditCurrency10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.epositoryItemSpinEditPercentage10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditEquipmentName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).EndInit();
            this.panelControl11.ResumeLayout(false);
            this.panelControl11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit11.Properties)).EndInit();
            this.xtraTabPageStep8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).EndInit();
            this.splitContainerControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06191OMJobMaterialEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2DP12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06108OMUnitDescriptorsPicklistBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditCurrency12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditPercentage12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditMaterialName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).EndInit();
            this.panelControl12.ResumeLayout(false);
            this.panelControl12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit12.Properties)).EndInit();
            this.xtraTabPageFinish.ResumeLayout(false);
            this.xtraTabPageFinish.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditChooseStaff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06145OMVisitEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Visit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06144OMVisitWizardSiteContractStartDatesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDuration1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditJobsFromSiteTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditJobsFromLastVisit)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageWelcome;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageStep1;
        private DevExpress.XtraEditors.SimpleButton btnWelcomeNext;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageStep2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageFinish;
        private DevExpress.XtraEditors.SimpleButton btnStep1Previous;
        private DevExpress.XtraEditors.SimpleButton btnStep1Next;
        private DevExpress.XtraEditors.SimpleButton btnStep2Next;
        private DevExpress.XtraEditors.SimpleButton btnStep2Previous;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraEditors.PictureEdit pictureEdit3;
        private DevExpress.XtraEditors.SimpleButton btnFinish;
        private DevExpress.XtraEditors.SimpleButton btnFinishPrevious;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.CheckEdit checkEdit3;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.CheckEdit checkEdit2;
        private DataSet_OM_Contract dataSet_OM_Contract;
        private DevExpress.XtraEditors.PictureEdit pictureEdit4;
        private DevExpress.XtraEditors.PictureEdit pictureEdit5;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.PictureEdit pictureEdit6;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageStep4;
        private DevExpress.XtraEditors.SimpleButton btnStep4Next;
        private DevExpress.XtraEditors.SimpleButton btnStep4Previous;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.PictureEdit pictureEdit7;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageStep5;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.PictureEdit pictureEdit8;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.SimpleButton btnStep5Next;
        private DevExpress.XtraEditors.SimpleButton btnStep5Previous;
        private DevExpress.XtraGrid.GridControl gridControl6;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DataSet_AT dataSet_AT;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraBars.Bar barStep1;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl3;
        private DevExpress.XtraBars.BarEditItem barEditItemDateRange;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditDateRange;
        private DevExpress.XtraBars.BarButtonItem bbiRefreshClientContracts;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlDateRange;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraEditors.SimpleButton btnDateRangeOK;
        private DevExpress.XtraBars.BarEditItem barEditItemActive;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colGCCompanyID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDirectorID;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime3;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colActive1;
        private DevExpress.XtraGrid.Columns.GridColumn colSectorTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractValue;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency3;
        private DevExpress.XtraGrid.Columns.GridColumn colYearlyPercentageIncrease;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage3;
        private DevExpress.XtraGrid.Columns.GridColumn colCheckRPIDate;
        private DevExpress.XtraGrid.Columns.GridColumn colLastClientPaymentDate;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPreviousContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colFinanceClientCode;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingCentreCodeID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colGCCompanyName;
        private DevExpress.XtraGrid.Columns.GridColumn colContractType;
        private DevExpress.XtraGrid.Columns.GridColumn colContractStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDirector;
        private DevExpress.XtraGrid.Columns.GridColumn colSectorType;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit6;
        private DevExpress.XtraEditors.LabelControl labelControlSelectedSiteCount;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageStep3;
        private DevExpress.XtraEditors.SimpleButton btnStep3Next;
        private DevExpress.XtraEditors.SimpleButton btnStep3Previous;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private DevExpress.XtraEditors.PictureEdit pictureEdit10;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency4;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit8;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditLatLong;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditChooseStaff;
        private DevExpress.XtraGrid.Columns.GridColumn colClientYearCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditInteger;
        private DevExpress.XtraEditors.TextEdit SelectedSiteContractCountTextEdit;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colReactive;
        private DataSet_OM_Visit dataSet_OM_Visit;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractID3;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractID3;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime2;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate3;
        private DevExpress.XtraGrid.Columns.GridColumn colActive3;
        private DevExpress.XtraGrid.Columns.GridColumn colContractValue2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency2;
        private DevExpress.XtraGrid.Columns.GridColumn colYearlyPercentageIncrease3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage2;
        private DevExpress.XtraGrid.Columns.GridColumn colLastClientPaymentDate2;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks4;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToParent3;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditInteger2;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitNumberMax;
        private System.Windows.Forms.BindingSource sp06144OMVisitWizardSiteContractStartDatesBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditInteger4;
        private DataSet_OM_VisitTableAdapters.sp06144_OM_Visit_Wizard_Site_Contract_Start_DatesTableAdapter sp06144_OM_Visit_Wizard_Site_Contract_Start_DatesTableAdapter;
        private DevExpress.XtraEditors.LabelControl labelControlSelectedClientCount;
        private System.Windows.Forms.BindingSource sp06145OMVisitEditBindingSource;
        private DataSet_OM_VisitTableAdapters.sp06145_OM_Visit_EditTableAdapter sp06145_OM_Visit_EditTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraScheduler.UI.RepositoryItemDuration repositoryItemDuration1;
        private System.Windows.Forms.BindingSource sp06156OMJobWizardClientContractsBindingSource;
        private DataSet_OM_Job dataSet_OM_Job;
        private DataSet_OM_JobTableAdapters.sp06156_OM_Job_Wizard_Client_ContractsTableAdapter sp06156_OM_Job_Wizard_Client_ContractsTableAdapter;
        private System.Windows.Forms.BindingSource sp06157OMJobWizardSiteContractsForClientContractBindingSource;
        private DataSet_OM_JobTableAdapters.sp06157_OM_Job_Wizard_Site_Contracts_For_Client_ContractTableAdapter sp06157_OM_Job_Wizard_Site_Contracts_For_Client_ContractTableAdapter;
        private System.Windows.Forms.BindingSource sp06158OMJobWizardVisitsForSiteContractBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractID1;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffID1;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedStartDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedEndDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate2;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate2;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCalculationLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitCost1;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitSell1;
        private DevExpress.XtraGrid.Columns.GridColumn colStartLatitude1;
        private DevExpress.XtraGrid.Columns.GridColumn colStartLongitude1;
        private DevExpress.XtraGrid.Columns.GridColumn colFinishLatitude1;
        private DevExpress.XtraGrid.Columns.GridColumn colFinishLongitude1;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCalculationLevelDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffName1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractID2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName2;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName2;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToParent;
        private DevExpress.XtraGrid.Columns.GridColumn colJobCount;
        private DataSet_OM_JobTableAdapters.sp06158_OM_Job_Wizard_Visits_For_Site_ContractTableAdapter sp06158_OM_Job_Wizard_Visits_For_Site_ContractTableAdapter;
        private DevExpress.XtraEditors.LabelControl labelControlSelectedVisitCount;
        private DevExpress.XtraEditors.TextEdit SelectedVisitCountTextEdit;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiSelectJobCollection;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl2;
        private DevExpress.XtraBars.BarButtonItem bbiJobCollectionManager;
        private DevExpress.XtraBars.BarButtonItem bbiMasterJobManager;
        private DevExpress.XtraEditors.LabelControl labelControlSelectedJobCount;
        private System.Windows.Forms.BindingSource sp06162OMJobWizardMasterJobSubTypesAvailableBindingSource;
        private DataSet_OM_JobTableAdapters.sp06162_OM_Job_Wizard_Master_Job_Sub_Types_AvailableTableAdapter sp06162_OM_Job_Wizard_Master_Job_Sub_Types_AvailableTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeRecordOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colstrMode;
        private DevExpress.XtraGrid.Columns.GridColumn colstrRecordIDs;
        private DevExpress.XtraGrid.Columns.GridColumn colDummyJobID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobID;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colReactive1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobNoLongerRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colRequiresAccessPermit;
        private DevExpress.XtraGrid.Columns.GridColumn colAccessPermitID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPONumber;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPOID;
        private DevExpress.XtraGrid.Columns.GridColumn colFinanceSystemPONumber;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedDurationUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedDurationUnitsDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colScheduleSentDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActualStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActualEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActualDurationUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colActualDurationUnitsDescriptionID;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalMaterialExVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalMaterialVATRate;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalMaterialVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalMaterialCost;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalMaterialExVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalMaterialVATRate;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalMaterialVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalMaterialCost;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalEquipmentExVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalEquipmentVATRate;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalEquipmentVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalEquipmentCost;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalEquipmentExVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalEquipmentVATRate;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalEquipmentVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalEquipmentCost;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalLabourExVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalLabourVATRate;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalLabourVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalLabourCost;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalLabourExVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalLabourVATRate;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalLabourVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalLabourCost;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalCostExVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalCostVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalCost;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalCostExVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalCostVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalCost;
        private DevExpress.XtraGrid.Columns.GridColumn colClientInvoiceID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateClientInvoiced;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoiceID;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoiceReceivedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoicePaidDate;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoiceAmountPaid;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotPayContractor;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotInvoiceClient;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colRouteOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colStartLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colStartLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colFinishLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colFinishLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colTenderID;
        private DevExpress.XtraGrid.Columns.GridColumn colTenderJobID;
        private DevExpress.XtraGrid.Columns.GridColumn colMinimumDaysFromLastVisit;
        private DevExpress.XtraGrid.Columns.GridColumn colMaximumDaysFromLastVisit;
        private DevExpress.XtraGrid.Columns.GridColumn colDaysLeeway;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingCentreCodeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID2;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID3;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientNameContractDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName1;
        private DevExpress.XtraGrid.Columns.GridColumn colSitePostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationX;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationY;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeJobSubTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitNumber;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit5;
        private System.Windows.Forms.BindingSource sp06176OMJobWizardVisitsSelectedBindingSource;
        private DataSet_OM_JobTableAdapters.sp06176_OM_Job_Wizard_Visits_SelectedTableAdapter sp06176_OM_Job_Wizard_Visits_SelectedTableAdapter;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageStep6;
        private DevExpress.XtraEditors.PanelControl panelControl10;
        private DevExpress.XtraEditors.PictureEdit pictureEdit9;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageStep7;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageStep8;
        private DevExpress.XtraEditors.PanelControl panelControl11;
        private DevExpress.XtraEditors.PictureEdit pictureEdit11;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.PanelControl panelControl12;
        private DevExpress.XtraEditors.PictureEdit pictureEdit12;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.SimpleButton btnStep6Next;
        private DevExpress.XtraEditors.SimpleButton btnStep6Previous;
        private DevExpress.XtraEditors.SimpleButton btnStep7Next;
        private DevExpress.XtraEditors.SimpleButton btnStep7Previous;
        private DevExpress.XtraEditors.SimpleButton btnStep8Next;
        private DevExpress.XtraEditors.SimpleButton btnStep8Previous;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraGrid.GridControl gridControl7;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime5;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency6;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit7;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP6;
        private DevExpress.XtraGrid.GridControl gridControl8;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn37;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn39;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn40;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn41;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn42;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn43;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn44;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn45;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn46;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn47;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn48;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn49;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn50;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn51;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn52;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn147;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn148;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn149;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn150;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn151;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn152;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn153;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn154;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn155;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn156;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn157;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn158;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn159;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn160;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn161;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn162;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn163;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn164;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn165;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn166;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn167;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn168;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn169;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn170;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn171;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn172;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn173;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn174;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn175;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn176;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn177;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn178;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn179;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn180;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn181;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn182;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn183;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn184;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn185;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn186;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn187;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn188;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn189;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn190;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn191;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn192;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn193;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn194;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn195;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn196;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn197;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn198;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn199;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn200;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn201;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn202;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn203;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn204;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn205;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn206;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn207;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn208;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn209;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn210;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn211;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage6;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditInteger6;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourCount;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentCount;
        private DevExpress.XtraGrid.Columns.GridColumn colMaterialCount;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourCount1;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentCount1;
        private DevExpress.XtraGrid.Columns.GridColumn colMaterialCount1;
        private System.Windows.Forms.BindingSource sp06182OMJobLabourEditBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colstrMode1;
        private DevExpress.XtraGrid.Columns.GridColumn colstrRecordIDs1;
        private DevExpress.XtraGrid.Columns.GridColumn colDummyJobID1;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourUsedID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobID1;
        private DevExpress.XtraGrid.Columns.GridColumn colCostUnitsUsed;
        private DevExpress.XtraGrid.Columns.GridColumn colCostUnitDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colSellUnitsUsed;
        private DevExpress.XtraGrid.Columns.GridColumn colSellUnitDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colCostPerUnitExVat;
        private DevExpress.XtraGrid.Columns.GridColumn colCostPerUnitVatRate;
        private DevExpress.XtraGrid.Columns.GridColumn colSellPerUnitExVat;
        private DevExpress.XtraGrid.Columns.GridColumn colSellPerUnitVatRate;
        private DevExpress.XtraGrid.Columns.GridColumn colCostValueExVat;
        private DevExpress.XtraGrid.Columns.GridColumn colCostValueVat;
        private DevExpress.XtraGrid.Columns.GridColumn colCostValueTotal;
        private DevExpress.XtraGrid.Columns.GridColumn colSellValueExVat;
        private DevExpress.XtraGrid.Columns.GridColumn colSellValueVat;
        private DevExpress.XtraGrid.Columns.GridColumn colSellValueTotal;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks3;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID4;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID3;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitID2;
        private DevExpress.XtraGrid.Columns.GridColumn colJobExpectedStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName3;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName3;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitNumber2;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colCostUnitDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colSellUnitDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourType;
        private DevExpress.XtraGrid.Columns.GridColumn colCISPercentage;
        private DevExpress.XtraGrid.Columns.GridColumn colCISValue;
        private DevExpress.XtraGrid.Columns.GridColumn colPurchaseOrderID;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDateTime;
        private DataSet_OM_JobTableAdapters.sp06182_OM_Job_Labour_EditTableAdapter sp06182_OM_Job_Labour_EditTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit2DP8;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditUnitDescriptor;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView9x;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditCurrency8;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditPercentage8;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit9;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditContractorName;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime8;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDescription3;
        private DevExpress.XtraGrid.Columns.GridColumn colClientNameContractDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeJobSubTypeDescription1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem bbiAddLabourFromContract;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationX1;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationY1;
        private DevExpress.XtraGrid.Columns.GridColumn colSitePostcode1;
        private DevExpress.XtraGrid.Columns.GridColumn colLatLongSiteDistance;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit6;
        private DevExpress.XtraGrid.Columns.GridColumn colPostcodeSiteDistance;
        private System.Windows.Forms.BindingSource sp06101OMWorkUnitTypesPicklistBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn53;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn54;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn55;
        private DataSet_OM_ContractTableAdapters.sp06101_OM_Work_Unit_Types_PicklistTableAdapter sp06101_OM_Work_Unit_Types_PicklistTableAdapter;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl4;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl3;
        private DevExpress.XtraGrid.GridControl gridControl9;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn56;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn57;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn58;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn59;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn60;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn61;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn62;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn63;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn64;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn65;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn66;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn67;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn68;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn69;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn70;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn71;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn72;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn73;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumeric2DP9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn74;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn75;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn76;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn77;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn78;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn79;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn80;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn81;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn82;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn83;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn84;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn85;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn86;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn87;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn88;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn89;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn90;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn91;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn92;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn93;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn94;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn95;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn96;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn97;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn98;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn99;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn100;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn101;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn102;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn103;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn104;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn105;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn106;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn107;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn108;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn109;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn110;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn111;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn112;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn113;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn114;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn115;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn116;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn117;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn118;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn119;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn120;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn121;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn122;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn123;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn124;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn125;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn126;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumeric0DP9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn127;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn128;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn129;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn130;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn131;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn132;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn133;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn134;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn135;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn136;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn137;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn138;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn139;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn140;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn141;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn142;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn143;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn144;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn145;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn146;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn212;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn213;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn214;
        private DevExpress.XtraGrid.GridControl gridControl10;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn215;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn216;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn217;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn218;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn219;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn220;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn221;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit epositoryItemSpinEdit2DP10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn222;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn223;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn224;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn225;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn226;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn227;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn228;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit epositoryItemSpinEditCurrency10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn229;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit epositoryItemSpinEditPercentage10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn230;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn231;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn232;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn233;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn234;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn235;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn236;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn237;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn238;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn239;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn240;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn241;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn242;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn243;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn244;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn245;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn246;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn247;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn248;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditEquipmentName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn249;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn250;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerType;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn254;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn255;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn256;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn257;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn258;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn259;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn260;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn261;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarButtonItem bbiAddEquipmentFromContract;
        private System.Windows.Forms.BindingSource sp06187OMJobEquipmentEditBindingSource;
        private DataSet_OM_JobTableAdapters.sp06187_OM_Job_Equipment_EditTableAdapter sp06187_OM_Job_Equipment_EditTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerName;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeID2;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeID2;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl4;
        private DevExpress.XtraGrid.GridControl gridControl11;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn251;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn252;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn253;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn262;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn263;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn264;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn265;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn266;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn267;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn268;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn269;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn270;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn271;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn272;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn273;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn274;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn275;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn276;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn277;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn278;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn279;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn280;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn281;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn282;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn283;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn284;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn285;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn286;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn287;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn288;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn289;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn290;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn291;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn292;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn293;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn294;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn295;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn296;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn297;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn298;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn299;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn300;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn301;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn302;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn303;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn304;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn305;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn306;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn307;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn308;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn309;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn310;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn311;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn312;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn313;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn314;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn315;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn316;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn317;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn318;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn319;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn320;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn321;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn322;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn323;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn324;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn325;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn326;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn327;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn328;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn329;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn330;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn331;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn332;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn333;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn334;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn335;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn336;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn337;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn338;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn339;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn340;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn341;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn342;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn343;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn344;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn345;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn346;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn347;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn348;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn349;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn350;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn351;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn352;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl5;
        private DevExpress.XtraGrid.GridControl gridControl12;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn353;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn354;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn355;
        private DevExpress.XtraGrid.Columns.GridColumn colMaterialUsedID;
        private DevExpress.XtraGrid.Columns.GridColumn colMaterialID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn358;
        private DevExpress.XtraGrid.Columns.GridColumn colCostUnitsUsed3;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit2DP12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn360;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn361;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn362;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn363;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn364;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn365;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn366;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditCurrency12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn367;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditPercentage12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn368;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn369;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn370;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn371;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn372;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn373;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn374;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn375;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn376;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn377;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn378;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn379;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn380;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn381;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn382;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn383;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn384;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn385;
        private DevExpress.XtraGrid.Columns.GridColumn colMaterialName;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditMaterialName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn387;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn388;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn390;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn391;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn392;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn393;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn394;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn395;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn396;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn397;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn399;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn400;
        private DevExpress.XtraBars.Bar bar4;
        private DevExpress.XtraBars.BarButtonItem bbiAddMaterialFromContract;
        private System.Windows.Forms.BindingSource sp06191OMJobMaterialEditBindingSource;
        private DataSet_OM_JobTableAdapters.sp06191_OM_Job_Material_EditTableAdapter sp06191_OM_Job_Material_EditTableAdapter;
        private System.Windows.Forms.BindingSource sp06108OMUnitDescriptorsPicklistBindingSource;
        private DataSet_OM_ContractTableAdapters.sp06108_OM_Unit_Descriptors_PicklistTableAdapter sp06108_OM_Unit_Descriptors_PicklistTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationX2;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationY2;
        private DevExpress.XtraGrid.Columns.GridColumn colSitePostcode2;
        private System.Windows.Forms.BindingSource sp06174OMJobEditBindingSource;
        private DataSet_OM_JobTableAdapters.sp06174_OM_Job_EditTableAdapter sp06174_OM_Job_EditTableAdapter;
        private DevExpress.XtraBars.BarButtonItem bbiAddLabourFromContractAll;
        private DevExpress.XtraBars.BarSubItem bsiRecordTicking;
        private DevExpress.XtraBars.BarButtonItem bbiTick;
        private DevExpress.XtraBars.BarButtonItem bbiUntick;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitCategoryID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonType;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditLinkedToPersonType;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPOID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPONumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPOID2;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPONumber2;
        private DevExpress.XtraEditors.CheckEdit checkEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitCategory1;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitCategoryID1;
        private DevExpress.Utils.Animation.TransitionManager transitionManager1;
        private DevExpress.XtraGrid.Columns.GridColumn colSellCalculationLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colSellCalculationLevelDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colSellCalculationLevel1;
        private DevExpress.XtraGrid.Columns.GridColumn colSellCalculationLevelDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitCostCalculationLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitCostCalculationLevel1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditClientPONumber;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit2DP;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditInteger3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditFloat;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditInteger;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditJobTypeJobSubType;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedFromVisitTemplate;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedFromVisitTemplateID;
        private DevExpress.XtraBars.BarEditItem beiJobsFromLastVisit;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit checkEditJobsFromLastVisit;
        private DevExpress.XtraGrid.Columns.GridColumn colLastVisitStartDate;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl6;
        private DevExpress.XtraBars.Bar bar5;
        private DevExpress.XtraBars.BarButtonItem bbiSelectVisitsFromCriteria;
        private System.Windows.Forms.BindingSource sp06177OMJobDurationDescriptorsWithBlankBindingSource;
        private DataSet_OM_JobTableAdapters.sp06177_OM_Job_Duration_Descriptors_With_BlankTableAdapter sp06177_OM_Job_Duration_Descriptors_With_BlankTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditEditDurationUnitDescriptor1;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit4View;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn356;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn357;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn359;
        private DevExpress.XtraGrid.Columns.GridColumn colOnHold;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit7;
        private DevExpress.XtraGrid.Columns.GridColumn colOnHoldReasonID;
        private DevExpress.XtraGrid.Columns.GridColumn colOnHoldReason;
        private DevExpress.XtraGrid.Columns.GridColumn colOnHoldStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colOnHoldEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCategory;
        private DevExpress.XtraEditors.CheckEdit checkEditSkipEquipmentAndMaterials;
        private DevExpress.XtraGrid.Columns.GridColumn colDisplayOrder;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colMandatory;
        private DevExpress.XtraEditors.LabelControl TenderDetailsLabelControl;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitType;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDefaultJobCollectionTemplate;
        private DevExpress.XtraGrid.Columns.GridColumn colDefaultJobCollectionTemplateID;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.SimpleButton simpleButtonApplyDefaultJobCollectionTemplate;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.CheckEdit StoreInDBCheckEdit;
        private DevExpress.XtraEditors.ButtonEdit DefaultJobCollectionTemplateButtonEdit;
        private DevExpress.XtraBars.BarEditItem beiJobsFromSiteTemplate;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit checkEditJobsFromSiteTemplate;
        private System.Windows.Forms.Timer timerWelcomePage;
    }
}

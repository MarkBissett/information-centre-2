﻿namespace WoodPlan5
{
    partial class frm_OM_Job_Collection_Template_Copy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Job_Collection_Template_Copy));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.dataSet_OM_Contract = new WoodPlan5.DataSet_OM_Contract();
            this.MemoEditRemarks = new DevExpress.XtraEditors.MemoEdit();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bsiInformation = new DevExpress.XtraBars.BarStaticItem();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.RecordOrderSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ClientNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.DescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.checkEditVisitNumbers = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditItemRules = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditVisitCategories = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditTemplateItems = new DevExpress.XtraEditors.CheckEdit();
            this.ClientIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MemoEditRemarks.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RecordOrderSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditVisitNumbers.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditItemRules.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditVisitCategories.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditTemplateItems.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientIDTextEdit.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(589, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 255);
            this.barDockControlBottom.Size = new System.Drawing.Size(589, 30);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 229);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(589, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 229);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.Images = this.imageCollection1;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bsiInformation});
            this.barManager1.MaxItemId = 32;
            this.barManager1.StatusBar = this.bar1;
            // 
            // dataSet_OM_Contract
            // 
            this.dataSet_OM_Contract.DataSetName = "DataSet_OM_Contract";
            this.dataSet_OM_Contract.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // MemoEditRemarks
            // 
            this.MemoEditRemarks.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MemoEditRemarks.Location = new System.Drawing.Point(6, 128);
            this.MemoEditRemarks.MenuManager = this.barManager1;
            this.MemoEditRemarks.Name = "MemoEditRemarks";
            this.MemoEditRemarks.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.MemoEditRemarks, true);
            this.MemoEditRemarks.Size = new System.Drawing.Size(395, 83);
            this.scSpellChecker.SetSpellCheckerOptions(this.MemoEditRemarks, optionsSpelling1);
            this.MemoEditRemarks.TabIndex = 11;
            // 
            // btnOK
            // 
            this.btnOK.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.ImageIndex = 1;
            this.btnOK.ImageList = this.imageCollection1;
            this.btnOK.Location = new System.Drawing.Point(421, 227);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(78, 22);
            this.btnOK.TabIndex = 13;
            this.btnOK.Text = "<b>OK</b>";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "Info_16x16.png");
            this.imageCollection1.InsertGalleryImage("apply_16x16.png", "images/actions/apply_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/apply_16x16.png"), 1);
            this.imageCollection1.Images.SetKeyName(1, "apply_16x16.png");
            this.imageCollection1.InsertGalleryImage("cancel_16x16.png", "images/actions/cancel_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/cancel_16x16.png"), 2);
            this.imageCollection1.Images.SetKeyName(2, "cancel_16x16.png");
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.ImageIndex = 2;
            this.btnCancel.ImageList = this.imageCollection1;
            this.btnCancel.Location = new System.Drawing.Point(505, 227);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(78, 22);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 3";
            this.bar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiInformation)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 3";
            // 
            // bsiInformation
            // 
            this.bsiInformation.Caption = "Complete the fields displayed then click OK.";
            this.bsiInformation.Id = 30;
            this.bsiInformation.ImageIndex = 0;
            this.bsiInformation.Name = "bsiInformation";
            this.bsiInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bsiInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // groupControl1
            // 
            this.groupControl1.AllowHtmlText = true;
            this.groupControl1.Controls.Add(this.ClientIDTextEdit);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.RecordOrderSpinEdit);
            this.groupControl1.Controls.Add(this.ClientNameButtonEdit);
            this.groupControl1.Controls.Add(this.DescriptionTextEdit);
            this.groupControl1.Controls.Add(this.MemoEditRemarks);
            this.groupControl1.Location = new System.Drawing.Point(9, 34);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(406, 216);
            this.groupControl1.TabIndex = 15;
            this.groupControl1.Text = "Job <B>Template</b> - Adjust details as required for generated template.";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(6, 109);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(45, 13);
            this.labelControl4.TabIndex = 53;
            this.labelControl4.Text = "Remarks:";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(6, 83);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(32, 13);
            this.labelControl3.TabIndex = 52;
            this.labelControl3.Text = "Order:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(6, 57);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(31, 13);
            this.labelControl2.TabIndex = 51;
            this.labelControl2.Text = "Client:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(6, 31);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(57, 13);
            this.labelControl1.TabIndex = 50;
            this.labelControl1.Text = "Description:";
            // 
            // RecordOrderSpinEdit
            // 
            this.RecordOrderSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.RecordOrderSpinEdit.Location = new System.Drawing.Point(74, 79);
            this.RecordOrderSpinEdit.MenuManager = this.barManager1;
            this.RecordOrderSpinEdit.Name = "RecordOrderSpinEdit";
            this.RecordOrderSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RecordOrderSpinEdit.Properties.IsFloatValue = false;
            this.RecordOrderSpinEdit.Properties.Mask.EditMask = "N00";
            this.RecordOrderSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.RecordOrderSpinEdit.Properties.MaxValue = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.RecordOrderSpinEdit.Size = new System.Drawing.Size(68, 20);
            this.RecordOrderSpinEdit.TabIndex = 49;
            // 
            // ClientNameButtonEdit
            // 
            this.ClientNameButtonEdit.EditValue = "";
            this.ClientNameButtonEdit.Location = new System.Drawing.Point(74, 53);
            this.ClientNameButtonEdit.MenuManager = this.barManager1;
            this.ClientNameButtonEdit.Name = "ClientNameButtonEdit";
            this.ClientNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "Click me to open the Select Client screen", "choose", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "Click me to Clear the selected Client", "clear", null, true)});
            this.ClientNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ClientNameButtonEdit.Size = new System.Drawing.Size(327, 20);
            this.ClientNameButtonEdit.TabIndex = 35;
            this.ClientNameButtonEdit.TabStop = false;
            this.ClientNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ClientNameButtonEdit_ButtonClick);
            // 
            // DescriptionTextEdit
            // 
            this.DescriptionTextEdit.Location = new System.Drawing.Point(74, 27);
            this.DescriptionTextEdit.MenuManager = this.barManager1;
            this.DescriptionTextEdit.Name = "DescriptionTextEdit";
            this.DescriptionTextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.DescriptionTextEdit, true);
            this.DescriptionTextEdit.Size = new System.Drawing.Size(327, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.DescriptionTextEdit, optionsSpelling3);
            this.DescriptionTextEdit.TabIndex = 34;
            // 
            // groupControl4
            // 
            this.groupControl4.AllowHtmlText = true;
            this.groupControl4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl4.Controls.Add(this.checkEditVisitNumbers);
            this.groupControl4.Controls.Add(this.checkEditItemRules);
            this.groupControl4.Controls.Add(this.checkEditVisitCategories);
            this.groupControl4.Controls.Add(this.checkEditTemplateItems);
            this.groupControl4.Location = new System.Drawing.Point(424, 34);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(159, 130);
            this.groupControl4.TabIndex = 22;
            this.groupControl4.Text = "<b>Linked Data</b> To Copy";
            // 
            // checkEditVisitNumbers
            // 
            this.checkEditVisitNumbers.EditValue = true;
            this.checkEditVisitNumbers.Location = new System.Drawing.Point(37, 97);
            this.checkEditVisitNumbers.MenuManager = this.barManager1;
            this.checkEditVisitNumbers.Name = "checkEditVisitNumbers";
            this.checkEditVisitNumbers.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEditVisitNumbers.Properties.Caption = "Visit <b>Numbers</b>";
            this.checkEditVisitNumbers.Size = new System.Drawing.Size(110, 19);
            this.checkEditVisitNumbers.TabIndex = 9;
            // 
            // checkEditItemRules
            // 
            this.checkEditItemRules.EditValue = true;
            this.checkEditItemRules.Location = new System.Drawing.Point(21, 74);
            this.checkEditItemRules.MenuManager = this.barManager1;
            this.checkEditItemRules.Name = "checkEditItemRules";
            this.checkEditItemRules.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEditItemRules.Properties.Caption = "Item <b>Rules</b>";
            this.checkEditItemRules.Size = new System.Drawing.Size(126, 19);
            this.checkEditItemRules.TabIndex = 7;
            this.checkEditItemRules.CheckedChanged += new System.EventHandler(this.checkEditItemRules_CheckedChanged);
            // 
            // checkEditVisitCategories
            // 
            this.checkEditVisitCategories.EditValue = true;
            this.checkEditVisitCategories.Location = new System.Drawing.Point(21, 51);
            this.checkEditVisitCategories.MenuManager = this.barManager1;
            this.checkEditVisitCategories.Name = "checkEditVisitCategories";
            this.checkEditVisitCategories.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEditVisitCategories.Properties.Caption = "Visit <b>Categories</b>";
            this.checkEditVisitCategories.Size = new System.Drawing.Size(126, 19);
            this.checkEditVisitCategories.TabIndex = 6;
            // 
            // checkEditTemplateItems
            // 
            this.checkEditTemplateItems.EditValue = true;
            this.checkEditTemplateItems.Location = new System.Drawing.Point(5, 28);
            this.checkEditTemplateItems.MenuManager = this.barManager1;
            this.checkEditTemplateItems.Name = "checkEditTemplateItems";
            this.checkEditTemplateItems.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEditTemplateItems.Properties.Caption = "Template <b>Items</b>";
            this.checkEditTemplateItems.Size = new System.Drawing.Size(142, 19);
            this.checkEditTemplateItems.TabIndex = 5;
            this.checkEditTemplateItems.CheckedChanged += new System.EventHandler(this.checkEditTemplateItems_CheckedChanged);
            // 
            // ClientIDTextEdit
            // 
            this.ClientIDTextEdit.Enabled = false;
            this.ClientIDTextEdit.Location = new System.Drawing.Point(314, 79);
            this.ClientIDTextEdit.MenuManager = this.barManager1;
            this.ClientIDTextEdit.Name = "ClientIDTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientIDTextEdit, true);
            this.ClientIDTextEdit.Size = new System.Drawing.Size(87, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientIDTextEdit, optionsSpelling2);
            this.ClientIDTextEdit.TabIndex = 54;
            this.ClientIDTextEdit.Visible = false;
            // 
            // frm_OM_Job_Collection_Template_Copy
            // 
            this.ClientSize = new System.Drawing.Size(589, 285);
            this.ControlBox = false;
            this.Controls.Add(this.groupControl4);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_OM_Job_Collection_Template_Copy";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Copy Job Collection Template";
            this.Load += new System.EventHandler(this.frm_OM_Job_Collection_Template_Copy_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.groupControl1, 0);
            this.Controls.SetChildIndex(this.groupControl4, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MemoEditRemarks.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RecordOrderSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditVisitNumbers.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditItemRules.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditVisitCategories.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditTemplateItems.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientIDTextEdit.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.MemoEdit MemoEditRemarks;
        private DataSet_OM_Contract dataSet_OM_Contract;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarStaticItem bsiInformation;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.CheckEdit checkEditItemRules;
        private DevExpress.XtraEditors.CheckEdit checkEditVisitCategories;
        private DevExpress.XtraEditors.CheckEdit checkEditTemplateItems;
        private DevExpress.XtraEditors.CheckEdit checkEditVisitNumbers;
        private DevExpress.XtraEditors.TextEdit DescriptionTextEdit;
        private DevExpress.XtraEditors.ButtonEdit ClientNameButtonEdit;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SpinEdit RecordOrderSpinEdit;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit ClientIDTextEdit;
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_OM_Site_Contract_Wizard_Block_Edit_Preferred_Labour_Info : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        public string strLinkedToPersonType = null;
        public string strContractorName = null;
        public int? intCostUnitDescriptorID = null;
        public int? intSellUnitDescriptorID = null;
        public decimal? decCostPerUnitExVat = null;
        public decimal? decCostPerUnitVatRate = null;
        public decimal? decSellPerUnitExVat = null;
        public decimal? decSellPerUnitVatRate = null;
        public decimal? decCISPercentage = null;
        public decimal? decCISValue = null;
        public decimal? decPostcodeSiteDistance = null;
        public decimal? decLatLongSiteDistance = null;
        public string strRemarks = null;

        public int? intLinkedToPersonTypeID = null;
        public int? intContractorID = null;
        public double? dbTeamLocationX = null;
        public double? dbTeamLocationY = null;
        public string strTeamPostcode = null;
        #endregion

        public frm_OM_Site_Contract_Wizard_Block_Edit_Preferred_Labour_Info()
        {
            InitializeComponent();
        }

        private void frm_OM_Site_Contract_Wizard_Block_Edit_Preferred_Labour_Info_Load(object sender, EventArgs e)
        {
            this.FormID = 500136;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            Application.DoEvents();  // Allow Form time to repaint itself //
            
            strConnectionString = this.GlobalSettings.ConnectionString;
            
            sp06101_OM_Work_Unit_Types_PicklistTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06101_OM_Work_Unit_Types_PicklistTableAdapter.Fill(dataSet_OM_Contract.sp06101_OM_Work_Unit_Types_Picklist, 1);

            LinkedToPersonTypeIDTextEdit.EditValue = null;
            LinkedToPersonTypeButtonEdit.EditValue = null;
            ContractorIDTextEdit.EditValue = null;
            ContractorNameButtonEdit.EditValue = null;
            CostUnitDescriptorIDGridLookUpEdit.EditValue = null;
            SellUnitDescriptorIDGridLookUpEdit.EditValue = null;
            CostPerUnitExVatSpinEdit.EditValue = null;
            CostPerUnitVatRateSpinEdit.EditValue = null;
            SellPerUnitExVatSpinEdit.EditValue = null;
            SellPerUnitVatRateSpinEdit.EditValue = null;
            CISPercentageSpinEdit.EditValue = null;
            CISValueSpinEdit.EditValue = null;
            PostcodeSiteDistanceSpinEdit.EditValue = null;
            LatLongSiteDistanceSpinEdit.EditValue = null;
            RemarksMemoEdit.EditValue = null;
            LocationXTextEdit.EditValue = null;
            LocationYTextEdit.EditValue = null;
            TeamPostcodeTextEdit.EditValue = null;
            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
        }

        private void LinkedToPersonTypeButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var fChildForm = new frm_Core_Select_Person_Type();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalPersonTypeID = 0;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    LinkedToPersonTypeButtonEdit.EditValue = fChildForm.strSelectedPersonType;
                    LinkedToPersonTypeIDTextEdit.EditValue = fChildForm.intSelectedPersonTypeID;
                    
                    ContractorIDTextEdit.EditValue = 0;
                    ContractorNameButtonEdit.EditValue = "";
                    LocationXTextEdit.EditValue = (double)0.00;
                    LocationYTextEdit.EditValue = (double)0.00;
                    TeamPostcodeTextEdit.EditValue = "";
                }
            }
        }

        private void ContractorNameButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                int intPersonTypeID = -1;
                try { intPersonTypeID = (string.IsNullOrEmpty(LinkedToPersonTypeIDTextEdit.EditValue.ToString()) ? 0 : Convert.ToInt32(LinkedToPersonTypeIDTextEdit.EditValue)); }
                catch (Exception) { }

                if (intPersonTypeID <= -1)
                {
                    XtraMessageBox.Show("Select the Person Type by clicking the Choose button on the Labour Type field before proceeding.", "Select Labour", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                if (intPersonTypeID == 1)  // Contractor //
                {
                    var fChildForm = new frm_OM_Select_Contractor();
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.intOriginalContractorID = 0;
                    fChildForm._PassedInLatitude = (double)0.00;
                    fChildForm._PassedInLongitude = (double)0.00;
                    fChildForm._PassedInPostcode = "";
                    if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                    {
                        ContractorIDTextEdit.EditValue = fChildForm.intSelectedContractorID;
                        ContractorNameButtonEdit.EditValue = fChildForm.strSelectedContractorName;

                        LocationXTextEdit.EditValue = fChildForm.dbSelectedLatitude;
                        LocationYTextEdit.EditValue = fChildForm.dbSelectedLongitude;
                        TeamPostcodeTextEdit.EditValue = fChildForm.strSelectedPostcode;
                    }
                }
                else  // Staff //
                {
                    var fChildForm = new frm_HR_Select_Staff();
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.intOriginalStaffID = 0;
                    if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                    {
                        ContractorIDTextEdit.EditValue = fChildForm.intSelectedStaffID;
                        ContractorNameButtonEdit.EditValue = fChildForm.strSelectedStaffName;

                        LocationXTextEdit.EditValue = (double)0.00;
                        LocationYTextEdit.EditValue = (double)0.00;
                        TeamPostcodeTextEdit.EditValue = "";
                    }
                }
            }
        }
        
        private void btnOK_Click(object sender, EventArgs e)
        {
            if (LinkedToPersonTypeIDTextEdit.EditValue != null) intLinkedToPersonTypeID = Convert.ToInt32(LinkedToPersonTypeIDTextEdit.EditValue);
            if (LinkedToPersonTypeButtonEdit.EditValue != null) strLinkedToPersonType = LinkedToPersonTypeButtonEdit.EditValue.ToString();
            if (ContractorIDTextEdit.EditValue != null) intContractorID = Convert.ToInt32(ContractorIDTextEdit.EditValue);
            if (ContractorNameButtonEdit.EditValue != null) strContractorName = ContractorNameButtonEdit.EditValue.ToString();
            if (CostUnitDescriptorIDGridLookUpEdit.EditValue != null) intCostUnitDescriptorID = Convert.ToInt32(CostUnitDescriptorIDGridLookUpEdit.EditValue);
            if (SellUnitDescriptorIDGridLookUpEdit.EditValue != null) intSellUnitDescriptorID = Convert.ToInt32(SellUnitDescriptorIDGridLookUpEdit.EditValue);
            if (CostPerUnitExVatSpinEdit.EditValue != null) decCostPerUnitExVat = Convert.ToDecimal(CostPerUnitExVatSpinEdit.EditValue);           
            if (CostPerUnitVatRateSpinEdit.EditValue != null) decCostPerUnitVatRate = Convert.ToDecimal(CostPerUnitVatRateSpinEdit.EditValue);
            if (SellPerUnitExVatSpinEdit.EditValue != null) decSellPerUnitExVat = Convert.ToDecimal(SellPerUnitExVatSpinEdit.EditValue);
            if (SellPerUnitVatRateSpinEdit.EditValue != null) decSellPerUnitVatRate = Convert.ToDecimal(SellPerUnitVatRateSpinEdit.EditValue);
            if (CISValueSpinEdit.EditValue != null) decCISPercentage = Convert.ToDecimal(CISValueSpinEdit.EditValue);
            if (CISPercentageSpinEdit.EditValue != null) decCISValue = Convert.ToDecimal(CISPercentageSpinEdit.EditValue);
            if (PostcodeSiteDistanceSpinEdit.EditValue != null) decPostcodeSiteDistance = Convert.ToDecimal(PostcodeSiteDistanceSpinEdit.EditValue);
            if (LatLongSiteDistanceSpinEdit.EditValue != null) decLatLongSiteDistance = Convert.ToDecimal(LatLongSiteDistanceSpinEdit.EditValue);
            if (RemarksMemoEdit.EditValue != null) strRemarks = RemarksMemoEdit.EditValue.ToString();

            if (TeamPostcodeTextEdit.EditValue != null) strTeamPostcode = TeamPostcodeTextEdit.EditValue.ToString();
            if (LocationXTextEdit.EditValue != null) dbTeamLocationX = Convert.ToDouble(LocationXTextEdit.EditValue);
            if (LocationYTextEdit.EditValue != null) dbTeamLocationY = Convert.ToDouble(LocationYTextEdit.EditValue);
              
            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }

 





    }
}

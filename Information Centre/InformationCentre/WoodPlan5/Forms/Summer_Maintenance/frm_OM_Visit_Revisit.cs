﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraBars;
using DevExpress.XtraTab;
using DevExpress.XtraBars.Docking;

using DevExpress.Utils.Menu;  // Required to disable Column Chooser on Grids //
using DevExpress.XtraGrid.Localization;  // Required to disable Column Chooser on Grids //
using DevExpress.XtraScheduler;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_OM_Visit_Revisit : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        private string strConnectionStringREADONLY = "";
        Settings set = Settings.Default;

        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;

        BaseObjects.GridCheckMarksSelection selection1;

        public string strRecordIDs = "";
        public string _ReturnedJobIDs = "";
        public DateTime _ReturnedStartDate = DateTime.Now;
        public DateTime _ReturnedEndDate = DateTime.Now;
        public int _VisitStatusID = 15;  // Sent //
        public int _jobStatusID = 40;  // Sent //
        public string _Remarks = "";
  
        #endregion

        public frm_OM_Visit_Revisit()
        {
            InitializeComponent();
        }

        private void frm_OM_Visit_Revisit_Load(object sender, EventArgs e)
        {
            this.FormID = 500204;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            strConnectionStringREADONLY = GlobalSettings.ConnectionStringREADONLY;
            Application.DoEvents();  // Allow Form time to repaint itself //
            
            strConnectionString = this.GlobalSettings.ConnectionString;
            
            Set_Grid_Highlighter_Transparent(this.Controls);

            sp06129_OM_Jobs_Linked_to_VisitsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06405_OM_Visit_ReVisit_Available_StatusesTableAdapter.Connection.ConnectionString = strConnectionString;
            try
            {
                sp06129_OM_Jobs_Linked_to_VisitsTableAdapter.Fill(dataSet_OM_Visit.sp06129_OM_Jobs_Linked_to_Visits, (string.IsNullOrEmpty(strRecordIDs) ? "" : strRecordIDs));
                sp06405_OM_Visit_ReVisit_Available_StatusesTableAdapter.Fill(dataSet_OM_Visit.sp06405_OM_Visit_ReVisit_Available_Statuses);
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Loading Job Data.\n\nMessage = [" + ex.Message + "].\n\nPlease close the screen then try again. If the problem persists, contact Technical Support.", "Load Jobs", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            dateEditJobStartDate.DateTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, 0, 0);
            dateEditJobEndDate.DateTime = dateEditJobStartDate.DateTime.AddHours(48);
            StatusGridLookUpEdit.EditValue = 2;  // Sent //

            gridControlJob.ForceInitialize();
            // Add record selection checkboxes to popup Callout Type grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControlJob.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 65; // Make wider to allow for caption in header //
            selection1.CheckMarkColumn.Caption = "     Select";  // Spaces at start so room for checkbox in front //
            selection1.CheckMarkColumn.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far;
            selection1.CheckMarkColumn.OptionsColumn.ShowCaption = true;
            selection1.CheckMarkColumn.ToolTip = "Controls which records are selected for revisit.";
            selection1.editColumnHeader.GlyphAlignment = DevExpress.Utils.HorzAlignment.Near;
            selection1.editColumnHeader.Caption = "";
            selection1.CheckMarkColumn.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            selection1.CheckMarkColumn.VisibleIndex = 0;

            GridView view = (GridView)gridControlJob.MainView;
            view.ExpandAllGroups();
            memoEditRemarks.EditValue = "";

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
        }


        #region GridView - Job

        private void gridViewJob_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Jobs Available");
        }

        private void gridViewJob_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridViewJob_DoubleClick(object sender, EventArgs e)
        {
        }

        private void gridViewJob_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridViewJob_GotFocus(object sender, EventArgs e)
        {
        }

        private void gridViewJob_MouseDown(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_Standard(sender, e);
        }

        private void gridViewJob_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }
                
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewJob_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void gridViewJob_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);
        }

        #endregion


        private void btnOK_Click(object sender, EventArgs e)
        {
            _Remarks = memoEditRemarks.EditValue.ToString();
            GridView view = (GridView)gridControlJob.MainView;
            int intCount = selection1.SelectedCount;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more Jobs to revisit before proceeding.", "Revisit Jobs", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "CheckMarkSelection")) == 1) sb.Append(view.GetRowCellValue(i, view.Columns["JobID"]).ToString() + ',');
            }
            _ReturnedJobIDs = sb.ToString();
            _ReturnedStartDate = dateEditJobStartDate.DateTime;
            _ReturnedEndDate = dateEditJobEndDate.DateTime;

            // Check if any of the revisits have been paid and warn user if yes //
            int intPaidVisitCount = 0;
            try
            {
                DataSet_OM_VisitTableAdapters.QueriesTableAdapter GetCount = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter();
                GetCount.ChangeConnectionString(strConnectionStringREADONLY);
                intPaidVisitCount = Convert.ToInt32(GetCount.sp06539_OM_Revisit_Get_Paid_Visit_Count(_ReturnedJobIDs));
            }
            catch (Exception) { }
            if (intPaidVisitCount > 0)
            {
                XtraMessageBox.Show("You have jobs on " + (intPaidVisitCount == 1 ? "1 Visit" : Convert.ToString(intPaidVisitCount) + " Visits") + " which have already been paid.\n\n<color=red>If sending to a different team, please contact Operations Support regarding payment information.</color>", "Revisit Jobs", MessageBoxButtons.OK, MessageBoxIcon.Warning, DefaultBoolean.True);
            }

            GridLookUpEdit glue = StatusGridLookUpEdit;
            int intID = Convert.ToInt32(glue.EditValue);
            view = glue.Properties.View;
            int intFoundRow = 0;
            intFoundRow = (glue.EditValue == DBNull.Value ? GridControl.InvalidRowHandle : view.LocateByValue(0, view.Columns["ID"], Convert.ToInt32(glue.EditValue)));
            if (intFoundRow != GridControl.InvalidRowHandle)
            {
                _VisitStatusID = Convert.ToInt32(view.GetRowCellValue(intFoundRow, "VisitID"));
                _jobStatusID = Convert.ToInt32(view.GetRowCellValue(intFoundRow, "JobID"));
            }
            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }




    }
}

namespace WoodPlan5
{
    partial class frm_OM_Tender_Group_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Tender_Group_Manager));
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            this.colActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp06504OMTenderGroupManagerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Tender = new WoodPlan5.DataSet_OM_Tender();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTenderGroupID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGroupDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateCreated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colCreatedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.repositoryItemCheckEditShowActiveEmployeesOnly = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemPopupContainerEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl8 = new DevExpress.XtraGrid.GridControl();
            this.sp06505OMTendersLinkedToTenderGroupBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTenderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientReferenceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTenderDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colCreatedByStaffID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContactID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKAM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReactive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colQuoteNotRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRequestReceivedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colReturnToClientByDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCMInitialAttendanceDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuoteSubmittedToClientDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuoteAcceptedByClientDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRequiredWorkCompletedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKAMQuoteRejectionReasonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOurInternalComments = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCMComments = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientComments = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTPORequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlanningAuthorityID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTPOCheckedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeProtected = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlanningAuthorityNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlanningAuthoritySubmittedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlanningAuthorityOutcomeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlanningAuthorityOkToProceed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGCCompanyName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSectorType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddress1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToParent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSitePostcode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteLocationX1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteLocationY1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedVisitCount1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedDocumentCount1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTenderGroupDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKAMName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCMName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTenderStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactPersonName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactPersonPosition = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactPersonTitle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlanningAuthorityOutcome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlanningAuthority = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKAMQuoteRejectionReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelected = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTenderType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTenderTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditMoney = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.sp06504_OM_Tender_Group_ManagerTableAdapter = new WoodPlan5.DataSet_OM_TenderTableAdapters.sp06504_OM_Tender_Group_ManagerTableAdapter();
            this.sp06505_OM_Tenders_Linked_To_Tender_GroupTableAdapter = new WoodPlan5.DataSet_OM_TenderTableAdapters.sp06505_OM_Tenders_Linked_To_Tender_GroupTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06504OMTenderGroupManagerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Tender)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditShowActiveEmployeesOnly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06505OMTendersLinkedToTenderGroupBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMoney)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(805, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 557);
            this.barDockControlBottom.Size = new System.Drawing.Size(805, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 557);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(805, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 557);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.MaxItemId = 33;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEdit2,
            this.repositoryItemCheckEditShowActiveEmployeesOnly});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colActive
            // 
            this.colActive.Caption = "Active";
            this.colActive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colActive.FieldName = "Active";
            this.colActive.Name = "colActive";
            this.colActive.OptionsColumn.AllowEdit = false;
            this.colActive.OptionsColumn.AllowFocus = false;
            this.colActive.OptionsColumn.ReadOnly = true;
            this.colActive.Visible = true;
            this.colActive.VisibleIndex = 1;
            this.colActive.Width = 50;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp06504OMTenderGroupManagerBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "Reload Data", "reload")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEdit1});
            this.gridControl1.Size = new System.Drawing.Size(780, 296);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp06504OMTenderGroupManagerBindingSource
            // 
            this.sp06504OMTenderGroupManagerBindingSource.DataMember = "sp06504_OM_Tender_Group_Manager";
            this.sp06504OMTenderGroupManagerBindingSource.DataSource = this.dataSet_OM_Tender;
            // 
            // dataSet_OM_Tender
            // 
            this.dataSet_OM_Tender.DataSetName = "DataSet_OM_Tender";
            this.dataSet_OM_Tender.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Preview_16x16, "Preview_16x16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "Preview_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.refresh_16x16, "refresh_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "refresh_16x16");
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTenderGroupID,
            this.colGroupDescription,
            this.colCreatedByStaffID,
            this.colDateCreated,
            this.colActive,
            this.colRemarks1,
            this.colCreatedByStaffName});
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.colActive;
            gridFormatRule1.Name = "FormatActive";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Gray;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView1.FormatRules.Add(gridFormatRule1);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsFind.FindDelay = 2000;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateCreated, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colGroupDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView1_ShowingEditor);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colTenderGroupID
            // 
            this.colTenderGroupID.Caption = "Tender Group ID";
            this.colTenderGroupID.FieldName = "TenderGroupID";
            this.colTenderGroupID.Name = "colTenderGroupID";
            this.colTenderGroupID.OptionsColumn.AllowEdit = false;
            this.colTenderGroupID.OptionsColumn.AllowFocus = false;
            this.colTenderGroupID.OptionsColumn.ReadOnly = true;
            this.colTenderGroupID.Width = 99;
            // 
            // colGroupDescription
            // 
            this.colGroupDescription.Caption = "Group Description";
            this.colGroupDescription.FieldName = "GroupDescription";
            this.colGroupDescription.Name = "colGroupDescription";
            this.colGroupDescription.OptionsColumn.AllowEdit = false;
            this.colGroupDescription.OptionsColumn.AllowFocus = false;
            this.colGroupDescription.OptionsColumn.ReadOnly = true;
            this.colGroupDescription.Visible = true;
            this.colGroupDescription.VisibleIndex = 0;
            this.colGroupDescription.Width = 381;
            // 
            // colCreatedByStaffID
            // 
            this.colCreatedByStaffID.Caption = "Created By Staff ID";
            this.colCreatedByStaffID.FieldName = "CreatedByStaffID";
            this.colCreatedByStaffID.Name = "colCreatedByStaffID";
            this.colCreatedByStaffID.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffID.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffID.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffID.Width = 114;
            // 
            // colDateCreated
            // 
            this.colDateCreated.Caption = "Date Created";
            this.colDateCreated.ColumnEdit = this.repositoryItemTextEdit1;
            this.colDateCreated.FieldName = "DateCreated";
            this.colDateCreated.Name = "colDateCreated";
            this.colDateCreated.OptionsColumn.AllowEdit = false;
            this.colDateCreated.OptionsColumn.AllowFocus = false;
            this.colDateCreated.OptionsColumn.ReadOnly = true;
            this.colDateCreated.Visible = true;
            this.colDateCreated.VisibleIndex = 3;
            this.colDateCreated.Width = 97;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "g";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 4;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colCreatedByStaffName
            // 
            this.colCreatedByStaffName.Caption = "Created By";
            this.colCreatedByStaffName.FieldName = "CreatedByStaffName";
            this.colCreatedByStaffName.Name = "colCreatedByStaffName";
            this.colCreatedByStaffName.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffName.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffName.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffName.Visible = true;
            this.colCreatedByStaffName.VisibleIndex = 2;
            this.colCreatedByStaffName.Width = 125;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.dataSet_AT;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // repositoryItemCheckEditShowActiveEmployeesOnly
            // 
            this.repositoryItemCheckEditShowActiveEmployeesOnly.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.Appearance.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.AutoHeight = false;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.Caption = "";
            this.repositoryItemCheckEditShowActiveEmployeesOnly.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.Name = "repositoryItemCheckEditShowActiveEmployeesOnly";
            this.repositoryItemCheckEditShowActiveEmployeesOnly.ValueChecked = 1;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.ValueUnchecked = 0;
            // 
            // repositoryItemPopupContainerEdit2
            // 
            this.repositoryItemPopupContainerEdit2.AutoHeight = false;
            this.repositoryItemPopupContainerEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit2.Name = "repositoryItemPopupContainerEdit2";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl1.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl1.Panel1.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Tender Groups";
            this.splitContainerControl1.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl1.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel2.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl1.Panel2.Controls.Add(this.gridSplitContainer2);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "Linked Tenders";
            this.splitContainerControl1.Size = new System.Drawing.Size(805, 557);
            this.splitContainerControl1.SplitterPosition = 252;
            this.splitContainerControl1.TabIndex = 4;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(780, 296);
            this.gridSplitContainer1.TabIndex = 2;
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer2.Grid = this.gridControl8;
            this.gridSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.gridControl8);
            this.gridSplitContainer2.Size = new System.Drawing.Size(780, 249);
            this.gridSplitContainer2.TabIndex = 0;
            // 
            // gridControl8
            // 
            this.gridControl8.DataSource = this.sp06505OMTendersLinkedToTenderGroupBindingSource;
            this.gridControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl8.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl8.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl8.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Ad New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "Reload Data", "reload")});
            this.gridControl8.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl8_EmbeddedNavigator_ButtonClick);
            this.gridControl8.Location = new System.Drawing.Point(0, 0);
            this.gridControl8.MainView = this.gridView8;
            this.gridControl8.MenuManager = this.barManager1;
            this.gridControl8.Name = "gridControl8";
            this.gridControl8.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit6,
            this.repositoryItemCheckEdit2,
            this.repositoryItemTextEditMoney,
            this.repositoryItemTextEditDateTime});
            this.gridControl8.Size = new System.Drawing.Size(780, 249);
            this.gridControl8.TabIndex = 3;
            this.gridControl8.UseEmbeddedNavigator = true;
            this.gridControl8.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView8});
            // 
            // sp06505OMTendersLinkedToTenderGroupBindingSource
            // 
            this.sp06505OMTendersLinkedToTenderGroupBindingSource.DataMember = "sp06505_OM_Tenders_Linked_To_Tender_Group";
            this.sp06505OMTendersLinkedToTenderGroupBindingSource.DataSource = this.dataSet_OM_Tender;
            // 
            // gridView8
            // 
            this.gridView8.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTenderID,
            this.colClientContractID1,
            this.colSiteContractID1,
            this.colClientReferenceNumber,
            this.colStatusID1,
            this.colTenderDescription,
            this.colCreatedByStaffID1,
            this.colClientContactID1,
            this.colKAM,
            this.colCM,
            this.colWorkTypeID,
            this.gridColumn1,
            this.colReactive,
            this.colQuoteNotRequired,
            this.colRequestReceivedDate,
            this.colReturnToClientByDate,
            this.colCMInitialAttendanceDate,
            this.colQuoteSubmittedToClientDate,
            this.colQuoteAcceptedByClientDate,
            this.colRequiredWorkCompletedDate,
            this.colClientPONumber,
            this.colKAMQuoteRejectionReasonID,
            this.colOurInternalComments,
            this.colCMComments,
            this.colClientComments,
            this.colTPORequired,
            this.colPlanningAuthorityID,
            this.colTPOCheckedDate,
            this.colTreeProtected,
            this.colPlanningAuthorityNumber,
            this.colPlanningAuthoritySubmittedDate,
            this.colPlanningAuthorityOutcomeID,
            this.colPlanningAuthorityOkToProceed,
            this.colClientName1,
            this.colGCCompanyName1,
            this.colSectorType1,
            this.colSiteName1,
            this.colSiteAddress1,
            this.colLinkedToParent,
            this.colContractDescription1,
            this.colSitePostcode1,
            this.colSiteLocationX1,
            this.colSiteLocationY1,
            this.colLinkedVisitCount1,
            this.colLinkedDocumentCount1,
            this.colTenderGroupDescription,
            this.colCreatedByStaffName1,
            this.colKAMName1,
            this.colCMName1,
            this.colTenderStatus,
            this.colContactPersonName,
            this.colContactPersonPosition,
            this.colContactPersonTitle,
            this.colPlanningAuthorityOutcome,
            this.colPlanningAuthority,
            this.colJobSubType,
            this.colJobType,
            this.colKAMQuoteRejectionReason,
            this.colWorkType,
            this.colSelected,
            this.colSiteCode1,
            this.colSiteID1,
            this.colClientID1,
            this.colTenderType,
            this.colTenderTypeID});
            this.gridView8.GridControl = this.gridControl8;
            this.gridView8.GroupCount = 1;
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView8.OptionsFind.FindDelay = 2000;
            this.gridView8.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView8.OptionsLayout.StoreAppearance = true;
            this.gridView8.OptionsLayout.StoreFormatRules = true;
            this.gridView8.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView8.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView8.OptionsSelection.MultiSelect = true;
            this.gridView8.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView8.OptionsView.ColumnAutoWidth = false;
            this.gridView8.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            this.gridView8.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTenderGroupDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTenderID, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView8.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView8_CustomDrawCell);
            this.gridView8.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView8_CustomRowCellEdit);
            this.gridView8.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView8.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView8.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView8.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView8.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView8.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView8.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView8_MouseUp);
            this.gridView8.DoubleClick += new System.EventHandler(this.gridView8_DoubleClick);
            this.gridView8.GotFocus += new System.EventHandler(this.gridView8_GotFocus);
            // 
            // colTenderID
            // 
            this.colTenderID.Caption = "Tender ID";
            this.colTenderID.FieldName = "TenderID";
            this.colTenderID.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colTenderID.Name = "colTenderID";
            this.colTenderID.OptionsColumn.AllowEdit = false;
            this.colTenderID.OptionsColumn.AllowFocus = false;
            this.colTenderID.OptionsColumn.ReadOnly = true;
            this.colTenderID.Visible = true;
            this.colTenderID.VisibleIndex = 0;
            this.colTenderID.Width = 122;
            // 
            // colClientContractID1
            // 
            this.colClientContractID1.Caption = "Client Contract ID";
            this.colClientContractID1.FieldName = "ClientContractID";
            this.colClientContractID1.Name = "colClientContractID1";
            this.colClientContractID1.OptionsColumn.AllowEdit = false;
            this.colClientContractID1.OptionsColumn.AllowFocus = false;
            this.colClientContractID1.OptionsColumn.ReadOnly = true;
            this.colClientContractID1.Width = 105;
            // 
            // colSiteContractID1
            // 
            this.colSiteContractID1.Caption = "Site Contract ID";
            this.colSiteContractID1.FieldName = "SiteContractID";
            this.colSiteContractID1.Name = "colSiteContractID1";
            this.colSiteContractID1.OptionsColumn.AllowEdit = false;
            this.colSiteContractID1.OptionsColumn.AllowFocus = false;
            this.colSiteContractID1.OptionsColumn.ReadOnly = true;
            this.colSiteContractID1.Width = 96;
            // 
            // colClientReferenceNumber
            // 
            this.colClientReferenceNumber.Caption = "Client Reference #";
            this.colClientReferenceNumber.FieldName = "ClientReferenceNumber";
            this.colClientReferenceNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colClientReferenceNumber.Name = "colClientReferenceNumber";
            this.colClientReferenceNumber.OptionsColumn.AllowEdit = false;
            this.colClientReferenceNumber.OptionsColumn.AllowFocus = false;
            this.colClientReferenceNumber.OptionsColumn.ReadOnly = true;
            this.colClientReferenceNumber.Visible = true;
            this.colClientReferenceNumber.VisibleIndex = 1;
            this.colClientReferenceNumber.Width = 110;
            // 
            // colStatusID1
            // 
            this.colStatusID1.Caption = "Status ID";
            this.colStatusID1.FieldName = "StatusID";
            this.colStatusID1.Name = "colStatusID1";
            this.colStatusID1.OptionsColumn.AllowEdit = false;
            this.colStatusID1.OptionsColumn.AllowFocus = false;
            this.colStatusID1.OptionsColumn.ReadOnly = true;
            // 
            // colTenderDescription
            // 
            this.colTenderDescription.Caption = "Tender Description";
            this.colTenderDescription.ColumnEdit = this.repositoryItemMemoExEdit6;
            this.colTenderDescription.FieldName = "TenderDescription";
            this.colTenderDescription.Name = "colTenderDescription";
            this.colTenderDescription.OptionsColumn.ReadOnly = true;
            this.colTenderDescription.Visible = true;
            this.colTenderDescription.VisibleIndex = 3;
            this.colTenderDescription.Width = 224;
            // 
            // repositoryItemMemoExEdit6
            // 
            this.repositoryItemMemoExEdit6.AutoHeight = false;
            this.repositoryItemMemoExEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit6.Name = "repositoryItemMemoExEdit6";
            this.repositoryItemMemoExEdit6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit6.ShowIcon = false;
            // 
            // colCreatedByStaffID1
            // 
            this.colCreatedByStaffID1.Caption = "Created By Staff ID";
            this.colCreatedByStaffID1.FieldName = "CreatedByStaffID";
            this.colCreatedByStaffID1.Name = "colCreatedByStaffID1";
            this.colCreatedByStaffID1.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffID1.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffID1.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffID1.Width = 127;
            // 
            // colClientContactID1
            // 
            this.colClientContactID1.Caption = "Client Contact ID";
            this.colClientContactID1.FieldName = "ClientContactID";
            this.colClientContactID1.Name = "colClientContactID1";
            this.colClientContactID1.OptionsColumn.AllowEdit = false;
            this.colClientContactID1.OptionsColumn.AllowFocus = false;
            this.colClientContactID1.OptionsColumn.ReadOnly = true;
            this.colClientContactID1.Width = 101;
            // 
            // colKAM
            // 
            this.colKAM.Caption = "KAM";
            this.colKAM.FieldName = "KAM";
            this.colKAM.Name = "colKAM";
            this.colKAM.OptionsColumn.AllowEdit = false;
            this.colKAM.OptionsColumn.AllowFocus = false;
            this.colKAM.OptionsColumn.ReadOnly = true;
            this.colKAM.Width = 40;
            // 
            // colCM
            // 
            this.colCM.Caption = "CM";
            this.colCM.FieldName = "CM";
            this.colCM.Name = "colCM";
            this.colCM.OptionsColumn.AllowEdit = false;
            this.colCM.OptionsColumn.AllowFocus = false;
            this.colCM.OptionsColumn.ReadOnly = true;
            this.colCM.Width = 34;
            // 
            // colWorkTypeID
            // 
            this.colWorkTypeID.Caption = "Work Sub-Type ID";
            this.colWorkTypeID.FieldName = "WorkSubTypeID";
            this.colWorkTypeID.Name = "colWorkTypeID";
            this.colWorkTypeID.OptionsColumn.AllowEdit = false;
            this.colWorkTypeID.OptionsColumn.AllowFocus = false;
            this.colWorkTypeID.OptionsColumn.ReadOnly = true;
            this.colWorkTypeID.Width = 85;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Tender Group ID";
            this.gridColumn1.FieldName = "TenderGroupID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Width = 99;
            // 
            // colReactive
            // 
            this.colReactive.Caption = "Reactive";
            this.colReactive.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colReactive.FieldName = "Reactive";
            this.colReactive.Name = "colReactive";
            this.colReactive.OptionsColumn.AllowEdit = false;
            this.colReactive.OptionsColumn.AllowFocus = false;
            this.colReactive.OptionsColumn.ReadOnly = true;
            this.colReactive.Visible = true;
            this.colReactive.VisibleIndex = 8;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // colQuoteNotRequired
            // 
            this.colQuoteNotRequired.Caption = "Quote Not Required";
            this.colQuoteNotRequired.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colQuoteNotRequired.FieldName = "QuoteNotRequired";
            this.colQuoteNotRequired.Name = "colQuoteNotRequired";
            this.colQuoteNotRequired.OptionsColumn.AllowEdit = false;
            this.colQuoteNotRequired.OptionsColumn.AllowFocus = false;
            this.colQuoteNotRequired.OptionsColumn.ReadOnly = true;
            this.colQuoteNotRequired.Visible = true;
            this.colQuoteNotRequired.VisibleIndex = 11;
            this.colQuoteNotRequired.Width = 115;
            // 
            // colRequestReceivedDate
            // 
            this.colRequestReceivedDate.Caption = "Request Received From Client";
            this.colRequestReceivedDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colRequestReceivedDate.FieldName = "RequestReceivedDate";
            this.colRequestReceivedDate.Name = "colRequestReceivedDate";
            this.colRequestReceivedDate.OptionsColumn.AllowEdit = false;
            this.colRequestReceivedDate.OptionsColumn.AllowFocus = false;
            this.colRequestReceivedDate.OptionsColumn.ReadOnly = true;
            this.colRequestReceivedDate.Visible = true;
            this.colRequestReceivedDate.VisibleIndex = 12;
            this.colRequestReceivedDate.Width = 163;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colReturnToClientByDate
            // 
            this.colReturnToClientByDate.Caption = "Return To Client By";
            this.colReturnToClientByDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colReturnToClientByDate.FieldName = "ReturnToClientByDate";
            this.colReturnToClientByDate.Name = "colReturnToClientByDate";
            this.colReturnToClientByDate.OptionsColumn.AllowEdit = false;
            this.colReturnToClientByDate.OptionsColumn.AllowFocus = false;
            this.colReturnToClientByDate.OptionsColumn.ReadOnly = true;
            this.colReturnToClientByDate.Visible = true;
            this.colReturnToClientByDate.VisibleIndex = 13;
            this.colReturnToClientByDate.Width = 112;
            // 
            // colCMInitialAttendanceDate
            // 
            this.colCMInitialAttendanceDate.Caption = "CM Initial Attendance";
            this.colCMInitialAttendanceDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colCMInitialAttendanceDate.FieldName = "CMInitialAttendanceDate";
            this.colCMInitialAttendanceDate.Name = "colCMInitialAttendanceDate";
            this.colCMInitialAttendanceDate.OptionsColumn.AllowEdit = false;
            this.colCMInitialAttendanceDate.OptionsColumn.AllowFocus = false;
            this.colCMInitialAttendanceDate.OptionsColumn.ReadOnly = true;
            this.colCMInitialAttendanceDate.Visible = true;
            this.colCMInitialAttendanceDate.VisibleIndex = 14;
            this.colCMInitialAttendanceDate.Width = 122;
            // 
            // colQuoteSubmittedToClientDate
            // 
            this.colQuoteSubmittedToClientDate.Caption = "Quote Submitted To Client";
            this.colQuoteSubmittedToClientDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colQuoteSubmittedToClientDate.FieldName = "QuoteSubmittedToClientDate";
            this.colQuoteSubmittedToClientDate.Name = "colQuoteSubmittedToClientDate";
            this.colQuoteSubmittedToClientDate.OptionsColumn.AllowEdit = false;
            this.colQuoteSubmittedToClientDate.OptionsColumn.AllowFocus = false;
            this.colQuoteSubmittedToClientDate.OptionsColumn.ReadOnly = true;
            this.colQuoteSubmittedToClientDate.Visible = true;
            this.colQuoteSubmittedToClientDate.VisibleIndex = 16;
            this.colQuoteSubmittedToClientDate.Width = 145;
            // 
            // colQuoteAcceptedByClientDate
            // 
            this.colQuoteAcceptedByClientDate.Caption = "Quote Accepted By Client";
            this.colQuoteAcceptedByClientDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colQuoteAcceptedByClientDate.FieldName = "QuoteAcceptedByClientDate";
            this.colQuoteAcceptedByClientDate.Name = "colQuoteAcceptedByClientDate";
            this.colQuoteAcceptedByClientDate.OptionsColumn.AllowEdit = false;
            this.colQuoteAcceptedByClientDate.OptionsColumn.AllowFocus = false;
            this.colQuoteAcceptedByClientDate.OptionsColumn.ReadOnly = true;
            this.colQuoteAcceptedByClientDate.Visible = true;
            this.colQuoteAcceptedByClientDate.VisibleIndex = 17;
            this.colQuoteAcceptedByClientDate.Width = 142;
            // 
            // colRequiredWorkCompletedDate
            // 
            this.colRequiredWorkCompletedDate.Caption = "Required Work Completed By";
            this.colRequiredWorkCompletedDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colRequiredWorkCompletedDate.FieldName = "RequiredWorkCompletedDate";
            this.colRequiredWorkCompletedDate.Name = "colRequiredWorkCompletedDate";
            this.colRequiredWorkCompletedDate.OptionsColumn.AllowEdit = false;
            this.colRequiredWorkCompletedDate.OptionsColumn.AllowFocus = false;
            this.colRequiredWorkCompletedDate.OptionsColumn.ReadOnly = true;
            this.colRequiredWorkCompletedDate.Visible = true;
            this.colRequiredWorkCompletedDate.VisibleIndex = 18;
            this.colRequiredWorkCompletedDate.Width = 159;
            // 
            // colClientPONumber
            // 
            this.colClientPONumber.Caption = "Client PO #";
            this.colClientPONumber.FieldName = "ClientPONumber";
            this.colClientPONumber.Name = "colClientPONumber";
            this.colClientPONumber.OptionsColumn.AllowEdit = false;
            this.colClientPONumber.OptionsColumn.AllowFocus = false;
            this.colClientPONumber.OptionsColumn.ReadOnly = true;
            this.colClientPONumber.Visible = true;
            this.colClientPONumber.VisibleIndex = 21;
            this.colClientPONumber.Width = 91;
            // 
            // colKAMQuoteRejectionReasonID
            // 
            this.colKAMQuoteRejectionReasonID.Caption = "KAm Quote Rejection Reason ID";
            this.colKAMQuoteRejectionReasonID.FieldName = "KAMQuoteRejectionReasonID";
            this.colKAMQuoteRejectionReasonID.Name = "colKAMQuoteRejectionReasonID";
            this.colKAMQuoteRejectionReasonID.OptionsColumn.AllowEdit = false;
            this.colKAMQuoteRejectionReasonID.OptionsColumn.AllowFocus = false;
            this.colKAMQuoteRejectionReasonID.OptionsColumn.ReadOnly = true;
            this.colKAMQuoteRejectionReasonID.Width = 174;
            // 
            // colOurInternalComments
            // 
            this.colOurInternalComments.Caption = "Our Internal Comments";
            this.colOurInternalComments.ColumnEdit = this.repositoryItemMemoExEdit6;
            this.colOurInternalComments.FieldName = "OurInternalComments";
            this.colOurInternalComments.Name = "colOurInternalComments";
            this.colOurInternalComments.OptionsColumn.ReadOnly = true;
            this.colOurInternalComments.Visible = true;
            this.colOurInternalComments.VisibleIndex = 24;
            this.colOurInternalComments.Width = 131;
            // 
            // colCMComments
            // 
            this.colCMComments.Caption = "CM Comments";
            this.colCMComments.ColumnEdit = this.repositoryItemMemoExEdit6;
            this.colCMComments.FieldName = "CMComments";
            this.colCMComments.Name = "colCMComments";
            this.colCMComments.OptionsColumn.ReadOnly = true;
            this.colCMComments.Visible = true;
            this.colCMComments.VisibleIndex = 25;
            this.colCMComments.Width = 120;
            // 
            // colClientComments
            // 
            this.colClientComments.Caption = "Client Comments";
            this.colClientComments.ColumnEdit = this.repositoryItemMemoExEdit6;
            this.colClientComments.FieldName = "ClientComments";
            this.colClientComments.Name = "colClientComments";
            this.colClientComments.OptionsColumn.ReadOnly = true;
            this.colClientComments.Visible = true;
            this.colClientComments.VisibleIndex = 26;
            this.colClientComments.Width = 122;
            // 
            // colTPORequired
            // 
            this.colTPORequired.Caption = "TPO Required";
            this.colTPORequired.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colTPORequired.FieldName = "TPORequired";
            this.colTPORequired.Name = "colTPORequired";
            this.colTPORequired.OptionsColumn.AllowEdit = false;
            this.colTPORequired.OptionsColumn.AllowFocus = false;
            this.colTPORequired.OptionsColumn.ReadOnly = true;
            this.colTPORequired.Visible = true;
            this.colTPORequired.VisibleIndex = 27;
            this.colTPORequired.Width = 85;
            // 
            // colPlanningAuthorityID
            // 
            this.colPlanningAuthorityID.Caption = "Planning Authority ID";
            this.colPlanningAuthorityID.FieldName = "PlanningAuthorityID";
            this.colPlanningAuthorityID.Name = "colPlanningAuthorityID";
            this.colPlanningAuthorityID.OptionsColumn.AllowEdit = false;
            this.colPlanningAuthorityID.OptionsColumn.AllowFocus = false;
            this.colPlanningAuthorityID.OptionsColumn.ReadOnly = true;
            this.colPlanningAuthorityID.Visible = true;
            this.colPlanningAuthorityID.VisibleIndex = 28;
            this.colPlanningAuthorityID.Width = 121;
            // 
            // colTPOCheckedDate
            // 
            this.colTPOCheckedDate.Caption = "TPO Checked Date";
            this.colTPOCheckedDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colTPOCheckedDate.FieldName = "TPOCheckedDate";
            this.colTPOCheckedDate.Name = "colTPOCheckedDate";
            this.colTPOCheckedDate.OptionsColumn.AllowEdit = false;
            this.colTPOCheckedDate.OptionsColumn.AllowFocus = false;
            this.colTPOCheckedDate.OptionsColumn.ReadOnly = true;
            this.colTPOCheckedDate.Visible = true;
            this.colTPOCheckedDate.VisibleIndex = 29;
            this.colTPOCheckedDate.Width = 109;
            // 
            // colTreeProtected
            // 
            this.colTreeProtected.Caption = "Tree Protected";
            this.colTreeProtected.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colTreeProtected.FieldName = "TreeProtected";
            this.colTreeProtected.Name = "colTreeProtected";
            this.colTreeProtected.OptionsColumn.AllowEdit = false;
            this.colTreeProtected.OptionsColumn.AllowFocus = false;
            this.colTreeProtected.OptionsColumn.ReadOnly = true;
            this.colTreeProtected.Visible = true;
            this.colTreeProtected.VisibleIndex = 30;
            this.colTreeProtected.Width = 91;
            // 
            // colPlanningAuthorityNumber
            // 
            this.colPlanningAuthorityNumber.Caption = "Planning Authority #";
            this.colPlanningAuthorityNumber.FieldName = "PlanningAuthorityNumber";
            this.colPlanningAuthorityNumber.Name = "colPlanningAuthorityNumber";
            this.colPlanningAuthorityNumber.OptionsColumn.AllowEdit = false;
            this.colPlanningAuthorityNumber.OptionsColumn.AllowFocus = false;
            this.colPlanningAuthorityNumber.OptionsColumn.ReadOnly = true;
            this.colPlanningAuthorityNumber.Visible = true;
            this.colPlanningAuthorityNumber.VisibleIndex = 31;
            this.colPlanningAuthorityNumber.Width = 118;
            // 
            // colPlanningAuthoritySubmittedDate
            // 
            this.colPlanningAuthoritySubmittedDate.Caption = "Planning Submitted Date";
            this.colPlanningAuthoritySubmittedDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colPlanningAuthoritySubmittedDate.FieldName = "PlanningAuthoritySubmittedDate";
            this.colPlanningAuthoritySubmittedDate.Name = "colPlanningAuthoritySubmittedDate";
            this.colPlanningAuthoritySubmittedDate.OptionsColumn.AllowEdit = false;
            this.colPlanningAuthoritySubmittedDate.OptionsColumn.AllowFocus = false;
            this.colPlanningAuthoritySubmittedDate.OptionsColumn.ReadOnly = true;
            this.colPlanningAuthoritySubmittedDate.Visible = true;
            this.colPlanningAuthoritySubmittedDate.VisibleIndex = 32;
            this.colPlanningAuthoritySubmittedDate.Width = 136;
            // 
            // colPlanningAuthorityOutcomeID
            // 
            this.colPlanningAuthorityOutcomeID.Caption = "Planning Outcome ID";
            this.colPlanningAuthorityOutcomeID.FieldName = "PlanningAuthorityOutcomeID";
            this.colPlanningAuthorityOutcomeID.Name = "colPlanningAuthorityOutcomeID";
            this.colPlanningAuthorityOutcomeID.OptionsColumn.AllowEdit = false;
            this.colPlanningAuthorityOutcomeID.OptionsColumn.AllowFocus = false;
            this.colPlanningAuthorityOutcomeID.OptionsColumn.ReadOnly = true;
            this.colPlanningAuthorityOutcomeID.Visible = true;
            this.colPlanningAuthorityOutcomeID.VisibleIndex = 33;
            this.colPlanningAuthorityOutcomeID.Width = 119;
            // 
            // colPlanningAuthorityOkToProceed
            // 
            this.colPlanningAuthorityOkToProceed.Caption = "Planning OK To Proceed";
            this.colPlanningAuthorityOkToProceed.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colPlanningAuthorityOkToProceed.FieldName = "PlanningAuthorityOkToProceed";
            this.colPlanningAuthorityOkToProceed.Name = "colPlanningAuthorityOkToProceed";
            this.colPlanningAuthorityOkToProceed.OptionsColumn.AllowEdit = false;
            this.colPlanningAuthorityOkToProceed.OptionsColumn.AllowFocus = false;
            this.colPlanningAuthorityOkToProceed.OptionsColumn.ReadOnly = true;
            this.colPlanningAuthorityOkToProceed.Visible = true;
            this.colPlanningAuthorityOkToProceed.VisibleIndex = 34;
            this.colPlanningAuthorityOkToProceed.Width = 133;
            // 
            // colClientName1
            // 
            this.colClientName1.Caption = "Client Name";
            this.colClientName1.FieldName = "ClientName";
            this.colClientName1.Name = "colClientName1";
            this.colClientName1.OptionsColumn.AllowEdit = false;
            this.colClientName1.OptionsColumn.AllowFocus = false;
            this.colClientName1.OptionsColumn.ReadOnly = true;
            this.colClientName1.Visible = true;
            this.colClientName1.VisibleIndex = 9;
            this.colClientName1.Width = 238;
            // 
            // colGCCompanyName1
            // 
            this.colGCCompanyName1.Caption = "GC Company Name";
            this.colGCCompanyName1.FieldName = "GCCompanyName";
            this.colGCCompanyName1.Name = "colGCCompanyName1";
            this.colGCCompanyName1.OptionsColumn.AllowEdit = false;
            this.colGCCompanyName1.OptionsColumn.AllowFocus = false;
            this.colGCCompanyName1.OptionsColumn.ReadOnly = true;
            this.colGCCompanyName1.Width = 111;
            // 
            // colSectorType1
            // 
            this.colSectorType1.Caption = "Sector Type";
            this.colSectorType1.FieldName = "SectorType";
            this.colSectorType1.Name = "colSectorType1";
            this.colSectorType1.OptionsColumn.AllowEdit = false;
            this.colSectorType1.OptionsColumn.AllowFocus = false;
            this.colSectorType1.OptionsColumn.ReadOnly = true;
            this.colSectorType1.Width = 77;
            // 
            // colSiteName1
            // 
            this.colSiteName1.Caption = "Site Name";
            this.colSiteName1.FieldName = "SiteName";
            this.colSiteName1.Name = "colSiteName1";
            this.colSiteName1.OptionsColumn.AllowEdit = false;
            this.colSiteName1.OptionsColumn.AllowFocus = false;
            this.colSiteName1.OptionsColumn.ReadOnly = true;
            this.colSiteName1.Visible = true;
            this.colSiteName1.VisibleIndex = 10;
            this.colSiteName1.Width = 263;
            // 
            // colSiteAddress1
            // 
            this.colSiteAddress1.Caption = "Site Address";
            this.colSiteAddress1.FieldName = "SiteAddress";
            this.colSiteAddress1.Name = "colSiteAddress1";
            this.colSiteAddress1.OptionsColumn.ReadOnly = true;
            this.colSiteAddress1.Visible = true;
            this.colSiteAddress1.VisibleIndex = 22;
            this.colSiteAddress1.Width = 208;
            // 
            // colLinkedToParent
            // 
            this.colLinkedToParent.Caption = "Linked To Parent";
            this.colLinkedToParent.FieldName = "LinkedToParent";
            this.colLinkedToParent.Name = "colLinkedToParent";
            this.colLinkedToParent.OptionsColumn.AllowEdit = false;
            this.colLinkedToParent.OptionsColumn.AllowFocus = false;
            this.colLinkedToParent.OptionsColumn.ReadOnly = true;
            this.colLinkedToParent.Width = 276;
            // 
            // colContractDescription1
            // 
            this.colContractDescription1.Caption = "Contract Description";
            this.colContractDescription1.FieldName = "ContractDescription";
            this.colContractDescription1.Name = "colContractDescription1";
            this.colContractDescription1.OptionsColumn.AllowEdit = false;
            this.colContractDescription1.OptionsColumn.AllowFocus = false;
            this.colContractDescription1.OptionsColumn.ReadOnly = true;
            this.colContractDescription1.Width = 155;
            // 
            // colSitePostcode1
            // 
            this.colSitePostcode1.Caption = "Site Postcode";
            this.colSitePostcode1.FieldName = "SitePostcode";
            this.colSitePostcode1.Name = "colSitePostcode1";
            this.colSitePostcode1.OptionsColumn.AllowEdit = false;
            this.colSitePostcode1.OptionsColumn.AllowFocus = false;
            this.colSitePostcode1.OptionsColumn.ReadOnly = true;
            this.colSitePostcode1.Visible = true;
            this.colSitePostcode1.VisibleIndex = 23;
            this.colSitePostcode1.Width = 84;
            // 
            // colSiteLocationX1
            // 
            this.colSiteLocationX1.Caption = "Site Location X";
            this.colSiteLocationX1.FieldName = "SiteLocationX";
            this.colSiteLocationX1.Name = "colSiteLocationX1";
            this.colSiteLocationX1.OptionsColumn.AllowEdit = false;
            this.colSiteLocationX1.OptionsColumn.AllowFocus = false;
            this.colSiteLocationX1.OptionsColumn.ReadOnly = true;
            this.colSiteLocationX1.Width = 89;
            // 
            // colSiteLocationY1
            // 
            this.colSiteLocationY1.Caption = "Site Location Y";
            this.colSiteLocationY1.FieldName = "SiteLocationY";
            this.colSiteLocationY1.Name = "colSiteLocationY1";
            this.colSiteLocationY1.OptionsColumn.AllowEdit = false;
            this.colSiteLocationY1.OptionsColumn.AllowFocus = false;
            this.colSiteLocationY1.OptionsColumn.ReadOnly = true;
            this.colSiteLocationY1.Width = 89;
            // 
            // colLinkedVisitCount1
            // 
            this.colLinkedVisitCount1.Caption = "Linked Visits";
            this.colLinkedVisitCount1.FieldName = "LinkedVisitCount";
            this.colLinkedVisitCount1.Name = "colLinkedVisitCount1";
            this.colLinkedVisitCount1.OptionsColumn.ReadOnly = true;
            this.colLinkedVisitCount1.Visible = true;
            this.colLinkedVisitCount1.VisibleIndex = 35;
            this.colLinkedVisitCount1.Width = 76;
            // 
            // colLinkedDocumentCount1
            // 
            this.colLinkedDocumentCount1.Caption = "Linked Documents";
            this.colLinkedDocumentCount1.FieldName = "LinkedDocumentCount";
            this.colLinkedDocumentCount1.Name = "colLinkedDocumentCount1";
            this.colLinkedDocumentCount1.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentCount1.Visible = true;
            this.colLinkedDocumentCount1.VisibleIndex = 36;
            this.colLinkedDocumentCount1.Width = 105;
            // 
            // colTenderGroupDescription
            // 
            this.colTenderGroupDescription.Caption = "Tender Group";
            this.colTenderGroupDescription.FieldName = "TenderGroupDescription";
            this.colTenderGroupDescription.Name = "colTenderGroupDescription";
            this.colTenderGroupDescription.OptionsColumn.AllowEdit = false;
            this.colTenderGroupDescription.OptionsColumn.AllowFocus = false;
            this.colTenderGroupDescription.OptionsColumn.ReadOnly = true;
            this.colTenderGroupDescription.Visible = true;
            this.colTenderGroupDescription.VisibleIndex = 25;
            this.colTenderGroupDescription.Width = 362;
            // 
            // colCreatedByStaffName1
            // 
            this.colCreatedByStaffName1.Caption = "Created By Name";
            this.colCreatedByStaffName1.FieldName = "CreatedByStaffName";
            this.colCreatedByStaffName1.Name = "colCreatedByStaffName1";
            this.colCreatedByStaffName1.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffName1.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffName1.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffName1.Visible = true;
            this.colCreatedByStaffName1.VisibleIndex = 7;
            this.colCreatedByStaffName1.Width = 112;
            // 
            // colKAMName1
            // 
            this.colKAMName1.Caption = "KAM Name";
            this.colKAMName1.FieldName = "KAMName";
            this.colKAMName1.Name = "colKAMName1";
            this.colKAMName1.OptionsColumn.AllowEdit = false;
            this.colKAMName1.OptionsColumn.AllowFocus = false;
            this.colKAMName1.OptionsColumn.ReadOnly = true;
            this.colKAMName1.Visible = true;
            this.colKAMName1.VisibleIndex = 5;
            this.colKAMName1.Width = 116;
            // 
            // colCMName1
            // 
            this.colCMName1.Caption = "CM Name";
            this.colCMName1.FieldName = "CMName";
            this.colCMName1.Name = "colCMName1";
            this.colCMName1.OptionsColumn.AllowEdit = false;
            this.colCMName1.OptionsColumn.AllowFocus = false;
            this.colCMName1.OptionsColumn.ReadOnly = true;
            this.colCMName1.Visible = true;
            this.colCMName1.VisibleIndex = 6;
            this.colCMName1.Width = 113;
            // 
            // colTenderStatus
            // 
            this.colTenderStatus.Caption = "Tender Status";
            this.colTenderStatus.FieldName = "TenderStatus";
            this.colTenderStatus.Name = "colTenderStatus";
            this.colTenderStatus.OptionsColumn.AllowEdit = false;
            this.colTenderStatus.OptionsColumn.AllowFocus = false;
            this.colTenderStatus.OptionsColumn.ReadOnly = true;
            this.colTenderStatus.Visible = true;
            this.colTenderStatus.VisibleIndex = 4;
            this.colTenderStatus.Width = 182;
            // 
            // colContactPersonName
            // 
            this.colContactPersonName.Caption = "Client Contact Person";
            this.colContactPersonName.FieldName = "ContactPersonName";
            this.colContactPersonName.Name = "colContactPersonName";
            this.colContactPersonName.OptionsColumn.AllowEdit = false;
            this.colContactPersonName.OptionsColumn.AllowFocus = false;
            this.colContactPersonName.OptionsColumn.ReadOnly = true;
            this.colContactPersonName.Width = 123;
            // 
            // colContactPersonPosition
            // 
            this.colContactPersonPosition.Caption = "Client Contact Position";
            this.colContactPersonPosition.FieldName = "ContactPersonPosition";
            this.colContactPersonPosition.Name = "colContactPersonPosition";
            this.colContactPersonPosition.OptionsColumn.AllowEdit = false;
            this.colContactPersonPosition.OptionsColumn.AllowFocus = false;
            this.colContactPersonPosition.OptionsColumn.ReadOnly = true;
            this.colContactPersonPosition.Width = 127;
            // 
            // colContactPersonTitle
            // 
            this.colContactPersonTitle.Caption = "Client Contact Title";
            this.colContactPersonTitle.FieldName = "ContactPersonTitle";
            this.colContactPersonTitle.Name = "colContactPersonTitle";
            this.colContactPersonTitle.OptionsColumn.AllowEdit = false;
            this.colContactPersonTitle.OptionsColumn.AllowFocus = false;
            this.colContactPersonTitle.OptionsColumn.ReadOnly = true;
            this.colContactPersonTitle.Width = 110;
            // 
            // colPlanningAuthorityOutcome
            // 
            this.colPlanningAuthorityOutcome.Caption = "Planning Outcome";
            this.colPlanningAuthorityOutcome.FieldName = "PlanningAuthorityOutcome";
            this.colPlanningAuthorityOutcome.Name = "colPlanningAuthorityOutcome";
            this.colPlanningAuthorityOutcome.OptionsColumn.AllowEdit = false;
            this.colPlanningAuthorityOutcome.OptionsColumn.AllowFocus = false;
            this.colPlanningAuthorityOutcome.OptionsColumn.ReadOnly = true;
            this.colPlanningAuthorityOutcome.Width = 105;
            // 
            // colPlanningAuthority
            // 
            this.colPlanningAuthority.Caption = "Planning Authority";
            this.colPlanningAuthority.FieldName = "PlanningAuthority";
            this.colPlanningAuthority.Name = "colPlanningAuthority";
            this.colPlanningAuthority.OptionsColumn.AllowEdit = false;
            this.colPlanningAuthority.OptionsColumn.AllowFocus = false;
            this.colPlanningAuthority.OptionsColumn.ReadOnly = true;
            this.colPlanningAuthority.Width = 107;
            // 
            // colJobSubType
            // 
            this.colJobSubType.Caption = "Work Sub-Type";
            this.colJobSubType.FieldName = "JobSubType";
            this.colJobSubType.Name = "colJobSubType";
            this.colJobSubType.OptionsColumn.AllowEdit = false;
            this.colJobSubType.OptionsColumn.AllowFocus = false;
            this.colJobSubType.OptionsColumn.ReadOnly = true;
            this.colJobSubType.Visible = true;
            this.colJobSubType.VisibleIndex = 20;
            this.colJobSubType.Width = 158;
            // 
            // colJobType
            // 
            this.colJobType.Caption = "Work Type";
            this.colJobType.FieldName = "JobType";
            this.colJobType.Name = "colJobType";
            this.colJobType.OptionsColumn.AllowEdit = false;
            this.colJobType.OptionsColumn.AllowFocus = false;
            this.colJobType.OptionsColumn.ReadOnly = true;
            this.colJobType.Visible = true;
            this.colJobType.VisibleIndex = 19;
            this.colJobType.Width = 166;
            // 
            // colKAMQuoteRejectionReason
            // 
            this.colKAMQuoteRejectionReason.FieldName = "KAMQuoteRejectionReason";
            this.colKAMQuoteRejectionReason.Name = "colKAMQuoteRejectionReason";
            this.colKAMQuoteRejectionReason.OptionsColumn.AllowEdit = false;
            this.colKAMQuoteRejectionReason.OptionsColumn.AllowFocus = false;
            this.colKAMQuoteRejectionReason.OptionsColumn.ReadOnly = true;
            this.colKAMQuoteRejectionReason.Visible = true;
            this.colKAMQuoteRejectionReason.VisibleIndex = 15;
            this.colKAMQuoteRejectionReason.Width = 160;
            // 
            // colWorkType
            // 
            this.colWorkType.Caption = "Work Type : Sub-Type";
            this.colWorkType.FieldName = "WorkType";
            this.colWorkType.Name = "colWorkType";
            this.colWorkType.OptionsColumn.AllowEdit = false;
            this.colWorkType.OptionsColumn.AllowFocus = false;
            this.colWorkType.OptionsColumn.ReadOnly = true;
            // 
            // colSelected
            // 
            this.colSelected.Caption = "Selected";
            this.colSelected.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colSelected.FieldName = "Selected";
            this.colSelected.Name = "colSelected";
            this.colSelected.Width = 60;
            // 
            // colSiteCode1
            // 
            this.colSiteCode1.Caption = "Site Code";
            this.colSiteCode1.FieldName = "SiteCode";
            this.colSiteCode1.Name = "colSiteCode1";
            this.colSiteCode1.OptionsColumn.AllowEdit = false;
            this.colSiteCode1.OptionsColumn.AllowFocus = false;
            this.colSiteCode1.OptionsColumn.ReadOnly = true;
            // 
            // colSiteID1
            // 
            this.colSiteID1.Caption = "Site ID";
            this.colSiteID1.FieldName = "SiteID";
            this.colSiteID1.Name = "colSiteID1";
            this.colSiteID1.OptionsColumn.AllowEdit = false;
            this.colSiteID1.OptionsColumn.AllowFocus = false;
            this.colSiteID1.OptionsColumn.ReadOnly = true;
            this.colSiteID1.Width = 74;
            // 
            // colClientID1
            // 
            this.colClientID1.Caption = "Client ID";
            this.colClientID1.FieldName = "ClientID";
            this.colClientID1.Name = "colClientID1";
            this.colClientID1.OptionsColumn.AllowEdit = false;
            this.colClientID1.OptionsColumn.AllowFocus = false;
            this.colClientID1.OptionsColumn.ReadOnly = true;
            // 
            // colTenderType
            // 
            this.colTenderType.Caption = "Tender Type";
            this.colTenderType.FieldName = "TenderType";
            this.colTenderType.Name = "colTenderType";
            this.colTenderType.OptionsColumn.AllowEdit = false;
            this.colTenderType.OptionsColumn.AllowFocus = false;
            this.colTenderType.OptionsColumn.ReadOnly = true;
            this.colTenderType.Visible = true;
            this.colTenderType.VisibleIndex = 2;
            this.colTenderType.Width = 125;
            // 
            // colTenderTypeID
            // 
            this.colTenderTypeID.Caption = "Tender Type ID";
            this.colTenderTypeID.FieldName = "TenderTypeID";
            this.colTenderTypeID.Name = "colTenderTypeID";
            this.colTenderTypeID.OptionsColumn.AllowEdit = false;
            this.colTenderTypeID.OptionsColumn.AllowFocus = false;
            this.colTenderTypeID.OptionsColumn.ReadOnly = true;
            this.colTenderTypeID.Width = 94;
            // 
            // repositoryItemTextEditMoney
            // 
            this.repositoryItemTextEditMoney.AutoHeight = false;
            this.repositoryItemTextEditMoney.Mask.EditMask = "c";
            this.repositoryItemTextEditMoney.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditMoney.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditMoney.Name = "repositoryItemTextEditMoney";
            // 
            // gridView3
            // 
            this.gridView3.Name = "gridView3";
            // 
            // sp06504_OM_Tender_Group_ManagerTableAdapter
            // 
            this.sp06504_OM_Tender_Group_ManagerTableAdapter.ClearBeforeFill = true;
            // 
            // sp06505_OM_Tenders_Linked_To_Tender_GroupTableAdapter
            // 
            this.sp06505_OM_Tenders_Linked_To_Tender_GroupTableAdapter.ClearBeforeFill = true;
            // 
            // frm_OM_Tender_Group_Manager
            // 
            this.ClientSize = new System.Drawing.Size(805, 557);
            this.Controls.Add(this.splitContainerControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_OM_Tender_Group_Manager";
            this.Text = "Tender Group Manager";
            this.Activated += new System.EventHandler(this.frm_OM_Tender_Group_Manager_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_OM_Tender_Group_Manager_FormClosing);
            this.Load += new System.EventHandler(this.frm_OM_Tender_Group_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06504OMTenderGroupManagerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Tender)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditShowActiveEmployeesOnly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06505OMTendersLinkedToTenderGroupBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMoney)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DataSet_AT dataSet_AT;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit2;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private DevExpress.XtraGrid.GridControl gridControl8;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditShowActiveEmployeesOnly;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private System.Windows.Forms.BindingSource sp06504OMTenderGroupManagerBindingSource;
        private DataSet_OM_Tender dataSet_OM_Tender;
        private DataSet_OM_TenderTableAdapters.sp06504_OM_Tender_Group_ManagerTableAdapter sp06504_OM_Tender_Group_ManagerTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colTenderGroupID;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateCreated;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colActive;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colTenderID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientReferenceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusID1;
        private DevExpress.XtraGrid.Columns.GridColumn colTenderDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContactID1;
        private DevExpress.XtraGrid.Columns.GridColumn colKAM;
        private DevExpress.XtraGrid.Columns.GridColumn colCM;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn colReactive;
        private DevExpress.XtraGrid.Columns.GridColumn colQuoteNotRequired;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditMoney;
        private DevExpress.XtraGrid.Columns.GridColumn colRequestReceivedDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colReturnToClientByDate;
        private DevExpress.XtraGrid.Columns.GridColumn colCMInitialAttendanceDate;
        private DevExpress.XtraGrid.Columns.GridColumn colQuoteSubmittedToClientDate;
        private DevExpress.XtraGrid.Columns.GridColumn colQuoteAcceptedByClientDate;
        private DevExpress.XtraGrid.Columns.GridColumn colRequiredWorkCompletedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPONumber;
        private DevExpress.XtraGrid.Columns.GridColumn colKAMQuoteRejectionReasonID;
        private DevExpress.XtraGrid.Columns.GridColumn colOurInternalComments;
        private DevExpress.XtraGrid.Columns.GridColumn colCMComments;
        private DevExpress.XtraGrid.Columns.GridColumn colClientComments;
        private DevExpress.XtraGrid.Columns.GridColumn colTPORequired;
        private DevExpress.XtraGrid.Columns.GridColumn colPlanningAuthorityID;
        private DevExpress.XtraGrid.Columns.GridColumn colTPOCheckedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeProtected;
        private DevExpress.XtraGrid.Columns.GridColumn colPlanningAuthorityNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPlanningAuthoritySubmittedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colPlanningAuthorityOutcomeID;
        private DevExpress.XtraGrid.Columns.GridColumn colPlanningAuthorityOkToProceed;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName1;
        private DevExpress.XtraGrid.Columns.GridColumn colGCCompanyName1;
        private DevExpress.XtraGrid.Columns.GridColumn colSectorType1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddress1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToParent;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colSitePostcode1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLocationX1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLocationY1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedVisitCount1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentCount1;
        private DevExpress.XtraGrid.Columns.GridColumn colTenderGroupDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffName1;
        private DevExpress.XtraGrid.Columns.GridColumn colKAMName1;
        private DevExpress.XtraGrid.Columns.GridColumn colCMName1;
        private DevExpress.XtraGrid.Columns.GridColumn colTenderStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colContactPersonName;
        private DevExpress.XtraGrid.Columns.GridColumn colContactPersonPosition;
        private DevExpress.XtraGrid.Columns.GridColumn colContactPersonTitle;
        private DevExpress.XtraGrid.Columns.GridColumn colPlanningAuthorityOutcome;
        private DevExpress.XtraGrid.Columns.GridColumn colPlanningAuthority;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubType;
        private DevExpress.XtraGrid.Columns.GridColumn colJobType;
        private DevExpress.XtraGrid.Columns.GridColumn colKAMQuoteRejectionReason;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkType;
        private DevExpress.XtraGrid.Columns.GridColumn colSelected;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID1;
        private System.Windows.Forms.BindingSource sp06505OMTendersLinkedToTenderGroupBindingSource;
        private DataSet_OM_TenderTableAdapters.sp06505_OM_Tenders_Linked_To_Tender_GroupTableAdapter sp06505_OM_Tenders_Linked_To_Tender_GroupTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colTenderType;
        private DevExpress.XtraGrid.Columns.GridColumn colTenderTypeID;
    }
}

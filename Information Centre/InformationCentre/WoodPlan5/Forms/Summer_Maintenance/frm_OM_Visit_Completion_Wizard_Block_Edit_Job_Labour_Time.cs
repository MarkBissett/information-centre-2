﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseObjects;
using WoodPlan5.Properties;
using DevExpress.XtraEditors;
using System.IO;

namespace WoodPlan5
{
    public partial class frm_OM_Visit_Completion_Wizard_Block_Edit_Job_Labour_Time : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        
        public string strSignaturePath = "";  // Holds the first part of the path, retrieved from the System_Settings table //

        public DateTime? dtStartDateTime = null;
        public DateTime? dtEndDateTime = null;
        public decimal? decDuration = null;
        public string strRemarks = null;

        #endregion

        public frm_OM_Visit_Completion_Wizard_Block_Edit_Job_Labour_Time()
        {
            InitializeComponent();
        }

        private void frm_OM_Visit_Completion_Wizard_Block_Edit_Job_Labour_Time_Load(object sender, EventArgs e)
        {
            this.FormID = 411;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            Application.DoEvents();  // Allow Form time to repaint itself //
            
            strConnectionString = this.GlobalSettings.ConnectionString;

            dateEditStartDateTime.EditValue = null;
            dateEditEndDateTime.EditValue = null;
            DurationTextEdit.EditValue = null;
            RemarksMemoEdit.EditValue = null;

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
        }
       
        private void btnOK_Click(object sender, EventArgs e)
        {
            if (dateEditStartDateTime.EditValue != null) dtStartDateTime = Convert.ToDateTime(dateEditStartDateTime.EditValue);
            if (dateEditEndDateTime.EditValue != null) dtEndDateTime = Convert.ToDateTime(dateEditEndDateTime.EditValue);
            if (DurationTextEdit.EditValue != null) decDuration = Convert.ToDecimal(DurationTextEdit.EditValue);
            if (RemarksMemoEdit.EditValue != null) strRemarks = RemarksMemoEdit.EditValue.ToString();
          
            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }

        private void dateEditActualStartDate_EditValueChanged(object sender, EventArgs e)
        {
            DateTime? dtStart = null;
            DateTime? dtEnd = null;

            DateEdit de = (DateEdit)sender;
            try { dtStart = de.DateTime; }
            catch (Exception) { }

            try { dtEnd = Convert.ToDateTime(dateEditEndDateTime.DateTime); }
            catch (Exception) { }

            CalculateDuration(dtStart, dtEnd);
        }

        private void dateEditActualEndDate_EditValueChanged(object sender, EventArgs e)
        {
            DateTime? dtStart = null;
            DateTime? dtEnd = null;

            try { dtStart = Convert.ToDateTime(dateEditStartDateTime.DateTime); }
            catch (Exception) { }

            DateEdit de = (DateEdit)sender;
            try { dtEnd = de.DateTime; }
            catch (Exception) { }

            CalculateDuration(dtStart, dtEnd);
        }

        private void CalculateDuration(DateTime? StartDate, DateTime? EndDate)
        {
            if (StartDate <= DateTime.MinValue || StartDate == null || EndDate <= DateTime.MinValue || EndDate == null) return;
            
            // Calculate the interval between the two dates.
            TimeSpan ts = (DateTime)EndDate - (DateTime)StartDate;
            decimal Units = (decimal)ts.TotalHours;
            DurationTextEdit.EditValue = (Units < (decimal)0.00 ? (decimal)0.00 : (decimal)Units);
        }






    }
}

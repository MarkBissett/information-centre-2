namespace WoodPlan5
{
    partial class frm_OM_Waste_Document_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Waste_Document_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.AddedByTypeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.sp06441OMWasteDocumentEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Core = new WoodPlan5.DataSet_OM_Core();
            this.AddedByTypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.DocumentTypeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06443OMWasteDocumentTypesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.AddedByTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.DocumentExtensionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LicenseNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.DateAddedDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.DocumentPathButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.AddedByIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.WasteDocumentIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ItemForWasteDocumentID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAddedByTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAddedByID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForDocumentPath = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForDateAdded = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForDocumentExtension = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAddedBy = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAddedByType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForLicenseNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDocumentTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.dataSet_Common_Functionality = new WoodPlan5.DataSet_Common_Functionality();
            this.sp00224LinkedDocumentLinkedRecordTypesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp06441_OM_Waste_Document_EditTableAdapter = new WoodPlan5.DataSet_OM_CoreTableAdapters.sp06441_OM_Waste_Document_EditTableAdapter();
            this.sp06443_OM_Waste_Document_TypesTableAdapter = new WoodPlan5.DataSet_OM_CoreTableAdapters.sp06443_OM_Waste_Document_TypesTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AddedByTypeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06441OMWasteDocumentEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddedByTypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DocumentTypeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06443OMWasteDocumentTypesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddedByTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DocumentExtensionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LicenseNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateAddedDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateAddedDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DocumentPathButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddedByIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WasteDocumentIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWasteDocumentID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddedByTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddedByID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDocumentPath)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateAdded)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDocumentExtension)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddedBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddedByType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLicenseNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDocumentTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Common_Functionality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00224LinkedDocumentLinkedRecordTypesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 502);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 476);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(628, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 476);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "ID";
            this.gridColumn1.FieldName = "ID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn1.Width = 53;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.ImageOptions.Image")));
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.ImageOptions.Image")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageOptions.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageOptions.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(628, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 502);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(628, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 476);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(628, 26);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 476);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "info_16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Delete_16x16.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.AddedByTypeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AddedByTypeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.DocumentTypeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.AddedByTextEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.DocumentExtensionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LicenseNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.DateAddedDateEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.DocumentPathButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.AddedByIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.WasteDocumentIDTextEdit);
            this.dataLayoutControl1.DataSource = this.sp06441OMWasteDocumentEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForWasteDocumentID,
            this.ItemForAddedByTypeID,
            this.ItemForAddedByID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1292, 209, 450, 350);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(628, 476);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // AddedByTypeTextEdit
            // 
            this.AddedByTypeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06441OMWasteDocumentEditBindingSource, "AddedByType", true));
            this.AddedByTypeTextEdit.Location = new System.Drawing.Point(124, 263);
            this.AddedByTypeTextEdit.MenuManager = this.barManager1;
            this.AddedByTypeTextEdit.Name = "AddedByTypeTextEdit";
            this.AddedByTypeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AddedByTypeTextEdit, true);
            this.AddedByTypeTextEdit.Size = new System.Drawing.Size(468, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AddedByTypeTextEdit, optionsSpelling1);
            this.AddedByTypeTextEdit.StyleController = this.dataLayoutControl1;
            this.AddedByTypeTextEdit.TabIndex = 16;
            // 
            // sp06441OMWasteDocumentEditBindingSource
            // 
            this.sp06441OMWasteDocumentEditBindingSource.DataMember = "sp06441_OM_Waste_Document_Edit";
            this.sp06441OMWasteDocumentEditBindingSource.DataSource = this.dataSet_OM_Core;
            // 
            // dataSet_OM_Core
            // 
            this.dataSet_OM_Core.DataSetName = "DataSet_OM_Core";
            this.dataSet_OM_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // AddedByTypeIDTextEdit
            // 
            this.AddedByTypeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06441OMWasteDocumentEditBindingSource, "AddedByTypeID", true));
            this.AddedByTypeIDTextEdit.Location = new System.Drawing.Point(122, 146);
            this.AddedByTypeIDTextEdit.MenuManager = this.barManager1;
            this.AddedByTypeIDTextEdit.Name = "AddedByTypeIDTextEdit";
            this.AddedByTypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AddedByTypeIDTextEdit, true);
            this.AddedByTypeIDTextEdit.Size = new System.Drawing.Size(494, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AddedByTypeIDTextEdit, optionsSpelling2);
            this.AddedByTypeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.AddedByTypeIDTextEdit.TabIndex = 17;
            // 
            // DocumentTypeIDGridLookUpEdit
            // 
            this.DocumentTypeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06441OMWasteDocumentEditBindingSource, "DocumentTypeID", true));
            this.DocumentTypeIDGridLookUpEdit.EditValue = "";
            this.DocumentTypeIDGridLookUpEdit.Location = new System.Drawing.Point(100, 35);
            this.DocumentTypeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.DocumentTypeIDGridLookUpEdit.Name = "DocumentTypeIDGridLookUpEdit";
            this.DocumentTypeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DocumentTypeIDGridLookUpEdit.Properties.DataSource = this.sp06443OMWasteDocumentTypesBindingSource;
            this.DocumentTypeIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.DocumentTypeIDGridLookUpEdit.Properties.PopupView = this.gridLookUpEdit1View;
            this.DocumentTypeIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.DocumentTypeIDGridLookUpEdit.Size = new System.Drawing.Size(516, 20);
            this.DocumentTypeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.DocumentTypeIDGridLookUpEdit.TabIndex = 16;
            this.DocumentTypeIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.DocumentTypeIDGridLookUpEdit_Validating);
            // 
            // sp06443OMWasteDocumentTypesBindingSource
            // 
            this.sp06443OMWasteDocumentTypesBindingSource.DataMember = "sp06443_OM_Waste_Document_Types";
            this.sp06443OMWasteDocumentTypesBindingSource.DataSource = this.dataSet_OM_Core;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.gridColumn1;
            gridFormatRule1.ColumnApplyTo = this.gridColumn1;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridLookUpEdit1View.FormatRules.Add(gridFormatRule1);
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Document Type";
            this.gridColumn2.FieldName = "Description";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 220;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Order";
            this.gridColumn3.FieldName = "Order";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // AddedByTextEdit
            // 
            this.AddedByTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06441OMWasteDocumentEditBindingSource, "AddedBy", true));
            this.AddedByTextEdit.Location = new System.Drawing.Point(124, 239);
            this.AddedByTextEdit.MenuManager = this.barManager1;
            this.AddedByTextEdit.Name = "AddedByTextEdit";
            this.AddedByTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AddedByTextEdit, true);
            this.AddedByTextEdit.Size = new System.Drawing.Size(468, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AddedByTextEdit, optionsSpelling3);
            this.AddedByTextEdit.StyleController = this.dataLayoutControl1;
            this.AddedByTextEdit.TabIndex = 15;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp06441OMWasteDocumentEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(100, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(176, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 13;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // DocumentExtensionTextEdit
            // 
            this.DocumentExtensionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06441OMWasteDocumentEditBindingSource, "DocumentExtension", true));
            this.DocumentExtensionTextEdit.Location = new System.Drawing.Point(124, 191);
            this.DocumentExtensionTextEdit.MenuManager = this.barManager1;
            this.DocumentExtensionTextEdit.Name = "DocumentExtensionTextEdit";
            this.DocumentExtensionTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.DocumentExtensionTextEdit, true);
            this.DocumentExtensionTextEdit.Size = new System.Drawing.Size(468, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.DocumentExtensionTextEdit, optionsSpelling4);
            this.DocumentExtensionTextEdit.StyleController = this.dataLayoutControl1;
            this.DocumentExtensionTextEdit.TabIndex = 8;
            // 
            // LicenseNumberTextEdit
            // 
            this.LicenseNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06441OMWasteDocumentEditBindingSource, "LicenseNumber", true));
            this.LicenseNumberTextEdit.Location = new System.Drawing.Point(100, 59);
            this.LicenseNumberTextEdit.MenuManager = this.barManager1;
            this.LicenseNumberTextEdit.Name = "LicenseNumberTextEdit";
            this.LicenseNumberTextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LicenseNumberTextEdit, true);
            this.LicenseNumberTextEdit.Size = new System.Drawing.Size(516, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LicenseNumberTextEdit, optionsSpelling5);
            this.LicenseNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.LicenseNumberTextEdit.TabIndex = 9;
            this.LicenseNumberTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.LicenseNumberTextEdit_Validating);
            // 
            // DateAddedDateEdit
            // 
            this.DateAddedDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06441OMWasteDocumentEditBindingSource, "DateAdded", true));
            this.DateAddedDateEdit.EditValue = null;
            this.DateAddedDateEdit.Location = new System.Drawing.Point(124, 215);
            this.DateAddedDateEdit.MenuManager = this.barManager1;
            this.DateAddedDateEdit.Name = "DateAddedDateEdit";
            this.DateAddedDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateAddedDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DateAddedDateEdit.Properties.ReadOnly = true;
            this.DateAddedDateEdit.Size = new System.Drawing.Size(468, 20);
            this.DateAddedDateEdit.StyleController = this.dataLayoutControl1;
            this.DateAddedDateEdit.TabIndex = 11;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06441OMWasteDocumentEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, 191);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(556, 239);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling6);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 12;
            // 
            // DocumentPathButtonEdit
            // 
            this.DocumentPathButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06441OMWasteDocumentEditBindingSource, "DocumentPath", true));
            this.DocumentPathButtonEdit.Location = new System.Drawing.Point(100, 83);
            this.DocumentPathButtonEdit.MenuManager = this.barManager1;
            this.DocumentPathButtonEdit.Name = "DocumentPathButtonEdit";
            editorButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions1.Image")));
            editorButtonImageOptions1.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.DocumentPathButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose File", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Select File", "select file", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View File", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", "view file", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.DocumentPathButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.DocumentPathButtonEdit.Size = new System.Drawing.Size(516, 22);
            this.DocumentPathButtonEdit.StyleController = this.dataLayoutControl1;
            this.DocumentPathButtonEdit.TabIndex = 7;
            this.DocumentPathButtonEdit.TabStop = false;
            this.DocumentPathButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.DocumentPathButtonEdit_ButtonClick);
            this.DocumentPathButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.DocumentPathButtonEdit_Validating);
            // 
            // AddedByIDTextEdit
            // 
            this.AddedByIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06441OMWasteDocumentEditBindingSource, "AddedByID", true));
            this.AddedByIDTextEdit.EditValue = "";
            this.AddedByIDTextEdit.Location = new System.Drawing.Point(122, 122);
            this.AddedByIDTextEdit.MenuManager = this.barManager1;
            this.AddedByIDTextEdit.Name = "AddedByIDTextEdit";
            this.AddedByIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AddedByIDTextEdit, true);
            this.AddedByIDTextEdit.Size = new System.Drawing.Size(494, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AddedByIDTextEdit, optionsSpelling7);
            this.AddedByIDTextEdit.StyleController = this.dataLayoutControl1;
            this.AddedByIDTextEdit.TabIndex = 10;
            this.AddedByIDTextEdit.TabStop = false;
            // 
            // WasteDocumentIDTextEdit
            // 
            this.WasteDocumentIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06441OMWasteDocumentEditBindingSource, "WasteDocumentID", true));
            this.WasteDocumentIDTextEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.WasteDocumentIDTextEdit.Location = new System.Drawing.Point(122, 194);
            this.WasteDocumentIDTextEdit.MenuManager = this.barManager1;
            this.WasteDocumentIDTextEdit.Name = "WasteDocumentIDTextEdit";
            this.WasteDocumentIDTextEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.WasteDocumentIDTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.WasteDocumentIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.WasteDocumentIDTextEdit, true);
            this.WasteDocumentIDTextEdit.Size = new System.Drawing.Size(494, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.WasteDocumentIDTextEdit, optionsSpelling8);
            this.WasteDocumentIDTextEdit.StyleController = this.dataLayoutControl1;
            this.WasteDocumentIDTextEdit.TabIndex = 4;
            // 
            // ItemForWasteDocumentID
            // 
            this.ItemForWasteDocumentID.Control = this.WasteDocumentIDTextEdit;
            this.ItemForWasteDocumentID.CustomizationFormText = "Waste Document ID:";
            this.ItemForWasteDocumentID.Location = new System.Drawing.Point(0, 182);
            this.ItemForWasteDocumentID.Name = "ItemForWasteDocumentID";
            this.ItemForWasteDocumentID.Size = new System.Drawing.Size(608, 24);
            this.ItemForWasteDocumentID.Text = "Waste Document ID:";
            this.ItemForWasteDocumentID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForAddedByTypeID
            // 
            this.ItemForAddedByTypeID.Control = this.AddedByTypeIDTextEdit;
            this.ItemForAddedByTypeID.Location = new System.Drawing.Point(0, 134);
            this.ItemForAddedByTypeID.Name = "ItemForAddedByTypeID";
            this.ItemForAddedByTypeID.Size = new System.Drawing.Size(608, 24);
            this.ItemForAddedByTypeID.Text = "Added By Type ID:";
            this.ItemForAddedByTypeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForAddedByID
            // 
            this.ItemForAddedByID.Control = this.AddedByIDTextEdit;
            this.ItemForAddedByID.CustomizationFormText = "Added By ID:";
            this.ItemForAddedByID.Location = new System.Drawing.Point(0, 110);
            this.ItemForAddedByID.Name = "ItemForAddedByID";
            this.ItemForAddedByID.Size = new System.Drawing.Size(608, 24);
            this.ItemForAddedByID.Text = "Added By ID:";
            this.ItemForAddedByID.TextSize = new System.Drawing.Size(107, 13);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(628, 476);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForDocumentPath,
            this.emptySpaceItem1,
            this.layoutControlGroup4,
            this.emptySpaceItem2,
            this.ItemForLicenseNumber,
            this.emptySpaceItem4,
            this.emptySpaceItem5,
            this.layoutControlItem1,
            this.ItemForDocumentTypeID});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(608, 456);
            // 
            // ItemForDocumentPath
            // 
            this.ItemForDocumentPath.AllowHide = false;
            this.ItemForDocumentPath.Control = this.DocumentPathButtonEdit;
            this.ItemForDocumentPath.CustomizationFormText = "Linked Document:";
            this.ItemForDocumentPath.Location = new System.Drawing.Point(0, 71);
            this.ItemForDocumentPath.Name = "ItemForDocumentPath";
            this.ItemForDocumentPath.Size = new System.Drawing.Size(608, 26);
            this.ItemForDocumentPath.Text = "Linked Document:";
            this.ItemForDocumentPath.TextSize = new System.Drawing.Size(85, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 446);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(608, 10);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Details";
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 109);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(608, 337);
            this.layoutControlGroup4.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup5;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(584, 291);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup5,
            this.layoutControlGroup3});
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Details";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForDateAdded,
            this.emptySpaceItem3,
            this.ItemForDocumentExtension,
            this.ItemForAddedBy,
            this.ItemForAddedByType});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(560, 243);
            this.layoutControlGroup5.Text = "Details";
            // 
            // ItemForDateAdded
            // 
            this.ItemForDateAdded.Control = this.DateAddedDateEdit;
            this.ItemForDateAdded.CustomizationFormText = "Date Added:";
            this.ItemForDateAdded.Location = new System.Drawing.Point(0, 24);
            this.ItemForDateAdded.Name = "ItemForDateAdded";
            this.ItemForDateAdded.Size = new System.Drawing.Size(560, 24);
            this.ItemForDateAdded.Text = "Date Added:";
            this.ItemForDateAdded.TextSize = new System.Drawing.Size(85, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 96);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(560, 147);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForDocumentExtension
            // 
            this.ItemForDocumentExtension.Control = this.DocumentExtensionTextEdit;
            this.ItemForDocumentExtension.CustomizationFormText = "File Type:";
            this.ItemForDocumentExtension.Location = new System.Drawing.Point(0, 0);
            this.ItemForDocumentExtension.Name = "ItemForDocumentExtension";
            this.ItemForDocumentExtension.Size = new System.Drawing.Size(560, 24);
            this.ItemForDocumentExtension.Text = "File Type:";
            this.ItemForDocumentExtension.TextSize = new System.Drawing.Size(85, 13);
            // 
            // ItemForAddedBy
            // 
            this.ItemForAddedBy.Control = this.AddedByTextEdit;
            this.ItemForAddedBy.CustomizationFormText = "Added By Name:";
            this.ItemForAddedBy.Location = new System.Drawing.Point(0, 48);
            this.ItemForAddedBy.Name = "ItemForAddedBy";
            this.ItemForAddedBy.Size = new System.Drawing.Size(560, 24);
            this.ItemForAddedBy.Text = "Added By Name:";
            this.ItemForAddedBy.TextSize = new System.Drawing.Size(85, 13);
            // 
            // ItemForAddedByType
            // 
            this.ItemForAddedByType.Control = this.AddedByTypeTextEdit;
            this.ItemForAddedByType.Location = new System.Drawing.Point(0, 72);
            this.ItemForAddedByType.Name = "ItemForAddedByType";
            this.ItemForAddedByType.Size = new System.Drawing.Size(560, 24);
            this.ItemForAddedByType.Text = "Added By Type:";
            this.ItemForAddedByType.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CaptionImageOptions.Image = global::WoodPlan5.Properties.Resources.Notes_16x16;
            this.layoutControlGroup3.CustomizationFormText = "Remarks";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(560, 243);
            this.layoutControlGroup3.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(560, 243);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 97);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(0, 12);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 12);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(608, 12);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForLicenseNumber
            // 
            this.ItemForLicenseNumber.AllowHide = false;
            this.ItemForLicenseNumber.Control = this.LicenseNumberTextEdit;
            this.ItemForLicenseNumber.CustomizationFormText = "Link Description:";
            this.ItemForLicenseNumber.Location = new System.Drawing.Point(0, 47);
            this.ItemForLicenseNumber.Name = "ItemForLicenseNumber";
            this.ItemForLicenseNumber.Size = new System.Drawing.Size(608, 24);
            this.ItemForLicenseNumber.Text = "License Number:";
            this.ItemForLicenseNumber.TextSize = new System.Drawing.Size(85, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(88, 0);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(88, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(88, 23);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(268, 0);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(340, 23);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(88, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(180, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // ItemForDocumentTypeID
            // 
            this.ItemForDocumentTypeID.Control = this.DocumentTypeIDGridLookUpEdit;
            this.ItemForDocumentTypeID.Location = new System.Drawing.Point(0, 23);
            this.ItemForDocumentTypeID.Name = "ItemForDocumentTypeID";
            this.ItemForDocumentTypeID.Size = new System.Drawing.Size(608, 24);
            this.ItemForDocumentTypeID.Text = "Document Type:";
            this.ItemForDocumentTypeID.TextSize = new System.Drawing.Size(85, 13);
            // 
            // dataSet_Common_Functionality
            // 
            this.dataSet_Common_Functionality.DataSetName = "DataSet_Common_Functionality";
            this.dataSet_Common_Functionality.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00224LinkedDocumentLinkedRecordTypesBindingSource
            // 
            this.sp00224LinkedDocumentLinkedRecordTypesBindingSource.DataMember = "sp00224_Linked_Document_Linked_Record_Types";
            this.sp00224LinkedDocumentLinkedRecordTypesBindingSource.DataSource = this.dataSet_AT;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp06441_OM_Waste_Document_EditTableAdapter
            // 
            this.sp06441_OM_Waste_Document_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp06443_OM_Waste_Document_TypesTableAdapter
            // 
            this.sp06443_OM_Waste_Document_TypesTableAdapter.ClearBeforeFill = true;
            // 
            // frm_OM_Waste_Document_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(628, 532);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_OM_Waste_Document_Edit";
            this.Text = "Edit Waste Document";
            this.Activated += new System.EventHandler(this.frm_OM_Waste_Document_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_OM_Waste_Document_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_OM_Waste_Document_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.AddedByTypeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06441OMWasteDocumentEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddedByTypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DocumentTypeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06443OMWasteDocumentTypesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddedByTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DocumentExtensionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LicenseNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateAddedDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateAddedDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DocumentPathButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddedByIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WasteDocumentIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWasteDocumentID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddedByTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddedByID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDocumentPath)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateAdded)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDocumentExtension)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddedBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddedByType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLicenseNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDocumentTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Common_Functionality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00224LinkedDocumentLinkedRecordTypesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DataSet_AT dataSet_AT;
        private DevExpress.XtraEditors.TextEdit DocumentExtensionTextEdit;
        private DevExpress.XtraEditors.TextEdit LicenseNumberTextEdit;
        private DevExpress.XtraEditors.DateEdit DateAddedDateEdit;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWasteDocumentID;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDocumentPath;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDocumentExtension;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLicenseNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddedByID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDateAdded;
        private DevExpress.XtraEditors.ButtonEdit DocumentPathButtonEdit;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private System.Windows.Forms.BindingSource sp00224LinkedDocumentLinkedRecordTypesBindingSource;
        private DevExpress.XtraEditors.TextEdit AddedByTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddedBy;
        private DevExpress.XtraEditors.TextEdit AddedByIDTextEdit;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.GridLookUpEdit DocumentTypeIDGridLookUpEdit;
        private DataSet_Common_Functionality dataSet_Common_Functionality;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDocumentTypeID;
        private System.Windows.Forms.BindingSource sp06441OMWasteDocumentEditBindingSource;
        private DataSet_OM_Core dataSet_OM_Core;
        private DataSet_OM_CoreTableAdapters.sp06441_OM_Waste_Document_EditTableAdapter sp06441_OM_Waste_Document_EditTableAdapter;
        private DevExpress.XtraEditors.TextEdit WasteDocumentIDTextEdit;
        private System.Windows.Forms.BindingSource sp06443OMWasteDocumentTypesBindingSource;
        private DataSet_OM_CoreTableAdapters.sp06443_OM_Waste_Document_TypesTableAdapter sp06443_OM_Waste_Document_TypesTableAdapter;
        private DevExpress.XtraEditors.TextEdit AddedByTypeIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddedByTypeID;
        private DevExpress.XtraEditors.TextEdit AddedByTypeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddedByType;
    }
}

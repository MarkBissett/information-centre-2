using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_OM_Select_Site_Contract_Multiple : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        GridHitInfo downHitInfo = null;
        bool internalRowFocusing;

        public string strOriginalParentSelectedIDs = "";
        public string strOriginalChildSelectedIDs = "";
        public string strSelectedParentIDs = "";
        public string strSelectedChildIDs = "";
        public string strSelectedParentDescriptions = "";
        public string strSelectedChildDescriptions = "";
        public string _strExcludeIDs = "";

        public DateTime? i_dt_FromDate = null;
        public DateTime? i_dt_ToDate = null;
        private string i_str_date_range = "";

        public int _SelectedCount1 = 0;
        public int _SelectedCount2 = 0;
        BaseObjects.GridCheckMarksSelection selection1;
        BaseObjects.GridCheckMarksSelection selection2;

        #endregion

        public frm_OM_Select_Site_Contract_Multiple()
        {
            InitializeComponent();
        }

        private void frm_OM_Select_Site_Contract_Multiple_Load(object sender, EventArgs e)
        {
            this.LockThisWindow();
            this.FormID = 500151;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            sp06066_OM_Select_Client_ContractTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06117_OM_Site_Contracts_For_Client_ContractTableAdapter.Connection.ConnectionString = strConnectionString;
            
            if (i_dt_FromDate != null) dateEditFromDate.DateTime = Convert.ToDateTime(i_dt_FromDate);
            if (i_dt_ToDate != null) dateEditToDate.DateTime = Convert.ToDateTime(i_dt_ToDate);
            beiDateFilter.EditValue = PopupContainerEditDateRange_Get_Selected();

            GridView view = (GridView)gridControl1.MainView;
            gridControl1.ForceInitialize();
            LoadData();
            
            // Add record selection checkboxes to popup grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;

            Array arrayRecords = strOriginalParentSelectedIDs.Split(',');  // Single quotes because char expected for delimeter //
            view.BeginUpdate();
            int intFoundRow = 0;
            foreach (string strElement in arrayRecords)
            {
                if (strElement == "") break;
                intFoundRow = view.LocateByValue(0, view.Columns["ClientContractID"], Convert.ToInt32(strElement));
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                    view.MakeRowVisible(intFoundRow, false);
                }
            }
            view.EndUpdate();

            view = (GridView)gridControl2.MainView;
            gridControl2.ForceInitialize();
            LoadLinkedData();

            // Add record selection checkboxes to popup grid control //
            selection2 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl2.MainView);
            selection2.CheckMarkColumn.VisibleIndex = 0;
            selection2.CheckMarkColumn.Width = 30;

            arrayRecords = strOriginalChildSelectedIDs.Split(',');  // Single quotes because char expected for delimeter //
            view.BeginUpdate();
            intFoundRow = 0;
            foreach (string strElement in arrayRecords)
            {
                if (strElement == "") break;
                intFoundRow = view.LocateByValue(0, view.Columns["SiteContractID"], Convert.ToInt32(strElement));
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                    view.MakeRowVisible(intFoundRow, false);
                }
            }
            view.EndUpdate();

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            Application.DoEvents();  // Allow Form time to repaint itself //
        }

        public override void PostLoadView(object objParameter)
        {
        }

        private void LoadData()
        {
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            DateTime dtFromDate = (i_dt_FromDate == null || i_dt_FromDate == Convert.ToDateTime("01/01/0001") ? new DateTime(2001, 01, 01) : Convert.ToDateTime(i_dt_FromDate));
            DateTime dtToDate = (i_dt_ToDate == null || i_dt_ToDate == Convert.ToDateTime("01/01/0001") ? new DateTime(2900, 12, 31) : Convert.ToDateTime(i_dt_ToDate));

            sp06066_OM_Select_Client_ContractTableAdapter.Fill(dataSet_OM_Contract.sp06066_OM_Select_Client_Contract, _strExcludeIDs, dtFromDate, dtToDate);
            view.EndUpdate();
        }

        private void LoadLinkedData()
        {
            GridView view = (GridView)gridControl1.MainView;
            string strSelectedIDs = "";    // Reset any prior values first //
            if (selection1.SelectedCount <= 0) return;
            int intCount = 0;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                {
                    strSelectedIDs += Convert.ToString(view.GetRowCellValue(i, "ClientContractID")) + ",";
                    intCount++;
                }
            }

            gridControl2.MainView.BeginUpdate();
            if (intCount == 0)
            {
                this.dataSet_OM_Contract.sp06066_OM_Select_Client_Contract.Clear();
            }
            else
            {
                try
                {
                    sp06117_OM_Site_Contracts_For_Client_ContractTableAdapter.Fill(dataSet_OM_Contract.sp06117_OM_Site_Contracts_For_Client_Contract, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), 0);
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while loading the related Site Contracts.\n\nTry selecting a Client Contract again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            gridControl2.MainView.EndUpdate();       
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Client Contacts Available - Try adjusting any Date Filters and click Refresh Data button";
                    break;
                case "gridView2":
                    message = "NNo Site Contracts Available - Select one or more Client Contracts by ticking them then click Load Site Contracts button";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_FocusedRowChanged_NoGroupSelection(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
        }

        public void GridView_MouseDown_NoGroupSelection(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        #region Date Range Filter Panel

        private void btnDateRangeFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void repositoryItemPopupContainerEditDateRange_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditDateRange_Get_Selected();
        }

        private string PopupContainerEditDateRange_Get_Selected()
        {
            i_dt_FromDate = null;    // Reset any prior values first //
            i_dt_ToDate = null;  // Reset any prior values first //
            i_str_date_range = "";
            GridView view = (GridView)gridControl1.MainView;

            i_dt_FromDate = dateEditFromDate.DateTime;
            i_dt_ToDate = dateEditToDate.DateTime;
            i_str_date_range = (i_dt_FromDate == null || i_dt_FromDate == Convert.ToDateTime("01/01/0001") ? "No Start" : Convert.ToDateTime(i_dt_FromDate).ToString("dd/MM/yyyy"));
            i_str_date_range += " - " + (i_dt_ToDate == null || i_dt_ToDate == Convert.ToDateTime("01/01/0001") ? "No End" : Convert.ToDateTime(i_dt_ToDate).ToString("dd/MM/yyyy"));
            return i_str_date_range;
        }

        #endregion


        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (strSelectedChildDescriptions == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one site contract by ticking it before proceeding.", "Select Site Contract", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void GetSelectedDetails()
        {
            strSelectedParentIDs = "";    // Reset any prior values first //
            strSelectedParentDescriptions = "";  // Reset any prior values first //
            strSelectedChildIDs = "";    // Reset any prior values first //
            strSelectedChildDescriptions = "";  // Reset any prior values first //

            if (selection1.SelectedCount <= 0) return;
            GridView view = (GridView)gridControl1.MainView;
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //  
            int intCount = 0;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                {
                    strSelectedParentIDs += Convert.ToString(view.GetRowCellValue(i, "ClientContractID")) + ",";
                    if (intCount == 0)
                    {
                        strSelectedParentDescriptions = view.GetRowCellValue(i, "ClientName").ToString() + " [" + view.GetRowCellValue(i, "ContractDescription").ToString() + "]";
                    }
                    else if (intCount >= 1)
                    {
                        strSelectedParentDescriptions += ", " + view.GetRowCellValue(i, "ClientName").ToString() + " [" + view.GetRowCellValue(i, "ContractDescription").ToString() + "]";
                    }
                    intCount++;
                }
            }

            if (selection2.SelectedCount <= 0) return;
            view = (GridView)gridControl2.MainView;
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //  
            intCount = 0;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                {
                    strSelectedChildIDs += Convert.ToString(view.GetRowCellValue(i, "SiteContractID")) + ",";
                    if (intCount == 0)
                    {
                        strSelectedChildDescriptions = view.GetRowCellValue(i, "SiteName").ToString() + " [" + view.GetRowCellValue(i, "LinkedToParent").ToString() + "]";
                    }
                    else if (intCount >= 1)
                    {
                        strSelectedChildDescriptions += ", " + view.GetRowCellValue(i, "SiteName").ToString() + " [" + view.GetRowCellValue(i, "LinkedToParent").ToString() + "]";
                    }
                    intCount++;
                }
            }
        }

        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            LoadData();
        }

        private void bbiLoadSiteContracts_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            LoadLinkedData();
        }



    }
}


using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Linq;

using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;
using DevExpress.Spreadsheet;
using DevExpress.Utils;

using WoodPlan5.Properties;
using WoodPlan5.Classes.Operations;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_OM_Tender_Edit : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        private string strConnectionStringREADONLY = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Date Calculation //
        private bool ibool_FormStillLoading = true;

        private string i_str_ClientQuoteSpreadsheetExtractFolder = "";
        private string i_str_TenderRequestNotesFolder = "";

        public int intPassedInClientID = 0;
        public string strPassedInClientName = "";
        public int intPassedInClientContractID = 0;
        public string strPassedInContractDescription = "";
        public int intPassedInClientContactID = 0;
        public string strPassedInContactPersonName = "";
        public string strPassedInContactPersonPosition = "";
        public string strPassedInContactPersonTitle = "";      
        public int intPassedSiteID = 0;
        public string strPassedInSiteName = "";
        public int intPassedInSiteContractID = 0;
        public string strPassedInSiteCode = "";
        public string strPassedInSiteAddress = "";
        public string strPassedInSitePostcode = "";
        public double dblPassedInSiteLocationX = (double)0.00;
        public double dblPassedInSiteLocationY = (double)0.00;
        public int intPassedInKAMID = 0;
        public string strPassedInKAMName = "";
        public int intPassedInCMID = 0;
        public string strPassedInCMName = "";
        public string strPassedInTenderDescription = "";
        public int intPassedInReactive = -1;  // Set to this so we can see if it changed - if not we will set it to 1 by default //
        public int intPassedInTenderGroupID = 0;
        public string strPassedInTenderGroupDescription = "";
        public string strPassedInGCCompanyName = "";
        public string strPassedInSectorType = "";
        public int intPassedInTenderReactiveDaysToReturnQuote = 0;
        public int intPassedInTenderProactiveDaysToReturnQuote = 0;

        public int intPassedInWorkSubTypeID = 0;
        public int intPassedInJobTypeID = 0;
        public string strPassedInJobSubTypeDescription = "";
        public string strPassedInJobTypeDescription = "";

        public int intPassedInProposedLabourID = 0;
        public string strPassedInProposedLabour = "";
        public int intPassedInProposedLabourTypeID = 1;
        public string strPassedInProposedLabourType = "Team";
        public double strPassedInProposedLabourLatitude = (double)0.00;
        public double strPassedInProposedLabourLongitude = (double)0.00;
        public string strPassedInProposedLabourPostcode = "";

        public bool iBool_ManualStatusChange = false;

        #endregion

        public frm_OM_Tender_Edit()
        {
            InitializeComponent();
        }
        
        private void frm_OM_Tender_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 500278;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;
            strConnectionStringREADONLY = GlobalSettings.ConnectionStringREADONLY;

            sp06502_OM_Tender_Quote_Rejection_Reasons_With_BlankTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            sp06502_OM_Tender_Quote_Rejection_Reasons_With_BlankTableAdapter.Fill(dataSet_OM_Tender.sp06502_OM_Tender_Quote_Rejection_Reasons_With_Blank, 1);

            sp06503_OM_Tender_Planning_Outcomes_With_BlankTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            sp06503_OM_Tender_Planning_Outcomes_With_BlankTableAdapter.Fill(dataSet_OM_Tender.sp06503_OM_Tender_Planning_Outcomes_With_Blank, 1);

            sp06525_OM_Tender_Types_With_BlankTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            sp06525_OM_Tender_Types_With_BlankTableAdapter.Fill(dataSet_OM_Tender.sp06525_OM_Tender_Types_With_Blank, 1);

            sp01052_Core_Conservation_Statuses_With_BlankTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            sp01052_Core_Conservation_Statuses_With_BlankTableAdapter.Fill(dataSet_Common_Functionality.sp01052_Core_Conservation_Statuses_With_Blank, 1);

            sp01053_Core_Listed_Building_Statuses_With_BlankTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            sp01053_Core_Listed_Building_Statuses_With_BlankTableAdapter.Fill(dataSet_Common_Functionality.sp01053_Core_Listed_Building_Statuses_With_Blank, 1);

            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionStringREADONLY);
            try
            {
                i_str_ClientQuoteSpreadsheetExtractFolder = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_ClientQuoteSpreadsheetExtractFolder").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for the Saved Client Quote Spreadsheet Extracts (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Client Quote Spreadsheet Extract Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (!i_str_ClientQuoteSpreadsheetExtractFolder.EndsWith("\\")) i_str_ClientQuoteSpreadsheetExtractFolder += "\\";  // Add Backslash to end //

            try
            {
                i_str_TenderRequestNotesFolder = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_CMTenderRequestNotesFileFolder").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for the Tender Request Notes (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Tender Request Notes Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (!i_str_TenderRequestNotesFolder.EndsWith("\\")) i_str_TenderRequestNotesFolder += "\\";  // Add Backslash to end //

            // Populate Main Dataset //
            sp06498_OM_Tender_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();

            progressBarControl1.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            progressBarControl1.Properties.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            progressBarControl1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            //progressBarControl1.Width = 18;
            switch (strFormMode)
            {
                case "add":
                case "blockadd":
                    try
                    {
                        DataRow drNewRow = dataSet_OM_Tender.sp06498_OM_Tender_Edit.NewRow();
                        drNewRow["strMode"] = strFormMode;
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["TenderID"] = 0;
                        drNewRow["ClientID"] = intPassedInClientID;
                        drNewRow["ClientName"] = strPassedInClientName;
                        drNewRow["ClientContractID"] = intPassedInClientContractID;
                        drNewRow["ContractDescription"] = strPassedInContractDescription;
                        drNewRow["ClientContactID"] = intPassedInClientContactID;
                        drNewRow["ContactPersonName"] = strPassedInContactPersonName;
                        drNewRow["ContactPersonPosition"] = strPassedInContactPersonPosition;
                        drNewRow["ContactPersonTitle"] = strPassedInContactPersonTitle;
                        drNewRow["KAM"] = intPassedInKAMID;
                        drNewRow["KAMName"] = strPassedInKAMName;
                        
                        drNewRow["SiteID"] = intPassedSiteID;
                        drNewRow["SiteName"] = strPassedInSiteName;
                        drNewRow["SiteContractID"] = intPassedInSiteContractID;
                        drNewRow["SiteName"] = strPassedInSiteName;
                        drNewRow["SiteCode"] = strPassedInSiteCode;
                        drNewRow["SiteAddress"] = strPassedInSiteAddress;
                        drNewRow["SitePostcode"] = strPassedInSitePostcode;
                        drNewRow["SiteLocationX"] = dblPassedInSiteLocationX;
                        drNewRow["SiteLocationY"] = dblPassedInSiteLocationY;
                        drNewRow["CM"] = intPassedInCMID;
                        drNewRow["CMName"] = strPassedInCMName;

                        drNewRow["WorkSubTypeID"] = intPassedInWorkSubTypeID;
                        drNewRow["JobTypeID"] = intPassedInJobTypeID;
                        drNewRow["JobSubTypeDescription"] = strPassedInJobSubTypeDescription;
                        drNewRow["JobTypeDescription"] = strPassedInJobTypeDescription;

                        drNewRow["TenderDescription"] = strPassedInTenderDescription;
                        drNewRow["Reactive"] = (intPassedInReactive == -1 ? 1 : intPassedInReactive);
                        drNewRow["TenderGroupID"] = intPassedInTenderGroupID;
                        drNewRow["TenderGroupDescription"] = strPassedInTenderGroupDescription;
                        drNewRow["GCCompanyName"] = strPassedInGCCompanyName;
                        drNewRow["SectorType"] = strPassedInSectorType;
                        drNewRow["TenderReactiveDaysToReturnQuote"] = intPassedInTenderReactiveDaysToReturnQuote;
                        drNewRow["TenderProactiveDaysToReturnQuote"] = intPassedInTenderProactiveDaysToReturnQuote;
                        drNewRow["StatusID"] = 10;  // Tender Created //
                        drNewRow["TenderStatus"] = "Tender Created";  // Tender Created //
                        drNewRow["RequestReceivedDate"] = DateTime.Now;
                        drNewRow["CreatedByStaffID"] = GlobalSettings.UserID;
                        drNewRow["CreatedByStaffName"] = GlobalSettings.UserSurname + ": " + GlobalSettings.UserForename;
                        drNewRow["NoClientPO"] = 0;
                        drNewRow["NoClientPOAuthorisedByDirectorID"] = 0;
                        drNewRow["NoClientPOEmailedToDirector"] = 0;

                        drNewRow["StatusIssueID"] = 0;
                        drNewRow["StatusIsssue"] = "";
                        drNewRow["ProposedLabourID"] = intPassedInProposedLabourID;
                        drNewRow["ProposedLabour"] = strPassedInProposedLabour;
                        drNewRow["ProposedLabourTypeID"] = intPassedInProposedLabourTypeID;
                        drNewRow["ProposedLabourType"] = strPassedInProposedLabourType;
                        drNewRow["ProposedLabourLatitude"] = strPassedInProposedLabourLatitude;
                        drNewRow["ProposedLabourLongitude"] = strPassedInProposedLabourLongitude;
                        drNewRow["ProposedLabourPostcode"] = strPassedInProposedLabourPostcode;


                        drNewRow["QuotedLabourSell"] = (decimal)0.00;
                        drNewRow["QuotedMaterialSell"] = (decimal)0.00;
                        drNewRow["QuotedEquipmentSell"] = (decimal)0.00;
                        drNewRow["QuotedTotalSell"] = (decimal)0.00;
                        drNewRow["AcceptedLabourSell"] = (decimal)0.00;
                        drNewRow["AcceptedMaterialSell"] = (decimal)0.00;
                        drNewRow["AcceptedEquipmentSell"] = (decimal)0.00;
                        drNewRow["AcceptedTotalSell"] = (decimal)0.00;
                        drNewRow["ActualLabourCost"] = (decimal)0.00;
                        drNewRow["ActualMaterialCost"] = (decimal)0.00;
                        drNewRow["ActualEquipmentCost"] = (decimal)0.00;
                        drNewRow["ActualTotalCost"] = (decimal)0.00;
                        drNewRow["ProposedLabourCost"] = (decimal)0.00;
                        drNewRow["ProposedMaterialCost"] = (decimal)0.00;
                        drNewRow["ProposedEquipmentCost"] = (decimal)0.00;
                        drNewRow["ProposedTotalCost"] = (decimal)0.00;

                        drNewRow["ProgressBarValue"] = 10;
                        drNewRow["VisitCount"] = 0;
                        drNewRow["JobCount"] = 0;

                        drNewRow["KAMQuoteRejectionReasonID"] = 0;
                        drNewRow["KAMQuoteRejectionReason"] = "";
                        drNewRow["KAMQuoteRejectionRemarks"] = "";
                        drNewRow["QuoteNotRequired"] = 0;
                        drNewRow["ClientQuoteSpreadsheetExtractFile"] = "";

                        drNewRow["TPORequired"] = 1;
                        drNewRow["PlanningAuthorityID"] = 0;
                        drNewRow["TenderTypeID"] = 0;
                        drNewRow["PlanningProceedStaffID"] = 0;
                        drNewRow["PlanningProceedStaffName"] = "";
                        drNewRow["PlanningAuthorityOkToProceed"] = 0;

                        drNewRow["TPONumber"] = "";
                        drNewRow["ConservationStatusID"] = 0;
                        drNewRow["ListedBuildingStatusID"] = 0;
                        drNewRow["RAMS"] = 0;
                        drNewRow["TreeProtected"] = 0;
                        drNewRow["ClientPONumber"] = "";

                        drNewRow["RevisionNumber"] = "";

                        dataSet_OM_Tender.sp06498_OM_Tender_Edit.Rows.Add(drNewRow);
                        if (intPassedInTenderReactiveDaysToReturnQuote > 0 || intPassedInTenderProactiveDaysToReturnQuote > 0) CalculateReturnToClientByDate(null, intPassedInReactive);
                    }
                    catch (Exception ex)
                    {
                    }
                    break;
                case "blockedit":
                    try
                    {
                        //DataRow drNewRow;
                        DataRow drNewRow = dataSet_OM_Tender.sp06498_OM_Tender_Edit.NewRow();
                        drNewRow["strMode"] = strFormMode;
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        dataSet_OM_Tender.sp06498_OM_Tender_Edit.Rows.Add(drNewRow);
                        drNewRow.AcceptChanges();
                    }
                    catch (Exception ex)
                    {
                    }
                    break;
                case "edit":
                case "view":
                    try
                    {
                        sp06498_OM_Tender_EditTableAdapter.Fill(this.dataSet_OM_Tender.sp06498_OM_Tender_Edit, strRecordIDs, strFormMode);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    break;
            }
            dataLayoutControl1.EndUpdate();

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
        }
       
        private void frm_OM_Tender_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_OM_Tender_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strFormMode == "view") return;
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }

 
        private void Attach_EditValueChanged_To_Children(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (this.dataSet_OM_Tender.sp06498_OM_Tender_Edit.Rows.Count == 0)
            {
                _KeepWaitFormOpen = false;
                splashScreenManager.CloseWaitForm();
                XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Tender", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        ClientNameButtonEdit.Focus();

                        ClientNameButtonEdit.Properties.ReadOnly = false;
                        ClientNameButtonEdit.Properties.Buttons[0].Enabled = true;
                        //ContractDescriptionTextEdit.Properties.ReadOnly = false;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        PlanningAuthorityOutcomeIDGridLookUpEdit.Focus();

                        ClientNameButtonEdit.Properties.ReadOnly = false;
                        ClientNameButtonEdit.Properties.Buttons[0].Enabled = true;
                        //ContractDescriptionTextEdit.Properties.ReadOnly = true;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        ClientNameButtonEdit.Focus();

                        ClientNameButtonEdit.Properties.ReadOnly = false;
                        ClientNameButtonEdit.Properties.Buttons[0].Enabled = true;
                        //ContractDescriptionTextEdit.Properties.ReadOnly = false;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        PlanningAuthorityOutcomeIDGridLookUpEdit.Focus();

                        ClientNameButtonEdit.Properties.ReadOnly = true;
                        ClientNameButtonEdit.Properties.Buttons[0].Enabled = false;
                        //ContractDescriptionTextEdit.Properties.ReadOnly = true;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Viewing";
                        barStaticItemFormMode.ImageIndex = 4;  // View //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                default:
                    break;
            }
            //Set_Planning_Enabled_Status(null);
            //Set_Enabled_Based_On_Status(null);
            //SetProgressBarColour();

            SetChangesPendingLabel();
            SetEditorButtons();
            TenderStatusButtonEdit.Properties.ReadOnly = !iBool_ManualStatusChange;

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }
            ibool_ignoreValidation = true;
            ibool_FormStillLoading = false;

            Set_Planning_Enabled_Status(null);
            Set_Enabled_Based_On_Status(null);
            SetProgressBarColour();
        }

        private void SetEditorButtons()
        {
            // Get Form Permissions //
        }
        
        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_OM_Tender.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            bbiLinkedDocuments.Enabled = !(strFormMode == "blockadd" || strFormMode == "blockedit");  // Set status of Linked Documents button //

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Saving Changes...");

            this.sp06498OMTenderEditBindingSource.EndEdit();
            try
            {
                sp06498_OM_Tender_EditTableAdapter.Update(dataSet_OM_Tender);  // Insert and Update queries defined in Table Adapter //
            }
            catch (Exception ex)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
                var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
                this.strFormMode = "edit";  // Switch mode to Edit so than any subsequent changes update this record //
                if (currentRow != null)
                {
                    currentRow.strMode = "edit";
                    currentRow.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                    strNewIDs = currentRow.TenderID + ";";
                }
            }

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_OM_Tender_Manager")
                    {
                        var fParentForm = (frm_OM_Tender_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, Utils.enmFocusedGrid.Tender, strNewIDs);
                    }
                }
            }

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < this.dataSet_OM_Tender.sp06498_OM_Tender_Edit.Rows.Count; i++)
            {
                switch (this.dataSet_OM_Tender.sp06498_OM_Tender_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void bbiLinkedDocuments_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (strFormMode == "blockedit" || strFormMode == "blockadd") return;
            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            int intTenderID = currentRow.TenderID;
            if (intTenderID <= 0)
            {
                switch (XtraMessageBox.Show("Unable to open Linked Documents - you must save the Tender first.\n\nWould you like to save it now?", "View Linked Documents for Current Tender", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.No:
                        return;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) return;  // Save Failed so abort process to allow user to correct //
                        break;
                }
            }
            intTenderID = currentRow.TenderID;  // Pick up TenderID in case the first save just occurred... //
            if (intTenderID <= 0) return;

            // Drilldown to Linked Document Manager //
            int intRecordType = 71;  // Tender //
            int intRecordSubType = 0;  // Not Used //
            string strRecordDescription = "Client: " + currentRow["ClientName"].ToString() + ", Tender: " + intTenderID.ToString();
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intTenderID, strRecordDescription);
        }
        private void Linked_Document_Drill_Down(int intRecordType, int intRecordSubType, int intRecordID, string strRecordDescription)
        {
            // Drilldown to Linked Document Manager //
            string strRecordIDs = "";
            DataSet_GC_Summer_CoreTableAdapters.QueriesTableAdapter GetSetting = new DataSet_GC_Summer_CoreTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            try
            {
                strRecordIDs = GetSetting.sp06049_OM_Linked_Docs_Drilldown_Get_IDs(intRecordID, intRecordType, intRecordSubType).ToString();
            }
            catch (Exception) { }
            if (string.IsNullOrWhiteSpace(strRecordIDs)) strRecordIDs = "-1,";  // Make sure the form starts in drilldown mode [use a dummy id] //
            Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "om_linked_documents", intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                else
                {
                    Set_Planning_Enabled_Status(null);
                    Set_Enabled_Based_On_Status(null);
                    SetProgressBarColour();
                }
            }
        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void TenderTypeIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && this.strFormMode != "view" && (string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "0"))
            {
                dxErrorProvider1.SetError(TenderTypeIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(TenderTypeIDGridLookUpEdit, "");
            }
        }
        
        private void ClientNameButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            
            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;
            
            if (e.Button.Tag.ToString() == "choose")
            {
                int intClientID = (string.IsNullOrEmpty(currentRow.ClientID.ToString()) ? 0 : Convert.ToInt32(currentRow.ClientID));
                var fChildForm = new frm_EP_Select_Client();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalClientID = intClientID;
                fChildForm.intFilterSummerMaintenanceClient = 1;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    if (intClientID != fChildForm.intSelectedClientID && fChildForm.intSelectedClientID > 0)
                    {
                        Get_Client_Contract(fChildForm.intSelectedClientID, fChildForm.strSelectedClientName);
                     }
                }
            }
            if (e.Button.Tag.ToString() == "add")
            {
                var fChildForm = new frm_Core_Add_Client_Or_Site_Check_Exists();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.strFindWhat = "client";
                DialogResult dialogResult = fChildForm.ShowDialog();
                switch (dialogResult)
                {
                    case DialogResult.Cancel:
                        {
                            return;
                        }
                    case DialogResult.OK:  // User has selected a found match //
                        {
                            int intClientID = fChildForm.intSelectedID1;
                            string strClientName = fChildForm.strSelectedDescription1;
                            if (intClientID <= 0) return;
                            Get_Client_Contract(fChildForm.intSelectedID1, fChildForm.strSelectedDescription1);
                        }
                        break;
                    case DialogResult.Yes:  // User has selected Add New Client - Add screen will call a method on this form to write the details once the Add screen is closed //
                        {
                            var fProgress = new frmProgress(10);
                            this.AddOwnedForm(fProgress);
                            fProgress.Show();  // ***** Closed in PostOpen event ***** //
                            Application.DoEvents();

                            var fChildForm2 = new frm_Core_Client_Edit();
                            fChildForm2.MdiParent = this.MdiParent;
                            fChildForm2.GlobalSettings = this.GlobalSettings;
                            fChildForm2.strRecordIDs = "";
                            fChildForm2.strFormMode = "add";
                            fChildForm2.strCaller = this.Name;
                            fChildForm2.intRecordCount = 0;
                            fChildForm2.FormPermissions = this.FormPermissions;
                            fChildForm2.fProgress = fProgress;
                            fChildForm2.intMaxOrder = 0;
                            fChildForm2.frm_Tender_Edit_Screen_To_Update = this;
                            fChildForm2.Show();
                            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                            if (method != null) method.Invoke(fChildForm2, new object[] { null });
                        }
                        break;
                }
            }
        }
        private void ClientNameButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(ClientNameButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(ClientNameButtonEdit, "");
            }
        }
        private void Get_Client_Contract(int intSelectedClientID, string strSelectedClientName)
        {
            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            // Pick up Extra Works Contract - if one doesn't exist, create it //
            int intContractID = 0;
            int intTenderReactiveDaysToReturnQuote = 0;
            int intTenderProactiveDaysToReturnQuote = 0;
            string strGCCompanyName = "";
            string strSectorType = "";
            string strContractDescription = "";

            int intNewContractCreated = 0;
            int intDefaultPersonResponsibilityCount = 0;
            int intDefaultPersonResponsibilityID = 0;
            string strDefaultPersonResponsibilityName = "";
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                DataSet ds = new DataSet("NewDataSet");

                using (var conn = new SqlConnection(strConnectionString))
                {
                    conn.Open();

                    using (var cmd2 = new SqlCommand())
                    {
                        cmd2.CommandText = "sp06500_OM_Tender_Get_Extra_Works_ContractID";
                        cmd2.CommandType = CommandType.StoredProcedure;
                        cmd2.Parameters.Add(new SqlParameter("@Level", 1));
                        cmd2.Parameters.Add(new SqlParameter("@RecordID", intSelectedClientID));
                        cmd2.Connection = conn;
                        sda = new SqlDataAdapter(cmd2);
                        sda.Fill(ds, "Table");
                    }
                    conn.Close();
                    if (ds.Tables[0].Rows.Count == 1)
                    {
                        DataRow dr = ds.Tables[0].Rows[0];
                        intContractID = Convert.ToInt32(dr["ID"]);
                        intTenderReactiveDaysToReturnQuote = Convert.ToInt32(dr["TenderReactiveDaysToReturnQuote"]);
                        intTenderProactiveDaysToReturnQuote = Convert.ToInt32(dr["TenderProactiveDaysToReturnQuote"]);
                        strGCCompanyName = dr["GCCompanyName"].ToString();
                        strSectorType = dr["SectorType"].ToString();
                        strContractDescription = dr["ContractDescription"].ToString();
                        intNewContractCreated = Convert.ToInt32(dr["NewContractCreated"]);
                        intDefaultPersonResponsibilityCount = Convert.ToInt32(dr["DefaultPersonResponsibilityCount"]);
                        intDefaultPersonResponsibilityID = Convert.ToInt32(dr["DefaultPersonResponsibilityID"]);
                        strDefaultPersonResponsibilityName = dr["DefaultPersonResponsibilityName"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Unable to select Client - no Extra Works Client Contract found and unable to create one. Please contact Technical Support for further help.", "Select Client", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (intContractID <= 0)  // No client Contract found or created so abort //
            {
                XtraMessageBox.Show("Unable to select Client - no Extra Works Client Contract found and unable to create one. Please contact Technical Support for further help.", "Select Client", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            currentRow.ClientContractID = intContractID;
            currentRow.ContractDescription = strContractDescription;
            currentRow.GCCompanyName = strGCCompanyName;
            currentRow.SectorType = strSectorType;
            currentRow.TenderReactiveDaysToReturnQuote = intTenderReactiveDaysToReturnQuote;
            currentRow.TenderProactiveDaysToReturnQuote = intTenderProactiveDaysToReturnQuote;

            currentRow.ClientID = intSelectedClientID;
            currentRow.ClientName = strSelectedClientName;

            // Clear Selected Site //
            currentRow.SiteContractID = 0;
            currentRow.SiteID = 0;
            currentRow.SiteName = "";
            currentRow.SiteCode = "";
            currentRow.SiteAddress = "";
            currentRow.SitePostcode = "";
            currentRow.SiteLocationX = (double)0.00;
            currentRow.SiteLocationY = (double)0.00;

            // Clear Contact Person //
            currentRow.ClientContactID = 0;
            currentRow.ContactPersonName = "";
            currentRow.ContactPersonPosition = "";
            currentRow.ContactPersonTitle = "";
            currentRow.ClientContactTelephone = "";
            currentRow.ClientContactMobile = "";
            currentRow.ClientContactEmail = "";

            bool boolKAMSet = false;
            if (intNewContractCreated == 1 || intDefaultPersonResponsibilityCount <= 0)  // Optionally Add Default KAM to client contract and link to tender //
            {
                string strMessage = (intNewContractCreated == 1 ? "Do you wish to Add a default KAM for the Newly Generated Extra Works Client Contract?" : "No Default KAM has been set up for the Selected Client Contract - Would you like to add one?");
                
                if (XtraMessageBox.Show(strMessage, "Add Default KAM", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    string strClientName = currentRow.ClientName;
                    var fChildForm = new frm_OM_Tender_Add_Default_Person_Responsibility();
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm._PassedInIncludeBlank = 1;
                    fChildForm._PassedInStaffID = 0;
                    fChildForm._PassedInResponsibilityDescription = "KAM";
                    fChildForm._PassedInAddToDescription = "Client: " + (string.IsNullOrEmpty(strClientName) ? "Unknown" : strClientName) + ", Client Contract:  " + (string.IsNullOrEmpty(strContractDescription) ? "Unknown" : strContractDescription);

                    int intResponsibleForRecordID = intContractID;
                    int intResponsibleForRecordTypeID = 2;  // 1 = Site Contract, 2 = Client Contract, 3 = Visit //
                    int intResponsibilityTypeID = 31;  // 31: KAM //;
                    int intPersonTypeID = 0;  // 0 = Staff, 1 = Client //

                    if (fChildForm.ShowDialog() == DialogResult.OK)
                    {
                        // Add Person Responsibility to new Contract //
                        try
                        {
                            using (var GetDefaultPersonResponsibility = new DataSet_OM_TenderTableAdapters.QueriesTableAdapter())
                            {
                                GetDefaultPersonResponsibility.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    GetDefaultPersonResponsibility.sp06512_OM_Tender_Add_Default_Person_Responsibility(fChildForm._SelectedStaffID, intResponsibleForRecordID, intResponsibleForRecordTypeID, intResponsibilityTypeID, intPersonTypeID);
                                }
                                catch (Exception) { }
                            }
                        }
                        catch (Exception ex)
                        {
                            XtraMessageBox.Show("Unable to Add Default KAM - an error occurred.\n\n" + ex.Message, "Add Default KAM", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }

                        // Link Person to Tender //
                        currentRow["KAM"] = fChildForm._SelectedStaffID;
                        currentRow["KAMName"] = fChildForm._SelectedSurnameForename;
                        boolKAMSet = true;
                    }
                }
            }
            else if (intDefaultPersonResponsibilityCount > 0 && intDefaultPersonResponsibilityID != 0)  // Update KAM Details //
            {
                currentRow["KAM"] = intDefaultPersonResponsibilityID;
                currentRow["KAMName"] = strDefaultPersonResponsibilityName;
                boolKAMSet = true;
            }
            if (boolKAMSet) dxErrorProvider1.SetError(KAMNameButtonEdit, "");
            
            sp06498OMTenderEditBindingSource.EndEdit();
            CalculateReturnToClientByDate(null, null);
        }
        public void SetTenderClientFromAddScreen(int ClientID, string ClientName)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            Get_Client_Contract(ClientID, ClientName);
        }  

        private void SiteNameButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            if (e.Button.Tag.ToString() == "choose")
            {
                int intClientID = 0;
                string strClientName = "";
                try 
                {
                    intClientID = (string.IsNullOrEmpty(currentRow.ClientID.ToString()) ? 0 : Convert.ToInt32(currentRow.ClientID));
                    strClientName = (string.IsNullOrEmpty(currentRow.ClientName.ToString()) ? "" : currentRow.ClientName);
                }
                catch (Exception) { };
                if (intClientID == 0)
                {
                    XtraMessageBox.Show("Please select the Client before attempting to select the Site.", "Select Site", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                int intSiteID = 0;
                try
                {
                    intSiteID = (string.IsNullOrEmpty(currentRow.SiteID.ToString()) ? 0 : Convert.ToInt32(currentRow.SiteID));
                }
                catch (Exception) { };

                var fChildForm = new frm_Core_Select_Site_For_Single_Client();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.strPassedInClientIDsFilter = intClientID.ToString() + ",";
                fChildForm.strPassedInClientNameFilter = strClientName;
                fChildForm.intOriginalSiteID = intSiteID;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    if (intSiteID != fChildForm.intSelectedSiteID)
                    {
                        // Pick up Extra Works Contract - if one doesn't exist, create it //
                        Get_Site_Contract(fChildForm.intSelectedSiteID, fChildForm.strSelectedSiteName, fChildForm.strSelectedSiteCode, fChildForm.strSelectedSiteAddress, fChildForm.strSelectedSitePostcode, fChildForm.dblSelectedSiteLocationX, fChildForm.dblSelectedSiteLocationY);
                    }
                }
            }
            if (e.Button.Tag.ToString() == "add")
            {                
                int intClientID = 0;
                try 
                {
                    intClientID = (string.IsNullOrEmpty(currentRow.ClientID.ToString()) ? 0 : Convert.ToInt32(currentRow.ClientID));
                }
                catch (Exception) { };
                
                if (intClientID == 0)
                {
                    XtraMessageBox.Show("Please select the Client before attempting to add a new Site.", "Add New Site", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                
                string strClientName = (string.IsNullOrEmpty(currentRow.ClientName) ? "" : currentRow.ClientName);

                var fChildForm = new frm_Core_Add_Client_Or_Site_Check_Exists();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.strFindWhat = "site";
                fChildForm.strPassedInFilterClientIDs = intClientID.ToString() + ",";
                fChildForm.strPassedInFilterClientDescription = strClientName;
                DialogResult dialogResult = fChildForm.ShowDialog();

                switch (dialogResult)
                {
                    case DialogResult.Cancel:
                        {
                            return;
                        }
                    case DialogResult.OK:  // User has selected a found match //
                        {
                            int intSiteID = fChildForm.intSelectedID1;
                            if (intClientID <= 0) return;
                            Get_Site_Contract(intSiteID, fChildForm.strSelectedDescription1, fChildForm.strSelectedDescription3, fChildForm.strSelectedDescription4, fChildForm.strSelectedDescription5, fChildForm.dblSelectedDouble1, fChildForm.dblSelectedDouble2);
                        }
                        break;
                    case DialogResult.Yes:  // User has selected Add New Site - Add screen will call a method on this form to write the details once the Add screen is closed //
                        {
                            var fProgress = new frmProgress(10);
                            this.AddOwnedForm(fProgress);
                            fProgress.Show();  // ***** Closed in PostOpen event ***** //
                            Application.DoEvents();

                            var fChildForm2 = new frm_Core_Site_Edit();
                            fChildForm2.MdiParent = this.MdiParent;
                            fChildForm2.GlobalSettings = this.GlobalSettings;
                            fChildForm2.strRecordIDs = "";
                            fChildForm2.strFormMode = "add";
                            fChildForm2.strCaller = this.Name;
                            fChildForm2.intRecordCount = 0;
                            fChildForm2.FormPermissions = this.FormPermissions;
                            fChildForm2.fProgress = fProgress;
                            fChildForm2.intMaxOrder = 0;
                            fChildForm2.intLinkedToRecordID = intClientID;

                            fChildForm2.frm_Tender_Edit_Screen_To_Update = this;
                            fChildForm2.AllowClientChange = false;

                            fChildForm2.Show();
                            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                            if (method != null) method.Invoke(fChildForm2, new object[] { null });
                        }
                        break;
                }
            }
            else if (e.Button.Tag.ToString() == "clear")
            {
                currentRow.SiteContractID = 0;
                currentRow.SiteID = 0;
                currentRow.SiteName = "";
                currentRow.SiteCode = "";
                currentRow.SiteAddress = "";
                currentRow.SitePostcode = "";
                currentRow.SiteLocationX = (double)0.00;
                currentRow.SiteLocationY = (double)0.00;
                sp06498OMTenderEditBindingSource.EndEdit();
            }
        }
        private void Get_Site_Contract(int intSelectedSiteID, string strSelectedSiteName, string strSelectedSiteCode, string strSelectedSiteAddress, string strSelectedSitePostcode, double dblSelectedSiteLocationX, double dblSelectedSiteLocationY)
        {
            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            int intContractID = 0;
            int intNewContractCreated = 0;
            int intDefaultPersonResponsibilityCount = 0;
            int intDefaultPersonResponsibilityID = 0;
            string strDefaultPersonResponsibilityName = "";
            
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                DataSet ds = new DataSet("NewDataSet");

                using (var conn = new SqlConnection(strConnectionString))
                {
                    conn.Open();

                    using (var cmd2 = new SqlCommand())
                    {
                        cmd2.CommandText = "sp06500_OM_Tender_Get_Extra_Works_ContractID";
                        cmd2.CommandType = CommandType.StoredProcedure;
                        cmd2.Parameters.Add(new SqlParameter("@Level", 2));
                        cmd2.Parameters.Add(new SqlParameter("@RecordID", intSelectedSiteID));
                        cmd2.Connection = conn;
                        sda = new SqlDataAdapter(cmd2);
                        sda.Fill(ds, "Table");
                    }
                    conn.Close();
                    if (ds.Tables[0].Rows.Count == 1)
                    {
                        DataRow dr = ds.Tables[0].Rows[0];
                        intContractID = Convert.ToInt32(dr["ID"]);
                        intNewContractCreated = Convert.ToInt32(dr["NewContractCreated"]);
                        intDefaultPersonResponsibilityCount = Convert.ToInt32(dr["DefaultPersonResponsibilityCount"]);
                        intDefaultPersonResponsibilityID = Convert.ToInt32(dr["DefaultPersonResponsibilityID"]);
                        strDefaultPersonResponsibilityName = dr["DefaultPersonResponsibilityName"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Unable to select Site - no Extra Works Site Contract found and unable to create one. Please contact Technical Support for further help.", "Select Site", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (intContractID <= 0)  // No client Contract found or created so abort //
            {
                XtraMessageBox.Show("Unable to select Site - no Extra Works Site Contract found and unable to create one. Please contact Technical Support for further help.", "Select Site", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            currentRow.SiteContractID = intContractID;

            currentRow.SiteID = intSelectedSiteID;
            currentRow.SiteName = strSelectedSiteName;
            currentRow.SiteCode = strSelectedSiteCode;
            currentRow.SiteAddress = strSelectedSiteAddress;
            currentRow.SitePostcode = strSelectedSitePostcode;
            currentRow.SiteLocationX = dblSelectedSiteLocationX;
            currentRow.SiteLocationY = dblSelectedSiteLocationY;

            string strSiteName = currentRow.SiteName;
            bool boolCMSet = false;
            if (intNewContractCreated == 1 || intDefaultPersonResponsibilityCount <= 0)  // Optionally Add Default CM to client contract and link to tender //
            {
                string strMessage = (intNewContractCreated == 1 ? "Do you wish to Add a default CM for the Newly Generated Extra Works Client Contract?" : "No Default CM has been set up for the Selected Client Contract - Would you like to add one?");

                if (XtraMessageBox.Show(strMessage, "Add Default CM", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    string strClientName = currentRow.ClientName;
                    var fChildForm = new frm_OM_Tender_Add_Default_Person_Responsibility();
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm._PassedInIncludeBlank = 1;
                    fChildForm._PassedInStaffID = 0;
                    fChildForm._PassedInResponsibilityDescription = "CM";
                    fChildForm._PassedInAddToDescription = "Client: " + (string.IsNullOrEmpty(strClientName) ? "Unknown" : strClientName) + ", Site:  " + (string.IsNullOrEmpty(strSiteName) ? "Unknown" : strSiteName) + " [Extra Works]";

                    int intResponsibleForRecordID = intContractID;
                    int intResponsibleForRecordTypeID = 1;  // 1 = Site Contract, 2 = Client Contract, 3 = Visit //
                    int intResponsibilityTypeID = 21;  // 31: CM //;
                    int intPersonTypeID = 0;  // 0 = Staff, 1 = Client //

                    if (fChildForm.ShowDialog() == DialogResult.OK)
                    {
                        // Add Person Responsibility to new Contract //
                        try
                        {
                            using (var GetDefaultPersonResponsibility = new DataSet_OM_TenderTableAdapters.QueriesTableAdapter())
                            {
                                GetDefaultPersonResponsibility.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    GetDefaultPersonResponsibility.sp06512_OM_Tender_Add_Default_Person_Responsibility(fChildForm._SelectedStaffID, intResponsibleForRecordID, intResponsibleForRecordTypeID, intResponsibilityTypeID, intPersonTypeID);
                                }
                                catch (Exception) { }
                            }
                        }
                        catch (Exception ex)
                        {
                            XtraMessageBox.Show("Unable to Add Default CM - an error occurred.\n\n" + ex.Message, "Add Default CM", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }

                        // Link Person to Tender //
                        currentRow["CM"] = fChildForm._SelectedStaffID;
                        currentRow["CMName"] = fChildForm._SelectedSurnameForename;
                        boolCMSet = true;
                    }
                }
            }
            else if (intDefaultPersonResponsibilityCount > 0 && intDefaultPersonResponsibilityID != 0)  // Update CM Details //
            {
                currentRow["CM"] = intDefaultPersonResponsibilityID;
                currentRow["CMName"] = strDefaultPersonResponsibilityName;
                boolCMSet = true;
            }
            if (boolCMSet)  dxErrorProvider1.SetError(CMNameButtonEdit, "");

            sp06498OMTenderEditBindingSource.EndEdit();
        }
        public void SetTenderSiteFromAddScreen(int SiteID, string SiteName, string SiteCode, string SiteAddress, string SitePostcode, double SiteLocationX, double SiteLocationY)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            Get_Site_Contract(SiteID, SiteName, SiteCode, SiteAddress, SitePostcode, SiteLocationX, SiteLocationY);
        }

        private void SiteCodeButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            int intClientID = 0;
            try
            {
                intClientID = (string.IsNullOrEmpty(currentRow.ClientID.ToString()) ? 0 : Convert.ToInt32(currentRow.ClientID));
            }
            catch (Exception) { };

            int intSiteID = 0;
            try
            {
                intSiteID = (string.IsNullOrEmpty(currentRow.SiteID.ToString()) ? 0 : Convert.ToInt32(currentRow.SiteID));
            }
            catch (Exception) { };
            if (intSiteID == 0)
            {
                XtraMessageBox.Show("Please select the Site before attempting to view it on the map.", "View Site On Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            var fChildForm = new frm_OM_Visit_Mapping_View();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.i_str_PassedInVisitIDs = "";
            fChildForm.i_str_PassedInSiteIDs = intSiteID.ToString() + ",";
            fChildForm.i_str_PassedInClientIDs = intClientID.ToString() + ",";
            fChildForm.i_str_selected_Site_descriptions = (string.IsNullOrEmpty(currentRow.SiteName) ? "" : currentRow.SiteName);
            fChildForm.i_bool_LoadVisitsInMapOnStart = false;
            fChildForm.i_bool_LoadSitesInMapOnStart = true;
            fChildForm.strFormMode = "view";
            fChildForm.FormPermissions = this.FormPermissions;
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();
            fChildForm.Show();

            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
        }

        private void RequestReceivedDateDateEdit_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void RequestReceivedDate_Validating(object sender, CancelEventArgs e)
        {
            DateEdit de = (DateEdit)sender;
            if (this.strFormMode != "blockedit" && (de.EditValue == null || string.IsNullOrEmpty(de.EditValue.ToString())))
            {
                dxErrorProvider1.SetError(RequestReceivedDateDateEdit, "Select\\enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(RequestReceivedDateDateEdit, "");
            }
        }
        private void RequestReceivedDateDateEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;
            if (this.strFormMode == "blockedit") return;

            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            DateTime? dtStart = null;
            DateEdit de = (DateEdit)sender;
            try { dtStart = de.DateTime; }
            catch (Exception) { }

            CalculateReturnToClientByDate(dtStart, null);
        }

        private void QuoteSubmittedToClientDateDateEdit_Validating(object sender, CancelEventArgs e)
        {
            //DateEdit de = (DateEdit)sender;
            //if (this.strFormMode == "blockedit" || this.strFormMode == "view") return;

            //bool boolStartDateValid = false;
            //bool boolEndDateValid = false;
            //ValidateDates(de.DateTime, QuoteAcceptedByClientDateDateEdit.DateTime, ref boolStartDateValid, ref boolEndDateValid);
        }
        private void QuoteAcceptedByClientDateDateEdit_Validating(object sender, CancelEventArgs e)
        {
            //DateEdit de = (DateEdit)sender;
            //if (this.strFormMode == "blockedit" || this.strFormMode == "view") return;

            //bool boolStartDateValid = false;
            //bool boolEndDateValid = false;
            //ValidateDates(QuoteSubmittedToClientDateDateEdit.DateTime, de.DateTime, ref boolStartDateValid, ref boolEndDateValid);
        }
        private void ValidateDates(DateTime? dtStart, DateTime? dtEnd, ref bool boolStartValid, ref bool boolEndValid)
        {
            bool boolStartDateProcessed = false;
            bool boolEndDateProcessed = false;

            if (dtStart > dtEnd && dtStart != DateTime.MinValue && dtEnd != DateTime.MinValue)
            {
                dxErrorProvider1.SetError(QuoteSubmittedToClientDateDateEdit, "Quote Accepted Date should be greater than Quote Submitted Date.");
                dxErrorProvider1.SetError(QuoteAcceptedByClientDateDateEdit, "Quote Accepted Date should be greater than Quote Submitted Date.");
                boolStartValid = false;
                boolEndValid = false;
                return;
            }

            if (dtEnd != DateTime.MinValue && dtStart == DateTime.MinValue)
            {
                dxErrorProvider1.SetError(QuoteSubmittedToClientDateDateEdit, "Quote Submitted Date should not be blank when a date has been entered in Quote Accepted Date.");
                boolStartValid = false;
                boolStartDateProcessed = true;
            }

            if ((dtStart == null || string.IsNullOrEmpty(dtStart.ToString())) && !boolStartDateProcessed)
            {
                dxErrorProvider1.SetError(QuoteSubmittedToClientDateDateEdit, "Select\\enter a value.");
                boolStartValid = false;
                boolStartDateProcessed = true;
            }
            if (!boolStartDateProcessed)
            {
                if (CheckIfDateInFuture(dtStart))
                {
                    dxErrorProvider1.SetError(QuoteSubmittedToClientDateDateEdit, "Select\\enter a value that is not in the future.");
                    boolStartValid = true;
                    boolStartDateProcessed = true;
                }
                else
                {
                    dxErrorProvider1.SetError(QuoteSubmittedToClientDateDateEdit, "");
                    boolStartValid = true;
                    boolStartDateProcessed = true;
                }
            }

            if ((dtEnd == null || string.IsNullOrEmpty(dtEnd.ToString())))
            {
                dxErrorProvider1.SetError(QuoteAcceptedByClientDateDateEdit, "Select\\enter a value.");
                boolEndValid = false;
                boolEndDateProcessed = true;
            }
            else if (CheckIfDateInFuture(dtEnd))
            {
                dxErrorProvider1.SetError(QuoteAcceptedByClientDateDateEdit, "Select\\enter a value that is not in the future.");
                boolStartValid = true;
                boolEndDateProcessed = true;
            }
            else
            {
                dxErrorProvider1.SetError(QuoteAcceptedByClientDateDateEdit, "");
                boolEndValid = true;
                boolEndDateProcessed = true;
            }
            return;
        }

        private void TPOCheckedDateDateEdit_Validating(object sender, CancelEventArgs e)
        {
            DateEdit de = (DateEdit)sender;
            if (this.strFormMode != "blockedit")
            {
                if (CheckIfDateInFuture(de.DateTime))
                {
                    dxErrorProvider1.SetError(TPOCheckedDateDateEdit, "Select\\enter a value that is not in the future.");
                    e.Cancel = true;  // Show stop icon as field is invalid //
                    return;
                }
                else
                {
                    dxErrorProvider1.SetError(TPOCheckedDateDateEdit, "");
                }
            }
            else
            {
                dxErrorProvider1.SetError(TPOCheckedDateDateEdit, "");
            }
        }

        private void PlanningAuthoritySubmittedDateDateEdit_Validating(object sender, CancelEventArgs e)
        {
            DateEdit de = (DateEdit)sender;
            if (this.strFormMode != "blockedit")
            {
                if (CheckIfDateInFuture(de.DateTime))
                {
                    dxErrorProvider1.SetError(PlanningAuthoritySubmittedDateDateEdit, "Select\\enter a value that is not in the future.");
                    e.Cancel = true;  // Show stop icon as field is invalid //
                    return;
                }
                else
                {
                    dxErrorProvider1.SetError(PlanningAuthoritySubmittedDateDateEdit, "");
                }
            }
            else
            {
                dxErrorProvider1.SetError(PlanningAuthoritySubmittedDateDateEdit, "");
            }

        }

        private bool CheckIfDateInFuture(DateTime? date)
        {
            if (date == null) return false;
            if (Convert.ToDateTime(date) > DateTime.Now) return true;
            return false;
        }


        private void ReactiveCheckEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void ReactiveCheckEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;
            if (this.strFormMode == "blockedit") return;

            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            int? intReactive = null;
            CheckEdit ce = (CheckEdit)sender;
            try { intReactive = (ce.Checked ? 1 : 0); }
            catch (Exception) { }

            CalculateReturnToClientByDate(null, intReactive);
        }

        private void TenderDescriptionMemoEdit_Validating(object sender, CancelEventArgs e)
        {
            MemoEdit me = (MemoEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(me.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(TenderDescriptionMemoEdit, "Enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(TenderDescriptionMemoEdit, "");
            }
        }
       
        private void ContactPersonNameButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;

            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;
            
            if (e.Button.Tag.ToString() == "choose")
            {
                int intClientID = 0;
                try
                {
                    intClientID = (string.IsNullOrEmpty(currentRow.ClientID.ToString()) ? 0 : Convert.ToInt32(currentRow.ClientID));
                }
                catch (Exception) { };
                if (intClientID == 0)
                {
                    XtraMessageBox.Show("Please select the Client before attempting to select the Client Contact Person.", "Select Client Contact Person", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                int intClientContactID = 0;
                try
                {
                    intClientContactID = (string.IsNullOrEmpty(currentRow.ClientContactID.ToString()) ? 0 : Convert.ToInt32(currentRow.ClientContactID));
                }
                catch (Exception) { };

                var fChildForm = new frm_Core_Select_Client_Contact_Person_For_Single_Client();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.strPassedInClientIDsFilter = intClientID.ToString() + ",";
                fChildForm.intOriginalClientContactID = intClientContactID;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    if (intClientContactID != fChildForm.intSelectedClientContactID)
                    {
                        currentRow.ClientContactID = fChildForm.intSelectedClientContactID;
                        currentRow.ContactPersonName = fChildForm.strSelectedPersonName;
                        currentRow.ContactPersonPosition = fChildForm.strSelectedPosition;
                        currentRow.ContactPersonTitle = fChildForm.strSelectedTitle;
                        currentRow.ClientContactTelephone = fChildForm.strSelectedTelephone;
                        currentRow.ClientContactMobile = fChildForm.strSelectedMobile;
                        currentRow.ClientContactEmail = fChildForm.strSelectedEmail;
                        sp06498OMTenderEditBindingSource.EndEdit();
                    }
                }
            }
            if (e.Button.Tag.ToString() == "add")
            {
                int intClientID = 0;
                try
                {
                    intClientID = (string.IsNullOrEmpty(currentRow.ClientID.ToString()) ? 0 : Convert.ToInt32(currentRow.ClientID));
                }
                catch (Exception) { };

                if (intClientID == 0)
                {
                    XtraMessageBox.Show("Please select the Client before attempting to add a new Client Contact Person.", "Add New Client Contact Person", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                string strClientName = (string.IsNullOrEmpty(currentRow.ClientName) ? "" : currentRow.ClientName);

                var fChildForm = new frm_Core_Add_Client_Or_Site_Check_Exists();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.strFindWhat = "clientcontactperson";
                fChildForm.strPassedInFilterClientIDs = intClientID.ToString() + ",";
                fChildForm.strPassedInFilterClientDescription = strClientName;
                DialogResult dialogResult = fChildForm.ShowDialog();

                switch (dialogResult)
                {
                    case DialogResult.Cancel:
                        {
                            return;
                        }
                    case DialogResult.OK:  // User has selected a found match //
                        {
                            int intClientContactPersonID = fChildForm.intSelectedID1;
                            if (intClientContactPersonID <= 0) return;

                            currentRow.ClientContactID = fChildForm.intSelectedID1;
                            currentRow.ContactPersonName = fChildForm.strSelectedDescription1;
                            currentRow.ContactPersonPosition = fChildForm.strSelectedDescription3;
                            currentRow.ContactPersonTitle = fChildForm.strSelectedDescription4;
                            currentRow.ClientContactTelephone = fChildForm.strSelectedDescription5;
                            currentRow.ClientContactMobile = fChildForm.strSelectedDescription6;
                            currentRow.ClientContactEmail = fChildForm.strSelectedDescription7;
                            sp06498OMTenderEditBindingSource.EndEdit();
                        }
                        break;
                    case DialogResult.Yes:  // User has selected Add New Site - Add screen will call a method on this form to write the details once the Add screen is closed //
                        {
                            var fProgress = new frmProgress(10);
                            this.AddOwnedForm(fProgress);
                            fProgress.Show();  // ***** Closed in PostOpen event ***** //
                            Application.DoEvents();

                            var fChildForm2 = new frm_GC_Client_Contact_Person_Edit();
                            fChildForm2.MdiParent = this.MdiParent;
                            fChildForm2.GlobalSettings = this.GlobalSettings;
                            fChildForm2.strRecordIDs = "";
                            fChildForm2.strFormMode = "add";
                            fChildForm2.strCaller = this.Name;
                            fChildForm2.intRecordCount = 0;
                            fChildForm2.FormPermissions = this.FormPermissions;
                            fChildForm2.fProgress = fProgress;
                            fChildForm2.intLinkedToRecordID = intClientID;

                            fChildForm2.ibool_Adding_FromTender = true;
                            fChildForm2.frm_Tender_Edit_Screen_To_Update = this;
                            fChildForm2.AllowClientChange = false;

                            fChildForm2.Show();
                            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                            if (method != null) method.Invoke(fChildForm2, new object[] { null });
                        }
                        break;
                }
            }
            else if (e.Button.Tag.ToString() == "clear")
            {
                currentRow.ClientContactID = 0;
                currentRow.ContactPersonName = "";
                currentRow.ContactPersonPosition = "";
                currentRow.ContactPersonTitle = "";
                currentRow.ClientContactTelephone = "";
                currentRow.ClientContactMobile = "";
                currentRow.ClientContactEmail = "";
                sp06498OMTenderEditBindingSource.EndEdit();
            }
        }
        public void SetTenderClientContactPersonFromAddScreen(int ClientContractPersonID, string PersonName, string Title, string Position)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            // See if user wants to set up some contact info (phone number, mobile number and email address) //
            var fChildForm = new frm_Core_Client_Contact_Person_Contact_Info_Add_Fast();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.ContactPerson = PersonName;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                using (var AddContacts = new DataSet_Common_FunctionalityTableAdapters.QueriesTableAdapter())
                {
                    AddContacts.ChangeConnectionString(strConnectionString);
                    try
                    {
                        AddContacts.sp01060_Core_Client_Contact_Info_Add_Fast(ClientContractPersonID, fChildForm.Telephone, fChildForm.Mobile, fChildForm.Email);
                    }
                    catch (Exception) { }
                }
            }

            currentRow.ClientContactID = ClientContractPersonID;
            currentRow.ContactPersonName = PersonName;
            currentRow.ContactPersonPosition = Position;
            currentRow.ContactPersonTitle = Title;
            currentRow.ClientContactTelephone = fChildForm.Telephone;
            currentRow.ClientContactMobile = fChildForm.Mobile;
            currentRow.ClientContactEmail = fChildForm.Email;
            sp06498OMTenderEditBindingSource.EndEdit();
        }

        private void ClientContactEmailHyperLinkEdit_OpenLink(object sender, OpenLinkEventArgs e)
        {
            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;
            string strEmailAddress = currentRow.ClientContactEmail.ToString().Replace(",", ";");
            if (string.IsNullOrEmpty(strEmailAddress)) return;
            try
            {
                System.Diagnostics.Process.Start("mailto:" + strEmailAddress);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to email: " + strEmailAddress + ".", "Email Client Contact Person", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void KAMNameButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            
            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            if (e.Button.Tag.ToString() == "choose")
            {
                int intStaffID = 0;
                try { intStaffID = (string.IsNullOrEmpty(currentRow["KAM"].ToString()) ? 0 : Convert.ToInt32(currentRow["KAM"])); }
                catch (Exception) { }

                // See if we have a Client Contract selected - if yes we can enable the Set As Default tickbox on the called select staff screen //
                int intClientContractID = 0; 
                try { intClientContractID = (string.IsNullOrEmpty(currentRow.ClientContractID.ToString()) ? 0 : Convert.ToInt32(currentRow.ClientContractID)); }
                catch (Exception) { }
              
                var fChildForm = new frm_HR_Select_Staff();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalStaffID = intStaffID;
                fChildForm.intPassedInPreFilterType = 1;  // 0 = All, 1 = KAM, 2 = CM //
                if (intClientContractID > 0)
                {
                    fChildForm.boolAllowSetAsDefault = true;
                    fChildForm.strSetAsDefaultText = "Set As Default KAM";
                }
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    if (intStaffID != fChildForm.intSelectedStaffID)
                    {
                        currentRow.KAM = fChildForm.intSelectedStaffID;
                        currentRow.KAMName = fChildForm.strSelectedStaffName;
                        sp06498OMTenderEditBindingSource.EndEdit();
                    }
                    if (intClientContractID > 0 && fChildForm.boolSetAsDefault)
                    {
                        // Write the KAM as a default against the contract. //
                        int intResponsibleForRecordID = intClientContractID;
                        int intResponsibleForRecordTypeID = 2;  // 1 = Site Contract, 2 = Client Contract, 3 = Visit //
                        int intResponsibilityTypeID = 31;  // 31: KAM //;
                        int intPersonTypeID = 0;  // 0 = Staff, 1 = Client //
                        try
                        {
                            using (var SetDefaultPersonResponsibility = new DataSet_OM_TenderTableAdapters.QueriesTableAdapter())
                            {
                                SetDefaultPersonResponsibility.ChangeConnectionString(strConnectionString);
                                SetDefaultPersonResponsibility.sp06536_OM_Delete_Person_Responsibilities(intResponsibleForRecordID, intResponsibleForRecordTypeID, intResponsibilityTypeID);
                                SetDefaultPersonResponsibility.sp06512_OM_Tender_Add_Default_Person_Responsibility(fChildForm.intSelectedStaffID, intResponsibleForRecordID, intResponsibleForRecordTypeID, intResponsibilityTypeID, intPersonTypeID);
                            }
                        }
                        catch (Exception ex)
                        {
                            XtraMessageBox.Show("Unable to Add Default KAM - an error occurred.\n\n" + ex.Message, "Add Default KAM", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                }
            }
            else if (e.Button.Tag.ToString() == "clear")
            {
                currentRow.KAM = 0;
                currentRow.KAMName = "";
                sp06498OMTenderEditBindingSource.EndEdit();
            }
        }
        private void KAMNameButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(KAMNameButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(KAMNameButtonEdit, "");
            }
        }

        private void CMNameButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;

            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            if (e.Button.Tag.ToString() == "choose")
            {
                int intStaffID = 0;
                try { intStaffID = (string.IsNullOrEmpty(currentRow["KAM"].ToString()) ? 0 : Convert.ToInt32(currentRow["CM"])); }
                catch (Exception) { }

                // See if we have a Site Contract selected - if yes we can enable the Set As Default tickbox on the called select staff screen //
                int intSiteContractID = 0;
                try { intSiteContractID = (string.IsNullOrEmpty(currentRow.SiteContractID.ToString()) ? 0 : Convert.ToInt32(currentRow.SiteContractID)); }
                catch (Exception) { }

                var fChildForm = new frm_HR_Select_Staff();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalStaffID = intStaffID;
                fChildForm.intPassedInPreFilterType = 2; // 0 = All, 1 = KAM, 2 = CM //
                if (intSiteContractID > 0)
                {
                    fChildForm.boolAllowSetAsDefault = true;
                    fChildForm.strSetAsDefaultText = "Set As Default CM";
                }
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    if (intStaffID != fChildForm.intSelectedStaffID)
                    {
                        currentRow.CM = fChildForm.intSelectedStaffID;
                        currentRow.CMName = fChildForm.strSelectedStaffName;
                        sp06498OMTenderEditBindingSource.EndEdit();
                    }
                    if (intSiteContractID > 0 && fChildForm.boolSetAsDefault)
                    {
                        // Write the CM as a default CM against the contract. //
                        int intResponsibleForRecordID = intSiteContractID;
                        int intResponsibleForRecordTypeID = 1;  // 1 = Site Contract, 2 = Client Contract, 3 = Visit //
                        int intResponsibilityTypeID = 21;  // 31: CM //;
                        int intPersonTypeID = 0;  // 0 = Staff, 1 = Client //
                        try
                        {
                            using (var SetDefaultPersonResponsibility = new DataSet_OM_TenderTableAdapters.QueriesTableAdapter())
                            {
                                SetDefaultPersonResponsibility.ChangeConnectionString(strConnectionString);
                                SetDefaultPersonResponsibility.sp06536_OM_Delete_Person_Responsibilities(intResponsibleForRecordID, intResponsibleForRecordTypeID, intResponsibilityTypeID);
                                SetDefaultPersonResponsibility.sp06512_OM_Tender_Add_Default_Person_Responsibility(fChildForm.intSelectedStaffID, intResponsibleForRecordID, intResponsibleForRecordTypeID, intResponsibilityTypeID, intPersonTypeID);
                            }
                        }
                        catch (Exception ex)
                        {
                            XtraMessageBox.Show("Unable to Add Default CM - an error occurred.\n\n" + ex.Message, "Add Default CM", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                }
            }
            else if (e.Button.Tag.ToString() == "clear")
            {
                currentRow.CM = 0;
                currentRow.CMName = "";
                sp06498OMTenderEditBindingSource.EndEdit();
            }
        }
        private void CMNameButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(CMNameButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(CMNameButtonEdit, "");
            }
        }

        private void LinkedToPersonTypeButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var fChildForm = new frm_Core_Select_Person_Type();
                fChildForm.GlobalSettings = this.GlobalSettings;

                if (currentRow == null) return;
                int intOriginalPersonTypeID = 0;
                try { intOriginalPersonTypeID = (string.IsNullOrEmpty(currentRow.ProposedLabourTypeID.ToString()) ? 0 : Convert.ToInt32(currentRow.ProposedLabourTypeID)); }
                catch (Exception) { }

                fChildForm.intOriginalPersonTypeID = intOriginalPersonTypeID;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow.ProposedLabourTypeID = fChildForm.intSelectedPersonTypeID;
                    currentRow.ProposedLabourType = fChildForm.strSelectedPersonType;

                    if (intOriginalPersonTypeID != fChildForm.intSelectedPersonTypeID)  // Clear selected person since the person type has changed //
                    {
                        currentRow.ProposedLabourID = 0;
                        currentRow.ProposedLabour = "";
                    }
                    sp06498OMTenderEditBindingSource.EndEdit();
                }
            }
            else if (e.Button.Tag.ToString() == "clear")
            {
                currentRow.ProposedLabourTypeID = -1;
                currentRow.ProposedLabourType = "";
                currentRow.ProposedLabourID = 0;
                currentRow.ProposedLabour = "";
                sp06498OMTenderEditBindingSource.EndEdit();
            }
        }

        private void ContractorNameButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                int intPersonTypeID = -1;
                try { intPersonTypeID = (string.IsNullOrEmpty(currentRow.ProposedLabourTypeID.ToString()) ? 0 : Convert.ToInt32(currentRow.ProposedLabourTypeID)); }
                catch (Exception) { }

                if (intPersonTypeID <= -1)
                {
                    XtraMessageBox.Show("Select the Labour Type by clicking the Choose button on the Labour Type field before proceeding.", "Select Labour", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                int intSiteContractID = 0;
                try { intSiteContractID = (string.IsNullOrEmpty(currentRow.SiteContractID.ToString()) ? 0 : Convert.ToInt32(currentRow.SiteContractID)); }
                catch (Exception) { }

                if (intPersonTypeID == 1)  // Contractor //
                {
                    int intContractorID = 0;
                    try { intContractorID = (string.IsNullOrEmpty(currentRow.ProposedLabourID.ToString()) ? 0 : Convert.ToInt32(currentRow.ProposedLabourID)); }
                    catch (Exception) { }

                    double dblSiteLocationX = (double)0.00;
                    try { dblSiteLocationX = (string.IsNullOrEmpty(currentRow.SiteLocationX.ToString()) ? (double)0.00 : Convert.ToDouble(currentRow.SiteLocationX)); }
                    catch (Exception) { }

                    double dblSiteLocationY = (double)0.00;
                    try { dblSiteLocationY = (string.IsNullOrEmpty(currentRow.SiteLocationY.ToString()) ? (double)0.00 : Convert.ToDouble(currentRow.SiteLocationY)); }
                    catch (Exception) { }

                    string strSitePostcode = "";
                    try { strSitePostcode = (string.IsNullOrEmpty(currentRow.SitePostcode.ToString()) ? "" : currentRow.SitePostcode.ToString()); }
                    catch (Exception) { }

                    string strJobSubTypeID = "";
                    try { strJobSubTypeID = (string.IsNullOrEmpty(currentRow.WorkSubTypeID.ToString()) ? "" : currentRow.WorkSubTypeID.ToString() + ","); }
                    catch (Exception) { }

                    var fChildForm = new frm_OM_Select_Contractor();
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.intOriginalContractorID = intContractorID;
                    fChildForm._PassedInLatitude = dblSiteLocationX;
                    fChildForm._PassedInLongitude = dblSiteLocationY;
                    fChildForm._PassedInPostcode = strSitePostcode;
                    fChildForm._PassedInJobSubTypes = strJobSubTypeID;
                    if (intSiteContractID > 0)
                    {
                        fChildForm._PassedInPreferredLabourEnabled = true;
                        fChildForm._PassedInSiteContractID = intSiteContractID;
                    }

                    if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                    {
                        currentRow.ProposedLabourID = fChildForm.intSelectedContractorID;
                        currentRow.ProposedLabour = fChildForm.strSelectedContractorName;
                        currentRow.ProposedLabourLatitude = fChildForm.dbSelectedLatitude;
                        currentRow.ProposedLabourLongitude = fChildForm.dbSelectedLongitude;
                        currentRow.ProposedLabourPostcode = fChildForm.strSelectedPostcode;
                        sp06498OMTenderEditBindingSource.EndEdit();
                    }
                }
                else  // Staff //
                {
                    var fChildForm = new frm_HR_Select_Staff();
                    fChildForm.GlobalSettings = this.GlobalSettings;

                    if (strFormMode == "blockadd" || strFormMode == "blockedit")
                    {
                        fChildForm.intOriginalStaffID = 0;
                    }
                    else
                    {
                        fChildForm.intOriginalStaffID = (string.IsNullOrEmpty(currentRow.ProposedLabourID.ToString()) ? 0 : Convert.ToInt32(currentRow.ProposedLabourID));
                        if (intSiteContractID > 0)
                        {
                            fChildForm.boolPassedInJustPreferredLabourEnabled = true;
                            fChildForm.intPassedInSiteContractID = intSiteContractID;
                        }
                    }

                    if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                    {
                        currentRow.ProposedLabourID = fChildForm.intSelectedStaffID;
                        currentRow.ProposedLabour = fChildForm.strSelectedStaffName;
                        sp06498OMTenderEditBindingSource.EndEdit();
                    }
                }
            }
            else if (e.Button.Tag.ToString() == "clear")
            {
                currentRow.ProposedLabourID = 0;
                currentRow.ProposedLabour = "";
                sp06498OMTenderEditBindingSource.EndEdit();
            }
        }

        private void TenderGroupDescriptionButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;

            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            if (e.Button.Tag.ToString() == "choose")
            {
                int intTenderGroupID = 0;
                try
                {
                    intTenderGroupID = (string.IsNullOrEmpty(currentRow["TenderGroupID"].ToString()) ? 0 : Convert.ToInt32(currentRow["TenderGroupID"]));
                }
                catch (Exception) { }

                var fChildForm = new frm_OM_Select_Tender_Group();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalSelectedID = intTenderGroupID;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    if (intTenderGroupID != fChildForm.intSelectedID)
                    {
                        currentRow["TenderGroupID"] = fChildForm.intSelectedID;
                        currentRow["TenderGroupDescription"] = fChildForm.strSelectedDescription;
                        sp06498OMTenderEditBindingSource.EndEdit();
                    }
                }
            }
            else if (e.Button.Tag.ToString() == "clear")
            {
                currentRow["TenderGroupID"] = 0;
                currentRow["TenderGroupDescription"] = "";
                sp06498OMTenderEditBindingSource.EndEdit();
            }
            else if (e.Button.Tag.ToString() == "add")
            {              
                var fChildForm = new frm_OM_Tender_Group_Edit();
                fChildForm.MdiParent = this.MdiParent;
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.strRecordIDs = "";
                fChildForm.strFormMode = "add";
                fChildForm.strCaller = this.Name;
                fChildForm.intRecordCount = 0;
                fChildForm.FormPermissions = this.FormPermissions;
                fChildForm.frm_Tender_Edit_Screen_To_Update = this;
                var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                fChildForm.splashScreenManager = splashScreenManager1;
                fChildForm.splashScreenManager.ShowWaitForm();
                fChildForm.Show();

                System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                if (method != null) method.Invoke(fChildForm, new object[] { null });

            }
        }
        public void SetTenderGroupFromAddScreen(int TenderGroupID, string TenderGroupDescription)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            currentRow["TenderGroupID"] = TenderGroupID;
            currentRow["TenderGroupDescription"] = TenderGroupDescription;
            sp06498OMTenderEditBindingSource.EndEdit();
        }

        private void JobSubTypeDescriptionButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {              
                var fChildForm = new frm_OM_Select_Job_Sub_Type();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm._Mode = "single";
                fChildForm.intExtraWorksJobsOnly = 1;

                if (strFormMode == "blockadd" || strFormMode == "blockedit")
                {
                    fChildForm.intOriginalParentID = 0;
                    fChildForm.intOriginalChildID = 0;
                }
                else
                {
                    fChildForm.intOriginalParentID = (string.IsNullOrWhiteSpace(currentRow.JobTypeID.ToString()) ? 0 : Convert.ToInt32(currentRow.JobTypeID));
                    fChildForm.intOriginalChildID = (string.IsNullOrWhiteSpace(currentRow.WorkSubTypeID.ToString()) ? 0 : Convert.ToInt32(currentRow.WorkSubTypeID));
                }

                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    if (fChildForm.intOriginalChildID != fChildForm.intSelectedChildID)
                    {
                        currentRow.WorkSubTypeID = fChildForm.intSelectedChildID;
                        currentRow.JobTypeID = fChildForm.intSelectedParentID;
                        currentRow.JobSubTypeDescription = fChildForm.strSelectedChildDescriptions;
                        currentRow.JobTypeDescription = fChildForm.strSelectedParentDescriptions;
                        sp06498OMTenderEditBindingSource.EndEdit();
                    }
                }
            }
            else if (e.Button.Tag.ToString() == "clear")
            {
                currentRow.WorkSubTypeID =0;
                currentRow.JobTypeID = 0;
                currentRow.JobSubTypeDescription = "";
                currentRow.JobTypeDescription = "";
                sp06498OMTenderEditBindingSource.EndEdit();
            }
        }
        private void JobSubTypeDescriptionButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(JobSubTypeDescriptionButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(JobSubTypeDescriptionButtonEdit, "");
            }
        }

        private void PlanningAuthorityButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var fChildForm = new frm_Core_Select_Local_Authority();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm._Mode = "single";

                if (strFormMode == "blockadd" || strFormMode == "blockedit")
                {
                    fChildForm.intOriginalLPAID = 0;
                }
                else
                {
                    fChildForm.intOriginalLPAID = (string.IsNullOrWhiteSpace(currentRow.PlanningAuthorityID.ToString()) ? 0 : Convert.ToInt32(currentRow.PlanningAuthorityID));
                }

                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    if (fChildForm.intOriginalLPAID != fChildForm.intSelectedLPAID)
                    {
                        currentRow.PlanningAuthorityID = fChildForm.intSelectedLPAID;
                        currentRow.PlanningAuthority = fChildForm.strSelectedName;
                        currentRow.LocalAuthorityWebSite = fChildForm.strSelectedLPAURL;
                        currentRow.LocalAuthorityTelephone = fChildForm.strSelectedTelephone;
                        sp06498OMTenderEditBindingSource.EndEdit();
                    }
                }
            }
            else if (e.Button.Tag.ToString() == "clear")
            {
                currentRow.PlanningAuthorityID = 0;
                currentRow.PlanningAuthority = "";
                currentRow.LocalAuthorityWebSite = "";
                currentRow.LocalAuthorityTelephone = "";
                sp06498OMTenderEditBindingSource.EndEdit();
            }
        }

        private void LocalAuthorityWebSiteButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            if (e.Button.Tag.ToString() == "website")  // Choose Button //
            {
                string strWebSite = (string.IsNullOrWhiteSpace(currentRow.LocalAuthorityWebSite) ? "" : currentRow.LocalAuthorityWebSite);
                if (string.IsNullOrEmpty(strWebSite))
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("No Website Available - unable to proceed.", "Go To Planning Authority Website", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                if (!strWebSite.ToLower().StartsWith("http://")) strWebSite = "http://" + strWebSite;
                try
                {
                    System.Diagnostics.Process.Start(strWebSite);
                }
                catch
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to connect to: " + strWebSite + ".", "Go To Planning Authority Website", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
        }

        private void CalculateReturnToClientByDate(DateTime? StartDate, int? Reactive)
        {
            if (strFormMode == "view" || strFormMode == "blockedit") return;
            
            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            DateTime dtStartDate = DateTime.MinValue;
            if (StartDate != null)
            {
                dtStartDate = Convert.ToDateTime(StartDate);
            }
            else
            {
                try
                {
                    dtStartDate = currentRow.RequestReceivedDate;
                }
                catch (Exception) { }
            }
            if (dtStartDate < Convert.ToDateTime("2000-01-01")) return;

            
            int intReactive = 0;
            if (Reactive != null)
            {
                intReactive = Convert.ToInt32(Reactive);
            }
            else
            {
                try
                {
                    intReactive = currentRow.Reactive;
                }
                catch (Exception) { }
            }
            if (intReactive == -1) return;
            
            int intUnits = 0;
            try
            {
                intUnits = (intReactive == 1 ? currentRow.TenderReactiveDaysToReturnQuote : currentRow.TenderProactiveDaysToReturnQuote);
            }
            catch (Exception) { }
            if (intUnits <= 0) return;

            DateTime dtNewDate = dtStartDate.AddDays(intUnits);
            if (dtNewDate < Convert.ToDateTime("2000-01-01")) return;

            currentRow.ReturnToClientByDate = dtNewDate;
            sp06498OMTenderEditBindingSource.EndEdit();
        }

        private void ClientQuoteSpreadsheetExtractFileButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;
            switch (e.Button.Tag.ToString())
            {
                case "create":
                    {
                        if (strFormMode == "view" || strFormMode == "blockadd" || strFormMode == "blockedit") return;
                        if (string.IsNullOrWhiteSpace(i_str_ClientQuoteSpreadsheetExtractFolder))
                        {
                            XtraMessageBox.Show("Unable to create Client Quote Spreadsheet Extract file - the default storage path has not been defined in System Settings - please contact Technical Support for further info.", "Create Client Quote Spreadsheet Extract File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        int intTenderID = currentRow.TenderID;
                        string strMessage = CheckForPendingSave();
                        if (intTenderID <= 0 || strMessage != "")
                        {
                            if (intTenderID <= 0)
                            {
                                strMessage = "Unable to create Client Quote Spreadsheet Extract file - you must save the Tender first.";
                            }
                            else
                            {
                                strMessage += "\nUnable to create Client Quote Spreadsheet Extract file. You must save the changes first.";
                            }
                            strMessage += "\n\nWould you like to save the change(s) now?";
                            switch (XtraMessageBox.Show(strMessage, "Create Client Quote Spreadsheet Extract file", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                            {
                                case DialogResult.No:
                                    return;
                                case DialogResult.Yes:
                                    // Fire Save //
                                    if (!string.IsNullOrEmpty(SaveChanges(true))) return;  // Save Failed so abort process to allow user to correct //
                                    break;
                            }
                        }
                        intTenderID = currentRow.TenderID;  // Pick up TenderID in case the first save just occurred... //

                        // Remove any old version of the file //
                        string strOldFileName = currentRow.ClientQuoteSpreadsheetExtractFile;
                        if (!string.IsNullOrWhiteSpace(strOldFileName))
                        {
                            string strOldPath = Path.Combine(i_str_ClientQuoteSpreadsheetExtractFolder, strOldFileName);
                            try
                            {
                                if (File.Exists(strOldPath)) File.Delete(strOldPath);
                            }
                            catch (Exception) { }
                        }

                        // Export Data to Spreadsheet //
                        string strFileName = "Quote_Extract_" + intTenderID.ToString().PadLeft(8, '0') + "_" + DateTime.Now.ToString("yyyy-MM-dd_HH_mm_ss") + ".xlsx";
                        string strPath = Path.Combine(i_str_ClientQuoteSpreadsheetExtractFolder, strFileName);
                                    
                        try
                        {
                            Workbook destWorkBook = new DevExpress.Spreadsheet.Workbook();
                            destWorkBook.CreateNewDocument();
                            destWorkBook.Worksheets.ActiveWorksheet.Name = "Tender Quote Data Extract";

                            Worksheet worksheet = destWorkBook.Worksheets.ActiveWorksheet;

                            SqlDataAdapter sda = new SqlDataAdapter();
                            DataSet ds = new DataSet("NewDataSet");

                            using (var conn = new SqlConnection(strConnectionString))
                            {
                                conn.Open();

                                using (var cmd = new SqlCommand())
                                {
                                    cmd.CommandText = "sp06516_OM_Tender_CLient_Quote_Extract";
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.Parameters.Add(new SqlParameter("@TenderID", intTenderID));
                                    cmd.Connection = conn;
                                    sda = new SqlDataAdapter(cmd);
                                    sda.Fill(ds, "Table");
                                }
                                conn.Close();

                                worksheet.Import(ds.Tables[0], true, 0, 0);
                                sda = null;
                                ds = null;

                                destWorkBook.SaveDocument(strPath);
                                destWorkBook.Dispose();

                                currentRow.ClientQuoteSpreadsheetExtractFile = strFileName;
                                sp06498OMTenderEditBindingSource.EndEdit();
                                SaveChanges(true);  // Commit the changes to the DB //
                            }
                        }
                        catch (Exception ex)
                        {
                            XtraMessageBox.Show("Unable to create Client Quote Spreadsheet Extract file - an error occurred:\n\n" + ex.Message, "Create Client Quote Spreadsheet Extract File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                    }
                    break;
                case "view":
                    {
                        string strFile = currentRow.ClientQuoteSpreadsheetExtractFile.ToString();

                        if (string.IsNullOrEmpty(strFile))
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Link the File to be Viewed before proceeding.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        try
                        {
                            if (Path.GetExtension(Path.Combine(i_str_ClientQuoteSpreadsheetExtractFolder + strFile)).ToLower() != ".pdf")
                            {
                                System.Diagnostics.Process.Start(Path.Combine(i_str_ClientQuoteSpreadsheetExtractFolder + strFile));
                            }
                            else
                            {
                                if (!BaseObjects.Keyboard.IsKeyDown(Keys.A))  // Only open in DevExpress PDF viewer if user is not holding down A key (CTRL Key doesn't work when fired from a grid as it's used for row selection) //
                                {
                                    frm_Core_PDF_Viewer fChildForm = new frm_Core_PDF_Viewer();
                                    fChildForm.strPDFFile = Path.Combine(i_str_ClientQuoteSpreadsheetExtractFolder + strFile);
                                    fChildForm.MdiParent = this.MdiParent;
                                    fChildForm.GlobalSettings = this.GlobalSettings;
                                    fChildForm.Show();
                                }
                                else
                                {
                                    System.Diagnostics.Process.Start(Path.Combine(i_str_ClientQuoteSpreadsheetExtractFolder + strFile));
                                }
                            }
                        }
                        catch
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view the Linked File: " + strFile + ".\n\nThe File may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                    break;

                case "clear":
                    {
                        if (strFormMode == "view") return;
                        
                        string strOldFileName = currentRow.ClientQuoteSpreadsheetExtractFile;
                        if (!string.IsNullOrWhiteSpace(strOldFileName))
                        {
                            string strOldPath = Path.Combine(i_str_ClientQuoteSpreadsheetExtractFolder, strOldFileName);
                            try
                            {
                                if (File.Exists(strOldPath)) File.Delete(strOldPath);
                            }
                            catch (Exception) { }
                        }

                        currentRow.ClientQuoteSpreadsheetExtractFile = "";
                        sp06498OMTenderEditBindingSource.EndEdit();
                    }
                    break;
            }
        }


        private void TreeProtectedCheckEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void TreeProtectedCheckEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;
            //if (this.strFormMode == "blockedit") return;

            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            int intTreeProtected = 0;
            CheckEdit ce = (CheckEdit)sender;
            try { intTreeProtected = (ce.Checked ? 1 : 0); }
            catch (Exception) { }

            if (intTreeProtected == 0)
            {
                if (currentRow.PlanningAuthorityID != 0) currentRow.PlanningAuthorityID = 0;
                if (currentRow.PlanningAuthorityNumber != "") currentRow.PlanningAuthorityNumber = "";
                if (currentRow.PlanningAuthorityOutcomeID != 0) currentRow.PlanningAuthorityOutcomeID = 0;
                if (currentRow.PlanningAuthorityOkToProceed != 0) currentRow.PlanningAuthorityOkToProceed = 0;
                if (currentRow["PlanningAuthoritySubmittedDate"] != DBNull.Value) currentRow["PlanningAuthoritySubmittedDate"] = DBNull.Value;
                
                if (currentRow["PlanningProceedDateSet"] != DBNull.Value) currentRow["PlanningProceedDateSet"] = DBNull.Value;
                if (currentRow.PlanningProceedStaffID != 0) currentRow.PlanningProceedStaffID = 0;
                if (currentRow.PlanningProceedStaffName != "") currentRow.PlanningProceedStaffName = "";
                sp06498OMTenderEditBindingSource.EndEdit();
            }
            Set_Planning_Enabled_Status(intTreeProtected);
        }
        private void Set_Planning_Enabled_Status(int? TreeProtected)
        {
            if (strFormMode == "blockedit") return;

            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;
            int intStatusID = 0;
            try
            {
                intStatusID = Convert.ToInt32(currentRow.StatusID);
            }
            catch (Exception) { }
            if (TreeProtected == null)
            {
                try
                {
                    TreeProtected = currentRow.TreeProtected;
                }
                catch (Exception) { }
            }
            if (TreeProtected == 1 && (intStatusID < 90 && intStatusID != 70))
            {
                PlanningAuthorityNumberTextEdit.Properties.ReadOnly = false;
                PlanningAuthoritySubmittedDateDateEdit.Properties.ReadOnly = false;
                PlanningAuthorityOutcomeIDGridLookUpEdit.Properties.ReadOnly = false;
                PlanningAuthorityOkToProceedCheckEdit.Properties.ReadOnly = false;
            }
            else  // Not tree protected so no need for planning or Tender Closed \ Rejected //
            {
                PlanningAuthorityNumberTextEdit.Properties.ReadOnly = true;
                PlanningAuthoritySubmittedDateDateEdit.Properties.ReadOnly = true;
                PlanningAuthorityOutcomeIDGridLookUpEdit.Properties.ReadOnly = true;
                PlanningAuthorityOkToProceedCheckEdit.Properties.ReadOnly = true;
            }
        }


        private void QuotedLabourSellSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void QuotedLabourSellSpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view" || strFormMode == "blockedit") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            SpinEdit se = (SpinEdit)sender;
            decimal decValue = 0;
            try { decValue = (string.IsNullOrEmpty(se.Value.ToString()) ? 0 : Convert.ToDecimal(se.Value)); }
            catch (Exception) { }

            Calculate_Quoted_Sell("QuotedLabourSell", decValue);
        }

        private void QuotedMaterialSellSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void QuotedMaterialSellSpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            SpinEdit se = (SpinEdit)sender;
            decimal decValue = 0;
            try { decValue = (string.IsNullOrEmpty(se.Value.ToString()) ? 0 : Convert.ToDecimal(se.Value)); }
            catch (Exception) { }

            Calculate_Quoted_Sell("QuotedMaterialSell", decValue);
        }

        private void QuotedEquipmentSellSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void QuotedEquipmentSellSpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            SpinEdit se = (SpinEdit)sender;
            decimal decValue = 0;
            try { decValue = (string.IsNullOrEmpty(se.Value.ToString()) ? 0 : Convert.ToDecimal(se.Value)); }
            catch (Exception) { }

            Calculate_Quoted_Sell("QuotedEquipmentSell", decValue);
        }


        private void AcceptedLabourSellSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void AcceptedLabourSellSpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            SpinEdit se = (SpinEdit)sender;
            decimal decValue = 0;
            try { decValue = (string.IsNullOrEmpty(se.Value.ToString()) ? 0 : Convert.ToDecimal(se.Value)); }
            catch (Exception) { }

            Calculate_Accepted_Sell("AcceptedLabourSell", decValue);
        }

        private void AcceptedMaterialSellSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void AcceptedMaterialSellSpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            SpinEdit se = (SpinEdit)sender;
            decimal decValue = 0;
            try { decValue = (string.IsNullOrEmpty(se.Value.ToString()) ? 0 : Convert.ToDecimal(se.Value)); }
            catch (Exception) { }

            Calculate_Accepted_Sell("AcceptedMaterialSell", decValue);
        }

        private void AcceptedEquipmentSellSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void AcceptedEquipmentSellSpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            SpinEdit se = (SpinEdit)sender;
            decimal decValue = 0;
            try { decValue = (string.IsNullOrEmpty(se.Value.ToString()) ? 0 : Convert.ToDecimal(se.Value)); }
            catch (Exception) { }

            Calculate_Accepted_Sell("AcceptedEquipmentSell", decValue);
        }


        private void ActualLabourCostSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void ActualLabourCostSpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            SpinEdit se = (SpinEdit)sender;
            decimal decValue = 0;
            try { decValue = (string.IsNullOrEmpty(se.Value.ToString()) ? 0 : Convert.ToDecimal(se.Value)); }
            catch (Exception) { }

            Calculate_Actual_Cost("ActualLabourCost", decValue);
        }

        private void ActualMaterialCostSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void ActualMaterialCostSpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            SpinEdit se = (SpinEdit)sender;
            decimal decValue = 0;
            try { decValue = (string.IsNullOrEmpty(se.Value.ToString()) ? 0 : Convert.ToDecimal(se.Value)); }
            catch (Exception) { }

            Calculate_Actual_Cost("ActualMaterialCost", decValue);
        }

        private void ActualEquipmentCostSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void ActualEquipmentCostSpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            SpinEdit se = (SpinEdit)sender;
            decimal decValue = 0;
            try { decValue = (string.IsNullOrEmpty(se.Value.ToString()) ? 0 : Convert.ToDecimal(se.Value)); }
            catch (Exception) { }

            Calculate_Actual_Cost("ActualEquipmentCost", decValue);
        }

        private void ProposedLabourCostSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void ProposedLabourCostSpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            SpinEdit se = (SpinEdit)sender;
            decimal decValue = 0;
            try { decValue = (string.IsNullOrEmpty(se.Value.ToString()) ? 0 : Convert.ToDecimal(se.Value)); }
            catch (Exception) { }

            Calculate_Proposed_Cost("ProposedLabourCost", decValue);
        }

        private void ProposedMaterialCostSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void ProposedMaterialCostSpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            SpinEdit se = (SpinEdit)sender;
            decimal decValue = 0;
            try { decValue = (string.IsNullOrEmpty(se.Value.ToString()) ? 0 : Convert.ToDecimal(se.Value)); }
            catch (Exception) { }

            Calculate_Proposed_Cost("ProposedMaterialCost", decValue);
        }

        private void ProposedEquipmentCostSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void ProposedEquipmentCostSpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            SpinEdit se = (SpinEdit)sender;
            decimal decValue = 0;
            try { decValue = (string.IsNullOrEmpty(se.Value.ToString()) ? 0 : Convert.ToDecimal(se.Value)); }
            catch (Exception) { }

            Calculate_Proposed_Cost("ProposedEquipmentCost", decValue);
        }

        private void Calculate_Quoted_Sell(string strCurrentColumn, decimal decValue)
        {
            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            decimal decQuotedLabourSell = (strCurrentColumn == "QuotedLabourSell" ? decValue : (string.IsNullOrWhiteSpace(currentRow["QuotedLabourSell"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.QuotedLabourSell)));
            decimal decQuotedMaterialSell = (strCurrentColumn == "QuotedMaterialSell" ? decValue : (string.IsNullOrWhiteSpace(currentRow["QuotedMaterialSell"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.QuotedMaterialSell)));
            decimal decQuotedEquipmentSell = (strCurrentColumn == "QuotedEquipmentSell" ? decValue : (string.IsNullOrWhiteSpace(currentRow["QuotedEquipmentSell"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.QuotedEquipmentSell)));

            decimal decCurrentValue = (strCurrentColumn == "QuotedTotalSell" ? decValue : (string.IsNullOrWhiteSpace(currentRow["QuotedTotalSell"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.QuotedTotalSell)));

            if (decCurrentValue != (decQuotedLabourSell + decQuotedMaterialSell + decQuotedEquipmentSell))
            {
                currentRow.QuotedTotalSell = (decQuotedLabourSell + decQuotedMaterialSell + decQuotedEquipmentSell);
                sp06498OMTenderEditBindingSource.EndEdit();
            }
        }
        private void Calculate_Accepted_Sell(string strCurrentColumn, decimal decValue)
        {
            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            decimal decAcceptedLabourSell = (strCurrentColumn == "AcceptedLabourSell" ? decValue : (string.IsNullOrWhiteSpace(currentRow["AcceptedLabourSell"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.AcceptedLabourSell)));
            decimal decAcceptedMaterialSell = (strCurrentColumn == "AcceptedMaterialSell" ? decValue : (string.IsNullOrWhiteSpace(currentRow["AcceptedMaterialSell"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.AcceptedMaterialSell)));
            decimal decAcceptedEquipmentSell = (strCurrentColumn == "AcceptedEquipmentSell" ? decValue : (string.IsNullOrWhiteSpace(currentRow["AcceptedEquipmentSell"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.AcceptedEquipmentSell)));

            decimal decCurrentValue = (strCurrentColumn == "AcceptedTotalSell" ? decValue : (string.IsNullOrWhiteSpace(currentRow["AcceptedTotalSell"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.AcceptedTotalSell)));

            if (decCurrentValue != (decAcceptedLabourSell + decAcceptedMaterialSell + decAcceptedEquipmentSell))
            {
                currentRow.AcceptedTotalSell = (decAcceptedLabourSell + decAcceptedMaterialSell + decAcceptedEquipmentSell);
                sp06498OMTenderEditBindingSource.EndEdit();
            }
        }
        private void Calculate_Actual_Cost(string strCurrentColumn, decimal decValue)
        {
            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            decimal decActualLabourCost = (strCurrentColumn == "ActualLabourCost" ? decValue : (string.IsNullOrWhiteSpace(currentRow["ActualLabourCost"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.ActualLabourCost)));
            decimal decActualMaterialCost = (strCurrentColumn == "ActualMaterialCost" ? decValue : (string.IsNullOrWhiteSpace(currentRow["ActualMaterialCost"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.ActualMaterialCost)));
            decimal decActualEquipmentCost = (strCurrentColumn == "ActualEquipmentCost" ? decValue : (string.IsNullOrWhiteSpace(currentRow["ActualEquipmentCost"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.ActualEquipmentCost)));

            decimal decCurrentValue = (strCurrentColumn == "AcceptedTotalSell" ? decValue : (string.IsNullOrWhiteSpace(currentRow["AcceptedTotalSell"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.AcceptedTotalSell)));

            if (decCurrentValue != (decActualLabourCost + decActualMaterialCost + decActualEquipmentCost))
            {
                currentRow.ActualTotalCost = (decActualLabourCost + decActualMaterialCost + decActualEquipmentCost);
                sp06498OMTenderEditBindingSource.EndEdit();
            }
        }
        private void Calculate_Proposed_Cost(string strCurrentColumn, decimal decValue)
        {
            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            decimal decProposedLabourCost = (strCurrentColumn == "ProposedLabourCost" ? decValue : (string.IsNullOrWhiteSpace(currentRow["ProposedLabourCost"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.ProposedLabourCost)));
            decimal decProposedMaterialCost = (strCurrentColumn == "ProposedMaterialCost" ? decValue : (string.IsNullOrWhiteSpace(currentRow["ProposedMaterialCost"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.ProposedMaterialCost)));
            decimal decProposedEquipmentCost = (strCurrentColumn == "ProposedEquipmentCost" ? decValue : (string.IsNullOrWhiteSpace(currentRow["ProposedEquipmentCost"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.ProposedEquipmentCost)));

            decimal decCurrentValue = (strCurrentColumn == "ProposedTotalCost" ? decValue : (string.IsNullOrWhiteSpace(currentRow["ProposedTotalCost"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.ProposedTotalCost)));

            if (decCurrentValue != (decProposedLabourCost + decProposedMaterialCost + decProposedEquipmentCost))
            {
                currentRow.ProposedTotalCost = (decProposedLabourCost + decProposedMaterialCost + decProposedEquipmentCost);
                sp06498OMTenderEditBindingSource.EndEdit();
            }
        }

        private void PlanningAuthorityOkToProceedCheckEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void PlanningAuthorityOkToProceedCheckEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;
            //if (this.strFormMode == "blockedit") return;

            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            int intOkToProceed = 0;
            CheckEdit ce = (CheckEdit)sender;
            try { intOkToProceed = (ce.Checked ? 1 : 0); }
            catch (Exception) { }

            if (intOkToProceed == 0)
            {
                if (currentRow["PlanningProceedDateSet"] != DBNull.Value) currentRow["PlanningProceedDateSet"] = DBNull.Value;
                if (currentRow.PlanningProceedStaffID != 0) currentRow.PlanningProceedStaffID = 0;
                if (currentRow.PlanningProceedStaffName != "") currentRow.PlanningProceedStaffName = "";
            }
            else
            {
                currentRow.PlanningProceedDateSet = DateTime.Now;
                currentRow.PlanningProceedStaffID = GlobalSettings.UserID;
                currentRow.PlanningProceedStaffName = GlobalSettings.UserSurname + ": " + GlobalSettings.UserForename;
            }
            sp06498OMTenderEditBindingSource.EndEdit();
        }


        #region Client POs
        private void ClientPONumberTextEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void ClientPONumberTextEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            TextEdit te = (TextEdit)sender;
            string strValue = "";
            try { strValue = (string.IsNullOrEmpty(te.EditValue.ToString()) ? "" : te.EditValue.ToString()); }
            catch (Exception) { }

            Check_Client_PO_Info(strValue, null, null);
        }

        private void NoClientPOCheckEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void NoClientPOCheckEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            CheckEdit ce = (CheckEdit)sender;
            int intValue = 0;
            try { intValue = (ce.Checked ? 1 : 0); }
            catch (Exception) { }

            Check_Client_PO_Info(null, intValue, null);
        }

        private void NoClientPOAuthorisedByDirectorButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;

            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            if (e.Button.Tag.ToString() == "choose")
            {
                bool boolValidationTurnedOffForNoClientPOAuthorisedByDirectorButtonEdit = false;
                if (!string.IsNullOrWhiteSpace(dxErrorProvider1.GetError(NoClientPOAuthorisedByDirectorButtonEdit)))
                {
                    boolValidationTurnedOffForNoClientPOAuthorisedByDirectorButtonEdit = true;
                    Set_NoClientPOAuthorisedByDirectorButtonEdit_Validation_Message(false);  // Take Error off column temporarily // 
                }

                string strMessage = "You are about to mark this Tender as Director Authorised with No Client PO Number.\n\n<color=red>If you proceed the Director selected from the next screen will be emailed informing them they have authorised this Tender.\n\n</color>Procced?";
                if (XtraMessageBox.Show(strMessage, "Add Default CM", MessageBoxButtons.YesNo, MessageBoxIcon.Question, DefaultBoolean.True) == DialogResult.No)
                {
                    if (boolValidationTurnedOffForNoClientPOAuthorisedByDirectorButtonEdit)  // Put error message back on column //
                    {
                        Set_NoClientPOAuthorisedByDirectorButtonEdit_Validation_Message(true);
                    }
                    return;
                }

                strMessage = CheckForPendingSave();
                if (strMessage != "")
                {
                    strMessage += "\nThere are outstanding changes pending on the screen. You must save these before proceeding with Selecting the Director to Authorise.\n\nWould you like to save the change(s) now?";
                    switch (XtraMessageBox.Show(strMessage, "Select Authorising Director", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                    {
                        case DialogResult.No:
                            if (boolValidationTurnedOffForNoClientPOAuthorisedByDirectorButtonEdit)  // Put error message back on column //
                            {
                                Set_NoClientPOAuthorisedByDirectorButtonEdit_Validation_Message(true);
                            }
                            return;
                        case DialogResult.Yes:
                            // Fire Save //
                            if (!string.IsNullOrEmpty(SaveChanges(true)))
                            {
                                if (boolValidationTurnedOffForNoClientPOAuthorisedByDirectorButtonEdit)  // Put error message back on column //
                                {
                                    Set_NoClientPOAuthorisedByDirectorButtonEdit_Validation_Message(true);
                                }
                                return;  // Save Failed so abort process to allow user to correct //
                            }
                            break;
                    }
                }

                int intNoClientPOAuthorisedByDirectorID = 0;
                try
                {
                    intNoClientPOAuthorisedByDirectorID = (string.IsNullOrEmpty(currentRow["NoClientPOAuthorisedByDirectorID"].ToString()) ? 0 : Convert.ToInt32(currentRow["NoClientPOAuthorisedByDirectorID"]));
                }
                catch (Exception) { }

                var fChildForm = new frm_HR_Select_Employee();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalEmployeeID = intNoClientPOAuthorisedByDirectorID;
                fChildForm.intPassedInGenericJobTitleIDFilter = 1090;  // Director //
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    if (intNoClientPOAuthorisedByDirectorID != fChildForm.intSelectedEmployeeID)
                    {
                        string strForename = fChildForm.strSelectedForename;
                        string strSurname = fChildForm.strSelectedSurname;
                        int intDirectorID = fChildForm.intSelectedEmployeeID;

                        #region Send Director Authorisation Email

                        int intTenderID = currentRow.TenderID;
                         
                        splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        splashScreenManager.ShowWaitForm();
                        splashScreenManager.SetWaitFormDescription("Sending Authorisation Email...");

                        try
                        {
                            #region Get Email Settings
                            string strHTMLBodyFile = "";
                            string strEmailFrom = "";
                            string strEmailSubjectLine = "";
                            string strCCToEmailAddress = "";

                            string strSMTPMailServerAddress = "";
                            string strSMTPMailServerUsername = "";
                            string strSMTPMailServerPassword = "";
                            string strSMTPMailServerPort = "";

                            SqlDataAdapter sdaSettings = new SqlDataAdapter();
                            DataSet dsSettings = new DataSet("NewDataSet");

                            using (var conn = new SqlConnection(strConnectionString))
                            {
                                conn.Open();
                                using (var cmd = new SqlCommand())
                                {
                                    cmd.CommandText = "sp06520_OM_Tender_Director_Authorisation_Email_Get_Email_Settings";
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.Parameters.Add(new SqlParameter("@StaffID", GlobalSettings.UserID));
                                    cmd.Connection = conn;
                                    sdaSettings = new SqlDataAdapter(cmd);
                                    sdaSettings.Fill(dsSettings, "Table");
                                }
                                conn.Close();
                            }
                            if (dsSettings.Tables[0].Rows.Count != 1)
                            {
                                if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the 'Email Settings' (from the System Configuration Screen) - number of rows returned not equal to 1.\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                if (boolValidationTurnedOffForNoClientPOAuthorisedByDirectorButtonEdit)  // Put error message back on column //
                                {
                                    Set_NoClientPOAuthorisedByDirectorButtonEdit_Validation_Message(true);
                                }
                                return;
                            }
                            DataRow dr1 = dsSettings.Tables[0].Rows[0];
                            strHTMLBodyFile = dr1["BodyFileName"].ToString();
                            strEmailFrom = dr1["EmailFrom"].ToString();
                            strEmailSubjectLine = dr1["SubjectLine"].ToString();
                            strCCToEmailAddress = dr1["CCToName"].ToString();

                            strSMTPMailServerAddress = dr1["SMTPMailServerAddress"].ToString();
                            strSMTPMailServerUsername = dr1["SMTPMailServerUsername"].ToString();
                            strSMTPMailServerPassword = dr1["SMTPMailServerPassword"].ToString();
                            strSMTPMailServerPort = dr1["SMTPMailServerPort"].ToString();
                            if (string.IsNullOrEmpty(strSMTPMailServerPort) || !CheckingFunctions.IsNumeric(strSMTPMailServerPort)) strSMTPMailServerPort = "0";
                            int intSMTPMailServerPort = Convert.ToInt32(strSMTPMailServerPort);

                            if (string.IsNullOrEmpty(strHTMLBodyFile) || string.IsNullOrEmpty(strEmailFrom) || string.IsNullOrEmpty(strSMTPMailServerAddress))
                            {
                                if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                                DevExpress.XtraEditors.XtraMessageBox.Show("One or more of the email settings (Email Layout File, From Email Address and SMTP Mail Server Name) are missing from the System Configuration Screen.\n\nPlease update the System Settings then try again. If the problem persists, contact Technical Support.", "Get Email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                if (boolValidationTurnedOffForNoClientPOAuthorisedByDirectorButtonEdit)  // Put error message back on column //
                                {
                                    Set_NoClientPOAuthorisedByDirectorButtonEdit_Validation_Message(true);
                                }
                                return;
                            }
                            #endregion

                            #region Ping Test
                            bool boolNoInternet = BaseObjects.PingTest.Ping("www.google.com");
                            if (!boolNoInternet) boolNoInternet = BaseObjects.PingTest.Ping("www.microsoft.com");  // try another site just in case that one is down //
                            if (!boolNoInternet) boolNoInternet = BaseObjects.PingTest.Ping("www.yahoo.com");  // try another site just in case that one is down //
                            if (!boolNoInternet) // alert user and halt process //
                            {
                                if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to connect to the internet - unable to send.\n\nPlease check your internet connection then try again.\n\nIf the problem persists, contact Technical Support.", "Check Internet Connection", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                if (boolValidationTurnedOffForNoClientPOAuthorisedByDirectorButtonEdit)  // Put error message back on column //
                                {
                                    Set_NoClientPOAuthorisedByDirectorButtonEdit_Validation_Message(true);
                                }
                                return;
                            }
                            #endregion

                            using (var conn = new SqlConnection(strConnectionString))
                            {
                                conn.Open();
                                SqlDataAdapter sda = new SqlDataAdapter();
                                DataSet ds = new DataSet("NewDataSet");
                                using (var cmd = new SqlCommand())
                                {
                                    cmd.CommandText = "sp06519_OM_Tender_Director_Authorisation_Email_Data";
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.Parameters.Add(new SqlParameter("@TenderID", intTenderID));
                                    cmd.Parameters.Add(new SqlParameter("@DirectorID", intDirectorID));
                                    cmd.Connection = conn;
                                    sda = new SqlDataAdapter(cmd);
                                    sda.Fill(ds, "Table");
                                }
                                conn.Close();
                                string strBody = System.IO.File.ReadAllText(strHTMLBodyFile);

                                // Merge the data into the HTML //
                                DataRow dr = ds.Tables[0].Rows[0];
                                foreach (DataColumn dc in ds.Tables[0].Columns)
                                {
                                    strBody = strBody.Replace("%" + dc.Caption + "%", dr[dc].ToString());
                                }
                                string strEmailAddresses = dr["DirectorEmail"].ToString();
                                if (string.IsNullOrWhiteSpace(strEmailAddresses))
                                {
                                    if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                                    DevExpress.XtraEditors.XtraMessageBox.Show("No email stored for selected Director - unable to sent Authorisation Email - contact Technical Support or HR Dept.", "Get Director Email Address", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    if (boolValidationTurnedOffForNoClientPOAuthorisedByDirectorButtonEdit)  // Put error message back on column //
                                    {
                                        Set_NoClientPOAuthorisedByDirectorButtonEdit_Validation_Message(true);
                                    }
                                    return;
                                }

                                char[] delimiters = new char[] { ',' };
                                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                                msg.From = new System.Net.Mail.MailAddress(strEmailFrom);
                                string[] strEmailTo = strEmailAddresses.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                                if (strEmailTo.Length > 0)
                                {
                                    foreach (string strEmailAddress in strEmailTo)
                                    {
                                        msg.To.Add(new System.Net.Mail.MailAddress(strEmailAddress));
                                    }
                                }
                                else
                                {
                                    msg.To.Add(new System.Net.Mail.MailAddress(strEmailAddresses));  // Original value wouldn't split as no commas so it's just one email address so use it //
                                }
                                msg.Subject = strEmailSubjectLine;
                                if (!string.IsNullOrEmpty(strCCToEmailAddress)) msg.CC.Add(strCCToEmailAddress);

                                // Add the person sending to the CC To Email so they have a copy for their own records //
                                msg.CC.Add(strEmailFrom);

                                msg.Priority = System.Net.Mail.MailPriority.High;
                                msg.IsBodyHtml = true;

                                System.Net.Mail.AlternateView plainView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(System.Text.RegularExpressions.Regex.Replace(strBody, @"<(.|\n)*?>", string.Empty), null, "text/plain");
                                System.Net.Mail.AlternateView htmlView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(strBody, null, "text/html");

                                // Create a new attachment //
                                //System.Net.Mail.Attachment mailAttachment = new System.Net.Mail.Attachment(strPDFName);  // Attach PDF report //
                                //msg.Attachments.Add(mailAttachment);

                                msg.AlternateViews.Add(plainView);
                                msg.AlternateViews.Add(htmlView);

                                object userState = msg;
                                System.Net.Mail.SmtpClient emailClient = null;
                                if (intSMTPMailServerPort != 0)
                                {
                                    emailClient = new System.Net.Mail.SmtpClient(strSMTPMailServerAddress, intSMTPMailServerPort);
                                }
                                else
                                {
                                    emailClient = new System.Net.Mail.SmtpClient(strSMTPMailServerAddress);
                                }
                                if (!string.IsNullOrEmpty(strSMTPMailServerUsername) && !string.IsNullOrEmpty(strSMTPMailServerPassword))
                                {
                                    System.Net.NetworkCredential basicCredential = new System.Net.NetworkCredential(strSMTPMailServerUsername, strSMTPMailServerPassword);
                                    emailClient.UseDefaultCredentials = false;
                                    emailClient.Credentials = basicCredential;
                                }
                                emailClient.SendAsync(msg, userState);

                                currentRow.NoClientPOAuthorisedByDirectorID = fChildForm.intSelectedEmployeeID;
                                currentRow.NoClientPOAuthorisedByDirector = strSurname + ": " + strForename;
                                currentRow.NoClientPOEmailedToDirector = 1;
                                sp06498OMTenderEditBindingSource.EndEdit();

                                Check_Client_PO_Info(null, null, fChildForm.intSelectedEmployeeID);
                                SaveChanges(true);  // Commit the changes to the DB //
                                
                                if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                                DevExpress.XtraEditors.XtraMessageBox.Show("Authorisation email sent to selected Director successfully,", "Send Authorisation Email to Director", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                        catch (Exception ex)
                        {
                            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                            XtraMessageBox.Show("Unable to Send Authorisation Email to Director - an error occurred:\n\n" + ex.Message, "Send Authorisation Email to Director", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            if (boolValidationTurnedOffForNoClientPOAuthorisedByDirectorButtonEdit)  // Put error message back on column //
                            {
                                Set_NoClientPOAuthorisedByDirectorButtonEdit_Validation_Message(true);
                            }
                            return;
                        }
                        #endregion
                    }
                }
            }
            else if (e.Button.Tag.ToString() == "clear")
            {
                currentRow["NoClientPOAuthorisedByDirectorID"] = 0;
                currentRow["NoClientPOAuthorisedByDirector"] = "";
                sp06498OMTenderEditBindingSource.EndEdit();

                Check_Client_PO_Info(null, null, 0);
            }
        }

        private void Check_Client_PO_Info(string ClientPONumber, int? NoClientPO, int? NoClientPOAuthorisedByDirectorID)
        {
            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            if (!string.IsNullOrWhiteSpace(ClientPONumber))  // Value passed in so this field was just edited - Clear the No PO fields //
            {
                NoClientPOAuthorisedByDirectorButtonEdit.Properties.ReadOnly = true;
                NoClientPOAuthorisedByDirectorButtonEdit.Properties.Buttons[0].Enabled = false;
                NoClientPOAuthorisedByDirectorButtonEdit.Properties.Buttons[1].Enabled = false;
                currentRow["NoClientPO"] = 0;
                currentRow["NoClientPOAuthorisedByDirectorID"] = 0;
                currentRow["NoClientPOAuthorisedByDirector"] = "";
                currentRow["NoClientPOEmailedToDirector"] = 0;  // Clear Email sent flag //
                sp06498OMTenderEditBindingSource.EndEdit();
            }
            else if (NoClientPO > 0)  // clear the PO number //
            {
                NoClientPOAuthorisedByDirectorButtonEdit.Properties.ReadOnly = false;
                NoClientPOAuthorisedByDirectorButtonEdit.Properties.Buttons[0].Enabled = true;
                NoClientPOAuthorisedByDirectorButtonEdit.Properties.Buttons[1].Enabled = true;
                currentRow["ClientPONumber"] = "";
                sp06498OMTenderEditBindingSource.EndEdit();
            }

            string strClientPONumber = (ClientPONumber == null ? (string.IsNullOrWhiteSpace(currentRow.ClientPONumber) ? "" : currentRow.ClientPONumber) : ClientPONumber);
            int intNoClientPO = (NoClientPO == null ? currentRow.NoClientPO : Convert.ToInt32(NoClientPO));
            int intNoClientPOAuthorisedByDirectorID = (NoClientPOAuthorisedByDirectorID == null ? currentRow.NoClientPOAuthorisedByDirectorID : Convert.ToInt32(NoClientPOAuthorisedByDirectorID));

            if (string.IsNullOrWhiteSpace(strClientPONumber) && intNoClientPO <= 0)
            {
                dxErrorProvider1.SetError(ClientPONumberTextEdit, "Enter a Client PO Number or Tick No Client PO and Select Authorised By");
                dxErrorProvider1.SetError(NoClientPOCheckEdit, "Enter a Client PO Number or Tick No Client PO and Select Authorised By");
            }
            else
            {
                dxErrorProvider1.SetError(ClientPONumberTextEdit, "");
                dxErrorProvider1.SetError(NoClientPOCheckEdit, "");
            }
            if (intNoClientPO > 0 && intNoClientPOAuthorisedByDirectorID <= 0)
            {
                Set_NoClientPOAuthorisedByDirectorButtonEdit_Validation_Message(true);
            }
            else
            {
                Set_NoClientPOAuthorisedByDirectorButtonEdit_Validation_Message(false);
            }
        }
        private void Set_NoClientPOAuthorisedByDirectorButtonEdit_Validation_Message(bool error)
        {
            dxErrorProvider1.SetError(NoClientPOAuthorisedByDirectorButtonEdit, (error ? "Select Authorised By or un-tick No Client and enter a Client PO Number" : ""));
        }

        #endregion


        #region Tender Statuses

        private void TenderStatusButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
                var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
                if (currentRow == null) return;
                int intStatusID = 0;
                try
                {
                    intStatusID = (string.IsNullOrEmpty(currentRow.StatusID.ToString()) ? 0 : Convert.ToInt32(currentRow.StatusID));
                }

                catch (Exception) { }
                var fChildForm = new frm_OM_Select_Tender_Status();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalID = intStatusID;
                fChildForm._Mode = "single";
                fChildForm._IncludeBlank = 0;
                fChildForm._ExcludedStatusIDs = "";
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow.StatusID = fChildForm.intSelectedID;
                    currentRow.TenderStatus = fChildForm.strSelectedDescription1;
                    currentRow.ProgressBarValue = fChildForm.intProgressBarValue;
                    if (strFormMode == "blockedit")
                    {
                        currentRow.StatusIssueID = 0;
                        currentRow.StatusIsssue = "";
                        SetProgressBarColour();
                        sp06498OMTenderEditBindingSource.EndEdit();
                    }
                    else
                    {
                        sp06498OMTenderEditBindingSource.EndEdit();
                        Set_Enabled_Based_On_Status(currentRow.StatusID);
                    }
                }
            }
        }
        private void TenderStatusButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(TenderStatusButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(TenderStatusButtonEdit, "");
            }
        }

        private void StatusIsssueButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            if (e.Button.Tag.ToString() == "choose")
            {
                int intTenderStatusID = 0;
                string strTenderStatusDescription = "";
                try
                {
                    intTenderStatusID = (string.IsNullOrEmpty(currentRow.StatusID.ToString()) ? 0 : Convert.ToInt32(currentRow.StatusID));
                    strTenderStatusDescription = (string.IsNullOrEmpty(currentRow.TenderStatus.ToString()) ? "" : currentRow.TenderStatus);
                }
                catch (Exception) { };
                if (intTenderStatusID == 0)
                {
                    XtraMessageBox.Show("Please select the Tender Status before attempting to select the Status Issue.", "Select Status Issue", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                int intStatusIssueID = 0;
                try
                {
                    intStatusIssueID = (string.IsNullOrEmpty(currentRow.StatusIssueID.ToString()) ? 0 : Convert.ToInt32(currentRow.StatusIssueID));
                }
                catch (Exception) { };

                var fChildForm = new frm_OM_Select_Status_Issue_For_Status();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.strPassedInTenderStatusIDsFilter = intTenderStatusID.ToString() + ",";
                fChildForm.strPassedInTenderStatusDescriptionFilter = strTenderStatusDescription;
                fChildForm.intOriginalTenderStatusIssueID = intStatusIssueID;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow.StatusIssueID = fChildForm.intSelectedTenderStatusIssueID;
                    currentRow.StatusIsssue = fChildForm.strSelectedIssueDescription;
                    sp06498OMTenderEditBindingSource.EndEdit();
                    SetProgressBarColour();
                }
            }
            else if (e.Button.Tag.ToString() == "clear")
            {
                currentRow.StatusIssueID = 0;
                currentRow.StatusIsssue = "";
                sp06498OMTenderEditBindingSource.EndEdit();
                SetProgressBarColour();
            }
        }
        private void SetProgressBarColour()
        {
            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            int intStatusIssueID = -1;
            try { intStatusIssueID = (string.IsNullOrEmpty(currentRow.StatusIssueID.ToString()) ? 0 : Convert.ToInt32(currentRow.StatusIssueID)); }
            catch (Exception) { }

            if (intStatusIssueID > 0)
            {
                progressBarControl1.Properties.StartColor = Color.Red;
                progressBarControl1.Properties.EndColor = Color.Pink;
            }
            else
            {
                progressBarControl1.Properties.StartColor = Color.Green;
                progressBarControl1.Properties.EndColor = Color.PaleGreen;
            }
        }

        private void ddBtnSentToCM_Click(object sender, EventArgs e)
        {
            SendCMQuote();
        }
        private void bbiSendQuoteToCM_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SendCMQuote();
        }
        private void SendCMQuote()
        {
            if (strFormMode == "view" || strFormMode == "blockadd" || strFormMode == "blockedit") return;

            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            #region Initial Data checks
            int intCM = currentRow.CM;
            if (intCM <= 0)
            {
                XtraMessageBox.Show("Unable to Send Tender to Contract Manager - the CM has not been selected for this Tender. Please select the CM before proceeding.", "Send Tender to Contract Manager", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            int intClientID = currentRow.ClientID;
            int intSiteID = currentRow.SiteID;
            if (intClientID <= 0 || intSiteID <= 0)
            {
                XtraMessageBox.Show("Unable to Send Tender to Contract Manager - the Client and\\or Site has not been selected for this Tender. Please select the Client and Site before proceeding.", "Send Tender to Contract Manager", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            int intTenderID = currentRow.TenderID;
            string strMessage = CheckForPendingSave();
            if (intTenderID <= 0 || strMessage != "")
            {
                if (intTenderID <= 0)
                {
                    strMessage = "Unable to Send Tender to Contract Manager - you must save the Tender first.";
                }
                else 
                {
                    strMessage += "\nUnable to Send Tender to Contract Manager. You must save the changes first.";
                }
                strMessage+= "\n\nWould you like to save the change(s) now?";
                switch (XtraMessageBox.Show(strMessage, "Send Tender to Contract Manager", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.No:
                        return;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) return;  // Save Failed so abort process to allow user to correct //
                        break;
                }
            }
            intTenderID = currentRow.TenderID;  // Pick up TenderID in case the first save just occurred... //
            int intClientContractID = currentRow.ClientContractID;

            if (XtraMessageBox.Show("You are about to send the Quote to the Contract Manager.\n\nAre you sure you wish to proceed?", "Send Quote to CM", MessageBoxButtons.YesNo, MessageBoxIcon.Question, DefaultBoolean.True) == DialogResult.No) return;
            #endregion

            // Open screen to allow the user to imput any required comments //
            var fChildForm = new frm_OM_Tender_Send_CM_Email_Add_Comment();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strPassedInTenderIDs = intTenderID.ToString() + ",";
            if (fChildForm.ShowDialog() != DialogResult.OK) return;              
            
            string strCMComment = fChildForm._Comments;
            if (!string.IsNullOrWhiteSpace(strCMComment))  // Append to Tender CM Comment first and save to DB then pass to email // 
            {
                string strExistingComment = "";
                if (!DBNull.Value.Equals(currentRow["CMComments"])) strExistingComment = currentRow.CMComments;
                if (!string.IsNullOrWhiteSpace(strExistingComment)) strExistingComment += "\r\n\r\n";
                strExistingComment += "KAM Comment to CM via CM Quote\r\n" +
                                        "Date: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "\r\n" +
                                        "CM: " + (string.IsNullOrWhiteSpace(currentRow.CMName) ? "" : currentRow.CMName) + "\r\n" +
                                        "KAM: " + (string.IsNullOrWhiteSpace(currentRow.KAMName) ? "" : currentRow.KAMName) + "\r\n" +
                                        "Comment:\r\n" + strCMComment;
                currentRow.CMComments = strExistingComment;
                sp06498OMTenderEditBindingSource.EndEdit();
                SaveChanges(true);  // Commit the changes to the DB //
            }

            DateTime dtNow = DateTime.Now;
            char[] delimiters = new char[] { ',' };

            #region Get CM Email Address
            string strEmailAddresses = "";
            try
            {
                var GetSetting = new WoodPlanDataSetTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strEmailAddresses = GetSetting.sp00240_Get_Staff_Email_Address(intCM).ToString();
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the CM Email Address from the Staff table.\n\nError: " + ex.Message + "\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get CM Email Address", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (string.IsNullOrWhiteSpace(strEmailAddresses))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The CM has no Email address stored against their name in the Staff tabel. Please update the CM Staff record before trying again.", "Get CM Email Address", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            #endregion

            splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager.ShowWaitForm();
            splashScreenManager.SetWaitFormDescription("Sending Tender to CM...");

            try
            {
                #region Get Email Settings
                string strHTMLBodyFile = "";
                string strEmailFrom = "";
                string strEmailSubjectLine = "";
                string strCCToEmailAddress = "";

                string strSMTPMailServerAddress = "";
                string strSMTPMailServerUsername = "";
                string strSMTPMailServerPassword = "";
                string strSMTPMailServerPort = "";
                string strLinkedNotesDocument = "";

                SqlDataAdapter sdaSettings = new SqlDataAdapter();
                DataSet dsSettings = new DataSet("NewDataSet");

                using (var conn = new SqlConnection(strConnectionString))
                {
                    conn.Open();
                    using (var cmd = new SqlCommand())
                    {
                        cmd.CommandText = "sp06518_OM_Tender_CM_Email_Get_Email_Settings";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@ClientContractID", intClientContractID));
                        cmd.Parameters.Add(new SqlParameter("@TenderID", intTenderID));
                        cmd.Parameters.Add(new SqlParameter("@StaffID", GlobalSettings.UserID));
                        cmd.Connection = conn;
                        sdaSettings = new SqlDataAdapter(cmd);
                        sdaSettings.Fill(dsSettings, "Table");
                    }
                    conn.Close();
                }
                if (dsSettings.Tables[0].Rows.Count != 1)
                {
                    if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the 'Email Settings' (from the System Configuration Screen) - number of rows returned not equal to 1.\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                DataRow dr1 = dsSettings.Tables[0].Rows[0];
                strHTMLBodyFile = dr1["BodyFileName"].ToString();
                strEmailFrom = dr1["EmailFrom"].ToString();
                strEmailSubjectLine = dr1["SubjectLine"].ToString();
                strCCToEmailAddress = dr1["CCToName"].ToString();
                
                strLinkedNotesDocument = dr1["LinkedNotesDocument"].ToString();
                if (!string.IsNullOrWhiteSpace(strLinkedNotesDocument) && !string.IsNullOrWhiteSpace(i_str_TenderRequestNotesFolder)) strLinkedNotesDocument = Path.Combine(i_str_TenderRequestNotesFolder, strLinkedNotesDocument);

                strSMTPMailServerAddress = dr1["SMTPMailServerAddress"].ToString();
                strSMTPMailServerUsername = dr1["SMTPMailServerUsername"].ToString();
                strSMTPMailServerPassword = dr1["SMTPMailServerPassword"].ToString();
                strSMTPMailServerPort = dr1["SMTPMailServerPort"].ToString();
                if (string.IsNullOrEmpty(strSMTPMailServerPort) || !CheckingFunctions.IsNumeric(strSMTPMailServerPort)) strSMTPMailServerPort = "0";
                int intSMTPMailServerPort = Convert.ToInt32(strSMTPMailServerPort);

                if (string.IsNullOrEmpty(strHTMLBodyFile) || string.IsNullOrEmpty(strEmailFrom) || string.IsNullOrEmpty(strSMTPMailServerAddress))
                {
                    if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more of the email settings (Email Layout File, From Email Address and SMTP Mail Server Name) are missing from the System Configuration Screen.\n\nPlease update the System Settings then try again. If the problem persists, contact Technical Support.", "Get Email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                #endregion


                #region Ping Test
                bool boolNoInternet = BaseObjects.PingTest.Ping("www.google.com");
                if (!boolNoInternet) boolNoInternet = BaseObjects.PingTest.Ping("www.microsoft.com");  // try another site just in case that one is down //
                if (!boolNoInternet) boolNoInternet = BaseObjects.PingTest.Ping("www.yahoo.com");  // try another site just in case that one is down //
                if (!boolNoInternet) // alert user and halt process //
                {
                    if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to connect to the internet - unable to send.\n\nPlease check your internet connection then try again.\n\nIf the problem persists, contact Technical Support.", "Check Internet Connection", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                #endregion

                using (var conn = new SqlConnection(strConnectionString))
                {
                    conn.Open();
                    SqlDataAdapter sda = new SqlDataAdapter();
                    DataSet ds = new DataSet("NewDataSet");
                    using (var cmd = new SqlCommand())
                    {
                        cmd.CommandText = "sp06517_OM_Tender_CM_Email_Data";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@TenderID", intTenderID));
                        cmd.Connection = conn;
                        sda = new SqlDataAdapter(cmd);
                        sda.Fill(ds, "Table");
                    }
                    conn.Close();
                    string strBody = System.IO.File.ReadAllText(strHTMLBodyFile);

                    // Merge the data into the HTML //
                    DataRow dr = ds.Tables[0].Rows[0];
                    foreach (DataColumn dc in ds.Tables[0].Columns)
                    {
                        //strBody = strBody.Replace("%" + dc.Caption + "%", dr[dc].ToString());
                        strBody = strBody.Replace("%" + dc.Caption + "%", dr[dc].ToString().Replace("\r\n", "<br />"));
                    }

                    System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                    msg.From = new System.Net.Mail.MailAddress(strEmailFrom);
                    string[] strEmailTo = strEmailAddresses.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    if (strEmailTo.Length > 0)
                    {
                        foreach (string strEmailAddress in strEmailTo)
                        {
                            msg.To.Add(new System.Net.Mail.MailAddress(strEmailAddress));
                        }
                    }
                    else
                    {
                        msg.To.Add(new System.Net.Mail.MailAddress(strEmailAddresses));  // Original value wouldn't split as no commas so it's just one email address so use it //
                    }
                    msg.Subject = strEmailSubjectLine;
                    if (!string.IsNullOrEmpty(strCCToEmailAddress)) msg.CC.Add(strCCToEmailAddress);

                    // Add the person sending to the CC To Email so they have a copy for their own records //
                    msg.CC.Add(strEmailFrom);

                    msg.Priority = System.Net.Mail.MailPriority.High;
                    msg.IsBodyHtml = true;

                    System.Net.Mail.AlternateView plainView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(System.Text.RegularExpressions.Regex.Replace(strBody, @"<(.|\n)*?>", string.Empty), null, "text/plain");
                    System.Net.Mail.AlternateView htmlView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(strBody, null, "text/html");

                    // Create a new attachment //
                    if (!string.IsNullOrWhiteSpace(strLinkedNotesDocument))
                    {
                        System.Net.Mail.Attachment mailAttachment = new System.Net.Mail.Attachment(strLinkedNotesDocument);  // Attach Quote Notes Template file from client contract //
                        msg.Attachments.Add(mailAttachment);
                    }


                    // Process any linked documents selected on the previous screen and add them as attachments //
                    GridView view = (GridView)fChildForm.gridControl3.MainView;
                    if (fChildForm.selection1.SelectedCount > 0 && view.RowCount > 0 )
                    {
                        string strLinkedDocumentPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //
                        DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                        try
                        {
                            strLinkedDocumentPath = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_TenderLinkedDocumentPath").ToString();
                        }
                        catch (Exception) { }
                        //if (!strLinkedDocumentPath.EndsWith("\\")) strLinkedDocumentPath += "\\";

                        for (int i = 0; i < view.DataRowCount; i++)
                        {
                            if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                            {
                                string strLinkedDocument = Path.Combine(strLinkedDocumentPath + Convert.ToString(view.GetRowCellValue(i, "DocumentPath")));
                                System.Net.Mail.Attachment mailAttachment = new System.Net.Mail.Attachment(strLinkedDocument);  // Create a new attachment for linked document //
                                msg.Attachments.Add(mailAttachment);  // Attach linked document to email //
                            }
                        }
                    }

                    msg.AlternateViews.Add(plainView);
                    msg.AlternateViews.Add(htmlView);

                    object userState = msg;
                    System.Net.Mail.SmtpClient emailClient = null;
                    if (intSMTPMailServerPort != 0)
                    {
                        emailClient = new System.Net.Mail.SmtpClient(strSMTPMailServerAddress, intSMTPMailServerPort);
                    }
                    else
                    {
                        emailClient = new System.Net.Mail.SmtpClient(strSMTPMailServerAddress);
                    }
                    if (!string.IsNullOrEmpty(strSMTPMailServerUsername) && !string.IsNullOrEmpty(strSMTPMailServerPassword))
                    {
                        System.Net.NetworkCredential basicCredential = new System.Net.NetworkCredential(strSMTPMailServerUsername, strSMTPMailServerPassword);
                        emailClient.UseDefaultCredentials = false;
                        emailClient.Credentials = basicCredential;
                    }
                    emailClient.SendAsync(msg, userState);

                    currentRow.SubmittedToCMDate = dtNow;

                    int intCurrentStatusID = currentRow.StatusID;
                    if (intCurrentStatusID < 20)
                    {
                        currentRow.StatusID = 20;
                        currentRow.TenderStatus = "Tender Awaiting Quote (CM)";
                        currentRow.ProgressBarValue = 20;
                        currentRow.StatusIssueID = 0;
                        currentRow.StatusIsssue = "";
                        currentRow.QuoteNotRequired = 0;
                    }
                    sp06498OMTenderEditBindingSource.EndEdit();
                    Set_Enabled_Based_On_Status(20);
                    SetProgressBarColour();

                    SaveChanges(true);  // Commit the changes to the DB //
                }
            }
            catch (Exception ex)
            {
                if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                XtraMessageBox.Show("Unable to Send Tender to Contract Manager - an error occurred:\n\n" + ex.Message, "Send Tender to Contract Manager", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
            DevExpress.XtraEditors.XtraMessageBox.Show("Tender Sent to Contract Manager successfully,", "Send Tender to Contract Manager", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void bbiNoQuoteRequired_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = true;
            if (this.strFormMode == "blockedit") return;

            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            int intSiteID = 0;
            try { intSiteID = (string.IsNullOrEmpty(currentRow.SiteID.ToString()) ? 0 : Convert.ToInt32(currentRow.SiteID)); }
            catch (Exception) { };
            if (intSiteID == 0)
            {
                XtraMessageBox.Show("Please select the Site before attempting to set as No Quote Required.", "Set as No Quote Required", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            
            int intCurrentStatusID = currentRow.StatusID;
            if (intCurrentStatusID < 30)
            {
                currentRow.StatusID = 30;
                currentRow.TenderStatus = "Tender Awaiting Authorisation (KAM)";
                currentRow.ProgressBarValue = 30;
                currentRow.StatusIssueID = 0;
                currentRow.StatusIsssue = "";
                currentRow.QuoteNotRequired = 1;
            }
            sp06498OMTenderEditBindingSource.EndEdit();
            Set_Enabled_Based_On_Status(30);
            SetProgressBarColour();
            ddBtnSentToCM.Enabled = false;  // Need this called again here as it's doesn't want to disable the calling button from the called Set_Enabled_Based_On_Status() for some weird reason //
        }

        private void btnReturnedFromCM_Click(object sender, EventArgs e)
        {
            if (strFormMode == "view" || strFormMode == "blockadd" || strFormMode == "blockedit") return;
            if (XtraMessageBox.Show("You have clicked CM Returned Quote - This will set the Tender as Tender Awaiting Authorisation (KAM).\n\nAre you sure you wish to proceed?", "CM Returned Quote", MessageBoxButtons.YesNo, MessageBoxIcon.Question, DefaultBoolean.True) == DialogResult.No) return;

            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            int intCurrentStatusID = currentRow.StatusID;
            if (intCurrentStatusID < 30)
            {
                currentRow.StatusID = 30;
                currentRow.TenderStatus = "Tender Awaiting Authorisation (KAM)";
                currentRow.ProgressBarValue = 30;
                currentRow.StatusIssueID = 0;
                currentRow.StatusIsssue = "";
            }
            sp06498OMTenderEditBindingSource.EndEdit();
            Set_Enabled_Based_On_Status(30);
            SetProgressBarColour();
        }

        private void ddBtnKAMAuthorised_Click(object sender, EventArgs e)
        {
            KAMAccepted();
        }
        private void bbiKAMAccepted_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            KAMAccepted();
        }
        private void KAMAccepted()
        {
            if (strFormMode == "view" || strFormMode == "blockadd" || strFormMode == "blockedit") return;
            if (XtraMessageBox.Show("You have clicked KAM Authorised - This will set the Tender as Ready To Send To Client.\n\nAre you sure you wish to proceed?", "KAM Authorised Quote", MessageBoxButtons.YesNo, MessageBoxIcon.Question, DefaultBoolean.True) == DialogResult.No) return;

            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            int intCurrentStatusID = currentRow.StatusID;
            if (intCurrentStatusID < 40)
            {
                currentRow.StatusID = 40;
                currentRow.TenderStatus = "Tender Ready To Send To Client";
                currentRow.ProgressBarValue = 40;
                currentRow.StatusIssueID = 0;
                currentRow.StatusIsssue = "";
            }
            sp06498OMTenderEditBindingSource.EndEdit();
            Set_Enabled_Based_On_Status(40);
            SetProgressBarColour();
        }

        private void bbiKAMRejected_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (strFormMode == "view" || strFormMode == "blockadd" || strFormMode == "blockedit") return;
            
            if (XtraMessageBox.Show("You have clicked KAM Rejected - You will asked to specify what to do next.\n\nAre you sure you wish to proceed?", "KAM Rejected Quote", MessageBoxButtons.YesNo, MessageBoxIcon.Question, DefaultBoolean.True) == DialogResult.No) return;
            
            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;


            var fChildForm = new frm_OM_Tender_KAM_Reject_Next_Action();
            fChildForm.GlobalSettings = this.GlobalSettings;
            if (fChildForm.ShowDialog() != DialogResult.OK) return;

            currentRow.KAMQuoteRejectionReasonID = fChildForm._RejectionReasonID;
            currentRow.KAMQuoteRejectionReason = fChildForm._RejectionReason;
            currentRow.KAMQuoteRejectionRemarks = fChildForm._KAMQuoteRejectionRemarks;

            if (fChildForm._NextStepID == 1)  // Resend to CM //
            {
                currentRow.StatusID = 10;
                currentRow.TenderStatus = "Tender Created";
                currentRow.ProgressBarValue = 10;
                sp06498OMTenderEditBindingSource.EndEdit();
                Set_Enabled_Based_On_Status(10);
            }
            else  // Cancel Tender //
            {
                currentRow.StatusID = 100;
                currentRow.TenderStatus = "Tender Declined (Internal)";
                currentRow.ProgressBarValue = 100;
                sp06498OMTenderEditBindingSource.EndEdit();
                Set_Enabled_Based_On_Status(100);
            }
            SetProgressBarColour();
        }

        private void btnSendToClient_Click(object sender, EventArgs e)
        {
            if (strFormMode == "view" || strFormMode == "blockadd" || strFormMode == "blockedit") return;
            if (XtraMessageBox.Show("You have clicked Sent To Client - This will set the Tender as Tender Sent To Client and the current Date\\Time will be stored as when it was sent to the client.\n\nAre you sure you wish to proceed?", "Tender Sent to Client", MessageBoxButtons.YesNo, MessageBoxIcon.Question, DefaultBoolean.True) == DialogResult.No) return;

            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            decimal decQuotedTotalSell = currentRow.QuotedTotalSell;
          
            DateTime? dtQuoteSubmittedToClientDate = null;
            try { dtQuoteSubmittedToClientDate = currentRow.QuoteSubmittedToClientDate; }
            catch (Exception) { }

            if (decQuotedTotalSell <= (decimal)0.00 || dtQuoteSubmittedToClientDate == null)
            {
                string strMessage = "";
                if (decQuotedTotalSell <= (decimal)0.00) strMessage = "You have not entered any quoted costs for the Client Price.\n\n";
                if (dtQuoteSubmittedToClientDate == null) strMessage += "You have not entered any Quote Sent To Client Date - if you proceed, the system will enter the current date and time.\n\n";
                strMessage += "Are you sure you wish to proceed?";
                if (XtraMessageBox.Show(strMessage, "Tender Sent to Client", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, DefaultBoolean.True) == DialogResult.No) return;
            }

            // currentRow.QuoteSubmittedToClientDate = DateTime.Now;  // Disabled - JIRA ICE-450 //
            int intCurrentStatusID = currentRow.StatusID;
            if (intCurrentStatusID < 50)
            {
                currentRow.StatusID = 50;
                currentRow.TenderStatus = "Tender Sent To Client";
                currentRow.ProgressBarValue = 50;
                currentRow.StatusIssueID = 0;
                currentRow.StatusIsssue = "";
                if (dtQuoteSubmittedToClientDate == null) currentRow.QuoteSubmittedToClientDate = DateTime.Now;
            }
            sp06498OMTenderEditBindingSource.EndEdit();
            Set_Enabled_Based_On_Status(50);
            SetProgressBarColour();
        }

        private void ddBtnClientResponse_Click(object sender, EventArgs e)
        {
            ClientAccepted();
        }
        private void bbiClientAccepted_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ClientAccepted();
        }
        private void ClientAccepted()
        {
            if (strFormMode == "view" || strFormMode == "blockadd" || strFormMode == "blockedit") return;
            if (XtraMessageBox.Show("You have clicked Client Accepted Quote - This will set the Tender as Tender Accepted.\n\nAre you sure you wish to proceed?", "Tender Accepted", MessageBoxButtons.YesNo, MessageBoxIcon.Question, DefaultBoolean.True) == DialogResult.No) return;

            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            currentRow.QuoteAcceptedByClientDate = DateTime.Now;
            int intCurrentStatusID = currentRow.StatusID;
            if (intCurrentStatusID < 80)
            {
                currentRow.StatusID = 80;
                currentRow.TenderStatus = "Tender Accepted By Client";
                currentRow.ProgressBarValue = 60;
                currentRow.StatusIssueID = 0;
                currentRow.StatusIsssue = "";
                currentRow.QuoteAcceptedByClientDate = DateTime.Now;
            }
            sp06498OMTenderEditBindingSource.EndEdit();
            Set_Enabled_Based_On_Status(80);
            SetProgressBarColour();
        }

        private void bbiClientOnHold_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (strFormMode == "view" || strFormMode == "blockadd" || strFormMode == "blockedit") return;
            if (XtraMessageBox.Show("You have clicked Client On-hold Quote - This will set the Tender as Tender On-hold.\n\nAre you sure you wish to proceed?", "Tender On-hold", MessageBoxButtons.YesNo, MessageBoxIcon.Question, DefaultBoolean.True) == DialogResult.No) return;

            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            int intCurrentStatusID = currentRow.StatusID;
            if (intCurrentStatusID < 60)
            {
                currentRow.StatusID = 60;
                currentRow.TenderStatus = "Tender On-hold By Client";
                currentRow.ProgressBarValue = 50;
                currentRow.StatusIssueID = 0;
                currentRow.StatusIsssue = "";
            }
            sp06498OMTenderEditBindingSource.EndEdit();
            Set_Enabled_Based_On_Status(60);
            SetProgressBarColour();
        }
        
        private void bbiClientRejected_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (strFormMode == "view" || strFormMode == "blockadd" || strFormMode == "blockedit") return;
            if (XtraMessageBox.Show("You have clicked Client Declined Quote - This will set the Tender as Tender Declined.\n\nAre you sure you wish to proceed?", "Tender Declined By Client", MessageBoxButtons.YesNo, MessageBoxIcon.Question, DefaultBoolean.True) == DialogResult.No) return;

            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            DialogResult dr = XtraMessageBox.Show("Would you like to Re-tender the quote?\n\n--> Click <b>Yes</b> to re-tender the quote.\n\n--> Click <b>No</b> to set the status to Tender Declined By Client. If select this you must select a Reason.\n\n--> Click <b>Cancel</b> to abort the status change.", "Tender Declined By Client", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, DefaultBoolean.True);
            switch (dr)
            {
                case DialogResult.Cancel:
                    return;
                case DialogResult.No:
                    {
                        // Get user to pick a reason //
                        var fChildForm = new frm_OM_Select_Status_Issue_For_Status();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInTenderStatusIDsFilter = "70,";
                        fChildForm.strPassedInTenderStatusDescriptionFilter = "Tender Declined By Client";
                        fChildForm.intOriginalTenderStatusIssueID = 70;
                        if (fChildForm.ShowDialog() != DialogResult.OK) return;
                        
                        currentRow.StatusIssueID = fChildForm.intSelectedTenderStatusIssueID;
                        currentRow.StatusIsssue = fChildForm.strSelectedIssueDescription;

                        int intCurrentStatusID = currentRow.StatusID;
                        if (intCurrentStatusID < 70)
                        {
                            currentRow.StatusID = 70;
                            currentRow.TenderStatus = "Tender Declined By Client";
                            currentRow.ProgressBarValue = 70;
                        }
                        sp06498OMTenderEditBindingSource.EndEdit();
                        Set_Enabled_Based_On_Status(70);
                        SetProgressBarColour();
                    }
                    break;
                case DialogResult.Yes:
                    {
                        int intCurrentStatusID = currentRow.StatusID;
                        if (intCurrentStatusID != 10)
                        {
                            currentRow.StatusID = 10;
                            currentRow.TenderStatus = "Tender Created";
                            currentRow.ProgressBarValue = 10;
                            currentRow.StatusIssueID = 0;
                            currentRow.StatusIsssue = "";
                        }
                        sp06498OMTenderEditBindingSource.EndEdit();
                        Set_Enabled_Based_On_Status(10);
                        SetProgressBarColour();
                    }
                    break;
                default:
                    break;
            }


        }

        private void btnCreateVisits_Click(object sender, EventArgs e)
        {
            if (strFormMode == "view" || strFormMode == "blockadd" || strFormMode == "blockedit") return;

            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            if (currentRow.TreeProtected == 1 && currentRow.PlanningAuthorityOkToProceed <= 0)
            {
                XtraMessageBox.Show("You have Tree Protected ticked within the TPO section but Ok To Proceed within the Planning section is not ticked.\n\n<color=red>You cannot start works until you have Planning Permissions.</color>\n\nPlease complete the Planning section and tick Ok to Proceed before continuing.", "Create Visits", MessageBoxButtons.OK, MessageBoxIcon.Stop, DefaultBoolean.True);
                return;
            }

            int intSiteID = 0;
            try { intSiteID = (string.IsNullOrEmpty(currentRow.SiteID.ToString()) ? 0 : Convert.ToInt32(currentRow.SiteID)); }
            catch (Exception) { };
            if (intSiteID == 0)
            {
                XtraMessageBox.Show("Please select the Site before attempting to create Visits.", "Create Visits", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            int intNoClientPO = 0;
            try { intNoClientPO = (string.IsNullOrEmpty(currentRow.NoClientPO.ToString()) ? 0 : Convert.ToInt32(currentRow.NoClientPO)); }
            catch (Exception) { };
            string strClientPoNumber = currentRow.ClientPONumber.ToString();
            if (intNoClientPO == 0 && string.IsNullOrWhiteSpace(strClientPoNumber))
            {
                XtraMessageBox.Show("Please input a Client PO Number or tick No Client PO before attempting to create Visits.", "Create Visits", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (XtraMessageBox.Show("You have clicked Create Visits - This will open the Visit Wizard screen to allow the creation of one or more visits and jobs.\n\nAre you sure you wish to proceed?", "Create Visits", MessageBoxButtons.YesNo, MessageBoxIcon.Question, DefaultBoolean.True) == DialogResult.No) return;

            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nThere are outstanding changes pending on the screen. You must save these before proceeding with Creating Visits.\n\nWould you like to save the change(s) now?";
                switch (XtraMessageBox.Show(strMessage, "Create Visits", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.No:
                        return;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) return;  // Save Failed so abort process to allow user to correct //
                        break;
                }
            }

            // Open Visit Wizard //
            var fChildForm = new frm_OM_Tender_Visit_Wizard();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strRecordIDs = "";
            fChildForm.strFormMode = "add";
            fChildForm.strCaller = this.Name;
            fChildForm.intRecordCount = 0;
            fChildForm.i_intPassedInTenderID = currentRow.TenderID;
            fChildForm.i_strPassedInTenderRevisionNumber = currentRow.RevisionNumber;
            fChildForm.i_intPassedInClientContractID = currentRow.ClientContractID;
            fChildForm.i_intPassedInClientID = currentRow.ClientID;
            fChildForm.i_intPassedInSiteID = intSiteID;
            fChildForm.i_strPassedInClientName = currentRow.ClientName;
            fChildForm.i_strPassedInClientContractDescription = currentRow.ContractDescription;
            fChildForm.i_strPassedInSiteName = currentRow.SiteName;
            fChildForm.i_intPassedInSiteContractID = currentRow.SiteContractID;
            fChildForm.i_strPassedInWorkDescription = currentRow.JobSubTypeDescription;
            
            fChildForm.i_decPassedInTenderLabourCost = currentRow.ActualLabourCost;
            fChildForm.i_decPassedInTenderMaterialCost = currentRow.ActualMaterialCost;
            fChildForm.i_decPassedInTenderEquipmentCost = currentRow.ActualEquipmentCost;
            fChildForm.i_decPassedInTenderTotalCost = currentRow.ActualTotalCost;
            
            fChildForm.i_decPassedInTenderLabourSell = currentRow.AcceptedLabourSell;
            fChildForm.i_decPassedInTenderMaterialSell = currentRow.AcceptedMaterialSell;
            fChildForm.i_decPassedInTenderEquipmentSell = currentRow.AcceptedEquipmentSell;
            fChildForm.i_decPassedInTenderTotalSell = currentRow.AcceptedTotalSell;

            fChildForm.i_intPassedInJobSubTypeID = currentRow.WorkSubTypeID;
            fChildForm.i_intPassedInLabourTypeID = currentRow.ProposedLabourTypeID;
            fChildForm.i_intPassedInLabourID = currentRow.ProposedLabourID;
            fChildForm.i_strPassedInLabourName = currentRow.ProposedLabour;
            fChildForm.i_dbPassedInLabourLatitude = currentRow.ProposedLabourLatitude;
            fChildForm.i_dbPassedInLabourLongitude = currentRow.ProposedLabourLongitude;
            fChildForm.i_strPassedInLabourPostcode = currentRow.ProposedLabourPostcode;
            try
            {
                fChildForm.i_dtPassedInWorkCompletedBy = currentRow.RequiredWorkCompletedDate;
            }
            catch (Exception) { }

            fChildForm.frmCaller = this; // Enable link so we can write back the number of created visits //

            fChildForm.FormPermissions = this.FormPermissions;
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();
            fChildForm.Show();

            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
        }
        public void SetTenderVisitCount(int TenderID, int NewVisitCount)
        {
            try
            {
                foreach (DataRow dr in dataSet_OM_Tender.sp06498_OM_Tender_Edit.Rows)
                {
                    if (Convert.ToInt32(dr["TenderID"]) == TenderID)
                    {
                        dr["VisitCount"] = Convert.ToInt32(dr["VisitCount"]) + NewVisitCount;
                    }
                }
                SaveChanges(true);  // Commit the changes to the DB //
            }
            catch (Exception) { }
        }
        public void SetTenderJobCount(int TenderID, int NewJobCount)
        {
            try
            {
                foreach (DataRow dr in dataSet_OM_Tender.sp06498_OM_Tender_Edit.Rows)
                {
                    if (Convert.ToInt32(dr["TenderID"]) == TenderID)
                    {
                        dr["JobCount"] = Convert.ToInt32(dr["JobCount"]) + NewJobCount;
                    }
                }
                SaveChanges(true);  // Commit the changes to the DB //
            }
            catch (Exception) { }
        }


        private void btnTenderClosed_Click(object sender, EventArgs e)
        {
            if (strFormMode == "view" || strFormMode == "blockadd" || strFormMode == "blockedit") return;

            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            int intVisits = currentRow.VisitCount;
            int intJobs = currentRow.JobCount;
            if (intVisits <= 0 || intJobs <= 0)
            {
                XtraMessageBox.Show("You must create at least one visit with at least on job before the tender can be set as Tender Closed.", "Tender Closed", MessageBoxButtons.OK, MessageBoxIcon.Stop, DefaultBoolean.True);
                return;
            }

            if (XtraMessageBox.Show("You have clicked Tender Closed - This will set the Tender as Tender Closed and lock the record to prevent any further changes - are you sure you wish to proceed?", "Tender Closed", MessageBoxButtons.YesNo, MessageBoxIcon.Question, DefaultBoolean.True) == DialogResult.No) return;

            int intCurrentStatusID = currentRow.StatusID;
            if (intCurrentStatusID < 90)
            {
                currentRow.StatusID = 90;
                currentRow.TenderStatus = "Tender Closed";
                currentRow.ProgressBarValue = 70;
            }
            sp06498OMTenderEditBindingSource.EndEdit();
            Set_Enabled_Based_On_Status(90);
            SetProgressBarColour();
        }

        private void btnCancelTender_Click(object sender, EventArgs e)
        {
            if (strFormMode == "view" || strFormMode == "blockadd" || strFormMode == "blockedit") return;

            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            if (XtraMessageBox.Show("You have clicked Cancel Tender - <color=red>This will set the Tender as Tender Declined (Internal)</color> and lock the record to prevent any further changes!\n\nAre you sure you wish to proceed?\n\nIf you proceed you must select a Reason.", "Tender Declined (Internal)", MessageBoxButtons.YesNo, MessageBoxIcon.Question, DefaultBoolean.True) == DialogResult.No) return;

            var fChildForm = new frm_OM_Select_Status_Issue_For_Status();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strPassedInTenderStatusIDsFilter = "100,";
            fChildForm.strPassedInTenderStatusDescriptionFilter = "Tender Declined (Internal)";
            fChildForm.intOriginalTenderStatusIssueID = 100;
            if (fChildForm.ShowDialog() != DialogResult.OK) return;

            currentRow.StatusIssueID = fChildForm.intSelectedTenderStatusIssueID;
            currentRow.StatusIsssue = fChildForm.strSelectedIssueDescription;

            int intCurrentStatusID = currentRow.StatusID;
            if (intCurrentStatusID < 100)
            {
                currentRow.StatusID = 100;
                currentRow.TenderStatus = "Tender Declined (Internal)";
                currentRow.ProgressBarValue = 70;
            }
            sp06498OMTenderEditBindingSource.EndEdit();
            Set_Enabled_Based_On_Status(100);
            SetProgressBarColour();

        }

        private void Set_Enabled_Based_On_Status(int? StatusID)
        {
            if (ibool_FormStillLoading) return;
            if (strFormMode == "view" || strFormMode == "blockadd") return;
            
            if (strFormMode == "blockedit")  // Disable all buttons //
            {
                ddBtnSentToCM.Enabled = false;
                btnReturnedFromCM.Enabled = false;
                ddBtnKAMAuthorised.Enabled = false;
                btnSendToClient.Enabled = false;
                ddBtnClientResponse.Enabled = false;
                btnCreateVisits.Enabled = false;
                btnTenderClosed.Enabled = false;
                btnCancelTender.Enabled = false;
                return;
            }

            var currentRowView = (DataRowView)sp06498OMTenderEditBindingSource.Current;
            var currentRow = (DataSet_OM_Tender.sp06498_OM_Tender_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            int intStatusID = (StatusID == null ? currentRow.StatusID : Convert.ToInt32(StatusID));
            
            switch (intStatusID)
            {
                case 10:  // Tender Created //
                    {
                        ddBtnSentToCM.Enabled = true;
                        btnReturnedFromCM.Enabled = false;
                        ddBtnKAMAuthorised.Enabled = false;
                        btnSendToClient.Enabled = false;
                        ddBtnClientResponse.Enabled = false;
                        btnCreateVisits.Enabled = false;
                        btnTenderClosed.Enabled = false;
                        btnCancelTender.Enabled = true;

                        if (currentRow.StatusIssueID != 0) currentRow.StatusIssueID = 0;
                        if (currentRow.StatusIsssue != "") currentRow.StatusIsssue = "";
                        if (currentRow["SubmittedToCMDate"] != DBNull.Value) currentRow["SubmittedToCMDate"] = DBNull.Value;
                        if (currentRow.QuoteNotRequired != 0) currentRow.QuoteNotRequired = 0;
                        if (currentRow.KAMQuoteRejectionReasonID != 0) currentRow.KAMQuoteRejectionReasonID = 0;
                        if (currentRow.KAMQuoteRejectionReason != "") currentRow.StatusIsssue = "";
                        if (currentRow["QuoteSubmittedToClientDate"] != DBNull.Value) currentRow["QuoteSubmittedToClientDate"] = DBNull.Value;
                        if (currentRow["QuoteAcceptedByClientDate"] != DBNull.Value) currentRow["QuoteAcceptedByClientDate"] = DBNull.Value;
                    }
                    break;
                case 20:  // Tender Awaiting Quote (CM) //
                    {
                        ddBtnSentToCM.Enabled = false;
                        btnReturnedFromCM.Enabled = true;
                        ddBtnKAMAuthorised.Enabled = false;
                        btnSendToClient.Enabled = false;
                        ddBtnClientResponse.Enabled = false;
                        btnCreateVisits.Enabled = false;
                        btnTenderClosed.Enabled = false;
                        btnCancelTender.Enabled = true;

                        if (currentRow.StatusIssueID != 0) currentRow.StatusIssueID = 0;
                        if (currentRow.StatusIsssue != "") currentRow.StatusIsssue = "";
                        if (currentRow.KAMQuoteRejectionReasonID != 0) currentRow.KAMQuoteRejectionReasonID = 0;
                        if (currentRow.KAMQuoteRejectionReason != "") currentRow.StatusIsssue = "";
                        if (currentRow["QuoteSubmittedToClientDate"] != DBNull.Value) currentRow["QuoteSubmittedToClientDate"] = DBNull.Value;
                        if (currentRow["QuoteAcceptedByClientDate"] != DBNull.Value) currentRow["QuoteAcceptedByClientDate"] = DBNull.Value;
                    }
                    break;
                case 30:  // Tender Awaiting Authorisation (KAM) //
                    {
                        ddBtnSentToCM.Enabled = false;
                        btnReturnedFromCM.Enabled = false;
                        ddBtnKAMAuthorised.Enabled = true;
                        btnSendToClient.Enabled = false;
                        ddBtnClientResponse.Enabled = false;
                        btnCreateVisits.Enabled = false;
                        btnTenderClosed.Enabled = false;
                        btnCancelTender.Enabled = true;

                        if (currentRow.StatusIssueID != 0) currentRow.StatusIssueID = 0;
                        if (currentRow.StatusIsssue != "") currentRow.StatusIsssue = "";
                        if (currentRow.KAMQuoteRejectionReasonID != 0) currentRow.KAMQuoteRejectionReasonID = 0;
                        if (currentRow.KAMQuoteRejectionReason != "") currentRow.StatusIsssue = "";
                        if (currentRow["QuoteSubmittedToClientDate"] != DBNull.Value) currentRow["QuoteSubmittedToClientDate"] = DBNull.Value;
                        if (currentRow["QuoteAcceptedByClientDate"] != DBNull.Value) currentRow["QuoteAcceptedByClientDate"] = DBNull.Value;
                    }
                    break;
                case 40:  // Tender Ready To Send To Client //
                    {
                        ddBtnSentToCM.Enabled = false;
                        btnReturnedFromCM.Enabled = false;
                        ddBtnKAMAuthorised.Enabled = false;
                        btnSendToClient.Enabled = true;
                        ddBtnClientResponse.Enabled = false;
                        btnCreateVisits.Enabled = false;
                        btnTenderClosed.Enabled = false;
                        btnCancelTender.Enabled = true;

                        if (currentRow.KAMQuoteRejectionReasonID != 0) currentRow.KAMQuoteRejectionReasonID = 0;
                        if (currentRow.KAMQuoteRejectionReason != "") currentRow.KAMQuoteRejectionReason = "";
                        if (currentRow.StatusIssueID != 0) currentRow.StatusIssueID = 0;
                        if (currentRow.StatusIsssue != "") currentRow.StatusIsssue = "";
                        if (currentRow["QuoteSubmittedToClientDate"] != DBNull.Value) currentRow["QuoteSubmittedToClientDate"] = DBNull.Value;
                        if (currentRow["QuoteAcceptedByClientDate"] != DBNull.Value) currentRow["QuoteAcceptedByClientDate"] = DBNull.Value;
                    }
                    break;
                case 50:  // Tender Sent To Client //
                    {
                        ddBtnSentToCM.Enabled = false;
                        btnReturnedFromCM.Enabled = false;
                        ddBtnKAMAuthorised.Enabled = false;
                        btnSendToClient.Enabled = false;
                        ddBtnClientResponse.Enabled = true;
                        btnCreateVisits.Enabled = false;
                        btnTenderClosed.Enabled = false;
                        btnCancelTender.Enabled = true;

                        if (currentRow.KAMQuoteRejectionReasonID != 0) currentRow.KAMQuoteRejectionReasonID = 0;
                        if (currentRow.KAMQuoteRejectionReason != "") currentRow.KAMQuoteRejectionReason = "";
                        if (currentRow.StatusIssueID != 0) currentRow.StatusIssueID = 0;
                        if (currentRow.StatusIsssue != "") currentRow.StatusIsssue = "";
                        if (currentRow["QuoteAcceptedByClientDate"] != DBNull.Value) currentRow["QuoteAcceptedByClientDate"] = DBNull.Value;
                    }
                    break;
                case 60: // Tender On-hold By Client //
                    {
                        ddBtnSentToCM.Enabled = false;
                        btnReturnedFromCM.Enabled = false;
                        ddBtnKAMAuthorised.Enabled = false;
                        btnSendToClient.Enabled = false;
                        ddBtnClientResponse.Enabled = true;
                        btnCreateVisits.Enabled = false;
                        btnTenderClosed.Enabled = false;
                        btnCancelTender.Enabled = true;

                        if (currentRow.KAMQuoteRejectionReasonID != 0) currentRow.KAMQuoteRejectionReasonID = 0;
                        if (currentRow.KAMQuoteRejectionReason != "") currentRow.KAMQuoteRejectionReason = "";
                        if (currentRow.StatusIssueID != 0) currentRow.StatusIssueID = 0;
                        if (currentRow.StatusIsssue != "") currentRow.StatusIsssue = "";
                        if (currentRow["QuoteAcceptedByClientDate"] != DBNull.Value) currentRow["QuoteAcceptedByClientDate"] = DBNull.Value;
                    }
                    break;
                case 70:  // Tender Declined By Client //
                    {
                        ddBtnSentToCM.Enabled = false;
                        btnReturnedFromCM.Enabled = false;
                        ddBtnKAMAuthorised.Enabled = false;
                        btnSendToClient.Enabled = false;
                        ddBtnClientResponse.Enabled = false;
                        btnCreateVisits.Enabled = false;
                        btnTenderClosed.Enabled = false;
                        btnCancelTender.Enabled = true;

                        if (currentRow.KAMQuoteRejectionReasonID != 0) currentRow.KAMQuoteRejectionReasonID = 0;
                        if (currentRow.KAMQuoteRejectionReason != "") currentRow.KAMQuoteRejectionReason = "";
                        if (currentRow["QuoteAcceptedByClientDate"] != DBNull.Value) currentRow["QuoteAcceptedByClientDate"] = DBNull.Value;
                    }
                    break;
                case 80:  // Tender Accepted By Client //
                    {
                        ddBtnSentToCM.Enabled = false;
                        btnReturnedFromCM.Enabled = false;
                        ddBtnKAMAuthorised.Enabled = false;
                        btnSendToClient.Enabled = false;
                        ddBtnClientResponse.Enabled = false;
                        btnCreateVisits.Enabled = true;
                        btnTenderClosed.Enabled = true;
                        btnCancelTender.Enabled = true;

                        if (currentRow.KAMQuoteRejectionReasonID != 0) currentRow.KAMQuoteRejectionReasonID = 0;
                        if (currentRow.KAMQuoteRejectionReason != "") currentRow.KAMQuoteRejectionReason = "";
                        if (currentRow.StatusIssueID != 0) currentRow.StatusIssueID = 0;
                        if (currentRow.StatusIsssue != "") currentRow.StatusIsssue = "";
                    }
                    break;
                case 90:  // Tender Closed //
                    {
                        ddBtnSentToCM.Enabled = false;
                        btnReturnedFromCM.Enabled = false;
                        ddBtnKAMAuthorised.Enabled = false;
                        btnSendToClient.Enabled = false;
                        ddBtnClientResponse.Enabled = false;
                        btnCreateVisits.Enabled = false;
                        btnTenderClosed.Enabled = false;
                        btnCancelTender.Enabled = false;

                        if (currentRow.KAMQuoteRejectionReasonID != 0) currentRow.KAMQuoteRejectionReasonID = 0;
                        if (currentRow.KAMQuoteRejectionReason != "") currentRow.KAMQuoteRejectionReason = "";
                        if (currentRow.StatusIssueID != 0) currentRow.StatusIssueID = 0;
                        if (currentRow.StatusIsssue != "") currentRow.StatusIsssue = "";
                    }
                    break;
                case 100:  // Tender Declined (Internal) //
                    {
                        ddBtnSentToCM.Enabled = false;
                        btnReturnedFromCM.Enabled = false;
                        ddBtnKAMAuthorised.Enabled = false;
                        btnSendToClient.Enabled = false;
                        ddBtnClientResponse.Enabled = false;
                        btnCreateVisits.Enabled = false;
                        btnTenderClosed.Enabled = false;
                        btnCancelTender.Enabled = false;
                    }
                    break;
            }
            sp06498OMTenderEditBindingSource.EndEdit();
            SetControlsReadOnly(intStatusID);
        }
        private void SetControlsReadOnly(int StatusID)
        {
            EditorButtonCollection buttons = null;
            RevisionNumberTextEdit.Properties.ReadOnly = (StatusID >= 70 ? true : false);
            
            ClientNameButtonEdit.Properties.ReadOnly = (StatusID >= 20 ? true : false);
            buttons = ClientNameButtonEdit.Properties.Buttons;
            foreach(EditorButton btn in buttons) 
            {
                btn.Enabled = (StatusID >= 20 ? false : true);
            }

            ClientReferenceNumberTextEdit.Properties.ReadOnly = (StatusID >= 70 ? true : false);

            ClientPONumberTextEdit.Properties.ReadOnly = ((StatusID == 70 || StatusID >= 90) ? true : false); //(StatusID >= 70 ? true : false);

            NoClientPOCheckEdit.Properties.ReadOnly = ((StatusID == 70 || StatusID >= 90) ? true : false); //(StatusID >= 70 ? true : false);

            NoClientPOAuthorisedByDirectorButtonEdit.Properties.ReadOnly = ((StatusID == 70 || StatusID >= 90) ? true : false); //(StatusID >= 70 ? true : false);
            buttons = NoClientPOAuthorisedByDirectorButtonEdit.Properties.Buttons;
            foreach (EditorButton btn in buttons)
            {
                btn.Enabled = ((StatusID == 70 || StatusID >= 90) ? false : true); //(StatusID >= 70 ? false : true);
            }

            ContactPersonNameButtonEdit.Properties.ReadOnly = (StatusID >= 70 ? true : false);
            buttons = ContactPersonNameButtonEdit.Properties.Buttons;
            foreach (EditorButton btn in buttons)
            {
                btn.Enabled = (StatusID >= 70 ? false : true);
            }
                 
            ClientQuoteSpreadsheetExtractFileButtonEdit.Properties.ReadOnly = (StatusID >= 70 ? true : false);
            buttons = ClientQuoteSpreadsheetExtractFileButtonEdit.Properties.Buttons;
            foreach (EditorButton btn in buttons)
            {
                btn.Enabled = (StatusID >= 20 ? false : true);
            }

            SiteNameButtonEdit.Properties.ReadOnly = (StatusID >= 20 ? true : false);
            buttons = SiteNameButtonEdit.Properties.Buttons;
            foreach (EditorButton btn in buttons)
            {
                btn.Enabled = (StatusID >= 20 ? false : true);
            }

            StatusIsssueButtonEdit.Properties.ReadOnly = (StatusID >= 70 ? true : false);
            buttons = StatusIsssueButtonEdit.Properties.Buttons;
            foreach (EditorButton btn in buttons)
            {
                btn.Enabled = (StatusID >= 70 ? false : true);
            }

            QuotedLabourSellSpinEdit.Properties.ReadOnly = (StatusID >= 70 ? true : false);
            QuotedMaterialSellSpinEdit.Properties.ReadOnly = (StatusID >= 70 ? true : false);
            QuotedEquipmentSellSpinEdit.Properties.ReadOnly = (StatusID >= 70 ? true : false);
            AcceptedLabourSellSpinEdit.Properties.ReadOnly = ((StatusID == 70 || StatusID >= 90) ? true : false);
            AcceptedMaterialSellSpinEdit.Properties.ReadOnly = ((StatusID == 70 || StatusID >= 90) ? true : false);
            AcceptedEquipmentSellSpinEdit.Properties.ReadOnly = ((StatusID == 70 || StatusID >= 90) ? true : false);

            RequestReceivedDateDateEdit.Properties.ReadOnly = (StatusID >= 20 ? true : false);

            ReturnToClientByDateDateEdit.Properties.ReadOnly = (StatusID >= 50 ? true : false);

            CMInitialAttendanceDateDateEdit.Properties.ReadOnly = (StatusID >= 20 ? true : false);

            RequiredWorkCompletedDateDateEdit.Properties.ReadOnly = (StatusID >= 70 ? true : false);

            ReactiveCheckEdit.Properties.ReadOnly = (StatusID >= 20 ? true : false);

            KAMNameButtonEdit.Properties.ReadOnly = (StatusID >= 70 ? true : false);
            buttons = KAMNameButtonEdit.Properties.Buttons;
            foreach (EditorButton btn in buttons)
            {
                btn.Enabled = (StatusID >= 70 ? false : true);
            }

            CMNameButtonEdit.Properties.ReadOnly = (StatusID >= 70 ? true : false);
            buttons = CMNameButtonEdit.Properties.Buttons;
            foreach (EditorButton btn in buttons)
            {
                btn.Enabled = (StatusID >= 70 ? false : true);
            }

            TenderDescriptionMemoEdit.Properties.ReadOnly = (StatusID >= 70 ? true : false);

            TenderGroupDescriptionButtonEdit.Properties.ReadOnly = (StatusID >= 90 ? true : false);
            buttons = TenderGroupDescriptionButtonEdit.Properties.Buttons;
            foreach (EditorButton btn in buttons)
            {
                btn.Enabled = (StatusID >= 90 ? false : true);
            }

            JobSubTypeDescriptionButtonEdit.Properties.ReadOnly = ((StatusID == 70 || StatusID >= 90) ? true : false);
            buttons = JobSubTypeDescriptionButtonEdit.Properties.Buttons;
            foreach (EditorButton btn in buttons)
            {
                btn.Enabled = ((StatusID == 70 || StatusID >= 90) ? false : true);
            }

            LinkedToPersonTypeButtonEdit.Properties.ReadOnly = ((StatusID == 70 || StatusID >= 90) ? true : false);
            buttons = LinkedToPersonTypeButtonEdit.Properties.Buttons;
            foreach (EditorButton btn in buttons)
            {
                btn.Enabled = ((StatusID == 70 || StatusID >= 90) ? false : true);
            }

            ContractorNameButtonEdit.Properties.ReadOnly = ((StatusID == 70 || StatusID >= 90) ? true : false);
            buttons = ContractorNameButtonEdit.Properties.Buttons;
            foreach (EditorButton btn in buttons)
            {
                btn.Enabled = ((StatusID == 70 || StatusID >= 90) ? false : true);
            }

            TPORequiredCheckEdit.Properties.ReadOnly = ((StatusID == 70 || StatusID >= 90) ? true : false);
            PlanningAuthorityButtonEdit.Properties.ReadOnly = ((StatusID == 70 || StatusID >= 90) ? true : false);
            TPOCheckedDateDateEdit.Properties.ReadOnly = ((StatusID == 70 || StatusID >= 90) ? true : false);
            TreeProtectedCheckEdit.Properties.ReadOnly = ((StatusID == 70 || StatusID >= 90) ? true : false);

            TPONumberTextEdit.Properties.ReadOnly = ((StatusID == 70 || StatusID >= 90) ? true : false);
            ConservationStatusIDGridLookUpEdit.Properties.ReadOnly = ((StatusID == 70 || StatusID >= 90) ? true : false);
            ListedBuildingStatusIDGridLookUpEdit.Properties.ReadOnly = ((StatusID == 70 || StatusID >= 90) ? true : false);
            RAMSCheckEdit.Properties.ReadOnly = ((StatusID == 70 || StatusID >= 90) ? true : false);

            //OurInternalCommentsMemoEdit.Properties.ReadOnly = ((StatusID == 70 || StatusID >= 90) ? true : false);
            //CMCommentsMemoEdit.Properties.ReadOnly = ((StatusID == 70 || StatusID >= 90) ? true : false);
            //ClientCommentsMemoEdit.Properties.ReadOnly = ((StatusID == 70 || StatusID >= 90) ? true : false);
        }

        #endregion

        #endregion

 








    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_OM_Job_Collection_Template_Item_Rule_Block_Add_Visit : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        BaseObjects.GridCheckMarksSelection selection1;
        public string strVisitNumbersToExclude = "";
        public string strRemarks = null;
        public string strVisitNumbers = null;
        int intMinNumber = 1;
        int intMaxNumber = 100;
        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        #endregion

        public frm_OM_Job_Collection_Template_Item_Rule_Block_Add_Visit()
        {
            InitializeComponent();
        }

        private void frm_OM_Job_Collection_Template_Item_Rule_Block_Add_Visit_Load(object sender, EventArgs e)
        {
            this.FormID = 500245;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            Application.DoEvents();  // Allow Form time to repaint itself //
            
            strConnectionString = this.GlobalSettings.ConnectionString;

            sp06338_OM_Job_Collection_Template_Item_Rule_Visit_Block_Add_ListTableAdapter.Connection.ConnectionString = strConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            GridView view = (GridView)gridControl1.MainView;
            gridControl1.ForceInitialize();

            // Add record selection checkboxes to popup grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;

            RemarksMemoEdit.EditValue = null;

            PostOpen();
        }

        private void frm_OM_Job_Collection_Template_Item_Rule_Block_Add_Visit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
            {
                // Store last used screen settings for current user //
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "MinNumber", intMinNumber.ToString());
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "MaxNumber", intMaxNumber.ToString());
                default_screen_settings.SaveDefaultScreenSettings();
            }
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            ConfigureFormAccordingToMode();
            LoadLastSavedUserScreenSettings();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                try
                {
                    string strFilter = default_screen_settings.RetrieveSetting("MinNumber");
                    if (!(string.IsNullOrEmpty(strFilter)))
                    {
                        intMinNumber = Convert.ToInt32(strFilter);
                        NumberFromSpinEdit.EditValue = intMinNumber;
                    }

                    strFilter = default_screen_settings.RetrieveSetting("MaxNumber");
                    if (!(string.IsNullOrEmpty(strFilter)))
                    {
                        intMaxNumber = Convert.ToInt32(strFilter);
                        NumberToSpinEdit.EditValue = intMaxNumber;
                    }
                }
                catch (Exception) { }
                Load_Data();
            }
        }

        private void Load_Data()
        {
            try
            {
                sp06338_OM_Job_Collection_Template_Item_Rule_Visit_Block_Add_ListTableAdapter.Fill(dataSet_OM_Job.sp06338_OM_Job_Collection_Template_Item_Rule_Visit_Block_Add_List, strVisitNumbersToExclude, intMinNumber, intMaxNumber);
            }
            catch (Exception) { }
        }
        
        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (string.IsNullOrEmpty(strVisitNumbers))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Visit Numbers by ticking them before proceeding.", "Select Visit Numbers", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (RemarksMemoEdit.EditValue != null) strRemarks = RemarksMemoEdit.EditValue.ToString();
            
            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }

        private void GetSelectedDetails()
        {
            strVisitNumbers = "";    // Reset any prior values first //

            GridView view = (GridView)gridControl1.MainView;
            if (selection1.SelectedCount <= 0) return;
            strVisitNumbers = "";
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                {
                    strVisitNumbers += Convert.ToString(view.GetRowCellValue(i, "VisitNumber")) + ",";
                }
            }
        }

        private void bntRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                intMinNumber = Convert.ToInt32(NumberFromSpinEdit.EditValue);
                intMaxNumber = Convert.ToInt32(NumberToSpinEdit.EditValue);
                Load_Data();
            }
            catch (Exception) { }
        }

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Visit Numbers");

        }
    }
}

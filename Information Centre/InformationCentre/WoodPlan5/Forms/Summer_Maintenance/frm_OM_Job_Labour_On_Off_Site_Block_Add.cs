using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_OM_Job_Labour_On_Off_Site_Block_Add : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        GridHitInfo downHitInfo = null;

        public string strPassedInTeamIDs = "";
        public string strPassedInStaffIDs = "";

        bool isRunning = false;

        BaseObjects.GridCheckMarksSelection selection1;

        #endregion

        public frm_OM_Job_Labour_On_Off_Site_Block_Add()
        {
            InitializeComponent();
        }

        private void frm_OM_Job_Labour_On_Off_Site_Block_Add_Load(object sender, EventArgs e)
        {
            this.LockThisWindow();
            this.FormID = 500263;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            sp06399_OM_Block_Add_Labour_On_Site_Select_Team_MembersTableAdapter.Connection.ConnectionString = strConnectionString;
            GridView view = (GridView)gridControl1.MainView;

            Set_Grid_Highlighter_Transparent(this.Controls);

            LoadData();
            gridControl1.ForceInitialize();

            // Add record selection checkboxes to popup grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
        }

        public override void PostLoadView(object objParameter)
        {
        }

        private void LoadData()
        {
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            sp06399_OM_Block_Add_Labour_On_Site_Select_Team_MembersTableAdapter.Fill(dataSet_OM_Job.sp06399_OM_Block_Add_Labour_On_Site_Select_Team_Members, strPassedInTeamIDs, strPassedInStaffIDs);
            view.ExpandAllGroups();
            view.EndUpdate();
        }


        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Team Members Available");
        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridView1_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }
        
        private void gridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void gridView1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);
        }

        private void gridView1_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            GridView view = (GridView)sender;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            int intFocusedRow = view.FocusedRowHandle;
            DataRow dr = view.GetDataRow(intFocusedRow);
            if (dr == null) return;
            switch (view.FocusedColumn.Name)
            {
                case "colStartDate":
                case "colEndDate":
                    {
                        bool boolValidFromDate = true;
                        bool boolValidToDate = true;
                        if (Convert.ToInt32(view.GetFocusedRowCellValue("CheckMarkSelection")) == 1)
                        {
                            DateTime? dtFromDate = null;
                            DateTime? dtToDate = null;
                            if (view.FocusedColumn.Name == "colStartDate")
                            {
                                if (e.Value != DBNull.Value) dtFromDate = Convert.ToDateTime(e.Value);
                                if (view.GetFocusedRowCellValue("EndDate") != DBNull.Value) dtToDate = Convert.ToDateTime(view.GetFocusedRowCellValue("EndDate"));
                            }
                            else
                            {
                                if (e.Value != DBNull.Value) dtToDate = Convert.ToDateTime(e.Value);
                                if (view.GetFocusedRowCellValue("StartDate") != DBNull.Value) dtFromDate = Convert.ToDateTime(view.GetFocusedRowCellValue("StartDate"));
                            }
                            if (dtFromDate == null || dtFromDate == DateTime.MinValue)
                            {
                                dr.SetColumnError("EndDate", String.Format("{0} requires a value.", view.Columns["EndDate"].Caption));
                                boolValidFromDate = false;
                            }
                            if (dtToDate == null || dtToDate == DateTime.MinValue)
                            {
                                dr.SetColumnError("EndDate", String.Format("{0} requires a value.", view.Columns["EndDate"].Caption));
                                boolValidToDate = false;
                            }
                            if (dtFromDate != null && dtToDate != null && dtFromDate > dtToDate)
                            {
                                dr.SetColumnError("StartDate", String.Format("{0} should be less than {1}.", view.Columns["StartDate"].Caption, view.Columns["EndDate"].Caption));
                                boolValidFromDate = false;

                                dr.SetColumnError("EndDate", String.Format("{0} should be greater than {1}.", view.Columns["EndDate"].Caption, view.Columns["StartDate"].Caption));
                                boolValidToDate = false;
                            }
                        }
                        if (boolValidFromDate) dr.SetColumnError("StartDate", "");  // Clear Error Text //
                        if (boolValidToDate) dr.SetColumnError("EndDate", "");  // Clear Error Text //
                    }
                    break;
            }
        }

        private int CheckRows(GridView view)
        {
            int intInvalidRowCount = 0;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                intInvalidRowCount += CheckRow(view, i);
            }
            return intInvalidRowCount;
        }
        private int CheckRow(GridView view, int RowHandle)
        {
            try
            {
                DataRow dr = view.GetDataRow(RowHandle);
                DateTime? dtFromDate = null;
                DateTime? dtToDate = null;

                bool boolValidFromDate = true;
                bool boolValidToDate = true;

                if (Convert.ToInt32(view.GetRowCellValue(RowHandle, "CheckMarkSelection")) == 1)
                {
                    if (view.GetRowCellValue(RowHandle, "StartDate") != DBNull.Value) dtFromDate = Convert.ToDateTime(view.GetRowCellValue(RowHandle, "StartDate"));
                    if (view.GetRowCellValue(RowHandle, "EndDate") != DBNull.Value) dtToDate = Convert.ToDateTime(view.GetRowCellValue(RowHandle, "EndDate"));

                    if (dtFromDate == null || dtFromDate == DateTime.MinValue)
                    {
                        dr.SetColumnError("StartDate", String.Format("{0} requires a value.", view.Columns["StartDate"].Caption));
                        boolValidFromDate = false;
                    }
                    else
                    {
                        dr.SetColumnError("StartDate", "");
                    }
                    if (dtToDate == null || dtToDate == DateTime.MinValue)
                    {
                        dr.SetColumnError("EndDate", String.Format("{0} requires a value.", view.Columns["EndDate"].Caption));
                        boolValidToDate = false;
                    }
                    else
                    {
                        dr.SetColumnError("EndDate", "");
                    }
                    if (dtFromDate != null && dtToDate != null && dtFromDate > dtToDate)
                    {
                        dr.SetColumnError("StartDate", String.Format("{0} should be less than {1}.", view.Columns["StartDate"].Caption, view.Columns["EndDate"].Caption));
                        dr.SetColumnError("EndDate", String.Format("{0} should be greater than {1}.", view.Columns["EndDate"].Caption, view.Columns["StartDate"].Caption));
                        boolValidFromDate = false;
                        boolValidToDate = false;
                    }
                    else if (boolValidFromDate && boolValidToDate)
                    {
                        dr.SetColumnError("StartDate", "");
                        dr.SetColumnError("EndDate", "");
                    }
                }
                else
                {
                    dr.SetColumnError("StartDate", "");
                    dr.SetColumnError("EndDate", "");
                }
                return (!boolValidFromDate || !boolValidToDate ? 1 : 0);
            }
            catch (Exception)
            {
            }
            return 0;
        }

        #endregion


        private void btnOK_Click(object sender, EventArgs e)
        {
            if (selection1.SelectedCount <= 0)
            {
                XtraMessageBox.Show("Select at least one record by ticking it before proceeding.", "Block Add Labour Time On \\ Off Site", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            GridView view = (GridView)gridControl1.MainView;
            view.PostEditor();
            int intErrors = CheckRows(view);
            if (intErrors > 0)
            {
                string strMessage = (intErrors == 1 ? "1 Row" : intErrors.ToString() + " Rows") + " with errors - correct the errors before proceeding.\n\nColumns with invalid values have warning icons in them.";
                XtraMessageBox.Show(strMessage, "Block Add Labour Time On \\ Off Site", MessageBoxButtons.OK, MessageBoxIcon.Warning, DefaultBoolean.True);
                return;
            }

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }


        private void btnApplyBlockEdit_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            view.PostEditor();
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more records to block edit Start and End Times for before proceeding.", "Block Edit Selected Records", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            DateTime dtBlankDate = new DateTime(1, 1, 1);
            DateTime? dtStartTime = null;
            try { dtStartTime = dateEditStartDate.DateTime; } 
            catch (Exception) { }

            DateTime? dtEndTime = null;
            try { dtEndTime = dateEditEndDate.DateTime; } 
            catch (Exception) { }

            if (dtStartTime == dtBlankDate && dtEndTime == dtBlankDate)
            {
                XtraMessageBox.Show("Enter a Start Time and \\ or End Time to use as block edit values before proceeding.", "Block Edit Selected Records", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            view.BeginUpdate();
            foreach (int intRowHandle in intRowHandles)
            {
                if (Convert.ToBoolean(view.GetRowCellValue(intRowHandle, "CheckMarkSelection")))
                {
                    if (dtStartTime != dtBlankDate) view.SetRowCellValue(intRowHandle, "StartDate", dtStartTime);
                    if (dtEndTime != dtBlankDate) view.SetRowCellValue(intRowHandle, "EndDate", dtEndTime);

                    DateTime? dtS = null;
                    DateTime? dtE = null;
                    if (dtStartTime != dtBlankDate)
                    {
                        dtS = dtStartTime;
                    }
                    else
                    {
                        try { dtS = Convert.ToDateTime(view.GetRowCellValue(intRowHandle, "StartDate")); } 
                        catch (Exception) { }
                    }
                    if (dtEndTime != dtBlankDate)
                    {
                        dtE = dtEndTime;
                    }
                    else
                    {
                        try { dtE = Convert.ToDateTime(view.GetRowCellValue(intRowHandle, "EndDate")); } 
                        catch (Exception) { }
                    }
                    view.SetRowCellValue(intRowHandle, "Duration", Calculate_Duration(dtS, dtE));
                    CheckRow(view, intRowHandle);
                }
            }
            view.EndUpdate();
        }
          
        private decimal Calculate_Duration(DateTime? StartDate, DateTime? EndDate)
        {
            DateTime dtBlankDate = new DateTime(1, 1, 1);
            if (StartDate <= dtBlankDate || StartDate == null || EndDate <= dtBlankDate || EndDate == null || EndDate <= dtBlankDate)
            {
                return (decimal)0.00;
            }
            // Calculate the interval between the two dates.
            TimeSpan ts = (DateTime)EndDate - (DateTime)StartDate;
            decimal Units = (decimal)ts.TotalHours;
            return (Units < (decimal)0.00 ? (decimal)0.00 : (decimal) Units);
        }

        private void repositoryItemDateEdit1_EditValueChanged(object sender, EventArgs e)
        {
            var currentRowView = (DataRowView)sp06399OMBlockAddLabourOnSiteSelectTeamMembersBindingSource.Current;
            var currentRow = (DataSet_OM_Job.sp06399_OM_Block_Add_Labour_On_Site_Select_Team_MembersRow)currentRowView.Row;
            if (currentRow == null) return;

            DateTime? dtStart = null;
            DateTime? dtEnd = null;

            DateEdit de = (DateEdit)sender;

            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedColumn.Name == "colStartDate")
            {
                try { dtStart = de.DateTime; }
                catch (Exception) { }

                try { dtEnd = currentRow.EndDate; }
                catch (Exception) { }
            }
            else
            {
                try { dtStart = currentRow.StartDate; }
                catch (Exception) { }

                try { dtEnd = de.DateTime; }
                catch (Exception) { }
            }

            currentRow.Duration = Calculate_Duration(dtStart, dtEnd);
            sp06399OMBlockAddLabourOnSiteSelectTeamMembersBindingSource.EndEdit();
            
            DataRow dr = view.GetDataRow(view.FocusedRowHandle);
            if (view.FocusedColumn.Name == "colStartDate")
            {
                dr.SetColumnError("StartDate", (dtStart == null || dtStart == DateTime.MinValue ? String.Format("{0} requires a value.", view.Columns["StartDate"].Caption) : ""));
            }
            else
            {
                dr.SetColumnError("EndDate", (dtStart == null || dtStart == DateTime.MinValue ? String.Format("{0} requires a value.", view.Columns["EndDate"].Caption) : ""));
            }
        }

        public override void PostViewClick(object sender, EventArgs e, int row)
        {
            GridView view = (GridView)sender;
            if (view.GridControl.MainView.Name != "gridView1") return;  // Only fire for appropriate view //
            if (row == GridControl.InvalidRowHandle) return;
            CheckRow(view, row);
        }



    }
}


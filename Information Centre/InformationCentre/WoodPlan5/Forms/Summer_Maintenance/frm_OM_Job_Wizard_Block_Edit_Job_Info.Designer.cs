﻿namespace WoodPlan5
{
    partial class frm_OM_Job_Wizard_Block_Edit_Job_Info
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Job_Wizard_Block_Edit_Job_Info));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.colID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.DaysLeewaySpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.MaximumDaysFromLastVisitSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.MinimumDaysFromLastVisitSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ReactiveCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.RequiresAccessPermitCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ExpectedDurationUnitsSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ExpectedEndDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.ExpectedStartDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.JobSubTypeDescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.JobTypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.JobTypeDescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ExpectedDurationUnitsDescriptorIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06177OMJobDurationDescriptorsWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Job = new WoodPlan5.DataSet_OM_Job();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.JobSubTypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.JobTypeJobSubTypeDescriptionButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ItemForJobSubTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobSubTypeDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobTypeDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobTypeJobSubTypeDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForExpectedStartDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRequiresAccessPermit = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForReactive = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForExpectedDurationUnits = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForExpectedDurationUnitsDescriptorID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForExpectedEndDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMinimumDaysFromLastVisit = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMaximumDaysFromLastVisit = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDaysLeeway = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.sp06177_OM_Job_Duration_Descriptors_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06177_OM_Job_Duration_Descriptors_With_BlankTableAdapter();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bsiInformation = new DevExpress.XtraBars.BarStaticItem();
            this.ClientPONumberButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.ItemForClientPONumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ClientPOIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ItemForClientPOID = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DaysLeewaySpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaximumDaysFromLastVisitSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinimumDaysFromLastVisitSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReactiveCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RequiresAccessPermitCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpectedDurationUnitsSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpectedEndDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpectedEndDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpectedStartDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpectedStartDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobSubTypeDescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobTypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobTypeDescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpectedDurationUnitsDescriptorIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06177OMJobDurationDescriptorsWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Job)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobSubTypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobTypeJobSubTypeDescriptionButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobSubTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobSubTypeDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobTypeDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobTypeJobSubTypeDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpectedStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRequiresAccessPermit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReactive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpectedDurationUnits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpectedDurationUnitsDescriptorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpectedEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMinimumDaysFromLastVisit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMaximumDaysFromLastVisit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDaysLeeway)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPONumberButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPONumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPOIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPOID)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(595, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 431);
            this.barDockControlBottom.Size = new System.Drawing.Size(595, 30);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 405);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(595, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 405);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bsiInformation});
            this.barManager1.MaxItemId = 31;
            this.barManager1.StatusBar = this.bar1;
            // 
            // colID1
            // 
            this.colID1.Caption = "ID";
            this.colID1.FieldName = "ID";
            this.colID1.Name = "colID1";
            this.colID1.OptionsColumn.AllowEdit = false;
            this.colID1.OptionsColumn.AllowFocus = false;
            this.colID1.OptionsColumn.ReadOnly = true;
            this.colID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colID1.Width = 53;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutControl1.Controls.Add(this.ClientPOIDTextEdit);
            this.layoutControl1.Controls.Add(this.ClientPONumberButtonEdit);
            this.layoutControl1.Controls.Add(this.DaysLeewaySpinEdit);
            this.layoutControl1.Controls.Add(this.MaximumDaysFromLastVisitSpinEdit);
            this.layoutControl1.Controls.Add(this.MinimumDaysFromLastVisitSpinEdit);
            this.layoutControl1.Controls.Add(this.ReactiveCheckEdit);
            this.layoutControl1.Controls.Add(this.RequiresAccessPermitCheckEdit);
            this.layoutControl1.Controls.Add(this.ExpectedDurationUnitsSpinEdit);
            this.layoutControl1.Controls.Add(this.ExpectedEndDateDateEdit);
            this.layoutControl1.Controls.Add(this.ExpectedStartDateDateEdit);
            this.layoutControl1.Controls.Add(this.JobSubTypeDescriptionTextEdit);
            this.layoutControl1.Controls.Add(this.JobTypeIDTextEdit);
            this.layoutControl1.Controls.Add(this.JobTypeDescriptionTextEdit);
            this.layoutControl1.Controls.Add(this.ExpectedDurationUnitsDescriptorIDGridLookUpEdit);
            this.layoutControl1.Controls.Add(this.JobSubTypeIDTextEdit);
            this.layoutControl1.Controls.Add(this.JobTypeJobSubTypeDescriptionButtonEdit);
            this.layoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.layoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForJobSubTypeID,
            this.ItemForJobTypeID,
            this.ItemForJobSubTypeDescription,
            this.ItemForJobTypeDescription,
            this.ItemForClientPOID});
            this.layoutControl1.Location = new System.Drawing.Point(0, 28);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1213, 293, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(594, 372);
            this.layoutControl1.TabIndex = 4;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // DaysLeewaySpinEdit
            // 
            this.DaysLeewaySpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.DaysLeewaySpinEdit.Location = new System.Drawing.Point(162, 180);
            this.DaysLeewaySpinEdit.MenuManager = this.barManager1;
            this.DaysLeewaySpinEdit.Name = "DaysLeewaySpinEdit";
            this.DaysLeewaySpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DaysLeewaySpinEdit.Properties.IsFloatValue = false;
            this.DaysLeewaySpinEdit.Properties.Mask.EditMask = "n0";
            this.DaysLeewaySpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DaysLeewaySpinEdit.Size = new System.Drawing.Size(154, 20);
            this.DaysLeewaySpinEdit.StyleController = this.layoutControl1;
            this.DaysLeewaySpinEdit.TabIndex = 7;
            // 
            // MaximumDaysFromLastVisitSpinEdit
            // 
            this.MaximumDaysFromLastVisitSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.MaximumDaysFromLastVisitSpinEdit.Location = new System.Drawing.Point(162, 156);
            this.MaximumDaysFromLastVisitSpinEdit.MenuManager = this.barManager1;
            this.MaximumDaysFromLastVisitSpinEdit.Name = "MaximumDaysFromLastVisitSpinEdit";
            this.MaximumDaysFromLastVisitSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MaximumDaysFromLastVisitSpinEdit.Properties.IsFloatValue = false;
            this.MaximumDaysFromLastVisitSpinEdit.Properties.Mask.EditMask = "n0";
            this.MaximumDaysFromLastVisitSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.MaximumDaysFromLastVisitSpinEdit.Size = new System.Drawing.Size(154, 20);
            this.MaximumDaysFromLastVisitSpinEdit.StyleController = this.layoutControl1;
            this.MaximumDaysFromLastVisitSpinEdit.TabIndex = 6;
            // 
            // MinimumDaysFromLastVisitSpinEdit
            // 
            this.MinimumDaysFromLastVisitSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.MinimumDaysFromLastVisitSpinEdit.Location = new System.Drawing.Point(162, 132);
            this.MinimumDaysFromLastVisitSpinEdit.MenuManager = this.barManager1;
            this.MinimumDaysFromLastVisitSpinEdit.Name = "MinimumDaysFromLastVisitSpinEdit";
            this.MinimumDaysFromLastVisitSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MinimumDaysFromLastVisitSpinEdit.Properties.IsFloatValue = false;
            this.MinimumDaysFromLastVisitSpinEdit.Properties.Mask.EditMask = "n0";
            this.MinimumDaysFromLastVisitSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.MinimumDaysFromLastVisitSpinEdit.Size = new System.Drawing.Size(154, 20);
            this.MinimumDaysFromLastVisitSpinEdit.StyleController = this.layoutControl1;
            this.MinimumDaysFromLastVisitSpinEdit.TabIndex = 5;
            // 
            // ReactiveCheckEdit
            // 
            this.ReactiveCheckEdit.Location = new System.Drawing.Point(162, 214);
            this.ReactiveCheckEdit.MenuManager = this.barManager1;
            this.ReactiveCheckEdit.Name = "ReactiveCheckEdit";
            this.ReactiveCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.ReactiveCheckEdit.Properties.ValueChecked = 1;
            this.ReactiveCheckEdit.Properties.ValueUnchecked = 0;
            this.ReactiveCheckEdit.Size = new System.Drawing.Size(420, 19);
            this.ReactiveCheckEdit.StyleController = this.layoutControl1;
            this.ReactiveCheckEdit.TabIndex = 8;
            // 
            // RequiresAccessPermitCheckEdit
            // 
            this.RequiresAccessPermitCheckEdit.Location = new System.Drawing.Point(162, 237);
            this.RequiresAccessPermitCheckEdit.MenuManager = this.barManager1;
            this.RequiresAccessPermitCheckEdit.Name = "RequiresAccessPermitCheckEdit";
            this.RequiresAccessPermitCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.RequiresAccessPermitCheckEdit.Properties.ValueChecked = 1;
            this.RequiresAccessPermitCheckEdit.Properties.ValueUnchecked = 0;
            this.RequiresAccessPermitCheckEdit.Size = new System.Drawing.Size(420, 19);
            this.RequiresAccessPermitCheckEdit.StyleController = this.layoutControl1;
            this.RequiresAccessPermitCheckEdit.TabIndex = 9;
            // 
            // ExpectedDurationUnitsSpinEdit
            // 
            this.ExpectedDurationUnitsSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ExpectedDurationUnitsSpinEdit.Location = new System.Drawing.Point(162, 60);
            this.ExpectedDurationUnitsSpinEdit.MenuManager = this.barManager1;
            this.ExpectedDurationUnitsSpinEdit.Name = "ExpectedDurationUnitsSpinEdit";
            this.ExpectedDurationUnitsSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ExpectedDurationUnitsSpinEdit.Properties.Mask.EditMask = "f2";
            this.ExpectedDurationUnitsSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ExpectedDurationUnitsSpinEdit.Properties.MaxValue = new decimal(new int[] {
            1410065407,
            2,
            0,
            131072});
            this.ExpectedDurationUnitsSpinEdit.Size = new System.Drawing.Size(154, 20);
            this.ExpectedDurationUnitsSpinEdit.StyleController = this.layoutControl1;
            this.ExpectedDurationUnitsSpinEdit.TabIndex = 2;
            // 
            // ExpectedEndDateDateEdit
            // 
            this.ExpectedEndDateDateEdit.EditValue = null;
            this.ExpectedEndDateDateEdit.Location = new System.Drawing.Point(162, 108);
            this.ExpectedEndDateDateEdit.MenuManager = this.barManager1;
            this.ExpectedEndDateDateEdit.Name = "ExpectedEndDateDateEdit";
            this.ExpectedEndDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.ExpectedEndDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ExpectedEndDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ExpectedEndDateDateEdit.Properties.Mask.EditMask = "g";
            this.ExpectedEndDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ExpectedEndDateDateEdit.Size = new System.Drawing.Size(154, 20);
            this.ExpectedEndDateDateEdit.StyleController = this.layoutControl1;
            this.ExpectedEndDateDateEdit.TabIndex = 4;
            // 
            // ExpectedStartDateDateEdit
            // 
            this.ExpectedStartDateDateEdit.EditValue = null;
            this.ExpectedStartDateDateEdit.Location = new System.Drawing.Point(162, 36);
            this.ExpectedStartDateDateEdit.MenuManager = this.barManager1;
            this.ExpectedStartDateDateEdit.Name = "ExpectedStartDateDateEdit";
            this.ExpectedStartDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.ExpectedStartDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ExpectedStartDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ExpectedStartDateDateEdit.Properties.Mask.EditMask = "g";
            this.ExpectedStartDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ExpectedStartDateDateEdit.Size = new System.Drawing.Size(154, 20);
            this.ExpectedStartDateDateEdit.StyleController = this.layoutControl1;
            this.ExpectedStartDateDateEdit.TabIndex = 1;
            // 
            // JobSubTypeDescriptionTextEdit
            // 
            this.JobSubTypeDescriptionTextEdit.Location = new System.Drawing.Point(141, 272);
            this.JobSubTypeDescriptionTextEdit.MenuManager = this.barManager1;
            this.JobSubTypeDescriptionTextEdit.Name = "JobSubTypeDescriptionTextEdit";
            this.JobSubTypeDescriptionTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.JobSubTypeDescriptionTextEdit, true);
            this.JobSubTypeDescriptionTextEdit.Size = new System.Drawing.Size(361, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.JobSubTypeDescriptionTextEdit, optionsSpelling2);
            this.JobSubTypeDescriptionTextEdit.StyleController = this.layoutControl1;
            this.JobSubTypeDescriptionTextEdit.TabIndex = 26;
            // 
            // JobTypeIDTextEdit
            // 
            this.JobTypeIDTextEdit.Location = new System.Drawing.Point(130, 272);
            this.JobTypeIDTextEdit.MenuManager = this.barManager1;
            this.JobTypeIDTextEdit.Name = "JobTypeIDTextEdit";
            this.JobTypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.JobTypeIDTextEdit, true);
            this.JobTypeIDTextEdit.Size = new System.Drawing.Size(372, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.JobTypeIDTextEdit, optionsSpelling3);
            this.JobTypeIDTextEdit.StyleController = this.layoutControl1;
            this.JobTypeIDTextEdit.TabIndex = 25;
            // 
            // JobTypeDescriptionTextEdit
            // 
            this.JobTypeDescriptionTextEdit.Location = new System.Drawing.Point(130, 272);
            this.JobTypeDescriptionTextEdit.MenuManager = this.barManager1;
            this.JobTypeDescriptionTextEdit.Name = "JobTypeDescriptionTextEdit";
            this.JobTypeDescriptionTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.JobTypeDescriptionTextEdit, true);
            this.JobTypeDescriptionTextEdit.Size = new System.Drawing.Size(372, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.JobTypeDescriptionTextEdit, optionsSpelling4);
            this.JobTypeDescriptionTextEdit.StyleController = this.layoutControl1;
            this.JobTypeDescriptionTextEdit.TabIndex = 24;
            // 
            // ExpectedDurationUnitsDescriptorIDGridLookUpEdit
            // 
            this.ExpectedDurationUnitsDescriptorIDGridLookUpEdit.Location = new System.Drawing.Point(162, 84);
            this.ExpectedDurationUnitsDescriptorIDGridLookUpEdit.MenuManager = this.barManager1;
            this.ExpectedDurationUnitsDescriptorIDGridLookUpEdit.Name = "ExpectedDurationUnitsDescriptorIDGridLookUpEdit";
            this.ExpectedDurationUnitsDescriptorIDGridLookUpEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.ExpectedDurationUnitsDescriptorIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ExpectedDurationUnitsDescriptorIDGridLookUpEdit.Properties.DataSource = this.sp06177OMJobDurationDescriptorsWithBlankBindingSource;
            this.ExpectedDurationUnitsDescriptorIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.ExpectedDurationUnitsDescriptorIDGridLookUpEdit.Properties.NullText = "";
            this.ExpectedDurationUnitsDescriptorIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.ExpectedDurationUnitsDescriptorIDGridLookUpEdit.Properties.View = this.gridLookUpEdit1View;
            this.ExpectedDurationUnitsDescriptorIDGridLookUpEdit.Size = new System.Drawing.Size(154, 20);
            this.ExpectedDurationUnitsDescriptorIDGridLookUpEdit.StyleController = this.layoutControl1;
            this.ExpectedDurationUnitsDescriptorIDGridLookUpEdit.TabIndex = 3;
            // 
            // sp06177OMJobDurationDescriptorsWithBlankBindingSource
            // 
            this.sp06177OMJobDurationDescriptorsWithBlankBindingSource.DataMember = "sp06177_OM_Job_Duration_Descriptors_With_Blank";
            this.sp06177OMJobDurationDescriptorsWithBlankBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // dataSet_OM_Job
            // 
            this.dataSet_OM_Job.DataSetName = "DataSet_OM_Job";
            this.dataSet_OM_Job.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID1,
            this.colDescription,
            this.colOrder});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colID1;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Duration Descriptor";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 153;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "RecordOrder";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            this.colOrder.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // JobSubTypeIDTextEdit
            // 
            this.JobSubTypeIDTextEdit.Location = new System.Drawing.Point(130, 224);
            this.JobSubTypeIDTextEdit.MenuManager = this.barManager1;
            this.JobSubTypeIDTextEdit.Name = "JobSubTypeIDTextEdit";
            this.JobSubTypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.JobSubTypeIDTextEdit, true);
            this.JobSubTypeIDTextEdit.Size = new System.Drawing.Size(372, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.JobSubTypeIDTextEdit, optionsSpelling5);
            this.JobSubTypeIDTextEdit.StyleController = this.layoutControl1;
            this.JobSubTypeIDTextEdit.TabIndex = 23;
            // 
            // JobTypeJobSubTypeDescriptionButtonEdit
            // 
            this.JobTypeJobSubTypeDescriptionButtonEdit.Location = new System.Drawing.Point(162, 12);
            this.JobTypeJobSubTypeDescriptionButtonEdit.MenuManager = this.barManager1;
            this.JobTypeJobSubTypeDescriptionButtonEdit.Name = "JobTypeJobSubTypeDescriptionButtonEdit";
            this.JobTypeJobSubTypeDescriptionButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, "Click me to open the Select Equipment screen", "choose", null, true)});
            this.JobTypeJobSubTypeDescriptionButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.JobTypeJobSubTypeDescriptionButtonEdit.Size = new System.Drawing.Size(420, 20);
            this.JobTypeJobSubTypeDescriptionButtonEdit.StyleController = this.layoutControl1;
            this.JobTypeJobSubTypeDescriptionButtonEdit.TabIndex = 0;
            this.JobTypeJobSubTypeDescriptionButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.JobTypeJobSubTypeDescriptionButtonEdit_ButtonClick);
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.Location = new System.Drawing.Point(162, 284);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.RemarksMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(420, 76);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling6);
            this.RemarksMemoEdit.StyleController = this.layoutControl1;
            this.RemarksMemoEdit.TabIndex = 11;
            // 
            // ItemForJobSubTypeID
            // 
            this.ItemForJobSubTypeID.Control = this.JobSubTypeIDTextEdit;
            this.ItemForJobSubTypeID.CustomizationFormText = "Job Sub-Type ID:";
            this.ItemForJobSubTypeID.Location = new System.Drawing.Point(0, 212);
            this.ItemForJobSubTypeID.Name = "ItemForJobSubTypeID";
            this.ItemForJobSubTypeID.Size = new System.Drawing.Size(494, 24);
            this.ItemForJobSubTypeID.Text = "Job Sub-Type ID:";
            this.ItemForJobSubTypeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForJobTypeID
            // 
            this.ItemForJobTypeID.Control = this.JobTypeIDTextEdit;
            this.ItemForJobTypeID.CustomizationFormText = "Job Type ID:";
            this.ItemForJobTypeID.Location = new System.Drawing.Point(0, 260);
            this.ItemForJobTypeID.Name = "ItemForJobTypeID";
            this.ItemForJobTypeID.Size = new System.Drawing.Size(494, 24);
            this.ItemForJobTypeID.Text = "Job Type ID:";
            this.ItemForJobTypeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForJobSubTypeDescription
            // 
            this.ItemForJobSubTypeDescription.Control = this.JobSubTypeDescriptionTextEdit;
            this.ItemForJobSubTypeDescription.CustomizationFormText = "Job Sub-Type Description:";
            this.ItemForJobSubTypeDescription.Location = new System.Drawing.Point(0, 260);
            this.ItemForJobSubTypeDescription.Name = "ItemForJobSubTypeDescription";
            this.ItemForJobSubTypeDescription.Size = new System.Drawing.Size(494, 24);
            this.ItemForJobSubTypeDescription.Text = "Job Sub-Type Description:";
            this.ItemForJobSubTypeDescription.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForJobTypeDescription
            // 
            this.ItemForJobTypeDescription.Control = this.JobTypeDescriptionTextEdit;
            this.ItemForJobTypeDescription.CustomizationFormText = "Job Type Description:";
            this.ItemForJobTypeDescription.Location = new System.Drawing.Point(0, 260);
            this.ItemForJobTypeDescription.Name = "ItemForJobTypeDescription";
            this.ItemForJobTypeDescription.Size = new System.Drawing.Size(494, 24);
            this.ItemForJobTypeDescription.Text = "Job Type Description:";
            this.ItemForJobTypeDescription.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks,
            this.ItemForJobTypeJobSubTypeDescription,
            this.ItemForExpectedStartDate,
            this.ItemForRequiresAccessPermit,
            this.ItemForReactive,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.ItemForExpectedDurationUnits,
            this.ItemForExpectedDurationUnitsDescriptorID,
            this.ItemForExpectedEndDate,
            this.ItemForMinimumDaysFromLastVisit,
            this.ItemForMaximumDaysFromLastVisit,
            this.ItemForDaysLeeway,
            this.ItemForClientPONumber});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(594, 372);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 272);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(574, 80);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(147, 13);
            // 
            // ItemForJobTypeJobSubTypeDescription
            // 
            this.ItemForJobTypeJobSubTypeDescription.Control = this.JobTypeJobSubTypeDescriptionButtonEdit;
            this.ItemForJobTypeJobSubTypeDescription.CustomizationFormText = "Job Sub-Type:";
            this.ItemForJobTypeJobSubTypeDescription.Location = new System.Drawing.Point(0, 0);
            this.ItemForJobTypeJobSubTypeDescription.Name = "ItemForJobTypeJobSubTypeDescription";
            this.ItemForJobTypeJobSubTypeDescription.Size = new System.Drawing.Size(574, 24);
            this.ItemForJobTypeJobSubTypeDescription.Text = "Job Sub-Type:";
            this.ItemForJobTypeJobSubTypeDescription.TextSize = new System.Drawing.Size(147, 13);
            // 
            // ItemForExpectedStartDate
            // 
            this.ItemForExpectedStartDate.Control = this.ExpectedStartDateDateEdit;
            this.ItemForExpectedStartDate.CustomizationFormText = "Expected Start Date:";
            this.ItemForExpectedStartDate.Location = new System.Drawing.Point(0, 24);
            this.ItemForExpectedStartDate.Name = "ItemForExpectedStartDate";
            this.ItemForExpectedStartDate.Size = new System.Drawing.Size(308, 24);
            this.ItemForExpectedStartDate.Text = "Expected Start Date:";
            this.ItemForExpectedStartDate.TextSize = new System.Drawing.Size(147, 13);
            // 
            // ItemForRequiresAccessPermit
            // 
            this.ItemForRequiresAccessPermit.Control = this.RequiresAccessPermitCheckEdit;
            this.ItemForRequiresAccessPermit.CustomizationFormText = "Requires Access Permit:";
            this.ItemForRequiresAccessPermit.Location = new System.Drawing.Point(0, 225);
            this.ItemForRequiresAccessPermit.Name = "ItemForRequiresAccessPermit";
            this.ItemForRequiresAccessPermit.Size = new System.Drawing.Size(574, 23);
            this.ItemForRequiresAccessPermit.Text = "Requires Access Permit:";
            this.ItemForRequiresAccessPermit.TextSize = new System.Drawing.Size(147, 13);
            // 
            // ItemForReactive
            // 
            this.ItemForReactive.Control = this.ReactiveCheckEdit;
            this.ItemForReactive.CustomizationFormText = "Reactive:";
            this.ItemForReactive.Location = new System.Drawing.Point(0, 202);
            this.ItemForReactive.Name = "ItemForReactive";
            this.ItemForReactive.Size = new System.Drawing.Size(574, 23);
            this.ItemForReactive.Text = "Reactive:";
            this.ItemForReactive.TextSize = new System.Drawing.Size(147, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(308, 24);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(266, 168);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 192);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(574, 10);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForExpectedDurationUnits
            // 
            this.ItemForExpectedDurationUnits.Control = this.ExpectedDurationUnitsSpinEdit;
            this.ItemForExpectedDurationUnits.CustomizationFormText = "Expected Duration Units:";
            this.ItemForExpectedDurationUnits.Location = new System.Drawing.Point(0, 48);
            this.ItemForExpectedDurationUnits.Name = "ItemForExpectedDurationUnits";
            this.ItemForExpectedDurationUnits.Size = new System.Drawing.Size(308, 24);
            this.ItemForExpectedDurationUnits.Text = "Expected Duration Units:";
            this.ItemForExpectedDurationUnits.TextSize = new System.Drawing.Size(147, 13);
            // 
            // ItemForExpectedDurationUnitsDescriptorID
            // 
            this.ItemForExpectedDurationUnitsDescriptorID.Control = this.ExpectedDurationUnitsDescriptorIDGridLookUpEdit;
            this.ItemForExpectedDurationUnitsDescriptorID.CustomizationFormText = "Expected Duration Descriptor:";
            this.ItemForExpectedDurationUnitsDescriptorID.Location = new System.Drawing.Point(0, 72);
            this.ItemForExpectedDurationUnitsDescriptorID.Name = "ItemForExpectedDurationUnitsDescriptorID";
            this.ItemForExpectedDurationUnitsDescriptorID.Size = new System.Drawing.Size(308, 24);
            this.ItemForExpectedDurationUnitsDescriptorID.Text = "Expected Duration Descriptor:";
            this.ItemForExpectedDurationUnitsDescriptorID.TextSize = new System.Drawing.Size(147, 13);
            // 
            // ItemForExpectedEndDate
            // 
            this.ItemForExpectedEndDate.Control = this.ExpectedEndDateDateEdit;
            this.ItemForExpectedEndDate.CustomizationFormText = "Expected End Date:";
            this.ItemForExpectedEndDate.Location = new System.Drawing.Point(0, 96);
            this.ItemForExpectedEndDate.Name = "ItemForExpectedEndDate";
            this.ItemForExpectedEndDate.Size = new System.Drawing.Size(308, 24);
            this.ItemForExpectedEndDate.Text = "Expected End Date:";
            this.ItemForExpectedEndDate.TextSize = new System.Drawing.Size(147, 13);
            // 
            // ItemForMinimumDaysFromLastVisit
            // 
            this.ItemForMinimumDaysFromLastVisit.Control = this.MinimumDaysFromLastVisitSpinEdit;
            this.ItemForMinimumDaysFromLastVisit.CustomizationFormText = "Minimum Days From Last Visit:";
            this.ItemForMinimumDaysFromLastVisit.Location = new System.Drawing.Point(0, 120);
            this.ItemForMinimumDaysFromLastVisit.Name = "ItemForMinimumDaysFromLastVisit";
            this.ItemForMinimumDaysFromLastVisit.Size = new System.Drawing.Size(308, 24);
            this.ItemForMinimumDaysFromLastVisit.Text = "Minimum Days From Last Visit:";
            this.ItemForMinimumDaysFromLastVisit.TextSize = new System.Drawing.Size(147, 13);
            // 
            // ItemForMaximumDaysFromLastVisit
            // 
            this.ItemForMaximumDaysFromLastVisit.Control = this.MaximumDaysFromLastVisitSpinEdit;
            this.ItemForMaximumDaysFromLastVisit.CustomizationFormText = "Maximum Days From Last Visit:";
            this.ItemForMaximumDaysFromLastVisit.Location = new System.Drawing.Point(0, 144);
            this.ItemForMaximumDaysFromLastVisit.Name = "ItemForMaximumDaysFromLastVisit";
            this.ItemForMaximumDaysFromLastVisit.Size = new System.Drawing.Size(308, 24);
            this.ItemForMaximumDaysFromLastVisit.Text = "Maximum Days From Last Visit:";
            this.ItemForMaximumDaysFromLastVisit.TextSize = new System.Drawing.Size(147, 13);
            // 
            // ItemForDaysLeeway
            // 
            this.ItemForDaysLeeway.Control = this.DaysLeewaySpinEdit;
            this.ItemForDaysLeeway.CustomizationFormText = "Days Leeway:";
            this.ItemForDaysLeeway.Location = new System.Drawing.Point(0, 168);
            this.ItemForDaysLeeway.Name = "ItemForDaysLeeway";
            this.ItemForDaysLeeway.Size = new System.Drawing.Size(308, 24);
            this.ItemForDaysLeeway.Text = "Days Leeway:";
            this.ItemForDaysLeeway.TextSize = new System.Drawing.Size(147, 13);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(375, 400);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(96, 22);
            this.btnOK.TabIndex = 13;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(486, 400);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(96, 22);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // sp06177_OM_Job_Duration_Descriptors_With_BlankTableAdapter
            // 
            this.sp06177_OM_Job_Duration_Descriptors_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 3";
            this.bar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiInformation)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 3";
            // 
            // bsiInformation
            // 
            this.bsiInformation.Caption = "Complete the fields displayed. Any fields left blank will not be overwritten.";
            this.bsiInformation.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiInformation.Glyph")));
            this.bsiInformation.Id = 30;
            this.bsiInformation.Name = "bsiInformation";
            this.bsiInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bsiInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // ClientPONumberButtonEdit
            // 
            this.ClientPONumberButtonEdit.EditValue = "";
            this.ClientPONumberButtonEdit.Location = new System.Drawing.Point(162, 260);
            this.ClientPONumberButtonEdit.MenuManager = this.barManager1;
            this.ClientPONumberButtonEdit.Name = "ClientPONumberButtonEdit";
            this.ClientPONumberButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "Click me to Open Select Client Purchase Order screen", "choose", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Close, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "Click me to Clear the selected Client PO", "clear", null, true)});
            this.ClientPONumberButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ClientPONumberButtonEdit.Size = new System.Drawing.Size(420, 20);
            this.ClientPONumberButtonEdit.StyleController = this.layoutControl1;
            this.ClientPONumberButtonEdit.TabIndex = 16;
            this.ClientPONumberButtonEdit.TabStop = false;
            this.ClientPONumberButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ClientPONumberButtonEdit_ButtonClick);
            // 
            // ItemForClientPONumber
            // 
            this.ItemForClientPONumber.Control = this.ClientPONumberButtonEdit;
            this.ItemForClientPONumber.Location = new System.Drawing.Point(0, 248);
            this.ItemForClientPONumber.Name = "ItemForClientPONumber";
            this.ItemForClientPONumber.Size = new System.Drawing.Size(574, 24);
            this.ItemForClientPONumber.Text = "Client PO #:";
            this.ItemForClientPONumber.TextSize = new System.Drawing.Size(147, 13);
            // 
            // ClientPOIDTextEdit
            // 
            this.ClientPOIDTextEdit.Location = new System.Drawing.Point(162, 284);
            this.ClientPOIDTextEdit.MenuManager = this.barManager1;
            this.ClientPOIDTextEdit.Name = "ClientPOIDTextEdit";
            this.ClientPOIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientPOIDTextEdit, true);
            this.ClientPOIDTextEdit.Size = new System.Drawing.Size(420, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientPOIDTextEdit, optionsSpelling1);
            this.ClientPOIDTextEdit.StyleController = this.layoutControl1;
            this.ClientPOIDTextEdit.TabIndex = 61;
            // 
            // ItemForClientPOID
            // 
            this.ItemForClientPOID.Control = this.ClientPOIDTextEdit;
            this.ItemForClientPOID.Location = new System.Drawing.Point(0, 272);
            this.ItemForClientPOID.Name = "ItemForClientPOID";
            this.ItemForClientPOID.Size = new System.Drawing.Size(574, 24);
            this.ItemForClientPOID.Text = "Client PO ID:";
            this.ItemForClientPOID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // frm_OM_Job_Wizard_Block_Edit_Job_Info
            // 
            this.ClientSize = new System.Drawing.Size(595, 461);
            this.ControlBox = false;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_OM_Job_Wizard_Block_Edit_Job_Info";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Job Wizard - Block Edit Jobs";
            this.Load += new System.EventHandler(this.frm_OM_Job_Wizard_Block_Edit_Job_Info_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DaysLeewaySpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaximumDaysFromLastVisitSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinimumDaysFromLastVisitSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReactiveCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RequiresAccessPermitCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpectedDurationUnitsSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpectedEndDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpectedEndDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpectedStartDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpectedStartDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobSubTypeDescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobTypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobTypeDescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpectedDurationUnitsDescriptorIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06177OMJobDurationDescriptorsWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Job)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobSubTypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobTypeJobSubTypeDescriptionButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobSubTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobSubTypeDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobTypeDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobTypeJobSubTypeDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpectedStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRequiresAccessPermit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReactive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpectedDurationUnits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpectedDurationUnitsDescriptorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpectedEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMinimumDaysFromLastVisit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMaximumDaysFromLastVisit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDaysLeeway)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPONumberButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPONumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPOIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPOID)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraEditors.TextEdit JobSubTypeIDTextEdit;
        private DevExpress.XtraEditors.ButtonEdit JobTypeJobSubTypeDescriptionButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobSubTypeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobTypeJobSubTypeDescription;
        private DevExpress.XtraEditors.GridLookUpEdit ExpectedDurationUnitsDescriptorIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn colID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExpectedDurationUnitsDescriptorID;
        private DevExpress.XtraEditors.TextEdit JobTypeIDTextEdit;
        private DevExpress.XtraEditors.TextEdit JobTypeDescriptionTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobTypeDescription;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobTypeID;
        private DevExpress.XtraEditors.TextEdit JobSubTypeDescriptionTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobSubTypeDescription;
        private DevExpress.XtraEditors.DateEdit ExpectedStartDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExpectedStartDate;
        private DevExpress.XtraEditors.DateEdit ExpectedEndDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExpectedEndDate;
        private DevExpress.XtraEditors.SpinEdit ExpectedDurationUnitsSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExpectedDurationUnits;
        private System.Windows.Forms.BindingSource sp06177OMJobDurationDescriptorsWithBlankBindingSource;
        private DataSet_OM_Job dataSet_OM_Job;
        private DataSet_OM_JobTableAdapters.sp06177_OM_Job_Duration_Descriptors_With_BlankTableAdapter sp06177_OM_Job_Duration_Descriptors_With_BlankTableAdapter;
        private DevExpress.XtraEditors.CheckEdit RequiresAccessPermitCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRequiresAccessPermit;
        private DevExpress.XtraEditors.CheckEdit ReactiveCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReactive;
        private DevExpress.XtraEditors.SpinEdit MinimumDaysFromLastVisitSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMinimumDaysFromLastVisit;
        private DevExpress.XtraEditors.SpinEdit DaysLeewaySpinEdit;
        private DevExpress.XtraEditors.SpinEdit MaximumDaysFromLastVisitSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMaximumDaysFromLastVisit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDaysLeeway;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarStaticItem bsiInformation;
        private DevExpress.XtraEditors.ButtonEdit ClientPONumberButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientPONumber;
        private DevExpress.XtraEditors.TextEdit ClientPOIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientPOID;
    }
}

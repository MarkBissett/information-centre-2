using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.IO;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;
using WoodPlan5.Properties;
using WoodPlan5.Classes.Operations;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_OM_Linked_Document_Edit : BaseObjects.frmBase
    {
        #region Instance Variables
        
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        
        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //
        private bool ibool_FormStillLoading = true;

        public int intLinkedToRecordID = 0;
        public string strLinkedToRecordDesc = "";
        public int intRecordTypeID = 0;
        public string strRecordTypeDescription = "";
        public int intRecordSubTypeID = 0;
        public string strRecordSubTypeDescription = "";

        private string strDefaultPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //
        private string strTenderDefaultPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //

        #region Sub Type Permission variables

        public bool iBool_Job = false;
        public bool iBool_JobLabour = false;
        public bool iBool_JobEquipment = false;
        public bool iBool_JobMaterial = false;
        public bool iBool_JobPicture = false;
        public bool iBool_JobHSE = false;
        public bool iBool_JobCRM = false;
        public bool iBool_JobComment = false;
        public bool iBool_VisitWaste = false;
        public bool iBool_VisitSpraying = false;
        public bool iBool_JobExtraInfo = false;
        public bool iBool_JobPO = false;
        public bool iBool_ClientContract = false;
        public bool iBool_ClientContractYear = false;
        public bool iBool_SiteContract = false;
        public bool iBool_Visit = false;
        public bool iBool_Accident = false;
        public bool iBool_AccidentInjury = false;
        public bool iBool_Tender = false;

        #endregion

        #endregion

        public frm_OM_Linked_Document_Edit()
        {
            InitializeComponent();
        }

        private void frm_OM_Linked_Document_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 500126;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_LinkedDocumentPath").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Linked Files path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strTenderDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_TenderLinkedDocumentPath").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Tender Linked Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Tender Linked Files path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            
            // Populate Main Dataset //
            this.sp06045_OM_Linked_Document_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = dataSet_GC_Summer_DataEntry.sp06045_OM_Linked_Document_Edit.NewRow();
                        drNewRow["strMode"] = strFormMode;
                        drNewRow["LinkedDocumentID"] = 0;
                        drNewRow["LinkedToRecordID"] = intLinkedToRecordID;
                        drNewRow["LinkedRecordDescription"] = strLinkedToRecordDesc;
                        drNewRow["LinkedDocumentTypeID"] = intRecordTypeID;
                        drNewRow["LinkedDocumentType"] = strRecordTypeDescription;
                        drNewRow["LinkedDocumentSubTypeID"] = intRecordSubTypeID;
                        drNewRow["LinkedDocumentSubType"] = strRecordSubTypeDescription;
                        drNewRow["AddedByStaffID"] = this.GlobalSettings.UserID;
                        drNewRow["AddedByStaffName"] = this.GlobalSettings.UserSurname + ": " + this.GlobalSettings.UserForename;
                        drNewRow["DateAdded"] = DateTime.Now;
                        dataSet_GC_Summer_DataEntry.sp06045_OM_Linked_Document_Edit.Rows.Add(drNewRow);
                    }
                    catch (Exception) { }
                    break;
                case "blockadd":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = dataSet_GC_Summer_DataEntry.sp06045_OM_Linked_Document_Edit.NewRow();
                        drNewRow["strMode"] = strFormMode;
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["LinkedToRecordID"] = 0;
                        drNewRow["LinkedDocumentTypeID"] = 0;
                        drNewRow["LinkedRecordDescription"] = "Not Applicable - Block Adding";
                        drNewRow["LinkedDocumentTypeID"] = intRecordTypeID;
                        drNewRow["LinkedDocumentType"] = strRecordTypeDescription;
                        drNewRow["LinkedDocumentSubTypeID"] = intRecordSubTypeID;
                        drNewRow["LinkedDocumentSubType"] = strRecordSubTypeDescription;
                        drNewRow["AddedByStaffID"] = this.GlobalSettings.UserID;
                        drNewRow["AddedByStaffName"] = this.GlobalSettings.UserSurname + ": " + this.GlobalSettings.UserForename;
                        drNewRow["DateAdded"] = DateTime.Now;
                        dataSet_GC_Summer_DataEntry.sp06045_OM_Linked_Document_Edit.Rows.Add(drNewRow);
                    }
                    catch (Exception) { }
                    break;
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = dataSet_GC_Summer_DataEntry.sp06045_OM_Linked_Document_Edit.NewRow();
                        drNewRow["strMode"] = strFormMode;
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["LinkedDocumentID"] = 0;
                        drNewRow["LinkedToRecordID"] = 0;
                        drNewRow["LinkedDocumentTypeID"] = 0;
                        dataSet_GC_Summer_DataEntry.sp06045_OM_Linked_Document_Edit.Rows.Add(drNewRow);
                        dataSet_GC_Summer_DataEntry.sp06045_OM_Linked_Document_Edit.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
                    }
                    catch (Exception) { }
                    break;
                case "edit":
                case "view":
                    try
                    {
                        sp06045_OM_Linked_Document_EditTableAdapter.Fill(dataSet_GC_Summer_DataEntry.sp06045_OM_Linked_Document_Edit, strRecordIDs, strFormMode);
                    }
                    catch (Exception) { }
                    break;
            }
            dataLayoutControl1.EndUpdate();

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
        }


        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (this.dataSet_GC_Summer_DataEntry.sp06045_OM_Linked_Document_Edit.Rows.Count == 0)
            {
                _KeepWaitFormOpen = false;
                splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Linked Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        LinkedRecordDescriptionButtonEdit.Focus();

                        LinkedRecordDescriptionButtonEdit.Properties.ReadOnly = false;
                        LinkedRecordDescriptionButtonEdit.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        LinkedRecordDescriptionButtonEdit.Focus();

                        LinkedRecordDescriptionButtonEdit.Properties.ReadOnly = true;
                        LinkedRecordDescriptionButtonEdit.Properties.Buttons[0].Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        DescriptionTextEdit.Focus();

                        LinkedRecordDescriptionButtonEdit.Properties.ReadOnly = false;
                        LinkedRecordDescriptionButtonEdit.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        DescriptionTextEdit.Focus();

                        LinkedRecordDescriptionButtonEdit.Properties.ReadOnly = true;
                        LinkedRecordDescriptionButtonEdit.Properties.Buttons[0].Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }
            ibool_ignoreValidation = false;
            ibool_FormStillLoading = false;
        }

        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_GC_Summer_DataEntry.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }


        private void frm_OM_Linked_Document_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_OM_Linked_Document_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            sp06045OMLinkedDocumentEditBindingSource.EndEdit();
            try
            {
                sp06045_OM_Linked_Document_EditTableAdapter.Update(dataSet_GC_Summer_DataEntry);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)sp06045OMLinkedDocumentEditBindingSource.Current;
                if (currentRow != null) strNewIDs = Convert.ToInt32(currentRow["LinkedDocumentID"]) + ";";
            }
            else if (this.strFormMode.ToLower() == "blockadd")
            {
                DataRowView currentRow = (DataRowView)sp06045OMLinkedDocumentEditBindingSource.Current;
                if (currentRow != null) strNewIDs = Convert.ToString(currentRow["strRecordIDs"]);  // Field originally held district IDs, but now holds new record linked document ids (changed via Update SP) //
            }

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_OM_Linked_Document_Manager")
                    {
                        var fParentForm = (frm_OM_Linked_Document_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, strNewIDs);
                    }
                    if (frmChild.Name == "frm_OM_Job_Manager")
                    {
                        var fParentForm = (frm_OM_Job_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, Classes.Operations.Utils.enmFocusedGrid.Jobs, "");
                    }
                    if (frmChild.Name == "frm_HR_Pension_Manager")
                    {
                        var fParentForm = (frm_HR_Pension_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, "", "");
                    }
                    if (frmChild.Name == "frm_HR_Sanction_Manager")
                    {
                        var fParentForm = (frm_HR_Sanction_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, "");
                    }
                    if (frmChild.Name == "frm_HR_Qualifications_Manager")
                    {
                        var fParentForm = (frm_HR_Qualifications_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, "");
                    }
                    if (frmChild.Name == "frm_HR_Holiday_Manager")
                    {
                        var fParentForm = (frm_HR_Holiday_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, "", "");
                    }
                    if (frmChild.Name == "frm_HR_Vetting_Manager")
                    {
                        var fParentForm = (frm_HR_Vetting_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, "", "");
                    }
                    if (frmChild.Name == "frm_HR_Bonus_Manager")
                    {
                        var fParentForm = (frm_HR_Bonus_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, "");
                    }
                    if (frmChild.Name == "frm_HR_CRM_Manager")
                    {
                        var fParentForm = (frm_HR_CRM_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, "");
                    }
                    if (frmChild.Name == "frm_OM_Tender_Manager")
                    {
                        var fParentForm = (frm_OM_Tender_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(5, Utils.enmFocusedGrid.LinkedDocument, strNewIDs);
                    }
                }
            }

            SetMenuStatus();  // Disables Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < dataSet_GC_Summer_DataEntry.sp06045_OM_Linked_Document_Edit.Rows.Count; i++)
            {
                switch (dataSet_GC_Summer_DataEntry.sp06045_OM_Linked_Document_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                if (this.strFormMode != "blockedit")
                {
                    strMessage = "You have unsaved changes on the current screen...\n\n";
                    if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                    if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                    if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
                }
                else  // Block Editing //
                {
                    strMessage = "You have unsaved block edit changes on the current screen...\n";
                }
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }

        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void LinkedDocumentTypeButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)sp06045OMLinkedDocumentEditBindingSource.Current;
                if (currentRow == null) return;
                int TypeID = (string.IsNullOrEmpty(currentRow["LinkedDocumentTypeID"].ToString()) ? 0 : Convert.ToInt32(currentRow["LinkedDocumentTypeID"]));
                int SubTypeID = (string.IsNullOrEmpty(currentRow["LinkedDocumentSubTypeID"].ToString()) ? 0 : Convert.ToInt32(currentRow["LinkedDocumentSubTypeID"]));

                var fChildForm = new frm_OM_Select_Linked_Document_Type();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intPassedInTypeID = TypeID;
                fChildForm.intPassedInSubTypeID = SubTypeID;
                fChildForm.iBool_Job = iBool_Job;
                fChildForm.iBool_JobLabour = iBool_JobLabour;
                fChildForm.iBool_JobEquipment = iBool_JobEquipment;
                fChildForm.iBool_JobMaterial = iBool_JobMaterial;
                fChildForm.iBool_JobPicture = iBool_JobPicture;
                fChildForm.iBool_JobHSE = iBool_JobHSE;
                fChildForm.iBool_JobCRM = iBool_JobCRM;
                fChildForm.iBool_JobComment = iBool_JobComment;
                fChildForm.iBool_VisitWaste = iBool_VisitWaste;
                fChildForm.iBool_VisitSpraying = iBool_VisitSpraying;
                fChildForm.iBool_JobExtraInfo = iBool_JobExtraInfo;
                fChildForm.iBool_JobPO = iBool_JobPO;
                fChildForm.iBool_ClientContract = iBool_ClientContract;
                fChildForm.iBool_ClientContractYear = iBool_ClientContractYear;
                fChildForm.iBool_SiteContract = iBool_SiteContract;
                fChildForm.iBool_Visit = iBool_Visit;
                fChildForm.iBool_Accident = iBool_Accident;
                fChildForm.iBool_AccidentInjury = iBool_AccidentInjury;
                fChildForm.iBool_Tender = iBool_Tender;

                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow["LinkedDocumentTypeID"] = fChildForm.intSelectedTypeID;
                    currentRow["LinkedDocumentSubTypeID"] = fChildForm.intSelectedSubTypeID;
                    currentRow["LinkedDocumentType"] = fChildForm.strSelectedTypeName;
                    currentRow["LinkedDocumentSubType"] = fChildForm.strSelectedSubTypeName;

                    if (TypeID != fChildForm.intSelectedTypeID)
                    {
                        currentRow["LinkedToRecordID"] = 0;
                        currentRow["LinkedRecordDescription"] = "";
                        if (strFormMode == "add" || strFormMode == "edit") dxErrorProvider1.SetError(LinkedRecordDescriptionButtonEdit, "Select a value.");
                    }
                    sp06045OMLinkedDocumentEditBindingSource.EndEdit();
                }
            }
        }
        private void LinkedDocumentTypeButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(LinkedDocumentTypeButtonEdit, "Enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(LinkedDocumentTypeButtonEdit, "");
            }
        }

        private void LinkedRecordDescriptionButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            DataRowView currentRow = (DataRowView)sp06045OMLinkedDocumentEditBindingSource.Current;
            if (currentRow == null) return;
            int RecordTypeID = (string.IsNullOrEmpty(currentRow["LinkedDocumentTypeID"].ToString()) ? 0 : Convert.ToInt32(currentRow["LinkedDocumentTypeID"]));
            if (RecordTypeID <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select the Linked Document Type before proceeding.", "Link Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            int intOriginalID = (string.IsNullOrEmpty(currentRow["LinkedToRecordID"].ToString()) ? 0 : Convert.ToInt32(currentRow["LinkedToRecordID"]));
            switch (RecordTypeID)
            {
                case 21:  // Job // 
                    {
                        var fChildForm = new frm_OM_Select_Job();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.intOriginalJobID = intOriginalID;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            if (intOriginalID == fChildForm.intSelectedJobID) return;
                            currentRow["LinkedToRecordID"] = fChildForm.intSelectedJobID;
                            currentRow["LinkedRecordDescription"] = "Client: " + fChildForm.strSelectedClientName + ", Site: " + fChildForm.strSelectedSiteName + ", Visit #: " + fChildForm.intSelectedVisitNumber.ToString() + ", Job: " + fChildForm.strSelectedJobTypeDescription + " - " + fChildForm.strSelectedJobSubTypeDescription;
                            sp06045OMLinkedDocumentEditBindingSource.EndEdit();
                        }
                    }
                    break;
                case 22:  // Job Labour // 
                    {
                        var fChildForm = new frm_OM_Select_Job_Labour();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.intOriginalLabourUsedID = intOriginalID;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            if (intOriginalID == fChildForm.intSelectedLabourUsedID) return;
                            currentRow["LinkedToRecordID"] = fChildForm.intSelectedLabourUsedID;
                            currentRow["LinkedRecordDescription"] = "Client: " + fChildForm.strSelectedClientName + ", Site: " + fChildForm.strSelectedSiteName + ", Visit #: " + fChildForm.intSelectedVisitNumber.ToString() + ", Job: " + fChildForm.strSelectedJobTypeDescription + " - " + fChildForm.strSelectedJobSubTypeDescription + ", Contractor: " + fChildForm.strSelectedContractorName;
                            sp06045OMLinkedDocumentEditBindingSource.EndEdit();
                        }
                    }
                    break;
                case 23:  // Job Equipment // 
                    {
                        var fChildForm = new frm_OM_Select_Job_Equipment();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.intOriginalEquipmentUsedID = intOriginalID;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            if (intOriginalID == fChildForm.intSelectedEquipmentUsedID) return;
                            currentRow["LinkedToRecordID"] = fChildForm.intSelectedEquipmentUsedID;
                            currentRow["LinkedRecordDescription"] = "Client: " + fChildForm.strSelectedClientName + ", Site: " + fChildForm.strSelectedSiteName + ", Visit #: " + fChildForm.intSelectedVisitNumber.ToString() + ", Job: " + fChildForm.strSelectedJobTypeDescription + " - " + fChildForm.strSelectedJobSubTypeDescription + ", Equipment: " + fChildForm.strSelectedEquipmentName;
                            sp06045OMLinkedDocumentEditBindingSource.EndEdit();
                        }
                    }
                    break;
                case 24:  // Job Materials // 
                    {
                        var fChildForm = new frm_OM_Select_Job_Material();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.intOriginalMaterialUsedID = intOriginalID;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            if (intOriginalID == fChildForm.intSelectedMaterialUsedID) return;
                            currentRow["LinkedToRecordID"] = fChildForm.intSelectedMaterialUsedID;
                            currentRow["LinkedRecordDescription"] = "Client: " + fChildForm.strSelectedClientName + ", Site: " + fChildForm.strSelectedSiteName + ", Visit #: " + fChildForm.intSelectedVisitNumber.ToString() + ", Job: " + fChildForm.strSelectedJobTypeDescription + " - " + fChildForm.strSelectedJobSubTypeDescription + ", Material: " + fChildForm.strSelectedMaterialName;
                            sp06045OMLinkedDocumentEditBindingSource.EndEdit();
                        }
                    }
                    break;
                case 25:  // Job Picture // 
                    {
                        var fChildForm = new frm_OM_Select_Job_Picture();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.intOriginalPictureID = intOriginalID;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            if (intOriginalID == fChildForm.intSelectedPictureID) return;
                            currentRow["LinkedToRecordID"] = fChildForm.intSelectedPictureID;
                            currentRow["LinkedRecordDescription"] = "Client: " + fChildForm.strSelectedClientName + ", Site: " + fChildForm.strSelectedSiteName + ", Visit #: " + fChildForm.intSelectedVisitNumber.ToString() + ", Job: " + fChildForm.strSelectedJobTypeDescription + " - " + fChildForm.strSelectedJobSubTypeDescription + ", Picture: " + fChildForm.strSelectedDateTime;
                            sp06045OMLinkedDocumentEditBindingSource.EndEdit();
                        }
                    }
                    break;
                case 26:  // Job Health and Safety Requirement // 
                    {
                        var fChildForm = new frm_OM_Select_Job_Safety();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.intOriginalSafetyUsedID = intOriginalID;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            if (intOriginalID == fChildForm.intSelectedSafetyUsedID) return;
                            currentRow["LinkedToRecordID"] = fChildForm.intSelectedSafetyUsedID;
                            currentRow["LinkedRecordDescription"] = "Client: " + fChildForm.strSelectedClientName + ", Site: " + fChildForm.strSelectedSiteName + ", Visit #: " + fChildForm.intSelectedVisitNumber.ToString() + ", Health & Safety Requirement: " + fChildForm.strSelectedSafetyRequirement;
                            sp06045OMLinkedDocumentEditBindingSource.EndEdit();
                        }
                    }
                    break;
                case 28:  // Job Comment // 
                    {
                        var fChildForm = new frm_OM_Select_Job_Comment();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.intOriginalCommentID = intOriginalID;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            if (intOriginalID == fChildForm.intSelectedCommentID) return;
                            currentRow["LinkedToRecordID"] = fChildForm.intSelectedCommentID;
                            currentRow["LinkedRecordDescription"] = "Client: " + fChildForm.strSelectedClientName + ", Site: " + fChildForm.strSelectedSiteName + ", Visit #: " + fChildForm.intSelectedVisitNumber.ToString() + ", Job: " + fChildForm.strSelectedJobTypeDescription + " - " + fChildForm.strSelectedJobSubTypeDescription + ", Comment Date: " + fChildForm.dtDateRecorded.ToString("dd/MM/yyyy HH:mm");
                            sp06045OMLinkedDocumentEditBindingSource.EndEdit();
                        }
                    }
                    break;
                case 29:  // Job Waste // 
                    {
                        if (intRecordSubTypeID == 0)
                        {
                            var fChildForm = new frm_OM_Select_Visit_Waste();
                            fChildForm.GlobalSettings = this.GlobalSettings;
                            fChildForm.intOriginalJobWasteID = intOriginalID;
                            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                            {
                                if (intOriginalID == fChildForm.intSelectedJobWasteID) return;
                                currentRow["LinkedToRecordID"] = fChildForm.intSelectedJobWasteID;
                                currentRow["LinkedRecordDescription"] = "Client: " + fChildForm.strSelectedClientName + ", Site: " + fChildForm.strSelectedSiteName + ", Visit #: " + fChildForm.intSelectedVisitNumber.ToString() + ", Waste: " + fChildForm.strSelectedWasteType;
                                sp06045OMLinkedDocumentEditBindingSource.EndEdit();
                            }
                        }
                        else
                        {
                            var fChildForm = new frm_OM_Select_Job_Waste();
                            fChildForm.GlobalSettings = this.GlobalSettings;
                            fChildForm.intOriginalJobWasteID = intOriginalID;
                            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                            {
                                if (intOriginalID == fChildForm.intSelectedJobWasteID) return;
                                currentRow["LinkedToRecordID"] = fChildForm.intSelectedJobWasteID;
                                currentRow["LinkedRecordDescription"] = "Client: " + fChildForm.strSelectedClientName + ", Site: " + fChildForm.strSelectedSiteName + ", Visit #: " + fChildForm.intSelectedVisitNumber.ToString() + ", Job: " + fChildForm.strSelectedJobTypeDescription + " - " + fChildForm.strSelectedJobSubTypeDescription + ", Waste: " + fChildForm.strSelectedWasteType;
                                sp06045OMLinkedDocumentEditBindingSource.EndEdit();
                            }
                        }
                    }
                    break;
                case 30:  // Job Chemical // 
                    {
                        if (intRecordSubTypeID == 0)
                        {
                            var fChildForm = new frm_OM_Select_Visit_Chemical();
                            fChildForm.GlobalSettings = this.GlobalSettings;
                            fChildForm.intOriginalSprayingID = intOriginalID;
                            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                            {
                                if (intOriginalID == fChildForm.intSelectedSprayingID) return;
                                currentRow["LinkedToRecordID"] = fChildForm.intSelectedSprayingID;
                                currentRow["LinkedRecordDescription"] = "Client: " + fChildForm.strSelectedClientName + ", Site: " + fChildForm.strSelectedSiteName + ", Visit #: " + fChildForm.intSelectedVisitNumber.ToString() + ", Spraying: " + fChildForm.strSelectedChemicalName;
                                sp06045OMLinkedDocumentEditBindingSource.EndEdit();
                            }
                        }
                        else
                        {
                            var fChildForm = new frm_OM_Select_Job_Chemical();
                            fChildForm.GlobalSettings = this.GlobalSettings;
                            fChildForm.intOriginalSprayingID = intOriginalID;
                            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                            {
                                if (intOriginalID == fChildForm.intSelectedSprayingID) return;
                                currentRow["LinkedToRecordID"] = fChildForm.intSelectedSprayingID;
                                currentRow["LinkedRecordDescription"] = "Client: " + fChildForm.strSelectedClientName + ", Site: " + fChildForm.strSelectedSiteName + ", Visit #: " + fChildForm.intSelectedVisitNumber.ToString() + ", Job: " + fChildForm.strSelectedJobTypeDescription + " - " + fChildForm.strSelectedJobSubTypeDescription + ", Spraying: " + fChildForm.strSelectedChemicalName;
                                sp06045OMLinkedDocumentEditBindingSource.EndEdit();
                            }
                        }
                    }
                    break;
                case 31:  // Job Attribute // 
                    {
                        var fChildForm = new frm_OM_Select_Job_Attribute();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.intOriginalJobAttributeID = intOriginalID;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            if (intOriginalID == fChildForm.intSelectedJobAttributeID) return;
                            currentRow["LinkedToRecordID"] = fChildForm.intSelectedJobAttributeID;
                            currentRow["LinkedRecordDescription"] = "Client: " + fChildForm.strSelectedClientName + ", Site: " + fChildForm.strSelectedSiteName + ", Visit #: " + fChildForm.intSelectedVisitNumber.ToString() + ", Job: " + fChildForm.strSelectedJobTypeDescription + " - " + fChildForm.strSelectedJobSubTypeDescription + ", Attribute: " + fChildForm.strSelectedOnScreenLabel;
                            sp06045OMLinkedDocumentEditBindingSource.EndEdit();
                        }
                    }
                    break;
                case 41:  // Client Contract // 
                    {
                        var fChildForm = new frm_OM_Select_Client_Contract();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.intOriginalSelectedID = intOriginalID;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            if (intOriginalID == fChildForm.intSelectedID) return;
                            currentRow["LinkedToRecordID"] = fChildForm.intSelectedID;
                            currentRow["LinkedRecordDescription"] = fChildForm.strSelectedDescription;
                            sp06045OMLinkedDocumentEditBindingSource.EndEdit();
                        }
                    }
                    break;
                case 42:  // Client Contract Year // 
                    {
                        var fChildForm = new frm_OM_Select_Client_Contract_Year();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.intOriginalChildSelectedID = intOriginalID;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            if (intOriginalID == fChildForm.intSelectedChildID) return;
                            currentRow["LinkedToRecordID"] = fChildForm.intSelectedChildID;
                            currentRow["LinkedRecordDescription"] = fChildForm.strSelectedChildDescription;
                            sp06045OMLinkedDocumentEditBindingSource.EndEdit();
                        }
                    }
                    break;
                case 51:  // Site Contract // 
                    {
                        var fChildForm = new frm_OM_Select_Site_Contract();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.intOriginalParentSelectedID = 0;
                        fChildForm.intOriginalChildSelectedID = intOriginalID;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            if (intOriginalID == fChildForm.intSelectedChildID) return;
                            currentRow["LinkedToRecordID"] = fChildForm.intSelectedChildID;
                            currentRow["LinkedRecordDescription"] = fChildForm.strSelectedChildDescription;
                            sp06045OMLinkedDocumentEditBindingSource.EndEdit();
                        }
                    }
                    break;
                case 52:  // Visit // 
                    {
                        var fChildForm = new frm_OM_Select_Visit();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.intOriginalClientContractID = 0;
                        fChildForm.intOriginalSiteContractID = 0;
                        fChildForm.intOriginalVisitID = intOriginalID;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            if (intOriginalID == fChildForm.intSelectedVisitID) return;
                            currentRow["LinkedToRecordID"] = fChildForm.intSelectedVisitID;
                            currentRow["LinkedRecordDescription"] = fChildForm.strSelectedChildDescription;
                            sp06045OMLinkedDocumentEditBindingSource.EndEdit();
                        }
                    }
                    break;
                case 61:  // Accident // 
                    {
                        var fChildForm = new frm_Core_Accident_Select();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm._OriginalAccidentID = intOriginalID;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            if (intOriginalID == fChildForm._SelectedAccidentID) return;
                            currentRow["LinkedToRecordID"] = fChildForm._SelectedAccidentID;
                            currentRow["LinkedRecordDescription"] = fChildForm._SelectedAccidentDescription;
                            sp06045OMLinkedDocumentEditBindingSource.EndEdit();
                        }
                    }
                    break;
                case 62:  // Accident Injury // 
                    {
                        var fChildForm = new frm_Core_Accident_Injury_Select();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm._OriginalInjuryID = intOriginalID;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            if (intOriginalID == fChildForm._SelectedInjuryID) return;
                            currentRow["LinkedToRecordID"] = fChildForm._SelectedInjuryID;
                            currentRow["LinkedRecordDescription"] = fChildForm._FullSelectedInjuryDescription;
                            sp06045OMLinkedDocumentEditBindingSource.EndEdit();
                        }
                    }
                    break;
                case 71:  // Tender // 
                    {
                        var fChildForm = new frm_OM_Select_Tender();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.intOriginalParentID = 0;
                        fChildForm.intOriginalChildID = intOriginalID;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            if (intOriginalID == fChildForm.intSelectedChildID) return;
                            currentRow["LinkedToRecordID"] = fChildForm.intSelectedChildID;
                            currentRow["LinkedRecordDescription"] = fChildForm.strSelectedChildDescriptionFull;
                            sp06045OMLinkedDocumentEditBindingSource.EndEdit();
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        private void LinkedRecordDescriptionButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit glue = (ButtonEdit)sender;
            string strValue = glue.EditValue.ToString();
            if (this.strFormMode != "blockadd" && this.strFormMode != "blockedit" && String.IsNullOrEmpty(strValue))
            {
                dxErrorProvider1.SetError(LinkedRecordDescriptionButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(LinkedRecordDescriptionButtonEdit, "");
            }
        }

        private void DescriptionTextEdit_Validating(object sender, CancelEventArgs e)
        {
            TextEdit te = (TextEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(te.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(DescriptionTextEdit, "Enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(DescriptionTextEdit, "");
            }
        }
 
        private void DocumentPathButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(DocumentPathButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(DocumentPathButtonEdit, "");
            }

        }
        private void DocumentPathButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DataRowView currentRow = (DataRowView)sp06045OMLinkedDocumentEditBindingSource.Current;
            if (currentRow == null) return;
            int intRecordTypeID = 0;
            try
            {
                intRecordTypeID = Convert.ToInt32(currentRow["LinkedDocumentTypeID"]);
            }
            catch (Exception) { }
            string strPath = (intRecordTypeID == 71 ? strTenderDefaultPath : strDefaultPath);

            switch (e.Button.Tag.ToString())
            {
                case "select file":
                    {
                        if (strFormMode == "view") return;
                        using (OpenFileDialog dlg = new OpenFileDialog())
                        {
                            dlg.DefaultExt = "tab";
                            dlg.Filter = "";
                            if (strPath != "") dlg.InitialDirectory = strPath;
                            dlg.Multiselect = false;
                            dlg.ShowDialog();
                            if (dlg.FileNames.Length > 0)
                            {
                                string strTempFileName = "";
                                string strTempResult = "";
                                string strExtension = "";
                                foreach (string filename in dlg.FileNames)
                                {
                                    if (strPath != "")
                                        if (!filename.ToLower().StartsWith(strPath.ToLower()))
                                        {
                                            DevExpress.XtraEditors.XtraMessageBox.Show("You can only select files from under the default folder [as specified in the System Configuration Screen - Default Linked Documents Folder] or one of it's sub-folders.\n\nFile Selection Aborted!", "Select File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                            break;
                                        }
                                    strTempFileName = filename.Substring(strPath.Length);
                                    if (strTempResult == "")
                                    {
                                        if (!strTempResult.Contains(strTempFileName)) strTempResult += strTempFileName;
                                    }
                                    else
                                    {
                                        if (!strTempResult.Contains(strTempFileName)) strTempResult += "\r\n" + strTempFileName;
                                    }
                                    // Update Extension Field with extension from filename //
                                    strExtension = System.IO.Path.GetExtension(filename);
                                    if (!string.IsNullOrEmpty(strExtension))
                                    {
                                        currentRow = (DataRowView)sp06045OMLinkedDocumentEditBindingSource.Current;
                                        if (currentRow != null)
                                        {
                                            currentRow["DocumentExtension"] = strExtension.Substring(1).ToUpper();  // Ignore the "." at the start of the extension //
                                        }
                                    }
                                }
                                DocumentPathButtonEdit.Text = strTempResult;
                            }
                            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter so linked file can be viewed if required //
                        }
                    }
                    break;
                case "view file":
                    {
                        currentRow = (DataRowView)sp06045OMLinkedDocumentEditBindingSource.Current;
                        if (currentRow == null) return;
                        string strFile = currentRow["DocumentPath"].ToString();
                        string strExtension = currentRow["DocumentExtension"].ToString();

                        if (string.IsNullOrEmpty(strFile))
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Link the File to be Viewed before proceeding.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        try
                        {
                            if (strExtension.ToLower() != "pdf")
                            {
                                System.Diagnostics.Process.Start(Path.Combine(strPath + strFile));
                            }
                            else
                            {
                                if (!BaseObjects.Keyboard.IsKeyDown(Keys.A))  // Only open in DevExpress PDF viewer if user is not holding down A key (CTRL Key doesn't work when fired from a grid as it's used for row selection) //
                                {
                                    frm_Core_PDF_Viewer fChildForm = new frm_Core_PDF_Viewer();
                                    fChildForm.strPDFFile = Path.Combine(strPath + strFile);
                                    fChildForm.MdiParent = this.MdiParent;
                                    fChildForm.GlobalSettings = this.GlobalSettings;
                                    fChildForm.Show();
                                }
                                else
                                {
                                    System.Diagnostics.Process.Start(Path.Combine(strPath + strFile));
                                }
                            }
                        }
                        catch
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view the Linked Document: " + Path.Combine(strPath + strFile) + ".\n\nThe File may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                    break;
            }
        }

        #endregion













   
    }
}


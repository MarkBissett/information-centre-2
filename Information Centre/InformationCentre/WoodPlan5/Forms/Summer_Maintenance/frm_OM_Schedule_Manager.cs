using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;  // Required for Path Statement //

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using DevExpress.XtraReports;
using DevExpress.XtraReports.UserDesigner;
using DevExpress.XtraReports.Design;
using DevExpress.XtraReports.UI;
using System.ComponentModel.Design;  // Used by process to auto link event for custom sorting when a object is dropped onto the design panel of the report //

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Reports;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_OM_Schedule_Manager : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;

        private int i_int_FocusedGrid = 1;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState2;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState8;  // Used by Grid View State Facilities //

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs2 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs8 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        DateTime _dtDateTime = DateTime.MaxValue;

        private DateTime i_dtStart = DateTime.MinValue;
        private DateTime i_dtEnd = DateTime.MaxValue;

        public string i_str_SavedDirectoryName = "";
        public string i_str_SavedSchedulePDFs = "";

        WaitDialogForm loadingForm;
        rpt_OM_Team_Schedule rptReport;

        bool iBool_EditLayoutButtonEnabled = false;

        #endregion

        public frm_OM_Schedule_Manager()
        {
            InitializeComponent();
        }

        private void frm_OM_Schedule_Manager_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 7015;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;
            
            _dtDateTime = DateTime.Now;

            i_dtStart = new DateTime(_dtDateTime.Year, 1, 1);
            i_dtEnd = i_dtStart.AddYears(1).AddSeconds(-1);
            dateEditFromDate.DateTime = i_dtStart;
            dateEditToDate.DateTime = i_dtEnd;
            barEditItemDateRange.EditValue = PopupContainerEditDateRange_Get_Selected();

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(this.dataSet_AT.sp00039GetFormPermissionsForUser, this.FormID, this.GlobalSettings.UserID, 0, this.GlobalSettings.ViewedPeriodID);  // 1 = Staff //
            ProcessPermissionsForForm();

            Set_Grid_Highlighter_Transparent(this.Controls);

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                i_str_SavedDirectoryName = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_ReportLayouts").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for the Team Schedule Layouts (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Report Layouts Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (!i_str_SavedDirectoryName.EndsWith("\\")) i_str_SavedDirectoryName += "\\";  // Add Backslash to end //

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                i_str_SavedSchedulePDFs = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_ScheduleReportSavedSentReports").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for the Saved Team Schedules (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Saved Team Schedules Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (!i_str_SavedSchedulePDFs.EndsWith("\\")) i_str_SavedSchedulePDFs += "\\";  // Add Backslash to end //

            
            sp06252_OM_Teams_With_Outstanding_Job_SchedulesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState1 = new RefreshGridState(gridView1, "ContractorID");

            sp06253_OM_Sent_Job_SchedulesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState2 = new RefreshGridState(gridView2, "SentScheduleID");

            
            sp06254_OM_Teams_With_JobsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState8 = new RefreshGridState(gridView8, "ContractorID");

            emptyEditor = new RepositoryItem();
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
            Application.DoEvents();  // Allow Form time to repaint itself //
            LoadLastSavedUserScreenSettings();
            Load_Data_1();  // Load records //
            Load_Data_8();  // Load records //
            Load_Data_2();  // Load records //

        }

        private void frm_OM_Schedule_Manager_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedRecordIDs1))
                {
                    Load_Data_1();
                }
                if (UpdateRefreshStatus == 2 || !string.IsNullOrEmpty(i_str_AddedRecordIDs2))
                {
                    Load_Data_2();
                }
                if (UpdateRefreshStatus == 8 || !string.IsNullOrEmpty(i_str_AddedRecordIDs8))
                {
                    Load_Data_8();
                }
            }
            SetMenuStatus();
        }

        private void frm_OM_Schedule_Manager_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                // Show Active Patterns //
                /*string strFilter = default_screen_settings.RetrieveSetting("ActivePatterns");
                if (!(string.IsNullOrEmpty(strFilter)))
                {
                    beiShowActiveOnly.EditValue = (strFilter == "0" ? 0 : 1);
                }*/

                Load_Data_1();
            }
        }

        public void UpdateFormRefreshStatus(int status, string strNewIDs1, string strNewIDs2, string strNewIDs8)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
            if (strNewIDs2 != "") i_str_AddedRecordIDs2 = strNewIDs2;
            if (strNewIDs8 != "") i_str_AddedRecordIDs8 = strNewIDs8;
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:    // Whole Form //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                    case 1:  // Add Wizard Button //    
                        if (sfpPermissions.blRead)
                        {
                            iBool_EditLayoutButtonEnabled = true;
                        }
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = null;
            int[] intRowHandles;
            if (i_int_FocusedGrid == 1)
            {
                view = (GridView)gridControl1.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                bbiPreviewReport.Enabled = (intRowHandles.Length == 1);
                bbiSend.Enabled = (intRowHandles.Length >= 1); 
            }
            else if (i_int_FocusedGrid == 2)
            {
                view = (GridView)gridControl2.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                bbiPreviewReport.Enabled = (intRowHandles.Length == 1);
                bbiSend.Enabled = (intRowHandles.Length >= 1);
            }
            else if (i_int_FocusedGrid == 8)
            {
                view = (GridView)gridControl8.MainView;
                intRowHandles = view.GetSelectedRows();
                bbiPreviewReport.Enabled = (intRowHandles.Length == 1);
                bbiSend.Enabled = (intRowHandles.Length >= 1);
            }

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView2 navigator custom buttons //
            view = (GridView)gridControl2.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl2.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControl2.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = true;

            // Set Enabled Status of Main Toolbar Buttons //
            bbiEditReportLayout.Enabled = iBool_EditLayoutButtonEnabled;


        }


        private void Load_Data_1()
        {
            _dtDateTime = DateTime.Now;
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();
            GridView view = (GridView)gridControl1.MainView;
            RefreshGridViewState1.SaveViewInfo();
            view.BeginUpdate();
            sp06252_OM_Teams_With_Outstanding_Job_SchedulesTableAdapter.Fill(dataSet_OM_Visit.sp06252_OM_Teams_With_Outstanding_Job_Schedules, _dtDateTime);
            RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
            view.ExpandAllGroups();
            view.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                //GridView view = (GridView)gridControl1.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["ContractorID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }

            if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
        }

        private void Load_Data_2()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();

            GridView view = (GridView)gridControl2.MainView;
            RefreshGridViewState2.SaveViewInfo();
            view.BeginUpdate();
            sp06253_OM_Sent_Job_SchedulesTableAdapter.Fill(dataSet_OM_Visit.sp06253_OM_Sent_Job_Schedules, i_dtStart);
            RefreshGridViewState2.LoadViewInfo();  // Reload any expanded groups and selected rows //
            view.ExpandAllGroups();
            view.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs2 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs2.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                //GridView view = (GridView)gridControl1.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["SentScheduleID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs2 = "";
            }

            if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
        }

        private void Load_Data_8()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();

            GridView view = (GridView)gridControl8.MainView;
            RefreshGridViewState8.SaveViewInfo();
            view.BeginUpdate();
            sp06254_OM_Teams_With_JobsTableAdapter.Fill(dataSet_OM_Visit.sp06254_OM_Teams_With_Jobs, _dtDateTime, i_dtStart, i_dtEnd);
            RefreshGridViewState8.LoadViewInfo();  // Reload any expanded groups and selected rows //
            view.ExpandAllGroups();
            view.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs8 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs8.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                //GridView view = (GridView)gridControl1.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["ContractorID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs8 = "";
            }

            if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Outstanding Schedules To Be Sent - Adjust any filters or click refresh button";
                    break;
                case "gridView8":
                    message = "No Teams with Work - Adjust any filters or click refresh button";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_MouseDown_Standard(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_Standard(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            SetMenuStatus();
        }

        #endregion


        #region GridView1

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        Load_Data_1();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            /*DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }*/
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 1;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        #endregion


        #region GridView2

        private void gridControl2_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        Load_Data_2();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView2_DoubleClick(object sender, EventArgs e)
        {
            /*DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }*/
        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 2;
            SetMenuStatus();
        }

        private void gridView2_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 2;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void repositoryItemHyperLinkEditReportName_OpenLink(object sender, OpenLinkEventArgs e)
        {            
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "SavedReportName").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Team Work Schedule Linked - unable to proceed.", "View Sent Team Work Schedule", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                if (!BaseObjects.Keyboard.IsKeyDown(Keys.A))  // Only open in DevExpress PDF viewer if user is not holding down A key (CTRL Key doesn't work when fired from a grid as it's used for row selection) //
                {
                    frm_Core_PDF_Viewer fChildForm = new frm_Core_PDF_Viewer();
                    fChildForm.strPDFFile = Path.Combine(i_str_SavedSchedulePDFs + strFile);
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.Show();
                }
                else
                {
                    System.Diagnostics.Process.Start(Path.Combine(i_str_SavedSchedulePDFs + strFile));
                }
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view the Team Work Schedule: " + strFile + ".\n\nThe File may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Sent Team Work Schedule", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion


        #region GridView8

        private void gridControl8_EmbeddedNavigator_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("reload".Equals(e.Button.Tag))
                    {
                        Load_Data_8();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView8_DoubleClick(object sender, EventArgs e)
        {
            /*DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }*/
        }

        private void gridView8_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 8;
            SetMenuStatus();
        }

        private void gridView8_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 8;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            switch (i_int_FocusedGrid)
            {
                case 1:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Teams to Clear the Schedule Change Log by clicking on them then try again.", "Clear Team Schedule Change Log", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Team" : Convert.ToString(intRowHandles.Length) + " Teams") + " selected for Clearing Team Schedule Change Log!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Schedule Change Log" : "these Schedule Change Logs") + " will no longer be available for selection!</color>";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Clear Team Schedule Change Log", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Clearing...");

                            int intRecordID = 0;
                            int intRecordTypeID = 0;
                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                foreach (int intRowHandle in intRowHandles)
                                {
                                    intRecordID = Convert.ToInt32(view.GetRowCellDisplayText(intRowHandle, "ContractorID"));
                                    intRecordTypeID = Convert.ToInt32(view.GetRowCellDisplayText(intRowHandle, "LinkedToPersonTypeID"));
                                    try
                                    {
                                        RemoveRecords.sp06316_OM_Delete_Job_Schedule_Change_Log(intRecordID, intRecordTypeID);  // Remove the records from the DB in one go //
                                    }
                                    catch (Exception) { }
                                }
                            }

                            Load_Data_1();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " Team Schedule Change Log(s) cleared.", "Clear Team Schedule Change Log", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 2:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl2.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Sent Schedules to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Sent Schedule" : Convert.ToString(intRowHandles.Length) + " Sent Schedules") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Sent Schedule" : "these Sent Schedules") + " will no longer be available for selection!</color>";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "SentScheduleID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("job_schedule_sent", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            Load_Data_2();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
            }
        }



        #region Callout Date Range Filter Panel

        private void btnDateRangeOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void repositoryItemPopupContainerEditDateRange_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditDateRange_Get_Selected();
        }

        private string PopupContainerEditDateRange_Get_Selected()
        {
            string strValue = "";
            if (dateEditFromDate.DateTime == DateTime.MinValue)
            {
                i_dtStart = new DateTime(1900, 1, 1);
            }
            else
            {
                i_dtStart = dateEditFromDate.DateTime;
            }
            if (dateEditToDate.DateTime == DateTime.MinValue)
            {
                i_dtEnd = new DateTime(1900, 1, 1);
            }
            else
            {
                i_dtEnd = dateEditToDate.DateTime;
            }
            if (dateEditFromDate.DateTime == DateTime.MinValue && dateEditToDate.DateTime == DateTime.MinValue)
            {
                strValue = "No Date Range Filter";
            }
            else
            {
                strValue = (i_dtStart == new DateTime(1900, 1, 1) ? "No Start" : i_dtStart.ToString("dd/MM/yyyy HH:mm")) + " - " + (i_dtEnd == new DateTime(1900, 1, 1) ? "No End" : i_dtEnd.ToString("dd/MM/yyyy HH:mm"));
            }
            return strValue;
        }

        #endregion


        #region Printing 

        private void bbiPreviewReport_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = null;
            int[] intRowHandles;

            if (i_int_FocusedGrid == 1 || i_int_FocusedGrid == 8)
            {
                if (i_int_FocusedGrid == 1)
                {
                    view = (GridView)gridControl1.MainView;
                    intRowHandles = view.GetSelectedRows();
                }
                else
                {
                    view = (GridView)gridControl8.MainView;
                    intRowHandles = view.GetSelectedRows();
                }

                if (intRowHandles.Length != 1)
                {
                    XtraMessageBox.Show("Select just one Team to Preview the Schedule for.", "Preview Schedule", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                //string strTeamIDs = "";
                int intTeamID = 0;
                int intLabourType = 1;
                if (intRowHandles.Length < 0)
                {
                    //strTeamIDs = "-999";
                    intTeamID = 0;
                }
                else
                {
                    //strTeamIDs += Convert.ToString(view.GetRowCellValue(intRowHandles[0], "ContractorID")) + ",";
                    intTeamID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "ContractorID"));
                    intLabourType = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "LinkedToPersonTypeID"));
                }

                string strReportFileName = "TeamJobSchedule.repx";
                //rpt_OM_Team_Schedule rptReport = new rpt_OM_Team_Schedule(this.GlobalSettings, strTeamIDs, intLabourType, i_dtStart, i_dtEnd);
                rpt_OM_Team_Schedule rptReport = new rpt_OM_Team_Schedule(this.GlobalSettings, intTeamID, intLabourType, i_dtStart.Year);

                loadingForm = new WaitDialogForm("Loading Report...", "Reporting");
                loadingForm.Show();
                try
                {
                    rptReport.LoadLayout(i_str_SavedDirectoryName + strReportFileName);
                }
                catch (Exception Ex)
                {
                    loadingForm.Close();
                    Console.WriteLine(Ex.Message);
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to view report, the layout may no longer exist or you may not have the necessary permissions to access it - contact Technical Support.", "View Report", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }

                documentViewer1.DocumentSource = rptReport;
                rptReport.CreateDocument();
                loadingForm.Close();
                xtraTabControl1.SelectedTabPage = xtraTabPage3;
            }
        }

        private void bbiEditReportLayout_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string strReportFileName = "TeamJobSchedule.repx";

            if (DevExpress.XtraEditors.XtraMessageBox.Show("Are you sure you wish to edit the selected layout?", "Edit Report Layout", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                //string strTeamIDs = "";
                int intTeamID = 0;
                int intLabourType = 1;
                GridView view = null;
                int[] intRowHandles;
                if (i_int_FocusedGrid == 1)
                {
                    view = (GridView)gridControl1.MainView;
                    intRowHandles = view.GetSelectedRows();
                }
                else
                {
                    view = (GridView)gridControl2.MainView;
                    intRowHandles = view.GetSelectedRows();
                }
                if (intRowHandles.Length <= 0)
                {
                    //strTeamIDs = "-999";
                    intTeamID = 0;
                }
                else
                {
                    //strTeamIDs += Convert.ToString(view.GetRowCellValue(intRowHandles[0], "ContractorID")) + ",";
                    intTeamID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "ContractorID"));
                    intLabourType = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "LinkedToPersonTypeID"));
                }

                //rpt_OM_Team_Schedule rptReport = new rpt_OM_Team_Schedule(this.GlobalSettings, strTeamIDs, intLabourType, i_dtStart, i_dtEnd);
                rpt_OM_Team_Schedule rptReport = new rpt_OM_Team_Schedule(this.GlobalSettings, intTeamID, intLabourType, i_dtStart.Year);
                try
                {
                    rptReport.LoadLayout(i_str_SavedDirectoryName + strReportFileName);
                }
                catch (Exception Ex)
                {
                    Console.WriteLine(Ex.Message);
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to load layout, it may no longer exist or you may not have the necessary permissions to access it - contact Technical Support.", "Load Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                loadingForm = new WaitDialogForm("Loading Report Builder...", "Reporting");
                loadingForm.Show();

                // Open the report in the Report Builder - Create a design form and get its panel //
                XRDesignRibbonFormEx form = new XRDesignRibbonFormEx();  // Create an End-User Designer Form with a Ribbon //
                //XRDesignFormEx form = new XRDesignFormEx();
                XRDesignPanel panel = form.DesignPanel;
                panel.SetCommandVisibility(ReportCommand.NewReport, CommandVisibility.None);
                panel.SetCommandVisibility(ReportCommand.OpenFile, CommandVisibility.None);
                panel.SetCommandVisibility(ReportCommand.SaveFileAs, CommandVisibility.None);

                // Add a new command handler to the Report Designer which saves the report in a custom way.
                panel.AddCommandHandler(new SaveCommandHandler(panel, i_str_SavedDirectoryName, strReportFileName));

                // Add a new command handler to the report panel to fire code on adding a new object to the report (Not currently used but could be used to preset values on new object for user) //
                panel.ComponentAdded += new ComponentEventHandler(panel_ComponentAdded);

                panel.OpenReport(rptReport);
                form.Shown += new EventHandler(ReportBuilder_Shown);  // Fires event after report builder is shown //
                form.WindowState = FormWindowState.Maximized;
                loadingForm.Close();
                form.ShowDialog();
                panel.CloseReport();
            }
        }



        void ReportBuilder_Shown(object sender, EventArgs e)
        {
            if (loadingForm != null) loadingForm.Close();
        }

        void panel_ComponentAdded(object sender, ComponentEventArgs e)
        {
            if (e.Component is XRTableCell)// && FormLoaded)
            {
                //((XRTableCell)e.Component).Font = new Font("Arial", 12, FontStyle.Bold);
            }
            if (e.Component is XRLabel)
            {
                //((XRLabel)e.Component).PreviewClick += new PreviewMouseEventHandler(My_PreviewClick);
            }
        }

        private void AdjustEventHandlers(XtraReport ActiveReport)
        {
            foreach (Band b in ActiveReport.Bands)
            {
                foreach (XRControl c in b.Controls)
                {
                    if (c is XRTable)
                    {
                        XRTable t = (XRTable)c;
                        foreach (XRControl row in t.Controls)
                        {
                            foreach (XRControl cell in row.Controls)
                            {
                                if (cell.Tag.ToString() != "")
                                {
                                    cell.PreviewClick += new PreviewMouseEventHandler(My_PreviewClick);
                                }
                            }
                        }
                    }
                }
            }
        }

        private XRTableCell currentSortCell;  // Used for sorting on-the-fly //
        private Boolean SortAscending = false;
        private void My_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            // ***** Used for on-the-fly sorting ***** //
            // Turn off sorting //
            DetailBand db = (DetailBand)rptReport.Bands.GetBandByType(typeof(DetailBand));
            if (db != null)
            {
                if (((XRControl)sender).Tag.ToString() == null) return;

                //printingSystem1.Begin();  // Switch redraw off //
                loadingForm = new WaitDialogForm("Sorting Report...", "Reporting");
                loadingForm.Show();

                db.SortFields.Clear();
                if (currentSortCell != null)
                {
                    currentSortCell.Text = currentSortCell.Text.Remove(currentSortCell.Text.Length - 1, 1);
                }
                // Create a new field to sort //
                GroupField grField = new GroupField();
                grField.FieldName = ((XRControl)sender).Tag.ToString();
                if (currentSortCell != null)
                {
                    if (currentSortCell.Text == ((XRLabel)sender).Text)
                    {
                        if (SortAscending)
                        {
                            grField.SortOrder = XRColumnSortOrder.Descending;
                            SortAscending = !SortAscending;
                        }
                        else
                        {
                            grField.SortOrder = XRColumnSortOrder.Ascending;
                            SortAscending = !SortAscending;
                        }
                    }
                    else
                    {
                        grField.SortOrder = XRColumnSortOrder.Ascending;
                        SortAscending = true;
                    }
                }
                else
                {
                    grField.SortOrder = XRColumnSortOrder.Ascending;
                    SortAscending = true;
                }
                // Add sorting //
                db.SortFields.Add(grField);
                ((XRLabel)sender).Text = ((XRLabel)sender).Text + "*";
                currentSortCell = (XRTableCell)sender;
                // Recreate the report document.
                rptReport.CreateDocument();
                loadingForm.Close();
                //printingSystem1.End();  // Switch redraw back on //
            }
        }

        public class SaveCommandHandler : DevExpress.XtraReports.UserDesigner.ICommandHandler
        {
            XRDesignPanel panel;
            public string strFullPath = "";
            public string strFileName = "";

            public SaveCommandHandler(XRDesignPanel panel, string strFullPath, string strFileName)
            {
                this.panel = panel;
                this.strFullPath = strFullPath;
                this.strFileName = strFileName;
            }

            public void HandleCommand(DevExpress.XtraReports.UserDesigner.ReportCommand command, object[] args)
            {
                //if (!CanHandleCommand(command)) return;
                Save();  // Save report //
                //handled = true;  // Set handled to true to avoid the standard saving procedure to be called.
            }

            public bool CanHandleCommand(DevExpress.XtraReports.UserDesigner.ReportCommand command, ref bool useNextHandler)
            {
                useNextHandler = !(command == ReportCommand.SaveFile ||
                    command == ReportCommand.SaveFileAs ||
                    command == ReportCommand.Closing);
                return !useNextHandler;
            }

            void Save()
            {
                Boolean blSaved = false;
                panel.ReportState = ReportState.Saved;  // Prevent the "Report has been changed" dialog from being shown //

                // Update existing file layout //
                panel.Report.DataSource = null;
                panel.Report.DataMember = null;
                panel.Report.DataAdapter = null;
                try
                {
                    panel.Report.SaveLayout(strFullPath + strFileName);
                    blSaved = true;
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save Report Layout... there is a problem with the default path!\n\nContact Technical Support.", "Save Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    Console.WriteLine(ex.Message);
                    blSaved = false;
                }
                if (blSaved)
                {
                    panel.ReportState = ReportState.Saved;   // Prevent the "Report has been changed" dialog from being shown //
                }
            }
        }



        #endregion


        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Load_Data_1();
            Load_Data_2();
            Load_Data_8();
        }

        private void bbiBack_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            xtraTabControl1.SelectedTabPage = xtraTabPage1;
        }

        private void xtraTabControl1_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            switch (xtraTabControl1.SelectedTabPage.Name)
            {
                case "xtraTabPage1":
                    //xtraTabControl1.ShowTabHeader = DefaultBoolean.True;
                    break;
                case "xtraTabPage2":
                    //xtraTabControl1.ShowTabHeader = DefaultBoolean.True;
                    break;
                case "xtraTabPage3":
                    //xtraTabControl1.ShowTabHeader = DefaultBoolean.False;
                    break;
            }
        }

        private void bbiSend_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = null;
            int[] intRowHandles;
            int intSentSuccess = 0;
            int intSentFail = 0;

            if (i_int_FocusedGrid == 1 || i_int_FocusedGrid == 8)
            {
                if (i_int_FocusedGrid == 1)
                {
                    view = (GridView)gridControl1.MainView;
                    intRowHandles = view.GetSelectedRows();
                }
                else
                {
                    view = (GridView)gridControl8.MainView;
                    intRowHandles = view.GetSelectedRows();
                }

                if (intRowHandles.Length <= 0)
                {
                    XtraMessageBox.Show("Select at least one Team to Send the Schedule for.", "Send Schedule", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                #region Get Email Settings

                // Get DB Settings for sending emails //
                string strConfirmationBodyFile = "";
                string strEmailFrom = "";
                string strConfirmationEmailSubjectLine = "";
                string strCCToEmailAddress = "";

                string strSMTPMailServerAddress = "";
                string strSMTPMailServerUsername = "";
                string strSMTPMailServerPassword = "";
                string strSMTPMailServerPort = "";
                SqlConnection SQlConn = new SqlConnection(strConnectionString);
                SqlCommand cmd = null;
                cmd = new SqlCommand("sp06261_OM_Team_Schedule_Email_Settings", SQlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter sdaSettings = new SqlDataAdapter(cmd);
                DataSet dsSettings = new DataSet("NewDataSet");
                sdaSettings.Fill(dsSettings, "Table");

                if (dsSettings.Tables[0].Rows.Count != 1)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the 'Team Email Settings' (from the System Configuration Screen) - number of rows returned not equal to 1.\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                DataRow dr1 = dsSettings.Tables[0].Rows[0];
                strConfirmationBodyFile = dr1["BodyFileName"].ToString();
                strEmailFrom = dr1["EmailFrom"].ToString();
                strConfirmationEmailSubjectLine = dr1["SubjectLine"].ToString();
                strCCToEmailAddress = dr1["CCToName"].ToString();

                strSMTPMailServerAddress = dr1["SMTPMailServerAddress"].ToString();
                strSMTPMailServerUsername = dr1["SMTPMailServerUsername"].ToString();
                strSMTPMailServerPassword = dr1["SMTPMailServerPassword"].ToString();
                strSMTPMailServerPort = dr1["SMTPMailServerPort"].ToString();
                if (string.IsNullOrEmpty(strSMTPMailServerPort) || !CheckingFunctions.IsNumeric(strSMTPMailServerPort)) strSMTPMailServerPort = "0";
                int intSMTPMailServerPort = Convert.ToInt32(strSMTPMailServerPort);

                if (string.IsNullOrEmpty(strConfirmationBodyFile) || string.IsNullOrEmpty(strEmailFrom) || string.IsNullOrEmpty(strSMTPMailServerAddress))
                {
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress = null;
                    }
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more of the email settings (Email Layout File, From Email Address and SMTP Mail Server Name) are missing from the System Configuration Screen.\n\nPlease update the System Settings then try again. If the problem persists, contact Technical Support.", "Get Email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                #endregion

                bool boolNoInternet = BaseObjects.PingTest.Ping("www.google.com");
                if (!boolNoInternet) boolNoInternet = BaseObjects.PingTest.Ping("www.microsoft.com");  // try another site just in case that one is down //
                if (!boolNoInternet) boolNoInternet = BaseObjects.PingTest.Ping("www.yahoo.com");  // try another site just in case that one is down //
                if (!boolNoInternet) // alert user and halt process //
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to connect to the internet - unable to send schedule(s).\n\nPlease check your internet connection then try again.\n\nIf the problem persists, contact Technical Support.", "Check Internet Connection", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                // If at this point, we are good to send schedule so get user confirmation //
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + (intRowHandles.Length == 1 ? "1 Team" : intRowHandles.Length.ToString() + " Teams") + " selected for sending work schedules to.\n\nProceed?", "Send Work Schedule(s)", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;
                splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Sending Schedule(s)...");

                var GetEmailAddress = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter();
                GetEmailAddress.ChangeConnectionString(strConnectionString);

                var AddScheduleSentRecord = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter();
                AddScheduleSentRecord.ChangeConnectionString(strConnectionString);

                char[] delimiters = new char[] { ',' };
                string strEmailAddresses = "";
                string strEmailPassword = "";
                int intTeamID = 0;
                int intLabourTypeID = 1;

                try
                {
                    string strBody = System.IO.File.ReadAllText(strConfirmationBodyFile);
                    string strPDFName = "";
                    string strJustFileName = "";
                    i_str_AddedRecordIDs2 = "";
                    DateTime dtTodayWithTime = DateTime.Now;
                    for (int i = 0; i < intRowHandles.Length; i++)
                    {
                        intTeamID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "ContractorID"));
                        intLabourTypeID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "LinkedToPersonTypeID"));
                        strEmailPassword = view.GetRowCellValue(intRowHandles[i], "EmailPassword").ToString();

                        // Load Report into memory //
                        strPDFName = "Team_Schedule_" + intTeamID.ToString().PadLeft(8, '0') + "_" + dtTodayWithTime.ToString("yyyy-MM-dd_HH_mm_ss") + ".PDF";
                        strJustFileName = strPDFName;

                        // Create report //
                        string strReportFileName = "TeamJobSchedule.repx";
                        //rpt_OM_Team_Schedule rptReport = new rpt_OM_Team_Schedule(this.GlobalSettings, intTeamID.ToString() + ",", intLabourTypeID, i_dtStart, i_dtEnd);
                        rpt_OM_Team_Schedule rptReport = new rpt_OM_Team_Schedule(this.GlobalSettings, intTeamID, intLabourTypeID, i_dtStart.Year);
                        rptReport.LoadLayout(Path.Combine(i_str_SavedDirectoryName + strReportFileName));

                        // Set security options of report so when it is exported, it can't be edited and is password protected //
                        rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsOptions.EnableCopying = false;
                        rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsOptions.ChangingPermissions = DevExpress.XtraPrinting.ChangingPermissions.None;
                        rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsOptions.PrintingPermissions = DevExpress.XtraPrinting.PrintingPermissions.HighResolution;
                        rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsPassword = "GroundControlXX";
                        if (!string.IsNullOrEmpty(strEmailPassword)) rptReport.ExportOptions.Pdf.PasswordSecurityOptions.OpenPassword = strEmailPassword;

                        // Save report to PDF //
                        strPDFName = Path.Combine(i_str_SavedSchedulePDFs + strPDFName);  // Put path onto start of filename //
                        rptReport.ExportToPdf(strPDFName);

                        // Now Send Email //
                        if (intLabourTypeID == 1)
                        {
                            strEmailAddresses = GetEmailAddress.sp06262_OM_Team_Schedule_Get_Team_Email_Addresses(intTeamID).ToString();
                            if (string.IsNullOrWhiteSpace(strEmailAddresses))
                            {
                                intSentFail++;
                                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while sending the Work Schedule(s) - Team: " + view.GetRowCellValue(intRowHandles[i], "ContractorName").ToString() + " has no email address.\n\nAny remaining selected Schedules will still be sent.", "Send Work Schedule(s)", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                continue;  // By-pass rest of loop (this team will be left in the list at the end when the data is re-loaded //
                            }
                        }
                        else  // We don't hold an email address in tblStaff table so we can't email the schedule repot to them //
                        {
                            intSentFail++;
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while sending the Work Schedule(s) - Staff: " + view.GetRowCellValue(intRowHandles[i], "ContractorName").ToString() + " has no email address.\n\nAny remaining selected Schedules will still be sent.", "Send Work Schedule(s)", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            continue;  // By-pass rest of loop (this team will be left in the list at the end when the data is re-loaded //
                        }
                        System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                        msg.From = new System.Net.Mail.MailAddress(strEmailFrom);
                        string[] strEmailTo = strEmailAddresses.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                        if (strEmailTo.Length > 0)
                        {
                            foreach (string strEmailAddress in strEmailTo)
                            {
                                msg.To.Add(new System.Net.Mail.MailAddress(strEmailAddress));
                            }
                        }
                        else
                        {
                            msg.To.Add(new System.Net.Mail.MailAddress(strEmailAddresses));  // Original value wouldn't split as no commas so it's just one email address so use it //
                        }
                        msg.Subject = strConfirmationEmailSubjectLine;
                        if (!string.IsNullOrEmpty(strCCToEmailAddress)) msg.CC.Add(strCCToEmailAddress);
                        msg.Priority = System.Net.Mail.MailPriority.High;
                        msg.IsBodyHtml = true;

                        System.Net.Mail.AlternateView plainView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(System.Text.RegularExpressions.Regex.Replace(strBody, @"<(.|\n)*?>", string.Empty), null, "text/plain");
                        System.Net.Mail.AlternateView htmlView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(strBody, null, "text/html");

                        // Create a new attachment //
                        System.Net.Mail.Attachment mailAttachment = new System.Net.Mail.Attachment(strPDFName);  // Attach PDF report //
                        msg.Attachments.Add(mailAttachment);

                        /*        // Create the LinkedResource (embedded image) //
                                System.Net.Mail.LinkedResource logo = new System.Net.Mail.LinkedResource(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\" + "Company_Logo.jpg");
                                logo.ContentId = "companylogo";
                                //add the LinkedResource to the appropriate view
                                htmlView.LinkedResources.Add(logo);

                                // Create the LinkedResource (embedded image) //
                                System.Net.Mail.LinkedResource logo2 = new System.Net.Mail.LinkedResource(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\" + "Company_Footer.gif");
                                logo2.ContentId = "companyfooter";
                       
                                // Add the LinkedResource to the appropriate view //
                                htmlView.LinkedResources.Add(logo2);
                        */
                        msg.AlternateViews.Add(plainView);
                        msg.AlternateViews.Add(htmlView);

                        object userState = msg;
                        System.Net.Mail.SmtpClient emailClient = null;
                        if (intSMTPMailServerPort != 0)
                        {
                            emailClient = new System.Net.Mail.SmtpClient(strSMTPMailServerAddress, intSMTPMailServerPort);
                        }
                        else
                        {
                            emailClient = new System.Net.Mail.SmtpClient(strSMTPMailServerAddress);
                        }
                        if (!string.IsNullOrEmpty(strSMTPMailServerUsername) && !string.IsNullOrEmpty(strSMTPMailServerPassword))
                        {
                            System.Net.NetworkCredential basicCredential = new System.Net.NetworkCredential(strSMTPMailServerUsername, strSMTPMailServerPassword);
                            emailClient.UseDefaultCredentials = false;
                            emailClient.Credentials = basicCredential;
                        }
                        emailClient.SendAsync(msg, userState);
                        //Application.DoEvents();  // Give Application time to catch up in case we run into a timing issue - MIGHT NEED TO SWITCH THIS ON //

                        // Write Record into Sent Schedule Table (this will also clear the changes out of the OM_Job_Schedule_Change_Log Table) //
                        i_str_AddedRecordIDs2 += AddScheduleSentRecord.sp06263_OM_Job_Schedule_Sent_Add(intTeamID, dtTodayWithTime, GlobalSettings.UserID, strJustFileName, null, intLabourTypeID).ToString() + ";";
                        intSentSuccess++;
                    }
                }
                catch (Exception ex)
                {
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress = null;
                    }
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while sending the Work Schedule(s).\n\nMessage = [" + ex.Message + "].\n\nPlease try again. If the problem persists, contact Technical Support.", "Send Work Schedule(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }

                // Reload Data //
                Load_Data_1();
                Load_Data_2();

                if (splashScreenManager.IsSplashFormVisible)
                {
                    splashScreenManager.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager.CloseWaitForm();
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("Work Schedule(s) Sending Complete.\n\n" + intSentSuccess.ToString() + " sent successfully.\n" + intSentFail.ToString() + " failed.", "Send Work Schedule(s)", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else  // Existing Sent Document being re-sent //
            {
                view = (GridView)gridControl2.MainView;
                intRowHandles = view.GetSelectedRows();

                if (intRowHandles.Length <= 0)
                {
                    XtraMessageBox.Show("Select at least one Team to Re-Send the Schedule for.", "Re-Send Schedule", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                #region Get Email Settings

                // Get DB Settings for sending emails //
                string strConfirmationBodyFile = "";
                string strEmailFrom = "";
                string strConfirmationEmailSubjectLine = "";
                string strCCToEmailAddress = "";

                string strSMTPMailServerAddress = "";
                string strSMTPMailServerUsername = "";
                string strSMTPMailServerPassword = "";
                string strSMTPMailServerPort = "";
                SqlConnection SQlConn = new SqlConnection(strConnectionString);
                SqlCommand cmd = null;
                cmd = new SqlCommand("sp06261_OM_Team_Schedule_Email_Settings", SQlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter sdaSettings = new SqlDataAdapter(cmd);
                DataSet dsSettings = new DataSet("NewDataSet");
                sdaSettings.Fill(dsSettings, "Table");

                if (dsSettings.Tables[0].Rows.Count != 1)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the 'Team Email Settings' (from the System Configuration Screen) - number of rows returned not equal to 1.\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                DataRow dr1 = dsSettings.Tables[0].Rows[0];
                strConfirmationBodyFile = dr1["BodyFileName"].ToString();
                strEmailFrom = dr1["EmailFrom"].ToString();
                strConfirmationEmailSubjectLine = dr1["SubjectLine"].ToString();
                strCCToEmailAddress = dr1["CCToName"].ToString();

                strSMTPMailServerAddress = dr1["SMTPMailServerAddress"].ToString();
                strSMTPMailServerUsername = dr1["SMTPMailServerUsername"].ToString();
                strSMTPMailServerPassword = dr1["SMTPMailServerPassword"].ToString();
                strSMTPMailServerPort = dr1["SMTPMailServerPort"].ToString();
                if (string.IsNullOrEmpty(strSMTPMailServerPort) || !CheckingFunctions.IsNumeric(strSMTPMailServerPort)) strSMTPMailServerPort = "0";
                int intSMTPMailServerPort = Convert.ToInt32(strSMTPMailServerPort);

                if (string.IsNullOrEmpty(strConfirmationBodyFile) || string.IsNullOrEmpty(strEmailFrom) || string.IsNullOrEmpty(strSMTPMailServerAddress))
                {
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress = null;
                    }
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more of the email settings (Email Layout File, From Email Address and SMTP Mail Server Name) are missing from the System Configuration Screen.\n\nPlease update the System Settings then try again. If the problem persists, contact Technical Support.", "Get Email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                #endregion

                bool boolNoInternet = BaseObjects.PingTest.Ping("www.google.com");
                if (!boolNoInternet) boolNoInternet = BaseObjects.PingTest.Ping("www.microsoft.com");  // try another site just in case that one is down //
                if (!boolNoInternet) boolNoInternet = BaseObjects.PingTest.Ping("www.yahoo.com");  // try another site just in case that one is down //
                if (!boolNoInternet) // alert user and halt process //
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to connect to the internet - unable to send schedule(s).\n\nPlease check your internet connection then try again.\n\nIf the problem persists, contact Technical Support.", "Check Internet Connection", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                // If at this point, we are good to send schedule so get user confirmation //
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + (intRowHandles.Length == 1 ? "1 Team" : intRowHandles.Length.ToString() + " Teams") + " selected for re-sending work schedules to.\n\nProceed?", "Re-Send Work Schedule(s)", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;
                splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Re-Sending Schedule(s)...");

                var GetEmailAddress = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter();
                GetEmailAddress.ChangeConnectionString(strConnectionString);

                var UpdateScheduleSentRecord = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter();
                UpdateScheduleSentRecord.ChangeConnectionString(strConnectionString);

                char[] delimiters = new char[] { ',' };
                string strEmailAddresses = "";
                string strEmailPassword = "";
                int intTeamID = 0;
                int intSentScheduleID = 0;
                try
                {
                    string strBody = System.IO.File.ReadAllText(strConfirmationBodyFile);
                    string strPDFName = "";
                    i_str_AddedRecordIDs2 = "";
                    DateTime dtTodayWithTime = DateTime.Now;
                    for (int i = 0; i < intRowHandles.Length; i++)
                    {
                        intTeamID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "ContractorID"));
                        //strEmailPassword = view.GetRowCellValue(intRowHandles[i], "EmailPassword").ToString();

                        // Get SavedReport //
                        strPDFName = Path.Combine(i_str_SavedSchedulePDFs + view.GetRowCellValue(intRowHandles[i], "SavedReportName").ToString());

                        // Now Send Email //
                        strEmailAddresses = GetEmailAddress.sp06262_OM_Team_Schedule_Get_Team_Email_Addresses(intTeamID).ToString();
                        if (string.IsNullOrWhiteSpace(strEmailAddresses))
                        {
                            intSentFail++;
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while sending the Work Schedule(s) - Team: " + view.GetRowCellValue(intRowHandles[i], "ContractorName").ToString() + " has no email address.\n\nAny remaining selected Schedules will still be sent.", "Send Work Schedule(s)", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            continue;  // By-pass rest of loop (this team will be left in the list at the end when the data is re-loaded //
                        }
                        System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                        msg.From = new System.Net.Mail.MailAddress(strEmailFrom);
                        string[] strEmailTo = strEmailAddresses.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                        if (strEmailTo.Length > 0)
                        {
                            foreach (string strEmailAddress in strEmailTo)
                            {
                                msg.To.Add(new System.Net.Mail.MailAddress(strEmailAddress));
                            }
                        }
                        else
                        {
                            msg.To.Add(new System.Net.Mail.MailAddress(strEmailAddresses));  // Original value wouldn't split as no commas so it's just one email address so use it //
                        }
                        msg.Subject = strConfirmationEmailSubjectLine;
                        if (!string.IsNullOrEmpty(strCCToEmailAddress)) msg.CC.Add(strCCToEmailAddress);
                        msg.Priority = System.Net.Mail.MailPriority.High;
                        msg.IsBodyHtml = true;

                        System.Net.Mail.AlternateView plainView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(System.Text.RegularExpressions.Regex.Replace(strBody, @"<(.|\n)*?>", string.Empty), null, "text/plain");
                        System.Net.Mail.AlternateView htmlView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(strBody, null, "text/html");

                        // Create a new attachment //
                        System.Net.Mail.Attachment mailAttachment = new System.Net.Mail.Attachment(strPDFName);  // Attach PDF report //
                        msg.Attachments.Add(mailAttachment);

                        /*        // Create the LinkedResource (embedded image) //
                                System.Net.Mail.LinkedResource logo = new System.Net.Mail.LinkedResource(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\" + "Company_Logo.jpg");
                                logo.ContentId = "companylogo";
                                //add the LinkedResource to the appropriate view
                                htmlView.LinkedResources.Add(logo);

                                // Create the LinkedResource (embedded image) //
                                System.Net.Mail.LinkedResource logo2 = new System.Net.Mail.LinkedResource(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\" + "Company_Footer.gif");
                                logo2.ContentId = "companyfooter";
                       
                                // Add the LinkedResource to the appropriate view //
                                htmlView.LinkedResources.Add(logo2);
                        */
                        msg.AlternateViews.Add(plainView);
                        msg.AlternateViews.Add(htmlView);

                        object userState = msg;
                        System.Net.Mail.SmtpClient emailClient = null;
                        if (intSMTPMailServerPort != 0)
                        {
                            emailClient = new System.Net.Mail.SmtpClient(strSMTPMailServerAddress, intSMTPMailServerPort);
                        }
                        else
                        {
                            emailClient = new System.Net.Mail.SmtpClient(strSMTPMailServerAddress);
                        }
                        if (!string.IsNullOrEmpty(strSMTPMailServerUsername) && !string.IsNullOrEmpty(strSMTPMailServerPassword))
                        {
                            System.Net.NetworkCredential basicCredential = new System.Net.NetworkCredential(strSMTPMailServerUsername, strSMTPMailServerPassword);
                            emailClient.UseDefaultCredentials = false;
                            emailClient.Credentials = basicCredential;
                        }
                        emailClient.SendAsync(msg, userState);
                        //Application.DoEvents();  // Give Application time to catch up in case we run into a timing issue - MIGHT NEED TO SWITCH THIS ON //

                        // Update Record in Sent Schedule Table //
                        intSentScheduleID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "SentScheduleID"));
                        UpdateScheduleSentRecord.sp06264_OM_Job_Schedule_Sent_Update(intSentScheduleID, dtTodayWithTime, GlobalSettings.UserID, null);
                        intSentSuccess++;
                    }
                }
                catch (Exception ex)
                {
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress = null;
                    }
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while re-sending the Work Schedule(s).\n\nMessage = [" + ex.Message + "].\n\nPlease try again. If the problem persists, contact Technical Support.", "Re-Send Work Schedule(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }

                Load_Data_2();

                if (splashScreenManager.IsSplashFormVisible)
                {
                    splashScreenManager.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager.CloseWaitForm();
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("Work Schedule(s) Sending Complete.\n\n" + intSentSuccess.ToString() + " sent successfully.\n" + intSentFail.ToString() + " failed." , "Re-Send Work Schedule(s)", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }






    }
}


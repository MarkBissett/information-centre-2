namespace WoodPlan5
{
    partial class frm_OM_Select_Tender_Group
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Select_Tender_Group));
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            this.colActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp06510OMTenderGroupSelectBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Tender = new WoodPlan5.DataSet_OM_Tender();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTenderGroupID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGroupDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateCreated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDatetime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colCreatedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sp07194UTPermissionDocumentSelectBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_WorkOrder = new WoodPlan5.DataSet_UT_WorkOrder();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerControlDateRange = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnDateRangeFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.beiDateFilter = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.beiActive = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemCheckEditActive = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.sp07194_UT_Permission_Document_SelectTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07194_UT_Permission_Document_SelectTableAdapter();
            this.sp06510_OM_Tender_Group_SelectTableAdapter = new WoodPlan5.DataSet_OM_TenderTableAdapters.sp06510_OM_Tender_Group_SelectTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06510OMTenderGroupSelectBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Tender)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDatetime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07194UTPermissionDocumentSelectBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_WorkOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).BeginInit();
            this.popupContainerControlDateRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditActive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(723, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Size = new System.Drawing.Size(723, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 511);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(723, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 511);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.beiDateFilter,
            this.bbiRefresh,
            this.beiActive});
            this.barManager1.MaxItemId = 33;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEdit1,
            this.repositoryItemCheckEditActive});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colActive
            // 
            this.colActive.Caption = "Active";
            this.colActive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colActive.FieldName = "Active";
            this.colActive.Name = "colActive";
            this.colActive.OptionsColumn.AllowEdit = false;
            this.colActive.OptionsColumn.AllowFocus = false;
            this.colActive.OptionsColumn.ReadOnly = true;
            this.colActive.Visible = true;
            this.colActive.VisibleIndex = 1;
            this.colActive.Width = 50;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp06510OMTenderGroupSelectBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemTextEditDatetime,
            this.repositoryItemCheckEdit1});
            this.gridControl1.Size = new System.Drawing.Size(722, 476);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp06510OMTenderGroupSelectBindingSource
            // 
            this.sp06510OMTenderGroupSelectBindingSource.DataMember = "sp06510_OM_Tender_Group_Select";
            this.sp06510OMTenderGroupSelectBindingSource.DataSource = this.dataSet_OM_Tender;
            // 
            // dataSet_OM_Tender
            // 
            this.dataSet_OM_Tender.DataSetName = "DataSet_OM_Tender";
            this.dataSet_OM_Tender.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTenderGroupID,
            this.colGroupDescription,
            this.colCreatedByStaffID,
            this.colDateCreated,
            this.colActive,
            this.colRemarks1,
            this.colCreatedByStaffName});
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.colActive;
            gridFormatRule1.Name = "FormatActive";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Gray;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView1.FormatRules.Add(gridFormatRule1);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsFind.FindDelay = 2000;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateCreated, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colGroupDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            // 
            // colTenderGroupID
            // 
            this.colTenderGroupID.Caption = "Tender Group ID";
            this.colTenderGroupID.FieldName = "TenderGroupID";
            this.colTenderGroupID.Name = "colTenderGroupID";
            this.colTenderGroupID.OptionsColumn.AllowEdit = false;
            this.colTenderGroupID.OptionsColumn.AllowFocus = false;
            this.colTenderGroupID.OptionsColumn.ReadOnly = true;
            this.colTenderGroupID.Width = 99;
            // 
            // colGroupDescription
            // 
            this.colGroupDescription.Caption = "Group Description";
            this.colGroupDescription.FieldName = "GroupDescription";
            this.colGroupDescription.Name = "colGroupDescription";
            this.colGroupDescription.OptionsColumn.AllowEdit = false;
            this.colGroupDescription.OptionsColumn.AllowFocus = false;
            this.colGroupDescription.OptionsColumn.ReadOnly = true;
            this.colGroupDescription.Visible = true;
            this.colGroupDescription.VisibleIndex = 0;
            this.colGroupDescription.Width = 381;
            // 
            // colCreatedByStaffID
            // 
            this.colCreatedByStaffID.Caption = "Created By Staff ID";
            this.colCreatedByStaffID.FieldName = "CreatedByStaffID";
            this.colCreatedByStaffID.Name = "colCreatedByStaffID";
            this.colCreatedByStaffID.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffID.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffID.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffID.Width = 114;
            // 
            // colDateCreated
            // 
            this.colDateCreated.Caption = "Date Created";
            this.colDateCreated.ColumnEdit = this.repositoryItemTextEditDatetime;
            this.colDateCreated.FieldName = "DateCreated";
            this.colDateCreated.Name = "colDateCreated";
            this.colDateCreated.OptionsColumn.AllowEdit = false;
            this.colDateCreated.OptionsColumn.AllowFocus = false;
            this.colDateCreated.OptionsColumn.ReadOnly = true;
            this.colDateCreated.Visible = true;
            this.colDateCreated.VisibleIndex = 3;
            this.colDateCreated.Width = 97;
            // 
            // repositoryItemTextEditDatetime
            // 
            this.repositoryItemTextEditDatetime.AutoHeight = false;
            this.repositoryItemTextEditDatetime.Mask.EditMask = "g";
            this.repositoryItemTextEditDatetime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDatetime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDatetime.Name = "repositoryItemTextEditDatetime";
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 4;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colCreatedByStaffName
            // 
            this.colCreatedByStaffName.Caption = "Created By";
            this.colCreatedByStaffName.FieldName = "CreatedByStaffName";
            this.colCreatedByStaffName.Name = "colCreatedByStaffName";
            this.colCreatedByStaffName.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffName.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffName.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffName.Visible = true;
            this.colCreatedByStaffName.VisibleIndex = 2;
            this.colCreatedByStaffName.Width = 125;
            // 
            // sp07194UTPermissionDocumentSelectBindingSource
            // 
            this.sp07194UTPermissionDocumentSelectBindingSource.DataMember = "sp07194_UT_Permission_Document_Select";
            this.sp07194UTPermissionDocumentSelectBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // dataSet_UT_WorkOrder
            // 
            this.dataSet_UT_WorkOrder.DataSetName = "DataSet_UT_WorkOrder";
            this.dataSet_UT_WorkOrder.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(555, 509);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(636, 509);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // popupContainerControlDateRange
            // 
            this.popupContainerControlDateRange.Controls.Add(this.btnDateRangeFilterOK);
            this.popupContainerControlDateRange.Controls.Add(this.groupControl1);
            this.popupContainerControlDateRange.Location = new System.Drawing.Point(11, 145);
            this.popupContainerControlDateRange.Name = "popupContainerControlDateRange";
            this.popupContainerControlDateRange.Size = new System.Drawing.Size(200, 109);
            this.popupContainerControlDateRange.TabIndex = 5;
            // 
            // btnDateRangeFilterOK
            // 
            this.btnDateRangeFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDateRangeFilterOK.Location = new System.Drawing.Point(3, 83);
            this.btnDateRangeFilterOK.Name = "btnDateRangeFilterOK";
            this.btnDateRangeFilterOK.Size = new System.Drawing.Size(34, 23);
            this.btnDateRangeFilterOK.TabIndex = 3;
            this.btnDateRangeFilterOK.Text = "OK";
            this.btnDateRangeFilterOK.Click += new System.EventHandler(this.btnDateRangeFilterOK_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.dateEditToDate);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.dateEditFromDate);
            this.groupControl1.Location = new System.Drawing.Point(3, 3);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(194, 76);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "Date Range";
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(40, 50);
            this.dateEditToDate.MenuManager = this.barManager1;
            this.dateEditToDate.Name = "dateEditToDate";
            this.dateEditToDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate.Properties.Mask.EditMask = "g";
            this.dateEditToDate.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Size = new System.Drawing.Size(150, 20);
            this.dateEditToDate.TabIndex = 3;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(6, 53);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(16, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "To:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(6, 27);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(28, 13);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "From:";
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(40, 24);
            this.dateEditFromDate.MenuManager = this.barManager1;
            this.dateEditFromDate.Name = "dateEditFromDate";
            this.dateEditFromDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate.Properties.Mask.EditMask = "g";
            this.dateEditFromDate.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Size = new System.Drawing.Size(150, 20);
            this.dateEditFromDate.TabIndex = 0;
            // 
            // beiDateFilter
            // 
            this.beiDateFilter.Caption = "Date Created:";
            this.beiDateFilter.Description = "Date Filter:";
            this.beiDateFilter.Edit = this.repositoryItemPopupContainerEdit1;
            this.beiDateFilter.EditValue = "No Date Filter";
            this.beiDateFilter.EditWidth = 168;
            this.beiDateFilter.Id = 30;
            this.beiDateFilter.Name = "beiDateFilter";
            this.beiDateFilter.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // repositoryItemPopupContainerEdit1
            // 
            this.repositoryItemPopupContainerEdit1.AutoHeight = false;
            this.repositoryItemPopupContainerEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit1.Name = "repositoryItemPopupContainerEdit1";
            this.repositoryItemPopupContainerEdit1.PopupControl = this.popupContainerControlDateRange;
            this.repositoryItemPopupContainerEdit1.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditDateRange_QueryResultValue);
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Caption = "Refresh Data";
            this.bbiRefresh.Id = 31;
            this.bbiRefresh.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRefresh.ImageOptions.Image")));
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 1;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.beiDateFilter),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.beiActive, "", false, true, true, 21),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefresh, true)});
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.Text = "Filter";
            // 
            // beiActive
            // 
            this.beiActive.Caption = "Active:";
            this.beiActive.Edit = this.repositoryItemCheckEditActive;
            this.beiActive.EditValue = 1;
            this.beiActive.Id = 32;
            this.beiActive.ItemAppearance.Disabled.BackColor = System.Drawing.Color.Transparent;
            this.beiActive.ItemAppearance.Disabled.Options.UseBackColor = true;
            this.beiActive.ItemAppearance.Hovered.BackColor = System.Drawing.Color.Transparent;
            this.beiActive.ItemAppearance.Hovered.Options.UseBackColor = true;
            this.beiActive.ItemAppearance.Normal.BackColor = System.Drawing.Color.Transparent;
            this.beiActive.ItemAppearance.Normal.Options.UseBackColor = true;
            this.beiActive.ItemAppearance.Pressed.BackColor = System.Drawing.Color.Transparent;
            this.beiActive.ItemAppearance.Pressed.Options.UseBackColor = true;
            this.beiActive.Name = "beiActive";
            this.beiActive.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // repositoryItemCheckEditActive
            // 
            this.repositoryItemCheckEditActive.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditActive.Appearance.Options.UseBackColor = true;
            this.repositoryItemCheckEditActive.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditActive.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemCheckEditActive.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditActive.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemCheckEditActive.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditActive.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemCheckEditActive.AutoHeight = false;
            this.repositoryItemCheckEditActive.Caption = "";
            this.repositoryItemCheckEditActive.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.repositoryItemCheckEditActive.Name = "repositoryItemCheckEditActive";
            this.repositoryItemCheckEditActive.ValueChecked = 1;
            this.repositoryItemCheckEditActive.ValueUnchecked = 0;
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(1, 27);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.popupContainerControlDateRange);
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(722, 476);
            this.gridSplitContainer1.TabIndex = 7;
            // 
            // sp07194_UT_Permission_Document_SelectTableAdapter
            // 
            this.sp07194_UT_Permission_Document_SelectTableAdapter.ClearBeforeFill = true;
            // 
            // sp06510_OM_Tender_Group_SelectTableAdapter
            // 
            this.sp06510_OM_Tender_Group_SelectTableAdapter.ClearBeforeFill = true;
            // 
            // frm_OM_Select_Tender_Group
            // 
            this.ClientSize = new System.Drawing.Size(723, 537);
            this.Controls.Add(this.gridSplitContainer1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_OM_Select_Tender_Group";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select Tender Group";
            this.Load += new System.EventHandler(this.frm_OM_Select_Tender_Group_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.gridSplitContainer1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06510OMTenderGroupSelectBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Tender)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDatetime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07194UTPermissionDocumentSelectBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_WorkOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).EndInit();
            this.popupContainerControlDateRange.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditActive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDatetime;
        private DevExpress.XtraBars.BarEditItem beiDateFilter;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit1;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlDateRange;
        private DevExpress.XtraEditors.SimpleButton btnDateRangeFilterOK;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private System.Windows.Forms.BindingSource sp07194UTPermissionDocumentSelectBindingSource;
        private DataSet_UT_WorkOrder dataSet_UT_WorkOrder;
        private DataSet_UT_WorkOrderTableAdapters.sp07194_UT_Permission_Document_SelectTableAdapter sp07194_UT_Permission_Document_SelectTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private System.Windows.Forms.BindingSource sp06510OMTenderGroupSelectBindingSource;
        private DataSet_OM_Tender dataSet_OM_Tender;
        private DevExpress.XtraGrid.Columns.GridColumn colTenderGroupID;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateCreated;
        private DevExpress.XtraGrid.Columns.GridColumn colActive;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffName;
        private DataSet_OM_TenderTableAdapters.sp06510_OM_Tender_Group_SelectTableAdapter sp06510_OM_Tender_Group_SelectTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraBars.BarEditItem beiActive;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditActive;
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_OM_Site_Contract_Wizard_Block_Edit_Site_Info : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        public DateTime? dtStartDate = null;
        public DateTime? dtEndDate = null;
        public int? intActive = null;
        public decimal? decContractValue = null;
        public decimal? decYearlyPercentageIncrease = null;
        public string strRemarks = null;
        public string strSiteCategory = null;
        public int? intClientBillingTypeID = null;
        public int? intDefaultScheduleTemplateID = null;
        public decimal? decDaysSeparationPercent = null;
        #endregion

        public frm_OM_Site_Contract_Wizard_Block_Edit_Site_Info()
        {
            InitializeComponent();
        }

        private void frm_OM_Site_Contract_Wizard_Block_Edit_Site_Info_Load(object sender, EventArgs e)
        {
            this.FormID = 500132;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            Application.DoEvents();  // Allow Form time to repaint itself //

            try
            {
                sp06355_OM_Client_Billing_Types_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06355_OM_Client_Billing_Types_With_BlankTableAdapter.Fill(dataSet_OM_Contract.sp06355_OM_Client_Billing_Types_With_Blank, 0);

                sp06141_OM_Visit_Template_Headers_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06141_OM_Visit_Template_Headers_With_BlankTableAdapter.Fill(dataSet_OM_Visit.sp06141_OM_Visit_Template_Headers_With_Blank);

            }
            catch (Exception) { }

            StartDateDateEdit.EditValue = null;
            EndDateDateEdit.EditValue = null;
            ActiveCheckEdit.EditValue = null;
            ContractValueSpinEdit.EditValue = null;
            YearlyPercentageIncreaseSpinEdit.EditValue = null;
            RemarksMemoEdit.EditValue = null;
            ClientBillingTypeIDGridLookUpEdit.EditValue = null;
            DefaultScheduleTemplateIDGridLookUpEdit.EditValue = null;
            DaysSeparationPercentSpinEdit.EditValue = null;
            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (StartDateDateEdit.EditValue != null) dtStartDate = Convert.ToDateTime(StartDateDateEdit.EditValue);
            if (EndDateDateEdit.EditValue != null) dtEndDate = Convert.ToDateTime(EndDateDateEdit.EditValue);
            if (ActiveCheckEdit.EditValue != null) intActive = Convert.ToInt32(ActiveCheckEdit.EditValue);
            if (ContractValueSpinEdit.EditValue != null) decContractValue = Convert.ToDecimal(ContractValueSpinEdit.EditValue);
            if (YearlyPercentageIncreaseSpinEdit.EditValue != null) decYearlyPercentageIncrease = Convert.ToDecimal(YearlyPercentageIncreaseSpinEdit.EditValue);
            if (RemarksMemoEdit.EditValue != null) strRemarks = RemarksMemoEdit.EditValue.ToString();
            if (SiteCategoryMemoEdit.EditValue != null) strSiteCategory = SiteCategoryMemoEdit.EditValue.ToString();
            if (ClientBillingTypeIDGridLookUpEdit.EditValue != null) intClientBillingTypeID = Convert.ToInt32(ClientBillingTypeIDGridLookUpEdit.EditValue);
            if (DefaultScheduleTemplateIDGridLookUpEdit.EditValue != null) intDefaultScheduleTemplateID = Convert.ToInt32(DefaultScheduleTemplateIDGridLookUpEdit.EditValue);
            if (DaysSeparationPercentSpinEdit.EditValue != null) decDaysSeparationPercent = Convert.ToDecimal(DaysSeparationPercentSpinEdit.EditValue);
              
            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }






    }
}

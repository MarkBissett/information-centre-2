namespace WoodPlan5
{
    partial class frmConnectedUsers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmConnectedUsers));
            this.gcActiveConnections = new DevExpress.XtraGrid.GridControl();
            this.sp00017GetConnectedUsersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.gvActiveConnections = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colspid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colloginame = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colhostname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colblk = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcmd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coldtToday = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrCurrentUser = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.lblConnectionCount = new DevExpress.XtraEditors.LabelControl();
            this.btnRefresh = new DevExpress.XtraEditors.SimpleButton();
            this.lblDetails = new DevExpress.XtraEditors.LabelControl();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp00017GetConnectedUsersTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00017GetConnectedUsersTableAdapter();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcActiveConnections)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00017GetConnectedUsersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvActiveConnections)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(688, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 241);
            this.barDockControlBottom.Size = new System.Drawing.Size(688, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 241);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(688, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 241);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.Caption = "Kill Database Connection";
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // gcActiveConnections
            // 
            this.gcActiveConnections.DataSource = this.sp00017GetConnectedUsersBindingSource;
            this.gcActiveConnections.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcActiveConnections.Location = new System.Drawing.Point(0, 0);
            this.gcActiveConnections.MainView = this.gvActiveConnections;
            this.gcActiveConnections.MenuManager = this.barManager1;
            this.gcActiveConnections.Name = "gcActiveConnections";
            this.gcActiveConnections.Size = new System.Drawing.Size(664, 177);
            this.gcActiveConnections.TabIndex = 0;
            this.gcActiveConnections.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvActiveConnections});
            // 
            // sp00017GetConnectedUsersBindingSource
            // 
            this.sp00017GetConnectedUsersBindingSource.DataMember = "sp00017GetConnectedUsers";
            this.sp00017GetConnectedUsersBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gvActiveConnections
            // 
            this.gvActiveConnections.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colspid,
            this.colstatus,
            this.colloginame,
            this.colhostname,
            this.colblk,
            this.colcmd,
            this.coldtToday,
            this.colstrCurrentUser});
            this.gvActiveConnections.GridControl = this.gcActiveConnections;
            this.gvActiveConnections.Name = "gvActiveConnections";
            this.gvActiveConnections.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gvActiveConnections.OptionsLayout.Columns.StoreAllOptions = true;
            this.gvActiveConnections.OptionsLayout.StoreAppearance = true;
            this.gvActiveConnections.OptionsLayout.StoreFormatRules = true;
            this.gvActiveConnections.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gvActiveConnections.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gvActiveConnections.OptionsView.AllowHtmlDrawHeaders = true;
            this.gvActiveConnections.OptionsView.ColumnAutoWidth = false;
            this.gvActiveConnections.OptionsView.EnableAppearanceEvenRow = true;
            this.gvActiveConnections.OptionsView.ShowGroupPanel = false;
            this.gvActiveConnections.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gvActiveConnections_CustomDrawEmptyForeground);
            this.gvActiveConnections.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gvActiveConnections_FocusedRowChanged);
            this.gvActiveConnections.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gvActiveConnections_MouseUp);
            // 
            // colspid
            // 
            this.colspid.Caption = "SP ID";
            this.colspid.FieldName = "spid";
            this.colspid.Name = "colspid";
            this.colspid.OptionsColumn.AllowEdit = false;
            this.colspid.OptionsColumn.AllowFocus = false;
            this.colspid.OptionsColumn.ReadOnly = true;
            this.colspid.Visible = true;
            this.colspid.VisibleIndex = 0;
            // 
            // colstatus
            // 
            this.colstatus.Caption = "Status";
            this.colstatus.FieldName = "status";
            this.colstatus.Name = "colstatus";
            this.colstatus.OptionsColumn.AllowEdit = false;
            this.colstatus.OptionsColumn.AllowFocus = false;
            this.colstatus.OptionsColumn.ReadOnly = true;
            this.colstatus.Visible = true;
            this.colstatus.VisibleIndex = 1;
            this.colstatus.Width = 115;
            // 
            // colloginame
            // 
            this.colloginame.Caption = "Login Name";
            this.colloginame.FieldName = "loginame";
            this.colloginame.Name = "colloginame";
            this.colloginame.OptionsColumn.AllowEdit = false;
            this.colloginame.OptionsColumn.AllowFocus = false;
            this.colloginame.OptionsColumn.ReadOnly = true;
            this.colloginame.Visible = true;
            this.colloginame.VisibleIndex = 2;
            this.colloginame.Width = 121;
            // 
            // colhostname
            // 
            this.colhostname.Caption = "Host Name";
            this.colhostname.FieldName = "hostname";
            this.colhostname.Name = "colhostname";
            this.colhostname.OptionsColumn.AllowEdit = false;
            this.colhostname.OptionsColumn.AllowFocus = false;
            this.colhostname.OptionsColumn.ReadOnly = true;
            this.colhostname.Visible = true;
            this.colhostname.VisibleIndex = 3;
            this.colhostname.Width = 107;
            // 
            // colblk
            // 
            this.colblk.Caption = "Block";
            this.colblk.FieldName = "blk";
            this.colblk.Name = "colblk";
            this.colblk.OptionsColumn.AllowEdit = false;
            this.colblk.OptionsColumn.AllowFocus = false;
            this.colblk.OptionsColumn.ReadOnly = true;
            this.colblk.Visible = true;
            this.colblk.VisibleIndex = 4;
            // 
            // colcmd
            // 
            this.colcmd.Caption = "Last Command";
            this.colcmd.FieldName = "cmd";
            this.colcmd.Name = "colcmd";
            this.colcmd.OptionsColumn.AllowEdit = false;
            this.colcmd.OptionsColumn.AllowFocus = false;
            this.colcmd.OptionsColumn.ReadOnly = true;
            this.colcmd.Visible = true;
            this.colcmd.VisibleIndex = 5;
            this.colcmd.Width = 132;
            // 
            // coldtToday
            // 
            this.coldtToday.Caption = "dtToday";
            this.coldtToday.FieldName = "dtToday";
            this.coldtToday.Name = "coldtToday";
            this.coldtToday.OptionsColumn.AllowEdit = false;
            this.coldtToday.OptionsColumn.AllowFocus = false;
            this.coldtToday.OptionsColumn.ReadOnly = true;
            // 
            // colstrCurrentUser
            // 
            this.colstrCurrentUser.Caption = "strCurrentUser";
            this.colstrCurrentUser.FieldName = "strCurrentUser";
            this.colstrCurrentUser.Name = "colstrCurrentUser";
            this.colstrCurrentUser.OptionsColumn.AllowEdit = false;
            this.colstrCurrentUser.OptionsColumn.AllowFocus = false;
            this.colstrCurrentUser.OptionsColumn.ReadOnly = true;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.panelControl1);
            this.layoutControl1.Controls.Add(this.gridSplitContainer1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.MenuManager = this.barManager1;
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1130, 107, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(688, 241);
            this.layoutControl1.TabIndex = 1;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.lblConnectionCount);
            this.panelControl1.Controls.Add(this.btnRefresh);
            this.panelControl1.Controls.Add(this.lblDetails);
            this.panelControl1.Location = new System.Drawing.Point(12, 12);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(664, 36);
            this.panelControl1.TabIndex = 4;
            // 
            // lblConnectionCount
            // 
            this.lblConnectionCount.AllowHtmlString = true;
            this.lblConnectionCount.Location = new System.Drawing.Point(519, 5);
            this.lblConnectionCount.Name = "lblConnectionCount";
            this.lblConnectionCount.Size = new System.Drawing.Size(102, 13);
            this.lblConnectionCount.TabIndex = 2;
            this.lblConnectionCount.Text = "<Connection Count>";
            // 
            // btnRefresh
            // 
            this.btnRefresh.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnRefresh.ImageOptions.Image")));
            this.btnRefresh.Location = new System.Drawing.Point(5, 5);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 23);
            this.btnRefresh.TabIndex = 1;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // lblDetails
            // 
            this.lblDetails.AllowHtmlString = true;
            this.lblDetails.Location = new System.Drawing.Point(102, 5);
            this.lblDetails.Name = "lblDetails";
            this.lblDetails.Size = new System.Drawing.Size(48, 13);
            this.lblDetails.TabIndex = 0;
            this.lblDetails.Text = "<Details>";
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Grid = this.gcActiveConnections;
            this.gridSplitContainer1.Location = new System.Drawing.Point(12, 52);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gcActiveConnections);
            this.gridSplitContainer1.Size = new System.Drawing.Size(664, 177);
            this.gridSplitContainer1.TabIndex = 5;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(688, 241);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridSplitContainer1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 40);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(668, 181);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.panelControl1;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 40);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(5, 40);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(668, 40);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // sp00017GetConnectedUsersTableAdapter
            // 
            this.sp00017GetConnectedUsersTableAdapter.ClearBeforeFill = true;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(5, 5);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 1;
            this.simpleButton1.Text = "simpleButton1";
            // 
            // frmConnectedUsers
            // 
            this.ClientSize = new System.Drawing.Size(688, 241);
            this.Controls.Add(this.layoutControl1);
            this.LookAndFeel.SkinName = "London Liquid Sky";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmConnectedUsers";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Active Database Connections";
            this.Load += new System.EventHandler(this.frmConnectedUsers_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcActiveConnections)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00017GetConnectedUsersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvActiveConnections)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private DevExpress.XtraGrid.GridControl gcActiveConnections;
        private DevExpress.XtraGrid.Views.Grid.GridView gvActiveConnections;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private System.Windows.Forms.BindingSource sp00017GetConnectedUsersBindingSource;
        private WoodPlanDataSet woodPlanDataSet;
        private DevExpress.XtraGrid.Columns.GridColumn colspid;
        private DevExpress.XtraGrid.Columns.GridColumn colstatus;
        private DevExpress.XtraGrid.Columns.GridColumn colloginame;
        private DevExpress.XtraGrid.Columns.GridColumn colhostname;
        private DevExpress.XtraGrid.Columns.GridColumn colblk;
        private DevExpress.XtraGrid.Columns.GridColumn colcmd;
        private DevExpress.XtraGrid.Columns.GridColumn coldtToday;
        private DevExpress.XtraGrid.Columns.GridColumn colstrCurrentUser;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00017GetConnectedUsersTableAdapter sp00017GetConnectedUsersTableAdapter;
        private DevExpress.XtraEditors.SimpleButton btnRefresh;
        private DevExpress.XtraEditors.LabelControl lblDetails;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.LabelControl lblConnectionCount;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Reflection;
using System.Windows.Forms;
using WoodPlan5.Properties;
using BaseObjects;

namespace WoodPlan5
{
    public struct stcUpdatedGlobals
    {
        public Boolean blShowTipsOnStartup;
        public Boolean blShowToolTips;
        public Boolean blShowConfirmations;
        public Boolean blMinimiseToTaskBar;
        public int intShowPhotoPopup;
        public string strDefaultFontName;
        public int intDefaultFontSize;
    }

    public partial class frmUserSettings : frmBase
    {
        Settings set = Settings.Default;
        string strConnectionString = "";
        public stcUpdatedGlobals stcGlobals;

        public frmUserSettings()
        {
            InitializeComponent();
            strConnectionString = set.WoodPlanConnectionString;
        }

        private void frmUserSettings_Load(object sender, EventArgs e)
        {
            Set_Grid_Highlighter_Transparent(this.Controls);

            this.sp00070_Get_Magnifier_ShapeTableAdapter.Fill(this.woodPlanDataSet.sp00070_Get_Magnifier_Shape);

            sp00047AvailableStartUpScreensForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00047AvailableStartUpScreensForUserTableAdapter.Fill(this.woodPlanDataSet.sp00047AvailableStartUpScreensForUser, GlobalSettings.UserID);
  
            sp00064GetStaffApplicationSettingsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00064GetStaffApplicationSettingsTableAdapter.Fill(this.woodPlanDataSet.sp00064GetStaffApplicationSettings, GlobalSettings.UserID);

            sp00068_Get_Magnifier_SettingsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00068_Get_Magnifier_SettingsTableAdapter.Fill(this.woodPlanDataSet.sp00068_Get_Magnifier_Settings, 0, GlobalSettings.UserID, 0);

            if (this.woodPlanDataSet.sp00068_Get_Magnifier_Settings.Rows.Count == 0)
            {
                DataRow drNew = this.woodPlanDataSet.sp00068_Get_Magnifier_Settings.NewRow();
                drNew["MagnifierID"] = 0;
                drNew["UserID"] = GlobalSettings.UserID;
                drNew["UserType"] = 0;
                drNew["LocationX"] = 0;
                drNew["LocationY"] = 0;
                drNew["CloseOnMouseUp"] = false;
                drNew["DoubleBuffered"] = true;
                drNew["HideMouseCursor"] = true;
                drNew["RememberLastPoint"] = true;
                drNew["ReturnToOrigin"] = true;
                drNew["TopMostWindow"] = true;
                drNew["MagnifierWidth"] = 150;
                drNew["MagnifierHeight"] = 150;
                drNew["ZoomFactor"] = 3;
                drNew["SpeedFactor"] = 0.35F;
                drNew["MagnifierShape"] = 0;
                this.woodPlanDataSet.sp00068_Get_Magnifier_Settings.Rows.Add(drNew);
            }

            sp00070_Get_Magnifier_ShapeTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00070_Get_Magnifier_ShapeTableAdapter.Fill(this.woodPlanDataSet.sp00070_Get_Magnifier_Shape);

            // Restrict list of fonts available //
            int intRows = repositoryItemFontEdit1.Items.Count;  //repositoryItemFontEdit1.Properties.Items.Count;
            string fontName = "";
            for (int i = intRows - 1; i >= 0; i--)
            {
                fontName = repositoryItemFontEdit1.Items[i].ToString(); // repositoryItemFontEdit1.Properties.Items[i].ToString();
                if (fontName != "Tahoma" && fontName != "Comic Sans MS" && fontName != "Microsoft Sans Serif" && fontName != "Arial" && fontName != "Times New Roman")
                {
                    repositoryItemFontEdit1.Items.RemoveAt(i);
                }
            }

        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            Boolean blShowTipsOnStartup = false;
            Boolean blShowToolTips = false;
            Boolean blShowConfirmations = false;
            Boolean blMinimiseToTaskBar = false;
            int intShowPhotoPopup = 1;
            int intOriginalPhotoPopup = GlobalSettings.ShowPhotoPopup;
            string strDefaultFontName = "";
            int intDefaultFontSize = 0;
            int intFirstScreenLoadedID = 0;
            int EstatePlanInspectionControlPanel = 0;

            blShowTipsOnStartup = Convert.ToBoolean(this.woodPlanDataSet.sp00064GetStaffApplicationSettings.Rows[0]["ShowTipsOnStartup"]);
            blShowToolTips = Convert.ToBoolean(this.woodPlanDataSet.sp00064GetStaffApplicationSettings.Rows[0]["ShowToolTips"]);
            blShowConfirmations = Convert.ToBoolean(this.woodPlanDataSet.sp00064GetStaffApplicationSettings.Rows[0]["ShowConfirmations"]);
            blMinimiseToTaskBar = Convert.ToBoolean(this.woodPlanDataSet.sp00064GetStaffApplicationSettings.Rows[0]["MinimiseToTaskBar"]);
            intShowPhotoPopup = Convert.ToInt32(this.woodPlanDataSet.sp00064GetStaffApplicationSettings.Rows[0]["ShowPhotoPopup"]);
            strDefaultFontName = Convert.ToString(this.woodPlanDataSet.sp00064GetStaffApplicationSettings.Rows[0]["DefaultFontName"]);
            intDefaultFontSize = Convert.ToInt32(this.woodPlanDataSet.sp00064GetStaffApplicationSettings.Rows[0]["DefaultFontSize"]);
            intFirstScreenLoadedID = Convert.ToInt32(this.woodPlanDataSet.sp00064GetStaffApplicationSettings.Rows[0]["FirstScreenLoadedID"]);
            EstatePlanInspectionControlPanel = Convert.ToInt32(this.woodPlanDataSet.sp00064GetStaffApplicationSettings.Rows[0]["EstatePlanInspectionControlPanel"]);

            
            sp00064GetStaffApplicationSettingsTableAdapter.sp00065StoreStaffApplicationSettings(GlobalSettings.UserID, GlobalSettings.Username, blShowTipsOnStartup, blShowToolTips,
                                                                  blShowConfirmations, blMinimiseToTaskBar, intShowPhotoPopup, strDefaultFontName, intDefaultFontSize, intFirstScreenLoadedID, EstatePlanInspectionControlPanel);

            stcGlobals = new stcUpdatedGlobals();
            stcGlobals.blShowTipsOnStartup = blShowTipsOnStartup;
            stcGlobals.blShowToolTips = blShowToolTips;
            stcGlobals.blShowConfirmations = blShowConfirmations;
            stcGlobals.blMinimiseToTaskBar = blMinimiseToTaskBar;
            stcGlobals.intShowPhotoPopup = intShowPhotoPopup;
            stcGlobals.strDefaultFontName = strDefaultFontName;
            stcGlobals.intDefaultFontSize = intDefaultFontSize;

            MethodInfo FontMethod = this.Owner.GetType().GetMethod("UpdateFontSettings");
            if (FontMethod != null)
            {
                FontMethod.Invoke(this.Owner, new object[] { strDefaultFontName, intDefaultFontSize });
            }

            if (intShowPhotoPopup != intOriginalPhotoPopup)
            {
                if (this.Owner != null)
                {
                    // Send clipboard switch on/off message to main form...
                    MethodInfo method = this.Owner.GetType().GetMethod("OnShowPhotoPopupChanged");
                    if (method != null)
                    {
                        method.Invoke(this.Owner, new object[] { this, intShowPhotoPopup });
                    }
                }
            }

            SaveMagnifierSettings();

            this.Close();
        }

        private void SaveMagnifierSettings()
        {
            int intMagnifierID = 0;
            int intUserID = 0;
            int intUserType = 0;
            int intLocationX = 0;
            int intLocationY = 0; 
            Boolean blCloseOnMouseUp = false;
			Boolean blDoubleBuffered = true;
            Boolean blHideMouseCursor = true;
            Boolean blRememberLastPoint = true;
			Boolean blReturnToOrigin = false;
            Boolean blTopMostWindow = true;
            int intMagnifierWidth = 150;
			int intMagnifierHeight = 150;
            int intZoomFactor = 3;
            double dblSpeedFactor = 0.35F;
            int intMagnifierShape = 0;

            intMagnifierID = Convert.ToInt32(this.woodPlanDataSet.sp00068_Get_Magnifier_Settings.Rows[0]["MagnifierID"]);
            intUserID = GlobalSettings.UserID;
            intUserType = 0;

            intLocationX = Convert.ToInt32(this.woodPlanDataSet.sp00068_Get_Magnifier_Settings.Rows[0]["LocationX"]);
            intLocationY = Convert.ToInt32(this.woodPlanDataSet.sp00068_Get_Magnifier_Settings.Rows[0]["LocationY"]);
            blCloseOnMouseUp = Convert.ToBoolean(this.woodPlanDataSet.sp00068_Get_Magnifier_Settings.Rows[0]["CloseOnMouseUp"]);
            blDoubleBuffered = Convert.ToBoolean(this.woodPlanDataSet.sp00068_Get_Magnifier_Settings.Rows[0]["DoubleBuffered"]);
            blHideMouseCursor = Convert.ToBoolean(this.woodPlanDataSet.sp00068_Get_Magnifier_Settings.Rows[0]["HideMouseCursor"]);
            blRememberLastPoint = Convert.ToBoolean(this.woodPlanDataSet.sp00068_Get_Magnifier_Settings.Rows[0]["RememberLastPoint"]);
            blReturnToOrigin = Convert.ToBoolean(this.woodPlanDataSet.sp00068_Get_Magnifier_Settings.Rows[0]["ReturnToOrigin"]);
            blTopMostWindow = Convert.ToBoolean(this.woodPlanDataSet.sp00068_Get_Magnifier_Settings.Rows[0]["TopMostWindow"]);
            intMagnifierWidth = Convert.ToInt32(this.woodPlanDataSet.sp00068_Get_Magnifier_Settings.Rows[0]["MagnifierWidth"]);
            intMagnifierHeight = Convert.ToInt32(this.woodPlanDataSet.sp00068_Get_Magnifier_Settings.Rows[0]["MagnifierHeight"]);
            intZoomFactor = Convert.ToInt32(this.woodPlanDataSet.sp00068_Get_Magnifier_Settings.Rows[0]["ZoomFactor"]);
            dblSpeedFactor = Convert.ToDouble(this.woodPlanDataSet.sp00068_Get_Magnifier_Settings.Rows[0]["SpeedFactor"]);
            intMagnifierShape = Convert.ToInt32(this.woodPlanDataSet.sp00068_Get_Magnifier_Settings.Rows[0]["MagnifierShape"]);

            sp00068_Get_Magnifier_SettingsTableAdapter.sp00069_Store_Magnifier_Settings(intMagnifierID, intUserID, intUserType, intLocationX, intLocationY,
                                                               blCloseOnMouseUp, blDoubleBuffered, blHideMouseCursor, blRememberLastPoint,
                                                               blReturnToOrigin, blTopMostWindow, intMagnifierWidth, intMagnifierHeight, intZoomFactor,
                                                               dblSpeedFactor, intMagnifierShape);

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}


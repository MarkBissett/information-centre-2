namespace WoodPlan5
{
    partial class frm_Core_Accident_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Core_Accident_Edit));
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling13 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling14 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling15 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling16 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling17 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition3 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling18 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling19 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling20 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling21 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bbiLinkedDocuments = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.ReportedByPersonTypeIDParentTexEdit = new DevExpress.XtraEditors.TextEdit();
            this.sp00303AccidentEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_Accident = new WoodPlan5.DataSet_Accident();
            this.LinkedToRecordTypeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp00306AccidentLinkedRecordTypesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.HSQETeamLeadNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.ClosedByStaffButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.ResponsibleManagerButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.SiteNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LinkedToRecordFullDescriptionButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.ReportedByPersonButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.ReportedByPersonTypeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AccidentTypeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClosedByStaffIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClosedDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.TargetClosureDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.HSQETeamLeadTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ResponsibleManagerIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CorrectiveActionRemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.SituationRemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ClientReferenceNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AccidentDescriptionMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ReportedToHSECheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ReportedByPersonOtherTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ReportedByPersonTypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ReportedByPersonIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AccidentSubTypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.BusinessAreaIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.spHR00099BusinessAreasWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_Core = new WoodPlan5.DataSet_HR_Core();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ExternalReferenceNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AccidentStatusIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp00305AccidentStatusesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.AccidentTypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LinkedToRecordIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.DateReportedToHSEDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.AccidentDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.AccidentSubTypeButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.AccidentIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ItemForResponsibleManagerID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForHSQETeamLead = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClosedByStaffID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAccidentTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAccidentSubTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAccidentID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForReportedByPersonID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForReportedByPersonTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForReportedByPersonTypeIDParent = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLinkedToRecordID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForReportedByPersonOther = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layGrpAddress = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForReportedToHSE = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForExternalReferenceNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientReferenceNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTargetClosureDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForReportedByPersonType = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForReportedByPerson = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup10 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForLinkedToRecordFullDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLinkedToRecordTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForDateReportedToHSE = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClosedDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClosedByStaff = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForAccidentType = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAccidentSubType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForAccidentDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSituationRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForCorrectiveActionRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForBusinessAreaID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForAccidentDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAccidentStatusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForResponsibleManager = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForHSQETeamLeadName = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.sp00303_Accident_EditTableAdapter = new WoodPlan5.DataSet_AccidentTableAdapters.sp00303_Accident_EditTableAdapter();
            this.sp_HR_00099_Business_Areas_With_BlankTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00099_Business_Areas_With_BlankTableAdapter();
            this.sp00305_Accident_Statuses_With_BlankTableAdapter = new WoodPlan5.DataSet_AccidentTableAdapters.sp00305_Accident_Statuses_With_BlankTableAdapter();
            this.sp00306_Accident_Linked_Record_Types_With_BlankTableAdapter = new WoodPlan5.DataSet_AccidentTableAdapters.sp00306_Accident_Linked_Record_Types_With_BlankTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ReportedByPersonTypeIDParentTexEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00303AccidentEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Accident)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToRecordTypeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00306AccidentLinkedRecordTypesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HSQETeamLeadNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClosedByStaffButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResponsibleManagerButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToRecordFullDescriptionButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportedByPersonButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportedByPersonTypeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentTypeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClosedByStaffIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClosedDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClosedDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TargetClosureDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TargetClosureDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HSQETeamLeadTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResponsibleManagerIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CorrectiveActionRemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SituationRemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientReferenceNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentDescriptionMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportedToHSECheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportedByPersonOtherTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportedByPersonTypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportedByPersonIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentSubTypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BusinessAreaIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00099BusinessAreasWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExternalReferenceNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentStatusIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00305AccidentStatusesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentTypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToRecordIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateReportedToHSEDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateReportedToHSEDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentSubTypeButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForResponsibleManagerID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHSQETeamLead)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClosedByStaffID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccidentTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccidentSubTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccidentID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReportedByPersonID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReportedByPersonTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReportedByPersonTypeIDParent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToRecordID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReportedByPersonOther)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReportedToHSE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExternalReferenceNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientReferenceNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTargetClosureDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReportedByPersonType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReportedByPerson)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToRecordFullDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToRecordTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateReportedToHSE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClosedDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClosedByStaff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccidentType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccidentSubType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccidentDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSituationRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCorrectiveActionRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBusinessAreaID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccidentDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccidentStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForResponsibleManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHSQETeamLeadName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(686, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 593);
            this.barDockControlBottom.Size = new System.Drawing.Size(686, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 567);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(686, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 567);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "ID";
            this.gridColumn7.FieldName = "ID";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn7.Width = 53;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "ID";
            this.gridColumn1.FieldName = "ID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn1.Width = 53;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "ID";
            this.gridColumn4.FieldName = "ID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn4.Width = 53;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation,
            this.bbiLinkedDocuments});
            this.barManager2.MaxItemId = 16;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiLinkedDocuments, true)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem2.Image")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bbiLinkedDocuments
            // 
            this.bbiLinkedDocuments.Caption = "Linked Documents";
            this.bbiLinkedDocuments.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiLinkedDocuments.Glyph")));
            this.bbiLinkedDocuments.Id = 15;
            this.bbiLinkedDocuments.Name = "bbiLinkedDocuments";
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem3.Image")));
            toolTipTitleItem3.Text = "Linked Documents - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to open the Linked Documents Manager, filtered on the current data entry" +
    " record.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiLinkedDocuments.SuperTip = superToolTip3;
            this.bbiLinkedDocuments.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLinkedDocuments_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Text = "Form Mode - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.barStaticItemFormMode.SuperTip = superToolTip4;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(686, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 593);
            this.barDockControl2.Size = new System.Drawing.Size(686, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 567);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(686, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 567);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Preview_16x16, "Preview_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.ReportedByPersonTypeIDParentTexEdit);
            this.dataLayoutControl1.Controls.Add(this.LinkedToRecordTypeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.HSQETeamLeadNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.ClosedByStaffButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.ResponsibleManagerButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LinkedToRecordFullDescriptionButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.ReportedByPersonButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.ReportedByPersonTypeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AccidentTypeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClosedByStaffIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClosedDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.TargetClosureDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.HSQETeamLeadTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ResponsibleManagerIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CorrectiveActionRemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.SituationRemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientReferenceNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AccidentDescriptionMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.ReportedToHSECheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ReportedByPersonOtherTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ReportedByPersonTypeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ReportedByPersonIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AccidentSubTypeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.BusinessAreaIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ExternalReferenceNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AccidentStatusIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.AccidentTypeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LinkedToRecordIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.DateReportedToHSEDateEdit);
            this.dataLayoutControl1.Controls.Add(this.AccidentDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.AccidentSubTypeButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.AccidentIDTextEdit);
            this.dataLayoutControl1.DataSource = this.sp00303AccidentEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForResponsibleManagerID,
            this.ItemForHSQETeamLead,
            this.ItemForClosedByStaffID,
            this.ItemForAccidentTypeID,
            this.ItemForAccidentSubTypeID,
            this.ItemForAccidentID,
            this.ItemForReportedByPersonID,
            this.ItemForReportedByPersonTypeID,
            this.ItemForReportedByPersonTypeIDParent,
            this.ItemForLinkedToRecordID,
            this.ItemForReportedByPersonOther});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1234, 151, 301, 422);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(686, 567);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // ReportedByPersonTypeIDParentTexEdit
            // 
            this.ReportedByPersonTypeIDParentTexEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00303AccidentEditBindingSource, "ReportedByPersonTypeIDParent", true));
            this.ReportedByPersonTypeIDParentTexEdit.Location = new System.Drawing.Point(227, 404);
            this.ReportedByPersonTypeIDParentTexEdit.MenuManager = this.barManager1;
            this.ReportedByPersonTypeIDParentTexEdit.Name = "ReportedByPersonTypeIDParentTexEdit";
            this.ReportedByPersonTypeIDParentTexEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ReportedByPersonTypeIDParentTexEdit, true);
            this.ReportedByPersonTypeIDParentTexEdit.Size = new System.Drawing.Size(394, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ReportedByPersonTypeIDParentTexEdit, optionsSpelling1);
            this.ReportedByPersonTypeIDParentTexEdit.StyleController = this.dataLayoutControl1;
            this.ReportedByPersonTypeIDParentTexEdit.TabIndex = 66;
            // 
            // sp00303AccidentEditBindingSource
            // 
            this.sp00303AccidentEditBindingSource.DataMember = "sp00303_Accident_Edit";
            this.sp00303AccidentEditBindingSource.DataSource = this.dataSet_Accident;
            // 
            // dataSet_Accident
            // 
            this.dataSet_Accident.DataSetName = "DataSet_Accident";
            this.dataSet_Accident.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // LinkedToRecordTypeIDGridLookUpEdit
            // 
            this.LinkedToRecordTypeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00303AccidentEditBindingSource, "LinkedToRecordTypeID", true));
            this.LinkedToRecordTypeIDGridLookUpEdit.Location = new System.Drawing.Point(178, 463);
            this.LinkedToRecordTypeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.LinkedToRecordTypeIDGridLookUpEdit.Name = "LinkedToRecordTypeIDGridLookUpEdit";
            this.LinkedToRecordTypeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LinkedToRecordTypeIDGridLookUpEdit.Properties.DataSource = this.sp00306AccidentLinkedRecordTypesWithBlankBindingSource;
            this.LinkedToRecordTypeIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.LinkedToRecordTypeIDGridLookUpEdit.Properties.NullText = "";
            this.LinkedToRecordTypeIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.LinkedToRecordTypeIDGridLookUpEdit.Properties.View = this.gridView2;
            this.LinkedToRecordTypeIDGridLookUpEdit.Size = new System.Drawing.Size(443, 20);
            this.LinkedToRecordTypeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.LinkedToRecordTypeIDGridLookUpEdit.TabIndex = 13;
            this.LinkedToRecordTypeIDGridLookUpEdit.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.LinkedToRecordTypeIDGridLookUpEdit_EditValueChanging);
            this.LinkedToRecordTypeIDGridLookUpEdit.Validated += new System.EventHandler(this.LinkedToRecordTypeIDGridLookUpEdit_Validated);
            // 
            // sp00306AccidentLinkedRecordTypesWithBlankBindingSource
            // 
            this.sp00306AccidentLinkedRecordTypesWithBlankBindingSource.DataMember = "sp00306_Accident_Linked_Record_Types_With_Blank";
            this.sp00306AccidentLinkedRecordTypesWithBlankBindingSource.DataSource = this.dataSet_Accident;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.gridColumn7;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView2.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn9, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Linked Record Type";
            this.gridColumn8.FieldName = "Description";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 0;
            this.gridColumn8.Width = 220;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Order";
            this.gridColumn9.FieldName = "Order";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // HSQETeamLeadNameButtonEdit
            // 
            this.HSQETeamLeadNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00303AccidentEditBindingSource, "HSQETeamLeadName", true));
            this.HSQETeamLeadNameButtonEdit.EditValue = "";
            this.HSQETeamLeadNameButtonEdit.Location = new System.Drawing.Point(142, 107);
            this.HSQETeamLeadNameButtonEdit.MenuManager = this.barManager1;
            this.HSQETeamLeadNameButtonEdit.Name = "HSQETeamLeadNameButtonEdit";
            this.HSQETeamLeadNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click me to Open Select Staff screen", "choose", null, true)});
            this.HSQETeamLeadNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.HSQETeamLeadNameButtonEdit.Size = new System.Drawing.Size(515, 20);
            this.HSQETeamLeadNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.HSQETeamLeadNameButtonEdit.TabIndex = 14;
            this.HSQETeamLeadNameButtonEdit.TabStop = false;
            this.HSQETeamLeadNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.HSQETeamLeadNameButtonEdit_ButtonClick);
            // 
            // ClosedByStaffButtonEdit
            // 
            this.ClosedByStaffButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00303AccidentEditBindingSource, "ClosedByStaff", true));
            this.ClosedByStaffButtonEdit.EditValue = "";
            this.ClosedByStaffButtonEdit.Location = new System.Drawing.Point(166, 687);
            this.ClosedByStaffButtonEdit.MenuManager = this.barManager1;
            this.ClosedByStaffButtonEdit.Name = "ClosedByStaffButtonEdit";
            this.ClosedByStaffButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Click me to Open Select Staff screen", "choose", null, true)});
            this.ClosedByStaffButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ClosedByStaffButtonEdit.Size = new System.Drawing.Size(467, 20);
            this.ClosedByStaffButtonEdit.StyleController = this.dataLayoutControl1;
            this.ClosedByStaffButtonEdit.TabIndex = 15;
            this.ClosedByStaffButtonEdit.TabStop = false;
            this.ClosedByStaffButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ClosedByStaffButtonEdit_ButtonClick);
            // 
            // ResponsibleManagerButtonEdit
            // 
            this.ResponsibleManagerButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00303AccidentEditBindingSource, "ResponsibleManager", true));
            this.ResponsibleManagerButtonEdit.EditValue = "";
            this.ResponsibleManagerButtonEdit.Location = new System.Drawing.Point(142, 83);
            this.ResponsibleManagerButtonEdit.MenuManager = this.barManager1;
            this.ResponsibleManagerButtonEdit.Name = "ResponsibleManagerButtonEdit";
            this.ResponsibleManagerButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Click me to Open Select Staff screen", "choose", null, true)});
            this.ResponsibleManagerButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ResponsibleManagerButtonEdit.Size = new System.Drawing.Size(515, 20);
            this.ResponsibleManagerButtonEdit.StyleController = this.dataLayoutControl1;
            this.ResponsibleManagerButtonEdit.TabIndex = 13;
            this.ResponsibleManagerButtonEdit.TabStop = false;
            this.ResponsibleManagerButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ResponsibleManagerButtonEdit_ButtonClick);
            // 
            // SiteNameTextEdit
            // 
            this.SiteNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00303AccidentEditBindingSource, "SiteName", true));
            this.SiteNameTextEdit.Location = new System.Drawing.Point(178, 511);
            this.SiteNameTextEdit.MenuManager = this.barManager1;
            this.SiteNameTextEdit.Name = "SiteNameTextEdit";
            this.SiteNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteNameTextEdit, true);
            this.SiteNameTextEdit.Size = new System.Drawing.Size(443, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteNameTextEdit, optionsSpelling2);
            this.SiteNameTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteNameTextEdit.TabIndex = 65;
            // 
            // ClientNameTextEdit
            // 
            this.ClientNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00303AccidentEditBindingSource, "ClientName", true));
            this.ClientNameTextEdit.Location = new System.Drawing.Point(178, 487);
            this.ClientNameTextEdit.MenuManager = this.barManager1;
            this.ClientNameTextEdit.Name = "ClientNameTextEdit";
            this.ClientNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientNameTextEdit, true);
            this.ClientNameTextEdit.Size = new System.Drawing.Size(443, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientNameTextEdit, optionsSpelling3);
            this.ClientNameTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientNameTextEdit.TabIndex = 64;
            // 
            // LinkedToRecordFullDescriptionButtonEdit
            // 
            this.LinkedToRecordFullDescriptionButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00303AccidentEditBindingSource, "LinkedToRecordFullDescription", true));
            this.LinkedToRecordFullDescriptionButtonEdit.EditValue = "";
            this.LinkedToRecordFullDescriptionButtonEdit.Location = new System.Drawing.Point(178, 535);
            this.LinkedToRecordFullDescriptionButtonEdit.MenuManager = this.barManager1;
            this.LinkedToRecordFullDescriptionButtonEdit.Name = "LinkedToRecordFullDescriptionButtonEdit";
            this.LinkedToRecordFullDescriptionButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Click me to Open Select Linked Record screen", "choose", null, true)});
            this.LinkedToRecordFullDescriptionButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.LinkedToRecordFullDescriptionButtonEdit.Size = new System.Drawing.Size(443, 20);
            this.LinkedToRecordFullDescriptionButtonEdit.StyleController = this.dataLayoutControl1;
            this.LinkedToRecordFullDescriptionButtonEdit.TabIndex = 14;
            this.LinkedToRecordFullDescriptionButtonEdit.TabStop = false;
            this.LinkedToRecordFullDescriptionButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.LinkedToRecordFullDescriptionButtonEdit_ButtonClick);
            // 
            // ReportedByPersonButtonEdit
            // 
            this.ReportedByPersonButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00303AccidentEditBindingSource, "ReportedByPerson", true));
            this.ReportedByPersonButtonEdit.EditValue = "";
            this.ReportedByPersonButtonEdit.Location = new System.Drawing.Point(178, 383);
            this.ReportedByPersonButtonEdit.MenuManager = this.barManager1;
            this.ReportedByPersonButtonEdit.Name = "ReportedByPersonButtonEdit";
            this.ReportedByPersonButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "Click me to Open Select Reported By screen", "choose", null, true)});
            this.ReportedByPersonButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ReportedByPersonButtonEdit.Size = new System.Drawing.Size(443, 20);
            this.ReportedByPersonButtonEdit.StyleController = this.dataLayoutControl1;
            this.ReportedByPersonButtonEdit.TabIndex = 13;
            this.ReportedByPersonButtonEdit.TabStop = false;
            this.ReportedByPersonButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ReportedByPersonButtonEdit_ButtonClick);
            // 
            // ReportedByPersonTypeTextEdit
            // 
            this.ReportedByPersonTypeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00303AccidentEditBindingSource, "ReportedByPersonType", true));
            this.ReportedByPersonTypeTextEdit.Location = new System.Drawing.Point(178, 359);
            this.ReportedByPersonTypeTextEdit.MenuManager = this.barManager1;
            this.ReportedByPersonTypeTextEdit.Name = "ReportedByPersonTypeTextEdit";
            this.ReportedByPersonTypeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ReportedByPersonTypeTextEdit, true);
            this.ReportedByPersonTypeTextEdit.Size = new System.Drawing.Size(443, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ReportedByPersonTypeTextEdit, optionsSpelling4);
            this.ReportedByPersonTypeTextEdit.StyleController = this.dataLayoutControl1;
            this.ReportedByPersonTypeTextEdit.TabIndex = 63;
            // 
            // AccidentTypeTextEdit
            // 
            this.AccidentTypeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00303AccidentEditBindingSource, "AccidentType", true));
            this.AccidentTypeTextEdit.Location = new System.Drawing.Point(178, 255);
            this.AccidentTypeTextEdit.MenuManager = this.barManager1;
            this.AccidentTypeTextEdit.Name = "AccidentTypeTextEdit";
            this.AccidentTypeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AccidentTypeTextEdit, true);
            this.AccidentTypeTextEdit.Size = new System.Drawing.Size(443, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AccidentTypeTextEdit, optionsSpelling5);
            this.AccidentTypeTextEdit.StyleController = this.dataLayoutControl1;
            this.AccidentTypeTextEdit.TabIndex = 62;
            // 
            // ClosedByStaffIDTextEdit
            // 
            this.ClosedByStaffIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00303AccidentEditBindingSource, "ClosedByStaffID", true));
            this.ClosedByStaffIDTextEdit.Location = new System.Drawing.Point(166, 292);
            this.ClosedByStaffIDTextEdit.MenuManager = this.barManager1;
            this.ClosedByStaffIDTextEdit.Name = "ClosedByStaffIDTextEdit";
            this.ClosedByStaffIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClosedByStaffIDTextEdit, true);
            this.ClosedByStaffIDTextEdit.Size = new System.Drawing.Size(467, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClosedByStaffIDTextEdit, optionsSpelling6);
            this.ClosedByStaffIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ClosedByStaffIDTextEdit.TabIndex = 61;
            // 
            // ClosedDateDateEdit
            // 
            this.ClosedDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00303AccidentEditBindingSource, "ClosedDate", true));
            this.ClosedDateDateEdit.EditValue = null;
            this.ClosedDateDateEdit.Location = new System.Drawing.Point(468, 663);
            this.ClosedDateDateEdit.MenuManager = this.barManager1;
            this.ClosedDateDateEdit.Name = "ClosedDateDateEdit";
            this.ClosedDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.ClosedDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ClosedDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ClosedDateDateEdit.Properties.Mask.EditMask = "g";
            this.ClosedDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ClosedDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.ClosedDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.ClosedDateDateEdit.Size = new System.Drawing.Size(165, 20);
            this.ClosedDateDateEdit.StyleController = this.dataLayoutControl1;
            this.ClosedDateDateEdit.TabIndex = 14;
            // 
            // TargetClosureDateDateEdit
            // 
            this.TargetClosureDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00303AccidentEditBindingSource, "TargetClosureDate", true));
            this.TargetClosureDateDateEdit.EditValue = null;
            this.TargetClosureDateDateEdit.Location = new System.Drawing.Point(166, 663);
            this.TargetClosureDateDateEdit.MenuManager = this.barManager1;
            this.TargetClosureDateDateEdit.Name = "TargetClosureDateDateEdit";
            this.TargetClosureDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.TargetClosureDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TargetClosureDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TargetClosureDateDateEdit.Properties.Mask.EditMask = "g";
            this.TargetClosureDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TargetClosureDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.TargetClosureDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.TargetClosureDateDateEdit.Size = new System.Drawing.Size(168, 20);
            this.TargetClosureDateDateEdit.StyleController = this.dataLayoutControl1;
            this.TargetClosureDateDateEdit.TabIndex = 13;
            // 
            // HSQETeamLeadTextEdit
            // 
            this.HSQETeamLeadTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00303AccidentEditBindingSource, "HSQETeamLead", true));
            this.HSQETeamLeadTextEdit.Location = new System.Drawing.Point(166, 316);
            this.HSQETeamLeadTextEdit.MenuManager = this.barManager1;
            this.HSQETeamLeadTextEdit.Name = "HSQETeamLeadTextEdit";
            this.HSQETeamLeadTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.HSQETeamLeadTextEdit, true);
            this.HSQETeamLeadTextEdit.Size = new System.Drawing.Size(467, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.HSQETeamLeadTextEdit, optionsSpelling7);
            this.HSQETeamLeadTextEdit.StyleController = this.dataLayoutControl1;
            this.HSQETeamLeadTextEdit.TabIndex = 59;
            // 
            // ResponsibleManagerIDTextEdit
            // 
            this.ResponsibleManagerIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00303AccidentEditBindingSource, "ResponsibleManagerID", true));
            this.ResponsibleManagerIDTextEdit.Location = new System.Drawing.Point(180, 436);
            this.ResponsibleManagerIDTextEdit.MenuManager = this.barManager1;
            this.ResponsibleManagerIDTextEdit.Name = "ResponsibleManagerIDTextEdit";
            this.ResponsibleManagerIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ResponsibleManagerIDTextEdit, true);
            this.ResponsibleManagerIDTextEdit.Size = new System.Drawing.Size(453, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ResponsibleManagerIDTextEdit, optionsSpelling8);
            this.ResponsibleManagerIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ResponsibleManagerIDTextEdit.TabIndex = 58;
            // 
            // CorrectiveActionRemarksMemoEdit
            // 
            this.CorrectiveActionRemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00303AccidentEditBindingSource, "CorrectiveActionRemarks", true));
            this.CorrectiveActionRemarksMemoEdit.Location = new System.Drawing.Point(36, 221);
            this.CorrectiveActionRemarksMemoEdit.MenuManager = this.barManager1;
            this.CorrectiveActionRemarksMemoEdit.Name = "CorrectiveActionRemarksMemoEdit";
            this.CorrectiveActionRemarksMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CorrectiveActionRemarksMemoEdit, true);
            this.CorrectiveActionRemarksMemoEdit.Size = new System.Drawing.Size(597, 506);
            this.scSpellChecker.SetSpellCheckerOptions(this.CorrectiveActionRemarksMemoEdit, optionsSpelling9);
            this.CorrectiveActionRemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.CorrectiveActionRemarksMemoEdit.TabIndex = 60;
            // 
            // SituationRemarksMemoEdit
            // 
            this.SituationRemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00303AccidentEditBindingSource, "SituationRemarks", true));
            this.SituationRemarksMemoEdit.Location = new System.Drawing.Point(36, 221);
            this.SituationRemarksMemoEdit.MenuManager = this.barManager1;
            this.SituationRemarksMemoEdit.Name = "SituationRemarksMemoEdit";
            this.SituationRemarksMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SituationRemarksMemoEdit, true);
            this.SituationRemarksMemoEdit.Size = new System.Drawing.Size(597, 506);
            this.scSpellChecker.SetSpellCheckerOptions(this.SituationRemarksMemoEdit, optionsSpelling10);
            this.SituationRemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.SituationRemarksMemoEdit.TabIndex = 57;
            // 
            // ClientReferenceNumberTextEdit
            // 
            this.ClientReferenceNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00303AccidentEditBindingSource, "ClientReferenceNumber", true));
            this.ClientReferenceNumberTextEdit.Location = new System.Drawing.Point(166, 605);
            this.ClientReferenceNumberTextEdit.MenuManager = this.barManager1;
            this.ClientReferenceNumberTextEdit.Name = "ClientReferenceNumberTextEdit";
            this.ClientReferenceNumberTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientReferenceNumberTextEdit, true);
            this.ClientReferenceNumberTextEdit.Size = new System.Drawing.Size(467, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientReferenceNumberTextEdit, optionsSpelling11);
            this.ClientReferenceNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientReferenceNumberTextEdit.TabIndex = 13;
            // 
            // AccidentDescriptionMemoEdit
            // 
            this.AccidentDescriptionMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00303AccidentEditBindingSource, "AccidentDescription", true));
            this.AccidentDescriptionMemoEdit.Location = new System.Drawing.Point(36, 221);
            this.AccidentDescriptionMemoEdit.MenuManager = this.barManager1;
            this.AccidentDescriptionMemoEdit.Name = "AccidentDescriptionMemoEdit";
            this.AccidentDescriptionMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AccidentDescriptionMemoEdit, true);
            this.AccidentDescriptionMemoEdit.Size = new System.Drawing.Size(597, 506);
            this.scSpellChecker.SetSpellCheckerOptions(this.AccidentDescriptionMemoEdit, optionsSpelling12);
            this.AccidentDescriptionMemoEdit.StyleController = this.dataLayoutControl1;
            this.AccidentDescriptionMemoEdit.TabIndex = 56;
            // 
            // ReportedToHSECheckEdit
            // 
            this.ReportedToHSECheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00303AccidentEditBindingSource, "ReportedToHSE", true));
            this.ReportedToHSECheckEdit.Location = new System.Drawing.Point(166, 639);
            this.ReportedToHSECheckEdit.MenuManager = this.barManager1;
            this.ReportedToHSECheckEdit.Name = "ReportedToHSECheckEdit";
            this.ReportedToHSECheckEdit.Properties.Caption = "[Tick if Yes]";
            this.ReportedToHSECheckEdit.Properties.ValueChecked = 1;
            this.ReportedToHSECheckEdit.Properties.ValueUnchecked = 0;
            this.ReportedToHSECheckEdit.Size = new System.Drawing.Size(168, 19);
            this.ReportedToHSECheckEdit.StyleController = this.dataLayoutControl1;
            this.ReportedToHSECheckEdit.TabIndex = 55;
            // 
            // ReportedByPersonOtherTextEdit
            // 
            this.ReportedByPersonOtherTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00303AccidentEditBindingSource, "ReportedByPersonOther", true));
            this.ReportedByPersonOtherTextEdit.Location = new System.Drawing.Point(182, 363);
            this.ReportedByPersonOtherTextEdit.MenuManager = this.barManager1;
            this.ReportedByPersonOtherTextEdit.Name = "ReportedByPersonOtherTextEdit";
            this.ReportedByPersonOtherTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ReportedByPersonOtherTextEdit, true);
            this.ReportedByPersonOtherTextEdit.Size = new System.Drawing.Size(439, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ReportedByPersonOtherTextEdit, optionsSpelling13);
            this.ReportedByPersonOtherTextEdit.StyleController = this.dataLayoutControl1;
            this.ReportedByPersonOtherTextEdit.TabIndex = 54;
            // 
            // ReportedByPersonTypeIDTextEdit
            // 
            this.ReportedByPersonTypeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00303AccidentEditBindingSource, "ReportedByPersonTypeID", true));
            this.ReportedByPersonTypeIDTextEdit.Location = new System.Drawing.Point(192, 404);
            this.ReportedByPersonTypeIDTextEdit.MenuManager = this.barManager1;
            this.ReportedByPersonTypeIDTextEdit.Name = "ReportedByPersonTypeIDTextEdit";
            this.ReportedByPersonTypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ReportedByPersonTypeIDTextEdit, true);
            this.ReportedByPersonTypeIDTextEdit.Size = new System.Drawing.Size(429, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ReportedByPersonTypeIDTextEdit, optionsSpelling14);
            this.ReportedByPersonTypeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ReportedByPersonTypeIDTextEdit.TabIndex = 53;
            // 
            // ReportedByPersonIDTextEdit
            // 
            this.ReportedByPersonIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00303AccidentEditBindingSource, "ReportedByPersonID", true));
            this.ReportedByPersonIDTextEdit.Location = new System.Drawing.Point(192, 428);
            this.ReportedByPersonIDTextEdit.MenuManager = this.barManager1;
            this.ReportedByPersonIDTextEdit.Name = "ReportedByPersonIDTextEdit";
            this.ReportedByPersonIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ReportedByPersonIDTextEdit, true);
            this.ReportedByPersonIDTextEdit.Size = new System.Drawing.Size(429, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ReportedByPersonIDTextEdit, optionsSpelling15);
            this.ReportedByPersonIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ReportedByPersonIDTextEdit.TabIndex = 52;
            // 
            // AccidentSubTypeIDTextEdit
            // 
            this.AccidentSubTypeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00303AccidentEditBindingSource, "AccidentSubTypeID", true));
            this.AccidentSubTypeIDTextEdit.Location = new System.Drawing.Point(166, 244);
            this.AccidentSubTypeIDTextEdit.MenuManager = this.barManager1;
            this.AccidentSubTypeIDTextEdit.Name = "AccidentSubTypeIDTextEdit";
            this.AccidentSubTypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AccidentSubTypeIDTextEdit, true);
            this.AccidentSubTypeIDTextEdit.Size = new System.Drawing.Size(467, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AccidentSubTypeIDTextEdit, optionsSpelling16);
            this.AccidentSubTypeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.AccidentSubTypeIDTextEdit.TabIndex = 51;
            // 
            // BusinessAreaIDGridLookUpEdit
            // 
            this.BusinessAreaIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00303AccidentEditBindingSource, "BusinessAreaID", true));
            this.BusinessAreaIDGridLookUpEdit.Location = new System.Drawing.Point(142, 35);
            this.BusinessAreaIDGridLookUpEdit.MenuManager = this.barManager1;
            this.BusinessAreaIDGridLookUpEdit.Name = "BusinessAreaIDGridLookUpEdit";
            this.BusinessAreaIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.BusinessAreaIDGridLookUpEdit.Properties.DataSource = this.spHR00099BusinessAreasWithBlankBindingSource;
            this.BusinessAreaIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.BusinessAreaIDGridLookUpEdit.Properties.NullText = "";
            this.BusinessAreaIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.BusinessAreaIDGridLookUpEdit.Properties.View = this.gridView3;
            this.BusinessAreaIDGridLookUpEdit.Size = new System.Drawing.Size(515, 20);
            this.BusinessAreaIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.BusinessAreaIDGridLookUpEdit.TabIndex = 7;
            this.BusinessAreaIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.BusinessAreaIDGridLookUpEdit_Validating);
            // 
            // spHR00099BusinessAreasWithBlankBindingSource
            // 
            this.spHR00099BusinessAreasWithBlankBindingSource.DataMember = "sp_HR_00099_Business_Areas_With_Blank";
            this.spHR00099BusinessAreasWithBlankBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // dataSet_HR_Core
            // 
            this.dataSet_HR_Core.DataSetName = "DataSet_HR_Core";
            this.dataSet_HR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.gridColumn1;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridView3.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Business Area";
            this.gridColumn2.FieldName = "Description";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 220;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Order";
            this.gridColumn3.FieldName = "Order";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // ExternalReferenceNumberTextEdit
            // 
            this.ExternalReferenceNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00303AccidentEditBindingSource, "ExternalReferenceNumber", true));
            this.ExternalReferenceNumberTextEdit.Location = new System.Drawing.Point(166, 581);
            this.ExternalReferenceNumberTextEdit.MenuManager = this.barManager1;
            this.ExternalReferenceNumberTextEdit.Name = "ExternalReferenceNumberTextEdit";
            this.ExternalReferenceNumberTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ExternalReferenceNumberTextEdit, true);
            this.ExternalReferenceNumberTextEdit.Size = new System.Drawing.Size(467, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ExternalReferenceNumberTextEdit, optionsSpelling17);
            this.ExternalReferenceNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.ExternalReferenceNumberTextEdit.TabIndex = 8;
            // 
            // AccidentStatusIDGridLookUpEdit
            // 
            this.AccidentStatusIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00303AccidentEditBindingSource, "AccidentStatusID", true));
            this.AccidentStatusIDGridLookUpEdit.Location = new System.Drawing.Point(402, 59);
            this.AccidentStatusIDGridLookUpEdit.MenuManager = this.barManager1;
            this.AccidentStatusIDGridLookUpEdit.Name = "AccidentStatusIDGridLookUpEdit";
            this.AccidentStatusIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AccidentStatusIDGridLookUpEdit.Properties.DataSource = this.sp00305AccidentStatusesWithBlankBindingSource;
            this.AccidentStatusIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.AccidentStatusIDGridLookUpEdit.Properties.NullText = "";
            this.AccidentStatusIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.AccidentStatusIDGridLookUpEdit.Properties.View = this.gridView1;
            this.AccidentStatusIDGridLookUpEdit.Size = new System.Drawing.Size(255, 20);
            this.AccidentStatusIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.AccidentStatusIDGridLookUpEdit.TabIndex = 5;
            this.AccidentStatusIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.AccidentStatusIDGridLookUpEdit_Validating);
            // 
            // sp00305AccidentStatusesWithBlankBindingSource
            // 
            this.sp00305AccidentStatusesWithBlankBindingSource.DataMember = "sp00305_Accident_Statuses_With_Blank";
            this.sp00305AccidentStatusesWithBlankBindingSource.DataSource = this.dataSet_Accident;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition3.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition3.Appearance.Options.UseForeColor = true;
            styleFormatCondition3.ApplyToRow = true;
            styleFormatCondition3.Column = this.gridColumn4;
            styleFormatCondition3.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition3.Value1 = 0;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition3});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn6, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Accident Status";
            this.gridColumn5.FieldName = "Description";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 220;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Order";
            this.gridColumn6.FieldName = "Order";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // AccidentTypeIDTextEdit
            // 
            this.AccidentTypeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00303AccidentEditBindingSource, "AccidentTypeID", true));
            this.AccidentTypeIDTextEdit.Location = new System.Drawing.Point(166, 268);
            this.AccidentTypeIDTextEdit.MenuManager = this.barManager1;
            this.AccidentTypeIDTextEdit.Name = "AccidentTypeIDTextEdit";
            this.AccidentTypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AccidentTypeIDTextEdit, true);
            this.AccidentTypeIDTextEdit.Size = new System.Drawing.Size(467, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AccidentTypeIDTextEdit, optionsSpelling18);
            this.AccidentTypeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.AccidentTypeIDTextEdit.TabIndex = 49;
            // 
            // LinkedToRecordIDTextEdit
            // 
            this.LinkedToRecordIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00303AccidentEditBindingSource, "LinkedToRecordID", true));
            this.LinkedToRecordIDTextEdit.Location = new System.Drawing.Point(178, 363);
            this.LinkedToRecordIDTextEdit.MenuManager = this.barManager1;
            this.LinkedToRecordIDTextEdit.Name = "LinkedToRecordIDTextEdit";
            this.LinkedToRecordIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LinkedToRecordIDTextEdit, true);
            this.LinkedToRecordIDTextEdit.Size = new System.Drawing.Size(443, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LinkedToRecordIDTextEdit, optionsSpelling19);
            this.LinkedToRecordIDTextEdit.StyleController = this.dataLayoutControl1;
            this.LinkedToRecordIDTextEdit.TabIndex = 44;
            // 
            // DateReportedToHSEDateEdit
            // 
            this.DateReportedToHSEDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00303AccidentEditBindingSource, "DateReportedToHSE", true));
            this.DateReportedToHSEDateEdit.EditValue = null;
            this.DateReportedToHSEDateEdit.Location = new System.Drawing.Point(468, 639);
            this.DateReportedToHSEDateEdit.MenuManager = this.barManager1;
            this.DateReportedToHSEDateEdit.Name = "DateReportedToHSEDateEdit";
            this.DateReportedToHSEDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.DateReportedToHSEDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateReportedToHSEDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateReportedToHSEDateEdit.Properties.Mask.EditMask = "g";
            this.DateReportedToHSEDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DateReportedToHSEDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.DateReportedToHSEDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.DateReportedToHSEDateEdit.Size = new System.Drawing.Size(165, 20);
            this.DateReportedToHSEDateEdit.StyleController = this.dataLayoutControl1;
            this.DateReportedToHSEDateEdit.TabIndex = 10;
            // 
            // AccidentDateDateEdit
            // 
            this.AccidentDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00303AccidentEditBindingSource, "AccidentDate", true));
            this.AccidentDateDateEdit.EditValue = null;
            this.AccidentDateDateEdit.Location = new System.Drawing.Point(142, 59);
            this.AccidentDateDateEdit.MenuManager = this.barManager1;
            this.AccidentDateDateEdit.Name = "AccidentDateDateEdit";
            this.AccidentDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.AccidentDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AccidentDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AccidentDateDateEdit.Properties.Mask.EditMask = "g";
            this.AccidentDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.AccidentDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.AccidentDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.AccidentDateDateEdit.Size = new System.Drawing.Size(126, 20);
            this.AccidentDateDateEdit.StyleController = this.dataLayoutControl1;
            this.AccidentDateDateEdit.TabIndex = 9;
            this.AccidentDateDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.AccidentDateDateEdit_Validating);
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00303AccidentEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, 221);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(597, 506);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling20);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 8;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp00303AccidentEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(142, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(173, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 24;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // AccidentSubTypeButtonEdit
            // 
            this.AccidentSubTypeButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00303AccidentEditBindingSource, "AccidentSubType", true));
            this.AccidentSubTypeButtonEdit.EditValue = "";
            this.AccidentSubTypeButtonEdit.Location = new System.Drawing.Point(178, 279);
            this.AccidentSubTypeButtonEdit.MenuManager = this.barManager1;
            this.AccidentSubTypeButtonEdit.Name = "AccidentSubTypeButtonEdit";
            this.AccidentSubTypeButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "Click me to Open Select Accident Type \\ Sub-Type screen", "choose", null, true)});
            this.AccidentSubTypeButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.AccidentSubTypeButtonEdit.Size = new System.Drawing.Size(443, 20);
            this.AccidentSubTypeButtonEdit.StyleController = this.dataLayoutControl1;
            this.AccidentSubTypeButtonEdit.TabIndex = 0;
            this.AccidentSubTypeButtonEdit.TabStop = false;
            this.AccidentSubTypeButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.AccidentSubTypeButtonEdit_ButtonClick);
            this.AccidentSubTypeButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.AccidentSubTypeButtonEdit_Validating);
            // 
            // AccidentIDTextEdit
            // 
            this.AccidentIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00303AccidentEditBindingSource, "AccidentID", true));
            this.AccidentIDTextEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.AccidentIDTextEdit.Location = new System.Drawing.Point(166, 220);
            this.AccidentIDTextEdit.MenuManager = this.barManager1;
            this.AccidentIDTextEdit.Name = "AccidentIDTextEdit";
            this.AccidentIDTextEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.AccidentIDTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.AccidentIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AccidentIDTextEdit, true);
            this.AccidentIDTextEdit.Size = new System.Drawing.Size(467, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AccidentIDTextEdit, optionsSpelling21);
            this.AccidentIDTextEdit.StyleController = this.dataLayoutControl1;
            this.AccidentIDTextEdit.TabIndex = 27;
            // 
            // ItemForResponsibleManagerID
            // 
            this.ItemForResponsibleManagerID.Control = this.ResponsibleManagerIDTextEdit;
            this.ItemForResponsibleManagerID.CustomizationFormText = "Responsible Manager ID:";
            this.ItemForResponsibleManagerID.Location = new System.Drawing.Point(0, 216);
            this.ItemForResponsibleManagerID.Name = "ItemForResponsibleManagerID";
            this.ItemForResponsibleManagerID.Size = new System.Drawing.Size(601, 24);
            this.ItemForResponsibleManagerID.Text = "Responsible Manager ID:";
            this.ItemForResponsibleManagerID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForHSQETeamLead
            // 
            this.ItemForHSQETeamLead.Control = this.HSQETeamLeadTextEdit;
            this.ItemForHSQETeamLead.CustomizationFormText = "HSQE Team Lead ID:";
            this.ItemForHSQETeamLead.Location = new System.Drawing.Point(0, 96);
            this.ItemForHSQETeamLead.Name = "ItemForHSQETeamLead";
            this.ItemForHSQETeamLead.Size = new System.Drawing.Size(601, 24);
            this.ItemForHSQETeamLead.Text = "HSQE Team Lead ID:";
            this.ItemForHSQETeamLead.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForClosedByStaffID
            // 
            this.ItemForClosedByStaffID.Control = this.ClosedByStaffIDTextEdit;
            this.ItemForClosedByStaffID.CustomizationFormText = "Closed By Staff ID:";
            this.ItemForClosedByStaffID.Location = new System.Drawing.Point(0, 72);
            this.ItemForClosedByStaffID.Name = "ItemForClosedByStaffID";
            this.ItemForClosedByStaffID.Size = new System.Drawing.Size(601, 24);
            this.ItemForClosedByStaffID.Text = "Closed By Staff ID:";
            this.ItemForClosedByStaffID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForAccidentTypeID
            // 
            this.ItemForAccidentTypeID.Control = this.AccidentTypeIDTextEdit;
            this.ItemForAccidentTypeID.CustomizationFormText = "Accident Type ID:";
            this.ItemForAccidentTypeID.Location = new System.Drawing.Point(0, 48);
            this.ItemForAccidentTypeID.Name = "ItemForAccidentTypeID";
            this.ItemForAccidentTypeID.Size = new System.Drawing.Size(601, 24);
            this.ItemForAccidentTypeID.Text = "Accident Type ID:";
            this.ItemForAccidentTypeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForAccidentSubTypeID
            // 
            this.ItemForAccidentSubTypeID.Control = this.AccidentSubTypeIDTextEdit;
            this.ItemForAccidentSubTypeID.CustomizationFormText = "Accident Sub-Type ID:";
            this.ItemForAccidentSubTypeID.Location = new System.Drawing.Point(0, 24);
            this.ItemForAccidentSubTypeID.Name = "ItemForAccidentSubTypeID";
            this.ItemForAccidentSubTypeID.Size = new System.Drawing.Size(601, 24);
            this.ItemForAccidentSubTypeID.Text = "Accident Sub-Type ID:";
            this.ItemForAccidentSubTypeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForAccidentID
            // 
            this.ItemForAccidentID.Control = this.AccidentIDTextEdit;
            this.ItemForAccidentID.CustomizationFormText = "Accident ID:";
            this.ItemForAccidentID.Location = new System.Drawing.Point(0, 0);
            this.ItemForAccidentID.Name = "ItemForAccidentID";
            this.ItemForAccidentID.Size = new System.Drawing.Size(601, 24);
            this.ItemForAccidentID.Text = "Accident ID:";
            this.ItemForAccidentID.TextSize = new System.Drawing.Size(84, 13);
            // 
            // ItemForReportedByPersonID
            // 
            this.ItemForReportedByPersonID.Control = this.ReportedByPersonIDTextEdit;
            this.ItemForReportedByPersonID.CustomizationFormText = "Reported By Person ID:";
            this.ItemForReportedByPersonID.Location = new System.Drawing.Point(0, 72);
            this.ItemForReportedByPersonID.Name = "ItemForReportedByPersonID";
            this.ItemForReportedByPersonID.Size = new System.Drawing.Size(577, 24);
            this.ItemForReportedByPersonID.Text = "Reported By Person ID:";
            this.ItemForReportedByPersonID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForReportedByPersonTypeID
            // 
            this.ItemForReportedByPersonTypeID.Control = this.ReportedByPersonTypeIDTextEdit;
            this.ItemForReportedByPersonTypeID.CustomizationFormText = "Reported By Person Type ID:";
            this.ItemForReportedByPersonTypeID.Location = new System.Drawing.Point(0, 48);
            this.ItemForReportedByPersonTypeID.Name = "ItemForReportedByPersonTypeID";
            this.ItemForReportedByPersonTypeID.Size = new System.Drawing.Size(577, 24);
            this.ItemForReportedByPersonTypeID.Text = "Reported By Person Type ID:";
            this.ItemForReportedByPersonTypeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForReportedByPersonTypeIDParent
            // 
            this.ItemForReportedByPersonTypeIDParent.Control = this.ReportedByPersonTypeIDParentTexEdit;
            this.ItemForReportedByPersonTypeIDParent.CustomizationFormText = "Reported By Person Type ID Parent:";
            this.ItemForReportedByPersonTypeIDParent.Location = new System.Drawing.Point(0, 48);
            this.ItemForReportedByPersonTypeIDParent.Name = "ItemForReportedByPersonTypeIDParent";
            this.ItemForReportedByPersonTypeIDParent.Size = new System.Drawing.Size(577, 24);
            this.ItemForReportedByPersonTypeIDParent.Text = "Reported By Person Type ID Parent:";
            this.ItemForReportedByPersonTypeIDParent.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForLinkedToRecordID
            // 
            this.ItemForLinkedToRecordID.Control = this.LinkedToRecordIDTextEdit;
            this.ItemForLinkedToRecordID.CustomizationFormText = "Linked To Record ID:";
            this.ItemForLinkedToRecordID.Location = new System.Drawing.Point(0, 96);
            this.ItemForLinkedToRecordID.Name = "ItemForLinkedToRecordID";
            this.ItemForLinkedToRecordID.Size = new System.Drawing.Size(577, 24);
            this.ItemForLinkedToRecordID.Text = "Linked To Record ID:";
            this.ItemForLinkedToRecordID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForReportedByPersonOther
            // 
            this.ItemForReportedByPersonOther.Control = this.ReportedByPersonOtherTextEdit;
            this.ItemForReportedByPersonOther.CustomizationFormText = "Reported By Person Other:";
            this.ItemForReportedByPersonOther.Location = new System.Drawing.Point(0, 96);
            this.ItemForReportedByPersonOther.Name = "ItemForReportedByPersonOther";
            this.ItemForReportedByPersonOther.Size = new System.Drawing.Size(577, 24);
            this.ItemForReportedByPersonOther.Text = "Reported By Person Other:";
            this.ItemForReportedByPersonOther.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(669, 763);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.emptySpaceItem4,
            this.layoutControlGroup6,
            this.ItemForBusinessAreaID,
            this.emptySpaceItem9,
            this.ItemForAccidentDate,
            this.ItemForAccidentStatusID,
            this.ItemForResponsibleManager,
            this.ItemForHSQETeamLeadName});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(649, 743);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(130, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(130, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(130, 23);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(307, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(342, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(130, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(177, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 129);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(649, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Details";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 139);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(649, 604);
            this.layoutControlGroup6.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layGrpAddress;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(625, 558);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layGrpAddress,
            this.layoutControlGroup5,
            this.layoutControlGroup7,
            this.layoutControlGroup8,
            this.layoutControlGroup4});
            // 
            // layGrpAddress
            // 
            this.layGrpAddress.CustomizationFormText = "Address";
            this.layGrpAddress.ExpandButtonVisible = true;
            this.layGrpAddress.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layGrpAddress.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem12,
            this.ItemForReportedToHSE,
            this.ItemForExternalReferenceNumber,
            this.ItemForClientReferenceNumber,
            this.ItemForTargetClosureDate,
            this.emptySpaceItem8,
            this.emptySpaceItem10,
            this.layoutControlGroup3,
            this.emptySpaceItem11,
            this.layoutControlGroup10,
            this.emptySpaceItem13,
            this.emptySpaceItem14,
            this.ItemForDateReportedToHSE,
            this.ItemForClosedDate,
            this.ItemForClosedByStaff,
            this.layoutControlGroup9});
            this.layGrpAddress.Location = new System.Drawing.Point(0, 0);
            this.layGrpAddress.Name = "layGrpAddress";
            this.layGrpAddress.Size = new System.Drawing.Size(601, 510);
            this.layGrpAddress.Text = "Details";
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.CustomizationFormText = "emptySpaceItem12";
            this.emptySpaceItem12.Location = new System.Drawing.Point(0, 500);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Size = new System.Drawing.Size(601, 10);
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForReportedToHSE
            // 
            this.ItemForReportedToHSE.Control = this.ReportedToHSECheckEdit;
            this.ItemForReportedToHSE.CustomizationFormText = "Reported To HSE:";
            this.ItemForReportedToHSE.Location = new System.Drawing.Point(0, 418);
            this.ItemForReportedToHSE.Name = "ItemForReportedToHSE";
            this.ItemForReportedToHSE.Size = new System.Drawing.Size(302, 24);
            this.ItemForReportedToHSE.Text = "Reported To HSE:";
            this.ItemForReportedToHSE.TextSize = new System.Drawing.Size(127, 13);
            // 
            // ItemForExternalReferenceNumber
            // 
            this.ItemForExternalReferenceNumber.Control = this.ExternalReferenceNumberTextEdit;
            this.ItemForExternalReferenceNumber.CustomizationFormText = "External Reference #:";
            this.ItemForExternalReferenceNumber.Location = new System.Drawing.Point(0, 360);
            this.ItemForExternalReferenceNumber.Name = "ItemForExternalReferenceNumber";
            this.ItemForExternalReferenceNumber.Size = new System.Drawing.Size(601, 24);
            this.ItemForExternalReferenceNumber.Text = "External Reference #:";
            this.ItemForExternalReferenceNumber.TextSize = new System.Drawing.Size(127, 13);
            // 
            // ItemForClientReferenceNumber
            // 
            this.ItemForClientReferenceNumber.Control = this.ClientReferenceNumberTextEdit;
            this.ItemForClientReferenceNumber.CustomizationFormText = "Client Reference #:";
            this.ItemForClientReferenceNumber.Location = new System.Drawing.Point(0, 384);
            this.ItemForClientReferenceNumber.Name = "ItemForClientReferenceNumber";
            this.ItemForClientReferenceNumber.Size = new System.Drawing.Size(601, 24);
            this.ItemForClientReferenceNumber.Text = "Client Reference #:";
            this.ItemForClientReferenceNumber.TextSize = new System.Drawing.Size(127, 13);
            // 
            // ItemForTargetClosureDate
            // 
            this.ItemForTargetClosureDate.Control = this.TargetClosureDateDateEdit;
            this.ItemForTargetClosureDate.CustomizationFormText = "Target Closure Date:";
            this.ItemForTargetClosureDate.Location = new System.Drawing.Point(0, 442);
            this.ItemForTargetClosureDate.Name = "ItemForTargetClosureDate";
            this.ItemForTargetClosureDate.Size = new System.Drawing.Size(302, 24);
            this.ItemForTargetClosureDate.Text = "Target Closure Date:";
            this.ItemForTargetClosureDate.TextSize = new System.Drawing.Size(127, 13);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(0, 490);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(601, 10);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.CustomizationFormText = "emptySpaceItem10";
            this.emptySpaceItem10.Location = new System.Drawing.Point(0, 408);
            this.emptySpaceItem10.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem10.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(601, 10);
            this.emptySpaceItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Reported By";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForReportedByPersonType,
            this.ItemForReportedByPerson});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 104);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(601, 94);
            this.layoutControlGroup3.Text = "Reported By";
            // 
            // ItemForReportedByPersonType
            // 
            this.ItemForReportedByPersonType.Control = this.ReportedByPersonTypeTextEdit;
            this.ItemForReportedByPersonType.CustomizationFormText = "Reported By Person Type:";
            this.ItemForReportedByPersonType.Location = new System.Drawing.Point(0, 0);
            this.ItemForReportedByPersonType.Name = "ItemForReportedByPersonType";
            this.ItemForReportedByPersonType.Size = new System.Drawing.Size(577, 24);
            this.ItemForReportedByPersonType.Text = "Reported By Person Type:";
            this.ItemForReportedByPersonType.TextSize = new System.Drawing.Size(127, 13);
            // 
            // ItemForReportedByPerson
            // 
            this.ItemForReportedByPerson.Control = this.ReportedByPersonButtonEdit;
            this.ItemForReportedByPerson.CustomizationFormText = "Reported By Person:";
            this.ItemForReportedByPerson.Location = new System.Drawing.Point(0, 24);
            this.ItemForReportedByPerson.Name = "ItemForReportedByPerson";
            this.ItemForReportedByPerson.Size = new System.Drawing.Size(577, 24);
            this.ItemForReportedByPerson.Text = "Reported By Person:";
            this.ItemForReportedByPerson.TextSize = new System.Drawing.Size(127, 13);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.CustomizationFormText = "emptySpaceItem11";
            this.emptySpaceItem11.Location = new System.Drawing.Point(0, 198);
            this.emptySpaceItem11.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem11.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(601, 10);
            this.emptySpaceItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup10
            // 
            this.layoutControlGroup10.CustomizationFormText = "Linked To";
            this.layoutControlGroup10.ExpandButtonVisible = true;
            this.layoutControlGroup10.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup10.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForLinkedToRecordFullDescription,
            this.ItemForClientName,
            this.ItemForSiteName,
            this.ItemForLinkedToRecordTypeID});
            this.layoutControlGroup10.Location = new System.Drawing.Point(0, 208);
            this.layoutControlGroup10.Name = "layoutControlGroup10";
            this.layoutControlGroup10.Size = new System.Drawing.Size(601, 142);
            this.layoutControlGroup10.Text = "Linked To";
            // 
            // ItemForLinkedToRecordFullDescription
            // 
            this.ItemForLinkedToRecordFullDescription.Control = this.LinkedToRecordFullDescriptionButtonEdit;
            this.ItemForLinkedToRecordFullDescription.CustomizationFormText = "Linked To Record:";
            this.ItemForLinkedToRecordFullDescription.Location = new System.Drawing.Point(0, 72);
            this.ItemForLinkedToRecordFullDescription.Name = "ItemForLinkedToRecordFullDescription";
            this.ItemForLinkedToRecordFullDescription.Size = new System.Drawing.Size(577, 24);
            this.ItemForLinkedToRecordFullDescription.Text = "Linked To Record:";
            this.ItemForLinkedToRecordFullDescription.TextSize = new System.Drawing.Size(127, 13);
            // 
            // ItemForClientName
            // 
            this.ItemForClientName.Control = this.ClientNameTextEdit;
            this.ItemForClientName.CustomizationFormText = "Client Name:";
            this.ItemForClientName.Location = new System.Drawing.Point(0, 24);
            this.ItemForClientName.Name = "ItemForClientName";
            this.ItemForClientName.Size = new System.Drawing.Size(577, 24);
            this.ItemForClientName.Text = "Client Name:";
            this.ItemForClientName.TextSize = new System.Drawing.Size(127, 13);
            // 
            // ItemForSiteName
            // 
            this.ItemForSiteName.Control = this.SiteNameTextEdit;
            this.ItemForSiteName.CustomizationFormText = "Site Name:";
            this.ItemForSiteName.Location = new System.Drawing.Point(0, 48);
            this.ItemForSiteName.Name = "ItemForSiteName";
            this.ItemForSiteName.Size = new System.Drawing.Size(577, 24);
            this.ItemForSiteName.Text = "Site Name:";
            this.ItemForSiteName.TextSize = new System.Drawing.Size(127, 13);
            // 
            // ItemForLinkedToRecordTypeID
            // 
            this.ItemForLinkedToRecordTypeID.Control = this.LinkedToRecordTypeIDGridLookUpEdit;
            this.ItemForLinkedToRecordTypeID.CustomizationFormText = "Linked To Record Type:";
            this.ItemForLinkedToRecordTypeID.Location = new System.Drawing.Point(0, 0);
            this.ItemForLinkedToRecordTypeID.Name = "ItemForLinkedToRecordTypeID";
            this.ItemForLinkedToRecordTypeID.Size = new System.Drawing.Size(577, 24);
            this.ItemForLinkedToRecordTypeID.Text = "Linked To Record Type:";
            this.ItemForLinkedToRecordTypeID.TextSize = new System.Drawing.Size(127, 13);
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.CustomizationFormText = "emptySpaceItem13";
            this.emptySpaceItem13.Location = new System.Drawing.Point(0, 350);
            this.emptySpaceItem13.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem13.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem13.Name = "emptySpaceItem13";
            this.emptySpaceItem13.Size = new System.Drawing.Size(601, 10);
            this.emptySpaceItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.CustomizationFormText = "emptySpaceItem14";
            this.emptySpaceItem14.Location = new System.Drawing.Point(0, 94);
            this.emptySpaceItem14.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem14.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem14.Name = "emptySpaceItem14";
            this.emptySpaceItem14.Size = new System.Drawing.Size(601, 10);
            this.emptySpaceItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForDateReportedToHSE
            // 
            this.ItemForDateReportedToHSE.Control = this.DateReportedToHSEDateEdit;
            this.ItemForDateReportedToHSE.CustomizationFormText = "Date Reported To HSE:";
            this.ItemForDateReportedToHSE.Location = new System.Drawing.Point(302, 418);
            this.ItemForDateReportedToHSE.Name = "ItemForDateReportedToHSE";
            this.ItemForDateReportedToHSE.Size = new System.Drawing.Size(299, 24);
            this.ItemForDateReportedToHSE.Text = "Date Reported To HSE:";
            this.ItemForDateReportedToHSE.TextSize = new System.Drawing.Size(127, 13);
            // 
            // ItemForClosedDate
            // 
            this.ItemForClosedDate.Control = this.ClosedDateDateEdit;
            this.ItemForClosedDate.CustomizationFormText = "Closed Date:";
            this.ItemForClosedDate.Location = new System.Drawing.Point(302, 442);
            this.ItemForClosedDate.Name = "ItemForClosedDate";
            this.ItemForClosedDate.Size = new System.Drawing.Size(299, 24);
            this.ItemForClosedDate.Text = "Closed Date:";
            this.ItemForClosedDate.TextSize = new System.Drawing.Size(127, 13);
            // 
            // ItemForClosedByStaff
            // 
            this.ItemForClosedByStaff.Control = this.ClosedByStaffButtonEdit;
            this.ItemForClosedByStaff.CustomizationFormText = "Closed By Staff:";
            this.ItemForClosedByStaff.Location = new System.Drawing.Point(0, 466);
            this.ItemForClosedByStaff.Name = "ItemForClosedByStaff";
            this.ItemForClosedByStaff.Size = new System.Drawing.Size(601, 24);
            this.ItemForClosedByStaff.Text = "Closed By Staff:";
            this.ItemForClosedByStaff.TextSize = new System.Drawing.Size(127, 13);
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.CustomizationFormText = "Type \\ Sub-Type";
            this.layoutControlGroup9.ExpandButtonVisible = true;
            this.layoutControlGroup9.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForAccidentType,
            this.ItemForAccidentSubType});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup9.Name = "layoutControlGroup9";
            this.layoutControlGroup9.Size = new System.Drawing.Size(601, 94);
            this.layoutControlGroup9.Text = "Type \\ Sub-Type";
            // 
            // ItemForAccidentType
            // 
            this.ItemForAccidentType.Control = this.AccidentTypeTextEdit;
            this.ItemForAccidentType.CustomizationFormText = "Type:";
            this.ItemForAccidentType.Location = new System.Drawing.Point(0, 0);
            this.ItemForAccidentType.Name = "ItemForAccidentType";
            this.ItemForAccidentType.Size = new System.Drawing.Size(577, 24);
            this.ItemForAccidentType.Text = "Type:";
            this.ItemForAccidentType.TextSize = new System.Drawing.Size(127, 13);
            // 
            // ItemForAccidentSubType
            // 
            this.ItemForAccidentSubType.AllowHide = false;
            this.ItemForAccidentSubType.Control = this.AccidentSubTypeButtonEdit;
            this.ItemForAccidentSubType.CustomizationFormText = "Sub-Type:";
            this.ItemForAccidentSubType.Location = new System.Drawing.Point(0, 24);
            this.ItemForAccidentSubType.Name = "ItemForAccidentSubType";
            this.ItemForAccidentSubType.Size = new System.Drawing.Size(577, 24);
            this.ItemForAccidentSubType.Text = "Sub-Type:";
            this.ItemForAccidentSubType.TextSize = new System.Drawing.Size(127, 13);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Accident Description";
            this.layoutControlGroup5.ExpandButtonVisible = true;
            this.layoutControlGroup5.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForAccidentDescription});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(601, 510);
            this.layoutControlGroup5.Text = "Accident Description";
            // 
            // ItemForAccidentDescription
            // 
            this.ItemForAccidentDescription.Control = this.AccidentDescriptionMemoEdit;
            this.ItemForAccidentDescription.CustomizationFormText = "Accident Description:";
            this.ItemForAccidentDescription.Location = new System.Drawing.Point(0, 0);
            this.ItemForAccidentDescription.Name = "ItemForAccidentDescription";
            this.ItemForAccidentDescription.Size = new System.Drawing.Size(601, 510);
            this.ItemForAccidentDescription.Text = "Accident Description:";
            this.ItemForAccidentDescription.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForAccidentDescription.TextVisible = false;
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "Situation";
            this.layoutControlGroup7.ExpandButtonVisible = true;
            this.layoutControlGroup7.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSituationRemarks});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(601, 510);
            this.layoutControlGroup7.Text = "Situation";
            // 
            // ItemForSituationRemarks
            // 
            this.ItemForSituationRemarks.Control = this.SituationRemarksMemoEdit;
            this.ItemForSituationRemarks.CustomizationFormText = "Situation:";
            this.ItemForSituationRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForSituationRemarks.Name = "ItemForSituationRemarks";
            this.ItemForSituationRemarks.Size = new System.Drawing.Size(601, 510);
            this.ItemForSituationRemarks.Text = "Situation:";
            this.ItemForSituationRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForSituationRemarks.TextVisible = false;
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.CustomizationFormText = "Corrective Actions";
            this.layoutControlGroup8.ExpandButtonVisible = true;
            this.layoutControlGroup8.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForCorrectiveActionRemarks});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Size = new System.Drawing.Size(601, 510);
            this.layoutControlGroup8.Text = "Corrective Actions";
            // 
            // ItemForCorrectiveActionRemarks
            // 
            this.ItemForCorrectiveActionRemarks.Control = this.CorrectiveActionRemarksMemoEdit;
            this.ItemForCorrectiveActionRemarks.CustomizationFormText = "Corrective Actions:";
            this.ItemForCorrectiveActionRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForCorrectiveActionRemarks.Name = "ItemForCorrectiveActionRemarks";
            this.ItemForCorrectiveActionRemarks.Size = new System.Drawing.Size(601, 510);
            this.ItemForCorrectiveActionRemarks.Text = "Corrective Actions:";
            this.ItemForCorrectiveActionRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForCorrectiveActionRemarks.TextVisible = false;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup4.CaptionImage")));
            this.layoutControlGroup4.CustomizationFormText = "Remarks";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(601, 510);
            this.layoutControlGroup4.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Comments:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(601, 510);
            this.ItemForRemarks.Text = "Comments:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // ItemForBusinessAreaID
            // 
            this.ItemForBusinessAreaID.Control = this.BusinessAreaIDGridLookUpEdit;
            this.ItemForBusinessAreaID.CustomizationFormText = "Business Area:";
            this.ItemForBusinessAreaID.Location = new System.Drawing.Point(0, 23);
            this.ItemForBusinessAreaID.Name = "ItemForBusinessAreaID";
            this.ItemForBusinessAreaID.Size = new System.Drawing.Size(649, 24);
            this.ItemForBusinessAreaID.Text = "Business Area:";
            this.ItemForBusinessAreaID.TextSize = new System.Drawing.Size(127, 13);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.CustomizationFormText = "emptySpaceItem9";
            this.emptySpaceItem9.Location = new System.Drawing.Point(0, 119);
            this.emptySpaceItem9.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem9.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(649, 10);
            this.emptySpaceItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForAccidentDate
            // 
            this.ItemForAccidentDate.Control = this.AccidentDateDateEdit;
            this.ItemForAccidentDate.CustomizationFormText = "Accident Date:";
            this.ItemForAccidentDate.Location = new System.Drawing.Point(0, 47);
            this.ItemForAccidentDate.MaxSize = new System.Drawing.Size(260, 24);
            this.ItemForAccidentDate.MinSize = new System.Drawing.Size(260, 24);
            this.ItemForAccidentDate.Name = "ItemForAccidentDate";
            this.ItemForAccidentDate.Size = new System.Drawing.Size(260, 24);
            this.ItemForAccidentDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForAccidentDate.Text = "Accident Date:";
            this.ItemForAccidentDate.TextSize = new System.Drawing.Size(127, 13);
            // 
            // ItemForAccidentStatusID
            // 
            this.ItemForAccidentStatusID.Control = this.AccidentStatusIDGridLookUpEdit;
            this.ItemForAccidentStatusID.CustomizationFormText = "Accident Status:";
            this.ItemForAccidentStatusID.Location = new System.Drawing.Point(260, 47);
            this.ItemForAccidentStatusID.Name = "ItemForAccidentStatusID";
            this.ItemForAccidentStatusID.Size = new System.Drawing.Size(389, 24);
            this.ItemForAccidentStatusID.Text = "Accident Status:";
            this.ItemForAccidentStatusID.TextSize = new System.Drawing.Size(127, 13);
            // 
            // ItemForResponsibleManager
            // 
            this.ItemForResponsibleManager.Control = this.ResponsibleManagerButtonEdit;
            this.ItemForResponsibleManager.CustomizationFormText = "Responsible Manager:";
            this.ItemForResponsibleManager.Location = new System.Drawing.Point(0, 71);
            this.ItemForResponsibleManager.Name = "ItemForResponsibleManager";
            this.ItemForResponsibleManager.Size = new System.Drawing.Size(649, 24);
            this.ItemForResponsibleManager.Text = "Responsible Manager:";
            this.ItemForResponsibleManager.TextSize = new System.Drawing.Size(127, 13);
            // 
            // ItemForHSQETeamLeadName
            // 
            this.ItemForHSQETeamLeadName.Control = this.HSQETeamLeadNameButtonEdit;
            this.ItemForHSQETeamLeadName.CustomizationFormText = "HSQE Team Lead:";
            this.ItemForHSQETeamLeadName.Location = new System.Drawing.Point(0, 95);
            this.ItemForHSQETeamLeadName.Name = "ItemForHSQETeamLeadName";
            this.ItemForHSQETeamLeadName.Size = new System.Drawing.Size(649, 24);
            this.ItemForHSQETeamLeadName.Text = "HSQE Team Lead:";
            this.ItemForHSQETeamLeadName.TextSize = new System.Drawing.Size(127, 13);
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp00303_Accident_EditTableAdapter
            // 
            this.sp00303_Accident_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00099_Business_Areas_With_BlankTableAdapter
            // 
            this.sp_HR_00099_Business_Areas_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp00305_Accident_Statuses_With_BlankTableAdapter
            // 
            this.sp00305_Accident_Statuses_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp00306_Accident_Linked_Record_Types_With_BlankTableAdapter
            // 
            this.sp00306_Accident_Linked_Record_Types_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // frm_Core_Accident_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(686, 623);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_Core_Accident_Edit";
            this.Text = "Edit Accident";
            this.Activated += new System.EventHandler(this.frm_Core_Accident_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_Core_Accident_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_Core_Accident_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ReportedByPersonTypeIDParentTexEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00303AccidentEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Accident)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToRecordTypeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00306AccidentLinkedRecordTypesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HSQETeamLeadNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClosedByStaffButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResponsibleManagerButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToRecordFullDescriptionButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportedByPersonButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportedByPersonTypeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentTypeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClosedByStaffIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClosedDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClosedDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TargetClosureDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TargetClosureDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HSQETeamLeadTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResponsibleManagerIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CorrectiveActionRemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SituationRemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientReferenceNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentDescriptionMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportedToHSECheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportedByPersonOtherTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportedByPersonTypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportedByPersonIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentSubTypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BusinessAreaIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00099BusinessAreasWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExternalReferenceNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentStatusIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00305AccidentStatusesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentTypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToRecordIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateReportedToHSEDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateReportedToHSEDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentSubTypeButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForResponsibleManagerID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHSQETeamLead)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClosedByStaffID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccidentTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccidentSubTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccidentID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReportedByPersonID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReportedByPersonTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReportedByPersonTypeIDParent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToRecordID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReportedByPersonOther)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReportedToHSE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExternalReferenceNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientReferenceNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTargetClosureDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReportedByPersonType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReportedByPerson)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToRecordFullDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToRecordTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateReportedToHSE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClosedDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClosedByStaff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccidentType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccidentSubType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccidentDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSituationRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCorrectiveActionRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBusinessAreaID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccidentDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccidentStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForResponsibleManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHSQETeamLeadName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAccidentSubType;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.ButtonEdit AccidentSubTypeButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAccidentID;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraEditors.TextEdit AccidentIDTextEdit;
        private DevExpress.XtraEditors.TextEdit LinkedToRecordIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedToRecordID;
        private DevExpress.XtraEditors.DateEdit AccidentDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAccidentDate;
        private DevExpress.XtraEditors.DateEdit DateReportedToHSEDateEdit;
        private DevExpress.XtraEditors.TextEdit AccidentTypeIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAccidentTypeID;
        private DevExpress.XtraEditors.GridLookUpEdit AccidentStatusIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAccidentStatusID;
        private DevExpress.XtraEditors.TextEdit ExternalReferenceNumberTextEdit;
        private DevExpress.XtraEditors.GridLookUpEdit BusinessAreaIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBusinessAreaID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private System.Windows.Forms.BindingSource sp00303AccidentEditBindingSource;
        private DataSet_Accident dataSet_Accident;
        private DataSet_AccidentTableAdapters.sp00303_Accident_EditTableAdapter sp00303_Accident_EditTableAdapter;
        private DataSet_HR_Core dataSet_HR_Core;
        private System.Windows.Forms.BindingSource spHR00099BusinessAreasWithBlankBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00099_Business_Areas_With_BlankTableAdapter sp_HR_00099_Business_Areas_With_BlankTableAdapter;
        private DevExpress.XtraEditors.TextEdit AccidentSubTypeIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAccidentSubTypeID;
        private System.Windows.Forms.BindingSource sp00305AccidentStatusesWithBlankBindingSource;
        private DataSet_AccidentTableAdapters.sp00305_Accident_Statuses_With_BlankTableAdapter sp00305_Accident_Statuses_With_BlankTableAdapter;
        private DevExpress.XtraEditors.TextEdit ReportedByPersonIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReportedByPersonID;
        private DevExpress.XtraEditors.TextEdit ReportedByPersonTypeIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReportedByPersonTypeID;
        private DevExpress.XtraEditors.TextEdit ReportedByPersonOtherTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReportedByPersonOther;
        private DevExpress.XtraEditors.CheckEdit ReportedToHSECheckEdit;
        private DevExpress.XtraEditors.MemoEdit AccidentDescriptionMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAccidentDescription;
        private DevExpress.XtraEditors.TextEdit ClientReferenceNumberTextEdit;
        private DevExpress.XtraEditors.MemoEdit SituationRemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSituationRemarks;
        private DevExpress.XtraEditors.TextEdit ResponsibleManagerIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForResponsibleManagerID;
        private DevExpress.XtraEditors.TextEdit HSQETeamLeadTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHSQETeamLead;
        private DevExpress.XtraEditors.MemoEdit CorrectiveActionRemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layGrpAddress;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReportedToHSE;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExternalReferenceNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientReferenceNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDateReportedToHSE;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCorrectiveActionRemarks;
        private DevExpress.XtraEditors.DateEdit ClosedDateDateEdit;
        private DevExpress.XtraEditors.DateEdit TargetClosureDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTargetClosureDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClosedDate;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraEditors.TextEdit ClosedByStaffIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClosedByStaffID;
        private DevExpress.XtraEditors.TextEdit AccidentTypeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAccidentType;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraEditors.ButtonEdit ReportedByPersonButtonEdit;
        private DevExpress.XtraEditors.TextEdit ReportedByPersonTypeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReportedByPersonType;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReportedByPerson;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraEditors.ButtonEdit LinkedToRecordFullDescriptionButtonEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup10;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedToRecordFullDescription;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraEditors.TextEdit SiteNameTextEdit;
        private DevExpress.XtraEditors.TextEdit ClientNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteName;
        private DevExpress.XtraEditors.ButtonEdit ResponsibleManagerButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForResponsibleManager;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
        private DevExpress.XtraEditors.ButtonEdit ClosedByStaffButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClosedByStaff;
        private DevExpress.XtraEditors.ButtonEdit HSQETeamLeadNameButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHSQETeamLeadName;
        private DevExpress.XtraEditors.GridLookUpEdit LinkedToRecordTypeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedToRecordTypeID;
        private System.Windows.Forms.BindingSource sp00306AccidentLinkedRecordTypesWithBlankBindingSource;
        private DataSet_AccidentTableAdapters.sp00306_Accident_Linked_Record_Types_With_BlankTableAdapter sp00306_Accident_Linked_Record_Types_With_BlankTableAdapter;
        private DevExpress.XtraEditors.TextEdit ReportedByPersonTypeIDParentTexEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReportedByPersonTypeIDParent;
        private DevExpress.XtraBars.BarButtonItem bbiLinkedDocuments;
    }
}

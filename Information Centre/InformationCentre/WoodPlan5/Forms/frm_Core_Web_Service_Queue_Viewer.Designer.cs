namespace WoodPlan5
{
    partial class frm_Core_Web_Service_Queue_Viewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Core_Web_Service_Queue_Viewer));
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            this.colStatusID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.popupContainerControlStatusFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnStatusFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp01023CoreWebServiceQueueViewerStatusListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_Common_Functionality = new WoodPlan5.DataSet_Common_Functionality();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colOrder1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerControlRecordTypeFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnRecordTypeFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp01022CoreWebServiceQueueViewerRecordTypeListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerControlModuleFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnModuleFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp01021CoreWebServiceQueueViewerModuleListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colModuleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModuleName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp01024CoreWebServiceQueueItemsListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colQueueID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSystemID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colData = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colErrorCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateAdded1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSystemDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colImageCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelected = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colCalculatedStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.splitContainerControl3 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp01025CoreWebServiceQueueItemsForQueueRecordBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colQueueItemID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQueueID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNumericValue1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordTypeDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelected1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl39 = new DevExpress.XtraGrid.GridControl();
            this.sp01026CoreWebServiceQueueImagesForQueueRecordBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView39 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colImageID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQueueID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelected2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemImageEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageEdit();
            this.pictureEditPreview = new DevExpress.XtraEditors.PictureEdit();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.checkEditShowErrorsOnly = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.btnClearAllFilters = new DevExpress.XtraEditors.SimpleButton();
            this.memoExEditValue = new DevExpress.XtraEditors.MemoExEdit();
            this.popupContainerEditStatusFilter = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerEditRecordTypeFilter = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerEditModuleFilter = new DevExpress.XtraEditors.PopupContainerEdit();
            this.btnLoad = new DevExpress.XtraEditors.SimpleButton();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForModuleFilter = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRecordTypeFilter = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStatusFilter = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForClearFilters = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.hideContainerLeft = new DevExpress.XtraBars.Docking.AutoHideContainer();
            this.dockPanelFilters = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bciFilterData = new DevExpress.XtraBars.BarCheckItem();
            this.bsiSelectedCount = new DevExpress.XtraBars.BarStaticItem();
            this.sp01021_Core_Web_Service_Queue_Viewer_Module_ListTableAdapter = new WoodPlan5.DataSet_Common_FunctionalityTableAdapters.sp01021_Core_Web_Service_Queue_Viewer_Module_ListTableAdapter();
            this.sp01022_Core_Web_Service_Queue_Viewer_RecordType_ListTableAdapter = new WoodPlan5.DataSet_Common_FunctionalityTableAdapters.sp01022_Core_Web_Service_Queue_Viewer_RecordType_ListTableAdapter();
            this.sp01023_Core_Web_Service_Queue_Viewer_Status_ListTableAdapter = new WoodPlan5.DataSet_Common_FunctionalityTableAdapters.sp01023_Core_Web_Service_Queue_Viewer_Status_ListTableAdapter();
            this.sp01024_Core_Web_Service_Queue_Items_ListTableAdapter = new WoodPlan5.DataSet_Common_FunctionalityTableAdapters.sp01024_Core_Web_Service_Queue_Items_ListTableAdapter();
            this.sp01025_Core_Web_Service_Queue_Items_For_Queue_RecordTableAdapter = new WoodPlan5.DataSet_Common_FunctionalityTableAdapters.sp01025_Core_Web_Service_Queue_Items_For_Queue_RecordTableAdapter();
            this.sp01026_Core_Web_Service_Queue_Images_For_Queue_RecordTableAdapter = new WoodPlan5.DataSet_Common_FunctionalityTableAdapters.sp01026_Core_Web_Service_Queue_Images_For_Queue_RecordTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlStatusFilter)).BeginInit();
            this.popupContainerControlStatusFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01023CoreWebServiceQueueViewerStatusListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Common_Functionality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlRecordTypeFilter)).BeginInit();
            this.popupContainerControlRecordTypeFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01022CoreWebServiceQueueViewerRecordTypeListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlModuleFilter)).BeginInit();
            this.popupContainerControlModuleFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01021CoreWebServiceQueueViewerModuleListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01024CoreWebServiceQueueItemsListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).BeginInit();
            this.splitContainerControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01025CoreWebServiceQueueItemsForQueueRecordBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01026CoreWebServiceQueueImagesForQueueRecordBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEditPreview.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditShowErrorsOnly.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEditValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditStatusFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditRecordTypeFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditModuleFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForModuleFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRecordTypeFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClearFilters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.hideContainerLeft.SuspendLayout();
            this.dockPanelFilters.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1060, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 659);
            this.barDockControlBottom.Size = new System.Drawing.Size(1060, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 659);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1060, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 659);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // pmEditContextMenu
            // 
            this.pmEditContextMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiUndo),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRedo),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCut, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCopy),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiPaste),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSelectAll, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiClear),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSpellChecker, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiGrammarCheck)});
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.standaloneBarDockControl1);
            this.barManager1.DockManager = this.dockManager1;
            this.barManager1.Images = this.imageCollection1;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiRefresh,
            this.bciFilterData,
            this.bsiSelectedCount});
            this.barManager1.MaxItemId = 32;
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colStatusID1
            // 
            this.colStatusID1.Caption = "Status ID";
            this.colStatusID1.FieldName = "StatusID";
            this.colStatusID1.Name = "colStatusID1";
            this.colStatusID1.OptionsColumn.AllowEdit = false;
            this.colStatusID1.OptionsColumn.AllowFocus = false;
            this.colStatusID1.OptionsColumn.ReadOnly = true;
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(23, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.popupContainerControlStatusFilter);
            this.splitContainerControl2.Panel1.Controls.Add(this.popupContainerControlRecordTypeFilter);
            this.splitContainerControl2.Panel1.Controls.Add(this.popupContainerControlModuleFilter);
            this.splitContainerControl2.Panel1.Controls.Add(this.standaloneBarDockControl1);
            this.splitContainerControl2.Panel1.Controls.Add(this.gridControl1);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.splitContainerControl3);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(1037, 659);
            this.splitContainerControl2.SplitterPosition = 453;
            this.splitContainerControl2.TabIndex = 2;
            this.splitContainerControl2.Text = "splitContainerControl2";
            this.splitContainerControl2.SplitGroupPanelCollapsed += new DevExpress.XtraEditors.SplitGroupPanelCollapsedEventHandler(this.splitContainerControl2_SplitGroupPanelCollapsed);
            // 
            // popupContainerControlStatusFilter
            // 
            this.popupContainerControlStatusFilter.Controls.Add(this.btnStatusFilterOK);
            this.popupContainerControlStatusFilter.Controls.Add(this.gridControl5);
            this.popupContainerControlStatusFilter.Location = new System.Drawing.Point(299, 161);
            this.popupContainerControlStatusFilter.Name = "popupContainerControlStatusFilter";
            this.popupContainerControlStatusFilter.Size = new System.Drawing.Size(142, 120);
            this.popupContainerControlStatusFilter.TabIndex = 25;
            // 
            // btnStatusFilterOK
            // 
            this.btnStatusFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnStatusFilterOK.Location = new System.Drawing.Point(3, 97);
            this.btnStatusFilterOK.Name = "btnStatusFilterOK";
            this.btnStatusFilterOK.Size = new System.Drawing.Size(50, 20);
            this.btnStatusFilterOK.TabIndex = 18;
            this.btnStatusFilterOK.Text = "OK";
            this.btnStatusFilterOK.Click += new System.EventHandler(this.btnStatusFilterOK_Click);
            // 
            // gridControl5
            // 
            this.gridControl5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl5.DataSource = this.sp01023CoreWebServiceQueueViewerStatusListBindingSource;
            this.gridControl5.Location = new System.Drawing.Point(3, 3);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.Size = new System.Drawing.Size(136, 92);
            this.gridControl5.TabIndex = 1;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp01023CoreWebServiceQueueViewerStatusListBindingSource
            // 
            this.sp01023CoreWebServiceQueueViewerStatusListBindingSource.DataMember = "sp01023_Core_Web_Service_Queue_Viewer_Status_List";
            this.sp01023CoreWebServiceQueueViewerStatusListBindingSource.DataSource = this.dataSet_Common_Functionality;
            // 
            // dataSet_Common_Functionality
            // 
            this.dataSet_Common_Functionality.DataSetName = "DataSet_Common_Functionality";
            this.dataSet_Common_Functionality.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colOrder1,
            this.colStatus,
            this.colStatusID});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsFind.AlwaysVisible = true;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder1, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colOrder1
            // 
            this.colOrder1.Caption = "Order";
            this.colOrder1.FieldName = "Order";
            this.colOrder1.Name = "colOrder1";
            this.colOrder1.OptionsColumn.AllowEdit = false;
            this.colOrder1.OptionsColumn.AllowFocus = false;
            this.colOrder1.OptionsColumn.ReadOnly = true;
            // 
            // colStatus
            // 
            this.colStatus.Caption = "Status";
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.OptionsColumn.AllowEdit = false;
            this.colStatus.OptionsColumn.AllowFocus = false;
            this.colStatus.OptionsColumn.ReadOnly = true;
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 0;
            this.colStatus.Width = 201;
            // 
            // colStatusID
            // 
            this.colStatusID.Caption = "Status ID";
            this.colStatusID.FieldName = "StatusID";
            this.colStatusID.Name = "colStatusID";
            this.colStatusID.OptionsColumn.AllowEdit = false;
            this.colStatusID.OptionsColumn.AllowFocus = false;
            this.colStatusID.OptionsColumn.ReadOnly = true;
            // 
            // popupContainerControlRecordTypeFilter
            // 
            this.popupContainerControlRecordTypeFilter.Controls.Add(this.btnRecordTypeFilterOK);
            this.popupContainerControlRecordTypeFilter.Controls.Add(this.gridControl2);
            this.popupContainerControlRecordTypeFilter.Location = new System.Drawing.Point(152, 161);
            this.popupContainerControlRecordTypeFilter.Name = "popupContainerControlRecordTypeFilter";
            this.popupContainerControlRecordTypeFilter.Size = new System.Drawing.Size(141, 120);
            this.popupContainerControlRecordTypeFilter.TabIndex = 24;
            // 
            // btnRecordTypeFilterOK
            // 
            this.btnRecordTypeFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRecordTypeFilterOK.Location = new System.Drawing.Point(3, 97);
            this.btnRecordTypeFilterOK.Name = "btnRecordTypeFilterOK";
            this.btnRecordTypeFilterOK.Size = new System.Drawing.Size(50, 20);
            this.btnRecordTypeFilterOK.TabIndex = 18;
            this.btnRecordTypeFilterOK.Text = "OK";
            this.btnRecordTypeFilterOK.Click += new System.EventHandler(this.btnRecordTypeFilterOK_Click);
            // 
            // gridControl2
            // 
            this.gridControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl2.DataSource = this.sp01022CoreWebServiceQueueViewerRecordTypeListBindingSource;
            this.gridControl2.Location = new System.Drawing.Point(3, 3);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(135, 92);
            this.gridControl2.TabIndex = 1;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp01022CoreWebServiceQueueViewerRecordTypeListBindingSource
            // 
            this.sp01022CoreWebServiceQueueViewerRecordTypeListBindingSource.DataMember = "sp01022_Core_Web_Service_Queue_Viewer_RecordType_List";
            this.sp01022CoreWebServiceQueueViewerRecordTypeListBindingSource.DataSource = this.dataSet_Common_Functionality;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.colRecordType,
            this.colRecordTypeID});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupCount = 1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsFind.AlwaysVisible = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn5, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn6, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Module ID";
            this.gridColumn4.FieldName = "ModuleID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Module Name";
            this.gridColumn5.FieldName = "ModuleName";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 179;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Order";
            this.gridColumn6.FieldName = "Order";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            // 
            // colRecordType
            // 
            this.colRecordType.Caption = "Record Type";
            this.colRecordType.FieldName = "RecordType";
            this.colRecordType.Name = "colRecordType";
            this.colRecordType.OptionsColumn.AllowEdit = false;
            this.colRecordType.OptionsColumn.AllowFocus = false;
            this.colRecordType.OptionsColumn.ReadOnly = true;
            this.colRecordType.Visible = true;
            this.colRecordType.VisibleIndex = 0;
            this.colRecordType.Width = 228;
            // 
            // colRecordTypeID
            // 
            this.colRecordTypeID.Caption = "Record Type ID";
            this.colRecordTypeID.FieldName = "RecordTypeID";
            this.colRecordTypeID.Name = "colRecordTypeID";
            this.colRecordTypeID.OptionsColumn.AllowEdit = false;
            this.colRecordTypeID.OptionsColumn.AllowFocus = false;
            this.colRecordTypeID.OptionsColumn.ReadOnly = true;
            this.colRecordTypeID.Width = 116;
            // 
            // popupContainerControlModuleFilter
            // 
            this.popupContainerControlModuleFilter.Controls.Add(this.btnModuleFilterOK);
            this.popupContainerControlModuleFilter.Controls.Add(this.gridControl3);
            this.popupContainerControlModuleFilter.Location = new System.Drawing.Point(6, 161);
            this.popupContainerControlModuleFilter.Name = "popupContainerControlModuleFilter";
            this.popupContainerControlModuleFilter.Size = new System.Drawing.Size(140, 120);
            this.popupContainerControlModuleFilter.TabIndex = 22;
            // 
            // btnModuleFilterOK
            // 
            this.btnModuleFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnModuleFilterOK.Location = new System.Drawing.Point(3, 97);
            this.btnModuleFilterOK.Name = "btnModuleFilterOK";
            this.btnModuleFilterOK.Size = new System.Drawing.Size(50, 20);
            this.btnModuleFilterOK.TabIndex = 18;
            this.btnModuleFilterOK.Text = "OK";
            this.btnModuleFilterOK.Click += new System.EventHandler(this.btnModuleFilterOK_Click);
            // 
            // gridControl3
            // 
            this.gridControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl3.DataSource = this.sp01021CoreWebServiceQueueViewerModuleListBindingSource;
            this.gridControl3.Location = new System.Drawing.Point(3, 3);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.Size = new System.Drawing.Size(134, 92);
            this.gridControl3.TabIndex = 1;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp01021CoreWebServiceQueueViewerModuleListBindingSource
            // 
            this.sp01021CoreWebServiceQueueViewerModuleListBindingSource.DataMember = "sp01021_Core_Web_Service_Queue_Viewer_Module_List";
            this.sp01021CoreWebServiceQueueViewerModuleListBindingSource.DataSource = this.dataSet_Common_Functionality;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colModuleID,
            this.colModuleName,
            this.colOrder});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsFind.AlwaysVisible = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colModuleID
            // 
            this.colModuleID.Caption = "Module ID";
            this.colModuleID.FieldName = "ModuleID";
            this.colModuleID.Name = "colModuleID";
            this.colModuleID.OptionsColumn.AllowEdit = false;
            this.colModuleID.OptionsColumn.AllowFocus = false;
            this.colModuleID.OptionsColumn.ReadOnly = true;
            // 
            // colModuleName
            // 
            this.colModuleName.Caption = "Module Name";
            this.colModuleName.FieldName = "ModuleName";
            this.colModuleName.Name = "colModuleName";
            this.colModuleName.OptionsColumn.AllowEdit = false;
            this.colModuleName.OptionsColumn.AllowFocus = false;
            this.colModuleName.OptionsColumn.ReadOnly = true;
            this.colModuleName.Visible = true;
            this.colModuleName.VisibleIndex = 0;
            this.colModuleName.Width = 179;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "Order";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl1.Manager = this.barManager1;
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(1037, 42);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.DataSource = this.sp01024CoreWebServiceQueueItemsListBindingSource;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "View Data Column in Viewer", "json viewer"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Check Selected Record(s)", "check")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 42);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEditDateTime2});
            this.gridControl1.Size = new System.Drawing.Size(1037, 411);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp01024CoreWebServiceQueueItemsListBindingSource
            // 
            this.sp01024CoreWebServiceQueueItemsListBindingSource.DataMember = "sp01024_Core_Web_Service_Queue_Items_List";
            this.sp01024CoreWebServiceQueueItemsListBindingSource.DataSource = this.dataSet_Common_Functionality;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Preview_16x16, "Preview_16x16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "Preview_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.refresh_16x16, "refresh_16x16", typeof(global::WoodPlan5.Properties.Resources), 5);
            this.imageCollection1.Images.SetKeyName(5, "refresh_16x16");
            this.imageCollection1.InsertGalleryImage("autoexpand_16x16.png", "images/filter%20elements/autoexpand_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/filter%20elements/autoexpand_16x16.png"), 6);
            this.imageCollection1.Images.SetKeyName(6, "autoexpand_16x16.png");
            this.imageCollection1.InsertGalleryImage("question_16x16.png", "images/support/question_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/support/question_16x16.png"), 7);
            this.imageCollection1.Images.SetKeyName(7, "question_16x16.png");
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colQueueID,
            this.colSystemID,
            this.colRecordID,
            this.colTypeID,
            this.colData,
            this.colStatusID1,
            this.colErrorCount,
            this.colDateAdded1,
            this.colSystemDescription,
            this.colStatusDescription,
            this.colRecordTypeDescription,
            this.colItemCount,
            this.colImageCount,
            this.colSelected,
            this.colCalculatedStatus});
            this.gridView1.CustomizationFormBounds = new System.Drawing.Rectangle(1392, 518, 208, 191);
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.colStatusID1;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Gray;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 1;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView1.FormatRules.Add(gridFormatRule1);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSystemDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateAdded1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colQueueID
            // 
            this.colQueueID.Caption = "Queue ID";
            this.colQueueID.FieldName = "QueueID";
            this.colQueueID.Name = "colQueueID";
            this.colQueueID.OptionsColumn.AllowEdit = false;
            this.colQueueID.OptionsColumn.AllowFocus = false;
            this.colQueueID.OptionsColumn.ReadOnly = true;
            this.colQueueID.Visible = true;
            this.colQueueID.VisibleIndex = 1;
            this.colQueueID.Width = 77;
            // 
            // colSystemID
            // 
            this.colSystemID.Caption = "Module ID";
            this.colSystemID.FieldName = "SystemID";
            this.colSystemID.Name = "colSystemID";
            this.colSystemID.OptionsColumn.AllowEdit = false;
            this.colSystemID.OptionsColumn.AllowFocus = false;
            this.colSystemID.OptionsColumn.ReadOnly = true;
            // 
            // colRecordID
            // 
            this.colRecordID.Caption = "Record ID";
            this.colRecordID.FieldName = "RecordID";
            this.colRecordID.Name = "colRecordID";
            this.colRecordID.OptionsColumn.AllowEdit = false;
            this.colRecordID.OptionsColumn.AllowFocus = false;
            this.colRecordID.OptionsColumn.ReadOnly = true;
            // 
            // colTypeID
            // 
            this.colTypeID.Caption = "Type ID";
            this.colTypeID.FieldName = "TypeID";
            this.colTypeID.Name = "colTypeID";
            this.colTypeID.OptionsColumn.AllowEdit = false;
            this.colTypeID.OptionsColumn.AllowFocus = false;
            this.colTypeID.OptionsColumn.ReadOnly = true;
            // 
            // colData
            // 
            this.colData.Caption = "Data";
            this.colData.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colData.FieldName = "Data";
            this.colData.Name = "colData";
            this.colData.OptionsColumn.ReadOnly = true;
            this.colData.Visible = true;
            this.colData.VisibleIndex = 5;
            this.colData.Width = 376;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colErrorCount
            // 
            this.colErrorCount.Caption = "Error Count";
            this.colErrorCount.FieldName = "ErrorCount";
            this.colErrorCount.Name = "colErrorCount";
            this.colErrorCount.OptionsColumn.AllowEdit = false;
            this.colErrorCount.OptionsColumn.AllowFocus = false;
            this.colErrorCount.OptionsColumn.ReadOnly = true;
            this.colErrorCount.Visible = true;
            this.colErrorCount.VisibleIndex = 4;
            // 
            // colDateAdded1
            // 
            this.colDateAdded1.Caption = "Date Added";
            this.colDateAdded1.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colDateAdded1.FieldName = "DateAdded";
            this.colDateAdded1.Name = "colDateAdded1";
            this.colDateAdded1.OptionsColumn.AllowEdit = false;
            this.colDateAdded1.OptionsColumn.AllowFocus = false;
            this.colDateAdded1.OptionsColumn.ReadOnly = true;
            this.colDateAdded1.Visible = true;
            this.colDateAdded1.VisibleIndex = 0;
            this.colDateAdded1.Width = 155;
            // 
            // repositoryItemTextEditDateTime2
            // 
            this.repositoryItemTextEditDateTime2.AutoHeight = false;
            this.repositoryItemTextEditDateTime2.Mask.EditMask = "dd/MM/yyyy HH:mm:ss.fff";
            this.repositoryItemTextEditDateTime2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime2.Name = "repositoryItemTextEditDateTime2";
            // 
            // colSystemDescription
            // 
            this.colSystemDescription.Caption = "Module";
            this.colSystemDescription.FieldName = "SystemDescription";
            this.colSystemDescription.Name = "colSystemDescription";
            this.colSystemDescription.OptionsColumn.AllowEdit = false;
            this.colSystemDescription.OptionsColumn.AllowFocus = false;
            this.colSystemDescription.OptionsColumn.ReadOnly = true;
            this.colSystemDescription.Visible = true;
            this.colSystemDescription.VisibleIndex = 2;
            this.colSystemDescription.Width = 126;
            // 
            // colStatusDescription
            // 
            this.colStatusDescription.Caption = "Status";
            this.colStatusDescription.FieldName = "StatusDescription";
            this.colStatusDescription.Name = "colStatusDescription";
            this.colStatusDescription.OptionsColumn.AllowEdit = false;
            this.colStatusDescription.OptionsColumn.AllowFocus = false;
            this.colStatusDescription.OptionsColumn.ReadOnly = true;
            this.colStatusDescription.Visible = true;
            this.colStatusDescription.VisibleIndex = 3;
            // 
            // colRecordTypeDescription
            // 
            this.colRecordTypeDescription.Caption = "Record Type";
            this.colRecordTypeDescription.FieldName = "RecordTypeDescription";
            this.colRecordTypeDescription.Name = "colRecordTypeDescription";
            this.colRecordTypeDescription.OptionsColumn.AllowEdit = false;
            this.colRecordTypeDescription.OptionsColumn.AllowFocus = false;
            this.colRecordTypeDescription.OptionsColumn.ReadOnly = true;
            this.colRecordTypeDescription.Visible = true;
            this.colRecordTypeDescription.VisibleIndex = 2;
            this.colRecordTypeDescription.Width = 107;
            // 
            // colItemCount
            // 
            this.colItemCount.Caption = "Linked Items";
            this.colItemCount.FieldName = "ItemCount";
            this.colItemCount.Name = "colItemCount";
            this.colItemCount.OptionsColumn.AllowEdit = false;
            this.colItemCount.OptionsColumn.AllowFocus = false;
            this.colItemCount.OptionsColumn.ReadOnly = true;
            this.colItemCount.Visible = true;
            this.colItemCount.VisibleIndex = 6;
            // 
            // colImageCount
            // 
            this.colImageCount.Caption = "Linked Images";
            this.colImageCount.FieldName = "ImageCount";
            this.colImageCount.Name = "colImageCount";
            this.colImageCount.OptionsColumn.AllowEdit = false;
            this.colImageCount.OptionsColumn.AllowFocus = false;
            this.colImageCount.OptionsColumn.ReadOnly = true;
            this.colImageCount.Visible = true;
            this.colImageCount.VisibleIndex = 7;
            this.colImageCount.Width = 87;
            // 
            // colSelected
            // 
            this.colSelected.Caption = "Selected";
            this.colSelected.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSelected.FieldName = "Selected";
            this.colSelected.Name = "colSelected";
            this.colSelected.OptionsColumn.ReadOnly = true;
            this.colSelected.Width = 60;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colCalculatedStatus
            // 
            this.colCalculatedStatus.Caption = "Status";
            this.colCalculatedStatus.FieldName = "CalculatedStatus";
            this.colCalculatedStatus.Name = "colCalculatedStatus";
            this.colCalculatedStatus.OptionsColumn.ReadOnly = true;
            this.colCalculatedStatus.Visible = true;
            this.colCalculatedStatus.VisibleIndex = 8;
            this.colCalculatedStatus.Width = 263;
            // 
            // splitContainerControl3
            // 
            this.splitContainerControl3.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl3.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl3.Name = "splitContainerControl3";
            this.splitContainerControl3.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl3.Panel1.Controls.Add(this.gridControl4);
            this.splitContainerControl3.Panel1.ShowCaption = true;
            this.splitContainerControl3.Panel1.Text = "Linked Items";
            this.splitContainerControl3.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl3.Panel2.Controls.Add(this.splitContainerControl1);
            this.splitContainerControl3.ShowCaption = true;
            this.splitContainerControl3.Size = new System.Drawing.Size(1037, 200);
            this.splitContainerControl3.SplitterPosition = 622;
            this.splitContainerControl3.TabIndex = 1;
            this.splitContainerControl3.Text = "splitContainerControl3";
            // 
            // gridControl4
            // 
            this.gridControl4.DataSource = this.sp01025CoreWebServiceQueueItemsForQueueRecordBindingSource;
            this.gridControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl4.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl4.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl4_EmbeddedNavigator_ButtonClick);
            this.gridControl4.Location = new System.Drawing.Point(0, 0);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit2});
            this.gridControl4.Size = new System.Drawing.Size(618, 176);
            this.gridControl4.TabIndex = 0;
            this.gridControl4.UseEmbeddedNavigator = true;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp01025CoreWebServiceQueueItemsForQueueRecordBindingSource
            // 
            this.sp01025CoreWebServiceQueueItemsForQueueRecordBindingSource.DataMember = "sp01025_Core_Web_Service_Queue_Items_For Queue_Record";
            this.sp01025CoreWebServiceQueueItemsForQueueRecordBindingSource.DataSource = this.dataSet_Common_Functionality;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colQueueItemID,
            this.colQueueID1,
            this.colRecordTypeID1,
            this.colRecordID1,
            this.colNumericValue1,
            this.colStatusID2,
            this.colRecordTypeDescription1,
            this.colSelected1});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.GroupCount = 1;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.MultiSelect = true;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colQueueID1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colQueueItemID, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView4.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView4.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView4.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView4.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView4.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView4_MouseUp);
            this.gridView4.DoubleClick += new System.EventHandler(this.gridView4_DoubleClick);
            this.gridView4.GotFocus += new System.EventHandler(this.gridView4_GotFocus);
            // 
            // colQueueItemID
            // 
            this.colQueueItemID.Caption = "Queue Item ID";
            this.colQueueItemID.FieldName = "QueueItemID";
            this.colQueueItemID.Name = "colQueueItemID";
            this.colQueueItemID.OptionsColumn.AllowEdit = false;
            this.colQueueItemID.OptionsColumn.AllowFocus = false;
            this.colQueueItemID.OptionsColumn.ReadOnly = true;
            this.colQueueItemID.Visible = true;
            this.colQueueItemID.VisibleIndex = 0;
            this.colQueueItemID.Width = 103;
            // 
            // colQueueID1
            // 
            this.colQueueID1.Caption = "Queue ID";
            this.colQueueID1.FieldName = "QueueID";
            this.colQueueID1.Name = "colQueueID1";
            this.colQueueID1.OptionsColumn.AllowEdit = false;
            this.colQueueID1.OptionsColumn.AllowFocus = false;
            this.colQueueID1.OptionsColumn.ReadOnly = true;
            this.colQueueID1.Visible = true;
            this.colQueueID1.VisibleIndex = 1;
            this.colQueueID1.Width = 115;
            // 
            // colRecordTypeID1
            // 
            this.colRecordTypeID1.Caption = "Record Type ID";
            this.colRecordTypeID1.FieldName = "RecordTypeID";
            this.colRecordTypeID1.Name = "colRecordTypeID1";
            this.colRecordTypeID1.OptionsColumn.AllowEdit = false;
            this.colRecordTypeID1.OptionsColumn.AllowFocus = false;
            this.colRecordTypeID1.OptionsColumn.ReadOnly = true;
            this.colRecordTypeID1.Width = 94;
            // 
            // colRecordID1
            // 
            this.colRecordID1.Caption = "Record ID";
            this.colRecordID1.FieldName = "RecordID";
            this.colRecordID1.Name = "colRecordID1";
            this.colRecordID1.OptionsColumn.AllowEdit = false;
            this.colRecordID1.OptionsColumn.AllowFocus = false;
            this.colRecordID1.OptionsColumn.ReadOnly = true;
            this.colRecordID1.Visible = true;
            this.colRecordID1.VisibleIndex = 2;
            this.colRecordID1.Width = 116;
            // 
            // colNumericValue1
            // 
            this.colNumericValue1.Caption = "Numeric Value 1";
            this.colNumericValue1.FieldName = "NumericValue1";
            this.colNumericValue1.Name = "colNumericValue1";
            this.colNumericValue1.OptionsColumn.AllowEdit = false;
            this.colNumericValue1.OptionsColumn.AllowFocus = false;
            this.colNumericValue1.OptionsColumn.ReadOnly = true;
            this.colNumericValue1.Visible = true;
            this.colNumericValue1.VisibleIndex = 3;
            this.colNumericValue1.Width = 106;
            // 
            // colStatusID2
            // 
            this.colStatusID2.Caption = "Status ID";
            this.colStatusID2.FieldName = "StatusID";
            this.colStatusID2.Name = "colStatusID2";
            this.colStatusID2.OptionsColumn.AllowEdit = false;
            this.colStatusID2.OptionsColumn.AllowFocus = false;
            this.colStatusID2.OptionsColumn.ReadOnly = true;
            this.colStatusID2.Visible = true;
            this.colStatusID2.VisibleIndex = 4;
            this.colStatusID2.Width = 95;
            // 
            // colRecordTypeDescription1
            // 
            this.colRecordTypeDescription1.Caption = "Record Type";
            this.colRecordTypeDescription1.FieldName = "RecordTypeDescription";
            this.colRecordTypeDescription1.Name = "colRecordTypeDescription1";
            this.colRecordTypeDescription1.OptionsColumn.AllowEdit = false;
            this.colRecordTypeDescription1.OptionsColumn.AllowFocus = false;
            this.colRecordTypeDescription1.OptionsColumn.ReadOnly = true;
            this.colRecordTypeDescription1.Visible = true;
            this.colRecordTypeDescription1.VisibleIndex = 1;
            this.colRecordTypeDescription1.Width = 146;
            // 
            // colSelected1
            // 
            this.colSelected1.Caption = "Selected";
            this.colSelected1.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colSelected1.FieldName = "Selected";
            this.colSelected1.Name = "colSelected1";
            this.colSelected1.OptionsColumn.ReadOnly = true;
            this.colSelected1.Width = 60;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.Controls.Add(this.gridControl39);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Linked Images";
            this.splitContainerControl1.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl1.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel2.Controls.Add(this.pictureEditPreview);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "Image Preview";
            this.splitContainerControl1.Size = new System.Drawing.Size(405, 196);
            this.splitContainerControl1.SplitterPosition = 204;
            this.splitContainerControl1.TabIndex = 84;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gridControl39
            // 
            this.gridControl39.DataSource = this.sp01026CoreWebServiceQueueImagesForQueueRecordBindingSource;
            this.gridControl39.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl39.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl39.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl39.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl39.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl39.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl39.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl39.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl39.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl39.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl39.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl39.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl39.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl39.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl39_EmbeddedNavigator_ButtonClick);
            this.gridControl39.Location = new System.Drawing.Point(0, 0);
            this.gridControl39.MainView = this.gridView39;
            this.gridControl39.MenuManager = this.barManager1;
            this.gridControl39.Name = "gridControl39";
            this.gridControl39.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit3,
            this.repositoryItemImageEdit1});
            this.gridControl39.Size = new System.Drawing.Size(200, 172);
            this.gridControl39.TabIndex = 83;
            this.gridControl39.UseEmbeddedNavigator = true;
            this.gridControl39.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView39});
            // 
            // sp01026CoreWebServiceQueueImagesForQueueRecordBindingSource
            // 
            this.sp01026CoreWebServiceQueueImagesForQueueRecordBindingSource.DataMember = "sp01026_Core_Web_Service_Queue_Images_For_Queue_Record";
            this.sp01026CoreWebServiceQueueImagesForQueueRecordBindingSource.DataSource = this.dataSet_Common_Functionality;
            // 
            // gridView39
            // 
            this.gridView39.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colImageID,
            this.colQueueID2,
            this.colSelected2});
            this.gridView39.GridControl = this.gridControl39;
            this.gridView39.Name = "gridView39";
            this.gridView39.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView39.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView39.OptionsLayout.StoreAppearance = true;
            this.gridView39.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView39.OptionsSelection.MultiSelect = true;
            this.gridView39.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView39.OptionsView.ColumnAutoWidth = false;
            this.gridView39.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView39.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView39.OptionsView.ShowGroupPanel = false;
            this.gridView39.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colImageID, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView39.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView39_CustomDrawCell);
            this.gridView39.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView39.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView39.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView39.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView39.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView39.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView39_MouseUp);
            this.gridView39.DoubleClick += new System.EventHandler(this.gridView39_DoubleClick);
            this.gridView39.GotFocus += new System.EventHandler(this.gridView39_GotFocus);
            // 
            // colImageID
            // 
            this.colImageID.Caption = "Image ID";
            this.colImageID.FieldName = "ImageID";
            this.colImageID.Name = "colImageID";
            this.colImageID.OptionsColumn.AllowEdit = false;
            this.colImageID.OptionsColumn.AllowFocus = false;
            this.colImageID.OptionsColumn.ReadOnly = true;
            this.colImageID.Visible = true;
            this.colImageID.VisibleIndex = 1;
            this.colImageID.Width = 73;
            // 
            // colQueueID2
            // 
            this.colQueueID2.Caption = "Queue ID";
            this.colQueueID2.FieldName = "QueueID";
            this.colQueueID2.Name = "colQueueID2";
            this.colQueueID2.OptionsColumn.AllowEdit = false;
            this.colQueueID2.OptionsColumn.AllowFocus = false;
            this.colQueueID2.OptionsColumn.ReadOnly = true;
            this.colQueueID2.Visible = true;
            this.colQueueID2.VisibleIndex = 0;
            this.colQueueID2.Width = 78;
            // 
            // colSelected2
            // 
            this.colSelected2.Caption = "Selected";
            this.colSelected2.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colSelected2.FieldName = "Selected";
            this.colSelected2.Name = "colSelected2";
            this.colSelected2.OptionsColumn.AllowEdit = false;
            this.colSelected2.OptionsColumn.AllowFocus = false;
            this.colSelected2.OptionsColumn.ReadOnly = true;
            this.colSelected2.Width = 60;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = 1;
            this.repositoryItemCheckEdit3.ValueUnchecked = 0;
            // 
            // repositoryItemImageEdit1
            // 
            this.repositoryItemImageEdit1.AutoHeight = false;
            this.repositoryItemImageEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageEdit1.Name = "repositoryItemImageEdit1";
            this.repositoryItemImageEdit1.ReadOnly = true;
            // 
            // pictureEditPreview
            // 
            this.pictureEditPreview.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEditPreview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureEditPreview.Location = new System.Drawing.Point(0, 0);
            this.pictureEditPreview.MenuManager = this.barManager1;
            this.pictureEditPreview.Name = "pictureEditPreview";
            this.pictureEditPreview.Properties.AllowScrollViaMouseDrag = true;
            this.pictureEditPreview.Properties.ShowScrollBars = true;
            this.pictureEditPreview.Properties.ShowZoomSubMenu = DevExpress.Utils.DefaultBoolean.True;
            this.pictureEditPreview.Properties.ZoomingOperationMode = DevExpress.XtraEditors.Repository.ZoomingOperationMode.ControlMouseWheel;
            this.pictureEditPreview.Size = new System.Drawing.Size(191, 172);
            this.pictureEditPreview.TabIndex = 0;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // checkEditShowErrorsOnly
            // 
            this.checkEditShowErrorsOnly.Location = new System.Drawing.Point(75, 79);
            this.checkEditShowErrorsOnly.MenuManager = this.barManager1;
            this.checkEditShowErrorsOnly.Name = "checkEditShowErrorsOnly";
            this.checkEditShowErrorsOnly.Properties.Caption = "[Tick if Yes]";
            this.checkEditShowErrorsOnly.Properties.ValueChecked = 1;
            this.checkEditShowErrorsOnly.Properties.ValueUnchecked = 0;
            this.checkEditShowErrorsOnly.Size = new System.Drawing.Size(246, 19);
            this.checkEditShowErrorsOnly.StyleController = this.layoutControl2;
            this.checkEditShowErrorsOnly.TabIndex = 8;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.btnClearAllFilters);
            this.layoutControl2.Controls.Add(this.memoExEditValue);
            this.layoutControl2.Controls.Add(this.popupContainerEditStatusFilter);
            this.layoutControl2.Controls.Add(this.popupContainerEditRecordTypeFilter);
            this.layoutControl2.Controls.Add(this.popupContainerEditModuleFilter);
            this.layoutControl2.Controls.Add(this.btnLoad);
            this.layoutControl2.Controls.Add(this.checkEditShowErrorsOnly);
            this.layoutControl2.Controls.Add(this.dateEditFromDate);
            this.layoutControl2.Controls.Add(this.dateEditToDate);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(853, 456, 544, 350);
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(328, 627);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // btnClearAllFilters
            // 
            this.btnClearAllFilters.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Filter_Clear_16x16;
            this.btnClearAllFilters.Location = new System.Drawing.Point(7, 248);
            this.btnClearAllFilters.Name = "btnClearAllFilters";
            this.btnClearAllFilters.Size = new System.Drawing.Size(90, 22);
            this.btnClearAllFilters.StyleController = this.layoutControl2;
            toolTipTitleItem2.Text = "Clear Filters - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to clear all filters (except Date Range filter).";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.btnClearAllFilters.SuperTip = superToolTip2;
            this.btnClearAllFilters.TabIndex = 23;
            this.btnClearAllFilters.Text = "Clear Filters";
            this.btnClearAllFilters.Click += new System.EventHandler(this.btnClearAllFilters_Click);
            // 
            // memoExEditValue
            // 
            this.memoExEditValue.Location = new System.Drawing.Point(75, 214);
            this.memoExEditValue.MenuManager = this.barManager1;
            this.memoExEditValue.Name = "memoExEditValue";
            this.memoExEditValue.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", "combo", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Clear Filter", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.memoExEditValue.Properties.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.memoExEditValue.Properties.ShowIcon = false;
            this.memoExEditValue.Size = new System.Drawing.Size(246, 20);
            this.memoExEditValue.StyleController = this.layoutControl2;
            toolTipTitleItem3.Text = "Value Filter - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "To search for multiple values, use a comma between each value to search for.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.memoExEditValue.SuperTip = superToolTip3;
            this.memoExEditValue.TabIndex = 24;
            this.memoExEditValue.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.memoExEditValue_ButtonClick);
            // 
            // popupContainerEditStatusFilter
            // 
            this.popupContainerEditStatusFilter.EditValue = "No Status Filter";
            this.popupContainerEditStatusFilter.Location = new System.Drawing.Point(75, 55);
            this.popupContainerEditStatusFilter.MenuManager = this.barManager1;
            this.popupContainerEditStatusFilter.Name = "popupContainerEditStatusFilter";
            this.popupContainerEditStatusFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditStatusFilter.Properties.PopupControl = this.popupContainerControlStatusFilter;
            this.popupContainerEditStatusFilter.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditStatusFilter.Size = new System.Drawing.Size(246, 20);
            this.popupContainerEditStatusFilter.StyleController = this.layoutControl2;
            this.popupContainerEditStatusFilter.TabIndex = 12;
            this.popupContainerEditStatusFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditStatusFilter_QueryResultValue);
            // 
            // popupContainerEditRecordTypeFilter
            // 
            this.popupContainerEditRecordTypeFilter.EditValue = "No Record Type Filter";
            this.popupContainerEditRecordTypeFilter.Location = new System.Drawing.Point(75, 31);
            this.popupContainerEditRecordTypeFilter.MenuManager = this.barManager1;
            this.popupContainerEditRecordTypeFilter.Name = "popupContainerEditRecordTypeFilter";
            this.popupContainerEditRecordTypeFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditRecordTypeFilter.Properties.PopupControl = this.popupContainerControlRecordTypeFilter;
            this.popupContainerEditRecordTypeFilter.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditRecordTypeFilter.Size = new System.Drawing.Size(246, 20);
            this.popupContainerEditRecordTypeFilter.StyleController = this.layoutControl2;
            this.popupContainerEditRecordTypeFilter.TabIndex = 11;
            this.popupContainerEditRecordTypeFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditRecordTypeFilter_QueryResultValue);
            // 
            // popupContainerEditModuleFilter
            // 
            this.popupContainerEditModuleFilter.EditValue = "No Module Filter";
            this.popupContainerEditModuleFilter.Location = new System.Drawing.Point(75, 7);
            this.popupContainerEditModuleFilter.MenuManager = this.barManager1;
            this.popupContainerEditModuleFilter.Name = "popupContainerEditModuleFilter";
            this.popupContainerEditModuleFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditModuleFilter.Properties.PopupControl = this.popupContainerControlModuleFilter;
            this.popupContainerEditModuleFilter.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditModuleFilter.Size = new System.Drawing.Size(246, 20);
            this.popupContainerEditModuleFilter.StyleController = this.layoutControl2;
            this.popupContainerEditModuleFilter.TabIndex = 10;
            this.popupContainerEditModuleFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditModuleFilter_QueryResultValue);
            // 
            // btnLoad
            // 
            this.btnLoad.ImageOptions.ImageIndex = 5;
            this.btnLoad.ImageOptions.ImageList = this.imageCollection1;
            this.btnLoad.Location = new System.Drawing.Point(239, 248);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(82, 22);
            this.btnLoad.StyleController = this.layoutControl2;
            toolTipTitleItem4.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Text = "Load Data - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to Load Action Data.\r\n\r\nOnly data matching the specified Filter Criteria" +
    " will be loaded.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.btnLoad.SuperTip = superToolTip4;
            this.btnLoad.TabIndex = 15;
            this.btnLoad.Text = "Load Data";
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(87, 144);
            this.dateEditFromDate.MenuManager = this.barManager1;
            this.dateEditFromDate.Name = "dateEditFromDate";
            superToolTip5.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem5.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Text = "Clear Date - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all Ac" +
    "tions will be loaded.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "", null, superToolTip5, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate.Properties.Mask.EditMask = "G";
            this.dateEditFromDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditFromDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullValuePrompt = "No Date";
            this.dateEditFromDate.Size = new System.Drawing.Size(222, 20);
            this.dateEditFromDate.StyleController = this.layoutControl2;
            this.dateEditFromDate.TabIndex = 10;
            this.dateEditFromDate.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditFromDate_ButtonClick);
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(87, 168);
            this.dateEditToDate.MenuManager = this.barManager1;
            this.dateEditToDate.Name = "dateEditToDate";
            superToolTip6.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem6.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Appearance.Options.UseImage = true;
            toolTipTitleItem6.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Text = "Clear Date - Information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all Ac" +
    "tions will be loaded.";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "", null, superToolTip6, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate.Properties.Mask.EditMask = "G";
            this.dateEditToDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditToDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullValuePrompt = "No Date";
            this.dateEditToDate.Size = new System.Drawing.Size(222, 20);
            this.dateEditToDate.StyleController = this.layoutControl2;
            this.dateEditToDate.TabIndex = 10;
            this.dateEditToDate.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditToDate_ButtonClick);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem2,
            this.layoutControlItem4,
            this.emptySpaceItem4,
            this.ItemForModuleFilter,
            this.ItemForRecordTypeFilter,
            this.ItemForStatusFilter,
            this.layoutControlGroup3,
            this.emptySpaceItem3,
            this.layoutControlItem1,
            this.ItemForValue,
            this.emptySpaceItem1,
            this.emptySpaceItem5,
            this.ItemForClearFilters});
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup2.Size = new System.Drawing.Size(328, 627);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 267);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(318, 350);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.btnLoad;
            this.layoutControlItem4.Location = new System.Drawing.Point(232, 241);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(86, 26);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(86, 26);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(86, 26);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "Load Data:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(94, 241);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(138, 26);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForModuleFilter
            // 
            this.ItemForModuleFilter.Control = this.popupContainerEditModuleFilter;
            this.ItemForModuleFilter.Location = new System.Drawing.Point(0, 0);
            this.ItemForModuleFilter.Name = "ItemForModuleFilter";
            this.ItemForModuleFilter.Size = new System.Drawing.Size(318, 24);
            this.ItemForModuleFilter.Text = "Module:";
            this.ItemForModuleFilter.TextSize = new System.Drawing.Size(65, 13);
            // 
            // ItemForRecordTypeFilter
            // 
            this.ItemForRecordTypeFilter.Control = this.popupContainerEditRecordTypeFilter;
            this.ItemForRecordTypeFilter.Location = new System.Drawing.Point(0, 24);
            this.ItemForRecordTypeFilter.Name = "ItemForRecordTypeFilter";
            this.ItemForRecordTypeFilter.Size = new System.Drawing.Size(318, 24);
            this.ItemForRecordTypeFilter.Text = "Record Type:";
            this.ItemForRecordTypeFilter.TextSize = new System.Drawing.Size(65, 13);
            // 
            // ItemForStatusFilter
            // 
            this.ItemForStatusFilter.Control = this.popupContainerEditStatusFilter;
            this.ItemForStatusFilter.Location = new System.Drawing.Point(0, 48);
            this.ItemForStatusFilter.Name = "ItemForStatusFilter";
            this.ItemForStatusFilter.Size = new System.Drawing.Size(318, 24);
            this.ItemForStatusFilter.Text = "Status:";
            this.ItemForStatusFilter.TextSize = new System.Drawing.Size(65, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem3});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 105);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(318, 92);
            this.layoutControlGroup3.Text = "Date Range:";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.dateEditFromDate;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(294, 24);
            this.layoutControlItem2.Text = "From Date:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(65, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.dateEditToDate;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(294, 24);
            this.layoutControlItem3.Text = "To Date:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(65, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 95);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(318, 10);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.checkEditShowErrorsOnly;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(318, 23);
            this.layoutControlItem1.Text = "Errors Only:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(65, 13);
            // 
            // ItemForValue
            // 
            this.ItemForValue.Control = this.memoExEditValue;
            this.ItemForValue.Location = new System.Drawing.Point(0, 207);
            this.ItemForValue.Name = "ItemForValue";
            this.ItemForValue.Size = new System.Drawing.Size(318, 24);
            this.ItemForValue.Text = "Value:";
            this.ItemForValue.TextSize = new System.Drawing.Size(65, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 231);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(318, 10);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 197);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(318, 10);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForClearFilters
            // 
            this.ItemForClearFilters.Control = this.btnClearAllFilters;
            this.ItemForClearFilters.Location = new System.Drawing.Point(0, 241);
            this.ItemForClearFilters.MaxSize = new System.Drawing.Size(94, 26);
            this.ItemForClearFilters.MinSize = new System.Drawing.Size(94, 26);
            this.ItemForClearFilters.Name = "ItemForClearFilters";
            this.ItemForClearFilters.Size = new System.Drawing.Size(94, 26);
            this.ItemForClearFilters.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForClearFilters.Text = "Clear Filters:";
            this.ItemForClearFilters.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForClearFilters.TextVisible = false;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            // 
            // dockManager1
            // 
            this.dockManager1.AutoHideContainers.AddRange(new DevExpress.XtraBars.Docking.AutoHideContainer[] {
            this.hideContainerLeft});
            this.dockManager1.Form = this;
            this.dockManager1.MenuManager = this.barManager1;
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane"});
            // 
            // hideContainerLeft
            // 
            this.hideContainerLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.hideContainerLeft.Controls.Add(this.dockPanelFilters);
            this.hideContainerLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.hideContainerLeft.Location = new System.Drawing.Point(0, 0);
            this.hideContainerLeft.Name = "hideContainerLeft";
            this.hideContainerLeft.Size = new System.Drawing.Size(23, 659);
            // 
            // dockPanelFilters
            // 
            this.dockPanelFilters.Controls.Add(this.dockPanel1_Container);
            this.dockPanelFilters.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelFilters.ID = new System.Guid("cfa6aa04-7a42-43c1-9b83-5fe8dca34d55");
            this.dockPanelFilters.Location = new System.Drawing.Point(0, 0);
            this.dockPanelFilters.Name = "dockPanelFilters";
            this.dockPanelFilters.Options.ShowCloseButton = false;
            this.dockPanelFilters.OriginalSize = new System.Drawing.Size(334, 200);
            this.dockPanelFilters.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelFilters.SavedIndex = 0;
            this.dockPanelFilters.Size = new System.Drawing.Size(334, 659);
            this.dockPanelFilters.Text = "Data Filter";
            this.dockPanelFilters.Visibility = DevExpress.XtraBars.Docking.DockVisibility.AutoHide;
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.layoutControl2);
            this.dockPanel1_Container.Location = new System.Drawing.Point(3, 29);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(328, 627);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // bar1
            // 
            this.bar1.BarName = "Filter";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.FloatLocation = new System.Drawing.Point(895, 171);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefresh, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciFilterData),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiSelectedCount, true)});
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar1.Text = "Custom 2";
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiRefresh.Caption = "Refresh";
            this.bbiRefresh.Id = 29;
            this.bbiRefresh.ImageOptions.Image = global::WoodPlan5.Properties.Resources.refresh_32x32;
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // bciFilterData
            // 
            this.bciFilterData.Caption = "Filter Selected";
            this.bciFilterData.Id = 30;
            this.bciFilterData.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Filter_32x32;
            this.bciFilterData.Name = "bciFilterData";
            this.bciFilterData.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Filter Selected - Information";
            toolTipItem1.LeftIndent = 6;
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bciFilterData.SuperTip = superToolTip1;
            this.bciFilterData.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciFilterData_CheckedChanged);
            // 
            // bsiSelectedCount
            // 
            this.bsiSelectedCount.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bsiSelectedCount.Caption = "0 Queue Records Selected";
            this.bsiSelectedCount.Id = 31;
            this.bsiSelectedCount.ImageOptions.ImageIndex = 4;
            this.bsiSelectedCount.ItemAppearance.Disabled.Options.UseTextOptions = true;
            this.bsiSelectedCount.ItemAppearance.Disabled.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bsiSelectedCount.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.bsiSelectedCount.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bsiSelectedCount.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.bsiSelectedCount.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bsiSelectedCount.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.bsiSelectedCount.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bsiSelectedCount.Name = "bsiSelectedCount";
            this.bsiSelectedCount.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bsiSelectedCount.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // sp01021_Core_Web_Service_Queue_Viewer_Module_ListTableAdapter
            // 
            this.sp01021_Core_Web_Service_Queue_Viewer_Module_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp01022_Core_Web_Service_Queue_Viewer_RecordType_ListTableAdapter
            // 
            this.sp01022_Core_Web_Service_Queue_Viewer_RecordType_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp01023_Core_Web_Service_Queue_Viewer_Status_ListTableAdapter
            // 
            this.sp01023_Core_Web_Service_Queue_Viewer_Status_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp01024_Core_Web_Service_Queue_Items_ListTableAdapter
            // 
            this.sp01024_Core_Web_Service_Queue_Items_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp01025_Core_Web_Service_Queue_Items_For_Queue_RecordTableAdapter
            // 
            this.sp01025_Core_Web_Service_Queue_Items_For_Queue_RecordTableAdapter.ClearBeforeFill = true;
            // 
            // sp01026_Core_Web_Service_Queue_Images_For_Queue_RecordTableAdapter
            // 
            this.sp01026_Core_Web_Service_Queue_Images_For_Queue_RecordTableAdapter.ClearBeforeFill = true;
            // 
            // frm_Core_Web_Service_Queue_Viewer
            // 
            this.ClientSize = new System.Drawing.Size(1060, 659);
            this.Controls.Add(this.splitContainerControl2);
            this.Controls.Add(this.hideContainerLeft);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_Core_Web_Service_Queue_Viewer";
            this.Text = "Web Service Queue Viewer";
            this.Activated += new System.EventHandler(this.frm_Core_Web_Service_Queue_Viewer_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_Core_Web_Service_Queue_Viewer_FormClosing);
            this.Load += new System.EventHandler(this.frm_Core_Web_Service_Queue_Viewer_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.hideContainerLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl2, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlStatusFilter)).EndInit();
            this.popupContainerControlStatusFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01023CoreWebServiceQueueViewerStatusListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Common_Functionality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlRecordTypeFilter)).EndInit();
            this.popupContainerControlRecordTypeFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01022CoreWebServiceQueueViewerRecordTypeListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlModuleFilter)).EndInit();
            this.popupContainerControlModuleFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01021CoreWebServiceQueueViewerModuleListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01024CoreWebServiceQueueItemsListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).EndInit();
            this.splitContainerControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01025CoreWebServiceQueueItemsForQueueRecordBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01026CoreWebServiceQueueImagesForQueueRecordBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEditPreview.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditShowErrorsOnly.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoExEditValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditStatusFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditRecordTypeFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditModuleFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForModuleFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRecordTypeFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClearFilters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.hideContainerLeft.ResumeLayout(false);
            this.dockPanelFilters.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraEditors.CheckEdit checkEditShowErrorsOnly;
        private DataSet_AT dataSet_AT;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraGrid.GridControl gridControl39;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView39;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelFilters;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraEditors.SimpleButton btnLoad;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraBars.BarCheckItem bciFilterData;
        private DevExpress.XtraBars.BarStaticItem bsiSelectedCount;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlModuleFilter;
        private DevExpress.XtraEditors.SimpleButton btnModuleFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditModuleFilter;
        private DevExpress.XtraLayout.LayoutControlItem ItemForModuleFilter;
        private DataSet_Common_Functionality dataSet_Common_Functionality;
        private System.Windows.Forms.BindingSource sp01021CoreWebServiceQueueViewerModuleListBindingSource;
        private DataSet_Common_FunctionalityTableAdapters.sp01021_Core_Web_Service_Queue_Viewer_Module_ListTableAdapter sp01021_Core_Web_Service_Queue_Viewer_Module_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colModuleID;
        private DevExpress.XtraGrid.Columns.GridColumn colModuleName;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlRecordTypeFilter;
        private DevExpress.XtraEditors.SimpleButton btnRecordTypeFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditRecordTypeFilter;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRecordTypeFilter;
        private System.Windows.Forms.BindingSource sp01022CoreWebServiceQueueViewerRecordTypeListBindingSource;
        private DataSet_Common_FunctionalityTableAdapters.sp01022_Core_Web_Service_Queue_Viewer_RecordType_ListTableAdapter sp01022_Core_Web_Service_Queue_Viewer_RecordType_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordType;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordTypeID;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlStatusFilter;
        private DevExpress.XtraEditors.SimpleButton btnStatusFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditStatusFilter;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStatusFilter;
        private System.Windows.Forms.BindingSource sp01023CoreWebServiceQueueViewerStatusListBindingSource;
        private DataSet_Common_FunctionalityTableAdapters.sp01023_Core_Web_Service_Queue_Viewer_Status_ListTableAdapter sp01023_Core_Web_Service_Queue_Viewer_Status_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder1;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusID;
        private DevExpress.XtraEditors.MemoExEdit memoExEditValue;
        private DevExpress.XtraLayout.LayoutControlItem ItemForValue;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraEditors.SimpleButton btnClearAllFilters;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClearFilters;
        private System.Windows.Forms.BindingSource sp01024CoreWebServiceQueueItemsListBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colQueueID;
        private DevExpress.XtraGrid.Columns.GridColumn colSystemID;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colData;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusID1;
        private DevExpress.XtraGrid.Columns.GridColumn colErrorCount;
        private DevExpress.XtraGrid.Columns.GridColumn colDateAdded1;
        private DevExpress.XtraGrid.Columns.GridColumn colSystemDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colItemCount;
        private DevExpress.XtraGrid.Columns.GridColumn colImageCount;
        private DevExpress.XtraGrid.Columns.GridColumn colSelected;
        private DataSet_Common_FunctionalityTableAdapters.sp01024_Core_Web_Service_Queue_Items_ListTableAdapter sp01024_Core_Web_Service_Queue_Items_ListTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime2;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeID;
        private System.Windows.Forms.BindingSource sp01025CoreWebServiceQueueItemsForQueueRecordBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colQueueItemID;
        private DevExpress.XtraGrid.Columns.GridColumn colQueueID1;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordTypeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID1;
        private DevExpress.XtraGrid.Columns.GridColumn colNumericValue1;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusID2;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordTypeDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colSelected1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DataSet_Common_FunctionalityTableAdapters.sp01025_Core_Web_Service_Queue_Items_For_Queue_RecordTableAdapter sp01025_Core_Web_Service_Queue_Items_For_Queue_RecordTableAdapter;
        private System.Windows.Forms.BindingSource sp01026CoreWebServiceQueueImagesForQueueRecordBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colImageID;
        private DevExpress.XtraGrid.Columns.GridColumn colQueueID2;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageEdit repositoryItemImageEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colSelected2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DataSet_Common_FunctionalityTableAdapters.sp01026_Core_Web_Service_Queue_Images_For_Queue_RecordTableAdapter sp01026_Core_Web_Service_Queue_Images_For_Queue_RecordTableAdapter;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.PictureEdit pictureEditPreview;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl3;
        private DevExpress.XtraGrid.Columns.GridColumn colCalculatedStatus;
        private DevExpress.XtraBars.Docking.AutoHideContainer hideContainerLeft;
    }
}

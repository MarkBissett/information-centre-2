using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;

using WoodPlan5.Properties;
using BaseObjects;
using Utilities;  // Used by Datasets //

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;

namespace WoodPlan5
{
    public partial class frm_EP_Asset_Edit : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        public int intLinkedToRecordID = 0;

        public bool ibool_CalledByTreePicker = false;
        //public int intObjectType = 0;
        public int intX = 0;
        public int intY = 0;
        public string strPolygonXY = "";
        public decimal decArea = (decimal)0.00;
        public decimal decLength = (decimal)0.00;
        public decimal decWidth = (decimal)0.00;
        public string strID = "";
        public string strMapIDs = "";  // Only required when block editing from the map - used to tell the tree picker which objects to reload //
        public int intAssetType = 0;

        public float flLatLongX = 0;  // Following 3 required to store LatLong version of plotted object from map //
        public float flLatLongY = 0;
        public string strLatLongPolygonXY = "";

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        private int i_int_FocusedGrid = 1;
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //
        string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //

        Boolean ibool_ignoreValidation = false;
        Boolean ibool_ignoreValidation2 = false;

        ArrayList ArrayListFilteredPicklists;  // Holds all the picklists bound to sp01372 - used for iterating round to bind and unbind generic Enter, Leave and ButtonClick events //
        private string i_strLastUsedSequencePrefix = "";

        #endregion

        public frm_EP_Asset_Edit()
        {
            InitializeComponent();
        }

        private void frm_EP_Asset_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 30031;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            ArrayListFilteredPicklists = new ArrayList();  // Note: Each GridLookUpEdit within the array should have it's tag value set to it's value in the SQL PicklistHeader Table //
            ArrayListFilteredPicklists.Add(StatusIDGridLookUpEdit);

            sp03028_EP_Sites_With_Blank_DDLBTableAdapter.Connection.ConnectionString = strConnectionString;
            sp03028_EP_Sites_With_Blank_DDLBTableAdapter.Fill(this.dataSet_EP_DataEntry.sp03028_EP_Sites_With_Blank_DDLB, "");

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp03019_EP_Asset_Types_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp03019_EP_Asset_Types_List_With_BlankTableAdapter.Fill(this.dataSet_EP_DataEntry.sp03019_EP_Asset_Types_List_With_Blank);

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp03030_EP_Asset_Sub_TypesTableAdapter.Connection.ConnectionString = strConnectionString;
            sp03030_EP_Asset_Sub_TypesTableAdapter.Fill(this.dataSet_EP_DataEntry.sp03030_EP_Asset_Sub_Types, "");

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp03031_EP_Asset_ConditionsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp03031_EP_Asset_ConditionsTableAdapter.Fill(this.dataSet_EP_DataEntry.sp03031_EP_Asset_Conditions, "");

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter.Fill(dataSet_AT_DataEntry.sp01372_AT_Multiple_Picklists_With_Blanks);
            sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01372_AT_Multiple_Picklists_With_Blanks);

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp03032_EP_Asset_Life_Expectancy_Description_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp03032_EP_Asset_Life_Expectancy_Description_With_BlankTableAdapter.Fill(this.dataSet_EP_DataEntry.sp03032_EP_Asset_Life_Expectancy_Description_With_Blank, "");

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp03035_EP_Actions_For_Passed_AssetsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState1 = new RefreshGridState(gridView1, "ActionID");

            // Populate Main Dataset //
            sp03026_EP_Asset_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                case "blockadd":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_EP_DataEntry.sp03026_EP_Asset_Edit.NewRow();
                        drNewRow["strMode"] = "add";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["SiteID"] = intLinkedToRecordID;
                        drNewRow["AssetTypeID"] = intAssetType;  // Defaults to 0 unless passed in from TreePicker screen //
                        //drNewRow["StatusID"] = 1;  // 0 = Planned, 1 = Actual, 2 = Historical //
                        if (ibool_CalledByTreePicker)
                        {
                            drNewRow["XCoordinate"] = intX;
                            drNewRow["YCoordinate"] = intY;
                            drNewRow["PolygonXY"] = strPolygonXY;
                            drNewRow["Area"] = decArea;
                            drNewRow["Length"] = decLength;
                            drNewRow["Width"] = decWidth;
                            drNewRow["MapID"] = strID;

                            drNewRow["LocationX"] = flLatLongX;
                            drNewRow["LocationY"] = flLatLongY;
                            drNewRow["LocationPolyXY"] = strLatLongPolygonXY;
                        }
                        this.dataSet_EP_DataEntry.sp03026_EP_Asset_Edit.Rows.Add(drNewRow);
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_EP_DataEntry.sp03026_EP_Asset_Edit.NewRow();
                        drNewRow["strMode"] = "blockedit";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        this.dataSet_EP_DataEntry.sp03026_EP_Asset_Edit.Rows.Add(drNewRow);
                        this.dataSet_EP_DataEntry.sp03026_EP_Asset_Edit.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                    try
                    {
                        sp03026_EP_Asset_EditTableAdapter.Fill(this.dataSet_EP_DataEntry.sp03026_EP_Asset_Edit, strRecordIDs, strFormMode);
                    }
                    catch (Exception)
                    {
                    }
                    break;
            }
            dataLayoutControl1.EndUpdate();

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event //
            Attach_Enter_To_GridLookUpEdits(ArrayListFilteredPicklists);  // Attach Enter Event to All Filtered GridLookUpEdits... Detached on Form Closing Event //
            Attach_ButtonClick_To_GridLookUpEdits(ArrayListFilteredPicklists);  // Attach ButtonClick Event to All Filtered GridLookUpEdits... Detached on Form Closing Event //

        }


        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                //if (item2.Name == "intLocalityIDGridLookUpEdit" || item2.Name == "intOwnershipIDGridLookUpEdit" || item2.Name == "DistrictNameTextEdit") continue; // These 3 filed ingnore and Generic_EditChanged code also copied into their Edit_Changed event - previous weird bug on block editing, had to select locality twice before it would be accepted! //
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        DataTable data;  // Used for holding filtered dataset for picklists //
        private void Attach_Enter_To_GridLookUpEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).Enter += new EventHandler(Filter_GridView);
            }
        }

        private void Detach_Enter_From_GridLookUpEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).Enter -= new EventHandler(Filter_GridView);
            }
        }

        private void Filter_GridView(object sender, EventArgs e)
        {
            /*GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            int intHeaderID = Convert.ToInt32(glue.Tag);
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = "[HeaderID] = " + Convert.ToString(intHeaderID) + " or [HeaderID] = 0";
            view.MakeRowVisible(-1, true);
            view.EndUpdate();*/

            GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            view.BeginUpdate();
            int intHeaderID = Convert.ToInt32(glue.Tag);
            data = this.dataSet_AT_DataEntry.sp01372_AT_Multiple_Picklists_With_Blanks;
            DataView newView = new DataView(data);
            newView.RowFilter = "[HeaderID] = " + Convert.ToString(intHeaderID) + " or [HeaderID] = 0";
            glue.Properties.DataSource = newView;
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
        }


        private void Attach_Leave_To_GridLookUpEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).Leave += new EventHandler(Clear_Filter_GridView);
            }
        }

        private void Detach_Leave_From_GridLookUpEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).Leave -= new EventHandler(Clear_Filter_GridView);
            }
        }

        private void Clear_Filter_GridView(object sender, EventArgs e)
        {
            /*GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.EndUpdate();*/

            GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            view.BeginUpdate();
            glue.Properties.DataSource = this.dataSet_AT_DataEntry.sp01372_AT_Multiple_Picklists_With_Blanks.DefaultView;
            view.EndUpdate();
        }


        private void Attach_ButtonClick_To_GridLookUpEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(ButtonClick_GridView);
            }
        }

        private void Detach_ButtonClick_From_GridLookUpEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).ButtonClick -= new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(ButtonClick_GridView);
            }
        }

        private void ButtonClick_GridView(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    GridLookUpEdit glue = (GridLookUpEdit)sender;
                    int intHeaderID = Convert.ToInt32(glue.Tag);
                    String strPickListName = "";
                    switch (glue.Name)
                    {
                        case "StatusIDGridLookUpEdit":
                            strPickListName = "Asset Statuses";
                            break;
                        default:
                            break;
                    }
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, intHeaderID, strPickListName);
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01372_AT_Multiple_Picklists_With_Blanks);
                    this.ValidateChildren();
                }
            }
        }



        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //

            if (this.dataSet_EP_DataEntry.sp03026_EP_Asset_Edit.Rows.Count == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Asset", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                // Following 3 lines allow form to kill the validation to allow it to close //
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        SiteIDGridLookUpEdit.Focus();

                        gridControl1.Enabled = false;
                        AssetNumberButtonEdit.Properties.ReadOnly = false;
                        AssetNumberButtonEdit.Properties.Buttons[0].Enabled = true;
                        AssetNumberButtonEdit.Properties.Buttons[1].Enabled = true;
                        MapIDTextEdit.Properties.ReadOnly = false;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        SiteIDGridLookUpEdit.Focus();

                        gridControl1.Enabled = false;
                        AssetNumberButtonEdit.Properties.ReadOnly = true;
                        AssetNumberButtonEdit.Properties.Buttons[0].Enabled = true;
                        AssetNumberButtonEdit.Properties.Buttons[1].Enabled = false;
                        MapIDTextEdit.Properties.ReadOnly = true;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        SiteIDGridLookUpEdit.Focus();

                        gridControl1.Enabled = true;
                        AssetNumberButtonEdit.Properties.ReadOnly = false;
                        AssetNumberButtonEdit.Properties.Buttons[0].Enabled = true;
                        AssetNumberButtonEdit.Properties.Buttons[1].Enabled = true;
                        MapIDTextEdit.Properties.ReadOnly = false;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        SiteIDGridLookUpEdit.Focus();

                        gridControl1.Enabled = false;
                        AssetNumberButtonEdit.Properties.ReadOnly = true;
                        AssetNumberButtonEdit.Properties.Buttons[0].Enabled = true;
                        AssetNumberButtonEdit.Properties.Buttons[1].Enabled = false;
                        MapIDTextEdit.Properties.ReadOnly = true;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //
        }

        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_EP_DataEntry.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            GridView view = null;
            int[] intRowHandles;
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();

            int AssetID = 0;  // Check if the current record has been saved at some point //
            DataRowView currentRow = (DataRowView)sp03026EPAssetEditBindingSource.Current;
            if (currentRow != null) AssetID = (currentRow["AssetID"] == null ? 0 : Convert.ToInt32(currentRow["AssetID"]));

            if (i_int_FocusedGrid == 1)  // Actions //
            {
                view = (GridView)gridControl1.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd && AssetID > 0)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                    if (intRowHandles.Length >= 1)
                    {
                        //bbiBlockAddAction.Enabled = true;
                    }
                }
                bbiBlockAdd.Enabled = false;
                if (iBool_AllowEdit && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView1 navigator custom buttons //
            if (iBool_AllowAdd && AssetID > 0)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEdit)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }
        }

        public void UpdateFormRefreshStatus(int status, string strNewIDs1, string strNewIDs2)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
        }

        private void frm_EP_Asset_Edit_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                LoadLinkedRecords();
            }
            SetMenuStatus();
        }

        private void frm_EP_Asset_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();

                        Detach_EditValuechanged_From_Children(this.Controls); // Detach Validating Event from all Editors to track changes...  Attached on Form Load Event //
                        Detach_Enter_From_GridLookUpEdits(ArrayListFilteredPicklists);  // Detach Enter Event from All Filtered GridLookUpEdits... Attached on Form Load Event //
                        Detach_Leave_From_GridLookUpEdits(ArrayListFilteredPicklists);  // Detach Leave Event from All Filtered GridLookUpEdits... Attached on Form Load Event //
                        Detach_ButtonClick_From_GridLookUpEdits(ArrayListFilteredPicklists);  // Detach ButtonClick Event from All Filtered GridLookUpEdits... Attached on Form Load Event //

                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            this.sp03026EPAssetEditBindingSource.EndEdit();
            try
            {
                this.sp03026_EP_Asset_EditTableAdapter.Update(dataSet_EP_DataEntry);  // Insert and Update queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlighted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)sp03026EPAssetEditBindingSource.Current;
                if (currentRow != null) strNewIDs = Convert.ToString(currentRow["AssetID"]) + ";";
                gridControl1.Enabled = true;

                // Switch mode to Edit so than any subsequent changes update this record //
                this.strFormMode = "edit";
                if (currentRow != null)
                {
                    currentRow["strMode"] = "edit";
                    currentRow.Row.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                }
            }
            //else if (this.strFormMode.ToLower() == "blockadd")
            //{
            //    DataRowView currentRow = (DataRowView)sp03026EPAssetEditBindingSource.Current;
            //    if (currentRow != null) strNewIDs = Convert.ToString(currentRow["strRecordIDs"]);  // Values returned from Update SP //
            //}
            // Pick up all Map IDs and concatenate together so Tree Picker can be updated if it is running //
            string strUpdateIDs = "";
            if (this.strFormMode.ToLower() == "blockedit")
            {
                strUpdateIDs = strMapIDs;
            }
            else
            {
                foreach (DataRow dr in this.dataSet_EP_DataEntry.sp03026_EP_Asset_Edit.Rows)
                {
                    if (!string.IsNullOrEmpty(dr["MapID"].ToString()))
                    {
                        strUpdateIDs += dr["MapID"].ToString() + ",";
                    }
                }
            }

            /*// Notify any open forms which reference this data that they will need to refresh their data on activating //
            string strAssetIDs = "";
            if (string.IsNullOrEmpty(strRecordIDs))
            {
                DataRowView currentRow = (DataRowView)sp03026EPAssetEditBindingSource.Current;
                if (currentRow != null) strAssetIDs = Convert.ToString(currentRow["AssetID"]) + ",";
            }
            else
            {
                strAssetIDs = strRecordIDs;
            }*/
            Broadcast_Data_Refresh Broadcast = new Broadcast_Data_Refresh();
            Broadcast.Asset_Refresh(this.ParentForm, this.Name, strNewIDs, strUpdateIDs);

            SetMenuStatus();  // Disables Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;

            this.ValidateChildren();  // Force Validation Message on any controls containing them //     

            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
            for (int i = 0; i < this.dataSet_EP_DataEntry.sp03026_EP_Asset_Edit.Rows.Count; i++)
            {
                switch (this.dataSet_EP_DataEntry.sp03026_EP_Asset_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void bbiShowMapButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // force changes back to dataset //
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            DataRowView currentRow = (DataRowView)sp03026EPAssetEditBindingSource.Current;
            if (currentRow == null) return;
            if (String.IsNullOrEmpty(currentRow["AssetID"].ToString()) || currentRow["AssetID"].ToString() == "0")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Save the current record before attempting to display it on the map.", "Show Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strSelectedIDs = currentRow["AssetID"].ToString() + ',';
            try
            {
                Mapping_Functions MapFunctions = new Mapping_Functions();
                MapFunctions.Show_Map_From_Assets_Screen(this.GlobalSettings, strConnectionString, this, strSelectedIDs, "asset");
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to open the map [" + ex.Message + "].\n\nTry opening the map again. If the problem persists contact Technical Support.", "Show Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
        }

        private void LoadLinkedRecords()
        {
             if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;

             string strAssetID = "";
             DataRowView currentRow = (DataRowView)sp03026EPAssetEditBindingSource.Current;
             if (currentRow != null)
             {
                 strAssetID = (currentRow["AssetID"] == null ? "" : currentRow["AssetID"].ToString() + ",");
             }

             // Actions //
             this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
             gridControl1.BeginUpdate();
             sp03035_EP_Actions_For_Passed_AssetsTableAdapter.Fill(this.dataSet_EP.sp03035_EP_Actions_For_Passed_Assets, strAssetID, null, null);
             this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
             gridControl1.EndUpdate();

             // Highlight any recently added new rows //
             if (i_str_AddedRecordIDs1 != "")
             {
                 char[] delimiters = new char[] { ';' };
                 string[] strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                 int intID = 0;
                 int intRowHandle = 0;
                 GridView view = (GridView)gridControl1.MainView;
                 view.ClearSelection(); // Clear any current selection so just the new record is selected //
                 foreach (string strElement in strArray)
                 {
                     intID = Convert.ToInt32(strElement);
                     intRowHandle = view.LocateByValue(0, view.Columns["ActionID"], intID);
                     if (intRowHandle != GridControl.InvalidRowHandle)
                     {
                         view.SelectRow(intRowHandle);
                         view.MakeRowVisible(intRowHandle, false);
                     }
                 }
                 i_str_AddedRecordIDs1 = "";
             }/*
             CheckInspectionsForLastInspectionDate();// Check if Inspection with the most recent date is the same as that on the Tree's Last Inspection Date. If not, update the Tree's Date //


             // Linked Documents //
             this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
             gridControl4.BeginUpdate();
             sp00220_Linked_Documents_ListTableAdapter.Fill(this.dataSet_AT.sp00220_Linked_Documents_List, strTreeID, 3, strDefaultPath);
             this.RefreshGridViewState3.LoadViewInfo();  // Reload any expanded groups and selected rows //
             gridControl4.EndUpdate();

             // Highlight any recently added new rows //
             if (i_str_AddedRecordIDs3 != "")
             {
                 char[] delimiters = new char[] { ';' };
                 string[] strArray = i_str_AddedRecordIDs3.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                 int intID = 0;
                 int intRowHandle = 0;
                 GridView view = (GridView)gridControl4.MainView;
                 view.ClearSelection(); // Clear any current selection so just the new record is selected //
                 foreach (string strElement in strArray)
                 {
                     intID = Convert.ToInt32(strElement);
                     intRowHandle = view.LocateByValue(0, view.Columns["LinkedDocumentID"], intID);
                     if (intRowHandle != GridControl.InvalidRowHandle)
                     {
                         view.SelectRow(intRowHandle);
                         view.MakeRowVisible(intRowHandle, false);
                     }
                 }
                 i_str_AddedRecordIDs3 = "";
             }*/
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            if (this.strFormMode != "blockedit")
            {
                LoadLinkedRecords();
                GridView view = (GridView)gridControl1.MainView;
                view.ExpandAllGroups();
                /*view = (GridView)gridControl3.MainView;
                view.ExpandAllGroups();
                view = (GridView)gridControl4.MainView;
                view.ExpandAllGroups(); */
            }
        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void SiteIDGridLookUpEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 3002, "Sites");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp03028_EP_Sites_With_Blank_DDLBTableAdapter.Fill(this.dataSet_EP_DataEntry.sp03028_EP_Sites_With_Blank_DDLB, "");
                }
            }
        }

        private void SiteIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && (string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "0"))
            {
                dxErrorProvider1.SetError(SiteIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(SiteIDGridLookUpEdit, "");
            }
        }


        private void AssetTypeIDGridLookUpEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 9060, "Asset Types");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp03019_EP_Asset_Types_List_With_BlankTableAdapter.Fill(this.dataSet_EP_DataEntry.sp03019_EP_Asset_Types_List_With_Blank);
                }
            }
        }

        private void AssetTypeIDGridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off for Asset Sub-Type Field //
            ibool_ignoreValidation2 = false;  // Toggles validation on and off for Asset Condition Field //
        }

        private void AssetTypeIDGridLookUpEdit_Validated(object sender, EventArgs e)
        {
            // Check if Asset Sub-Type and condition is valid for current asset type - if not, clear it //
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (glue.EditValue == DBNull.Value || glue.EditValue.ToString() == "0")
            {
                AssetSubTypeIDGridLookUpEdit.EditValue = 0;  // Clear Asset Sub-Type //
                CurrentConditionIDGridLookUpEdit.EditValue = 0;  // Clear Asset Condition //
                return;
            }
            int intCurrentAssetTypeID = Convert.ToInt32(glue.EditValue);
            glue = (GridLookUpEdit)AssetSubTypeIDGridLookUpEdit;
            GridView view = glue.Properties.View;
            int intFoundRow = 0;
            intFoundRow = (glue.EditValue == DBNull.Value ? GridControl.InvalidRowHandle : view.LocateByValue(0, view.Columns["AssetSubTypeID"], Convert.ToInt32(glue.EditValue)));
            if (intFoundRow != GridControl.InvalidRowHandle)
            {
                if (intCurrentAssetTypeID != Convert.ToInt32(view.GetRowCellValue(intFoundRow, "AssetTypeID"))) glue.EditValue = 0;
            }

            glue = (GridLookUpEdit)CurrentConditionIDGridLookUpEdit;
            view = glue.Properties.View;
            intFoundRow = 0;
            intFoundRow = (glue.EditValue == DBNull.Value ? GridControl.InvalidRowHandle : view.LocateByValue(0, view.Columns["ConditionID"], Convert.ToInt32(glue.EditValue)));
            if (intFoundRow != GridControl.InvalidRowHandle)
            {
                if (intCurrentAssetTypeID != Convert.ToInt32(view.GetRowCellValue(intFoundRow, "AssetTypeID"))) glue.EditValue = 0;
            }
  
        }

        private void AssetTypeIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && (string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "0"))
            {
                dxErrorProvider1.SetError(AssetTypeIDGridLookUpEdit, "Select a value.");

                // Clear Linked Picklist since the Validated event for this control will not fire due to the line below [e.Cancel = true] //
                AssetSubTypeIDGridLookUpEdit.EditValue = 0;  // Clear Asset Sub-Type //
                CurrentConditionIDGridLookUpEdit.EditValue = 0;  // Clear Asset Condition //

                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(AssetTypeIDGridLookUpEdit, "");
            }
        }


        private void AssetSubTypeIDGridLookUpEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 9060, "Asset Types");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp03030_EP_Asset_Sub_TypesTableAdapter.Fill(this.dataSet_EP_DataEntry.sp03030_EP_Asset_Sub_Types, "");
                }
            }
        }

        private void AssetSubTypeIDGridLookUpEdit_Enter(object sender, EventArgs e)
        {
            string strAssetTypeID = AssetTypeIDGridLookUpEdit.EditValue.ToString();
            if (string.IsNullOrEmpty(strAssetTypeID) || strAssetTypeID == "0")
            {
                return;
            }
            else
            {
                GridLookUpEdit glue = (GridLookUpEdit)sender;
                GridView view = glue.Properties.View;
                view.BeginUpdate();
                view.ActiveFilter.Clear();
                view.ActiveFilter.NonColumnFilter = "[AssetTypeID] = " + strAssetTypeID + " or [AssetTypeID] = 0";
                view.MakeRowVisible(-1, true);
                view.EndUpdate();
            }
        }

        private void AssetSubTypeIDGridLookUpEdit_Leave(object sender, EventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.EndUpdate();
        }

        private void AssetSubTypeIDGridLookUpEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;
            string strAssetTypeID = AssetTypeIDGridLookUpEdit.EditValue.ToString();
            if (string.IsNullOrEmpty(strAssetTypeID) || strAssetTypeID == "0")  // No Asset Type set, so pick it up from Asset Sub-Type //
            {
                GridLookUpEdit glue = (GridLookUpEdit)sender;
                if (glue.EditValue == DBNull.Value || string.IsNullOrEmpty(glue.EditValue.ToString())) return;

                GridView view = glue.Properties.View;
                int intFoundRow = 0;
                intFoundRow = (glue.EditValue == DBNull.Value ? GridControl.InvalidRowHandle : view.LocateByValue(0, view.Columns["AssetSubTypeID"], Convert.ToInt32(glue.EditValue)));
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    int AssetTypeID = Convert.ToInt32(view.GetRowCellValue(intFoundRow, "AssetTypeID"));
                    AssetTypeIDGridLookUpEdit.EditValue = AssetTypeID;
                    dxErrorProvider1.SetError(AssetTypeIDGridLookUpEdit, "");  // Mark field as valid //
                }
            }
        }


        private void CurrentConditionIDGridLookUpEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 9060, "Asset Conditions");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp03031_EP_Asset_ConditionsTableAdapter.Fill(this.dataSet_EP_DataEntry.sp03031_EP_Asset_Conditions, "");
                }
            }
        }

        private void CurrentConditionIDGridLookUpEdit_Enter(object sender, EventArgs e)
        {
            string strAssetTypeID = AssetTypeIDGridLookUpEdit.EditValue.ToString();
            if (string.IsNullOrEmpty(strAssetTypeID) || strAssetTypeID == "0")
            {
                return;
            }
            else
            {
                GridLookUpEdit glue = (GridLookUpEdit)sender;
                GridView view = glue.Properties.View;
                view.BeginUpdate();
                view.ActiveFilter.Clear();
                view.ActiveFilter.NonColumnFilter = "[AssetTypeID] = " + strAssetTypeID + " or [AssetTypeID] = 0";
                view.MakeRowVisible(-1, true);
                view.EndUpdate();
            }
        }

        private void CurrentConditionIDGridLookUpEdit_Leave(object sender, EventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.EndUpdate();
        }

        private void CurrentConditionIDGridLookUpEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_ignoreValidation2) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation2 = true;
            string strAssetTypeID = AssetTypeIDGridLookUpEdit.EditValue.ToString();
            if (string.IsNullOrEmpty(strAssetTypeID) || strAssetTypeID == "0")  // No Asset Type set, so pick it up from Asset Condition //
            {
                GridLookUpEdit glue = (GridLookUpEdit)sender;
                if (glue.EditValue == DBNull.Value || string.IsNullOrEmpty(glue.EditValue.ToString())) return;

                GridView view = glue.Properties.View;
                int intFoundRow = 0;
                intFoundRow = (glue.EditValue == DBNull.Value ? GridControl.InvalidRowHandle : view.LocateByValue(0, view.Columns["ConditionID"], Convert.ToInt32(glue.EditValue)));
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    int AssetTypeID = Convert.ToInt32(view.GetRowCellValue(intFoundRow, "AssetTypeID"));
                    AssetTypeIDGridLookUpEdit.EditValue = AssetTypeID;
                    dxErrorProvider1.SetError(AssetTypeIDGridLookUpEdit, "");  // Mark field as valid //
                }
            }
        }


        private void AssetNumberButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            string strTableName = "EP_Asset";
            string strFieldName = "AssetNumber";

            if (e.Button.Tag.ToString() == "Sequence")  // Sequence Button //
            {
                /* Attempt to find corresponding Client ID from Site ID *
                * Create a clone of the Binding source and filter it to find a match.
                * Don't do it on the original as the Height Band list shares it and it causes funny effects on the field */
                int intClientID = 0;
                int intSiteID = (string.IsNullOrEmpty(SiteIDGridLookUpEdit.EditValue.ToString()) ? 0 : Convert.ToInt32(SiteIDGridLookUpEdit.EditValue));
                BindingSource newBindingSource = new BindingSource(sp03028EPSitesWithBlankDDLBBindingSource.DataSource, sp03028EPSitesWithBlankDDLBBindingSource.DataMember);
                newBindingSource.Filter = "";  // Clear any Prior Filter //
                newBindingSource.Filter = "SiteID = '" + intSiteID.ToString() + "'";
                if (newBindingSource.Count == 1)
                {
                    DataRowView currentRow = (DataRowView)newBindingSource[0];
                    intClientID = Convert.ToInt32(currentRow["ClientID"]);
                }               
                
                frm_Core_Sequence_Selection fChildForm = new frm_Core_Sequence_Selection();
                fChildForm.PassedInTableName = strTableName;
                fChildForm.PassedInFieldName = strFieldName;
                fChildForm.PassedInClientID = intClientID;
                fChildForm.PassedInSiteID = intSiteID;
                this.ParentForm.AddOwnedForm(fChildForm);
                fChildForm.GlobalSettings = this.GlobalSettings;

                if (fChildForm.ShowDialog() == DialogResult.OK)
                {
                    AssetNumberButtonEdit.EditValue = fChildForm.SelectedSequence;
                    i_strLastUsedSequencePrefix = fChildForm.SelectedSequence;  // Store Sequence Prefix for saving against Last Saved Record //
                }
            }
            else if (e.Button.Tag.ToString() == "Number")
            {
                // Get next value in sequence //
                string strSequence = AssetNumberButtonEdit.EditValue.ToString() ?? "";

                SequenceNumberGetNext sequence = new SequenceNumberGetNext();
                string strNextNumber = sequence.GetNextNumberInSequence(strConnectionString, strFieldName, strTableName, strSequence);  // Calculate Next in Sequence //

                if (strNextNumber == "")
                {
                    return;
                }
                if (strSequence.StartsWith("?")) strSequence = strSequence.Remove(0, 2);  // Strip off '?x' when X was the numeric length of sequence to generate [Locality Code as Seed] //

                int intRequiredLength = strNextNumber.Length;
                int intNextNumber = Convert.ToInt32(strNextNumber);
                int intIncrement = 0;
                string strTempNumber = "";

                // Check if value is already present within dataset //
                string strCurrentAssetID = AssetIDTextEdit.EditValue.ToString();
                if (string.IsNullOrEmpty(strCurrentAssetID)) strCurrentAssetID = "";
                bool boolUniqueValueFound = false;
                bool boolDuplicateFound = false;
                do
                {
                    boolDuplicateFound = false;
                    strTempNumber = Convert.ToString(intNextNumber + intIncrement);
                    if (strTempNumber.Length > intRequiredLength)  // Abort as the calculated number + sequence will be too big to fit in the field //
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to calculate the next sequence number!\n\nThe length of the sequence plus the calculated sequence number will exceed the length of the field (20 characters).Either reduce the length of the first part of the number or adjust the number of numeric places that the sequence number can span from the Sequence Manager screen.", "Get Next Sequence Number", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    else
                    {
                        strTempNumber = strTempNumber.PadLeft(intRequiredLength, '0');
                    }
                    foreach (DataRow dr in this.dataSet_EP_DataEntry.sp03026_EP_Asset_Edit.Rows)
                    {
                        if (dr["AssetNumber"].ToString() == (strSequence + strTempNumber))
                        {
                            intIncrement++;
                            boolDuplicateFound = true;
                            break;
                        }
                    }
                    if (!boolDuplicateFound) boolUniqueValueFound = true;  // Exit Loop //
                } while (!boolUniqueValueFound);
                AssetNumberButtonEdit.EditValue = strSequence + strTempNumber;
            }
        }

        private void LastVisitDateDateEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nAre you sure you wish to proceed?", "Clear Date Value", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    LastVisitDateDateEdit.EditValue = null;
                }
            }
        }

        private void NextVisitDateDateEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nAre you sure you wish to proceed?", "Clear Date Value", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    NextVisitDateDateEdit.EditValue = null;
                }
            }
        }


        #endregion


        #region Grid - Actions

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (view.RowCount != 0) return;
            string message = "";
            DataRowView currentRow = (DataRowView)sp03026EPAssetEditBindingSource.Current;
            if (currentRow != null)
            {
                message = (String.IsNullOrEmpty(currentRow["AssetID"].ToString()) || currentRow["AssetID"].ToString() == "0" ? "No Actions - Save New Asset Record Before Adding Actions" : "No Actions");
            }
            else message = "No Actions";
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                /*bbiShowMap.Enabled = false;
                bbiDatasetCreate.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                if (view.RowCount > 0)
                {
                    bbiDatasetSelection.Enabled = true;
                    bsiDataset.Enabled = true;
                }
                else
                {
                    bbiDatasetSelection.Enabled = false;
                    bsiDataset.Enabled = false;
                }
                bsiDataset.Enabled = true;
                bbiDatasetManager.Enabled = true;*/
                bbiShowMap.Enabled = (view.SelectedRowsCount > 0 ? true : false);

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            SetMenuStatus();
        }

        private void gridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void gridView1_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }




        #endregion


        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit_Record();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void Add_Record()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            int intParentID = 0;
            string strParentDescription = "";
            int intParentAssetType = 0;
            int intParentSiteID = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Actions //
                    {
                        if (!iBool_AllowAdd) return;

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        //this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        frm_EP_Action_Edit fChildForm = new frm_EP_Action_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = "frm_EP_Asset_Edit";
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;

                        DataRowView currentRow = (DataRowView)sp03026EPAssetEditBindingSource.Current;
                        if (currentRow != null)
                        {
                            intParentID = (currentRow["AssetID"] == null ? 0 : Convert.ToInt32(currentRow["AssetID"]));
                            strParentDescription = (currentRow["AssetNumber"] == null ? "" : currentRow["AssetNumber"].ToString());
                            if (string.IsNullOrEmpty(strParentDescription)) strParentDescription = "Asset [No Number]";
                            intParentAssetType = (currentRow["AssetTypeID"] == null ? 0 : Convert.ToInt32(currentRow["AssetTypeID"]));
                            intParentSiteID = (currentRow["SiteID"] == null ? 0 : Convert.ToInt32(currentRow["SiteID"]));                        
                        }
                        fChildForm.intLinkedToAssetID = intParentID;
                        fChildForm.strLinkedAssetDescription = strParentDescription;
                        fChildForm.intLinkedToAssetTypeID = intParentAssetType;
                        fChildForm.intLinkedSiteID = intParentSiteID;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                /*case 3:     // Linked Documents //
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControl2.MainView;
                        view.PostEditor();
                        var fChildForm = new frm_AT_Linked_Document_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = "frm_AT_Inspection_Edit";
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.intRecordTypeID = 4;  // Inspections //

                        DataRowView currentRow = (DataRowView)sp01381ATEditInspectionDetailsBindingSource.Current;
                        if (currentRow != null)
                        {
                            intInspectionID = (currentRow["intInspectionID"] == null ? 0 : Convert.ToInt32(currentRow["intInspectionID"]));
                            strInspectionReference = (currentRow["strInspectRef"] == null ? "" : currentRow["strInspectRef"].ToString());
                        }
                        fChildForm.intLinkedToRecordID = intInspectionID;
                        fChildForm.strLinkedToRecordDesc = strInspectionReference;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }*/
                default:
                    break;
            }
        }

        private void Block_Edit_Record()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Actions //
                    if (!iBool_AllowEdit) return;
                    view = (GridView)gridControl1.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 1)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    fProgress = new frmProgress(10);
                    this.AddOwnedForm(fProgress);
                    fProgress.Show();  // ***** Closed in PostOpen event ***** //
                    Application.DoEvents();

                    foreach (int intRowHandle in intRowHandles)
                    {
                        strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ActionID")) + ',';
                    }
                    this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                    frm_EP_Action_Edit fChildForm = new frm_EP_Action_Edit();
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strRecordIDs = strRecordIDs;
                    fChildForm.strFormMode = "blockedit";
                    fChildForm.strCaller = "frm_EP_Asset_Edit";
                    fChildForm.intRecordCount = intCount;
                    fChildForm.FormPermissions = this.FormPermissions;
                    fChildForm.fProgress = fProgress;
                    fChildForm.Show();

                    method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm, new object[] { null });
                    break;
                default:
                    break;
            }
        }

        private void Edit_Record()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Actions //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ActionID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        frm_EP_Action_Edit fChildForm = new frm_EP_Action_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = "frm_EP_Asset_Edit";
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                /*case 3:     // Linked Documents //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl2.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedDocumentID")) + ',';
                        }
                        var fChildForm = new frm_AT_Linked_Document_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = "frm_AT_Inspection_Edit";
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.intRecordTypeID = 4;  // Inspections //
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }*/
                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            switch (i_int_FocusedGrid)
            {
                case 1:  // Action //
                    if (!iBool_AllowDelete) return;
                    view = (GridView)gridControl1.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Actions to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    // Checks passed so delete selected record(s) //
                    strMessage = "You have " + (intCount == 1 ? "1 Action" : Convert.ToString(intRowHandles.Length) + " Actions") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this action" : "these actions") + " will no longer be available for selection!";
                    if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        frmProgress fProgress = new frmProgress(20);
                        fProgress.UpdateCaption("Deleting...");
                        fProgress.Show();
                        Application.DoEvents();

                        string strRecordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "ActionID")) + ",";
                        }
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                        DataSet_EPTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_EPTableAdapters.QueriesTableAdapter();
                        RemoveRecords.ChangeConnectionString(strConnectionString);
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        RemoveRecords.sp03034_EP_Action_Delete(strRecordIDs);  // Remove the records from the DB in one go //
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                        LoadLinkedRecords();

                        if (fProgress != null)
                        {
                            fProgress.UpdateProgress(20); // Update Progress Bar //
                            fProgress.Close();
                            fProgress = null;
                        }
                        // Notify any open forms which reference this data that they will need to refresh their data on activating //
                        Broadcast_Data_Refresh Broadcast = new Broadcast_Data_Refresh();
                        Broadcast.Asset_Action_Refresh(this.ParentForm, this.Name, "", "");

                        if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                /*case 3:  // Linked Documents //
                    if (!iBool_AllowDelete) return;
                    view = (GridView)gridControl2.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Linked Documents to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    // Checks passed so delete selected record(s) //
                    strMessage = "You have " + (intCount == 1 ? "1 Linked Document" : Convert.ToString(intRowHandles.Length) + " Linked Documents") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Linked Document" : "these Linked Documents") + " will no longer be available for selection but the files(s) will still exist on the computer!";
                    if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        frmProgress fProgress = new frmProgress(20);
                        fProgress.UpdateCaption("Deleting...");
                        fProgress.Show();
                        Application.DoEvents();

                        string strRecordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "LinkedDocumentID")) + ",";
                        }
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                        DataSet_ATTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_ATTableAdapters.QueriesTableAdapter();
                        RemoveRecords.ChangeConnectionString(strConnectionString);
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        RemoveRecords.sp00223_Linked_Document_Delete(strRecordIDs);  // Remove the records from the DB in one go //
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                        LoadLinkedRecords();
                        // Notify any open forms which reference this data that they will need to refresh their data on activating //
                        Broadcast_Data_Refresh Broadcast = new Broadcast_Data_Refresh();
                        Broadcast.LinkedDocument_Refresh(this.ParentForm, this.Name, "");

                        if (fProgress != null)
                        {
                            fProgress.UpdateProgress(20); // Update Progress Bar //
                            fProgress.Close();
                            fProgress = null;
                        }
                        if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;*/
                default:
                    break;
            }
        }

        public override void OnShowMapEvent(object sender, EventArgs e)
        {
            GridView view = null;
            string strSelectedIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            view = (GridView)gridControl1.MainView;
            view.PostEditor();
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to display on the map before proceeding.", "Show Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AssetID")) + ',';
            }
            try
            {
                Mapping_Functions MapFunctions = new Mapping_Functions();
                MapFunctions.Show_Map_From_Assets_Screen(this.GlobalSettings, strConnectionString, this, strSelectedIDs, "asset");
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to open the map [" + ex.Message + "].\n\nTry opening the map again. If the problem persists contact Technical Support.", "Show Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
        }



    }
}


namespace WoodPlan5
{
    partial class frm_EP_Scheduler_Add_Jobs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_EP_Scheduler_Add_Jobs));
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions7 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject25 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject26 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject27 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject28 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions8 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject29 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject30 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject31 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject32 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions9 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject33 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject34 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject35 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject36 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions10 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject37 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject38 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject39 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject40 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions11 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject41 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject42 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject43 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject44 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions12 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject45 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject46 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject47 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject48 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            this.colJobID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.buttonEditSiteFilter2 = new DevExpress.XtraEditors.ButtonEdit();
            this.buttonEditClientFilter2 = new DevExpress.XtraEditors.ButtonEdit();
            this.popupContainerEdit3 = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerControl3 = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp03038EPAssetJobsMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_EP = new WoodPlan5.DataSet_EP();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDefaultWorkUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobDisabled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colJobOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnJobFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerEdit2 = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerControl2 = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnAssetSubTypeFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp03021EPAssetTypeFilterDropDownBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAssetTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetTypeOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.btnLoadActions = new DevExpress.XtraEditors.SimpleButton();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp03085EPSchedulerEPJobsAvailableForAddingBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_Scheduler = new WoodPlan5.DataSet_Scheduler();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colActionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionDueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionDoneDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedManHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colSiteName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetTypeDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetTypeOrder1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetSubTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetSubTypeOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colXCoordinate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYCoordinate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPolygonXY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colArea = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLength = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWidth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetStatusDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetPartNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetSerialNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetModelNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetLifeSpanValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetLifeSpanValueDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetConditionDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetLastVisitDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetNextVisitDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMapID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionCompletionStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionTimeliness = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.buttonEditSiteFilter = new DevExpress.XtraEditors.ButtonEdit();
            this.buttonEditClientFilter = new DevExpress.XtraEditors.ButtonEdit();
            this.popupContainerEdit5 = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerControl5 = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl7 = new DevExpress.XtraGrid.GridControl();
            this.sp03086EPSchedulerATMasterJobTypesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colJobID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDefaultWorkUnits1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colDisabled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.btnJobTypeFilter2OK = new DevExpress.XtraEditors.SimpleButton();
            this.btnLoadActions2 = new DevExpress.XtraEditors.SimpleButton();
            this.dateEditToDate2 = new DevExpress.XtraEditors.DateEdit();
            this.dateEditFromDate2 = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl8 = new DevExpress.XtraGrid.GridControl();
            this.sp03087EPSchedulerATJobsAvailableForAddingBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colActionID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeSpeciesName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeDBH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeDBHRange = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeHeight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeHeightRange = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeXCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeYCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreePolygonXY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoneDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAction = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNearestHouse = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionPriority = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp03021_EP_Asset_Type_Filter_DropDownTableAdapter = new WoodPlan5.DataSet_EPTableAdapters.sp03021_EP_Asset_Type_Filter_DropDownTableAdapter();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.sp03038_EP_Asset_Jobs_MasterTableAdapter = new WoodPlan5.DataSet_EPTableAdapters.sp03038_EP_Asset_Jobs_MasterTableAdapter();
            this.sp03085_EP_Scheduler_EP_Jobs_Available_For_AddingTableAdapter = new WoodPlan5.DataSet_SchedulerTableAdapters.sp03085_EP_Scheduler_EP_Jobs_Available_For_AddingTableAdapter();
            this.sp03086_EP_Scheduler_AT_Master_Job_TypesTableAdapter = new WoodPlan5.DataSet_SchedulerTableAdapters.sp03086_EP_Scheduler_AT_Master_Job_TypesTableAdapter();
            this.sp03087_EP_Scheduler_AT_Jobs_Available_For_AddingTableAdapter = new WoodPlan5.DataSet_SchedulerTableAdapters.sp03087_EP_Scheduler_AT_Jobs_Available_For_AddingTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending8 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending2 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending5 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending4 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending7 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditSiteFilter2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditClientFilter2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl3)).BeginInit();
            this.popupContainerControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03038EPAssetJobsMasterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_EP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl2)).BeginInit();
            this.popupContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03021EPAssetTypeFilterDropDownBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03085EPSchedulerEPJobsAvailableForAddingBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Scheduler)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditSiteFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditClientFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl5)).BeginInit();
            this.popupContainerControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03086EPSchedulerATMasterJobTypesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate2.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate2.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03087EPSchedulerATJobsAvailableForAddingBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(938, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Size = new System.Drawing.Size(938, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 511);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(938, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 511);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colJobID
            // 
            this.colJobID.Caption = "Job ID";
            this.colJobID.FieldName = "JobID";
            this.colJobID.Name = "colJobID";
            this.colJobID.OptionsColumn.AllowEdit = false;
            this.colJobID.OptionsColumn.AllowFocus = false;
            this.colJobID.OptionsColumn.ReadOnly = true;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 26);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(938, 511);
            this.xtraTabControl1.TabIndex = 4;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.splitContainerControl1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(933, 485);
            this.xtraTabPage1.Text = "Asset Management Actions";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.Controls.Add(this.layoutControl1);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Asset Action Filter";
            this.splitContainerControl1.Panel2.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(933, 485);
            this.splitContainerControl1.SplitterPosition = 70;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.buttonEditSiteFilter2);
            this.layoutControl1.Controls.Add(this.buttonEditClientFilter2);
            this.layoutControl1.Controls.Add(this.popupContainerEdit3);
            this.layoutControl1.Controls.Add(this.popupContainerEdit2);
            this.layoutControl1.Controls.Add(this.dateEditToDate);
            this.layoutControl1.Controls.Add(this.btnLoadActions);
            this.layoutControl1.Controls.Add(this.dateEditFromDate);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(929, 46);
            this.layoutControl1.TabIndex = 2;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // buttonEditSiteFilter2
            // 
            this.buttonEditSiteFilter2.Location = new System.Drawing.Point(92, 26);
            this.buttonEditSiteFilter2.MenuManager = this.barManager1;
            this.buttonEditSiteFilter2.Name = "buttonEditSiteFilter2";
            this.buttonEditSiteFilter2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click to open Choose Site Filter Screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Clear Filter", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.buttonEditSiteFilter2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.buttonEditSiteFilter2.Size = new System.Drawing.Size(259, 20);
            this.buttonEditSiteFilter2.StyleController = this.layoutControl1;
            this.buttonEditSiteFilter2.TabIndex = 14;
            this.buttonEditSiteFilter2.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEditSiteFilter2_ButtonClick);
            // 
            // buttonEditClientFilter2
            // 
            this.buttonEditClientFilter2.Location = new System.Drawing.Point(92, 2);
            this.buttonEditClientFilter2.MenuManager = this.barManager1;
            this.buttonEditClientFilter2.Name = "buttonEditClientFilter2";
            this.buttonEditClientFilter2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Click to open Choose Client Filter Screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Clear Filter", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.buttonEditClientFilter2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.buttonEditClientFilter2.Size = new System.Drawing.Size(259, 20);
            this.buttonEditClientFilter2.StyleController = this.layoutControl1;
            this.buttonEditClientFilter2.TabIndex = 7;
            this.buttonEditClientFilter2.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEditClientFilter2_ButtonClick);
            // 
            // popupContainerEdit3
            // 
            this.popupContainerEdit3.EditValue = "No Action Type Filter";
            this.popupContainerEdit3.Location = new System.Drawing.Point(445, 2);
            this.popupContainerEdit3.MenuManager = this.barManager1;
            this.popupContainerEdit3.Name = "popupContainerEdit3";
            this.popupContainerEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEdit3.Properties.PopupControl = this.popupContainerControl3;
            this.popupContainerEdit3.Properties.ShowPopupCloseButton = false;
            this.popupContainerEdit3.Size = new System.Drawing.Size(149, 20);
            this.popupContainerEdit3.StyleController = this.layoutControl1;
            this.popupContainerEdit3.TabIndex = 11;
            this.popupContainerEdit3.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEdit3_QueryResultValue);
            // 
            // popupContainerControl3
            // 
            this.popupContainerControl3.Controls.Add(this.gridControl4);
            this.popupContainerControl3.Controls.Add(this.btnJobFilterOK);
            this.popupContainerControl3.Location = new System.Drawing.Point(329, 38);
            this.popupContainerControl3.Name = "popupContainerControl3";
            this.popupContainerControl3.Size = new System.Drawing.Size(312, 253);
            this.popupContainerControl3.TabIndex = 8;
            // 
            // gridControl4
            // 
            this.gridControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl4.DataSource = this.sp03038EPAssetJobsMasterBindingSource;
            this.gridControl4.Location = new System.Drawing.Point(3, 3);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.gridControl4.Size = new System.Drawing.Size(306, 222);
            this.gridControl4.TabIndex = 3;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp03038EPAssetJobsMasterBindingSource
            // 
            this.sp03038EPAssetJobsMasterBindingSource.DataMember = "sp03038_EP_Asset_Jobs_Master";
            this.sp03038EPAssetJobsMasterBindingSource.DataSource = this.dataSet_EP;
            // 
            // dataSet_EP
            // 
            this.dataSet_EP.DataSetName = "DataSet_EP";
            this.dataSet_EP.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.colDefaultWorkUnits,
            this.colJobCode,
            this.colJobDescription,
            this.colJobDisabled,
            this.colJobID,
            this.colJobOrder,
            this.colRemarks});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colJobID;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView4.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.GroupCount = 1;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsCustomization.AllowFilter = false;
            this.gridView4.OptionsFilter.AllowFilterEditor = false;
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colJobOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView4.GotFocus += new System.EventHandler(this.gridView4_GotFocus);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Asset Type";
            this.gridColumn1.FieldName = "AssetTypeDescription";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Width = 102;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Asset Type Order";
            this.gridColumn2.FieldName = "AssetTypeOrder";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Width = 108;
            // 
            // colDefaultWorkUnits
            // 
            this.colDefaultWorkUnits.Caption = "Default Work Units";
            this.colDefaultWorkUnits.FieldName = "DefaultWorkUnits";
            this.colDefaultWorkUnits.Name = "colDefaultWorkUnits";
            this.colDefaultWorkUnits.OptionsColumn.AllowEdit = false;
            this.colDefaultWorkUnits.OptionsColumn.AllowFocus = false;
            this.colDefaultWorkUnits.OptionsColumn.ReadOnly = true;
            this.colDefaultWorkUnits.Width = 115;
            // 
            // colJobCode
            // 
            this.colJobCode.Caption = "Job Code";
            this.colJobCode.FieldName = "JobCode";
            this.colJobCode.Name = "colJobCode";
            this.colJobCode.OptionsColumn.AllowEdit = false;
            this.colJobCode.OptionsColumn.AllowFocus = false;
            this.colJobCode.OptionsColumn.ReadOnly = true;
            this.colJobCode.Visible = true;
            this.colJobCode.VisibleIndex = 0;
            this.colJobCode.Width = 109;
            // 
            // colJobDescription
            // 
            this.colJobDescription.Caption = "Job Description";
            this.colJobDescription.FieldName = "JobDescription";
            this.colJobDescription.Name = "colJobDescription";
            this.colJobDescription.OptionsColumn.AllowEdit = false;
            this.colJobDescription.OptionsColumn.AllowFocus = false;
            this.colJobDescription.OptionsColumn.ReadOnly = true;
            this.colJobDescription.Visible = true;
            this.colJobDescription.VisibleIndex = 1;
            this.colJobDescription.Width = 281;
            // 
            // colJobDisabled
            // 
            this.colJobDisabled.Caption = "Job Disabled";
            this.colJobDisabled.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colJobDisabled.FieldName = "JobDisabled";
            this.colJobDisabled.Name = "colJobDisabled";
            this.colJobDisabled.OptionsColumn.AllowEdit = false;
            this.colJobDisabled.OptionsColumn.AllowFocus = false;
            this.colJobDisabled.OptionsColumn.ReadOnly = true;
            this.colJobDisabled.Visible = true;
            this.colJobDisabled.VisibleIndex = 2;
            this.colJobDisabled.Width = 84;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colJobOrder
            // 
            this.colJobOrder.Caption = "Job Order";
            this.colJobOrder.FieldName = "JobOrder";
            this.colJobOrder.Name = "colJobOrder";
            this.colJobOrder.OptionsColumn.AllowEdit = false;
            this.colJobOrder.OptionsColumn.AllowFocus = false;
            this.colJobOrder.OptionsColumn.ReadOnly = true;
            this.colJobOrder.Width = 86;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 3;
            this.colRemarks.Width = 70;
            // 
            // btnJobFilterOK
            // 
            this.btnJobFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnJobFilterOK.Location = new System.Drawing.Point(4, 227);
            this.btnJobFilterOK.Name = "btnJobFilterOK";
            this.btnJobFilterOK.Size = new System.Drawing.Size(75, 23);
            this.btnJobFilterOK.TabIndex = 2;
            this.btnJobFilterOK.Text = "OK";
            this.btnJobFilterOK.Click += new System.EventHandler(this.btnJobSubTypeFilterOK_Click);
            // 
            // popupContainerEdit2
            // 
            this.popupContainerEdit2.EditValue = "No Asset Type Filter";
            this.popupContainerEdit2.Location = new System.Drawing.Point(445, 26);
            this.popupContainerEdit2.MenuManager = this.barManager1;
            this.popupContainerEdit2.Name = "popupContainerEdit2";
            this.popupContainerEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEdit2.Properties.PopupControl = this.popupContainerControl2;
            this.popupContainerEdit2.Properties.ShowPopupCloseButton = false;
            this.popupContainerEdit2.Size = new System.Drawing.Size(149, 20);
            this.popupContainerEdit2.StyleController = this.layoutControl1;
            this.popupContainerEdit2.TabIndex = 10;
            this.popupContainerEdit2.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEdit2_QueryResultValue);
            // 
            // popupContainerControl2
            // 
            this.popupContainerControl2.Controls.Add(this.btnAssetSubTypeFilterOK);
            this.popupContainerControl2.Controls.Add(this.gridControl5);
            this.popupContainerControl2.Location = new System.Drawing.Point(11, 41);
            this.popupContainerControl2.Name = "popupContainerControl2";
            this.popupContainerControl2.Size = new System.Drawing.Size(312, 253);
            this.popupContainerControl2.TabIndex = 6;
            // 
            // btnAssetSubTypeFilterOK
            // 
            this.btnAssetSubTypeFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAssetSubTypeFilterOK.Location = new System.Drawing.Point(3, 227);
            this.btnAssetSubTypeFilterOK.Name = "btnAssetSubTypeFilterOK";
            this.btnAssetSubTypeFilterOK.Size = new System.Drawing.Size(75, 23);
            this.btnAssetSubTypeFilterOK.TabIndex = 1;
            this.btnAssetSubTypeFilterOK.Text = "OK";
            this.btnAssetSubTypeFilterOK.Click += new System.EventHandler(this.btnAssetSubTypeFilterOK_Click);
            // 
            // gridControl5
            // 
            this.gridControl5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl5.DataSource = this.sp03021EPAssetTypeFilterDropDownBindingSource;
            this.gridControl5.Location = new System.Drawing.Point(3, 4);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.Size = new System.Drawing.Size(306, 221);
            this.gridControl5.TabIndex = 0;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp03021EPAssetTypeFilterDropDownBindingSource
            // 
            this.sp03021EPAssetTypeFilterDropDownBindingSource.DataMember = "sp03021_EP_Asset_Type_Filter_DropDown";
            this.sp03021EPAssetTypeFilterDropDownBindingSource.DataSource = this.dataSet_EP;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAssetTypeID,
            this.colAssetTypeDescription,
            this.colAssetTypeOrder});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAssetTypeOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView5.GotFocus += new System.EventHandler(this.gridView5_GotFocus);
            // 
            // colAssetTypeID
            // 
            this.colAssetTypeID.Caption = "Asset Type ID";
            this.colAssetTypeID.FieldName = "AssetTypeID";
            this.colAssetTypeID.Name = "colAssetTypeID";
            this.colAssetTypeID.OptionsColumn.AllowEdit = false;
            this.colAssetTypeID.OptionsColumn.AllowFocus = false;
            this.colAssetTypeID.OptionsColumn.ReadOnly = true;
            this.colAssetTypeID.Width = 94;
            // 
            // colAssetTypeDescription
            // 
            this.colAssetTypeDescription.Caption = "Asset Type Description";
            this.colAssetTypeDescription.FieldName = "AssetTypeDescription";
            this.colAssetTypeDescription.Name = "colAssetTypeDescription";
            this.colAssetTypeDescription.OptionsColumn.AllowEdit = false;
            this.colAssetTypeDescription.OptionsColumn.AllowFocus = false;
            this.colAssetTypeDescription.OptionsColumn.ReadOnly = true;
            this.colAssetTypeDescription.Visible = true;
            this.colAssetTypeDescription.VisibleIndex = 0;
            this.colAssetTypeDescription.Width = 273;
            // 
            // colAssetTypeOrder
            // 
            this.colAssetTypeOrder.Caption = "Order";
            this.colAssetTypeOrder.FieldName = "AssetTypeOrder";
            this.colAssetTypeOrder.Name = "colAssetTypeOrder";
            this.colAssetTypeOrder.OptionsColumn.AllowEdit = false;
            this.colAssetTypeOrder.OptionsColumn.AllowFocus = false;
            this.colAssetTypeOrder.OptionsColumn.ReadOnly = true;
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(688, 26);
            this.dateEditToDate.MenuManager = this.barManager1;
            this.dateEditToDate.Name = "dateEditToDate";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Clear Date - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all Ac" +
    "tions will be loaded.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "", null, superToolTip1, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullValuePrompt = "No Date";
            this.dateEditToDate.Size = new System.Drawing.Size(126, 20);
            this.dateEditToDate.StyleController = this.layoutControl1;
            this.dateEditToDate.TabIndex = 9;
            this.dateEditToDate.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditToDate_ButtonClick);
            // 
            // btnLoadActions
            // 
            this.btnLoadActions.ImageOptions.Image = global::WoodPlan5.Properties.Resources.refresh_16x16;
            this.btnLoadActions.Location = new System.Drawing.Point(818, 2);
            this.btnLoadActions.Name = "btnLoadActions";
            this.btnLoadActions.Size = new System.Drawing.Size(92, 22);
            this.btnLoadActions.StyleController = this.layoutControl1;
            this.btnLoadActions.TabIndex = 4;
            this.btnLoadActions.Text = "Load Actions";
            this.btnLoadActions.Click += new System.EventHandler(this.btnLoadActions_Click);
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(688, 2);
            this.dateEditFromDate.MenuManager = this.barManager1;
            this.dateEditFromDate.Name = "dateEditFromDate";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Clear Date - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all Ac" +
    "tions will be loaded.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "", null, superToolTip2, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullValuePrompt = "No Date";
            this.dateEditFromDate.Size = new System.Drawing.Size(126, 20);
            this.dateEditFromDate.StyleController = this.layoutControl1;
            this.dateEditFromDate.TabIndex = 9;
            this.dateEditFromDate.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditFromDate_ButtonClick);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem6,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem5,
            this.layoutControlItem4});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(912, 48);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.btnLoadActions;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(816, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(96, 26);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(96, 26);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(96, 48);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.dateEditToDate;
            this.layoutControlItem3.CustomizationFormText = "To Date:";
            this.layoutControlItem3.Location = new System.Drawing.Point(596, 24);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(220, 24);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(220, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(220, 24);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "Action To Date:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(87, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.dateEditFromDate;
            this.layoutControlItem6.CustomizationFormText = "From Date:";
            this.layoutControlItem6.Location = new System.Drawing.Point(596, 0);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(220, 24);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(220, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(220, 24);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "Action From Date:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(87, 13);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.buttonEditClientFilter2;
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(353, 24);
            this.layoutControlItem13.Text = "Client:";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(87, 13);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.buttonEditSiteFilter2;
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(353, 24);
            this.layoutControlItem14.Text = "Site:";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(87, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.popupContainerEdit3;
            this.layoutControlItem5.CustomizationFormText = "Action Type(s):";
            this.layoutControlItem5.Location = new System.Drawing.Point(353, 0);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(145, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(243, 24);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "Action Type(s):";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(87, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.popupContainerEdit2;
            this.layoutControlItem4.CustomizationFormText = "Asset Sub-Types:";
            this.layoutControlItem4.Location = new System.Drawing.Point(353, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(243, 24);
            this.layoutControlItem4.Text = "Asset Type(s):";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(87, 13);
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.popupContainerControl2);
            this.gridSplitContainer1.Panel1.Controls.Add(this.popupContainerControl3);
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(933, 409);
            this.gridSplitContainer1.TabIndex = 6;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp03085EPSchedulerEPJobsAvailableForAddingBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1});
            this.gridControl1.Size = new System.Drawing.Size(933, 409);
            this.gridControl1.TabIndex = 7;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1,
            this.gridView3});
            // 
            // sp03085EPSchedulerEPJobsAvailableForAddingBindingSource
            // 
            this.sp03085EPSchedulerEPJobsAvailableForAddingBindingSource.DataMember = "sp03085_EP_Scheduler_EP_Jobs_Available_For_Adding";
            this.sp03085EPSchedulerEPJobsAvailableForAddingBindingSource.DataSource = this.dataSet_Scheduler;
            // 
            // dataSet_Scheduler
            // 
            this.dataSet_Scheduler.DataSetName = "DataSet_Scheduler";
            this.dataSet_Scheduler.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colActionID,
            this.colAssetID,
            this.colInspectionID,
            this.colActionNumber,
            this.colJobTypeID,
            this.colJobTypeDescription,
            this.colActionDueDate,
            this.colActionDoneDate,
            this.colEstimatedManHours,
            this.colEstimatedCost,
            this.colActionRemarks,
            this.colSiteName1,
            this.colSiteCode1,
            this.colClientName1,
            this.colClientCode1,
            this.colAssetTypeDescription1,
            this.colAssetTypeOrder1,
            this.colAssetSubTypeDescription,
            this.colAssetSubTypeOrder,
            this.colXCoordinate1,
            this.colYCoordinate1,
            this.colPolygonXY,
            this.colArea,
            this.colLength,
            this.colWidth,
            this.colAssetStatusDescription,
            this.colAssetPartNumber,
            this.colAssetSerialNumber,
            this.colAssetModelNumber,
            this.colAssetNumber,
            this.colAssetLifeSpanValue,
            this.colAssetLifeSpanValueDescriptor,
            this.colAssetConditionDescription,
            this.colAssetLastVisitDate,
            this.colAssetNextVisitDate,
            this.colAssetRemarks,
            this.colMapID,
            this.colActionCompletionStatus,
            this.colActionTimeliness,
            this.colInspectionDescription});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 3;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAssetNumber, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colActionDueDate, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.GridView_FocusedRowChanged_NoGroupSelection);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_NoGroupSelection);
            this.gridView1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseMove);
            // 
            // colActionID
            // 
            this.colActionID.Caption = "Action ID";
            this.colActionID.FieldName = "ActionID";
            this.colActionID.Name = "colActionID";
            this.colActionID.OptionsColumn.AllowEdit = false;
            this.colActionID.OptionsColumn.AllowFocus = false;
            this.colActionID.OptionsColumn.ReadOnly = true;
            // 
            // colAssetID
            // 
            this.colAssetID.Caption = "Asset ID";
            this.colAssetID.FieldName = "AssetID";
            this.colAssetID.Name = "colAssetID";
            this.colAssetID.OptionsColumn.AllowEdit = false;
            this.colAssetID.OptionsColumn.AllowFocus = false;
            this.colAssetID.OptionsColumn.ReadOnly = true;
            // 
            // colInspectionID
            // 
            this.colInspectionID.Caption = "Inspection ID";
            this.colInspectionID.FieldName = "InspectionID";
            this.colInspectionID.Name = "colInspectionID";
            this.colInspectionID.OptionsColumn.AllowEdit = false;
            this.colInspectionID.OptionsColumn.AllowFocus = false;
            this.colInspectionID.OptionsColumn.ReadOnly = true;
            this.colInspectionID.Width = 96;
            // 
            // colActionNumber
            // 
            this.colActionNumber.Caption = "Action Number";
            this.colActionNumber.FieldName = "ActionNumber";
            this.colActionNumber.Name = "colActionNumber";
            this.colActionNumber.OptionsColumn.AllowEdit = false;
            this.colActionNumber.OptionsColumn.AllowFocus = false;
            this.colActionNumber.OptionsColumn.ReadOnly = true;
            this.colActionNumber.Visible = true;
            this.colActionNumber.VisibleIndex = 0;
            this.colActionNumber.Width = 129;
            // 
            // colJobTypeID
            // 
            this.colJobTypeID.Caption = "Job Type ID";
            this.colJobTypeID.FieldName = "JobTypeID";
            this.colJobTypeID.Name = "colJobTypeID";
            this.colJobTypeID.OptionsColumn.AllowEdit = false;
            this.colJobTypeID.OptionsColumn.AllowFocus = false;
            this.colJobTypeID.OptionsColumn.ReadOnly = true;
            // 
            // colJobTypeDescription
            // 
            this.colJobTypeDescription.Caption = "Job Type";
            this.colJobTypeDescription.FieldName = "JobTypeDescription";
            this.colJobTypeDescription.Name = "colJobTypeDescription";
            this.colJobTypeDescription.OptionsColumn.AllowEdit = false;
            this.colJobTypeDescription.OptionsColumn.AllowFocus = false;
            this.colJobTypeDescription.OptionsColumn.ReadOnly = true;
            this.colJobTypeDescription.Visible = true;
            this.colJobTypeDescription.VisibleIndex = 1;
            this.colJobTypeDescription.Width = 174;
            // 
            // colActionDueDate
            // 
            this.colActionDueDate.Caption = "Due Date";
            this.colActionDueDate.FieldName = "ActionDueDate";
            this.colActionDueDate.Name = "colActionDueDate";
            this.colActionDueDate.OptionsColumn.AllowEdit = false;
            this.colActionDueDate.OptionsColumn.AllowFocus = false;
            this.colActionDueDate.OptionsColumn.ReadOnly = true;
            this.colActionDueDate.Visible = true;
            this.colActionDueDate.VisibleIndex = 2;
            this.colActionDueDate.Width = 90;
            // 
            // colActionDoneDate
            // 
            this.colActionDoneDate.Caption = "Done Date";
            this.colActionDoneDate.FieldName = "ActionDoneDate";
            this.colActionDoneDate.Name = "colActionDoneDate";
            this.colActionDoneDate.OptionsColumn.AllowEdit = false;
            this.colActionDoneDate.OptionsColumn.AllowFocus = false;
            this.colActionDoneDate.OptionsColumn.ReadOnly = true;
            this.colActionDoneDate.Visible = true;
            this.colActionDoneDate.VisibleIndex = 3;
            this.colActionDoneDate.Width = 97;
            // 
            // colEstimatedManHours
            // 
            this.colEstimatedManHours.Caption = "Estimated Man Hours";
            this.colEstimatedManHours.FieldName = "EstimatedManHours";
            this.colEstimatedManHours.Name = "colEstimatedManHours";
            this.colEstimatedManHours.OptionsColumn.AllowEdit = false;
            this.colEstimatedManHours.OptionsColumn.AllowFocus = false;
            this.colEstimatedManHours.OptionsColumn.ReadOnly = true;
            this.colEstimatedManHours.Visible = true;
            this.colEstimatedManHours.VisibleIndex = 4;
            this.colEstimatedManHours.Width = 130;
            // 
            // colEstimatedCost
            // 
            this.colEstimatedCost.Caption = "Estimated Cost";
            this.colEstimatedCost.FieldName = "EstimatedCost";
            this.colEstimatedCost.Name = "colEstimatedCost";
            this.colEstimatedCost.OptionsColumn.AllowEdit = false;
            this.colEstimatedCost.OptionsColumn.AllowFocus = false;
            this.colEstimatedCost.OptionsColumn.ReadOnly = true;
            this.colEstimatedCost.Visible = true;
            this.colEstimatedCost.VisibleIndex = 5;
            this.colEstimatedCost.Width = 97;
            // 
            // colActionRemarks
            // 
            this.colActionRemarks.Caption = "Action Remarks";
            this.colActionRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colActionRemarks.FieldName = "ActionRemarks";
            this.colActionRemarks.Name = "colActionRemarks";
            this.colActionRemarks.OptionsColumn.ReadOnly = true;
            this.colActionRemarks.Visible = true;
            this.colActionRemarks.VisibleIndex = 8;
            this.colActionRemarks.Width = 94;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colSiteName1
            // 
            this.colSiteName1.Caption = "Site Name";
            this.colSiteName1.FieldName = "SiteName";
            this.colSiteName1.Name = "colSiteName1";
            this.colSiteName1.OptionsColumn.AllowEdit = false;
            this.colSiteName1.OptionsColumn.AllowFocus = false;
            this.colSiteName1.OptionsColumn.ReadOnly = true;
            // 
            // colSiteCode1
            // 
            this.colSiteCode1.Caption = "SiteCode";
            this.colSiteCode1.FieldName = "SiteCode";
            this.colSiteCode1.Name = "colSiteCode1";
            this.colSiteCode1.OptionsColumn.AllowEdit = false;
            this.colSiteCode1.OptionsColumn.AllowFocus = false;
            this.colSiteCode1.OptionsColumn.ReadOnly = true;
            // 
            // colClientName1
            // 
            this.colClientName1.Caption = "Client Name";
            this.colClientName1.FieldName = "ClientName";
            this.colClientName1.Name = "colClientName1";
            this.colClientName1.OptionsColumn.AllowEdit = false;
            this.colClientName1.OptionsColumn.AllowFocus = false;
            this.colClientName1.OptionsColumn.ReadOnly = true;
            // 
            // colClientCode1
            // 
            this.colClientCode1.Caption = "Client Code";
            this.colClientCode1.FieldName = "ClientCode";
            this.colClientCode1.Name = "colClientCode1";
            this.colClientCode1.OptionsColumn.AllowEdit = false;
            this.colClientCode1.OptionsColumn.AllowFocus = false;
            this.colClientCode1.OptionsColumn.ReadOnly = true;
            // 
            // colAssetTypeDescription1
            // 
            this.colAssetTypeDescription1.Caption = "Asset Type";
            this.colAssetTypeDescription1.FieldName = "AssetTypeDescription";
            this.colAssetTypeDescription1.Name = "colAssetTypeDescription1";
            this.colAssetTypeDescription1.OptionsColumn.AllowEdit = false;
            this.colAssetTypeDescription1.OptionsColumn.AllowFocus = false;
            this.colAssetTypeDescription1.OptionsColumn.ReadOnly = true;
            this.colAssetTypeDescription1.Visible = true;
            this.colAssetTypeDescription1.VisibleIndex = 10;
            this.colAssetTypeDescription1.Width = 121;
            // 
            // colAssetTypeOrder1
            // 
            this.colAssetTypeOrder1.Caption = "Asset Type Order";
            this.colAssetTypeOrder1.FieldName = "AssetTypeOrder";
            this.colAssetTypeOrder1.Name = "colAssetTypeOrder1";
            this.colAssetTypeOrder1.OptionsColumn.AllowEdit = false;
            this.colAssetTypeOrder1.OptionsColumn.AllowFocus = false;
            this.colAssetTypeOrder1.OptionsColumn.ReadOnly = true;
            this.colAssetTypeOrder1.Width = 106;
            // 
            // colAssetSubTypeDescription
            // 
            this.colAssetSubTypeDescription.Caption = "Asset Sub-Type";
            this.colAssetSubTypeDescription.FieldName = "AssetSubTypeDescription";
            this.colAssetSubTypeDescription.Name = "colAssetSubTypeDescription";
            this.colAssetSubTypeDescription.OptionsColumn.AllowEdit = false;
            this.colAssetSubTypeDescription.OptionsColumn.AllowFocus = false;
            this.colAssetSubTypeDescription.OptionsColumn.ReadOnly = true;
            this.colAssetSubTypeDescription.Visible = true;
            this.colAssetSubTypeDescription.VisibleIndex = 11;
            this.colAssetSubTypeDescription.Width = 112;
            // 
            // colAssetSubTypeOrder
            // 
            this.colAssetSubTypeOrder.Caption = "Asset Sub-Type Order";
            this.colAssetSubTypeOrder.FieldName = "AssetSubTypeOrder";
            this.colAssetSubTypeOrder.Name = "colAssetSubTypeOrder";
            this.colAssetSubTypeOrder.OptionsColumn.AllowEdit = false;
            this.colAssetSubTypeOrder.OptionsColumn.AllowFocus = false;
            this.colAssetSubTypeOrder.OptionsColumn.ReadOnly = true;
            this.colAssetSubTypeOrder.Width = 136;
            // 
            // colXCoordinate1
            // 
            this.colXCoordinate1.Caption = "X Coordinate";
            this.colXCoordinate1.FieldName = "XCoordinate";
            this.colXCoordinate1.Name = "colXCoordinate1";
            this.colXCoordinate1.OptionsColumn.AllowEdit = false;
            this.colXCoordinate1.OptionsColumn.AllowFocus = false;
            this.colXCoordinate1.OptionsColumn.ReadOnly = true;
            // 
            // colYCoordinate1
            // 
            this.colYCoordinate1.Caption = "Y Coordinate";
            this.colYCoordinate1.FieldName = "YCoordinate";
            this.colYCoordinate1.Name = "colYCoordinate1";
            this.colYCoordinate1.OptionsColumn.AllowEdit = false;
            this.colYCoordinate1.OptionsColumn.AllowFocus = false;
            this.colYCoordinate1.OptionsColumn.ReadOnly = true;
            // 
            // colPolygonXY
            // 
            this.colPolygonXY.Caption = "Polygon XY Coordinates ";
            this.colPolygonXY.FieldName = "PolygonXY";
            this.colPolygonXY.Name = "colPolygonXY";
            this.colPolygonXY.OptionsColumn.ReadOnly = true;
            this.colPolygonXY.Width = 152;
            // 
            // colArea
            // 
            this.colArea.Caption = "Area";
            this.colArea.FieldName = "Area";
            this.colArea.Name = "colArea";
            this.colArea.OptionsColumn.AllowEdit = false;
            this.colArea.OptionsColumn.AllowFocus = false;
            this.colArea.OptionsColumn.ReadOnly = true;
            // 
            // colLength
            // 
            this.colLength.Caption = "Length (M)";
            this.colLength.FieldName = "Length";
            this.colLength.Name = "colLength";
            this.colLength.OptionsColumn.AllowEdit = false;
            this.colLength.OptionsColumn.AllowFocus = false;
            this.colLength.OptionsColumn.ReadOnly = true;
            // 
            // colWidth
            // 
            this.colWidth.Caption = "Width (M)";
            this.colWidth.FieldName = "Width";
            this.colWidth.Name = "colWidth";
            this.colWidth.OptionsColumn.AllowEdit = false;
            this.colWidth.OptionsColumn.AllowFocus = false;
            this.colWidth.OptionsColumn.ReadOnly = true;
            // 
            // colAssetStatusDescription
            // 
            this.colAssetStatusDescription.Caption = "Asset Status";
            this.colAssetStatusDescription.FieldName = "AssetStatusDescription";
            this.colAssetStatusDescription.Name = "colAssetStatusDescription";
            this.colAssetStatusDescription.OptionsColumn.AllowEdit = false;
            this.colAssetStatusDescription.OptionsColumn.AllowFocus = false;
            this.colAssetStatusDescription.OptionsColumn.ReadOnly = true;
            this.colAssetStatusDescription.Visible = true;
            this.colAssetStatusDescription.VisibleIndex = 12;
            this.colAssetStatusDescription.Width = 120;
            // 
            // colAssetPartNumber
            // 
            this.colAssetPartNumber.Caption = "Asset Part Number";
            this.colAssetPartNumber.FieldName = "AssetPartNumber";
            this.colAssetPartNumber.Name = "colAssetPartNumber";
            this.colAssetPartNumber.OptionsColumn.AllowEdit = false;
            this.colAssetPartNumber.OptionsColumn.AllowFocus = false;
            this.colAssetPartNumber.OptionsColumn.ReadOnly = true;
            this.colAssetPartNumber.Visible = true;
            this.colAssetPartNumber.VisibleIndex = 13;
            this.colAssetPartNumber.Width = 116;
            // 
            // colAssetSerialNumber
            // 
            this.colAssetSerialNumber.Caption = "Asset Serial Number";
            this.colAssetSerialNumber.FieldName = "AssetSerialNumber";
            this.colAssetSerialNumber.Name = "colAssetSerialNumber";
            this.colAssetSerialNumber.OptionsColumn.AllowEdit = false;
            this.colAssetSerialNumber.OptionsColumn.AllowFocus = false;
            this.colAssetSerialNumber.OptionsColumn.ReadOnly = true;
            this.colAssetSerialNumber.Visible = true;
            this.colAssetSerialNumber.VisibleIndex = 14;
            this.colAssetSerialNumber.Width = 126;
            // 
            // colAssetModelNumber
            // 
            this.colAssetModelNumber.Caption = "Asset Model Number";
            this.colAssetModelNumber.FieldName = "AssetModelNumber";
            this.colAssetModelNumber.Name = "colAssetModelNumber";
            this.colAssetModelNumber.OptionsColumn.AllowEdit = false;
            this.colAssetModelNumber.OptionsColumn.AllowFocus = false;
            this.colAssetModelNumber.OptionsColumn.ReadOnly = true;
            this.colAssetModelNumber.Visible = true;
            this.colAssetModelNumber.VisibleIndex = 15;
            this.colAssetModelNumber.Width = 128;
            // 
            // colAssetNumber
            // 
            this.colAssetNumber.Caption = "Asset Number";
            this.colAssetNumber.FieldName = "AssetNumber";
            this.colAssetNumber.Name = "colAssetNumber";
            this.colAssetNumber.OptionsColumn.AllowEdit = false;
            this.colAssetNumber.OptionsColumn.AllowFocus = false;
            this.colAssetNumber.OptionsColumn.ReadOnly = true;
            this.colAssetNumber.Width = 122;
            // 
            // colAssetLifeSpanValue
            // 
            this.colAssetLifeSpanValue.Caption = "Asset Life Span";
            this.colAssetLifeSpanValue.FieldName = "AssetLifeSpanValue";
            this.colAssetLifeSpanValue.Name = "colAssetLifeSpanValue";
            this.colAssetLifeSpanValue.OptionsColumn.AllowEdit = false;
            this.colAssetLifeSpanValue.OptionsColumn.AllowFocus = false;
            this.colAssetLifeSpanValue.OptionsColumn.ReadOnly = true;
            this.colAssetLifeSpanValue.Width = 104;
            // 
            // colAssetLifeSpanValueDescriptor
            // 
            this.colAssetLifeSpanValueDescriptor.Caption = "Asset Life Span Descriptor";
            this.colAssetLifeSpanValueDescriptor.FieldName = "AssetLifeSpanValueDescriptor";
            this.colAssetLifeSpanValueDescriptor.Name = "colAssetLifeSpanValueDescriptor";
            this.colAssetLifeSpanValueDescriptor.OptionsColumn.AllowEdit = false;
            this.colAssetLifeSpanValueDescriptor.OptionsColumn.AllowFocus = false;
            this.colAssetLifeSpanValueDescriptor.OptionsColumn.ReadOnly = true;
            this.colAssetLifeSpanValueDescriptor.Width = 161;
            // 
            // colAssetConditionDescription
            // 
            this.colAssetConditionDescription.Caption = "Asset Condition";
            this.colAssetConditionDescription.FieldName = "AssetConditionDescription";
            this.colAssetConditionDescription.Name = "colAssetConditionDescription";
            this.colAssetConditionDescription.OptionsColumn.AllowEdit = false;
            this.colAssetConditionDescription.OptionsColumn.AllowFocus = false;
            this.colAssetConditionDescription.OptionsColumn.ReadOnly = true;
            this.colAssetConditionDescription.Width = 117;
            // 
            // colAssetLastVisitDate
            // 
            this.colAssetLastVisitDate.Caption = "Asset Last Visit";
            this.colAssetLastVisitDate.FieldName = "AssetLastVisitDate";
            this.colAssetLastVisitDate.Name = "colAssetLastVisitDate";
            this.colAssetLastVisitDate.OptionsColumn.AllowEdit = false;
            this.colAssetLastVisitDate.OptionsColumn.AllowFocus = false;
            this.colAssetLastVisitDate.OptionsColumn.ReadOnly = true;
            this.colAssetLastVisitDate.Width = 112;
            // 
            // colAssetNextVisitDate
            // 
            this.colAssetNextVisitDate.Caption = "Asset Next Visit";
            this.colAssetNextVisitDate.FieldName = "AssetNextVisitDate";
            this.colAssetNextVisitDate.Name = "colAssetNextVisitDate";
            this.colAssetNextVisitDate.OptionsColumn.AllowEdit = false;
            this.colAssetNextVisitDate.OptionsColumn.AllowFocus = false;
            this.colAssetNextVisitDate.OptionsColumn.ReadOnly = true;
            this.colAssetNextVisitDate.Width = 110;
            // 
            // colAssetRemarks
            // 
            this.colAssetRemarks.Caption = "Asset Remarks";
            this.colAssetRemarks.FieldName = "AssetRemarks";
            this.colAssetRemarks.Name = "colAssetRemarks";
            this.colAssetRemarks.OptionsColumn.ReadOnly = true;
            this.colAssetRemarks.Width = 110;
            // 
            // colMapID
            // 
            this.colMapID.Caption = "Map ID";
            this.colMapID.FieldName = "MapID";
            this.colMapID.Name = "colMapID";
            this.colMapID.OptionsColumn.AllowEdit = false;
            this.colMapID.OptionsColumn.AllowFocus = false;
            this.colMapID.OptionsColumn.ReadOnly = true;
            // 
            // colActionCompletionStatus
            // 
            this.colActionCompletionStatus.Caption = "Completion Status";
            this.colActionCompletionStatus.FieldName = "ActionCompletionStatus";
            this.colActionCompletionStatus.Name = "colActionCompletionStatus";
            this.colActionCompletionStatus.OptionsColumn.AllowEdit = false;
            this.colActionCompletionStatus.OptionsColumn.AllowFocus = false;
            this.colActionCompletionStatus.OptionsColumn.ReadOnly = true;
            this.colActionCompletionStatus.Visible = true;
            this.colActionCompletionStatus.VisibleIndex = 6;
            this.colActionCompletionStatus.Width = 114;
            // 
            // colActionTimeliness
            // 
            this.colActionTimeliness.Caption = "Timeliness";
            this.colActionTimeliness.FieldName = "ActionTimeliness";
            this.colActionTimeliness.Name = "colActionTimeliness";
            this.colActionTimeliness.OptionsColumn.AllowEdit = false;
            this.colActionTimeliness.OptionsColumn.AllowFocus = false;
            this.colActionTimeliness.OptionsColumn.ReadOnly = true;
            this.colActionTimeliness.Visible = true;
            this.colActionTimeliness.VisibleIndex = 7;
            this.colActionTimeliness.Width = 71;
            // 
            // colInspectionDescription
            // 
            this.colInspectionDescription.Caption = "Inspection Number";
            this.colInspectionDescription.FieldName = "InspectionDescription";
            this.colInspectionDescription.Name = "colInspectionDescription";
            this.colInspectionDescription.OptionsColumn.ReadOnly = true;
            this.colInspectionDescription.Visible = true;
            this.colInspectionDescription.VisibleIndex = 9;
            this.colInspectionDescription.Width = 114;
            // 
            // gridView3
            // 
            this.gridView3.GridControl = this.gridControl1;
            this.gridView3.Name = "gridView3";
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.splitContainerControl2);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(933, 485);
            this.xtraTabPage2.Text = "Amenity Tree Actions";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl2.Panel1.Controls.Add(this.layoutControl2);
            this.splitContainerControl2.Panel1.ShowCaption = true;
            this.splitContainerControl2.Panel1.Text = "Amenity Trees Action Filter";
            this.splitContainerControl2.Panel2.Controls.Add(this.gridSplitContainer2);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(933, 485);
            this.splitContainerControl2.SplitterPosition = 74;
            this.splitContainerControl2.TabIndex = 0;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.buttonEditSiteFilter);
            this.layoutControl2.Controls.Add(this.buttonEditClientFilter);
            this.layoutControl2.Controls.Add(this.popupContainerEdit5);
            this.layoutControl2.Controls.Add(this.btnLoadActions2);
            this.layoutControl2.Controls.Add(this.dateEditToDate2);
            this.layoutControl2.Controls.Add(this.dateEditFromDate2);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(929, 50);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // buttonEditSiteFilter
            // 
            this.buttonEditSiteFilter.Location = new System.Drawing.Point(92, 26);
            this.buttonEditSiteFilter.MenuManager = this.barManager1;
            this.buttonEditSiteFilter.Name = "buttonEditSiteFilter";
            this.buttonEditSiteFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions7, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject25, serializableAppearanceObject26, serializableAppearanceObject27, serializableAppearanceObject28, "Click to open Choose Site Filter Screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions8, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject29, serializableAppearanceObject30, serializableAppearanceObject31, serializableAppearanceObject32, "Clear Filter", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.buttonEditSiteFilter.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.buttonEditSiteFilter.Size = new System.Drawing.Size(300, 20);
            this.buttonEditSiteFilter.StyleController = this.layoutControl2;
            this.buttonEditSiteFilter.TabIndex = 13;
            this.buttonEditSiteFilter.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEditSiteFilter_ButtonClick);
            // 
            // buttonEditClientFilter
            // 
            this.buttonEditClientFilter.Location = new System.Drawing.Point(92, 2);
            this.buttonEditClientFilter.MenuManager = this.barManager1;
            this.buttonEditClientFilter.Name = "buttonEditClientFilter";
            this.buttonEditClientFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions9, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject33, serializableAppearanceObject34, serializableAppearanceObject35, serializableAppearanceObject36, "Click to open Choose Client Filter Screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions10, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject37, serializableAppearanceObject38, serializableAppearanceObject39, serializableAppearanceObject40, "Clear Filter", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.buttonEditClientFilter.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.buttonEditClientFilter.Size = new System.Drawing.Size(300, 20);
            this.buttonEditClientFilter.StyleController = this.layoutControl2;
            this.buttonEditClientFilter.TabIndex = 6;
            this.buttonEditClientFilter.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEditClientFilter_ButtonClick);
            // 
            // popupContainerEdit5
            // 
            this.popupContainerEdit5.EditValue = "No Action Type Filter";
            this.popupContainerEdit5.Location = new System.Drawing.Point(682, 2);
            this.popupContainerEdit5.MenuManager = this.barManager1;
            this.popupContainerEdit5.Name = "popupContainerEdit5";
            this.popupContainerEdit5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEdit5.Properties.PopupControl = this.popupContainerControl5;
            this.popupContainerEdit5.Properties.ShowPopupCloseButton = false;
            this.popupContainerEdit5.Size = new System.Drawing.Size(245, 20);
            this.popupContainerEdit5.StyleController = this.layoutControl2;
            this.popupContainerEdit5.TabIndex = 11;
            this.popupContainerEdit5.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEdit5_QueryResultValue);
            // 
            // popupContainerControl5
            // 
            this.popupContainerControl5.Controls.Add(this.gridControl7);
            this.popupContainerControl5.Controls.Add(this.btnJobTypeFilter2OK);
            this.popupContainerControl5.Location = new System.Drawing.Point(179, 92);
            this.popupContainerControl5.Name = "popupContainerControl5";
            this.popupContainerControl5.Size = new System.Drawing.Size(312, 253);
            this.popupContainerControl5.TabIndex = 9;
            // 
            // gridControl7
            // 
            this.gridControl7.DataSource = this.sp03086EPSchedulerATMasterJobTypesBindingSource;
            this.gridControl7.Location = new System.Drawing.Point(4, 4);
            this.gridControl7.MainView = this.gridView7;
            this.gridControl7.MenuManager = this.barManager1;
            this.gridControl7.Name = "gridControl7";
            this.gridControl7.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit2,
            this.repositoryItemMemoExEdit2});
            this.gridControl7.Size = new System.Drawing.Size(305, 221);
            this.gridControl7.TabIndex = 5;
            this.gridControl7.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView7});
            // 
            // sp03086EPSchedulerATMasterJobTypesBindingSource
            // 
            this.sp03086EPSchedulerATMasterJobTypesBindingSource.DataMember = "sp03086_EP_Scheduler_AT_Master_Job_Types";
            this.sp03086EPSchedulerATMasterJobTypesBindingSource.DataSource = this.dataSet_Scheduler;
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colJobID1,
            this.colJobCode1,
            this.colJobDescription1,
            this.colDefaultWorkUnits1,
            this.colRemarks1,
            this.colDisabled});
            this.gridView7.GridControl = this.gridControl7;
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView7.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView7.OptionsLayout.StoreAppearance = true;
            this.gridView7.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView7.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView7.OptionsView.ColumnAutoWidth = false;
            this.gridView7.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            this.gridView7.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView7.OptionsView.ShowIndicator = false;
            this.gridView7.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colJobDescription1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView7.GotFocus += new System.EventHandler(this.gridView7_GotFocus);
            // 
            // colJobID1
            // 
            this.colJobID1.Caption = "Job ID";
            this.colJobID1.FieldName = "JobID";
            this.colJobID1.Name = "colJobID1";
            this.colJobID1.OptionsColumn.AllowEdit = false;
            this.colJobID1.OptionsColumn.AllowFocus = false;
            this.colJobID1.OptionsColumn.ReadOnly = true;
            // 
            // colJobCode1
            // 
            this.colJobCode1.Caption = "Job Code";
            this.colJobCode1.FieldName = "JobCode";
            this.colJobCode1.Name = "colJobCode1";
            this.colJobCode1.OptionsColumn.AllowEdit = false;
            this.colJobCode1.OptionsColumn.AllowFocus = false;
            this.colJobCode1.OptionsColumn.ReadOnly = true;
            // 
            // colJobDescription1
            // 
            this.colJobDescription1.Caption = "Job Description";
            this.colJobDescription1.FieldName = "JobDescription";
            this.colJobDescription1.Name = "colJobDescription1";
            this.colJobDescription1.OptionsColumn.AllowEdit = false;
            this.colJobDescription1.OptionsColumn.AllowFocus = false;
            this.colJobDescription1.OptionsColumn.ReadOnly = true;
            this.colJobDescription1.Visible = true;
            this.colJobDescription1.VisibleIndex = 0;
            this.colJobDescription1.Width = 214;
            // 
            // colDefaultWorkUnits1
            // 
            this.colDefaultWorkUnits1.Caption = "Default Work Units";
            this.colDefaultWorkUnits1.FieldName = "DefaultWorkUnits";
            this.colDefaultWorkUnits1.Name = "colDefaultWorkUnits1";
            this.colDefaultWorkUnits1.OptionsColumn.AllowEdit = false;
            this.colDefaultWorkUnits1.OptionsColumn.AllowFocus = false;
            this.colDefaultWorkUnits1.OptionsColumn.ReadOnly = true;
            this.colDefaultWorkUnits1.Width = 115;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colDisabled
            // 
            this.colDisabled.Caption = "Disabled";
            this.colDisabled.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colDisabled.FieldName = "Disabled";
            this.colDisabled.Name = "colDisabled";
            this.colDisabled.OptionsColumn.AllowEdit = false;
            this.colDisabled.OptionsColumn.AllowFocus = false;
            this.colDisabled.OptionsColumn.ReadOnly = true;
            this.colDisabled.Visible = true;
            this.colDisabled.VisibleIndex = 1;
            this.colDisabled.Width = 67;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // btnJobTypeFilter2OK
            // 
            this.btnJobTypeFilter2OK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnJobTypeFilter2OK.Location = new System.Drawing.Point(3, 227);
            this.btnJobTypeFilter2OK.Name = "btnJobTypeFilter2OK";
            this.btnJobTypeFilter2OK.Size = new System.Drawing.Size(75, 23);
            this.btnJobTypeFilter2OK.TabIndex = 4;
            this.btnJobTypeFilter2OK.Text = "OK";
            this.btnJobTypeFilter2OK.Click += new System.EventHandler(this.btnJobSubTypeFilterOK2_Click);
            // 
            // btnLoadActions2
            // 
            this.btnLoadActions2.ImageOptions.Image = global::WoodPlan5.Properties.Resources.refresh_16x16;
            this.btnLoadActions2.Location = new System.Drawing.Point(835, 26);
            this.btnLoadActions2.Name = "btnLoadActions2";
            this.btnLoadActions2.Size = new System.Drawing.Size(92, 22);
            this.btnLoadActions2.StyleController = this.layoutControl2;
            this.btnLoadActions2.TabIndex = 9;
            this.btnLoadActions2.Text = "Load Actions";
            this.btnLoadActions2.Click += new System.EventHandler(this.btnLoadActions2_Click);
            // 
            // dateEditToDate2
            // 
            this.dateEditToDate2.EditValue = null;
            this.dateEditToDate2.Location = new System.Drawing.Point(486, 2);
            this.dateEditToDate2.MenuManager = this.barManager1;
            this.dateEditToDate2.Name = "dateEditToDate2";
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Clear Date - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all Ac" +
    "tions will be loaded.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.dateEditToDate2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions11, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject41, serializableAppearanceObject42, serializableAppearanceObject43, serializableAppearanceObject44, "", null, superToolTip3, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dateEditToDate2.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate2.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate2.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate2.Properties.NullDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate2.Properties.NullValuePrompt = "No Date";
            this.dateEditToDate2.Size = new System.Drawing.Size(102, 20);
            this.dateEditToDate2.StyleController = this.layoutControl2;
            this.dateEditToDate2.TabIndex = 10;
            this.dateEditToDate2.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditToDate2_ButtonClick);
            // 
            // dateEditFromDate2
            // 
            this.dateEditFromDate2.EditValue = null;
            this.dateEditFromDate2.Location = new System.Drawing.Point(486, 26);
            this.dateEditFromDate2.MenuManager = this.barManager1;
            this.dateEditFromDate2.Name = "dateEditFromDate2";
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Text = "Clear Date - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all Ac" +
    "tions will be loaded.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.dateEditFromDate2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions12, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject45, serializableAppearanceObject46, serializableAppearanceObject47, serializableAppearanceObject48, "", null, superToolTip4, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dateEditFromDate2.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate2.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate2.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate2.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate2.Properties.NullValuePrompt = "No Date";
            this.dateEditFromDate2.Size = new System.Drawing.Size(102, 20);
            this.dateEditFromDate2.StyleController = this.layoutControl2;
            this.dateEditFromDate2.TabIndex = 10;
            this.dateEditFromDate2.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditFromDate2_ButtonClick);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem8,
            this.layoutControlItem10,
            this.emptySpaceItem2,
            this.layoutControlItem12,
            this.layoutControlItem7,
            this.layoutControlItem11,
            this.layoutControlItem9});
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(929, 50);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.dateEditFromDate2;
            this.layoutControlItem8.CustomizationFormText = "Action From Date:";
            this.layoutControlItem8.Location = new System.Drawing.Point(394, 24);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(196, 24);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(196, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(196, 26);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "Action From Date:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(87, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.btnLoadActions2;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(833, 24);
            this.layoutControlItem10.MaxSize = new System.Drawing.Size(96, 26);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(96, 26);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(96, 26);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(590, 24);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(243, 26);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.buttonEditClientFilter;
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(394, 24);
            this.layoutControlItem12.Text = "Client:";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(87, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.buttonEditSiteFilter;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(394, 26);
            this.layoutControlItem7.Text = "Site:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(87, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.popupContainerEdit5;
            this.layoutControlItem11.CustomizationFormText = "Action Type(s):";
            this.layoutControlItem11.Location = new System.Drawing.Point(590, 0);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(339, 24);
            this.layoutControlItem11.Text = "Action Type(s):";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(87, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.dateEditToDate2;
            this.layoutControlItem9.CustomizationFormText = "Action To Date:";
            this.layoutControlItem9.Location = new System.Drawing.Point(394, 0);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(196, 24);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(196, 24);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(196, 24);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "Action To Date:";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(87, 13);
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer2.Grid = this.gridControl8;
            this.gridSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.popupContainerControl5);
            this.gridSplitContainer2.Panel1.Controls.Add(this.gridControl8);
            this.gridSplitContainer2.Size = new System.Drawing.Size(933, 405);
            this.gridSplitContainer2.TabIndex = 9;
            // 
            // gridControl8
            // 
            this.gridControl8.DataSource = this.sp03087EPSchedulerATJobsAvailableForAddingBindingSource;
            this.gridControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl8.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl8.Location = new System.Drawing.Point(0, 0);
            this.gridControl8.MainView = this.gridView8;
            this.gridControl8.MenuManager = this.barManager1;
            this.gridControl8.Name = "gridControl8";
            this.gridControl8.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3});
            this.gridControl8.Size = new System.Drawing.Size(933, 405);
            this.gridControl8.TabIndex = 10;
            this.gridControl8.UseEmbeddedNavigator = true;
            this.gridControl8.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView8});
            // 
            // sp03087EPSchedulerATJobsAvailableForAddingBindingSource
            // 
            this.sp03087EPSchedulerATJobsAvailableForAddingBindingSource.DataMember = "sp03087_EP_Scheduler_AT_Jobs_Available_For_Adding";
            this.sp03087EPSchedulerATJobsAvailableForAddingBindingSource.DataSource = this.dataSet_Scheduler;
            // 
            // gridView8
            // 
            this.gridView8.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colActionID1,
            this.colInspectionID1,
            this.colClientName2,
            this.colSiteName2,
            this.colTreeReference,
            this.colTreeSpeciesName,
            this.colTreeDBH,
            this.colTreeDBHRange,
            this.colTreeHeight,
            this.colTreeHeightRange,
            this.colTreeXCoordinate,
            this.colTreeYCoordinate,
            this.colTreePolygonXY,
            this.colInspectionDate,
            this.colInspectionReference,
            this.colDueDate,
            this.colDoneDate,
            this.colAction,
            this.colActionBy,
            this.colJobNumber,
            this.colActionRemarks1,
            this.colNearestHouse,
            this.colActionPriority});
            this.gridView8.GridControl = this.gridControl8;
            this.gridView8.GroupCount = 2;
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView8.OptionsFind.AlwaysVisible = true;
            this.gridView8.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView8.OptionsLayout.StoreAppearance = true;
            this.gridView8.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView8.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView8.OptionsView.ColumnAutoWidth = false;
            this.gridView8.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            this.gridView8.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTreeReference, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colJobNumber, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView8.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView8.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView8.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.GridView_FocusedRowChanged_NoGroupSelection);
            this.gridView8.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView8.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView8.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_NoGroupSelection);
            this.gridView8.MouseMove += new System.Windows.Forms.MouseEventHandler(this.gridView8_MouseMove);
            // 
            // colActionID1
            // 
            this.colActionID1.Caption = "Action ID";
            this.colActionID1.FieldName = "ActionID";
            this.colActionID1.Name = "colActionID1";
            this.colActionID1.OptionsColumn.AllowEdit = false;
            this.colActionID1.OptionsColumn.AllowFocus = false;
            this.colActionID1.OptionsColumn.ReadOnly = true;
            // 
            // colInspectionID1
            // 
            this.colInspectionID1.Caption = "Inspection ID";
            this.colInspectionID1.FieldName = "InspectionID";
            this.colInspectionID1.Name = "colInspectionID1";
            this.colInspectionID1.OptionsColumn.AllowEdit = false;
            this.colInspectionID1.OptionsColumn.AllowFocus = false;
            this.colInspectionID1.OptionsColumn.ReadOnly = true;
            // 
            // colClientName2
            // 
            this.colClientName2.Caption = "Client Name";
            this.colClientName2.FieldName = "ClientName";
            this.colClientName2.Name = "colClientName2";
            this.colClientName2.OptionsColumn.AllowEdit = false;
            this.colClientName2.OptionsColumn.AllowFocus = false;
            this.colClientName2.OptionsColumn.ReadOnly = true;
            this.colClientName2.Width = 144;
            // 
            // colSiteName2
            // 
            this.colSiteName2.Caption = "Site Name";
            this.colSiteName2.FieldName = "SiteName";
            this.colSiteName2.Name = "colSiteName2";
            this.colSiteName2.OptionsColumn.AllowEdit = false;
            this.colSiteName2.OptionsColumn.AllowFocus = false;
            this.colSiteName2.OptionsColumn.ReadOnly = true;
            this.colSiteName2.Width = 179;
            // 
            // colTreeReference
            // 
            this.colTreeReference.Caption = "Tree Reference";
            this.colTreeReference.FieldName = "TreeReference";
            this.colTreeReference.Name = "colTreeReference";
            this.colTreeReference.OptionsColumn.AllowEdit = false;
            this.colTreeReference.OptionsColumn.AllowFocus = false;
            this.colTreeReference.OptionsColumn.ReadOnly = true;
            this.colTreeReference.Visible = true;
            this.colTreeReference.VisibleIndex = 0;
            this.colTreeReference.Width = 124;
            // 
            // colTreeSpeciesName
            // 
            this.colTreeSpeciesName.Caption = "Species";
            this.colTreeSpeciesName.FieldName = "TreeSpeciesName";
            this.colTreeSpeciesName.Name = "colTreeSpeciesName";
            this.colTreeSpeciesName.OptionsColumn.AllowEdit = false;
            this.colTreeSpeciesName.OptionsColumn.AllowFocus = false;
            this.colTreeSpeciesName.OptionsColumn.ReadOnly = true;
            this.colTreeSpeciesName.Visible = true;
            this.colTreeSpeciesName.VisibleIndex = 6;
            this.colTreeSpeciesName.Width = 91;
            // 
            // colTreeDBH
            // 
            this.colTreeDBH.Caption = "DBH";
            this.colTreeDBH.FieldName = "TreeDBH";
            this.colTreeDBH.Name = "colTreeDBH";
            this.colTreeDBH.OptionsColumn.AllowEdit = false;
            this.colTreeDBH.OptionsColumn.AllowFocus = false;
            this.colTreeDBH.OptionsColumn.ReadOnly = true;
            this.colTreeDBH.Visible = true;
            this.colTreeDBH.VisibleIndex = 7;
            this.colTreeDBH.Width = 50;
            // 
            // colTreeDBHRange
            // 
            this.colTreeDBHRange.Caption = "DBH Range";
            this.colTreeDBHRange.FieldName = "TreeDBHRange";
            this.colTreeDBHRange.Name = "colTreeDBHRange";
            this.colTreeDBHRange.OptionsColumn.AllowEdit = false;
            this.colTreeDBHRange.OptionsColumn.AllowFocus = false;
            this.colTreeDBHRange.OptionsColumn.ReadOnly = true;
            this.colTreeDBHRange.Visible = true;
            this.colTreeDBHRange.VisibleIndex = 8;
            this.colTreeDBHRange.Width = 100;
            // 
            // colTreeHeight
            // 
            this.colTreeHeight.Caption = "Height";
            this.colTreeHeight.FieldName = "TreeHeight";
            this.colTreeHeight.Name = "colTreeHeight";
            this.colTreeHeight.OptionsColumn.AllowEdit = false;
            this.colTreeHeight.OptionsColumn.AllowFocus = false;
            this.colTreeHeight.OptionsColumn.ReadOnly = true;
            this.colTreeHeight.Visible = true;
            this.colTreeHeight.VisibleIndex = 9;
            this.colTreeHeight.Width = 55;
            // 
            // colTreeHeightRange
            // 
            this.colTreeHeightRange.Caption = "Height Range";
            this.colTreeHeightRange.FieldName = "TreeHeightRange";
            this.colTreeHeightRange.Name = "colTreeHeightRange";
            this.colTreeHeightRange.OptionsColumn.AllowEdit = false;
            this.colTreeHeightRange.OptionsColumn.AllowFocus = false;
            this.colTreeHeightRange.OptionsColumn.ReadOnly = true;
            this.colTreeHeightRange.Visible = true;
            this.colTreeHeightRange.VisibleIndex = 10;
            this.colTreeHeightRange.Width = 114;
            // 
            // colTreeXCoordinate
            // 
            this.colTreeXCoordinate.Caption = "X Coordinate";
            this.colTreeXCoordinate.FieldName = "TreeXCoordinate";
            this.colTreeXCoordinate.Name = "colTreeXCoordinate";
            this.colTreeXCoordinate.OptionsColumn.AllowEdit = false;
            this.colTreeXCoordinate.OptionsColumn.AllowFocus = false;
            this.colTreeXCoordinate.OptionsColumn.ReadOnly = true;
            this.colTreeXCoordinate.Width = 87;
            // 
            // colTreeYCoordinate
            // 
            this.colTreeYCoordinate.Caption = "Y Coordinate";
            this.colTreeYCoordinate.FieldName = "TreeYCoordinate";
            this.colTreeYCoordinate.Name = "colTreeYCoordinate";
            this.colTreeYCoordinate.OptionsColumn.AllowEdit = false;
            this.colTreeYCoordinate.OptionsColumn.AllowFocus = false;
            this.colTreeYCoordinate.OptionsColumn.ReadOnly = true;
            this.colTreeYCoordinate.Width = 104;
            // 
            // colTreePolygonXY
            // 
            this.colTreePolygonXY.Caption = "Polygon XY";
            this.colTreePolygonXY.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colTreePolygonXY.FieldName = "TreePolygonXY";
            this.colTreePolygonXY.Name = "colTreePolygonXY";
            this.colTreePolygonXY.OptionsColumn.ReadOnly = true;
            this.colTreePolygonXY.Width = 103;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colInspectionDate
            // 
            this.colInspectionDate.Caption = "Inspection Date";
            this.colInspectionDate.FieldName = "InspectionDate";
            this.colInspectionDate.Name = "colInspectionDate";
            this.colInspectionDate.OptionsColumn.AllowEdit = false;
            this.colInspectionDate.OptionsColumn.AllowFocus = false;
            this.colInspectionDate.OptionsColumn.ReadOnly = true;
            this.colInspectionDate.Visible = true;
            this.colInspectionDate.VisibleIndex = 11;
            this.colInspectionDate.Width = 96;
            // 
            // colInspectionReference
            // 
            this.colInspectionReference.Caption = "Inspection Reference";
            this.colInspectionReference.FieldName = "InspectionReference";
            this.colInspectionReference.Name = "colInspectionReference";
            this.colInspectionReference.OptionsColumn.AllowEdit = false;
            this.colInspectionReference.OptionsColumn.AllowFocus = false;
            this.colInspectionReference.OptionsColumn.ReadOnly = true;
            this.colInspectionReference.Visible = true;
            this.colInspectionReference.VisibleIndex = 12;
            this.colInspectionReference.Width = 123;
            // 
            // colDueDate
            // 
            this.colDueDate.Caption = "Due Date";
            this.colDueDate.FieldName = "DueDate";
            this.colDueDate.Name = "colDueDate";
            this.colDueDate.OptionsColumn.AllowEdit = false;
            this.colDueDate.OptionsColumn.AllowFocus = false;
            this.colDueDate.OptionsColumn.ReadOnly = true;
            this.colDueDate.Visible = true;
            this.colDueDate.VisibleIndex = 3;
            // 
            // colDoneDate
            // 
            this.colDoneDate.Caption = "Done Date";
            this.colDoneDate.FieldName = "DoneDate";
            this.colDoneDate.Name = "colDoneDate";
            this.colDoneDate.OptionsColumn.AllowEdit = false;
            this.colDoneDate.OptionsColumn.AllowFocus = false;
            this.colDoneDate.OptionsColumn.ReadOnly = true;
            this.colDoneDate.Visible = true;
            this.colDoneDate.VisibleIndex = 13;
            // 
            // colAction
            // 
            this.colAction.Caption = "Action";
            this.colAction.FieldName = "Action";
            this.colAction.Name = "colAction";
            this.colAction.OptionsColumn.AllowEdit = false;
            this.colAction.OptionsColumn.AllowFocus = false;
            this.colAction.OptionsColumn.ReadOnly = true;
            this.colAction.Visible = true;
            this.colAction.VisibleIndex = 2;
            this.colAction.Width = 120;
            // 
            // colActionBy
            // 
            this.colActionBy.Caption = "Action By ID";
            this.colActionBy.FieldName = "ActionBy";
            this.colActionBy.Name = "colActionBy";
            this.colActionBy.OptionsColumn.AllowEdit = false;
            this.colActionBy.OptionsColumn.AllowFocus = false;
            this.colActionBy.OptionsColumn.ReadOnly = true;
            // 
            // colJobNumber
            // 
            this.colJobNumber.Caption = "Job Number";
            this.colJobNumber.FieldName = "JobNumber";
            this.colJobNumber.Name = "colJobNumber";
            this.colJobNumber.OptionsColumn.AllowEdit = false;
            this.colJobNumber.OptionsColumn.AllowFocus = false;
            this.colJobNumber.OptionsColumn.ReadOnly = true;
            this.colJobNumber.Visible = true;
            this.colJobNumber.VisibleIndex = 1;
            this.colJobNumber.Width = 95;
            // 
            // colActionRemarks1
            // 
            this.colActionRemarks1.Caption = "Action Remarks";
            this.colActionRemarks1.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colActionRemarks1.FieldName = "ActionRemarks";
            this.colActionRemarks1.Name = "colActionRemarks1";
            this.colActionRemarks1.OptionsColumn.ReadOnly = true;
            this.colActionRemarks1.Visible = true;
            this.colActionRemarks1.VisibleIndex = 5;
            this.colActionRemarks1.Width = 101;
            // 
            // colNearestHouse
            // 
            this.colNearestHouse.Caption = "Nearest House";
            this.colNearestHouse.FieldName = "NearestHouse";
            this.colNearestHouse.Name = "colNearestHouse";
            this.colNearestHouse.OptionsColumn.AllowEdit = false;
            this.colNearestHouse.OptionsColumn.AllowFocus = false;
            this.colNearestHouse.OptionsColumn.ReadOnly = true;
            this.colNearestHouse.Width = 95;
            // 
            // colActionPriority
            // 
            this.colActionPriority.Caption = "Action Priority";
            this.colActionPriority.FieldName = "ActionPriority";
            this.colActionPriority.Name = "colActionPriority";
            this.colActionPriority.OptionsColumn.AllowEdit = false;
            this.colActionPriority.OptionsColumn.AllowFocus = false;
            this.colActionPriority.OptionsColumn.ReadOnly = true;
            this.colActionPriority.Visible = true;
            this.colActionPriority.VisibleIndex = 4;
            this.colActionPriority.Width = 106;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp03021_EP_Asset_Type_Filter_DropDownTableAdapter
            // 
            this.sp03021_EP_Asset_Type_Filter_DropDownTableAdapter.ClearBeforeFill = true;
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl1.Appearance.Image")));
            this.labelControl1.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControl1.Appearance.Options.UseBackColor = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Appearance.Options.UseImage = true;
            this.labelControl1.Appearance.Options.UseImageAlign = true;
            this.labelControl1.Appearance.Options.UseTextOptions = true;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.labelControl1.Location = new System.Drawing.Point(542, 29);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(392, 16);
            this.labelControl1.TabIndex = 5;
            this.labelControl1.Text = "        Drag and Drop from List of Actions onto Scheduler to add action to schedu" +
    "le.";
            // 
            // sp03038_EP_Asset_Jobs_MasterTableAdapter
            // 
            this.sp03038_EP_Asset_Jobs_MasterTableAdapter.ClearBeforeFill = true;
            // 
            // sp03085_EP_Scheduler_EP_Jobs_Available_For_AddingTableAdapter
            // 
            this.sp03085_EP_Scheduler_EP_Jobs_Available_For_AddingTableAdapter.ClearBeforeFill = true;
            // 
            // sp03086_EP_Scheduler_AT_Master_Job_TypesTableAdapter
            // 
            this.sp03086_EP_Scheduler_AT_Master_Job_TypesTableAdapter.ClearBeforeFill = true;
            // 
            // sp03087_EP_Scheduler_AT_Jobs_Available_For_AddingTableAdapter
            // 
            this.sp03087_EP_Scheduler_AT_Jobs_Available_For_AddingTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // xtraGridBlending8
            // 
            this.xtraGridBlending8.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending8.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending8.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending8.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending8.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending8.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending8.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending8.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending8.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending8.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending8.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending8.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending8.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending8.GridControl = this.gridControl8;
            // 
            // xtraGridBlending2
            // 
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending2.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Preview", 125);
            // 
            // xtraGridBlending5
            // 
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending5.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending5.GridControl = this.gridControl5;
            // 
            // xtraGridBlending4
            // 
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending4.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending4.GridControl = this.gridControl4;
            // 
            // xtraGridBlending7
            // 
            this.xtraGridBlending7.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending7.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending7.GridControl = this.gridControl7;
            // 
            // frm_EP_Scheduler_Add_Jobs
            // 
            this.ClientSize = new System.Drawing.Size(938, 537);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.xtraTabControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_EP_Scheduler_Add_Jobs";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add Actions to Schedule  -  [Drag and Drop from list of Actions to Scheduler]";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_EP_Scheduler_Add_Jobs_FormClosing);
            this.Load += new System.EventHandler(this.frm_EP_Scheduler_Add_Jobs_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.xtraTabControl1, 0);
            this.Controls.SetChildIndex(this.labelControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditSiteFilter2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditClientFilter2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl3)).EndInit();
            this.popupContainerControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03038EPAssetJobsMasterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_EP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl2)).EndInit();
            this.popupContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03021EPAssetTypeFilterDropDownBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03085EPSchedulerEPJobsAvailableForAddingBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Scheduler)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditSiteFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditClientFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl5)).EndInit();
            this.popupContainerControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03086EPSchedulerATMasterJobTypesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate2.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate2.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03087EPSchedulerATJobsAvailableForAddingBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControl2;
        private DevExpress.XtraEditors.SimpleButton btnAssetSubTypeFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetTypeOrder;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEdit2;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraEditors.SimpleButton btnLoadActions;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DataSet_EP dataSet_EP;
        private System.Windows.Forms.BindingSource sp03021EPAssetTypeFilterDropDownBindingSource;
        private WoodPlan5.DataSet_EPTableAdapters.sp03021_EP_Asset_Type_Filter_DropDownTableAdapter sp03021_EP_Asset_Type_Filter_DropDownTableAdapter;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEdit3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControl3;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraEditors.SimpleButton btnJobFilterOK;
        private System.Windows.Forms.BindingSource sp03038EPAssetJobsMasterBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colJobID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobCode;
        private DevExpress.XtraGrid.Columns.GridColumn colJobDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colJobOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colDefaultWorkUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colJobDisabled;
        private WoodPlan5.DataSet_EPTableAdapters.sp03038_EP_Asset_Jobs_MasterTableAdapter sp03038_EP_Asset_Jobs_MasterTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private System.Windows.Forms.BindingSource sp03085EPSchedulerEPJobsAvailableForAddingBindingSource;
        private DataSet_Scheduler dataSet_Scheduler;
        private DevExpress.XtraGrid.Columns.GridColumn colActionID;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetID;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionID;
        private DevExpress.XtraGrid.Columns.GridColumn colActionNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colActionDueDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionDoneDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedManHours;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActionRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetTypeDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetTypeOrder1;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetSubTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetSubTypeOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colXCoordinate1;
        private DevExpress.XtraGrid.Columns.GridColumn colYCoordinate1;
        private DevExpress.XtraGrid.Columns.GridColumn colPolygonXY;
        private DevExpress.XtraGrid.Columns.GridColumn colArea;
        private DevExpress.XtraGrid.Columns.GridColumn colLength;
        private DevExpress.XtraGrid.Columns.GridColumn colWidth;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetStatusDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetPartNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetSerialNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetModelNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetLifeSpanValue;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetLifeSpanValueDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetConditionDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetLastVisitDate;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetNextVisitDate;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colMapID;
        private DevExpress.XtraGrid.Columns.GridColumn colActionCompletionStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colActionTimeliness;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionDescription;
        private WoodPlan5.DataSet_SchedulerTableAdapters.sp03085_EP_Scheduler_EP_Jobs_Available_For_AddingTableAdapter sp03085_EP_Scheduler_EP_Jobs_Available_For_AddingTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DataSet_AT dataSet_AT;
        private DevExpress.XtraEditors.SimpleButton btnLoadActions2;
        private DevExpress.XtraEditors.DateEdit dateEditToDate2;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControl5;
        private DevExpress.XtraEditors.SimpleButton btnJobTypeFilter2OK;
        private DevExpress.XtraGrid.GridControl gridControl7;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private System.Windows.Forms.BindingSource sp03086EPSchedulerATMasterJobTypesBindingSource;
        private WoodPlan5.DataSet_SchedulerTableAdapters.sp03086_EP_Scheduler_AT_Master_Job_TypesTableAdapter sp03086_EP_Scheduler_AT_Master_Job_TypesTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colJobID1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colDefaultWorkUnits1;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colDisabled;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEdit5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraGrid.GridControl gridControl8;
        private System.Windows.Forms.BindingSource sp03087EPSchedulerATJobsAvailableForAddingBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private DevExpress.XtraGrid.Columns.GridColumn colActionID1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName2;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReference;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeSpeciesName;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeDBH;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeDBHRange;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeHeight;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeHeightRange;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeXCoordinate;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeYCoordinate;
        private DevExpress.XtraGrid.Columns.GridColumn colTreePolygonXY;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionReference;
        private DevExpress.XtraGrid.Columns.GridColumn colDueDate;
        private DevExpress.XtraGrid.Columns.GridColumn colDoneDate;
        private DevExpress.XtraGrid.Columns.GridColumn colAction;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBy;
        private DevExpress.XtraGrid.Columns.GridColumn colJobNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colActionRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colNearestHouse;
        private DevExpress.XtraGrid.Columns.GridColumn colActionPriority;
        private WoodPlan5.DataSet_SchedulerTableAdapters.sp03087_EP_Scheduler_AT_Jobs_Available_For_AddingTableAdapter sp03087_EP_Scheduler_AT_Jobs_Available_For_AddingTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private DevExpress.XtraEditors.ButtonEdit buttonEditClientFilter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraEditors.ButtonEdit buttonEditSiteFilter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending8;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending2;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending5;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending4;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending7;
        private DevExpress.XtraEditors.ButtonEdit buttonEditSiteFilter2;
        private DevExpress.XtraEditors.ButtonEdit buttonEditClientFilter2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
    }
}

namespace WoodPlan5
{
    partial class frm_EP_Action_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_EP_Action_Edit));
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem9 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem9 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject25 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject26 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject27 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject28 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip10 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem10 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem10 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject29 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject30 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject31 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject32 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip11 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem11 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem11 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject33 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject34 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject35 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject36 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject37 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject38 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject39 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject40 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject41 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject42 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject43 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject44 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.colJobID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar4 = new DevExpress.XtraBars.Bar();
            this.bbiShowMapButton = new DevExpress.XtraBars.BarButtonItem();
            this.bciShowPrevious = new DevExpress.XtraBars.BarCheckItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.ActionIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.sp03036EPAssetActionEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_EP_DataEntry = new WoodPlan5.DataSet_EP_DataEntry();
            this.AssetIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.InspectionIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp00220LinkedDocumentsListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLinkedDocumentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colDocumentExtension = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateAdded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedRecordDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.ActionNumberButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.ActionDueDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.ActionDoneDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.EstimatedManHoursSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.EstimatedCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.AssetDescriptionButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.InspectionDescriptionButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.JobTypeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp03041EPAssetMasterJobsWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAssetTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetTypeOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDefaultWorkUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobDisabled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ItemForActionID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAssetID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForInspectionID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForActionNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForActionDueDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForActionDoneDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEstimatedManHours = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEstimatedCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAssetDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForInspectionDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp03036_EP_Asset_Action_EditTableAdapter = new WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03036_EP_Asset_Action_EditTableAdapter();
            this.sp03041_EP_Asset_Master_Jobs_With_BlankTableAdapter = new WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03041_EP_Asset_Master_Jobs_With_BlankTableAdapter();
            this.sp00220_Linked_Documents_ListTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00220_Linked_Documents_ListTableAdapter();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ActionIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03036EPAssetActionEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_EP_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AssetIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InspectionIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00220LinkedDocumentsListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActionNumberButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActionDueDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActionDueDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActionDoneDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActionDoneDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EstimatedManHoursSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EstimatedCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AssetDescriptionButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InspectionDescriptionButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobTypeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03041EPAssetMasterJobsWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActionID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAssetID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInspectionID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActionNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActionDueDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActionDoneDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEstimatedManHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEstimatedCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAssetDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInspectionDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 532);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 506);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(628, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 506);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // colJobID
            // 
            this.colJobID.Caption = "Job ID";
            this.colJobID.FieldName = "JobID";
            this.colJobID.Name = "colJobID";
            this.colJobID.OptionsColumn.AllowEdit = false;
            this.colJobID.OptionsColumn.AllowFocus = false;
            this.colJobID.OptionsColumn.ReadOnly = true;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3,
            this.bar4});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation,
            this.bciShowPrevious,
            this.bbiShowMapButton});
            this.barManager2.MaxItemId = 27;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bar4
            // 
            this.bar4.BarName = "Custom 5";
            this.bar4.DockCol = 1;
            this.bar4.DockRow = 0;
            this.bar4.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar4.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiShowMapButton),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciShowPrevious, true)});
            this.bar4.Text = "Custom 5";
            // 
            // bbiShowMapButton
            // 
            this.bbiShowMapButton.Caption = "Show Map";
            this.bbiShowMapButton.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiShowMapButton.Glyph")));
            this.bbiShowMapButton.Id = 26;
            this.bbiShowMapButton.Name = "bbiShowMapButton";
            toolTipTitleItem4.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Text = "View On Map - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to view the parent action on the map.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.bbiShowMapButton.SuperTip = superToolTip4;
            this.bbiShowMapButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiShowMapButton_ItemClick);
            // 
            // bciShowPrevious
            // 
            this.bciShowPrevious.BindableChecked = true;
            this.bciShowPrevious.Caption = "Show Previous Details";
            this.bciShowPrevious.Checked = true;
            this.bciShowPrevious.Enabled = false;
            this.bciShowPrevious.Glyph = ((System.Drawing.Image)(resources.GetObject("bciShowPrevious.Glyph")));
            this.bciShowPrevious.Id = 23;
            this.bciShowPrevious.Name = "bciShowPrevious";
            toolTipTitleItem5.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Text = "Show Last Action - Information\r\n";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to show \\ hide the Last Action Details Panel.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.bciShowPrevious.SuperTip = superToolTip5;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(628, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 532);
            this.barDockControl2.Size = new System.Drawing.Size(628, 28);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 506);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(628, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 506);
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.ActionIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AssetIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.InspectionIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.gridControl1);
            this.dataLayoutControl1.Controls.Add(this.ActionNumberButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.ActionDueDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.ActionDoneDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.EstimatedManHoursSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.EstimatedCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.AssetDescriptionButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.InspectionDescriptionButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.JobTypeIDGridLookUpEdit);
            this.dataLayoutControl1.DataSource = this.sp03036EPAssetActionEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForActionID,
            this.ItemForAssetID,
            this.ItemForInspectionID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1150, 330, 450, 350);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(628, 506);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // ActionIDTextEdit
            // 
            this.ActionIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03036EPAssetActionEditBindingSource, "ActionID", true));
            this.ActionIDTextEdit.Location = new System.Drawing.Point(122, 12);
            this.ActionIDTextEdit.MenuManager = this.barManager1;
            this.ActionIDTextEdit.Name = "ActionIDTextEdit";
            this.ActionIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ActionIDTextEdit, true);
            this.ActionIDTextEdit.Size = new System.Drawing.Size(494, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ActionIDTextEdit, optionsSpelling1);
            this.ActionIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ActionIDTextEdit.TabIndex = 4;
            // 
            // sp03036EPAssetActionEditBindingSource
            // 
            this.sp03036EPAssetActionEditBindingSource.DataMember = "sp03036_EP_Asset_Action_Edit";
            this.sp03036EPAssetActionEditBindingSource.DataSource = this.dataSet_EP_DataEntry;
            // 
            // dataSet_EP_DataEntry
            // 
            this.dataSet_EP_DataEntry.DataSetName = "DataSet_EP_DataEntry";
            this.dataSet_EP_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // AssetIDTextEdit
            // 
            this.AssetIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03036EPAssetActionEditBindingSource, "AssetID", true));
            this.AssetIDTextEdit.Location = new System.Drawing.Point(122, 12);
            this.AssetIDTextEdit.MenuManager = this.barManager1;
            this.AssetIDTextEdit.Name = "AssetIDTextEdit";
            this.AssetIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AssetIDTextEdit, true);
            this.AssetIDTextEdit.Size = new System.Drawing.Size(494, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AssetIDTextEdit, optionsSpelling2);
            this.AssetIDTextEdit.StyleController = this.dataLayoutControl1;
            this.AssetIDTextEdit.TabIndex = 5;
            // 
            // InspectionIDTextEdit
            // 
            this.InspectionIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03036EPAssetActionEditBindingSource, "InspectionID", true));
            this.InspectionIDTextEdit.Location = new System.Drawing.Point(122, 12);
            this.InspectionIDTextEdit.MenuManager = this.barManager1;
            this.InspectionIDTextEdit.Name = "InspectionIDTextEdit";
            this.InspectionIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.InspectionIDTextEdit, true);
            this.InspectionIDTextEdit.Size = new System.Drawing.Size(494, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.InspectionIDTextEdit, optionsSpelling3);
            this.InspectionIDTextEdit.StyleController = this.dataLayoutControl1;
            this.InspectionIDTextEdit.TabIndex = 6;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp00220LinkedDocumentsListBindingSource;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(36, 319);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit1,
            this.repositoryItemMemoExEdit2});
            this.gridControl1.Size = new System.Drawing.Size(556, 141);
            this.gridControl1.TabIndex = 19;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp00220LinkedDocumentsListBindingSource
            // 
            this.sp00220LinkedDocumentsListBindingSource.DataMember = "sp00220_Linked_Documents_List";
            this.sp00220LinkedDocumentsListBindingSource.DataSource = this.dataSet_AT;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLinkedDocumentID,
            this.colLinkedToRecordID,
            this.colLinkedToRecordTypeID,
            this.colDocumentPath,
            this.colDocumentExtension,
            this.colDescription,
            this.colAddedByStaffID,
            this.colDateAdded,
            this.colLinkedRecordDescription,
            this.colAddedByStaffName,
            this.colDocumentRemarks});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedRecordDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateAdded, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colLinkedDocumentID
            // 
            this.colLinkedDocumentID.Caption = "Linked Document ID";
            this.colLinkedDocumentID.FieldName = "LinkedDocumentID";
            this.colLinkedDocumentID.Name = "colLinkedDocumentID";
            this.colLinkedDocumentID.OptionsColumn.AllowEdit = false;
            this.colLinkedDocumentID.OptionsColumn.AllowFocus = false;
            this.colLinkedDocumentID.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentID.Width = 116;
            // 
            // colLinkedToRecordID
            // 
            this.colLinkedToRecordID.Caption = "Linked Record ID";
            this.colLinkedToRecordID.FieldName = "LinkedToRecordID";
            this.colLinkedToRecordID.Name = "colLinkedToRecordID";
            this.colLinkedToRecordID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordID.Width = 102;
            // 
            // colLinkedToRecordTypeID
            // 
            this.colLinkedToRecordTypeID.Caption = "Linked Record Type ID";
            this.colLinkedToRecordTypeID.FieldName = "LinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.Name = "colLinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordTypeID.Width = 129;
            // 
            // colDocumentPath
            // 
            this.colDocumentPath.Caption = "Document";
            this.colDocumentPath.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colDocumentPath.FieldName = "DocumentPath";
            this.colDocumentPath.Name = "colDocumentPath";
            this.colDocumentPath.OptionsColumn.ReadOnly = true;
            this.colDocumentPath.Visible = true;
            this.colDocumentPath.VisibleIndex = 2;
            this.colDocumentPath.Width = 360;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            this.repositoryItemHyperLinkEdit1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit1_OpenLink);
            // 
            // colDocumentExtension
            // 
            this.colDocumentExtension.Caption = "Type";
            this.colDocumentExtension.FieldName = "DocumentExtension";
            this.colDocumentExtension.Name = "colDocumentExtension";
            this.colDocumentExtension.OptionsColumn.AllowEdit = false;
            this.colDocumentExtension.OptionsColumn.AllowFocus = false;
            this.colDocumentExtension.OptionsColumn.ReadOnly = true;
            this.colDocumentExtension.Visible = true;
            this.colDocumentExtension.VisibleIndex = 3;
            this.colDocumentExtension.Width = 84;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 1;
            this.colDescription.Width = 319;
            // 
            // colAddedByStaffID
            // 
            this.colAddedByStaffID.Caption = "Added By Staff ID";
            this.colAddedByStaffID.FieldName = "AddedByStaffID";
            this.colAddedByStaffID.Name = "colAddedByStaffID";
            this.colAddedByStaffID.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffID.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffID.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffID.Width = 108;
            // 
            // colDateAdded
            // 
            this.colDateAdded.Caption = "Date Added";
            this.colDateAdded.FieldName = "DateAdded";
            this.colDateAdded.Name = "colDateAdded";
            this.colDateAdded.OptionsColumn.AllowEdit = false;
            this.colDateAdded.OptionsColumn.AllowFocus = false;
            this.colDateAdded.OptionsColumn.ReadOnly = true;
            this.colDateAdded.Visible = true;
            this.colDateAdded.VisibleIndex = 0;
            this.colDateAdded.Width = 91;
            // 
            // colLinkedRecordDescription
            // 
            this.colLinkedRecordDescription.Caption = "Linked To";
            this.colLinkedRecordDescription.FieldName = "LinkedRecordDescription";
            this.colLinkedRecordDescription.Name = "colLinkedRecordDescription";
            this.colLinkedRecordDescription.OptionsColumn.AllowEdit = false;
            this.colLinkedRecordDescription.OptionsColumn.AllowFocus = false;
            this.colLinkedRecordDescription.OptionsColumn.ReadOnly = true;
            this.colLinkedRecordDescription.Width = 131;
            // 
            // colAddedByStaffName
            // 
            this.colAddedByStaffName.Caption = "Added By";
            this.colAddedByStaffName.FieldName = "AddedByStaffName";
            this.colAddedByStaffName.Name = "colAddedByStaffName";
            this.colAddedByStaffName.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffName.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffName.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffName.Visible = true;
            this.colAddedByStaffName.VisibleIndex = 4;
            this.colAddedByStaffName.Width = 138;
            // 
            // colDocumentRemarks
            // 
            this.colDocumentRemarks.Caption = "Remarks";
            this.colDocumentRemarks.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colDocumentRemarks.FieldName = "DocumentRemarks";
            this.colDocumentRemarks.Name = "colDocumentRemarks";
            this.colDocumentRemarks.OptionsColumn.ReadOnly = true;
            this.colDocumentRemarks.Visible = true;
            this.colDocumentRemarks.VisibleIndex = 5;
            this.colDocumentRemarks.Width = 91;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // ActionNumberButtonEdit
            // 
            this.ActionNumberButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03036EPAssetActionEditBindingSource, "ActionNumber", true));
            this.ActionNumberButtonEdit.Location = new System.Drawing.Point(120, 83);
            this.ActionNumberButtonEdit.MenuManager = this.barManager1;
            this.ActionNumberButtonEdit.Name = "ActionNumberButtonEdit";
            superToolTip6.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem6.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Appearance.Options.UseImage = true;
            toolTipTitleItem6.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Text = "Sequence Button - Information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Click me to <b>open</b> the <b>Choose Sequence screen</b> to <b>select the prefix" +
    "</b> for the sequence.";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            superToolTip7.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem7.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Appearance.Options.UseImage = true;
            toolTipTitleItem7.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Text = "Number Button - Information";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Click me to <b>calculate</b> the <b>number suffix</b> for the sequence.";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            this.ActionNumberButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Sequence", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", "Sequence", superToolTip6, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Number", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", "Number", superToolTip7, true)});
            this.ActionNumberButtonEdit.Size = new System.Drawing.Size(496, 20);
            this.ActionNumberButtonEdit.StyleController = this.dataLayoutControl1;
            this.ActionNumberButtonEdit.TabIndex = 7;
            this.ActionNumberButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ActionNumberButtonEdit_ButtonClick);
            this.ActionNumberButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ActionNumberButtonEdit_Validating);
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp03036EPAssetActionEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(121, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(258, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 18;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // ActionDueDateDateEdit
            // 
            this.ActionDueDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03036EPAssetActionEditBindingSource, "ActionDueDate", true));
            this.ActionDueDateDateEdit.EditValue = null;
            this.ActionDueDateDateEdit.Location = new System.Drawing.Point(120, 143);
            this.ActionDueDateDateEdit.MenuManager = this.barManager1;
            this.ActionDueDateDateEdit.Name = "ActionDueDateDateEdit";
            this.ActionDueDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Clear Date", "clear", null, true)});
            this.ActionDueDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.ActionDueDateDateEdit.Size = new System.Drawing.Size(172, 20);
            this.ActionDueDateDateEdit.StyleController = this.dataLayoutControl1;
            this.ActionDueDateDateEdit.TabIndex = 9;
            this.ActionDueDateDateEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ActionDueDateDateEdit_ButtonClick);
            // 
            // ActionDoneDateDateEdit
            // 
            this.ActionDoneDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03036EPAssetActionEditBindingSource, "ActionDoneDate", true));
            this.ActionDoneDateDateEdit.EditValue = null;
            this.ActionDoneDateDateEdit.Location = new System.Drawing.Point(120, 167);
            this.ActionDoneDateDateEdit.MenuManager = this.barManager1;
            this.ActionDoneDateDateEdit.Name = "ActionDoneDateDateEdit";
            this.ActionDoneDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Clear Date", "clear", null, true)});
            this.ActionDoneDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.ActionDoneDateDateEdit.Size = new System.Drawing.Size(172, 20);
            this.ActionDoneDateDateEdit.StyleController = this.dataLayoutControl1;
            this.ActionDoneDateDateEdit.TabIndex = 10;
            this.ActionDoneDateDateEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ActionDoneDateDateEdit_ButtonClick);
            // 
            // EstimatedManHoursSpinEdit
            // 
            this.EstimatedManHoursSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03036EPAssetActionEditBindingSource, "EstimatedManHours", true));
            this.EstimatedManHoursSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.EstimatedManHoursSpinEdit.Location = new System.Drawing.Point(120, 191);
            this.EstimatedManHoursSpinEdit.MenuManager = this.barManager1;
            this.EstimatedManHoursSpinEdit.Name = "EstimatedManHoursSpinEdit";
            this.EstimatedManHoursSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.EstimatedManHoursSpinEdit.Properties.Mask.EditMask = "f";
            this.EstimatedManHoursSpinEdit.Size = new System.Drawing.Size(172, 20);
            this.EstimatedManHoursSpinEdit.StyleController = this.dataLayoutControl1;
            this.EstimatedManHoursSpinEdit.TabIndex = 11;
            // 
            // EstimatedCostSpinEdit
            // 
            this.EstimatedCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03036EPAssetActionEditBindingSource, "EstimatedCost", true));
            this.EstimatedCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.EstimatedCostSpinEdit.Location = new System.Drawing.Point(120, 215);
            this.EstimatedCostSpinEdit.MenuManager = this.barManager1;
            this.EstimatedCostSpinEdit.Name = "EstimatedCostSpinEdit";
            this.EstimatedCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.EstimatedCostSpinEdit.Properties.Mask.EditMask = "c";
            this.EstimatedCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.EstimatedCostSpinEdit.Size = new System.Drawing.Size(172, 20);
            this.EstimatedCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.EstimatedCostSpinEdit.TabIndex = 12;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03036EPAssetActionEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, 319);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(556, 141);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling4);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 13;
            // 
            // AssetDescriptionButtonEdit
            // 
            this.AssetDescriptionButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03036EPAssetActionEditBindingSource, "AssetDescription", true));
            this.AssetDescriptionButtonEdit.Location = new System.Drawing.Point(120, 35);
            this.AssetDescriptionButtonEdit.MenuManager = this.barManager1;
            this.AssetDescriptionButtonEdit.Name = "AssetDescriptionButtonEdit";
            superToolTip8.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem8.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem8.Appearance.Options.UseImage = true;
            toolTipTitleItem8.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem8.Text = "Choose Button - Information";
            toolTipItem8.LeftIndent = 6;
            toolTipItem8.Text = "Click me to <b>open</b> the <b>Choose Asset screen</b> to <b>select the parent as" +
    "set</b> for the action.";
            superToolTip8.Items.Add(toolTipTitleItem8);
            superToolTip8.Items.Add(toolTipItem8);
            superToolTip9.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem9.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem9.Appearance.Options.UseImage = true;
            toolTipTitleItem9.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem9.Text = "View Button - Information";
            toolTipItem9.LeftIndent = 6;
            toolTipItem9.Text = "Click me to <b>open</b> the <b>Parent Asset</b> record for the current action.";
            superToolTip9.Items.Add(toolTipTitleItem9);
            superToolTip9.Items.Add(toolTipItem9);
            this.AssetDescriptionButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "", "choose", superToolTip8, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "", "view", superToolTip9, true)});
            this.AssetDescriptionButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.AssetDescriptionButtonEdit.Size = new System.Drawing.Size(496, 20);
            this.AssetDescriptionButtonEdit.StyleController = this.dataLayoutControl1;
            this.AssetDescriptionButtonEdit.TabIndex = 16;
            this.AssetDescriptionButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.AssetDescriptionButtonEdit_ButtonClick);
            this.AssetDescriptionButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.AssetDescriptionButtonEdit_Validating);
            // 
            // InspectionDescriptionButtonEdit
            // 
            this.InspectionDescriptionButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03036EPAssetActionEditBindingSource, "InspectionDescription", true));
            this.InspectionDescriptionButtonEdit.Location = new System.Drawing.Point(120, 59);
            this.InspectionDescriptionButtonEdit.MenuManager = this.barManager1;
            this.InspectionDescriptionButtonEdit.Name = "InspectionDescriptionButtonEdit";
            superToolTip10.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem10.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem10.Appearance.Options.UseImage = true;
            toolTipTitleItem10.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem10.Text = "Choose Button - Information";
            toolTipItem10.LeftIndent = 6;
            toolTipItem10.Text = "Click me to <b>open</b> the <b>Choose Inspection screen</b> to <b>select the pare" +
    "nt inspection</b> for the action.";
            superToolTip10.Items.Add(toolTipTitleItem10);
            superToolTip10.Items.Add(toolTipItem10);
            superToolTip11.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem11.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem11.Appearance.Options.UseImage = true;
            toolTipTitleItem11.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem11.Text = "View Button - Information";
            toolTipItem11.LeftIndent = 6;
            toolTipItem11.Text = "Click me to <b>open</b> the <b>Parent Inspection</b> record for the current actio" +
    "n.";
            superToolTip11.Items.Add(toolTipTitleItem11);
            superToolTip11.Items.Add(toolTipItem11);
            this.InspectionDescriptionButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject25, serializableAppearanceObject26, serializableAppearanceObject27, serializableAppearanceObject28, "", "choose", superToolTip10, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject29, serializableAppearanceObject30, serializableAppearanceObject31, serializableAppearanceObject32, "", "view", superToolTip11, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject33, serializableAppearanceObject34, serializableAppearanceObject35, serializableAppearanceObject36, "Clear Selected Inspection", "clear", null, true)});
            this.InspectionDescriptionButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.InspectionDescriptionButtonEdit.Size = new System.Drawing.Size(496, 20);
            this.InspectionDescriptionButtonEdit.StyleController = this.dataLayoutControl1;
            this.InspectionDescriptionButtonEdit.TabIndex = 17;
            this.InspectionDescriptionButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.InspectionDescriptionButtonEdit_ButtonClick);
            // 
            // JobTypeIDGridLookUpEdit
            // 
            this.JobTypeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03036EPAssetActionEditBindingSource, "JobTypeID", true));
            this.JobTypeIDGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.JobTypeIDGridLookUpEdit.Location = new System.Drawing.Point(120, 107);
            this.JobTypeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.JobTypeIDGridLookUpEdit.Name = "JobTypeIDGridLookUpEdit";
            this.JobTypeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::WoodPlan5.Properties.Resources.Edit_16x16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject37, serializableAppearanceObject38, serializableAppearanceObject39, serializableAppearanceObject40, "Edit Underlying Data", "edit", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::WoodPlan5.Properties.Resources.Refresh2_16x16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject41, serializableAppearanceObject42, serializableAppearanceObject43, serializableAppearanceObject44, "Reload Underlying Data", "reload", null, true)});
            this.JobTypeIDGridLookUpEdit.Properties.DataSource = this.sp03041EPAssetMasterJobsWithBlankBindingSource;
            this.JobTypeIDGridLookUpEdit.Properties.DisplayMember = "JobDescription";
            this.JobTypeIDGridLookUpEdit.Properties.NullText = "";
            this.JobTypeIDGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemCheckEdit1});
            this.JobTypeIDGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.JobTypeIDGridLookUpEdit.Properties.ValueMember = "JobID";
            this.JobTypeIDGridLookUpEdit.Properties.View = this.gridLookUpEdit1View;
            this.JobTypeIDGridLookUpEdit.Size = new System.Drawing.Size(496, 22);
            this.JobTypeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.JobTypeIDGridLookUpEdit.TabIndex = 8;
            this.JobTypeIDGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.JobTypeIDGridLookUpEdit_ButtonClick);
            this.JobTypeIDGridLookUpEdit.Enter += new System.EventHandler(this.JobTypeIDGridLookUpEdit_Enter);
            this.JobTypeIDGridLookUpEdit.Leave += new System.EventHandler(this.JobTypeIDGridLookUpEdit_Leave);
            this.JobTypeIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.JobTypeIDGridLookUpEdit_Validating);
            // 
            // sp03041EPAssetMasterJobsWithBlankBindingSource
            // 
            this.sp03041EPAssetMasterJobsWithBlankBindingSource.DataMember = "sp03041_EP_Asset_Master_Jobs_With_Blank";
            this.sp03041EPAssetMasterJobsWithBlankBindingSource.DataSource = this.dataSet_EP_DataEntry;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAssetTypeDescription,
            this.colAssetTypeOrder,
            this.colDefaultWorkUnits,
            this.colJobCode,
            this.colJobDescription,
            this.colJobDisabled,
            this.colJobID,
            this.colJobOrder,
            this.colRemarks,
            this.colAssetTypeID});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colJobID;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridLookUpEdit1View.GroupCount = 1;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsCustomization.AllowFilter = false;
            this.gridLookUpEdit1View.OptionsFilter.AllowFilterEditor = false;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAssetTypeDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colJobOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colAssetTypeDescription
            // 
            this.colAssetTypeDescription.Caption = "Asset Type";
            this.colAssetTypeDescription.FieldName = "AssetTypeDescription";
            this.colAssetTypeDescription.Name = "colAssetTypeDescription";
            this.colAssetTypeDescription.OptionsColumn.AllowEdit = false;
            this.colAssetTypeDescription.OptionsColumn.AllowFocus = false;
            this.colAssetTypeDescription.OptionsColumn.ReadOnly = true;
            this.colAssetTypeDescription.Width = 102;
            // 
            // colAssetTypeOrder
            // 
            this.colAssetTypeOrder.Caption = "Asset Type Order";
            this.colAssetTypeOrder.FieldName = "AssetTypeOrder";
            this.colAssetTypeOrder.Name = "colAssetTypeOrder";
            this.colAssetTypeOrder.OptionsColumn.AllowEdit = false;
            this.colAssetTypeOrder.OptionsColumn.AllowFocus = false;
            this.colAssetTypeOrder.OptionsColumn.ReadOnly = true;
            this.colAssetTypeOrder.Width = 108;
            // 
            // colDefaultWorkUnits
            // 
            this.colDefaultWorkUnits.Caption = "Default Work Units";
            this.colDefaultWorkUnits.FieldName = "DefaultWorkUnits";
            this.colDefaultWorkUnits.Name = "colDefaultWorkUnits";
            this.colDefaultWorkUnits.OptionsColumn.AllowEdit = false;
            this.colDefaultWorkUnits.OptionsColumn.AllowFocus = false;
            this.colDefaultWorkUnits.OptionsColumn.ReadOnly = true;
            this.colDefaultWorkUnits.Width = 115;
            // 
            // colJobCode
            // 
            this.colJobCode.Caption = "Job Code";
            this.colJobCode.FieldName = "JobCode";
            this.colJobCode.Name = "colJobCode";
            this.colJobCode.OptionsColumn.AllowEdit = false;
            this.colJobCode.OptionsColumn.AllowFocus = false;
            this.colJobCode.OptionsColumn.ReadOnly = true;
            this.colJobCode.Visible = true;
            this.colJobCode.VisibleIndex = 1;
            this.colJobCode.Width = 109;
            // 
            // colJobDescription
            // 
            this.colJobDescription.Caption = "Job Description";
            this.colJobDescription.FieldName = "JobDescription";
            this.colJobDescription.Name = "colJobDescription";
            this.colJobDescription.OptionsColumn.AllowEdit = false;
            this.colJobDescription.OptionsColumn.AllowFocus = false;
            this.colJobDescription.OptionsColumn.ReadOnly = true;
            this.colJobDescription.Visible = true;
            this.colJobDescription.VisibleIndex = 0;
            this.colJobDescription.Width = 281;
            // 
            // colJobDisabled
            // 
            this.colJobDisabled.Caption = "Job Disabled";
            this.colJobDisabled.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colJobDisabled.FieldName = "JobDisabled";
            this.colJobDisabled.Name = "colJobDisabled";
            this.colJobDisabled.OptionsColumn.AllowEdit = false;
            this.colJobDisabled.OptionsColumn.AllowFocus = false;
            this.colJobDisabled.OptionsColumn.ReadOnly = true;
            this.colJobDisabled.Visible = true;
            this.colJobDisabled.VisibleIndex = 2;
            this.colJobDisabled.Width = 84;
            // 
            // colJobOrder
            // 
            this.colJobOrder.Caption = "Job Order";
            this.colJobOrder.FieldName = "JobOrder";
            this.colJobOrder.Name = "colJobOrder";
            this.colJobOrder.OptionsColumn.AllowEdit = false;
            this.colJobOrder.OptionsColumn.AllowFocus = false;
            this.colJobOrder.OptionsColumn.ReadOnly = true;
            this.colJobOrder.Width = 86;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 3;
            this.colRemarks.Width = 70;
            // 
            // colAssetTypeID
            // 
            this.colAssetTypeID.Caption = "Asset Type ID";
            this.colAssetTypeID.FieldName = "AssetTypeID";
            this.colAssetTypeID.Name = "colAssetTypeID";
            this.colAssetTypeID.OptionsColumn.AllowEdit = false;
            this.colAssetTypeID.OptionsColumn.AllowFocus = false;
            this.colAssetTypeID.OptionsColumn.ReadOnly = true;
            this.colAssetTypeID.Width = 97;
            // 
            // ItemForActionID
            // 
            this.ItemForActionID.Control = this.ActionIDTextEdit;
            this.ItemForActionID.CustomizationFormText = "Action ID:";
            this.ItemForActionID.Location = new System.Drawing.Point(0, 0);
            this.ItemForActionID.Name = "ItemForActionID";
            this.ItemForActionID.Size = new System.Drawing.Size(608, 24);
            this.ItemForActionID.Text = "Action ID:";
            this.ItemForActionID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForAssetID
            // 
            this.ItemForAssetID.Control = this.AssetIDTextEdit;
            this.ItemForAssetID.CustomizationFormText = "Asset ID:";
            this.ItemForAssetID.Location = new System.Drawing.Point(0, 0);
            this.ItemForAssetID.Name = "ItemForAssetID";
            this.ItemForAssetID.Size = new System.Drawing.Size(608, 24);
            this.ItemForAssetID.Text = "Asset ID:";
            this.ItemForAssetID.TextSize = new System.Drawing.Size(106, 13);
            // 
            // ItemForInspectionID
            // 
            this.ItemForInspectionID.Control = this.InspectionIDTextEdit;
            this.ItemForInspectionID.CustomizationFormText = "Inspection ID:";
            this.ItemForInspectionID.Location = new System.Drawing.Point(0, 0);
            this.ItemForInspectionID.Name = "ItemForInspectionID";
            this.ItemForInspectionID.Size = new System.Drawing.Size(608, 24);
            this.ItemForInspectionID.Text = "Inspection ID:";
            this.ItemForInspectionID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(628, 506);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForActionNumber,
            this.ItemForJobTypeID,
            this.ItemForActionDueDate,
            this.ItemForActionDoneDate,
            this.ItemForEstimatedManHours,
            this.ItemForEstimatedCost,
            this.ItemForAssetDescription,
            this.ItemForInspectionDescription,
            this.layoutControlGroup4,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.emptySpaceItem3,
            this.layoutControlItem1,
            this.emptySpaceItem5,
            this.emptySpaceItem4,
            this.emptySpaceItem6,
            this.emptySpaceItem7,
            this.emptySpaceItem8,
            this.emptySpaceItem9});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(608, 486);
            // 
            // ItemForActionNumber
            // 
            this.ItemForActionNumber.Control = this.ActionNumberButtonEdit;
            this.ItemForActionNumber.CustomizationFormText = "Action Number:";
            this.ItemForActionNumber.Location = new System.Drawing.Point(0, 71);
            this.ItemForActionNumber.Name = "ItemForActionNumber";
            this.ItemForActionNumber.Size = new System.Drawing.Size(608, 24);
            this.ItemForActionNumber.Text = "Action Number:";
            this.ItemForActionNumber.TextSize = new System.Drawing.Size(105, 13);
            // 
            // ItemForJobTypeID
            // 
            this.ItemForJobTypeID.Control = this.JobTypeIDGridLookUpEdit;
            this.ItemForJobTypeID.CustomizationFormText = "Job Type:";
            this.ItemForJobTypeID.Location = new System.Drawing.Point(0, 95);
            this.ItemForJobTypeID.Name = "ItemForJobTypeID";
            this.ItemForJobTypeID.Size = new System.Drawing.Size(608, 26);
            this.ItemForJobTypeID.Text = "Job Type:";
            this.ItemForJobTypeID.TextSize = new System.Drawing.Size(105, 13);
            // 
            // ItemForActionDueDate
            // 
            this.ItemForActionDueDate.Control = this.ActionDueDateDateEdit;
            this.ItemForActionDueDate.CustomizationFormText = "Action Due Date:";
            this.ItemForActionDueDate.Location = new System.Drawing.Point(0, 131);
            this.ItemForActionDueDate.MaxSize = new System.Drawing.Size(284, 24);
            this.ItemForActionDueDate.MinSize = new System.Drawing.Size(284, 24);
            this.ItemForActionDueDate.Name = "ItemForActionDueDate";
            this.ItemForActionDueDate.Size = new System.Drawing.Size(284, 24);
            this.ItemForActionDueDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForActionDueDate.Text = "Action Due Date:";
            this.ItemForActionDueDate.TextSize = new System.Drawing.Size(105, 13);
            // 
            // ItemForActionDoneDate
            // 
            this.ItemForActionDoneDate.Control = this.ActionDoneDateDateEdit;
            this.ItemForActionDoneDate.CustomizationFormText = "Action Done Date:";
            this.ItemForActionDoneDate.Location = new System.Drawing.Point(0, 155);
            this.ItemForActionDoneDate.MaxSize = new System.Drawing.Size(284, 24);
            this.ItemForActionDoneDate.MinSize = new System.Drawing.Size(284, 24);
            this.ItemForActionDoneDate.Name = "ItemForActionDoneDate";
            this.ItemForActionDoneDate.Size = new System.Drawing.Size(284, 24);
            this.ItemForActionDoneDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForActionDoneDate.Text = "Action Done Date:";
            this.ItemForActionDoneDate.TextSize = new System.Drawing.Size(105, 13);
            // 
            // ItemForEstimatedManHours
            // 
            this.ItemForEstimatedManHours.Control = this.EstimatedManHoursSpinEdit;
            this.ItemForEstimatedManHours.CustomizationFormText = "Estimated Man Hours:";
            this.ItemForEstimatedManHours.Location = new System.Drawing.Point(0, 179);
            this.ItemForEstimatedManHours.MaxSize = new System.Drawing.Size(284, 24);
            this.ItemForEstimatedManHours.MinSize = new System.Drawing.Size(284, 24);
            this.ItemForEstimatedManHours.Name = "ItemForEstimatedManHours";
            this.ItemForEstimatedManHours.Size = new System.Drawing.Size(284, 24);
            this.ItemForEstimatedManHours.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForEstimatedManHours.Text = "Estimated Man Hours:";
            this.ItemForEstimatedManHours.TextSize = new System.Drawing.Size(105, 13);
            // 
            // ItemForEstimatedCost
            // 
            this.ItemForEstimatedCost.Control = this.EstimatedCostSpinEdit;
            this.ItemForEstimatedCost.CustomizationFormText = "Estimated Cost:";
            this.ItemForEstimatedCost.Location = new System.Drawing.Point(0, 203);
            this.ItemForEstimatedCost.MaxSize = new System.Drawing.Size(284, 24);
            this.ItemForEstimatedCost.MinSize = new System.Drawing.Size(284, 24);
            this.ItemForEstimatedCost.Name = "ItemForEstimatedCost";
            this.ItemForEstimatedCost.Size = new System.Drawing.Size(284, 24);
            this.ItemForEstimatedCost.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForEstimatedCost.Text = "Estimated Cost:";
            this.ItemForEstimatedCost.TextSize = new System.Drawing.Size(105, 13);
            // 
            // ItemForAssetDescription
            // 
            this.ItemForAssetDescription.Control = this.AssetDescriptionButtonEdit;
            this.ItemForAssetDescription.CustomizationFormText = "Linked To Asset:";
            this.ItemForAssetDescription.Location = new System.Drawing.Point(0, 23);
            this.ItemForAssetDescription.Name = "ItemForAssetDescription";
            this.ItemForAssetDescription.Size = new System.Drawing.Size(608, 24);
            this.ItemForAssetDescription.Text = "Linked To Asset:";
            this.ItemForAssetDescription.TextSize = new System.Drawing.Size(105, 13);
            // 
            // ItemForInspectionDescription
            // 
            this.ItemForInspectionDescription.Control = this.InspectionDescriptionButtonEdit;
            this.ItemForInspectionDescription.CustomizationFormText = "Linked To Inspection:";
            this.ItemForInspectionDescription.Location = new System.Drawing.Point(0, 47);
            this.ItemForInspectionDescription.Name = "ItemForInspectionDescription";
            this.ItemForInspectionDescription.Size = new System.Drawing.Size(608, 24);
            this.ItemForInspectionDescription.Text = "Linked To Inspection:";
            this.ItemForInspectionDescription.TextSize = new System.Drawing.Size(105, 13);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Details";
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 237);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(608, 239);
            this.layoutControlGroup4.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup3;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(584, 193);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3,
            this.layoutControlGroup5});
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CaptionImage = global::WoodPlan5.Properties.Resources.Notes_16x16;
            this.layoutControlGroup3.CustomizationFormText = "Remarks";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(560, 145);
            this.layoutControlGroup3.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(560, 145);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CaptionImage = global::WoodPlan5.Properties.Resources.linked_documents_16_16;
            this.layoutControlGroup5.CustomizationFormText = "Linked Documents";
            this.layoutControlGroup5.ExpandButtonVisible = true;
            this.layoutControlGroup5.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(560, 145);
            this.layoutControlGroup5.Text = "Linked Documents";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridControl1;
            this.layoutControlItem2.CustomizationFormText = "Linked Documents Grid:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(560, 145);
            this.layoutControlItem2.Text = "Linked Documents Grid:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 227);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(608, 10);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 476);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(608, 10);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 121);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(608, 10);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(109, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(262, 23);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(262, 23);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(262, 23);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(371, 0);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(237, 23);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(109, 0);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(109, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(109, 23);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.Location = new System.Drawing.Point(284, 131);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(324, 24);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.Location = new System.Drawing.Point(284, 155);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(324, 24);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.Location = new System.Drawing.Point(284, 179);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(324, 24);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.Location = new System.Drawing.Point(284, 203);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(324, 24);
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp03036_EP_Asset_Action_EditTableAdapter
            // 
            this.sp03036_EP_Asset_Action_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp03041_EP_Asset_Master_Jobs_With_BlankTableAdapter
            // 
            this.sp03041_EP_Asset_Master_Jobs_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp00220_Linked_Documents_ListTableAdapter
            // 
            this.sp00220_Linked_Documents_ListTableAdapter.ClearBeforeFill = true;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Delete_16x16");
            // 
            // frm_EP_Action_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(628, 560);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_EP_Action_Edit";
            this.Text = "Edit Asset Action";
            this.Activated += new System.EventHandler(this.frm_EP_Action_Edit_Activated);
            this.Deactivate += new System.EventHandler(this.frm_EP_Action_Edit_Deactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_EP_Action_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_EP_Action_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ActionIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03036EPAssetActionEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_EP_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AssetIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InspectionIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00220LinkedDocumentsListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActionNumberButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActionDueDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActionDueDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActionDoneDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActionDoneDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EstimatedManHoursSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EstimatedCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AssetDescriptionButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InspectionDescriptionButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobTypeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03041EPAssetMasterJobsWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActionID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAssetID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInspectionID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActionNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActionDueDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActionDoneDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEstimatedManHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEstimatedCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAssetDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInspectionDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.Bar bar4;
        private DevExpress.XtraBars.BarCheckItem bciShowPrevious;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private System.Windows.Forms.BindingSource sp03036EPAssetActionEditBindingSource;
        private DataSet_EP_DataEntry dataSet_EP_DataEntry;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03036_EP_Asset_Action_EditTableAdapter sp03036_EP_Asset_Action_EditTableAdapter;
        private DevExpress.XtraEditors.TextEdit ActionIDTextEdit;
        private DevExpress.XtraEditors.TextEdit AssetIDTextEdit;
        private DevExpress.XtraEditors.TextEdit InspectionIDTextEdit;
        private DevExpress.XtraEditors.ButtonEdit ActionNumberButtonEdit;
        private DevExpress.XtraEditors.DateEdit ActionDueDateDateEdit;
        private DevExpress.XtraEditors.DateEdit ActionDoneDateDateEdit;
        private DevExpress.XtraEditors.SpinEdit EstimatedManHoursSpinEdit;
        private DevExpress.XtraEditors.SpinEdit EstimatedCostSpinEdit;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraEditors.ButtonEdit AssetDescriptionButtonEdit;
        private DevExpress.XtraEditors.ButtonEdit InspectionDescriptionButtonEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActionID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAssetID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInspectionID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActionNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobTypeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActionDueDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActionDoneDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEstimatedManHours;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEstimatedCost;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAssetDescription;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInspectionDescription;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.GridLookUpEdit JobTypeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource sp03041EPAssetMasterJobsWithBlankBindingSource;
        private WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03041_EP_Asset_Master_Jobs_With_BlankTableAdapter sp03041_EP_Asset_Master_Jobs_With_BlankTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetTypeOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colDefaultWorkUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colJobCode;
        private DevExpress.XtraGrid.Columns.GridColumn colJobDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colJobDisabled;
        private DevExpress.XtraGrid.Columns.GridColumn colJobID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetTypeID;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private System.Windows.Forms.BindingSource sp00220LinkedDocumentsListBindingSource;
        private DataSet_AT dataSet_AT;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentPath;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentExtension;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateAdded;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffName;
        private WoodPlan5.DataSet_ATTableAdapters.sp00220_Linked_Documents_ListTableAdapter sp00220_Linked_Documents_ListTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraBars.BarButtonItem bbiShowMapButton;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.Utils.ImageCollection imageCollection1;
    }
}

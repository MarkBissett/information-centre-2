namespace WoodPlan5
{
    partial class frm_EP_Site_Inspection_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_EP_Site_Inspection_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem9 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem9 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions7 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject25 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject26 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject27 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject28 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions8 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject29 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject30 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject31 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject32 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions9 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject33 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject34 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject35 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject36 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions10 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject37 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject38 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject39 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject40 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.Utils.SuperToolTip superToolTip10 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem10 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem10 = new DevExpress.Utils.ToolTipItem();
            this.colSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bbiOpenMap = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp03058EPActionsForPassedInspectionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_EP_DataEntry = new WoodPlan5.DataSet_EP_DataEntry();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colActionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionDueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionDoneDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedManHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colActionRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colXCoordinate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYCoordinate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPolygonXY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colArea = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLength = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWidth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetStatusDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetPartNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetSerialNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetModelNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetLifeSpanValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetLifeSpanValueDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetConditionDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetLastVisitDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetNextVisitDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMapID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp03057EPTimeDeductionsForInspectionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDeductionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colStartTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEndTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.btnUnlink = new DevExpress.XtraEditors.SimpleButton();
            this.btnLink = new DevExpress.XtraEditors.SimpleButton();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.sp03051EPSiteInspectionEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.InspectionIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.InspectionDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.InspectionStartTimeTimeEdit = new DevExpress.XtraEditors.TimeEdit();
            this.InspectionEndTimeTimeEdit = new DevExpress.XtraEditors.TimeEdit();
            this.TotalTimeDeductionsSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.TotalTimeDeductionsRemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.TotalTimeTakenSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.InspectionNumberButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.SiteIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp03028EPSitesWithBlankDDLBBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactPerson = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colXCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.InspectorIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp00190ContractorListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDisabled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colEmailPassword = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInternalContractor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInternalCOntractorDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMobile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserDefined1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserDefined2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserDefined3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVatReg = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWebsite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnSaveAndOpenMap = new DevExpress.XtraEditors.SimpleButton();
            this.ItemForInspectionID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSiteID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForInspectionDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForInspectorID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForInspectionNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForTotalTimeDeductions = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTotalTimeDeductionsRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForInspectionStartTime = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForInspectionEndTime = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTotalTimeTaken = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp03051_EP_Site_Inspection_EditTableAdapter = new WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03051_EP_Site_Inspection_EditTableAdapter();
            this.sp03028_EP_Sites_With_Blank_DDLBTableAdapter = new WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03028_EP_Sites_With_Blank_DDLBTableAdapter();
            this.sp00190_Contractor_List_With_BlankTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00190_Contractor_List_With_BlankTableAdapter();
            this.sp03057_EP_Time_Deductions_For_InspectionTableAdapter = new WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03057_EP_Time_Deductions_For_InspectionTableAdapter();
            this.sp03058_EP_Actions_For_Passed_InspectionTableAdapter = new WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03058_EP_Actions_For_Passed_InspectionTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03058EPActionsForPassedInspectionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_EP_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03057EPTimeDeductionsForInspectionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03051EPSiteInspectionEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InspectionIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InspectionDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InspectionDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InspectionStartTimeTimeEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InspectionEndTimeTimeEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalTimeDeductionsSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalTimeDeductionsRemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalTimeTakenSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InspectionNumberButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03028EPSitesWithBlankDDLBBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InspectorIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00190ContractorListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInspectionID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInspectionDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInspectorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInspectionNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalTimeDeductions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalTimeDeductionsRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInspectionStartTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInspectionEndTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalTimeTaken)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 507);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 481);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(628, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 481);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colSiteID
            // 
            this.colSiteID.Caption = "Site ID";
            this.colSiteID.FieldName = "SiteID";
            this.colSiteID.Name = "colSiteID";
            this.colSiteID.OptionsColumn.AllowEdit = false;
            this.colSiteID.OptionsColumn.AllowFocus = false;
            this.colSiteID.OptionsColumn.ReadOnly = true;
            // 
            // colContractorID
            // 
            this.colContractorID.Caption = "Contractor ID";
            this.colContractorID.FieldName = "ContractorID";
            this.colContractorID.Name = "colContractorID";
            this.colContractorID.OptionsColumn.AllowEdit = false;
            this.colContractorID.OptionsColumn.AllowFocus = false;
            this.colContractorID.OptionsColumn.ReadOnly = true;
            this.colContractorID.Width = 87;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3,
            this.bar2});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation,
            this.bbiOpenMap});
            this.barManager2.MaxItemId = 17;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.ImageOptions.Image = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.ImageOptions.Image = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageOptions.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageOptions.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bar2
            // 
            this.bar2.BarName = "Custom 4";
            this.bar2.DockCol = 1;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiOpenMap)});
            this.bar2.Text = "Custom 4";
            // 
            // bbiOpenMap
            // 
            this.bbiOpenMap.Caption = "Show Map";
            this.bbiOpenMap.Id = 16;
            this.bbiOpenMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiOpenMap.ImageOptions.Image")));
            this.bbiOpenMap.Name = "bbiOpenMap";
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Text = "Show Map button - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = resources.GetString("toolTipItem4.Text");
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.bbiOpenMap.SuperTip = superToolTip4;
            this.bbiOpenMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiOpenMap_ItemClick);
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(628, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 507);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(628, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 481);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(628, 26);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 481);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Delete_16x16");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.gridControl2);
            this.dataLayoutControl1.Controls.Add(this.gridControl1);
            this.dataLayoutControl1.Controls.Add(this.btnUnlink);
            this.dataLayoutControl1.Controls.Add(this.btnLink);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.InspectionIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.InspectionDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.InspectionStartTimeTimeEdit);
            this.dataLayoutControl1.Controls.Add(this.InspectionEndTimeTimeEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalTimeDeductionsSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalTimeDeductionsRemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalTimeTakenSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.InspectionNumberButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.InspectorIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.btnSaveAndOpenMap);
            this.dataLayoutControl1.DataSource = this.sp03051EPSiteInspectionEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForInspectionID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(628, 481);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.sp03058EPActionsForPassedInspectionBindingSource;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl2.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl2_EmbeddedNavigator_ButtonClick);
            this.gridControl2.Location = new System.Drawing.Point(36, 252);
            this.gridControl2.MainView = this.gridView3;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2,
            this.repositoryItemTextEdit2});
            this.gridControl2.Size = new System.Drawing.Size(556, 183);
            this.gridControl2.TabIndex = 20;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp03058EPActionsForPassedInspectionBindingSource
            // 
            this.sp03058EPActionsForPassedInspectionBindingSource.DataMember = "sp03058_EP_Actions_For_Passed_Inspection";
            this.sp03058EPActionsForPassedInspectionBindingSource.DataSource = this.dataSet_EP_DataEntry;
            // 
            // dataSet_EP_DataEntry
            // 
            this.dataSet_EP_DataEntry.DataSetName = "DataSet_EP_DataEntry";
            this.dataSet_EP_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colActionID,
            this.colAssetID,
            this.gridColumn1,
            this.colActionNumber,
            this.colJobTypeID,
            this.colJobTypeDescription,
            this.colActionDueDate,
            this.colActionDoneDate,
            this.colEstimatedManHours,
            this.colEstimatedCost,
            this.colActionRemarks,
            this.colSiteName1,
            this.colSiteCode1,
            this.colClientName1,
            this.colClientCode1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.colXCoordinate1,
            this.colYCoordinate1,
            this.colPolygonXY,
            this.colArea,
            this.colLength,
            this.colWidth,
            this.colAssetStatusDescription,
            this.colAssetPartNumber,
            this.colAssetSerialNumber,
            this.colAssetModelNumber,
            this.colAssetNumber,
            this.colAssetLifeSpanValue,
            this.colAssetLifeSpanValueDescriptor,
            this.colAssetConditionDescription,
            this.colAssetLastVisitDate,
            this.colAssetNextVisitDate,
            this.colAssetRemarks,
            this.colMapID,
            this.colInspectionDate1,
            this.colInspectionNumber1});
            this.gridView3.GridControl = this.gridControl2;
            this.gridView3.GroupCount = 1;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.MultiSelect = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colInspectionDate1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colActionDueDate, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView3.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView3.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView3.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView3.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView3_MouseUp);
            this.gridView3.DoubleClick += new System.EventHandler(this.gridView3_DoubleClick);
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colActionID
            // 
            this.colActionID.Caption = "Action ID";
            this.colActionID.FieldName = "ActionID";
            this.colActionID.Name = "colActionID";
            this.colActionID.OptionsColumn.AllowEdit = false;
            this.colActionID.OptionsColumn.AllowFocus = false;
            this.colActionID.OptionsColumn.ReadOnly = true;
            // 
            // colAssetID
            // 
            this.colAssetID.Caption = "Asset ID";
            this.colAssetID.FieldName = "AssetID";
            this.colAssetID.Name = "colAssetID";
            this.colAssetID.OptionsColumn.AllowEdit = false;
            this.colAssetID.OptionsColumn.AllowFocus = false;
            this.colAssetID.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Inspection ID";
            this.gridColumn1.FieldName = "InspectionID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Width = 96;
            // 
            // colActionNumber
            // 
            this.colActionNumber.Caption = "Action Number";
            this.colActionNumber.FieldName = "ActionNumber";
            this.colActionNumber.Name = "colActionNumber";
            this.colActionNumber.OptionsColumn.AllowEdit = false;
            this.colActionNumber.OptionsColumn.AllowFocus = false;
            this.colActionNumber.OptionsColumn.ReadOnly = true;
            this.colActionNumber.Visible = true;
            this.colActionNumber.VisibleIndex = 4;
            this.colActionNumber.Width = 129;
            // 
            // colJobTypeID
            // 
            this.colJobTypeID.Caption = "Job Type ID";
            this.colJobTypeID.FieldName = "JobTypeID";
            this.colJobTypeID.Name = "colJobTypeID";
            this.colJobTypeID.OptionsColumn.AllowEdit = false;
            this.colJobTypeID.OptionsColumn.AllowFocus = false;
            this.colJobTypeID.OptionsColumn.ReadOnly = true;
            // 
            // colJobTypeDescription
            // 
            this.colJobTypeDescription.Caption = "Job Type";
            this.colJobTypeDescription.FieldName = "JobTypeDescription";
            this.colJobTypeDescription.Name = "colJobTypeDescription";
            this.colJobTypeDescription.OptionsColumn.AllowEdit = false;
            this.colJobTypeDescription.OptionsColumn.AllowFocus = false;
            this.colJobTypeDescription.OptionsColumn.ReadOnly = true;
            this.colJobTypeDescription.Visible = true;
            this.colJobTypeDescription.VisibleIndex = 5;
            this.colJobTypeDescription.Width = 174;
            // 
            // colActionDueDate
            // 
            this.colActionDueDate.Caption = "Due Date";
            this.colActionDueDate.FieldName = "ActionDueDate";
            this.colActionDueDate.Name = "colActionDueDate";
            this.colActionDueDate.OptionsColumn.AllowEdit = false;
            this.colActionDueDate.OptionsColumn.AllowFocus = false;
            this.colActionDueDate.OptionsColumn.ReadOnly = true;
            this.colActionDueDate.Visible = true;
            this.colActionDueDate.VisibleIndex = 6;
            this.colActionDueDate.Width = 90;
            // 
            // colActionDoneDate
            // 
            this.colActionDoneDate.Caption = "Done Date";
            this.colActionDoneDate.FieldName = "ActionDoneDate";
            this.colActionDoneDate.Name = "colActionDoneDate";
            this.colActionDoneDate.OptionsColumn.AllowEdit = false;
            this.colActionDoneDate.OptionsColumn.AllowFocus = false;
            this.colActionDoneDate.OptionsColumn.ReadOnly = true;
            this.colActionDoneDate.Visible = true;
            this.colActionDoneDate.VisibleIndex = 7;
            this.colActionDoneDate.Width = 97;
            // 
            // colEstimatedManHours
            // 
            this.colEstimatedManHours.Caption = "Estaimated Man Hours";
            this.colEstimatedManHours.FieldName = "EstimatedManHours";
            this.colEstimatedManHours.Name = "colEstimatedManHours";
            this.colEstimatedManHours.OptionsColumn.AllowEdit = false;
            this.colEstimatedManHours.OptionsColumn.AllowFocus = false;
            this.colEstimatedManHours.OptionsColumn.ReadOnly = true;
            this.colEstimatedManHours.Visible = true;
            this.colEstimatedManHours.VisibleIndex = 8;
            this.colEstimatedManHours.Width = 130;
            // 
            // colEstimatedCost
            // 
            this.colEstimatedCost.Caption = "Estimated Cost";
            this.colEstimatedCost.ColumnEdit = this.repositoryItemTextEdit2;
            this.colEstimatedCost.FieldName = "EstimatedCost";
            this.colEstimatedCost.Name = "colEstimatedCost";
            this.colEstimatedCost.OptionsColumn.AllowEdit = false;
            this.colEstimatedCost.OptionsColumn.AllowFocus = false;
            this.colEstimatedCost.OptionsColumn.ReadOnly = true;
            this.colEstimatedCost.Visible = true;
            this.colEstimatedCost.VisibleIndex = 9;
            this.colEstimatedCost.Width = 97;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Mask.EditMask = "c";
            this.repositoryItemTextEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // colActionRemarks
            // 
            this.colActionRemarks.Caption = "Action Remarks";
            this.colActionRemarks.FieldName = "ActionRemarks";
            this.colActionRemarks.Name = "colActionRemarks";
            this.colActionRemarks.OptionsColumn.ReadOnly = true;
            this.colActionRemarks.Visible = true;
            this.colActionRemarks.VisibleIndex = 10;
            this.colActionRemarks.Width = 94;
            // 
            // colSiteName1
            // 
            this.colSiteName1.Caption = "Site Name";
            this.colSiteName1.FieldName = "SiteName";
            this.colSiteName1.Name = "colSiteName1";
            this.colSiteName1.OptionsColumn.AllowEdit = false;
            this.colSiteName1.OptionsColumn.AllowFocus = false;
            this.colSiteName1.OptionsColumn.ReadOnly = true;
            this.colSiteName1.Visible = true;
            this.colSiteName1.VisibleIndex = 1;
            this.colSiteName1.Width = 139;
            // 
            // colSiteCode1
            // 
            this.colSiteCode1.Caption = "SiteCode";
            this.colSiteCode1.FieldName = "SiteCode";
            this.colSiteCode1.Name = "colSiteCode1";
            this.colSiteCode1.OptionsColumn.AllowEdit = false;
            this.colSiteCode1.OptionsColumn.AllowFocus = false;
            this.colSiteCode1.OptionsColumn.ReadOnly = true;
            // 
            // colClientName1
            // 
            this.colClientName1.Caption = "Client Name";
            this.colClientName1.FieldName = "ClientName";
            this.colClientName1.Name = "colClientName1";
            this.colClientName1.OptionsColumn.AllowEdit = false;
            this.colClientName1.OptionsColumn.AllowFocus = false;
            this.colClientName1.OptionsColumn.ReadOnly = true;
            this.colClientName1.Visible = true;
            this.colClientName1.VisibleIndex = 0;
            this.colClientName1.Width = 125;
            // 
            // colClientCode1
            // 
            this.colClientCode1.Caption = "Client Code";
            this.colClientCode1.FieldName = "ClientCode";
            this.colClientCode1.Name = "colClientCode1";
            this.colClientCode1.OptionsColumn.AllowEdit = false;
            this.colClientCode1.OptionsColumn.AllowFocus = false;
            this.colClientCode1.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Asset Type";
            this.gridColumn2.FieldName = "AssetTypeDescription";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 11;
            this.gridColumn2.Width = 121;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Asset Type Order";
            this.gridColumn3.FieldName = "AssetTypeOrder";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Width = 106;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Asset Sub-Type";
            this.gridColumn4.FieldName = "AssetSubTypeDescription";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 12;
            this.gridColumn4.Width = 112;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Asset Sub-Type Order";
            this.gridColumn5.FieldName = "AssetSubTypeOrder";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Width = 136;
            // 
            // colXCoordinate1
            // 
            this.colXCoordinate1.Caption = "X Coordinate";
            this.colXCoordinate1.FieldName = "XCoordinate";
            this.colXCoordinate1.Name = "colXCoordinate1";
            this.colXCoordinate1.OptionsColumn.AllowEdit = false;
            this.colXCoordinate1.OptionsColumn.AllowFocus = false;
            this.colXCoordinate1.OptionsColumn.ReadOnly = true;
            // 
            // colYCoordinate1
            // 
            this.colYCoordinate1.Caption = "Y Coordinate";
            this.colYCoordinate1.FieldName = "YCoordinate";
            this.colYCoordinate1.Name = "colYCoordinate1";
            this.colYCoordinate1.OptionsColumn.AllowEdit = false;
            this.colYCoordinate1.OptionsColumn.AllowFocus = false;
            this.colYCoordinate1.OptionsColumn.ReadOnly = true;
            // 
            // colPolygonXY
            // 
            this.colPolygonXY.Caption = "Polygon XY Coordinates ";
            this.colPolygonXY.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colPolygonXY.FieldName = "PolygonXY";
            this.colPolygonXY.Name = "colPolygonXY";
            this.colPolygonXY.OptionsColumn.ReadOnly = true;
            this.colPolygonXY.Width = 152;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colArea
            // 
            this.colArea.Caption = "Area";
            this.colArea.FieldName = "Area";
            this.colArea.Name = "colArea";
            this.colArea.OptionsColumn.AllowEdit = false;
            this.colArea.OptionsColumn.AllowFocus = false;
            this.colArea.OptionsColumn.ReadOnly = true;
            // 
            // colLength
            // 
            this.colLength.Caption = "Length (M)";
            this.colLength.FieldName = "Length";
            this.colLength.Name = "colLength";
            this.colLength.OptionsColumn.AllowEdit = false;
            this.colLength.OptionsColumn.AllowFocus = false;
            this.colLength.OptionsColumn.ReadOnly = true;
            // 
            // colWidth
            // 
            this.colWidth.Caption = "Width (M)";
            this.colWidth.FieldName = "Width";
            this.colWidth.Name = "colWidth";
            this.colWidth.OptionsColumn.AllowEdit = false;
            this.colWidth.OptionsColumn.AllowFocus = false;
            this.colWidth.OptionsColumn.ReadOnly = true;
            // 
            // colAssetStatusDescription
            // 
            this.colAssetStatusDescription.Caption = "Asset Status";
            this.colAssetStatusDescription.FieldName = "AssetStatusDescription";
            this.colAssetStatusDescription.Name = "colAssetStatusDescription";
            this.colAssetStatusDescription.OptionsColumn.AllowEdit = false;
            this.colAssetStatusDescription.OptionsColumn.AllowFocus = false;
            this.colAssetStatusDescription.OptionsColumn.ReadOnly = true;
            this.colAssetStatusDescription.Visible = true;
            this.colAssetStatusDescription.VisibleIndex = 13;
            this.colAssetStatusDescription.Width = 120;
            // 
            // colAssetPartNumber
            // 
            this.colAssetPartNumber.Caption = "Asset Part Number";
            this.colAssetPartNumber.FieldName = "AssetPartNumber";
            this.colAssetPartNumber.Name = "colAssetPartNumber";
            this.colAssetPartNumber.OptionsColumn.AllowEdit = false;
            this.colAssetPartNumber.OptionsColumn.AllowFocus = false;
            this.colAssetPartNumber.OptionsColumn.ReadOnly = true;
            this.colAssetPartNumber.Visible = true;
            this.colAssetPartNumber.VisibleIndex = 14;
            this.colAssetPartNumber.Width = 116;
            // 
            // colAssetSerialNumber
            // 
            this.colAssetSerialNumber.Caption = "Asset Serial Number";
            this.colAssetSerialNumber.FieldName = "AssetSerialNumber";
            this.colAssetSerialNumber.Name = "colAssetSerialNumber";
            this.colAssetSerialNumber.OptionsColumn.AllowEdit = false;
            this.colAssetSerialNumber.OptionsColumn.AllowFocus = false;
            this.colAssetSerialNumber.OptionsColumn.ReadOnly = true;
            this.colAssetSerialNumber.Visible = true;
            this.colAssetSerialNumber.VisibleIndex = 15;
            this.colAssetSerialNumber.Width = 126;
            // 
            // colAssetModelNumber
            // 
            this.colAssetModelNumber.Caption = "Asset Model Number";
            this.colAssetModelNumber.FieldName = "AssetModelNumber";
            this.colAssetModelNumber.Name = "colAssetModelNumber";
            this.colAssetModelNumber.OptionsColumn.AllowEdit = false;
            this.colAssetModelNumber.OptionsColumn.AllowFocus = false;
            this.colAssetModelNumber.OptionsColumn.ReadOnly = true;
            this.colAssetModelNumber.Visible = true;
            this.colAssetModelNumber.VisibleIndex = 16;
            this.colAssetModelNumber.Width = 128;
            // 
            // colAssetNumber
            // 
            this.colAssetNumber.Caption = "Asset Number";
            this.colAssetNumber.FieldName = "AssetNumber";
            this.colAssetNumber.Name = "colAssetNumber";
            this.colAssetNumber.OptionsColumn.AllowEdit = false;
            this.colAssetNumber.OptionsColumn.AllowFocus = false;
            this.colAssetNumber.OptionsColumn.ReadOnly = true;
            this.colAssetNumber.Visible = true;
            this.colAssetNumber.VisibleIndex = 2;
            this.colAssetNumber.Width = 122;
            // 
            // colAssetLifeSpanValue
            // 
            this.colAssetLifeSpanValue.Caption = "Asset Life Span";
            this.colAssetLifeSpanValue.FieldName = "AssetLifeSpanValue";
            this.colAssetLifeSpanValue.Name = "colAssetLifeSpanValue";
            this.colAssetLifeSpanValue.OptionsColumn.AllowEdit = false;
            this.colAssetLifeSpanValue.OptionsColumn.AllowFocus = false;
            this.colAssetLifeSpanValue.OptionsColumn.ReadOnly = true;
            this.colAssetLifeSpanValue.Width = 104;
            // 
            // colAssetLifeSpanValueDescriptor
            // 
            this.colAssetLifeSpanValueDescriptor.Caption = "Asset Life Span Descriptor";
            this.colAssetLifeSpanValueDescriptor.FieldName = "AssetLifeSpanValueDescriptor";
            this.colAssetLifeSpanValueDescriptor.Name = "colAssetLifeSpanValueDescriptor";
            this.colAssetLifeSpanValueDescriptor.OptionsColumn.AllowEdit = false;
            this.colAssetLifeSpanValueDescriptor.OptionsColumn.AllowFocus = false;
            this.colAssetLifeSpanValueDescriptor.OptionsColumn.ReadOnly = true;
            this.colAssetLifeSpanValueDescriptor.Width = 161;
            // 
            // colAssetConditionDescription
            // 
            this.colAssetConditionDescription.Caption = "Asset Condition";
            this.colAssetConditionDescription.FieldName = "AssetConditionDescription";
            this.colAssetConditionDescription.Name = "colAssetConditionDescription";
            this.colAssetConditionDescription.OptionsColumn.AllowEdit = false;
            this.colAssetConditionDescription.OptionsColumn.AllowFocus = false;
            this.colAssetConditionDescription.OptionsColumn.ReadOnly = true;
            this.colAssetConditionDescription.Width = 117;
            // 
            // colAssetLastVisitDate
            // 
            this.colAssetLastVisitDate.Caption = "Asset Last Visit";
            this.colAssetLastVisitDate.FieldName = "AssetLastVisitDate";
            this.colAssetLastVisitDate.Name = "colAssetLastVisitDate";
            this.colAssetLastVisitDate.OptionsColumn.AllowEdit = false;
            this.colAssetLastVisitDate.OptionsColumn.AllowFocus = false;
            this.colAssetLastVisitDate.OptionsColumn.ReadOnly = true;
            this.colAssetLastVisitDate.Width = 112;
            // 
            // colAssetNextVisitDate
            // 
            this.colAssetNextVisitDate.Caption = "Asset Next Visit";
            this.colAssetNextVisitDate.FieldName = "AssetNextVisitDate";
            this.colAssetNextVisitDate.Name = "colAssetNextVisitDate";
            this.colAssetNextVisitDate.OptionsColumn.AllowEdit = false;
            this.colAssetNextVisitDate.OptionsColumn.AllowFocus = false;
            this.colAssetNextVisitDate.OptionsColumn.ReadOnly = true;
            this.colAssetNextVisitDate.Width = 110;
            // 
            // colAssetRemarks
            // 
            this.colAssetRemarks.Caption = "Asset Remarks";
            this.colAssetRemarks.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colAssetRemarks.FieldName = "AssetRemarks";
            this.colAssetRemarks.Name = "colAssetRemarks";
            this.colAssetRemarks.OptionsColumn.ReadOnly = true;
            this.colAssetRemarks.Width = 110;
            // 
            // colMapID
            // 
            this.colMapID.Caption = "Map ID";
            this.colMapID.FieldName = "MapID";
            this.colMapID.Name = "colMapID";
            this.colMapID.OptionsColumn.AllowEdit = false;
            this.colMapID.OptionsColumn.AllowFocus = false;
            this.colMapID.OptionsColumn.ReadOnly = true;
            // 
            // colInspectionDate1
            // 
            this.colInspectionDate1.Caption = "Inspection Date";
            this.colInspectionDate1.FieldName = "InspectionDate";
            this.colInspectionDate1.Name = "colInspectionDate1";
            this.colInspectionDate1.OptionsColumn.AllowEdit = false;
            this.colInspectionDate1.OptionsColumn.AllowFocus = false;
            this.colInspectionDate1.OptionsColumn.ReadOnly = true;
            this.colInspectionDate1.Width = 96;
            // 
            // colInspectionNumber1
            // 
            this.colInspectionNumber1.Caption = "Inspection Number";
            this.colInspectionNumber1.FieldName = "InspectionNumber";
            this.colInspectionNumber1.Name = "colInspectionNumber1";
            this.colInspectionNumber1.OptionsColumn.AllowFocus = false;
            this.colInspectionNumber1.OptionsColumn.ReadOnly = true;
            this.colInspectionNumber1.Visible = true;
            this.colInspectionNumber1.VisibleIndex = 3;
            this.colInspectionNumber1.Width = 116;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp03057EPTimeDeductionsForInspectionBindingSource;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(36, 252);
            this.gridControl1.MainView = this.gridView2;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit4});
            this.gridControl1.Size = new System.Drawing.Size(556, 183);
            this.gridControl1.TabIndex = 19;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp03057EPTimeDeductionsForInspectionBindingSource
            // 
            this.sp03057EPTimeDeductionsForInspectionBindingSource.DataMember = "sp03057_EP_Time_Deductions_For_Inspection";
            this.sp03057EPTimeDeductionsForInspectionBindingSource.DataSource = this.dataSet_EP_DataEntry;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDeductionID,
            this.colInspectionID,
            this.colDescription,
            this.colStartTime,
            this.colEndTime,
            this.colInspectionDate,
            this.colInspectionNumber,
            this.gridColumn6});
            this.gridView2.GridControl = this.gridControl1;
            this.gridView2.GroupCount = 1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colInspectionNumber, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colStartTime, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView2_CustomDrawEmptyForeground);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView2_MouseUp);
            this.gridView2.DoubleClick += new System.EventHandler(this.gridView2_DoubleClick);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // colDeductionID
            // 
            this.colDeductionID.Caption = "Deduction ID";
            this.colDeductionID.FieldName = "DeductionID";
            this.colDeductionID.Name = "colDeductionID";
            this.colDeductionID.OptionsColumn.AllowEdit = false;
            this.colDeductionID.OptionsColumn.AllowFocus = false;
            this.colDeductionID.OptionsColumn.ReadOnly = true;
            this.colDeductionID.Width = 106;
            // 
            // colInspectionID
            // 
            this.colInspectionID.Caption = "Inspection ID";
            this.colInspectionID.FieldName = "InspectionID";
            this.colInspectionID.Name = "colInspectionID";
            this.colInspectionID.OptionsColumn.AllowEdit = false;
            this.colInspectionID.OptionsColumn.AllowFocus = false;
            this.colInspectionID.OptionsColumn.ReadOnly = true;
            this.colInspectionID.Width = 107;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Deduction Description";
            this.colDescription.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 3;
            this.colDescription.Width = 322;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colStartTime
            // 
            this.colStartTime.Caption = "Start";
            this.colStartTime.ColumnEdit = this.repositoryItemTextEdit1;
            this.colStartTime.FieldName = "StartTime";
            this.colStartTime.Name = "colStartTime";
            this.colStartTime.OptionsColumn.AllowEdit = false;
            this.colStartTime.OptionsColumn.AllowFocus = false;
            this.colStartTime.OptionsColumn.ReadOnly = true;
            this.colStartTime.Visible = true;
            this.colStartTime.VisibleIndex = 0;
            this.colStartTime.Width = 114;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "g";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colEndTime
            // 
            this.colEndTime.Caption = "End";
            this.colEndTime.ColumnEdit = this.repositoryItemTextEdit1;
            this.colEndTime.FieldName = "EndTime";
            this.colEndTime.Name = "colEndTime";
            this.colEndTime.OptionsColumn.AllowEdit = false;
            this.colEndTime.OptionsColumn.AllowFocus = false;
            this.colEndTime.OptionsColumn.ReadOnly = true;
            this.colEndTime.Visible = true;
            this.colEndTime.VisibleIndex = 1;
            this.colEndTime.Width = 97;
            // 
            // colInspectionDate
            // 
            this.colInspectionDate.Caption = "Inspection Date";
            this.colInspectionDate.FieldName = "InspectionDate";
            this.colInspectionDate.Name = "colInspectionDate";
            this.colInspectionDate.OptionsColumn.AllowEdit = false;
            this.colInspectionDate.OptionsColumn.AllowFocus = false;
            this.colInspectionDate.OptionsColumn.ReadOnly = true;
            this.colInspectionDate.Width = 99;
            // 
            // colInspectionNumber
            // 
            this.colInspectionNumber.Caption = "Inspection Number";
            this.colInspectionNumber.FieldName = "InspectionNumber";
            this.colInspectionNumber.Name = "colInspectionNumber";
            this.colInspectionNumber.OptionsColumn.AllowEdit = false;
            this.colInspectionNumber.OptionsColumn.AllowFocus = false;
            this.colInspectionNumber.OptionsColumn.ReadOnly = true;
            this.colInspectionNumber.Width = 211;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Elapsed Time";
            this.gridColumn6.ColumnEdit = this.repositoryItemTextEdit4;
            this.gridColumn6.FieldName = "gridColumn6";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.ShowUnboundExpressionMenu = true;
            this.gridColumn6.UnboundExpression = "DateDiffMinute([StartTime],[EndTime] )";
            this.gridColumn6.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 2;
            this.gridColumn6.Width = 82;
            // 
            // repositoryItemTextEdit4
            // 
            this.repositoryItemTextEdit4.AutoHeight = false;
            this.repositoryItemTextEdit4.Mask.EditMask = "######0 Minutes";
            this.repositoryItemTextEdit4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit4.Name = "repositoryItemTextEdit4";
            // 
            // btnUnlink
            // 
            this.btnUnlink.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnUnlink.ImageOptions.Image")));
            this.btnUnlink.Location = new System.Drawing.Point(396, 146);
            this.btnUnlink.Name = "btnUnlink";
            this.btnUnlink.Size = new System.Drawing.Size(196, 22);
            this.btnUnlink.StyleController = this.dataLayoutControl1;
            toolTipTitleItem5.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Text = "Unlink Inspection from Switchboard button - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = resources.GetString("toolTipItem5.Text");
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.btnUnlink.SuperTip = superToolTip5;
            this.btnUnlink.TabIndex = 18;
            this.btnUnlink.Text = "Unlink Inspection from Switchboard";
            this.btnUnlink.Click += new System.EventHandler(this.btnUnlink_Click);
            // 
            // btnLink
            // 
            this.btnLink.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnLink.ImageOptions.Image")));
            this.btnLink.Location = new System.Drawing.Point(218, 146);
            this.btnLink.Name = "btnLink";
            this.btnLink.Size = new System.Drawing.Size(174, 22);
            this.btnLink.StyleController = this.dataLayoutControl1;
            superToolTip6.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem6.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Appearance.Options.UseImage = true;
            toolTipTitleItem6.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Text = "Link Inspection to Switchboard button - Information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Click me to link the current inspection to the Site Inspection Switchboard Contro" +
    "l Panel from which it can be controlled.";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this.btnLink.SuperTip = superToolTip6;
            this.btnLink.TabIndex = 17;
            this.btnLink.Text = "Link Inspection to Switchboard";
            this.btnLink.Click += new System.EventHandler(this.btnLink_Click);
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp03051EPSiteInspectionEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(141, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(173, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 15;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // sp03051EPSiteInspectionEditBindingSource
            // 
            this.sp03051EPSiteInspectionEditBindingSource.DataMember = "sp03051_EP_Site_Inspection_Edit";
            this.sp03051EPSiteInspectionEditBindingSource.DataSource = this.dataSet_EP_DataEntry;
            // 
            // InspectionIDTextEdit
            // 
            this.InspectionIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03051EPSiteInspectionEditBindingSource, "InspectionID", true));
            this.InspectionIDTextEdit.Location = new System.Drawing.Point(165, 12);
            this.InspectionIDTextEdit.MenuManager = this.barManager1;
            this.InspectionIDTextEdit.Name = "InspectionIDTextEdit";
            this.InspectionIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.InspectionIDTextEdit, true);
            this.InspectionIDTextEdit.Size = new System.Drawing.Size(451, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.InspectionIDTextEdit, optionsSpelling1);
            this.InspectionIDTextEdit.StyleController = this.dataLayoutControl1;
            this.InspectionIDTextEdit.TabIndex = 4;
            // 
            // InspectionDateDateEdit
            // 
            this.InspectionDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03051EPSiteInspectionEditBindingSource, "InspectionDate", true));
            this.InspectionDateDateEdit.EditValue = null;
            this.InspectionDateDateEdit.Location = new System.Drawing.Point(141, 62);
            this.InspectionDateDateEdit.MenuManager = this.barManager1;
            this.InspectionDateDateEdit.Name = "InspectionDateDateEdit";
            this.InspectionDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.InspectionDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.InspectionDateDateEdit.Size = new System.Drawing.Size(475, 20);
            this.InspectionDateDateEdit.StyleController = this.dataLayoutControl1;
            this.InspectionDateDateEdit.TabIndex = 6;
            this.InspectionDateDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.InspectionDateDateEdit_Validating);
            // 
            // InspectionStartTimeTimeEdit
            // 
            this.InspectionStartTimeTimeEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03051EPSiteInspectionEditBindingSource, "InspectionStartTime", true));
            this.InspectionStartTimeTimeEdit.EditValue = new System.DateTime(2011, 3, 14, 0, 0, 0, 0);
            this.InspectionStartTimeTimeEdit.Location = new System.Drawing.Point(165, 252);
            this.InspectionStartTimeTimeEdit.MenuManager = this.barManager1;
            this.InspectionStartTimeTimeEdit.Name = "InspectionStartTimeTimeEdit";
            this.InspectionStartTimeTimeEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Enter Current Time", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", "now", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.InspectionStartTimeTimeEdit.Properties.Mask.EditMask = "g";
            this.InspectionStartTimeTimeEdit.Size = new System.Drawing.Size(427, 20);
            this.InspectionStartTimeTimeEdit.StyleController = this.dataLayoutControl1;
            this.InspectionStartTimeTimeEdit.TabIndex = 8;
            this.InspectionStartTimeTimeEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.InspectionStartTimeTimeEdit_ButtonClick);
            // 
            // InspectionEndTimeTimeEdit
            // 
            this.InspectionEndTimeTimeEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03051EPSiteInspectionEditBindingSource, "InspectionEndTime", true));
            this.InspectionEndTimeTimeEdit.EditValue = new System.DateTime(2011, 3, 14, 0, 0, 0, 0);
            this.InspectionEndTimeTimeEdit.Location = new System.Drawing.Point(165, 276);
            this.InspectionEndTimeTimeEdit.MenuManager = this.barManager1;
            this.InspectionEndTimeTimeEdit.Name = "InspectionEndTimeTimeEdit";
            this.InspectionEndTimeTimeEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Enter Current Time", -1, true, true, true, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", "now", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.InspectionEndTimeTimeEdit.Properties.Mask.EditMask = "g";
            this.InspectionEndTimeTimeEdit.Size = new System.Drawing.Size(427, 20);
            this.InspectionEndTimeTimeEdit.StyleController = this.dataLayoutControl1;
            this.InspectionEndTimeTimeEdit.TabIndex = 9;
            this.InspectionEndTimeTimeEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.InspectionEndTimeTimeEdit_ButtonClick);
            // 
            // TotalTimeDeductionsSpinEdit
            // 
            this.TotalTimeDeductionsSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03051EPSiteInspectionEditBindingSource, "TotalTimeDeductions", true));
            this.TotalTimeDeductionsSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TotalTimeDeductionsSpinEdit.Location = new System.Drawing.Point(165, 300);
            this.TotalTimeDeductionsSpinEdit.MenuManager = this.barManager1;
            this.TotalTimeDeductionsSpinEdit.Name = "TotalTimeDeductionsSpinEdit";
            toolTipTitleItem7.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Appearance.Options.UseImage = true;
            toolTipTitleItem7.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Text = "Calculate Time Deductions Button - Information";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Click me to sum the total time deductions from linked Time Deduction records.";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            this.TotalTimeDeductionsSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Calculate", -1, true, true, true, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "", "calculate", superToolTip7, DevExpress.Utils.ToolTipAnchor.Default)});
            this.TotalTimeDeductionsSpinEdit.Properties.Mask.EditMask = "######0.00 Hour(s)";
            this.TotalTimeDeductionsSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalTimeDeductionsSpinEdit.Properties.ReadOnly = true;
            this.TotalTimeDeductionsSpinEdit.Size = new System.Drawing.Size(427, 20);
            this.TotalTimeDeductionsSpinEdit.StyleController = this.dataLayoutControl1;
            this.TotalTimeDeductionsSpinEdit.TabIndex = 10;
            this.TotalTimeDeductionsSpinEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.TotalTimeDeductionsSpinEdit_ButtonClick);
            // 
            // TotalTimeDeductionsRemarksMemoEdit
            // 
            this.TotalTimeDeductionsRemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03051EPSiteInspectionEditBindingSource, "TotalTimeDeductionsRemarks", true));
            this.TotalTimeDeductionsRemarksMemoEdit.Location = new System.Drawing.Point(165, 324);
            this.TotalTimeDeductionsRemarksMemoEdit.MenuManager = this.barManager1;
            this.TotalTimeDeductionsRemarksMemoEdit.Name = "TotalTimeDeductionsRemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.TotalTimeDeductionsRemarksMemoEdit, true);
            this.TotalTimeDeductionsRemarksMemoEdit.Size = new System.Drawing.Size(427, 87);
            this.scSpellChecker.SetSpellCheckerOptions(this.TotalTimeDeductionsRemarksMemoEdit, optionsSpelling2);
            this.TotalTimeDeductionsRemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.TotalTimeDeductionsRemarksMemoEdit.TabIndex = 11;
            // 
            // TotalTimeTakenSpinEdit
            // 
            this.TotalTimeTakenSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03051EPSiteInspectionEditBindingSource, "TotalTimeTaken", true));
            this.TotalTimeTakenSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TotalTimeTakenSpinEdit.Location = new System.Drawing.Point(165, 415);
            this.TotalTimeTakenSpinEdit.MenuManager = this.barManager1;
            this.TotalTimeTakenSpinEdit.Name = "TotalTimeTakenSpinEdit";
            this.TotalTimeTakenSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Calculate", -1, true, true, true, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Calculate Elapsed Time [End - (Start + Total Time Deducted)]", "calculate", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.TotalTimeTakenSpinEdit.Properties.Mask.EditMask = "######0.00 Hour(s)";
            this.TotalTimeTakenSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalTimeTakenSpinEdit.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.TotalTimeTakenSpinEdit_Properties_ButtonClick);
            this.TotalTimeTakenSpinEdit.Size = new System.Drawing.Size(427, 20);
            this.TotalTimeTakenSpinEdit.StyleController = this.dataLayoutControl1;
            this.TotalTimeTakenSpinEdit.TabIndex = 12;
            this.TotalTimeTakenSpinEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.TotalTimeTakenSpinEdit_Properties_ButtonClick);
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03051EPSiteInspectionEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, 252);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(556, 183);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling3);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 13;
            // 
            // InspectionNumberButtonEdit
            // 
            this.InspectionNumberButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03051EPSiteInspectionEditBindingSource, "InspectionNumber", true));
            this.InspectionNumberButtonEdit.Location = new System.Drawing.Point(141, 86);
            this.InspectionNumberButtonEdit.MenuManager = this.barManager1;
            this.InspectionNumberButtonEdit.Name = "InspectionNumberButtonEdit";
            superToolTip8.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem8.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem8.Appearance.Options.UseImage = true;
            toolTipTitleItem8.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem8.Text = "Sequence Button - Information";
            toolTipItem8.LeftIndent = 6;
            toolTipItem8.Text = "Click me to <b>open</b> the <b>Choose Sequence screen</b> to <b>select the prefix" +
    "</b> for the sequence.";
            superToolTip8.Items.Add(toolTipTitleItem8);
            superToolTip8.Items.Add(toolTipItem8);
            superToolTip9.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem9.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem9.Appearance.Options.UseImage = true;
            toolTipTitleItem9.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem9.Text = "Number Button - Information";
            toolTipItem9.LeftIndent = 6;
            toolTipItem9.Text = "Click me to <b>calculate</b> the <b>number suffix</b> for the sequence.";
            superToolTip9.Items.Add(toolTipTitleItem9);
            superToolTip9.Items.Add(toolTipItem9);
            this.InspectionNumberButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Sequence", -1, true, true, true, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "", "Sequence", superToolTip8, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Number", -1, true, true, false, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "", "Number", superToolTip9, DevExpress.Utils.ToolTipAnchor.Default)});
            this.InspectionNumberButtonEdit.Size = new System.Drawing.Size(475, 20);
            this.InspectionNumberButtonEdit.StyleController = this.dataLayoutControl1;
            this.InspectionNumberButtonEdit.TabIndex = 14;
            this.InspectionNumberButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.InspectionNumberButtonEdit_ButtonClick);
            // 
            // SiteIDGridLookUpEdit
            // 
            this.SiteIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03051EPSiteInspectionEditBindingSource, "SiteID", true));
            this.SiteIDGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SiteIDGridLookUpEdit.Location = new System.Drawing.Point(141, 36);
            this.SiteIDGridLookUpEdit.MenuManager = this.barManager1;
            this.SiteIDGridLookUpEdit.Name = "SiteIDGridLookUpEdit";
            editorButtonImageOptions7.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions8.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.SiteIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions7, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject25, serializableAppearanceObject26, serializableAppearanceObject27, serializableAppearanceObject28, "Edit Underlying Data", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions8, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject29, serializableAppearanceObject30, serializableAppearanceObject31, serializableAppearanceObject32, "Reload Underlying Data", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.SiteIDGridLookUpEdit.Properties.DataSource = this.sp03028EPSitesWithBlankDDLBBindingSource;
            this.SiteIDGridLookUpEdit.Properties.DisplayMember = "ClientSiteName";
            this.SiteIDGridLookUpEdit.Properties.NullText = "";
            this.SiteIDGridLookUpEdit.Properties.PopupView = this.gridLookUpEdit1View;
            this.SiteIDGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.SiteIDGridLookUpEdit.Properties.ValueMember = "SiteID";
            this.SiteIDGridLookUpEdit.Size = new System.Drawing.Size(475, 22);
            this.SiteIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.SiteIDGridLookUpEdit.TabIndex = 5;
            this.SiteIDGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.SiteIDGridLookUpEdit_ButtonClick);
            this.SiteIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.SiteIDGridLookUpEdit_Validating);
            // 
            // sp03028EPSitesWithBlankDDLBBindingSource
            // 
            this.sp03028EPSitesWithBlankDDLBBindingSource.DataMember = "sp03028_EP_Sites_With_Blank_DDLB";
            this.sp03028EPSitesWithBlankDDLBBindingSource.DataSource = this.dataSet_EP_DataEntry;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientCode,
            this.colClientID,
            this.colClientName,
            this.colClientSiteName,
            this.colContactPerson,
            this.colSiteAddressLine1,
            this.colSiteCode,
            this.colSiteID,
            this.colSiteName,
            this.colSiteTypeDescription,
            this.colSiteTypeID,
            this.colXCoordinate,
            this.colYCoordinate});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colSiteID;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridLookUpEdit1View.GroupCount = 1;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colClientCode
            // 
            this.colClientCode.Caption = "Client Code";
            this.colClientCode.FieldName = "ClientCode";
            this.colClientCode.Name = "colClientCode";
            this.colClientCode.OptionsColumn.AllowEdit = false;
            this.colClientCode.OptionsColumn.AllowFocus = false;
            this.colClientCode.OptionsColumn.ReadOnly = true;
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Width = 181;
            // 
            // colClientSiteName
            // 
            this.colClientSiteName.Caption = "Client \\ Site Name";
            this.colClientSiteName.FieldName = "ClientSiteName";
            this.colClientSiteName.Name = "colClientSiteName";
            this.colClientSiteName.OptionsColumn.AllowEdit = false;
            this.colClientSiteName.OptionsColumn.AllowFocus = false;
            this.colClientSiteName.OptionsColumn.ReadOnly = true;
            this.colClientSiteName.Width = 242;
            // 
            // colContactPerson
            // 
            this.colContactPerson.Caption = "Contact Person";
            this.colContactPerson.FieldName = "ContactPerson";
            this.colContactPerson.Name = "colContactPerson";
            this.colContactPerson.OptionsColumn.AllowEdit = false;
            this.colContactPerson.OptionsColumn.AllowFocus = false;
            this.colContactPerson.OptionsColumn.ReadOnly = true;
            this.colContactPerson.Visible = true;
            this.colContactPerson.VisibleIndex = 3;
            this.colContactPerson.Width = 168;
            // 
            // colSiteAddressLine1
            // 
            this.colSiteAddressLine1.Caption = "Address Line 1";
            this.colSiteAddressLine1.FieldName = "SiteAddressLine1";
            this.colSiteAddressLine1.Name = "colSiteAddressLine1";
            this.colSiteAddressLine1.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine1.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine1.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine1.Visible = true;
            this.colSiteAddressLine1.VisibleIndex = 4;
            this.colSiteAddressLine1.Width = 141;
            // 
            // colSiteCode
            // 
            this.colSiteCode.Caption = "Site Code";
            this.colSiteCode.FieldName = "SiteCode";
            this.colSiteCode.Name = "colSiteCode";
            this.colSiteCode.OptionsColumn.AllowEdit = false;
            this.colSiteCode.OptionsColumn.AllowFocus = false;
            this.colSiteCode.OptionsColumn.ReadOnly = true;
            this.colSiteCode.Visible = true;
            this.colSiteCode.VisibleIndex = 1;
            this.colSiteCode.Width = 96;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.Visible = true;
            this.colSiteName.VisibleIndex = 0;
            this.colSiteName.Width = 251;
            // 
            // colSiteTypeDescription
            // 
            this.colSiteTypeDescription.Caption = "Site Type";
            this.colSiteTypeDescription.FieldName = "SiteTypeDescription";
            this.colSiteTypeDescription.Name = "colSiteTypeDescription";
            this.colSiteTypeDescription.OptionsColumn.AllowEdit = false;
            this.colSiteTypeDescription.OptionsColumn.AllowFocus = false;
            this.colSiteTypeDescription.OptionsColumn.ReadOnly = true;
            this.colSiteTypeDescription.Visible = true;
            this.colSiteTypeDescription.VisibleIndex = 2;
            this.colSiteTypeDescription.Width = 99;
            // 
            // colSiteTypeID
            // 
            this.colSiteTypeID.Caption = "Site Type ID";
            this.colSiteTypeID.FieldName = "SiteTypeID";
            this.colSiteTypeID.Name = "colSiteTypeID";
            this.colSiteTypeID.OptionsColumn.AllowEdit = false;
            this.colSiteTypeID.OptionsColumn.AllowFocus = false;
            this.colSiteTypeID.OptionsColumn.ReadOnly = true;
            // 
            // colXCoordinate
            // 
            this.colXCoordinate.Caption = "X Coordinate";
            this.colXCoordinate.FieldName = "XCoordinate";
            this.colXCoordinate.Name = "colXCoordinate";
            this.colXCoordinate.OptionsColumn.AllowEdit = false;
            this.colXCoordinate.OptionsColumn.AllowFocus = false;
            this.colXCoordinate.OptionsColumn.ReadOnly = true;
            // 
            // colYCoordinate
            // 
            this.colYCoordinate.Caption = "Y Coordinate";
            this.colYCoordinate.FieldName = "YCoordinate";
            this.colYCoordinate.Name = "colYCoordinate";
            this.colYCoordinate.OptionsColumn.AllowEdit = false;
            this.colYCoordinate.OptionsColumn.AllowFocus = false;
            this.colYCoordinate.OptionsColumn.ReadOnly = true;
            // 
            // InspectorIDGridLookUpEdit
            // 
            this.InspectorIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03051EPSiteInspectionEditBindingSource, "InspectorID", true));
            this.InspectorIDGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.InspectorIDGridLookUpEdit.Location = new System.Drawing.Point(141, 110);
            this.InspectorIDGridLookUpEdit.MenuManager = this.barManager1;
            this.InspectorIDGridLookUpEdit.Name = "InspectorIDGridLookUpEdit";
            editorButtonImageOptions9.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions10.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.InspectorIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions9, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject33, serializableAppearanceObject34, serializableAppearanceObject35, serializableAppearanceObject36, "Edit Underlying Data", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions10, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject37, serializableAppearanceObject38, serializableAppearanceObject39, serializableAppearanceObject40, "Reload Underlying Data", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.InspectorIDGridLookUpEdit.Properties.DataSource = this.sp00190ContractorListWithBlankBindingSource;
            this.InspectorIDGridLookUpEdit.Properties.DisplayMember = "ContractorName";
            this.InspectorIDGridLookUpEdit.Properties.NullText = "";
            this.InspectorIDGridLookUpEdit.Properties.PopupView = this.gridView1;
            this.InspectorIDGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.InspectorIDGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.InspectorIDGridLookUpEdit.Properties.ValueMember = "ContractorID";
            this.InspectorIDGridLookUpEdit.Size = new System.Drawing.Size(475, 22);
            this.InspectorIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.InspectorIDGridLookUpEdit.TabIndex = 7;
            this.InspectorIDGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.InspectorIDGridLookUpEdit_ButtonClick);
            this.InspectorIDGridLookUpEdit.Enter += new System.EventHandler(this.InspectorIDGridLookUpEdit_Enter);
            this.InspectorIDGridLookUpEdit.Leave += new System.EventHandler(this.InspectorIDGridLookUpEdit_Leave);
            // 
            // sp00190ContractorListWithBlankBindingSource
            // 
            this.sp00190ContractorListWithBlankBindingSource.DataMember = "sp00190_Contractor_List_With_Blank";
            this.sp00190ContractorListWithBlankBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAddressLine1,
            this.colAddressLine2,
            this.colAddressLine3,
            this.colAddressLine4,
            this.colAddressLine5,
            this.colContractorCode,
            this.colContractorID,
            this.colContractorName,
            this.colDisabled,
            this.colEmailPassword,
            this.colInternalContractor,
            this.colInternalCOntractorDescription,
            this.colMobile,
            this.colPostcode,
            this.colRemarks,
            this.colTelephone1,
            this.colTelephone2,
            this.colTypeDescription,
            this.colTypeID,
            this.colUserDefined1,
            this.colUserDefined2,
            this.colUserDefined3,
            this.colVatReg,
            this.colWebsite});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.colContractorID;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridView1.GroupCount = 1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colInternalCOntractorDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colContractorName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colAddressLine1
            // 
            this.colAddressLine1.Caption = "Address Line 1";
            this.colAddressLine1.FieldName = "AddressLine1";
            this.colAddressLine1.Name = "colAddressLine1";
            this.colAddressLine1.OptionsColumn.AllowEdit = false;
            this.colAddressLine1.OptionsColumn.AllowFocus = false;
            this.colAddressLine1.OptionsColumn.ReadOnly = true;
            this.colAddressLine1.Visible = true;
            this.colAddressLine1.VisibleIndex = 2;
            this.colAddressLine1.Width = 144;
            // 
            // colAddressLine2
            // 
            this.colAddressLine2.Caption = "Address Line 2";
            this.colAddressLine2.FieldName = "AddressLine2";
            this.colAddressLine2.Name = "colAddressLine2";
            this.colAddressLine2.OptionsColumn.AllowEdit = false;
            this.colAddressLine2.OptionsColumn.AllowFocus = false;
            this.colAddressLine2.OptionsColumn.ReadOnly = true;
            this.colAddressLine2.Width = 91;
            // 
            // colAddressLine3
            // 
            this.colAddressLine3.Caption = "Address Line 3";
            this.colAddressLine3.FieldName = "AddressLine3";
            this.colAddressLine3.Name = "colAddressLine3";
            this.colAddressLine3.OptionsColumn.AllowEdit = false;
            this.colAddressLine3.OptionsColumn.AllowFocus = false;
            this.colAddressLine3.OptionsColumn.ReadOnly = true;
            this.colAddressLine3.Width = 91;
            // 
            // colAddressLine4
            // 
            this.colAddressLine4.Caption = "Address Line 4";
            this.colAddressLine4.FieldName = "AddressLine4";
            this.colAddressLine4.Name = "colAddressLine4";
            this.colAddressLine4.OptionsColumn.AllowEdit = false;
            this.colAddressLine4.OptionsColumn.AllowFocus = false;
            this.colAddressLine4.OptionsColumn.ReadOnly = true;
            this.colAddressLine4.Width = 91;
            // 
            // colAddressLine5
            // 
            this.colAddressLine5.Caption = "Address Line 5";
            this.colAddressLine5.FieldName = "AddressLine5";
            this.colAddressLine5.Name = "colAddressLine5";
            this.colAddressLine5.OptionsColumn.AllowEdit = false;
            this.colAddressLine5.OptionsColumn.AllowFocus = false;
            this.colAddressLine5.OptionsColumn.ReadOnly = true;
            this.colAddressLine5.Width = 91;
            // 
            // colContractorCode
            // 
            this.colContractorCode.Caption = "Contractor Code";
            this.colContractorCode.FieldName = "ContractorCode";
            this.colContractorCode.Name = "colContractorCode";
            this.colContractorCode.OptionsColumn.AllowEdit = false;
            this.colContractorCode.OptionsColumn.AllowFocus = false;
            this.colContractorCode.OptionsColumn.ReadOnly = true;
            this.colContractorCode.Width = 101;
            // 
            // colContractorName
            // 
            this.colContractorName.Caption = "Contractor Name";
            this.colContractorName.FieldName = "ContractorName";
            this.colContractorName.Name = "colContractorName";
            this.colContractorName.OptionsColumn.AllowEdit = false;
            this.colContractorName.OptionsColumn.AllowFocus = false;
            this.colContractorName.OptionsColumn.ReadOnly = true;
            this.colContractorName.Visible = true;
            this.colContractorName.VisibleIndex = 0;
            this.colContractorName.Width = 273;
            // 
            // colDisabled
            // 
            this.colDisabled.Caption = "Disabled";
            this.colDisabled.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDisabled.FieldName = "Disabled";
            this.colDisabled.Name = "colDisabled";
            this.colDisabled.OptionsColumn.AllowEdit = false;
            this.colDisabled.OptionsColumn.AllowFocus = false;
            this.colDisabled.OptionsColumn.ReadOnly = true;
            this.colDisabled.Visible = true;
            this.colDisabled.VisibleIndex = 3;
            this.colDisabled.Width = 61;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colEmailPassword
            // 
            this.colEmailPassword.Caption = "Email Password";
            this.colEmailPassword.FieldName = "EmailPassword";
            this.colEmailPassword.Name = "colEmailPassword";
            this.colEmailPassword.OptionsColumn.AllowEdit = false;
            this.colEmailPassword.OptionsColumn.AllowFocus = false;
            this.colEmailPassword.OptionsColumn.ReadOnly = true;
            this.colEmailPassword.Width = 94;
            // 
            // colInternalContractor
            // 
            this.colInternalContractor.Caption = "Internal Contractor ID";
            this.colInternalContractor.FieldName = "InternalContractor";
            this.colInternalContractor.Name = "colInternalContractor";
            this.colInternalContractor.OptionsColumn.AllowEdit = false;
            this.colInternalContractor.OptionsColumn.AllowFocus = false;
            this.colInternalContractor.OptionsColumn.ReadOnly = true;
            this.colInternalContractor.Width = 118;
            // 
            // colInternalCOntractorDescription
            // 
            this.colInternalCOntractorDescription.Caption = "Status";
            this.colInternalCOntractorDescription.FieldName = "InternalCOntractorDescription";
            this.colInternalCOntractorDescription.Name = "colInternalCOntractorDescription";
            this.colInternalCOntractorDescription.OptionsColumn.AllowEdit = false;
            this.colInternalCOntractorDescription.OptionsColumn.AllowFocus = false;
            this.colInternalCOntractorDescription.OptionsColumn.ReadOnly = true;
            this.colInternalCOntractorDescription.Width = 52;
            // 
            // colMobile
            // 
            this.colMobile.Caption = "Mobile";
            this.colMobile.FieldName = "Mobile";
            this.colMobile.Name = "colMobile";
            this.colMobile.OptionsColumn.AllowEdit = false;
            this.colMobile.OptionsColumn.AllowFocus = false;
            this.colMobile.OptionsColumn.ReadOnly = true;
            this.colMobile.Width = 51;
            // 
            // colPostcode
            // 
            this.colPostcode.Caption = "Postcode";
            this.colPostcode.FieldName = "Postcode";
            this.colPostcode.Name = "colPostcode";
            this.colPostcode.OptionsColumn.AllowEdit = false;
            this.colPostcode.OptionsColumn.AllowFocus = false;
            this.colPostcode.OptionsColumn.ReadOnly = true;
            this.colPostcode.Width = 65;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Width = 62;
            // 
            // colTelephone1
            // 
            this.colTelephone1.Caption = "Telephone 1";
            this.colTelephone1.FieldName = "Telephone1";
            this.colTelephone1.Name = "colTelephone1";
            this.colTelephone1.OptionsColumn.AllowEdit = false;
            this.colTelephone1.OptionsColumn.AllowFocus = false;
            this.colTelephone1.OptionsColumn.ReadOnly = true;
            this.colTelephone1.Width = 80;
            // 
            // colTelephone2
            // 
            this.colTelephone2.Caption = "Telephone 2";
            this.colTelephone2.FieldName = "Telephone2";
            this.colTelephone2.Name = "colTelephone2";
            this.colTelephone2.OptionsColumn.AllowEdit = false;
            this.colTelephone2.OptionsColumn.AllowFocus = false;
            this.colTelephone2.OptionsColumn.ReadOnly = true;
            this.colTelephone2.Width = 80;
            // 
            // colTypeDescription
            // 
            this.colTypeDescription.Caption = "Type";
            this.colTypeDescription.FieldName = "TypeDescription";
            this.colTypeDescription.Name = "colTypeDescription";
            this.colTypeDescription.OptionsColumn.AllowEdit = false;
            this.colTypeDescription.OptionsColumn.AllowFocus = false;
            this.colTypeDescription.OptionsColumn.ReadOnly = true;
            this.colTypeDescription.Visible = true;
            this.colTypeDescription.VisibleIndex = 1;
            this.colTypeDescription.Width = 111;
            // 
            // colTypeID
            // 
            this.colTypeID.Caption = "Type ID";
            this.colTypeID.FieldName = "TypeID";
            this.colTypeID.Name = "colTypeID";
            this.colTypeID.OptionsColumn.AllowEdit = false;
            this.colTypeID.OptionsColumn.AllowFocus = false;
            this.colTypeID.OptionsColumn.ReadOnly = true;
            this.colTypeID.Width = 49;
            // 
            // colUserDefined1
            // 
            this.colUserDefined1.Caption = "User Defined 1";
            this.colUserDefined1.FieldName = "UserDefined1";
            this.colUserDefined1.Name = "colUserDefined1";
            this.colUserDefined1.OptionsColumn.AllowEdit = false;
            this.colUserDefined1.OptionsColumn.AllowFocus = false;
            this.colUserDefined1.OptionsColumn.ReadOnly = true;
            this.colUserDefined1.Width = 92;
            // 
            // colUserDefined2
            // 
            this.colUserDefined2.Caption = "User Defined 2";
            this.colUserDefined2.FieldName = "UserDefined2";
            this.colUserDefined2.Name = "colUserDefined2";
            this.colUserDefined2.OptionsColumn.AllowEdit = false;
            this.colUserDefined2.OptionsColumn.AllowFocus = false;
            this.colUserDefined2.OptionsColumn.ReadOnly = true;
            this.colUserDefined2.Width = 92;
            // 
            // colUserDefined3
            // 
            this.colUserDefined3.Caption = "User Defined 3";
            this.colUserDefined3.FieldName = "UserDefined3";
            this.colUserDefined3.Name = "colUserDefined3";
            this.colUserDefined3.OptionsColumn.AllowEdit = false;
            this.colUserDefined3.OptionsColumn.AllowFocus = false;
            this.colUserDefined3.OptionsColumn.ReadOnly = true;
            this.colUserDefined3.Width = 92;
            // 
            // colVatReg
            // 
            this.colVatReg.Caption = "VAT Reg";
            this.colVatReg.FieldName = "VatReg";
            this.colVatReg.Name = "colVatReg";
            this.colVatReg.OptionsColumn.AllowEdit = false;
            this.colVatReg.OptionsColumn.AllowFocus = false;
            this.colVatReg.OptionsColumn.ReadOnly = true;
            this.colVatReg.Width = 62;
            // 
            // colWebsite
            // 
            this.colWebsite.Caption = "Website";
            this.colWebsite.FieldName = "Website";
            this.colWebsite.Name = "colWebsite";
            this.colWebsite.OptionsColumn.AllowEdit = false;
            this.colWebsite.OptionsColumn.AllowFocus = false;
            this.colWebsite.OptionsColumn.ReadOnly = true;
            this.colWebsite.Width = 60;
            // 
            // btnSaveAndOpenMap
            // 
            this.btnSaveAndOpenMap.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Save_16x16;
            this.btnSaveAndOpenMap.Location = new System.Drawing.Point(12, 146);
            this.btnSaveAndOpenMap.Name = "btnSaveAndOpenMap";
            this.btnSaveAndOpenMap.Size = new System.Drawing.Size(202, 22);
            this.btnSaveAndOpenMap.StyleController = this.dataLayoutControl1;
            superToolTip10.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem10.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem10.Appearance.Options.UseImage = true;
            toolTipTitleItem10.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem10.Text = "Save Inspection && Proceed to Map button - Information";
            toolTipItem10.LeftIndent = 6;
            toolTipItem10.Text = "Click me to Save the current Site Inspection and open the map where linked assets" +
    " can be viewed and added.";
            superToolTip10.Items.Add(toolTipTitleItem10);
            superToolTip10.Items.Add(toolTipItem10);
            this.btnSaveAndOpenMap.SuperTip = superToolTip10;
            this.btnSaveAndOpenMap.TabIndex = 16;
            this.btnSaveAndOpenMap.Text = "Save Inspection && Proceed to Map";
            this.btnSaveAndOpenMap.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // ItemForInspectionID
            // 
            this.ItemForInspectionID.Control = this.InspectionIDTextEdit;
            this.ItemForInspectionID.CustomizationFormText = "Inspection ID:";
            this.ItemForInspectionID.Location = new System.Drawing.Point(0, 0);
            this.ItemForInspectionID.Name = "ItemForInspectionID";
            this.ItemForInspectionID.Size = new System.Drawing.Size(608, 24);
            this.ItemForInspectionID.Text = "Inspection ID:";
            this.ItemForInspectionID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(628, 481);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSiteID,
            this.ItemForInspectionDate,
            this.ItemForInspectorID,
            this.ItemForInspectionNumber,
            this.layoutControlGroup5,
            this.emptySpaceItem1,
            this.emptySpaceItem3,
            this.layoutControlItem1,
            this.emptySpaceItem4,
            this.emptySpaceItem7,
            this.layoutControlItem2,
            this.emptySpaceItem5,
            this.emptySpaceItem6,
            this.layoutControlItem3,
            this.layoutControlItem4});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(608, 461);
            // 
            // ItemForSiteID
            // 
            this.ItemForSiteID.AllowHide = false;
            this.ItemForSiteID.Control = this.SiteIDGridLookUpEdit;
            this.ItemForSiteID.CustomizationFormText = "Linked To Site:";
            this.ItemForSiteID.Location = new System.Drawing.Point(0, 24);
            this.ItemForSiteID.Name = "ItemForSiteID";
            this.ItemForSiteID.Size = new System.Drawing.Size(608, 26);
            this.ItemForSiteID.Text = "Linked To Site:";
            this.ItemForSiteID.TextSize = new System.Drawing.Size(126, 13);
            // 
            // ItemForInspectionDate
            // 
            this.ItemForInspectionDate.AllowHide = false;
            this.ItemForInspectionDate.Control = this.InspectionDateDateEdit;
            this.ItemForInspectionDate.CustomizationFormText = "Inspection Date:";
            this.ItemForInspectionDate.Location = new System.Drawing.Point(0, 50);
            this.ItemForInspectionDate.Name = "ItemForInspectionDate";
            this.ItemForInspectionDate.Size = new System.Drawing.Size(608, 24);
            this.ItemForInspectionDate.Text = "Inspection Date:";
            this.ItemForInspectionDate.TextSize = new System.Drawing.Size(126, 13);
            // 
            // ItemForInspectorID
            // 
            this.ItemForInspectorID.Control = this.InspectorIDGridLookUpEdit;
            this.ItemForInspectorID.CustomizationFormText = "Inspector:";
            this.ItemForInspectorID.Location = new System.Drawing.Point(0, 98);
            this.ItemForInspectorID.Name = "ItemForInspectorID";
            this.ItemForInspectorID.Size = new System.Drawing.Size(608, 26);
            this.ItemForInspectorID.Text = "Inspector:";
            this.ItemForInspectorID.TextSize = new System.Drawing.Size(126, 13);
            // 
            // ItemForInspectionNumber
            // 
            this.ItemForInspectionNumber.Control = this.InspectionNumberButtonEdit;
            this.ItemForInspectionNumber.CustomizationFormText = "Inspection Number:";
            this.ItemForInspectionNumber.Location = new System.Drawing.Point(0, 74);
            this.ItemForInspectionNumber.Name = "ItemForInspectionNumber";
            this.ItemForInspectionNumber.Size = new System.Drawing.Size(608, 24);
            this.ItemForInspectionNumber.Text = "Inspection Number:";
            this.ItemForInspectionNumber.TextSize = new System.Drawing.Size(126, 13);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Details";
            this.layoutControlGroup5.ExpandButtonVisible = true;
            this.layoutControlGroup5.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 170);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(608, 281);
            this.layoutControlGroup5.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup4;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(584, 235);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup4,
            this.layoutControlGroup6,
            this.layoutControlGroup7,
            this.layoutControlGroup3});
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup4.CaptionImageOptions.Image")));
            this.layoutControlGroup4.CustomizationFormText = "Time Taken";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForTotalTimeDeductions,
            this.ItemForTotalTimeDeductionsRemarks,
            this.ItemForInspectionStartTime,
            this.ItemForInspectionEndTime,
            this.ItemForTotalTimeTaken});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(560, 187);
            this.layoutControlGroup4.Text = "Time Taken";
            // 
            // ItemForTotalTimeDeductions
            // 
            this.ItemForTotalTimeDeductions.Control = this.TotalTimeDeductionsSpinEdit;
            this.ItemForTotalTimeDeductions.CustomizationFormText = "Total Time Deductions:";
            this.ItemForTotalTimeDeductions.Location = new System.Drawing.Point(0, 48);
            this.ItemForTotalTimeDeductions.Name = "ItemForTotalTimeDeductions";
            this.ItemForTotalTimeDeductions.Size = new System.Drawing.Size(560, 24);
            this.ItemForTotalTimeDeductions.Text = "Total Time Deductions:";
            this.ItemForTotalTimeDeductions.TextSize = new System.Drawing.Size(126, 13);
            // 
            // ItemForTotalTimeDeductionsRemarks
            // 
            this.ItemForTotalTimeDeductionsRemarks.Control = this.TotalTimeDeductionsRemarksMemoEdit;
            this.ItemForTotalTimeDeductionsRemarks.CustomizationFormText = "Time Deductions Remarks:";
            this.ItemForTotalTimeDeductionsRemarks.Location = new System.Drawing.Point(0, 72);
            this.ItemForTotalTimeDeductionsRemarks.Name = "ItemForTotalTimeDeductionsRemarks";
            this.ItemForTotalTimeDeductionsRemarks.Size = new System.Drawing.Size(560, 91);
            this.ItemForTotalTimeDeductionsRemarks.Text = "Time Deductions Remarks:";
            this.ItemForTotalTimeDeductionsRemarks.TextSize = new System.Drawing.Size(126, 13);
            // 
            // ItemForInspectionStartTime
            // 
            this.ItemForInspectionStartTime.Control = this.InspectionStartTimeTimeEdit;
            this.ItemForInspectionStartTime.CustomizationFormText = "Start Time:";
            this.ItemForInspectionStartTime.Location = new System.Drawing.Point(0, 0);
            this.ItemForInspectionStartTime.Name = "ItemForInspectionStartTime";
            this.ItemForInspectionStartTime.Size = new System.Drawing.Size(560, 24);
            this.ItemForInspectionStartTime.Text = "Start Time:";
            this.ItemForInspectionStartTime.TextSize = new System.Drawing.Size(126, 13);
            // 
            // ItemForInspectionEndTime
            // 
            this.ItemForInspectionEndTime.Control = this.InspectionEndTimeTimeEdit;
            this.ItemForInspectionEndTime.CustomizationFormText = "End Time:";
            this.ItemForInspectionEndTime.Location = new System.Drawing.Point(0, 24);
            this.ItemForInspectionEndTime.Name = "ItemForInspectionEndTime";
            this.ItemForInspectionEndTime.Size = new System.Drawing.Size(560, 24);
            this.ItemForInspectionEndTime.Text = "End Time:";
            this.ItemForInspectionEndTime.TextSize = new System.Drawing.Size(126, 13);
            // 
            // ItemForTotalTimeTaken
            // 
            this.ItemForTotalTimeTaken.Control = this.TotalTimeTakenSpinEdit;
            this.ItemForTotalTimeTaken.CustomizationFormText = "Total Time Taken:";
            this.ItemForTotalTimeTaken.Location = new System.Drawing.Point(0, 163);
            this.ItemForTotalTimeTaken.Name = "ItemForTotalTimeTaken";
            this.ItemForTotalTimeTaken.Size = new System.Drawing.Size(560, 24);
            this.ItemForTotalTimeTaken.Text = "Total Time Taken:";
            this.ItemForTotalTimeTaken.TextSize = new System.Drawing.Size(126, 13);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Time Deductions";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(560, 187);
            this.layoutControlGroup6.Text = "Time Deductions";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.gridControl1;
            this.layoutControlItem5.CustomizationFormText = "Time Deductions Grid";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(560, 187);
            this.layoutControlItem5.Text = "Time Deductions Grid";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "Linked Actions";
            this.layoutControlGroup7.ExpandButtonVisible = true;
            this.layoutControlGroup7.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(560, 187);
            this.layoutControlGroup7.Text = "Linked Actions";
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.gridControl2;
            this.layoutControlItem6.CustomizationFormText = "Linked Actions Grid";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(560, 187);
            this.layoutControlItem6.Text = "Linked Actions Grid";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CaptionImageOptions.Image = global::WoodPlan5.Properties.Resources.Notes_16x16;
            this.layoutControlGroup3.CustomizationFormText = "Remarks";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(560, 187);
            this.layoutControlGroup3.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(560, 187);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 160);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(608, 10);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(129, 0);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(129, 24);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(129, 24);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(129, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(177, 24);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(306, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(302, 24);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 451);
            this.emptySpaceItem7.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem7.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(608, 10);
            this.emptySpaceItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.btnSaveAndOpenMap;
            this.layoutControlItem2.CustomizationFormText = "Save Inspection && Proceed to Map button";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 134);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(206, 26);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(206, 26);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(206, 26);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "Save && Open Map Button";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(584, 134);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(0, 26);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 26);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(24, 26);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 124);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(608, 10);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnLink;
            this.layoutControlItem3.CustomizationFormText = "Link Inspection to Switchboard Button";
            this.layoutControlItem3.Location = new System.Drawing.Point(206, 134);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(178, 26);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(178, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(178, 26);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "Link Inspection to Switchboard Button";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.btnUnlink;
            this.layoutControlItem4.CustomizationFormText = "Unlink Inspection from Switchboard button";
            this.layoutControlItem4.Location = new System.Drawing.Point(384, 134);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(200, 26);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(200, 26);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(200, 26);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // sp03051_EP_Site_Inspection_EditTableAdapter
            // 
            this.sp03051_EP_Site_Inspection_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp03028_EP_Sites_With_Blank_DDLBTableAdapter
            // 
            this.sp03028_EP_Sites_With_Blank_DDLBTableAdapter.ClearBeforeFill = true;
            // 
            // sp00190_Contractor_List_With_BlankTableAdapter
            // 
            this.sp00190_Contractor_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp03057_EP_Time_Deductions_For_InspectionTableAdapter
            // 
            this.sp03057_EP_Time_Deductions_For_InspectionTableAdapter.ClearBeforeFill = true;
            // 
            // sp03058_EP_Actions_For_Passed_InspectionTableAdapter
            // 
            this.sp03058_EP_Actions_For_Passed_InspectionTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // frm_EP_Site_Inspection_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(628, 537);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_EP_Site_Inspection_Edit";
            this.Text = "Edit Site Inspection";
            this.Activated += new System.EventHandler(this.frm_EP_Site_Inspection_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_EP_Site_Inspection_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_EP_Site_Inspection_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03058EPActionsForPassedInspectionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_EP_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03057EPTimeDeductionsForInspectionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03051EPSiteInspectionEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InspectionIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InspectionDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InspectionDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InspectionStartTimeTimeEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InspectionEndTimeTimeEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalTimeDeductionsSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalTimeDeductionsRemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalTimeTakenSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InspectionNumberButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03028EPSitesWithBlankDDLBBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InspectorIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00190ContractorListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInspectionID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInspectionDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInspectorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInspectionNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalTimeDeductions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalTimeDeductionsRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInspectionStartTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInspectionEndTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalTimeTaken)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.TextEdit InspectionIDTextEdit;
        private System.Windows.Forms.BindingSource sp03051EPSiteInspectionEditBindingSource;
        private DataSet_EP_DataEntry dataSet_EP_DataEntry;
        private DevExpress.XtraEditors.DateEdit InspectionDateDateEdit;
        private DevExpress.XtraEditors.TimeEdit InspectionStartTimeTimeEdit;
        private DevExpress.XtraEditors.TimeEdit InspectionEndTimeTimeEdit;
        private DevExpress.XtraEditors.SpinEdit TotalTimeDeductionsSpinEdit;
        private DevExpress.XtraEditors.MemoEdit TotalTimeDeductionsRemarksMemoEdit;
        private DevExpress.XtraEditors.SpinEdit TotalTimeTakenSpinEdit;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraEditors.ButtonEdit InspectionNumberButtonEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInspectionID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInspectionDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInspectorID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInspectionStartTime;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInspectionEndTime;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalTimeDeductions;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalTimeDeductionsRemarks;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalTimeTaken;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInspectionNumber;
        private WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03051_EP_Site_Inspection_EditTableAdapter sp03051_EP_Site_Inspection_EditTableAdapter;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraEditors.GridLookUpEdit SiteIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraEditors.GridLookUpEdit InspectorIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private System.Windows.Forms.BindingSource sp03028EPSitesWithBlankDDLBBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colClientCode;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colContactPerson;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCode;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colXCoordinate;
        private DevExpress.XtraGrid.Columns.GridColumn colYCoordinate;
        private WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03028_EP_Sites_With_Blank_DDLBTableAdapter sp03028_EP_Sites_With_Blank_DDLBTableAdapter;
        private WoodPlanDataSet woodPlanDataSet;
        private System.Windows.Forms.BindingSource sp00190ContractorListWithBlankBindingSource;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00190_Contractor_List_With_BlankTableAdapter sp00190_Contractor_List_With_BlankTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine2;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine3;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine4;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine5;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorCode;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colDisabled;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailPassword;
        private DevExpress.XtraGrid.Columns.GridColumn colInternalContractor;
        private DevExpress.XtraGrid.Columns.GridColumn colInternalCOntractorDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colMobile;
        private DevExpress.XtraGrid.Columns.GridColumn colPostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone1;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone2;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colUserDefined1;
        private DevExpress.XtraGrid.Columns.GridColumn colUserDefined2;
        private DevExpress.XtraGrid.Columns.GridColumn colUserDefined3;
        private DevExpress.XtraGrid.Columns.GridColumn colVatReg;
        private DevExpress.XtraGrid.Columns.GridColumn colWebsite;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.SimpleButton btnSaveAndOpenMap;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem bbiOpenMap;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraEditors.SimpleButton btnUnlink;
        private DevExpress.XtraEditors.SimpleButton btnLink;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private System.Windows.Forms.BindingSource sp03057EPTimeDeductionsForInspectionBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colDeductionID;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionID;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colStartTime;
        private DevExpress.XtraGrid.Columns.GridColumn colEndTime;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionNumber;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03057_EP_Time_Deductions_For_InspectionTableAdapter sp03057_EP_Time_Deductions_For_InspectionTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private System.Windows.Forms.BindingSource sp03058EPActionsForPassedInspectionBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colActionID;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn colActionNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colActionDueDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionDoneDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedManHours;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActionRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientCode1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn colXCoordinate1;
        private DevExpress.XtraGrid.Columns.GridColumn colYCoordinate1;
        private DevExpress.XtraGrid.Columns.GridColumn colPolygonXY;
        private DevExpress.XtraGrid.Columns.GridColumn colArea;
        private DevExpress.XtraGrid.Columns.GridColumn colLength;
        private DevExpress.XtraGrid.Columns.GridColumn colWidth;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetStatusDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetPartNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetSerialNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetModelNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetLifeSpanValue;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetLifeSpanValueDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetConditionDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetLastVisitDate;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetNextVisitDate;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colMapID;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionNumber1;
        private WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03058_EP_Actions_For_Passed_InspectionTableAdapter sp03058_EP_Actions_For_Passed_InspectionTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit4;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.Utils.ImageCollection imageCollection1;
    }
}

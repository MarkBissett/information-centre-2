namespace WoodPlan5
{
    partial class frm_EP_Asset_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_EP_Asset_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition3 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions7 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject25 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject26 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject27 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject28 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions8 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject29 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject30 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject31 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject32 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition4 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions9 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject33 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject34 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject35 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject36 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions10 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject37 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject38 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject39 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject40 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition5 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions11 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject41 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject42 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject43 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject44 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions12 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject45 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject46 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject47 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject48 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition6 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions13 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject49 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject50 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject51 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject52 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions14 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject53 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject54 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject55 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject56 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            this.colItemID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetSubTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colConditionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bbiShowMapButton = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.LocationPolyXYMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.sp03026EPAssetEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_EP_DataEntry = new WoodPlan5.DataSet_EP_DataEntry();
            this.LocationYSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.LocationXSpinEdit1 = new DevExpress.XtraEditors.SpinEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp03035EPActionsForPassedAssetsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_EP = new WoodPlan5.DataSet_EP();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colActionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionDueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionDoneDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedManHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colXCoordinate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYCoordinate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPolygonXY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colArea = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLength = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWidth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetStatusDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetPartNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetSerialNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetModelNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetLifeSpanValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetLifeSpanValueDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetConditionDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetLastVisitDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetNextVisitDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMapID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.AssetIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.XCoordinateSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.YCoordinateSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.PolygonXYMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.AreaSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.LengthSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.WidthSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.PartNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SerialNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ModelNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LifeSpanValueSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.LastVisitDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.NextVisitDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.MapIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.StatusIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp01372ATMultiplePicklistsWithBlanksBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colItemDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LifeSpanValueDescriptorGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp03032EPAssetLifeExpectancyDescriptionWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.AssetTypeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp03019EPAssetTypesListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAssetTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetTypeOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.AssetSubTypeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp03030EPAssetSubTypesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAssetSubTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetSubTypeOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetTypeDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetTypeOrder1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SiteIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp03028EPSitesWithBlankDDLBBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactPerson = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colXCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CurrentConditionIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp03031EPAssetConditionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAssetTypeDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetTypeID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetTypeOrder2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colConditionDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colConditionOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.AssetNumberButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.ItemForAssetID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSiteID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAssetTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAssetSubTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStatusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForLifeSpanValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCurrentConditionID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLastVisitDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForNextVisitDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForLifeSpanValueDescriptor = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForXCoordinate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPolygonXY = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForArea = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMapID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForYCoordinate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLength = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWidth = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForPartNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSerialNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForModelNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAssetNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp03026_EP_Asset_EditTableAdapter = new WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03026_EP_Asset_EditTableAdapter();
            this.sp03028_EP_Sites_With_Blank_DDLBTableAdapter = new WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03028_EP_Sites_With_Blank_DDLBTableAdapter();
            this.sp03019_EP_Asset_Types_List_With_BlankTableAdapter = new WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03019_EP_Asset_Types_List_With_BlankTableAdapter();
            this.sp03030_EP_Asset_Sub_TypesTableAdapter = new WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03030_EP_Asset_Sub_TypesTableAdapter();
            this.sp03031_EP_Asset_ConditionsTableAdapter = new WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03031_EP_Asset_ConditionsTableAdapter();
            this.sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter();
            this.sp03032_EP_Asset_Life_Expectancy_Description_With_BlankTableAdapter = new WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03032_EP_Asset_Life_Expectancy_Description_With_BlankTableAdapter();
            this.sp03035_EP_Actions_For_Passed_AssetsTableAdapter = new WoodPlan5.DataSet_EPTableAdapters.sp03035_EP_Actions_For_Passed_AssetsTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LocationPolyXYMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03026EPAssetEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_EP_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LocationYSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LocationXSpinEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03035EPActionsForPassedAssetsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_EP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AssetIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XCoordinateSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.YCoordinateSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PolygonXYMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AreaSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LengthSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WidthSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PartNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SerialNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ModelNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LifeSpanValueSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LastVisitDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LastVisitDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NextVisitDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NextVisitDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MapIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01372ATMultiplePicklistsWithBlanksBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LifeSpanValueDescriptorGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03032EPAssetLifeExpectancyDescriptionWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AssetTypeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03019EPAssetTypesListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AssetSubTypeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03030EPAssetSubTypesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03028EPSitesWithBlankDDLBBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CurrentConditionIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03031EPAssetConditionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AssetNumberButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAssetID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAssetTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAssetSubTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLifeSpanValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCurrentConditionID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLastVisitDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNextVisitDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLifeSpanValueDescriptor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForXCoordinate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPolygonXY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMapID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForYCoordinate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLength)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPartNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSerialNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForModelNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAssetNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(913, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 507);
            this.barDockControlBottom.Size = new System.Drawing.Size(913, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 481);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(913, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 481);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colItemID
            // 
            this.colItemID.Caption = "ID";
            this.colItemID.FieldName = "ItemID";
            this.colItemID.Name = "colItemID";
            this.colItemID.OptionsColumn.AllowEdit = false;
            this.colItemID.OptionsColumn.AllowFocus = false;
            this.colItemID.OptionsColumn.ReadOnly = true;
            // 
            // colValue
            // 
            this.colValue.Caption = "Value";
            this.colValue.FieldName = "Value";
            this.colValue.Name = "colValue";
            this.colValue.OptionsColumn.AllowEdit = false;
            this.colValue.OptionsColumn.AllowFocus = false;
            this.colValue.OptionsColumn.ReadOnly = true;
            // 
            // colAssetTypeID
            // 
            this.colAssetTypeID.Caption = "Asset Type ID";
            this.colAssetTypeID.FieldName = "AssetTypeID";
            this.colAssetTypeID.Name = "colAssetTypeID";
            this.colAssetTypeID.OptionsColumn.AllowEdit = false;
            this.colAssetTypeID.OptionsColumn.AllowFocus = false;
            this.colAssetTypeID.OptionsColumn.ReadOnly = true;
            this.colAssetTypeID.Width = 88;
            // 
            // colAssetSubTypeID
            // 
            this.colAssetSubTypeID.Caption = "Asset Sub-Type ID";
            this.colAssetSubTypeID.FieldName = "AssetSubTypeID";
            this.colAssetSubTypeID.Name = "colAssetSubTypeID";
            this.colAssetSubTypeID.OptionsColumn.AllowEdit = false;
            this.colAssetSubTypeID.OptionsColumn.AllowFocus = false;
            this.colAssetSubTypeID.OptionsColumn.ReadOnly = true;
            this.colAssetSubTypeID.Width = 125;
            // 
            // colSiteID
            // 
            this.colSiteID.Caption = "Site ID";
            this.colSiteID.FieldName = "SiteID";
            this.colSiteID.Name = "colSiteID";
            this.colSiteID.OptionsColumn.AllowEdit = false;
            this.colSiteID.OptionsColumn.AllowFocus = false;
            this.colSiteID.OptionsColumn.ReadOnly = true;
            // 
            // colConditionID
            // 
            this.colConditionID.Caption = "Condition ID";
            this.colConditionID.FieldName = "ConditionID";
            this.colConditionID.Name = "colConditionID";
            this.colConditionID.OptionsColumn.AllowEdit = false;
            this.colConditionID.OptionsColumn.AllowFocus = false;
            this.colConditionID.OptionsColumn.ReadOnly = true;
            this.colConditionID.Width = 83;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3,
            this.bar2});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation,
            this.bbiShowMapButton});
            this.barManager2.MaxItemId = 16;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.ImageOptions.Image = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.ImageOptions.Image = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageOptions.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageOptions.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bar2
            // 
            this.bar2.BarName = "Custom 4";
            this.bar2.DockCol = 1;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiShowMapButton)});
            this.bar2.Text = "Mapping";
            // 
            // bbiShowMapButton
            // 
            this.bbiShowMapButton.Caption = "Show Map";
            this.bbiShowMapButton.Id = 15;
            this.bbiShowMapButton.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMapButton.ImageOptions.Image")));
            this.bbiShowMapButton.Name = "bbiShowMapButton";
            toolTipTitleItem4.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Text = "View On Map - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to view the current record on the map.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.bbiShowMapButton.SuperTip = superToolTip4;
            this.bbiShowMapButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiShowMapButton_ItemClick);
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(913, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 507);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(913, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 481);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(913, 26);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 481);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Delete_16x16");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.LocationPolyXYMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.LocationYSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.LocationXSpinEdit1);
            this.dataLayoutControl1.Controls.Add(this.gridControl1);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.AssetIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.XCoordinateSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.YCoordinateSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.PolygonXYMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.AreaSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.LengthSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.WidthSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.PartNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SerialNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ModelNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LifeSpanValueSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.LastVisitDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.NextVisitDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.MapIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.StatusIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.LifeSpanValueDescriptorGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.AssetTypeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.AssetSubTypeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.CurrentConditionIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.AssetNumberButtonEdit);
            this.dataLayoutControl1.DataSource = this.sp03026EPAssetEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForAssetID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1473, 336, 450, 350);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(913, 481);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // LocationPolyXYMemoEdit
            // 
            this.LocationPolyXYMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03026EPAssetEditBindingSource, "LocationPolyXY", true));
            this.LocationPolyXYMemoEdit.Location = new System.Drawing.Point(561, 303);
            this.LocationPolyXYMemoEdit.MenuManager = this.barManager1;
            this.LocationPolyXYMemoEdit.Name = "LocationPolyXYMemoEdit";
            this.LocationPolyXYMemoEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LocationPolyXYMemoEdit, true);
            this.LocationPolyXYMemoEdit.Size = new System.Drawing.Size(328, 72);
            this.scSpellChecker.SetSpellCheckerOptions(this.LocationPolyXYMemoEdit, optionsSpelling1);
            this.LocationPolyXYMemoEdit.StyleController = this.dataLayoutControl1;
            this.LocationPolyXYMemoEdit.TabIndex = 30;
            // 
            // sp03026EPAssetEditBindingSource
            // 
            this.sp03026EPAssetEditBindingSource.DataMember = "sp03026_EP_Asset_Edit";
            this.sp03026EPAssetEditBindingSource.DataSource = this.dataSet_EP_DataEntry;
            // 
            // dataSet_EP_DataEntry
            // 
            this.dataSet_EP_DataEntry.DataSetName = "DataSet_EP_DataEntry";
            this.dataSet_EP_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // LocationYSpinEdit
            // 
            this.LocationYSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03026EPAssetEditBindingSource, "LocationY", true));
            this.LocationYSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.LocationYSpinEdit.Location = new System.Drawing.Point(561, 279);
            this.LocationYSpinEdit.MenuManager = this.barManager1;
            this.LocationYSpinEdit.Name = "LocationYSpinEdit";
            this.LocationYSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.LocationYSpinEdit.Properties.ReadOnly = true;
            this.LocationYSpinEdit.Size = new System.Drawing.Size(328, 20);
            this.LocationYSpinEdit.StyleController = this.dataLayoutControl1;
            this.LocationYSpinEdit.TabIndex = 29;
            // 
            // LocationXSpinEdit1
            // 
            this.LocationXSpinEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03026EPAssetEditBindingSource, "LocationX", true));
            this.LocationXSpinEdit1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.LocationXSpinEdit1.Location = new System.Drawing.Point(561, 255);
            this.LocationXSpinEdit1.MenuManager = this.barManager1;
            this.LocationXSpinEdit1.Name = "LocationXSpinEdit1";
            this.LocationXSpinEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.LocationXSpinEdit1.Properties.ReadOnly = true;
            this.LocationXSpinEdit1.Size = new System.Drawing.Size(328, 20);
            this.LocationXSpinEdit1.StyleController = this.dataLayoutControl1;
            this.LocationXSpinEdit1.TabIndex = 28;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp03035EPActionsForPassedAssetsBindingSource;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(24, 231);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(865, 192);
            this.gridControl1.TabIndex = 27;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1,
            this.gridView2});
            // 
            // sp03035EPActionsForPassedAssetsBindingSource
            // 
            this.sp03035EPActionsForPassedAssetsBindingSource.DataMember = "sp03035_EP_Actions_For_Passed_Assets";
            this.sp03035EPActionsForPassedAssetsBindingSource.DataSource = this.dataSet_EP;
            // 
            // dataSet_EP
            // 
            this.dataSet_EP.DataSetName = "DataSet_EP";
            this.dataSet_EP.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colActionID,
            this.colAssetID,
            this.colInspectionID,
            this.colActionNumber,
            this.colJobTypeID,
            this.colJobTypeDescription,
            this.colActionDueDate,
            this.colActionDoneDate,
            this.colEstimatedManHours,
            this.colEstimatedCost,
            this.colActionRemarks,
            this.colSiteName1,
            this.colSiteCode1,
            this.colClientName1,
            this.colClientCode1,
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.colXCoordinate1,
            this.colYCoordinate1,
            this.colPolygonXY,
            this.colArea,
            this.colLength,
            this.colWidth,
            this.colAssetStatusDescription,
            this.colAssetPartNumber,
            this.colAssetSerialNumber,
            this.colAssetModelNumber,
            this.colAssetNumber,
            this.colAssetLifeSpanValue,
            this.colAssetLifeSpanValueDescriptor,
            this.colAssetConditionDescription,
            this.colAssetLastVisitDate,
            this.colAssetNextVisitDate,
            this.colAssetRemarks,
            this.colMapID});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 3;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAssetNumber, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colActionDueDate, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colActionID
            // 
            this.colActionID.Caption = "Action ID";
            this.colActionID.FieldName = "ActionID";
            this.colActionID.Name = "colActionID";
            this.colActionID.OptionsColumn.AllowEdit = false;
            this.colActionID.OptionsColumn.AllowFocus = false;
            this.colActionID.OptionsColumn.ReadOnly = true;
            // 
            // colAssetID
            // 
            this.colAssetID.Caption = "Asset ID";
            this.colAssetID.FieldName = "AssetID";
            this.colAssetID.Name = "colAssetID";
            this.colAssetID.OptionsColumn.AllowEdit = false;
            this.colAssetID.OptionsColumn.AllowFocus = false;
            this.colAssetID.OptionsColumn.ReadOnly = true;
            // 
            // colInspectionID
            // 
            this.colInspectionID.Caption = "Inspection ID";
            this.colInspectionID.FieldName = "InspectionID";
            this.colInspectionID.Name = "colInspectionID";
            this.colInspectionID.OptionsColumn.AllowEdit = false;
            this.colInspectionID.OptionsColumn.AllowFocus = false;
            this.colInspectionID.OptionsColumn.ReadOnly = true;
            this.colInspectionID.Width = 96;
            // 
            // colActionNumber
            // 
            this.colActionNumber.Caption = "Action Number";
            this.colActionNumber.FieldName = "ActionNumber";
            this.colActionNumber.Name = "colActionNumber";
            this.colActionNumber.OptionsColumn.AllowEdit = false;
            this.colActionNumber.OptionsColumn.AllowFocus = false;
            this.colActionNumber.OptionsColumn.ReadOnly = true;
            this.colActionNumber.Visible = true;
            this.colActionNumber.VisibleIndex = 0;
            this.colActionNumber.Width = 129;
            // 
            // colJobTypeID
            // 
            this.colJobTypeID.Caption = "Job Type ID";
            this.colJobTypeID.FieldName = "JobTypeID";
            this.colJobTypeID.Name = "colJobTypeID";
            this.colJobTypeID.OptionsColumn.AllowEdit = false;
            this.colJobTypeID.OptionsColumn.AllowFocus = false;
            this.colJobTypeID.OptionsColumn.ReadOnly = true;
            // 
            // colJobTypeDescription
            // 
            this.colJobTypeDescription.Caption = "Job Type";
            this.colJobTypeDescription.FieldName = "JobTypeDescription";
            this.colJobTypeDescription.Name = "colJobTypeDescription";
            this.colJobTypeDescription.OptionsColumn.AllowEdit = false;
            this.colJobTypeDescription.OptionsColumn.AllowFocus = false;
            this.colJobTypeDescription.OptionsColumn.ReadOnly = true;
            this.colJobTypeDescription.Visible = true;
            this.colJobTypeDescription.VisibleIndex = 1;
            this.colJobTypeDescription.Width = 174;
            // 
            // colActionDueDate
            // 
            this.colActionDueDate.Caption = "Due Date";
            this.colActionDueDate.FieldName = "ActionDueDate";
            this.colActionDueDate.Name = "colActionDueDate";
            this.colActionDueDate.OptionsColumn.AllowEdit = false;
            this.colActionDueDate.OptionsColumn.AllowFocus = false;
            this.colActionDueDate.OptionsColumn.ReadOnly = true;
            this.colActionDueDate.Visible = true;
            this.colActionDueDate.VisibleIndex = 2;
            this.colActionDueDate.Width = 90;
            // 
            // colActionDoneDate
            // 
            this.colActionDoneDate.Caption = "Done Date";
            this.colActionDoneDate.FieldName = "ActionDoneDate";
            this.colActionDoneDate.Name = "colActionDoneDate";
            this.colActionDoneDate.OptionsColumn.AllowEdit = false;
            this.colActionDoneDate.OptionsColumn.AllowFocus = false;
            this.colActionDoneDate.OptionsColumn.ReadOnly = true;
            this.colActionDoneDate.Visible = true;
            this.colActionDoneDate.VisibleIndex = 3;
            this.colActionDoneDate.Width = 97;
            // 
            // colEstimatedManHours
            // 
            this.colEstimatedManHours.Caption = "Estaimated Man Hours";
            this.colEstimatedManHours.FieldName = "EstimatedManHours";
            this.colEstimatedManHours.Name = "colEstimatedManHours";
            this.colEstimatedManHours.OptionsColumn.AllowEdit = false;
            this.colEstimatedManHours.OptionsColumn.AllowFocus = false;
            this.colEstimatedManHours.OptionsColumn.ReadOnly = true;
            this.colEstimatedManHours.Visible = true;
            this.colEstimatedManHours.VisibleIndex = 4;
            this.colEstimatedManHours.Width = 130;
            // 
            // colEstimatedCost
            // 
            this.colEstimatedCost.Caption = "Estimated Cost";
            this.colEstimatedCost.FieldName = "EstimatedCost";
            this.colEstimatedCost.Name = "colEstimatedCost";
            this.colEstimatedCost.OptionsColumn.AllowEdit = false;
            this.colEstimatedCost.OptionsColumn.AllowFocus = false;
            this.colEstimatedCost.OptionsColumn.ReadOnly = true;
            this.colEstimatedCost.Visible = true;
            this.colEstimatedCost.VisibleIndex = 5;
            this.colEstimatedCost.Width = 97;
            // 
            // colActionRemarks
            // 
            this.colActionRemarks.Caption = "Action Remarks";
            this.colActionRemarks.FieldName = "ActionRemarks";
            this.colActionRemarks.Name = "colActionRemarks";
            this.colActionRemarks.OptionsColumn.ReadOnly = true;
            this.colActionRemarks.Visible = true;
            this.colActionRemarks.VisibleIndex = 6;
            this.colActionRemarks.Width = 94;
            // 
            // colSiteName1
            // 
            this.colSiteName1.Caption = "Site Name";
            this.colSiteName1.FieldName = "SiteName";
            this.colSiteName1.Name = "colSiteName1";
            this.colSiteName1.OptionsColumn.AllowEdit = false;
            this.colSiteName1.OptionsColumn.AllowFocus = false;
            this.colSiteName1.OptionsColumn.ReadOnly = true;
            this.colSiteName1.Visible = true;
            this.colSiteName1.VisibleIndex = 7;
            // 
            // colSiteCode1
            // 
            this.colSiteCode1.Caption = "SiteCode";
            this.colSiteCode1.FieldName = "SiteCode";
            this.colSiteCode1.Name = "colSiteCode1";
            this.colSiteCode1.OptionsColumn.AllowEdit = false;
            this.colSiteCode1.OptionsColumn.AllowFocus = false;
            this.colSiteCode1.OptionsColumn.ReadOnly = true;
            // 
            // colClientName1
            // 
            this.colClientName1.Caption = "Client Name";
            this.colClientName1.FieldName = "ClientName";
            this.colClientName1.Name = "colClientName1";
            this.colClientName1.OptionsColumn.AllowEdit = false;
            this.colClientName1.OptionsColumn.AllowFocus = false;
            this.colClientName1.OptionsColumn.ReadOnly = true;
            this.colClientName1.Visible = true;
            this.colClientName1.VisibleIndex = 8;
            // 
            // colClientCode1
            // 
            this.colClientCode1.Caption = "Client Code";
            this.colClientCode1.FieldName = "ClientCode";
            this.colClientCode1.Name = "colClientCode1";
            this.colClientCode1.OptionsColumn.AllowEdit = false;
            this.colClientCode1.OptionsColumn.AllowFocus = false;
            this.colClientCode1.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Asset Type";
            this.gridColumn1.FieldName = "AssetTypeDescription";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 7;
            this.gridColumn1.Width = 121;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Asset Type Order";
            this.gridColumn2.FieldName = "AssetTypeOrder";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Width = 106;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Asset Sub-Type";
            this.gridColumn3.FieldName = "AssetSubTypeDescription";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 8;
            this.gridColumn3.Width = 112;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Asset Sub-Type Order";
            this.gridColumn4.FieldName = "AssetSubTypeOrder";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Width = 136;
            // 
            // colXCoordinate1
            // 
            this.colXCoordinate1.Caption = "X Coordinate";
            this.colXCoordinate1.FieldName = "XCoordinate";
            this.colXCoordinate1.Name = "colXCoordinate1";
            this.colXCoordinate1.OptionsColumn.AllowEdit = false;
            this.colXCoordinate1.OptionsColumn.AllowFocus = false;
            this.colXCoordinate1.OptionsColumn.ReadOnly = true;
            // 
            // colYCoordinate1
            // 
            this.colYCoordinate1.Caption = "Y Coordinate";
            this.colYCoordinate1.FieldName = "YCoordinate";
            this.colYCoordinate1.Name = "colYCoordinate1";
            this.colYCoordinate1.OptionsColumn.AllowEdit = false;
            this.colYCoordinate1.OptionsColumn.AllowFocus = false;
            this.colYCoordinate1.OptionsColumn.ReadOnly = true;
            // 
            // colPolygonXY
            // 
            this.colPolygonXY.Caption = "Polygon XY Coordinates ";
            this.colPolygonXY.FieldName = "PolygonXY";
            this.colPolygonXY.Name = "colPolygonXY";
            this.colPolygonXY.OptionsColumn.ReadOnly = true;
            this.colPolygonXY.Width = 152;
            // 
            // colArea
            // 
            this.colArea.Caption = "Area";
            this.colArea.FieldName = "Area";
            this.colArea.Name = "colArea";
            this.colArea.OptionsColumn.AllowEdit = false;
            this.colArea.OptionsColumn.AllowFocus = false;
            this.colArea.OptionsColumn.ReadOnly = true;
            // 
            // colLength
            // 
            this.colLength.Caption = "Length (M)";
            this.colLength.FieldName = "Length";
            this.colLength.Name = "colLength";
            this.colLength.OptionsColumn.AllowEdit = false;
            this.colLength.OptionsColumn.AllowFocus = false;
            this.colLength.OptionsColumn.ReadOnly = true;
            // 
            // colWidth
            // 
            this.colWidth.Caption = "Width (M)";
            this.colWidth.FieldName = "Width";
            this.colWidth.Name = "colWidth";
            this.colWidth.OptionsColumn.AllowEdit = false;
            this.colWidth.OptionsColumn.AllowFocus = false;
            this.colWidth.OptionsColumn.ReadOnly = true;
            // 
            // colAssetStatusDescription
            // 
            this.colAssetStatusDescription.Caption = "Asset Status";
            this.colAssetStatusDescription.FieldName = "AssetStatusDescription";
            this.colAssetStatusDescription.Name = "colAssetStatusDescription";
            this.colAssetStatusDescription.OptionsColumn.AllowEdit = false;
            this.colAssetStatusDescription.OptionsColumn.AllowFocus = false;
            this.colAssetStatusDescription.OptionsColumn.ReadOnly = true;
            this.colAssetStatusDescription.Visible = true;
            this.colAssetStatusDescription.VisibleIndex = 9;
            this.colAssetStatusDescription.Width = 120;
            // 
            // colAssetPartNumber
            // 
            this.colAssetPartNumber.Caption = "Asset Part Number";
            this.colAssetPartNumber.FieldName = "AssetPartNumber";
            this.colAssetPartNumber.Name = "colAssetPartNumber";
            this.colAssetPartNumber.OptionsColumn.AllowEdit = false;
            this.colAssetPartNumber.OptionsColumn.AllowFocus = false;
            this.colAssetPartNumber.OptionsColumn.ReadOnly = true;
            this.colAssetPartNumber.Visible = true;
            this.colAssetPartNumber.VisibleIndex = 10;
            this.colAssetPartNumber.Width = 116;
            // 
            // colAssetSerialNumber
            // 
            this.colAssetSerialNumber.Caption = "Asset Serial Number";
            this.colAssetSerialNumber.FieldName = "AssetSerialNumber";
            this.colAssetSerialNumber.Name = "colAssetSerialNumber";
            this.colAssetSerialNumber.OptionsColumn.AllowEdit = false;
            this.colAssetSerialNumber.OptionsColumn.AllowFocus = false;
            this.colAssetSerialNumber.OptionsColumn.ReadOnly = true;
            this.colAssetSerialNumber.Visible = true;
            this.colAssetSerialNumber.VisibleIndex = 11;
            this.colAssetSerialNumber.Width = 126;
            // 
            // colAssetModelNumber
            // 
            this.colAssetModelNumber.Caption = "Asset Model Number";
            this.colAssetModelNumber.FieldName = "AssetModelNumber";
            this.colAssetModelNumber.Name = "colAssetModelNumber";
            this.colAssetModelNumber.OptionsColumn.AllowEdit = false;
            this.colAssetModelNumber.OptionsColumn.AllowFocus = false;
            this.colAssetModelNumber.OptionsColumn.ReadOnly = true;
            this.colAssetModelNumber.Visible = true;
            this.colAssetModelNumber.VisibleIndex = 12;
            this.colAssetModelNumber.Width = 128;
            // 
            // colAssetNumber
            // 
            this.colAssetNumber.Caption = "Asset Number";
            this.colAssetNumber.FieldName = "AssetNumber";
            this.colAssetNumber.Name = "colAssetNumber";
            this.colAssetNumber.OptionsColumn.AllowEdit = false;
            this.colAssetNumber.OptionsColumn.AllowFocus = false;
            this.colAssetNumber.OptionsColumn.ReadOnly = true;
            this.colAssetNumber.Visible = true;
            this.colAssetNumber.VisibleIndex = 15;
            this.colAssetNumber.Width = 122;
            // 
            // colAssetLifeSpanValue
            // 
            this.colAssetLifeSpanValue.Caption = "Asset Life Span";
            this.colAssetLifeSpanValue.FieldName = "AssetLifeSpanValue";
            this.colAssetLifeSpanValue.Name = "colAssetLifeSpanValue";
            this.colAssetLifeSpanValue.OptionsColumn.AllowEdit = false;
            this.colAssetLifeSpanValue.OptionsColumn.AllowFocus = false;
            this.colAssetLifeSpanValue.OptionsColumn.ReadOnly = true;
            this.colAssetLifeSpanValue.Width = 104;
            // 
            // colAssetLifeSpanValueDescriptor
            // 
            this.colAssetLifeSpanValueDescriptor.Caption = "Asset Life Span Descriptor";
            this.colAssetLifeSpanValueDescriptor.FieldName = "AssetLifeSpanValueDescriptor";
            this.colAssetLifeSpanValueDescriptor.Name = "colAssetLifeSpanValueDescriptor";
            this.colAssetLifeSpanValueDescriptor.OptionsColumn.AllowEdit = false;
            this.colAssetLifeSpanValueDescriptor.OptionsColumn.AllowFocus = false;
            this.colAssetLifeSpanValueDescriptor.OptionsColumn.ReadOnly = true;
            this.colAssetLifeSpanValueDescriptor.Width = 161;
            // 
            // colAssetConditionDescription
            // 
            this.colAssetConditionDescription.Caption = "Asset Condition";
            this.colAssetConditionDescription.FieldName = "AssetConditionDescription";
            this.colAssetConditionDescription.Name = "colAssetConditionDescription";
            this.colAssetConditionDescription.OptionsColumn.AllowEdit = false;
            this.colAssetConditionDescription.OptionsColumn.AllowFocus = false;
            this.colAssetConditionDescription.OptionsColumn.ReadOnly = true;
            this.colAssetConditionDescription.Width = 117;
            // 
            // colAssetLastVisitDate
            // 
            this.colAssetLastVisitDate.Caption = "Asset Last Visit";
            this.colAssetLastVisitDate.FieldName = "AssetLastVisitDate";
            this.colAssetLastVisitDate.Name = "colAssetLastVisitDate";
            this.colAssetLastVisitDate.OptionsColumn.AllowEdit = false;
            this.colAssetLastVisitDate.OptionsColumn.AllowFocus = false;
            this.colAssetLastVisitDate.OptionsColumn.ReadOnly = true;
            this.colAssetLastVisitDate.Width = 112;
            // 
            // colAssetNextVisitDate
            // 
            this.colAssetNextVisitDate.Caption = "Asset Next Visit";
            this.colAssetNextVisitDate.FieldName = "AssetNextVisitDate";
            this.colAssetNextVisitDate.Name = "colAssetNextVisitDate";
            this.colAssetNextVisitDate.OptionsColumn.AllowEdit = false;
            this.colAssetNextVisitDate.OptionsColumn.AllowFocus = false;
            this.colAssetNextVisitDate.OptionsColumn.ReadOnly = true;
            this.colAssetNextVisitDate.Width = 110;
            // 
            // colAssetRemarks
            // 
            this.colAssetRemarks.Caption = "Asset Remarks";
            this.colAssetRemarks.FieldName = "AssetRemarks";
            this.colAssetRemarks.Name = "colAssetRemarks";
            this.colAssetRemarks.OptionsColumn.ReadOnly = true;
            this.colAssetRemarks.Width = 110;
            // 
            // colMapID
            // 
            this.colMapID.Caption = "Map ID";
            this.colMapID.FieldName = "MapID";
            this.colMapID.Name = "colMapID";
            this.colMapID.OptionsColumn.AllowEdit = false;
            this.colMapID.OptionsColumn.AllowFocus = false;
            this.colMapID.OptionsColumn.ReadOnly = true;
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gridControl1;
            this.gridView2.Name = "gridView2";
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp03026EPAssetEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(12, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(196, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 26;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // AssetIDTextEdit
            // 
            this.AssetIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03026EPAssetEditBindingSource, "AssetID", true));
            this.AssetIDTextEdit.Location = new System.Drawing.Point(120, 12);
            this.AssetIDTextEdit.MenuManager = this.barManager1;
            this.AssetIDTextEdit.Name = "AssetIDTextEdit";
            this.AssetIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AssetIDTextEdit, true);
            this.AssetIDTextEdit.Size = new System.Drawing.Size(764, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AssetIDTextEdit, optionsSpelling2);
            this.AssetIDTextEdit.StyleController = this.dataLayoutControl1;
            this.AssetIDTextEdit.TabIndex = 4;
            // 
            // XCoordinateSpinEdit
            // 
            this.XCoordinateSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03026EPAssetEditBindingSource, "XCoordinate", true));
            this.XCoordinateSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.XCoordinateSpinEdit.Location = new System.Drawing.Point(127, 255);
            this.XCoordinateSpinEdit.MenuManager = this.barManager1;
            this.XCoordinateSpinEdit.Name = "XCoordinateSpinEdit";
            this.XCoordinateSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.XCoordinateSpinEdit.Properties.ReadOnly = true;
            this.XCoordinateSpinEdit.Size = new System.Drawing.Size(327, 20);
            this.XCoordinateSpinEdit.StyleController = this.dataLayoutControl1;
            this.XCoordinateSpinEdit.TabIndex = 8;
            // 
            // YCoordinateSpinEdit
            // 
            this.YCoordinateSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03026EPAssetEditBindingSource, "YCoordinate", true));
            this.YCoordinateSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.YCoordinateSpinEdit.Location = new System.Drawing.Point(127, 279);
            this.YCoordinateSpinEdit.MenuManager = this.barManager1;
            this.YCoordinateSpinEdit.Name = "YCoordinateSpinEdit";
            this.YCoordinateSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.YCoordinateSpinEdit.Properties.ReadOnly = true;
            this.YCoordinateSpinEdit.Size = new System.Drawing.Size(327, 20);
            this.YCoordinateSpinEdit.StyleController = this.dataLayoutControl1;
            this.YCoordinateSpinEdit.TabIndex = 9;
            // 
            // PolygonXYMemoEdit
            // 
            this.PolygonXYMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03026EPAssetEditBindingSource, "PolygonXY", true));
            this.PolygonXYMemoEdit.Location = new System.Drawing.Point(127, 303);
            this.PolygonXYMemoEdit.MenuManager = this.barManager1;
            this.PolygonXYMemoEdit.Name = "PolygonXYMemoEdit";
            this.PolygonXYMemoEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PolygonXYMemoEdit, true);
            this.PolygonXYMemoEdit.Size = new System.Drawing.Size(327, 72);
            this.scSpellChecker.SetSpellCheckerOptions(this.PolygonXYMemoEdit, optionsSpelling3);
            this.PolygonXYMemoEdit.StyleController = this.dataLayoutControl1;
            this.PolygonXYMemoEdit.TabIndex = 10;
            // 
            // AreaSpinEdit
            // 
            this.AreaSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03026EPAssetEditBindingSource, "Area", true));
            this.AreaSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.AreaSpinEdit.Location = new System.Drawing.Point(127, 403);
            this.AreaSpinEdit.MenuManager = this.barManager1;
            this.AreaSpinEdit.Name = "AreaSpinEdit";
            this.AreaSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.AreaSpinEdit.Properties.Mask.EditMask = "f2";
            this.AreaSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.AreaSpinEdit.Properties.ReadOnly = true;
            this.AreaSpinEdit.Size = new System.Drawing.Size(327, 20);
            this.AreaSpinEdit.StyleController = this.dataLayoutControl1;
            this.AreaSpinEdit.TabIndex = 11;
            // 
            // LengthSpinEdit
            // 
            this.LengthSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03026EPAssetEditBindingSource, "Length", true));
            this.LengthSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.LengthSpinEdit.Location = new System.Drawing.Point(127, 379);
            this.LengthSpinEdit.MenuManager = this.barManager1;
            this.LengthSpinEdit.Name = "LengthSpinEdit";
            this.LengthSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.LengthSpinEdit.Properties.Mask.EditMask = "f2";
            this.LengthSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.LengthSpinEdit.Properties.ReadOnly = true;
            this.LengthSpinEdit.Size = new System.Drawing.Size(327, 20);
            this.LengthSpinEdit.StyleController = this.dataLayoutControl1;
            this.LengthSpinEdit.TabIndex = 12;
            // 
            // WidthSpinEdit
            // 
            this.WidthSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03026EPAssetEditBindingSource, "Width", true));
            this.WidthSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.WidthSpinEdit.Location = new System.Drawing.Point(561, 379);
            this.WidthSpinEdit.MenuManager = this.barManager1;
            this.WidthSpinEdit.Name = "WidthSpinEdit";
            this.WidthSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.WidthSpinEdit.Properties.Mask.EditMask = "f2";
            this.WidthSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.WidthSpinEdit.Properties.ReadOnly = true;
            this.WidthSpinEdit.Size = new System.Drawing.Size(328, 20);
            this.WidthSpinEdit.StyleController = this.dataLayoutControl1;
            this.WidthSpinEdit.TabIndex = 13;
            // 
            // PartNumberTextEdit
            // 
            this.PartNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03026EPAssetEditBindingSource, "PartNumber", true));
            this.PartNumberTextEdit.Location = new System.Drawing.Point(609, 69);
            this.PartNumberTextEdit.MenuManager = this.barManager1;
            this.PartNumberTextEdit.Name = "PartNumberTextEdit";
            this.PartNumberTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PartNumberTextEdit, true);
            this.PartNumberTextEdit.Size = new System.Drawing.Size(280, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PartNumberTextEdit, optionsSpelling4);
            this.PartNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.PartNumberTextEdit.TabIndex = 15;
            // 
            // SerialNumberTextEdit
            // 
            this.SerialNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03026EPAssetEditBindingSource, "SerialNumber", true));
            this.SerialNumberTextEdit.Location = new System.Drawing.Point(609, 93);
            this.SerialNumberTextEdit.MenuManager = this.barManager1;
            this.SerialNumberTextEdit.Name = "SerialNumberTextEdit";
            this.SerialNumberTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SerialNumberTextEdit, true);
            this.SerialNumberTextEdit.Size = new System.Drawing.Size(280, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SerialNumberTextEdit, optionsSpelling5);
            this.SerialNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.SerialNumberTextEdit.TabIndex = 16;
            // 
            // ModelNumberTextEdit
            // 
            this.ModelNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03026EPAssetEditBindingSource, "ModelNumber", true));
            this.ModelNumberTextEdit.Location = new System.Drawing.Point(609, 117);
            this.ModelNumberTextEdit.MenuManager = this.barManager1;
            this.ModelNumberTextEdit.Name = "ModelNumberTextEdit";
            this.ModelNumberTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ModelNumberTextEdit, true);
            this.ModelNumberTextEdit.Size = new System.Drawing.Size(280, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ModelNumberTextEdit, optionsSpelling6);
            this.ModelNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.ModelNumberTextEdit.TabIndex = 17;
            // 
            // LifeSpanValueSpinEdit
            // 
            this.LifeSpanValueSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03026EPAssetEditBindingSource, "LifeSpanValue", true));
            this.LifeSpanValueSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.LifeSpanValueSpinEdit.Location = new System.Drawing.Point(127, 231);
            this.LifeSpanValueSpinEdit.MenuManager = this.barManager1;
            this.LifeSpanValueSpinEdit.Name = "LifeSpanValueSpinEdit";
            this.LifeSpanValueSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.LifeSpanValueSpinEdit.Properties.Mask.EditMask = "f";
            this.LifeSpanValueSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.LifeSpanValueSpinEdit.Size = new System.Drawing.Size(146, 20);
            this.LifeSpanValueSpinEdit.StyleController = this.dataLayoutControl1;
            this.LifeSpanValueSpinEdit.TabIndex = 19;
            // 
            // LastVisitDateDateEdit
            // 
            this.LastVisitDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03026EPAssetEditBindingSource, "LastVisitDate", true));
            this.LastVisitDateDateEdit.EditValue = null;
            this.LastVisitDateDateEdit.Location = new System.Drawing.Point(127, 281);
            this.LastVisitDateDateEdit.MenuManager = this.barManager1;
            this.LastVisitDateDateEdit.Name = "LastVisitDateDateEdit";
            this.LastVisitDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear Date", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Clear Date", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.LastVisitDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.LastVisitDateDateEdit.Size = new System.Drawing.Size(146, 20);
            this.LastVisitDateDateEdit.StyleController = this.dataLayoutControl1;
            this.LastVisitDateDateEdit.TabIndex = 22;
            this.LastVisitDateDateEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.LastVisitDateDateEdit_ButtonClick);
            // 
            // NextVisitDateDateEdit
            // 
            this.NextVisitDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03026EPAssetEditBindingSource, "NextVisitDate", true));
            this.NextVisitDateDateEdit.EditValue = null;
            this.NextVisitDateDateEdit.Location = new System.Drawing.Point(127, 305);
            this.NextVisitDateDateEdit.MenuManager = this.barManager1;
            this.NextVisitDateDateEdit.Name = "NextVisitDateDateEdit";
            this.NextVisitDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear Date", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Clear Date", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.NextVisitDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.NextVisitDateDateEdit.Size = new System.Drawing.Size(146, 20);
            this.NextVisitDateDateEdit.StyleController = this.dataLayoutControl1;
            this.NextVisitDateDateEdit.TabIndex = 23;
            this.NextVisitDateDateEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.NextVisitDateDateEdit_ButtonClick);
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03026EPAssetEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(24, 231);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(865, 192);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling7);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 24;
            // 
            // MapIDTextEdit
            // 
            this.MapIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03026EPAssetEditBindingSource, "MapID", true));
            this.MapIDTextEdit.Location = new System.Drawing.Point(127, 231);
            this.MapIDTextEdit.MenuManager = this.barManager1;
            this.MapIDTextEdit.Name = "MapIDTextEdit";
            this.MapIDTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.MapIDTextEdit, true);
            this.MapIDTextEdit.Size = new System.Drawing.Size(762, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.MapIDTextEdit, optionsSpelling8);
            this.MapIDTextEdit.StyleController = this.dataLayoutControl1;
            this.MapIDTextEdit.TabIndex = 25;
            // 
            // StatusIDGridLookUpEdit
            // 
            this.StatusIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03026EPAssetEditBindingSource, "StatusID", true));
            this.StatusIDGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.StatusIDGridLookUpEdit.Location = new System.Drawing.Point(127, 147);
            this.StatusIDGridLookUpEdit.MenuManager = this.barManager1;
            this.StatusIDGridLookUpEdit.Name = "StatusIDGridLookUpEdit";
            editorButtonImageOptions3.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions4.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.StatusIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Edit Underlying Data", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Reload Underlying Data", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.StatusIDGridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.StatusIDGridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.StatusIDGridLookUpEdit.Properties.NullText = "";
            this.StatusIDGridLookUpEdit.Properties.PopupView = this.gridLookUpEdit1View;
            this.StatusIDGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.StatusIDGridLookUpEdit.Properties.ValueMember = "ItemID";
            this.StatusIDGridLookUpEdit.Size = new System.Drawing.Size(345, 22);
            this.StatusIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.StatusIDGridLookUpEdit.TabIndex = 14;
            this.StatusIDGridLookUpEdit.Tag = "160";
            // 
            // sp01372ATMultiplePicklistsWithBlanksBindingSource
            // 
            this.sp01372ATMultiplePicklistsWithBlanksBindingSource.DataMember = "sp01372_AT_Multiple_Picklists_With_Blanks";
            this.sp01372ATMultiplePicklistsWithBlanksBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colItemDescription,
            this.colItemID,
            this.colOrder});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colItemID;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colItemDescription
            // 
            this.colItemDescription.Caption = "Description";
            this.colItemDescription.FieldName = "ItemDescription";
            this.colItemDescription.Name = "colItemDescription";
            this.colItemDescription.OptionsColumn.AllowEdit = false;
            this.colItemDescription.OptionsColumn.AllowFocus = false;
            this.colItemDescription.OptionsColumn.ReadOnly = true;
            this.colItemDescription.Visible = true;
            this.colItemDescription.VisibleIndex = 0;
            this.colItemDescription.Width = 202;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "Order";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            // 
            // LifeSpanValueDescriptorGridLookUpEdit
            // 
            this.LifeSpanValueDescriptorGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03026EPAssetEditBindingSource, "LifeSpanValueDescriptor", true));
            this.LifeSpanValueDescriptorGridLookUpEdit.Location = new System.Drawing.Point(380, 231);
            this.LifeSpanValueDescriptorGridLookUpEdit.MenuManager = this.barManager1;
            this.LifeSpanValueDescriptorGridLookUpEdit.Name = "LifeSpanValueDescriptorGridLookUpEdit";
            this.LifeSpanValueDescriptorGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LifeSpanValueDescriptorGridLookUpEdit.Properties.DataSource = this.sp03032EPAssetLifeExpectancyDescriptionWithBlankBindingSource;
            this.LifeSpanValueDescriptorGridLookUpEdit.Properties.DisplayMember = "Description";
            this.LifeSpanValueDescriptorGridLookUpEdit.Properties.NullText = "";
            this.LifeSpanValueDescriptorGridLookUpEdit.Properties.PopupView = this.gridView3;
            this.LifeSpanValueDescriptorGridLookUpEdit.Properties.ValueMember = "Value";
            this.LifeSpanValueDescriptorGridLookUpEdit.Size = new System.Drawing.Size(509, 20);
            this.LifeSpanValueDescriptorGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.LifeSpanValueDescriptorGridLookUpEdit.TabIndex = 20;
            // 
            // sp03032EPAssetLifeExpectancyDescriptionWithBlankBindingSource
            // 
            this.sp03032EPAssetLifeExpectancyDescriptionWithBlankBindingSource.DataMember = "sp03032_EP_Asset_Life_Expectancy_Description_With_Blank";
            this.sp03032EPAssetLifeExpectancyDescriptionWithBlankBindingSource.DataSource = this.dataSet_EP_DataEntry;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription,
            this.colOrder1,
            this.colValue});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.colValue;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = "";
            this.gridView3.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder1, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Life Expectancy Unit Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 195;
            // 
            // colOrder1
            // 
            this.colOrder1.Caption = "Order";
            this.colOrder1.FieldName = "Order";
            this.colOrder1.Name = "colOrder1";
            this.colOrder1.OptionsColumn.AllowEdit = false;
            this.colOrder1.OptionsColumn.AllowFocus = false;
            this.colOrder1.OptionsColumn.ReadOnly = true;
            // 
            // AssetTypeIDGridLookUpEdit
            // 
            this.AssetTypeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03026EPAssetEditBindingSource, "AssetTypeID", true));
            this.AssetTypeIDGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.AssetTypeIDGridLookUpEdit.Location = new System.Drawing.Point(127, 95);
            this.AssetTypeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.AssetTypeIDGridLookUpEdit.Name = "AssetTypeIDGridLookUpEdit";
            editorButtonImageOptions5.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions6.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.AssetTypeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "Edit Underlying Data", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "Reload Underlying Data", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.AssetTypeIDGridLookUpEdit.Properties.DataSource = this.sp03019EPAssetTypesListWithBlankBindingSource;
            this.AssetTypeIDGridLookUpEdit.Properties.DisplayMember = "AssetTypeDescription";
            this.AssetTypeIDGridLookUpEdit.Properties.NullText = "";
            this.AssetTypeIDGridLookUpEdit.Properties.PopupView = this.gridView4;
            this.AssetTypeIDGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1});
            this.AssetTypeIDGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.AssetTypeIDGridLookUpEdit.Properties.ValueMember = "AssetTypeID";
            this.AssetTypeIDGridLookUpEdit.Size = new System.Drawing.Size(345, 22);
            this.AssetTypeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.AssetTypeIDGridLookUpEdit.TabIndex = 6;
            this.AssetTypeIDGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.AssetTypeIDGridLookUpEdit_ButtonClick);
            this.AssetTypeIDGridLookUpEdit.EditValueChanged += new System.EventHandler(this.AssetTypeIDGridLookUpEdit_EditValueChanged);
            this.AssetTypeIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.AssetTypeIDGridLookUpEdit_Validating);
            this.AssetTypeIDGridLookUpEdit.Validated += new System.EventHandler(this.AssetTypeIDGridLookUpEdit_Validated);
            // 
            // sp03019EPAssetTypesListWithBlankBindingSource
            // 
            this.sp03019EPAssetTypesListWithBlankBindingSource.DataMember = "sp03019_EP_Asset_Types_List_With_Blank";
            this.sp03019EPAssetTypesListWithBlankBindingSource.DataSource = this.dataSet_EP_DataEntry;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAssetTypeDescription,
            this.colAssetTypeID,
            this.colAssetTypeOrder,
            this.colRemarks});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition3.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition3.Appearance.Options.UseForeColor = true;
            styleFormatCondition3.ApplyToRow = true;
            styleFormatCondition3.Column = this.colAssetTypeID;
            styleFormatCondition3.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition3.Value1 = "0";
            this.gridView4.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition3});
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAssetTypeOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colAssetTypeDescription
            // 
            this.colAssetTypeDescription.Caption = "Asset Type Description";
            this.colAssetTypeDescription.FieldName = "AssetTypeDescription";
            this.colAssetTypeDescription.Name = "colAssetTypeDescription";
            this.colAssetTypeDescription.OptionsColumn.AllowEdit = false;
            this.colAssetTypeDescription.OptionsColumn.AllowFocus = false;
            this.colAssetTypeDescription.OptionsColumn.ReadOnly = true;
            this.colAssetTypeDescription.Visible = true;
            this.colAssetTypeDescription.VisibleIndex = 0;
            this.colAssetTypeDescription.Width = 291;
            // 
            // colAssetTypeOrder
            // 
            this.colAssetTypeOrder.Caption = "Order";
            this.colAssetTypeOrder.FieldName = "AssetTypeOrder";
            this.colAssetTypeOrder.Name = "colAssetTypeOrder";
            this.colAssetTypeOrder.OptionsColumn.AllowEdit = false;
            this.colAssetTypeOrder.OptionsColumn.AllowFocus = false;
            this.colAssetTypeOrder.OptionsColumn.ReadOnly = true;
            this.colAssetTypeOrder.Visible = true;
            this.colAssetTypeOrder.VisibleIndex = 1;
            this.colAssetTypeOrder.Width = 64;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.AllowFocus = false;
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 2;
            // 
            // AssetSubTypeIDGridLookUpEdit
            // 
            this.AssetSubTypeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03026EPAssetEditBindingSource, "AssetSubTypeID", true));
            this.AssetSubTypeIDGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.AssetSubTypeIDGridLookUpEdit.Location = new System.Drawing.Point(127, 121);
            this.AssetSubTypeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.AssetSubTypeIDGridLookUpEdit.Name = "AssetSubTypeIDGridLookUpEdit";
            editorButtonImageOptions7.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions8.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.AssetSubTypeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions7, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject25, serializableAppearanceObject26, serializableAppearanceObject27, serializableAppearanceObject28, "Edit Underlying Data", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions8, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject29, serializableAppearanceObject30, serializableAppearanceObject31, serializableAppearanceObject32, "Reload Underlying Data", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.AssetSubTypeIDGridLookUpEdit.Properties.DataSource = this.sp03030EPAssetSubTypesBindingSource;
            this.AssetSubTypeIDGridLookUpEdit.Properties.DisplayMember = "AssetSubTypeDescription";
            this.AssetSubTypeIDGridLookUpEdit.Properties.NullText = "";
            this.AssetSubTypeIDGridLookUpEdit.Properties.PopupView = this.gridView5;
            this.AssetSubTypeIDGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2});
            this.AssetSubTypeIDGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.AssetSubTypeIDGridLookUpEdit.Properties.ValueMember = "AssetSubTypeID";
            this.AssetSubTypeIDGridLookUpEdit.Size = new System.Drawing.Size(345, 22);
            this.AssetSubTypeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.AssetSubTypeIDGridLookUpEdit.TabIndex = 7;
            this.AssetSubTypeIDGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.AssetSubTypeIDGridLookUpEdit_ButtonClick);
            this.AssetSubTypeIDGridLookUpEdit.Enter += new System.EventHandler(this.AssetSubTypeIDGridLookUpEdit_Enter);
            this.AssetSubTypeIDGridLookUpEdit.Leave += new System.EventHandler(this.AssetSubTypeIDGridLookUpEdit_Leave);
            this.AssetSubTypeIDGridLookUpEdit.Validated += new System.EventHandler(this.AssetSubTypeIDGridLookUpEdit_Validated);
            // 
            // sp03030EPAssetSubTypesBindingSource
            // 
            this.sp03030EPAssetSubTypesBindingSource.DataMember = "sp03030_EP_Asset_Sub_Types";
            this.sp03030EPAssetSubTypesBindingSource.DataSource = this.dataSet_EP_DataEntry;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAssetSubTypeDescription,
            this.colAssetSubTypeID,
            this.colAssetSubTypeOrder,
            this.colAssetTypeDescription1,
            this.colAssetTypeID1,
            this.colAssetTypeOrder1,
            this.colRemarks1});
            this.gridView5.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition4.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition4.Appearance.Options.UseForeColor = true;
            styleFormatCondition4.ApplyToRow = true;
            styleFormatCondition4.Column = this.colAssetSubTypeID;
            styleFormatCondition4.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition4.Value1 = 0;
            this.gridView5.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition4});
            this.gridView5.GroupCount = 1;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAssetTypeDescription1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAssetSubTypeOrder, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAssetSubTypeDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colAssetSubTypeDescription
            // 
            this.colAssetSubTypeDescription.Caption = "Asset Sub-Type";
            this.colAssetSubTypeDescription.FieldName = "AssetSubTypeDescription";
            this.colAssetSubTypeDescription.Name = "colAssetSubTypeDescription";
            this.colAssetSubTypeDescription.OptionsColumn.AllowEdit = false;
            this.colAssetSubTypeDescription.OptionsColumn.AllowFocus = false;
            this.colAssetSubTypeDescription.OptionsColumn.ReadOnly = true;
            this.colAssetSubTypeDescription.Visible = true;
            this.colAssetSubTypeDescription.VisibleIndex = 0;
            this.colAssetSubTypeDescription.Width = 253;
            // 
            // colAssetSubTypeOrder
            // 
            this.colAssetSubTypeOrder.Caption = "Asset Sub-Type Order";
            this.colAssetSubTypeOrder.FieldName = "AssetSubTypeOrder";
            this.colAssetSubTypeOrder.Name = "colAssetSubTypeOrder";
            this.colAssetSubTypeOrder.OptionsColumn.AllowEdit = false;
            this.colAssetSubTypeOrder.OptionsColumn.AllowFocus = false;
            this.colAssetSubTypeOrder.OptionsColumn.ReadOnly = true;
            this.colAssetSubTypeOrder.Width = 162;
            // 
            // colAssetTypeDescription1
            // 
            this.colAssetTypeDescription1.Caption = "Asset Type";
            this.colAssetTypeDescription1.FieldName = "AssetTypeDescription";
            this.colAssetTypeDescription1.Name = "colAssetTypeDescription1";
            this.colAssetTypeDescription1.OptionsColumn.AllowEdit = false;
            this.colAssetTypeDescription1.OptionsColumn.AllowFocus = false;
            this.colAssetTypeDescription1.OptionsColumn.ReadOnly = true;
            this.colAssetTypeDescription1.Visible = true;
            this.colAssetTypeDescription1.VisibleIndex = 1;
            this.colAssetTypeDescription1.Width = 171;
            // 
            // colAssetTypeID1
            // 
            this.colAssetTypeID1.Caption = "Asset Type ID";
            this.colAssetTypeID1.FieldName = "AssetTypeID";
            this.colAssetTypeID1.Name = "colAssetTypeID1";
            this.colAssetTypeID1.OptionsColumn.AllowEdit = false;
            this.colAssetTypeID1.OptionsColumn.AllowFocus = false;
            this.colAssetTypeID1.OptionsColumn.ReadOnly = true;
            this.colAssetTypeID1.Width = 160;
            // 
            // colAssetTypeOrder1
            // 
            this.colAssetTypeOrder1.Caption = "Asset Type Order";
            this.colAssetTypeOrder1.FieldName = "AssetTypeOrder";
            this.colAssetTypeOrder1.Name = "colAssetTypeOrder1";
            this.colAssetTypeOrder1.OptionsColumn.AllowEdit = false;
            this.colAssetTypeOrder1.OptionsColumn.AllowFocus = false;
            this.colAssetTypeOrder1.OptionsColumn.ReadOnly = true;
            this.colAssetTypeOrder1.Width = 158;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Asset Sub-Type Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.Width = 148;
            // 
            // SiteIDGridLookUpEdit
            // 
            this.SiteIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03026EPAssetEditBindingSource, "SiteID", true));
            this.SiteIDGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SiteIDGridLookUpEdit.Location = new System.Drawing.Point(127, 69);
            this.SiteIDGridLookUpEdit.MenuManager = this.barManager1;
            this.SiteIDGridLookUpEdit.Name = "SiteIDGridLookUpEdit";
            editorButtonImageOptions9.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions10.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.SiteIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions9, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject33, serializableAppearanceObject34, serializableAppearanceObject35, serializableAppearanceObject36, "Edit Underlying Data", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions10, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject37, serializableAppearanceObject38, serializableAppearanceObject39, serializableAppearanceObject40, "Reload Underlying Data", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.SiteIDGridLookUpEdit.Properties.DataSource = this.sp03028EPSitesWithBlankDDLBBindingSource;
            this.SiteIDGridLookUpEdit.Properties.DisplayMember = "ClientSiteName";
            this.SiteIDGridLookUpEdit.Properties.NullText = "";
            this.SiteIDGridLookUpEdit.Properties.PopupView = this.gridView6;
            this.SiteIDGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.SiteIDGridLookUpEdit.Properties.ValueMember = "SiteID";
            this.SiteIDGridLookUpEdit.Size = new System.Drawing.Size(345, 22);
            this.SiteIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.SiteIDGridLookUpEdit.TabIndex = 5;
            this.SiteIDGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.SiteIDGridLookUpEdit_ButtonClick);
            this.SiteIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.SiteIDGridLookUpEdit_Validating);
            // 
            // sp03028EPSitesWithBlankDDLBBindingSource
            // 
            this.sp03028EPSitesWithBlankDDLBBindingSource.DataMember = "sp03028_EP_Sites_With_Blank_DDLB";
            this.sp03028EPSitesWithBlankDDLBBindingSource.DataSource = this.dataSet_EP_DataEntry;
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientCode,
            this.colClientID,
            this.colClientName,
            this.colClientSiteName,
            this.colContactPerson,
            this.colSiteAddressLine1,
            this.colSiteCode,
            this.colSiteID,
            this.colSiteName,
            this.colSiteTypeDescription,
            this.colSiteTypeID,
            this.colXCoordinate,
            this.colYCoordinate});
            this.gridView6.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition5.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition5.Appearance.Options.UseForeColor = true;
            styleFormatCondition5.ApplyToRow = true;
            styleFormatCondition5.Column = this.colSiteID;
            styleFormatCondition5.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition5.Value1 = 0;
            this.gridView6.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition5});
            this.gridView6.GroupCount = 1;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView6.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView6.OptionsLayout.StoreAppearance = true;
            this.gridView6.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView6.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView6.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView6.OptionsView.ShowIndicator = false;
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colClientCode
            // 
            this.colClientCode.Caption = "Client Code";
            this.colClientCode.FieldName = "ClientCode";
            this.colClientCode.Name = "colClientCode";
            this.colClientCode.OptionsColumn.AllowEdit = false;
            this.colClientCode.OptionsColumn.AllowFocus = false;
            this.colClientCode.OptionsColumn.ReadOnly = true;
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Width = 181;
            // 
            // colClientSiteName
            // 
            this.colClientSiteName.Caption = "Client \\ Site Name";
            this.colClientSiteName.FieldName = "ClientSiteName";
            this.colClientSiteName.Name = "colClientSiteName";
            this.colClientSiteName.OptionsColumn.AllowEdit = false;
            this.colClientSiteName.OptionsColumn.AllowFocus = false;
            this.colClientSiteName.OptionsColumn.ReadOnly = true;
            this.colClientSiteName.Width = 242;
            // 
            // colContactPerson
            // 
            this.colContactPerson.Caption = "Contact Person";
            this.colContactPerson.FieldName = "ContactPerson";
            this.colContactPerson.Name = "colContactPerson";
            this.colContactPerson.OptionsColumn.AllowEdit = false;
            this.colContactPerson.OptionsColumn.AllowFocus = false;
            this.colContactPerson.OptionsColumn.ReadOnly = true;
            this.colContactPerson.Visible = true;
            this.colContactPerson.VisibleIndex = 3;
            this.colContactPerson.Width = 168;
            // 
            // colSiteAddressLine1
            // 
            this.colSiteAddressLine1.Caption = "Address Line 1";
            this.colSiteAddressLine1.FieldName = "SiteAddressLine1";
            this.colSiteAddressLine1.Name = "colSiteAddressLine1";
            this.colSiteAddressLine1.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine1.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine1.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine1.Visible = true;
            this.colSiteAddressLine1.VisibleIndex = 4;
            this.colSiteAddressLine1.Width = 141;
            // 
            // colSiteCode
            // 
            this.colSiteCode.Caption = "Site Code";
            this.colSiteCode.FieldName = "SiteCode";
            this.colSiteCode.Name = "colSiteCode";
            this.colSiteCode.OptionsColumn.AllowEdit = false;
            this.colSiteCode.OptionsColumn.AllowFocus = false;
            this.colSiteCode.OptionsColumn.ReadOnly = true;
            this.colSiteCode.Visible = true;
            this.colSiteCode.VisibleIndex = 1;
            this.colSiteCode.Width = 96;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.Visible = true;
            this.colSiteName.VisibleIndex = 0;
            this.colSiteName.Width = 251;
            // 
            // colSiteTypeDescription
            // 
            this.colSiteTypeDescription.Caption = "Site Type";
            this.colSiteTypeDescription.FieldName = "SiteTypeDescription";
            this.colSiteTypeDescription.Name = "colSiteTypeDescription";
            this.colSiteTypeDescription.OptionsColumn.AllowEdit = false;
            this.colSiteTypeDescription.OptionsColumn.AllowFocus = false;
            this.colSiteTypeDescription.OptionsColumn.ReadOnly = true;
            this.colSiteTypeDescription.Visible = true;
            this.colSiteTypeDescription.VisibleIndex = 2;
            this.colSiteTypeDescription.Width = 99;
            // 
            // colSiteTypeID
            // 
            this.colSiteTypeID.Caption = "Site Type ID";
            this.colSiteTypeID.FieldName = "SiteTypeID";
            this.colSiteTypeID.Name = "colSiteTypeID";
            this.colSiteTypeID.OptionsColumn.AllowEdit = false;
            this.colSiteTypeID.OptionsColumn.AllowFocus = false;
            this.colSiteTypeID.OptionsColumn.ReadOnly = true;
            // 
            // colXCoordinate
            // 
            this.colXCoordinate.Caption = "X Coordinate";
            this.colXCoordinate.FieldName = "XCoordinate";
            this.colXCoordinate.Name = "colXCoordinate";
            this.colXCoordinate.OptionsColumn.AllowEdit = false;
            this.colXCoordinate.OptionsColumn.AllowFocus = false;
            this.colXCoordinate.OptionsColumn.ReadOnly = true;
            // 
            // colYCoordinate
            // 
            this.colYCoordinate.Caption = "Y Coordinate";
            this.colYCoordinate.FieldName = "YCoordinate";
            this.colYCoordinate.Name = "colYCoordinate";
            this.colYCoordinate.OptionsColumn.AllowEdit = false;
            this.colYCoordinate.OptionsColumn.AllowFocus = false;
            this.colYCoordinate.OptionsColumn.ReadOnly = true;
            // 
            // CurrentConditionIDGridLookUpEdit
            // 
            this.CurrentConditionIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03026EPAssetEditBindingSource, "CurrentConditionID", true));
            this.CurrentConditionIDGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CurrentConditionIDGridLookUpEdit.Location = new System.Drawing.Point(127, 255);
            this.CurrentConditionIDGridLookUpEdit.MenuManager = this.barManager1;
            this.CurrentConditionIDGridLookUpEdit.Name = "CurrentConditionIDGridLookUpEdit";
            editorButtonImageOptions11.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions12.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.CurrentConditionIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions11, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject41, serializableAppearanceObject42, serializableAppearanceObject43, serializableAppearanceObject44, "Edit Underlying Data", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions12, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject45, serializableAppearanceObject46, serializableAppearanceObject47, serializableAppearanceObject48, "Reload Underlying Data", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.CurrentConditionIDGridLookUpEdit.Properties.DataSource = this.sp03031EPAssetConditionsBindingSource;
            this.CurrentConditionIDGridLookUpEdit.Properties.DisplayMember = "ConditionDescription";
            this.CurrentConditionIDGridLookUpEdit.Properties.NullText = "";
            this.CurrentConditionIDGridLookUpEdit.Properties.PopupView = this.gridView7;
            this.CurrentConditionIDGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3});
            this.CurrentConditionIDGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.CurrentConditionIDGridLookUpEdit.Properties.ValueMember = "ConditionID";
            this.CurrentConditionIDGridLookUpEdit.Size = new System.Drawing.Size(762, 22);
            this.CurrentConditionIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.CurrentConditionIDGridLookUpEdit.TabIndex = 21;
            this.CurrentConditionIDGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.CurrentConditionIDGridLookUpEdit_ButtonClick);
            this.CurrentConditionIDGridLookUpEdit.Enter += new System.EventHandler(this.CurrentConditionIDGridLookUpEdit_Enter);
            this.CurrentConditionIDGridLookUpEdit.Leave += new System.EventHandler(this.CurrentConditionIDGridLookUpEdit_Leave);
            this.CurrentConditionIDGridLookUpEdit.Validated += new System.EventHandler(this.CurrentConditionIDGridLookUpEdit_Validated);
            // 
            // sp03031EPAssetConditionsBindingSource
            // 
            this.sp03031EPAssetConditionsBindingSource.DataMember = "sp03031_EP_Asset_Conditions";
            this.sp03031EPAssetConditionsBindingSource.DataSource = this.dataSet_EP_DataEntry;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAssetTypeDescription2,
            this.colAssetTypeID2,
            this.colAssetTypeOrder2,
            this.colConditionDescription,
            this.colConditionID,
            this.colConditionOrder,
            this.colRemarks2});
            this.gridView7.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition6.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition6.Appearance.Options.UseForeColor = true;
            styleFormatCondition6.ApplyToRow = true;
            styleFormatCondition6.Column = this.colConditionID;
            styleFormatCondition6.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition6.Value1 = 0;
            this.gridView7.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition6});
            this.gridView7.GroupCount = 1;
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView7.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView7.OptionsLayout.StoreAppearance = true;
            this.gridView7.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView7.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView7.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView7.OptionsView.ColumnAutoWidth = false;
            this.gridView7.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView7.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            this.gridView7.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView7.OptionsView.ShowIndicator = false;
            this.gridView7.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAssetTypeDescription2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colConditionOrder, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colConditionDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colAssetTypeDescription2
            // 
            this.colAssetTypeDescription2.Caption = "Asset Type";
            this.colAssetTypeDescription2.FieldName = "AssetTypeDescription";
            this.colAssetTypeDescription2.Name = "colAssetTypeDescription2";
            this.colAssetTypeDescription2.OptionsColumn.AllowEdit = false;
            this.colAssetTypeDescription2.OptionsColumn.AllowFocus = false;
            this.colAssetTypeDescription2.OptionsColumn.ReadOnly = true;
            this.colAssetTypeDescription2.Visible = true;
            this.colAssetTypeDescription2.VisibleIndex = 0;
            this.colAssetTypeDescription2.Width = 160;
            // 
            // colAssetTypeID2
            // 
            this.colAssetTypeID2.Caption = "Asset Type ID";
            this.colAssetTypeID2.FieldName = "AssetTypeID";
            this.colAssetTypeID2.Name = "colAssetTypeID2";
            this.colAssetTypeID2.OptionsColumn.AllowEdit = false;
            this.colAssetTypeID2.OptionsColumn.AllowFocus = false;
            this.colAssetTypeID2.OptionsColumn.ReadOnly = true;
            this.colAssetTypeID2.Width = 100;
            // 
            // colAssetTypeOrder2
            // 
            this.colAssetTypeOrder2.Caption = "Asset Type Order";
            this.colAssetTypeOrder2.FieldName = "AssetTypeOrder";
            this.colAssetTypeOrder2.Name = "colAssetTypeOrder2";
            this.colAssetTypeOrder2.OptionsColumn.AllowEdit = false;
            this.colAssetTypeOrder2.OptionsColumn.AllowFocus = false;
            this.colAssetTypeOrder2.OptionsColumn.ReadOnly = true;
            this.colAssetTypeOrder2.Width = 111;
            // 
            // colConditionDescription
            // 
            this.colConditionDescription.Caption = "Condition";
            this.colConditionDescription.FieldName = "ConditionDescription";
            this.colConditionDescription.Name = "colConditionDescription";
            this.colConditionDescription.OptionsColumn.AllowEdit = false;
            this.colConditionDescription.OptionsColumn.AllowFocus = false;
            this.colConditionDescription.OptionsColumn.ReadOnly = true;
            this.colConditionDescription.Visible = true;
            this.colConditionDescription.VisibleIndex = 0;
            this.colConditionDescription.Width = 233;
            // 
            // colConditionOrder
            // 
            this.colConditionOrder.Caption = "Condition Order";
            this.colConditionOrder.FieldName = "ConditionOrder";
            this.colConditionOrder.Name = "colConditionOrder";
            this.colConditionOrder.OptionsColumn.AllowEdit = false;
            this.colConditionOrder.OptionsColumn.AllowFocus = false;
            this.colConditionOrder.OptionsColumn.ReadOnly = true;
            this.colConditionOrder.Width = 113;
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Condition Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.ReadOnly = true;
            this.colRemarks2.Width = 117;
            // 
            // AssetNumberButtonEdit
            // 
            this.AssetNumberButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03026EPAssetEditBindingSource, "AssetNumber", true));
            this.AssetNumberButtonEdit.Location = new System.Drawing.Point(609, 141);
            this.AssetNumberButtonEdit.MenuManager = this.barManager1;
            this.AssetNumberButtonEdit.Name = "AssetNumberButtonEdit";
            superToolTip5.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem5.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Text = "Sequence Button - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to <b>open</b> the <b>Choose Sequence screen</b> to <b>select the prefix" +
    "</b> for the sequence.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            superToolTip6.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem6.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Appearance.Options.UseImage = true;
            toolTipTitleItem6.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Text = "Number Button - Information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Click me to <b>calculate</b> the <b>number suffix</b> for the sequence.";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this.AssetNumberButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Sequence", -1, true, true, true, editorButtonImageOptions13, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject49, serializableAppearanceObject50, serializableAppearanceObject51, serializableAppearanceObject52, "", "Sequence", superToolTip5, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Number", -1, true, true, false, editorButtonImageOptions14, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject53, serializableAppearanceObject54, serializableAppearanceObject55, serializableAppearanceObject56, "", "Number", superToolTip6, DevExpress.Utils.ToolTipAnchor.Default)});
            this.AssetNumberButtonEdit.Properties.MaxLength = 50;
            this.AssetNumberButtonEdit.Size = new System.Drawing.Size(280, 20);
            this.AssetNumberButtonEdit.StyleController = this.dataLayoutControl1;
            this.AssetNumberButtonEdit.TabIndex = 18;
            this.AssetNumberButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.AssetNumberButtonEdit_ButtonClick);
            // 
            // ItemForAssetID
            // 
            this.ItemForAssetID.Control = this.AssetIDTextEdit;
            this.ItemForAssetID.CustomizationFormText = "Asset ID:";
            this.ItemForAssetID.Location = new System.Drawing.Point(0, 0);
            this.ItemForAssetID.Name = "ItemForAssetID";
            this.ItemForAssetID.Size = new System.Drawing.Size(876, 24);
            this.ItemForAssetID.Text = "Asset ID:";
            this.ItemForAssetID.TextSize = new System.Drawing.Size(104, 13);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(913, 481);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.layoutControlItem1,
            this.layoutControlGroup4,
            this.tabbedControlGroup1,
            this.emptySpaceItem4,
            this.layoutControlGroup7,
            this.splitterItem1,
            this.emptySpaceItem3});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(893, 461);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(200, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(693, 23);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(200, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Core Data";
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSiteID,
            this.ItemForAssetTypeID,
            this.ItemForAssetSubTypeID,
            this.ItemForStatusID});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 23);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(476, 150);
            this.layoutControlGroup4.Text = "Core Data";
            // 
            // ItemForSiteID
            // 
            this.ItemForSiteID.AllowHide = false;
            this.ItemForSiteID.Control = this.SiteIDGridLookUpEdit;
            this.ItemForSiteID.CustomizationFormText = "Parent Site:";
            this.ItemForSiteID.Location = new System.Drawing.Point(0, 0);
            this.ItemForSiteID.Name = "ItemForSiteID";
            this.ItemForSiteID.Size = new System.Drawing.Size(452, 26);
            this.ItemForSiteID.Text = "Parent Site:";
            this.ItemForSiteID.TextSize = new System.Drawing.Size(100, 13);
            // 
            // ItemForAssetTypeID
            // 
            this.ItemForAssetTypeID.AllowHide = false;
            this.ItemForAssetTypeID.Control = this.AssetTypeIDGridLookUpEdit;
            this.ItemForAssetTypeID.CustomizationFormText = "Asset Type:";
            this.ItemForAssetTypeID.Location = new System.Drawing.Point(0, 26);
            this.ItemForAssetTypeID.Name = "ItemForAssetTypeID";
            this.ItemForAssetTypeID.Size = new System.Drawing.Size(452, 26);
            this.ItemForAssetTypeID.Text = "Asset Type:";
            this.ItemForAssetTypeID.TextSize = new System.Drawing.Size(100, 13);
            // 
            // ItemForAssetSubTypeID
            // 
            this.ItemForAssetSubTypeID.Control = this.AssetSubTypeIDGridLookUpEdit;
            this.ItemForAssetSubTypeID.CustomizationFormText = "Asset Sub Type:";
            this.ItemForAssetSubTypeID.Location = new System.Drawing.Point(0, 52);
            this.ItemForAssetSubTypeID.Name = "ItemForAssetSubTypeID";
            this.ItemForAssetSubTypeID.Size = new System.Drawing.Size(452, 26);
            this.ItemForAssetSubTypeID.Text = "Asset Sub-Type:";
            this.ItemForAssetSubTypeID.TextSize = new System.Drawing.Size(100, 13);
            // 
            // ItemForStatusID
            // 
            this.ItemForStatusID.Control = this.StatusIDGridLookUpEdit;
            this.ItemForStatusID.CustomizationFormText = "Status:";
            this.ItemForStatusID.Location = new System.Drawing.Point(0, 78);
            this.ItemForStatusID.Name = "ItemForStatusID";
            this.ItemForStatusID.Size = new System.Drawing.Size(452, 26);
            this.ItemForStatusID.Text = "Status:";
            this.ItemForStatusID.TextSize = new System.Drawing.Size(100, 13);
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 183);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup8;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(893, 244);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup8,
            this.layoutControlGroup3,
            this.layoutControlGroup5,
            this.layoutControlGroup6});
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.CustomizationFormText = "General Details";
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForLifeSpanValue,
            this.ItemForCurrentConditionID,
            this.ItemForLastVisitDate,
            this.emptySpaceItem5,
            this.ItemForNextVisitDate,
            this.emptySpaceItem6,
            this.emptySpaceItem7,
            this.ItemForLifeSpanValueDescriptor});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Size = new System.Drawing.Size(869, 196);
            this.layoutControlGroup8.Text = "General Details";
            // 
            // ItemForLifeSpanValue
            // 
            this.ItemForLifeSpanValue.Control = this.LifeSpanValueSpinEdit;
            this.ItemForLifeSpanValue.CustomizationFormText = "Life Span Value:";
            this.ItemForLifeSpanValue.Location = new System.Drawing.Point(0, 0);
            this.ItemForLifeSpanValue.MaxSize = new System.Drawing.Size(253, 24);
            this.ItemForLifeSpanValue.MinSize = new System.Drawing.Size(253, 24);
            this.ItemForLifeSpanValue.Name = "ItemForLifeSpanValue";
            this.ItemForLifeSpanValue.Size = new System.Drawing.Size(253, 24);
            this.ItemForLifeSpanValue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForLifeSpanValue.Text = "Life Span Value:";
            this.ItemForLifeSpanValue.TextSize = new System.Drawing.Size(100, 13);
            // 
            // ItemForCurrentConditionID
            // 
            this.ItemForCurrentConditionID.Control = this.CurrentConditionIDGridLookUpEdit;
            this.ItemForCurrentConditionID.CustomizationFormText = "Current Condition:";
            this.ItemForCurrentConditionID.Location = new System.Drawing.Point(0, 24);
            this.ItemForCurrentConditionID.Name = "ItemForCurrentConditionID";
            this.ItemForCurrentConditionID.Size = new System.Drawing.Size(869, 26);
            this.ItemForCurrentConditionID.Text = "Current Condition:";
            this.ItemForCurrentConditionID.TextSize = new System.Drawing.Size(100, 13);
            // 
            // ItemForLastVisitDate
            // 
            this.ItemForLastVisitDate.Control = this.LastVisitDateDateEdit;
            this.ItemForLastVisitDate.CustomizationFormText = "Last Visit Date:";
            this.ItemForLastVisitDate.Location = new System.Drawing.Point(0, 50);
            this.ItemForLastVisitDate.MaxSize = new System.Drawing.Size(253, 24);
            this.ItemForLastVisitDate.MinSize = new System.Drawing.Size(253, 24);
            this.ItemForLastVisitDate.Name = "ItemForLastVisitDate";
            this.ItemForLastVisitDate.Size = new System.Drawing.Size(253, 24);
            this.ItemForLastVisitDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForLastVisitDate.Text = "Last Visit Date:";
            this.ItemForLastVisitDate.TextSize = new System.Drawing.Size(100, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 98);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(869, 98);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForNextVisitDate
            // 
            this.ItemForNextVisitDate.Control = this.NextVisitDateDateEdit;
            this.ItemForNextVisitDate.CustomizationFormText = "Next Visit Date:";
            this.ItemForNextVisitDate.Location = new System.Drawing.Point(0, 74);
            this.ItemForNextVisitDate.MaxSize = new System.Drawing.Size(253, 24);
            this.ItemForNextVisitDate.MinSize = new System.Drawing.Size(253, 24);
            this.ItemForNextVisitDate.Name = "ItemForNextVisitDate";
            this.ItemForNextVisitDate.Size = new System.Drawing.Size(253, 24);
            this.ItemForNextVisitDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForNextVisitDate.Text = "Next Visit Date:";
            this.ItemForNextVisitDate.TextSize = new System.Drawing.Size(100, 13);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.Location = new System.Drawing.Point(253, 50);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(616, 24);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.Location = new System.Drawing.Point(253, 74);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(616, 24);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForLifeSpanValueDescriptor
            // 
            this.ItemForLifeSpanValueDescriptor.Control = this.LifeSpanValueDescriptorGridLookUpEdit;
            this.ItemForLifeSpanValueDescriptor.CustomizationFormText = "Life Span Descriptor:";
            this.ItemForLifeSpanValueDescriptor.Location = new System.Drawing.Point(253, 0);
            this.ItemForLifeSpanValueDescriptor.Name = "ItemForLifeSpanValueDescriptor";
            this.ItemForLifeSpanValueDescriptor.Size = new System.Drawing.Size(616, 24);
            this.ItemForLifeSpanValueDescriptor.Text = "Life Span Descriptor:";
            this.ItemForLifeSpanValueDescriptor.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Linked Actions";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(869, 196);
            this.layoutControlGroup3.Text = "Linked Actions";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridControl1;
            this.layoutControlItem2.CustomizationFormText = "Linked Actions:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(869, 196);
            this.layoutControlItem2.Text = "Linked Actions:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Mapping";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForXCoordinate,
            this.ItemForPolygonXY,
            this.ItemForArea,
            this.ItemForMapID,
            this.layoutControlItem4,
            this.ItemForYCoordinate,
            this.layoutControlItem3,
            this.layoutControlItem5,
            this.ItemForLength,
            this.ItemForWidth,
            this.emptySpaceItem2});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(869, 196);
            this.layoutControlGroup5.Text = "Mapping";
            // 
            // ItemForXCoordinate
            // 
            this.ItemForXCoordinate.Control = this.XCoordinateSpinEdit;
            this.ItemForXCoordinate.CustomizationFormText = "X Coordinate:";
            this.ItemForXCoordinate.Location = new System.Drawing.Point(0, 24);
            this.ItemForXCoordinate.Name = "ItemForXCoordinate";
            this.ItemForXCoordinate.Size = new System.Drawing.Size(434, 24);
            this.ItemForXCoordinate.Text = "X Coordinate:";
            this.ItemForXCoordinate.TextSize = new System.Drawing.Size(100, 13);
            // 
            // ItemForPolygonXY
            // 
            this.ItemForPolygonXY.Control = this.PolygonXYMemoEdit;
            this.ItemForPolygonXY.CustomizationFormText = "Poly X\\Y Coordinates:";
            this.ItemForPolygonXY.Location = new System.Drawing.Point(0, 72);
            this.ItemForPolygonXY.Name = "ItemForPolygonXY";
            this.ItemForPolygonXY.Size = new System.Drawing.Size(434, 76);
            this.ItemForPolygonXY.Text = "Poly X \\ Y Coords:";
            this.ItemForPolygonXY.TextSize = new System.Drawing.Size(100, 13);
            // 
            // ItemForArea
            // 
            this.ItemForArea.Control = this.AreaSpinEdit;
            this.ItemForArea.CustomizationFormText = "Area (M�):";
            this.ItemForArea.Location = new System.Drawing.Point(0, 172);
            this.ItemForArea.Name = "ItemForArea";
            this.ItemForArea.Size = new System.Drawing.Size(434, 24);
            this.ItemForArea.Text = "Area (M�):";
            this.ItemForArea.TextSize = new System.Drawing.Size(100, 13);
            // 
            // ItemForMapID
            // 
            this.ItemForMapID.Control = this.MapIDTextEdit;
            this.ItemForMapID.CustomizationFormText = "Map ID:";
            this.ItemForMapID.Location = new System.Drawing.Point(0, 0);
            this.ItemForMapID.Name = "ItemForMapID";
            this.ItemForMapID.Size = new System.Drawing.Size(869, 24);
            this.ItemForMapID.Text = "Map ID:";
            this.ItemForMapID.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.LocationYSpinEdit;
            this.layoutControlItem4.CustomizationFormText = "Longitude:";
            this.layoutControlItem4.Location = new System.Drawing.Point(434, 48);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(435, 24);
            this.layoutControlItem4.Text = "Longitude:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(100, 13);
            // 
            // ItemForYCoordinate
            // 
            this.ItemForYCoordinate.Control = this.YCoordinateSpinEdit;
            this.ItemForYCoordinate.CustomizationFormText = "Y Coordinate:";
            this.ItemForYCoordinate.Location = new System.Drawing.Point(0, 48);
            this.ItemForYCoordinate.Name = "ItemForYCoordinate";
            this.ItemForYCoordinate.Size = new System.Drawing.Size(434, 24);
            this.ItemForYCoordinate.Text = "Y Coordinate:";
            this.ItemForYCoordinate.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.LocationXSpinEdit1;
            this.layoutControlItem3.CustomizationFormText = "Latitude:";
            this.layoutControlItem3.Location = new System.Drawing.Point(434, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(435, 24);
            this.layoutControlItem3.Text = "Latitude:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.LocationPolyXYMemoEdit;
            this.layoutControlItem5.CustomizationFormText = "Poly Lat \\ Long:";
            this.layoutControlItem5.Location = new System.Drawing.Point(434, 72);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(435, 76);
            this.layoutControlItem5.Text = "Poly Lat \\ Long:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(100, 13);
            // 
            // ItemForLength
            // 
            this.ItemForLength.Control = this.LengthSpinEdit;
            this.ItemForLength.CustomizationFormText = "Length (M):";
            this.ItemForLength.Location = new System.Drawing.Point(0, 148);
            this.ItemForLength.Name = "ItemForLength";
            this.ItemForLength.Size = new System.Drawing.Size(434, 24);
            this.ItemForLength.Text = "Length (M):";
            this.ItemForLength.TextSize = new System.Drawing.Size(100, 13);
            // 
            // ItemForWidth
            // 
            this.ItemForWidth.Control = this.WidthSpinEdit;
            this.ItemForWidth.CustomizationFormText = "Width (M):";
            this.ItemForWidth.Location = new System.Drawing.Point(434, 148);
            this.ItemForWidth.Name = "ItemForWidth";
            this.ItemForWidth.Size = new System.Drawing.Size(435, 24);
            this.ItemForWidth.Text = "Width (M):";
            this.ItemForWidth.TextSize = new System.Drawing.Size(100, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(434, 172);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(435, 24);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CaptionImageOptions.Image = global::WoodPlan5.Properties.Resources.Notes_16x16;
            this.layoutControlGroup6.CustomizationFormText = "Remarks";
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(869, 196);
            this.layoutControlGroup6.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(869, 196);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 173);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(893, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "Numbers";
            this.layoutControlGroup7.ExpandButtonVisible = true;
            this.layoutControlGroup7.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForPartNumber,
            this.ItemForSerialNumber,
            this.ItemForModelNumber,
            this.ItemForAssetNumber});
            this.layoutControlGroup7.Location = new System.Drawing.Point(482, 23);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(411, 150);
            this.layoutControlGroup7.Text = "Numbers";
            // 
            // ItemForPartNumber
            // 
            this.ItemForPartNumber.Control = this.PartNumberTextEdit;
            this.ItemForPartNumber.CustomizationFormText = "Part Number:";
            this.ItemForPartNumber.Location = new System.Drawing.Point(0, 0);
            this.ItemForPartNumber.Name = "ItemForPartNumber";
            this.ItemForPartNumber.Size = new System.Drawing.Size(387, 24);
            this.ItemForPartNumber.Text = "Part Number:";
            this.ItemForPartNumber.TextSize = new System.Drawing.Size(100, 13);
            // 
            // ItemForSerialNumber
            // 
            this.ItemForSerialNumber.Control = this.SerialNumberTextEdit;
            this.ItemForSerialNumber.CustomizationFormText = "Serial Number:";
            this.ItemForSerialNumber.Location = new System.Drawing.Point(0, 24);
            this.ItemForSerialNumber.Name = "ItemForSerialNumber";
            this.ItemForSerialNumber.Size = new System.Drawing.Size(387, 24);
            this.ItemForSerialNumber.Text = "Serial Number:";
            this.ItemForSerialNumber.TextSize = new System.Drawing.Size(100, 13);
            // 
            // ItemForModelNumber
            // 
            this.ItemForModelNumber.Control = this.ModelNumberTextEdit;
            this.ItemForModelNumber.CustomizationFormText = "Model Number:";
            this.ItemForModelNumber.Location = new System.Drawing.Point(0, 48);
            this.ItemForModelNumber.Name = "ItemForModelNumber";
            this.ItemForModelNumber.Size = new System.Drawing.Size(387, 24);
            this.ItemForModelNumber.Text = "Model Number:";
            this.ItemForModelNumber.TextSize = new System.Drawing.Size(100, 13);
            // 
            // ItemForAssetNumber
            // 
            this.ItemForAssetNumber.Control = this.AssetNumberButtonEdit;
            this.ItemForAssetNumber.CustomizationFormText = "Asset Number:";
            this.ItemForAssetNumber.Location = new System.Drawing.Point(0, 72);
            this.ItemForAssetNumber.Name = "ItemForAssetNumber";
            this.ItemForAssetNumber.Size = new System.Drawing.Size(387, 32);
            this.ItemForAssetNumber.Text = "Asset Number:";
            this.ItemForAssetNumber.TextSize = new System.Drawing.Size(100, 13);
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.CustomizationFormText = "splitterItem1";
            this.splitterItem1.Location = new System.Drawing.Point(476, 23);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(6, 150);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 427);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(893, 34);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp03026_EP_Asset_EditTableAdapter
            // 
            this.sp03026_EP_Asset_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp03028_EP_Sites_With_Blank_DDLBTableAdapter
            // 
            this.sp03028_EP_Sites_With_Blank_DDLBTableAdapter.ClearBeforeFill = true;
            // 
            // sp03019_EP_Asset_Types_List_With_BlankTableAdapter
            // 
            this.sp03019_EP_Asset_Types_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp03030_EP_Asset_Sub_TypesTableAdapter
            // 
            this.sp03030_EP_Asset_Sub_TypesTableAdapter.ClearBeforeFill = true;
            // 
            // sp03031_EP_Asset_ConditionsTableAdapter
            // 
            this.sp03031_EP_Asset_ConditionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter
            // 
            this.sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter.ClearBeforeFill = true;
            // 
            // sp03032_EP_Asset_Life_Expectancy_Description_With_BlankTableAdapter
            // 
            this.sp03032_EP_Asset_Life_Expectancy_Description_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp03035_EP_Actions_For_Passed_AssetsTableAdapter
            // 
            this.sp03035_EP_Actions_For_Passed_AssetsTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // frm_EP_Asset_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(913, 537);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_EP_Asset_Edit";
            this.Text = "Edit Asset";
            this.Activated += new System.EventHandler(this.frm_EP_Asset_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_EP_Asset_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_EP_Asset_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LocationPolyXYMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03026EPAssetEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_EP_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LocationYSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LocationXSpinEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03035EPActionsForPassedAssetsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_EP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AssetIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XCoordinateSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.YCoordinateSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PolygonXYMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AreaSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LengthSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WidthSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PartNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SerialNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ModelNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LifeSpanValueSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LastVisitDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LastVisitDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NextVisitDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NextVisitDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MapIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01372ATMultiplePicklistsWithBlanksBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LifeSpanValueDescriptorGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03032EPAssetLifeExpectancyDescriptionWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AssetTypeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03019EPAssetTypesListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AssetSubTypeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03030EPAssetSubTypesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03028EPSitesWithBlankDDLBBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CurrentConditionIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03031EPAssetConditionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AssetNumberButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAssetID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAssetTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAssetSubTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLifeSpanValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCurrentConditionID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLastVisitDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNextVisitDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLifeSpanValueDescriptor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForXCoordinate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPolygonXY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMapID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForYCoordinate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLength)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPartNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSerialNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForModelNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAssetNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.TextEdit AssetIDTextEdit;
        private System.Windows.Forms.BindingSource sp03026EPAssetEditBindingSource;
        private DataSet_EP_DataEntry dataSet_EP_DataEntry;
        private DevExpress.XtraEditors.SpinEdit XCoordinateSpinEdit;
        private DevExpress.XtraEditors.SpinEdit YCoordinateSpinEdit;
        private DevExpress.XtraEditors.MemoEdit PolygonXYMemoEdit;
        private DevExpress.XtraEditors.SpinEdit AreaSpinEdit;
        private DevExpress.XtraEditors.SpinEdit LengthSpinEdit;
        private DevExpress.XtraEditors.SpinEdit WidthSpinEdit;
        private DevExpress.XtraEditors.TextEdit PartNumberTextEdit;
        private DevExpress.XtraEditors.TextEdit SerialNumberTextEdit;
        private DevExpress.XtraEditors.TextEdit ModelNumberTextEdit;
        private DevExpress.XtraEditors.SpinEdit LifeSpanValueSpinEdit;
        private DevExpress.XtraEditors.DateEdit LastVisitDateDateEdit;
        private DevExpress.XtraEditors.DateEdit NextVisitDateDateEdit;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraEditors.TextEdit MapIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAssetID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAssetTypeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAssetSubTypeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForYCoordinate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPolygonXY;
        private DevExpress.XtraLayout.LayoutControlItem ItemForArea;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLength;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWidth;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStatusID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPartNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSerialNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForModelNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAssetNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLifeSpanValue;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLifeSpanValueDescriptor;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCurrentConditionID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLastVisitDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNextVisitDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMapID;
        private WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03026_EP_Asset_EditTableAdapter sp03026_EP_Asset_EditTableAdapter;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForXCoordinate;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.GridLookUpEdit StatusIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraEditors.GridLookUpEdit LifeSpanValueDescriptorGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem bbiShowMapButton;
        private DevExpress.XtraEditors.GridLookUpEdit AssetTypeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraEditors.GridLookUpEdit AssetSubTypeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraEditors.GridLookUpEdit SiteIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private System.Windows.Forms.BindingSource sp03028EPSitesWithBlankDDLBBindingSource;
        private WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03028_EP_Sites_With_Blank_DDLBTableAdapter sp03028_EP_Sites_With_Blank_DDLBTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colClientCode;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colContactPerson;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCode;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colXCoordinate;
        private DevExpress.XtraGrid.Columns.GridColumn colYCoordinate;
        private System.Windows.Forms.BindingSource sp03019EPAssetTypesListWithBlankBindingSource;
        private WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03019_EP_Asset_Types_List_With_BlankTableAdapter sp03019_EP_Asset_Types_List_With_BlankTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetTypeOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraEditors.GridLookUpEdit CurrentConditionIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private System.Windows.Forms.BindingSource sp03030EPAssetSubTypesBindingSource;
        private WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03030_EP_Asset_Sub_TypesTableAdapter sp03030_EP_Asset_Sub_TypesTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetSubTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetSubTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetSubTypeOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetTypeDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetTypeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetTypeOrder1;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource sp03031EPAssetConditionsBindingSource;
        private WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03031_EP_Asset_ConditionsTableAdapter sp03031_EP_Asset_ConditionsTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetTypeDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetTypeID2;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetTypeOrder2;
        private DevExpress.XtraGrid.Columns.GridColumn colConditionDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colConditionID;
        private DevExpress.XtraGrid.Columns.GridColumn colConditionOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private System.Windows.Forms.BindingSource sp01372ATMultiplePicklistsWithBlanksBindingSource;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colItemDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colItemID;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private System.Windows.Forms.BindingSource sp03032EPAssetLifeExpectancyDescriptionWithBlankBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder1;
        private DevExpress.XtraGrid.Columns.GridColumn colValue;
        private WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03032_EP_Asset_Life_Expectancy_Description_With_BlankTableAdapter sp03032_EP_Asset_Life_Expectancy_Description_With_BlankTableAdapter;
        private System.Windows.Forms.BindingSource sp03035EPActionsForPassedAssetsBindingSource;
        private DataSet_EP dataSet_EP;
        private DevExpress.XtraGrid.Columns.GridColumn colActionID;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetID;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionID;
        private DevExpress.XtraGrid.Columns.GridColumn colActionNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colActionDueDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionDoneDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedManHours;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActionRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientCode1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn colXCoordinate1;
        private DevExpress.XtraGrid.Columns.GridColumn colYCoordinate1;
        private DevExpress.XtraGrid.Columns.GridColumn colPolygonXY;
        private DevExpress.XtraGrid.Columns.GridColumn colArea;
        private DevExpress.XtraGrid.Columns.GridColumn colLength;
        private DevExpress.XtraGrid.Columns.GridColumn colWidth;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetStatusDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetPartNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetSerialNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetModelNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetLifeSpanValue;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetLifeSpanValueDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetConditionDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetLastVisitDate;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetNextVisitDate;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colMapID;
        private WoodPlan5.DataSet_EPTableAdapters.sp03035_EP_Actions_For_Passed_AssetsTableAdapter sp03035_EP_Actions_For_Passed_AssetsTableAdapter;
        private DevExpress.XtraEditors.ButtonEdit AssetNumberButtonEdit;
        private DevExpress.XtraEditors.MemoEdit LocationPolyXYMemoEdit;
        private DevExpress.XtraEditors.SpinEdit LocationYSpinEdit;
        private DevExpress.XtraEditors.SpinEdit LocationXSpinEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.Utils.ImageCollection imageCollection1;
    }
}

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WoodPlan5.Properties;
using BaseObjects;
using DevExpress.LookAndFeel;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraLayout;

namespace WoodPlan5
{
    public partial class frm_EP_Site_Inspection_Edit_Pause : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public int intInspectionID = 0;
        public string strInspectionDescription = "";
        public int intTimeDeductionID = 0;
        public DateTime dtStartDate;
        public string strReason = "";

        #endregion

        public frm_EP_Site_Inspection_Edit_Pause()
        {
            InitializeComponent();
        }

        private void frm_EP_Site_Inspection_Edit_Pause_Load(object sender, EventArgs e)
        {
            this.FormID = 30053;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            InspectionDescriptionTextEdit.EditValue = strInspectionDescription;
            PauseStartDateEdit.EditValue = DateTime.Now;
 
            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            this.ValidateChildren();  // Force Validation Message on any controls containing them //
        }

        public override void PostLoadView(object objParameter)
        {
        }


        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, LayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.ValidateChildren();  // Force Validation Message on any controls containing them //

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, layoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current screen!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            strReason = memoEdit1.EditValue.ToString();
            dtStartDate = PauseStartDateEdit.DateTime;
            
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }


        #region Editors

        private void PauseStartDateEdit_Validating(object sender, CancelEventArgs e)
        {
            DateEdit de = (DateEdit)sender;
            if (de.EditValue == null || string.IsNullOrEmpty(de.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(PauseStartDateEdit, "Select\\enter a date and time.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(PauseStartDateEdit, "");
            }
        }

        private void memoEdit1_Validating(object sender, CancelEventArgs e)
        {
            MemoEdit me = (MemoEdit)sender;
            if (me.EditValue == null || string.IsNullOrEmpty(me.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(memoEdit1, "Enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(memoEdit1, "");
            }
        }

        #endregion








    }
}


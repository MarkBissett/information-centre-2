namespace WoodPlan5
{
    partial class frm_EP_Asset_Type_Job_Master_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_EP_Asset_Type_Job_Master_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.colAssetTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.sp03040EPAssetJobsMasterEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_EP_DataEntry = new WoodPlan5.DataSet_EP_DataEntry();
            this.JobIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.JobCodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.JobDescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.JobOrderSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.DefaultWorkUnitsSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.JobDisabledCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.AssetTypeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp03019EPAssetTypesListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAssetTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetTypeOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ItemForJobID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForAssetTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobOrder = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDefaultWorkUnits = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobDisabled = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp03019_EP_Asset_Types_List_With_BlankTableAdapter = new WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03019_EP_Asset_Types_List_With_BlankTableAdapter();
            this.sp03040_EP_Asset_Jobs_Master_EditTableAdapter = new WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03040_EP_Asset_Jobs_Master_EditTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sp03040EPAssetJobsMasterEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_EP_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobCodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobDescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobOrderSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DefaultWorkUnitsSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobDisabledCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AssetTypeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03019EPAssetTypesListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAssetTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDefaultWorkUnits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobDisabled)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 460);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 434);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(628, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 434);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // colAssetTypeID
            // 
            this.colAssetTypeID.Caption = "Asset Type ID";
            this.colAssetTypeID.FieldName = "AssetTypeID";
            this.colAssetTypeID.Name = "colAssetTypeID";
            this.colAssetTypeID.OptionsColumn.AllowEdit = false;
            this.colAssetTypeID.OptionsColumn.AllowFocus = false;
            this.colAssetTypeID.OptionsColumn.ReadOnly = true;
            this.colAssetTypeID.Width = 88;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(628, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 460);
            this.barDockControl2.Size = new System.Drawing.Size(628, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 434);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(628, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 434);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Delete_16x16");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.JobIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.JobCodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.JobDescriptionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.JobOrderSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.DefaultWorkUnitsSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.JobDisabledCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.AssetTypeIDGridLookUpEdit);
            this.dataLayoutControl1.DataSource = this.sp03040EPAssetJobsMasterEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForJobID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(628, 434);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            this.dataLayoutControl1.Click += new System.EventHandler(this.dataLayoutControl1_Click);
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp03040EPAssetJobsMasterEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(116, 7);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(196, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 7;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // sp03040EPAssetJobsMasterEditBindingSource
            // 
            this.sp03040EPAssetJobsMasterEditBindingSource.DataMember = "sp03040_EP_Asset_Jobs_Master_Edit";
            this.sp03040EPAssetJobsMasterEditBindingSource.DataSource = this.dataSet_EP_DataEntry;
            // 
            // dataSet_EP_DataEntry
            // 
            this.dataSet_EP_DataEntry.DataSetName = "DataSet_EP_DataEntry";
            this.dataSet_EP_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // JobIDTextEdit
            // 
            this.JobIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03040EPAssetJobsMasterEditBindingSource, "JobID", true));
            this.JobIDTextEdit.Location = new System.Drawing.Point(117, 30);
            this.JobIDTextEdit.MenuManager = this.barManager1;
            this.JobIDTextEdit.Name = "JobIDTextEdit";
            this.JobIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.JobIDTextEdit, true);
            this.JobIDTextEdit.Size = new System.Drawing.Size(504, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.JobIDTextEdit, optionsSpelling1);
            this.JobIDTextEdit.StyleController = this.dataLayoutControl1;
            this.JobIDTextEdit.TabIndex = 8;
            // 
            // JobCodeTextEdit
            // 
            this.JobCodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03040EPAssetJobsMasterEditBindingSource, "JobCode", true));
            this.JobCodeTextEdit.Location = new System.Drawing.Point(116, 56);
            this.JobCodeTextEdit.MenuManager = this.barManager1;
            this.JobCodeTextEdit.Name = "JobCodeTextEdit";
            this.JobCodeTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.JobCodeTextEdit, true);
            this.JobCodeTextEdit.Size = new System.Drawing.Size(505, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.JobCodeTextEdit, optionsSpelling2);
            this.JobCodeTextEdit.StyleController = this.dataLayoutControl1;
            this.JobCodeTextEdit.TabIndex = 10;
            // 
            // JobDescriptionTextEdit
            // 
            this.JobDescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03040EPAssetJobsMasterEditBindingSource, "JobDescription", true));
            this.JobDescriptionTextEdit.Location = new System.Drawing.Point(116, 80);
            this.JobDescriptionTextEdit.MenuManager = this.barManager1;
            this.JobDescriptionTextEdit.Name = "JobDescriptionTextEdit";
            this.JobDescriptionTextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.JobDescriptionTextEdit, true);
            this.JobDescriptionTextEdit.Size = new System.Drawing.Size(505, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.JobDescriptionTextEdit, optionsSpelling3);
            this.JobDescriptionTextEdit.StyleController = this.dataLayoutControl1;
            this.JobDescriptionTextEdit.TabIndex = 11;
            this.JobDescriptionTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.JobDescriptionTextEdit_Validating);
            // 
            // JobOrderSpinEdit
            // 
            this.JobOrderSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03040EPAssetJobsMasterEditBindingSource, "JobOrder", true));
            this.JobOrderSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.JobOrderSpinEdit.Location = new System.Drawing.Point(116, 104);
            this.JobOrderSpinEdit.MenuManager = this.barManager1;
            this.JobOrderSpinEdit.Name = "JobOrderSpinEdit";
            this.JobOrderSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.JobOrderSpinEdit.Properties.IsFloatValue = false;
            this.JobOrderSpinEdit.Properties.Mask.EditMask = "N00";
            this.JobOrderSpinEdit.Size = new System.Drawing.Size(505, 20);
            this.JobOrderSpinEdit.StyleController = this.dataLayoutControl1;
            this.JobOrderSpinEdit.TabIndex = 12;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03040EPAssetJobsMasterEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(31, 255);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(566, 138);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling4);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 13;
            // 
            // DefaultWorkUnitsSpinEdit
            // 
            this.DefaultWorkUnitsSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03040EPAssetJobsMasterEditBindingSource, "DefaultWorkUnits", true));
            this.DefaultWorkUnitsSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.DefaultWorkUnitsSpinEdit.Location = new System.Drawing.Point(116, 128);
            this.DefaultWorkUnitsSpinEdit.MenuManager = this.barManager1;
            this.DefaultWorkUnitsSpinEdit.Name = "DefaultWorkUnitsSpinEdit";
            this.DefaultWorkUnitsSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DefaultWorkUnitsSpinEdit.Properties.Mask.EditMask = "f2";
            this.DefaultWorkUnitsSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DefaultWorkUnitsSpinEdit.Size = new System.Drawing.Size(505, 20);
            this.DefaultWorkUnitsSpinEdit.StyleController = this.dataLayoutControl1;
            this.DefaultWorkUnitsSpinEdit.TabIndex = 14;
            // 
            // JobDisabledCheckEdit
            // 
            this.JobDisabledCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03040EPAssetJobsMasterEditBindingSource, "JobDisabled", true));
            this.JobDisabledCheckEdit.Location = new System.Drawing.Point(116, 152);
            this.JobDisabledCheckEdit.MenuManager = this.barManager1;
            this.JobDisabledCheckEdit.Name = "JobDisabledCheckEdit";
            this.JobDisabledCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.JobDisabledCheckEdit.Properties.ValueChecked = 1;
            this.JobDisabledCheckEdit.Properties.ValueUnchecked = 0;
            this.JobDisabledCheckEdit.Size = new System.Drawing.Size(505, 19);
            this.JobDisabledCheckEdit.StyleController = this.dataLayoutControl1;
            this.JobDisabledCheckEdit.TabIndex = 15;
            // 
            // AssetTypeIDGridLookUpEdit
            // 
            this.AssetTypeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03040EPAssetJobsMasterEditBindingSource, "AssetTypeID", true));
            this.AssetTypeIDGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.AssetTypeIDGridLookUpEdit.Location = new System.Drawing.Point(116, 30);
            this.AssetTypeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.AssetTypeIDGridLookUpEdit.Name = "AssetTypeIDGridLookUpEdit";
            this.AssetTypeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::WoodPlan5.Properties.Resources.Edit_16x16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Edit Underlying Data", "edit", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::WoodPlan5.Properties.Resources.Refresh2_16x16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Reload Underlying Data", "reload", null, true)});
            this.AssetTypeIDGridLookUpEdit.Properties.DataSource = this.sp03019EPAssetTypesListWithBlankBindingSource;
            this.AssetTypeIDGridLookUpEdit.Properties.DisplayMember = "AssetTypeDescription";
            this.AssetTypeIDGridLookUpEdit.Properties.NullText = "";
            this.AssetTypeIDGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.AssetTypeIDGridLookUpEdit.Properties.ValueMember = "AssetTypeID";
            this.AssetTypeIDGridLookUpEdit.Properties.View = this.gridLookUpEdit1View;
            this.AssetTypeIDGridLookUpEdit.Size = new System.Drawing.Size(505, 22);
            this.AssetTypeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.AssetTypeIDGridLookUpEdit.TabIndex = 9;
            this.AssetTypeIDGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.AssetTypeIDGridLookUpEdit_ButtonClick);
            this.AssetTypeIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.AssetTypeIDGridLookUpEdit_Validating);
            // 
            // sp03019EPAssetTypesListWithBlankBindingSource
            // 
            this.sp03019EPAssetTypesListWithBlankBindingSource.DataMember = "sp03019_EP_Asset_Types_List_With_Blank";
            this.sp03019EPAssetTypesListWithBlankBindingSource.DataSource = this.dataSet_EP_DataEntry;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAssetTypeDescription,
            this.colAssetTypeID,
            this.colAssetTypeOrder,
            this.colRemarks});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colAssetTypeID;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = "0";
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAssetTypeOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colAssetTypeDescription
            // 
            this.colAssetTypeDescription.Caption = "Asset Type Description";
            this.colAssetTypeDescription.FieldName = "AssetTypeDescription";
            this.colAssetTypeDescription.Name = "colAssetTypeDescription";
            this.colAssetTypeDescription.OptionsColumn.AllowEdit = false;
            this.colAssetTypeDescription.OptionsColumn.AllowFocus = false;
            this.colAssetTypeDescription.OptionsColumn.ReadOnly = true;
            this.colAssetTypeDescription.Visible = true;
            this.colAssetTypeDescription.VisibleIndex = 0;
            this.colAssetTypeDescription.Width = 291;
            // 
            // colAssetTypeOrder
            // 
            this.colAssetTypeOrder.Caption = "Order";
            this.colAssetTypeOrder.FieldName = "AssetTypeOrder";
            this.colAssetTypeOrder.Name = "colAssetTypeOrder";
            this.colAssetTypeOrder.OptionsColumn.AllowEdit = false;
            this.colAssetTypeOrder.OptionsColumn.AllowFocus = false;
            this.colAssetTypeOrder.OptionsColumn.ReadOnly = true;
            this.colAssetTypeOrder.Visible = true;
            this.colAssetTypeOrder.VisibleIndex = 1;
            this.colAssetTypeOrder.Width = 64;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.AllowFocus = false;
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 2;
            // 
            // ItemForJobID
            // 
            this.ItemForJobID.Control = this.JobIDTextEdit;
            this.ItemForJobID.CustomizationFormText = "Job ID";
            this.ItemForJobID.Location = new System.Drawing.Point(0, 0);
            this.ItemForJobID.Name = "ItemForJobID";
            this.ItemForJobID.Size = new System.Drawing.Size(618, 24);
            this.ItemForJobID.Text = "Job ID:";
            this.ItemForJobID.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Size = new System.Drawing.Size(628, 434);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.emptySpaceItem1,
            this.emptySpaceItem2});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(618, 23);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(109, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(200, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(109, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(109, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(109, 23);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(309, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(309, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.AllowDrawBackground = false;
            this.layoutControlGroup4.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.GroupBordersVisible = false;
            this.layoutControlGroup4.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForAssetTypeID,
            this.ItemForJobCode,
            this.ItemForJobDescription,
            this.ItemForJobOrder,
            this.emptySpaceItem3,
            this.layoutControlGroup3,
            this.ItemForDefaultWorkUnits,
            this.ItemForJobDisabled,
            this.emptySpaceItem4});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 23);
            this.layoutControlGroup4.Name = "item0";
            this.layoutControlGroup4.Size = new System.Drawing.Size(618, 401);
            // 
            // ItemForAssetTypeID
            // 
            this.ItemForAssetTypeID.Control = this.AssetTypeIDGridLookUpEdit;
            this.ItemForAssetTypeID.CustomizationFormText = "Asset Type ID";
            this.ItemForAssetTypeID.Location = new System.Drawing.Point(0, 0);
            this.ItemForAssetTypeID.Name = "ItemForAssetTypeID";
            this.ItemForAssetTypeID.Size = new System.Drawing.Size(618, 26);
            this.ItemForAssetTypeID.Text = "Linked To Asset Type:";
            this.ItemForAssetTypeID.TextSize = new System.Drawing.Size(106, 13);
            // 
            // ItemForJobCode
            // 
            this.ItemForJobCode.Control = this.JobCodeTextEdit;
            this.ItemForJobCode.CustomizationFormText = "Job Code";
            this.ItemForJobCode.Location = new System.Drawing.Point(0, 26);
            this.ItemForJobCode.Name = "ItemForJobCode";
            this.ItemForJobCode.Size = new System.Drawing.Size(618, 24);
            this.ItemForJobCode.Text = "Job Code:";
            this.ItemForJobCode.TextSize = new System.Drawing.Size(106, 13);
            // 
            // ItemForJobDescription
            // 
            this.ItemForJobDescription.Control = this.JobDescriptionTextEdit;
            this.ItemForJobDescription.CustomizationFormText = "Job Description";
            this.ItemForJobDescription.Location = new System.Drawing.Point(0, 50);
            this.ItemForJobDescription.Name = "ItemForJobDescription";
            this.ItemForJobDescription.Size = new System.Drawing.Size(618, 24);
            this.ItemForJobDescription.Text = "Job Description:";
            this.ItemForJobDescription.TextSize = new System.Drawing.Size(106, 13);
            // 
            // ItemForJobOrder
            // 
            this.ItemForJobOrder.Control = this.JobOrderSpinEdit;
            this.ItemForJobOrder.CustomizationFormText = "Job Order";
            this.ItemForJobOrder.Location = new System.Drawing.Point(0, 74);
            this.ItemForJobOrder.Name = "ItemForJobOrder";
            this.ItemForJobOrder.Size = new System.Drawing.Size(618, 24);
            this.ItemForJobOrder.Text = "Job Order:";
            this.ItemForJobOrder.TextSize = new System.Drawing.Size(106, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 145);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(618, 10);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Details";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 155);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(618, 236);
            this.layoutControlGroup3.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup5;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(594, 190);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup5});
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup5.CaptionImage")));
            this.layoutControlGroup5.CustomizationFormText = "Remarks";
            this.layoutControlGroup5.ExpandButtonVisible = true;
            this.layoutControlGroup5.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(570, 142);
            this.layoutControlGroup5.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(570, 142);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // ItemForDefaultWorkUnits
            // 
            this.ItemForDefaultWorkUnits.Control = this.DefaultWorkUnitsSpinEdit;
            this.ItemForDefaultWorkUnits.CustomizationFormText = "Default Work Units";
            this.ItemForDefaultWorkUnits.Location = new System.Drawing.Point(0, 98);
            this.ItemForDefaultWorkUnits.Name = "ItemForDefaultWorkUnits";
            this.ItemForDefaultWorkUnits.Size = new System.Drawing.Size(618, 24);
            this.ItemForDefaultWorkUnits.Text = "Default Work Units:";
            this.ItemForDefaultWorkUnits.TextSize = new System.Drawing.Size(106, 13);
            // 
            // ItemForJobDisabled
            // 
            this.ItemForJobDisabled.Control = this.JobDisabledCheckEdit;
            this.ItemForJobDisabled.CustomizationFormText = "Job Disabled";
            this.ItemForJobDisabled.Location = new System.Drawing.Point(0, 122);
            this.ItemForJobDisabled.Name = "ItemForJobDisabled";
            this.ItemForJobDisabled.Size = new System.Drawing.Size(618, 23);
            this.ItemForJobDisabled.Text = "Job Disabled:";
            this.ItemForJobDisabled.TextSize = new System.Drawing.Size(106, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 391);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(618, 10);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp03019_EP_Asset_Types_List_With_BlankTableAdapter
            // 
            this.sp03019_EP_Asset_Types_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp03040_EP_Asset_Jobs_Master_EditTableAdapter
            // 
            this.sp03040_EP_Asset_Jobs_Master_EditTableAdapter.ClearBeforeFill = true;
            // 
            // frm_EP_Asset_Type_Job_Master_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(628, 490);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_EP_Asset_Type_Job_Master_Edit";
            this.Text = "Edit Asset Job Master Type";
            this.Activated += new System.EventHandler(this.frm_EP_Asset_Type_Job_Master_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_EP_Asset_Type_Job_Master_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_EP_Asset_Type_Job_Master_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sp03040EPAssetJobsMasterEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_EP_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobCodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobDescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobOrderSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DefaultWorkUnitsSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobDisabledCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AssetTypeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03019EPAssetTypesListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAssetTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDefaultWorkUnits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobDisabled)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DataSet_EP_DataEntry dataSet_EP_DataEntry;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private System.Windows.Forms.BindingSource sp03019EPAssetTypesListWithBlankBindingSource;
        private WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03019_EP_Asset_Types_List_With_BlankTableAdapter sp03019_EP_Asset_Types_List_With_BlankTableAdapter;
        private DevExpress.XtraEditors.TextEdit JobIDTextEdit;
        private System.Windows.Forms.BindingSource sp03040EPAssetJobsMasterEditBindingSource;
        private DevExpress.XtraEditors.TextEdit JobCodeTextEdit;
        private DevExpress.XtraEditors.TextEdit JobDescriptionTextEdit;
        private DevExpress.XtraEditors.SpinEdit JobOrderSpinEdit;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraEditors.SpinEdit DefaultWorkUnitsSpinEdit;
        private DevExpress.XtraEditors.CheckEdit JobDisabledCheckEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAssetTypeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobCode;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobDescription;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobOrder;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDefaultWorkUnits;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobDisabled;
        private WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03040_EP_Asset_Jobs_Master_EditTableAdapter sp03040_EP_Asset_Jobs_Master_EditTableAdapter;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraEditors.GridLookUpEdit AssetTypeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetTypeOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.Utils.ImageCollection imageCollection1;
    }
}

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;

using WoodPlan5.Properties;
using BaseObjects;
using Utilities;  // Used by Datasets //

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;

namespace WoodPlan5
{
    public partial class frm_EP_Action_Edit : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        public int intLinkedToAssetID = 0;
        public string strLinkedAssetDescription = "";
        public int intLinkedToAssetTypeID = 0;
        public int intLinkedToInspectionID = 0;
        public string strLinkedInspectionDescription = "";
        public int intLinkedSiteID = 0;

        Boolean ibool_ignoreValidation = false;

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;
        private string strDefaultPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //

        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        private int i_int_FocusedGrid = 1;
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //
        string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        private string i_strLastUsedSequencePrefix = "";
        #endregion

        public frm_EP_Action_Edit()
        {
            InitializeComponent();
        }

        private void frm_EP_Action_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 30041;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            sp03041_EP_Asset_Master_Jobs_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp03041_EP_Asset_Master_Jobs_With_BlankTableAdapter.Fill(this.dataSet_EP_DataEntry.sp03041_EP_Asset_Master_Jobs_With_Blank, "");
            
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp00220_Linked_Documents_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState1 = new RefreshGridState(gridView1, "LinkedDocumentID");
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesLinkedDocumentsPath").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Linked Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Populate Main Dataset //
            sp03036_EP_Asset_Action_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                case "blockadd":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_EP_DataEntry.sp03036_EP_Asset_Action_Edit.NewRow();
                        drNewRow["strMode"] = "add";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["AssetID"] = intLinkedToAssetID;
                        drNewRow["AssetDescription"] = strLinkedAssetDescription;
                        drNewRow["AssetTypeID"] = intLinkedToAssetTypeID;
                        if (this.GlobalSettings.CurrentSiteInspectionID == 0)
                        {
                            if (intLinkedToInspectionID > 0)
                            {
                                drNewRow["InspectionID"] = intLinkedToInspectionID;
                                drNewRow["InspectionDescription"] = strLinkedInspectionDescription;
                            }
                        }
                        else
                        {
                            drNewRow["InspectionID"] = this.GlobalSettings.CurrentSiteInspectionID;
                            drNewRow["InspectionDescription"] = this.GlobalSettings.CurrentSiteInspectionDescription;
                        }
                        drNewRow["AssetSiteID"] = intLinkedSiteID;
                        this.dataSet_EP_DataEntry.sp03036_EP_Asset_Action_Edit.Rows.Add(drNewRow);
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_EP_DataEntry.sp03036_EP_Asset_Action_Edit.NewRow();
                        drNewRow["strMode"] = "blockedit";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        this.dataSet_EP_DataEntry.sp03036_EP_Asset_Action_Edit.Rows.Add(drNewRow);
                        this.dataSet_EP_DataEntry.sp03036_EP_Asset_Action_Edit.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                    try
                    {
                        sp03036_EP_Asset_Action_EditTableAdapter.Fill(this.dataSet_EP_DataEntry.sp03036_EP_Asset_Action_Edit, strRecordIDs, strFormMode);
                    }
                    catch (Exception)
                    {
                    }
                    break;
            }
            dataLayoutControl1.EndUpdate();

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event //
        }


        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                //if (item2.Name == "intLocalityIDGridLookUpEdit" || item2.Name == "intOwnershipIDGridLookUpEdit" || item2.Name == "DistrictNameTextEdit") continue; // These 3 filed ingnore and Generic_EditChanged code also copied into their Edit_Changed event - previous weird bug on block editing, had to select locality twice before it would be accepted! //
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //

            if (this.dataSet_EP_DataEntry.sp03036_EP_Asset_Action_Edit.Rows.Count == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Asset Action", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                // Following 3 lines allow form to kill the validation to allow it to close //
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        AssetDescriptionButtonEdit.Focus();

                        AssetDescriptionButtonEdit.Properties.Buttons[0].Enabled = true;
                        AssetDescriptionButtonEdit.Properties.Buttons[1].Enabled = true;

                        ActionNumberButtonEdit.Properties.ReadOnly = false;
                        ActionNumberButtonEdit.Properties.Buttons[0].Enabled = true;
                        ActionNumberButtonEdit.Properties.Buttons[1].Enabled = true;

                        InspectionDescriptionButtonEdit.Properties.Buttons[0].Enabled = true;
                        InspectionDescriptionButtonEdit.Properties.Buttons[1].Enabled = true;
                        gridControl1.Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        AssetDescriptionButtonEdit.Focus();

                        AssetDescriptionButtonEdit.Properties.Buttons[0].Enabled = false;
                        AssetDescriptionButtonEdit.Properties.Buttons[1].Enabled = false;

                        ActionNumberButtonEdit.Properties.ReadOnly = true;
                        ActionNumberButtonEdit.Properties.Buttons[0].Enabled = true;
                        ActionNumberButtonEdit.Properties.Buttons[1].Enabled = false;

                        InspectionDescriptionButtonEdit.Properties.Buttons[0].Enabled = false;
                        InspectionDescriptionButtonEdit.Properties.Buttons[1].Enabled = false;
                        gridControl1.Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        ActionNumberButtonEdit.Focus();

                        AssetDescriptionButtonEdit.Properties.Buttons[0].Enabled = true;
                        AssetDescriptionButtonEdit.Properties.Buttons[1].Enabled = true;

                        ActionNumberButtonEdit.Properties.ReadOnly = false;
                        ActionNumberButtonEdit.Properties.Buttons[0].Enabled = true;
                        ActionNumberButtonEdit.Properties.Buttons[1].Enabled = true;

                        InspectionDescriptionButtonEdit.Properties.Buttons[0].Enabled = true;
                        InspectionDescriptionButtonEdit.Properties.Buttons[1].Enabled = true;
                        gridControl1.Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        ActionNumberButtonEdit.Focus();

                        AssetDescriptionButtonEdit.Properties.Buttons[0].Enabled = true;
                        AssetDescriptionButtonEdit.Properties.Buttons[1].Enabled = true;

                        ActionNumberButtonEdit.Properties.ReadOnly = true;
                        ActionNumberButtonEdit.Properties.Buttons[0].Enabled = true;
                        ActionNumberButtonEdit.Properties.Buttons[1].Enabled = false;

                        InspectionDescriptionButtonEdit.Properties.Buttons[0].Enabled = true;
                        InspectionDescriptionButtonEdit.Properties.Buttons[1].Enabled = true;
                        gridControl1.Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //
        }

        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_EP_DataEntry.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            int intActionID = 0;  // Check if the current record has been saved at some point //
            DataRowView currentRow = (DataRowView)sp03036EPAssetActionEditBindingSource.Current;
            if (currentRow != null) intActionID = (currentRow["ActionID"] == null ? 0 : Convert.ToInt32(currentRow["ActionID"]));

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView1 navigator custom buttons //
            GridView view = null;
            int[] intRowHandles;
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();
            int intRecordID = 0;  // Check if the current record has been saved at some point //
            if (currentRow != null) intRecordID = (currentRow["ActionID"] == null ? 0 : Convert.ToInt32(currentRow["ActionID"]));

            if (iBool_AllowAdd && intRecordID > 0)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEdit)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }
        }

        public void UpdateFormRefreshStatus(int status, string strNewIDs1)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
        }

        public void External_Call_For_Immediate_Refresh_Linked_Records(string strNewIDs1)
        {
            // Triggered by main MDI Form's Site Inspection Control Panel //
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
            LoadLinkedRecords();
        }


        private void frm_EP_Action_Edit_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                LoadLinkedRecords();
            }
            SetMenuStatus();
        }

        private void frm_EP_Action_Edit_Deactivate(object sender, EventArgs e)
        {
            /*arraylistFloatingPanels = new ArrayList();
            foreach (DevExpress.XtraBars.Docking.DockPanel dp in dockManager1.Panels)
            {
                if (dp.Dock == DevExpress.XtraBars.Docking.DockingStyle.Float && dp.Visibility == DevExpress.XtraBars.Docking.DockVisibility.Visible)
                {
                    arraylistFloatingPanels.Add(dp);
                    dp.Hide();
                }
            }*/

        }

        private void frm_EP_Action_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Detach Validating Event from all Editors to track changes...  Attached on Form Load Event //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            this.sp03036EPAssetActionEditBindingSource.EndEdit();
            try
            {
                this.sp03036_EP_Asset_Action_EditTableAdapter.Update(dataSet_EP_DataEntry);  // Insert and Update queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            Broadcast_Data_Refresh Broadcast = new Broadcast_Data_Refresh();
            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlighted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)sp03036EPAssetActionEditBindingSource.Current;
                if (currentRow != null) 
                {
                    strNewIDs = Convert.ToString(currentRow["ActionID"]) + ";";
                    // Switch mode to Edit so than any subsequent changes update this record //
                    this.strFormMode = "edit";
                    currentRow["strMode"] = "edit";
                    currentRow.Row.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                }
                gridControl1.Enabled = true;
            }
            else if (this.strFormMode.ToLower() == "blockadd")
            {
                DataRowView currentRow = (DataRowView)sp03036EPAssetActionEditBindingSource.Current;
                if (currentRow != null)
                {
                    strNewIDs = Convert.ToString(currentRow["strRecordIDs"]);  // Field originally held district IDs, but now holds new record linked document ids (changed via Update SP) //
                    currentRow["strMode"] = "blockedit";
                    currentRow.Row.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                }
            }

            // Notify any open forms which reference this data that they will need to refresh their data on activating //
            Broadcast.Asset_Action_Refresh(this.ParentForm, this.Name, strNewIDs, "");
            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }


        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;

            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
            for (int i = 0; i < this.dataSet_EP_DataEntry.sp03036_EP_Asset_Action_Edit.Rows.Count; i++)
            {
                switch (this.dataSet_EP_DataEntry.sp03036_EP_Asset_Action_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void LoadLinkedRecords()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;

            string strRecordIDs = "";
            DataRowView currentRow = (DataRowView)sp03036EPAssetActionEditBindingSource.Current;
            if (currentRow != null)
            {
                strRecordIDs = (currentRow["ActionID"] == null ? "" : currentRow["ActionID"].ToString() + ",");
            }

            // Linked Inspections //
            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
            gridControl1.BeginUpdate();
            sp00220_Linked_Documents_ListTableAdapter.Fill(dataSet_AT.sp00220_Linked_Documents_List, (string.IsNullOrEmpty(strRecordIDs) ? "" : strRecordIDs), 11, strDefaultPath);
            this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl1.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)gridControl1.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["LinkedDocumentID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            if (this.strFormMode != "blockedit")
            {
                LoadLinkedRecords();
                GridView view = (GridView)gridControl1.MainView;
                view.ExpandAllGroups();
            }
        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void AssetDescriptionButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                frm_EP_Select_Asset fChildForm = new frm_EP_Select_Asset();
                fChildForm.GlobalSettings = this.GlobalSettings;
                if (fChildForm.ShowDialog() == DialogResult.Yes)  // User Clicked OK on child Form //
                {
                    DataRowView currentRow = (DataRowView)sp03036EPAssetActionEditBindingSource.Current;
                    if (currentRow != null)
                    {
                        currentRow["AssetID"] = fChildForm.intSelectedID;
                        currentRow["AssetDescription"] = fChildForm.strSelectedValue;
                        currentRow["AssetTypeID"] = fChildForm.intSelectedTypeID;
                        currentRow["AssetSiteID"] = fChildForm.intSelectedSiteID;
                        currentRow["JobTypeID"] = 0;
                        //if (this.strFormMode != "blockedit" && this.strFormMode != "blockadd") LoadPriorActionRecords();
                    }
                }
            }
            else if (e.Button.Tag.ToString() == "view")  // View Inspection Button //
            {
                DataRowView currentRow = (DataRowView)sp03036EPAssetActionEditBindingSource.Current;
                if (currentRow != null)
                {
                    int intAssetID = (string.IsNullOrEmpty(currentRow["AssetID"].ToString()) ? 0 : Convert.ToInt32(currentRow["AssetID"]));
                    if (intAssetID <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to display parent asset - no asset set as parent.", "View Parent Asset", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    fProgress = new frmProgress(10);
                    this.AddOwnedForm(fProgress);
                    fProgress.Show();  // ***** Closed in PostOpen event ***** //
                    Application.DoEvents();

                    frm_EP_Asset_Edit fChildForm = new frm_EP_Asset_Edit();
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strRecordIDs = intAssetID.ToString() + ",";
                    fChildForm.strFormMode = "edit";
                    fChildForm.strCaller = "frm_EP_Action_Edit";
                    fChildForm.intRecordCount = 1;
                    fChildForm.FormPermissions = this.FormPermissions;
                    fChildForm.fProgress = fProgress;
                    fChildForm.Show();

                    System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm, new object[] { null });
                }
            }
        }

        private void AssetDescriptionButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if ((this.strFormMode == "edit" || this.strFormMode == "add") && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(AssetDescriptionButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(AssetDescriptionButtonEdit, "");
            }
        }

        private void JobTypeIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(glue.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(JobTypeIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(JobTypeIDGridLookUpEdit, "");
            }
        }

        private void ActionNumberButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            string strTableName = "EP_Action";
            string strFieldName = "ActionNumber";
            if (e.Button.Tag.ToString() == "Sequence")  // Sequence Button //
            {
                int intClientID = 0;
                int intSiteID = 0;

                // Get Parent Client and Site ID //
                DataRowView currentRow = (DataRowView)sp03036EPAssetActionEditBindingSource.Current;
                if (currentRow == null) return;
                int intAssetID = (string.IsNullOrEmpty(currentRow["AssetID"].ToString()) ? 0 : Convert.ToInt32(currentRow["AssetID"]));
                try
                {
                    DataSet_EP_DataEntryTableAdapters.QueriesTableAdapter GetClientAndSiteIDs = new DataSet_EP_DataEntryTableAdapters.QueriesTableAdapter();
                    GetClientAndSiteIDs.ChangeConnectionString(strConnectionString);
                    string strReturnedValue = GetClientAndSiteIDs.sp03078_EP_Get_Site_And_Client_IDs_From_Child_ID(intAssetID, "Action").ToString();

                    char[] delimiters = new char[] { '|' };
                    string[] strArray = strReturnedValue.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    if (strArray.Length != 2) return;
                    intClientID = Convert.ToInt32(strArray[0]);
                    intSiteID = Convert.ToInt32(strArray[1]);
                }
                catch (Exception) 
                {
                    return;
                }

                frm_Core_Sequence_Selection fChildForm = new frm_Core_Sequence_Selection();
                fChildForm.PassedInTableName = strTableName;
                fChildForm.PassedInFieldName = strFieldName;
                fChildForm.PassedInClientID = intClientID;
                fChildForm.PassedInSiteID = intSiteID;
                this.ParentForm.AddOwnedForm(fChildForm);
                fChildForm.GlobalSettings = this.GlobalSettings;

                if (fChildForm.ShowDialog() == DialogResult.OK)
                {
                    ActionNumberButtonEdit.EditValue = fChildForm.SelectedSequence;
                    i_strLastUsedSequencePrefix = fChildForm.SelectedSequence;  // Store Sequence Prefix for saving against Last Saved Record //
                }
            }
            else if (e.Button.Tag.ToString() == "Number")
            {
                // Get next value in sequence //
                string strSequence = ActionNumberButtonEdit.EditValue.ToString() ?? "";

                SequenceNumberGetNext sequence = new SequenceNumberGetNext();
                string strNextNumber = sequence.GetNextNumberInSequence(strConnectionString, strFieldName, strTableName, strSequence);  // Calculate Next in Sequence //

                if (strNextNumber == "")
                {
                    return;
                }
                if (strSequence.StartsWith("?")) strSequence = strSequence.Remove(0, 2);  // Strip off '?x' when X was the numeric length of sequence to generate [Locality Code as Seed] //

                int intRequiredLength = strNextNumber.Length;
                int intNextNumber = Convert.ToInt32(strNextNumber);
                int intIncrement = 0;
                string strTempNumber = "";

                // Check if value is already present within dataset //
                string strCurrentActionID = ActionIDTextEdit.EditValue.ToString();
                if (string.IsNullOrEmpty(strCurrentActionID)) strCurrentActionID = "";
                bool boolUniqueValueFound = false;
                bool boolDuplicateFound = false;
                do
                {
                    boolDuplicateFound = false;
                    strTempNumber = Convert.ToString(intNextNumber + intIncrement);
                    if (strTempNumber.Length > intRequiredLength)  // Abort as the calculated number + sequence will be too big to fit in the field //
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to calculate the next sequence number!\n\nThe length of the sequence plus the calculated sequence number will exceed the length of the field (20 characters).Either reduce the length of the first part of the number or adjust the number of numeric places that the sequence number can span from the Sequence Manager screen.", "Get Next Sequence Number", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    else
                    {
                        strTempNumber = strTempNumber.PadLeft(intRequiredLength, '0');
                    }
                    foreach (DataRow dr in this.dataSet_EP_DataEntry.sp03036_EP_Asset_Action_Edit.Rows)
                    {
                        if (dr["ActionNumber"].ToString() == (strSequence + strTempNumber))
                        {
                            intIncrement++;
                            boolDuplicateFound = true;
                            break;
                        }
                    }
                    if (!boolDuplicateFound) boolUniqueValueFound = true;  // Exit Loop //
                } while (!boolUniqueValueFound);
                ActionNumberButtonEdit.EditValue = strSequence + strTempNumber;
            }
        }

        private void ActionNumberButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(ActionNumberButtonEdit, "Select\\enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(ActionNumberButtonEdit, "");
            }
        }

        private void InspectionDescriptionButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                frm_EP_Select_Inspection fChildForm = new frm_EP_Select_Inspection();
                fChildForm.GlobalSettings = this.GlobalSettings;
                this.sp03036EPAssetActionEditBindingSource.EndEdit();
                DataRowView currentRow = (DataRowView)sp03036EPAssetActionEditBindingSource.Current;
                if (currentRow != null)
                {
                    fChildForm.strPassedInSites = (string.IsNullOrEmpty(currentRow["AssetSiteID"].ToString()) || currentRow["AssetSiteID"].ToString() == "0" ? "" : currentRow["AssetSiteID"].ToString() + ",");
                    fChildForm.intOriginalInspectionID = (String.IsNullOrEmpty(currentRow["InspectionID"].ToString()) ? 0 : Convert.ToInt32(currentRow["InspectionID"]));
                }
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                {
                    if (currentRow != null)
                    {
                        currentRow["InspectionID"] = fChildForm.intSelectedInspectionID;
                        currentRow["InspectionDescription"] = fChildForm.strSelectedInspectionNumber;
                        this.sp03036EPAssetActionEditBindingSource.EndEdit();
                    }
                }
            }
            else if (e.Button.Tag.ToString() == "view")  // View Inspection Button //
            {
                DataRowView currentRow = (DataRowView)sp03036EPAssetActionEditBindingSource.Current;
                if (currentRow != null)
                {
                    int intInspectionID = (string.IsNullOrEmpty(currentRow["InspectionID"].ToString()) ? 0 : Convert.ToInt32(currentRow["InspectionID"]));
                    if (intInspectionID <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to display parent inspection - no inspection set as parent.", "View Parent Inspection", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                     fProgress = new frmProgress(10);
                     this.AddOwnedForm(fProgress);
                     fProgress.Show();  // ***** Closed in PostOpen event ***** //
                     Application.DoEvents();

                     frm_EP_Site_Inspection_Edit fChildForm = new frm_EP_Site_Inspection_Edit();
                     fChildForm.MdiParent = this.MdiParent;
                     fChildForm.GlobalSettings = this.GlobalSettings;
                     fChildForm.strRecordIDs = intInspectionID.ToString() + ",";
                     fChildForm.strFormMode = "edit";
                     fChildForm.strCaller = "frm_EP_Action_Edit";
                     fChildForm.intRecordCount = 1;
                     fChildForm.FormPermissions = this.FormPermissions;
                     fChildForm.fProgress = fProgress;
                     fChildForm.Show();

                     System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                     if (method != null) method.Invoke(fChildForm, new object[] { null });
                }
            }
            else if (e.Button.Tag.ToString() == "clear")  // Clear Inspection Button //
            {
                DataRowView currentRow = (DataRowView)sp03036EPAssetActionEditBindingSource.Current;
                if (currentRow != null)
                {
                    if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the selected Inspection.\n\nAre you sure you wish to proceed?", ItemForInspectionDescription.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        currentRow["InspectionID"] = 0;
                        currentRow["InspectionDescription"] = "";
                    }
                }
            }
        }

        private void JobTypeIDGridLookUpEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 9060, "Asset Types");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp03041_EP_Asset_Master_Jobs_With_BlankTableAdapter.Fill(this.dataSet_EP_DataEntry.sp03041_EP_Asset_Master_Jobs_With_Blank, "");
                }
            }
        }

        private void JobTypeIDGridLookUpEdit_Enter(object sender, EventArgs e)
        {
            string strAssetTypeID = "";
            this.sp03036EPAssetActionEditBindingSource.EndEdit();
            DataRowView currentRow = (DataRowView)sp03036EPAssetActionEditBindingSource.Current;
            if (currentRow != null)
            {
                strAssetTypeID = (string.IsNullOrEmpty(currentRow["AssetTypeID"].ToString()) ? "0" : currentRow["AssetTypeID"].ToString());
            }
            if (string.IsNullOrEmpty(strAssetTypeID) || strAssetTypeID == "0")
            {
                return;
            }
            else
            {
                GridLookUpEdit glue = (GridLookUpEdit)sender;
                GridView view = glue.Properties.View;
                view.BeginUpdate();
                view.ActiveFilter.Clear();
                view.ActiveFilter.NonColumnFilter = "[AssetTypeID] = " + strAssetTypeID + " or [AssetTypeID] = 0";
                view.MakeRowVisible(-1, true);
                view.EndUpdate();
            }
        }

        private void JobTypeIDGridLookUpEdit_Leave(object sender, EventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.EndUpdate();
        }

        private void ActionDueDateDateEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nAre you sure you wish to proceed?", ItemForActionDueDate.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    ActionDueDateDateEdit.EditValue = null;
                }
            }
        }

        private void ActionDoneDateDateEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nAre you sure you wish to proceed?", ItemForActionDoneDate.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    ActionDoneDateDateEdit.EditValue = null;
                }
            }

        }


        #endregion


        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (view.RowCount != 0) return;
            string message = "";
            DataRowView currentRow = (DataRowView)sp03036EPAssetActionEditBindingSource.Current;
            if (currentRow != null)
            {
                message = (String.IsNullOrEmpty(currentRow["ActionID"].ToString()) || currentRow["ActionID"].ToString() == "0" ? "No Linked Documents - Save New Site Asset Action Before Adding Link Documents" : "No Linked Documents");
            }
            else message = "No Linked Documents";
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            SetMenuStatus();
        }

        private void gridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void gridView1_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void repositoryItemHyperLinkEdit1_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "DocumentPath").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Document Linked - unable to proceed.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(strFile);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view file: " + strFile + ".\n\nThe file may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit_Record();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void Add_Record()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Linked Documents //
                    {
                        if (!iBool_AllowAdd) return;

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        frm_AT_Linked_Document_Edit fChildForm2 = new frm_AT_Linked_Document_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = "";
                        fChildForm2.strFormMode = "add";
                        fChildForm2.strCaller = "frm_EP_Action_Edit";
                        fChildForm2.intRecordCount = 0;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.intRecordTypeID = 11;  // Asset Action //

                        DataRowView currentRow = (DataRowView)sp03036EPAssetActionEditBindingSource.Current;
                        if (currentRow != null)
                        {
                            fChildForm2.intLinkedToRecordID = (currentRow["ActionID"] == null ? 0 : Convert.ToInt32(currentRow["ActionID"]));
                            string strParentDesc = currentRow["ActionNumber"].ToString();
                            if (string.IsNullOrEmpty(strParentDesc)) strParentDesc = "Action [No Number]";
                            fChildForm2.strLinkedToRecordDesc = strParentDesc;
                        }
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                        break;
                    }
                default:
                    break;
            }
        }

        private void Block_Edit_Record()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Linked Documents //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedDocumentID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        frm_AT_Linked_Document_Edit fChildForm = new frm_AT_Linked_Document_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = "frm_EP_Action_Edit";
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                default:
                    break;
            }
        }

        private void Edit_Record()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Linked Documents //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedDocumentID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        frm_AT_Linked_Document_Edit fChildForm = new frm_AT_Linked_Document_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = "frm_EP_Action_Edit";
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            switch (i_int_FocusedGrid)
            {
                case 1:  // Linked Documents //
                    if (!iBool_AllowDelete) return;
                    view = (GridView)gridControl1.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Site Linked Documents to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    // Checks passed so delete selected record(s) //
                    strMessage = "You have " + (intCount == 1 ? "1 Linked Document" : Convert.ToString(intRowHandles.Length) + " Linked Documents") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Linked Document" : "these Linked Documents") + " will no longer be available for selection but the files(s) will still exist on the computer!";
                    if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        frmProgress fProgress = new frmProgress(20);
                        fProgress.UpdateCaption("Deleting...");
                        fProgress.Show();
                        Application.DoEvents();

                        string strRecordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedDocumentID")) + ",";
                        }
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                        
                        DataSet_ATTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_ATTableAdapters.QueriesTableAdapter();
                        RemoveRecords.ChangeConnectionString(strConnectionString);
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                        RemoveRecords.sp00223_Linked_Document_Delete(strRecordIDs);  // Remove the records from the DB in one go //
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                        LoadLinkedRecords();

                        if (fProgress != null)
                        {
                            fProgress.UpdateProgress(20); // Update Progress Bar //
                            fProgress.Close();
                            fProgress = null;
                        }

                        // Notify any open forms which reference this data that they will need to refresh their data on activating //
                        Broadcast_Data_Refresh Broadcast = new Broadcast_Data_Refresh();
                        Broadcast.LinkedDocument_Refresh(this.ParentForm, this.Name, "");

                        if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                default:
                    break;
            }
        }

        private void bbiShowMapButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // force changes back to dataset //
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            DataRowView currentRow = (DataRowView)sp03036EPAssetActionEditBindingSource.Current;
            if (currentRow == null) return;
            if (String.IsNullOrEmpty(currentRow["AssetID"].ToString()) || currentRow["AssetID"].ToString() == "0")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Save the current record before attempting to display it on the map.", "Show Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strSelectedIDs = currentRow["AssetID"].ToString() + ',';
            try
            {
                Mapping_Functions MapFunctions = new Mapping_Functions();
                MapFunctions.Show_Map_From_Assets_Screen(this.GlobalSettings, strConnectionString, this, strSelectedIDs, "asset");
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to open the map [" + ex.Message + "].\n\nTry opening the map again. If the problem persists contact Technical Support.", "Show Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
        }






    }
}


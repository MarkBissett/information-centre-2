using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraScheduler;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_EP_Scheduler_Add_Jobs : WoodPlan5.frmBase_Modal
    {

        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        GridHitInfo downHitInfo = null;

        BaseObjects.GridCheckMarksSelection selection2;
        BaseObjects.GridCheckMarksSelection selection3;
        BaseObjects.GridCheckMarksSelection selection5;

        private string i_str_selected_asset_type_ids = "";
        private string i_str_selected_asset_type_names = "";
        private string i_str_selected_job_type_ids = "";
        private string i_str_selected_job_type_names = "";
        private string i_str_selected_client_ids = "";
        private string i_str_selected_client_names = "";
        private string i_str_selected_site_ids = "";
        private string i_str_selected_site_names = "";
        private string i_str_selected_job_type_ids2 = "";
        private string i_str_selected_job_type_names2 = "";

        private string i_str_selected_client_ids2 = "";
        private string i_str_selected_client_names2 = "";
        private string i_str_selected_site_ids2 = "";
        private string i_str_selected_site_names2 = "";

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState2;  // Used by Grid View State Facilities //

        public SchedulerStorage PassedInSchedulerStorage = null;
        public int i_intMappingShowAmenityTreeObjects = 0;  // Controls If the Amenity Tree Actions page is enabled //
        public int i_intMappingShowEstatePlanObjects = 0;   // Controls If the Asset Actions page is enabled // 
        public int SelectedActionID = 0;
        public int SelectedActionTypeID = 0;

        #endregion

        public frm_EP_Scheduler_Add_Jobs()
        {
            InitializeComponent();
        }

        private void frm_EP_Scheduler_Add_Jobs_Load(object sender, EventArgs e)
        {
            this.LockThisWindow();
            this.FormID = 30071;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            if (i_intMappingShowEstatePlanObjects == 0 && i_intMappingShowAmenityTreeObjects == 0)
            {
                i_intMappingShowEstatePlanObjects = 1; // Both pages are disabled for some reason so enable Asset Actions page as we need a page enabled to drag actions from //
            }

            sp03021_EP_Asset_Type_Filter_DropDownTableAdapter.Connection.ConnectionString = strConnectionString;
            this.sp03021_EP_Asset_Type_Filter_DropDownTableAdapter.Fill(this.dataSet_EP.sp03021_EP_Asset_Type_Filter_DropDown, "");
            gridControl5.ForceInitialize();

            sp03038_EP_Asset_Jobs_MasterTableAdapter.Connection.ConnectionString = strConnectionString;
            this.sp03038_EP_Asset_Jobs_MasterTableAdapter.Fill(this.dataSet_EP.sp03038_EP_Asset_Jobs_Master, "");
            gridControl4.ForceInitialize();

            sp03086_EP_Scheduler_AT_Master_Job_TypesTableAdapter.Connection.ConnectionString = strConnectionString;
            sp03086_EP_Scheduler_AT_Master_Job_TypesTableAdapter.Fill(this.dataSet_Scheduler.sp03086_EP_Scheduler_AT_Master_Job_Types);
            gridControl7.ForceInitialize();

            sp03085_EP_Scheduler_EP_Jobs_Available_For_AddingTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState1 = new RefreshGridState(gridView1, "ActionID");

            sp03087_EP_Scheduler_AT_Jobs_Available_For_AddingTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState2 = new RefreshGridState(gridView8, "ActionID");

            dateEditFromDate.DateTime = this.GlobalSettings.ViewedStartDate;
            dateEditToDate.DateTime = this.GlobalSettings.ViewedEndDate;
            dateEditFromDate2.DateTime = this.GlobalSettings.ViewedStartDate;
            dateEditToDate2.DateTime = this.GlobalSettings.ViewedEndDate;

            // Add record selection checkboxes to popup grid control //
            selection2 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl5.MainView);
            selection2.CheckMarkColumn.VisibleIndex = 0;
            selection2.CheckMarkColumn.Width = 30;

            // Add record selection checkboxes to popup grid control //
            selection3 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl4.MainView);
            selection3.CheckMarkColumn.VisibleIndex = 0;
            selection3.CheckMarkColumn.Width = 30;

            // Add record selection checkboxes to popup grid control //
            selection5 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl7.MainView);
            selection5.CheckMarkColumn.VisibleIndex = 0;
            selection5.CheckMarkColumn.Width = 30;

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            _KeepWaitFormOpen = false;
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();

            LoadLastSavedUserScreenSettings();
            ConfigureScreen();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureScreen();
        }

        private void ConfigureScreen()
        {
            xtraTabPage1.PageEnabled = (i_intMappingShowEstatePlanObjects == 0 ? false : true);
            xtraTabPage2.PageEnabled = (i_intMappingShowAmenityTreeObjects == 0 ? false : true);
            xtraTabControl1.SelectedTabPageIndex = (i_intMappingShowEstatePlanObjects == 1 ? 0 : 1);
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                // From Date //
                string strFromDate = default_screen_settings.RetrieveSetting("FromDate");
                if (!string.IsNullOrEmpty(strFromDate)) dateEditFromDate.DateTime = Convert.ToDateTime(strFromDate);

                // To Date //
                string strToDate = default_screen_settings.RetrieveSetting("ToDate");
                if (!string.IsNullOrEmpty(strToDate)) dateEditToDate.DateTime = Convert.ToDateTime(strToDate);


                // Asset Type Filter //
                int intFoundRow = 0;
                string strAssetTypeFilter = default_screen_settings.RetrieveSetting("AssetTypeFilter");
                if (!string.IsNullOrEmpty(strAssetTypeFilter))
                {
                    Array arrayAssetTypes = strAssetTypeFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewAssetTypes = (GridView)gridControl5.MainView;
                    viewAssetTypes.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayAssetTypes)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewAssetTypes.LocateByValue(0, viewAssetTypes.Columns["AssetTypeID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewAssetTypes.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewAssetTypes.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewAssetTypes.EndUpdate();
                    popupContainerEdit2.Text = PopupContainerEdit2_Get_Selected();
                }

                // Job Type Filter //
                intFoundRow = 0;
                string strJobTypeFilter = default_screen_settings.RetrieveSetting("JobTypeFilter");
                if (!string.IsNullOrEmpty(strJobTypeFilter))
                {
                    Array arrayJobTypes = strJobTypeFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewJobTypes = (GridView)gridControl4.MainView;
                    viewJobTypes.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayJobTypes)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewJobTypes.LocateByValue(0, viewJobTypes.Columns["JobID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewJobTypes.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewJobTypes.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewJobTypes.EndUpdate();
                    popupContainerEdit3.Text = PopupContainerEdit3_Get_Selected();
                }

                // From Date 2 //
                string strFromDate2 = default_screen_settings.RetrieveSetting("FromDate2");
                if (!string.IsNullOrEmpty(strFromDate2)) dateEditFromDate2.DateTime = Convert.ToDateTime(strFromDate);

                // To Date 2 //
                string strToDate2 = default_screen_settings.RetrieveSetting("ToDate2");
                if (!string.IsNullOrEmpty(strToDate2)) dateEditToDate2.DateTime = Convert.ToDateTime(strToDate);


                // Client Filter //
                strAssetTypeFilter = default_screen_settings.RetrieveSetting("ClientFilter");
                if (!string.IsNullOrEmpty(strAssetTypeFilter))
                {
                    i_str_selected_client_ids = strAssetTypeFilter;
                    var ResolveIDs = new DataSet_GC_Summer_CoreTableAdapters.QueriesTableAdapter();
                    ResolveIDs.ChangeConnectionString(strConnectionString);
                    try
                    {
                        buttonEditClientFilter.Text = ResolveIDs.sp06023_OM_Get_Clients_From_ClientIDs(strAssetTypeFilter).ToString();
                        i_str_selected_client_names = buttonEditClientFilter.Text;
                    }
                    catch (Exception) { }
                }

                // Site Filter //
                strAssetTypeFilter = default_screen_settings.RetrieveSetting("SiteFilter2");
                if (!string.IsNullOrEmpty(strAssetTypeFilter))
                {
                    i_str_selected_site_ids = strAssetTypeFilter;
                    var ResolveIDs = new DataSet_GC_Summer_CoreTableAdapters.QueriesTableAdapter();
                    ResolveIDs.ChangeConnectionString(strConnectionString);
                    try
                    {
                        buttonEditSiteFilter.Text = ResolveIDs.sp06024_OM_Get_Sites_From_SiteIDs(strAssetTypeFilter).ToString();
                        i_str_selected_site_names = buttonEditSiteFilter.Text;
                    }
                    catch (Exception) { }
                }


                // Client Filter2 //
                strAssetTypeFilter = default_screen_settings.RetrieveSetting("ClientFilter2");
                if (!string.IsNullOrEmpty(strAssetTypeFilter))
                {
                    i_str_selected_client_ids2 = strAssetTypeFilter;
                    var ResolveIDs = new DataSet_GC_Summer_CoreTableAdapters.QueriesTableAdapter();
                    ResolveIDs.ChangeConnectionString(strConnectionString);
                    try
                    {
                        buttonEditClientFilter2.Text = ResolveIDs.sp06023_OM_Get_Clients_From_ClientIDs(strAssetTypeFilter).ToString();
                        i_str_selected_client_names2 = buttonEditClientFilter.Text;
                    }
                    catch (Exception) { }
                }

                // Site Filter //
                strAssetTypeFilter = default_screen_settings.RetrieveSetting("SiteFilter2");
                if (!string.IsNullOrEmpty(strAssetTypeFilter))
                {
                    i_str_selected_site_ids2 = strAssetTypeFilter;
                    var ResolveIDs = new DataSet_GC_Summer_CoreTableAdapters.QueriesTableAdapter();
                    ResolveIDs.ChangeConnectionString(strConnectionString);
                    try
                    {
                        buttonEditSiteFilter2.Text = ResolveIDs.sp06024_OM_Get_Sites_From_SiteIDs(strAssetTypeFilter).ToString();
                        i_str_selected_site_names2 = buttonEditSiteFilter.Text;
                    }
                    catch (Exception) { }
                }


                // Job Type 2 Filter //
                intFoundRow = 0;
                string strJobTypeFilter2 = default_screen_settings.RetrieveSetting("JobTypeFilter2");
                if (!string.IsNullOrEmpty(strJobTypeFilter2))
                {
                    Array arrayJobTypes2 = strJobTypeFilter2.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewJobTypes2 = (GridView)gridControl7.MainView;
                    viewJobTypes2.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayJobTypes2)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewJobTypes2.LocateByValue(0, viewJobTypes2.Columns["JobID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewJobTypes2.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewJobTypes2.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewJobTypes2.EndUpdate();
                    popupContainerEdit5.Text = PopupContainerEdit5_Get_Selected();
                }

            }
        }


        private void LoadData(string strLoadWhat)
        {
            if (!splashScreenManager.IsSplashFormVisible)
            {
                this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Loading Actions...");
            }
            if (strLoadWhat == "EP")
            {
                if (i_str_selected_site_ids == null) i_str_selected_site_ids = "";
                GridView view = (GridView)gridControl1.MainView;
                view.BeginUpdate();
                DateTime dtFromDate = dateEditFromDate.DateTime;
                DateTime dtToDate = dateEditToDate.DateTime;

                this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                sp03085_EP_Scheduler_EP_Jobs_Available_For_AddingTableAdapter.Fill(this.dataSet_Scheduler.sp03085_EP_Scheduler_EP_Jobs_Available_For_Adding, i_str_selected_site_ids2, i_str_selected_asset_type_ids, i_str_selected_job_type_ids, dtFromDate, dtToDate);
                this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
                view.EndUpdate();
            }
            else  // AT //
            {
                if (i_str_selected_site_ids == null) i_str_selected_site_ids = "";
                GridView view = (GridView)gridControl8.MainView;
                view.BeginUpdate();
                DateTime dtFromDate = dateEditFromDate.DateTime;
                DateTime dtToDate = dateEditToDate2.DateTime;

                this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                sp03087_EP_Scheduler_AT_Jobs_Available_For_AddingTableAdapter.Fill(this.dataSet_Scheduler.sp03087_EP_Scheduler_AT_Jobs_Available_For_Adding, i_str_selected_site_ids, i_str_selected_job_type_ids2, dtFromDate, dtToDate);
                this.RefreshGridViewState2.LoadViewInfo();  // Reload any expanded groups and selected rows //
                view.EndUpdate();
            }
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }

        private void frm_EP_Scheduler_Add_Jobs_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
            {
                // Store last used screen settings for current user //
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "SiteFilter", i_str_selected_site_ids);
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "FromDate", dateEditFromDate.DateTime.ToString());
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ToDate", dateEditToDate.DateTime.ToString());
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "AssetTypeFilter", i_str_selected_asset_type_ids);
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "JobTypeFilter", i_str_selected_job_type_ids);
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ClientFilter", i_str_selected_client_ids);
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "SiteFilter", i_str_selected_site_ids);
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "FromDate2", dateEditFromDate2.DateTime.ToString());
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ToDate2", dateEditToDate2.DateTime.ToString());
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "JobTypeFilter2", i_str_selected_job_type_ids2);
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ClientFilter2", i_str_selected_client_ids2);
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "SiteFilter2", i_str_selected_site_ids2);
                default_screen_settings.SaveDefaultScreenSettings();
            }

        }

        SchedulerDragData GetDragData(GridView view)
        {
            int[] selection = view.GetSelectedRows();
            if (selection == null)
                return null;

            AppointmentBaseCollection appointments = new AppointmentBaseCollection();
            int count = selection.Length;
            for (int i = 0; i < count; i++)
            {
                int rowIndex = selection[i];
                Appointment apt = PassedInSchedulerStorage.CreateAppointment(AppointmentType.Normal);
                apt.Subject = view.GetRowCellValue(rowIndex, "JobTypeDescription").ToString();
                apt.Location = view.GetRowCellValue(rowIndex, "SiteName").ToString() + " \\ " + view.GetRowCellValue(rowIndex, "AssetNumber").ToString();
                apt.Description = view.GetRowCellValue(rowIndex, "ActionRemarks").ToString();
                //apt.LabelId = (int)view.GetRowCellValue(rowIndex, "Severity");
                // apt.StatusId = (int)view.GetRowCellValue(rowIndex, "Priority");
                apt.Duration = TimeSpan.FromHours(2); //TimeSpan.FromHours((int)view.GetRowCellValue(rowIndex, "Duration"));
                apt.CustomFields["LinkedToRecordID"] = Convert.ToInt32(view.GetRowCellValue(rowIndex, "ActionID"));
                apt.CustomFields["LinkedToRecordTypeID"] = Convert.ToInt32("1");  // 1 = EP Action, 0 = AT Action //
                apt.CustomFields["CustomField1"] = view.GetRowCellValue(rowIndex, "AssetTypeDescription").ToString();
                apt.CustomFields["CustomField2"] = view.GetRowCellValue(rowIndex, "AssetSubTypeDescription").ToString();
                appointments.Add(apt);
                SelectedActionID = Convert.ToInt32(view.GetRowCellValue(rowIndex, "ActionID"));  // Store for later use (if aciton is dropped successfully on schedule) //
                SelectedActionTypeID = 1;
            }
            return new SchedulerDragData(appointments, 0);
        }

        SchedulerDragData GetDragData2(GridView view)
        {
            int[] selection = view.GetSelectedRows();
            if (selection == null)
                return null;

            AppointmentBaseCollection appointments = new AppointmentBaseCollection();
            int count = selection.Length;
            for (int i = 0; i < count; i++)
            {
                int rowIndex = selection[i];
                Appointment apt = PassedInSchedulerStorage.CreateAppointment(AppointmentType.Normal);
                apt.Subject = view.GetRowCellValue(rowIndex, "Action").ToString();
                apt.Location = view.GetRowCellValue(rowIndex, "SiteName").ToString() + " \\ " + view.GetRowCellValue(rowIndex, "TreeReference").ToString();
                apt.Description = view.GetRowCellValue(rowIndex, "ActionRemarks").ToString();
                //apt.LabelId = (int)view.GetRowCellValue(rowIndex, "Severity");
                // apt.StatusId = (int)view.GetRowCellValue(rowIndex, "Priority");
                apt.Duration = TimeSpan.FromHours(2); //TimeSpan.FromHours((int)view.GetRowCellValue(rowIndex, "Duration"));
                apt.CustomFields["LinkedToRecordID"] = Convert.ToInt32(view.GetRowCellValue(rowIndex, "ActionID"));
                apt.CustomFields["LinkedToRecordTypeID"] = Convert.ToInt32("0");  // 1 = EP Action, 0 = AT Action //
                apt.CustomFields["CustomField1"] = view.GetRowCellValue(rowIndex, "TreeSpeciesName").ToString();
                apt.CustomFields["CustomField2"] = "";
                appointments.Add(apt);
                SelectedActionID = Convert.ToInt32(view.GetRowCellValue(rowIndex, "ActionID"));  // Store for later use (if aciton is dropped successfully on schedule) //
                SelectedActionTypeID = 0;
            }
            return new SchedulerDragData(appointments, 0);
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Actions Available - Click Load Actions button";
                    break;
                case "gridView8":
                    message = "No Actions Available - Click Load Actions button";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_FocusedRowChanged_NoGroupSelection(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
        }

        public void GridView_MouseDown_NoGroupSelection(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        bool internalRowFocusing;

        #region GridView1

        private void gridView1_MouseMove(object sender, MouseEventArgs e)
        {
            GridView view = sender as GridView;
            if (e.Button == MouseButtons.Left && downHitInfo != null)
            {
                Size dragSize = SystemInformation.DragSize;
                Rectangle dragRect = new Rectangle(new Point(downHitInfo.HitPoint.X - dragSize.Width / 2,
                    downHitInfo.HitPoint.Y - dragSize.Height / 2), dragSize);

                if (!dragRect.Contains(new Point(e.X, e.Y)))
                {
                    view.GridControl.DoDragDrop(GetDragData(view), DragDropEffects.All);
                    downHitInfo = null;
                }
            }
        }

        #endregion


        #region GridView8

        private void gridView8_MouseMove(object sender, MouseEventArgs e)
        {
            GridView view = sender as GridView;
            if (e.Button == MouseButtons.Left && downHitInfo != null)
            {
                Size dragSize = SystemInformation.DragSize;
                Rectangle dragRect = new Rectangle(new Point(downHitInfo.HitPoint.X - dragSize.Width / 2,
                    downHitInfo.HitPoint.Y - dragSize.Height / 2), dragSize);

                if (!dragRect.Contains(new Point(e.X, e.Y)))
                {
                    view.GridControl.DoDragDrop(GetDragData2(view), DragDropEffects.All);
                    downHitInfo = null;
                }
            }
        }

        #endregion


        #region Site Filter Panel

        private void buttonEditClientFilter2_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "clear")
            {
                i_str_selected_client_ids2 = "";
                i_str_selected_client_names2 = "";
                buttonEditClientFilter2.Text = "";
            }
            else if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var fChildForm = new frm_Core_Select_Client_Site();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intAllowChildSelection = 0;
                fChildForm.intMustSelectChildren = 0;
                fChildForm.strPassedInParentIDs = i_str_selected_client_ids2;
                fChildForm.i_dtStart = DateTime.Today.AddMonths(-12);  // back a year //
                fChildForm.i_dtEnd = DateTime.Today.AddMonths(12);  // forward a year //
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    i_str_selected_client_ids2 = fChildForm.strSelectedParentIDs;
                    i_str_selected_client_names2 = fChildForm.strSelectedParentDescriptions;
                    buttonEditClientFilter2.Text = i_str_selected_client_names;

                    if (!string.IsNullOrWhiteSpace(fChildForm.strSelectedChildIDs))
                    {
                        i_str_selected_site_ids2 = fChildForm.strSelectedChildIDs;
                        i_str_selected_site_names2 = fChildForm.strSelectedChildDescriptions;
                        buttonEditSiteFilter2.Text = i_str_selected_site_names;
                    }
                }
            }
        }

        private void buttonEditSiteFilter2_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "clear")
            {
                i_str_selected_site_ids2 = "";
                i_str_selected_site_names2 = "";
                buttonEditSiteFilter2.Text = "";
            }
            else if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var fChildForm = new frm_Core_Select_Client_Site();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intAllowChildSelection = 1;
                fChildForm.intMustSelectChildren = 1;
                fChildForm.strPassedInParentIDs = i_str_selected_client_ids2;
                fChildForm.strPassedInChildIDs = i_str_selected_site_ids2;
                fChildForm.i_dtStart = DateTime.Today.AddMonths(-12);  // back a year //
                fChildForm.i_dtEnd = DateTime.Today.AddMonths(12);  // forward a year //
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    i_str_selected_client_ids2 = fChildForm.strSelectedParentIDs;
                    i_str_selected_client_names2 = fChildForm.strSelectedParentDescriptions;
                    buttonEditClientFilter2.Text = i_str_selected_client_names;

                    i_str_selected_site_ids2 = fChildForm.strSelectedChildIDs;
                    i_str_selected_site_names2 = fChildForm.strSelectedChildDescriptions;
                    buttonEditSiteFilter2.Text = i_str_selected_site_names;
                }
            }
        }

        #endregion


        #region Asset SubType Filter Panel

        private void btnAssetSubTypeFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView5_GotFocus(object sender, EventArgs e)
        {
            //i_int_FocusedGrid = 2;
            //SetMenuStatus();
        }

        private void popupContainerEdit2_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit2_Get_Selected();
        }

        private string PopupContainerEdit2_Get_Selected()
        {
            i_str_selected_asset_type_ids = "";    // Reset any prior values first //
            i_str_selected_asset_type_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl5.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_asset_type_ids = "";
                return "No Asset Type Filter";

            }
            else if (selection2.SelectedCount <= 0)
            {
                i_str_selected_asset_type_ids = "";
                return "No Asset Type Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_asset_type_ids += Convert.ToString(view.GetRowCellValue(i, "AssetTypeID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_asset_type_names = Convert.ToString(view.GetRowCellValue(i, "AssetTypeDescription"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_asset_type_names += ", " + Convert.ToString(view.GetRowCellValue(i, "AssetTypeDescription"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_asset_type_names;
        }

        #endregion


        #region Job Type Filter Panel

        private void btnJobSubTypeFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView4_GotFocus(object sender, EventArgs e)
        {
            //i_int_FocusedGrid = 2;
            //SetMenuStatus();
        }

        private void popupContainerEdit3_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit3_Get_Selected();
        }

        private string PopupContainerEdit3_Get_Selected()
        {
            i_str_selected_job_type_ids = "";    // Reset any prior values first //
            i_str_selected_job_type_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl4.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_job_type_ids = "";
                return "No Job Type Filter";

            }
            else if (selection3.SelectedCount <= 0)
            {
                i_str_selected_job_type_ids = "";
                return "No Job Type Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_job_type_ids += Convert.ToString(view.GetRowCellValue(i, "JobID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_job_type_names = Convert.ToString(view.GetRowCellValue(i, "JobDescription"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_job_type_names += ", " + Convert.ToString(view.GetRowCellValue(i, "JobDescription"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_job_type_names;
        }

        #endregion


        #region Site Filter Panel

        private void buttonEditClientFilter_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "clear")
            {
                i_str_selected_client_ids = "";
                i_str_selected_client_names = "";
                buttonEditClientFilter.Text = "";
            }
            else if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var fChildForm = new frm_Core_Select_Client_Site();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intAllowChildSelection = 0;
                fChildForm.intMustSelectChildren = 0;
                fChildForm.strPassedInParentIDs = i_str_selected_client_ids;
                fChildForm.i_dtStart = DateTime.Today.AddMonths(-12);  // back a year //
                fChildForm.i_dtEnd = DateTime.Today.AddMonths(12);  // forward a year //
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    i_str_selected_client_ids = fChildForm.strSelectedParentIDs;
                    i_str_selected_client_names = fChildForm.strSelectedParentDescriptions;
                    buttonEditClientFilter.Text = i_str_selected_client_names;

                    if (!string.IsNullOrWhiteSpace(fChildForm.strSelectedChildIDs))
                    {
                        i_str_selected_site_ids = fChildForm.strSelectedChildIDs;
                        i_str_selected_site_names = fChildForm.strSelectedChildDescriptions;
                        buttonEditSiteFilter.Text = i_str_selected_site_names;
                    }
                }
            }
        }

        private void buttonEditSiteFilter_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "clear")
            {
                i_str_selected_site_ids = "";
                i_str_selected_site_names = "";
                buttonEditSiteFilter.Text = "";
            }
            else if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var fChildForm = new frm_Core_Select_Client_Site();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intAllowChildSelection = 1;
                fChildForm.intMustSelectChildren = 1;
                fChildForm.strPassedInParentIDs = i_str_selected_client_ids;
                fChildForm.strPassedInChildIDs = i_str_selected_site_ids;
                fChildForm.i_dtStart = DateTime.Today.AddMonths(-12);  // back a year //
                fChildForm.i_dtEnd = DateTime.Today.AddMonths(12);  // forward a year //
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    i_str_selected_client_ids = fChildForm.strSelectedParentIDs;
                    i_str_selected_client_names = fChildForm.strSelectedParentDescriptions;
                    buttonEditClientFilter.Text = i_str_selected_client_names;

                    i_str_selected_site_ids = fChildForm.strSelectedChildIDs;
                    i_str_selected_site_names = fChildForm.strSelectedChildDescriptions;
                    buttonEditSiteFilter.Text = i_str_selected_site_names;
                }
            }
        }

        #endregion


        #region Job Type 2 Filter Panel

        private void btnJobSubTypeFilterOK2_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView7_GotFocus(object sender, EventArgs e)
        {
            //i_int_FocusedGrid = 2;
            //SetMenuStatus();
        }

        private void popupContainerEdit5_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit5_Get_Selected();
        }

        private string PopupContainerEdit5_Get_Selected()
        {
            i_str_selected_job_type_ids2 = "";    // Reset any prior values first //
            i_str_selected_job_type_names2 = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl7.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_job_type_ids2 = "";
                return "No Job Type Filter";

            }
            else if (selection5.SelectedCount <= 0)
            {
                i_str_selected_job_type_ids2 = "";
                return "No Job Type Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_job_type_ids2 += Convert.ToString(view.GetRowCellValue(i, "JobID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_job_type_names2 = Convert.ToString(view.GetRowCellValue(i, "JobDescription"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_job_type_names2 += ", " + Convert.ToString(view.GetRowCellValue(i, "JobDescription"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_job_type_names2;
        }

        #endregion


        public void ActionAddedToSchedule()
        {
            if (SelectedActionID != 0)  // Action ID stored so remove the row from the grid if it is found so it can't be added to the schedule again //
            {
                GridView view = null;
                if (SelectedActionTypeID == 1)
                {
                    view = (GridView)gridControl1.MainView;
                }
                else  // = 0 //
                {
                    view = (GridView)gridControl8.MainView;
                }
                int intFoundRow = view.LocateByValue("ActionID", SelectedActionID);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.BeginUpdate();
                    view.DeleteRow(intFoundRow);
                    view.EndUpdate();
                }
                SelectedActionID = 0;  // Reset Value //
            }
        }

        private void dateEditFromDate_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditFromDate.EditValue = null;
                }
            }
        }

        private void dateEditToDate_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditToDate.EditValue = null;
                }
            }
        }

        private void btnLoadActions_Click(object sender, EventArgs e)
        {
            LoadData("EP");
        }


        private void dateEditFromDate2_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditFromDate2.EditValue = null;
                }
            }
        }

        private void dateEditToDate2_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditToDate2.EditValue = null;
                }
            }
        }

        private void btnLoadActions2_Click(object sender, EventArgs e)
        {
            LoadData("AT");
        }







    }
}


using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraTab;
using DevExpress.XtraTab.ViewInfo;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraBars;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;
using Utilities;  // Used by Datasets //
using LocusEffects;

namespace WoodPlan5
{
    public partial class frm_EP_Site_Inspection_Manager : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        public string strPassedInDrillDownIDs = "";  // Used to hold IDs when form opened from another form in drill-down mode //

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;

        private int i_int_FocusedGrid = 1;
        string i_str_selected_client_ids = "";
        string i_str_selected_client_names = "";
        string i_str_selected_site_ids = "";
        string i_str_selected_site_names = "";

        private LocusEffects.LocusEffectsProvider locusEffectsProvider1;
        private ArrowLocusEffect m_customArrowLocusEffect1 = null;

        SuperToolTip superToolTipSiteContractFilter = null;
        SuperToolTipSetupArgs superToolTipSetupArgsSiteContractFilter = null;

        SuperToolTip superToolTipClientContractFilter = null;
        SuperToolTipSetupArgs superToolTipSetupArgsClientContractFilter = null;
 
        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState2;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState3;  // Used by Grid View State Facilities //

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs2 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs3 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        private string strDefaultPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //

        private DataSet_Selection DS_Selection2;
        private DataSet_Selection DS_Selection3;

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;

        #endregion

        public frm_EP_Site_Inspection_Manager()
        {
            InitializeComponent();
        }

        private void frm_EP_Site_Inspection_Manager_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 3005;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(this.dataSet_AT.sp00039GetFormPermissionsForUser, this.FormID, this.GlobalSettings.UserID, 0, this.GlobalSettings.ViewedPeriodID);  // 1 = Staff //
            ProcessPermissionsForForm();

            this.sp03046_EP_Site_Inspection_ManagerTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState1 = new RefreshGridState(gridView1, "InspectionID");

            dateEditFromDate.DateTime = this.GlobalSettings.ViewedStartDate;
            dateEditToDate.DateTime = this.GlobalSettings.ViewedEndDate;

            DS_Selection2 = new Utilities.DataSet_Selection((GridView)gridView1, strConnectionString);  // Inspections List //
            DS_Selection3 = new Utilities.DataSet_Selection((GridView)gridView3, strConnectionString);  // Actions List //
            emptyEditor = new RepositoryItem();

            sp03047_EP_Actions_For_Passed_InspectionsTableAdapter.Connection.ConnectionString = strConnectionString;       
            RefreshGridViewState2 = new RefreshGridState(gridView3, "ActionID");

            sp00220_Linked_Documents_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState3 = new RefreshGridState(gridView4, "LinkedDocumentID");

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesLinkedDocumentsPath").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Linked Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Create a SuperToolTip //
            superToolTipSiteContractFilter = new SuperToolTip();
            superToolTipSetupArgsSiteContractFilter = new SuperToolTipSetupArgs();
            superToolTipSetupArgsSiteContractFilter.Title.Text = "Site Filter - Information";
            superToolTipSetupArgsSiteContractFilter.Title.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            superToolTipSetupArgsSiteContractFilter.Contents.Text = "I store the currently applied Site Filter.\n\nNo Filter Set.";
            superToolTipSetupArgsSiteContractFilter.ShowFooterSeparator = false;
            superToolTipSetupArgsSiteContractFilter.Footer.Text = "";
            superToolTipSiteContractFilter.Setup(superToolTipSetupArgsSiteContractFilter);
            superToolTipSiteContractFilter.AllowHtmlText = DefaultBoolean.True;
            buttonEditSiteFilter.SuperTip = superToolTipSiteContractFilter;

            // Create a SuperToolTip //
            superToolTipClientContractFilter = new SuperToolTip();
            superToolTipSetupArgsClientContractFilter = new SuperToolTipSetupArgs();
            superToolTipSetupArgsClientContractFilter.Title.Text = "Client Filter - Information";
            superToolTipSetupArgsClientContractFilter.Title.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            superToolTipSetupArgsClientContractFilter.Contents.Text = "I store the currently applied Client Filter.\n\nNo Filter Set.";
            superToolTipSetupArgsClientContractFilter.ShowFooterSeparator = false;
            superToolTipSetupArgsClientContractFilter.Footer.Text = "";
            superToolTipClientContractFilter.Setup(superToolTipSetupArgsClientContractFilter);
            superToolTipClientContractFilter.AllowHtmlText = DefaultBoolean.True;
            buttonEditClientFilter.SuperTip = superToolTipClientContractFilter;

            if (strPassedInDrillDownIDs != "")  // Opened in drill-down mode //
            {
                buttonEditClientFilter.Text = "Custom Filter";
                buttonEditSiteFilter.Text = "Custom Filter";

                Load_Data();  // Load records //
            }
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            _KeepWaitFormOpen = false;
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();

            if (strPassedInDrillDownIDs == "")  // Not opened in drill-down mode so load last saved screen settings for current user //
            {
                Application.DoEvents();  // Allow Form time to repaint itself //
                LoadLastSavedUserScreenSettings();
            }
        }

        private void frm_EP_Site_Inspection_Manager_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedRecordIDs1))
                {
                    Load_Data();
                }
                else if (UpdateRefreshStatus >= 2)
                {
                    LoadLinkedRecords();
                }
            }
            SetMenuStatus();
        }

        private void frm_EP_Site_Inspection_Manager_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strPassedInDrillDownIDs == "")  // Not opened in drill-down mode so save screen settings for current user //
            {
                if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
                {
                    // Store last used screen settings for current user //
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ClientFilter", i_str_selected_client_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "SiteFilter", i_str_selected_site_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "FromDate", dateEditFromDate.DateTime.ToString());
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ToDate", dateEditToDate.DateTime.ToString());
                    default_screen_settings.SaveDefaultScreenSettings();
                }
            }
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                // From Date //
                string strFromDate = default_screen_settings.RetrieveSetting("FromDate");
                if (!string.IsNullOrEmpty(strFromDate)) dateEditFromDate.DateTime = Convert.ToDateTime(strFromDate);

                // To Date //
                string strToDate = default_screen_settings.RetrieveSetting("ToDate");
                if (!string.IsNullOrEmpty(strToDate)) dateEditToDate.DateTime = Convert.ToDateTime(strToDate);

                // Client Filter //
                string strItemFilter = default_screen_settings.RetrieveSetting("ClientFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    i_str_selected_client_ids = strItemFilter;
                    var ResolveIDs = new DataSet_GC_Summer_CoreTableAdapters.QueriesTableAdapter();
                    ResolveIDs.ChangeConnectionString(strConnectionString);
                    try
                    {
                        buttonEditClientFilter.Text = ResolveIDs.sp06023_OM_Get_Clients_From_ClientIDs(strItemFilter).ToString();
                        i_str_selected_client_names = buttonEditClientFilter.Text;

                        // Update Filter control's tooltip //
                        string strTooltipText = i_str_selected_client_names.Replace(", ", "\n");
                        superToolTipSetupArgsClientContractFilter.Contents.Text = "I store the currently applied Client Filter.\n\n" + strTooltipText;
                    }
                    catch (Exception) { }
                }

                // Site Filter //
                strItemFilter = default_screen_settings.RetrieveSetting("SiteFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    i_str_selected_site_ids = strItemFilter;
                    var ResolveIDs = new DataSet_GC_Summer_CoreTableAdapters.QueriesTableAdapter();
                    ResolveIDs.ChangeConnectionString(strConnectionString);
                    try
                    {
                        buttonEditSiteFilter.Text = ResolveIDs.sp06024_OM_Get_Sites_From_SiteIDs(strItemFilter).ToString();
                        i_str_selected_site_names = buttonEditSiteFilter.Text;

                        // Update Filter control's tooltip //
                        string strTooltipText = i_str_selected_site_names.Replace(", ", "\n");
                        superToolTipSetupArgsSiteContractFilter.Contents.Text = "I store the currently applied Site Filter.\n\n" + strTooltipText;
                    }
                    catch (Exception) { }
                }

                try
                {
                    DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                    GetSetting.ChangeConnectionString(strConnectionString);
                    if (GetSetting.sp00043_RetrieveSingleSystemSetting(2, "EstatePlanInspectionManagerLoadInspectionsOnOpen").ToString() == "On")
                    {
                        btnLoadInspections.PerformClick();  // Switched on so load data //
                        GridView view = (GridView)gridControl1.MainView;
                        view.ExpandAllGroups();
                    }
                }
                catch (Exception)
                {
                }

                // Prepare LocusEffects and add custom effect //
                locusEffectsProvider1 = new LocusEffectsProvider();
                locusEffectsProvider1.Initialize();
                locusEffectsProvider1.FramesPerSecond = 30;
                m_customArrowLocusEffect1 = new ArrowLocusEffect();
                m_customArrowLocusEffect1.Name = "CustomeArrow1";
                m_customArrowLocusEffect1.AnimationStartColor = Color.Orange;
                m_customArrowLocusEffect1.AnimationEndColor = Color.Red;
                m_customArrowLocusEffect1.MovementMode = MovementMode.OneWayAlongVector;
                m_customArrowLocusEffect1.MovementCycles = 20;
                m_customArrowLocusEffect1.MovementAmplitude = 200;
                m_customArrowLocusEffect1.MovementVectorAngle = 45; //degrees
                m_customArrowLocusEffect1.LeadInTime = 0; //msec
                m_customArrowLocusEffect1.LeadOutTime = 1000; //msec
                m_customArrowLocusEffect1.AnimationTime = 2000; //msec
                locusEffectsProvider1.AddLocusEffect(m_customArrowLocusEffect1);

                System.Drawing.Point location = dockPanelFilters.PointToScreen(System.Drawing.Point.Empty);
                System.Drawing.Point screenPoint = new System.Drawing.Point(location.X + 10, location.Y + 5);
                locusEffectsProvider1.ShowLocusEffect(this, screenPoint, m_customArrowLocusEffect1.Name);
            }
        }

        public void UpdateFormRefreshStatus(int status, string strNewIDs1, string strNewIDs2, string strNewIDs3)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
            if (strNewIDs2 != "") i_str_AddedRecordIDs2 = strNewIDs2;
            if (strNewIDs3 != "") i_str_AddedRecordIDs3 = strNewIDs3;
        }

        public void External_Call_For_Immediate_Refresh_Linked_Records(string strNewIDs1, string strNewIDs2, string strNewIDs3)
        {
            // Triggered by main MDI Form's Site Inspection Control Panel //
            Load_Data();
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:    // Whole Form //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = true;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;
            bbiBlockAddAction.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = null;
            int[] intRowHandles;
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();

            if (i_int_FocusedGrid == 1)  // Inspections //
            {
                view = (GridView)gridControl1.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                    if (intRowHandles.Length >= 1)
                    {
                        bbiBlockAddAction.Enabled = true;
                    }
                }
                bbiBlockAdd.Enabled = false;
                if (iBool_AllowEdit && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                bsiSwitchboardControlPanel.Enabled = true;
                bbiSwitchboardControlPanelLink.Enabled = (intRowHandles.Length == 1 ? true : false);
                bbiSwitchboardControlPanelUnlink.Enabled = (this.GlobalSettings.CurrentSiteInspectionID == 0 ? false : true);
                bsiSwitchboardControlPanel.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
            }
            else if (i_int_FocusedGrid == 2)  // Linked Actions //
            {
                view = (GridView)gridControl3.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                    /*
                    GridView viewParent = (GridView)gridControl1.MainView;
                    int[] intRowHandlesParent;
                    intRowHandlesParent = viewParent.GetSelectedRows();
                    if (intRowHandlesParent.Length >= 2)
                    {
                        alItems.Add("iBlockAdd");
                        bbiBlockAdd.Enabled = true;
                    }*/
                }
                if (iBool_AllowEdit && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                bsiSwitchboardControlPanel.Enabled = false;
                bsiSwitchboardControlPanel.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            }
            else if (i_int_FocusedGrid == 3)  // Linked Documents //
            {
                view = (GridView)gridControl4.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;

                    GridView viewParent = (GridView)gridControl1.MainView;
                    int[] intRowHandlesParent;
                    intRowHandlesParent = viewParent.GetSelectedRows();
                    if (intRowHandlesParent.Length >= 2)
                    {
                        alItems.Add("iBlockAdd");
                        bbiBlockAdd.Enabled = true;
                    }
                }
                //bbiBlockAdd.Enabled = false;
                if (iBool_AllowEdit && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                bsiSwitchboardControlPanel.Enabled = false;
                bsiSwitchboardControlPanel.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            }
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView1 navigator custom buttons //
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowAdd);
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0 ? true : false);
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0 ? true : false);

            // Set enabled status of GridView3 navigator custom buttons //
            view = (GridView)gridControl3.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl3.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowAdd);
            gridControl3.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0 ? true : false);
            gridControl3.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0 ? true : false);

            // Set enabled status of GridView4 navigator custom buttons //
            view = (GridView)gridControl4.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl4.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowAdd);
            gridControl4.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0 ? true : false);
            gridControl4.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0 ? true : false);

        }

        
        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockAddEvent(object sender, EventArgs e)
        {
            Block_Add_Record();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit_Record();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void Add_Record()
        {
            GridView view = null;
            GridView ParentView = null;
            int[] intRowHandles;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Inspections //
                    {
                        if (!iBool_AllowAdd) return;

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        frm_EP_Site_Inspection_Edit fChildForm = new frm_EP_Site_Inspection_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = "frm_EP_Site_Inspection_Manager";
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        ParentView = (GridView)gridControl1.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            fChildForm.intLinkedToRecordID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "SiteID"));
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 2:     // Actions //
                    {
                        if (!iBool_AllowAdd) return;

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        view = (GridView)gridControl3.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        frm_EP_Action_Edit fChildForm = new frm_EP_Action_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = "frm_EP_Site_Inspection_Manager";
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        ParentView = (GridView)gridControl1.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            fChildForm.intLinkedToInspectionID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "InspectionID"));
                            fChildForm.strLinkedInspectionDescription = Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "InspectionNumber"));
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 3:     // Linked Documents //
                    {
                        if (!iBool_AllowAdd) return;

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        view = (GridView)gridControl4.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        frm_AT_Linked_Document_Edit fChildForm2 = new frm_AT_Linked_Document_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = "";
                        fChildForm2.strFormMode = "add";
                        fChildForm2.strCaller = "frm_EP_Site_Inspection_Manager";
                        fChildForm2.intRecordCount = 0;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.intRecordTypeID = 12;  // Site Inspections //

                        ParentView = (GridView)gridControl1.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            fChildForm2.intLinkedToRecordID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "InspectionID"));
                            string strParentDesc = Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "InspectionNumber"));
                            if (string.IsNullOrEmpty(strParentDesc)) strParentDesc = "Inspection [No Number]";
                            fChildForm2.strLinkedToRecordDesc = strParentDesc;
                        }
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Block_Add_Record()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 3:     // Related Documents Link //
                    if (!iBool_AllowAdd) return;
                    view = (GridView)gridControl1.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }

                    fProgress = new frmProgress(10);
                    this.AddOwnedForm(fProgress);
                    fProgress.Show();  // ***** Closed in PostOpen event ***** //
                    Application.DoEvents();

                    foreach (int intRowHandle in intRowHandles)
                    {
                        strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "InspectionID")) + ',';
                    }
                    this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                    this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                    frm_AT_Linked_Document_Edit fChildForm2 = new frm_AT_Linked_Document_Edit();
                    fChildForm2.MdiParent = this.MdiParent;
                    fChildForm2.GlobalSettings = this.GlobalSettings;
                    fChildForm2.strRecordIDs = strRecordIDs;
                    fChildForm2.strFormMode = "blockadd";
                    fChildForm2.strCaller = "frm_EP_Site_Inspection_Manager";
                    fChildForm2.intRecordCount = intCount;
                    fChildForm2.FormPermissions = this.FormPermissions;
                    fChildForm2.fProgress = fProgress;
                    fChildForm2.intRecordTypeID = 12;  // Clients //
                    fChildForm2.Show();

                    method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    break;
                default:
                    break;
            }
        }

        private void Block_Edit_Record()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Inspections //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "InspectionID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        frm_EP_Site_Inspection_Edit fChildForm = new frm_EP_Site_Inspection_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = "frm_EP_Site_Inspection_Manager";
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 2:     // Actions //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl3.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ActionID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        frm_EP_Action_Edit fChildForm2 = new frm_EP_Action_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "blockedit";
                        fChildForm2.strCaller = "frm_EP_Site_Inspection_Manager";
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                case 3:     // Linked Documents //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl4.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedDocumentID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        frm_AT_Linked_Document_Edit fChildForm2 = new frm_AT_Linked_Document_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "blockedit";
                        fChildForm2.strCaller = "frm_EP_Site_Inspection_Manager";
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.intRecordTypeID = 12;  // Site Inspections //
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Edit_Record()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Inspections //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "InspectionID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        frm_EP_Site_Inspection_Edit fChildForm = new frm_EP_Site_Inspection_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = "frm_EP_Site_Inspection_Manager";
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 2:     // Actions //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl3.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ActionID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        frm_EP_Action_Edit fChildForm2 = new frm_EP_Action_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "edit";
                        fChildForm2.strCaller = "frm_EP_Site_Inspection_Manager";
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                case 3:     // Linked Documents //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl4.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedDocumentID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        frm_AT_Linked_Document_Edit fChildForm2 = new frm_AT_Linked_Document_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "edit";
                        fChildForm2.strCaller = "frm_EP_Site_Inspection_Manager";
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.intRecordTypeID = 12;  // Site Inspections //
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            switch (i_int_FocusedGrid)
            {
                case 1:  // Inspections //
                    if (!iBool_AllowDelete) return;
                    view = (GridView)gridControl1.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Inspections to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    // Checks passed so delete selected record(s) //
                    strMessage = "You have " + (intCount == 1 ? "1 Inspection" : Convert.ToString(intRowHandles.Length) + " Inspections") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Inspection" : "these Inspections") + " will no longer be available for selection and any related Linked Document LINKS will also be deleted!";
                    if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        splashScreenManager1.ShowWaitForm();
                        splashScreenManager1.SetWaitFormDescription("Deleting...");

                        string strRecordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "InspectionID")) + ",";
                        }

                        DataSet_EPTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_EPTableAdapters.QueriesTableAdapter();
                        RemoveRecords.ChangeConnectionString(strConnectionString);

                        RemoveRecords.sp03053_EP_Site_Inspection_Delete(strRecordIDs);  // Remove the records from the DB in one go //
                        Load_Data();

                        if (splashScreenManager1.IsSplashFormVisible)
                        {
                            splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                            splashScreenManager1.CloseWaitForm();
                        }

                        // Notify any open forms which reference this data that they will need to refresh their data on activating //
                        Broadcast_Data_Refresh Broadcast = new Broadcast_Data_Refresh();
                        Broadcast.Site_Inspection_Refresh(this.ParentForm, this.Name, "", "");

                        if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case 2:  // Actions //
                    if (!iBool_AllowDelete) return;
                    view = (GridView)gridControl3.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Actions to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    // Checks passed so delete selected record(s) //
                    strMessage = "You have " + (intCount == 1 ? "1 Action" : Convert.ToString(intRowHandles.Length) + " Actions") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this action" : "these actions") + " will no longer be available for selection!";
                    if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        splashScreenManager1.ShowWaitForm();
                        splashScreenManager1.SetWaitFormDescription("Deleting...");

                        string strRecordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ActionID")) + ",";
                        }

                        DataSet_EPTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_EPTableAdapters.QueriesTableAdapter();
                        RemoveRecords.ChangeConnectionString(strConnectionString);

                        RemoveRecords.sp03034_EP_Action_Delete(strRecordIDs);  // Remove the records from the DB in one go //
                        Load_Data();

                        if (splashScreenManager1.IsSplashFormVisible)
                        {
                            splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                            splashScreenManager1.CloseWaitForm();
                        }

                        // Notify any open forms which reference this data that they will need to refresh their data on activating //
                        Broadcast_Data_Refresh Broadcast = new Broadcast_Data_Refresh();
                        Broadcast.Asset_Action_Refresh(this.ParentForm, this.Name, "", "");

                        if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case 3:  // Linked Documents //
                    if (!iBool_AllowDelete) return;
                    view = (GridView)gridControl4.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Linked Documents to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    // Checks passed so delete selected record(s) //
                    strMessage = "You have " + (intCount == 1 ? "1 Linked Document" : Convert.ToString(intRowHandles.Length) + " Linked Documents") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Linked Document" : "these Linked Documents") + " will no longer be available for selection but the files(s) will still exist on the computer!";
                    if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        splashScreenManager1.ShowWaitForm();
                        splashScreenManager1.SetWaitFormDescription("Deleting...");

                        string strRecordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedDocumentID")) + ",";
                        }

                        DataSet_ATTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_ATTableAdapters.QueriesTableAdapter();
                        RemoveRecords.ChangeConnectionString(strConnectionString);

                        RemoveRecords.sp00223_Linked_Document_Delete(strRecordIDs);  // Remove the records from the DB in one go //
                        LoadLinkedRecords();

                        if (splashScreenManager1.IsSplashFormVisible)
                        {
                            splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                            splashScreenManager1.CloseWaitForm();
                        }
                        if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
            }
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Inspections - Click Load Inspections button";
                    break;
                case "gridView3":
                    message = "No Actions Available - Select one or more Inspections to view Related Actions";
                    break;
                case "gridView4":
                    message = "No Linked Documents Available - Select one or more Site Inspections to view Linked Documents";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    if (splitContainerControl2.PanelVisibility == SplitPanelVisibility.Both)
                    {
                        LoadLinkedRecords();
                        view = (GridView)gridControl3.MainView;
                        view.ExpandAllGroups();
                        view = (GridView)gridControl4.MainView;
                        view.ExpandAllGroups();
                    }
                    Set_Selected_Count();
                    break;
                case "gridView3":
                    Set_Selected_Count();
                    break;
                default:
                    break;
            }
            SetMenuStatus();
        }

        #endregion


        #region GridView1

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 1;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bbiDatasetCreate.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                if (view.RowCount > 0)
                {
                    //bbiDatasetSelection.Enabled = true;
                    //bsiDataset.Enabled = true;
                }
                else
                {
                    bbiDatasetSelection.Enabled = false;
                    bsiDataset.Enabled = false;
                }
                //bsiDataset.Enabled = true;
                //bbiDatasetManager.Enabled = true;
                bbiShowMap.Enabled = (view.SelectedRowsCount > 0 ? true : false);

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void Set_Selected_Count()
        {
            GridView view = (GridView)gridControl1.MainView;
            int intSelectedCount = view.GetSelectedRows().Length;
            view = (GridView)gridControl3.MainView;
            int intSelectedCount2 = view.GetSelectedRows().Length;
            if (intSelectedCount == 0 && intSelectedCount2 == 0)
            {
                bsiSelectedCount.Visibility = BarItemVisibility.Never;
            }
            else
            {
                bsiSelectedCount.Visibility = BarItemVisibility.Always;
                string strText = "";
                if (intSelectedCount > 0)
                {
                    strText = (intSelectedCount == 1 ? "1 Inspection Selected" : "<color=red>" + intSelectedCount.ToString() + " Inspections</Color> Selected");
                }
                if (intSelectedCount > 0 && intSelectedCount2 > 0) strText += "<br>";
                if (intSelectedCount2 > 0)
                {
                    strText += (intSelectedCount2 == 1 ? "1 Action Selected" : "<color=red>" + intSelectedCount2.ToString() + " Actions</Color> Selected");
                }
                bsiSelectedCount.Caption = strText;
            }
        }

        private void gridView1_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "LinkedActionCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedActionCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView1_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedActionCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedActionCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit1_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strInspectionIDs = view.GetRowCellValue(view.FocusedRowHandle, "InspectionID").ToString() + ",";
            DataSet_EPTableAdapters.QueriesTableAdapter GetSetting = new DataSet_EPTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            string strRecordIDs = "";
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedActionCount":
                    strRecordIDs = GetSetting.sp03048_EP_Site_Inspection_Manager_Get_Linked_Action_IDs(strInspectionIDs).ToString();
                    break;
                default:
                    break;
            }
            Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "site_inspection");
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView3

        private void gridView3_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 2;
            SetMenuStatus();
        }

        private void gridView3_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 2;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bbiDatasetCreate.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                if (view.RowCount > 0)
                {
                    //bbiDatasetSelection.Enabled = true;
                   // bsiDataset.Enabled = true;
                }
                else
                {
                    bbiDatasetSelection.Enabled = false;
                    bsiDataset.Enabled = false;
                }
                //bsiDataset.Enabled = true;
               // bbiDatasetManager.Enabled = true;
                bbiShowMap.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl3_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }
        #endregion


        #region GridView4

        private void gridView4_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView4_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 3;
            SetMenuStatus();
        }

        private void gridView4_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 3;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bbiShowMap.Enabled = false;
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void repositoryItemHyperLinkEdit2_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "DocumentPath").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Document Linked - unable to proceed.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(strFile);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view file: " + strFile + ".\n\nThe file may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void gridControl4_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region Filter Panel

        private void buttonEditClientFilter_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "clear")
            {
                i_str_selected_client_ids = "";
                i_str_selected_client_names = "";
                buttonEditClientFilter.Text = "";

                // Update Filter control's tooltip //
                superToolTipSetupArgsClientContractFilter.Contents.Text = "I store the currently applied Client Filter.\n\nNo Filter Set.";
            }
            else if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var fChildForm = new frm_Core_Select_Client_Site();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intAllowChildSelection = 0;
                fChildForm.intMustSelectChildren = 0;
                fChildForm.strPassedInParentIDs = i_str_selected_client_ids;
                fChildForm.i_dtStart = DateTime.Today.AddMonths(-12);  // back a year //
                fChildForm.i_dtEnd = DateTime.Today.AddMonths(12);  // forward a year //
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    i_str_selected_client_ids = fChildForm.strSelectedParentIDs;
                    i_str_selected_client_names = fChildForm.strSelectedParentDescriptions;
                    buttonEditClientFilter.Text = i_str_selected_client_names;

                    // Update Filter control's tooltip //
                    string strTooltipText = i_str_selected_client_names.Replace(", ", "\n");
                    superToolTipSetupArgsClientContractFilter.Contents.Text = "I store the currently applied Client Filter.\n\n" + strTooltipText;

                    if (!string.IsNullOrWhiteSpace(fChildForm.strSelectedChildIDs))
                    {
                        i_str_selected_site_ids = fChildForm.strSelectedChildIDs;
                        i_str_selected_site_names = fChildForm.strSelectedChildDescriptions;
                        buttonEditSiteFilter.Text = i_str_selected_site_names;

                        // Update Filter control's tooltip //
                        strTooltipText = i_str_selected_site_names.Replace(", ", "\n");
                        superToolTipSetupArgsSiteContractFilter.Contents.Text = "I store the currently applied Site Filter.\n\n" + strTooltipText;
                    }
                    else
                    {
                        // Update Filter control's tooltip //
                        strTooltipText = i_str_selected_site_names.Replace(", ", "\n");
                        superToolTipSetupArgsSiteContractFilter.Contents.Text = "I store the currently applied Site Filter.\n\nNo Filter Set.";
                    }
                }
            }
        }

        private void buttonEditSiteFilter_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "clear")
            {
                i_str_selected_site_ids = "";
                i_str_selected_site_names = "";
                buttonEditSiteFilter.Text = "";

                // Update Filter control's tooltip //
                superToolTipSetupArgsSiteContractFilter.Contents.Text = "I store the currently applied Site Filter.\n\nNo Filter Set.";
            }
            else if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var fChildForm = new frm_Core_Select_Client_Site();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intAllowChildSelection = 1;
                fChildForm.intMustSelectChildren = 1;
                fChildForm.strPassedInParentIDs = i_str_selected_client_ids;
                fChildForm.strPassedInChildIDs = i_str_selected_site_ids;
                fChildForm.i_dtStart = DateTime.Today.AddMonths(-12);  // back a year //
                fChildForm.i_dtEnd = DateTime.Today.AddMonths(12);  // forward a year //
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    i_str_selected_client_ids = fChildForm.strSelectedParentIDs;
                    i_str_selected_client_names = fChildForm.strSelectedParentDescriptions;
                    buttonEditClientFilter.Text = i_str_selected_client_names;

                    // Update Filter control's tooltip //
                    string strTooltipText = i_str_selected_client_names.Replace(", ", "\n");
                    superToolTipSetupArgsClientContractFilter.Contents.Text = "I store the currently applied Client Filter.\n\n" + strTooltipText;

                    i_str_selected_site_ids = fChildForm.strSelectedChildIDs;
                    i_str_selected_site_names = fChildForm.strSelectedChildDescriptions;
                    buttonEditSiteFilter.Text = i_str_selected_site_names;

                    // Update Filter control's tooltip //
                    strTooltipText = i_str_selected_site_names.Replace(", ", "\n");
                    superToolTipSetupArgsSiteContractFilter.Contents.Text = "I store the currently applied Site Filter.\n\n" + strTooltipText;
                }
            }
        }

        #endregion


        #region DataSet

        public override void OnDatasetCreateEvent(object sender, EventArgs e)
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strCurrentID = "";
            string strSelectedIDs = "";
            string strSelectedTrees = "";
            string strSelectedTree = "";
            int intSelectedTreeCount = 0;
            string strSelectedInspections = "";
            string strSelectedInspection = "";
            int intSelectedInspectionCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:  // Available Inspections Grid //
                    view = (GridView)gridControl1.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to add to the dataset before proceeding!", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    foreach (int intRowHandle in intRowHandles)
                    {
                        // Inspections //
                        if (strSelectedIDs == "")
                        {
                            strSelectedIDs = Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["InspectionID"])) + ',';
                        }
                        else
                        {
                            strCurrentID = Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["InspectionID"])) + ',';
                            if (!strSelectedIDs.Contains("," + strCurrentID))  // Put leading comma in temporarily for checking purposes //
                            {
                                strSelectedIDs += strCurrentID;
                            }
                        }
                        // Trees //
                        if (strSelectedTrees == "")
                        {
                            strSelectedTrees = Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["TreeID"])) + ',';
                            intSelectedTreeCount++;
                        }
                        else
                        {
                            strSelectedTree = Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["TreeID"])) + ',';
                            if (!strSelectedTrees.Contains("," + strSelectedTree))  // Put leading comma in temporarily for checking purposes //
                            {
                                strSelectedTrees += strSelectedTree;
                                intSelectedTreeCount++;
                            }
                        }
                    }
                    CreateDataset("Inspection", intSelectedTreeCount, strSelectedTrees, intCount, strSelectedIDs, 0, "");
                    break;
                case 2:  // Available Action Grid //
                    view = (GridView)gridControl3.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to add to the dataset before proceeding!", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    foreach (int intRowHandle in intRowHandles)
                    {
                        // Actions //
                        if (strSelectedIDs == "")
                        {
                            strSelectedIDs = Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["ActionID"])) + ',';
                        }
                        else
                        {
                            strCurrentID = Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["ActionID"])) + ',';
                            if (!strSelectedIDs.Contains("," + strCurrentID))  // Put leading comma in temporarily for checking purposes //
                            {
                                strSelectedIDs += strCurrentID;
                            }
                        }
                        // Trees //
                        if (strSelectedTrees == "")
                        {
                            strSelectedTrees = Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["TreeID"])) + ',';
                            intSelectedTreeCount++;
                        }
                        else
                        {
                            strSelectedTree = Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["TreeID"])) + ',';
                            if (!strSelectedTrees.Contains("," + strSelectedTree))  // Put leading comma in temporarily for checking purposes //
                            {
                                strSelectedTrees += strSelectedTree;
                                intSelectedTreeCount++;
                            }
                        }
                        // Inspections //
                        if (strSelectedInspections == "")
                        {
                            strSelectedInspections = Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["InspectionID"])) + ',';
                            intSelectedInspectionCount++;
                        }
                        else
                        {
                            strSelectedInspection = Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["InspectionID"])) + ',';
                            if (!strSelectedInspections.Contains("," + strSelectedInspection))  // Put leading comma in temporarily for checking purposes //
                            {
                                strSelectedInspections += strSelectedInspection;
                                intSelectedInspectionCount++;
                            }
                        }
                    }
                    CreateDataset("Action", intSelectedTreeCount, strSelectedTrees, intSelectedInspectionCount, strSelectedInspections, intCount, strSelectedIDs);
                    break;
                default:
                    return;
            }
        }

        private void CreateDataset(string strType, int intTreeCount, string strSelectedTreeIDs, int intInspectionCount, string strSelectedInspectionIDs, int intActionCount, string strSelectedActionIDs)
        {
            frmDatasetCreate fChildForm = new frmDatasetCreate();
            //Form frmMain = this.MdiParent;
            //frmMain.AddOwnedForm(fChildForm);
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.i_str_dataset_type = strType;
            fChildForm.i_int_selected_tree_count = intTreeCount;
            fChildForm.i_int_selected_inspection_count = intInspectionCount;
            fChildForm.i_int_selected_action_count = intActionCount;

            if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Dataset Created.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            else
            {
                int intAction = fChildForm.i_int_returned_action;
                string strName = fChildForm.i_str_dataset_name;
                strType = fChildForm.i_str_dataset_type;
                int intDatasetID = fChildForm.i_int_selected_dataset;
                string strDatasetType = fChildForm.i_str_dataset_type;
                int intReturnValue = 0;
                string strSelectedIDs = "";
                switch (strDatasetType.ToUpper())
                {
                    case "TREE":
                        strSelectedIDs = strSelectedTreeIDs;
                        break;
                    case "INSPECTION":
                        strSelectedIDs = strSelectedInspectionIDs;
                        break;
                    case "ACTION":
                        strSelectedIDs = strSelectedActionIDs;
                        break;
                    default:
                        strSelectedIDs = "";
                        break;
                }
                switch (intAction)
                {
                    case 1:  // Create New dataset and dataset members //
                        Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter AddDataset = new Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter();
                        AddDataset.ChangeConnectionString(strConnectionString);

                        intReturnValue = Convert.ToInt32(AddDataset.sp01224_AT_Dataset_create(strDatasetType, strName, this.GlobalSettings.UserID, strSelectedIDs));
                        if (intReturnValue <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while attempting to create the dataset!\n\nNo Dataset Created.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        return;
                    case 2:  // Append to dataset members //
                        Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter AppendDataset = new Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter();
                        AppendDataset.ChangeConnectionString(strConnectionString);
                        intReturnValue = Convert.ToInt32(AppendDataset.sp01226_AT_Dataset_append_dataset_members(intDatasetID, strSelectedIDs));
                        if (intReturnValue <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while attempting to append the selected records to the dataset!\n\nNo Records Append to the Dataset.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        return;
                    case 3: // Replace dataset members //
                        Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter ReplaceDataset = new Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter();
                        ReplaceDataset.ChangeConnectionString(strConnectionString);
                        intReturnValue = Convert.ToInt32(ReplaceDataset.sp01227_AT_Dataset_replace_dataset_members(intDatasetID, strSelectedIDs));
                        if (intReturnValue <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while attempting to replace the records in the selected dataset!\n\nNo Records Replaced in the Dataset.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        return;
                    default:
                        return;
                }
            }
        }

        public override void OnDatasetSelectionEvent(object sender, EventArgs e)
        {
            frmDatasetSelection fChildForm = new frmDatasetSelection();
            Form frmMain = this.MdiParent;
            int intSelectedDataset;
            string strSelectedDatasetType;
            switch (i_int_FocusedGrid)
            {
                case 1:  // Available Inspections Grid //
                    frmMain.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.i_str_dataset_types = "Tree,Inspection,"; // Comma seperated list needs to be passed //

                    if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
                    {
                        return;
                    }
                    intSelectedDataset = fChildForm.i_int_selected_dataset;
                    strSelectedDatasetType = fChildForm.i_str_selected_dataset_type;
                    switch (strSelectedDatasetType)
                    {
                        case "Tree":
                            DS_Selection2.SelectRecordsFromDataset(intSelectedDataset, "TreeID");
                            break;
                        case "Inspection":
                            DS_Selection2.SelectRecordsFromDataset(intSelectedDataset, "InspectionID");
                            break;
                        default:
                            break;
                    }
                    return;
                case 2:  // Available Actions Grid //
                    frmMain.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.i_str_dataset_types = "Tree,Inspection,Action,"; // Comma seperated list needs to be passed //

                    if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
                    {
                        return;
                    }
                    intSelectedDataset = fChildForm.i_int_selected_dataset;
                    strSelectedDatasetType = fChildForm.i_str_selected_dataset_type;
                    switch (strSelectedDatasetType)
                    {
                        case "Tree":
                            DS_Selection3.SelectRecordsFromDataset(intSelectedDataset, "TreeID");
                            break;
                        case "Inspection":
                            DS_Selection3.SelectRecordsFromDataset(intSelectedDataset, "InspectionID");
                            break;
                        case "Action":
                            DS_Selection3.SelectRecordsFromDataset(intSelectedDataset, "ActionID");
                            break;
                        default:
                            break;
                    }
                    return;
                default:
                    return;
            }
        }

        public override void OnDatasetSelectionInvertedEvent(object sender, EventArgs e)
        {
            frmDatasetSelection fChildForm = new frmDatasetSelection();
            Form frmMain = this.MdiParent;
            int intSelectedDataset;
            string strSelectedDatasetType;
            switch (i_int_FocusedGrid)
            {
                case 1:  // Available Inspections Grid //
                    frmMain.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.i_str_dataset_types = "Tree,Inspection,"; // Comma seperated list needs to be passed //

                    if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
                    {
                        return;
                    }
                    intSelectedDataset = fChildForm.i_int_selected_dataset;
                    strSelectedDatasetType = fChildForm.i_str_selected_dataset_type;
                    switch (strSelectedDatasetType)
                    {
                        case "Tree":
                            DS_Selection2.SelectRecordsFromDatasetInverted(intSelectedDataset, "TreeID");
                            break;
                        case "Inspection":
                            DS_Selection2.SelectRecordsFromDatasetInverted(intSelectedDataset, "InspectionID");
                            break;
                        default:
                            break;
                    }
                    return;
                case 2:  // Available Actions Grid //
                    frmMain.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.i_str_dataset_types = "Tree,Inspection,Action,"; // Comma seperated list needs to be passed //

                    if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
                    {
                        return;
                    }
                    intSelectedDataset = fChildForm.i_int_selected_dataset;
                    strSelectedDatasetType = fChildForm.i_str_selected_dataset_type;
                    switch (strSelectedDatasetType)
                    {
                        case "Tree":
                            DS_Selection3.SelectRecordsFromDatasetInverted(intSelectedDataset, "TreeID");
                            break;
                        case "Inspection":
                            DS_Selection3.SelectRecordsFromDatasetInverted(intSelectedDataset, "InspectionID");
                            break;
                        case "Action":
                            DS_Selection3.SelectRecordsFromDatasetInverted(intSelectedDataset, "ActionID");
                            break;
                        default:
                            break;
                    }
                    return;
                default:
                    return;
            }
        }

        public override void OnDatasetManagerEvent(object sender, EventArgs e)
        {
        }

        #endregion


        public override void OnShowMapEvent(object sender, EventArgs e)
        {
            GridView view = null;
            string strSelectedIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Site Inspections //
                    {
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to display on the map before proceeding.", "Show Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strSelectedIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "InspectionID")) + ',';
                        }
                        try
                        {
                            Mapping_Functions MapFunctions = new Mapping_Functions();
                            MapFunctions.Show_Map_From_Assets_Screen(this.GlobalSettings, strConnectionString, this, strSelectedIDs, "site inspection");
                        }
                        catch (Exception ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to open the map [" + ex.Message + "].\n\nTry opening the map again. If the problem persists contact Technical Support.", "Show Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                    }
                    break;
                case 2:     // Linked Asset Actions //
                    {
                        view = (GridView)gridControl3.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to display on the map before proceeding.", "Show Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strSelectedIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AssetID")) + ',';
                        }
                        try
                        {
                            Mapping_Functions MapFunctions = new Mapping_Functions();
                            MapFunctions.Show_Map_From_Assets_Screen(this.GlobalSettings, strConnectionString, this, strSelectedIDs, "asset");
                        }
                        catch (Exception ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to open the map [" + ex.Message + "].\n\nTry opening the map again. If the problem persists contact Technical Support.", "Show Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                    }
                    break;
            }
        }

        private void btnLoadInspections_Click(object sender, EventArgs e)
        {
            Load_Data();
        }

        private void Load_Data()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            if (!splashScreenManager.IsSplashFormVisible)
            {
                this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Loading Inspections...");
            }

            if (i_str_selected_site_ids == null) i_str_selected_site_ids = "";
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            DateTime dtFromDate = dateEditFromDate.DateTime;
            DateTime dtToDate = dateEditToDate.DateTime;
            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
            if (buttonEditClientFilter.Text == "Custom Filter" && strPassedInDrillDownIDs != "")  // Load passed in Inspections //
            {
                sp03046_EP_Site_Inspection_ManagerTableAdapter.Fill(this.dataSet_EP.sp03046_EP_Site_Inspection_Manager, "", "", strPassedInDrillDownIDs, dtFromDate, dtToDate);
                this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
                view.ExpandAllGroups();
            }
            else // Load users selection //
            {
                sp03046_EP_Site_Inspection_ManagerTableAdapter.Fill(this.dataSet_EP.sp03046_EP_Site_Inspection_Manager, i_str_selected_site_ids, i_str_selected_client_ids, "", dtFromDate, dtToDate);
                this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            view.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                //GridView view = (GridView)gridControl1.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["InspectionID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }

            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }

        private void LoadLinkedRecords()
        {
            if (splitContainerControl2.Collapsed) return;  // Don't bother loading related data as the parent panel is collapsed so the grids are invisible //

            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["InspectionID"])) + ',';
            }

            //Populate Linked Actions //
            gridControl3.MainView.BeginUpdate();
            this.RefreshGridViewState2.SaveViewInfo();
            if (intCount == 0)
            {
                this.dataSet_EP.sp03047_EP_Actions_For_Passed_Inspections.Clear();
            }
            else
            {
                DateTime dtFromDate = dateEditFromDate.DateTime;
                DateTime dtToDate = dateEditToDate.DateTime;
                sp03047_EP_Actions_For_Passed_InspectionsTableAdapter.Fill(dataSet_EP.sp03047_EP_Actions_For_Passed_Inspections, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), dtFromDate, dtToDate);
                this.RefreshGridViewState2.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl3.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            if (intCount > 0)
            {
                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDs2 != "")
                {
                    strArray = i_str_AddedRecordIDs2.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControl3.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["ActionID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDs2 = "";
                }
            }

            //Populate Linked Documents //
            gridControl4.MainView.BeginUpdate();
            this.RefreshGridViewState3.SaveViewInfo();
            if (intCount == 0)
            {
                this.dataSet_AT.sp00220_Linked_Documents_List.Clear();
            }
            else
            {
                sp00220_Linked_Documents_ListTableAdapter.Fill(dataSet_AT.sp00220_Linked_Documents_List, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), 12, strDefaultPath);
                this.RefreshGridViewState3.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl4.MainView.EndUpdate();

            if (intCount > 0)
            {
                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDs3 != "")
                {
                    strArray = i_str_AddedRecordIDs3.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControl4.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["LinkedDocumentID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDs3 = "";
                }
            }
        }

        private void dateEditFromDate_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditFromDate.EditValue = null;
                }
            }
        }

        private void dateEditToDate_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditToDate.EditValue = null;
                }
            }
        }

        private void splitContainerControl2_SplitGroupPanelCollapsed(object sender, SplitGroupPanelCollapsedEventArgs e)
        {
            if (!e.Collapsed) LoadLinkedRecords();
        }


        private void bbiSwitchboardControlPanelLink_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length != 1)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select just one inspection to link to the Switchboard Site Inspection Control Panel before proceeding.", "Link Inspection to Switchboard Site Inspection Control Panel", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            int intInspectionID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "InspectionID"));
            string strInspectionNumber = (string.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(intRowHandles[0], "InspectionNumber"))) ? "Inspection [No Number]" : Convert.ToString(view.GetRowCellValue(intRowHandles[0], "InspectionNumber")));
            frmMain2 MDI_Form = (frmMain2)this.MdiParent;
            this.GlobalSettings.CurrentSiteInspectionID = intInspectionID;  // Store Current ID for use with Switchboard Inspection Control Panel and Mapping //
            this.GlobalSettings.CurrentSiteInspectionDescription = strInspectionNumber;
            MDI_Form.SetControlPanelStatus("first_saved", strInspectionNumber);
            SetMenuStatus();  // Refresh Menu Items Enabled \ Disabled settings //
        }

        private void bbiSwitchboardControlPanelUnlink_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (DevExpress.XtraEditors.XtraMessageBox.Show("Please confirm you wish to unlink the currently stored inspection from the Control Panel.", "Unlink Inspection from Control Panel", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                this.GlobalSettings.CurrentSiteInspectionID = 0;
                frmMain2 MDI_Form = (frmMain2)this.MdiParent;
                this.GlobalSettings.CurrentSiteInspectionID = 0;  // Store Current ID for use with Switchboard Inspection Control Panel and Mapping //
                this.GlobalSettings.CurrentSiteInspectionDescription = "";
                MDI_Form.SetControlPanelStatus("unlink", "");
            }
            SetMenuStatus();  // Refresh Menu Items Enabled \ Disabled settings //
        }


        private void bciFilterData_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            if (bciFilterData.Checked)  // Filter Selected rows //
            {
                try
                {
                    int[] intRowHandles = view.GetSelectedRows();
                    int intCount = intRowHandles.Length;
                    DataRow dr = null;
                    if (intCount <= 0)
                    {
                        XtraMessageBox.Show("Select one or more Inspection records to filter by before proceeding.", "Filter Selected Inspection Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    gridControl1.BeginUpdate();
                    foreach (int intRowHandle in intRowHandles)
                    {
                        dr = view.GetDataRow(intRowHandle);
                        if (dr != null) dr["Selected"] = 1;
                    }
                }
                catch (Exception) { }
                view.ActiveFilter.Clear();
                view.ActiveFilter.NonColumnFilter = "[Selected] = 1";
                gridControl1.EndUpdate();
            }
            else  // Clear Filter //
            {
                gridControl1.BeginUpdate();
                try
                {
                    view.ActiveFilter.Clear();
                    foreach (DataRow dr in dataSet_AT.sp01200_TreeListALL.Rows)
                    {
                        if (Convert.ToInt32(dr["Selected"]) == 1) dr["Selected"] = 0;
                    }
                }
                catch (Exception) { }
            }
            gridControl1.EndUpdate();
        }

        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Load_Data();
        }





 


    }
}


using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraTab;
using DevExpress.XtraTab.ViewInfo;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_Core_Picklist_Manager : BaseObjects.frmBase
    {

        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        GridHitInfo downHitInfo = null;

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;

        public bool UpdateRefreshStatus = false; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //

        #endregion
       
        public frm_Core_Picklist_Manager()
        {
            InitializeComponent();
        }

        private void frm_Core_Picklist_Manager_Load(object sender, EventArgs e)
        {
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 4;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(this.dataSet_AT.sp00039GetFormPermissionsForUser, this.FormID, this.GlobalSettings.UserID, 0, this.GlobalSettings.ViewedPeriodID);  // 1 = Staff //
            ProcessPermissionsForForm();
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp00155_picklist_headersTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState1 = new RefreshGridState(gridView1, "HeaderID");
            
            gridControl1.BeginUpdate();
            Load_Data();
            gridControl1.EndUpdate();

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //  
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
            SetMenuStatus();
        }

        private void frm_Core_Picklist_Manager_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus)
            {
                gridControl1.BeginUpdate();
                Load_Data();
                gridControl1.EndUpdate();
            }
            SetMenuStatus();
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:    // Whole Form //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = (GridView)gridControl1.MainView; ;
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            bbiDelete.Enabled = false;
            if (iBool_AllowEdit && intRowHandles.Length >= 1)
            {
                alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                bsiEdit.Enabled = true;
                bbiSingleEdit.Enabled = true;
            }
            bbiBlockEdit.Enabled = false;
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            if (iBool_AllowEdit)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }

        }

        private void Load_Data()
        {
            if (UpdateRefreshStatus) UpdateRefreshStatus = false;
            sp00155_picklist_headersTableAdapter.Fill(this.woodPlanDataSet.sp00155_picklist_headers, this.GlobalSettings.UserID);
            this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
        }

        public void UpdateFormRefreshStatus(bool status)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
        }



        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Picklists Available - Check your access rights with your System Administrator");
        }

        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }
        
        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }


        bool internalRowFocusing;
        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void gridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void gridView1_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        Load_Data();
                    }
                    break;
                default:
                    break;
            }

        }

        #endregion


        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        private void Edit_Record()
        {
            GridView view = (GridView)gridControl1.MainView; ;
            if (!iBool_AllowEdit) return;
            view.PostEditor();
            int intPicklistHeaderID = 0;
            string strPickListHeaderName = "";
            int[] intRowHandles;
            int intCount = 0;
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select a picklist to edit before proceeding.", "Edit Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            frmProgress fProgress = new frmProgress(10);
            this.AddOwnedForm(fProgress);
            fProgress.Show();  // ***** Closed in PostOpen event ***** //
            Application.DoEvents();

            string strChildEditScreen = "";
            foreach (int intRowHandle in intRowHandles)
            {
                intPicklistHeaderID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "HeaderID"));
                strPickListHeaderName = Convert.ToString(view.GetRowCellValue(intRowHandle, "HeaderDescription"));
                strChildEditScreen = Convert.ToString(view.GetRowCellValue(intRowHandle, "ChildEditScreen"));
            }

            System.Reflection.MethodInfo method = null;
            switch (strChildEditScreen.ToLower())
            {
                case "sequence manager":
                    frm_Core_Sequence_Manager fSequence = new frm_Core_Sequence_Manager();
                    fSequence.MdiParent = this.MdiParent;
                    fSequence.GlobalSettings = this.GlobalSettings;
                    fSequence.fProgress = fProgress;
                    fSequence.Show();
                    method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fSequence, new object[] { null });
                    return;
                case "species manager":
                    frm_Core_Species_Manager fSpecies = new frm_Core_Species_Manager();
                    fSpecies.MdiParent = this.MdiParent;
                    fSpecies.GlobalSettings = this.GlobalSettings;
                    fSpecies.fProgress = fProgress;
                    fSpecies.Show();
                    method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fSpecies, new object[] { null });
                    return;
                case "contractor manager":
                    frm_Core_Contractor_Manager fContractors = new frm_Core_Contractor_Manager();
                    fContractors.MdiParent = this.MdiParent;
                    fContractors.GlobalSettings = this.GlobalSettings;
                    fContractors.fProgress = fProgress;
                    fContractors.Show();
                    method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fContractors, new object[] { null });
                    return;
                case "company header manager":
                    frm_Core_Company_Header_Manager fCompanyHeaders = new frm_Core_Company_Header_Manager();
                    fCompanyHeaders.MdiParent = this.MdiParent;
                    fCompanyHeaders.GlobalSettings = this.GlobalSettings;
                    fCompanyHeaders.fProgress = fProgress;
                    fCompanyHeaders.Show();
                    method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fCompanyHeaders, new object[] { null });
                    return;
                case "asset types manager":
                    frm_EP_Asset_Types_Manager fAssetTypes = new frm_EP_Asset_Types_Manager();
                    fAssetTypes.MdiParent = this.MdiParent;
                    fAssetTypes.GlobalSettings = this.GlobalSettings;
                    fAssetTypes.fProgress = fProgress;
                    fAssetTypes.Show();
                    method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fAssetTypes, new object[] { null });
                    return;
                // ***** Utilities ***** //
                case "master equipment manager":
                    {
                        if (fProgress != null) fProgress.Close();
                        frm_UT_Equipment_Manager fChild = new frm_UT_Equipment_Manager();
                        fChild.MdiParent = this.MdiParent;
                        fChild.GlobalSettings = this.GlobalSettings;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChild.splashScreenManager = splashScreenManager1;
                        fChild.splashScreenManager.ShowWaitForm();
                        fChild.Show();
                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChild, new object[] { null });
                    }
                    return;
                case "master material manager":
                    {
                        if (fProgress != null) fProgress.Close();
                        frm_UT_Material_Manager fChild = new frm_UT_Material_Manager();
                        fChild.MdiParent = this.MdiParent;
                        fChild.GlobalSettings = this.GlobalSettings;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChild.splashScreenManager = splashScreenManager1;
                        fChild.splashScreenManager.ShowWaitForm();
                        fChild.Show();
                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChild, new object[] { null });
                    }
                    return;
                case "master tree defect manager":
                    {
                        if (fProgress != null) fProgress.Close();
                        frm_UT_Tree_Defect_Manager fChild = new frm_UT_Tree_Defect_Manager();
                        fChild.MdiParent = this.MdiParent;
                        fChild.GlobalSettings = this.GlobalSettings;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChild.splashScreenManager = splashScreenManager1;
                        fChild.splashScreenManager.ShowWaitForm();
                        fChild.Show();
                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChild, new object[] { null });
                    }
                    return;
                case "master job manager":
                    {
                        if (fProgress != null) fProgress.Close();
                        frm_Core_Master_Job_Manager fChild = new frm_Core_Master_Job_Manager();
                        fChild.MdiParent = this.MdiParent;
                        fChild.GlobalSettings = this.GlobalSettings;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChild.splashScreenManager = splashScreenManager1;
                        fChild.splashScreenManager.ShowWaitForm();
                        fChild.Show();
                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChild, new object[] { null });
                    }
                    return;
                case "master risk questions":
                    {
                        if (fProgress != null) fProgress.Close();
                        frm_UT_Risk_Question_Manager fChild = new frm_UT_Risk_Question_Manager();
                        fChild.MdiParent = this.MdiParent;
                        fChild.GlobalSettings = this.GlobalSettings;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChild.splashScreenManager = splashScreenManager1;
                        fChild.splashScreenManager.ShowWaitForm();
                        fChild.Show();
                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChild, new object[] { null });
                    }
                    return;
                case "master site completion questions":
                    {
                        if (fProgress != null) fProgress.Close();
                        frm_UT_Master_Site_Completion_Question_Manager fChild = new frm_UT_Master_Site_Completion_Question_Manager();
                        fChild.MdiParent = this.MdiParent;
                        fChild.GlobalSettings = this.GlobalSettings;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChild.splashScreenManager = splashScreenManager1;
                        fChild.splashScreenManager.ShowWaitForm();
                        fChild.Show();
                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChild, new object[] { null });
                    }
                    return;
                // ***** HR ***** //
                case "master salary bandings manager":
                    {
                        if (fProgress != null) fProgress.Close();
                        frm_HR_Master_Salary_Banding_Manager fChild = new frm_HR_Master_Salary_Banding_Manager();
                        fChild.MdiParent = this.MdiParent;
                        fChild.GlobalSettings = this.GlobalSettings;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChild.splashScreenManager = splashScreenManager1;
                        fChild.splashScreenManager.ShowWaitForm();
                        fChild.Show();
                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChild, new object[] { null });
                    }
                    return;
                case "master vetting types":
                    {
                        if (fProgress != null) fProgress.Close();
                        frm_HR_Master_Vetting_Types_Manager fChild = new frm_HR_Master_Vetting_Types_Manager();
                        fChild.MdiParent = this.MdiParent;
                        fChild.GlobalSettings = this.GlobalSettings;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChild.splashScreenManager = splashScreenManager1;
                        fChild.splashScreenManager.ShowWaitForm();
                        fChild.Show();
                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChild, new object[] { null });
                    }
                    return;
                case "master qualification types":
                    {
                        if (fProgress != null) fProgress.Close();
                        frm_HR_Master_Qualification_Types_Manager fChild = new frm_HR_Master_Qualification_Types_Manager();
                        fChild.MdiParent = this.MdiParent;
                        fChild.GlobalSettings = this.GlobalSettings;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChild.splashScreenManager = splashScreenManager1;
                        fChild.splashScreenManager.ShowWaitForm();
                        fChild.Show();
                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChild, new object[] { null });
                    }
                    return;
                 // ***** OM ***** //
                case "master chemical manager":
                    {
                        if (fProgress != null) fProgress.Close();
                        frm_OM_Master_Chemical_Manager fChild = new frm_OM_Master_Chemical_Manager();
                        fChild.MdiParent = this.MdiParent;
                        fChild.GlobalSettings = this.GlobalSettings;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChild.splashScreenManager = splashScreenManager1;
                        fChild.splashScreenManager.ShowWaitForm();
                        fChild.Show();
                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChild, new object[] { null });
                    }
                    return;
                   
                default:  
                    this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                    frm_Core_Picklist_Edit fChildForm = new frm_Core_Picklist_Edit();
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.intPicklistHeaderID = intPicklistHeaderID;
                    fChildForm.strPickListHeaderName = strPickListHeaderName;
                    fChildForm.strFormMode = "edit";
                    fChildForm.strCaller = "frm_Core_Picklist_Manager";
                    fChildForm.intRecordCount = intCount;
                    fChildForm.iBool_AllowAdd = this.iBool_AllowEdit;  // All 3 values set the same as the data entry sreen is a grid //
                    fChildForm.iBool_AllowEdit = this.iBool_AllowEdit;
                    fChildForm.iBool_AllowDelete = this.iBool_AllowEdit;
                    fChildForm.fProgress = fProgress;
                    fChildForm.Show();

                    method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm, new object[] { null });
                    break;
            }
        }





    }
}


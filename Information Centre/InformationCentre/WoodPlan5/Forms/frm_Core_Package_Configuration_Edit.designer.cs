﻿namespace WoodPlan5
{
    partial class frm_Core_Package_Configuration_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Core_Package_Configuration_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.tableAdapterManager = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager();
            this.barStatus = new DevExpress.XtraBars.Bar();
            this.bsiFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.editFormLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.editFormDataNavigator = new DevExpress.XtraEditors.DataNavigator();
            this.dataSet_PC_Core = new WoodPlan5.DataSet_PC_Core();
            this.editFormDataLayoutControlGroup = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.txtEmailSubjectLine = new DevExpress.XtraEditors.TextEdit();
            this.deDateCreated = new DevExpress.XtraEditors.DateEdit();
            this.deDateLastRun = new DevExpress.XtraEditors.DateEdit();
            this.ceIsActive = new DevExpress.XtraEditors.CheckEdit();
            this.txtErrorEmail = new DevExpress.XtraEditors.TextEdit();
            this.txtJobPackageDescription = new DevExpress.XtraEditors.TextEdit();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForEmailSubjectLine = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForDateLastRun = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDateCreated = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIsActive = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForErrorEmail = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobPackageDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.spPC922701PackageConfigurationItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_PC_922701_Package_Configuration_ItemTableAdapter = new WoodPlan5.DataSet_PC_CoreTableAdapters.sp_PC_922701_Package_Configuration_ItemTableAdapter();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_PC_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).BeginInit();
            this.editFormDataLayoutControlGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmailSubjectLine.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDateCreated.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDateCreated.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDateLastRun.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDateLastRun.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceIsActive.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtErrorEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJobPackageDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmailSubjectLine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateLastRun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateCreated)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsActive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForErrorEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobPackageDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spPC922701PackageConfigurationItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1059, 31);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 702);
            this.barDockControlBottom.Size = new System.Drawing.Size(1059, 27);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 31);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 671);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1059, 31);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 671);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barStatus,
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.bsiFormMode,
            this.barStaticItem1,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemChangesPending,
            this.barStaticItemInformation,
            this.bbiRefresh});
            this.barManager1.MaxItemId = 37;
            this.barManager1.StatusBar = this.barStatus;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.sp_AS_11002_Equipment_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11005_Vehicle_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11008_Plant_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11011_Equipment_Category_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11014_Make_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11017_Model_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11020_Supplier_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Tracker_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Verilocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11026_Gadget_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11029_Office_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11032_Hardware_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11035_Software_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11041_Transaction_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11047_Equipment_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11050_Depreciation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Data_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Interval_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11057_Company_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11060_Department_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11063_Cost_Centre_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11066_Depreciation_Settings_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11075_Cover_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11078_Work_Detail_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11081_Incident_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11084_Purpose_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11087_Equipment_Category_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11089_Fuel_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11093_Initial_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11096_Road_Tax_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11099_Improvement_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11102_Rental_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11105_Speeding_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11108_Rental_Details_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11112_Keeper_Email_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11117_Sim_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11120_Additional_FollowUps_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11123_Sim_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_AlertTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11129_Software_Assign_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11133_Picklist_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_AS_11141_Hardware_Specifications_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11157_Email_Schedule_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11163_Service_ScheduleTableAdapter = null;
            this.tableAdapterManager.sp_AS_11166_P11D_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11169_NetBookValue_ItemTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // barStatus
            // 
            this.barStatus.BarName = "Status Bar";
            this.barStatus.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.barStatus.DockCol = 0;
            this.barStatus.DockRow = 0;
            this.barStatus.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.barStatus.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiFormMode, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barStaticItemInformation, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barStatus.OptionsBar.AllowQuickCustomization = false;
            this.barStatus.OptionsBar.DrawDragBorder = false;
            this.barStatus.OptionsBar.UseWholeRow = true;
            this.barStatus.Text = "Status bar";
            // 
            // bsiFormMode
            // 
            this.bsiFormMode.Caption = "Form Mode: Editing";
            this.bsiFormMode.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.Glyph")));
            this.bsiFormMode.Id = 29;
            this.bsiFormMode.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.LargeGlyph")));
            this.bsiFormMode.Name = "bsiFormMode";
            toolTipTitleItem1.Text = "Form Mode - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bsiFormMode.SuperTip = superToolTip1;
            this.bsiFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 31;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 32;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.Glyph")));
            this.barStaticItemInformation.Id = 33;
            this.barStaticItemInformation.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.LargeGlyph")));
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 27;
            this.bbiFormSave.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.LargeGlyph")));
            this.bbiFormSave.Name = "bbiFormSave";
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Text = "Save Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormSave.SuperTip = superToolTip2;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.Glyph")));
            this.bbiFormCancel.Id = 28;
            this.bbiFormCancel.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.LargeGlyph")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Text = "Cancel Button - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiFormCancel.SuperTip = superToolTip3;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Id = 36;
            this.bbiRefresh.Name = "bbiRefresh";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "barStaticItem1";
            this.barStaticItem1.Id = 30;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // editFormLayoutControlGroup
            // 
            this.editFormLayoutControlGroup.CustomizationFormText = "Main Layout Control Group";
            this.editFormLayoutControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.editFormLayoutControlGroup.GroupBordersVisible = false;
            this.editFormLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.ItemForEmailSubjectLine,
            this.emptySpaceItem3,
            this.ItemForDateLastRun,
            this.ItemForDateCreated,
            this.ItemForIsActive,
            this.ItemForErrorEmail,
            this.ItemForJobPackageDescription,
            this.layoutControlGroup1});
            this.editFormLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.editFormLayoutControlGroup.Name = "Root";
            this.editFormLayoutControlGroup.Size = new System.Drawing.Size(1059, 671);
            this.editFormLayoutControlGroup.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.editFormDataNavigator;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(386, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(245, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // editFormDataNavigator
            // 
            this.editFormDataNavigator.Buttons.Append.Visible = false;
            this.editFormDataNavigator.Buttons.CancelEdit.Visible = false;
            this.editFormDataNavigator.Buttons.EndEdit.Visible = false;
            this.editFormDataNavigator.Buttons.Remove.Visible = false;
            this.editFormDataNavigator.Location = new System.Drawing.Point(398, 12);
            this.editFormDataNavigator.Name = "editFormDataNavigator";
            this.editFormDataNavigator.Size = new System.Drawing.Size(241, 19);
            this.editFormDataNavigator.StyleController = this.editFormDataLayoutControlGroup;
            this.editFormDataNavigator.TabIndex = 122;
            this.editFormDataNavigator.Text = "depreciationDataNavigator";
            this.editFormDataNavigator.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            // 
            // dataSet_PC_Core
            // 
            this.dataSet_PC_Core.DataSetName = "DataSet_PC_Core";
            this.dataSet_PC_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // editFormDataLayoutControlGroup
            // 
            this.editFormDataLayoutControlGroup.Controls.Add(this.editFormDataNavigator);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtEmailSubjectLine);
            this.editFormDataLayoutControlGroup.Controls.Add(this.deDateCreated);
            this.editFormDataLayoutControlGroup.Controls.Add(this.deDateLastRun);
            this.editFormDataLayoutControlGroup.Controls.Add(this.ceIsActive);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtErrorEmail);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtJobPackageDescription);
            this.editFormDataLayoutControlGroup.DataSource = this.spPC922701PackageConfigurationItemBindingSource;
            this.editFormDataLayoutControlGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editFormDataLayoutControlGroup.Location = new System.Drawing.Point(0, 31);
            this.editFormDataLayoutControlGroup.Name = "editFormDataLayoutControlGroup";
            this.editFormDataLayoutControlGroup.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(861, 238, 250, 350);
            this.editFormDataLayoutControlGroup.Root = this.editFormLayoutControlGroup;
            this.editFormDataLayoutControlGroup.Size = new System.Drawing.Size(1059, 671);
            this.editFormDataLayoutControlGroup.TabIndex = 4;
            this.editFormDataLayoutControlGroup.Text = "dataLayoutControl1";
            // 
            // txtEmailSubjectLine
            // 
            this.txtEmailSubjectLine.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spPC922701PackageConfigurationItemBindingSource, "EmailSubjectLine", true));
            this.txtEmailSubjectLine.Location = new System.Drawing.Point(105, 59);
            this.txtEmailSubjectLine.MenuManager = this.barManager1;
            this.txtEmailSubjectLine.Name = "txtEmailSubjectLine";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtEmailSubjectLine, true);
            this.txtEmailSubjectLine.Size = new System.Drawing.Size(942, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtEmailSubjectLine, optionsSpelling1);
            this.txtEmailSubjectLine.StyleController = this.editFormDataLayoutControlGroup;
            this.txtEmailSubjectLine.TabIndex = 124;
            this.txtEmailSubjectLine.Validating += new System.ComponentModel.CancelEventHandler(this.textBox_Validating);
            // 
            // deDateCreated
            // 
            this.deDateCreated.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spPC922701PackageConfigurationItemBindingSource, "DateCreated", true));
            this.deDateCreated.EditValue = null;
            this.deDateCreated.Location = new System.Drawing.Point(624, 83);
            this.deDateCreated.MenuManager = this.barManager1;
            this.deDateCreated.Name = "deDateCreated";
            this.deDateCreated.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deDateCreated.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deDateCreated.Size = new System.Drawing.Size(423, 20);
            this.deDateCreated.StyleController = this.editFormDataLayoutControlGroup;
            this.deDateCreated.TabIndex = 125;
            this.deDateCreated.Validating += new System.ComponentModel.CancelEventHandler(this.dateEdit_Validating);
            // 
            // deDateLastRun
            // 
            this.deDateLastRun.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spPC922701PackageConfigurationItemBindingSource, "DateLastRun", true));
            this.deDateLastRun.EditValue = null;
            this.deDateLastRun.Location = new System.Drawing.Point(624, 107);
            this.deDateLastRun.MenuManager = this.barManager1;
            this.deDateLastRun.Name = "deDateLastRun";
            this.deDateLastRun.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deDateLastRun.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deDateLastRun.Size = new System.Drawing.Size(423, 20);
            this.deDateLastRun.StyleController = this.editFormDataLayoutControlGroup;
            this.deDateLastRun.TabIndex = 126;
            this.deDateLastRun.Validating += new System.ComponentModel.CancelEventHandler(this.dateEdit_Validating);
            // 
            // ceIsActive
            // 
            this.ceIsActive.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spPC922701PackageConfigurationItemBindingSource, "IsActive", true));
            this.ceIsActive.Location = new System.Drawing.Point(105, 107);
            this.ceIsActive.MenuManager = this.barManager1;
            this.ceIsActive.Name = "ceIsActive";
            this.ceIsActive.Properties.Caption = "Is Active";
            this.ceIsActive.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ceIsActive.Size = new System.Drawing.Size(422, 19);
            this.ceIsActive.StyleController = this.editFormDataLayoutControlGroup;
            this.ceIsActive.TabIndex = 127;
            // 
            // txtErrorEmail
            // 
            this.txtErrorEmail.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spPC922701PackageConfigurationItemBindingSource, "ErrorEmail", true));
            this.txtErrorEmail.Location = new System.Drawing.Point(105, 83);
            this.txtErrorEmail.MenuManager = this.barManager1;
            this.txtErrorEmail.Name = "txtErrorEmail";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtErrorEmail, true);
            this.txtErrorEmail.Size = new System.Drawing.Size(422, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtErrorEmail, optionsSpelling2);
            this.txtErrorEmail.StyleController = this.editFormDataLayoutControlGroup;
            this.txtErrorEmail.TabIndex = 128;
            this.txtErrorEmail.Validating += new System.ComponentModel.CancelEventHandler(this.textBox_Validating);
            // 
            // txtJobPackageDescription
            // 
            this.txtJobPackageDescription.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.spPC922701PackageConfigurationItemBindingSource, "JobPackageDescription", true));
            this.txtJobPackageDescription.Location = new System.Drawing.Point(105, 35);
            this.txtJobPackageDescription.MenuManager = this.barManager1;
            this.txtJobPackageDescription.Name = "txtJobPackageDescription";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtJobPackageDescription, true);
            this.txtJobPackageDescription.Size = new System.Drawing.Size(942, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtJobPackageDescription, optionsSpelling3);
            this.txtJobPackageDescription.StyleController = this.editFormDataLayoutControlGroup;
            this.txtJobPackageDescription.TabIndex = 123;
            this.txtJobPackageDescription.Validating += new System.ComponentModel.CancelEventHandler(this.textBox_Validating);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(386, 23);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(631, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(408, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForEmailSubjectLine
            // 
            this.ItemForEmailSubjectLine.Control = this.txtEmailSubjectLine;
            this.ItemForEmailSubjectLine.Location = new System.Drawing.Point(0, 47);
            this.ItemForEmailSubjectLine.Name = "ItemForEmailSubjectLine";
            this.ItemForEmailSubjectLine.Size = new System.Drawing.Size(1039, 24);
            this.ItemForEmailSubjectLine.Text = "Email Subject Line";
            this.ItemForEmailSubjectLine.TextSize = new System.Drawing.Size(90, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 119);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(1039, 508);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForDateLastRun
            // 
            this.ItemForDateLastRun.Control = this.deDateLastRun;
            this.ItemForDateLastRun.Location = new System.Drawing.Point(519, 95);
            this.ItemForDateLastRun.Name = "ItemForDateLastRun";
            this.ItemForDateLastRun.Size = new System.Drawing.Size(520, 24);
            this.ItemForDateLastRun.Text = "Date Last Run";
            this.ItemForDateLastRun.TextSize = new System.Drawing.Size(90, 13);
            // 
            // ItemForDateCreated
            // 
            this.ItemForDateCreated.Control = this.deDateCreated;
            this.ItemForDateCreated.Location = new System.Drawing.Point(519, 71);
            this.ItemForDateCreated.Name = "ItemForDateCreated";
            this.ItemForDateCreated.Size = new System.Drawing.Size(520, 24);
            this.ItemForDateCreated.Text = "Date Created";
            this.ItemForDateCreated.TextSize = new System.Drawing.Size(90, 13);
            // 
            // ItemForIsActive
            // 
            this.ItemForIsActive.Control = this.ceIsActive;
            this.ItemForIsActive.Location = new System.Drawing.Point(0, 95);
            this.ItemForIsActive.Name = "ItemForIsActive";
            this.ItemForIsActive.Size = new System.Drawing.Size(519, 24);
            this.ItemForIsActive.Text = "Is Active";
            this.ItemForIsActive.TextSize = new System.Drawing.Size(90, 13);
            // 
            // ItemForErrorEmail
            // 
            this.ItemForErrorEmail.Control = this.txtErrorEmail;
            this.ItemForErrorEmail.Location = new System.Drawing.Point(0, 71);
            this.ItemForErrorEmail.Name = "ItemForErrorEmail";
            this.ItemForErrorEmail.Size = new System.Drawing.Size(519, 24);
            this.ItemForErrorEmail.Text = "Error Email";
            this.ItemForErrorEmail.TextSize = new System.Drawing.Size(90, 13);
            // 
            // ItemForJobPackageDescription
            // 
            this.ItemForJobPackageDescription.Control = this.txtJobPackageDescription;
            this.ItemForJobPackageDescription.Location = new System.Drawing.Point(0, 23);
            this.ItemForJobPackageDescription.Name = "ItemForJobPackageDescription";
            this.ItemForJobPackageDescription.Size = new System.Drawing.Size(1039, 24);
            this.ItemForJobPackageDescription.StartNewLine = true;
            this.ItemForJobPackageDescription.Text = "Job Package Name";
            this.ItemForJobPackageDescription.TextSize = new System.Drawing.Size(90, 13);
            // 
            // spPC922701PackageConfigurationItemBindingSource
            // 
            this.spPC922701PackageConfigurationItemBindingSource.DataMember = "sp_PC_922701_Package_Configuration_Item";
            this.spPC922701PackageConfigurationItemBindingSource.DataSource = this.dataSet_PC_Core;
            // 
            // sp_PC_922701_Package_Configuration_ItemTableAdapter
            // 
            this.sp_PC_922701_Package_Configuration_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AllowDrawBackground = false;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 627);
            this.layoutControlGroup1.Name = "autoGeneratedGroup0";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1039, 24);
            // 
            // frm_Core_Package_Configuration_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1059, 729);
            this.Controls.Add(this.editFormDataLayoutControlGroup);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1065, 586);
            this.Name = "frm_Core_Package_Configuration_Edit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Manage Package Configuration Details";
            this.Activated += new System.EventHandler(this.frm_Core_Package_Configuration_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_Core_Package_Configuration_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_Core_Package_Configuration_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.editFormDataLayoutControlGroup, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_PC_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).EndInit();
            this.editFormDataLayoutControlGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtEmailSubjectLine.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDateCreated.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDateCreated.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDateLastRun.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDateLastRun.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceIsActive.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtErrorEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJobPackageDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmailSubjectLine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateLastRun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateCreated)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsActive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForErrorEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobPackageDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spPC922701PackageConfigurationItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DataSet_AS_DataEntryTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraBars.Bar barStatus;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.BarStaticItem bsiFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private DevExpress.XtraDataLayout.DataLayoutControl editFormDataLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlGroup editFormLayoutControlGroup;
        private DevExpress.XtraEditors.DataNavigator editFormDataNavigator;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DataSet_PC_Core dataSet_PC_Core;
        private DevExpress.XtraEditors.TextEdit txtEmailSubjectLine;
        private DevExpress.XtraEditors.DateEdit deDateCreated;
        private DevExpress.XtraEditors.DateEdit deDateLastRun;
        private DevExpress.XtraEditors.CheckEdit ceIsActive;
        private DevExpress.XtraEditors.TextEdit txtErrorEmail;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmailSubjectLine;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.TextEdit txtJobPackageDescription;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDateLastRun;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDateCreated;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsActive;
        private DevExpress.XtraLayout.LayoutControlItem ItemForErrorEmail;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobPackageDescription;
        private System.Windows.Forms.BindingSource spPC922701PackageConfigurationItemBindingSource;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DataSet_PC_CoreTableAdapters.sp_PC_922701_Package_Configuration_ItemTableAdapter sp_PC_922701_Package_Configuration_ItemTableAdapter;
    }
}

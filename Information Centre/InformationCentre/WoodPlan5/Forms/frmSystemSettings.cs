using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using WoodPlan5.Properties;
using BaseObjects;
using DevExpress.XtraEditors;
using DevExpress.XtraVerticalGrid;
using DevExpress.XtraVerticalGrid.Editors;
using DevExpress.XtraVerticalGrid.Data;
using DevExpress.XtraVerticalGrid.Rows;
using DevExpress.XtraVerticalGrid.Utils;
using DevExpress.XtraVerticalGrid.ViewInfo;

namespace WoodPlan5
{
 
    public partial class frmSystemSettings : frmBase
    {
        #region Instance Variables

        Settings set = Settings.Default;
        string strConnectionString = "";
        public int intLivePeriod = 0;
        public Boolean boolDataEntryOff = false;
        public frmSystemSettings()
        {
            InitializeComponent();
            strConnectionString = set.WoodPlanConnectionString;
        }
        public DateTime dtLiveStartDate;
        public DateTime dtLiveEndDate;
        public string strSystemDataTransferMode;

        #endregion

        private void frmSystemSettings_Load(object sender, EventArgs e)
        {
            Set_Grid_Highlighter_Transparent(this.Controls);

            sp00149_system_settings_mapping_projections_listTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00149_system_settings_mapping_projections_listTableAdapter.Fill(this.woodPlanDataSet.sp00149_system_settings_mapping_projections_list);
                  
            sp00148_system_settings_map_generation_methods_listTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00148_system_settings_map_generation_methods_listTableAdapter.Fill(woodPlanDataSet.sp00148_system_settings_map_generation_methods_list);
            
            sp01239_AT_WorkOrders_Print_Scale_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01239_AT_WorkOrders_Print_Scale_ListTableAdapter.Fill(dataSet_AT_WorkOrders.sp01239_AT_WorkOrders_Print_Scale_List);
            
            sp00127_system_settings_show_other_tree_details_listTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00127_system_settings_show_other_tree_details_listTableAdapter.Fill(woodPlanDataSet.sp00127_system_settings_show_other_tree_details_list);

            sp00128_system_settings_data_entry_off_listTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00128_system_settings_data_entry_off_listTableAdapter.Fill(woodPlanDataSet.sp00128_system_settings_data_entry_off_list);

            sp00129_system_settings_live_period_listTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00129_system_settings_live_period_listTableAdapter.Fill(woodPlanDataSet.sp00129_system_settings_live_period_list, 1);

            sp01277_AT_Default_WorkOrder_Layout_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01277_AT_Default_WorkOrder_Layout_ListTableAdapter.Fill(woodPlanDataSet.sp01277_AT_Default_WorkOrder_Layout_List, 1, 1);

            sp01300_AT_WorkOrder_Map_Page_SizesTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01300_AT_WorkOrder_Map_Page_SizesTableAdapter.Fill(woodPlanDataSet.sp01300_AT_WorkOrder_Map_Page_Sizes);

            sp00147_system_settings_screen_position_listTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00147_system_settings_screen_position_listTableAdapter.Fill(woodPlanDataSet.sp00147_system_settings_screen_position_list);

            sp00150_system_settings_data_transfer_mode_listTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00150_system_settings_data_transfer_mode_listTableAdapter.Fill(woodPlanDataSet.sp00150_system_settings_data_transfer_mode_list);

            sp00023_System_Settings_Rate_Criteria_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00023_System_Settings_Rate_Criteria_ListTableAdapter.Fill(woodPlanDataSet.sp00023_System_Settings_Rate_Criteria_List);

            sp00190_Contractor_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00190_Contractor_List_With_BlankTableAdapter.Fill(woodPlanDataSet.sp00190_Contractor_List_With_Blank);

            sp00232_Company_Header_Drop_Down_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00232_Company_Header_Drop_Down_List_With_BlankTableAdapter.Fill(dataSet_AT_WorkOrders.sp00232_Company_Header_Drop_Down_List_With_Blank);


            sp00236_Report_Watermark_Text_DirectionsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00236_Report_Watermark_Text_DirectionsTableAdapter.Fill(woodPlanDataSet.sp00236_Report_Watermark_Text_Directions);

            sp00237_Report_Watermark_Image_AlignmentTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00237_Report_Watermark_Image_AlignmentTableAdapter.Fill(woodPlanDataSet.sp00237_Report_Watermark_Image_Alignment);

            sp00238_Report_Watermark_Image_ViewModeTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00238_Report_Watermark_Image_ViewModeTableAdapter.Fill(woodPlanDataSet.sp00238_Report_Watermark_Image_ViewMode);

            // Ensure all Child editors are loaded before loading the main data source //
            sp00045_GetSystemSettingsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00045_GetSystemSettingsTableAdapter.Fill(woodPlanDataSet.sp00045_GetSystemSettings);

            string strTreeRefFormat = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTrees_TreePicker_Tree_Ref_Format_Used"]);
            if (!string.IsNullOrEmpty(strTreeRefFormat))
            {
                if (strTreeRefFormat.StartsWith("Default"))
                {
                    checkEditTreeRef1.Checked = true;
                }
                else if (strTreeRefFormat.StartsWith("Remove First"))
                {
                    checkEditTreeRef2.Checked = true;
                    strTreeRefFormat = strTreeRefFormat.Substring(14, strTreeRefFormat.Length - 27);
                    spinEditTreeRefRemoveNumberOfChars.EditValue = (CheckingFunctions.IsNumeric(strTreeRefFormat) ? Convert.ToInt32(strTreeRefFormat) : 1);
                }
                else if (strTreeRefFormat.StartsWith("Remove All"))
                {
                    checkEditTreeRef3.Checked = true;
                    textEditTreeRefRemovePreceedingChars.EditValue =  strTreeRefFormat.Substring(43);
                }
                spinEditTreeRefRemoveNumberOfChars.Enabled = checkEditTreeRef2.Checked;
                textEditTreeRefRemovePreceedingChars.Enabled = checkEditTreeRef3.Checked;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            string DataEntryOff = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["DataEntryOff"]);
            string LivePeriod = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["LivePeriod"]);
            string LiveDBName = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["LiveDBName"]);
            string StaffImagePath = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["StaffImagePath"]);
            string StaffImageThumbPath = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["StaffImageThumbPath"]);
            string DefaultPhoto = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["DefaultPhoto"]);
            string AmenityTreesWorkOrderLayoutLocation = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesWorkOrderLayoutLocation"]);
            string AmenityTreesReportLayoutLocation = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesReportLayoutLocation"]);
            string AmenityTreesWorkOrderMapLocation = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesWorkOrderMapLocation"]);
            string AmenityTreesWorkOrderPrintShowDetailsOfOtherTrees = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesWorkOrderPrintShowDetailsOfOtherTrees"]);
            string AmenityTreesMapTreeFilesFolder = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesMapTreeFilesFolder"]);
            string AmenityTreesMapBackgroundFolder = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesMapBackgroundFolder"]);
            string AmenityTreesWorkOrderDefaultMapScale = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesWorkOrderDefaultMapScale"]);
            string AmenityTreesExportToGBM_Background_Mapping_Folder = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesExportToGBM_Background_Mapping_Folder"]);
            string AmenityTreesExportToGBM_Geoset_Folder = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesExportToGBM_Geoset_Folder"]);
            string AmenityTreesExportToGBM_Profile_Folder = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesExportToGBM_Profile_Folder"]);
            string AmenityTreesExportToGBM_GBM_Mobile_XML_Folder = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesExportToGBM_GBM_Mobile_XML_Folder"]);
            string AmenityTreesExportToGBM_Tab_Folder = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesExportToGBM_Tab_Folder"]);
            string AmenityTreesExportToGBM_Mid_Mif_Folder = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesExportToGBM_Mid_Mif_Folder"]);
            string AmenityTreesImportFromGBM_Default_Tab_Folder = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesImportFromGBM_Default_Tab_Folder"]);
            string AmenityTreesExportToGBM_Pda_Reference_Data_Folder = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesExportToGBM_Pda_Reference_Data_Folder"]);

            string AmenityTrees_TreePicker_Polygon_Area = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTrees_TreePicker_Polygon_Area"]);
            string AmenityTrees_TreePicker_GPS_CrossHair_Colour = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTrees_TreePicker_GPS_CrossHair_Colour"]);
            string AmenityTrees_TreePicker_GPS_Polygon_Line_Plot_Colour = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTrees_TreePicker_GPS_Polygon_Line_Plot_Colour"]);
            string AmenityTrees_TreePicker_GPS_Polygon_Plot_Line_Style = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTrees_TreePicker_GPS_Polygon_Plot_Line_Style"]);
            string AmenityTrees_TreePicker_GPS_Polygon_Plot_Line_Width = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTrees_TreePicker_GPS_Polygon_Plot_Line_Width"]);
            string AmenityTrees_Sequence_From_Locality_Length = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTrees_Sequence_From_Locality_Length"]);
            string AmenityTrees_Sequence_From_Locality_Seperator = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTrees_Sequence_From_Locality_Seperator"]);
            string AmenityTreesWorkOrderDefaultLayoutID = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesWorkOrderDefaultLayoutID"]);

            string AmenityTreesWorkOrderDefaultPaperSize = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesWorkOrderDefaultPaperSize"]);
            string AmenityTreesWorkOrderDefaultLegendPosition = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesWorkOrderDefaultLegendPosition"]);
            string AmenityTreesWorkOrderDefaultNorthArrowPosition = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesWorkOrderDefaultNorthArrowPosition"]);
            string AmenityTreesWorkOrderDefaultScaleBarPosition = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesWorkOrderDefaultScaleBarPosition"]);
            string AmenityTreesWorkOrderOSCopyrightDefaultPosition = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesWorkOrderOSCopyrightDefaultPosition"]);
            string AmenityTreesWorkOrderOSCopyright = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesWorkOrderOSCopyright"]);
            string AmenityTreesWorkOrderMapGenerationMethod = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesWorkOrderMapGenerationMethod"]);
            string AmenityTreesWorkOrderMapTreeHighlightColour = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesWorkOrderMapTreeHighlightColour"]);

            string AmenityTreesVersion4WoodPlan_Incident_Link_Exe = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesVersion4WoodPlan_Incident_Link_Exe"]);

            string AmenityTreesWorkOrderMapTicked = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesWorkOrderMapTicked"]);
            string AmenityTrees_TreePicker_Thematic_Band_Width = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTrees_TreePicker_Thematic_Band_Width"]);
            string AmenityTrees_TreePicker_Gazetteer_Search_Radius = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTrees_TreePicker_Gazetteer_Search_Radius"]);
            string AmenityTrees_TreePicker_Gazetteer_Search_Radius_Colour = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTrees_TreePicker_Gazetteer_Search_Radius_Colour"]);
            string AmenityTrees_TreePicker_Gazetteer_Search_Radius_Transparency = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTrees_TreePicker_Gazetteer_Search_Radius_Transparency"]);
            string AmenityTrees_TreePicker_Tree_Ref_Format_Used = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTrees_TreePicker_Tree_Ref_Format_Used"]);

            string AmenityTreesPictureFilesFolder = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesPictureFilesFolder"]);

            string AmenityTreesDefaultMappingProjection = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesDefaultMappingProjection"]);

            string SystemDataTransferMode = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["SystemDataTransferMode"]);
            strSystemDataTransferMode = SystemDataTransferMode;
            string MobileInterfaceExportFolder = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["MobileInterfaceExportFolder"]);
            string MobileInterfaceImportFolder = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["MobileInterfaceImportFolder"]);
            string AmenityTreesMapBackgroundFolderTablet = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesMapBackgroundFolderTablet"]);

            string AmenityTreesJobRateCriteriaID = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesJobRateCriteriaID"]);
            string AmenityTreesJobDiscountRateCriteriaID = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesJobDiscountRateCriteriaID"]);

            string AmenityTreesLinkedDocumentsPath = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesLinkedDocumentsPath"]);
            string AmenityTrees_TreePicker_Default_Hedge_Width = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTrees_TreePicker_Default_Hedge_Width"]);

            string AmenityTreesDefaultCompanyHeader = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesDefaultCompanyHeader"]);
            string AmenityTreesDefaultContractor = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesDefaultContractor"]);

            string AmenityTreesCavatCalculation = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesCavatCalculation"]);
            string AmenityTreesCavatCalculationMethod = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesCavatCalculationMethod"]);
            string AmenityTreesCavatDefaultUnitValueFactor = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesCavatDefaultUnitValueFactor"]);

            string AmenityTreesTreeManagerLoadTreesOnOpen = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesTreeManagerLoadTreesOnOpen"]);
            string AmenityTreesInspectionManagerLoadInspectionsOnOpen = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesInspectionManagerLoadInspectionsOnOpen"]);
            string AmenityTreesActionManagerLoadActionsOnOpen = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesActionManagerLoadActionsOnOpen"]);

            string EstatePlanAssetManagerLoadAssetsOnOpen = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["EstatePlanAssetManagerLoadAssetsOnOpen"]);
            string EstatePlanActionManagerLoadActionsOnOpen = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["EstatePlanActionManagerLoadActionsOnOpen"]);
            string EstatePlanInspectionManagerLoadInspectionsOnOpen = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["EstatePlanInspectionManagerLoadInspectionsOnOpen"]);

            string MappingShowAmenityTreeObjects = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["MappingShowAmenityTreeObjects"]);
            string MappingShowEstatePlanObjects = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["MappingShowEstatePlanObjects"]);

            string GroundControlSiteDrawingsPath = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GroundControlSiteDrawingsPath"]);
            string GroundControlContractorCertificatesPath = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GroundControlContractorCertificatesPath"]);

            string GrittingForecastType1FilePath = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingForecastType1FilePath"]);
            string GrittingForecastType2FilePath = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingForecastType2FilePath"]);
            string GrittingConfirmationSentPath = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingConfirmationSentPath"]);
            string GrittingConfirmationReceivedPath = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingConfirmationReceivedPath"]);
            string GrittingAuthorisationSentHtmlLayoutFile = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingAuthorisationSentHtmlLayoutFile"]);
            string GrittingConfirmationSentHtmlLayoutFile = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingConfirmationSentHtmlLayoutFile"]);
            string GrittingConfirmationCCToEmailAddress = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingConfirmationCCToEmailAddress"]);

            string GrittingClientEmailAuthorisationSubjectLine = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingClientEmailAuthorisationSubjectLine"]);
            string GrittingClientEmailDailyReportSubjectLine = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingClientEmailDailyReportSubjectLine"]);
            string SMTPMailServerAddress = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["SMTPMailServerAddress"]);
            string GrittingTeamReportHtmlLayoutFile = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingTeamReportHtmlLayoutFile"]);
            string GrittingEmailsFromName = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingEmailsFromName"]);
            string TextAnywhereUsername = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["TextAnywhereUsername"]);
            string TextAnywherePassword = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["TextAnywherePassword"]);
            string GrittingDefaultVatRate = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingDefaultVatRate"]);
            string TeamEmailReportSubjectLine = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["TeamEmailReportSubjectLine"]);

            string GrittingAuthorisationGeneratedFileType = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingAuthorisationGeneratedFileType"]);
            string GrittingAuthorisationOddRowColour = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingAuthorisationOddRowColour"]);
            string GrittingAuthorisationEvenRowColour = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingAuthorisationEvenRowColour"]);

            string GrittingCalloutPictureFilesFolder = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingCalloutPictureFilesFolder"]);
            string GrittingTeamConfirmationCCToEmailAddress = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingTeamConfirmationCCToEmailAddress"]);
            string GrittingTeamReportEmailFromAddress = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingTeamReportEmailFromAddress"]);

            string GrittingSelfBillingInvoicePDFPath = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingSelfBillingInvoicePDFPath"]);
            string GrittingSelfBillingInvoiceEmailCCFromAddress = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingSelfBillingInvoiceEmailCCFromAddress"]);
            string GrittingSelfBillingInvoiceEmailHTMLLayoutFile = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingSelfBillingInvoiceEmailHTMLLayoutFile"]);
            string GrittingSelfBillingInvoiceEmailFromAddress = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingSelfBillingInvoiceEmailFromAddress"]);
            string GrittingSelfBillingInvoiceEmailSubjectLine = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingSelfBillingInvoiceEmailSubjectLine"]);

            string GrittingReportLayouts = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingReportLayouts"]);

            string GrittingSavedCalloutSignatureFolder = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingSavedCalloutSignatureFolder"]);

            string SnowClearanceTeamPOPDFPath = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["SnowClearanceTeamPOPDFPath"]);
            string SnowClearanceTeamPOEmailFromAddress = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["SnowClearanceTeamPOEmailFromAddress"]);
            string SnowClearanceTeamPOEmailCCAddress = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["SnowClearanceTeamPOEmailCCAddress"]);
            string SnowClearanceTeamPOEmailHTMLLayoutFile = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["SnowClearanceTeamPOEmailHTMLLayoutFile"]);
            string SnowClearanceTeamPOEmailSubjectLine = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["SnowClearanceTeamPOEmailSubjectLine"]);

            string SaltStockControlPeriodID = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["SaltStockControlPeriodID"]);

            string ClientJobBreakdownReportPDFPath = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["ClientJobBreakdownReportPDFPath"]);
            string ClientJobBreakdownReportEmailFromAddress = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["ClientJobBreakdownReportEmailFromAddress"]);
            string ClientJobBreakdownReportEmailCCAddress = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["ClientJobBreakdownReportEmailCCAddress"]);
            string ClientJobBreakdownReportHTMLLayoutFile = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["ClientJobBreakdownReportHTMLLayoutFile"]);
            string ClientJobBreakdownReportEmailSubjectLine = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["ClientJobBreakdownReportEmailSubjectLine"]);

            string SMTPMailServerUsername = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["SMTPMailServerUsername"]);
            string SMTPMailServerPassword = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["SMTPMailServerPassword"]);

            string FinanceClientInvoiceSSDefaultSavePath = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["FinanceClientInvoiceSSDefaultSavePath"]);

            string FinanceTeamCostsSSDefaultSavePath = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["FinanceTeamCostsSSDefaultSavePath"]);
            string FinanceTeamCostSSGrittingAnalysisCode = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["FinanceTeamCostSSGrittingAnalysisCode"]);
            string FinanceTeamCostSSSnowClearanceAnalysisCode = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["FinanceTeamCostSSSnowClearanceAnalysisCode"]);
            string FinanceTeamCostSSGrittingCostCentre = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["FinanceTeamCostSSGrittingCostCentre"]);
            string FinanceTeamCostSSSnowClearanceCostCentre = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["FinanceTeamCostSSSnowClearanceCostCentre"]);

            string SMTPMailServerPort = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["SMTPMailServerPort"]);

            string MissingJobNumberSheetSavePath = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["MissingJobNumberSheetSavePath"]);
            string MissingJobNumberSheetEmailFromAddress = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["MissingJobNumberSheetEmailFromAddress"]);
            string MissingJobNumberSheetEmailCCAddress = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["MissingJobNumberSheetEmailCCAddress"]);
            string MissingJobNumberSheetSS_HTMLLayoutFile = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["MissingJobNumberSheetSS_HTMLLayoutFile"]);
            string MissingJobNumberSheetSubjectLine = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["MissingJobNumberSheetSubjectLine"]);
            string MissingJobNumberSheetLoadPath = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["MissingJobNumberSheetLoadPath"]);
            string MissingJobNumberSheetPDF_HTMLLayoutFile = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["MissingJobNumberSheetPDF_HTMLLayoutFile"]);

            string TenderStakeholderEmailPDFPath = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["TenderStakeholderEmailPDFPath"]);
            string TenderStakeholderEmailFromAddress = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["TenderStakeholderEmailFromAddress"]);
            string TenderStakeholderEmailCCAddress = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["TenderStakeholderEmailCCAddress"]);
            string TenderStakeholderEmailHTMLLayoutFile = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["TenderStakeholderEmailHTMLLayoutFile"]);
            string TenderStakeholderEmailSubjectLine = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["TenderStakeholderEmailSubjectLine"]);

            string TenderReportLayouts = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["TenderReportLayouts"]);

            string ExtraWorkQuotationEmailPDFPath = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["ExtraWorkQuotationEmailPDFPath"]);
            string ExtraWorkQuotationEmailFromAddress = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["ExtraWorkQuotationEmailFromAddress"]);
            string ExtraWorkQuotationEmailCCAddress = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["ExtraWorkQuotationEmailCCAddress"]);
            string ExtraWorkQuotationEmailHTMLLayoutFile = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["ExtraWorkQuotationEmailHTMLLayoutFile"]);
            string ExtraWorkQuotationEmailSubjectLine = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["ExtraWorkQuotationEmailSubjectLine"]);

            string ArchivaWorkPermitFile = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["ArchivaWorkPermitFile"]);

            string GrittingCalloutPictureFilesFolderInternal = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingCalloutPictureFilesFolderInternal"]);

            string UtilitiesSavedReportLayouts = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["UtilitiesSavedReportLayouts"]);
            string SavedPermissionDocuments = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["SavedPermissionDocuments"]);
            string SavedMaps = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["SavedMaps"]);
            string SavedPictures = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["SavedPictures"]);
            string SavedSignatures = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["SavedSignatures"]);
            string PermissionDocumentEmailFromAddressClient = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["PermissionDocumentEmailFromAddressClient"]);
            string PermissionDocumentEmailFromAddressCustomer = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["PermissionDocumentEmailFromAddressCustomer"]);
            string PermissionDocumentEmailCCAddressClient = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["PermissionDocumentEmailCCAddressClient"]);
            string PermissionDocumentEmailCCAddressCustomer = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["PermissionDocumentEmailCCAddressCustomer"]);
            string PermissionDocumentEmailHTMLLayoutFileClient = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["PermissionDocumentEmailHTMLLayoutFileClient"]);
            string PermissionDocumentEmailHTMLLayoutFileCustomer = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["PermissionDocumentEmailHTMLLayoutFileCustomer"]);
            string PermissionDocumentEmailSubjectLineClient = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["PermissionDocumentEmailSubjectLineClient"]);
            string PermissionDocumentEmailSubjectLineCustomer = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["PermissionDocumentEmailSubjectLineCustomer"]);
            string DataSynchronisationMachineID = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["DataSynchronisationMachineID"]);
            string SavedReferenceFiles = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["SavedReferenceFiles"]);

            string SavedWorkOrderDocuments = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["SavedWorkOrderDocuments"]);
            string WorkOrderDocumentEmailFromAddress = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["WorkOrderDocumentEmailFromAddress"]);
            string WorkOrderDocumentEmailCCAddress = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["WorkOrderDocumentEmailCCAddress"]);
            string WorkOrderDocumentEmailHTMLLayoutFile = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["WorkOrderDocumentEmailHTMLLayoutFile"]);
            string WorkOrderDocumentEmailSubjectLine = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["WorkOrderDocumentEmailSubjectLine"]);

            string GrittingForecastTypeFloatingFilePath = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingForecastTypeFloatingFilePath"]);
            string GrittingFloatingSiteEmailHtmlLayoutFile = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingFloatingSiteEmailHtmlLayoutFile"]);
            string GrittingFloatingSiteEmailCCToEmailAddress = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingFloatingSiteEmailCCToEmailAddress"]);
            string GrittingFloatingSiteEmailSubjectLine = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingFloatingSiteEmailSubjectLine"]);
            string GrittingFloatingJobsFilePath = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingFloatingJobsFilePath"]);
            string GrittingFloatingSiteEmailFromAddress = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingFloatingSiteEmailFromAddress"]);

            string GrittingRouteOptimisationURL = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingRouteOptimisationURL"]);
            string GrittingRouteOptimisationUsername = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingRouteOptimisationUsername"]);
            string GrittingRouteOptimisationPassword = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingRouteOptimisationPassword"]);
            string GrittingRouteOptimisationJobFileSavePath = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingRouteOptimisationJobFileSavePath"]);
            string GrittingRouteOptimisationIniFileSavePath = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingRouteOptimisationIniFileSavePath"]);
            string GrittingRouteOptimisationOptimisedRouteSavePath = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingRouteOptimisationOptimisedRouteSavePath"]);

            string GrittingCompletionEmailHtmlLayoutFile = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingCompletionEmailHtmlLayoutFile"]);
            string GrittingCompletionEmailFromEmailAddress = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingCompletionEmailFromEmailAddress"]);
            string GrittingCompletionEmailCCToEmailAddress = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingCompletionEmailCCToEmailAddress"]);
            string GrittingCompletionEmailSubjectLine = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["GrittingCompletionEmailSubjectLine"]);

            string WatermarkText = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["WatermarkText"]);
            string WatermarkTextDirection = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["WatermarkTextDirection"]);
            string WatermarkTextFont = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["WatermarkTextFont"]);
            string WatermarkTextFontSize = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["WatermarkTextFontSize"]);
            string WatermarkTextFontBold = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["WatermarkTextFontBold"]);
            string WatermarkTextFontItalic = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["WatermarkTextFontItalic"]);
            string WatermarkTextColour = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["WatermarkTextColour"]);
            string WatermarkTextTransparency = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["WatermarkTextTransparency"]);
            string WatermarkImage = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["WatermarkImage"]);
            string WatermarkImageAlign = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["WatermarkImageAlign"]);
            string WatermarkImageTiling = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["WatermarkImageTiling"]);
            string WatermarkImageViewMode = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["WatermarkImageViewMode"]);
            string WatermarkImageTransparency = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["WatermarkImageTransparency"]);
            string WatermarkShowBehind = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["WatermarkShowBehind"]);
            string WatermarkTextPageRange = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["WatermarkTextPageRange"]);
            string AmenityTreesWorkOrderOSCopyrightWidth = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["AmenityTreesWorkOrderOSCopyrightWidth"]);

            string HR_QualificationCertificatePath = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["HR_QualificationCertificatePath"]);
            string HR_LinkedDocumentPath = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["HR_LinkedDocumentPath"]);
            string HR_PayrollReportExportSavedFolder = Convert.ToString(woodPlanDataSet.sp00045_GetSystemSettings.Rows[0]["HR_PayrollReportExportSavedFolder"]);

            
            intLivePeriod = Convert.ToInt32(LivePeriod);
            if (DataEntryOff == "0")
            {
                boolDataEntryOff = false;
            }
            else
            {
                boolDataEntryOff = true;
            }
            object row = repositoryItemLookUpEdit4.GetDataSourceRowByKeyValue(LivePeriod);
            dtLiveStartDate = Convert.ToDateTime((row as DataRowView)["dtFromDate"]);
            dtLiveEndDate = Convert.ToDateTime((row as DataRowView)["dtToDate"]); 

            // Update Database... //
            try
            {
                using (WoodPlanDataSetTableAdapters.QueriesTableAdapter UpdateDatabase = new WoodPlanDataSetTableAdapters.QueriesTableAdapter())
                {
                    UpdateDatabase.ChangeConnectionString(strConnectionString);
                    UpdateDatabase.sp00130_system_settings_update(DataEntryOff, 
                                                                    LivePeriod, 
                                                                    LiveDBName, 
                                                                    StaffImagePath, 
                                                                    StaffImageThumbPath, 
                                                                    DefaultPhoto, 
                                                                    AmenityTreesWorkOrderLayoutLocation, 
                                                                    AmenityTreesReportLayoutLocation, 
                                                                    AmenityTreesWorkOrderMapLocation, 
                                                                    AmenityTreesWorkOrderPrintShowDetailsOfOtherTrees, 
                                                                    AmenityTreesMapTreeFilesFolder, 
                                                                    AmenityTreesMapBackgroundFolder, 
                                                                    AmenityTreesWorkOrderDefaultMapScale, 
                                                                    AmenityTreesExportToGBM_Background_Mapping_Folder, 
                                                                    AmenityTreesExportToGBM_Geoset_Folder, 
                                                                    AmenityTreesExportToGBM_Profile_Folder, 
                                                                    AmenityTreesExportToGBM_GBM_Mobile_XML_Folder, 
                                                                    AmenityTreesExportToGBM_Tab_Folder, 
                                                                    AmenityTreesExportToGBM_Mid_Mif_Folder,
                                                                    AmenityTreesImportFromGBM_Default_Tab_Folder,
                                                                    AmenityTreesExportToGBM_Pda_Reference_Data_Folder,
                                                                    AmenityTrees_TreePicker_Polygon_Area,
                                                                    AmenityTrees_TreePicker_GPS_CrossHair_Colour,
                                                                    AmenityTrees_TreePicker_GPS_Polygon_Line_Plot_Colour,
                                                                    AmenityTrees_TreePicker_GPS_Polygon_Plot_Line_Style,
                                                                    AmenityTrees_TreePicker_GPS_Polygon_Plot_Line_Width,
                                                                    AmenityTrees_Sequence_From_Locality_Length,
                                                                    AmenityTrees_Sequence_From_Locality_Seperator,
                                                                    AmenityTreesWorkOrderDefaultLayoutID,
                                                                    AmenityTreesWorkOrderDefaultPaperSize,
                                                                    AmenityTreesWorkOrderDefaultLegendPosition,
                                                                    AmenityTreesWorkOrderDefaultNorthArrowPosition,
                                                                    AmenityTreesWorkOrderDefaultScaleBarPosition,
                                                                    AmenityTreesWorkOrderOSCopyrightDefaultPosition,
                                                                    AmenityTreesWorkOrderOSCopyright,
                                                                    AmenityTreesWorkOrderMapGenerationMethod,
                                                                    AmenityTreesWorkOrderMapTreeHighlightColour,
                                                                    AmenityTreesVersion4WoodPlan_Incident_Link_Exe,
                                                                    AmenityTreesWorkOrderMapTicked,
                                                                    AmenityTrees_TreePicker_Thematic_Band_Width,
                                                                    AmenityTrees_TreePicker_Gazetteer_Search_Radius,
                                                                    AmenityTrees_TreePicker_Gazetteer_Search_Radius_Colour,
                                                                    AmenityTrees_TreePicker_Gazetteer_Search_Radius_Transparency,
                                                                    AmenityTrees_TreePicker_Tree_Ref_Format_Used,
                                                                    AmenityTreesPictureFilesFolder,
                                                                    AmenityTreesDefaultMappingProjection,
                                                                    SystemDataTransferMode,
                                                                    MobileInterfaceExportFolder,
                                                                    MobileInterfaceImportFolder,
                                                                    AmenityTreesMapBackgroundFolderTablet,
                                                                    AmenityTreesJobRateCriteriaID,
                                                                    AmenityTreesJobDiscountRateCriteriaID,
                                                                    AmenityTreesLinkedDocumentsPath,
                                                                    AmenityTrees_TreePicker_Default_Hedge_Width,
                                                                    AmenityTreesDefaultCompanyHeader,
                                                                    AmenityTreesDefaultContractor,
                                                                    AmenityTreesCavatCalculation,
                                                                    AmenityTreesCavatCalculationMethod,
                                                                    AmenityTreesCavatDefaultUnitValueFactor,
                                                                    AmenityTreesTreeManagerLoadTreesOnOpen,
                                                                    AmenityTreesInspectionManagerLoadInspectionsOnOpen,
                                                                    AmenityTreesActionManagerLoadActionsOnOpen,
                                                                    EstatePlanAssetManagerLoadAssetsOnOpen,
                                                                    EstatePlanActionManagerLoadActionsOnOpen,
                                                                    EstatePlanInspectionManagerLoadInspectionsOnOpen,
                                                                    MappingShowAmenityTreeObjects,
                                                                    MappingShowEstatePlanObjects,
                                                                    GroundControlSiteDrawingsPath,
                                                                    GroundControlContractorCertificatesPath,
                                                                    GrittingForecastType1FilePath,
                                                                    GrittingForecastType2FilePath,
                                                                    GrittingConfirmationSentPath,
                                                                    GrittingConfirmationReceivedPath,
                                                                    GrittingAuthorisationSentHtmlLayoutFile,
                                                                    GrittingConfirmationSentHtmlLayoutFile,
                                                                    GrittingConfirmationCCToEmailAddress,
                                                                    GrittingClientEmailAuthorisationSubjectLine,
                                                                    GrittingClientEmailDailyReportSubjectLine,
                                                                    SMTPMailServerAddress,
                                                                    GrittingTeamReportHtmlLayoutFile,
                                                                    GrittingEmailsFromName,
                                                                    TextAnywhereUsername,
                                                                    TextAnywherePassword,
                                                                    GrittingDefaultVatRate,
                                                                    TeamEmailReportSubjectLine,
                                                                    GrittingAuthorisationGeneratedFileType,
                                                                    GrittingAuthorisationOddRowColour,
                                                                    GrittingAuthorisationEvenRowColour,
                                                                    GrittingCalloutPictureFilesFolder,
                                                                    GrittingTeamConfirmationCCToEmailAddress,
                                                                    GrittingTeamReportEmailFromAddress,
                                                                    GrittingSelfBillingInvoicePDFPath,
                                                                    GrittingSelfBillingInvoiceEmailCCFromAddress,
                                                                    GrittingSelfBillingInvoiceEmailHTMLLayoutFile,
                                                                    GrittingSelfBillingInvoiceEmailFromAddress,
                                                                    GrittingSelfBillingInvoiceEmailSubjectLine,
                                                                    GrittingReportLayouts,
                                                                    GrittingSavedCalloutSignatureFolder,
                                                                    SnowClearanceTeamPOPDFPath,
                                                                    SnowClearanceTeamPOEmailFromAddress,
                                                                    SnowClearanceTeamPOEmailCCAddress,
                                                                    SnowClearanceTeamPOEmailHTMLLayoutFile,
                                                                    SnowClearanceTeamPOEmailSubjectLine,
                                                                    SaltStockControlPeriodID,
                                                                    ClientJobBreakdownReportPDFPath,
                                                                    ClientJobBreakdownReportEmailFromAddress,
                                                                    ClientJobBreakdownReportEmailCCAddress,
                                                                    ClientJobBreakdownReportHTMLLayoutFile,
                                                                    ClientJobBreakdownReportEmailSubjectLine,
                                                                    SMTPMailServerUsername,
                                                                    SMTPMailServerPassword,
                                                                    FinanceClientInvoiceSSDefaultSavePath,
                                                                    FinanceTeamCostsSSDefaultSavePath,
                                                                    FinanceTeamCostSSGrittingAnalysisCode,
                                                                    FinanceTeamCostSSSnowClearanceAnalysisCode,
                                                                    FinanceTeamCostSSGrittingCostCentre,
                                                                    FinanceTeamCostSSSnowClearanceCostCentre,
                                                                    SMTPMailServerPort,
                                                                    MissingJobNumberSheetSavePath,
                                                                    MissingJobNumberSheetEmailFromAddress,
                                                                    MissingJobNumberSheetEmailCCAddress,
                                                                    MissingJobNumberSheetSS_HTMLLayoutFile,
                                                                    MissingJobNumberSheetSubjectLine,
                                                                    MissingJobNumberSheetLoadPath,
                                                                    MissingJobNumberSheetPDF_HTMLLayoutFile,
                                                                    TenderStakeholderEmailPDFPath,
                                                                    TenderStakeholderEmailFromAddress,
                                                                    TenderStakeholderEmailCCAddress,
                                                                    TenderStakeholderEmailHTMLLayoutFile,
                                                                    TenderStakeholderEmailSubjectLine,
                                                                    TenderReportLayouts,
                                                                    ExtraWorkQuotationEmailPDFPath,
                                                                    ExtraWorkQuotationEmailFromAddress,
                                                                    ExtraWorkQuotationEmailCCAddress,
                                                                    ExtraWorkQuotationEmailHTMLLayoutFile,
                                                                    ExtraWorkQuotationEmailSubjectLine,
                                                                    ArchivaWorkPermitFile,
                                                                    GrittingCalloutPictureFilesFolderInternal,
                                                                    UtilitiesSavedReportLayouts,
                                                                    SavedPermissionDocuments,
                                                                    SavedMaps,
                                                                    SavedPictures,
                                                                    SavedSignatures,
                                                                    PermissionDocumentEmailFromAddressClient,
                                                                    PermissionDocumentEmailFromAddressCustomer,
                                                                    PermissionDocumentEmailCCAddressClient,
                                                                    PermissionDocumentEmailCCAddressCustomer,
                                                                    PermissionDocumentEmailHTMLLayoutFileClient,
                                                                    PermissionDocumentEmailHTMLLayoutFileCustomer,
                                                                    PermissionDocumentEmailSubjectLineClient,
                                                                    PermissionDocumentEmailSubjectLineCustomer,
                                                                    DataSynchronisationMachineID,
                                                                    SavedReferenceFiles,
                                                                    SavedWorkOrderDocuments,
                                                                    WorkOrderDocumentEmailFromAddress,
                                                                    WorkOrderDocumentEmailCCAddress,
                                                                    WorkOrderDocumentEmailHTMLLayoutFile,
                                                                    WorkOrderDocumentEmailSubjectLine,
                                                                    GrittingForecastTypeFloatingFilePath,
                                                                    GrittingFloatingSiteEmailHtmlLayoutFile,
                                                                    GrittingFloatingSiteEmailCCToEmailAddress,
                                                                    GrittingFloatingSiteEmailSubjectLine,
                                                                    GrittingFloatingJobsFilePath,
                                                                    GrittingFloatingSiteEmailFromAddress,
                                                                    GrittingRouteOptimisationURL,
                                                                    GrittingRouteOptimisationUsername,
                                                                    GrittingRouteOptimisationPassword,
                                                                    GrittingRouteOptimisationJobFileSavePath,
                                                                    GrittingRouteOptimisationIniFileSavePath,
                                                                    GrittingRouteOptimisationOptimisedRouteSavePath,
                                                                    GrittingCompletionEmailHtmlLayoutFile,
                                                                    GrittingCompletionEmailFromEmailAddress,
                                                                    GrittingCompletionEmailCCToEmailAddress,
                                                                    GrittingCompletionEmailSubjectLine,
                                                                    WatermarkText,
                                                                    WatermarkTextDirection,
                                                                    WatermarkTextFont,
                                                                    WatermarkTextFontSize,
                                                                    WatermarkTextFontBold,
                                                                    WatermarkTextFontItalic,
                                                                    WatermarkTextColour,
                                                                    WatermarkTextTransparency,
                                                                    WatermarkImage,
                                                                    WatermarkImageAlign,
                                                                    WatermarkImageTiling,
                                                                    WatermarkImageViewMode,
                                                                    WatermarkImageTransparency,
                                                                    WatermarkShowBehind,
                                                                    WatermarkTextPageRange,
                                                                    AmenityTreesWorkOrderOSCopyrightWidth,
                                                                    HR_QualificationCertificatePath,
                                                                    HR_LinkedDocumentPath,
                                                                    HR_PayrollReportExportSavedFolder
                                                                    );
                }
            }
            catch (Exception Ex)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to save the changes.\n\nError: " + Ex.Message + "\n\nWould you like to try saving again?\n\nClick Yes to close this message then click OK again.\n\nClick No to close this form without saving.", "Save System Settings", MessageBoxButtons.YesNo, MessageBoxIcon.Stop) == DialogResult.Yes)return;  // Abort so user can attempt save again //
            }
            DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                ButtonEdit edit = sender as ButtonEdit;
                
                /*OpenFileDialog dlg = new OpenFileDialog();
                dlg.Filter = "All Files(*.*)|*.*";
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    edit.EditValue = new System.IO.FileInfo(dlg.FileName).Name;
                }*/
                using (FolderBrowserDialog fbd = new FolderBrowserDialog())
                {
                    fbd.SelectedPath = edit.EditValue.ToString();
                    fbd.Description = "Select a Folder then click OK.";
                    fbd.ShowNewFolderButton = false;
                    if (fbd.ShowDialog() == DialogResult.OK)
                        edit.EditValue = fbd.SelectedPath;
                }
            }
        }

        private void repositoryItemButtonEditGetFileName_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                ButtonEdit edit = sender as ButtonEdit;
                
                using (OpenFileDialog dlg = new OpenFileDialog())
                {
                    dlg.Filter = "All Files(*.*)|*.*"; ;
                    dlg.Multiselect = false;
                    if (dlg.ShowDialog() == DialogResult.OK) 
                        edit.EditValue = new System.IO.FileInfo(dlg.FileName).ToString();
                }
            }
        }

        #region PopupContainer TreeRefStructure

        private void checkEditTreeRef1_CheckedChanged(object sender, EventArgs e)
        {
            SetTreeRefStructureControlStatus();
        }

        private void checkEditTreeRef2_CheckedChanged(object sender, EventArgs e)
        {
            SetTreeRefStructureControlStatus();
        }

        private void checkEditTreeRef3_CheckedChanged(object sender, EventArgs e)
        {
            SetTreeRefStructureControlStatus();
        }

        private void SetTreeRefStructureControlStatus()
        {
            spinEditTreeRefRemoveNumberOfChars.Enabled = checkEditTreeRef2.Checked;
            textEditTreeRefRemovePreceedingChars.Enabled = checkEditTreeRef3.Checked;
        }

        private void btnSetTreeRefStruc_Click(object sender, EventArgs e)
        {
            if (checkEditTreeRef3.Checked)
            {
                if (string.IsNullOrEmpty(textEditTreeRefRemovePreceedingChars.EditValue.ToString().Trim()))
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Enter one or more chracters in the '" + checkEditTreeRef3.Text + "' box before proceeding.", "Set Tree Reference Structure", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            else if (checkEditTreeRef2.Checked)
            {
                if (Convert.ToInt32(spinEditTreeRefRemoveNumberOfChars.EditValue) == 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Enter the number of characters to remove from the start of the Tree References before proceeding.", "Set Tree Reference Structure", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }

            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();

        }

        private void repositoryItemPopupContainerEdit2_QueryResultValue(object sender, DevExpress.XtraEditors.Controls.QueryResultValueEventArgs e)
        {
            e.Value = popupContainerEditTreeRefStructure_Get_Selected();
        }
            
        private string popupContainerEditTreeRefStructure_Get_Selected()
        {
            string strReturnedValue = "";
            if (checkEditTreeRef1.Checked)
            {
                strReturnedValue = checkEditTreeRef1.Text;
            }
            else if (checkEditTreeRef2.Checked)
            {
                strReturnedValue = checkEditTreeRef2.Text + " " + spinEditTreeRefRemoveNumberOfChars.EditValue.ToString() + " character(s)";
            }
            else  // checkEditTreeRef3.Checked //
            {
                strReturnedValue = checkEditTreeRef3.Text + " " + textEditTreeRefRemovePreceedingChars.EditValue.ToString();
            }
            return strReturnedValue;
        }
     

        #endregion

        private void repositoryItemTextEdit3_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            TextEdit te = (TextEdit)sender;
            if (!string.IsNullOrEmpty(te.EditValue.ToString()))
            {
                if (!te.EditValue.ToString().StartsWith("\\")) XtraMessageBox.Show("The value entered should start with '\\' otherwise you may have problems with exported data.", "GBM Mobile Export - PDA Working Directory Location", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

    

    }
}


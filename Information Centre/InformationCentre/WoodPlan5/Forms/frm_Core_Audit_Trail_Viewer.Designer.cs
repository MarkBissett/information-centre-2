namespace WoodPlan5
{
    partial class frm_Core_Audit_Trail_Viewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Core_Audit_Trail_Viewer));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions7 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject25 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject26 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject27 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject28 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions8 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject29 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject30 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject31 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject32 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions9 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject33 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject34 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject35 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject36 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions10 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject37 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject38 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject39 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject40 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions11 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject41 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject42 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject43 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject44 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions12 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject45 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject46 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject47 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject48 = new DevExpress.Utils.SerializableAppearanceObject();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.gridControlHeader = new DevExpress.XtraGrid.GridControl();
            this.sp01005CoreAuditTrailViewerUniqueTransactionsListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_Common_Functionality = new WoodPlan5.DataSet_Common_Functionality();
            this.gridViewHeader = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTransactionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateChanged = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colUser = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colApplication = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTableName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHostName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colChangeTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colChangeTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFriendlyTableName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colColumnsAltered = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IconStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.dataSet_GC_Core = new WoodPlan5.DataSet_GC_Core();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.popupContainerControlChangeTypeFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnChangeTypeFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp01010CoreAuditTrailViewerChangeTypesListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerControlColumnFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnColumnFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp01009CoreAuditTrailViewerUniqueColumnNamesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTableName3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFriendlyTableName3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colColumnName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerControlTableNameFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnTableFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp01008CoreAuditTrailViewerUniqueTableNamesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTableName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFriendlyTableName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerControlDateRange = new DevExpress.XtraEditors.PopupContainerControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.btnDateRangeOK = new DevExpress.XtraEditors.SimpleButton();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageDetail = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlDetail = new DevExpress.XtraGrid.GridControl();
            this.sp01006CoreAuditTrailViewerTransactionDetailsListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTransactionID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colParentRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateChanged1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colUser1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colApplication1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTableName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHostName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colChangeTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colColumnName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOldValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colNewValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSQL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colChangeTypeDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFriendlyTableName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colParentRecordDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHTML1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.xtraTabPageSQL = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlSQL = new DevExpress.XtraGrid.GridControl();
            this.sp01007CoreAuditTrailViewerTransactionDetailsSQLBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewSQL = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHTML2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dataSet_GC_Summer_Core = new WoodPlan5.DataSet_GC_Summer_Core();
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bsiCreateRollbackSQL = new DevExpress.XtraBars.BarSubItem();
            this.bbiRollbackSQLFile = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRollbackSQLClipboard = new DevExpress.XtraBars.BarButtonItem();
            this.toolTipController1 = new DevExpress.Utils.ToolTipController(this.components);
            this.repositoryItemPopupContainerEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.dockPanel1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.hideContainerLeft = new DevExpress.XtraBars.Docking.AutoHideContainer();
            this.dockPanelFilters = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.checkEditNewValueSucceedingWildcard = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditNewValueProceedingWildcard = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditOldValueSucceedingWildcard = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditOldValueProceedingWildcard = new DevExpress.XtraEditors.CheckEdit();
            this.popupContainerEditChangeTypeFilter = new DevExpress.XtraEditors.PopupContainerEdit();
            this.buttonEditHostNameFilter = new DevExpress.XtraEditors.ButtonEdit();
            this.buttonEditApplicationFilter = new DevExpress.XtraEditors.ButtonEdit();
            this.checkEditApplyFilterToDetail = new DevExpress.XtraEditors.CheckEdit();
            this.memoExEditNewValue = new DevExpress.XtraEditors.MemoExEdit();
            this.memoExEditOldValue = new DevExpress.XtraEditors.MemoExEdit();
            this.popupContainerEditColumnFilter = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerEditTableFilter = new DevExpress.XtraEditors.PopupContainerEdit();
            this.btnLoad = new DevExpress.XtraEditors.SimpleButton();
            this.btnClearAllFilters = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerEditDateRange = new DevExpress.XtraEditors.PopupContainerEdit();
            this.buttonEditUserFilter = new DevExpress.XtraEditors.ButtonEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp01005_Core_Audit_Trail_Viewer_Unique_Transactions_ListTableAdapter = new WoodPlan5.DataSet_Common_FunctionalityTableAdapters.sp01005_Core_Audit_Trail_Viewer_Unique_Transactions_ListTableAdapter();
            this.sp01006_Core_Audit_Trail_Viewer_Transaction_Details_ListTableAdapter = new WoodPlan5.DataSet_Common_FunctionalityTableAdapters.sp01006_Core_Audit_Trail_Viewer_Transaction_Details_ListTableAdapter();
            this.sp01007_Core_Audit_Trail_Viewer_Transaction_Details_SQLTableAdapter = new WoodPlan5.DataSet_Common_FunctionalityTableAdapters.sp01007_Core_Audit_Trail_Viewer_Transaction_Details_SQLTableAdapter();
            this.sp01008_Core_Audit_Trail_Viewer_Unique_Table_NamesTableAdapter = new WoodPlan5.DataSet_Common_FunctionalityTableAdapters.sp01008_Core_Audit_Trail_Viewer_Unique_Table_NamesTableAdapter();
            this.sp01009_Core_Audit_Trail_Viewer_Unique_Column_NamesTableAdapter = new WoodPlan5.DataSet_Common_FunctionalityTableAdapters.sp01009_Core_Audit_Trail_Viewer_Unique_Column_NamesTableAdapter();
            this.sp01010_Core_Audit_Trail_Viewer_Change_Types_ListTableAdapter = new WoodPlan5.DataSet_Common_FunctionalityTableAdapters.sp01010_Core_Audit_Trail_Viewer_Change_Types_ListTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01005CoreAuditTrailViewerUniqueTransactionsListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Common_Functionality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlChangeTypeFilter)).BeginInit();
            this.popupContainerControlChangeTypeFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01010CoreAuditTrailViewerChangeTypesListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlColumnFilter)).BeginInit();
            this.popupContainerControlColumnFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01009CoreAuditTrailViewerUniqueColumnNamesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlTableNameFilter)).BeginInit();
            this.popupContainerControlTableNameFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01008CoreAuditTrailViewerUniqueTableNamesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).BeginInit();
            this.popupContainerControlDateRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPageDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01006CoreAuditTrailViewerTransactionDetailsListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML1)).BeginInit();
            this.xtraTabPageSQL.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSQL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01007CoreAuditTrailViewerTransactionDetailsSQLBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSQL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Summer_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockPanel1)).BeginInit();
            this.hideContainerLeft.SuspendLayout();
            this.dockPanelFilters.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditNewValueSucceedingWildcard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditNewValueProceedingWildcard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditOldValueSucceedingWildcard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditOldValueProceedingWildcard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditChangeTypeFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditHostNameFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditApplicationFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditApplyFilterToDetail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEditNewValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEditOldValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditColumnFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditTableFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditDateRange.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditUserFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1027, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 577);
            this.barDockControlBottom.Size = new System.Drawing.Size(1027, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 577);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1027, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 577);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.standaloneBarDockControl1);
            this.barManager1.DockManager = this.dockPanel1;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiRefresh,
            this.bsiCreateRollbackSQL,
            this.bbiRollbackSQLFile,
            this.bbiRollbackSQLClipboard});
            this.barManager1.MaxItemId = 68;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEdit1});
            this.barManager1.SharedImageCollectionImageSizeMode = DevExpress.Utils.SharedImageCollectionImageSizeMode.UseImageSize;
            this.barManager1.HighlightedLinkChanged += new DevExpress.XtraBars.HighlightedLinkChangedEventHandler(this.barManager1_HighlightedLinkChanged);
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridControlHeader
            // 
            this.gridControlHeader.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControlHeader.DataSource = this.sp01005CoreAuditTrailViewerUniqueTransactionsListBindingSource;
            this.gridControlHeader.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlHeader.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlHeader.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlHeader.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlHeader.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlHeader.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlHeader.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlHeader.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlHeader.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlHeader.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlHeader.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlHeader_EmbeddedNavigator_ButtonClick);
            this.gridControlHeader.Location = new System.Drawing.Point(-1, 43);
            this.gridControlHeader.MainView = this.gridViewHeader;
            this.gridControlHeader.MenuManager = this.barManager1;
            this.gridControlHeader.Name = "gridControlHeader";
            this.gridControlHeader.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1,
            this.repositoryItemPictureEdit1});
            this.gridControlHeader.Size = new System.Drawing.Size(980, 296);
            this.gridControlHeader.TabIndex = 4;
            this.gridControlHeader.UseEmbeddedNavigator = true;
            this.gridControlHeader.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewHeader});
            // 
            // sp01005CoreAuditTrailViewerUniqueTransactionsListBindingSource
            // 
            this.sp01005CoreAuditTrailViewerUniqueTransactionsListBindingSource.DataMember = "sp01005_Core_Audit_Trail_Viewer_Unique_Transactions_List";
            this.sp01005CoreAuditTrailViewerUniqueTransactionsListBindingSource.DataSource = this.dataSet_Common_Functionality;
            // 
            // dataSet_Common_Functionality
            // 
            this.dataSet_Common_Functionality.DataSetName = "DataSet_Common_Functionality";
            this.dataSet_Common_Functionality.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridViewHeader
            // 
            this.gridViewHeader.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTransactionID,
            this.colRecordID,
            this.colDateChanged,
            this.colUser,
            this.colApplication,
            this.colTableName,
            this.colHostName,
            this.colChangeTypeID,
            this.colChangeTypeDescription,
            this.colFriendlyTableName,
            this.colColumnsAltered,
            this.IconStatus});
            this.gridViewHeader.GridControl = this.gridControlHeader;
            this.gridViewHeader.Name = "gridViewHeader";
            this.gridViewHeader.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewHeader.OptionsFind.AlwaysVisible = true;
            this.gridViewHeader.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewHeader.OptionsLayout.StoreAppearance = true;
            this.gridViewHeader.OptionsLayout.StoreFormatRules = true;
            this.gridViewHeader.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewHeader.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewHeader.OptionsSelection.MultiSelect = true;
            this.gridViewHeader.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewHeader.OptionsView.ColumnAutoWidth = false;
            this.gridViewHeader.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewHeader.OptionsView.ShowGroupPanel = false;
            this.gridViewHeader.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateChanged, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colFriendlyTableName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewHeader.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridViewHeader_CustomDrawCell);
            this.gridViewHeader.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewHeader_CustomRowCellEdit);
            this.gridViewHeader.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewHeader.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewHeader.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewHeader.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewHeader.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewHeader.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.gridViewHeader_CustomUnboundColumnData);
            this.gridViewHeader.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewHeader_MouseUp);
            this.gridViewHeader.DoubleClick += new System.EventHandler(this.gridViewHeader_DoubleClick);
            this.gridViewHeader.GotFocus += new System.EventHandler(this.gridViewHeader_GotFocus);
            // 
            // colTransactionID
            // 
            this.colTransactionID.Caption = "Transaction ID";
            this.colTransactionID.FieldName = "TransactionID";
            this.colTransactionID.Name = "colTransactionID";
            this.colTransactionID.OptionsColumn.AllowEdit = false;
            this.colTransactionID.OptionsColumn.AllowFocus = false;
            this.colTransactionID.OptionsColumn.ReadOnly = true;
            this.colTransactionID.Visible = true;
            this.colTransactionID.VisibleIndex = 9;
            this.colTransactionID.Width = 234;
            // 
            // colRecordID
            // 
            this.colRecordID.Caption = "Record ID";
            this.colRecordID.FieldName = "RecordID";
            this.colRecordID.Name = "colRecordID";
            this.colRecordID.OptionsColumn.AllowEdit = false;
            this.colRecordID.OptionsColumn.AllowFocus = false;
            this.colRecordID.OptionsColumn.ReadOnly = true;
            this.colRecordID.Visible = true;
            this.colRecordID.VisibleIndex = 5;
            // 
            // colDateChanged
            // 
            this.colDateChanged.Caption = "Date Changed";
            this.colDateChanged.ColumnEdit = this.repositoryItemTextEdit1;
            this.colDateChanged.FieldName = "DateChanged";
            this.colDateChanged.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colDateChanged.Name = "colDateChanged";
            this.colDateChanged.OptionsColumn.AllowEdit = false;
            this.colDateChanged.OptionsColumn.AllowFocus = false;
            this.colDateChanged.OptionsColumn.ReadOnly = true;
            this.colDateChanged.Visible = true;
            this.colDateChanged.VisibleIndex = 1;
            this.colDateChanged.Width = 134;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "dd/MM/yyyy HH:mm:ss.fff";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colUser
            // 
            this.colUser.Caption = "Changed By User";
            this.colUser.FieldName = "User";
            this.colUser.Name = "colUser";
            this.colUser.OptionsColumn.AllowEdit = false;
            this.colUser.OptionsColumn.AllowFocus = false;
            this.colUser.OptionsColumn.ReadOnly = true;
            this.colUser.Visible = true;
            this.colUser.VisibleIndex = 6;
            this.colUser.Width = 165;
            // 
            // colApplication
            // 
            this.colApplication.Caption = "Application Used";
            this.colApplication.FieldName = "Application";
            this.colApplication.Name = "colApplication";
            this.colApplication.OptionsColumn.AllowEdit = false;
            this.colApplication.OptionsColumn.AllowFocus = false;
            this.colApplication.OptionsColumn.ReadOnly = true;
            this.colApplication.Visible = true;
            this.colApplication.VisibleIndex = 7;
            this.colApplication.Width = 166;
            // 
            // colTableName
            // 
            this.colTableName.Caption = "Actual Table Name ";
            this.colTableName.FieldName = "TableName";
            this.colTableName.Name = "colTableName";
            this.colTableName.OptionsColumn.AllowEdit = false;
            this.colTableName.OptionsColumn.AllowFocus = false;
            this.colTableName.OptionsColumn.ReadOnly = true;
            this.colTableName.Width = 111;
            // 
            // colHostName
            // 
            this.colHostName.Caption = "Host Name";
            this.colHostName.FieldName = "HostName";
            this.colHostName.Name = "colHostName";
            this.colHostName.OptionsColumn.AllowEdit = false;
            this.colHostName.OptionsColumn.AllowFocus = false;
            this.colHostName.OptionsColumn.ReadOnly = true;
            this.colHostName.Visible = true;
            this.colHostName.VisibleIndex = 8;
            this.colHostName.Width = 98;
            // 
            // colChangeTypeID
            // 
            this.colChangeTypeID.Caption = "Change Type ID";
            this.colChangeTypeID.FieldName = "ChangeTypeID";
            this.colChangeTypeID.Name = "colChangeTypeID";
            this.colChangeTypeID.OptionsColumn.AllowEdit = false;
            this.colChangeTypeID.OptionsColumn.AllowFocus = false;
            this.colChangeTypeID.OptionsColumn.ReadOnly = true;
            this.colChangeTypeID.Width = 97;
            // 
            // colChangeTypeDescription
            // 
            this.colChangeTypeDescription.Caption = "Change Type";
            this.colChangeTypeDescription.FieldName = "ChangeTypeDescription";
            this.colChangeTypeDescription.Name = "colChangeTypeDescription";
            this.colChangeTypeDescription.OptionsColumn.AllowEdit = false;
            this.colChangeTypeDescription.OptionsColumn.AllowFocus = false;
            this.colChangeTypeDescription.OptionsColumn.ReadOnly = true;
            this.colChangeTypeDescription.Visible = true;
            this.colChangeTypeDescription.VisibleIndex = 3;
            this.colChangeTypeDescription.Width = 83;
            // 
            // colFriendlyTableName
            // 
            this.colFriendlyTableName.Caption = "Table Name";
            this.colFriendlyTableName.FieldName = "FriendlyTableName";
            this.colFriendlyTableName.Name = "colFriendlyTableName";
            this.colFriendlyTableName.OptionsColumn.AllowEdit = false;
            this.colFriendlyTableName.OptionsColumn.AllowFocus = false;
            this.colFriendlyTableName.OptionsColumn.ReadOnly = true;
            this.colFriendlyTableName.Visible = true;
            this.colFriendlyTableName.VisibleIndex = 2;
            this.colFriendlyTableName.Width = 147;
            // 
            // colColumnsAltered
            // 
            this.colColumnsAltered.Caption = "Columns Altered";
            this.colColumnsAltered.FieldName = "ColumnsAltered";
            this.colColumnsAltered.Name = "colColumnsAltered";
            this.colColumnsAltered.OptionsColumn.AllowEdit = false;
            this.colColumnsAltered.OptionsColumn.AllowFocus = false;
            this.colColumnsAltered.OptionsColumn.ReadOnly = true;
            this.colColumnsAltered.Visible = true;
            this.colColumnsAltered.VisibleIndex = 4;
            this.colColumnsAltered.Width = 97;
            // 
            // IconStatus
            // 
            this.IconStatus.Caption = "Icon";
            this.IconStatus.ColumnEdit = this.repositoryItemPictureEdit1;
            this.IconStatus.FieldName = "IconStatus";
            this.IconStatus.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.IconStatus.Name = "IconStatus";
            this.IconStatus.OptionsColumn.AllowEdit = false;
            this.IconStatus.OptionsColumn.AllowFocus = false;
            this.IconStatus.OptionsColumn.AllowSize = false;
            this.IconStatus.OptionsColumn.FixedWidth = true;
            this.IconStatus.OptionsColumn.ReadOnly = true;
            this.IconStatus.OptionsColumn.ShowCaption = false;
            this.IconStatus.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.IconStatus.Visible = true;
            this.IconStatus.VisibleIndex = 0;
            this.IconStatus.Width = 20;
            // 
            // repositoryItemPictureEdit1
            // 
            this.repositoryItemPictureEdit1.Name = "repositoryItemPictureEdit1";
            this.repositoryItemPictureEdit1.ReadOnly = true;
            this.repositoryItemPictureEdit1.ShowMenu = false;
            // 
            // dataSet_GC_Core
            // 
            this.dataSet_GC_Core.DataSetName = "DataSet_GC_Core";
            this.dataSet_GC_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(23, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl1.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControlChangeTypeFilter);
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControlColumnFilter);
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControlTableNameFilter);
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControlDateRange);
            this.splitContainerControl1.Panel1.Controls.Add(this.gridControlHeader);
            this.splitContainerControl1.Panel1.Controls.Add(this.standaloneBarDockControl1);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Transactions";
            this.splitContainerControl1.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl1.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel2.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl1.Panel2.Controls.Add(this.xtraTabControl1);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "Transaction Details";
            this.splitContainerControl1.Size = new System.Drawing.Size(1004, 577);
            this.splitContainerControl1.SplitterPosition = 229;
            this.splitContainerControl1.TabIndex = 5;
            this.splitContainerControl1.Text = "splitContainerControl1";
            this.splitContainerControl1.SplitGroupPanelCollapsed += new DevExpress.XtraEditors.SplitGroupPanelCollapsedEventHandler(this.splitContainerControl1_SplitGroupPanelCollapsed);
            // 
            // popupContainerControlChangeTypeFilter
            // 
            this.popupContainerControlChangeTypeFilter.Controls.Add(this.btnChangeTypeFilterOK);
            this.popupContainerControlChangeTypeFilter.Controls.Add(this.gridControl1);
            this.popupContainerControlChangeTypeFilter.Location = new System.Drawing.Point(278, 156);
            this.popupContainerControlChangeTypeFilter.Name = "popupContainerControlChangeTypeFilter";
            this.popupContainerControlChangeTypeFilter.Size = new System.Drawing.Size(131, 135);
            this.popupContainerControlChangeTypeFilter.TabIndex = 24;
            // 
            // btnChangeTypeFilterOK
            // 
            this.btnChangeTypeFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnChangeTypeFilterOK.Location = new System.Drawing.Point(3, 112);
            this.btnChangeTypeFilterOK.Name = "btnChangeTypeFilterOK";
            this.btnChangeTypeFilterOK.Size = new System.Drawing.Size(50, 20);
            this.btnChangeTypeFilterOK.TabIndex = 18;
            this.btnChangeTypeFilterOK.Text = "OK";
            this.btnChangeTypeFilterOK.Click += new System.EventHandler(this.btnChangeTypeFilterOK_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.DataSource = this.sp01010CoreAuditTrailViewerChangeTypesListBindingSource;
            this.gridControl1.Location = new System.Drawing.Point(3, 3);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(125, 107);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp01010CoreAuditTrailViewerChangeTypesListBindingSource
            // 
            this.sp01010CoreAuditTrailViewerChangeTypesListBindingSource.DataMember = "sp01010_Core_Audit_Trail_Viewer_Change_Types_List";
            this.sp01010CoreAuditTrailViewerChangeTypesListBindingSource.DataSource = this.dataSet_Common_Functionality;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID,
            this.colDescription,
            this.colOrder});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colID
            // 
            this.colID.Caption = "ID";
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.OptionsColumn.AllowEdit = false;
            this.colID.OptionsColumn.AllowFocus = false;
            this.colID.OptionsColumn.ReadOnly = true;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Change Type";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 140;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "Order";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            this.colOrder.Width = 60;
            // 
            // popupContainerControlColumnFilter
            // 
            this.popupContainerControlColumnFilter.Controls.Add(this.btnColumnFilterOK);
            this.popupContainerControlColumnFilter.Controls.Add(this.gridControl5);
            this.popupContainerControlColumnFilter.Location = new System.Drawing.Point(141, 156);
            this.popupContainerControlColumnFilter.Name = "popupContainerControlColumnFilter";
            this.popupContainerControlColumnFilter.Size = new System.Drawing.Size(131, 135);
            this.popupContainerControlColumnFilter.TabIndex = 22;
            // 
            // btnColumnFilterOK
            // 
            this.btnColumnFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnColumnFilterOK.Location = new System.Drawing.Point(3, 112);
            this.btnColumnFilterOK.Name = "btnColumnFilterOK";
            this.btnColumnFilterOK.Size = new System.Drawing.Size(50, 20);
            this.btnColumnFilterOK.TabIndex = 18;
            this.btnColumnFilterOK.Text = "OK";
            this.btnColumnFilterOK.Click += new System.EventHandler(this.btnColumnFilterOK_Click);
            // 
            // gridControl5
            // 
            this.gridControl5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl5.DataSource = this.sp01009CoreAuditTrailViewerUniqueColumnNamesBindingSource;
            this.gridControl5.Location = new System.Drawing.Point(3, 3);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.Size = new System.Drawing.Size(125, 107);
            this.gridControl5.TabIndex = 1;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp01009CoreAuditTrailViewerUniqueColumnNamesBindingSource
            // 
            this.sp01009CoreAuditTrailViewerUniqueColumnNamesBindingSource.DataMember = "sp01009_Core_Audit_Trail_Viewer_Unique_Column_Names";
            this.sp01009CoreAuditTrailViewerUniqueColumnNamesBindingSource.DataSource = this.dataSet_Common_Functionality;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTableName3,
            this.colFriendlyTableName3,
            this.colColumnName1});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.GroupCount = 1;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsFind.AlwaysVisible = true;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colFriendlyTableName3, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colColumnName1, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colTableName3
            // 
            this.colTableName3.Caption = "Internal Table Name";
            this.colTableName3.FieldName = "TableName";
            this.colTableName3.Name = "colTableName3";
            this.colTableName3.OptionsColumn.AllowEdit = false;
            this.colTableName3.OptionsColumn.AllowFocus = false;
            this.colTableName3.OptionsColumn.ReadOnly = true;
            this.colTableName3.Width = 116;
            // 
            // colFriendlyTableName3
            // 
            this.colFriendlyTableName3.Caption = "Table Name";
            this.colFriendlyTableName3.FieldName = "FriendlyTableName";
            this.colFriendlyTableName3.Name = "colFriendlyTableName3";
            this.colFriendlyTableName3.OptionsColumn.AllowEdit = false;
            this.colFriendlyTableName3.OptionsColumn.AllowFocus = false;
            this.colFriendlyTableName3.OptionsColumn.ReadOnly = true;
            this.colFriendlyTableName3.Visible = true;
            this.colFriendlyTableName3.VisibleIndex = 0;
            this.colFriendlyTableName3.Width = 172;
            // 
            // colColumnName1
            // 
            this.colColumnName1.Caption = "Column Name";
            this.colColumnName1.FieldName = "ColumnName";
            this.colColumnName1.Name = "colColumnName1";
            this.colColumnName1.OptionsColumn.AllowEdit = false;
            this.colColumnName1.OptionsColumn.AllowFocus = false;
            this.colColumnName1.OptionsColumn.ReadOnly = true;
            this.colColumnName1.Visible = true;
            this.colColumnName1.VisibleIndex = 0;
            this.colColumnName1.Width = 216;
            // 
            // popupContainerControlTableNameFilter
            // 
            this.popupContainerControlTableNameFilter.Controls.Add(this.btnTableFilterOK);
            this.popupContainerControlTableNameFilter.Controls.Add(this.gridControl3);
            this.popupContainerControlTableNameFilter.Location = new System.Drawing.Point(4, 156);
            this.popupContainerControlTableNameFilter.Name = "popupContainerControlTableNameFilter";
            this.popupContainerControlTableNameFilter.Size = new System.Drawing.Size(131, 135);
            this.popupContainerControlTableNameFilter.TabIndex = 21;
            // 
            // btnTableFilterOK
            // 
            this.btnTableFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnTableFilterOK.Location = new System.Drawing.Point(3, 112);
            this.btnTableFilterOK.Name = "btnTableFilterOK";
            this.btnTableFilterOK.Size = new System.Drawing.Size(50, 20);
            this.btnTableFilterOK.TabIndex = 18;
            this.btnTableFilterOK.Text = "OK";
            this.btnTableFilterOK.Click += new System.EventHandler(this.btnTableFilterOK_Click);
            // 
            // gridControl3
            // 
            this.gridControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl3.DataSource = this.sp01008CoreAuditTrailViewerUniqueTableNamesBindingSource;
            this.gridControl3.Location = new System.Drawing.Point(3, 3);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.Size = new System.Drawing.Size(125, 107);
            this.gridControl3.TabIndex = 1;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp01008CoreAuditTrailViewerUniqueTableNamesBindingSource
            // 
            this.sp01008CoreAuditTrailViewerUniqueTableNamesBindingSource.DataMember = "sp01008_Core_Audit_Trail_Viewer_Unique_Table_Names";
            this.sp01008CoreAuditTrailViewerUniqueTableNamesBindingSource.DataSource = this.dataSet_Common_Functionality;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTableName2,
            this.colFriendlyTableName2});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsFind.AlwaysVisible = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            // 
            // colTableName2
            // 
            this.colTableName2.Caption = "Table Name Internal";
            this.colTableName2.FieldName = "TableName";
            this.colTableName2.Name = "colTableName2";
            this.colTableName2.OptionsColumn.AllowEdit = false;
            this.colTableName2.OptionsColumn.AllowFocus = false;
            this.colTableName2.OptionsColumn.ReadOnly = true;
            this.colTableName2.Width = 116;
            // 
            // colFriendlyTableName2
            // 
            this.colFriendlyTableName2.Caption = "Table Name";
            this.colFriendlyTableName2.FieldName = "FriendlyTableName";
            this.colFriendlyTableName2.Name = "colFriendlyTableName2";
            this.colFriendlyTableName2.OptionsColumn.AllowEdit = false;
            this.colFriendlyTableName2.OptionsColumn.AllowFocus = false;
            this.colFriendlyTableName2.OptionsColumn.ReadOnly = true;
            this.colFriendlyTableName2.Visible = true;
            this.colFriendlyTableName2.VisibleIndex = 0;
            this.colFriendlyTableName2.Width = 184;
            // 
            // popupContainerControlDateRange
            // 
            this.popupContainerControlDateRange.Controls.Add(this.groupControl1);
            this.popupContainerControlDateRange.Controls.Add(this.btnDateRangeOK);
            this.popupContainerControlDateRange.Location = new System.Drawing.Point(415, 156);
            this.popupContainerControlDateRange.Name = "popupContainerControlDateRange";
            this.popupContainerControlDateRange.Size = new System.Drawing.Size(200, 105);
            this.popupContainerControlDateRange.TabIndex = 17;
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.dateEditFromDate);
            this.groupControl1.Controls.Add(this.dateEditToDate);
            this.groupControl1.Location = new System.Drawing.Point(3, 3);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(194, 76);
            this.groupControl1.TabIndex = 20;
            this.groupControl1.Text = "Date Range  [Transaction Date]";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(6, 53);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(16, 13);
            this.labelControl2.TabIndex = 13;
            this.labelControl2.Text = "To:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(6, 27);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(28, 13);
            this.labelControl1.TabIndex = 12;
            this.labelControl1.Text = "From:";
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(40, 24);
            this.dateEditFromDate.MenuManager = this.barManager1;
            this.dateEditFromDate.Name = "dateEditFromDate";
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditFromDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditFromDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullValuePrompt = "No Date";
            this.dateEditFromDate.Size = new System.Drawing.Size(149, 20);
            this.dateEditFromDate.TabIndex = 10;
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(40, 50);
            this.dateEditToDate.MenuManager = this.barManager1;
            this.dateEditToDate.Name = "dateEditToDate";
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditToDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditToDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullValuePrompt = "No Date";
            this.dateEditToDate.Size = new System.Drawing.Size(149, 20);
            this.dateEditToDate.TabIndex = 11;
            // 
            // btnDateRangeOK
            // 
            this.btnDateRangeOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDateRangeOK.Location = new System.Drawing.Point(3, 82);
            this.btnDateRangeOK.Name = "btnDateRangeOK";
            this.btnDateRangeOK.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnDateRangeOK.Size = new System.Drawing.Size(50, 20);
            this.btnDateRangeOK.TabIndex = 19;
            this.btnDateRangeOK.Text = "OK";
            this.btnDateRangeOK.Click += new System.EventHandler(this.btnDateRangeOK_Click);
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 1);
            this.standaloneBarDockControl1.Manager = this.barManager1;
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(981, 42);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPageDetail;
            this.xtraTabControl1.Size = new System.Drawing.Size(979, 226);
            this.xtraTabControl1.TabIndex = 1;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageDetail,
            this.xtraTabPageSQL});
            // 
            // xtraTabPageDetail
            // 
            this.xtraTabPageDetail.Controls.Add(this.gridControlDetail);
            this.xtraTabPageDetail.Name = "xtraTabPageDetail";
            this.xtraTabPageDetail.Size = new System.Drawing.Size(974, 200);
            this.xtraTabPageDetail.Text = "Detail";
            // 
            // gridControlDetail
            // 
            this.gridControlDetail.DataSource = this.sp01006CoreAuditTrailViewerTransactionDetailsListBindingSource;
            this.gridControlDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlDetail.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlDetail.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlDetail.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlDetail.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlDetail.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlDetail.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlDetail.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlDetail.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlDetail.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlDetail.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlDetail.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlDetail_EmbeddedNavigator_ButtonClick);
            this.gridControlDetail.Location = new System.Drawing.Point(0, 0);
            this.gridControlDetail.MainView = this.gridViewDetail;
            this.gridControlDetail.MenuManager = this.barManager1;
            this.gridControlDetail.Name = "gridControlDetail";
            this.gridControlDetail.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit5,
            this.repositoryItemTextEdit2,
            this.repositoryItemTextEditHTML1});
            this.gridControlDetail.Size = new System.Drawing.Size(974, 200);
            this.gridControlDetail.TabIndex = 26;
            this.gridControlDetail.UseEmbeddedNavigator = true;
            this.gridControlDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDetail});
            // 
            // sp01006CoreAuditTrailViewerTransactionDetailsListBindingSource
            // 
            this.sp01006CoreAuditTrailViewerTransactionDetailsListBindingSource.DataMember = "sp01006_Core_Audit_Trail_Viewer_Transaction_Details_List";
            this.sp01006CoreAuditTrailViewerTransactionDetailsListBindingSource.DataSource = this.dataSet_Common_Functionality;
            // 
            // gridViewDetail
            // 
            this.gridViewDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTransactionID1,
            this.colRecordID1,
            this.colParentRecordID,
            this.colDateChanged1,
            this.colUser1,
            this.colApplication1,
            this.colTableName1,
            this.colHostName1,
            this.colChangeTypeID1,
            this.colColumnName,
            this.colOldValue,
            this.colNewValue,
            this.colSQL,
            this.colChangeTypeDescription1,
            this.colFriendlyTableName1,
            this.colParentRecordDescription});
            this.gridViewDetail.GridControl = this.gridControlDetail;
            this.gridViewDetail.GroupCount = 1;
            this.gridViewDetail.Name = "gridViewDetail";
            this.gridViewDetail.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewDetail.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewDetail.OptionsLayout.StoreAppearance = true;
            this.gridViewDetail.OptionsLayout.StoreFormatRules = true;
            this.gridViewDetail.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewDetail.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewDetail.OptionsSelection.MultiSelect = true;
            this.gridViewDetail.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewDetail.OptionsView.ColumnAutoWidth = false;
            this.gridViewDetail.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewDetail.OptionsView.ShowGroupPanel = false;
            this.gridViewDetail.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colParentRecordDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colColumnName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewDetail.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewDetail.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewDetail.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewDetail.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewDetail.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewDetail.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewDetail_MouseUp);
            this.gridViewDetail.DoubleClick += new System.EventHandler(this.gridViewDetail_DoubleClick);
            this.gridViewDetail.GotFocus += new System.EventHandler(this.gridViewDetail_GotFocus);
            // 
            // colTransactionID1
            // 
            this.colTransactionID1.Caption = "Transaction ID";
            this.colTransactionID1.FieldName = "TransactionID";
            this.colTransactionID1.Name = "colTransactionID1";
            this.colTransactionID1.OptionsColumn.AllowEdit = false;
            this.colTransactionID1.OptionsColumn.AllowFocus = false;
            this.colTransactionID1.OptionsColumn.ReadOnly = true;
            this.colTransactionID1.Width = 89;
            // 
            // colRecordID1
            // 
            this.colRecordID1.Caption = "Audit ID";
            this.colRecordID1.FieldName = "RecordID";
            this.colRecordID1.Name = "colRecordID1";
            this.colRecordID1.OptionsColumn.AllowEdit = false;
            this.colRecordID1.OptionsColumn.AllowFocus = false;
            this.colRecordID1.OptionsColumn.ReadOnly = true;
            this.colRecordID1.Width = 64;
            // 
            // colParentRecordID
            // 
            this.colParentRecordID.Caption = "Parent Record ID";
            this.colParentRecordID.FieldName = "ParentRecordID";
            this.colParentRecordID.Name = "colParentRecordID";
            this.colParentRecordID.OptionsColumn.AllowEdit = false;
            this.colParentRecordID.OptionsColumn.AllowFocus = false;
            this.colParentRecordID.OptionsColumn.ReadOnly = true;
            this.colParentRecordID.Width = 102;
            // 
            // colDateChanged1
            // 
            this.colDateChanged1.Caption = "Date Changed";
            this.colDateChanged1.ColumnEdit = this.repositoryItemTextEdit2;
            this.colDateChanged1.FieldName = "DateChanged";
            this.colDateChanged1.Name = "colDateChanged1";
            this.colDateChanged1.OptionsColumn.AllowEdit = false;
            this.colDateChanged1.OptionsColumn.AllowFocus = false;
            this.colDateChanged1.OptionsColumn.ReadOnly = true;
            this.colDateChanged1.Width = 139;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Mask.EditMask = "dd/MM/yyy HH:mm:ss.fff";
            this.repositoryItemTextEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // colUser1
            // 
            this.colUser1.Caption = "Changed By User";
            this.colUser1.FieldName = "User";
            this.colUser1.Name = "colUser1";
            this.colUser1.OptionsColumn.AllowEdit = false;
            this.colUser1.OptionsColumn.AllowFocus = false;
            this.colUser1.OptionsColumn.ReadOnly = true;
            this.colUser1.Width = 166;
            // 
            // colApplication1
            // 
            this.colApplication1.Caption = "Application Used";
            this.colApplication1.FieldName = "Application";
            this.colApplication1.Name = "colApplication1";
            this.colApplication1.OptionsColumn.AllowEdit = false;
            this.colApplication1.OptionsColumn.AllowFocus = false;
            this.colApplication1.OptionsColumn.ReadOnly = true;
            this.colApplication1.Width = 178;
            // 
            // colTableName1
            // 
            this.colTableName1.Caption = "Actual Table Name";
            this.colTableName1.FieldName = "TableName";
            this.colTableName1.Name = "colTableName1";
            this.colTableName1.OptionsColumn.AllowEdit = false;
            this.colTableName1.OptionsColumn.AllowFocus = false;
            this.colTableName1.OptionsColumn.ReadOnly = true;
            this.colTableName1.Width = 213;
            // 
            // colHostName1
            // 
            this.colHostName1.Caption = "Host Name";
            this.colHostName1.FieldName = "HostName";
            this.colHostName1.Name = "colHostName1";
            this.colHostName1.OptionsColumn.AllowEdit = false;
            this.colHostName1.OptionsColumn.AllowFocus = false;
            this.colHostName1.OptionsColumn.ReadOnly = true;
            this.colHostName1.Width = 172;
            // 
            // colChangeTypeID1
            // 
            this.colChangeTypeID1.Caption = "Change Type ID";
            this.colChangeTypeID1.FieldName = "ChangeTypeID";
            this.colChangeTypeID1.Name = "colChangeTypeID1";
            this.colChangeTypeID1.OptionsColumn.AllowEdit = false;
            this.colChangeTypeID1.OptionsColumn.AllowFocus = false;
            this.colChangeTypeID1.OptionsColumn.ReadOnly = true;
            this.colChangeTypeID1.Width = 97;
            // 
            // colColumnName
            // 
            this.colColumnName.Caption = "Column Name";
            this.colColumnName.FieldName = "ColumnName";
            this.colColumnName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colColumnName.Name = "colColumnName";
            this.colColumnName.OptionsColumn.AllowEdit = false;
            this.colColumnName.OptionsColumn.AllowFocus = false;
            this.colColumnName.OptionsColumn.ReadOnly = true;
            this.colColumnName.Visible = true;
            this.colColumnName.VisibleIndex = 0;
            this.colColumnName.Width = 250;
            // 
            // colOldValue
            // 
            this.colOldValue.Caption = "Old Value";
            this.colOldValue.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.colOldValue.FieldName = "OldValue";
            this.colOldValue.Name = "colOldValue";
            this.colOldValue.OptionsColumn.ReadOnly = true;
            this.colOldValue.Visible = true;
            this.colOldValue.VisibleIndex = 1;
            this.colOldValue.Width = 300;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // colNewValue
            // 
            this.colNewValue.Caption = "New Value";
            this.colNewValue.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.colNewValue.FieldName = "NewValue";
            this.colNewValue.Name = "colNewValue";
            this.colNewValue.OptionsColumn.ReadOnly = true;
            this.colNewValue.Visible = true;
            this.colNewValue.VisibleIndex = 2;
            this.colNewValue.Width = 300;
            // 
            // colSQL
            // 
            this.colSQL.Caption = "SQL";
            this.colSQL.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.colSQL.FieldName = "SQL";
            this.colSQL.Name = "colSQL";
            this.colSQL.OptionsColumn.ReadOnly = true;
            this.colSQL.Width = 182;
            // 
            // colChangeTypeDescription1
            // 
            this.colChangeTypeDescription1.Caption = "Change Type";
            this.colChangeTypeDescription1.FieldName = "ChangeTypeDescription";
            this.colChangeTypeDescription1.Name = "colChangeTypeDescription1";
            this.colChangeTypeDescription1.OptionsColumn.AllowEdit = false;
            this.colChangeTypeDescription1.OptionsColumn.AllowFocus = false;
            this.colChangeTypeDescription1.OptionsColumn.ReadOnly = true;
            this.colChangeTypeDescription1.Width = 102;
            // 
            // colFriendlyTableName1
            // 
            this.colFriendlyTableName1.Caption = "Table Name";
            this.colFriendlyTableName1.FieldName = "FriendlyTableName";
            this.colFriendlyTableName1.Name = "colFriendlyTableName1";
            this.colFriendlyTableName1.OptionsColumn.AllowEdit = false;
            this.colFriendlyTableName1.OptionsColumn.AllowFocus = false;
            this.colFriendlyTableName1.OptionsColumn.ReadOnly = true;
            this.colFriendlyTableName1.Width = 163;
            // 
            // colParentRecordDescription
            // 
            this.colParentRecordDescription.Caption = "Parent Transaction";
            this.colParentRecordDescription.ColumnEdit = this.repositoryItemTextEditHTML1;
            this.colParentRecordDescription.FieldName = "ParentRecordDescription";
            this.colParentRecordDescription.Name = "colParentRecordDescription";
            this.colParentRecordDescription.OptionsColumn.AllowEdit = false;
            this.colParentRecordDescription.OptionsColumn.AllowFocus = false;
            this.colParentRecordDescription.OptionsColumn.ReadOnly = true;
            this.colParentRecordDescription.Visible = true;
            this.colParentRecordDescription.VisibleIndex = 3;
            this.colParentRecordDescription.Width = 360;
            // 
            // repositoryItemTextEditHTML1
            // 
            this.repositoryItemTextEditHTML1.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTextEditHTML1.AutoHeight = false;
            this.repositoryItemTextEditHTML1.Name = "repositoryItemTextEditHTML1";
            // 
            // xtraTabPageSQL
            // 
            this.xtraTabPageSQL.Controls.Add(this.gridControlSQL);
            this.xtraTabPageSQL.Name = "xtraTabPageSQL";
            this.xtraTabPageSQL.Size = new System.Drawing.Size(974, 200);
            this.xtraTabPageSQL.Text = "SQL";
            // 
            // gridControlSQL
            // 
            this.gridControlSQL.DataSource = this.sp01007CoreAuditTrailViewerTransactionDetailsSQLBindingSource;
            this.gridControlSQL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlSQL.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlSQL.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlSQL.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlSQL.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlSQL.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlSQL.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlSQL.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlSQL.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlSQL.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlSQL.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlSQL.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlSQL_EmbeddedNavigator_ButtonClick);
            this.gridControlSQL.Location = new System.Drawing.Point(0, 0);
            this.gridControlSQL.MainView = this.gridViewSQL;
            this.gridControlSQL.MenuManager = this.barManager1;
            this.gridControlSQL.Name = "gridControlSQL";
            this.gridControlSQL.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemTextEditHTML2});
            this.gridControlSQL.Size = new System.Drawing.Size(974, 200);
            this.gridControlSQL.TabIndex = 27;
            this.gridControlSQL.UseEmbeddedNavigator = true;
            this.gridControlSQL.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewSQL});
            // 
            // sp01007CoreAuditTrailViewerTransactionDetailsSQLBindingSource
            // 
            this.sp01007CoreAuditTrailViewerTransactionDetailsSQLBindingSource.DataMember = "sp01007_Core_Audit_Trail_Viewer_Transaction_Details_SQL";
            this.sp01007CoreAuditTrailViewerTransactionDetailsSQLBindingSource.DataSource = this.dataSet_Common_Functionality;
            // 
            // gridViewSQL
            // 
            this.gridViewSQL.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn3,
            this.gridColumn16,
            this.gridColumn19});
            this.gridViewSQL.GridControl = this.gridControlSQL;
            this.gridViewSQL.GroupCount = 1;
            this.gridViewSQL.Name = "gridViewSQL";
            this.gridViewSQL.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewSQL.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewSQL.OptionsLayout.StoreAppearance = true;
            this.gridViewSQL.OptionsLayout.StoreFormatRules = true;
            this.gridViewSQL.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewSQL.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewSQL.OptionsSelection.MultiSelect = true;
            this.gridViewSQL.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewSQL.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewSQL.OptionsView.ShowGroupPanel = false;
            this.gridViewSQL.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn19, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewSQL.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewSQL.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewSQL.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewSQL.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewSQL.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewSQL.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewSQL_MouseUp);
            this.gridViewSQL.DoubleClick += new System.EventHandler(this.gridViewSQL_DoubleClick);
            this.gridViewSQL.GotFocus += new System.EventHandler(this.gridViewSQL_GotFocus);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Transaction ID";
            this.gridColumn1.FieldName = "TransactionID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Width = 89;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Parent Record ID";
            this.gridColumn3.FieldName = "ParentRecordID";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Width = 102;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "SQL";
            this.gridColumn16.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.gridColumn16.FieldName = "SQL";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 0;
            this.gridColumn16.Width = 943;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Parent Transaction";
            this.gridColumn19.ColumnEdit = this.repositoryItemTextEditHTML2;
            this.gridColumn19.FieldName = "ParentRecordDescription";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.OptionsColumn.AllowFocus = false;
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 1;
            this.gridColumn19.Width = 360;
            // 
            // repositoryItemTextEditHTML2
            // 
            this.repositoryItemTextEditHTML2.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTextEditHTML2.AutoHeight = false;
            this.repositoryItemTextEditHTML2.Name = "repositoryItemTextEditHTML2";
            // 
            // dataSet_GC_Summer_Core
            // 
            this.dataSet_GC_Summer_Core.DataSetName = "DataSet_GC_Summer_Core";
            this.dataSet_GC_Summer_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bar1
            // 
            this.bar1.BarName = "GrittingCalloutToolbar";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.FloatLocation = new System.Drawing.Point(519, 261);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefresh),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiCreateRollbackSQL, true)});
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.DisableCustomization = true;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar1.Text = "Gritting Callout Toolbar";
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiRefresh.Caption = "Refresh";
            this.bbiRefresh.Id = 58;
            this.bbiRefresh.ImageOptions.Image = global::WoodPlan5.Properties.Resources.refresh_32x32;
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // bsiCreateRollbackSQL
            // 
            this.bsiCreateRollbackSQL.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bsiCreateRollbackSQL.Caption = "Create Rollback SQL";
            this.bsiCreateRollbackSQL.Id = 65;
            this.bsiCreateRollbackSQL.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Reset2_32x32;
            this.bsiCreateRollbackSQL.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRollbackSQLFile),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRollbackSQLClipboard, true)});
            this.bsiCreateRollbackSQL.Name = "bsiCreateRollbackSQL";
            this.bsiCreateRollbackSQL.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bbiRollbackSQLFile
            // 
            this.bbiRollbackSQLFile.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiRollbackSQLFile.Caption = "Create Rollback SQL in <b>new File</b>";
            this.bbiRollbackSQLFile.Id = 66;
            this.bbiRollbackSQLFile.ImageOptions.ImageIndex = 13;
            this.bbiRollbackSQLFile.Name = "bbiRollbackSQLFile";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Create Rollback SQL in new File - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to create SQL code which can be used to rollback the selected items in t" +
    "he change log.\r\n\r\n<b>Note:</b> The created SQL will be stored in the file you sp" +
    "ecify.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiRollbackSQLFile.SuperTip = superToolTip1;
            this.bbiRollbackSQLFile.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRollbackSQLFile_ItemClick);
            // 
            // bbiRollbackSQLClipboard
            // 
            this.bbiRollbackSQLClipboard.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiRollbackSQLClipboard.Caption = "Create Rollback SQL in <b>Clipboard</b>";
            this.bbiRollbackSQLClipboard.Id = 67;
            this.bbiRollbackSQLClipboard.ImageOptions.ImageIndex = 13;
            this.bbiRollbackSQLClipboard.Name = "bbiRollbackSQLClipboard";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem2.Text = "Create Rollback SQL in Clipboard - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = resources.GetString("toolTipItem2.Text");
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiRollbackSQLClipboard.SuperTip = superToolTip2;
            this.bbiRollbackSQLClipboard.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRollbackSQLClipboard_ItemClick);
            // 
            // toolTipController1
            // 
            this.toolTipController1.CloseOnClick = DevExpress.Utils.DefaultBoolean.True;
            // 
            // repositoryItemPopupContainerEdit1
            // 
            this.repositoryItemPopupContainerEdit1.AutoHeight = false;
            this.repositoryItemPopupContainerEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit1.Name = "repositoryItemPopupContainerEdit1";
            // 
            // dockPanel1
            // 
            this.dockPanel1.AutoHideContainers.AddRange(new DevExpress.XtraBars.Docking.AutoHideContainer[] {
            this.hideContainerLeft});
            this.dockPanel1.Form = this;
            this.dockPanel1.MenuManager = this.barManager1;
            this.dockPanel1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane"});
            // 
            // hideContainerLeft
            // 
            this.hideContainerLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.hideContainerLeft.Controls.Add(this.dockPanelFilters);
            this.hideContainerLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.hideContainerLeft.Location = new System.Drawing.Point(0, 0);
            this.hideContainerLeft.Name = "hideContainerLeft";
            this.hideContainerLeft.Size = new System.Drawing.Size(23, 577);
            // 
            // dockPanelFilters
            // 
            this.dockPanelFilters.Controls.Add(this.dockPanel1_Container);
            this.dockPanelFilters.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelFilters.ID = new System.Guid("74ea37b4-0038-4577-83f7-870045aaedee");
            this.dockPanelFilters.Location = new System.Drawing.Point(0, 0);
            this.dockPanelFilters.Name = "dockPanelFilters";
            this.dockPanelFilters.Options.ShowCloseButton = false;
            this.dockPanelFilters.OriginalSize = new System.Drawing.Size(320, 200);
            this.dockPanelFilters.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelFilters.SavedIndex = 0;
            this.dockPanelFilters.Size = new System.Drawing.Size(320, 577);
            this.dockPanelFilters.Text = "Data Filter";
            this.dockPanelFilters.Visibility = DevExpress.XtraBars.Docking.DockVisibility.AutoHide;
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.layoutControl1);
            this.dockPanel1_Container.Location = new System.Drawing.Point(3, 29);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(314, 545);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.checkEditNewValueSucceedingWildcard);
            this.layoutControl1.Controls.Add(this.checkEditNewValueProceedingWildcard);
            this.layoutControl1.Controls.Add(this.checkEditOldValueSucceedingWildcard);
            this.layoutControl1.Controls.Add(this.checkEditOldValueProceedingWildcard);
            this.layoutControl1.Controls.Add(this.popupContainerEditChangeTypeFilter);
            this.layoutControl1.Controls.Add(this.buttonEditHostNameFilter);
            this.layoutControl1.Controls.Add(this.buttonEditApplicationFilter);
            this.layoutControl1.Controls.Add(this.checkEditApplyFilterToDetail);
            this.layoutControl1.Controls.Add(this.memoExEditNewValue);
            this.layoutControl1.Controls.Add(this.memoExEditOldValue);
            this.layoutControl1.Controls.Add(this.popupContainerEditColumnFilter);
            this.layoutControl1.Controls.Add(this.popupContainerEditTableFilter);
            this.layoutControl1.Controls.Add(this.btnLoad);
            this.layoutControl1.Controls.Add(this.btnClearAllFilters);
            this.layoutControl1.Controls.Add(this.popupContainerEditDateRange);
            this.layoutControl1.Controls.Add(this.buttonEditUserFilter);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1349, 345, 562, 480);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(314, 545);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // checkEditNewValueSucceedingWildcard
            // 
            this.checkEditNewValueSucceedingWildcard.Location = new System.Drawing.Point(105, 372);
            this.checkEditNewValueSucceedingWildcard.MenuManager = this.barManager1;
            this.checkEditNewValueSucceedingWildcard.Name = "checkEditNewValueSucceedingWildcard";
            this.checkEditNewValueSucceedingWildcard.Properties.Caption = "Succeeding";
            this.checkEditNewValueSucceedingWildcard.Properties.ValueChecked = 1;
            this.checkEditNewValueSucceedingWildcard.Properties.ValueUnchecked = 0;
            this.checkEditNewValueSucceedingWildcard.Size = new System.Drawing.Size(75, 19);
            this.checkEditNewValueSucceedingWildcard.StyleController = this.layoutControl1;
            this.checkEditNewValueSucceedingWildcard.TabIndex = 28;
            // 
            // checkEditNewValueProceedingWildcard
            // 
            this.checkEditNewValueProceedingWildcard.Location = new System.Drawing.Point(27, 372);
            this.checkEditNewValueProceedingWildcard.MenuManager = this.barManager1;
            this.checkEditNewValueProceedingWildcard.Name = "checkEditNewValueProceedingWildcard";
            this.checkEditNewValueProceedingWildcard.Properties.Caption = "Proceeding";
            this.checkEditNewValueProceedingWildcard.Properties.ValueChecked = 1;
            this.checkEditNewValueProceedingWildcard.Properties.ValueUnchecked = 0;
            this.checkEditNewValueProceedingWildcard.Size = new System.Drawing.Size(74, 19);
            this.checkEditNewValueProceedingWildcard.StyleController = this.layoutControl1;
            this.checkEditNewValueProceedingWildcard.TabIndex = 27;
            // 
            // checkEditOldValueSucceedingWildcard
            // 
            this.checkEditOldValueSucceedingWildcard.Location = new System.Drawing.Point(105, 279);
            this.checkEditOldValueSucceedingWildcard.MenuManager = this.barManager1;
            this.checkEditOldValueSucceedingWildcard.Name = "checkEditOldValueSucceedingWildcard";
            this.checkEditOldValueSucceedingWildcard.Properties.Caption = "Succeeding";
            this.checkEditOldValueSucceedingWildcard.Properties.ValueChecked = 1;
            this.checkEditOldValueSucceedingWildcard.Properties.ValueUnchecked = 0;
            this.checkEditOldValueSucceedingWildcard.Size = new System.Drawing.Size(75, 19);
            this.checkEditOldValueSucceedingWildcard.StyleController = this.layoutControl1;
            this.checkEditOldValueSucceedingWildcard.TabIndex = 27;
            // 
            // checkEditOldValueProceedingWildcard
            // 
            this.checkEditOldValueProceedingWildcard.Location = new System.Drawing.Point(27, 279);
            this.checkEditOldValueProceedingWildcard.MenuManager = this.barManager1;
            this.checkEditOldValueProceedingWildcard.Name = "checkEditOldValueProceedingWildcard";
            this.checkEditOldValueProceedingWildcard.Properties.Caption = "Proceeding";
            this.checkEditOldValueProceedingWildcard.Properties.ValueChecked = 1;
            this.checkEditOldValueProceedingWildcard.Properties.ValueUnchecked = 0;
            this.checkEditOldValueProceedingWildcard.Size = new System.Drawing.Size(74, 19);
            this.checkEditOldValueProceedingWildcard.StyleController = this.layoutControl1;
            this.checkEditOldValueProceedingWildcard.TabIndex = 26;
            // 
            // popupContainerEditChangeTypeFilter
            // 
            this.popupContainerEditChangeTypeFilter.EditValue = "No Change Type Filter";
            this.popupContainerEditChangeTypeFilter.Location = new System.Drawing.Point(78, 31);
            this.popupContainerEditChangeTypeFilter.MenuManager = this.barManager1;
            this.popupContainerEditChangeTypeFilter.Name = "popupContainerEditChangeTypeFilter";
            this.popupContainerEditChangeTypeFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditChangeTypeFilter.Properties.PopupControl = this.popupContainerControlChangeTypeFilter;
            this.popupContainerEditChangeTypeFilter.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditChangeTypeFilter.Size = new System.Drawing.Size(229, 20);
            this.popupContainerEditChangeTypeFilter.StyleController = this.layoutControl1;
            this.popupContainerEditChangeTypeFilter.TabIndex = 9;
            this.popupContainerEditChangeTypeFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditChangeTypeFilter_QueryResultValue);
            // 
            // buttonEditHostNameFilter
            // 
            this.buttonEditHostNameFilter.EditValue = "No Host Name Filter";
            this.buttonEditHostNameFilter.Location = new System.Drawing.Point(78, 127);
            this.buttonEditHostNameFilter.MenuManager = this.barManager1;
            this.buttonEditHostNameFilter.Name = "buttonEditHostNameFilter";
            this.buttonEditHostNameFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click to open Choose Host Name Screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Clear Filter", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.buttonEditHostNameFilter.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.buttonEditHostNameFilter.Size = new System.Drawing.Size(229, 20);
            this.buttonEditHostNameFilter.StyleController = this.layoutControl1;
            this.buttonEditHostNameFilter.TabIndex = 9;
            this.buttonEditHostNameFilter.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEditHostNameFilter_ButtonClick);
            // 
            // buttonEditApplicationFilter
            // 
            this.buttonEditApplicationFilter.EditValue = "No Application Filter";
            this.buttonEditApplicationFilter.Location = new System.Drawing.Point(78, 103);
            this.buttonEditApplicationFilter.MenuManager = this.barManager1;
            this.buttonEditApplicationFilter.Name = "buttonEditApplicationFilter";
            this.buttonEditApplicationFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Click to open Choose Application Screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Clear Filter", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.buttonEditApplicationFilter.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.buttonEditApplicationFilter.Size = new System.Drawing.Size(229, 20);
            this.buttonEditApplicationFilter.StyleController = this.layoutControl1;
            this.buttonEditApplicationFilter.TabIndex = 8;
            this.buttonEditApplicationFilter.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEditApplicationFilter_ButtonClick);
            // 
            // checkEditApplyFilterToDetail
            // 
            this.checkEditApplyFilterToDetail.Location = new System.Drawing.Point(19, 413);
            this.checkEditApplyFilterToDetail.MenuManager = this.barManager1;
            this.checkEditApplyFilterToDetail.Name = "checkEditApplyFilterToDetail";
            this.checkEditApplyFilterToDetail.Properties.Caption = "Also Apply Filter to Detail Level";
            this.checkEditApplyFilterToDetail.Properties.ValueChecked = 1;
            this.checkEditApplyFilterToDetail.Properties.ValueUnchecked = 0;
            this.checkEditApplyFilterToDetail.Size = new System.Drawing.Size(276, 19);
            this.checkEditApplyFilterToDetail.StyleController = this.layoutControl1;
            this.checkEditApplyFilterToDetail.TabIndex = 25;
            // 
            // memoExEditNewValue
            // 
            this.memoExEditNewValue.Location = new System.Drawing.Point(90, 320);
            this.memoExEditNewValue.MenuManager = this.barManager1;
            this.memoExEditNewValue.Name = "memoExEditNewValue";
            this.memoExEditNewValue.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "", "combo", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Search, "Choose", -1, true, true, false, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "Click to open Choose Value Screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions7, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject25, serializableAppearanceObject26, serializableAppearanceObject27, serializableAppearanceObject28, "Clear Filter", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.memoExEditNewValue.Properties.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.memoExEditNewValue.Properties.ShowIcon = false;
            this.memoExEditNewValue.Size = new System.Drawing.Size(205, 20);
            this.memoExEditNewValue.StyleController = this.layoutControl1;
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image4")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image5")));
            toolTipTitleItem3.Text = "New Value Filter - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "To search for multiple new values, use a comma between each value to search for.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.memoExEditNewValue.SuperTip = superToolTip3;
            this.memoExEditNewValue.TabIndex = 24;
            this.memoExEditNewValue.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.memoExEditNewValue_ButtonClick);
            // 
            // memoExEditOldValue
            // 
            this.memoExEditOldValue.Location = new System.Drawing.Point(90, 227);
            this.memoExEditOldValue.MenuManager = this.barManager1;
            this.memoExEditOldValue.Name = "memoExEditOldValue";
            this.memoExEditOldValue.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, editorButtonImageOptions8, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject29, serializableAppearanceObject30, serializableAppearanceObject31, serializableAppearanceObject32, "", "combo", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Search, "Choose", -1, true, true, false, editorButtonImageOptions9, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject33, serializableAppearanceObject34, serializableAppearanceObject35, serializableAppearanceObject36, "Click to open Choose Value Screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions10, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject37, serializableAppearanceObject38, serializableAppearanceObject39, serializableAppearanceObject40, "Clear Filter", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.memoExEditOldValue.Properties.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.memoExEditOldValue.Properties.ShowIcon = false;
            this.memoExEditOldValue.Size = new System.Drawing.Size(205, 20);
            this.memoExEditOldValue.StyleController = this.layoutControl1;
            toolTipTitleItem4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image6")));
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image7")));
            toolTipTitleItem4.Text = "Old Value Filter - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "To search for multiple old values, use a comma between each value to search for.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.memoExEditOldValue.SuperTip = superToolTip4;
            this.memoExEditOldValue.TabIndex = 23;
            this.memoExEditOldValue.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.memoExEditOldValue_ButtonClick);
            // 
            // popupContainerEditColumnFilter
            // 
            this.popupContainerEditColumnFilter.EditValue = "No Column Filter";
            this.popupContainerEditColumnFilter.Location = new System.Drawing.Point(90, 193);
            this.popupContainerEditColumnFilter.MenuManager = this.barManager1;
            this.popupContainerEditColumnFilter.Name = "popupContainerEditColumnFilter";
            this.popupContainerEditColumnFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditColumnFilter.Properties.PopupControl = this.popupContainerControlColumnFilter;
            this.popupContainerEditColumnFilter.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditColumnFilter.Size = new System.Drawing.Size(205, 20);
            this.popupContainerEditColumnFilter.StyleController = this.layoutControl1;
            this.popupContainerEditColumnFilter.TabIndex = 9;
            this.popupContainerEditColumnFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditColumnFilter_QueryResultValue);
            // 
            // popupContainerEditTableFilter
            // 
            this.popupContainerEditTableFilter.EditValue = "No Table Filter";
            this.popupContainerEditTableFilter.Location = new System.Drawing.Point(78, 55);
            this.popupContainerEditTableFilter.MenuManager = this.barManager1;
            this.popupContainerEditTableFilter.Name = "popupContainerEditTableFilter";
            this.popupContainerEditTableFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditTableFilter.Properties.PopupControl = this.popupContainerControlTableNameFilter;
            this.popupContainerEditTableFilter.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditTableFilter.Size = new System.Drawing.Size(229, 20);
            this.popupContainerEditTableFilter.StyleController = this.layoutControl1;
            this.popupContainerEditTableFilter.TabIndex = 8;
            this.popupContainerEditTableFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditTableFilter_QueryResultValue);
            // 
            // btnLoad
            // 
            this.btnLoad.ImageOptions.Image = global::WoodPlan5.Properties.Resources.refresh_16x16;
            this.btnLoad.Location = new System.Drawing.Point(228, 458);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(79, 22);
            this.btnLoad.StyleController = this.layoutControl1;
            toolTipTitleItem5.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image8")));
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image9")));
            toolTipTitleItem5.Text = "Load Data - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to Load Audit Data.\r\n\r\nOnly data matching the specified Filter Criteria " +
    "(From and To Dates etc) will be loaded.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.btnLoad.SuperTip = superToolTip5;
            this.btnLoad.TabIndex = 18;
            this.btnLoad.Text = "Load Data";
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click_1);
            // 
            // btnClearAllFilters
            // 
            this.btnClearAllFilters.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Filter_Clear_16x16;
            this.btnClearAllFilters.Location = new System.Drawing.Point(7, 458);
            this.btnClearAllFilters.Name = "btnClearAllFilters";
            this.btnClearAllFilters.Size = new System.Drawing.Size(87, 22);
            this.btnClearAllFilters.StyleController = this.layoutControl1;
            toolTipTitleItem6.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image10")));
            toolTipTitleItem6.Appearance.Options.UseImage = true;
            toolTipTitleItem6.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image11")));
            toolTipTitleItem6.Text = "Clear Filters - Information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Click me to clear all filters (except Date Range filter).";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this.btnClearAllFilters.SuperTip = superToolTip6;
            this.btnClearAllFilters.TabIndex = 22;
            this.btnClearAllFilters.Text = "Clear Filters";
            this.btnClearAllFilters.Click += new System.EventHandler(this.btnClearAllFilters_Click);
            // 
            // popupContainerEditDateRange
            // 
            this.popupContainerEditDateRange.EditValue = "No Date Range Filter";
            this.popupContainerEditDateRange.Location = new System.Drawing.Point(78, 7);
            this.popupContainerEditDateRange.MenuManager = this.barManager1;
            this.popupContainerEditDateRange.Name = "popupContainerEditDateRange";
            this.popupContainerEditDateRange.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditDateRange.Properties.PopupControl = this.popupContainerControlDateRange;
            this.popupContainerEditDateRange.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditDateRange.Size = new System.Drawing.Size(229, 20);
            this.popupContainerEditDateRange.StyleController = this.layoutControl1;
            this.popupContainerEditDateRange.TabIndex = 7;
            this.popupContainerEditDateRange.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditDateRange_QueryResultValue);
            // 
            // buttonEditUserFilter
            // 
            this.buttonEditUserFilter.EditValue = "No User Filter";
            this.buttonEditUserFilter.Location = new System.Drawing.Point(78, 79);
            this.buttonEditUserFilter.MenuManager = this.barManager1;
            this.buttonEditUserFilter.Name = "buttonEditUserFilter";
            this.buttonEditUserFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions11, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject41, serializableAppearanceObject42, serializableAppearanceObject43, serializableAppearanceObject44, "Click to open Choose User Screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions12, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject45, serializableAppearanceObject46, serializableAppearanceObject47, serializableAppearanceObject48, "Clear Filter", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.buttonEditUserFilter.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.buttonEditUserFilter.Size = new System.Drawing.Size(229, 20);
            this.buttonEditUserFilter.StyleController = this.layoutControl1;
            this.buttonEditUserFilter.TabIndex = 7;
            this.buttonEditUserFilter.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEditUserFilter_ButtonClick);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem4,
            this.emptySpaceItem3,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem2,
            this.layoutControlGroup2,
            this.emptySpaceItem5,
            this.layoutControlItem1,
            this.layoutControlItem7,
            this.layoutControlItem11,
            this.layoutControlItem12});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Size = new System.Drawing.Size(314, 545);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 477);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(304, 58);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 441);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(304, 10);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.btnClearAllFilters;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 451);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(91, 26);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(91, 26);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(91, 26);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(91, 451);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(130, 26);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.btnLoad;
            this.layoutControlItem5.Location = new System.Drawing.Point(221, 451);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(83, 26);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(83, 26);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(83, 26);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.popupContainerEditTableFilter;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(304, 24);
            this.layoutControlItem6.Text = "Table:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(68, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.popupContainerEditDateRange;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(304, 24);
            this.layoutControlItem2.Text = "Date Range:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(68, 13);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlGroup3,
            this.emptySpaceItem6,
            this.emptySpaceItem7,
            this.layoutControlGroup4,
            this.emptySpaceItem9});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 154);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(304, 287);
            this.layoutControlGroup2.Text = "Transaction Details";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.popupContainerEditColumnFilter;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(280, 24);
            this.layoutControlItem3.Text = "Column:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(68, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.memoExEditOldValue;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 34);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(280, 24);
            this.layoutControlItem8.Text = "Old Value:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(68, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.memoExEditNewValue;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 127);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(280, 24);
            this.layoutControlItem9.Text = "New Value:";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(68, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.checkEditApplyFilterToDetail;
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 220);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(280, 23);
            this.layoutControlItem10.Text = "Apply Filter To Details:";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.emptySpaceItem4});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 58);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup3.Size = new System.Drawing.Size(280, 59);
            this.layoutControlGroup3.Text = "Wildcard Search";
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.checkEditOldValueProceedingWildcard;
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem13.MaxSize = new System.Drawing.Size(78, 23);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(78, 23);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(78, 23);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Text = "Old Value Preceeding Wildcard:";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextVisible = false;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.checkEditOldValueSucceedingWildcard;
            this.layoutControlItem14.Location = new System.Drawing.Point(78, 0);
            this.layoutControlItem14.MaxSize = new System.Drawing.Size(79, 23);
            this.layoutControlItem14.MinSize = new System.Drawing.Size(79, 23);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(79, 23);
            this.layoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem14.Text = "Old Value Succeeding Wildcard:";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem14.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(157, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(107, 23);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 210);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(280, 10);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 117);
            this.emptySpaceItem7.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem7.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(280, 10);
            this.emptySpaceItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.emptySpaceItem8});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 151);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup4.Size = new System.Drawing.Size(280, 59);
            this.layoutControlGroup4.Text = "Wildcard Search";
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.checkEditNewValueProceedingWildcard;
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem15.MaxSize = new System.Drawing.Size(78, 23);
            this.layoutControlItem15.MinSize = new System.Drawing.Size(78, 23);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(78, 23);
            this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem15.Text = "New Value Preceeding Wildcard:";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextVisible = false;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.checkEditNewValueSucceedingWildcard;
            this.layoutControlItem16.Location = new System.Drawing.Point(78, 0);
            this.layoutControlItem16.MaxSize = new System.Drawing.Size(79, 23);
            this.layoutControlItem16.MinSize = new System.Drawing.Size(79, 23);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(79, 23);
            this.layoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem16.Text = "New Value Succeeding Wildcard:";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextVisible = false;
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.Location = new System.Drawing.Point(157, 0);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(107, 23);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.Location = new System.Drawing.Point(0, 24);
            this.emptySpaceItem9.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem9.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(280, 10);
            this.emptySpaceItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 144);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(304, 10);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.buttonEditUserFilter;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(304, 24);
            this.layoutControlItem1.Text = "User:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(68, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.buttonEditApplicationFilter;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(304, 24);
            this.layoutControlItem7.Text = "Application:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(68, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.buttonEditHostNameFilter;
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(304, 24);
            this.layoutControlItem11.Text = "Host Name:";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(68, 13);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.popupContainerEditChangeTypeFilter;
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(304, 24);
            this.layoutControlItem12.Text = "Change Type:";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(68, 13);
            // 
            // sp01005_Core_Audit_Trail_Viewer_Unique_Transactions_ListTableAdapter
            // 
            this.sp01005_Core_Audit_Trail_Viewer_Unique_Transactions_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp01006_Core_Audit_Trail_Viewer_Transaction_Details_ListTableAdapter
            // 
            this.sp01006_Core_Audit_Trail_Viewer_Transaction_Details_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp01007_Core_Audit_Trail_Viewer_Transaction_Details_SQLTableAdapter
            // 
            this.sp01007_Core_Audit_Trail_Viewer_Transaction_Details_SQLTableAdapter.ClearBeforeFill = true;
            // 
            // sp01008_Core_Audit_Trail_Viewer_Unique_Table_NamesTableAdapter
            // 
            this.sp01008_Core_Audit_Trail_Viewer_Unique_Table_NamesTableAdapter.ClearBeforeFill = true;
            // 
            // sp01009_Core_Audit_Trail_Viewer_Unique_Column_NamesTableAdapter
            // 
            this.sp01009_Core_Audit_Trail_Viewer_Unique_Column_NamesTableAdapter.ClearBeforeFill = true;
            // 
            // sp01010_Core_Audit_Trail_Viewer_Change_Types_ListTableAdapter
            // 
            this.sp01010_Core_Audit_Trail_Viewer_Change_Types_ListTableAdapter.ClearBeforeFill = true;
            // 
            // frm_Core_Audit_Trail_Viewer
            // 
            this.ClientSize = new System.Drawing.Size(1027, 577);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.hideContainerLeft);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_Core_Audit_Trail_Viewer";
            this.Text = "Audit Trail Viewer";
            this.Activated += new System.EventHandler(this.frm_Core_Audit_Trail_Viewer_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_Core_Audit_Trail_Viewer_FormClosing);
            this.Load += new System.EventHandler(this.frm_Core_Audit_Trail_Viewer_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.hideContainerLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01005CoreAuditTrailViewerUniqueTransactionsListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Common_Functionality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlChangeTypeFilter)).EndInit();
            this.popupContainerControlChangeTypeFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01010CoreAuditTrailViewerChangeTypesListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlColumnFilter)).EndInit();
            this.popupContainerControlColumnFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01009CoreAuditTrailViewerUniqueColumnNamesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlTableNameFilter)).EndInit();
            this.popupContainerControlTableNameFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01008CoreAuditTrailViewerUniqueTableNamesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).EndInit();
            this.popupContainerControlDateRange.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPageDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01006CoreAuditTrailViewerTransactionDetailsListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML1)).EndInit();
            this.xtraTabPageSQL.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSQL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01007CoreAuditTrailViewerTransactionDetailsSQLBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSQL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Summer_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockPanel1)).EndInit();
            this.hideContainerLeft.ResumeLayout(false);
            this.dockPanelFilters.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditNewValueSucceedingWildcard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditNewValueProceedingWildcard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditOldValueSucceedingWildcard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditOldValueProceedingWildcard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditChangeTypeFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditHostNameFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditApplicationFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditApplyFilterToDetail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEditNewValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEditOldValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditColumnFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditTableFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditDateRange.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditUserFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraGrid.GridControl gridControlHeader;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewHeader;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DataSet_GC_Core dataSet_GC_Core;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.Utils.ToolTipController toolTipController1;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private WoodPlan5.DataSet_AT dataSet_AT;
        private WoodPlanDataSet woodPlanDataSet;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlDateRange;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraEditors.SimpleButton btnDateRangeOK;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageSQL;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageDetail;
        private DevExpress.XtraGrid.GridControl gridControlDetail;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewDetail;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private DevExpress.XtraBars.Docking.DockManager dockPanel1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelFilters;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.ButtonEdit buttonEditUserFilter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditDateRange;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.SimpleButton btnClearAllFilters;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.SimpleButton btnLoad;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditTableFilter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlTableNameFilter;
        private DevExpress.XtraEditors.SimpleButton btnTableFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlColumnFilter;
        private DevExpress.XtraEditors.SimpleButton btnColumnFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DataSet_GC_Summer_Core dataSet_GC_Summer_Core;
        private System.Windows.Forms.BindingSource sp01005CoreAuditTrailViewerUniqueTransactionsListBindingSource;
        private DataSet_Common_Functionality dataSet_Common_Functionality;
        private DevExpress.XtraGrid.Columns.GridColumn colTransactionID;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateChanged;
        private DevExpress.XtraGrid.Columns.GridColumn colUser;
        private DevExpress.XtraGrid.Columns.GridColumn colApplication;
        private DevExpress.XtraGrid.Columns.GridColumn colTableName;
        private DevExpress.XtraGrid.Columns.GridColumn colHostName;
        private DevExpress.XtraGrid.Columns.GridColumn colChangeTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colChangeTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colFriendlyTableName;
        private DevExpress.XtraGrid.Columns.GridColumn colColumnsAltered;
        private DataSet_Common_FunctionalityTableAdapters.sp01005_Core_Audit_Trail_Viewer_Unique_Transactions_ListTableAdapter sp01005_Core_Audit_Trail_Viewer_Unique_Transactions_ListTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private System.Windows.Forms.BindingSource sp01006CoreAuditTrailViewerTransactionDetailsListBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colTransactionID1;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID1;
        private DevExpress.XtraGrid.Columns.GridColumn colParentRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateChanged1;
        private DevExpress.XtraGrid.Columns.GridColumn colUser1;
        private DevExpress.XtraGrid.Columns.GridColumn colApplication1;
        private DevExpress.XtraGrid.Columns.GridColumn colTableName1;
        private DevExpress.XtraGrid.Columns.GridColumn colHostName1;
        private DevExpress.XtraGrid.Columns.GridColumn colChangeTypeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colColumnName;
        private DevExpress.XtraGrid.Columns.GridColumn colOldValue;
        private DevExpress.XtraGrid.Columns.GridColumn colNewValue;
        private DevExpress.XtraGrid.Columns.GridColumn colSQL;
        private DevExpress.XtraGrid.Columns.GridColumn colChangeTypeDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colFriendlyTableName1;
        private DevExpress.XtraGrid.Columns.GridColumn colParentRecordDescription;
        private DataSet_Common_FunctionalityTableAdapters.sp01006_Core_Audit_Trail_Viewer_Transaction_Details_ListTableAdapter sp01006_Core_Audit_Trail_Viewer_Transaction_Details_ListTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraGrid.GridControl gridControlSQL;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewSQL;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private System.Windows.Forms.BindingSource sp01007CoreAuditTrailViewerTransactionDetailsSQLBindingSource;
        private DataSet_Common_FunctionalityTableAdapters.sp01007_Core_Audit_Trail_Viewer_Transaction_Details_SQLTableAdapter sp01007_Core_Audit_Trail_Viewer_Transaction_Details_SQLTableAdapter;
        private System.Windows.Forms.BindingSource sp01008CoreAuditTrailViewerUniqueTableNamesBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colTableName2;
        private DevExpress.XtraGrid.Columns.GridColumn colFriendlyTableName2;
        private DataSet_Common_FunctionalityTableAdapters.sp01008_Core_Audit_Trail_Viewer_Unique_Table_NamesTableAdapter sp01008_Core_Audit_Trail_Viewer_Unique_Table_NamesTableAdapter;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditColumnFilter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private System.Windows.Forms.BindingSource sp01009CoreAuditTrailViewerUniqueColumnNamesBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colTableName3;
        private DevExpress.XtraGrid.Columns.GridColumn colFriendlyTableName3;
        private DevExpress.XtraGrid.Columns.GridColumn colColumnName1;
        private DataSet_Common_FunctionalityTableAdapters.sp01009_Core_Audit_Trail_Viewer_Unique_Column_NamesTableAdapter sp01009_Core_Audit_Trail_Viewer_Unique_Column_NamesTableAdapter;
        private DevExpress.XtraEditors.MemoExEdit memoExEditNewValue;
        private DevExpress.XtraEditors.MemoExEdit memoExEditOldValue;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.CheckEdit checkEditApplyFilterToDetail;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraGrid.Columns.GridColumn IconStatus;
        private DevExpress.XtraEditors.ButtonEdit buttonEditHostNameFilter;
        private DevExpress.XtraEditors.ButtonEdit buttonEditApplicationFilter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlChangeTypeFilter;
        private DevExpress.XtraEditors.SimpleButton btnChangeTypeFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditChangeTypeFilter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit1;
        private System.Windows.Forms.BindingSource sp01010CoreAuditTrailViewerChangeTypesListBindingSource;
        private DataSet_Common_FunctionalityTableAdapters.sp01010_Core_Audit_Trail_Viewer_Change_Types_ListTableAdapter sp01010_Core_Audit_Trail_Viewer_Change_Types_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraBars.BarSubItem bsiCreateRollbackSQL;
        private DevExpress.XtraBars.BarButtonItem bbiRollbackSQLFile;
        private DevExpress.XtraBars.BarButtonItem bbiRollbackSQLClipboard;
        private DevExpress.XtraEditors.CheckEdit checkEditNewValueSucceedingWildcard;
        private DevExpress.XtraEditors.CheckEdit checkEditNewValueProceedingWildcard;
        private DevExpress.XtraEditors.CheckEdit checkEditOldValueSucceedingWildcard;
        private DevExpress.XtraEditors.CheckEdit checkEditOldValueProceedingWildcard;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraBars.Docking.AutoHideContainer hideContainerLeft;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHTML1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHTML2;
    }
}

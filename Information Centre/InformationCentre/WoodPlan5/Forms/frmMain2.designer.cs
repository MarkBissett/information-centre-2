using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraBars.Ribbon.Gallery;

namespace WoodPlan5
{
    /// <summary>
    /// 
    /// </summary>
    partial class frmMain2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain2));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraBars.Ribbon.GalleryItemGroup galleryItemGroup1 = new DevExpress.XtraBars.Ribbon.GalleryItemGroup();
            DevExpress.XtraBars.Ribbon.GalleryItemGroup galleryItemGroup2 = new DevExpress.XtraBars.Ribbon.GalleryItemGroup();
            DevExpress.XtraBars.Ribbon.GalleryItemGroup galleryItemGroup3 = new DevExpress.XtraBars.Ribbon.GalleryItemGroup();
            DevExpress.XtraBars.Ribbon.GalleryItemGroup galleryItemGroup4 = new DevExpress.XtraBars.Ribbon.GalleryItemGroup();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraBars.Ribbon.GalleryItemGroup galleryItemGroup5 = new DevExpress.XtraBars.Ribbon.GalleryItemGroup();
            DevExpress.XtraBars.Ribbon.GalleryItemGroup galleryItemGroup6 = new DevExpress.XtraBars.Ribbon.GalleryItemGroup();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem9 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem9 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip10 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem10 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem10 = new DevExpress.Utils.ToolTipItem();
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.applicationMenu1 = new DevExpress.XtraBars.Ribbon.ApplicationMenu(this.components);
            this.iExport = new DevExpress.XtraBars.BarButtonItem();
            this.iPrintSetup = new DevExpress.XtraBars.BarButtonItem();
            this.iExit = new DevExpress.XtraBars.BarButtonItem();
            this.barAndDockingController1 = new DevExpress.XtraBars.BarAndDockingController(this.components);
            this.iCut = new DevExpress.XtraBars.BarButtonItem();
            this.iCopy = new DevExpress.XtraBars.BarButtonItem();
            this.iPaste = new DevExpress.XtraBars.BarButtonItem();
            this.iSpellCheck = new DevExpress.XtraBars.BarButtonItem();
            this.iUndo = new DevExpress.XtraBars.BarButtonItem();
            this.iRedo = new DevExpress.XtraBars.BarButtonItem();
            this.iPreview = new DevExpress.XtraBars.BarButtonItem();
            this.iPrint = new DevExpress.XtraBars.BarButtonItem();
            this.iDelete = new DevExpress.XtraBars.BarButtonItem();
            this.iSave = new DevExpress.XtraBars.BarButtonItem();
            this.iAdd = new DevExpress.XtraBars.BarButtonItem();
            this.iBlockAdd = new DevExpress.XtraBars.BarButtonItem();
            this.iEdit = new DevExpress.XtraBars.BarButtonItem();
            this.iBlockEdit = new DevExpress.XtraBars.BarButtonItem();
            this.sbiFile = new DevExpress.XtraBars.BarSubItem();
            this.iClear = new DevExpress.XtraBars.BarButtonItem();
            this.sbiSelectAll = new DevExpress.XtraBars.BarButtonItem();
            this.SelectMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.iSelectAll = new DevExpress.XtraBars.BarButtonItem();
            this.iSelectGroup = new DevExpress.XtraBars.BarButtonItem();
            this.iSaveLayout = new DevExpress.XtraBars.BarButtonItem();
            this.iSaveLayoutAs = new DevExpress.XtraBars.BarButtonItem();
            this.sbiOptions = new DevExpress.XtraBars.BarSubItem();
            this.iMyOptions = new DevExpress.XtraBars.BarButtonItem();
            this.iSystemOptions = new DevExpress.XtraBars.BarButtonItem();
            this.MicroHelpStaticItem = new DevExpress.XtraBars.BarStaticItem();
            this.iProgressBar = new DevExpress.XtraBars.BarEditItem();
            this.rpiProgressBar = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.iCurrentUser = new DevExpress.XtraBars.BarStaticItem();
            this.iDatabase = new DevExpress.XtraBars.BarStaticItem();
            this.iGrammerCheck = new DevExpress.XtraBars.BarButtonItem();
            this.sbiCommentBank = new DevExpress.XtraBars.BarButtonItem();
            this.CommentBankMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.iCommentBank = new DevExpress.XtraBars.BarButtonItem();
            this.bbiEditComments = new DevExpress.XtraBars.BarButtonItem();
            this.iCommentBankCodes = new DevExpress.XtraBars.BarButtonItem();
            this.iScratchPad = new DevExpress.XtraBars.BarButtonItem();
            this.iClipboard = new DevExpress.XtraBars.BarButtonItem();
            this.sbiToDoList = new DevExpress.XtraBars.BarButtonItem();
            this.ToDoListMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.iToDoList = new DevExpress.XtraBars.BarButtonItem();
            this.iLinkRecordToDoList = new DevExpress.XtraBars.BarButtonItem();
            this.iBold = new DevExpress.XtraBars.BarButtonItem();
            this.iItalic = new DevExpress.XtraBars.BarButtonItem();
            this.iUnderline = new DevExpress.XtraBars.BarButtonItem();
            this.iAlignLeft = new DevExpress.XtraBars.BarButtonItem();
            this.iAlignCentre = new DevExpress.XtraBars.BarButtonItem();
            this.iAlignRight = new DevExpress.XtraBars.BarButtonItem();
            this.iBullet = new DevExpress.XtraBars.BarButtonItem();
            this.iFont = new DevExpress.XtraBars.BarButtonItem();
            this.gddFont = new DevExpress.XtraBars.Ribbon.GalleryDropDown(this.components);
            this.beiFontSize = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemSpinEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.iFontColour = new DevExpress.XtraBars.BarButtonItem();
            this.gddFontColour = new DevExpress.XtraBars.Ribbon.GalleryDropDown(this.components);
            this.iFilterManager = new DevExpress.XtraBars.BarButtonItem();
            this.iFilterCreate = new DevExpress.XtraBars.BarButtonItem();
            this.iDefaultView = new DevExpress.XtraBars.BarButtonItem();
            this.sbiFind = new DevExpress.XtraBars.BarSubItem();
            this.iFind = new DevExpress.XtraBars.BarButtonItem();
            this.iReplace = new DevExpress.XtraBars.BarButtonItem();
            this.rgbiSkins = new DevExpress.XtraBars.RibbonGalleryBarItem();
            this.sbiAdd = new DevExpress.XtraBars.BarButtonItem();
            this.AddMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.sbiEdit = new DevExpress.XtraBars.BarButtonItem();
            this.EditMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.iImage = new DevExpress.XtraBars.BarButtonItem();
            this.beiLivePeriod = new DevExpress.XtraBars.BarEditItem();
            this.LivePeriodLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.sp00022GetDatePeriodsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.beiViewedPeriod = new DevExpress.XtraBars.BarEditItem();
            this.ViewedPeriodLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.iEditSystemPeriods = new DevExpress.XtraBars.BarButtonItem();
            this.iCascade = new DevExpress.XtraBars.BarButtonItem();
            this.iTileHorizontal = new DevExpress.XtraBars.BarButtonItem();
            this.iTileVertical = new DevExpress.XtraBars.BarButtonItem();
            this.barMdiChildrenListItem1 = new DevExpress.XtraBars.BarMdiChildrenListItem();
            this.bbiSelect = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDelete = new DevExpress.XtraBars.BarButtonItem();
            this.bbiReportSelect = new DevExpress.XtraBars.BarButtonItem();
            this.bbiReportDelete = new DevExpress.XtraBars.BarButtonItem();
            this.iSaveFilter = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDefaultView = new DevExpress.XtraBars.BarButtonItem();
            this.bsiMyLayouts = new DevExpress.XtraBars.BarSubItem();
            this.bsiAvailableLayouts = new DevExpress.XtraBars.BarSubItem();
            this.iLayoutManager = new DevExpress.XtraBars.BarButtonItem();
            this.iMyFilters = new DevExpress.XtraBars.BarSubItem();
            this.iAvailableFilters = new DevExpress.XtraBars.BarSubItem();
            this.iSaveFilterAs = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCloseAll = new DevExpress.XtraBars.BarButtonItem();
            this.bbiShowLoggedInUsers = new DevExpress.XtraBars.BarButtonItem();
            this.bbiChangeCurrentUserSuper = new DevExpress.XtraBars.BarButtonItem();
            this.bbiChangeCurrentUser = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCalculator = new DevExpress.XtraBars.BarButtonItem();
            this.bbiScriptBuilder = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCloseThisForm = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCloseAllForms = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCloseAllButThisForm = new DevExpress.XtraBars.BarButtonItem();
            this.beTraining = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPictureEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.bbiMagnifier = new DevExpress.XtraBars.BarButtonItem();
            this.iFastFind = new DevExpress.XtraBars.BarButtonItem();
            this.iLoadLayout = new DevExpress.XtraBars.BarButtonItem();
            this.iLoadFilter = new DevExpress.XtraBars.BarButtonItem();
            this.iAbout = new DevExpress.XtraBars.BarButtonItem();
            this.bbiChartPalette = new DevExpress.XtraBars.BarButtonItem();
            this.gddChartPalette = new DevExpress.XtraBars.Ribbon.GalleryDropDown(this.components);
            this.rgbiChartAppearance = new DevExpress.XtraBars.RibbonGalleryBarItem();
            this.rgbiChartTypes = new DevExpress.XtraBars.RibbonGalleryBarItem();
            this.bbiChartWizard = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRotateAxis = new DevExpress.XtraBars.BarButtonItem();
            this.bbiLayoutFlip = new DevExpress.XtraBars.BarButtonItem();
            this.bbiLayoutRotate = new DevExpress.XtraBars.BarButtonItem();
            this.bbiVisitWebSite = new DevExpress.XtraBars.BarButtonItem();
            this.bbiColourMixer = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup11 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup5 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup6 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage3 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup7 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup8 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage2 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage5 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup9 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage4 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup13 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup12 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup14 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup15 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemPopupContainerEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.repositoryItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.gddStaff = new DevExpress.XtraBars.Ribbon.GalleryDropDown(this.components);
            this.clientPanel = new DevExpress.XtraEditors.PanelControl();
            this.popupControlContainer1 = new DevExpress.XtraBars.PopupControlContainer(this.components);
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.DataEditMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.ViewMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.ToolsMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.DataMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.PrintingMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.SystemPeriodsMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dpActiveForm = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel3_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.nbcActiveForm = new DevExpress.XtraNavBar.NavBarControl();
            this.panelContainer2 = new DevExpress.XtraBars.Docking.DockPanel();
            this.panelContainer1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.nbcSwitchboard = new DevExpress.XtraNavBar.NavBarControl();
            this.dockPanel2 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel2_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.nbcReports = new DevExpress.XtraNavBar.NavBarControl();
            this.panelContainer3 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dpPersonImage = new DevExpress.XtraBars.Docking.DockPanel();
            this.controlContainer1 = new DevExpress.XtraBars.Docking.ControlContainer();
            this.xtraScrollableControl1 = new DevExpress.XtraEditors.XtraScrollableControl();
            this.pePersonsPhoto = new DevExpress.XtraEditors.PictureEdit();
            this.dockPanelSiteInspection = new DevExpress.XtraBars.Docking.DockPanel();
            this.controlContainer2 = new DevExpress.XtraBars.Docking.ControlContainer();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnResume = new DevExpress.XtraEditors.SimpleButton();
            this.labelControlSelectedInspection = new DevExpress.XtraEditors.LabelControl();
            this.btnStart = new DevExpress.XtraEditors.SimpleButton();
            this.btnPause = new DevExpress.XtraEditors.SimpleButton();
            this.btnFinish = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
            this.sp00022GetDatePeriodsTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00022GetDatePeriodsTableAdapter();
            this.WindowMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.tmrNotification = new System.Timers.Timer();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.openGiroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sp00020GetUserApplicationSettingsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00020GetUserApplicationSettingsTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00020GetUserApplicationSettingsTableAdapter();
            this.sp00019GetUserDetailsForStartUpBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00019GetUserDetailsForStartUpTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00019GetUserDetailsForStartUpTableAdapter();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.SwitchboardMenu = new DevExpress.XtraBars.PopupMenu(this.components);
            this.ReportsMenu = new DevExpress.XtraBars.PopupMenu(this.components);
            this.sp00001PopulateSwitchboardBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00001PopulateSwitchboardTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00001PopulateSwitchboardTableAdapter();
            this.FilterMenu = new DevExpress.XtraBars.PopupMenu(this.components);
            this.LayoutMenu = new DevExpress.XtraBars.PopupMenu(this.components);
            this.pmCurrentUserMenu = new DevExpress.XtraBars.PopupMenu(this.components);
            this.pmChildForms = new DevExpress.XtraBars.PopupMenu(this.components);
            this.sp00068_Get_Magnifier_SettingsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00068_Get_Magnifier_SettingsTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00068_Get_Magnifier_SettingsTableAdapter();
            this.sp00071_get_available_loginsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00071_get_available_loginsTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00071_get_available_loginsTableAdapter();
            this.styleController1 = new DevExpress.XtraEditors.StyleController(this.components);
            this.lcNonLiveDB = new DevExpress.XtraEditors.LabelControl();
            this.lcDataTransferMode = new DevExpress.XtraEditors.LabelControl();
            this.imageCollectionChartTypes = new DevExpress.Utils.ImageCollection(this.components);
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.documentManager1 = new DevExpress.XtraBars.Docking2010.DocumentManager(this.components);
            this.tabbedView1 = new DevExpress.XtraBars.Docking2010.Views.Tabbed.TabbedView(this.components);
            this.imageCollection3 = new DevExpress.Utils.ImageCollection(this.components);
            this.sharedImageCollection16x16 = new DevExpress.Utils.SharedImageCollection(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicationMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelectMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpiProgressBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CommentBankMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ToDoListMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gddFont)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gddFontColour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EditMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LivePeriodLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00022GetDatePeriodsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ViewedPeriodLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gddChartPalette)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gddStaff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientPanel)).BeginInit();
            this.clientPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupControlContainer1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataEditMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ViewMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ToolsMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrintingMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SystemPeriodsMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dpActiveForm.SuspendLayout();
            this.dockPanel3_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nbcActiveForm)).BeginInit();
            this.panelContainer2.SuspendLayout();
            this.panelContainer1.SuspendLayout();
            this.dockPanel1.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nbcSwitchboard)).BeginInit();
            this.dockPanel2.SuspendLayout();
            this.dockPanel2_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nbcReports)).BeginInit();
            this.panelContainer3.SuspendLayout();
            this.dpPersonImage.SuspendLayout();
            this.controlContainer1.SuspendLayout();
            this.xtraScrollableControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pePersonsPhoto.Properties)).BeginInit();
            this.dockPanelSiteInspection.SuspendLayout();
            this.controlContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WindowMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tmrNotification)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sp00020GetUserApplicationSettingsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00019GetUserDetailsForStartUpBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SwitchboardMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportsMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00001PopulateSwitchboardBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FilterMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmCurrentUserMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmChildForms)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00068_Get_Magnifier_SettingsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00071_get_available_loginsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.styleController1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollectionChartTypes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sharedImageCollection16x16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sharedImageCollection16x16.ImageSource)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbon
            // 
            this.ribbon.ApplicationButtonDropDownControl = this.applicationMenu1;
            this.ribbon.ApplicationButtonText = null;
            this.ribbon.ApplicationCaption = "Information Centre";
            this.ribbon.Categories.AddRange(new DevExpress.XtraBars.BarManagerCategory[] {
            new DevExpress.XtraBars.BarManagerCategory("Text Formatting", new System.Guid("d93503f0-608f-4234-a758-ac3aa2a1d0cd")),
            new DevExpress.XtraBars.BarManagerCategory("File", new System.Guid("a8b359a9-baf3-40f2-b7dd-076dc1bae8b6")),
            new DevExpress.XtraBars.BarManagerCategory("Tools", new System.Guid("deebb05d-8009-474c-83ac-a95b2c85199f")),
            new DevExpress.XtraBars.BarManagerCategory("Data", new System.Guid("51624475-daf8-43f2-b723-ef19db5672a1")),
            new DevExpress.XtraBars.BarManagerCategory("Edit", new System.Guid("c6d5f1ac-52e6-4755-9272-aa24aa54000e")),
            new DevExpress.XtraBars.BarManagerCategory("View", new System.Guid("d6bbd404-6dc5-47da-a255-da3ff407a6fe")),
            new DevExpress.XtraBars.BarManagerCategory("StatusBar", new System.Guid("58910032-9fab-4d2b-b381-fb03af240642")),
            new DevExpress.XtraBars.BarManagerCategory("SystemPeriods", new System.Guid("ff40d82f-e413-4303-bb72-fbb441bcaf0f")),
            new DevExpress.XtraBars.BarManagerCategory("Printing", new System.Guid("4c92dee4-f264-49d4-895e-d92153443484")),
            new DevExpress.XtraBars.BarManagerCategory("Filter", new System.Guid("de3525c7-5920-4f99-9e40-de6b86c7e1fa")),
            new DevExpress.XtraBars.BarManagerCategory("Layout", new System.Guid("b6170956-22ef-4aa4-99ed-dafa1c16b2e4"))});
            this.ribbon.Controller = this.barAndDockingController1;
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.iCut,
            this.iCopy,
            this.iPaste,
            this.iSpellCheck,
            this.iUndo,
            this.iRedo,
            this.iPreview,
            this.iPrint,
            this.iDelete,
            this.iSave,
            this.iAdd,
            this.iBlockAdd,
            this.iEdit,
            this.iBlockEdit,
            this.sbiFile,
            this.iPrintSetup,
            this.iClear,
            this.sbiSelectAll,
            this.iSaveLayout,
            this.iSaveLayoutAs,
            this.sbiOptions,
            this.iMyOptions,
            this.iSystemOptions,
            this.iExport,
            this.iExit,
            this.MicroHelpStaticItem,
            this.iProgressBar,
            this.iCurrentUser,
            this.iDatabase,
            this.iGrammerCheck,
            this.sbiCommentBank,
            this.iScratchPad,
            this.iClipboard,
            this.sbiToDoList,
            this.iBold,
            this.iItalic,
            this.iUnderline,
            this.iAlignLeft,
            this.iAlignCentre,
            this.iAlignRight,
            this.iBullet,
            this.iFont,
            this.iFontColour,
            this.beiFontSize,
            this.iFilterManager,
            this.iFilterCreate,
            this.iDefaultView,
            this.sbiFind,
            this.iFind,
            this.iReplace,
            this.rgbiSkins,
            this.sbiAdd,
            this.sbiEdit,
            this.iToDoList,
            this.iLinkRecordToDoList,
            this.iCommentBank,
            this.iCommentBankCodes,
            this.iImage,
            this.beiLivePeriod,
            this.beiViewedPeriod,
            this.iEditSystemPeriods,
            this.iCascade,
            this.iTileHorizontal,
            this.iTileVertical,
            this.barMdiChildrenListItem1,
            this.iSelectGroup,
            this.iSelectAll,
            this.bbiSelect,
            this.bbiDelete,
            this.bbiReportSelect,
            this.bbiReportDelete,
            this.iSaveFilter,
            this.bbiDefaultView,
            this.bsiMyLayouts,
            this.bsiAvailableLayouts,
            this.iLayoutManager,
            this.iMyFilters,
            this.iAvailableFilters,
            this.iSaveFilterAs,
            this.bbiCloseAll,
            this.bbiShowLoggedInUsers,
            this.bbiChangeCurrentUserSuper,
            this.bbiChangeCurrentUser,
            this.bbiEditComments,
            this.bbiCalculator,
            this.bbiScriptBuilder,
            this.bbiCloseThisForm,
            this.bbiCloseAllForms,
            this.bbiCloseAllButThisForm,
            this.beTraining,
            this.bbiMagnifier,
            this.iFastFind,
            this.iLoadLayout,
            this.iLoadFilter,
            this.iAbout,
            this.bbiChartPalette,
            this.rgbiChartAppearance,
            this.rgbiChartTypes,
            this.bbiChartWizard,
            this.bbiRotateAxis,
            this.bbiLayoutFlip,
            this.bbiLayoutRotate,
            this.bbiVisitWebSite,
            this.bbiColourMixer});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 235;
            this.ribbon.Name = "ribbon";
            this.ribbon.PageHeaderItemLinks.Add(this.bbiVisitWebSite);
            this.ribbon.PageHeaderItemLinks.Add(this.iAbout);
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1,
            this.ribbonPage3,
            this.ribbonPage2,
            this.ribbonPage5,
            this.ribbonPage4});
            this.ribbon.QuickToolbarItemLinks.Add(this.sbiAdd);
            this.ribbon.QuickToolbarItemLinks.Add(this.sbiEdit);
            this.ribbon.QuickToolbarItemLinks.Add(this.iDelete);
            this.ribbon.QuickToolbarItemLinks.Add(this.iSave);
            this.ribbon.QuickToolbarItemLinks.Add(this.bbiMagnifier, true);
            this.ribbon.QuickToolbarItemLinks.Add(this.bbiCloseAll, true);
            this.ribbon.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rpiProgressBar,
            this.repositoryItemSpinEdit1,
            this.repositoryItemSpinEdit2,
            this.repositoryItemPopupContainerEdit1,
            this.LivePeriodLookUpEdit1,
            this.ViewedPeriodLookUpEdit1,
            this.repositoryItemPictureEdit1,
            this.repositoryItemPictureEdit2});
            this.ribbon.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2013;
            this.ribbon.ShowExpandCollapseButton = DevExpress.Utils.DefaultBoolean.True;
            this.ribbon.Size = new System.Drawing.Size(1105, 147);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            // 
            // applicationMenu1
            // 
            this.applicationMenu1.ItemLinks.Add(this.iExport);
            this.applicationMenu1.ItemLinks.Add(this.iPrintSetup, true);
            this.applicationMenu1.ItemLinks.Add(this.iExit, true);
            this.applicationMenu1.Name = "applicationMenu1";
            this.applicationMenu1.Ribbon = this.ribbon;
            // 
            // iExport
            // 
            this.iExport.Caption = "Export";
            this.iExport.CategoryGuid = new System.Guid("a8b359a9-baf3-40f2-b7dd-076dc1bae8b6");
            this.iExport.Description = "Export the current screen / report contents to another format.";
            this.iExport.Hint = "Export the current screen / report contents to another format";
            this.iExport.Id = 105;
            this.iExport.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Export_16x16;
            this.iExport.ImageOptions.LargeImage = global::WoodPlan5.Properties.Resources.Export_32x32;
            this.iExport.Name = "iExport";
            this.iExport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iExport_ItemClick);
            // 
            // iPrintSetup
            // 
            this.iPrintSetup.Caption = "Print Setup";
            this.iPrintSetup.CategoryGuid = new System.Guid("a8b359a9-baf3-40f2-b7dd-076dc1bae8b6");
            this.iPrintSetup.Description = "Configures printer settings.";
            this.iPrintSetup.Hint = "Configures printer settings";
            this.iPrintSetup.Id = 77;
            this.iPrintSetup.ImageOptions.Image = global::WoodPlan5.Properties.Resources.PrintDialog_16x16;
            this.iPrintSetup.ImageOptions.LargeImage = global::WoodPlan5.Properties.Resources.PrintDialog_32x32;
            this.iPrintSetup.Name = "iPrintSetup";
            this.iPrintSetup.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iPrintSetup_ItemClick);
            // 
            // iExit
            // 
            this.iExit.Caption = "Exit";
            this.iExit.CategoryGuid = new System.Guid("a8b359a9-baf3-40f2-b7dd-076dc1bae8b6");
            this.iExit.Description = "Close the program.";
            this.iExit.Hint = "Close the program";
            this.iExit.Id = 107;
            this.iExit.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("iExit.ImageOptions.LargeImage")));
            this.iExit.Name = "iExit";
            this.iExit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iExit_ItemClick);
            // 
            // barAndDockingController1
            // 
            this.barAndDockingController1.PropertiesBar.AllowLinkLighting = false;
            this.barAndDockingController1.PropertiesBar.DefaultGlyphSize = new System.Drawing.Size(16, 16);
            this.barAndDockingController1.PropertiesBar.DefaultLargeGlyphSize = new System.Drawing.Size(32, 32);
            // 
            // iCut
            // 
            this.iCut.Caption = "Cut";
            this.iCut.CategoryGuid = new System.Guid("c6d5f1ac-52e6-4755-9272-aa24aa54000e");
            this.iCut.Description = "Removes the selection from the active document and places it on the Clipboard.";
            this.iCut.Hint = "Removes the selection from the active document and places it on the Clipboard";
            this.iCut.Id = 9;
            this.iCut.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Cut_16x16;
            this.iCut.ImageOptions.LargeImage = global::WoodPlan5.Properties.Resources.Cut_32x32;
            this.iCut.Name = "iCut";
            this.iCut.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iCut_ItemClick);
            // 
            // iCopy
            // 
            this.iCopy.Caption = "Copy";
            this.iCopy.CategoryGuid = new System.Guid("c6d5f1ac-52e6-4755-9272-aa24aa54000e");
            this.iCopy.Description = "Copies the selection to the Clipboard.";
            this.iCopy.Hint = "Copies the selection to the Clipboard";
            this.iCopy.Id = 10;
            this.iCopy.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Copy_16x16;
            this.iCopy.ImageOptions.LargeImage = global::WoodPlan5.Properties.Resources.Copy_32x32;
            this.iCopy.Name = "iCopy";
            this.iCopy.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iCopy_ItemClick);
            // 
            // iPaste
            // 
            this.iPaste.Caption = "Paste";
            this.iPaste.CategoryGuid = new System.Guid("c6d5f1ac-52e6-4755-9272-aa24aa54000e");
            this.iPaste.Description = "Inserts the contents of the Clipboard at the insertion point.";
            this.iPaste.Hint = "Inserts the contents of the Clipboard at the insertion point";
            this.iPaste.Id = 11;
            this.iPaste.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Paste_16x16;
            this.iPaste.ImageOptions.LargeImage = global::WoodPlan5.Properties.Resources.Paste_32x32;
            this.iPaste.Name = "iPaste";
            this.iPaste.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iPaste_ItemClick);
            // 
            // iSpellCheck
            // 
            this.iSpellCheck.Caption = "Spell Check";
            this.iSpellCheck.CategoryGuid = new System.Guid("deebb05d-8009-474c-83ac-a95b2c85199f");
            this.iSpellCheck.Description = "Spell checks the selected text.";
            this.iSpellCheck.Enabled = false;
            this.iSpellCheck.Hint = "Spell checks the selected text";
            this.iSpellCheck.Id = 12;
            this.iSpellCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("iSpellCheck.ImageOptions.Image")));
            this.iSpellCheck.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("iSpellCheck.ImageOptions.LargeImage")));
            this.iSpellCheck.Name = "iSpellCheck";
            this.iSpellCheck.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.iSpellCheck.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iSpellCheck_ItemClick);
            // 
            // iUndo
            // 
            this.iUndo.Caption = "Undo";
            this.iUndo.CategoryGuid = new System.Guid("c6d5f1ac-52e6-4755-9272-aa24aa54000e");
            this.iUndo.Description = "Reverses the last command or deletes the last entry you typed.";
            this.iUndo.Hint = "Reverses the last command or deletes the last entry you typed";
            this.iUndo.Id = 13;
            this.iUndo.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Undo_16x16;
            this.iUndo.ImageOptions.LargeImage = global::WoodPlan5.Properties.Resources.Undo_32x32;
            this.iUndo.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z));
            this.iUndo.Name = "iUndo";
            this.iUndo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iUndo_ItemClick);
            // 
            // iRedo
            // 
            this.iRedo.Caption = "Redo";
            this.iRedo.CategoryGuid = new System.Guid("c6d5f1ac-52e6-4755-9272-aa24aa54000e");
            this.iRedo.Description = "Redoes the last action.";
            this.iRedo.Hint = "Redo the Last Action";
            this.iRedo.Id = 14;
            this.iRedo.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Redo_16x16;
            this.iRedo.ImageOptions.LargeImage = global::WoodPlan5.Properties.Resources.Redo_32x32;
            this.iRedo.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y));
            this.iRedo.Name = "iRedo";
            this.iRedo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iRedo_ItemClick);
            // 
            // iPreview
            // 
            this.iPreview.Caption = "Preview";
            this.iPreview.CategoryGuid = new System.Guid("4c92dee4-f264-49d4-895e-d92153443484");
            this.iPreview.Description = "Previews how the item will look when printed.";
            this.iPreview.Hint = "Previews how the item will look when printed";
            this.iPreview.Id = 15;
            this.iPreview.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Preview_16x16;
            this.iPreview.ImageOptions.LargeImage = global::WoodPlan5.Properties.Resources.Preview_32x32;
            this.iPreview.Name = "iPreview";
            this.iPreview.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iPreview_ItemClick);
            // 
            // iPrint
            // 
            this.iPrint.Caption = "Print";
            this.iPrint.CategoryGuid = new System.Guid("4c92dee4-f264-49d4-895e-d92153443484");
            this.iPrint.Description = "Prints the item.";
            this.iPrint.Hint = "Prints the item";
            this.iPrint.Id = 16;
            this.iPrint.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Print_16x16;
            this.iPrint.ImageOptions.LargeImage = global::WoodPlan5.Properties.Resources.Print_32x32;
            this.iPrint.Name = "iPrint";
            this.iPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iPrint_ItemClick);
            // 
            // iDelete
            // 
            this.iDelete.Caption = "Delete";
            this.iDelete.CategoryGuid = new System.Guid("51624475-daf8-43f2-b723-ef19db5672a1");
            this.iDelete.Description = "Delete current record(s).";
            this.iDelete.Hint = "Deletes current record(s)";
            this.iDelete.Id = 20;
            this.iDelete.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Delete_16x16;
            this.iDelete.ImageOptions.LargeImage = global::WoodPlan5.Properties.Resources.Delete_32x32;
            this.iDelete.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Delete));
            this.iDelete.Name = "iDelete";
            this.iDelete.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iDelete_ItemClick);
            // 
            // iSave
            // 
            this.iSave.Caption = "Save";
            this.iSave.CategoryGuid = new System.Guid("51624475-daf8-43f2-b723-ef19db5672a1");
            this.iSave.Description = "Saves the current changes.";
            this.iSave.Hint = "Saves the current changes";
            this.iSave.Id = 23;
            this.iSave.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Save_16x16;
            this.iSave.ImageOptions.LargeImage = global::WoodPlan5.Properties.Resources.Save_32x32;
            this.iSave.Name = "iSave";
            this.iSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iSave_ItemClick);
            // 
            // iAdd
            // 
            this.iAdd.Caption = "Add";
            this.iAdd.CategoryGuid = new System.Guid("51624475-daf8-43f2-b723-ef19db5672a1");
            this.iAdd.Description = "Creates a new record.";
            this.iAdd.Hint = "Creates a new record";
            this.iAdd.Id = 43;
            this.iAdd.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Add_16x16;
            this.iAdd.ImageOptions.LargeImage = global::WoodPlan5.Properties.Resources.Add_32x32;
            this.iAdd.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Insert));
            this.iAdd.Name = "iAdd";
            this.iAdd.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iAdd_ItemClick);
            // 
            // iBlockAdd
            // 
            this.iBlockAdd.Caption = "Block Add";
            this.iBlockAdd.CategoryGuid = new System.Guid("51624475-daf8-43f2-b723-ef19db5672a1");
            this.iBlockAdd.Description = "Creates multiple new records based on a template.";
            this.iBlockAdd.Hint = "Creates multiple new records based on a template";
            this.iBlockAdd.Id = 44;
            this.iBlockAdd.ImageOptions.Image = global::WoodPlan5.Properties.Resources.BlockAdd_16x16;
            this.iBlockAdd.ImageOptions.LargeImage = global::WoodPlan5.Properties.Resources.Block_Add_32x32;
            this.iBlockAdd.Name = "iBlockAdd";
            this.iBlockAdd.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iBlockAdd_ItemClick);
            // 
            // iEdit
            // 
            this.iEdit.Caption = "Edit";
            this.iEdit.CategoryGuid = new System.Guid("51624475-daf8-43f2-b723-ef19db5672a1");
            this.iEdit.Description = "Edits the currently selected record(s).";
            this.iEdit.Hint = "Edits the currently selected record(s)";
            this.iEdit.Id = 45;
            this.iEdit.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            this.iEdit.ImageOptions.LargeImage = global::WoodPlan5.Properties.Resources.Edit_32x32;
            this.iEdit.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E));
            this.iEdit.Name = "iEdit";
            this.iEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iEdit_ItemClick);
            // 
            // iBlockEdit
            // 
            this.iBlockEdit.Caption = "Block Edit";
            this.iBlockEdit.CategoryGuid = new System.Guid("51624475-daf8-43f2-b723-ef19db5672a1");
            this.iBlockEdit.Description = "Applies a set of changes to multiple records.";
            this.iBlockEdit.Hint = "Apply a set of changes to multiple records";
            this.iBlockEdit.Id = 46;
            this.iBlockEdit.ImageOptions.Image = global::WoodPlan5.Properties.Resources.BlockEdit_16x16;
            this.iBlockEdit.ImageOptions.LargeImage = global::WoodPlan5.Properties.Resources.Block_Edit_32x32;
            this.iBlockEdit.Name = "iBlockEdit";
            this.iBlockEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iBlockEdit_ItemClick);
            // 
            // sbiFile
            // 
            this.sbiFile.Caption = "File";
            this.sbiFile.CategoryGuid = new System.Guid("a8b359a9-baf3-40f2-b7dd-076dc1bae8b6");
            this.sbiFile.Id = 49;
            this.sbiFile.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.iExport),
            new DevExpress.XtraBars.LinkPersistInfo(this.iPrintSetup)});
            this.sbiFile.Name = "sbiFile";
            // 
            // iClear
            // 
            this.iClear.Caption = "Clear";
            this.iClear.CategoryGuid = new System.Guid("c6d5f1ac-52e6-4755-9272-aa24aa54000e");
            this.iClear.Description = "Deletes the selected text without putting it on the Clipboard. This command is av" +
    "ailable only if a piece of text is selected.";
            this.iClear.Hint = "Deletes the selected text without putting it on the Clipboard. This command is av" +
    "ailable only if a piece of text is selected";
            this.iClear.Id = 84;
            this.iClear.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Clear_16x16;
            this.iClear.ImageOptions.LargeImage = global::WoodPlan5.Properties.Resources.Clear_32x32;
            this.iClear.Name = "iClear";
            this.iClear.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iClear_ItemClick);
            // 
            // sbiSelectAll
            // 
            this.sbiSelectAll.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.sbiSelectAll.Caption = "Select All";
            this.sbiSelectAll.CategoryGuid = new System.Guid("c6d5f1ac-52e6-4755-9272-aa24aa54000e");
            this.sbiSelectAll.Description = "Selects all text in the active field or selects all records.";
            this.sbiSelectAll.DropDownControl = this.SelectMenu1;
            this.sbiSelectAll.Hint = "Selects all text in the active field or selects all records";
            this.sbiSelectAll.Id = 85;
            this.sbiSelectAll.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Select_16x16;
            this.sbiSelectAll.ImageOptions.LargeImage = global::WoodPlan5.Properties.Resources.Select_32x32;
            this.sbiSelectAll.Name = "sbiSelectAll";
            this.sbiSelectAll.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iSelectAll_ItemClick);
            // 
            // SelectMenu1
            // 
            this.SelectMenu1.ItemLinks.Add(this.iSelectAll);
            this.SelectMenu1.ItemLinks.Add(this.iSelectGroup);
            this.SelectMenu1.MenuDrawMode = DevExpress.XtraBars.MenuDrawMode.LargeImagesText;
            this.SelectMenu1.Name = "SelectMenu1";
            this.SelectMenu1.Ribbon = this.ribbon;
            // 
            // iSelectAll
            // 
            this.iSelectAll.Caption = "Select All";
            this.iSelectAll.Description = "Sellect all records / text).";
            this.iSelectAll.Hint = "Sellect all records / text)";
            this.iSelectAll.Id = 173;
            this.iSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("iSelectAll.ImageOptions.Image")));
            this.iSelectAll.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("iSelectAll.ImageOptions.LargeImage")));
            this.iSelectAll.Name = "iSelectAll";
            this.iSelectAll.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iSelectAll_ItemClick);
            // 
            // iSelectGroup
            // 
            this.iSelectGroup.Caption = "Select Group";
            this.iSelectGroup.Description = "Select all records in group.";
            this.iSelectGroup.Hint = "Select all records in group";
            this.iSelectGroup.Id = 172;
            this.iSelectGroup.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("iSelectGroup.ImageOptions.Image")));
            this.iSelectGroup.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("iSelectGroup.ImageOptions.LargeImage")));
            this.iSelectGroup.Name = "iSelectGroup";
            this.iSelectGroup.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iSelectGroup_ItemClick);
            // 
            // iSaveLayout
            // 
            this.iSaveLayout.AllowHtmlText = DevExpress.Utils.DefaultBoolean.False;
            this.iSaveLayout.Caption = "Save Layout";
            this.iSaveLayout.CategoryGuid = new System.Guid("d6bbd404-6dc5-47da-a255-da3ff407a6fe");
            this.iSaveLayout.Description = "Save layout of current screen.";
            this.iSaveLayout.Hint = "Save Layout of current screen";
            this.iSaveLayout.Id = 88;
            this.iSaveLayout.ImageOptions.Image = global::WoodPlan5.Properties.Resources.view_save_32x32;
            this.iSaveLayout.ImageOptions.LargeImage = global::WoodPlan5.Properties.Resources.view_save_32x32;
            this.iSaveLayout.Name = "iSaveLayout";
            this.iSaveLayout.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iSaveLayout_ItemClick);
            // 
            // iSaveLayoutAs
            // 
            this.iSaveLayoutAs.AllowHtmlText = DevExpress.Utils.DefaultBoolean.False;
            this.iSaveLayoutAs.Caption = "Save Layout As...";
            this.iSaveLayoutAs.CategoryGuid = new System.Guid("d6bbd404-6dc5-47da-a255-da3ff407a6fe");
            this.iSaveLayoutAs.Description = "Save current screen layout as new view.";
            this.iSaveLayoutAs.Hint = "Save current screen layout as new view";
            this.iSaveLayoutAs.Id = 89;
            this.iSaveLayoutAs.ImageOptions.Image = global::WoodPlan5.Properties.Resources.view_save_as_32x32;
            this.iSaveLayoutAs.ImageOptions.LargeImage = global::WoodPlan5.Properties.Resources.view_save_as_32x32;
            this.iSaveLayoutAs.Name = "iSaveLayoutAs";
            this.iSaveLayoutAs.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iSaveLayoutAs_ItemClick);
            // 
            // sbiOptions
            // 
            this.sbiOptions.Caption = "Options";
            this.sbiOptions.CategoryGuid = new System.Guid("deebb05d-8009-474c-83ac-a95b2c85199f");
            this.sbiOptions.Id = 99;
            this.sbiOptions.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.iMyOptions),
            new DevExpress.XtraBars.LinkPersistInfo(this.iSystemOptions)});
            this.sbiOptions.MenuDrawMode = DevExpress.XtraBars.MenuDrawMode.LargeImagesText;
            this.sbiOptions.Name = "sbiOptions";
            // 
            // iMyOptions
            // 
            this.iMyOptions.Caption = "My Options";
            this.iMyOptions.CategoryGuid = new System.Guid("deebb05d-8009-474c-83ac-a95b2c85199f");
            this.iMyOptions.Description = "Opens My Options screen for user configuration.";
            this.iMyOptions.Hint = "Opens My Options screen for user configuration";
            this.iMyOptions.Id = 100;
            this.iMyOptions.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("iMyOptions.ImageOptions.Image")));
            this.iMyOptions.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("iMyOptions.ImageOptions.LargeImage")));
            this.iMyOptions.Name = "iMyOptions";
            this.iMyOptions.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iMyOptions_ItemClick);
            // 
            // iSystemOptions
            // 
            this.iSystemOptions.Caption = "System Options";
            this.iSystemOptions.CategoryGuid = new System.Guid("deebb05d-8009-474c-83ac-a95b2c85199f");
            this.iSystemOptions.Description = "Opens System Options screen for system configuration.";
            this.iSystemOptions.Hint = "Opens System Options screen for system configuration";
            this.iSystemOptions.Id = 101;
            this.iSystemOptions.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("iSystemOptions.ImageOptions.Image")));
            this.iSystemOptions.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("iSystemOptions.ImageOptions.LargeImage")));
            this.iSystemOptions.Name = "iSystemOptions";
            this.iSystemOptions.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iSystemOptions_ItemClick);
            // 
            // MicroHelpStaticItem
            // 
            this.MicroHelpStaticItem.Caption = "Ready";
            this.MicroHelpStaticItem.CategoryGuid = new System.Guid("58910032-9fab-4d2b-b381-fb03af240642");
            this.MicroHelpStaticItem.Description = "Microhelp Area - Displays helpful hints and tips.";
            this.MicroHelpStaticItem.Hint = "Microhelp Area - Displays helpful hints and tips";
            this.MicroHelpStaticItem.Id = 111;
            this.MicroHelpStaticItem.Name = "MicroHelpStaticItem";
            toolTipTitleItem1.Text = "Microhelp Area";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "I display helpful hints and tips";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.MicroHelpStaticItem.SuperTip = superToolTip1;
            // 
            // iProgressBar
            // 
            this.iProgressBar.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.iProgressBar.CategoryGuid = new System.Guid("58910032-9fab-4d2b-b381-fb03af240642");
            this.iProgressBar.Description = "Progress Bar - Displays progress of long running tasks.";
            this.iProgressBar.Edit = this.rpiProgressBar;
            this.iProgressBar.EditWidth = 100;
            this.iProgressBar.Hint = "Progress Bar - Displays progress of long running tasks";
            this.iProgressBar.Id = 112;
            this.iProgressBar.Name = "iProgressBar";
            toolTipTitleItem2.Text = "Progress Bar";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "I display the progress of long running tasks";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.iProgressBar.SuperTip = superToolTip2;
            // 
            // rpiProgressBar
            // 
            this.rpiProgressBar.Name = "rpiProgressBar";
            // 
            // iCurrentUser
            // 
            this.iCurrentUser.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.iCurrentUser.Caption = "Current User";
            this.iCurrentUser.CategoryGuid = new System.Guid("58910032-9fab-4d2b-b381-fb03af240642");
            this.iCurrentUser.Description = "Current logged in user.";
            this.iCurrentUser.Hint = "Current logged in user";
            this.iCurrentUser.Id = 113;
            this.iCurrentUser.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("iCurrentUser.ImageOptions.Image")));
            this.iCurrentUser.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("iCurrentUser.ImageOptions.LargeImage")));
            this.iCurrentUser.Name = "iCurrentUser";
            toolTipTitleItem3.Text = "Current User";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I display the name of the logged in user";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.iCurrentUser.SuperTip = superToolTip3;
            this.iCurrentUser.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iCurrentUser_ItemClick);
            // 
            // iDatabase
            // 
            this.iDatabase.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.iDatabase.Caption = "Database";
            this.iDatabase.CategoryGuid = new System.Guid("58910032-9fab-4d2b-b381-fb03af240642");
            this.iDatabase.Description = "Current database connected to.";
            this.iDatabase.Hint = "Current database connected to";
            this.iDatabase.Id = 114;
            this.iDatabase.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("iDatabase.ImageOptions.Image")));
            this.iDatabase.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("iDatabase.ImageOptions.LargeImage")));
            this.iDatabase.Name = "iDatabase";
            toolTipTitleItem4.Text = "Connected Database";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "I hold the database the program is currently connected to";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.iDatabase.SuperTip = superToolTip4;
            // 
            // iGrammerCheck
            // 
            this.iGrammerCheck.Caption = "Grammer Check";
            this.iGrammerCheck.CategoryGuid = new System.Guid("deebb05d-8009-474c-83ac-a95b2c85199f");
            this.iGrammerCheck.Description = "Grammer checks the selected text.";
            this.iGrammerCheck.Enabled = false;
            this.iGrammerCheck.Hint = "Grammer checks the selected text";
            this.iGrammerCheck.Id = 115;
            this.iGrammerCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("iGrammerCheck.ImageOptions.Image")));
            this.iGrammerCheck.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("iGrammerCheck.ImageOptions.LargeImage")));
            this.iGrammerCheck.Name = "iGrammerCheck";
            this.iGrammerCheck.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.iGrammerCheck.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iGrammerCheck_ItemClick);
            // 
            // sbiCommentBank
            // 
            this.sbiCommentBank.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.sbiCommentBank.Caption = "Comment Bank";
            this.sbiCommentBank.CategoryGuid = new System.Guid("deebb05d-8009-474c-83ac-a95b2c85199f");
            this.sbiCommentBank.Description = "Opens the comment Bank (boiler plate text).";
            this.sbiCommentBank.DropDownControl = this.CommentBankMenu1;
            this.sbiCommentBank.Hint = "Opens the comment Bank (boiler plate text)";
            this.sbiCommentBank.Id = 116;
            this.sbiCommentBank.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("sbiCommentBank.ImageOptions.Image")));
            this.sbiCommentBank.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("sbiCommentBank.ImageOptions.LargeImage")));
            this.sbiCommentBank.Name = "sbiCommentBank";
            this.sbiCommentBank.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.sbiCommentBank_ItemClick);
            // 
            // CommentBankMenu1
            // 
            this.CommentBankMenu1.ItemLinks.Add(this.iCommentBank);
            this.CommentBankMenu1.ItemLinks.Add(this.bbiEditComments);
            this.CommentBankMenu1.ItemLinks.Add(this.iCommentBankCodes);
            this.CommentBankMenu1.MenuDrawMode = DevExpress.XtraBars.MenuDrawMode.LargeImagesText;
            this.CommentBankMenu1.Name = "CommentBankMenu1";
            this.CommentBankMenu1.Ribbon = this.ribbon;
            // 
            // iCommentBank
            // 
            this.iCommentBank.Caption = "Comment Bank";
            this.iCommentBank.CategoryGuid = new System.Guid("deebb05d-8009-474c-83ac-a95b2c85199f");
            this.iCommentBank.Description = "Opens the Comment Bank (boiler plate text).";
            this.iCommentBank.Hint = "Opens the Comment Bank (boiler plate text)";
            this.iCommentBank.Id = 157;
            this.iCommentBank.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("iCommentBank.ImageOptions.Image")));
            this.iCommentBank.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("iCommentBank.ImageOptions.LargeImage")));
            this.iCommentBank.Name = "iCommentBank";
            this.iCommentBank.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iCommentBank_ItemClick);
            // 
            // bbiEditComments
            // 
            this.bbiEditComments.Caption = "Comments Editor";
            this.bbiEditComments.Id = 207;
            this.bbiEditComments.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiEditComments.ImageOptions.Image")));
            this.bbiEditComments.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiEditComments.ImageOptions.LargeImage")));
            this.bbiEditComments.Name = "bbiEditComments";
            this.bbiEditComments.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiEditComments_ItemClick);
            // 
            // iCommentBankCodes
            // 
            this.iCommentBankCodes.Caption = "Comment Bank Code List";
            this.iCommentBankCodes.CategoryGuid = new System.Guid("deebb05d-8009-474c-83ac-a95b2c85199f");
            this.iCommentBankCodes.Description = "Opens the Comment Bank Code list.";
            this.iCommentBankCodes.Hint = "Opens the Comment Bank Code list";
            this.iCommentBankCodes.Id = 158;
            this.iCommentBankCodes.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("iCommentBankCodes.ImageOptions.Image")));
            this.iCommentBankCodes.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("iCommentBankCodes.ImageOptions.LargeImage")));
            this.iCommentBankCodes.Name = "iCommentBankCodes";
            this.iCommentBankCodes.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iCommentBankCodes_ItemClick);
            // 
            // iScratchPad
            // 
            this.iScratchPad.Caption = "Scratch Pad";
            this.iScratchPad.CategoryGuid = new System.Guid("deebb05d-8009-474c-83ac-a95b2c85199f");
            this.iScratchPad.Description = "Opens the Scratch Pad (temporary storage area for working on multiple pieces of i" +
    "nformation at once).";
            this.iScratchPad.Enabled = false;
            this.iScratchPad.Hint = "Opens the Scratch Pad (temporary storage area for working on multiple pieces of i" +
    "nformation at once)";
            this.iScratchPad.Id = 117;
            this.iScratchPad.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("iScratchPad.ImageOptions.Image")));
            this.iScratchPad.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("iScratchPad.ImageOptions.LargeImage")));
            this.iScratchPad.Name = "iScratchPad";
            this.iScratchPad.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.iScratchPad.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iScratchPad_ItemClick);
            // 
            // iClipboard
            // 
            this.iClipboard.Caption = "Clipboard";
            this.iClipboard.CategoryGuid = new System.Guid("deebb05d-8009-474c-83ac-a95b2c85199f");
            this.iClipboard.Description = "Opens the Clipboard (stores pieces of text).";
            this.iClipboard.Hint = "Opens the Clipboard (stores pieces of text)";
            this.iClipboard.Id = 118;
            this.iClipboard.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Report_16x16;
            this.iClipboard.ImageOptions.LargeImage = global::WoodPlan5.Properties.Resources.Report_32x32;
            this.iClipboard.Name = "iClipboard";
            this.iClipboard.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iClipboard_ItemClick);
            // 
            // sbiToDoList
            // 
            this.sbiToDoList.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.sbiToDoList.Caption = "To Do List";
            this.sbiToDoList.CategoryGuid = new System.Guid("deebb05d-8009-474c-83ac-a95b2c85199f");
            this.sbiToDoList.Description = "Opens the To Do List.";
            this.sbiToDoList.DropDownControl = this.ToDoListMenu1;
            this.sbiToDoList.Hint = "Opens the To Do List";
            this.sbiToDoList.Id = 119;
            this.sbiToDoList.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Notes_16x16;
            this.sbiToDoList.ImageOptions.LargeImage = global::WoodPlan5.Properties.Resources.Notes_32x32;
            this.sbiToDoList.Name = "sbiToDoList";
            this.sbiToDoList.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.sbiToDoList_ItemClick);
            // 
            // ToDoListMenu1
            // 
            this.ToDoListMenu1.ItemLinks.Add(this.iToDoList);
            this.ToDoListMenu1.ItemLinks.Add(this.iLinkRecordToDoList);
            this.ToDoListMenu1.MenuDrawMode = DevExpress.XtraBars.MenuDrawMode.LargeImagesText;
            this.ToDoListMenu1.Name = "ToDoListMenu1";
            this.ToDoListMenu1.Ribbon = this.ribbon;
            // 
            // iToDoList
            // 
            this.iToDoList.Caption = "To Do List";
            this.iToDoList.CategoryGuid = new System.Guid("deebb05d-8009-474c-83ac-a95b2c85199f");
            this.iToDoList.Description = "Opens the To Do List.";
            this.iToDoList.Hint = "Opens the To Do List";
            this.iToDoList.Id = 154;
            this.iToDoList.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Notes_16x16;
            this.iToDoList.ImageOptions.LargeImage = global::WoodPlan5.Properties.Resources.Notes_32x32;
            this.iToDoList.Name = "iToDoList";
            this.iToDoList.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iToDoList_ItemClick);
            // 
            // iLinkRecordToDoList
            // 
            this.iLinkRecordToDoList.Caption = "Link Record With To Do List";
            this.iLinkRecordToDoList.CategoryGuid = new System.Guid("deebb05d-8009-474c-83ac-a95b2c85199f");
            this.iLinkRecordToDoList.Description = "Links the current record to a new To Do List item.";
            this.iLinkRecordToDoList.Hint = "Links the current record to a new To Do List item";
            this.iLinkRecordToDoList.Id = 155;
            this.iLinkRecordToDoList.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Notes_16x16;
            this.iLinkRecordToDoList.ImageOptions.LargeImage = global::WoodPlan5.Properties.Resources.Notes_32x32;
            this.iLinkRecordToDoList.Name = "iLinkRecordToDoList";
            this.iLinkRecordToDoList.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iLinkRecordToDoList_ItemClick);
            // 
            // iBold
            // 
            this.iBold.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.iBold.Caption = "&Bold";
            this.iBold.CategoryGuid = new System.Guid("d93503f0-608f-4234-a758-ac3aa2a1d0cd");
            this.iBold.Description = "Makes selected text and numbers bold. If the selection is already bold, clicking " +
    "button removes bold formatting.";
            this.iBold.Hint = "Makes selected text and numbers bold. If the selection is already bold, clicking " +
    "button removes bold formatting";
            this.iBold.Id = 121;
            this.iBold.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("iBold.ImageOptions.Image")));
            this.iBold.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("iBold.ImageOptions.LargeImage")));
            this.iBold.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.B));
            this.iBold.Name = "iBold";
            this.iBold.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iBold_ItemClick);
            // 
            // iItalic
            // 
            this.iItalic.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.iItalic.Caption = "&Italic";
            this.iItalic.CategoryGuid = new System.Guid("d93503f0-608f-4234-a758-ac3aa2a1d0cd");
            this.iItalic.Description = "Makes selected text and numbers italic. If the selection is already italic, click" +
    "ing button removes italic formatting.";
            this.iItalic.Hint = "Makes selected text and numbers italic. If the selection is already italic, click" +
    "ing button removes italic formatting";
            this.iItalic.Id = 122;
            this.iItalic.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("iItalic.ImageOptions.Image")));
            this.iItalic.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("iItalic.ImageOptions.LargeImage")));
            this.iItalic.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I));
            this.iItalic.Name = "iItalic";
            this.iItalic.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iItalic_ItemClick);
            // 
            // iUnderline
            // 
            this.iUnderline.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.iUnderline.Caption = "&Underline";
            this.iUnderline.CategoryGuid = new System.Guid("d93503f0-608f-4234-a758-ac3aa2a1d0cd");
            this.iUnderline.Description = "Underlines selected text and numbers. If the selection is already underlined, cli" +
    "cking button removes underlining.";
            this.iUnderline.Hint = "Underlines selected text and numbers. If the selection is already underlined, cli" +
    "cking button removes underlining";
            this.iUnderline.Id = 123;
            this.iUnderline.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("iUnderline.ImageOptions.Image")));
            this.iUnderline.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("iUnderline.ImageOptions.LargeImage")));
            this.iUnderline.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.U));
            this.iUnderline.Name = "iUnderline";
            this.iUnderline.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iUnderline_ItemClick);
            // 
            // iAlignLeft
            // 
            this.iAlignLeft.Caption = "Align &Left";
            this.iAlignLeft.CategoryGuid = new System.Guid("d93503f0-608f-4234-a758-ac3aa2a1d0cd");
            this.iAlignLeft.Description = "Aligns the selected text to the left.";
            this.iAlignLeft.GroupIndex = 1;
            this.iAlignLeft.Hint = "Aligns the selected text to the left";
            this.iAlignLeft.Id = 125;
            this.iAlignLeft.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("iAlignLeft.ImageOptions.Image")));
            this.iAlignLeft.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("iAlignLeft.ImageOptions.LargeImage")));
            this.iAlignLeft.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L));
            this.iAlignLeft.Name = "iAlignLeft";
            this.iAlignLeft.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iAlignLeft_ItemClick);
            // 
            // iAlignCentre
            // 
            this.iAlignCentre.Caption = "Align C&entre";
            this.iAlignCentre.CategoryGuid = new System.Guid("d93503f0-608f-4234-a758-ac3aa2a1d0cd");
            this.iAlignCentre.Description = "Centres the selected text.";
            this.iAlignCentre.GroupIndex = 1;
            this.iAlignCentre.Hint = "Centres the selected text";
            this.iAlignCentre.Id = 126;
            this.iAlignCentre.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("iAlignCentre.ImageOptions.Image")));
            this.iAlignCentre.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("iAlignCentre.ImageOptions.LargeImage")));
            this.iAlignCentre.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
                | System.Windows.Forms.Keys.E));
            this.iAlignCentre.Name = "iAlignCentre";
            this.iAlignCentre.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iAlignCentre_ItemClick);
            // 
            // iAlignRight
            // 
            this.iAlignRight.Caption = "Align &Right";
            this.iAlignRight.CategoryGuid = new System.Guid("d93503f0-608f-4234-a758-ac3aa2a1d0cd");
            this.iAlignRight.Description = "Aligns the selected text to the right.";
            this.iAlignRight.GroupIndex = 1;
            this.iAlignRight.Hint = "Aligns the selected text to the right";
            this.iAlignRight.Id = 127;
            this.iAlignRight.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("iAlignRight.ImageOptions.Image")));
            this.iAlignRight.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("iAlignRight.ImageOptions.LargeImage")));
            this.iAlignRight.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R));
            this.iAlignRight.Name = "iAlignRight";
            this.iAlignRight.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iAlignRight_ItemClick);
            // 
            // iBullet
            // 
            this.iBullet.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.iBullet.Caption = "Bullets";
            this.iBullet.CategoryGuid = new System.Guid("d93503f0-608f-4234-a758-ac3aa2a1d0cd");
            this.iBullet.Description = "Adds bullets to or removes bullets from the selected paragraphs.";
            this.iBullet.Hint = "Adds bullets to or removes bullets from the selected paragraphs";
            this.iBullet.Id = 128;
            this.iBullet.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("iBullet.ImageOptions.Image")));
            this.iBullet.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("iBullet.ImageOptions.LargeImage")));
            this.iBullet.Name = "iBullet";
            this.iBullet.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iBullet_ItemClick);
            // 
            // iFont
            // 
            this.iFont.ActAsDropDown = true;
            this.iFont.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.iFont.Caption = "Font...";
            this.iFont.CategoryGuid = new System.Guid("d93503f0-608f-4234-a758-ac3aa2a1d0cd");
            this.iFont.Description = "Changes the font of the selected text.";
            this.iFont.DropDownControl = this.gddFont;
            this.iFont.Hint = "Changes the font of the selected text";
            this.iFont.Id = 129;
            this.iFont.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("iFont.ImageOptions.Image")));
            this.iFont.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("iFont.ImageOptions.LargeImage")));
            this.iFont.Name = "iFont";
            this.iFont.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.iFont.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iFont_ItemClick);
            // 
            // gddFont
            // 
            // 
            // 
            // 
            this.gddFont.Gallery.Appearance.ItemCaptionAppearance.Normal.Options.UseTextOptions = true;
            this.gddFont.Gallery.Appearance.ItemCaptionAppearance.Normal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gddFont.Gallery.Appearance.ItemCaptionAppearance.Normal.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gddFont.Gallery.Appearance.ItemDescriptionAppearance.Normal.Options.UseTextOptions = true;
            this.gddFont.Gallery.Appearance.ItemDescriptionAppearance.Normal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gddFont.Gallery.Appearance.ItemDescriptionAppearance.Normal.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gddFont.Gallery.ColumnCount = 1;
            this.gddFont.Gallery.FixedImageSize = false;
            galleryItemGroup1.Caption = "Main";
            this.gddFont.Gallery.Groups.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItemGroup[] {
            galleryItemGroup1});
            this.gddFont.Gallery.ItemImageLocation = DevExpress.Utils.Locations.Left;
            this.gddFont.Gallery.RowCount = 6;
            this.gddFont.Gallery.ShowGroupCaption = false;
            this.gddFont.Gallery.ShowItemText = true;
            this.gddFont.Gallery.SizeMode = DevExpress.XtraBars.Ribbon.GallerySizeMode.Vertical;
            this.gddFont.ItemLinks.Add(this.beiFontSize);
            this.gddFont.Name = "gddFont";
            this.gddFont.Ribbon = this.ribbon;
            this.gddFont.Popup += new System.EventHandler(this.gddFont_Popup);
            // 
            // beiFontSize
            // 
            this.beiFontSize.Caption = "Font Size";
            this.beiFontSize.CategoryGuid = new System.Guid("d93503f0-608f-4234-a758-ac3aa2a1d0cd");
            this.beiFontSize.Description = "Changes the font size of the selected text.";
            this.beiFontSize.Edit = this.repositoryItemSpinEdit2;
            this.beiFontSize.Hint = "Changes the font size of the selected text";
            this.beiFontSize.Id = 134;
            this.beiFontSize.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("beiFontSize.ImageOptions.Image")));
            this.beiFontSize.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("beiFontSize.ImageOptions.LargeImage")));
            this.beiFontSize.Name = "beiFontSize";
            this.beiFontSize.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.beiFontSize_ItemClick);
            // 
            // repositoryItemSpinEdit2
            // 
            this.repositoryItemSpinEdit2.AutoHeight = false;
            this.repositoryItemSpinEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit2.IsFloatValue = false;
            this.repositoryItemSpinEdit2.Mask.EditMask = "N00";
            this.repositoryItemSpinEdit2.MaxValue = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.repositoryItemSpinEdit2.MinValue = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.repositoryItemSpinEdit2.Name = "repositoryItemSpinEdit2";
            // 
            // iFontColour
            // 
            this.iFontColour.ActAsDropDown = true;
            this.iFontColour.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.iFontColour.Caption = "Font Colour";
            this.iFontColour.CategoryGuid = new System.Guid("d93503f0-608f-4234-a758-ac3aa2a1d0cd");
            this.iFontColour.Description = "Formats the selected text with the colour you click.";
            this.iFontColour.DropDownControl = this.gddFontColour;
            this.iFontColour.Hint = "Formats the selected text with the colour you click";
            this.iFontColour.Id = 130;
            this.iFontColour.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("iFontColour.ImageOptions.Image")));
            this.iFontColour.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("iFontColour.ImageOptions.LargeImage")));
            this.iFontColour.Name = "iFontColour";
            this.iFontColour.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iFontColor_ItemClick);
            // 
            // gddFontColour
            // 
            // 
            // 
            // 
            this.gddFontColour.Gallery.Appearance.ItemCaptionAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gddFontColour.Gallery.Appearance.ItemCaptionAppearance.Normal.Options.UseFont = true;
            this.gddFontColour.Gallery.Appearance.ItemCaptionAppearance.Normal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gddFontColour.Gallery.FilterCaption = "All Colours";
            this.gddFontColour.Gallery.FixedHoverImageSize = false;
            galleryItemGroup2.Caption = "Web Colours";
            galleryItemGroup3.Caption = "System Colours";
            this.gddFontColour.Gallery.Groups.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItemGroup[] {
            galleryItemGroup2,
            galleryItemGroup3});
            this.gddFontColour.Gallery.ImageSize = new System.Drawing.Size(48, 16);
            this.gddFontColour.Gallery.ItemImageLocation = DevExpress.Utils.Locations.Top;
            this.gddFontColour.Gallery.RowCount = 6;
            this.gddFontColour.Gallery.ShowItemText = true;
            this.gddFontColour.Gallery.SizeMode = DevExpress.XtraBars.Ribbon.GallerySizeMode.Both;
            this.gddFontColour.MenuDrawMode = DevExpress.XtraBars.MenuDrawMode.LargeImagesText;
            this.gddFontColour.Name = "gddFontColour";
            this.gddFontColour.Ribbon = this.ribbon;
            this.gddFontColour.Popup += new System.EventHandler(this.gddFontColor_Popup);
            // 
            // iFilterManager
            // 
            this.iFilterManager.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.iFilterManager.Caption = "Filter <b>Manager</b>";
            this.iFilterManager.CategoryGuid = new System.Guid("de3525c7-5920-4f99-9e40-de6b86c7e1fa");
            this.iFilterManager.Description = "Opens the Filter Manager screen.";
            this.iFilterManager.Hint = "Opens the Filter Manager screen";
            this.iFilterManager.Id = 135;
            this.iFilterManager.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Grid_16x16;
            this.iFilterManager.ImageOptions.LargeImage = global::WoodPlan5.Properties.Resources.Grid_32x32;
            this.iFilterManager.Name = "iFilterManager";
            this.iFilterManager.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iFilterManager_ItemClick);
            // 
            // iFilterCreate
            // 
            this.iFilterCreate.AllowHtmlText = DevExpress.Utils.DefaultBoolean.False;
            this.iFilterCreate.Caption = "Create Filter...";
            this.iFilterCreate.CategoryGuid = new System.Guid("de3525c7-5920-4f99-9e40-de6b86c7e1fa");
            this.iFilterCreate.Description = "Opens the Create filter screen.";
            this.iFilterCreate.Hint = "Opens the Create filter screen";
            this.iFilterCreate.Id = 136;
            this.iFilterCreate.ImageOptions.Image = global::WoodPlan5.Properties.Resources.FilterAdd_32;
            this.iFilterCreate.ImageOptions.LargeImage = global::WoodPlan5.Properties.Resources.FilterAdd_32;
            this.iFilterCreate.Name = "iFilterCreate";
            this.iFilterCreate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iFilterCreate_ItemClick);
            // 
            // iDefaultView
            // 
            this.iDefaultView.Caption = "Default View";
            this.iDefaultView.CategoryGuid = new System.Guid("d6bbd404-6dc5-47da-a255-da3ff407a6fe");
            this.iDefaultView.Id = 138;
            this.iDefaultView.Name = "iDefaultView";
            this.iDefaultView.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iDefaultView_ItemClick);
            // 
            // sbiFind
            // 
            this.sbiFind.Caption = "Find";
            this.sbiFind.CategoryGuid = new System.Guid("c6d5f1ac-52e6-4755-9272-aa24aa54000e");
            this.sbiFind.Description = "Searches for the specified text.";
            this.sbiFind.Hint = "Searches for the specified text";
            this.sbiFind.Id = 140;
            this.sbiFind.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.iFind),
            new DevExpress.XtraBars.LinkPersistInfo(this.iReplace)});
            this.sbiFind.Name = "sbiFind";
            // 
            // iFind
            // 
            this.iFind.Caption = "&Find...";
            this.iFind.CategoryGuid = new System.Guid("c6d5f1ac-52e6-4755-9272-aa24aa54000e");
            this.iFind.Description = "Searches for the specified text.";
            this.iFind.Hint = "Searches for the specified text";
            this.iFind.Id = 141;
            this.iFind.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Find_16x16;
            this.iFind.ImageOptions.LargeImage = global::WoodPlan5.Properties.Resources.Find_32x32;
            this.iFind.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F));
            this.iFind.Name = "iFind";
            this.iFind.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iFind_ItemClick);
            // 
            // iReplace
            // 
            this.iReplace.Caption = "R&eplace...";
            this.iReplace.CategoryGuid = new System.Guid("c6d5f1ac-52e6-4755-9272-aa24aa54000e");
            this.iReplace.Description = "Searches for and replaces the specified text.";
            this.iReplace.Hint = "Searches for and replaces the specified text";
            this.iReplace.Id = 142;
            this.iReplace.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Replace_16x16;
            this.iReplace.ImageOptions.LargeImage = global::WoodPlan5.Properties.Resources.Replace_32x32;
            this.iReplace.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.H));
            this.iReplace.Name = "iReplace";
            this.iReplace.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iReplace_ItemClick);
            // 
            // rgbiSkins
            // 
            this.rgbiSkins.Caption = "Themes";
            this.rgbiSkins.CategoryGuid = new System.Guid("d6bbd404-6dc5-47da-a255-da3ff407a6fe");
            this.rgbiSkins.Description = "Displays a list of different themes.";
            // 
            // 
            // 
            this.rgbiSkins.Gallery.AllowHoverImages = true;
            this.rgbiSkins.Gallery.ColumnCount = 4;
            this.rgbiSkins.Gallery.FixedHoverImageSize = false;
            galleryItemGroup4.Caption = "Main Skins";
            this.rgbiSkins.Gallery.Groups.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItemGroup[] {
            galleryItemGroup4});
            this.rgbiSkins.Gallery.ImageSize = new System.Drawing.Size(32, 17);
            this.rgbiSkins.Gallery.ItemImageLocation = DevExpress.Utils.Locations.Top;
            this.rgbiSkins.Gallery.RowCount = 4;
            this.rgbiSkins.Id = 144;
            this.rgbiSkins.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("rgbiSkins.ImageOptions.Image")));
            this.rgbiSkins.Name = "rgbiSkins";
            // 
            // sbiAdd
            // 
            this.sbiAdd.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.sbiAdd.Caption = "Add";
            this.sbiAdd.CategoryGuid = new System.Guid("51624475-daf8-43f2-b723-ef19db5672a1");
            this.sbiAdd.Description = "Creates a new record.";
            this.sbiAdd.DropDownControl = this.AddMenu1;
            this.sbiAdd.Hint = "Creates a new record";
            this.sbiAdd.Id = 150;
            this.sbiAdd.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Add_16x16;
            this.sbiAdd.ImageOptions.LargeImage = global::WoodPlan5.Properties.Resources.Add_32x32;
            this.sbiAdd.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Insert));
            this.sbiAdd.Name = "sbiAdd";
            this.sbiAdd.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.sbiAdd_ItemClick);
            // 
            // AddMenu1
            // 
            this.AddMenu1.ItemLinks.Add(this.iAdd);
            this.AddMenu1.ItemLinks.Add(this.iBlockAdd);
            this.AddMenu1.MenuDrawMode = DevExpress.XtraBars.MenuDrawMode.LargeImagesText;
            this.AddMenu1.Name = "AddMenu1";
            this.AddMenu1.Ribbon = this.ribbon;
            // 
            // sbiEdit
            // 
            this.sbiEdit.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.sbiEdit.Caption = "Edit";
            this.sbiEdit.CategoryGuid = new System.Guid("51624475-daf8-43f2-b723-ef19db5672a1");
            this.sbiEdit.Description = "Edits the currently selected record(s).";
            this.sbiEdit.DropDownControl = this.EditMenu1;
            this.sbiEdit.Hint = "Edits the currently selected record(s)";
            this.sbiEdit.Id = 152;
            this.sbiEdit.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            this.sbiEdit.ImageOptions.LargeImage = global::WoodPlan5.Properties.Resources.Edit_32x32;
            this.sbiEdit.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E));
            this.sbiEdit.Name = "sbiEdit";
            this.sbiEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.sbiEdit_ItemClick);
            // 
            // EditMenu1
            // 
            this.EditMenu1.ItemLinks.Add(this.iEdit);
            this.EditMenu1.ItemLinks.Add(this.iBlockEdit);
            this.EditMenu1.MenuDrawMode = DevExpress.XtraBars.MenuDrawMode.LargeImagesText;
            this.EditMenu1.Name = "EditMenu1";
            this.EditMenu1.Ribbon = this.ribbon;
            // 
            // iImage
            // 
            this.iImage.Caption = "Image";
            this.iImage.CategoryGuid = new System.Guid("d93503f0-608f-4234-a758-ac3aa2a1d0cd");
            this.iImage.Description = "Adds pictures into the selected text.";
            this.iImage.Hint = "Adds pictures into the selected text";
            this.iImage.Id = 159;
            this.iImage.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("iImage.ImageOptions.Image")));
            this.iImage.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("iImage.ImageOptions.LargeImage")));
            this.iImage.Name = "iImage";
            this.iImage.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iImage_ItemClick);
            // 
            // beiLivePeriod
            // 
            this.beiLivePeriod.CanOpenEdit = false;
            this.beiLivePeriod.Caption = "Live:     ";
            this.beiLivePeriod.CategoryGuid = new System.Guid("ff40d82f-e413-4303-bb72-fbb441bcaf0f");
            this.beiLivePeriod.Description = "Live Period - Data within this date range can be modified.";
            this.beiLivePeriod.Edit = this.LivePeriodLookUpEdit1;
            this.beiLivePeriod.EditWidth = 150;
            this.beiLivePeriod.Id = 162;
            this.beiLivePeriod.Name = "beiLivePeriod";
            toolTipTitleItem5.Text = "Live System Period - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Only data within this date range can be modified.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.beiLivePeriod.SuperTip = superToolTip5;
            this.beiLivePeriod.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.beiLivePeriod_ItemClick);
            // 
            // LivePeriodLookUpEdit1
            // 
            this.LivePeriodLookUpEdit1.Appearance.Options.UseFont = true;
            this.LivePeriodLookUpEdit1.AppearanceDisabled.Options.UseFont = true;
            this.LivePeriodLookUpEdit1.AppearanceDropDown.Options.UseFont = true;
            this.LivePeriodLookUpEdit1.AppearanceDropDownHeader.Options.UseFont = true;
            this.LivePeriodLookUpEdit1.AppearanceFocused.Options.UseFont = true;
            this.LivePeriodLookUpEdit1.AppearanceReadOnly.Options.UseFont = true;
            this.LivePeriodLookUpEdit1.AutoHeight = false;
            this.LivePeriodLookUpEdit1.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("intPeriodID", "intPeriodID", 72, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("strDateDesc", "Date Periods", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("dtFromDate", "dtFromDate", 63, DevExpress.Utils.FormatType.DateTime, "dd/MM/yyyy", false, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("dtToDate", "dtToDate", 51, DevExpress.Utils.FormatType.DateTime, "dd/MM/yyyy", false, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("intSystemID", "intSystemID", 64, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("intLiveID", "intLiveID", 48, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("intViewedID", "intViewedID", 63, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ForeColour", "ForeColour", 59, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("BackColour", "BackColour", 59, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("strDesc", "strDesc", 42, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.LivePeriodLookUpEdit1.DataSource = this.sp00022GetDatePeriodsBindingSource;
            this.LivePeriodLookUpEdit1.DisplayMember = "strDateDesc";
            this.LivePeriodLookUpEdit1.Name = "LivePeriodLookUpEdit1";
            this.LivePeriodLookUpEdit1.NullText = "---";
            this.LivePeriodLookUpEdit1.ValueMember = "intPeriodID";
            // 
            // sp00022GetDatePeriodsBindingSource
            // 
            this.sp00022GetDatePeriodsBindingSource.DataMember = "sp00022GetDatePeriods";
            this.sp00022GetDatePeriodsBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // beiViewedPeriod
            // 
            this.beiViewedPeriod.Caption = "Viewed:";
            this.beiViewedPeriod.CategoryGuid = new System.Guid("ff40d82f-e413-4303-bb72-fbb441bcaf0f");
            this.beiViewedPeriod.Description = "Viewed Period - System will only show data within this date range.";
            this.beiViewedPeriod.Edit = this.ViewedPeriodLookUpEdit1;
            this.beiViewedPeriod.EditWidth = 150;
            this.beiViewedPeriod.Id = 163;
            this.beiViewedPeriod.Name = "beiViewedPeriod";
            superToolTip6.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem6.Text = "Viewed System Period - Information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "The system will only show data within this date range by default.\r\n\r\nDefaults to " +
    "the same value as Live Period.";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this.beiViewedPeriod.SuperTip = superToolTip6;
            this.beiViewedPeriod.EditValueChanged += new System.EventHandler(this.beiViewedPeriod_EditValueChanged);
            // 
            // ViewedPeriodLookUpEdit1
            // 
            this.ViewedPeriodLookUpEdit1.Appearance.Options.UseFont = true;
            this.ViewedPeriodLookUpEdit1.AppearanceDisabled.Options.UseFont = true;
            this.ViewedPeriodLookUpEdit1.AppearanceDropDown.Options.UseFont = true;
            this.ViewedPeriodLookUpEdit1.AppearanceDropDownHeader.Options.UseFont = true;
            this.ViewedPeriodLookUpEdit1.AppearanceFocused.Options.UseFont = true;
            this.ViewedPeriodLookUpEdit1.AppearanceReadOnly.Options.UseFont = true;
            this.ViewedPeriodLookUpEdit1.AutoHeight = false;
            editorButtonImageOptions1.EnableTransparency = false;
            serializableAppearanceObject1.Options.UseFont = true;
            serializableAppearanceObject2.Options.UseFont = true;
            serializableAppearanceObject3.Options.UseFont = true;
            serializableAppearanceObject4.Options.UseFont = true;
            this.ViewedPeriodLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ViewedPeriodLookUpEdit1.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("intPeriodID", "intPeriodID", 72, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("strDateDesc", "Date Periods", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("dtFromDate", "dtFromDate", 63, DevExpress.Utils.FormatType.DateTime, "dd/MM/yyyy", false, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("dtToDate", "dtToDate", 51, DevExpress.Utils.FormatType.DateTime, "dd/MM/yyyy", false, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("intSystemID", "intSystemID", 64, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("intLiveID", "intLiveID", 48, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("intViewedID", "intViewedID", 63, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ForeColour", "ForeColour", 59, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("BackColour", "BackColour", 59, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("strDesc", "strDesc", 42, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.ViewedPeriodLookUpEdit1.DataSource = this.sp00022GetDatePeriodsBindingSource;
            this.ViewedPeriodLookUpEdit1.DisplayMember = "strDateDesc";
            this.ViewedPeriodLookUpEdit1.Name = "ViewedPeriodLookUpEdit1";
            this.ViewedPeriodLookUpEdit1.NullText = "---";
            this.ViewedPeriodLookUpEdit1.ValueMember = "intPeriodID";
            // 
            // iEditSystemPeriods
            // 
            this.iEditSystemPeriods.Caption = "Edit System Periods";
            this.iEditSystemPeriods.CategoryGuid = new System.Guid("ff40d82f-e413-4303-bb72-fbb441bcaf0f");
            this.iEditSystemPeriods.Description = "Opens the System Date Manager for setting up system periods.";
            this.iEditSystemPeriods.Hint = "Opens the System Date Manager for setting up system periods";
            this.iEditSystemPeriods.Id = 164;
            this.iEditSystemPeriods.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("iEditSystemPeriods.ImageOptions.Image")));
            this.iEditSystemPeriods.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("iEditSystemPeriods.ImageOptions.LargeImage")));
            this.iEditSystemPeriods.Name = "iEditSystemPeriods";
            this.iEditSystemPeriods.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iEditSystemPeriods_ItemClick);
            // 
            // iCascade
            // 
            this.iCascade.Id = 230;
            this.iCascade.Name = "iCascade";
            // 
            // iTileHorizontal
            // 
            this.iTileHorizontal.Id = 231;
            this.iTileHorizontal.Name = "iTileHorizontal";
            // 
            // iTileVertical
            // 
            this.iTileVertical.Id = 232;
            this.iTileVertical.Name = "iTileVertical";
            // 
            // barMdiChildrenListItem1
            // 
            this.barMdiChildrenListItem1.Caption = "Open Windows";
            this.barMdiChildrenListItem1.CategoryGuid = new System.Guid("d6bbd404-6dc5-47da-a255-da3ff407a6fe");
            this.barMdiChildrenListItem1.Description = "Shows a list of open windows (click on a window to switch to it).";
            this.barMdiChildrenListItem1.Hint = "Shows a list of open windows (click on a window to switch to it)";
            this.barMdiChildrenListItem1.Id = 170;
            this.barMdiChildrenListItem1.Name = "barMdiChildrenListItem1";
            // 
            // bbiSelect
            // 
            this.bbiSelect.Caption = "Select";
            this.bbiSelect.Id = 178;
            this.bbiSelect.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelect.ImageOptions.Image")));
            this.bbiSelect.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiSelect.ImageOptions.LargeImage")));
            this.bbiSelect.Name = "bbiSelect";
            this.bbiSelect.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSelect_ItemClick);
            // 
            // bbiDelete
            // 
            this.bbiDelete.Caption = "Remove";
            this.bbiDelete.Id = 179;
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            this.bbiDelete.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.LargeImage")));
            this.bbiDelete.Name = "bbiDelete";
            this.bbiDelete.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiDelete_ItemClick);
            // 
            // bbiReportSelect
            // 
            this.bbiReportSelect.Caption = "Select";
            this.bbiReportSelect.Id = 178;
            this.bbiReportSelect.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiReportSelect.ImageOptions.Image")));
            this.bbiReportSelect.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiReportSelect.ImageOptions.LargeImage")));
            this.bbiReportSelect.Name = "bbiReportSelect";
            this.bbiReportSelect.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiReportSelect_ItemClick);
            // 
            // bbiReportDelete
            // 
            this.bbiReportDelete.Caption = "Delete";
            this.bbiReportDelete.Id = 179;
            this.bbiReportDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiReportDelete.ImageOptions.Image")));
            this.bbiReportDelete.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiReportDelete.ImageOptions.LargeImage")));
            this.bbiReportDelete.Name = "bbiReportDelete";
            this.bbiReportDelete.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiReportDelete_ItemClick);
            // 
            // iSaveFilter
            // 
            this.iSaveFilter.AllowHtmlText = DevExpress.Utils.DefaultBoolean.False;
            this.iSaveFilter.Caption = "Save Filter";
            this.iSaveFilter.CategoryGuid = new System.Guid("de3525c7-5920-4f99-9e40-de6b86c7e1fa");
            this.iSaveFilter.Description = "Opens the Save Filter Screen";
            this.iSaveFilter.Hint = "Opens the Save Filter Screen";
            this.iSaveFilter.Id = 181;
            this.iSaveFilter.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("iSaveFilter.ImageOptions.Image")));
            this.iSaveFilter.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("iSaveFilter.ImageOptions.LargeImage")));
            this.iSaveFilter.Name = "iSaveFilter";
            this.iSaveFilter.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iSaveFilter_ItemClick);
            // 
            // bbiDefaultView
            // 
            this.bbiDefaultView.Caption = "Default View";
            this.bbiDefaultView.CategoryGuid = new System.Guid("b6170956-22ef-4aa4-99ed-dafa1c16b2e4");
            this.bbiDefaultView.Id = 182;
            this.bbiDefaultView.Name = "bbiDefaultView";
            this.bbiDefaultView.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiDefaultView_ItemClick);
            // 
            // bsiMyLayouts
            // 
            this.bsiMyLayouts.Caption = "My Layouts";
            this.bsiMyLayouts.CategoryGuid = new System.Guid("b6170956-22ef-4aa4-99ed-dafa1c16b2e4");
            this.bsiMyLayouts.Id = 185;
            this.bsiMyLayouts.Name = "bsiMyLayouts";
            // 
            // bsiAvailableLayouts
            // 
            this.bsiAvailableLayouts.Caption = "Available Layouts";
            this.bsiAvailableLayouts.CategoryGuid = new System.Guid("b6170956-22ef-4aa4-99ed-dafa1c16b2e4");
            this.bsiAvailableLayouts.Id = 186;
            this.bsiAvailableLayouts.Name = "bsiAvailableLayouts";
            // 
            // iLayoutManager
            // 
            this.iLayoutManager.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.iLayoutManager.Caption = "Layout <b>Manager</b>";
            this.iLayoutManager.Id = 198;
            this.iLayoutManager.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Views_16x16;
            this.iLayoutManager.ImageOptions.LargeImage = global::WoodPlan5.Properties.Resources.Views_32x32;
            this.iLayoutManager.Name = "iLayoutManager";
            this.iLayoutManager.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iLayoutManager_ItemClick);
            // 
            // iMyFilters
            // 
            this.iMyFilters.Caption = "My Filters";
            this.iMyFilters.Id = 200;
            this.iMyFilters.Name = "iMyFilters";
            // 
            // iAvailableFilters
            // 
            this.iAvailableFilters.Caption = "Available Filters";
            this.iAvailableFilters.Id = 201;
            this.iAvailableFilters.Name = "iAvailableFilters";
            // 
            // iSaveFilterAs
            // 
            this.iSaveFilterAs.AllowHtmlText = DevExpress.Utils.DefaultBoolean.False;
            this.iSaveFilterAs.Caption = "Save Filter As...";
            this.iSaveFilterAs.Id = 202;
            this.iSaveFilterAs.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("iSaveFilterAs.ImageOptions.Image")));
            this.iSaveFilterAs.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("iSaveFilterAs.ImageOptions.LargeImage")));
            this.iSaveFilterAs.Name = "iSaveFilterAs";
            this.iSaveFilterAs.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iSaveFilterAs_ItemClick);
            // 
            // bbiCloseAll
            // 
            this.bbiCloseAll.Caption = "Close All Forms";
            this.bbiCloseAll.Hint = "Closes all open forms";
            this.bbiCloseAll.Id = 203;
            this.bbiCloseAll.ImageOptions.Image = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiCloseAll.ImageOptions.LargeImage = global::WoodPlan5.Properties.Resources.close_32x32;
            this.bbiCloseAll.Name = "bbiCloseAll";
            this.bbiCloseAll.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCloseAll_ItemClick);
            // 
            // bbiShowLoggedInUsers
            // 
            this.bbiShowLoggedInUsers.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiShowLoggedInUsers.Caption = "<b>Show</b> Logged In Users";
            this.bbiShowLoggedInUsers.Id = 204;
            this.bbiShowLoggedInUsers.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowLoggedInUsers.ImageOptions.Image")));
            this.bbiShowLoggedInUsers.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiShowLoggedInUsers.ImageOptions.LargeImage")));
            this.bbiShowLoggedInUsers.Name = "bbiShowLoggedInUsers";
            this.bbiShowLoggedInUsers.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiShowLoggedInUsers_ItemClick);
            // 
            // bbiChangeCurrentUserSuper
            // 
            this.bbiChangeCurrentUserSuper.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiChangeCurrentUserSuper.Caption = "<b>Change</b> Current User (<b>Login</b> Method)";
            this.bbiChangeCurrentUserSuper.Id = 205;
            this.bbiChangeCurrentUserSuper.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiChangeCurrentUserSuper.ImageOptions.Image")));
            this.bbiChangeCurrentUserSuper.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiChangeCurrentUserSuper.ImageOptions.LargeImage")));
            this.bbiChangeCurrentUserSuper.Name = "bbiChangeCurrentUserSuper";
            this.bbiChangeCurrentUserSuper.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiChangeCurrentUserSuper_ItemClick);
            // 
            // bbiChangeCurrentUser
            // 
            this.bbiChangeCurrentUser.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiChangeCurrentUser.Caption = "<b>Change</b> Current User (<b>Non Login</b> Method)";
            this.bbiChangeCurrentUser.Enabled = false;
            this.bbiChangeCurrentUser.Id = 206;
            this.bbiChangeCurrentUser.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiChangeCurrentUser.ImageOptions.Image")));
            this.bbiChangeCurrentUser.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiChangeCurrentUser.ImageOptions.LargeImage")));
            this.bbiChangeCurrentUser.Name = "bbiChangeCurrentUser";
            this.bbiChangeCurrentUser.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiChangeCurrentUser_ItemClick);
            // 
            // bbiCalculator
            // 
            this.bbiCalculator.Caption = "Calculator";
            this.bbiCalculator.Id = 208;
            this.bbiCalculator.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCalculator.ImageOptions.Image")));
            this.bbiCalculator.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiCalculator.ImageOptions.LargeImage")));
            this.bbiCalculator.Name = "bbiCalculator";
            this.bbiCalculator.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCalculator_ItemClick);
            // 
            // bbiScriptBuilder
            // 
            this.bbiScriptBuilder.Caption = "Script Builder";
            this.bbiScriptBuilder.Enabled = false;
            this.bbiScriptBuilder.Id = 209;
            this.bbiScriptBuilder.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Scripts_16x16;
            this.bbiScriptBuilder.ImageOptions.LargeImage = global::WoodPlan5.Properties.Resources.Scripts_32x32;
            this.bbiScriptBuilder.Name = "bbiScriptBuilder";
            this.bbiScriptBuilder.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.bbiScriptBuilder.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiScriptBuilder_ItemClick);
            // 
            // bbiCloseThisForm
            // 
            this.bbiCloseThisForm.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiCloseThisForm.Caption = "Close <b>this</b> Tab";
            this.bbiCloseThisForm.Id = 210;
            this.bbiCloseThisForm.ImageOptions.Image = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiCloseThisForm.ImageOptions.LargeImage = global::WoodPlan5.Properties.Resources.close_32x32;
            this.bbiCloseThisForm.Name = "bbiCloseThisForm";
            this.bbiCloseThisForm.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCloseThisForm_ItemClick);
            // 
            // bbiCloseAllForms
            // 
            this.bbiCloseAllForms.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiCloseAllForms.Caption = "Close <b>all</b> Tabs";
            this.bbiCloseAllForms.Id = 211;
            this.bbiCloseAllForms.ImageOptions.Image = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiCloseAllForms.ImageOptions.LargeImage = global::WoodPlan5.Properties.Resources.close_32x32;
            this.bbiCloseAllForms.Name = "bbiCloseAllForms";
            this.bbiCloseAllForms.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCloseAllForms_ItemClick);
            // 
            // bbiCloseAllButThisForm
            // 
            this.bbiCloseAllButThisForm.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiCloseAllButThisForm.Caption = "Close <b>all but this</b> Tab";
            this.bbiCloseAllButThisForm.Id = 212;
            this.bbiCloseAllButThisForm.ImageOptions.Image = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiCloseAllButThisForm.ImageOptions.LargeImage = global::WoodPlan5.Properties.Resources.close_32x32;
            this.bbiCloseAllButThisForm.Name = "bbiCloseAllButThisForm";
            this.bbiCloseAllButThisForm.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCloseAllButThisForm_ItemClick);
            // 
            // beTraining
            // 
            this.beTraining.Edit = this.repositoryItemPictureEdit2;
            this.beTraining.Id = 216;
            this.beTraining.Name = "beTraining";
            // 
            // repositoryItemPictureEdit2
            // 
            this.repositoryItemPictureEdit2.Name = "repositoryItemPictureEdit2";
            // 
            // bbiMagnifier
            // 
            this.bbiMagnifier.Caption = "Magnifier";
            this.bbiMagnifier.Enabled = false;
            this.bbiMagnifier.Hint = "Opens the Magnifier screen";
            this.bbiMagnifier.Id = 217;
            this.bbiMagnifier.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Preview_16x16;
            this.bbiMagnifier.ImageOptions.LargeImage = global::WoodPlan5.Properties.Resources.Preview_32x32;
            this.bbiMagnifier.Name = "bbiMagnifier";
            this.bbiMagnifier.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiMagnifier_ItemClick);
            // 
            // iFastFind
            // 
            this.iFastFind.Caption = "Fast Find";
            this.iFastFind.Description = "Opens the Fast Find panel to search for records";
            this.iFastFind.Hint = "Opens the Fast Find panel to search for records";
            this.iFastFind.Id = 219;
            this.iFastFind.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Find_16x16;
            this.iFastFind.ImageOptions.LargeImage = global::WoodPlan5.Properties.Resources.Find_32x32;
            this.iFastFind.Name = "iFastFind";
            this.iFastFind.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iFastFind_ItemClick);
            // 
            // iLoadLayout
            // 
            this.iLoadLayout.AllowHtmlText = DevExpress.Utils.DefaultBoolean.False;
            this.iLoadLayout.Caption = "Load Layout...";
            this.iLoadLayout.Hint = "Load screen layout (replacing current screen layout)";
            this.iLoadLayout.Id = 220;
            this.iLoadLayout.ImageOptions.Image = global::WoodPlan5.Properties.Resources.view_load_32x32;
            this.iLoadLayout.ImageOptions.LargeImage = global::WoodPlan5.Properties.Resources.view_load_32x32;
            this.iLoadLayout.Name = "iLoadLayout";
            this.iLoadLayout.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iLoadLayout_ItemClick);
            // 
            // iLoadFilter
            // 
            this.iLoadFilter.AllowHtmlText = DevExpress.Utils.DefaultBoolean.False;
            this.iLoadFilter.Caption = "Load Filter...";
            this.iLoadFilter.Id = 221;
            this.iLoadFilter.ImageOptions.Image = global::WoodPlan5.Properties.Resources.FilterLoad_32x32;
            this.iLoadFilter.ImageOptions.LargeImage = global::WoodPlan5.Properties.Resources.FilterLoad_32x32;
            this.iLoadFilter.Name = "iLoadFilter";
            this.iLoadFilter.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iLoadFilter_ItemClick);
            // 
            // iAbout
            // 
            this.iAbout.Caption = "About";
            this.iAbout.Hint = "Displays general program information";
            this.iAbout.Id = 222;
            this.iAbout.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_16x16;
            this.iAbout.ImageOptions.LargeImage = global::WoodPlan5.Properties.Resources.Info_32x32;
            this.iAbout.Name = "iAbout";
            this.iAbout.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iAbout_ItemClick);
            // 
            // bbiChartPalette
            // 
            this.bbiChartPalette.ActAsDropDown = true;
            this.bbiChartPalette.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.bbiChartPalette.Caption = "Pallet";
            this.bbiChartPalette.DropDownControl = this.gddChartPalette;
            this.bbiChartPalette.Id = 223;
            this.bbiChartPalette.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiChartPalette.ImageOptions.Image")));
            this.bbiChartPalette.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiChartPalette.ImageOptions.LargeImage")));
            this.bbiChartPalette.Name = "bbiChartPalette";
            // 
            // gddChartPalette
            // 
            // 
            // 
            // 
            this.gddChartPalette.Gallery.Appearance.ItemCaptionAppearance.Normal.Options.UseTextOptions = true;
            this.gddChartPalette.Gallery.Appearance.ItemCaptionAppearance.Normal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gddChartPalette.Gallery.ColumnCount = 1;
            this.gddChartPalette.Gallery.DrawImageBackground = false;
            this.gddChartPalette.Gallery.FixedImageSize = false;
            galleryItemGroup5.Caption = "Built-In";
            this.gddChartPalette.Gallery.Groups.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItemGroup[] {
            galleryItemGroup5});
            this.gddChartPalette.Gallery.ItemCheckMode = DevExpress.XtraBars.Ribbon.Gallery.ItemCheckMode.SingleRadio;
            this.gddChartPalette.Gallery.ItemImageLayout = DevExpress.Utils.Drawing.ImageLayoutMode.MiddleLeft;
            this.gddChartPalette.Gallery.ItemImageLocation = DevExpress.Utils.Locations.Right;
            this.gddChartPalette.Gallery.RowCount = 15;
            this.gddChartPalette.Gallery.ShowItemText = true;
            this.gddChartPalette.Gallery.SizeMode = DevExpress.XtraBars.Ribbon.GallerySizeMode.Vertical;
            this.gddChartPalette.Gallery.UseMaxImageSize = true;
            this.gddChartPalette.Name = "gddChartPalette";
            this.gddChartPalette.Ribbon = this.ribbon;
            this.gddChartPalette.GalleryItemClick += new DevExpress.XtraBars.Ribbon.GalleryItemClickEventHandler(this.gddChartPalette_GalleryItemClick);
            // 
            // rgbiChartAppearance
            // 
            this.rgbiChartAppearance.Caption = "ribbonGalleryBarItem1";
            // 
            // 
            // 
            this.rgbiChartAppearance.Gallery.ImageSize = new System.Drawing.Size(80, 45);
            this.rgbiChartAppearance.Gallery.ItemCheckMode = DevExpress.XtraBars.Ribbon.Gallery.ItemCheckMode.SingleRadio;
            this.rgbiChartAppearance.Id = 224;
            this.rgbiChartAppearance.Name = "rgbiChartAppearance";
            this.rgbiChartAppearance.GalleryItemClick += new DevExpress.XtraBars.Ribbon.GalleryItemClickEventHandler(this.rgbiChartAppearance_GalleryItemClick);
            this.rgbiChartAppearance.GalleryInitDropDownGallery += new DevExpress.XtraBars.Ribbon.InplaceGalleryEventHandler(this.rgbiChartAppearance_GalleryInitDropDownGallery);
            this.rgbiChartAppearance.GalleryPopupClose += new DevExpress.XtraBars.Ribbon.InplaceGalleryEventHandler(this.rgbiChartAppearance_GalleryPopupClose);
            // 
            // rgbiChartTypes
            // 
            this.rgbiChartTypes.Caption = "ribbonGalleryBarItem1";
            // 
            // 
            // 
            this.rgbiChartTypes.Gallery.ImageSize = new System.Drawing.Size(48, 48);
            this.rgbiChartTypes.Gallery.ItemCheckMode = DevExpress.XtraBars.Ribbon.Gallery.ItemCheckMode.SingleRadio;
            this.rgbiChartTypes.Id = 225;
            this.rgbiChartTypes.Name = "rgbiChartTypes";
            this.rgbiChartTypes.GalleryItemClick += new DevExpress.XtraBars.Ribbon.GalleryItemClickEventHandler(this.rgbiChartTypes_GalleryItemClick);
            this.rgbiChartTypes.GalleryInitDropDownGallery += new DevExpress.XtraBars.Ribbon.InplaceGalleryEventHandler(this.rgbiChartTypes_GalleryInitDropDownGallery);
            this.rgbiChartTypes.GalleryPopupClose += new DevExpress.XtraBars.Ribbon.InplaceGalleryEventHandler(this.rgbiChartTypes_GalleryPopupClose);
            // 
            // bbiChartWizard
            // 
            this.bbiChartWizard.Caption = "Wizard";
            this.bbiChartWizard.Hint = "Open Chart Wizard";
            this.bbiChartWizard.Id = 226;
            this.bbiChartWizard.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiChartWizard.ImageOptions.LargeImage")));
            this.bbiChartWizard.Name = "bbiChartWizard";
            this.bbiChartWizard.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiChartWizard_ItemClick);
            // 
            // bbiRotateAxis
            // 
            this.bbiRotateAxis.Caption = "Rotate Axis";
            this.bbiRotateAxis.Hint = "Rotate Chart Axis";
            this.bbiRotateAxis.Id = 227;
            this.bbiRotateAxis.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRotateAxis.ImageOptions.Image")));
            this.bbiRotateAxis.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiRotateAxis.ImageOptions.LargeImage")));
            this.bbiRotateAxis.Name = "bbiRotateAxis";
            this.bbiRotateAxis.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRotateAxis_ItemClick);
            // 
            // bbiLayoutFlip
            // 
            this.bbiLayoutFlip.Caption = "Flip Layout";
            this.bbiLayoutFlip.Hint = "Flip Screen Layout";
            this.bbiLayoutFlip.Id = 228;
            this.bbiLayoutFlip.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiLayoutFlip.ImageOptions.Image")));
            this.bbiLayoutFlip.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiLayoutFlip.ImageOptions.LargeImage")));
            this.bbiLayoutFlip.Name = "bbiLayoutFlip";
            this.bbiLayoutFlip.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLayoutFlip_ItemClick);
            // 
            // bbiLayoutRotate
            // 
            this.bbiLayoutRotate.Caption = "Rotate Layout";
            this.bbiLayoutRotate.Hint = "Rotate Screen Layout";
            this.bbiLayoutRotate.Id = 229;
            this.bbiLayoutRotate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiLayoutRotate.ImageOptions.Image")));
            this.bbiLayoutRotate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiLayoutRotate.ImageOptions.LargeImage")));
            this.bbiLayoutRotate.Name = "bbiLayoutRotate";
            this.bbiLayoutRotate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLayoutRotate_ItemClick);
            // 
            // bbiVisitWebSite
            // 
            this.bbiVisitWebSite.Caption = "Visit Website";
            this.bbiVisitWebSite.Hint = "Opens the Ground Control Website";
            this.bbiVisitWebSite.Id = 233;
            this.bbiVisitWebSite.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiVisitWebSite.ImageOptions.Image")));
            this.bbiVisitWebSite.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiVisitWebSite.ImageOptions.LargeImage")));
            this.bbiVisitWebSite.Name = "bbiVisitWebSite";
            this.bbiVisitWebSite.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiVisitWebSite_ItemClick);
            // 
            // bbiColourMixer
            // 
            this.bbiColourMixer.Caption = "Colour Mixer";
            this.bbiColourMixer.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.bbiColourMixer.Id = 234;
            this.bbiColourMixer.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiColourMixer.ImageOptions.Image")));
            this.bbiColourMixer.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiColourMixer.ImageOptions.LargeImage")));
            this.bbiColourMixer.Name = "bbiColourMixer";
            this.bbiColourMixer.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiColourMixer_ItemClick);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup11,
            this.ribbonPageGroup2,
            this.ribbonPageGroup5,
            this.ribbonPageGroup6});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "Core";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.sbiAdd);
            this.ribbonPageGroup1.ItemLinks.Add(this.sbiEdit);
            this.ribbonPageGroup1.ItemLinks.Add(this.iDelete);
            this.ribbonPageGroup1.ItemLinks.Add(this.iSave);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "Data";
            this.ribbonPageGroup1.CaptionButtonClick += new DevExpress.XtraBars.Ribbon.RibbonPageGroupEventHandler(this.ribbonPageGroup1_CaptionButtonClick);
            // 
            // ribbonPageGroup11
            // 
            this.ribbonPageGroup11.ItemLinks.Add(this.iLoadFilter);
            this.ribbonPageGroup11.ItemLinks.Add(this.iFilterCreate);
            this.ribbonPageGroup11.ItemLinks.Add(this.iSaveFilter);
            this.ribbonPageGroup11.ItemLinks.Add(this.iSaveFilterAs);
            this.ribbonPageGroup11.Name = "ribbonPageGroup11";
            this.ribbonPageGroup11.Text = "Filter";
            this.ribbonPageGroup11.CaptionButtonClick += new DevExpress.XtraBars.Ribbon.RibbonPageGroupEventHandler(this.ribbonPageGroup11_CaptionButtonClick);
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.iCut);
            this.ribbonPageGroup2.ItemLinks.Add(this.iCopy);
            this.ribbonPageGroup2.ItemLinks.Add(this.iPaste);
            this.ribbonPageGroup2.ItemLinks.Add(this.iUndo);
            this.ribbonPageGroup2.ItemLinks.Add(this.iRedo);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.Text = "Edit";
            this.ribbonPageGroup2.CaptionButtonClick += new DevExpress.XtraBars.Ribbon.RibbonPageGroupEventHandler(this.ribbonPageGroup2_CaptionButtonClick);
            // 
            // ribbonPageGroup5
            // 
            this.ribbonPageGroup5.ItemLinks.Add(this.iPreview);
            this.ribbonPageGroup5.ItemLinks.Add(this.iPrint);
            this.ribbonPageGroup5.Name = "ribbonPageGroup5";
            this.ribbonPageGroup5.Text = "Printing";
            this.ribbonPageGroup5.CaptionButtonClick += new DevExpress.XtraBars.Ribbon.RibbonPageGroupEventHandler(this.ribbonPageGroup5_CaptionButtonClick);
            // 
            // ribbonPageGroup6
            // 
            this.ribbonPageGroup6.ItemLinks.Add(this.beiLivePeriod);
            this.ribbonPageGroup6.ItemLinks.Add(this.beiViewedPeriod);
            this.ribbonPageGroup6.Name = "ribbonPageGroup6";
            this.ribbonPageGroup6.Text = "System Periods";
            this.ribbonPageGroup6.CaptionButtonClick += new DevExpress.XtraBars.Ribbon.RibbonPageGroupEventHandler(this.ribbonPageGroup6_CaptionButtonClick);
            // 
            // ribbonPage3
            // 
            this.ribbonPage3.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup7,
            this.ribbonPageGroup8});
            this.ribbonPage3.Name = "ribbonPage3";
            this.ribbonPage3.Text = "Format";
            this.ribbonPage3.Visible = false;
            // 
            // ribbonPageGroup7
            // 
            this.ribbonPageGroup7.ItemLinks.Add(this.iBold);
            this.ribbonPageGroup7.ItemLinks.Add(this.iItalic);
            this.ribbonPageGroup7.ItemLinks.Add(this.iUnderline);
            this.ribbonPageGroup7.ItemLinks.Add(this.iAlignLeft, true);
            this.ribbonPageGroup7.ItemLinks.Add(this.iAlignCentre);
            this.ribbonPageGroup7.ItemLinks.Add(this.iAlignRight);
            this.ribbonPageGroup7.ItemLinks.Add(this.iBullet, true);
            this.ribbonPageGroup7.ItemLinks.Add(this.iImage);
            this.ribbonPageGroup7.Name = "ribbonPageGroup7";
            this.ribbonPageGroup7.ShowCaptionButton = false;
            this.ribbonPageGroup7.Text = "Format";
            // 
            // ribbonPageGroup8
            // 
            this.ribbonPageGroup8.ItemLinks.Add(this.iFont);
            this.ribbonPageGroup8.ItemLinks.Add(this.iFontColour);
            this.ribbonPageGroup8.Name = "ribbonPageGroup8";
            this.ribbonPageGroup8.ShowCaptionButton = false;
            this.ribbonPageGroup8.Text = "Font";
            // 
            // ribbonPage2
            // 
            this.ribbonPage2.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup3});
            this.ribbonPage2.Name = "ribbonPage2";
            this.ribbonPage2.Text = "Tools";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.iSpellCheck);
            this.ribbonPageGroup3.ItemLinks.Add(this.iGrammerCheck);
            this.ribbonPageGroup3.ItemLinks.Add(this.sbiToDoList);
            this.ribbonPageGroup3.ItemLinks.Add(this.sbiCommentBank);
            this.ribbonPageGroup3.ItemLinks.Add(this.iScratchPad);
            this.ribbonPageGroup3.ItemLinks.Add(this.iClipboard);
            this.ribbonPageGroup3.ItemLinks.Add(this.bbiCalculator);
            this.ribbonPageGroup3.ItemLinks.Add(this.bbiScriptBuilder);
            this.ribbonPageGroup3.ItemLinks.Add(this.iFastFind);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.Text = "Tools";
            this.ribbonPageGroup3.CaptionButtonClick += new DevExpress.XtraBars.Ribbon.RibbonPageGroupEventHandler(this.ribbonPageGroup3_CaptionButtonClick);
            // 
            // ribbonPage5
            // 
            this.ribbonPage5.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup9,
            this.ribbonPageGroup4});
            this.ribbonPage5.Name = "ribbonPage5";
            this.ribbonPage5.Text = "View";
            // 
            // ribbonPageGroup9
            // 
            this.ribbonPageGroup9.ItemLinks.Add(this.iLoadLayout);
            this.ribbonPageGroup9.ItemLinks.Add(this.iSaveLayout);
            this.ribbonPageGroup9.ItemLinks.Add(this.iSaveLayoutAs);
            this.ribbonPageGroup9.Name = "ribbonPageGroup9";
            this.ribbonPageGroup9.Text = "View";
            this.ribbonPageGroup9.CaptionButtonClick += new DevExpress.XtraBars.Ribbon.RibbonPageGroupEventHandler(this.ribbonPageGroup9_CaptionButtonClick);
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.ItemLinks.Add(this.rgbiSkins);
            this.ribbonPageGroup4.ItemLinks.Add(this.bbiColourMixer);
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            this.ribbonPageGroup4.ShowCaptionButton = false;
            this.ribbonPageGroup4.Text = "Appearance";
            // 
            // ribbonPage4
            // 
            this.ribbonPage4.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup13,
            this.ribbonPageGroup12,
            this.ribbonPageGroup14,
            this.ribbonPageGroup15});
            this.ribbonPage4.Name = "ribbonPage4";
            this.ribbonPage4.Text = "Charting";
            // 
            // ribbonPageGroup13
            // 
            this.ribbonPageGroup13.ItemLinks.Add(this.rgbiChartTypes);
            this.ribbonPageGroup13.Name = "ribbonPageGroup13";
            this.ribbonPageGroup13.ShowCaptionButton = false;
            this.ribbonPageGroup13.Text = "Chart Type";
            // 
            // ribbonPageGroup12
            // 
            this.ribbonPageGroup12.ItemLinks.Add(this.bbiChartPalette, true);
            this.ribbonPageGroup12.ItemLinks.Add(this.rgbiChartAppearance);
            this.ribbonPageGroup12.Name = "ribbonPageGroup12";
            this.ribbonPageGroup12.ShowCaptionButton = false;
            this.ribbonPageGroup12.Text = "Chart Appearance";
            // 
            // ribbonPageGroup14
            // 
            this.ribbonPageGroup14.ItemLinks.Add(this.bbiRotateAxis);
            this.ribbonPageGroup14.ItemLinks.Add(this.bbiChartWizard, true);
            this.ribbonPageGroup14.Name = "ribbonPageGroup14";
            this.ribbonPageGroup14.ShowCaptionButton = false;
            this.ribbonPageGroup14.Text = "Chart Other";
            // 
            // ribbonPageGroup15
            // 
            this.ribbonPageGroup15.ItemLinks.Add(this.bbiLayoutFlip);
            this.ribbonPageGroup15.ItemLinks.Add(this.bbiLayoutRotate);
            this.ribbonPageGroup15.Name = "ribbonPageGroup15";
            this.ribbonPageGroup15.ShowCaptionButton = false;
            this.ribbonPageGroup15.Text = "Layout";
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit1.IsFloatValue = false;
            this.repositoryItemSpinEdit1.Mask.EditMask = "N00";
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.MinValue = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // repositoryItemPopupContainerEdit1
            // 
            this.repositoryItemPopupContainerEdit1.AutoHeight = false;
            this.repositoryItemPopupContainerEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit1.Name = "repositoryItemPopupContainerEdit1";
            // 
            // repositoryItemPictureEdit1
            // 
            this.repositoryItemPictureEdit1.Name = "repositoryItemPictureEdit1";
            this.repositoryItemPictureEdit1.ShowMenu = false;
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.ItemLinks.Add(this.MicroHelpStaticItem);
            this.ribbonStatusBar.ItemLinks.Add(this.iCurrentUser, true);
            this.ribbonStatusBar.ItemLinks.Add(this.iDatabase, true);
            this.ribbonStatusBar.ItemLinks.Add(this.iProgressBar, true);
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 599);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            this.ribbonStatusBar.Size = new System.Drawing.Size(1105, 23);
            // 
            // gddStaff
            // 
            // 
            // 
            // 
            this.gddStaff.Gallery.AllowHoverImages = true;
            this.gddStaff.Gallery.Appearance.ItemCaptionAppearance.Normal.Options.UseTextOptions = true;
            this.gddStaff.Gallery.Appearance.ItemCaptionAppearance.Normal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gddStaff.Gallery.Appearance.ItemCaptionAppearance.Normal.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gddStaff.Gallery.Appearance.ItemDescriptionAppearance.Normal.Options.UseTextOptions = true;
            this.gddStaff.Gallery.Appearance.ItemDescriptionAppearance.Normal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gddStaff.Gallery.ColumnCount = 3;
            galleryItemGroup6.Caption = "Staff";
            this.gddStaff.Gallery.Groups.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItemGroup[] {
            galleryItemGroup6});
            this.gddStaff.Gallery.HoverImageSize = new System.Drawing.Size(130, 180);
            this.gddStaff.Gallery.ImageSize = new System.Drawing.Size(26, 36);
            this.gddStaff.Gallery.ItemImageLocation = DevExpress.Utils.Locations.Left;
            this.gddStaff.Gallery.RowCount = 6;
            this.gddStaff.Gallery.ShowItemText = true;
            this.gddStaff.Gallery.SizeMode = DevExpress.XtraBars.Ribbon.GallerySizeMode.Both;
            this.gddStaff.Name = "gddStaff";
            this.gddStaff.Ribbon = this.ribbon;
            this.gddStaff.Popup += new System.EventHandler(this.gddStaff_Popup);
            // 
            // clientPanel
            // 
            this.clientPanel.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.clientPanel.Controls.Add(this.popupControlContainer1);
            this.clientPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clientPanel.Location = new System.Drawing.Point(200, 147);
            this.clientPanel.Name = "clientPanel";
            this.clientPanel.Size = new System.Drawing.Size(905, 452);
            this.clientPanel.TabIndex = 2;
            this.clientPanel.Visible = false;
            // 
            // popupControlContainer1
            // 
            this.popupControlContainer1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.popupControlContainer1.Location = new System.Drawing.Point(829, 29);
            this.popupControlContainer1.Name = "popupControlContainer1";
            this.popupControlContainer1.Size = new System.Drawing.Size(44, 46);
            this.popupControlContainer1.TabIndex = 7;
            this.popupControlContainer1.Visible = false;
            // 
            // defaultLookAndFeel1
            // 
            this.defaultLookAndFeel1.LookAndFeel.SkinName = "Blue";
            // 
            // DataEditMenu1
            // 
            this.DataEditMenu1.ItemLinks.Add(this.iUndo);
            this.DataEditMenu1.ItemLinks.Add(this.iRedo);
            this.DataEditMenu1.ItemLinks.Add(this.iCopy, true);
            this.DataEditMenu1.ItemLinks.Add(this.iCut);
            this.DataEditMenu1.ItemLinks.Add(this.iPaste);
            this.DataEditMenu1.ItemLinks.Add(this.iClear);
            this.DataEditMenu1.ItemLinks.Add(this.sbiSelectAll, true);
            this.DataEditMenu1.ItemLinks.Add(this.iFind, true);
            this.DataEditMenu1.ItemLinks.Add(this.iReplace);
            this.DataEditMenu1.MenuDrawMode = DevExpress.XtraBars.MenuDrawMode.LargeImagesText;
            this.DataEditMenu1.Name = "DataEditMenu1";
            this.DataEditMenu1.Ribbon = this.ribbon;
            // 
            // ViewMenu1
            // 
            this.ViewMenu1.ItemLinks.Add(this.iSaveLayout, true);
            this.ViewMenu1.ItemLinks.Add(this.iSaveLayoutAs);
            this.ViewMenu1.MenuDrawMode = DevExpress.XtraBars.MenuDrawMode.LargeImagesText;
            this.ViewMenu1.Name = "ViewMenu1";
            this.ViewMenu1.Ribbon = this.ribbon;
            // 
            // ToolsMenu1
            // 
            this.ToolsMenu1.ItemLinks.Add(this.iSpellCheck);
            this.ToolsMenu1.ItemLinks.Add(this.iGrammerCheck);
            this.ToolsMenu1.ItemLinks.Add(this.sbiToDoList);
            this.ToolsMenu1.ItemLinks.Add(this.sbiCommentBank);
            this.ToolsMenu1.ItemLinks.Add(this.iScratchPad);
            this.ToolsMenu1.ItemLinks.Add(this.iClipboard);
            this.ToolsMenu1.ItemLinks.Add(this.sbiOptions, true);
            this.ToolsMenu1.MenuDrawMode = DevExpress.XtraBars.MenuDrawMode.LargeImagesText;
            this.ToolsMenu1.Name = "ToolsMenu1";
            this.ToolsMenu1.Ribbon = this.ribbon;
            // 
            // DataMenu1
            // 
            this.DataMenu1.ItemLinks.Add(this.sbiAdd);
            this.DataMenu1.ItemLinks.Add(this.sbiEdit);
            this.DataMenu1.ItemLinks.Add(this.iDelete);
            this.DataMenu1.ItemLinks.Add(this.iSave);
            this.DataMenu1.MenuDrawMode = DevExpress.XtraBars.MenuDrawMode.LargeImagesText;
            this.DataMenu1.Name = "DataMenu1";
            this.DataMenu1.Ribbon = this.ribbon;
            // 
            // PrintingMenu1
            // 
            this.PrintingMenu1.ItemLinks.Add(this.iPreview);
            this.PrintingMenu1.ItemLinks.Add(this.iPrint);
            this.PrintingMenu1.ItemLinks.Add(this.iPrintSetup, true);
            this.PrintingMenu1.MenuDrawMode = DevExpress.XtraBars.MenuDrawMode.LargeImagesText;
            this.PrintingMenu1.Name = "PrintingMenu1";
            this.PrintingMenu1.Ribbon = this.ribbon;
            // 
            // SystemPeriodsMenu1
            // 
            this.SystemPeriodsMenu1.ItemLinks.Add(this.iEditSystemPeriods);
            this.SystemPeriodsMenu1.MenuDrawMode = DevExpress.XtraBars.MenuDrawMode.LargeImagesText;
            this.SystemPeriodsMenu1.Name = "SystemPeriodsMenu1";
            this.SystemPeriodsMenu1.Ribbon = this.ribbon;
            // 
            // dockManager1
            // 
            this.dockManager1.Controller = this.barAndDockingController1;
            this.dockManager1.Form = this;
            this.dockManager1.HiddenPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dpActiveForm});
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.panelContainer2});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            this.dockManager1.ClosingPanel += new DevExpress.XtraBars.Docking.DockPanelCancelEventHandler(this.dockManager1_ClosingPanel);
            this.dockManager1.ClosedPanel += new DevExpress.XtraBars.Docking.DockPanelEventHandler(this.dockManager1_ClosedPanel);
            // 
            // dpActiveForm
            // 
            this.dpActiveForm.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.dpActiveForm.Appearance.Options.UseBackColor = true;
            this.dpActiveForm.Appearance.Options.UseFont = true;
            this.dpActiveForm.Controls.Add(this.dockPanel3_Container);
            this.dpActiveForm.Dock = DevExpress.XtraBars.Docking.DockingStyle.Bottom;
            this.dpActiveForm.FloatVertical = true;
            this.dpActiveForm.ID = new System.Guid("be6ea986-f495-4e94-a9db-b2271df752ec");
            this.dpActiveForm.Location = new System.Drawing.Point(0, 228);
            this.dpActiveForm.Name = "dpActiveForm";
            this.dpActiveForm.Options.ShowCloseButton = false;
            this.dpActiveForm.OriginalSize = new System.Drawing.Size(200, 71);
            this.dpActiveForm.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Bottom;
            this.dpActiveForm.SavedIndex = 1;
            this.dpActiveForm.SavedParent = this.panelContainer2;
            this.dpActiveForm.Size = new System.Drawing.Size(200, 71);
            this.dpActiveForm.Text = "Active Form";
            this.dpActiveForm.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            // 
            // dockPanel3_Container
            // 
            this.dockPanel3_Container.Controls.Add(this.nbcActiveForm);
            this.dockPanel3_Container.Location = new System.Drawing.Point(3, 29);
            this.dockPanel3_Container.Name = "dockPanel3_Container";
            this.dockPanel3_Container.Size = new System.Drawing.Size(194, 39);
            this.dockPanel3_Container.TabIndex = 0;
            // 
            // nbcActiveForm
            // 
            this.nbcActiveForm.Appearance.GroupHeader.Options.UseFont = true;
            this.nbcActiveForm.Appearance.Item.Options.UseFont = true;
            this.nbcActiveForm.ContentButtonHint = null;
            this.nbcActiveForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nbcActiveForm.Location = new System.Drawing.Point(0, 0);
            this.nbcActiveForm.Name = "nbcActiveForm";
            this.nbcActiveForm.OptionsNavPane.ExpandedWidth = 194;
            this.nbcActiveForm.Size = new System.Drawing.Size(194, 39);
            this.nbcActiveForm.TabIndex = 0;
            this.nbcActiveForm.Text = "navBarControl1";
            this.nbcActiveForm.View = new DevExpress.XtraNavBar.ViewInfo.SkinExplorerBarViewInfoRegistrator();
            this.nbcActiveForm.CustomDrawGroupCaption += new DevExpress.XtraNavBar.ViewInfo.CustomDrawNavBarElementEventHandler(this.nbcActiveForm_CustomDrawGroupCaption);
            this.nbcActiveForm.CustomDrawLink += new DevExpress.XtraNavBar.ViewInfo.CustomDrawNavBarElementEventHandler(this.nbcActiveForm_CustomDrawLink);
            this.nbcActiveForm.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.nbcActiveForm_LinkClicked);
            // 
            // panelContainer2
            // 
            this.panelContainer2.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panelContainer2.Appearance.Options.UseBackColor = true;
            this.panelContainer2.Controls.Add(this.panelContainer1);
            this.panelContainer2.Controls.Add(this.panelContainer3);
            this.panelContainer2.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.panelContainer2.ID = new System.Guid("1050124a-8472-45f3-895f-9b0bf6f1a123");
            this.panelContainer2.Location = new System.Drawing.Point(0, 147);
            this.panelContainer2.Name = "panelContainer2";
            this.panelContainer2.OriginalSize = new System.Drawing.Size(200, 200);
            this.panelContainer2.Size = new System.Drawing.Size(200, 452);
            this.panelContainer2.Text = "panelContainer2";
            // 
            // panelContainer1
            // 
            this.panelContainer1.ActiveChild = this.dockPanel1;
            this.panelContainer1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panelContainer1.Appearance.Options.UseBackColor = true;
            this.panelContainer1.Appearance.Options.UseFont = true;
            this.panelContainer1.Controls.Add(this.dockPanel1);
            this.panelContainer1.Controls.Add(this.dockPanel2);
            this.panelContainer1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Bottom;
            this.panelContainer1.ID = new System.Guid("25a108ef-8b67-4b05-8890-5edc1622a927");
            this.panelContainer1.Location = new System.Drawing.Point(0, 0);
            this.panelContainer1.Name = "panelContainer1";
            this.panelContainer1.Options.ShowCloseButton = false;
            this.panelContainer1.OriginalSize = new System.Drawing.Size(200, 283);
            this.panelContainer1.Size = new System.Drawing.Size(200, 321);
            this.panelContainer1.Tabbed = true;
            this.panelContainer1.Text = "Switchboard";
            // 
            // dockPanel1
            // 
            this.dockPanel1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.dockPanel1.Appearance.Options.UseBackColor = true;
            this.dockPanel1.Appearance.Options.UseFont = true;
            this.dockPanel1.Controls.Add(this.dockPanel1_Container);
            this.dockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Fill;
            this.dockPanel1.ID = new System.Guid("2d357a86-ec8f-467f-ac7a-d43a8b20beae");
            this.dockPanel1.Location = new System.Drawing.Point(3, 29);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.Options.ShowCloseButton = false;
            this.dockPanel1.OriginalSize = new System.Drawing.Size(194, 268);
            this.dockPanel1.Size = new System.Drawing.Size(193, 267);
            this.dockPanel1.Text = "Modules";
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.nbcSwitchboard);
            this.dockPanel1_Container.Location = new System.Drawing.Point(0, 0);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(193, 267);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // nbcSwitchboard
            // 
            this.nbcSwitchboard.Appearance.GroupHeader.Options.UseFont = true;
            this.nbcSwitchboard.Appearance.Item.Options.UseFont = true;
            this.nbcSwitchboard.Appearance.Item.Options.UseImage = true;
            this.nbcSwitchboard.Appearance.ItemActive.Options.UseFont = true;
            this.nbcSwitchboard.ContentButtonHint = null;
            this.nbcSwitchboard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nbcSwitchboard.Location = new System.Drawing.Point(0, 0);
            this.nbcSwitchboard.Name = "nbcSwitchboard";
            this.nbcSwitchboard.OptionsNavPane.ExpandedWidth = 193;
            this.nbcSwitchboard.PaintStyleKind = DevExpress.XtraNavBar.NavBarViewKind.ExplorerBar;
            this.nbcSwitchboard.Size = new System.Drawing.Size(193, 267);
            this.nbcSwitchboard.StoreDefaultPaintStyleName = true;
            this.nbcSwitchboard.TabIndex = 2;
            this.nbcSwitchboard.Text = "navBarControl1";
            this.nbcSwitchboard.NavDragOver += new DevExpress.XtraNavBar.ViewInfo.NavBarDragDropEventHandler(this.nbcSwitchboard_NavDragOver);
            this.nbcSwitchboard.CustomDrawGroupCaption += new DevExpress.XtraNavBar.ViewInfo.CustomDrawNavBarElementEventHandler(this.nbcSwitchboard_CustomDrawGroupCaption);
            this.nbcSwitchboard.CustomDrawLink += new DevExpress.XtraNavBar.ViewInfo.CustomDrawNavBarElementEventHandler(this.nbcSwitchboard_CustomDrawLink);
            this.nbcSwitchboard.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.nbcSwitchboard_LinkClicked);
            this.nbcSwitchboard.DragDrop += new System.Windows.Forms.DragEventHandler(this.nbcSwitchboard_DragDrop);
            this.nbcSwitchboard.MouseDown += new System.Windows.Forms.MouseEventHandler(this.nbcSwitchboard_MouseDown);
            // 
            // dockPanel2
            // 
            this.dockPanel2.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.dockPanel2.Appearance.Options.UseBackColor = true;
            this.dockPanel2.Appearance.Options.UseFont = true;
            this.dockPanel2.Controls.Add(this.dockPanel2_Container);
            this.dockPanel2.Dock = DevExpress.XtraBars.Docking.DockingStyle.Fill;
            this.dockPanel2.FloatVertical = true;
            this.dockPanel2.ID = new System.Guid("3e85491e-a1e1-4247-9822-7b63006fa28e");
            this.dockPanel2.Location = new System.Drawing.Point(3, 29);
            this.dockPanel2.Name = "dockPanel2";
            this.dockPanel2.Options.ShowCloseButton = false;
            this.dockPanel2.OriginalSize = new System.Drawing.Size(194, 268);
            this.dockPanel2.Size = new System.Drawing.Size(193, 267);
            this.dockPanel2.Text = "Reports";
            // 
            // dockPanel2_Container
            // 
            this.dockPanel2_Container.Controls.Add(this.nbcReports);
            this.dockPanel2_Container.Location = new System.Drawing.Point(0, 0);
            this.dockPanel2_Container.Name = "dockPanel2_Container";
            this.dockPanel2_Container.Size = new System.Drawing.Size(193, 267);
            this.dockPanel2_Container.TabIndex = 0;
            // 
            // nbcReports
            // 
            this.nbcReports.Appearance.Item.Options.UseFont = true;
            this.nbcReports.ContentButtonHint = null;
            this.nbcReports.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nbcReports.Location = new System.Drawing.Point(0, 0);
            this.nbcReports.Name = "nbcReports";
            this.nbcReports.OptionsNavPane.ExpandedWidth = 193;
            this.nbcReports.PaintStyleKind = DevExpress.XtraNavBar.NavBarViewKind.ExplorerBar;
            this.nbcReports.Size = new System.Drawing.Size(193, 267);
            this.nbcReports.StoreDefaultPaintStyleName = true;
            this.nbcReports.TabIndex = 1;
            this.nbcReports.Text = "navBarControl2";
            this.nbcReports.NavDragOver += new DevExpress.XtraNavBar.ViewInfo.NavBarDragDropEventHandler(this.nbcReports_NavDragOver);
            this.nbcReports.CustomDrawGroupCaption += new DevExpress.XtraNavBar.ViewInfo.CustomDrawNavBarElementEventHandler(this.nbcReports_CustomDrawGroupCaption);
            this.nbcReports.CustomDrawLink += new DevExpress.XtraNavBar.ViewInfo.CustomDrawNavBarElementEventHandler(this.nbcReports_CustomDrawLink);
            this.nbcReports.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.nbcReports_LinkClicked);
            this.nbcReports.DragDrop += new System.Windows.Forms.DragEventHandler(this.nbcReports_DragDrop);
            this.nbcReports.MouseDown += new System.Windows.Forms.MouseEventHandler(this.nbcReports_MouseDown);
            // 
            // panelContainer3
            // 
            this.panelContainer3.ActiveChild = this.dpPersonImage;
            this.panelContainer3.Controls.Add(this.dockPanelSiteInspection);
            this.panelContainer3.Controls.Add(this.dpPersonImage);
            this.panelContainer3.Dock = DevExpress.XtraBars.Docking.DockingStyle.Fill;
            this.panelContainer3.FloatSize = new System.Drawing.Size(300, 450);
            this.panelContainer3.FloatVertical = true;
            this.panelContainer3.ID = new System.Guid("0d70911a-e4fa-4142-afd6-a2a6b6c3feed");
            this.panelContainer3.Location = new System.Drawing.Point(0, 321);
            this.panelContainer3.Name = "panelContainer3";
            this.panelContainer3.OriginalSize = new System.Drawing.Size(200, 169);
            this.panelContainer3.Size = new System.Drawing.Size(200, 131);
            this.panelContainer3.Tabbed = true;
            this.panelContainer3.Text = "panelContainer3";
            // 
            // dpPersonImage
            // 
            this.dpPersonImage.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.dpPersonImage.Appearance.Options.UseBackColor = true;
            this.dpPersonImage.Appearance.Options.UseFont = true;
            this.dpPersonImage.Controls.Add(this.controlContainer1);
            this.dpPersonImage.Dock = DevExpress.XtraBars.Docking.DockingStyle.Fill;
            this.dpPersonImage.FloatSize = new System.Drawing.Size(300, 450);
            this.dpPersonImage.FloatVertical = true;
            this.dpPersonImage.ID = new System.Guid("b4c9080f-6728-434c-8f0e-d74a80d0ccb4");
            this.dpPersonImage.Location = new System.Drawing.Point(3, 29);
            this.dpPersonImage.Name = "dpPersonImage";
            this.dpPersonImage.Options.ShowCloseButton = false;
            this.dpPersonImage.OriginalSize = new System.Drawing.Size(194, 78);
            this.dpPersonImage.Size = new System.Drawing.Size(193, 78);
            this.dpPersonImage.Text = "Image Preview";
            // 
            // controlContainer1
            // 
            this.controlContainer1.Controls.Add(this.xtraScrollableControl1);
            this.controlContainer1.Location = new System.Drawing.Point(0, 0);
            this.controlContainer1.Name = "controlContainer1";
            this.controlContainer1.Size = new System.Drawing.Size(193, 78);
            this.controlContainer1.TabIndex = 0;
            // 
            // xtraScrollableControl1
            // 
            this.xtraScrollableControl1.Controls.Add(this.pePersonsPhoto);
            this.xtraScrollableControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraScrollableControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraScrollableControl1.Name = "xtraScrollableControl1";
            this.xtraScrollableControl1.Size = new System.Drawing.Size(193, 78);
            this.xtraScrollableControl1.TabIndex = 1;
            // 
            // pePersonsPhoto
            // 
            this.pePersonsPhoto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pePersonsPhoto.Location = new System.Drawing.Point(0, 0);
            this.pePersonsPhoto.Name = "pePersonsPhoto";
            this.pePersonsPhoto.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pePersonsPhoto.Properties.Appearance.Options.UseBackColor = true;
            this.pePersonsPhoto.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.pePersonsPhoto.Properties.ShowMenu = false;
            this.pePersonsPhoto.Size = new System.Drawing.Size(193, 78);
            this.pePersonsPhoto.TabIndex = 0;
            this.pePersonsPhoto.Visible = false;
            // 
            // dockPanelSiteInspection
            // 
            this.dockPanelSiteInspection.Controls.Add(this.controlContainer2);
            this.dockPanelSiteInspection.Dock = DevExpress.XtraBars.Docking.DockingStyle.Fill;
            this.dockPanelSiteInspection.ID = new System.Guid("9acd9dda-73d9-4d8c-9f53-43917afd59bd");
            this.dockPanelSiteInspection.Location = new System.Drawing.Point(3, 29);
            this.dockPanelSiteInspection.Name = "dockPanelSiteInspection";
            this.dockPanelSiteInspection.Options.ShowCloseButton = false;
            this.dockPanelSiteInspection.OriginalSize = new System.Drawing.Size(194, 78);
            this.dockPanelSiteInspection.Size = new System.Drawing.Size(193, 78);
            this.dockPanelSiteInspection.Text = "Site Inspection";
            // 
            // controlContainer2
            // 
            this.controlContainer2.Controls.Add(this.layoutControl1);
            this.controlContainer2.Location = new System.Drawing.Point(0, 0);
            this.controlContainer2.Name = "controlContainer2";
            this.controlContainer2.Size = new System.Drawing.Size(193, 78);
            this.controlContainer2.TabIndex = 0;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btnResume);
            this.layoutControl1.Controls.Add(this.labelControlSelectedInspection);
            this.layoutControl1.Controls.Add(this.btnStart);
            this.layoutControl1.Controls.Add(this.btnPause);
            this.layoutControl1.Controls.Add(this.btnFinish);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(193, 78);
            this.layoutControl1.TabIndex = 4;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btnResume
            // 
            this.btnResume.Enabled = false;
            this.btnResume.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.btnResume.Location = new System.Drawing.Point(103, 43);
            this.btnResume.Name = "btnResume";
            this.btnResume.Size = new System.Drawing.Size(88, 22);
            this.btnResume.StyleController = this.layoutControl1;
            toolTipTitleItem7.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Appearance.Options.UseImage = true;
            toolTipTitleItem7.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Text = "Resume Inspection button - Information";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Click me to resume a puased Site Inspection.";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            this.btnResume.SuperTip = superToolTip7;
            this.btnResume.TabIndex = 4;
            this.btnResume.Text = "Resume";
            this.btnResume.Click += new System.EventHandler(this.btnResume_Click);
            // 
            // labelControlSelectedInspection
            // 
            this.labelControlSelectedInspection.AllowHtmlString = true;
            this.labelControlSelectedInspection.Appearance.Font = new System.Drawing.Font("Tahoma", 7F);
            this.labelControlSelectedInspection.Appearance.Options.UseFont = true;
            this.labelControlSelectedInspection.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControlSelectedInspection.Location = new System.Drawing.Point(2, 2);
            this.labelControlSelectedInspection.Name = "labelControlSelectedInspection";
            this.labelControlSelectedInspection.Size = new System.Drawing.Size(189, 11);
            this.labelControlSelectedInspection.StyleController = this.layoutControl1;
            this.labelControlSelectedInspection.TabIndex = 2;
            this.labelControlSelectedInspection.Text = "No Inspection, <b>Click Start</b> to Proceed";
            // 
            // btnStart
            // 
            this.btnStart.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Add_16x16;
            this.btnStart.Location = new System.Drawing.Point(2, 17);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(97, 22);
            this.btnStart.StyleController = this.layoutControl1;
            toolTipTitleItem8.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem8.Appearance.Options.UseImage = true;
            toolTipTitleItem8.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem8.Text = "Start Inspection button - Information";
            toolTipItem8.LeftIndent = 6;
            toolTipItem8.Text = "Click me to start a new Site Inspection.";
            superToolTip8.Items.Add(toolTipTitleItem8);
            superToolTip8.Items.Add(toolTipItem8);
            this.btnStart.SuperTip = superToolTip8;
            this.btnStart.TabIndex = 0;
            this.btnStart.Text = "Start";
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnPause
            // 
            this.btnPause.Enabled = false;
            this.btnPause.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnPause.ImageOptions.Image")));
            this.btnPause.Location = new System.Drawing.Point(2, 43);
            this.btnPause.Name = "btnPause";
            this.btnPause.Size = new System.Drawing.Size(97, 22);
            this.btnPause.StyleController = this.layoutControl1;
            toolTipTitleItem9.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem9.Appearance.Options.UseImage = true;
            toolTipTitleItem9.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem9.Text = "Pause Inspection button - Information";
            toolTipItem9.LeftIndent = 6;
            superToolTip9.Items.Add(toolTipTitleItem9);
            superToolTip9.Items.Add(toolTipItem9);
            this.btnPause.SuperTip = superToolTip9;
            this.btnPause.TabIndex = 3;
            this.btnPause.Text = "Pause";
            this.btnPause.Click += new System.EventHandler(this.btnPause_Click);
            // 
            // btnFinish
            // 
            this.btnFinish.Enabled = false;
            this.btnFinish.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnFinish.ImageOptions.Image")));
            this.btnFinish.Location = new System.Drawing.Point(103, 17);
            this.btnFinish.Name = "btnFinish";
            this.btnFinish.Size = new System.Drawing.Size(88, 22);
            this.btnFinish.StyleController = this.layoutControl1;
            toolTipTitleItem10.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem10.Appearance.Options.UseImage = true;
            toolTipTitleItem10.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem10.Text = "Finish Inspection button - Information";
            toolTipItem10.LeftIndent = 6;
            toolTipItem10.Text = "Click me to complete a Site Inspection. \r\n\r\nOn completing, the system will enter " +
    "the end time and calculate how long the inspection took.";
            superToolTip10.Items.Add(toolTipTitleItem10);
            superToolTip10.Items.Add(toolTipItem10);
            this.btnFinish.SuperTip = superToolTip10;
            this.btnFinish.TabIndex = 1;
            this.btnFinish.Text = "Finish";
            this.btnFinish.Click += new System.EventHandler(this.btnFinish_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem1,
            this.emptySpaceItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem5});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(193, 78);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.labelControlSelectedInspection;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(193, 15);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.btnStart;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 15);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(101, 26);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(101, 67);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(92, 11);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.btnFinish;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(101, 15);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(92, 26);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnPause;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 41);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(101, 37);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.btnResume;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(101, 41);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(92, 26);
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // splitterControl1
            // 
            this.splitterControl1.Location = new System.Drawing.Point(200, 147);
            this.splitterControl1.Name = "splitterControl1";
            this.splitterControl1.Size = new System.Drawing.Size(6, 452);
            this.splitterControl1.TabIndex = 6;
            this.splitterControl1.TabStop = false;
            // 
            // sp00022GetDatePeriodsTableAdapter
            // 
            this.sp00022GetDatePeriodsTableAdapter.ClearBeforeFill = true;
            // 
            // WindowMenu1
            // 
            this.WindowMenu1.ItemLinks.Add(this.iCascade, true);
            this.WindowMenu1.ItemLinks.Add(this.iTileHorizontal);
            this.WindowMenu1.ItemLinks.Add(this.iTileVertical);
            this.WindowMenu1.ItemLinks.Add(this.barMdiChildrenListItem1, true);
            this.WindowMenu1.MenuDrawMode = DevExpress.XtraBars.MenuDrawMode.LargeImagesText;
            this.WindowMenu1.Name = "WindowMenu1";
            this.WindowMenu1.Ribbon = this.ribbon;
            // 
            // tmrNotification
            // 
            this.tmrNotification.Enabled = true;
            this.tmrNotification.Interval = 60000D;
            this.tmrNotification.SynchronizingObject = this;
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipText = "Information Centre";
            this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip1;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "Information Centre";
            this.notifyIcon1.DoubleClick += new System.EventHandler(this.notifyIcon1_DoubleClick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openGiroToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(208, 48);
            // 
            // openGiroToolStripMenuItem
            // 
            this.openGiroToolStripMenuItem.Name = "openGiroToolStripMenuItem";
            this.openGiroToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.openGiroToolStripMenuItem.Text = "Show Information Centre";
            this.openGiroToolStripMenuItem.Click += new System.EventHandler(this.openGiroToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // sp00020GetUserApplicationSettingsBindingSource
            // 
            this.sp00020GetUserApplicationSettingsBindingSource.DataMember = "sp00020GetUserApplicationSettings";
            this.sp00020GetUserApplicationSettingsBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // sp00020GetUserApplicationSettingsTableAdapter
            // 
            this.sp00020GetUserApplicationSettingsTableAdapter.ClearBeforeFill = true;
            // 
            // sp00019GetUserDetailsForStartUpBindingSource
            // 
            this.sp00019GetUserDetailsForStartUpBindingSource.DataMember = "sp00019GetUserDetailsForStartUp";
            this.sp00019GetUserDetailsForStartUpBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // sp00019GetUserDetailsForStartUpTableAdapter
            // 
            this.sp00019GetUserDetailsForStartUpTableAdapter.ClearBeforeFill = true;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // SwitchboardMenu
            // 
            this.SwitchboardMenu.ItemLinks.Add(this.bbiSelect);
            this.SwitchboardMenu.ItemLinks.Add(this.bbiDelete);
            this.SwitchboardMenu.Name = "SwitchboardMenu";
            this.SwitchboardMenu.Ribbon = this.ribbon;
            // 
            // ReportsMenu
            // 
            this.ReportsMenu.ItemLinks.Add(this.bbiReportSelect);
            this.ReportsMenu.ItemLinks.Add(this.bbiReportDelete);
            this.ReportsMenu.Name = "ReportsMenu";
            this.ReportsMenu.Ribbon = this.ribbon;
            // 
            // sp00001PopulateSwitchboardBindingSource
            // 
            this.sp00001PopulateSwitchboardBindingSource.DataMember = "sp00001PopulateSwitchboard";
            this.sp00001PopulateSwitchboardBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // sp00001PopulateSwitchboardTableAdapter
            // 
            this.sp00001PopulateSwitchboardTableAdapter.ClearBeforeFill = true;
            // 
            // FilterMenu
            // 
            this.FilterMenu.ItemLinks.Add(this.iLoadFilter, true);
            this.FilterMenu.ItemLinks.Add(this.iFilterCreate);
            this.FilterMenu.ItemLinks.Add(this.iSaveFilter);
            this.FilterMenu.ItemLinks.Add(this.iSaveFilterAs);
            this.FilterMenu.ItemLinks.Add(this.iFilterManager, true);
            this.FilterMenu.MenuDrawMode = DevExpress.XtraBars.MenuDrawMode.LargeImagesText;
            this.FilterMenu.Name = "FilterMenu";
            this.FilterMenu.Ribbon = this.ribbon;
            // 
            // LayoutMenu
            // 
            this.LayoutMenu.ItemLinks.Add(this.iLoadLayout);
            this.LayoutMenu.ItemLinks.Add(this.iSaveLayout);
            this.LayoutMenu.ItemLinks.Add(this.iSaveLayoutAs);
            this.LayoutMenu.ItemLinks.Add(this.iLayoutManager, true);
            this.LayoutMenu.MenuDrawMode = DevExpress.XtraBars.MenuDrawMode.LargeImagesText;
            this.LayoutMenu.Name = "LayoutMenu";
            this.LayoutMenu.Ribbon = this.ribbon;
            // 
            // pmCurrentUserMenu
            // 
            this.pmCurrentUserMenu.ItemLinks.Add(this.bbiShowLoggedInUsers);
            this.pmCurrentUserMenu.ItemLinks.Add(this.bbiChangeCurrentUserSuper, true);
            this.pmCurrentUserMenu.ItemLinks.Add(this.bbiChangeCurrentUser);
            this.pmCurrentUserMenu.Name = "pmCurrentUserMenu";
            this.pmCurrentUserMenu.Ribbon = this.ribbon;
            // 
            // pmChildForms
            // 
            this.pmChildForms.ItemLinks.Add(this.bbiCloseThisForm);
            this.pmChildForms.ItemLinks.Add(this.bbiCloseAllForms);
            this.pmChildForms.ItemLinks.Add(this.bbiCloseAllButThisForm);
            this.pmChildForms.Name = "pmChildForms";
            this.pmChildForms.Ribbon = this.ribbon;
            // 
            // sp00068_Get_Magnifier_SettingsBindingSource
            // 
            this.sp00068_Get_Magnifier_SettingsBindingSource.DataMember = "sp00068_Get_Magnifier_Settings";
            this.sp00068_Get_Magnifier_SettingsBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // sp00068_Get_Magnifier_SettingsTableAdapter
            // 
            this.sp00068_Get_Magnifier_SettingsTableAdapter.ClearBeforeFill = true;
            // 
            // sp00071_get_available_loginsBindingSource
            // 
            this.sp00071_get_available_loginsBindingSource.DataMember = "sp00071_get_available_logins";
            this.sp00071_get_available_loginsBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // sp00071_get_available_loginsTableAdapter
            // 
            this.sp00071_get_available_loginsTableAdapter.ClearBeforeFill = true;
            // 
            // styleController1
            // 
            this.styleController1.Appearance.Options.UseFont = true;
            this.styleController1.AppearanceDisabled.Options.UseFont = true;
            this.styleController1.AppearanceDropDown.Options.UseFont = true;
            this.styleController1.AppearanceDropDownHeader.Options.UseFont = true;
            this.styleController1.AppearanceFocused.Options.UseFont = true;
            this.styleController1.AppearanceReadOnly.Options.UseFont = true;
            // 
            // lcNonLiveDB
            // 
            this.lcNonLiveDB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lcNonLiveDB.Appearance.BackColor = System.Drawing.Color.Red;
            this.lcNonLiveDB.Appearance.BorderColor = System.Drawing.Color.White;
            this.lcNonLiveDB.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lcNonLiveDB.Appearance.ForeColor = System.Drawing.Color.White;
            this.lcNonLiveDB.Appearance.Options.UseBackColor = true;
            this.lcNonLiveDB.Appearance.Options.UseBorderColor = true;
            this.lcNonLiveDB.Appearance.Options.UseFont = true;
            this.lcNonLiveDB.Appearance.Options.UseForeColor = true;
            this.lcNonLiveDB.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lcNonLiveDB.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.lcNonLiveDB.Location = new System.Drawing.Point(879, 7);
            this.lcNonLiveDB.Name = "lcNonLiveDB";
            this.lcNonLiveDB.Size = new System.Drawing.Size(111, 19);
            this.lcNonLiveDB.TabIndex = 14;
            this.lcNonLiveDB.Text = " Non-Live Database ";
            this.lcNonLiveDB.Visible = false;
            // 
            // lcDataTransferMode
            // 
            this.lcDataTransferMode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lcDataTransferMode.Appearance.BackColor = System.Drawing.Color.Red;
            this.lcDataTransferMode.Appearance.BorderColor = System.Drawing.Color.White;
            this.lcDataTransferMode.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lcDataTransferMode.Appearance.ForeColor = System.Drawing.Color.White;
            this.lcDataTransferMode.Appearance.Options.UseBackColor = true;
            this.lcDataTransferMode.Appearance.Options.UseBorderColor = true;
            this.lcDataTransferMode.Appearance.Options.UseFont = true;
            this.lcDataTransferMode.Appearance.Options.UseForeColor = true;
            this.lcDataTransferMode.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lcDataTransferMode.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.lcDataTransferMode.Location = new System.Drawing.Point(779, 7);
            this.lcDataTransferMode.Name = "lcDataTransferMode";
            this.lcDataTransferMode.Size = new System.Drawing.Size(97, 19);
            this.lcDataTransferMode.TabIndex = 18;
            this.lcDataTransferMode.Text = " Tablet PC Mode";
            this.lcDataTransferMode.Visible = false;
            // 
            // imageCollectionChartTypes
            // 
            this.imageCollectionChartTypes.ImageSize = new System.Drawing.Size(48, 48);
            this.imageCollectionChartTypes.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollectionChartTypes.ImageStream")));
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertGalleryImage("ide_16x16.png", "images/programming/ide_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/programming/ide_16x16.png"), 0);
            this.imageCollection1.Images.SetKeyName(0, "ide_16x16.png");
            this.imageCollection1.Images.SetKeyName(1, "manager.png");
            this.imageCollection1.Images.SetKeyName(2, "incidents_16.png");
            this.imageCollection1.Images.SetKeyName(3, "show_map_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Person_16x16.png");
            this.imageCollection1.Images.SetKeyName(5, "Views_16x16.png");
            this.imageCollection1.Images.SetKeyName(6, "Period_16x16.png");
            this.imageCollection1.Images.SetKeyName(7, "Revenue_16x16.png");
            this.imageCollection1.InsertGalleryImage("team_16x16.png", "images/people/team_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/people/team_16x16.png"), 8);
            this.imageCollection1.Images.SetKeyName(8, "team_16x16.png");
            this.imageCollection1.Images.SetKeyName(9, "EditRangePermission_16x16.png");
            this.imageCollection1.Images.SetKeyName(10, "RevenueSplit_16x16.png");
            this.imageCollection1.Images.SetKeyName(11, "Preview_16x16.png");
            this.imageCollection1.Images.SetKeyName(12, "CustomersKPI_16x16.png");
            this.imageCollection1.Images.SetKeyName(13, "TopCustomers_16x16.png");
            this.imageCollection1.Images.SetKeyName(14, "filtermanager_16.png");
            this.imageCollection1.Images.SetKeyName(15, "Picklist_Manager_16.png");
            this.imageCollection1.Images.SetKeyName(16, "cascade_16.png");
            this.imageCollection1.Images.SetKeyName(17, "Categories_16x16.png");
            this.imageCollection1.Images.SetKeyName(18, "mai_lmerge_16.gif");
            this.imageCollection1.Images.SetKeyName(19, "CustomerRevenue_16x16.png");
            this.imageCollection1.Images.SetKeyName(20, "View_16x16.png");
            this.imageCollection1.Images.SetKeyName(21, "View_16x16.png");
            this.imageCollection1.Images.SetKeyName(22, "Tree_16.png");
            this.imageCollection1.Images.SetKeyName(23, "CustomerInfoCards_16x16.png");
            this.imageCollection1.InsertGalleryImage("home_16x16.png", "images/navigation/home_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/navigation/home_16x16.png"), 24);
            this.imageCollection1.Images.SetKeyName(24, "home_16x16.png");
            this.imageCollection1.InsertGalleryImage("time_16x16.png", "images/scheduling/time_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/scheduling/time_16x16.png"), 25);
            this.imageCollection1.Images.SetKeyName(25, "time_16x16.png");
            this.imageCollection1.Images.SetKeyName(26, "Employee_16x16.png");
            this.imageCollection1.Images.SetKeyName(27, "thermometer_16.png");
            this.imageCollection1.Images.SetKeyName(28, "Ok_16x16.png");
            this.imageCollection1.Images.SetKeyName(29, "weather-few-clouds.png");
            this.imageCollection1.Images.SetKeyName(30, "thermometer_snowflake_16.png");
            this.imageCollection1.InsertGalleryImage("customer_16x16.png", "images/people/customer_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/people/customer_16x16.png"), 31);
            this.imageCollection1.Images.SetKeyName(31, "customer_16x16.png");
            this.imageCollection1.Images.SetKeyName(32, "Finance.png");
            this.imageCollection1.Images.SetKeyName(33, "job_sheet_numbering_16.png");
            this.imageCollection1.InsertGalleryImage("gaugestylehalfcircular_16x16.png", "images/gauges/gaugestylehalfcircular_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/gauges/gaugestylehalfcircular_16x16.png"), 34);
            this.imageCollection1.Images.SetKeyName(34, "gaugestylehalfcircular_16x16.png");
            this.imageCollection1.Images.SetKeyName(35, "quote_16.png");
            this.imageCollection1.Images.SetKeyName(36, "thumb_up_16.png");
            this.imageCollection1.Images.SetKeyName(37, "thumb_down_16.png");
            this.imageCollection1.Images.SetKeyName(38, "CRM_16.png");
            this.imageCollection1.Images.SetKeyName(39, "defaults_16.png");
            this.imageCollection1.Images.SetKeyName(40, "WorkInProgress_16.png");
            this.imageCollection1.Images.SetKeyName(41, "framework16.png");
            this.imageCollection1.Images.SetKeyName(42, "Issue_16.png");
            this.imageCollection1.InsertGalleryImage("task_16x16.png", "images/tasks/task_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/tasks/task_16x16.png"), 43);
            this.imageCollection1.Images.SetKeyName(43, "task_16x16.png");
            this.imageCollection1.Images.SetKeyName(44, "circuit_16.png");
            this.imageCollection1.Images.SetKeyName(45, "UT_poles_16.png");
            this.imageCollection1.Images.SetKeyName(46, "surveyed_poles_16.png");
            this.imageCollection1.Images.SetKeyName(47, "Permissions_Manager_16.png");
            this.imageCollection1.InsertGalleryImage("switchtimescalesto_16x16.png", "images/scheduling/switchtimescalesto_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/scheduling/switchtimescalesto_16x16.png"), 48);
            this.imageCollection1.Images.SetKeyName(48, "switchtimescalesto_16x16.png");
            this.imageCollection1.InsertGalleryImage("monthview_16x16.png", "images/scheduling/monthview_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/scheduling/monthview_16x16.png"), 49);
            this.imageCollection1.Images.SetKeyName(49, "monthview_16x16.png");
            this.imageCollection1.InsertGalleryImage("contentarrangeinrows_16x16.png", "images/alignment/contentarrangeinrows_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/alignment/contentarrangeinrows_16x16.png"), 50);
            this.imageCollection1.Images.SetKeyName(50, "contentarrangeinrows_16x16.png");
            this.imageCollection1.InsertGalleryImage("findcustomers_16x16.png", "images/find/findcustomers_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/find/findcustomers_16x16.png"), 51);
            this.imageCollection1.Images.SetKeyName(51, "findcustomers_16x16.png");
            this.imageCollection1.Images.SetKeyName(52, "employee_contract_16.png");
            this.imageCollection1.Images.SetKeyName(53, "shutdown_16.png");
            this.imageCollection1.Images.SetKeyName(54, "Find_16x16.png");
            this.imageCollection1.Images.SetKeyName(55, "employee_bonus_16.png");
            this.imageCollection1.InsertGalleryImage("documentmap_16x16.png", "images/navigation/documentmap_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/navigation/documentmap_16x16.png"), 56);
            this.imageCollection1.Images.SetKeyName(56, "documentmap_16x16.png");
            this.imageCollection1.Images.SetKeyName(57, "employee_crm.png");
            this.imageCollection1.InsertGalleryImage("country_16x16.png", "images/miscellaneous/country_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/miscellaneous/country_16x16.png"), 58);
            this.imageCollection1.Images.SetKeyName(58, "country_16x16.png");
            this.imageCollection1.Images.SetKeyName(59, "jobs.png");
            this.imageCollection1.Images.SetKeyName(60, "client_contract_16.png");
            this.imageCollection1.Images.SetKeyName(61, "site_contract_16.png");
            this.imageCollection1.InsertGalleryImage("calendar_16x16.png", "images/scheduling/calendar_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/scheduling/calendar_16x16.png"), 62);
            this.imageCollection1.Images.SetKeyName(62, "calendar_16x16.png");
            this.imageCollection1.Images.SetKeyName(63, "dialog-warning.png");
            this.imageCollection1.Images.SetKeyName(64, "client_po_16.png");
            this.imageCollection1.Images.SetKeyName(65, "AudiTrail_16.png");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.refresh_16x16, "refresh_16x16", typeof(global::WoodPlan5.Properties.Resources), 66);
            this.imageCollection1.Images.SetKeyName(66, "refresh_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.TreeView_16x16, "TreeView_16x16", typeof(global::WoodPlan5.Properties.Resources), 67);
            this.imageCollection1.Images.SetKeyName(67, "TreeView_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.linked_documents_16_16, "linked_documents_16_16", typeof(global::WoodPlan5.Properties.Resources), 68);
            this.imageCollection1.Images.SetKeyName(68, "linked_documents_16_16");
            this.imageCollection1.Images.SetKeyName(69, "team_send_16x16.png");
            this.imageCollection1.Images.SetKeyName(70, "team_invoice_16x16.png");
            this.imageCollection1.InsertGalleryImage("grouprows_16x16.png", "images/spreadsheet/grouprows_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/spreadsheet/grouprows_16x16.png"), 71);
            this.imageCollection1.Images.SetKeyName(71, "grouprows_16x16.png");
            // 
            // documentManager1
            // 
            this.documentManager1.BarAndDockingController = this.barAndDockingController1;
            this.documentManager1.MdiParent = this;
            this.documentManager1.MenuManager = this.ribbon;
            this.documentManager1.View = this.tabbedView1;
            this.documentManager1.ViewCollection.AddRange(new DevExpress.XtraBars.Docking2010.Views.BaseView[] {
            this.tabbedView1});
            // 
            // tabbedView1
            // 
            this.tabbedView1.DocumentAdded += new DevExpress.XtraBars.Docking2010.Views.DocumentEventHandler(this.tabbedView1_DocumentAdded);
            this.tabbedView1.DocumentActivated += new DevExpress.XtraBars.Docking2010.Views.DocumentEventHandler(this.tabbedView1_DocumentActivated);
            this.tabbedView1.DocumentClosed += new DevExpress.XtraBars.Docking2010.Views.DocumentEventHandler(this.tabbedView1_DocumentClosed);
            // 
            // imageCollection3
            // 
            this.imageCollection3.ImageSize = new System.Drawing.Size(32, 32);
            this.imageCollection3.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection3.ImageStream")));
            this.imageCollection3.Images.SetKeyName(0, "Tree_32.png");
            this.imageCollection3.InsertGalleryImage("home_32x32.png", "images/navigation/home_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/navigation/home_32x32.png"), 1);
            this.imageCollection3.Images.SetKeyName(1, "home_32x32.png");
            this.imageCollection3.Images.SetKeyName(2, "weather_snow_32.png");
            this.imageCollection3.InsertGalleryImage("ide_32x32.png", "images/programming/ide_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/programming/ide_32x32.png"), 3);
            this.imageCollection3.Images.SetKeyName(3, "ide_32x32.png");
            this.imageCollection3.Images.SetKeyName(4, "User_Settings_32.png");
            this.imageCollection3.Images.SetKeyName(5, "UserFavourites_32x32.png");
            this.imageCollection3.Images.SetKeyName(6, "quote_32.png");
            this.imageCollection3.Images.SetKeyName(7, "CRM_32.png");
            this.imageCollection3.Images.SetKeyName(8, "weather-clear.png");
            this.imageCollection3.Images.SetKeyName(9, "routing_32.png");
            this.imageCollection3.Images.SetKeyName(10, "Employee_32x32.png");
            this.imageCollection3.Images.SetKeyName(11, "bucket_truck.png");
            this.imageCollection3.InsertGalleryImage("technology_32x32.png", "images/programming/technology_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/programming/technology_32x32.png"), 12);
            this.imageCollection3.Images.SetKeyName(12, "technology_32x32.png");
            this.imageCollection3.InsertGalleryImage("bodepartment_32x32.png", "images/business%20objects/bodepartment_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/business%20objects/bodepartment_32x32.png"), 13);
            this.imageCollection3.Images.SetKeyName(13, "bodepartment_32x32.png");
            // 
            // sharedImageCollection16x16
            // 
            // 
            // 
            // 
            this.sharedImageCollection16x16.ImageSource.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("sharedImageCollection16x16.ImageSource.ImageStream")));
            this.sharedImageCollection16x16.ImageSource.Images.SetKeyName(0, "Add_16x16.png");
            this.sharedImageCollection16x16.ImageSource.Images.SetKeyName(1, "Edit_16x16.png");
            this.sharedImageCollection16x16.ImageSource.Images.SetKeyName(2, "Delete_16x16.png");
            this.sharedImageCollection16x16.ImageSource.Images.SetKeyName(3, "Preview_16x16.png");
            this.sharedImageCollection16x16.ImageSource.Images.SetKeyName(4, "Refresh_16x16.png");
            this.sharedImageCollection16x16.ImageSource.Images.SetKeyName(5, "BlockAdd_16x16.png");
            this.sharedImageCollection16x16.ImageSource.Images.SetKeyName(6, "BlockEdit_16x16.png");
            this.sharedImageCollection16x16.ImageSource.Images.SetKeyName(7, "Info_16x16.png");
            this.sharedImageCollection16x16.ImageSource.Images.SetKeyName(8, "attention_16.png");
            this.sharedImageCollection16x16.ImageSource.Images.SetKeyName(9, "apply_16x16.png");
            this.sharedImageCollection16x16.ImageSource.Images.SetKeyName(10, "cancel_16x16.png");
            this.sharedImageCollection16x16.ImageSource.Images.SetKeyName(11, "clearfilter_16x16.png");
            this.sharedImageCollection16x16.ImageSource.Images.SetKeyName(12, "refresh_32x32.png");
            this.sharedImageCollection16x16.ImageSource.Images.SetKeyName(13, "reset2_32x32.png");
            this.sharedImageCollection16x16.ImageSource.Images.SetKeyName(14, "refresh2_32x32.png");
            this.sharedImageCollection16x16.ImageSource.Images.SetKeyName(15, "info_32x32.png");
            this.sharedImageCollection16x16.ParentControl = this;
            // 
            // frmMain2
            // 
            this.AllowFormGlass = DevExpress.Utils.DefaultBoolean.False;
            this.Appearance.Options.UseFont = true;
            this.ClientSize = new System.Drawing.Size(1105, 622);
            this.Controls.Add(this.lcDataTransferMode);
            this.Controls.Add(this.lcNonLiveDB);
            this.Controls.Add(this.splitterControl1);
            this.Controls.Add(this.clientPanel);
            this.Controls.Add(this.panelContainer2);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbon);
            this.IsMdiContainer = true;
            this.Name = "frmMain2";
            this.Ribbon = this.ribbon;
            this.StatusBar = this.ribbonStatusBar;
            this.Text = "Information Centre";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain2_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmMain2_FormClosed);
            this.Load += new System.EventHandler(this.frmMain2_Load);
            this.MdiChildActivate += new System.EventHandler(this.frmMain2_MdiChildActivate);
            this.Resize += new System.EventHandler(this.frmMain2_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicationMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelectMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpiProgressBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CommentBankMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ToDoListMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gddFont)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gddFontColour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EditMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LivePeriodLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00022GetDatePeriodsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ViewedPeriodLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gddChartPalette)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gddStaff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientPanel)).EndInit();
            this.clientPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupControlContainer1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataEditMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ViewMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ToolsMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrintingMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SystemPeriodsMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dpActiveForm.ResumeLayout(false);
            this.dockPanel3_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nbcActiveForm)).EndInit();
            this.panelContainer2.ResumeLayout(false);
            this.panelContainer1.ResumeLayout(false);
            this.dockPanel1.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nbcSwitchboard)).EndInit();
            this.dockPanel2.ResumeLayout(false);
            this.dockPanel2_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nbcReports)).EndInit();
            this.panelContainer3.ResumeLayout(false);
            this.dpPersonImage.ResumeLayout(false);
            this.controlContainer1.ResumeLayout(false);
            this.xtraScrollableControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pePersonsPhoto.Properties)).EndInit();
            this.dockPanelSiteInspection.ResumeLayout(false);
            this.controlContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WindowMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tmrNotification)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sp00020GetUserApplicationSettingsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00019GetUserDetailsForStartUpBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SwitchboardMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportsMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00001PopulateSwitchboardBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FilterMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmCurrentUserMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmChildForms)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00068_Get_Magnifier_SettingsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00071_get_available_loginsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.styleController1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollectionChartTypes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sharedImageCollection16x16.ImageSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sharedImageCollection16x16)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.PanelControl clientPanel;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.BarButtonItem iCut;
        private DevExpress.XtraBars.BarButtonItem iCopy;
        private DevExpress.XtraBars.BarButtonItem iPaste;
        private DevExpress.XtraBars.BarButtonItem iSpellCheck;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.BarButtonItem iUndo;
        private DevExpress.XtraBars.BarButtonItem iRedo;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private DevExpress.XtraBars.BarButtonItem iPreview;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup5;
        private DevExpress.XtraBars.BarButtonItem iPrint;
        private DevExpress.XtraBars.BarButtonItem iDelete;
        private DevExpress.XtraBars.BarButtonItem iSave;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup6;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage3;
        private DevExpress.XtraBars.BarButtonItem iAdd;
        private DevExpress.XtraBars.BarButtonItem iBlockAdd;
        private DevExpress.XtraBars.BarButtonItem iEdit;
        private DevExpress.XtraBars.BarButtonItem iBlockEdit;
        private DevExpress.XtraBars.BarSubItem sbiFile;
        private DevExpress.XtraBars.BarButtonItem iPrintSetup;
        private DevExpress.XtraBars.PopupMenu DataEditMenu1;
        private DevExpress.XtraBars.BarButtonItem iClear;
        private DevExpress.XtraBars.BarButtonItem sbiSelectAll;
        private DevExpress.XtraBars.PopupMenu ViewMenu1;
        private DevExpress.XtraBars.BarButtonItem iSaveLayout;
        private DevExpress.XtraBars.BarButtonItem iSaveLayoutAs;
        private DevExpress.XtraBars.PopupMenu ToolsMenu1;
        private DevExpress.XtraBars.BarSubItem sbiOptions;
        private DevExpress.XtraBars.BarButtonItem iMyOptions;
        private DevExpress.XtraBars.BarButtonItem iSystemOptions;
        private DevExpress.XtraBars.BarButtonItem iExport;
        private DevExpress.XtraBars.Ribbon.ApplicationMenu applicationMenu1;
        private DevExpress.XtraBars.BarButtonItem iExit;
        private DevExpress.XtraBars.BarStaticItem MicroHelpStaticItem;
        private DevExpress.XtraBars.BarEditItem iProgressBar;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar rpiProgressBar;
        private DevExpress.XtraBars.BarStaticItem iCurrentUser;
        private DevExpress.XtraBars.BarStaticItem iDatabase;
        private DevExpress.XtraBars.BarButtonItem iGrammerCheck;
        private DevExpress.XtraBars.BarButtonItem sbiCommentBank;
        private DevExpress.XtraBars.BarButtonItem iScratchPad;
        private DevExpress.XtraBars.BarButtonItem iClipboard;
        private DevExpress.XtraBars.BarButtonItem sbiToDoList;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup7;
        private DevExpress.XtraBars.BarButtonItem iBold;
        private DevExpress.XtraBars.BarButtonItem iItalic;
        private DevExpress.XtraBars.BarButtonItem iUnderline;
        private DevExpress.XtraBars.BarButtonItem iAlignLeft;
        private DevExpress.XtraBars.BarButtonItem iAlignCentre;
        private DevExpress.XtraBars.BarButtonItem iAlignRight;
        private DevExpress.XtraBars.BarButtonItem iBullet;
        private DevExpress.XtraBars.BarButtonItem iFont;
        private DevExpress.XtraBars.BarButtonItem iFontColour;
        private DevExpress.XtraBars.PopupMenu DataMenu1;
        private DevExpress.XtraBars.Ribbon.GalleryDropDown gddFont;
        private DevExpress.XtraBars.Ribbon.GalleryDropDown gddFontColour;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup8;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraBars.BarEditItem beiFontSize;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit2;
        private DevExpress.XtraBars.BarButtonItem iFilterManager;
        private DevExpress.XtraBars.BarButtonItem iFilterCreate;
        private DevExpress.XtraBars.BarButtonItem iDefaultView;
        private DevExpress.XtraBars.BarSubItem sbiFind;
        private DevExpress.XtraBars.BarButtonItem iFind;
        private DevExpress.XtraBars.BarButtonItem iReplace;
        private DevExpress.XtraBars.RibbonGalleryBarItem rgbiSkins;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private DevExpress.XtraBars.PopupControlContainer popupControlContainer1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage5;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup9;
        private DevExpress.XtraBars.Ribbon.GalleryDropDown gddStaff;
        private DevExpress.XtraBars.BarButtonItem sbiAdd;
        private DevExpress.XtraBars.PopupMenu AddMenu1;
        private DevExpress.XtraBars.BarButtonItem sbiEdit;
        private DevExpress.XtraBars.PopupMenu EditMenu1;
        private DevExpress.XtraBars.BarButtonItem iToDoList;
        private DevExpress.XtraBars.BarButtonItem iLinkRecordToDoList;
        private DevExpress.XtraBars.PopupMenu ToDoListMenu1;
        private DevExpress.XtraBars.BarButtonItem iCommentBank;
        private DevExpress.XtraBars.BarButtonItem iCommentBankCodes;
        private DevExpress.XtraBars.PopupMenu CommentBankMenu1;
        private DevExpress.XtraBars.BarButtonItem iImage;
        private DevExpress.XtraBars.PopupMenu PrintingMenu1;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit1;
        private DevExpress.XtraBars.BarEditItem beiLivePeriod;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LivePeriodLookUpEdit1;
        private DevExpress.XtraBars.BarEditItem beiViewedPeriod;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit ViewedPeriodLookUpEdit1;
        private DevExpress.XtraBars.BarButtonItem iEditSystemPeriods;
        private DevExpress.XtraBars.PopupMenu SystemPeriodsMenu1;
        private DevExpress.XtraBars.BarAndDockingController barAndDockingController1;
        private DevExpress.XtraBars.Docking.DockPanel panelContainer1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel1;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel2;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel2_Container;
        /// <summary>
        /// 
        /// </summary>
        public  DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraEditors.SplitterControl splitterControl1;
        private WoodPlanDataSet woodPlanDataSet;
        private System.Windows.Forms.BindingSource sp00022GetDatePeriodsBindingSource;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00022GetDatePeriodsTableAdapter sp00022GetDatePeriodsTableAdapter;
        private DevExpress.XtraBars.BarButtonItem iCascade;
        private DevExpress.XtraBars.BarButtonItem iTileHorizontal;
        private DevExpress.XtraBars.BarButtonItem iTileVertical;
        private DevExpress.XtraBars.BarMdiChildrenListItem barMdiChildrenListItem1;
        private DevExpress.XtraBars.PopupMenu WindowMenu1;
        private System.Timers.Timer tmrNotification;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem openGiroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.BindingSource sp00020GetUserApplicationSettingsBindingSource;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00020GetUserApplicationSettingsTableAdapter sp00020GetUserApplicationSettingsTableAdapter;
        private System.Windows.Forms.BindingSource sp00019GetUserDetailsForStartUpBindingSource;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00019GetUserDetailsForStartUpTableAdapter sp00019GetUserDetailsForStartUpTableAdapter;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraNavBar.NavBarControl nbcSwitchboard;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraNavBar.NavBarControl nbcReports;
        private DevExpress.XtraBars.BarButtonItem iSelectGroup;
        private DevExpress.XtraBars.PopupMenu SelectMenu1;
        private DevExpress.XtraBars.BarButtonItem iSelectAll;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraBars.Docking.DockPanel panelContainer2;
        private DevExpress.XtraBars.Docking.DockPanel dpActiveForm;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel3_Container;
        private DevExpress.XtraNavBar.NavBarControl nbcActiveForm;
        private DevExpress.XtraBars.PopupMenu SwitchboardMenu;
        private DevExpress.XtraBars.BarButtonItem bbiSelect;
        private DevExpress.XtraBars.BarButtonItem bbiDelete;
        private DevExpress.XtraBars.PopupMenu ReportsMenu;
        private DevExpress.XtraBars.BarButtonItem bbiReportSelect;
        private DevExpress.XtraBars.BarButtonItem bbiReportDelete;
        private System.Windows.Forms.BindingSource sp00001PopulateSwitchboardBindingSource;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00001PopulateSwitchboardTableAdapter sp00001PopulateSwitchboardTableAdapter;
        private DevExpress.XtraBars.BarButtonItem iSaveFilter;
        private DevExpress.XtraBars.BarButtonItem bbiDefaultView;
        private DevExpress.XtraBars.BarSubItem bsiMyLayouts;
        private DevExpress.XtraBars.BarSubItem bsiAvailableLayouts;
        private RibbonPageGroup ribbonPageGroup11;
        private DevExpress.XtraBars.PopupMenu FilterMenu;
        private DevExpress.XtraBars.PopupMenu LayoutMenu;
        private DevExpress.XtraBars.BarButtonItem iLayoutManager;
        private DevExpress.XtraBars.BarSubItem iMyFilters;
        private DevExpress.XtraBars.BarSubItem iAvailableFilters;
        private DevExpress.XtraBars.BarButtonItem iSaveFilterAs;
        private DevExpress.XtraBars.BarButtonItem bbiCloseAll;
        private DevExpress.XtraBars.BarButtonItem bbiShowLoggedInUsers;
        private DevExpress.XtraBars.BarButtonItem bbiChangeCurrentUserSuper;
        private DevExpress.XtraBars.PopupMenu pmCurrentUserMenu;
        private DevExpress.XtraBars.BarButtonItem bbiChangeCurrentUser;
        private DevExpress.XtraBars.BarButtonItem bbiEditComments;
        private DevExpress.XtraBars.BarButtonItem bbiCalculator;
        private DevExpress.XtraBars.BarButtonItem bbiScriptBuilder;
        private DevExpress.XtraBars.BarButtonItem bbiCloseThisForm;
        private DevExpress.XtraBars.BarButtonItem bbiCloseAllForms;
        private DevExpress.XtraBars.BarButtonItem bbiCloseAllButThisForm;
        private DevExpress.XtraBars.PopupMenu pmChildForms;
        private DevExpress.XtraBars.Docking.DockPanel dpPersonImage;
        private DevExpress.XtraBars.Docking.ControlContainer controlContainer1;
        private DevExpress.XtraEditors.PictureEdit pePersonsPhoto;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit1;
        private DevExpress.XtraBars.BarEditItem beTraining;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit2;
        private DevExpress.XtraBars.BarButtonItem bbiMagnifier;
        private System.Windows.Forms.BindingSource sp00068_Get_Magnifier_SettingsBindingSource;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00068_Get_Magnifier_SettingsTableAdapter sp00068_Get_Magnifier_SettingsTableAdapter;
        private System.Windows.Forms.BindingSource sp00071_get_available_loginsBindingSource;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00071_get_available_loginsTableAdapter sp00071_get_available_loginsTableAdapter;
        private DevExpress.XtraEditors.StyleController styleController1;
        private DevExpress.XtraBars.BarButtonItem iFastFind;
        private DevExpress.XtraBars.BarButtonItem iLoadLayout;
        private DevExpress.XtraBars.BarButtonItem iLoadFilter;
        private DevExpress.XtraEditors.XtraScrollableControl xtraScrollableControl1;
        private DevExpress.XtraEditors.LabelControl lcNonLiveDB;
        private DevExpress.XtraEditors.LabelControl lcDataTransferMode;
        private DevExpress.XtraBars.BarButtonItem iAbout;
        private RibbonPage ribbonPage4;
        private RibbonPageGroup ribbonPageGroup12;
        public DevExpress.XtraBars.BarButtonItem bbiChartPalette;
        public GalleryDropDown gddChartPalette;
        public DevExpress.XtraBars.RibbonGalleryBarItem rgbiChartAppearance;
        public DevExpress.XtraBars.RibbonGalleryBarItem rgbiChartTypes;
        private DevExpress.Utils.ImageCollection imageCollectionChartTypes;
        private DevExpress.XtraBars.BarButtonItem bbiChartWizard;
        private DevExpress.XtraBars.BarButtonItem bbiRotateAxis;
        private RibbonPageGroup ribbonPageGroup13;
        private RibbonPageGroup ribbonPageGroup14;
        private DevExpress.XtraBars.BarButtonItem bbiLayoutFlip;
        private RibbonPageGroup ribbonPageGroup15;
        private DevExpress.XtraBars.BarButtonItem bbiLayoutRotate;
        public DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelSiteInspection;
        private DevExpress.XtraBars.Docking.ControlContainer controlContainer2;
        private DevExpress.XtraBars.Docking.DockPanel panelContainer3;
        private DevExpress.XtraEditors.SimpleButton btnFinish;
        private DevExpress.XtraEditors.SimpleButton btnStart;
        private DevExpress.XtraEditors.LabelControl labelControlSelectedInspection;
        private DevExpress.XtraEditors.SimpleButton btnPause;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.SimpleButton btnResume;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraBars.Docking2010.Views.Tabbed.TabbedView tabbedView1;
        private DevExpress.XtraBars.BarButtonItem bbiVisitWebSite;
        public DevExpress.Utils.ImageCollection imageCollection3;
        public DevExpress.XtraBars.Docking2010.DocumentManager documentManager1;
        private DevExpress.XtraBars.BarButtonItem bbiColourMixer;
        private DevExpress.Utils.SharedImageCollection sharedImageCollection16x16;
    }
}

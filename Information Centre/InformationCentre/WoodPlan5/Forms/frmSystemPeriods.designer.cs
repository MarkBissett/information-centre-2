namespace WoodPlan5
{
    partial class frmSystemPeriods
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSystemPeriods));
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp00097dateperiodslistBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colintPeriodID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrDateDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coldtFromDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.coldtToDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colintSystemID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colForeColour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBackColour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSystem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rpiSystemPeriodPictureEdit = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.colViewed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rpiViewedPeriodPictureEdit = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.UnverifiedPictureEdit = new DevExpress.XtraEditors.PictureEdit();
            this.imageList3 = new System.Windows.Forms.ImageList(this.components);
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.bbiSetViewedPeriod = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSetLivePeriod = new DevExpress.XtraBars.BarButtonItem();
            this.sp00097_date_periods_listTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00097_date_periods_listTableAdapter();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00097dateperiodslistBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpiSystemPeriodPictureEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpiViewedPeriodPictureEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnverifiedPictureEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSetViewedPeriod, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSetLivePeriod)});
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(537, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 246);
            this.barDockControlBottom.Size = new System.Drawing.Size(537, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 246);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(537, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 246);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiSetViewedPeriod,
            this.bbiSetLivePeriod});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp00097dateperiodslistBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rpiSystemPeriodPictureEdit,
            this.rpiViewedPeriodPictureEdit,
            this.repositoryItemDateEdit1,
            this.repositoryItemDateEdit2,
            this.repositoryItemTextEdit1});
            this.gridControl1.Size = new System.Drawing.Size(527, 189);
            this.gridControl1.TabIndex = 6;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp00097dateperiodslistBindingSource
            // 
            this.sp00097dateperiodslistBindingSource.DataMember = "sp00097_date_periods_list";
            this.sp00097dateperiodslistBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colintPeriodID,
            this.colstrDateDesc,
            this.coldtFromDate,
            this.coldtToDate,
            this.colintSystemID,
            this.colForeColour,
            this.colBackColour,
            this.colstrDesc,
            this.colSystem,
            this.colViewed});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            this.gridView1.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.gridView1_CustomUnboundColumnData);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridView1_ValidatingEditor);
            // 
            // colintPeriodID
            // 
            this.colintPeriodID.Caption = "Period ID";
            this.colintPeriodID.FieldName = "intPeriodID";
            this.colintPeriodID.Name = "colintPeriodID";
            this.colintPeriodID.OptionsColumn.AllowEdit = false;
            this.colintPeriodID.OptionsColumn.ReadOnly = true;
            // 
            // colstrDateDesc
            // 
            this.colstrDateDesc.Caption = "Date Description";
            this.colstrDateDesc.FieldName = "strDateDesc";
            this.colstrDateDesc.Name = "colstrDateDesc";
            // 
            // coldtFromDate
            // 
            this.coldtFromDate.Caption = "From Date";
            this.coldtFromDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.coldtFromDate.FieldName = "dtFromDate";
            this.coldtFromDate.Name = "coldtFromDate";
            this.coldtFromDate.Visible = true;
            this.coldtFromDate.VisibleIndex = 2;
            this.coldtFromDate.Width = 76;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit1.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            this.repositoryItemDateEdit1.NullValuePrompt = "Enter Start Date";
            // 
            // coldtToDate
            // 
            this.coldtToDate.Caption = "To Date";
            this.coldtToDate.ColumnEdit = this.repositoryItemDateEdit2;
            this.coldtToDate.FieldName = "dtToDate";
            this.coldtToDate.Name = "coldtToDate";
            this.coldtToDate.Visible = true;
            this.coldtToDate.VisibleIndex = 3;
            // 
            // repositoryItemDateEdit2
            // 
            this.repositoryItemDateEdit2.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemDateEdit2.AutoHeight = false;
            this.repositoryItemDateEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit2.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.repositoryItemDateEdit2.Name = "repositoryItemDateEdit2";
            this.repositoryItemDateEdit2.NullValuePrompt = "Enter End Date";
            // 
            // colintSystemID
            // 
            this.colintSystemID.Caption = "System ID";
            this.colintSystemID.FieldName = "intSystemID";
            this.colintSystemID.Name = "colintSystemID";
            this.colintSystemID.OptionsColumn.AllowEdit = false;
            this.colintSystemID.OptionsColumn.ReadOnly = true;
            this.colintSystemID.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colForeColour
            // 
            this.colForeColour.Caption = "ForeColour";
            this.colForeColour.FieldName = "ForeColour";
            this.colForeColour.Name = "colForeColour";
            this.colForeColour.OptionsColumn.AllowEdit = false;
            this.colForeColour.OptionsColumn.ReadOnly = true;
            this.colForeColour.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colBackColour
            // 
            this.colBackColour.Caption = "BackColour";
            this.colBackColour.FieldName = "BackColour";
            this.colBackColour.Name = "colBackColour";
            this.colBackColour.OptionsColumn.AllowEdit = false;
            this.colBackColour.OptionsColumn.ReadOnly = true;
            this.colBackColour.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colstrDesc
            // 
            this.colstrDesc.Caption = "Description";
            this.colstrDesc.ColumnEdit = this.repositoryItemTextEdit1;
            this.colstrDesc.FieldName = "strDesc";
            this.colstrDesc.Name = "colstrDesc";
            this.colstrDesc.Visible = true;
            this.colstrDesc.VisibleIndex = 4;
            this.colstrDesc.Width = 275;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.MaxLength = 50;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            this.repositoryItemTextEdit1.NullValuePrompt = "Enter Description";
            // 
            // colSystem
            // 
            this.colSystem.Caption = "Live";
            this.colSystem.ColumnEdit = this.rpiSystemPeriodPictureEdit;
            this.colSystem.CustomizationCaption = "Live Period Highlighter";
            this.colSystem.FieldName = "SystemPeriod";
            this.colSystem.Name = "colSystem";
            this.colSystem.OptionsColumn.AllowEdit = false;
            this.colSystem.OptionsColumn.AllowFocus = false;
            this.colSystem.OptionsColumn.ReadOnly = true;
            this.colSystem.OptionsColumn.TabStop = false;
            this.colSystem.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.colSystem.Visible = true;
            this.colSystem.VisibleIndex = 0;
            this.colSystem.Width = 31;
            // 
            // rpiSystemPeriodPictureEdit
            // 
            this.rpiSystemPeriodPictureEdit.Name = "rpiSystemPeriodPictureEdit";
            // 
            // colViewed
            // 
            this.colViewed.Caption = "Viewed";
            this.colViewed.ColumnEdit = this.rpiViewedPeriodPictureEdit;
            this.colViewed.CustomizationCaption = "Viewed Period Highlighter";
            this.colViewed.FieldName = "ViewedPeriod";
            this.colViewed.Name = "colViewed";
            this.colViewed.OptionsColumn.AllowEdit = false;
            this.colViewed.OptionsColumn.AllowFocus = false;
            this.colViewed.OptionsColumn.ReadOnly = true;
            this.colViewed.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.colViewed.Visible = true;
            this.colViewed.VisibleIndex = 1;
            this.colViewed.Width = 53;
            // 
            // rpiViewedPeriodPictureEdit
            // 
            this.rpiViewedPeriodPictureEdit.Name = "rpiViewedPeriodPictureEdit";
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(457, 217);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(376, 217);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 7;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.AllowHtmlString = true;
            this.labelControl2.Location = new System.Drawing.Point(22, 5);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(52, 13);
            this.labelControl2.TabIndex = 8;
            this.labelControl2.Text = "Live Period";
            // 
            // labelControl1
            // 
            this.labelControl1.AllowHtmlString = true;
            this.labelControl1.Location = new System.Drawing.Point(103, 5);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(67, 13);
            this.labelControl1.TabIndex = 7;
            this.labelControl1.Text = "Viewed Period";
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.EditValue = ((object)(resources.GetObject("pictureEdit1.EditValue")));
            this.pictureEdit1.Location = new System.Drawing.Point(5, 4);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.ReadOnly = true;
            this.pictureEdit1.Properties.ShowMenu = false;
            this.pictureEdit1.Size = new System.Drawing.Size(16, 16);
            this.pictureEdit1.TabIndex = 2;
            // 
            // UnverifiedPictureEdit
            // 
            this.UnverifiedPictureEdit.EditValue = ((object)(resources.GetObject("UnverifiedPictureEdit.EditValue")));
            this.UnverifiedPictureEdit.Location = new System.Drawing.Point(86, 4);
            this.UnverifiedPictureEdit.Name = "UnverifiedPictureEdit";
            this.UnverifiedPictureEdit.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.UnverifiedPictureEdit.Properties.Appearance.Options.UseBackColor = true;
            this.UnverifiedPictureEdit.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.UnverifiedPictureEdit.Properties.ReadOnly = true;
            this.UnverifiedPictureEdit.Properties.ShowMenu = false;
            this.UnverifiedPictureEdit.Size = new System.Drawing.Size(16, 16);
            this.UnverifiedPictureEdit.TabIndex = 1;
            // 
            // imageList3
            // 
            this.imageList3.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList3.ImageStream")));
            this.imageList3.TransparentColor = System.Drawing.Color.Magenta;
            this.imageList3.Images.SetKeyName(0, "system_live");
            this.imageList3.Images.SetKeyName(1, "viewed");
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "-";
            this.barStaticItem1.Id = 23;
            this.barStaticItem1.Name = "barStaticItem1";
            // 
            // bbiSetViewedPeriod
            // 
            this.bbiSetViewedPeriod.Caption = "Set as Viewed Period";
            this.bbiSetViewedPeriod.Id = 24;
            this.bbiSetViewedPeriod.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSetViewedPeriod.ImageOptions.Image")));
            this.bbiSetViewedPeriod.ItemAppearance.Normal.ForeColor = System.Drawing.Color.Green;
            this.bbiSetViewedPeriod.ItemAppearance.Normal.Options.UseForeColor = true;
            this.bbiSetViewedPeriod.Name = "bbiSetViewedPeriod";
            this.bbiSetViewedPeriod.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSetViewedPeriod_ItemClick);
            // 
            // bbiSetLivePeriod
            // 
            this.bbiSetLivePeriod.Caption = "Set as Live Period";
            this.bbiSetLivePeriod.Id = 25;
            this.bbiSetLivePeriod.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSetLivePeriod.ImageOptions.Image")));
            this.bbiSetLivePeriod.ItemAppearance.Normal.ForeColor = System.Drawing.Color.Red;
            this.bbiSetLivePeriod.ItemAppearance.Normal.Options.UseForeColor = true;
            this.bbiSetLivePeriod.Name = "bbiSetLivePeriod";
            this.bbiSetLivePeriod.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSetLivePeriod_ItemClick);
            // 
            // sp00097_date_periods_listTableAdapter
            // 
            this.sp00097_date_periods_listTableAdapter.ClearBeforeFill = true;
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(5, 23);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(527, 189);
            this.gridSplitContainer1.TabIndex = 9;
            // 
            // frmSystemPeriods
            // 
            this.ClientSize = new System.Drawing.Size(537, 246);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.UnverifiedPictureEdit);
            this.Controls.Add(this.pictureEdit1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.gridSplitContainer1);
            this.Controls.Add(this.btnOK);
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSystemPeriods";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "System Periods";
            this.Load += new System.EventHandler(this.frmSystemPeriods_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.gridSplitContainer1, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.pictureEdit1, 0);
            this.Controls.SetChildIndex(this.UnverifiedPictureEdit, 0);
            this.Controls.SetChildIndex(this.labelControl2, 0);
            this.Controls.SetChildIndex(this.labelControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00097dateperiodslistBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpiSystemPeriodPictureEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpiViewedPeriodPictureEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnverifiedPictureEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private WoodPlanDataSet woodPlanDataSet;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colintPeriodID;
        private DevExpress.XtraGrid.Columns.GridColumn colstrDateDesc;
        private DevExpress.XtraGrid.Columns.GridColumn coldtFromDate;
        private DevExpress.XtraGrid.Columns.GridColumn coldtToDate;
        private DevExpress.XtraGrid.Columns.GridColumn colintSystemID;
        private DevExpress.XtraGrid.Columns.GridColumn colForeColour;
        private DevExpress.XtraGrid.Columns.GridColumn colBackColour;
        private DevExpress.XtraGrid.Columns.GridColumn colstrDesc;
        private DevExpress.XtraGrid.Columns.GridColumn colSystem;
        private DevExpress.XtraGrid.Columns.GridColumn colViewed;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit rpiSystemPeriodPictureEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit rpiViewedPeriodPictureEdit;
        private System.Windows.Forms.ImageList imageList3;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.PictureEdit UnverifiedPictureEdit;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarButtonItem bbiSetViewedPeriod;
        private DevExpress.XtraBars.BarButtonItem bbiSetLivePeriod;
        private System.Windows.Forms.BindingSource sp00097dateperiodslistBindingSource;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00097_date_periods_listTableAdapter sp00097_date_periods_listTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
    }
}

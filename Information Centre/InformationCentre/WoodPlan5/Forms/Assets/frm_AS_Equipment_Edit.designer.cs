﻿namespace WoodPlan5
{
    partial class frm_AS_Equipment_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem9 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem9 = new DevExpress.Utils.ToolTipItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AS_Equipment_Edit));
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling21 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling13 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling14 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling15 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject25 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject26 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject27 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject28 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject29 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject30 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject31 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject32 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling16 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling17 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling18 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling19 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SuperToolTip superToolTip10 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem10 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem10 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip11 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem11 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem11 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip12 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem12 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem12 = new DevExpress.Utils.ToolTipItem();
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.tableAdapterManager = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager();
            this.barStatus = new DevExpress.XtraBars.Bar();
            this.bsiFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.dxErrorProviderEquipment = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.lueEquipment_Category = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11002EquipmentItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spAS11011EquipmentCategoryListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.equipmentDataLayoutControl = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.txtPlantRegistrationPlate = new DevExpress.XtraEditors.TextEdit();
            this.spAS11008PlantItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lueExchequerCategoryID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLExchequerCategoryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dePUWERDate = new DevExpress.XtraEditors.DateEdit();
            this.deLOLERDate = new DevExpress.XtraEditors.DateEdit();
            this.txtServiceTag = new DevExpress.XtraEditors.TextEdit();
            this.spAS11032HardwareItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.txtSerialNumber = new DevExpress.XtraEditors.TextEdit();
            this.lueOfficeCategoryID = new DevExpress.XtraEditors.LookUpEdit();
            this.sp_AS_11029_Office_ItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spAS11000PLOfficeCategoryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.txtIMEI = new DevExpress.XtraEditors.TextEdit();
            this.spAS11026GadgetItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.equipmentChildTabControl = new DevExpress.XtraTab.XtraTabControl();
            this.roadTaxTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.roadTaxGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11096RoadTaxItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.roadTaxGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colRoadTaxID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTaxExpiryDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.moneyTextEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colTaxPeriodID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTaxPeriod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.keeperTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.keeperGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11044KeeperAllocationItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.keeperGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colKeeperAllocationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeper = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllocationStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllocationStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllocationDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllocationEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNotes1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lueKeeperNotes = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.transactionTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.transactionGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11041TransactionItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.transactionsGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTransactionID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransactionTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransactionType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransactionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransactionOrderNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPurchaseInvoice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompletedOnExchequer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNetTransactionPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.moneyTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ceOnExchequer = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.purposeTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.purposeGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11084PurposeItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.purposeGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colEquipmentPurposeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPurpose = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPurposeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit16 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemMemoExEdit17 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.fuelCardTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.fuelCardGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11089FuelCardItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.fuelCardGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colFuelCardID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCardNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegistrationMark = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeper1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperAllocationID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpiryDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSupplierReference2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSupplierID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCardStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCardStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNotes5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit18 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemMemoExEdit19 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.trackerTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.trackerGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11023TrackerListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.trackerGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTrackerInformationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegistration = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTrackerVID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTrackerReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMake1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModel1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOdometerMetres = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOdometerMiles = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMPG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLatestStartJourney = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLatestEndJourney = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeedKPH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeedMPH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAverageSpeed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlaceName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRoadName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostSect = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGeofenceName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistrict = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastUpdate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDriverName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDriverMobile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDriverEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemMemoExEdit10 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.billingTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.billingGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11050DepreciationItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.billingGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDepreciationID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNarrative1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBalanceSheetCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProfitLossCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingCentreCodeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartmentCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCentreCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepreciationAmount1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.moneyTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colPreviousValue1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentValue1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPeriodNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPeriodStartDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPeriodEndDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllowEdit1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.depreciationTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.depreciationGridControl = new DevExpress.XtraGrid.GridControl();
            this.depreciationGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDepreciationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNarrative = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBalanceSheetCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProfitLossCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingCentreCodeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartmentCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCentreCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepreciationAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.moneyTextEditDep = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colPreviousValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPeriodNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPeriodStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPeriodEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllowEdit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.coverTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.coverGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11075CoverItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.coverGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCoverID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colEquipmentReference16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCoverType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCoverTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSupplierID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSupplierReference1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPolicy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAnnualCoverCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.moneyTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRenewalDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNotes3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExcess = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.serviceDatalTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.serviceDataGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11054ServiceDataItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.serviceDataGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colServiceDataID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServiceTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServiceType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistanceUnitsID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistanceUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistanceFrequency = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeUnitsID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeFrequency = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWarrantyPeriod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWarrantyEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWarrantyEndDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServiceDataNotes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServiceStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServiceDueDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServiceDueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAlertWithinXDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAlertWithinXTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIntervalScheduleNotes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsCurrentInterval = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit11 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemMemoExEdit12 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.workDetailTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.workDetailGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11078WorkDetailItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.workDetailGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colWorkDetailID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateRaised = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSupplierID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSupplierReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransactionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentMileage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkCompletionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServiceIntervalID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServiceType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNotes2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit13 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemMemoExEdit14 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.incidentTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.incidentGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11081IncidentItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.incidentGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colIncidentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateHappened = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRepairDueID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRepairDue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSeverityID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSeverity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWitness = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperAtFaultID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperAtFault = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLegalActionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLegalAction = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFollowUpDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.moneyTextEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colNotes4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.incidentNotesMemoEdit = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.colMode3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.speedingTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.speedingGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11105SpeedingItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.speedingGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSpeedingID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeedMPH1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTravelTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRoadTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRoadType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlaceName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistrict1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReported = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ceReported = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colMode17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtShortDescription = new DevExpress.XtraEditors.TextEdit();
            this.lueDepreciationSetting = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11066DepreciationSettingsItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lueVehicleCategoryID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11005VehicleItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spAS11000PLVehicleCategoryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.deliveryDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.spnDayOfMonthApplyDepreciation = new DevExpress.XtraEditors.SpinEdit();
            this.spnMaximumOccurrence = new DevExpress.XtraEditors.SpinEdit();
            this.lueOccursUnitDescriptor = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLUnitDescriptorBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ceArchive = new DevExpress.XtraEditors.CheckEdit();
            this.equipmentDataNavigator = new DevExpress.XtraEditors.DataNavigator();
            this.spnQuantity = new DevExpress.XtraEditors.SpinEdit();
            this.lueSoftwareProduct_ID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11035SoftwareItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spAS11000PLSoftwareProductBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lueSoftwareVersion_ID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLSoftwareVersionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.deSoftwareYear = new DevExpress.XtraEditors.DateEdit();
            this.txtSoftwareLicence = new DevExpress.XtraEditors.TextEdit();
            this.lueSoftwareParent_Program = new DevExpress.XtraEditors.LookUpEdit();
            this.lueSoftwareAcquisation_Method_ID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLSoftwareAcquisationMethodBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spnSoftwareQuantity_Limit = new DevExpress.XtraEditors.SpinEdit();
            this.txtSoftwareLicence_Key = new DevExpress.XtraEditors.TextEdit();
            this.lueSoftwareLicence_Type_ID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLLicenceTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.deSoftwareValid_Until = new DevExpress.XtraEditors.DateEdit();
            this.lueHardware_Type_ID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLHardwareTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lueHardwareSpecification_ID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11141HardwareSpecificationsItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.txtHardwareDefault_Password = new DevExpress.XtraEditors.TextEdit();
            this.lueGadget_Type_ID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLGadgetTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lueGadgetCasing_ID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLCasingBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.luePlantCategory_ID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLPlantCategoryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spnPlantEngine_Size = new DevExpress.XtraEditors.SpinEdit();
            this.luePlantFuel_Type_ID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLFuelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.txtRegistration_Plate = new DevExpress.XtraEditors.TextEdit();
            this.txtLog_Book_Number = new DevExpress.XtraEditors.TextEdit();
            this.txtModel_Specifications = new DevExpress.XtraEditors.TextEdit();
            this.spnVehicleEngine_Size = new DevExpress.XtraEditors.SpinEdit();
            this.lueVehicleFuel_Type_ID = new DevExpress.XtraEditors.LookUpEdit();
            this.spnEmissions = new DevExpress.XtraEditors.SpinEdit();
            this.spnTotal_Tyre_Number = new DevExpress.XtraEditors.SpinEdit();
            this.lueTowbar_Type_ID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLVehicleTowbarTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cpeColour = new DevExpress.XtraEditors.ColorPickEdit();
            this.deRegistration_Date = new DevExpress.XtraEditors.DateEdit();
            this.deMOT_Due_Date = new DevExpress.XtraEditors.DateEdit();
            this.spnCurrentMileage = new DevExpress.XtraEditors.SpinEdit();
            this.spnService_Due_Mileage = new DevExpress.XtraEditors.SpinEdit();
            this.txtRadioCode = new DevExpress.XtraEditors.TextEdit();
            this.txtKeyCode = new DevExpress.XtraEditors.TextEdit();
            this.txtElectronicCode = new DevExpress.XtraEditors.TextEdit();
            this.txtEquipmentReference = new DevExpress.XtraEditors.TextEdit();
            this.txtManufacturer_ID = new DevExpress.XtraEditors.TextEdit();
            this.lueMake = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11014MakeListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lueModel = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11017ModelListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lueSupplier = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11020SupplierListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lueGc_Marked = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLEquipmentGCMarkedBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lueOwnership_Status = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLEquipmentOwnershipStatusBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lueAvailability = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLEquipmentAvailabilityBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.deDepreciation_Start_Date = new DevExpress.XtraEditors.DateEdit();
            this.txtEx_Asset_ID = new DevExpress.XtraEditors.TextEdit();
            this.txtSage_Asset_Ref = new DevExpress.XtraEditors.TextEdit();
            this.meNotes = new DevExpress.XtraEditors.MemoEdit();
            this.memDescription = new DevExpress.XtraEditors.MemoEdit();
            this.soldDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.vehicleLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.vehRegistrationLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem54 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem53 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.vehSpecificationLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem49 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem39 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.vehMaintenanceLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem42 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem44 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem46 = new DevExpress.XtraLayout.LayoutControlItem();
            this.vehCodesLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem48 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem50 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem52 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.vehPerfomanceLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem47 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem45 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem43 = new DevExpress.XtraLayout.LayoutControlItem();
            this.NoDetailsLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.plantLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem51 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem64 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem67 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem57 = new DevExpress.XtraLayout.LayoutControlItem();
            this.gadgetLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem63 = new DevExpress.XtraLayout.LayoutControlItem();
            this.hardwareLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem41 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem65 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem66 = new DevExpress.XtraLayout.LayoutControlItem();
            this.softwareLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.officeLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroup2 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem62 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.linkedChildDataGroup = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.equipmentLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.equipRefLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.depreciationLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem40 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem56 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem61 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem60 = new DevExpress.XtraLayout.LayoutControlItem();
            this.GCLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem59 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.financeLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem68 = new DevExpress.XtraLayout.LayoutControlItem();
            this.manufacturerLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lblManufacturerID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem55 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.remarksLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.popupContainerControl1 = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.spAS11000FleetManagerDummyScreenBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnPopUp1Ok = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem58 = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp_AS_11005_Vehicle_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11005_Vehicle_ItemTableAdapter();
            this.sp_AS_11008_Plant_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11008_Plant_ItemTableAdapter();
            this.sp_AS_11035_Software_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11035_Software_ItemTableAdapter();
            this.sp_AS_11032_Hardware_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11032_Hardware_ItemTableAdapter();
            this.sp_AS_11011_Equipment_Category_ListTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11011_Equipment_Category_ListTableAdapter();
            this.sp_AS_11000_PL_Equipment_GC_MarkedTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Equipment_GC_MarkedTableAdapter();
            this.sp_AS_11000_PL_Equipment_Ownership_StatusTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Equipment_Ownership_StatusTableAdapter();
            this.sp_AS_11000_PL_Equipment_AvailabilityTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Equipment_AvailabilityTableAdapter();
            this.sp_AS_11014_Make_ListTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11014_Make_ListTableAdapter();
            this.sp_AS_11017_Model_ListTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11017_Model_ListTableAdapter();
            this.sp_AS_11020_Supplier_ListTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11020_Supplier_ListTableAdapter();
            this.sp_AS_11023_Tracker_ListTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11023_Tracker_ListTableAdapter();
            this.sp_AS_11000_PL_Vehicle_Towbar_TypeTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Vehicle_Towbar_TypeTableAdapter();
            this.sp_AS_11000_PL_Plant_CategoryTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Plant_CategoryTableAdapter();
            this.sp_AS_11000_PL_FuelTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_FuelTableAdapter();
            this.sp_AS_11000_PL_Gadget_TypeTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Gadget_TypeTableAdapter();
            this.sp_AS_11000_PL_CasingTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_CasingTableAdapter();
            this.sp_AS_11000_PL_Hardware_TypeTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Hardware_TypeTableAdapter();
            this.sp_AS_11000_PL_Software_ProductTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Software_ProductTableAdapter();
            this.sp_AS_11000_PL_Software_VersionTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Software_VersionTableAdapter();
            this.sp_AS_11000_PL_Licence_TypeTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Licence_TypeTableAdapter();
            this.sp_AS_11000_PL_Software_Acquisation_MethodTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Software_Acquisation_MethodTableAdapter();
            this.spAS11000DuplicateMakeModelSearchBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11000_Duplicate_Make_Model_SearchTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_Duplicate_Make_Model_SearchTableAdapter();
            this.spAS11000DuplicateSearchBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11000_Duplicate_SearchTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_Duplicate_SearchTableAdapter();
            this.sp_AS_11066_Depreciation_Settings_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11066_Depreciation_Settings_ItemTableAdapter();
            this.sp_AS_11000_PL_Unit_DescriptorTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Unit_DescriptorTableAdapter();
            this.sp_AS_11029_Office_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11029_Office_ItemTableAdapter();
            this.sp_AS_11000_PL_Office_CategoryTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Office_CategoryTableAdapter();
            this.sp_AS_11000_PL_Vehicle_CategoryTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Vehicle_CategoryTableAdapter();
            this.sp_AS_11026_Gadget_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11026_Gadget_ItemTableAdapter();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.sp_AS_11000_FleetManagerDummyScreenTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_FleetManagerDummyScreenTableAdapter();
            this.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11044_Keeper_Allocation_ItemTableAdapter();
            this.sp_AS_11041_Transaction_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11041_Transaction_ItemTableAdapter();
            this.sp_AS_11050_Depreciation_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11050_Depreciation_ItemTableAdapter();
            this.sp_AS_11075_Cover_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11075_Cover_ItemTableAdapter();
            this.sp_AS_11078_Work_Detail_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11078_Work_Detail_ItemTableAdapter();
            this.sp_AS_11081_Incident_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11081_Incident_ItemTableAdapter();
            this.sp_AS_11084_Purpose_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11084_Purpose_ItemTableAdapter();
            this.sp_AS_11089_Fuel_Card_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11089_Fuel_Card_ItemTableAdapter();
            this.sp_AS_11105_Speeding_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11105_Speeding_ItemTableAdapter();
            this.sp_AS_11002_Equipment_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11002_Equipment_ItemTableAdapter();
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp_AS_11044_Keeper_Allocation_ListTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11044_Keeper_Allocation_ListTableAdapter();
            this.sp_AS_11000_PL_Incident_StatusTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Incident_StatusTableAdapter();
            this.sp_AS_11000_PL_Incident_TypeTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Incident_TypeTableAdapter();
            this.sp_AS_11000_PL_Incident_Repair_DueTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Incident_Repair_DueTableAdapter();
            this.sp_AS_11000_PL_SeverityTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_SeverityTableAdapter();
            this.sp_AS_11000_PL_Legal_ActionTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Legal_ActionTableAdapter();
            this.sp_AS_11000_PL_Keeper_At_FaultTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Keeper_At_FaultTableAdapter();
            this.sp_AS_11096_Road_Tax_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11096_Road_Tax_ItemTableAdapter();
            this.sp_AS_11141_Hardware_Specifications_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11141_Hardware_Specifications_ItemTableAdapter();
            this.sp_AS_11000_PL_Exchequer_CategoryTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Exchequer_CategoryTableAdapter();
            this.sp_AS_11054_Service_Data_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11054_Service_Data_ItemTableAdapter();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItem2 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem3 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem4 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem5 = new DevExpress.XtraBars.BarStaticItem();
            this.bar4 = new DevExpress.XtraBars.Bar();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.bar5 = new DevExpress.XtraBars.Bar();
            this.barEditItem2 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerControl2 = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.barStaticItem6 = new DevExpress.XtraBars.BarStaticItem();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProviderEquipment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueEquipment_Category.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11002EquipmentItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11011EquipmentCategoryListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentDataLayoutControl)).BeginInit();
            this.equipmentDataLayoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlantRegistrationPlate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11008PlantItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueExchequerCategoryID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLExchequerCategoryBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dePUWERDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dePUWERDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deLOLERDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deLOLERDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceTag.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11032HardwareItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSerialNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueOfficeCategoryID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11029_Office_ItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLOfficeCategoryBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIMEI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11026GadgetItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentChildTabControl)).BeginInit();
            this.equipmentChildTabControl.SuspendLayout();
            this.roadTaxTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.roadTaxGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11096RoadTaxItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.roadTaxGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit4)).BeginInit();
            this.keeperTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.keeperGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11044KeeperAllocationItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.keeperGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueKeeperNotes)).BeginInit();
            this.transactionTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.transactionGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11041TransactionItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.transactionsGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceOnExchequer)).BeginInit();
            this.purposeTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.purposeGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11084PurposeItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.purposeGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit17)).BeginInit();
            this.fuelCardTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fuelCardGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11089FuelCardItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fuelCardGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit19)).BeginInit();
            this.trackerTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackerGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11023TrackerListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackerGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit10)).BeginInit();
            this.billingTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.billingGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11050DepreciationItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.billingGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            this.depreciationTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.depreciationGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.depreciationGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEditDep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            this.coverTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.coverGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11075CoverItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.coverGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit1)).BeginInit();
            this.serviceDatalTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.serviceDataGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11054ServiceDataItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.serviceDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit12)).BeginInit();
            this.workDetailTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.workDetailGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11078WorkDetailItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.workDetailGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit14)).BeginInit();
            this.incidentTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.incidentGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11081IncidentItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.incidentGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.incidentNotesMemoEdit)).BeginInit();
            this.speedingTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.speedingGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11105SpeedingItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speedingGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceReported)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShortDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueDepreciationSetting.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11066DepreciationSettingsItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueVehicleCategoryID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11005VehicleItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLVehicleCategoryBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deliveryDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deliveryDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnDayOfMonthApplyDepreciation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnMaximumOccurrence.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueOccursUnitDescriptor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLUnitDescriptorBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceArchive.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnQuantity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSoftwareProduct_ID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11035SoftwareItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLSoftwareProductBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSoftwareVersion_ID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLSoftwareVersionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deSoftwareYear.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deSoftwareYear.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoftwareLicence.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSoftwareParent_Program.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSoftwareAcquisation_Method_ID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLSoftwareAcquisationMethodBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnSoftwareQuantity_Limit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoftwareLicence_Key.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSoftwareLicence_Type_ID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLLicenceTypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deSoftwareValid_Until.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deSoftwareValid_Until.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueHardware_Type_ID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLHardwareTypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueHardwareSpecification_ID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11141HardwareSpecificationsItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHardwareDefault_Password.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueGadget_Type_ID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLGadgetTypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueGadgetCasing_ID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLCasingBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luePlantCategory_ID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLPlantCategoryBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnPlantEngine_Size.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luePlantFuel_Type_ID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLFuelBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegistration_Plate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLog_Book_Number.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtModel_Specifications.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnVehicleEngine_Size.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueVehicleFuel_Type_ID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnEmissions.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnTotal_Tyre_Number.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTowbar_Type_ID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLVehicleTowbarTypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cpeColour.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deRegistration_Date.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deRegistration_Date.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deMOT_Due_Date.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deMOT_Due_Date.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnCurrentMileage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnService_Due_Mileage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRadioCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKeyCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtElectronicCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentReference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtManufacturer_ID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueMake.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11014MakeListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueModel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11017ModelListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSupplier.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11020SupplierListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueGc_Marked.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLEquipmentGCMarkedBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueOwnership_Status.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLEquipmentOwnershipStatusBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueAvailability.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLEquipmentAvailabilityBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDepreciation_Start_Date.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDepreciation_Start_Date.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEx_Asset_ID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSage_Asset_Ref.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meNotes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.soldDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.soldDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehicleLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehRegistrationLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehSpecificationLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehMaintenanceLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehCodesLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehPerfomanceLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoDetailsLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem67)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gadgetLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hardwareLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem66)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.softwareLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.officeLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkedChildDataGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipRefLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.depreciationLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GCLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.financeLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem68)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.manufacturerLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblManufacturerID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.remarksLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl1)).BeginInit();
            this.popupContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000FleetManagerDummyScreenBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000DuplicateMakeModelSearchBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000DuplicateSearchBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl2)).BeginInit();
            this.popupContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1362, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 710);
            this.barDockControlBottom.Size = new System.Drawing.Size(1362, 30);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 684);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1362, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 684);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barStatus,
            this.bar1,
            this.bar2});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.bsiFormMode,
            this.barStaticItem1,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemChangesPending,
            this.barStaticItemInformation,
            this.barEditItem1});
            this.barManager1.MaxItemId = 36;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEdit1});
            this.barManager1.StatusBar = this.barStatus;
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "dataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.sp_AS_11002_Equipment_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11005_Vehicle_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11008_Plant_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11011_Equipment_Category_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11014_Make_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11017_Model_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11020_Supplier_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Tracker_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Verilocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11026_Gadget_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11029_Office_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11032_Hardware_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11035_Software_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11041_Transaction_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11047_Equipment_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11050_Depreciation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Data_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Interval_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11057_Company_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11060_Department_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11063_Cost_Centre_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11066_Depreciation_Settings_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11075_Cover_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11078_Work_Detail_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11081_Incident_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11084_Purpose_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11087_Equipment_Category_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11089_Fuel_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11093_Initial_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11096_Road_Tax_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11099_Improvement_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11102_Rental_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11105_Speeding_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11108_Rental_Details_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11112_Keeper_Email_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11117_Sim_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11120_Additional_FollowUps_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11123_Sim_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_AlertTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11129_Software_Assign_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11133_Picklist_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_AS_11141_Hardware_Specifications_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11157_Email_Schedule_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11163_Service_ScheduleTableAdapter = null;
            this.tableAdapterManager.sp_AS_11166_P11D_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11169_NetBookValue_ItemTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // barStatus
            // 
            this.barStatus.BarName = "Status Bar";
            this.barStatus.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.barStatus.DockCol = 0;
            this.barStatus.DockRow = 0;
            this.barStatus.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.barStatus.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiFormMode, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barStaticItemInformation, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barStatus.OptionsBar.AllowQuickCustomization = false;
            this.barStatus.OptionsBar.DrawDragBorder = false;
            this.barStatus.OptionsBar.UseWholeRow = true;
            this.barStatus.Text = "Status bar";
            // 
            // bsiFormMode
            // 
            this.bsiFormMode.Caption = "Form Mode: Editing";
            this.bsiFormMode.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.Glyph")));
            this.bsiFormMode.Id = 29;
            this.bsiFormMode.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.LargeGlyph")));
            this.bsiFormMode.Name = "bsiFormMode";
            toolTipTitleItem9.Text = "Form Mode - Information";
            toolTipItem9.LeftIndent = 6;
            toolTipItem9.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add";
            superToolTip9.Items.Add(toolTipTitleItem9);
            superToolTip9.Items.Add(toolTipItem9);
            this.bsiFormMode.SuperTip = superToolTip9;
            this.bsiFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 31;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 32;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.Glyph")));
            this.barStaticItemInformation.Id = 33;
            this.barStaticItemInformation.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.LargeGlyph")));
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 27;
            this.bbiFormSave.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.LargeGlyph")));
            this.bbiFormSave.Name = "bbiFormSave";
            toolTipTitleItem7.Appearance.Options.UseImage = true;
            toolTipTitleItem7.Text = "Save Button - Information";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            this.bbiFormSave.SuperTip = superToolTip7;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.Glyph")));
            this.bbiFormCancel.Id = 28;
            this.bbiFormCancel.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.LargeGlyph")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            toolTipTitleItem8.Appearance.Options.UseImage = true;
            toolTipTitleItem8.Text = "Cancel Button - Information";
            toolTipItem8.LeftIndent = 6;
            toolTipItem8.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip8.Items.Add(toolTipTitleItem8);
            superToolTip8.Items.Add(toolTipItem8);
            this.bbiFormCancel.SuperTip = superToolTip8;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "barStaticItem1";
            this.barStaticItem1.Id = 30;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // dxErrorProviderEquipment
            // 
            this.dxErrorProviderEquipment.ContainerControl = this;
            // 
            // lueEquipment_Category
            // 
            this.lueEquipment_Category.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11002EquipmentItemBindingSource, "EquipmentCategory", true));
            this.lueEquipment_Category.Location = new System.Drawing.Point(203, 127);
            this.lueEquipment_Category.MenuManager = this.barManager1;
            this.lueEquipment_Category.Name = "lueEquipment_Category";
            this.lueEquipment_Category.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueEquipment_Category.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Equipment Category", 120, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ProfitLossCode", "Profit Loss Code", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("BalanceSheetCode", "Balance Sheet Code", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DisposalsGL", "Disposals GL", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueEquipment_Category.Properties.DataSource = this.spAS11011EquipmentCategoryListBindingSource;
            this.lueEquipment_Category.Properties.DisplayMember = "Description";
            this.lueEquipment_Category.Properties.NullText = "";
            this.lueEquipment_Category.Properties.NullValuePrompt = "--Please Select a Category--";
            this.lueEquipment_Category.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueEquipment_Category.Properties.PopupWidth = 350;
            this.lueEquipment_Category.Properties.ValueMember = "EquipmentCategory";
            this.lueEquipment_Category.Size = new System.Drawing.Size(50, 20);
            this.lueEquipment_Category.StyleController = this.equipmentDataLayoutControl;
            this.lueEquipment_Category.TabIndex = 9;
            this.lueEquipment_Category.Tag = "Equipment Category";
            this.lueEquipment_Category.EditValueChanged += new System.EventHandler(this.lueEquipment_Category_EditValueChanged);
            this.lueEquipment_Category.Validating += new System.ComponentModel.CancelEventHandler(this.lueEquipment_Category_Validating);
            // 
            // spAS11002EquipmentItemBindingSource
            // 
            this.spAS11002EquipmentItemBindingSource.DataMember = "sp_AS_11002_Equipment_Item";
            this.spAS11002EquipmentItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            this.spAS11002EquipmentItemBindingSource.CurrentChanged += new System.EventHandler(this.spAS11002EquipmentItemBindingSource_CurrentChanged);
            // 
            // spAS11011EquipmentCategoryListBindingSource
            // 
            this.spAS11011EquipmentCategoryListBindingSource.DataMember = "sp_AS_11011_Equipment_Category_List";
            this.spAS11011EquipmentCategoryListBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // equipmentDataLayoutControl
            // 
            this.equipmentDataLayoutControl.Controls.Add(this.txtPlantRegistrationPlate);
            this.equipmentDataLayoutControl.Controls.Add(this.lueExchequerCategoryID);
            this.equipmentDataLayoutControl.Controls.Add(this.dePUWERDate);
            this.equipmentDataLayoutControl.Controls.Add(this.deLOLERDate);
            this.equipmentDataLayoutControl.Controls.Add(this.txtServiceTag);
            this.equipmentDataLayoutControl.Controls.Add(this.txtSerialNumber);
            this.equipmentDataLayoutControl.Controls.Add(this.lueOfficeCategoryID);
            this.equipmentDataLayoutControl.Controls.Add(this.txtIMEI);
            this.equipmentDataLayoutControl.Controls.Add(this.equipmentChildTabControl);
            this.equipmentDataLayoutControl.Controls.Add(this.txtShortDescription);
            this.equipmentDataLayoutControl.Controls.Add(this.lueDepreciationSetting);
            this.equipmentDataLayoutControl.Controls.Add(this.lueVehicleCategoryID);
            this.equipmentDataLayoutControl.Controls.Add(this.deliveryDateDateEdit);
            this.equipmentDataLayoutControl.Controls.Add(this.spnDayOfMonthApplyDepreciation);
            this.equipmentDataLayoutControl.Controls.Add(this.spnMaximumOccurrence);
            this.equipmentDataLayoutControl.Controls.Add(this.lueOccursUnitDescriptor);
            this.equipmentDataLayoutControl.Controls.Add(this.ceArchive);
            this.equipmentDataLayoutControl.Controls.Add(this.equipmentDataNavigator);
            this.equipmentDataLayoutControl.Controls.Add(this.spnQuantity);
            this.equipmentDataLayoutControl.Controls.Add(this.lueSoftwareProduct_ID);
            this.equipmentDataLayoutControl.Controls.Add(this.lueSoftwareVersion_ID);
            this.equipmentDataLayoutControl.Controls.Add(this.deSoftwareYear);
            this.equipmentDataLayoutControl.Controls.Add(this.txtSoftwareLicence);
            this.equipmentDataLayoutControl.Controls.Add(this.lueSoftwareParent_Program);
            this.equipmentDataLayoutControl.Controls.Add(this.lueSoftwareAcquisation_Method_ID);
            this.equipmentDataLayoutControl.Controls.Add(this.spnSoftwareQuantity_Limit);
            this.equipmentDataLayoutControl.Controls.Add(this.txtSoftwareLicence_Key);
            this.equipmentDataLayoutControl.Controls.Add(this.lueSoftwareLicence_Type_ID);
            this.equipmentDataLayoutControl.Controls.Add(this.deSoftwareValid_Until);
            this.equipmentDataLayoutControl.Controls.Add(this.lueHardware_Type_ID);
            this.equipmentDataLayoutControl.Controls.Add(this.lueHardwareSpecification_ID);
            this.equipmentDataLayoutControl.Controls.Add(this.txtHardwareDefault_Password);
            this.equipmentDataLayoutControl.Controls.Add(this.lueGadget_Type_ID);
            this.equipmentDataLayoutControl.Controls.Add(this.lueGadgetCasing_ID);
            this.equipmentDataLayoutControl.Controls.Add(this.luePlantCategory_ID);
            this.equipmentDataLayoutControl.Controls.Add(this.spnPlantEngine_Size);
            this.equipmentDataLayoutControl.Controls.Add(this.luePlantFuel_Type_ID);
            this.equipmentDataLayoutControl.Controls.Add(this.txtRegistration_Plate);
            this.equipmentDataLayoutControl.Controls.Add(this.txtLog_Book_Number);
            this.equipmentDataLayoutControl.Controls.Add(this.txtModel_Specifications);
            this.equipmentDataLayoutControl.Controls.Add(this.spnVehicleEngine_Size);
            this.equipmentDataLayoutControl.Controls.Add(this.lueVehicleFuel_Type_ID);
            this.equipmentDataLayoutControl.Controls.Add(this.spnEmissions);
            this.equipmentDataLayoutControl.Controls.Add(this.spnTotal_Tyre_Number);
            this.equipmentDataLayoutControl.Controls.Add(this.lueTowbar_Type_ID);
            this.equipmentDataLayoutControl.Controls.Add(this.cpeColour);
            this.equipmentDataLayoutControl.Controls.Add(this.deRegistration_Date);
            this.equipmentDataLayoutControl.Controls.Add(this.deMOT_Due_Date);
            this.equipmentDataLayoutControl.Controls.Add(this.spnCurrentMileage);
            this.equipmentDataLayoutControl.Controls.Add(this.spnService_Due_Mileage);
            this.equipmentDataLayoutControl.Controls.Add(this.txtRadioCode);
            this.equipmentDataLayoutControl.Controls.Add(this.txtKeyCode);
            this.equipmentDataLayoutControl.Controls.Add(this.txtElectronicCode);
            this.equipmentDataLayoutControl.Controls.Add(this.txtEquipmentReference);
            this.equipmentDataLayoutControl.Controls.Add(this.lueEquipment_Category);
            this.equipmentDataLayoutControl.Controls.Add(this.txtManufacturer_ID);
            this.equipmentDataLayoutControl.Controls.Add(this.lueMake);
            this.equipmentDataLayoutControl.Controls.Add(this.lueModel);
            this.equipmentDataLayoutControl.Controls.Add(this.lueSupplier);
            this.equipmentDataLayoutControl.Controls.Add(this.lueGc_Marked);
            this.equipmentDataLayoutControl.Controls.Add(this.lueOwnership_Status);
            this.equipmentDataLayoutControl.Controls.Add(this.lueAvailability);
            this.equipmentDataLayoutControl.Controls.Add(this.deDepreciation_Start_Date);
            this.equipmentDataLayoutControl.Controls.Add(this.txtEx_Asset_ID);
            this.equipmentDataLayoutControl.Controls.Add(this.txtSage_Asset_Ref);
            this.equipmentDataLayoutControl.Controls.Add(this.meNotes);
            this.equipmentDataLayoutControl.Controls.Add(this.memDescription);
            this.equipmentDataLayoutControl.Controls.Add(this.soldDateDateEdit);
            this.equipmentDataLayoutControl.DataSource = this.spAS11002EquipmentItemBindingSource;
            this.equipmentDataLayoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.equipmentDataLayoutControl.Location = new System.Drawing.Point(0, 26);
            this.equipmentDataLayoutControl.Name = "equipmentDataLayoutControl";
            this.equipmentDataLayoutControl.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(437, 134, 572, 672);
            this.equipmentDataLayoutControl.Root = this.layoutControlGroup1;
            this.equipmentDataLayoutControl.Size = new System.Drawing.Size(1362, 684);
            this.equipmentDataLayoutControl.TabIndex = 4;
            this.equipmentDataLayoutControl.Text = "dataLayoutControl1";
            // 
            // txtPlantRegistrationPlate
            // 
            this.txtPlantRegistrationPlate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11008PlantItemBindingSource, "RegistrationPlate", true));
            this.txtPlantRegistrationPlate.Location = new System.Drawing.Point(191, 539);
            this.txtPlantRegistrationPlate.MenuManager = this.barManager1;
            this.txtPlantRegistrationPlate.Name = "txtPlantRegistrationPlate";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtPlantRegistrationPlate, true);
            this.txtPlantRegistrationPlate.Size = new System.Drawing.Size(451, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtPlantRegistrationPlate, optionsSpelling21);
            this.txtPlantRegistrationPlate.StyleController = this.equipmentDataLayoutControl;
            this.txtPlantRegistrationPlate.TabIndex = 144;
            this.txtPlantRegistrationPlate.Validating += new System.ComponentModel.CancelEventHandler(this.txtPlantRegistrationPlate_Validating);
            // 
            // spAS11008PlantItemBindingSource
            // 
            this.spAS11008PlantItemBindingSource.DataMember = "sp_AS_11008_Plant_Item";
            this.spAS11008PlantItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // lueExchequerCategoryID
            // 
            this.lueExchequerCategoryID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11002EquipmentItemBindingSource, "ExchequerCategoryID", true));
            this.lueExchequerCategoryID.Location = new System.Drawing.Point(203, 333);
            this.lueExchequerCategoryID.MenuManager = this.barManager1;
            this.lueExchequerCategoryID.Name = "lueExchequerCategoryID";
            this.lueExchequerCategoryID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueExchequerCategoryID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Exchequer Category", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueExchequerCategoryID.Properties.DataSource = this.spAS11000PLExchequerCategoryBindingSource;
            this.lueExchequerCategoryID.Properties.DisplayMember = "Value";
            this.lueExchequerCategoryID.Properties.NullText = "";
            this.lueExchequerCategoryID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueExchequerCategoryID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueExchequerCategoryID.Properties.ValueMember = "PickListID";
            this.lueExchequerCategoryID.Size = new System.Drawing.Size(50, 20);
            this.lueExchequerCategoryID.StyleController = this.equipmentDataLayoutControl;
            this.lueExchequerCategoryID.TabIndex = 143;
            this.lueExchequerCategoryID.Tag = "Exchequer Category";
            this.lueExchequerCategoryID.Validating += new System.ComponentModel.CancelEventHandler(this.lueExchequerCategoryID_Validating);
            // 
            // spAS11000PLExchequerCategoryBindingSource
            // 
            this.spAS11000PLExchequerCategoryBindingSource.DataMember = "sp_AS_11000_PL_Exchequer_Category";
            this.spAS11000PLExchequerCategoryBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // dePUWERDate
            // 
            this.dePUWERDate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11008PlantItemBindingSource, "PUWERDate", true));
            this.dePUWERDate.EditValue = null;
            this.dePUWERDate.Location = new System.Drawing.Point(813, 515);
            this.dePUWERDate.MenuManager = this.barManager1;
            this.dePUWERDate.Name = "dePUWERDate";
            this.dePUWERDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dePUWERDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dePUWERDate.Size = new System.Drawing.Size(508, 20);
            this.dePUWERDate.StyleController = this.equipmentDataLayoutControl;
            this.dePUWERDate.TabIndex = 142;
            // 
            // deLOLERDate
            // 
            this.deLOLERDate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11008PlantItemBindingSource, "LOLERDate", true));
            this.deLOLERDate.EditValue = null;
            this.deLOLERDate.Location = new System.Drawing.Point(191, 515);
            this.deLOLERDate.MenuManager = this.barManager1;
            this.deLOLERDate.Name = "deLOLERDate";
            this.deLOLERDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deLOLERDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deLOLERDate.Size = new System.Drawing.Size(451, 20);
            this.deLOLERDate.StyleController = this.equipmentDataLayoutControl;
            this.deLOLERDate.TabIndex = 141;
            // 
            // txtServiceTag
            // 
            this.txtServiceTag.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11032HardwareItemBindingSource, "ServiceTag", true));
            this.txtServiceTag.Location = new System.Drawing.Point(839, 491);
            this.txtServiceTag.MenuManager = this.barManager1;
            this.txtServiceTag.Name = "txtServiceTag";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtServiceTag, true);
            this.txtServiceTag.Size = new System.Drawing.Size(482, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtServiceTag, optionsSpelling1);
            this.txtServiceTag.StyleController = this.equipmentDataLayoutControl;
            this.txtServiceTag.TabIndex = 140;
            // 
            // spAS11032HardwareItemBindingSource
            // 
            this.spAS11032HardwareItemBindingSource.DataMember = "sp_AS_11032_Hardware_Item";
            this.spAS11032HardwareItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // txtSerialNumber
            // 
            this.txtSerialNumber.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11032HardwareItemBindingSource, "SerialNumber", true));
            this.txtSerialNumber.Location = new System.Drawing.Point(191, 491);
            this.txtSerialNumber.MenuManager = this.barManager1;
            this.txtSerialNumber.Name = "txtSerialNumber";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtSerialNumber, true);
            this.txtSerialNumber.Size = new System.Drawing.Size(477, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtSerialNumber, optionsSpelling2);
            this.txtSerialNumber.StyleController = this.equipmentDataLayoutControl;
            this.txtSerialNumber.TabIndex = 139;
            // 
            // lueOfficeCategoryID
            // 
            this.lueOfficeCategoryID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp_AS_11029_Office_ItemBindingSource, "OfficeCategoryID", true));
            this.lueOfficeCategoryID.Location = new System.Drawing.Point(191, 467);
            this.lueOfficeCategoryID.MenuManager = this.barManager1;
            this.lueOfficeCategoryID.Name = "lueOfficeCategoryID";
            this.lueOfficeCategoryID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueOfficeCategoryID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Office Category", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueOfficeCategoryID.Properties.DataSource = this.spAS11000PLOfficeCategoryBindingSource;
            this.lueOfficeCategoryID.Properties.DisplayMember = "Value";
            this.lueOfficeCategoryID.Properties.NullText = "";
            this.lueOfficeCategoryID.Properties.NullValuePrompt = "-- Please Select Category --";
            this.lueOfficeCategoryID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueOfficeCategoryID.Properties.ValueMember = "PickListID";
            this.lueOfficeCategoryID.Size = new System.Drawing.Size(477, 20);
            this.lueOfficeCategoryID.StyleController = this.equipmentDataLayoutControl;
            this.lueOfficeCategoryID.TabIndex = 137;
            // 
            // sp_AS_11029_Office_ItemBindingSource
            // 
            this.sp_AS_11029_Office_ItemBindingSource.DataMember = "sp_AS_11029_Office_Item";
            this.sp_AS_11029_Office_ItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spAS11000PLOfficeCategoryBindingSource
            // 
            this.spAS11000PLOfficeCategoryBindingSource.DataMember = "sp_AS_11000_PL_Office_Category";
            this.spAS11000PLOfficeCategoryBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // txtIMEI
            // 
            this.txtIMEI.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11026GadgetItemBindingSource, "IMEI", true));
            this.txtIMEI.Location = new System.Drawing.Point(203, 499);
            this.txtIMEI.MenuManager = this.barManager1;
            this.txtIMEI.Name = "txtIMEI";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtIMEI, true);
            this.txtIMEI.Size = new System.Drawing.Size(471, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtIMEI, optionsSpelling3);
            this.txtIMEI.StyleController = this.equipmentDataLayoutControl;
            this.txtIMEI.TabIndex = 136;
            this.txtIMEI.Validating += new System.ComponentModel.CancelEventHandler(this.txtIMEI_Validating);
            // 
            // spAS11026GadgetItemBindingSource
            // 
            this.spAS11026GadgetItemBindingSource.DataMember = "sp_AS_11026_Gadget_Item";
            this.spAS11026GadgetItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // equipmentChildTabControl
            // 
            this.equipmentChildTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.equipmentChildTabControl.Location = new System.Drawing.Point(26, 870);
            this.equipmentChildTabControl.Name = "equipmentChildTabControl";
            this.equipmentChildTabControl.SelectedTabPage = this.roadTaxTabPage;
            this.equipmentChildTabControl.Size = new System.Drawing.Size(1295, 226);
            this.equipmentChildTabControl.TabIndex = 6;
            this.equipmentChildTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.keeperTabPage,
            this.transactionTabPage,
            this.purposeTabPage,
            this.fuelCardTabPage,
            this.trackerTabPage,
            this.roadTaxTabPage,
            this.billingTabPage,
            this.depreciationTabPage,
            this.coverTabPage,
            this.serviceDatalTabPage,
            this.workDetailTabPage,
            this.incidentTabPage,
            this.speedingTabPage});
            // 
            // roadTaxTabPage
            // 
            this.roadTaxTabPage.Controls.Add(this.roadTaxGridControl);
            this.roadTaxTabPage.Name = "roadTaxTabPage";
            this.roadTaxTabPage.Size = new System.Drawing.Size(1290, 200);
            this.roadTaxTabPage.Text = "Road Tax";
            // 
            // roadTaxGridControl
            // 
            this.roadTaxGridControl.DataSource = this.spAS11096RoadTaxItemBindingSource;
            this.roadTaxGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.roadTaxGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.roadTaxGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.roadTaxGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.roadTaxGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.roadTaxGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.roadTaxGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.roadTaxGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.roadTaxGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.roadTaxGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.roadTaxGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.roadTaxGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.roadTaxGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.roadTaxGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.roadTaxGridControl.Location = new System.Drawing.Point(0, 0);
            this.roadTaxGridControl.MainView = this.roadTaxGridView;
            this.roadTaxGridControl.MenuManager = this.barManager1;
            this.roadTaxGridControl.Name = "roadTaxGridControl";
            this.roadTaxGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.moneyTextEdit4});
            this.roadTaxGridControl.Size = new System.Drawing.Size(1290, 200);
            this.roadTaxGridControl.TabIndex = 1;
            this.roadTaxGridControl.UseEmbeddedNavigator = true;
            this.roadTaxGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.roadTaxGridView});
            // 
            // spAS11096RoadTaxItemBindingSource
            // 
            this.spAS11096RoadTaxItemBindingSource.DataMember = "sp_AS_11096_Road_Tax_Item";
            this.spAS11096RoadTaxItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertGalleryImage("add_16x16.png", "images/actions/add_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/add_16x16.png"), 0);
            this.imageCollection1.Images.SetKeyName(0, "add_16x16.png");
            this.imageCollection1.InsertGalleryImage("edit_16x16.png", "images/edit/edit_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/edit_16x16.png"), 1);
            this.imageCollection1.Images.SetKeyName(1, "edit_16x16.png");
            this.imageCollection1.InsertGalleryImage("delete_16x16.png", "images/edit/delete_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/delete_16x16.png"), 2);
            this.imageCollection1.Images.SetKeyName(2, "delete_16x16.png");
            this.imageCollection1.InsertGalleryImage("preview_16x16.png", "images/print/preview_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/print/preview_16x16.png"), 3);
            this.imageCollection1.Images.SetKeyName(3, "preview_16x16.png");
            this.imageCollection1.InsertGalleryImage("country_16x16.png", "images/miscellaneous/country_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/miscellaneous/country_16x16.png"), 4);
            this.imageCollection1.Images.SetKeyName(4, "country_16x16.png");
            this.imageCollection1.InsertGalleryImage("insert_16x16.png", "images/actions/insert_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/insert_16x16.png"), 5);
            this.imageCollection1.Images.SetKeyName(5, "insert_16x16.png");
            // 
            // roadTaxGridView
            // 
            this.roadTaxGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRoadTaxID,
            this.colEquipmentReference12,
            this.colEquipmentID14,
            this.colTaxExpiryDate,
            this.colAmount,
            this.colTaxPeriodID,
            this.colTaxPeriod,
            this.colMode13,
            this.colRecordID13});
            this.roadTaxGridView.GridControl = this.roadTaxGridControl;
            this.roadTaxGridView.Name = "roadTaxGridView";
            this.roadTaxGridView.OptionsCustomization.AllowFilter = false;
            this.roadTaxGridView.OptionsCustomization.AllowGroup = false;
            this.roadTaxGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.roadTaxGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.roadTaxGridView.OptionsLayout.StoreAppearance = true;
            this.roadTaxGridView.OptionsSelection.MultiSelect = true;
            this.roadTaxGridView.OptionsView.ColumnAutoWidth = false;
            this.roadTaxGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.roadTaxGridView.OptionsView.ShowGroupPanel = false;
            this.roadTaxGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.roadTaxGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.roadTaxGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.roadTaxGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.roadTaxGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.roadTaxGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.roadTaxGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colRoadTaxID
            // 
            this.colRoadTaxID.FieldName = "RoadTaxID";
            this.colRoadTaxID.Name = "colRoadTaxID";
            this.colRoadTaxID.OptionsColumn.AllowEdit = false;
            this.colRoadTaxID.OptionsColumn.AllowFocus = false;
            this.colRoadTaxID.OptionsColumn.ReadOnly = true;
            this.colRoadTaxID.OptionsColumn.ShowInCustomizationForm = false;
            this.colRoadTaxID.OptionsColumn.ShowInExpressionEditor = false;
            this.colRoadTaxID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRoadTaxID.Width = 157;
            // 
            // colEquipmentReference12
            // 
            this.colEquipmentReference12.FieldName = "EquipmentReference";
            this.colEquipmentReference12.Name = "colEquipmentReference12";
            this.colEquipmentReference12.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference12.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference12.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference12.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference12.Visible = true;
            this.colEquipmentReference12.VisibleIndex = 0;
            this.colEquipmentReference12.Width = 187;
            // 
            // colEquipmentID14
            // 
            this.colEquipmentID14.FieldName = "EquipmentID";
            this.colEquipmentID14.Name = "colEquipmentID14";
            this.colEquipmentID14.OptionsColumn.AllowEdit = false;
            this.colEquipmentID14.OptionsColumn.AllowFocus = false;
            this.colEquipmentID14.OptionsColumn.ReadOnly = true;
            this.colEquipmentID14.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID14.Width = 154;
            // 
            // colTaxExpiryDate
            // 
            this.colTaxExpiryDate.FieldName = "TaxExpiryDate";
            this.colTaxExpiryDate.Name = "colTaxExpiryDate";
            this.colTaxExpiryDate.OptionsColumn.AllowEdit = false;
            this.colTaxExpiryDate.OptionsColumn.AllowFocus = false;
            this.colTaxExpiryDate.OptionsColumn.ReadOnly = true;
            this.colTaxExpiryDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTaxExpiryDate.Visible = true;
            this.colTaxExpiryDate.VisibleIndex = 1;
            this.colTaxExpiryDate.Width = 203;
            // 
            // colAmount
            // 
            this.colAmount.ColumnEdit = this.moneyTextEdit4;
            this.colAmount.FieldName = "Amount";
            this.colAmount.Name = "colAmount";
            this.colAmount.OptionsColumn.AllowEdit = false;
            this.colAmount.OptionsColumn.AllowFocus = false;
            this.colAmount.OptionsColumn.ReadOnly = true;
            this.colAmount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAmount.Visible = true;
            this.colAmount.VisibleIndex = 2;
            this.colAmount.Width = 222;
            // 
            // moneyTextEdit4
            // 
            this.moneyTextEdit4.AutoHeight = false;
            this.moneyTextEdit4.DisplayFormat.FormatString = "c2";
            this.moneyTextEdit4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.moneyTextEdit4.Name = "moneyTextEdit4";
            // 
            // colTaxPeriodID
            // 
            this.colTaxPeriodID.FieldName = "TaxPeriodID";
            this.colTaxPeriodID.Name = "colTaxPeriodID";
            this.colTaxPeriodID.OptionsColumn.AllowEdit = false;
            this.colTaxPeriodID.OptionsColumn.AllowFocus = false;
            this.colTaxPeriodID.OptionsColumn.ReadOnly = true;
            this.colTaxPeriodID.OptionsColumn.ShowInCustomizationForm = false;
            this.colTaxPeriodID.OptionsColumn.ShowInExpressionEditor = false;
            this.colTaxPeriodID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTaxPeriodID.Width = 77;
            // 
            // colTaxPeriod
            // 
            this.colTaxPeriod.FieldName = "TaxPeriod";
            this.colTaxPeriod.Name = "colTaxPeriod";
            this.colTaxPeriod.OptionsColumn.AllowEdit = false;
            this.colTaxPeriod.OptionsColumn.AllowFocus = false;
            this.colTaxPeriod.OptionsColumn.ReadOnly = true;
            this.colTaxPeriod.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTaxPeriod.Visible = true;
            this.colTaxPeriod.VisibleIndex = 3;
            this.colTaxPeriod.Width = 367;
            // 
            // colMode13
            // 
            this.colMode13.FieldName = "Mode";
            this.colMode13.Name = "colMode13";
            this.colMode13.OptionsColumn.AllowEdit = false;
            this.colMode13.OptionsColumn.AllowFocus = false;
            this.colMode13.OptionsColumn.ReadOnly = true;
            this.colMode13.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode13.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode13.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID13
            // 
            this.colRecordID13.FieldName = "RecordID";
            this.colRecordID13.Name = "colRecordID13";
            this.colRecordID13.OptionsColumn.AllowEdit = false;
            this.colRecordID13.OptionsColumn.AllowFocus = false;
            this.colRecordID13.OptionsColumn.ReadOnly = true;
            this.colRecordID13.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID13.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID13.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // keeperTabPage
            // 
            this.keeperTabPage.AutoScroll = true;
            this.keeperTabPage.Controls.Add(this.keeperGridControl);
            this.keeperTabPage.Name = "keeperTabPage";
            this.keeperTabPage.Size = new System.Drawing.Size(1290, 200);
            this.keeperTabPage.Text = "Keeper Allocations";
            // 
            // keeperGridControl
            // 
            this.keeperGridControl.DataSource = this.spAS11044KeeperAllocationItemBindingSource;
            this.keeperGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.keeperGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.keeperGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.keeperGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.keeperGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.keeperGridControl.Location = new System.Drawing.Point(0, 0);
            this.keeperGridControl.MainView = this.keeperGridView;
            this.keeperGridControl.MenuManager = this.barManager1;
            this.keeperGridControl.Name = "keeperGridControl";
            this.keeperGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.lueKeeperNotes});
            this.keeperGridControl.Size = new System.Drawing.Size(1290, 200);
            this.keeperGridControl.TabIndex = 2;
            this.keeperGridControl.UseEmbeddedNavigator = true;
            this.keeperGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.keeperGridView});
            // 
            // spAS11044KeeperAllocationItemBindingSource
            // 
            this.spAS11044KeeperAllocationItemBindingSource.DataMember = "sp_AS_11044_Keeper_Allocation_Item";
            this.spAS11044KeeperAllocationItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // keeperGridView
            // 
            this.keeperGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colKeeperAllocationID,
            this.colEquipmentReference9,
            this.colKeeperTypeID,
            this.colKeeperType,
            this.colKeeperID,
            this.colKeeper,
            this.colEquipmentID11,
            this.colAllocationStatusID,
            this.colAllocationStatus,
            this.colAllocationDate,
            this.colAllocationEndDate,
            this.colNotes1,
            this.colMode7,
            this.colRecordID7});
            this.keeperGridView.GridControl = this.keeperGridControl;
            this.keeperGridView.Name = "keeperGridView";
            this.keeperGridView.OptionsCustomization.AllowGroup = false;
            this.keeperGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.keeperGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.keeperGridView.OptionsLayout.StoreAppearance = true;
            this.keeperGridView.OptionsSelection.MultiSelect = true;
            this.keeperGridView.OptionsView.ColumnAutoWidth = false;
            this.keeperGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.keeperGridView.OptionsView.ShowGroupPanel = false;
            this.keeperGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.keeperGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.keeperGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.keeperGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.keeperGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.keeperGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.keeperGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colKeeperAllocationID
            // 
            this.colKeeperAllocationID.FieldName = "KeeperAllocationID";
            this.colKeeperAllocationID.Name = "colKeeperAllocationID";
            this.colKeeperAllocationID.OptionsColumn.AllowEdit = false;
            this.colKeeperAllocationID.OptionsColumn.AllowFocus = false;
            this.colKeeperAllocationID.OptionsColumn.ReadOnly = true;
            this.colKeeperAllocationID.OptionsColumn.ShowInCustomizationForm = false;
            this.colKeeperAllocationID.OptionsColumn.ShowInExpressionEditor = false;
            this.colKeeperAllocationID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colKeeperAllocationID.Width = 119;
            // 
            // colEquipmentReference9
            // 
            this.colEquipmentReference9.FieldName = "EquipmentReference";
            this.colEquipmentReference9.Name = "colEquipmentReference9";
            this.colEquipmentReference9.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference9.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference9.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference9.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference9.Visible = true;
            this.colEquipmentReference9.VisibleIndex = 0;
            this.colEquipmentReference9.Width = 125;
            // 
            // colKeeperTypeID
            // 
            this.colKeeperTypeID.FieldName = "KeeperTypeID";
            this.colKeeperTypeID.Name = "colKeeperTypeID";
            this.colKeeperTypeID.OptionsColumn.AllowEdit = false;
            this.colKeeperTypeID.OptionsColumn.AllowFocus = false;
            this.colKeeperTypeID.OptionsColumn.ReadOnly = true;
            this.colKeeperTypeID.OptionsColumn.ShowInCustomizationForm = false;
            this.colKeeperTypeID.OptionsColumn.ShowInExpressionEditor = false;
            this.colKeeperTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colKeeperTypeID.Width = 97;
            // 
            // colKeeperType
            // 
            this.colKeeperType.FieldName = "KeeperType";
            this.colKeeperType.Name = "colKeeperType";
            this.colKeeperType.OptionsColumn.AllowEdit = false;
            this.colKeeperType.OptionsColumn.AllowFocus = false;
            this.colKeeperType.OptionsColumn.ReadOnly = true;
            this.colKeeperType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colKeeperType.Visible = true;
            this.colKeeperType.VisibleIndex = 1;
            this.colKeeperType.Width = 83;
            // 
            // colKeeperID
            // 
            this.colKeeperID.FieldName = "KeeperID";
            this.colKeeperID.Name = "colKeeperID";
            this.colKeeperID.OptionsColumn.AllowEdit = false;
            this.colKeeperID.OptionsColumn.AllowFocus = false;
            this.colKeeperID.OptionsColumn.ReadOnly = true;
            this.colKeeperID.OptionsColumn.ShowInCustomizationForm = false;
            this.colKeeperID.OptionsColumn.ShowInExpressionEditor = false;
            this.colKeeperID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colKeeper
            // 
            this.colKeeper.FieldName = "Keeper";
            this.colKeeper.Name = "colKeeper";
            this.colKeeper.OptionsColumn.AllowEdit = false;
            this.colKeeper.OptionsColumn.AllowFocus = false;
            this.colKeeper.OptionsColumn.ReadOnly = true;
            this.colKeeper.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colKeeper.Visible = true;
            this.colKeeper.VisibleIndex = 2;
            // 
            // colEquipmentID11
            // 
            this.colEquipmentID11.FieldName = "EquipmentID";
            this.colEquipmentID11.Name = "colEquipmentID11";
            this.colEquipmentID11.OptionsColumn.AllowEdit = false;
            this.colEquipmentID11.OptionsColumn.AllowFocus = false;
            this.colEquipmentID11.OptionsColumn.ReadOnly = true;
            this.colEquipmentID11.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID11.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID11.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID11.Width = 86;
            // 
            // colAllocationStatusID
            // 
            this.colAllocationStatusID.FieldName = "AllocationStatusID";
            this.colAllocationStatusID.Name = "colAllocationStatusID";
            this.colAllocationStatusID.OptionsColumn.AllowEdit = false;
            this.colAllocationStatusID.OptionsColumn.AllowFocus = false;
            this.colAllocationStatusID.OptionsColumn.ReadOnly = true;
            this.colAllocationStatusID.OptionsColumn.ShowInCustomizationForm = false;
            this.colAllocationStatusID.OptionsColumn.ShowInExpressionEditor = false;
            this.colAllocationStatusID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAllocationStatusID.Width = 116;
            // 
            // colAllocationStatus
            // 
            this.colAllocationStatus.FieldName = "AllocationStatus";
            this.colAllocationStatus.Name = "colAllocationStatus";
            this.colAllocationStatus.OptionsColumn.AllowEdit = false;
            this.colAllocationStatus.OptionsColumn.AllowFocus = false;
            this.colAllocationStatus.OptionsColumn.ReadOnly = true;
            this.colAllocationStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAllocationStatus.Visible = true;
            this.colAllocationStatus.VisibleIndex = 3;
            this.colAllocationStatus.Width = 102;
            // 
            // colAllocationDate
            // 
            this.colAllocationDate.FieldName = "AllocationDate";
            this.colAllocationDate.Name = "colAllocationDate";
            this.colAllocationDate.OptionsColumn.AllowEdit = false;
            this.colAllocationDate.OptionsColumn.AllowFocus = false;
            this.colAllocationDate.OptionsColumn.ReadOnly = true;
            this.colAllocationDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAllocationDate.Visible = true;
            this.colAllocationDate.VisibleIndex = 4;
            this.colAllocationDate.Width = 94;
            // 
            // colAllocationEndDate
            // 
            this.colAllocationEndDate.FieldName = "AllocationEndDate";
            this.colAllocationEndDate.Name = "colAllocationEndDate";
            this.colAllocationEndDate.OptionsColumn.AllowEdit = false;
            this.colAllocationEndDate.OptionsColumn.AllowFocus = false;
            this.colAllocationEndDate.OptionsColumn.ReadOnly = true;
            this.colAllocationEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAllocationEndDate.Visible = true;
            this.colAllocationEndDate.VisibleIndex = 5;
            this.colAllocationEndDate.Width = 115;
            // 
            // colNotes1
            // 
            this.colNotes1.FieldName = "Notes";
            this.colNotes1.Name = "colNotes1";
            this.colNotes1.OptionsColumn.AllowEdit = false;
            this.colNotes1.OptionsColumn.AllowFocus = false;
            this.colNotes1.OptionsColumn.ReadOnly = true;
            this.colNotes1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNotes1.Visible = true;
            this.colNotes1.VisibleIndex = 6;
            // 
            // colMode7
            // 
            this.colMode7.FieldName = "Mode";
            this.colMode7.Name = "colMode7";
            this.colMode7.OptionsColumn.AllowEdit = false;
            this.colMode7.OptionsColumn.AllowFocus = false;
            this.colMode7.OptionsColumn.ReadOnly = true;
            this.colMode7.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode7.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode7.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID7
            // 
            this.colRecordID7.FieldName = "RecordID";
            this.colRecordID7.Name = "colRecordID7";
            this.colRecordID7.OptionsColumn.AllowEdit = false;
            this.colRecordID7.OptionsColumn.AllowFocus = false;
            this.colRecordID7.OptionsColumn.ReadOnly = true;
            this.colRecordID7.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID7.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID7.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // lueKeeperNotes
            // 
            this.lueKeeperNotes.Name = "lueKeeperNotes";
            // 
            // transactionTabPage
            // 
            this.transactionTabPage.AutoScroll = true;
            this.transactionTabPage.Controls.Add(this.transactionGridControl);
            this.transactionTabPage.Name = "transactionTabPage";
            this.transactionTabPage.Size = new System.Drawing.Size(1290, 200);
            this.transactionTabPage.Text = "Transactions";
            // 
            // transactionGridControl
            // 
            this.transactionGridControl.DataSource = this.spAS11041TransactionItemBindingSource;
            this.transactionGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.transactionGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.transactionGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.transactionGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.transactionGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.transactionGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.transactionGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.transactionGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.transactionGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.transactionGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.transactionGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.transactionGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.transactionGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.transactionGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.transactionGridControl.Location = new System.Drawing.Point(0, 0);
            this.transactionGridControl.MainView = this.transactionsGridView;
            this.transactionGridControl.MenuManager = this.barManager1;
            this.transactionGridControl.Name = "transactionGridControl";
            this.transactionGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.ceOnExchequer,
            this.moneyTextEdit});
            this.transactionGridControl.Size = new System.Drawing.Size(1290, 200);
            this.transactionGridControl.TabIndex = 3;
            this.transactionGridControl.UseEmbeddedNavigator = true;
            this.transactionGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.transactionsGridView});
            // 
            // spAS11041TransactionItemBindingSource
            // 
            this.spAS11041TransactionItemBindingSource.DataMember = "sp_AS_11041_Transaction_Item";
            this.spAS11041TransactionItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // transactionsGridView
            // 
            this.transactionsGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTransactionID1,
            this.colEquipmentReference10,
            this.colEquipmentID12,
            this.colTransactionTypeID,
            this.colTransactionType,
            this.colTransactionDate,
            this.colTransactionOrderNumber,
            this.colPurchaseInvoice,
            this.colDescription1,
            this.colCompletedOnExchequer,
            this.colNetTransactionPrice,
            this.colVAT,
            this.colMode8,
            this.colRecordID8});
            this.transactionsGridView.GridControl = this.transactionGridControl;
            this.transactionsGridView.Name = "transactionsGridView";
            this.transactionsGridView.OptionsCustomization.AllowGroup = false;
            this.transactionsGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.transactionsGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.transactionsGridView.OptionsLayout.StoreAppearance = true;
            this.transactionsGridView.OptionsSelection.MultiSelect = true;
            this.transactionsGridView.OptionsView.ColumnAutoWidth = false;
            this.transactionsGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.transactionsGridView.OptionsView.ShowGroupPanel = false;
            this.transactionsGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.transactionsGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.transactionsGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.transactionsGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.transactionsGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.transactionsGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.transactionsGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colTransactionID1
            // 
            this.colTransactionID1.FieldName = "TransactionID";
            this.colTransactionID1.Name = "colTransactionID1";
            this.colTransactionID1.OptionsColumn.AllowEdit = false;
            this.colTransactionID1.OptionsColumn.AllowFocus = false;
            this.colTransactionID1.OptionsColumn.ReadOnly = true;
            this.colTransactionID1.OptionsColumn.ShowInCustomizationForm = false;
            this.colTransactionID1.OptionsColumn.ShowInExpressionEditor = false;
            this.colTransactionID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTransactionID1.Width = 92;
            // 
            // colEquipmentReference10
            // 
            this.colEquipmentReference10.FieldName = "EquipmentReference";
            this.colEquipmentReference10.Name = "colEquipmentReference10";
            this.colEquipmentReference10.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference10.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference10.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference10.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference10.Visible = true;
            this.colEquipmentReference10.VisibleIndex = 0;
            this.colEquipmentReference10.Width = 125;
            // 
            // colEquipmentID12
            // 
            this.colEquipmentID12.FieldName = "EquipmentID";
            this.colEquipmentID12.Name = "colEquipmentID12";
            this.colEquipmentID12.OptionsColumn.AllowEdit = false;
            this.colEquipmentID12.OptionsColumn.AllowFocus = false;
            this.colEquipmentID12.OptionsColumn.ReadOnly = true;
            this.colEquipmentID12.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID12.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID12.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID12.Width = 86;
            // 
            // colTransactionTypeID
            // 
            this.colTransactionTypeID.FieldName = "TransactionTypeID";
            this.colTransactionTypeID.Name = "colTransactionTypeID";
            this.colTransactionTypeID.OptionsColumn.AllowEdit = false;
            this.colTransactionTypeID.OptionsColumn.AllowFocus = false;
            this.colTransactionTypeID.OptionsColumn.ReadOnly = true;
            this.colTransactionTypeID.OptionsColumn.ShowInCustomizationForm = false;
            this.colTransactionTypeID.OptionsColumn.ShowInExpressionEditor = false;
            this.colTransactionTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTransactionTypeID.Width = 119;
            // 
            // colTransactionType
            // 
            this.colTransactionType.FieldName = "TransactionType";
            this.colTransactionType.Name = "colTransactionType";
            this.colTransactionType.OptionsColumn.AllowEdit = false;
            this.colTransactionType.OptionsColumn.AllowFocus = false;
            this.colTransactionType.OptionsColumn.ReadOnly = true;
            this.colTransactionType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTransactionType.Visible = true;
            this.colTransactionType.VisibleIndex = 1;
            this.colTransactionType.Width = 105;
            // 
            // colTransactionDate
            // 
            this.colTransactionDate.FieldName = "TransactionDate";
            this.colTransactionDate.Name = "colTransactionDate";
            this.colTransactionDate.OptionsColumn.AllowEdit = false;
            this.colTransactionDate.OptionsColumn.AllowFocus = false;
            this.colTransactionDate.OptionsColumn.ReadOnly = true;
            this.colTransactionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTransactionDate.Visible = true;
            this.colTransactionDate.VisibleIndex = 2;
            this.colTransactionDate.Width = 104;
            // 
            // colTransactionOrderNumber
            // 
            this.colTransactionOrderNumber.FieldName = "TransactionOrderNumber";
            this.colTransactionOrderNumber.Name = "colTransactionOrderNumber";
            this.colTransactionOrderNumber.OptionsColumn.AllowEdit = false;
            this.colTransactionOrderNumber.OptionsColumn.AllowFocus = false;
            this.colTransactionOrderNumber.OptionsColumn.ReadOnly = true;
            this.colTransactionOrderNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTransactionOrderNumber.Visible = true;
            this.colTransactionOrderNumber.VisibleIndex = 3;
            this.colTransactionOrderNumber.Width = 149;
            // 
            // colPurchaseInvoice
            // 
            this.colPurchaseInvoice.FieldName = "PurchaseInvoice";
            this.colPurchaseInvoice.Name = "colPurchaseInvoice";
            this.colPurchaseInvoice.OptionsColumn.AllowEdit = false;
            this.colPurchaseInvoice.OptionsColumn.AllowFocus = false;
            this.colPurchaseInvoice.OptionsColumn.ReadOnly = true;
            this.colPurchaseInvoice.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPurchaseInvoice.Visible = true;
            this.colPurchaseInvoice.VisibleIndex = 4;
            this.colPurchaseInvoice.Width = 104;
            // 
            // colDescription1
            // 
            this.colDescription1.FieldName = "Description";
            this.colDescription1.Name = "colDescription1";
            this.colDescription1.OptionsColumn.AllowEdit = false;
            this.colDescription1.OptionsColumn.AllowFocus = false;
            this.colDescription1.OptionsColumn.ReadOnly = true;
            this.colDescription1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDescription1.Visible = true;
            this.colDescription1.VisibleIndex = 5;
            // 
            // colCompletedOnExchequer
            // 
            this.colCompletedOnExchequer.FieldName = "CompletedOnExchequer";
            this.colCompletedOnExchequer.Name = "colCompletedOnExchequer";
            this.colCompletedOnExchequer.OptionsColumn.AllowEdit = false;
            this.colCompletedOnExchequer.OptionsColumn.AllowFocus = false;
            this.colCompletedOnExchequer.OptionsColumn.ReadOnly = true;
            this.colCompletedOnExchequer.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCompletedOnExchequer.Width = 144;
            // 
            // colNetTransactionPrice
            // 
            this.colNetTransactionPrice.ColumnEdit = this.moneyTextEdit;
            this.colNetTransactionPrice.FieldName = "NetTransactionPrice";
            this.colNetTransactionPrice.Name = "colNetTransactionPrice";
            this.colNetTransactionPrice.OptionsColumn.AllowEdit = false;
            this.colNetTransactionPrice.OptionsColumn.AllowFocus = false;
            this.colNetTransactionPrice.OptionsColumn.ReadOnly = true;
            this.colNetTransactionPrice.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNetTransactionPrice.Visible = true;
            this.colNetTransactionPrice.VisibleIndex = 6;
            this.colNetTransactionPrice.Width = 124;
            // 
            // moneyTextEdit
            // 
            this.moneyTextEdit.AutoHeight = false;
            this.moneyTextEdit.DisplayFormat.FormatString = "c2";
            this.moneyTextEdit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.moneyTextEdit.Name = "moneyTextEdit";
            // 
            // colVAT
            // 
            this.colVAT.ColumnEdit = this.moneyTextEdit;
            this.colVAT.FieldName = "VAT";
            this.colVAT.Name = "colVAT";
            this.colVAT.OptionsColumn.AllowEdit = false;
            this.colVAT.OptionsColumn.AllowFocus = false;
            this.colVAT.OptionsColumn.ReadOnly = true;
            this.colVAT.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colVAT.Visible = true;
            this.colVAT.VisibleIndex = 7;
            // 
            // colMode8
            // 
            this.colMode8.FieldName = "Mode";
            this.colMode8.Name = "colMode8";
            this.colMode8.OptionsColumn.AllowEdit = false;
            this.colMode8.OptionsColumn.AllowFocus = false;
            this.colMode8.OptionsColumn.ReadOnly = true;
            this.colMode8.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode8.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode8.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID8
            // 
            this.colRecordID8.FieldName = "RecordID";
            this.colRecordID8.Name = "colRecordID8";
            this.colRecordID8.OptionsColumn.AllowEdit = false;
            this.colRecordID8.OptionsColumn.AllowFocus = false;
            this.colRecordID8.OptionsColumn.ReadOnly = true;
            this.colRecordID8.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID8.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID8.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // ceOnExchequer
            // 
            this.ceOnExchequer.AutoHeight = false;
            this.ceOnExchequer.Caption = "Check";
            this.ceOnExchequer.Name = "ceOnExchequer";
            // 
            // purposeTabPage
            // 
            this.purposeTabPage.Controls.Add(this.purposeGridControl);
            this.purposeTabPage.Name = "purposeTabPage";
            this.purposeTabPage.Size = new System.Drawing.Size(1290, 200);
            this.purposeTabPage.Text = "Asset Purpose";
            // 
            // purposeGridControl
            // 
            this.purposeGridControl.DataSource = this.spAS11084PurposeItemBindingSource;
            this.purposeGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.purposeGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.purposeGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.purposeGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.purposeGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.purposeGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.purposeGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.purposeGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.purposeGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.purposeGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.purposeGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.purposeGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.purposeGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.purposeGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.purposeGridControl.Location = new System.Drawing.Point(0, 0);
            this.purposeGridControl.MainView = this.purposeGridView;
            this.purposeGridControl.MenuManager = this.barManager1;
            this.purposeGridControl.Name = "purposeGridControl";
            this.purposeGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit16,
            this.repositoryItemMemoExEdit17});
            this.purposeGridControl.Size = new System.Drawing.Size(1290, 200);
            this.purposeGridControl.TabIndex = 3;
            this.purposeGridControl.UseEmbeddedNavigator = true;
            this.purposeGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.purposeGridView});
            // 
            // spAS11084PurposeItemBindingSource
            // 
            this.spAS11084PurposeItemBindingSource.DataMember = "sp_AS_11084_Purpose_Item";
            this.spAS11084PurposeItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // purposeGridView
            // 
            this.purposeGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colEquipmentPurposeID,
            this.colEquipmentID15,
            this.colEquipmentReference13,
            this.colPurpose,
            this.colPurposeID,
            this.colMode14,
            this.colRecordID14});
            this.purposeGridView.GridControl = this.purposeGridControl;
            this.purposeGridView.Name = "purposeGridView";
            this.purposeGridView.OptionsCustomization.AllowFilter = false;
            this.purposeGridView.OptionsCustomization.AllowGroup = false;
            this.purposeGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.purposeGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.purposeGridView.OptionsLayout.StoreAppearance = true;
            this.purposeGridView.OptionsSelection.MultiSelect = true;
            this.purposeGridView.OptionsView.ColumnAutoWidth = false;
            this.purposeGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.purposeGridView.OptionsView.ShowGroupPanel = false;
            this.purposeGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.purposeGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.purposeGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.purposeGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.purposeGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.purposeGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.purposeGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colEquipmentPurposeID
            // 
            this.colEquipmentPurposeID.FieldName = "EquipmentPurposeID";
            this.colEquipmentPurposeID.Name = "colEquipmentPurposeID";
            this.colEquipmentPurposeID.OptionsColumn.AllowEdit = false;
            this.colEquipmentPurposeID.OptionsColumn.AllowFocus = false;
            this.colEquipmentPurposeID.OptionsColumn.ReadOnly = true;
            this.colEquipmentPurposeID.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentPurposeID.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentPurposeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentPurposeID.Width = 118;
            // 
            // colEquipmentID15
            // 
            this.colEquipmentID15.FieldName = "EquipmentID";
            this.colEquipmentID15.Name = "colEquipmentID15";
            this.colEquipmentID15.OptionsColumn.AllowEdit = false;
            this.colEquipmentID15.OptionsColumn.AllowFocus = false;
            this.colEquipmentID15.OptionsColumn.ReadOnly = true;
            this.colEquipmentID15.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID15.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID15.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID15.Width = 76;
            // 
            // colEquipmentReference13
            // 
            this.colEquipmentReference13.FieldName = "EquipmentReference";
            this.colEquipmentReference13.Name = "colEquipmentReference13";
            this.colEquipmentReference13.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference13.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference13.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference13.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference13.Visible = true;
            this.colEquipmentReference13.VisibleIndex = 0;
            this.colEquipmentReference13.Width = 115;
            // 
            // colPurpose
            // 
            this.colPurpose.FieldName = "Purpose";
            this.colPurpose.Name = "colPurpose";
            this.colPurpose.OptionsColumn.AllowEdit = false;
            this.colPurpose.OptionsColumn.AllowFocus = false;
            this.colPurpose.OptionsColumn.ReadOnly = true;
            this.colPurpose.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPurpose.Visible = true;
            this.colPurpose.VisibleIndex = 1;
            // 
            // colPurposeID
            // 
            this.colPurposeID.FieldName = "PurposeID";
            this.colPurposeID.Name = "colPurposeID";
            this.colPurposeID.OptionsColumn.AllowEdit = false;
            this.colPurposeID.OptionsColumn.AllowFocus = false;
            this.colPurposeID.OptionsColumn.ReadOnly = true;
            this.colPurposeID.OptionsColumn.ShowInCustomizationForm = false;
            this.colPurposeID.OptionsColumn.ShowInExpressionEditor = false;
            this.colPurposeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colMode14
            // 
            this.colMode14.FieldName = "Mode";
            this.colMode14.Name = "colMode14";
            this.colMode14.OptionsColumn.AllowEdit = false;
            this.colMode14.OptionsColumn.AllowFocus = false;
            this.colMode14.OptionsColumn.ReadOnly = true;
            this.colMode14.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode14.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode14.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID14
            // 
            this.colRecordID14.FieldName = "RecordID";
            this.colRecordID14.Name = "colRecordID14";
            this.colRecordID14.OptionsColumn.AllowEdit = false;
            this.colRecordID14.OptionsColumn.AllowFocus = false;
            this.colRecordID14.OptionsColumn.ReadOnly = true;
            this.colRecordID14.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID14.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID14.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // repositoryItemMemoExEdit16
            // 
            this.repositoryItemMemoExEdit16.AutoHeight = false;
            this.repositoryItemMemoExEdit16.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit16.Name = "repositoryItemMemoExEdit16";
            this.repositoryItemMemoExEdit16.ReadOnly = true;
            this.repositoryItemMemoExEdit16.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit16.ShowIcon = false;
            // 
            // repositoryItemMemoExEdit17
            // 
            this.repositoryItemMemoExEdit17.AutoHeight = false;
            this.repositoryItemMemoExEdit17.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit17.Name = "repositoryItemMemoExEdit17";
            this.repositoryItemMemoExEdit17.ReadOnly = true;
            this.repositoryItemMemoExEdit17.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit17.ShowIcon = false;
            // 
            // fuelCardTabPage
            // 
            this.fuelCardTabPage.Controls.Add(this.fuelCardGridControl);
            this.fuelCardTabPage.Name = "fuelCardTabPage";
            this.fuelCardTabPage.Size = new System.Drawing.Size(1290, 200);
            this.fuelCardTabPage.Text = "Fuel Card";
            // 
            // fuelCardGridControl
            // 
            this.fuelCardGridControl.DataSource = this.spAS11089FuelCardItemBindingSource;
            this.fuelCardGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fuelCardGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.fuelCardGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.fuelCardGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.fuelCardGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.fuelCardGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.fuelCardGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.fuelCardGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.fuelCardGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.fuelCardGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.fuelCardGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.fuelCardGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.fuelCardGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.fuelCardGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.fuelCardGridControl.Location = new System.Drawing.Point(0, 0);
            this.fuelCardGridControl.MainView = this.fuelCardGridView;
            this.fuelCardGridControl.MenuManager = this.barManager1;
            this.fuelCardGridControl.Name = "fuelCardGridControl";
            this.fuelCardGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit18,
            this.repositoryItemMemoExEdit19});
            this.fuelCardGridControl.Size = new System.Drawing.Size(1290, 200);
            this.fuelCardGridControl.TabIndex = 3;
            this.fuelCardGridControl.UseEmbeddedNavigator = true;
            this.fuelCardGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.fuelCardGridView});
            // 
            // spAS11089FuelCardItemBindingSource
            // 
            this.spAS11089FuelCardItemBindingSource.DataMember = "sp_AS_11089_Fuel_Card_Item";
            this.spAS11089FuelCardItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // fuelCardGridView
            // 
            this.fuelCardGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colFuelCardID,
            this.colCardNumber,
            this.colRegistrationMark,
            this.colEquipmentReference14,
            this.colEquipmentID16,
            this.colKeeper1,
            this.colKeeperAllocationID1,
            this.colExpiryDate,
            this.colSupplierReference2,
            this.colSupplierID2,
            this.colCardStatus,
            this.colCardStatusID,
            this.colNotes5,
            this.colMode15,
            this.colRecordID15});
            this.fuelCardGridView.GridControl = this.fuelCardGridControl;
            this.fuelCardGridView.Name = "fuelCardGridView";
            this.fuelCardGridView.OptionsCustomization.AllowFilter = false;
            this.fuelCardGridView.OptionsCustomization.AllowGroup = false;
            this.fuelCardGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.fuelCardGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.fuelCardGridView.OptionsLayout.StoreAppearance = true;
            this.fuelCardGridView.OptionsSelection.MultiSelect = true;
            this.fuelCardGridView.OptionsView.ColumnAutoWidth = false;
            this.fuelCardGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.fuelCardGridView.OptionsView.ShowGroupPanel = false;
            this.fuelCardGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.fuelCardGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.fuelCardGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.fuelCardGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.fuelCardGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.fuelCardGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.fuelCardGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colFuelCardID
            // 
            this.colFuelCardID.FieldName = "FuelCardID";
            this.colFuelCardID.Name = "colFuelCardID";
            this.colFuelCardID.OptionsColumn.AllowEdit = false;
            this.colFuelCardID.OptionsColumn.AllowFocus = false;
            this.colFuelCardID.OptionsColumn.ReadOnly = true;
            this.colFuelCardID.OptionsColumn.ShowInCustomizationForm = false;
            this.colFuelCardID.OptionsColumn.ShowInExpressionEditor = false;
            this.colFuelCardID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colCardNumber
            // 
            this.colCardNumber.FieldName = "CardNumber";
            this.colCardNumber.Name = "colCardNumber";
            this.colCardNumber.OptionsColumn.AllowEdit = false;
            this.colCardNumber.OptionsColumn.AllowFocus = false;
            this.colCardNumber.OptionsColumn.ReadOnly = true;
            this.colCardNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCardNumber.Visible = true;
            this.colCardNumber.VisibleIndex = 1;
            // 
            // colRegistrationMark
            // 
            this.colRegistrationMark.FieldName = "Registration Mark";
            this.colRegistrationMark.Name = "colRegistrationMark";
            this.colRegistrationMark.OptionsColumn.AllowEdit = false;
            this.colRegistrationMark.OptionsColumn.AllowFocus = false;
            this.colRegistrationMark.OptionsColumn.ReadOnly = true;
            this.colRegistrationMark.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRegistrationMark.Visible = true;
            this.colRegistrationMark.VisibleIndex = 2;
            this.colRegistrationMark.Width = 96;
            // 
            // colEquipmentReference14
            // 
            this.colEquipmentReference14.FieldName = "EquipmentReference";
            this.colEquipmentReference14.Name = "colEquipmentReference14";
            this.colEquipmentReference14.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference14.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference14.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference14.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference14.Visible = true;
            this.colEquipmentReference14.VisibleIndex = 0;
            this.colEquipmentReference14.Width = 115;
            // 
            // colEquipmentID16
            // 
            this.colEquipmentID16.FieldName = "EquipmentID";
            this.colEquipmentID16.Name = "colEquipmentID16";
            this.colEquipmentID16.OptionsColumn.AllowEdit = false;
            this.colEquipmentID16.OptionsColumn.AllowFocus = false;
            this.colEquipmentID16.OptionsColumn.ReadOnly = true;
            this.colEquipmentID16.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID16.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID16.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID16.Width = 76;
            // 
            // colKeeper1
            // 
            this.colKeeper1.FieldName = "Keeper";
            this.colKeeper1.Name = "colKeeper1";
            this.colKeeper1.OptionsColumn.AllowEdit = false;
            this.colKeeper1.OptionsColumn.AllowFocus = false;
            this.colKeeper1.OptionsColumn.ReadOnly = true;
            this.colKeeper1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colKeeper1.Visible = true;
            this.colKeeper1.VisibleIndex = 3;
            // 
            // colKeeperAllocationID1
            // 
            this.colKeeperAllocationID1.FieldName = "KeeperAllocationID";
            this.colKeeperAllocationID1.Name = "colKeeperAllocationID1";
            this.colKeeperAllocationID1.OptionsColumn.AllowEdit = false;
            this.colKeeperAllocationID1.OptionsColumn.AllowFocus = false;
            this.colKeeperAllocationID1.OptionsColumn.ReadOnly = true;
            this.colKeeperAllocationID1.OptionsColumn.ShowInCustomizationForm = false;
            this.colKeeperAllocationID1.OptionsColumn.ShowInExpressionEditor = false;
            this.colKeeperAllocationID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colKeeperAllocationID1.Width = 109;
            // 
            // colExpiryDate
            // 
            this.colExpiryDate.FieldName = "ExpiryDate";
            this.colExpiryDate.Name = "colExpiryDate";
            this.colExpiryDate.OptionsColumn.AllowEdit = false;
            this.colExpiryDate.OptionsColumn.AllowFocus = false;
            this.colExpiryDate.OptionsColumn.ReadOnly = true;
            this.colExpiryDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colExpiryDate.Visible = true;
            this.colExpiryDate.VisibleIndex = 4;
            // 
            // colSupplierReference2
            // 
            this.colSupplierReference2.FieldName = "SupplierReference";
            this.colSupplierReference2.Name = "colSupplierReference2";
            this.colSupplierReference2.OptionsColumn.AllowEdit = false;
            this.colSupplierReference2.OptionsColumn.AllowFocus = false;
            this.colSupplierReference2.OptionsColumn.ReadOnly = true;
            this.colSupplierReference2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSupplierReference2.Visible = true;
            this.colSupplierReference2.VisibleIndex = 5;
            this.colSupplierReference2.Width = 103;
            // 
            // colSupplierID2
            // 
            this.colSupplierID2.FieldName = "SupplierID";
            this.colSupplierID2.Name = "colSupplierID2";
            this.colSupplierID2.OptionsColumn.AllowEdit = false;
            this.colSupplierID2.OptionsColumn.AllowFocus = false;
            this.colSupplierID2.OptionsColumn.ReadOnly = true;
            this.colSupplierID2.OptionsColumn.ShowInCustomizationForm = false;
            this.colSupplierID2.OptionsColumn.ShowInExpressionEditor = false;
            this.colSupplierID2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colCardStatus
            // 
            this.colCardStatus.FieldName = "CardStatus";
            this.colCardStatus.Name = "colCardStatus";
            this.colCardStatus.OptionsColumn.AllowEdit = false;
            this.colCardStatus.OptionsColumn.AllowFocus = false;
            this.colCardStatus.OptionsColumn.ReadOnly = true;
            this.colCardStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCardStatus.Visible = true;
            this.colCardStatus.VisibleIndex = 6;
            // 
            // colCardStatusID
            // 
            this.colCardStatusID.FieldName = "CardStatusID";
            this.colCardStatusID.Name = "colCardStatusID";
            this.colCardStatusID.OptionsColumn.AllowEdit = false;
            this.colCardStatusID.OptionsColumn.AllowFocus = false;
            this.colCardStatusID.OptionsColumn.ReadOnly = true;
            this.colCardStatusID.OptionsColumn.ShowInCustomizationForm = false;
            this.colCardStatusID.OptionsColumn.ShowInExpressionEditor = false;
            this.colCardStatusID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCardStatusID.Width = 83;
            // 
            // colNotes5
            // 
            this.colNotes5.FieldName = "Notes";
            this.colNotes5.Name = "colNotes5";
            this.colNotes5.OptionsColumn.AllowEdit = false;
            this.colNotes5.OptionsColumn.AllowFocus = false;
            this.colNotes5.OptionsColumn.ReadOnly = true;
            this.colNotes5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNotes5.Visible = true;
            this.colNotes5.VisibleIndex = 7;
            // 
            // colMode15
            // 
            this.colMode15.FieldName = "Mode";
            this.colMode15.Name = "colMode15";
            this.colMode15.OptionsColumn.AllowEdit = false;
            this.colMode15.OptionsColumn.AllowFocus = false;
            this.colMode15.OptionsColumn.ReadOnly = true;
            this.colMode15.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode15.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode15.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID15
            // 
            this.colRecordID15.FieldName = "RecordID";
            this.colRecordID15.Name = "colRecordID15";
            this.colRecordID15.OptionsColumn.AllowEdit = false;
            this.colRecordID15.OptionsColumn.AllowFocus = false;
            this.colRecordID15.OptionsColumn.ReadOnly = true;
            this.colRecordID15.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID15.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID15.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // repositoryItemMemoExEdit18
            // 
            this.repositoryItemMemoExEdit18.AutoHeight = false;
            this.repositoryItemMemoExEdit18.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit18.Name = "repositoryItemMemoExEdit18";
            this.repositoryItemMemoExEdit18.ReadOnly = true;
            this.repositoryItemMemoExEdit18.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit18.ShowIcon = false;
            // 
            // repositoryItemMemoExEdit19
            // 
            this.repositoryItemMemoExEdit19.AutoHeight = false;
            this.repositoryItemMemoExEdit19.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit19.Name = "repositoryItemMemoExEdit19";
            this.repositoryItemMemoExEdit19.ReadOnly = true;
            this.repositoryItemMemoExEdit19.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit19.ShowIcon = false;
            // 
            // trackerTabPage
            // 
            this.trackerTabPage.Controls.Add(this.trackerGridControl);
            this.trackerTabPage.Name = "trackerTabPage";
            this.trackerTabPage.Size = new System.Drawing.Size(1290, 200);
            this.trackerTabPage.Text = "Verilocation";
            // 
            // trackerGridControl
            // 
            this.trackerGridControl.DataSource = this.spAS11023TrackerListBindingSource;
            this.trackerGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trackerGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.trackerGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.trackerGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.trackerGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.trackerGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.trackerGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.trackerGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.trackerGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.trackerGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.trackerGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.trackerGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.trackerGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.trackerGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.trackerGridControl.Location = new System.Drawing.Point(0, 0);
            this.trackerGridControl.MainView = this.trackerGridView;
            this.trackerGridControl.MenuManager = this.barManager1;
            this.trackerGridControl.Name = "trackerGridControl";
            this.trackerGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit5,
            this.repositoryItemMemoExEdit10});
            this.trackerGridControl.Size = new System.Drawing.Size(1290, 200);
            this.trackerGridControl.TabIndex = 1;
            this.trackerGridControl.UseEmbeddedNavigator = true;
            this.trackerGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.trackerGridView});
            // 
            // spAS11023TrackerListBindingSource
            // 
            this.spAS11023TrackerListBindingSource.DataMember = "sp_AS_11023_Tracker_List";
            this.spAS11023TrackerListBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // trackerGridView
            // 
            this.trackerGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTrackerInformationID,
            this.colEquipmentReference6,
            this.colRegistration,
            this.colTrackerVID,
            this.colTrackerReference,
            this.colEquipmentID8,
            this.colMake1,
            this.colModel1,
            this.colOdometerMetres,
            this.colOdometerMiles,
            this.colReason,
            this.colMPG,
            this.colLatitude,
            this.colLatestStartJourney,
            this.colLatestEndJourney,
            this.colLongitude,
            this.colSpeedKPH,
            this.colSpeedMPH,
            this.colAverageSpeed,
            this.colPlaceName,
            this.colRoadName,
            this.colPostSect,
            this.colGeofenceName,
            this.colDistrict,
            this.colLastUpdate,
            this.colDriverName,
            this.colDriverMobile,
            this.colDriverEmail,
            this.colMode4,
            this.colRecordID4});
            this.trackerGridView.GridControl = this.trackerGridControl;
            this.trackerGridView.Name = "trackerGridView";
            this.trackerGridView.OptionsCustomization.AllowFilter = false;
            this.trackerGridView.OptionsCustomization.AllowGroup = false;
            this.trackerGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.trackerGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.trackerGridView.OptionsLayout.StoreAppearance = true;
            this.trackerGridView.OptionsSelection.MultiSelect = true;
            this.trackerGridView.OptionsView.ColumnAutoWidth = false;
            this.trackerGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.trackerGridView.OptionsView.ShowGroupPanel = false;
            this.trackerGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.trackerGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.trackerGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.trackerGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.trackerGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.trackerGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.trackerGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colTrackerInformationID
            // 
            this.colTrackerInformationID.FieldName = "TrackerInformationID";
            this.colTrackerInformationID.Name = "colTrackerInformationID";
            this.colTrackerInformationID.OptionsColumn.AllowEdit = false;
            this.colTrackerInformationID.OptionsColumn.AllowFocus = false;
            this.colTrackerInformationID.OptionsColumn.ReadOnly = true;
            this.colTrackerInformationID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTrackerInformationID.Width = 121;
            // 
            // colEquipmentReference6
            // 
            this.colEquipmentReference6.FieldName = "EquipmentReference";
            this.colEquipmentReference6.Name = "colEquipmentReference6";
            this.colEquipmentReference6.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference6.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference6.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference6.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference6.Visible = true;
            this.colEquipmentReference6.VisibleIndex = 0;
            this.colEquipmentReference6.Width = 115;
            // 
            // colRegistration
            // 
            this.colRegistration.FieldName = "Registration";
            this.colRegistration.Name = "colRegistration";
            this.colRegistration.OptionsColumn.AllowEdit = false;
            this.colRegistration.OptionsColumn.AllowFocus = false;
            this.colRegistration.OptionsColumn.ReadOnly = true;
            this.colRegistration.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRegistration.Visible = true;
            this.colRegistration.VisibleIndex = 1;
            // 
            // colTrackerVID
            // 
            this.colTrackerVID.FieldName = "TrackerVID";
            this.colTrackerVID.Name = "colTrackerVID";
            this.colTrackerVID.OptionsColumn.AllowEdit = false;
            this.colTrackerVID.OptionsColumn.AllowFocus = false;
            this.colTrackerVID.OptionsColumn.ReadOnly = true;
            this.colTrackerVID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTrackerVID.Visible = true;
            this.colTrackerVID.VisibleIndex = 2;
            // 
            // colTrackerReference
            // 
            this.colTrackerReference.FieldName = "TrackerReference";
            this.colTrackerReference.Name = "colTrackerReference";
            this.colTrackerReference.OptionsColumn.AllowEdit = false;
            this.colTrackerReference.OptionsColumn.AllowFocus = false;
            this.colTrackerReference.OptionsColumn.ReadOnly = true;
            this.colTrackerReference.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTrackerReference.Visible = true;
            this.colTrackerReference.VisibleIndex = 3;
            this.colTrackerReference.Width = 162;
            // 
            // colEquipmentID8
            // 
            this.colEquipmentID8.FieldName = "EquipmentID";
            this.colEquipmentID8.Name = "colEquipmentID8";
            this.colEquipmentID8.OptionsColumn.AllowEdit = false;
            this.colEquipmentID8.OptionsColumn.AllowFocus = false;
            this.colEquipmentID8.OptionsColumn.ReadOnly = true;
            this.colEquipmentID8.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID8.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID8.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID8.Width = 76;
            // 
            // colMake1
            // 
            this.colMake1.FieldName = "Make";
            this.colMake1.Name = "colMake1";
            this.colMake1.OptionsColumn.AllowEdit = false;
            this.colMake1.OptionsColumn.AllowFocus = false;
            this.colMake1.OptionsColumn.ReadOnly = true;
            this.colMake1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colMake1.Visible = true;
            this.colMake1.VisibleIndex = 5;
            // 
            // colModel1
            // 
            this.colModel1.FieldName = "Model";
            this.colModel1.Name = "colModel1";
            this.colModel1.OptionsColumn.AllowEdit = false;
            this.colModel1.OptionsColumn.AllowFocus = false;
            this.colModel1.OptionsColumn.ReadOnly = true;
            this.colModel1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colModel1.Visible = true;
            this.colModel1.VisibleIndex = 6;
            // 
            // colOdometerMetres
            // 
            this.colOdometerMetres.FieldName = "OdometerMetres";
            this.colOdometerMetres.Name = "colOdometerMetres";
            this.colOdometerMetres.OptionsColumn.AllowEdit = false;
            this.colOdometerMetres.OptionsColumn.AllowFocus = false;
            this.colOdometerMetres.OptionsColumn.ReadOnly = true;
            this.colOdometerMetres.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colOdometerMetres.Visible = true;
            this.colOdometerMetres.VisibleIndex = 7;
            this.colOdometerMetres.Width = 96;
            // 
            // colOdometerMiles
            // 
            this.colOdometerMiles.FieldName = "OdometerMiles";
            this.colOdometerMiles.Name = "colOdometerMiles";
            this.colOdometerMiles.OptionsColumn.AllowEdit = false;
            this.colOdometerMiles.OptionsColumn.AllowFocus = false;
            this.colOdometerMiles.OptionsColumn.ReadOnly = true;
            this.colOdometerMiles.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colOdometerMiles.Visible = true;
            this.colOdometerMiles.VisibleIndex = 8;
            this.colOdometerMiles.Width = 86;
            // 
            // colReason
            // 
            this.colReason.FieldName = "Reason";
            this.colReason.Name = "colReason";
            this.colReason.OptionsColumn.AllowEdit = false;
            this.colReason.OptionsColumn.AllowFocus = false;
            this.colReason.OptionsColumn.ReadOnly = true;
            this.colReason.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colReason.Visible = true;
            this.colReason.VisibleIndex = 9;
            // 
            // colMPG
            // 
            this.colMPG.FieldName = "MPG";
            this.colMPG.Name = "colMPG";
            this.colMPG.OptionsColumn.AllowEdit = false;
            this.colMPG.OptionsColumn.AllowFocus = false;
            this.colMPG.OptionsColumn.ReadOnly = true;
            this.colMPG.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colMPG.Visible = true;
            this.colMPG.VisibleIndex = 10;
            // 
            // colLatitude
            // 
            this.colLatitude.FieldName = "Latitude";
            this.colLatitude.Name = "colLatitude";
            this.colLatitude.OptionsColumn.AllowEdit = false;
            this.colLatitude.OptionsColumn.AllowFocus = false;
            this.colLatitude.OptionsColumn.ReadOnly = true;
            this.colLatitude.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLatitude.Visible = true;
            this.colLatitude.VisibleIndex = 11;
            // 
            // colLatestStartJourney
            // 
            this.colLatestStartJourney.FieldName = "LatestStartJourney";
            this.colLatestStartJourney.Name = "colLatestStartJourney";
            this.colLatestStartJourney.OptionsColumn.AllowEdit = false;
            this.colLatestStartJourney.OptionsColumn.AllowFocus = false;
            this.colLatestStartJourney.OptionsColumn.ShowInCustomizationForm = false;
            this.colLatestStartJourney.OptionsColumn.ShowInExpressionEditor = false;
            this.colLatestStartJourney.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLatestStartJourney.Width = 166;
            // 
            // colLatestEndJourney
            // 
            this.colLatestEndJourney.FieldName = "LatestEndJourney";
            this.colLatestEndJourney.Name = "colLatestEndJourney";
            this.colLatestEndJourney.OptionsColumn.AllowEdit = false;
            this.colLatestEndJourney.OptionsColumn.AllowFocus = false;
            this.colLatestEndJourney.OptionsColumn.ShowInCustomizationForm = false;
            this.colLatestEndJourney.OptionsColumn.ShowInExpressionEditor = false;
            this.colLatestEndJourney.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLatestEndJourney.Width = 160;
            // 
            // colLongitude
            // 
            this.colLongitude.FieldName = "Longitude";
            this.colLongitude.Name = "colLongitude";
            this.colLongitude.OptionsColumn.AllowEdit = false;
            this.colLongitude.OptionsColumn.AllowFocus = false;
            this.colLongitude.OptionsColumn.ReadOnly = true;
            this.colLongitude.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLongitude.Visible = true;
            this.colLongitude.VisibleIndex = 12;
            // 
            // colSpeedKPH
            // 
            this.colSpeedKPH.FieldName = "SpeedKPH";
            this.colSpeedKPH.Name = "colSpeedKPH";
            this.colSpeedKPH.OptionsColumn.AllowEdit = false;
            this.colSpeedKPH.OptionsColumn.AllowFocus = false;
            this.colSpeedKPH.OptionsColumn.ReadOnly = true;
            this.colSpeedKPH.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSpeedKPH.Visible = true;
            this.colSpeedKPH.VisibleIndex = 13;
            // 
            // colSpeedMPH
            // 
            this.colSpeedMPH.FieldName = "SpeedMPH";
            this.colSpeedMPH.Name = "colSpeedMPH";
            this.colSpeedMPH.OptionsColumn.AllowEdit = false;
            this.colSpeedMPH.OptionsColumn.AllowFocus = false;
            this.colSpeedMPH.OptionsColumn.ReadOnly = true;
            this.colSpeedMPH.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSpeedMPH.Visible = true;
            this.colSpeedMPH.VisibleIndex = 14;
            // 
            // colAverageSpeed
            // 
            this.colAverageSpeed.FieldName = "AverageSpeed";
            this.colAverageSpeed.Name = "colAverageSpeed";
            this.colAverageSpeed.OptionsColumn.AllowEdit = false;
            this.colAverageSpeed.OptionsColumn.AllowFocus = false;
            this.colAverageSpeed.OptionsColumn.ReadOnly = true;
            this.colAverageSpeed.OptionsColumn.ShowInCustomizationForm = false;
            this.colAverageSpeed.OptionsColumn.ShowInExpressionEditor = false;
            this.colAverageSpeed.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAverageSpeed.Width = 86;
            // 
            // colPlaceName
            // 
            this.colPlaceName.FieldName = "PlaceName";
            this.colPlaceName.Name = "colPlaceName";
            this.colPlaceName.OptionsColumn.AllowEdit = false;
            this.colPlaceName.OptionsColumn.AllowFocus = false;
            this.colPlaceName.OptionsColumn.ReadOnly = true;
            this.colPlaceName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPlaceName.Visible = true;
            this.colPlaceName.VisibleIndex = 15;
            // 
            // colRoadName
            // 
            this.colRoadName.FieldName = "RoadName";
            this.colRoadName.Name = "colRoadName";
            this.colRoadName.OptionsColumn.AllowEdit = false;
            this.colRoadName.OptionsColumn.AllowFocus = false;
            this.colRoadName.OptionsColumn.ReadOnly = true;
            this.colRoadName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRoadName.Visible = true;
            this.colRoadName.VisibleIndex = 16;
            // 
            // colPostSect
            // 
            this.colPostSect.FieldName = "PostSect";
            this.colPostSect.Name = "colPostSect";
            this.colPostSect.OptionsColumn.AllowEdit = false;
            this.colPostSect.OptionsColumn.AllowFocus = false;
            this.colPostSect.OptionsColumn.ReadOnly = true;
            this.colPostSect.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPostSect.Visible = true;
            this.colPostSect.VisibleIndex = 17;
            // 
            // colGeofenceName
            // 
            this.colGeofenceName.FieldName = "GeofenceName";
            this.colGeofenceName.Name = "colGeofenceName";
            this.colGeofenceName.OptionsColumn.AllowEdit = false;
            this.colGeofenceName.OptionsColumn.AllowFocus = false;
            this.colGeofenceName.OptionsColumn.ReadOnly = true;
            this.colGeofenceName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colGeofenceName.Visible = true;
            this.colGeofenceName.VisibleIndex = 18;
            this.colGeofenceName.Width = 88;
            // 
            // colDistrict
            // 
            this.colDistrict.FieldName = "District";
            this.colDistrict.Name = "colDistrict";
            this.colDistrict.OptionsColumn.AllowEdit = false;
            this.colDistrict.OptionsColumn.AllowFocus = false;
            this.colDistrict.OptionsColumn.ReadOnly = true;
            this.colDistrict.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDistrict.Visible = true;
            this.colDistrict.VisibleIndex = 19;
            // 
            // colLastUpdate
            // 
            this.colLastUpdate.FieldName = "LastUpdate";
            this.colLastUpdate.Name = "colLastUpdate";
            this.colLastUpdate.OptionsColumn.AllowEdit = false;
            this.colLastUpdate.OptionsColumn.AllowFocus = false;
            this.colLastUpdate.OptionsColumn.ReadOnly = true;
            this.colLastUpdate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLastUpdate.Visible = true;
            this.colLastUpdate.VisibleIndex = 20;
            // 
            // colDriverName
            // 
            this.colDriverName.FieldName = "DriverName";
            this.colDriverName.Name = "colDriverName";
            this.colDriverName.OptionsColumn.AllowEdit = false;
            this.colDriverName.OptionsColumn.AllowFocus = false;
            this.colDriverName.OptionsColumn.ReadOnly = true;
            this.colDriverName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDriverName.Visible = true;
            this.colDriverName.VisibleIndex = 4;
            // 
            // colDriverMobile
            // 
            this.colDriverMobile.FieldName = "DriverMobile";
            this.colDriverMobile.Name = "colDriverMobile";
            this.colDriverMobile.OptionsColumn.AllowEdit = false;
            this.colDriverMobile.OptionsColumn.AllowFocus = false;
            this.colDriverMobile.OptionsColumn.ReadOnly = true;
            this.colDriverMobile.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDriverMobile.Visible = true;
            this.colDriverMobile.VisibleIndex = 21;
            // 
            // colDriverEmail
            // 
            this.colDriverEmail.FieldName = "DriverEmail";
            this.colDriverEmail.Name = "colDriverEmail";
            this.colDriverEmail.OptionsColumn.AllowEdit = false;
            this.colDriverEmail.OptionsColumn.AllowFocus = false;
            this.colDriverEmail.OptionsColumn.ReadOnly = true;
            this.colDriverEmail.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDriverEmail.Visible = true;
            this.colDriverEmail.VisibleIndex = 22;
            // 
            // colMode4
            // 
            this.colMode4.FieldName = "Mode";
            this.colMode4.Name = "colMode4";
            this.colMode4.OptionsColumn.AllowEdit = false;
            this.colMode4.OptionsColumn.AllowFocus = false;
            this.colMode4.OptionsColumn.ReadOnly = true;
            this.colMode4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID4
            // 
            this.colRecordID4.FieldName = "RecordID";
            this.colRecordID4.Name = "colRecordID4";
            this.colRecordID4.OptionsColumn.AllowEdit = false;
            this.colRecordID4.OptionsColumn.AllowFocus = false;
            this.colRecordID4.OptionsColumn.ReadOnly = true;
            this.colRecordID4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ReadOnly = true;
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // repositoryItemMemoExEdit10
            // 
            this.repositoryItemMemoExEdit10.AutoHeight = false;
            this.repositoryItemMemoExEdit10.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit10.Name = "repositoryItemMemoExEdit10";
            this.repositoryItemMemoExEdit10.ReadOnly = true;
            this.repositoryItemMemoExEdit10.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit10.ShowIcon = false;
            // 
            // billingTabPage
            // 
            this.billingTabPage.AutoScroll = true;
            this.billingTabPage.Controls.Add(this.billingGridControl);
            this.billingTabPage.Name = "billingTabPage";
            this.billingTabPage.Size = new System.Drawing.Size(1290, 200);
            this.billingTabPage.Text = "Billing Details";
            // 
            // billingGridControl
            // 
            this.billingGridControl.DataSource = this.spAS11050DepreciationItemBindingSource;
            this.billingGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.billingGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.billingGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.billingGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.billingGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.billingGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.billingGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.billingGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.billingGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.billingGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.billingGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.billingGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.billingGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, -1, true, false, "", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, -1, true, false, "", "delete")});
            this.billingGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.billingGridControl.Location = new System.Drawing.Point(0, 0);
            this.billingGridControl.MainView = this.billingGridView;
            this.billingGridControl.MenuManager = this.barManager1;
            this.billingGridControl.Name = "billingGridControl";
            this.billingGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.moneyTextEdit3});
            this.billingGridControl.Size = new System.Drawing.Size(1290, 200);
            this.billingGridControl.TabIndex = 4;
            this.billingGridControl.UseEmbeddedNavigator = true;
            this.billingGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.billingGridView});
            // 
            // spAS11050DepreciationItemBindingSource
            // 
            this.spAS11050DepreciationItemBindingSource.DataMember = "sp_AS_11050_Depreciation_Item";
            this.spAS11050DepreciationItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // billingGridView
            // 
            this.billingGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDepreciationID1,
            this.colEquipmentID1,
            this.colEquipmentReference,
            this.colNarrative1,
            this.colBalanceSheetCode1,
            this.colProfitLossCode1,
            this.colBillingCentreCodeID1,
            this.colCompanyCode1,
            this.colDepartmentCode1,
            this.colCostCentreCode1,
            this.colDepreciationAmount1,
            this.colPreviousValue1,
            this.colCurrentValue1,
            this.colPeriodNumber1,
            this.colPeriodStartDate1,
            this.colPeriodEndDate1,
            this.colRemarks1,
            this.colAllowEdit1,
            this.colMode9,
            this.colRecordID9});
            this.billingGridView.GridControl = this.billingGridControl;
            this.billingGridView.Name = "billingGridView";
            this.billingGridView.OptionsCustomization.AllowGroup = false;
            this.billingGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.billingGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.billingGridView.OptionsLayout.StoreAppearance = true;
            this.billingGridView.OptionsSelection.MultiSelect = true;
            this.billingGridView.OptionsView.ColumnAutoWidth = false;
            this.billingGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.billingGridView.OptionsView.ShowGroupPanel = false;
            this.billingGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.billingGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.billingGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.billingGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.billingGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.billingGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.billingGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colDepreciationID1
            // 
            this.colDepreciationID1.FieldName = "DepreciationID";
            this.colDepreciationID1.Name = "colDepreciationID1";
            this.colDepreciationID1.OptionsColumn.AllowEdit = false;
            this.colDepreciationID1.OptionsColumn.AllowFocus = false;
            this.colDepreciationID1.OptionsColumn.ReadOnly = true;
            this.colDepreciationID1.OptionsColumn.ShowInCustomizationForm = false;
            this.colDepreciationID1.OptionsColumn.ShowInExpressionEditor = false;
            this.colDepreciationID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colEquipmentID1
            // 
            this.colEquipmentID1.FieldName = "EquipmentID";
            this.colEquipmentID1.Name = "colEquipmentID1";
            this.colEquipmentID1.OptionsColumn.AllowEdit = false;
            this.colEquipmentID1.OptionsColumn.AllowFocus = false;
            this.colEquipmentID1.OptionsColumn.ReadOnly = true;
            this.colEquipmentID1.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID1.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID1.Width = 86;
            // 
            // colEquipmentReference
            // 
            this.colEquipmentReference.FieldName = "EquipmentReference";
            this.colEquipmentReference.Name = "colEquipmentReference";
            this.colEquipmentReference.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference.Visible = true;
            this.colEquipmentReference.VisibleIndex = 0;
            this.colEquipmentReference.Width = 125;
            // 
            // colNarrative1
            // 
            this.colNarrative1.FieldName = "Narrative";
            this.colNarrative1.Name = "colNarrative1";
            this.colNarrative1.OptionsColumn.AllowEdit = false;
            this.colNarrative1.OptionsColumn.AllowFocus = false;
            this.colNarrative1.OptionsColumn.ReadOnly = true;
            this.colNarrative1.OptionsColumn.ShowInCustomizationForm = false;
            this.colNarrative1.OptionsColumn.ShowInExpressionEditor = false;
            this.colNarrative1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colBalanceSheetCode1
            // 
            this.colBalanceSheetCode1.FieldName = "BalanceSheetCode";
            this.colBalanceSheetCode1.Name = "colBalanceSheetCode1";
            this.colBalanceSheetCode1.OptionsColumn.AllowEdit = false;
            this.colBalanceSheetCode1.OptionsColumn.AllowFocus = false;
            this.colBalanceSheetCode1.OptionsColumn.ReadOnly = true;
            this.colBalanceSheetCode1.OptionsColumn.ShowInCustomizationForm = false;
            this.colBalanceSheetCode1.OptionsColumn.ShowInExpressionEditor = false;
            this.colBalanceSheetCode1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colBalanceSheetCode1.Width = 118;
            // 
            // colProfitLossCode1
            // 
            this.colProfitLossCode1.FieldName = "ProfitLossCode";
            this.colProfitLossCode1.Name = "colProfitLossCode1";
            this.colProfitLossCode1.OptionsColumn.AllowEdit = false;
            this.colProfitLossCode1.OptionsColumn.AllowFocus = false;
            this.colProfitLossCode1.OptionsColumn.ReadOnly = true;
            this.colProfitLossCode1.OptionsColumn.ShowInCustomizationForm = false;
            this.colProfitLossCode1.OptionsColumn.ShowInExpressionEditor = false;
            this.colProfitLossCode1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colProfitLossCode1.Width = 100;
            // 
            // colBillingCentreCodeID1
            // 
            this.colBillingCentreCodeID1.FieldName = "BillingCentreCodeID";
            this.colBillingCentreCodeID1.Name = "colBillingCentreCodeID1";
            this.colBillingCentreCodeID1.OptionsColumn.AllowEdit = false;
            this.colBillingCentreCodeID1.OptionsColumn.AllowFocus = false;
            this.colBillingCentreCodeID1.OptionsColumn.ReadOnly = true;
            this.colBillingCentreCodeID1.OptionsColumn.ShowInCustomizationForm = false;
            this.colBillingCentreCodeID1.OptionsColumn.ShowInExpressionEditor = false;
            this.colBillingCentreCodeID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colBillingCentreCodeID1.Width = 126;
            // 
            // colCompanyCode1
            // 
            this.colCompanyCode1.FieldName = "CompanyCode";
            this.colCompanyCode1.Name = "colCompanyCode1";
            this.colCompanyCode1.OptionsColumn.AllowEdit = false;
            this.colCompanyCode1.OptionsColumn.AllowFocus = false;
            this.colCompanyCode1.OptionsColumn.ReadOnly = true;
            this.colCompanyCode1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCompanyCode1.Visible = true;
            this.colCompanyCode1.VisibleIndex = 1;
            this.colCompanyCode1.Width = 95;
            // 
            // colDepartmentCode1
            // 
            this.colDepartmentCode1.FieldName = "DepartmentCode";
            this.colDepartmentCode1.Name = "colDepartmentCode1";
            this.colDepartmentCode1.OptionsColumn.AllowEdit = false;
            this.colDepartmentCode1.OptionsColumn.AllowFocus = false;
            this.colDepartmentCode1.OptionsColumn.ReadOnly = true;
            this.colDepartmentCode1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDepartmentCode1.Visible = true;
            this.colDepartmentCode1.VisibleIndex = 2;
            this.colDepartmentCode1.Width = 107;
            // 
            // colCostCentreCode1
            // 
            this.colCostCentreCode1.FieldName = "CostCentreCode";
            this.colCostCentreCode1.Name = "colCostCentreCode1";
            this.colCostCentreCode1.OptionsColumn.AllowEdit = false;
            this.colCostCentreCode1.OptionsColumn.AllowFocus = false;
            this.colCostCentreCode1.OptionsColumn.ReadOnly = true;
            this.colCostCentreCode1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCostCentreCode1.Visible = true;
            this.colCostCentreCode1.VisibleIndex = 3;
            this.colCostCentreCode1.Width = 108;
            // 
            // colDepreciationAmount1
            // 
            this.colDepreciationAmount1.ColumnEdit = this.moneyTextEdit3;
            this.colDepreciationAmount1.FieldName = "DepreciationAmount";
            this.colDepreciationAmount1.Name = "colDepreciationAmount1";
            this.colDepreciationAmount1.OptionsColumn.AllowEdit = false;
            this.colDepreciationAmount1.OptionsColumn.AllowFocus = false;
            this.colDepreciationAmount1.OptionsColumn.ReadOnly = true;
            this.colDepreciationAmount1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDepreciationAmount1.Visible = true;
            this.colDepreciationAmount1.VisibleIndex = 4;
            this.colDepreciationAmount1.Width = 122;
            // 
            // moneyTextEdit3
            // 
            this.moneyTextEdit3.AutoHeight = false;
            this.moneyTextEdit3.DisplayFormat.FormatString = "c2";
            this.moneyTextEdit3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.moneyTextEdit3.Name = "moneyTextEdit3";
            // 
            // colPreviousValue1
            // 
            this.colPreviousValue1.FieldName = "PreviousValue";
            this.colPreviousValue1.Name = "colPreviousValue1";
            this.colPreviousValue1.OptionsColumn.AllowEdit = false;
            this.colPreviousValue1.OptionsColumn.AllowFocus = false;
            this.colPreviousValue1.OptionsColumn.ReadOnly = true;
            this.colPreviousValue1.OptionsColumn.ShowInCustomizationForm = false;
            this.colPreviousValue1.OptionsColumn.ShowInExpressionEditor = false;
            this.colPreviousValue1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPreviousValue1.Width = 92;
            // 
            // colCurrentValue1
            // 
            this.colCurrentValue1.FieldName = "CurrentValue";
            this.colCurrentValue1.Name = "colCurrentValue1";
            this.colCurrentValue1.OptionsColumn.AllowEdit = false;
            this.colCurrentValue1.OptionsColumn.AllowFocus = false;
            this.colCurrentValue1.OptionsColumn.ReadOnly = true;
            this.colCurrentValue1.OptionsColumn.ShowInCustomizationForm = false;
            this.colCurrentValue1.OptionsColumn.ShowInExpressionEditor = false;
            this.colCurrentValue1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCurrentValue1.Width = 88;
            // 
            // colPeriodNumber1
            // 
            this.colPeriodNumber1.FieldName = "PeriodNumber";
            this.colPeriodNumber1.Name = "colPeriodNumber1";
            this.colPeriodNumber1.OptionsColumn.AllowEdit = false;
            this.colPeriodNumber1.OptionsColumn.AllowFocus = false;
            this.colPeriodNumber1.OptionsColumn.ReadOnly = true;
            this.colPeriodNumber1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPeriodNumber1.Visible = true;
            this.colPeriodNumber1.VisibleIndex = 5;
            this.colPeriodNumber1.Width = 92;
            // 
            // colPeriodStartDate1
            // 
            this.colPeriodStartDate1.FieldName = "PeriodStartDate";
            this.colPeriodStartDate1.Name = "colPeriodStartDate1";
            this.colPeriodStartDate1.OptionsColumn.AllowEdit = false;
            this.colPeriodStartDate1.OptionsColumn.AllowFocus = false;
            this.colPeriodStartDate1.OptionsColumn.ReadOnly = true;
            this.colPeriodStartDate1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPeriodStartDate1.Visible = true;
            this.colPeriodStartDate1.VisibleIndex = 6;
            this.colPeriodStartDate1.Width = 105;
            // 
            // colPeriodEndDate1
            // 
            this.colPeriodEndDate1.FieldName = "PeriodEndDate";
            this.colPeriodEndDate1.Name = "colPeriodEndDate1";
            this.colPeriodEndDate1.OptionsColumn.AllowEdit = false;
            this.colPeriodEndDate1.OptionsColumn.AllowFocus = false;
            this.colPeriodEndDate1.OptionsColumn.ReadOnly = true;
            this.colPeriodEndDate1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPeriodEndDate1.Visible = true;
            this.colPeriodEndDate1.VisibleIndex = 7;
            this.colPeriodEndDate1.Width = 99;
            // 
            // colRemarks1
            // 
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.AllowEdit = false;
            this.colRemarks1.OptionsColumn.AllowFocus = false;
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.OptionsColumn.ShowInCustomizationForm = false;
            this.colRemarks1.OptionsColumn.ShowInExpressionEditor = false;
            this.colRemarks1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colAllowEdit1
            // 
            this.colAllowEdit1.FieldName = "AllowEdit";
            this.colAllowEdit1.Name = "colAllowEdit1";
            this.colAllowEdit1.OptionsColumn.AllowEdit = false;
            this.colAllowEdit1.OptionsColumn.AllowFocus = false;
            this.colAllowEdit1.OptionsColumn.ReadOnly = true;
            this.colAllowEdit1.OptionsColumn.ShowInCustomizationForm = false;
            this.colAllowEdit1.OptionsColumn.ShowInExpressionEditor = false;
            this.colAllowEdit1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colMode9
            // 
            this.colMode9.FieldName = "Mode";
            this.colMode9.Name = "colMode9";
            this.colMode9.OptionsColumn.AllowEdit = false;
            this.colMode9.OptionsColumn.AllowFocus = false;
            this.colMode9.OptionsColumn.ReadOnly = true;
            this.colMode9.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode9.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode9.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID9
            // 
            this.colRecordID9.FieldName = "RecordID";
            this.colRecordID9.Name = "colRecordID9";
            this.colRecordID9.OptionsColumn.AllowEdit = false;
            this.colRecordID9.OptionsColumn.AllowFocus = false;
            this.colRecordID9.OptionsColumn.ReadOnly = true;
            this.colRecordID9.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID9.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID9.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // depreciationTabPage
            // 
            this.depreciationTabPage.Controls.Add(this.depreciationGridControl);
            this.depreciationTabPage.Name = "depreciationTabPage";
            this.depreciationTabPage.Size = new System.Drawing.Size(1290, 200);
            this.depreciationTabPage.Text = "Depreciation Data";
            // 
            // depreciationGridControl
            // 
            this.depreciationGridControl.DataSource = this.spAS11050DepreciationItemBindingSource;
            this.depreciationGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.depreciationGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.depreciationGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.depreciationGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.depreciationGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.depreciationGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.depreciationGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.depreciationGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.depreciationGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.depreciationGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.depreciationGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.depreciationGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.depreciationGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.depreciationGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.depreciationGridControl.Location = new System.Drawing.Point(0, 0);
            this.depreciationGridControl.MainView = this.depreciationGridView;
            this.depreciationGridControl.MenuManager = this.barManager1;
            this.depreciationGridControl.Name = "depreciationGridControl";
            this.depreciationGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoEdit1,
            this.moneyTextEditDep});
            this.depreciationGridControl.Size = new System.Drawing.Size(1290, 200);
            this.depreciationGridControl.TabIndex = 3;
            this.depreciationGridControl.UseEmbeddedNavigator = true;
            this.depreciationGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.depreciationGridView});
            // 
            // depreciationGridView
            // 
            this.depreciationGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDepreciationID,
            this.colEquipmentID,
            this.colEquipmentReference1,
            this.colNarrative,
            this.colBalanceSheetCode,
            this.colProfitLossCode,
            this.colBillingCentreCodeID,
            this.colCompanyCode,
            this.colDepartmentCode,
            this.colCostCentreCode,
            this.colDepreciationAmount,
            this.colPreviousValue,
            this.colCurrentValue,
            this.colPeriodNumber,
            this.colPeriodStartDate,
            this.colPeriodEndDate,
            this.colRemarks,
            this.colAllowEdit,
            this.colMode,
            this.colRecordID});
            this.depreciationGridView.GridControl = this.depreciationGridControl;
            this.depreciationGridView.Name = "depreciationGridView";
            this.depreciationGridView.OptionsCustomization.AllowGroup = false;
            this.depreciationGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.depreciationGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.depreciationGridView.OptionsLayout.StoreAppearance = true;
            this.depreciationGridView.OptionsSelection.MultiSelect = true;
            this.depreciationGridView.OptionsView.ColumnAutoWidth = false;
            this.depreciationGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.depreciationGridView.OptionsView.ShowGroupPanel = false;
            this.depreciationGridView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colNarrative, DevExpress.Data.ColumnSortOrder.Descending)});
            this.depreciationGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.depreciationGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.depreciationGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.depreciationGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.depreciationGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.depreciationGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.depreciationGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colDepreciationID
            // 
            this.colDepreciationID.FieldName = "DepreciationID";
            this.colDepreciationID.Name = "colDepreciationID";
            this.colDepreciationID.OptionsColumn.AllowEdit = false;
            this.colDepreciationID.OptionsColumn.AllowFocus = false;
            this.colDepreciationID.OptionsColumn.ReadOnly = true;
            this.colDepreciationID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colEquipmentID
            // 
            this.colEquipmentID.FieldName = "EquipmentID";
            this.colEquipmentID.Name = "colEquipmentID";
            this.colEquipmentID.OptionsColumn.AllowEdit = false;
            this.colEquipmentID.OptionsColumn.AllowFocus = false;
            this.colEquipmentID.OptionsColumn.ReadOnly = true;
            this.colEquipmentID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colEquipmentReference1
            // 
            this.colEquipmentReference1.FieldName = "EquipmentReference";
            this.colEquipmentReference1.Name = "colEquipmentReference1";
            this.colEquipmentReference1.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference1.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference1.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference1.Visible = true;
            this.colEquipmentReference1.VisibleIndex = 0;
            this.colEquipmentReference1.Width = 125;
            // 
            // colNarrative
            // 
            this.colNarrative.FieldName = "Narrative";
            this.colNarrative.Name = "colNarrative";
            this.colNarrative.OptionsColumn.AllowEdit = false;
            this.colNarrative.OptionsColumn.AllowFocus = false;
            this.colNarrative.OptionsColumn.ReadOnly = true;
            this.colNarrative.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNarrative.Visible = true;
            this.colNarrative.VisibleIndex = 1;
            this.colNarrative.Width = 80;
            // 
            // colBalanceSheetCode
            // 
            this.colBalanceSheetCode.FieldName = "BalanceSheetCode";
            this.colBalanceSheetCode.Name = "colBalanceSheetCode";
            this.colBalanceSheetCode.OptionsColumn.AllowEdit = false;
            this.colBalanceSheetCode.OptionsColumn.AllowFocus = false;
            this.colBalanceSheetCode.OptionsColumn.ReadOnly = true;
            this.colBalanceSheetCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colBalanceSheetCode.Visible = true;
            this.colBalanceSheetCode.VisibleIndex = 2;
            this.colBalanceSheetCode.Width = 118;
            // 
            // colProfitLossCode
            // 
            this.colProfitLossCode.FieldName = "ProfitLossCode";
            this.colProfitLossCode.Name = "colProfitLossCode";
            this.colProfitLossCode.OptionsColumn.AllowEdit = false;
            this.colProfitLossCode.OptionsColumn.AllowFocus = false;
            this.colProfitLossCode.OptionsColumn.ReadOnly = true;
            this.colProfitLossCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colProfitLossCode.Visible = true;
            this.colProfitLossCode.VisibleIndex = 3;
            this.colProfitLossCode.Width = 100;
            // 
            // colBillingCentreCodeID
            // 
            this.colBillingCentreCodeID.FieldName = "BillingCentreCodeID";
            this.colBillingCentreCodeID.Name = "colBillingCentreCodeID";
            this.colBillingCentreCodeID.OptionsColumn.AllowEdit = false;
            this.colBillingCentreCodeID.OptionsColumn.AllowFocus = false;
            this.colBillingCentreCodeID.OptionsColumn.ReadOnly = true;
            this.colBillingCentreCodeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colBillingCentreCodeID.Width = 126;
            // 
            // colCompanyCode
            // 
            this.colCompanyCode.FieldName = "CompanyCode";
            this.colCompanyCode.Name = "colCompanyCode";
            this.colCompanyCode.OptionsColumn.AllowEdit = false;
            this.colCompanyCode.OptionsColumn.AllowFocus = false;
            this.colCompanyCode.OptionsColumn.ReadOnly = true;
            this.colCompanyCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCompanyCode.Visible = true;
            this.colCompanyCode.VisibleIndex = 4;
            this.colCompanyCode.Width = 95;
            // 
            // colDepartmentCode
            // 
            this.colDepartmentCode.FieldName = "DepartmentCode";
            this.colDepartmentCode.Name = "colDepartmentCode";
            this.colDepartmentCode.OptionsColumn.AllowEdit = false;
            this.colDepartmentCode.OptionsColumn.AllowFocus = false;
            this.colDepartmentCode.OptionsColumn.ReadOnly = true;
            this.colDepartmentCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDepartmentCode.Visible = true;
            this.colDepartmentCode.VisibleIndex = 5;
            this.colDepartmentCode.Width = 107;
            // 
            // colCostCentreCode
            // 
            this.colCostCentreCode.FieldName = "CostCentreCode";
            this.colCostCentreCode.Name = "colCostCentreCode";
            this.colCostCentreCode.OptionsColumn.AllowEdit = false;
            this.colCostCentreCode.OptionsColumn.AllowFocus = false;
            this.colCostCentreCode.OptionsColumn.ReadOnly = true;
            this.colCostCentreCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCostCentreCode.Visible = true;
            this.colCostCentreCode.VisibleIndex = 6;
            this.colCostCentreCode.Width = 108;
            // 
            // colDepreciationAmount
            // 
            this.colDepreciationAmount.ColumnEdit = this.moneyTextEditDep;
            this.colDepreciationAmount.FieldName = "DepreciationAmount";
            this.colDepreciationAmount.Name = "colDepreciationAmount";
            this.colDepreciationAmount.OptionsColumn.AllowEdit = false;
            this.colDepreciationAmount.OptionsColumn.AllowFocus = false;
            this.colDepreciationAmount.OptionsColumn.ReadOnly = true;
            this.colDepreciationAmount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDepreciationAmount.Visible = true;
            this.colDepreciationAmount.VisibleIndex = 7;
            this.colDepreciationAmount.Width = 122;
            // 
            // moneyTextEditDep
            // 
            this.moneyTextEditDep.AutoHeight = false;
            this.moneyTextEditDep.DisplayFormat.FormatString = "c2";
            this.moneyTextEditDep.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.moneyTextEditDep.Name = "moneyTextEditDep";
            // 
            // colPreviousValue
            // 
            this.colPreviousValue.ColumnEdit = this.moneyTextEditDep;
            this.colPreviousValue.FieldName = "PreviousValue";
            this.colPreviousValue.Name = "colPreviousValue";
            this.colPreviousValue.OptionsColumn.AllowEdit = false;
            this.colPreviousValue.OptionsColumn.AllowFocus = false;
            this.colPreviousValue.OptionsColumn.ReadOnly = true;
            this.colPreviousValue.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPreviousValue.Visible = true;
            this.colPreviousValue.VisibleIndex = 8;
            this.colPreviousValue.Width = 92;
            // 
            // colCurrentValue
            // 
            this.colCurrentValue.ColumnEdit = this.moneyTextEditDep;
            this.colCurrentValue.FieldName = "CurrentValue";
            this.colCurrentValue.Name = "colCurrentValue";
            this.colCurrentValue.OptionsColumn.AllowEdit = false;
            this.colCurrentValue.OptionsColumn.AllowFocus = false;
            this.colCurrentValue.OptionsColumn.ReadOnly = true;
            this.colCurrentValue.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCurrentValue.Visible = true;
            this.colCurrentValue.VisibleIndex = 9;
            this.colCurrentValue.Width = 88;
            // 
            // colPeriodNumber
            // 
            this.colPeriodNumber.FieldName = "PeriodNumber";
            this.colPeriodNumber.Name = "colPeriodNumber";
            this.colPeriodNumber.OptionsColumn.AllowEdit = false;
            this.colPeriodNumber.OptionsColumn.AllowFocus = false;
            this.colPeriodNumber.OptionsColumn.ReadOnly = true;
            this.colPeriodNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPeriodNumber.Visible = true;
            this.colPeriodNumber.VisibleIndex = 10;
            this.colPeriodNumber.Width = 92;
            // 
            // colPeriodStartDate
            // 
            this.colPeriodStartDate.FieldName = "PeriodStartDate";
            this.colPeriodStartDate.Name = "colPeriodStartDate";
            this.colPeriodStartDate.OptionsColumn.AllowEdit = false;
            this.colPeriodStartDate.OptionsColumn.AllowFocus = false;
            this.colPeriodStartDate.OptionsColumn.ReadOnly = true;
            this.colPeriodStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPeriodStartDate.Visible = true;
            this.colPeriodStartDate.VisibleIndex = 11;
            this.colPeriodStartDate.Width = 105;
            // 
            // colPeriodEndDate
            // 
            this.colPeriodEndDate.FieldName = "PeriodEndDate";
            this.colPeriodEndDate.Name = "colPeriodEndDate";
            this.colPeriodEndDate.OptionsColumn.AllowEdit = false;
            this.colPeriodEndDate.OptionsColumn.AllowFocus = false;
            this.colPeriodEndDate.OptionsColumn.ReadOnly = true;
            this.colPeriodEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPeriodEndDate.Visible = true;
            this.colPeriodEndDate.VisibleIndex = 12;
            this.colPeriodEndDate.Width = 99;
            // 
            // colRemarks
            // 
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.AllowEdit = false;
            this.colRemarks.OptionsColumn.AllowFocus = false;
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 13;
            // 
            // colAllowEdit
            // 
            this.colAllowEdit.FieldName = "AllowEdit";
            this.colAllowEdit.Name = "colAllowEdit";
            this.colAllowEdit.OptionsColumn.AllowEdit = false;
            this.colAllowEdit.OptionsColumn.AllowFocus = false;
            this.colAllowEdit.OptionsColumn.ReadOnly = true;
            this.colAllowEdit.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colMode
            // 
            this.colMode.FieldName = "Mode";
            this.colMode.Name = "colMode";
            this.colMode.OptionsColumn.AllowEdit = false;
            this.colMode.OptionsColumn.AllowFocus = false;
            this.colMode.OptionsColumn.ReadOnly = true;
            this.colMode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID
            // 
            this.colRecordID.FieldName = "RecordID";
            this.colRecordID.Name = "colRecordID";
            this.colRecordID.OptionsColumn.AllowEdit = false;
            this.colRecordID.OptionsColumn.AllowFocus = false;
            this.colRecordID.OptionsColumn.ReadOnly = true;
            this.colRecordID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            // 
            // coverTabPage
            // 
            this.coverTabPage.Controls.Add(this.coverGridControl);
            this.coverTabPage.Name = "coverTabPage";
            this.coverTabPage.Size = new System.Drawing.Size(1290, 200);
            this.coverTabPage.Text = "Policy and Insurance Cover Details";
            // 
            // coverGridControl
            // 
            this.coverGridControl.DataSource = this.spAS11075CoverItemBindingSource;
            this.coverGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.coverGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.coverGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.coverGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.coverGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.coverGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.coverGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.coverGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.coverGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.coverGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.coverGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.coverGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.coverGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.coverGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.coverGridControl.Location = new System.Drawing.Point(0, 0);
            this.coverGridControl.MainView = this.coverGridView;
            this.coverGridControl.MenuManager = this.barManager1;
            this.coverGridControl.Name = "coverGridControl";
            this.coverGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit9,
            this.moneyTextEdit1});
            this.coverGridControl.Size = new System.Drawing.Size(1290, 200);
            this.coverGridControl.TabIndex = 1;
            this.coverGridControl.UseEmbeddedNavigator = true;
            this.coverGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.coverGridView});
            // 
            // spAS11075CoverItemBindingSource
            // 
            this.spAS11075CoverItemBindingSource.DataMember = "sp_AS_11075_Cover_Item";
            this.spAS11075CoverItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // coverGridView
            // 
            this.coverGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCoverID,
            this.colEquipmentReference16,
            this.colEquipmentID3,
            this.colCoverType,
            this.colCoverTypeID,
            this.colSupplierID1,
            this.colSupplierReference1,
            this.colPolicy,
            this.colAnnualCoverCost,
            this.colRenewalDate,
            this.colNotes3,
            this.colExcess,
            this.colMode11,
            this.colRecordID11});
            this.coverGridView.GridControl = this.coverGridControl;
            this.coverGridView.Name = "coverGridView";
            this.coverGridView.OptionsCustomization.AllowFilter = false;
            this.coverGridView.OptionsCustomization.AllowGroup = false;
            this.coverGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.coverGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.coverGridView.OptionsLayout.StoreAppearance = true;
            this.coverGridView.OptionsSelection.MultiSelect = true;
            this.coverGridView.OptionsView.ColumnAutoWidth = false;
            this.coverGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.coverGridView.OptionsView.ShowGroupPanel = false;
            this.coverGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.coverGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.coverGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.coverGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.coverGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.coverGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.coverGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colCoverID
            // 
            this.colCoverID.ColumnEdit = this.repositoryItemMemoExEdit9;
            this.colCoverID.FieldName = "CoverID";
            this.colCoverID.Name = "colCoverID";
            this.colCoverID.OptionsColumn.AllowEdit = false;
            this.colCoverID.OptionsColumn.AllowFocus = false;
            this.colCoverID.OptionsColumn.ReadOnly = true;
            this.colCoverID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // repositoryItemMemoExEdit9
            // 
            this.repositoryItemMemoExEdit9.AutoHeight = false;
            this.repositoryItemMemoExEdit9.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit9.Name = "repositoryItemMemoExEdit9";
            this.repositoryItemMemoExEdit9.ReadOnly = true;
            this.repositoryItemMemoExEdit9.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit9.ShowIcon = false;
            // 
            // colEquipmentReference16
            // 
            this.colEquipmentReference16.FieldName = "EquipmentReference";
            this.colEquipmentReference16.Name = "colEquipmentReference16";
            this.colEquipmentReference16.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference16.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference16.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference16.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference16.Visible = true;
            this.colEquipmentReference16.VisibleIndex = 0;
            this.colEquipmentReference16.Width = 148;
            // 
            // colEquipmentID3
            // 
            this.colEquipmentID3.FieldName = "EquipmentID";
            this.colEquipmentID3.Name = "colEquipmentID3";
            this.colEquipmentID3.OptionsColumn.AllowEdit = false;
            this.colEquipmentID3.OptionsColumn.AllowFocus = false;
            this.colEquipmentID3.OptionsColumn.ReadOnly = true;
            this.colEquipmentID3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID3.Width = 76;
            // 
            // colCoverType
            // 
            this.colCoverType.FieldName = "CoverType";
            this.colCoverType.Name = "colCoverType";
            this.colCoverType.OptionsColumn.AllowEdit = false;
            this.colCoverType.OptionsColumn.AllowFocus = false;
            this.colCoverType.OptionsColumn.ReadOnly = true;
            this.colCoverType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCoverType.Visible = true;
            this.colCoverType.VisibleIndex = 1;
            // 
            // colCoverTypeID
            // 
            this.colCoverTypeID.FieldName = "CoverTypeID";
            this.colCoverTypeID.Name = "colCoverTypeID";
            this.colCoverTypeID.OptionsColumn.AllowEdit = false;
            this.colCoverTypeID.OptionsColumn.AllowFocus = false;
            this.colCoverTypeID.OptionsColumn.ReadOnly = true;
            this.colCoverTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCoverTypeID.Width = 82;
            // 
            // colSupplierID1
            // 
            this.colSupplierID1.FieldName = "SupplierID";
            this.colSupplierID1.Name = "colSupplierID1";
            this.colSupplierID1.OptionsColumn.AllowEdit = false;
            this.colSupplierID1.OptionsColumn.AllowFocus = false;
            this.colSupplierID1.OptionsColumn.ReadOnly = true;
            this.colSupplierID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colSupplierReference1
            // 
            this.colSupplierReference1.FieldName = "SupplierReference";
            this.colSupplierReference1.Name = "colSupplierReference1";
            this.colSupplierReference1.OptionsColumn.AllowEdit = false;
            this.colSupplierReference1.OptionsColumn.AllowFocus = false;
            this.colSupplierReference1.OptionsColumn.ReadOnly = true;
            this.colSupplierReference1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSupplierReference1.Visible = true;
            this.colSupplierReference1.VisibleIndex = 2;
            this.colSupplierReference1.Width = 154;
            // 
            // colPolicy
            // 
            this.colPolicy.FieldName = "Policy";
            this.colPolicy.Name = "colPolicy";
            this.colPolicy.OptionsColumn.AllowEdit = false;
            this.colPolicy.OptionsColumn.AllowFocus = false;
            this.colPolicy.OptionsColumn.ReadOnly = true;
            this.colPolicy.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPolicy.Visible = true;
            this.colPolicy.VisibleIndex = 3;
            // 
            // colAnnualCoverCost
            // 
            this.colAnnualCoverCost.ColumnEdit = this.moneyTextEdit1;
            this.colAnnualCoverCost.FieldName = "AnnualCoverCost";
            this.colAnnualCoverCost.Name = "colAnnualCoverCost";
            this.colAnnualCoverCost.OptionsColumn.AllowEdit = false;
            this.colAnnualCoverCost.OptionsColumn.AllowFocus = false;
            this.colAnnualCoverCost.OptionsColumn.ReadOnly = true;
            this.colAnnualCoverCost.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAnnualCoverCost.Visible = true;
            this.colAnnualCoverCost.VisibleIndex = 4;
            this.colAnnualCoverCost.Width = 149;
            // 
            // moneyTextEdit1
            // 
            this.moneyTextEdit1.AutoHeight = false;
            this.moneyTextEdit1.DisplayFormat.FormatString = "c2";
            this.moneyTextEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.moneyTextEdit1.Name = "moneyTextEdit1";
            // 
            // colRenewalDate
            // 
            this.colRenewalDate.FieldName = "RenewalDate";
            this.colRenewalDate.Name = "colRenewalDate";
            this.colRenewalDate.OptionsColumn.AllowEdit = false;
            this.colRenewalDate.OptionsColumn.AllowFocus = false;
            this.colRenewalDate.OptionsColumn.ReadOnly = true;
            this.colRenewalDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRenewalDate.Visible = true;
            this.colRenewalDate.VisibleIndex = 5;
            this.colRenewalDate.Width = 143;
            // 
            // colNotes3
            // 
            this.colNotes3.FieldName = "Notes";
            this.colNotes3.Name = "colNotes3";
            this.colNotes3.OptionsColumn.AllowEdit = false;
            this.colNotes3.OptionsColumn.AllowFocus = false;
            this.colNotes3.OptionsColumn.ReadOnly = true;
            this.colNotes3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNotes3.Visible = true;
            this.colNotes3.VisibleIndex = 7;
            this.colNotes3.Width = 335;
            // 
            // colExcess
            // 
            this.colExcess.ColumnEdit = this.moneyTextEdit1;
            this.colExcess.FieldName = "Excess";
            this.colExcess.Name = "colExcess";
            this.colExcess.OptionsColumn.AllowEdit = false;
            this.colExcess.OptionsColumn.AllowFocus = false;
            this.colExcess.OptionsColumn.ReadOnly = true;
            this.colExcess.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colExcess.Visible = true;
            this.colExcess.VisibleIndex = 6;
            this.colExcess.Width = 91;
            // 
            // colMode11
            // 
            this.colMode11.FieldName = "Mode";
            this.colMode11.Name = "colMode11";
            this.colMode11.OptionsColumn.AllowEdit = false;
            this.colMode11.OptionsColumn.AllowFocus = false;
            this.colMode11.OptionsColumn.ReadOnly = true;
            this.colMode11.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID11
            // 
            this.colRecordID11.FieldName = "RecordID";
            this.colRecordID11.Name = "colRecordID11";
            this.colRecordID11.OptionsColumn.AllowEdit = false;
            this.colRecordID11.OptionsColumn.AllowFocus = false;
            this.colRecordID11.OptionsColumn.ReadOnly = true;
            this.colRecordID11.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // serviceDatalTabPage
            // 
            this.serviceDatalTabPage.Controls.Add(this.serviceDataGridControl);
            this.serviceDatalTabPage.Name = "serviceDatalTabPage";
            this.serviceDatalTabPage.Size = new System.Drawing.Size(1290, 200);
            this.serviceDatalTabPage.Text = "Service Data";
            // 
            // serviceDataGridControl
            // 
            this.serviceDataGridControl.DataSource = this.spAS11054ServiceDataItemBindingSource;
            this.serviceDataGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.serviceDataGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.serviceDataGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.serviceDataGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.serviceDataGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.serviceDataGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.serviceDataGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.serviceDataGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.serviceDataGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.serviceDataGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.serviceDataGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.serviceDataGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.serviceDataGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.serviceDataGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.serviceDataGridControl.Location = new System.Drawing.Point(0, 0);
            this.serviceDataGridControl.MainView = this.serviceDataGridView;
            this.serviceDataGridControl.MenuManager = this.barManager1;
            this.serviceDataGridControl.Name = "serviceDataGridControl";
            this.serviceDataGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit11,
            this.repositoryItemMemoExEdit12});
            this.serviceDataGridControl.Size = new System.Drawing.Size(1290, 200);
            this.serviceDataGridControl.TabIndex = 2;
            this.serviceDataGridControl.Tag = "Service Interval Details";
            this.serviceDataGridControl.UseEmbeddedNavigator = true;
            this.serviceDataGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.serviceDataGridView});
            // 
            // spAS11054ServiceDataItemBindingSource
            // 
            this.spAS11054ServiceDataItemBindingSource.DataMember = "sp_AS_11054_Service_Data_Item";
            this.spAS11054ServiceDataItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // serviceDataGridView
            // 
            this.serviceDataGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colServiceDataID,
            this.colEquipmentID13,
            this.colEquipmentReference15,
            this.colServiceTypeID,
            this.colServiceType1,
            this.colDistanceUnitsID,
            this.colDistanceUnits,
            this.colDistanceFrequency,
            this.colTimeUnitsID,
            this.colTimeUnits,
            this.colTimeFrequency,
            this.colWarrantyPeriod,
            this.colWarrantyEndDate,
            this.colWarrantyEndDistance,
            this.colServiceDataNotes,
            this.colServiceStatus,
            this.colServiceDueDistance,
            this.colServiceDueDate,
            this.colAlertWithinXDistance,
            this.colAlertWithinXTime,
            this.colWorkType1,
            this.colIntervalScheduleNotes,
            this.colIsCurrentInterval,
            this.colMode12,
            this.colRecordID12});
            this.serviceDataGridView.GridControl = this.serviceDataGridControl;
            this.serviceDataGridView.Name = "serviceDataGridView";
            this.serviceDataGridView.OptionsCustomization.AllowFilter = false;
            this.serviceDataGridView.OptionsCustomization.AllowGroup = false;
            this.serviceDataGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.serviceDataGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.serviceDataGridView.OptionsLayout.StoreAppearance = true;
            this.serviceDataGridView.OptionsView.ColumnAutoWidth = false;
            this.serviceDataGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.serviceDataGridView.OptionsView.ShowGroupPanel = false;
            this.serviceDataGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.serviceDataGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.serviceDataGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.serviceDataGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.serviceDataGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.serviceDataGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.serviceDataGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colServiceDataID
            // 
            this.colServiceDataID.FieldName = "ServiceDataID";
            this.colServiceDataID.Name = "colServiceDataID";
            this.colServiceDataID.OptionsColumn.AllowEdit = false;
            this.colServiceDataID.OptionsColumn.AllowFocus = false;
            this.colServiceDataID.OptionsColumn.ReadOnly = true;
            this.colServiceDataID.OptionsColumn.ShowInCustomizationForm = false;
            this.colServiceDataID.OptionsColumn.ShowInExpressionEditor = false;
            this.colServiceDataID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colServiceDataID.Width = 87;
            // 
            // colEquipmentID13
            // 
            this.colEquipmentID13.FieldName = "EquipmentID";
            this.colEquipmentID13.Name = "colEquipmentID13";
            this.colEquipmentID13.OptionsColumn.AllowEdit = false;
            this.colEquipmentID13.OptionsColumn.AllowFocus = false;
            this.colEquipmentID13.OptionsColumn.ReadOnly = true;
            this.colEquipmentID13.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID13.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID13.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID13.Width = 76;
            // 
            // colEquipmentReference15
            // 
            this.colEquipmentReference15.FieldName = "EquipmentReference";
            this.colEquipmentReference15.Name = "colEquipmentReference15";
            this.colEquipmentReference15.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference15.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference15.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference15.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference15.Visible = true;
            this.colEquipmentReference15.VisibleIndex = 0;
            this.colEquipmentReference15.Width = 115;
            // 
            // colServiceTypeID
            // 
            this.colServiceTypeID.FieldName = "ServiceTypeID";
            this.colServiceTypeID.Name = "colServiceTypeID";
            this.colServiceTypeID.OptionsColumn.AllowEdit = false;
            this.colServiceTypeID.OptionsColumn.AllowFocus = false;
            this.colServiceTypeID.OptionsColumn.ReadOnly = true;
            this.colServiceTypeID.OptionsColumn.ShowInCustomizationForm = false;
            this.colServiceTypeID.OptionsColumn.ShowInExpressionEditor = false;
            this.colServiceTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colServiceTypeID.Width = 88;
            // 
            // colServiceType1
            // 
            this.colServiceType1.FieldName = "ServiceType";
            this.colServiceType1.Name = "colServiceType1";
            this.colServiceType1.OptionsColumn.AllowEdit = false;
            this.colServiceType1.OptionsColumn.AllowFocus = false;
            this.colServiceType1.OptionsColumn.ReadOnly = true;
            this.colServiceType1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colServiceType1.Visible = true;
            this.colServiceType1.VisibleIndex = 1;
            // 
            // colDistanceUnitsID
            // 
            this.colDistanceUnitsID.FieldName = "DistanceUnitsID";
            this.colDistanceUnitsID.Name = "colDistanceUnitsID";
            this.colDistanceUnitsID.OptionsColumn.AllowEdit = false;
            this.colDistanceUnitsID.OptionsColumn.AllowFocus = false;
            this.colDistanceUnitsID.OptionsColumn.ReadOnly = true;
            this.colDistanceUnitsID.OptionsColumn.ShowInCustomizationForm = false;
            this.colDistanceUnitsID.OptionsColumn.ShowInExpressionEditor = false;
            this.colDistanceUnitsID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDistanceUnitsID.Width = 94;
            // 
            // colDistanceUnits
            // 
            this.colDistanceUnits.FieldName = "DistanceUnits";
            this.colDistanceUnits.Name = "colDistanceUnits";
            this.colDistanceUnits.OptionsColumn.AllowEdit = false;
            this.colDistanceUnits.OptionsColumn.AllowFocus = false;
            this.colDistanceUnits.OptionsColumn.ReadOnly = true;
            this.colDistanceUnits.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDistanceUnits.Visible = true;
            this.colDistanceUnits.VisibleIndex = 2;
            this.colDistanceUnits.Width = 80;
            // 
            // colDistanceFrequency
            // 
            this.colDistanceFrequency.FieldName = "DistanceFrequency";
            this.colDistanceFrequency.Name = "colDistanceFrequency";
            this.colDistanceFrequency.OptionsColumn.AllowEdit = false;
            this.colDistanceFrequency.OptionsColumn.AllowFocus = false;
            this.colDistanceFrequency.OptionsColumn.ReadOnly = true;
            this.colDistanceFrequency.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDistanceFrequency.Visible = true;
            this.colDistanceFrequency.VisibleIndex = 3;
            this.colDistanceFrequency.Width = 107;
            // 
            // colTimeUnitsID
            // 
            this.colTimeUnitsID.FieldName = "TimeUnitsID";
            this.colTimeUnitsID.Name = "colTimeUnitsID";
            this.colTimeUnitsID.OptionsColumn.AllowEdit = false;
            this.colTimeUnitsID.OptionsColumn.AllowFocus = false;
            this.colTimeUnitsID.OptionsColumn.ReadOnly = true;
            this.colTimeUnitsID.OptionsColumn.ShowInCustomizationForm = false;
            this.colTimeUnitsID.OptionsColumn.ShowInExpressionEditor = false;
            this.colTimeUnitsID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colTimeUnits
            // 
            this.colTimeUnits.FieldName = "TimeUnits";
            this.colTimeUnits.Name = "colTimeUnits";
            this.colTimeUnits.OptionsColumn.AllowEdit = false;
            this.colTimeUnits.OptionsColumn.AllowFocus = false;
            this.colTimeUnits.OptionsColumn.ReadOnly = true;
            this.colTimeUnits.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTimeUnits.Visible = true;
            this.colTimeUnits.VisibleIndex = 4;
            // 
            // colTimeFrequency
            // 
            this.colTimeFrequency.FieldName = "TimeFrequency";
            this.colTimeFrequency.Name = "colTimeFrequency";
            this.colTimeFrequency.OptionsColumn.AllowEdit = false;
            this.colTimeFrequency.OptionsColumn.AllowFocus = false;
            this.colTimeFrequency.OptionsColumn.ReadOnly = true;
            this.colTimeFrequency.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTimeFrequency.Visible = true;
            this.colTimeFrequency.VisibleIndex = 5;
            this.colTimeFrequency.Width = 88;
            // 
            // colWarrantyPeriod
            // 
            this.colWarrantyPeriod.FieldName = "WarrantyPeriod";
            this.colWarrantyPeriod.Name = "colWarrantyPeriod";
            this.colWarrantyPeriod.OptionsColumn.AllowEdit = false;
            this.colWarrantyPeriod.OptionsColumn.AllowFocus = false;
            this.colWarrantyPeriod.OptionsColumn.ReadOnly = true;
            this.colWarrantyPeriod.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWarrantyPeriod.Visible = true;
            this.colWarrantyPeriod.VisibleIndex = 6;
            this.colWarrantyPeriod.Width = 91;
            // 
            // colWarrantyEndDate
            // 
            this.colWarrantyEndDate.FieldName = "WarrantyEndDate";
            this.colWarrantyEndDate.Name = "colWarrantyEndDate";
            this.colWarrantyEndDate.OptionsColumn.AllowEdit = false;
            this.colWarrantyEndDate.OptionsColumn.AllowFocus = false;
            this.colWarrantyEndDate.OptionsColumn.ReadOnly = true;
            this.colWarrantyEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWarrantyEndDate.Visible = true;
            this.colWarrantyEndDate.VisibleIndex = 7;
            this.colWarrantyEndDate.Width = 105;
            // 
            // colWarrantyEndDistance
            // 
            this.colWarrantyEndDistance.FieldName = "WarrantyEndDistance";
            this.colWarrantyEndDistance.Name = "colWarrantyEndDistance";
            this.colWarrantyEndDistance.OptionsColumn.AllowEdit = false;
            this.colWarrantyEndDistance.OptionsColumn.AllowFocus = false;
            this.colWarrantyEndDistance.OptionsColumn.ReadOnly = true;
            this.colWarrantyEndDistance.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWarrantyEndDistance.Visible = true;
            this.colWarrantyEndDistance.VisibleIndex = 8;
            this.colWarrantyEndDistance.Width = 123;
            // 
            // colServiceDataNotes
            // 
            this.colServiceDataNotes.FieldName = "ServiceDataNotes";
            this.colServiceDataNotes.Name = "colServiceDataNotes";
            this.colServiceDataNotes.OptionsColumn.AllowEdit = false;
            this.colServiceDataNotes.OptionsColumn.AllowFocus = false;
            this.colServiceDataNotes.OptionsColumn.ReadOnly = true;
            this.colServiceDataNotes.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colServiceDataNotes.Visible = true;
            this.colServiceDataNotes.VisibleIndex = 17;
            this.colServiceDataNotes.Width = 302;
            // 
            // colServiceStatus
            // 
            this.colServiceStatus.FieldName = "ServiceStatus";
            this.colServiceStatus.Name = "colServiceStatus";
            this.colServiceStatus.OptionsColumn.AllowEdit = false;
            this.colServiceStatus.OptionsColumn.AllowFocus = false;
            this.colServiceStatus.OptionsColumn.ReadOnly = true;
            this.colServiceStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colServiceStatus.Visible = true;
            this.colServiceStatus.VisibleIndex = 9;
            this.colServiceStatus.Width = 81;
            // 
            // colServiceDueDistance
            // 
            this.colServiceDueDistance.FieldName = "ServiceDueDistance";
            this.colServiceDueDistance.Name = "colServiceDueDistance";
            this.colServiceDueDistance.OptionsColumn.AllowEdit = false;
            this.colServiceDueDistance.OptionsColumn.AllowFocus = false;
            this.colServiceDueDistance.OptionsColumn.ReadOnly = true;
            this.colServiceDueDistance.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colServiceDueDistance.Visible = true;
            this.colServiceDueDistance.VisibleIndex = 10;
            this.colServiceDueDistance.Width = 113;
            // 
            // colServiceDueDate
            // 
            this.colServiceDueDate.FieldName = "ServiceDueDate";
            this.colServiceDueDate.Name = "colServiceDueDate";
            this.colServiceDueDate.OptionsColumn.AllowEdit = false;
            this.colServiceDueDate.OptionsColumn.AllowFocus = false;
            this.colServiceDueDate.OptionsColumn.ReadOnly = true;
            this.colServiceDueDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colServiceDueDate.Visible = true;
            this.colServiceDueDate.VisibleIndex = 11;
            this.colServiceDueDate.Width = 95;
            // 
            // colAlertWithinXDistance
            // 
            this.colAlertWithinXDistance.FieldName = "AlertWithinXDistance";
            this.colAlertWithinXDistance.Name = "colAlertWithinXDistance";
            this.colAlertWithinXDistance.OptionsColumn.AllowEdit = false;
            this.colAlertWithinXDistance.OptionsColumn.AllowFocus = false;
            this.colAlertWithinXDistance.OptionsColumn.ReadOnly = true;
            this.colAlertWithinXDistance.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAlertWithinXDistance.Visible = true;
            this.colAlertWithinXDistance.VisibleIndex = 12;
            this.colAlertWithinXDistance.Width = 118;
            // 
            // colAlertWithinXTime
            // 
            this.colAlertWithinXTime.FieldName = "AlertWithinXTime";
            this.colAlertWithinXTime.Name = "colAlertWithinXTime";
            this.colAlertWithinXTime.OptionsColumn.AllowEdit = false;
            this.colAlertWithinXTime.OptionsColumn.AllowFocus = false;
            this.colAlertWithinXTime.OptionsColumn.ReadOnly = true;
            this.colAlertWithinXTime.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAlertWithinXTime.Visible = true;
            this.colAlertWithinXTime.VisibleIndex = 13;
            this.colAlertWithinXTime.Width = 99;
            // 
            // colWorkType1
            // 
            this.colWorkType1.FieldName = "WorkType";
            this.colWorkType1.Name = "colWorkType1";
            this.colWorkType1.OptionsColumn.AllowEdit = false;
            this.colWorkType1.OptionsColumn.AllowFocus = false;
            this.colWorkType1.OptionsColumn.ReadOnly = true;
            this.colWorkType1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWorkType1.Visible = true;
            this.colWorkType1.VisibleIndex = 14;
            // 
            // colIntervalScheduleNotes
            // 
            this.colIntervalScheduleNotes.FieldName = "IntervalScheduleNotes";
            this.colIntervalScheduleNotes.Name = "colIntervalScheduleNotes";
            this.colIntervalScheduleNotes.OptionsColumn.AllowEdit = false;
            this.colIntervalScheduleNotes.OptionsColumn.AllowFocus = false;
            this.colIntervalScheduleNotes.OptionsColumn.ReadOnly = true;
            this.colIntervalScheduleNotes.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIntervalScheduleNotes.Visible = true;
            this.colIntervalScheduleNotes.VisibleIndex = 16;
            this.colIntervalScheduleNotes.Width = 276;
            // 
            // colIsCurrentInterval
            // 
            this.colIsCurrentInterval.FieldName = "IsCurrentInterval";
            this.colIsCurrentInterval.Name = "colIsCurrentInterval";
            this.colIsCurrentInterval.OptionsColumn.AllowEdit = false;
            this.colIsCurrentInterval.OptionsColumn.AllowFocus = false;
            this.colIsCurrentInterval.OptionsColumn.ReadOnly = true;
            this.colIsCurrentInterval.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIsCurrentInterval.Visible = true;
            this.colIsCurrentInterval.VisibleIndex = 15;
            this.colIsCurrentInterval.Width = 102;
            // 
            // colMode12
            // 
            this.colMode12.FieldName = "Mode";
            this.colMode12.Name = "colMode12";
            this.colMode12.OptionsColumn.AllowEdit = false;
            this.colMode12.OptionsColumn.AllowFocus = false;
            this.colMode12.OptionsColumn.ReadOnly = true;
            this.colMode12.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode12.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode12.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID12
            // 
            this.colRecordID12.FieldName = "RecordID";
            this.colRecordID12.Name = "colRecordID12";
            this.colRecordID12.OptionsColumn.AllowEdit = false;
            this.colRecordID12.OptionsColumn.AllowFocus = false;
            this.colRecordID12.OptionsColumn.ReadOnly = true;
            this.colRecordID12.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID12.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID12.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // repositoryItemMemoExEdit11
            // 
            this.repositoryItemMemoExEdit11.AutoHeight = false;
            this.repositoryItemMemoExEdit11.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit11.Name = "repositoryItemMemoExEdit11";
            this.repositoryItemMemoExEdit11.ReadOnly = true;
            this.repositoryItemMemoExEdit11.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit11.ShowIcon = false;
            // 
            // repositoryItemMemoExEdit12
            // 
            this.repositoryItemMemoExEdit12.AutoHeight = false;
            this.repositoryItemMemoExEdit12.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit12.Name = "repositoryItemMemoExEdit12";
            this.repositoryItemMemoExEdit12.ReadOnly = true;
            this.repositoryItemMemoExEdit12.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit12.ShowIcon = false;
            // 
            // workDetailTabPage
            // 
            this.workDetailTabPage.Controls.Add(this.workDetailGridControl);
            this.workDetailTabPage.Name = "workDetailTabPage";
            this.workDetailTabPage.Size = new System.Drawing.Size(1290, 200);
            this.workDetailTabPage.Text = "Work Order Details";
            // 
            // workDetailGridControl
            // 
            this.workDetailGridControl.DataSource = this.spAS11078WorkDetailItemBindingSource;
            this.workDetailGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.workDetailGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.workDetailGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.workDetailGridControl.Location = new System.Drawing.Point(0, 0);
            this.workDetailGridControl.MainView = this.workDetailGridView;
            this.workDetailGridControl.MenuManager = this.barManager1;
            this.workDetailGridControl.Name = "workDetailGridControl";
            this.workDetailGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit13,
            this.repositoryItemMemoExEdit14});
            this.workDetailGridControl.Size = new System.Drawing.Size(1290, 200);
            this.workDetailGridControl.TabIndex = 1;
            this.workDetailGridControl.UseEmbeddedNavigator = true;
            this.workDetailGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.workDetailGridView});
            // 
            // spAS11078WorkDetailItemBindingSource
            // 
            this.spAS11078WorkDetailItemBindingSource.DataMember = "sp_AS_11078_Work_Detail_Item";
            this.spAS11078WorkDetailItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // workDetailGridView
            // 
            this.workDetailGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colWorkDetailID,
            this.colEquipmentID2,
            this.colEquipmentReference11,
            this.colWorkType,
            this.colWorkTypeID,
            this.colDateRaised,
            this.colSupplierID,
            this.colSupplierReference,
            this.colStatusID,
            this.colStatus,
            this.colDescription2,
            this.colTransactionID,
            this.colOrderNumber,
            this.colCurrentMileage,
            this.colWorkCompletionDate,
            this.colServiceIntervalID,
            this.colServiceType,
            this.colNotes2,
            this.colMode10,
            this.colRecordID10});
            this.workDetailGridView.GridControl = this.workDetailGridControl;
            this.workDetailGridView.Name = "workDetailGridView";
            this.workDetailGridView.OptionsCustomization.AllowFilter = false;
            this.workDetailGridView.OptionsCustomization.AllowGroup = false;
            this.workDetailGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.workDetailGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.workDetailGridView.OptionsLayout.StoreAppearance = true;
            this.workDetailGridView.OptionsSelection.MultiSelect = true;
            this.workDetailGridView.OptionsView.ColumnAutoWidth = false;
            this.workDetailGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.workDetailGridView.OptionsView.ShowGroupPanel = false;
            this.workDetailGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.workDetailGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.workDetailGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.workDetailGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.workDetailGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.workDetailGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.workDetailGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colWorkDetailID
            // 
            this.colWorkDetailID.FieldName = "WorkDetailID";
            this.colWorkDetailID.Name = "colWorkDetailID";
            this.colWorkDetailID.OptionsColumn.AllowEdit = false;
            this.colWorkDetailID.OptionsColumn.AllowFocus = false;
            this.colWorkDetailID.OptionsColumn.ReadOnly = true;
            this.colWorkDetailID.OptionsColumn.ShowInCustomizationForm = false;
            this.colWorkDetailID.OptionsColumn.ShowInExpressionEditor = false;
            this.colWorkDetailID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWorkDetailID.Width = 81;
            // 
            // colEquipmentID2
            // 
            this.colEquipmentID2.FieldName = "EquipmentID";
            this.colEquipmentID2.Name = "colEquipmentID2";
            this.colEquipmentID2.OptionsColumn.AllowEdit = false;
            this.colEquipmentID2.OptionsColumn.AllowFocus = false;
            this.colEquipmentID2.OptionsColumn.ReadOnly = true;
            this.colEquipmentID2.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID2.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID2.Width = 76;
            // 
            // colEquipmentReference11
            // 
            this.colEquipmentReference11.FieldName = "EquipmentReference";
            this.colEquipmentReference11.Name = "colEquipmentReference11";
            this.colEquipmentReference11.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference11.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference11.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference11.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference11.Visible = true;
            this.colEquipmentReference11.VisibleIndex = 0;
            this.colEquipmentReference11.Width = 115;
            // 
            // colWorkType
            // 
            this.colWorkType.FieldName = "WorkType";
            this.colWorkType.Name = "colWorkType";
            this.colWorkType.OptionsColumn.AllowEdit = false;
            this.colWorkType.OptionsColumn.AllowFocus = false;
            this.colWorkType.OptionsColumn.ReadOnly = true;
            this.colWorkType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWorkType.Visible = true;
            this.colWorkType.VisibleIndex = 1;
            // 
            // colWorkTypeID
            // 
            this.colWorkTypeID.FieldName = "WorkTypeID";
            this.colWorkTypeID.Name = "colWorkTypeID";
            this.colWorkTypeID.OptionsColumn.AllowEdit = false;
            this.colWorkTypeID.OptionsColumn.AllowFocus = false;
            this.colWorkTypeID.OptionsColumn.ReadOnly = true;
            this.colWorkTypeID.OptionsColumn.ShowInCustomizationForm = false;
            this.colWorkTypeID.OptionsColumn.ShowInExpressionEditor = false;
            this.colWorkTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWorkTypeID.Width = 78;
            // 
            // colDateRaised
            // 
            this.colDateRaised.FieldName = "DateRaised";
            this.colDateRaised.Name = "colDateRaised";
            this.colDateRaised.OptionsColumn.AllowEdit = false;
            this.colDateRaised.OptionsColumn.AllowFocus = false;
            this.colDateRaised.OptionsColumn.ReadOnly = true;
            this.colDateRaised.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDateRaised.Visible = true;
            this.colDateRaised.VisibleIndex = 2;
            // 
            // colSupplierID
            // 
            this.colSupplierID.FieldName = "SupplierID";
            this.colSupplierID.Name = "colSupplierID";
            this.colSupplierID.OptionsColumn.AllowEdit = false;
            this.colSupplierID.OptionsColumn.AllowFocus = false;
            this.colSupplierID.OptionsColumn.ReadOnly = true;
            this.colSupplierID.OptionsColumn.ShowInCustomizationForm = false;
            this.colSupplierID.OptionsColumn.ShowInExpressionEditor = false;
            this.colSupplierID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colSupplierReference
            // 
            this.colSupplierReference.FieldName = "SupplierReference";
            this.colSupplierReference.Name = "colSupplierReference";
            this.colSupplierReference.OptionsColumn.AllowEdit = false;
            this.colSupplierReference.OptionsColumn.AllowFocus = false;
            this.colSupplierReference.OptionsColumn.ReadOnly = true;
            this.colSupplierReference.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSupplierReference.Visible = true;
            this.colSupplierReference.VisibleIndex = 3;
            this.colSupplierReference.Width = 103;
            // 
            // colStatusID
            // 
            this.colStatusID.FieldName = "StatusID";
            this.colStatusID.Name = "colStatusID";
            this.colStatusID.OptionsColumn.AllowEdit = false;
            this.colStatusID.OptionsColumn.AllowFocus = false;
            this.colStatusID.OptionsColumn.ReadOnly = true;
            this.colStatusID.OptionsColumn.ShowInCustomizationForm = false;
            this.colStatusID.OptionsColumn.ShowInExpressionEditor = false;
            this.colStatusID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colStatus
            // 
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.OptionsColumn.AllowEdit = false;
            this.colStatus.OptionsColumn.AllowFocus = false;
            this.colStatus.OptionsColumn.ReadOnly = true;
            this.colStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 4;
            // 
            // colDescription2
            // 
            this.colDescription2.FieldName = "Description";
            this.colDescription2.Name = "colDescription2";
            this.colDescription2.OptionsColumn.AllowEdit = false;
            this.colDescription2.OptionsColumn.AllowFocus = false;
            this.colDescription2.OptionsColumn.ReadOnly = true;
            this.colDescription2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDescription2.Visible = true;
            this.colDescription2.VisibleIndex = 5;
            // 
            // colTransactionID
            // 
            this.colTransactionID.FieldName = "TransactionID";
            this.colTransactionID.Name = "colTransactionID";
            this.colTransactionID.OptionsColumn.AllowEdit = false;
            this.colTransactionID.OptionsColumn.AllowFocus = false;
            this.colTransactionID.OptionsColumn.ReadOnly = true;
            this.colTransactionID.OptionsColumn.ShowInCustomizationForm = false;
            this.colTransactionID.OptionsColumn.ShowInExpressionEditor = false;
            this.colTransactionID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTransactionID.Width = 82;
            // 
            // colOrderNumber
            // 
            this.colOrderNumber.FieldName = "OrderNumber";
            this.colOrderNumber.Name = "colOrderNumber";
            this.colOrderNumber.OptionsColumn.AllowEdit = false;
            this.colOrderNumber.OptionsColumn.AllowFocus = false;
            this.colOrderNumber.OptionsColumn.ReadOnly = true;
            this.colOrderNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colOrderNumber.Visible = true;
            this.colOrderNumber.VisibleIndex = 6;
            this.colOrderNumber.Width = 80;
            // 
            // colCurrentMileage
            // 
            this.colCurrentMileage.FieldName = "CurrentMileage";
            this.colCurrentMileage.Name = "colCurrentMileage";
            this.colCurrentMileage.OptionsColumn.AllowEdit = false;
            this.colCurrentMileage.OptionsColumn.AllowFocus = false;
            this.colCurrentMileage.OptionsColumn.ReadOnly = true;
            this.colCurrentMileage.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCurrentMileage.Visible = true;
            this.colCurrentMileage.VisibleIndex = 7;
            this.colCurrentMileage.Width = 88;
            // 
            // colWorkCompletionDate
            // 
            this.colWorkCompletionDate.FieldName = "WorkCompletionDate";
            this.colWorkCompletionDate.Name = "colWorkCompletionDate";
            this.colWorkCompletionDate.OptionsColumn.AllowEdit = false;
            this.colWorkCompletionDate.OptionsColumn.AllowFocus = false;
            this.colWorkCompletionDate.OptionsColumn.ReadOnly = true;
            this.colWorkCompletionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWorkCompletionDate.Visible = true;
            this.colWorkCompletionDate.VisibleIndex = 8;
            this.colWorkCompletionDate.Width = 119;
            // 
            // colServiceIntervalID
            // 
            this.colServiceIntervalID.FieldName = "ServiceIntervalID";
            this.colServiceIntervalID.Name = "colServiceIntervalID";
            this.colServiceIntervalID.OptionsColumn.AllowEdit = false;
            this.colServiceIntervalID.OptionsColumn.AllowFocus = false;
            this.colServiceIntervalID.OptionsColumn.ReadOnly = true;
            this.colServiceIntervalID.OptionsColumn.ShowInCustomizationForm = false;
            this.colServiceIntervalID.OptionsColumn.ShowInExpressionEditor = false;
            this.colServiceIntervalID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colServiceIntervalID.Width = 102;
            // 
            // colServiceType
            // 
            this.colServiceType.FieldName = "ServiceType";
            this.colServiceType.Name = "colServiceType";
            this.colServiceType.OptionsColumn.AllowEdit = false;
            this.colServiceType.OptionsColumn.AllowFocus = false;
            this.colServiceType.OptionsColumn.ReadOnly = true;
            this.colServiceType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colServiceType.Visible = true;
            this.colServiceType.VisibleIndex = 9;
            // 
            // colNotes2
            // 
            this.colNotes2.FieldName = "Notes";
            this.colNotes2.Name = "colNotes2";
            this.colNotes2.OptionsColumn.AllowEdit = false;
            this.colNotes2.OptionsColumn.AllowFocus = false;
            this.colNotes2.OptionsColumn.ReadOnly = true;
            this.colNotes2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNotes2.Visible = true;
            this.colNotes2.VisibleIndex = 10;
            // 
            // colMode10
            // 
            this.colMode10.FieldName = "Mode";
            this.colMode10.Name = "colMode10";
            this.colMode10.OptionsColumn.AllowEdit = false;
            this.colMode10.OptionsColumn.AllowFocus = false;
            this.colMode10.OptionsColumn.ReadOnly = true;
            this.colMode10.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode10.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode10.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID10
            // 
            this.colRecordID10.FieldName = "RecordID";
            this.colRecordID10.Name = "colRecordID10";
            this.colRecordID10.OptionsColumn.AllowEdit = false;
            this.colRecordID10.OptionsColumn.AllowFocus = false;
            this.colRecordID10.OptionsColumn.ReadOnly = true;
            this.colRecordID10.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID10.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID10.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // repositoryItemMemoExEdit13
            // 
            this.repositoryItemMemoExEdit13.AutoHeight = false;
            this.repositoryItemMemoExEdit13.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit13.Name = "repositoryItemMemoExEdit13";
            this.repositoryItemMemoExEdit13.ReadOnly = true;
            this.repositoryItemMemoExEdit13.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit13.ShowIcon = false;
            // 
            // repositoryItemMemoExEdit14
            // 
            this.repositoryItemMemoExEdit14.AutoHeight = false;
            this.repositoryItemMemoExEdit14.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit14.Name = "repositoryItemMemoExEdit14";
            this.repositoryItemMemoExEdit14.ReadOnly = true;
            this.repositoryItemMemoExEdit14.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit14.ShowIcon = false;
            // 
            // incidentTabPage
            // 
            this.incidentTabPage.Controls.Add(this.incidentGridControl);
            this.incidentTabPage.Name = "incidentTabPage";
            this.incidentTabPage.Size = new System.Drawing.Size(1290, 200);
            this.incidentTabPage.Text = "Incident Details";
            // 
            // incidentGridControl
            // 
            this.incidentGridControl.DataSource = this.spAS11081IncidentItemBindingSource;
            this.incidentGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.incidentGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.incidentGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.incidentGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.incidentGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.incidentGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.incidentGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.incidentGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.incidentGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.incidentGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.incidentGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.incidentGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.incidentGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.incidentGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.incidentGridControl.Location = new System.Drawing.Point(0, 0);
            this.incidentGridControl.MainView = this.incidentGridView;
            this.incidentGridControl.MenuManager = this.barManager1;
            this.incidentGridControl.Name = "incidentGridControl";
            this.incidentGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.incidentNotesMemoEdit,
            this.moneyTextEdit5});
            this.incidentGridControl.Size = new System.Drawing.Size(1290, 200);
            this.incidentGridControl.TabIndex = 2;
            this.incidentGridControl.UseEmbeddedNavigator = true;
            this.incidentGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.incidentGridView});
            // 
            // spAS11081IncidentItemBindingSource
            // 
            this.spAS11081IncidentItemBindingSource.DataMember = "sp_AS_11081_Incident_Item";
            this.spAS11081IncidentItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // incidentGridView
            // 
            this.incidentGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colIncidentID,
            this.colEquipmentID7,
            this.colEquipmentReference5,
            this.colIncidentStatusID,
            this.colIncidentStatus,
            this.colIncidentTypeID,
            this.colIncidentType,
            this.colIncidentReference,
            this.colDateHappened,
            this.colLocation,
            this.colRepairDueID,
            this.colRepairDue,
            this.colSeverityID,
            this.colSeverity,
            this.colWitness,
            this.colKeeperAtFaultID,
            this.colKeeperAtFault,
            this.colLegalActionID,
            this.colLegalAction,
            this.colFollowUpDate,
            this.colCost,
            this.colNotes4,
            this.colMode3,
            this.colRecordID3});
            this.incidentGridView.GridControl = this.incidentGridControl;
            this.incidentGridView.Name = "incidentGridView";
            this.incidentGridView.OptionsCustomization.AllowFilter = false;
            this.incidentGridView.OptionsCustomization.AllowGroup = false;
            this.incidentGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.incidentGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.incidentGridView.OptionsLayout.StoreAppearance = true;
            this.incidentGridView.OptionsSelection.MultiSelect = true;
            this.incidentGridView.OptionsView.ColumnAutoWidth = false;
            this.incidentGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.incidentGridView.OptionsView.ShowGroupPanel = false;
            this.incidentGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.incidentGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.incidentGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.incidentGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.incidentGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.incidentGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.incidentGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colIncidentID
            // 
            this.colIncidentID.FieldName = "IncidentID";
            this.colIncidentID.Name = "colIncidentID";
            this.colIncidentID.OptionsColumn.AllowEdit = false;
            this.colIncidentID.OptionsColumn.AllowFocus = false;
            this.colIncidentID.OptionsColumn.ReadOnly = true;
            this.colIncidentID.OptionsColumn.ShowInCustomizationForm = false;
            this.colIncidentID.OptionsColumn.ShowInExpressionEditor = false;
            this.colIncidentID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colEquipmentID7
            // 
            this.colEquipmentID7.FieldName = "EquipmentID";
            this.colEquipmentID7.Name = "colEquipmentID7";
            this.colEquipmentID7.OptionsColumn.AllowEdit = false;
            this.colEquipmentID7.OptionsColumn.AllowFocus = false;
            this.colEquipmentID7.OptionsColumn.ReadOnly = true;
            this.colEquipmentID7.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID7.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID7.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID7.Width = 76;
            // 
            // colEquipmentReference5
            // 
            this.colEquipmentReference5.FieldName = "EquipmentReference";
            this.colEquipmentReference5.Name = "colEquipmentReference5";
            this.colEquipmentReference5.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference5.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference5.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference5.Visible = true;
            this.colEquipmentReference5.VisibleIndex = 0;
            this.colEquipmentReference5.Width = 115;
            // 
            // colIncidentStatusID
            // 
            this.colIncidentStatusID.FieldName = "IncidentStatusID";
            this.colIncidentStatusID.Name = "colIncidentStatusID";
            this.colIncidentStatusID.OptionsColumn.AllowEdit = false;
            this.colIncidentStatusID.OptionsColumn.AllowFocus = false;
            this.colIncidentStatusID.OptionsColumn.ReadOnly = true;
            this.colIncidentStatusID.OptionsColumn.ShowInCustomizationForm = false;
            this.colIncidentStatusID.OptionsColumn.ShowInExpressionEditor = false;
            this.colIncidentStatusID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIncidentStatusID.Width = 99;
            // 
            // colIncidentStatus
            // 
            this.colIncidentStatus.FieldName = "IncidentStatus";
            this.colIncidentStatus.Name = "colIncidentStatus";
            this.colIncidentStatus.OptionsColumn.AllowEdit = false;
            this.colIncidentStatus.OptionsColumn.AllowFocus = false;
            this.colIncidentStatus.OptionsColumn.ReadOnly = true;
            this.colIncidentStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIncidentStatus.Visible = true;
            this.colIncidentStatus.VisibleIndex = 2;
            this.colIncidentStatus.Width = 96;
            // 
            // colIncidentTypeID
            // 
            this.colIncidentTypeID.FieldName = "IncidentTypeID";
            this.colIncidentTypeID.Name = "colIncidentTypeID";
            this.colIncidentTypeID.OptionsColumn.AllowEdit = false;
            this.colIncidentTypeID.OptionsColumn.AllowFocus = false;
            this.colIncidentTypeID.OptionsColumn.ReadOnly = true;
            this.colIncidentTypeID.OptionsColumn.ShowInCustomizationForm = false;
            this.colIncidentTypeID.OptionsColumn.ShowInExpressionEditor = false;
            this.colIncidentTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIncidentTypeID.Width = 92;
            // 
            // colIncidentType
            // 
            this.colIncidentType.FieldName = "IncidentType";
            this.colIncidentType.Name = "colIncidentType";
            this.colIncidentType.OptionsColumn.AllowEdit = false;
            this.colIncidentType.OptionsColumn.AllowFocus = false;
            this.colIncidentType.OptionsColumn.ReadOnly = true;
            this.colIncidentType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIncidentType.Visible = true;
            this.colIncidentType.VisibleIndex = 3;
            this.colIncidentType.Width = 97;
            // 
            // colIncidentReference
            // 
            this.colIncidentReference.FieldName = "IncidentReference";
            this.colIncidentReference.Name = "colIncidentReference";
            this.colIncidentReference.OptionsColumn.AllowEdit = false;
            this.colIncidentReference.OptionsColumn.AllowFocus = false;
            this.colIncidentReference.OptionsColumn.ReadOnly = true;
            this.colIncidentReference.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIncidentReference.Visible = true;
            this.colIncidentReference.VisibleIndex = 1;
            this.colIncidentReference.Width = 104;
            // 
            // colDateHappened
            // 
            this.colDateHappened.FieldName = "DateHappened";
            this.colDateHappened.Name = "colDateHappened";
            this.colDateHappened.OptionsColumn.AllowEdit = false;
            this.colDateHappened.OptionsColumn.AllowFocus = false;
            this.colDateHappened.OptionsColumn.ReadOnly = true;
            this.colDateHappened.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDateHappened.Visible = true;
            this.colDateHappened.VisibleIndex = 4;
            this.colDateHappened.Width = 87;
            // 
            // colLocation
            // 
            this.colLocation.FieldName = "Location";
            this.colLocation.Name = "colLocation";
            this.colLocation.OptionsColumn.AllowEdit = false;
            this.colLocation.OptionsColumn.AllowFocus = false;
            this.colLocation.OptionsColumn.ReadOnly = true;
            this.colLocation.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLocation.Visible = true;
            this.colLocation.VisibleIndex = 5;
            this.colLocation.Width = 233;
            // 
            // colRepairDueID
            // 
            this.colRepairDueID.FieldName = "RepairDueID";
            this.colRepairDueID.Name = "colRepairDueID";
            this.colRepairDueID.OptionsColumn.AllowEdit = false;
            this.colRepairDueID.OptionsColumn.AllowFocus = false;
            this.colRepairDueID.OptionsColumn.ReadOnly = true;
            this.colRepairDueID.OptionsColumn.ShowInCustomizationForm = false;
            this.colRepairDueID.OptionsColumn.ShowInExpressionEditor = false;
            this.colRepairDueID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRepairDueID.Width = 79;
            // 
            // colRepairDue
            // 
            this.colRepairDue.FieldName = "RepairDue";
            this.colRepairDue.Name = "colRepairDue";
            this.colRepairDue.OptionsColumn.AllowEdit = false;
            this.colRepairDue.OptionsColumn.AllowFocus = false;
            this.colRepairDue.OptionsColumn.ReadOnly = true;
            this.colRepairDue.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRepairDue.Visible = true;
            this.colRepairDue.VisibleIndex = 6;
            this.colRepairDue.Width = 91;
            // 
            // colSeverityID
            // 
            this.colSeverityID.FieldName = "SeverityID";
            this.colSeverityID.Name = "colSeverityID";
            this.colSeverityID.OptionsColumn.AllowEdit = false;
            this.colSeverityID.OptionsColumn.AllowFocus = false;
            this.colSeverityID.OptionsColumn.ReadOnly = true;
            this.colSeverityID.OptionsColumn.ShowInCustomizationForm = false;
            this.colSeverityID.OptionsColumn.ShowInExpressionEditor = false;
            this.colSeverityID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colSeverity
            // 
            this.colSeverity.FieldName = "Severity";
            this.colSeverity.Name = "colSeverity";
            this.colSeverity.OptionsColumn.AllowEdit = false;
            this.colSeverity.OptionsColumn.AllowFocus = false;
            this.colSeverity.OptionsColumn.ReadOnly = true;
            this.colSeverity.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSeverity.Visible = true;
            this.colSeverity.VisibleIndex = 7;
            this.colSeverity.Width = 92;
            // 
            // colWitness
            // 
            this.colWitness.FieldName = "Witness";
            this.colWitness.Name = "colWitness";
            this.colWitness.OptionsColumn.AllowEdit = false;
            this.colWitness.OptionsColumn.AllowFocus = false;
            this.colWitness.OptionsColumn.ReadOnly = true;
            this.colWitness.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWitness.Visible = true;
            this.colWitness.VisibleIndex = 8;
            this.colWitness.Width = 248;
            // 
            // colKeeperAtFaultID
            // 
            this.colKeeperAtFaultID.FieldName = "KeeperAtFaultID";
            this.colKeeperAtFaultID.Name = "colKeeperAtFaultID";
            this.colKeeperAtFaultID.OptionsColumn.AllowEdit = false;
            this.colKeeperAtFaultID.OptionsColumn.AllowFocus = false;
            this.colKeeperAtFaultID.OptionsColumn.ReadOnly = true;
            this.colKeeperAtFaultID.OptionsColumn.ShowInCustomizationForm = false;
            this.colKeeperAtFaultID.OptionsColumn.ShowInExpressionEditor = false;
            this.colKeeperAtFaultID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colKeeperAtFaultID.Width = 101;
            // 
            // colKeeperAtFault
            // 
            this.colKeeperAtFault.FieldName = "KeeperAtFault";
            this.colKeeperAtFault.Name = "colKeeperAtFault";
            this.colKeeperAtFault.OptionsColumn.AllowEdit = false;
            this.colKeeperAtFault.OptionsColumn.AllowFocus = false;
            this.colKeeperAtFault.OptionsColumn.ReadOnly = true;
            this.colKeeperAtFault.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colKeeperAtFault.Visible = true;
            this.colKeeperAtFault.VisibleIndex = 9;
            this.colKeeperAtFault.Width = 93;
            // 
            // colLegalActionID
            // 
            this.colLegalActionID.FieldName = "LegalActionID";
            this.colLegalActionID.Name = "colLegalActionID";
            this.colLegalActionID.OptionsColumn.AllowEdit = false;
            this.colLegalActionID.OptionsColumn.AllowFocus = false;
            this.colLegalActionID.OptionsColumn.ReadOnly = true;
            this.colLegalActionID.OptionsColumn.ShowInCustomizationForm = false;
            this.colLegalActionID.OptionsColumn.ShowInExpressionEditor = false;
            this.colLegalActionID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLegalActionID.Width = 84;
            // 
            // colLegalAction
            // 
            this.colLegalAction.FieldName = "LegalAction";
            this.colLegalAction.Name = "colLegalAction";
            this.colLegalAction.OptionsColumn.AllowEdit = false;
            this.colLegalAction.OptionsColumn.AllowFocus = false;
            this.colLegalAction.OptionsColumn.ReadOnly = true;
            this.colLegalAction.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLegalAction.Visible = true;
            this.colLegalAction.VisibleIndex = 10;
            this.colLegalAction.Width = 119;
            // 
            // colFollowUpDate
            // 
            this.colFollowUpDate.FieldName = "FollowUpDate";
            this.colFollowUpDate.Name = "colFollowUpDate";
            this.colFollowUpDate.OptionsColumn.AllowEdit = false;
            this.colFollowUpDate.OptionsColumn.AllowFocus = false;
            this.colFollowUpDate.OptionsColumn.ReadOnly = true;
            this.colFollowUpDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colFollowUpDate.Visible = true;
            this.colFollowUpDate.VisibleIndex = 11;
            this.colFollowUpDate.Width = 99;
            // 
            // colCost
            // 
            this.colCost.ColumnEdit = this.moneyTextEdit5;
            this.colCost.FieldName = "Cost";
            this.colCost.Name = "colCost";
            this.colCost.OptionsColumn.AllowEdit = false;
            this.colCost.OptionsColumn.AllowFocus = false;
            this.colCost.OptionsColumn.ReadOnly = true;
            this.colCost.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCost.Visible = true;
            this.colCost.VisibleIndex = 12;
            this.colCost.Width = 84;
            // 
            // moneyTextEdit5
            // 
            this.moneyTextEdit5.AutoHeight = false;
            this.moneyTextEdit5.DisplayFormat.FormatString = "c2";
            this.moneyTextEdit5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.moneyTextEdit5.Name = "moneyTextEdit5";
            // 
            // colNotes4
            // 
            this.colNotes4.ColumnEdit = this.incidentNotesMemoEdit;
            this.colNotes4.FieldName = "Notes";
            this.colNotes4.Name = "colNotes4";
            this.colNotes4.OptionsColumn.AllowEdit = false;
            this.colNotes4.OptionsColumn.AllowFocus = false;
            this.colNotes4.OptionsColumn.ReadOnly = true;
            this.colNotes4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNotes4.Visible = true;
            this.colNotes4.VisibleIndex = 13;
            this.colNotes4.Width = 472;
            // 
            // incidentNotesMemoEdit
            // 
            this.incidentNotesMemoEdit.Name = "incidentNotesMemoEdit";
            // 
            // colMode3
            // 
            this.colMode3.FieldName = "Mode";
            this.colMode3.Name = "colMode3";
            this.colMode3.OptionsColumn.AllowEdit = false;
            this.colMode3.OptionsColumn.AllowFocus = false;
            this.colMode3.OptionsColumn.ReadOnly = true;
            this.colMode3.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode3.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID3
            // 
            this.colRecordID3.FieldName = "RecordID";
            this.colRecordID3.Name = "colRecordID3";
            this.colRecordID3.OptionsColumn.AllowEdit = false;
            this.colRecordID3.OptionsColumn.AllowFocus = false;
            this.colRecordID3.OptionsColumn.ReadOnly = true;
            this.colRecordID3.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID3.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // speedingTabPage
            // 
            this.speedingTabPage.Controls.Add(this.speedingGridControl);
            this.speedingTabPage.Name = "speedingTabPage";
            this.speedingTabPage.Size = new System.Drawing.Size(1290, 200);
            this.speedingTabPage.Text = "Speeding Data";
            // 
            // speedingGridControl
            // 
            this.speedingGridControl.DataSource = this.spAS11105SpeedingItemBindingSource;
            this.speedingGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.speedingGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.speedingGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.speedingGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.speedingGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.speedingGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.speedingGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.speedingGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.speedingGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.speedingGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.speedingGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.speedingGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.speedingGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Add To Incident", "insert"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, false, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, false, "Delete Selected Record(s)", "delete")});
            this.speedingGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.speedingGridControl.Location = new System.Drawing.Point(0, 0);
            this.speedingGridControl.MainView = this.speedingGridView;
            this.speedingGridControl.MenuManager = this.barManager1;
            this.speedingGridControl.Name = "speedingGridControl";
            this.speedingGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.ceReported});
            this.speedingGridControl.Size = new System.Drawing.Size(1290, 200);
            this.speedingGridControl.TabIndex = 2;
            this.speedingGridControl.UseEmbeddedNavigator = true;
            this.speedingGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.speedingGridView});
            // 
            // spAS11105SpeedingItemBindingSource
            // 
            this.spAS11105SpeedingItemBindingSource.DataMember = "sp_AS_11105_Speeding_Item";
            this.spAS11105SpeedingItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // speedingGridView
            // 
            this.speedingGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSpeedingID,
            this.colEquipmentReference17,
            this.colVID,
            this.colEquipmentID17,
            this.colSpeed,
            this.colSpeedMPH1,
            this.colTravelTime,
            this.colRoadTypeID,
            this.colRoadType,
            this.colPlaceName1,
            this.colDistrict1,
            this.colReported,
            this.colMode17,
            this.colRecordID17});
            this.speedingGridView.GridControl = this.speedingGridControl;
            this.speedingGridView.Name = "speedingGridView";
            this.speedingGridView.OptionsCustomization.AllowFilter = false;
            this.speedingGridView.OptionsCustomization.AllowGroup = false;
            this.speedingGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.speedingGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.speedingGridView.OptionsLayout.StoreAppearance = true;
            this.speedingGridView.OptionsSelection.MultiSelect = true;
            this.speedingGridView.OptionsView.ColumnAutoWidth = false;
            this.speedingGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.speedingGridView.OptionsView.ShowGroupPanel = false;
            this.speedingGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.speedingGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.speedingGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.speedingGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.speedingGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.speedingGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.speedingGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colSpeedingID
            // 
            this.colSpeedingID.FieldName = "SpeedingID";
            this.colSpeedingID.Image = ((System.Drawing.Image)(resources.GetObject("colSpeedingID.Image")));
            this.colSpeedingID.Name = "colSpeedingID";
            this.colSpeedingID.OptionsColumn.AllowEdit = false;
            this.colSpeedingID.OptionsColumn.AllowFocus = false;
            this.colSpeedingID.OptionsColumn.ReadOnly = true;
            this.colSpeedingID.OptionsColumn.ShowInCustomizationForm = false;
            this.colSpeedingID.OptionsColumn.ShowInExpressionEditor = false;
            this.colSpeedingID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colEquipmentReference17
            // 
            this.colEquipmentReference17.FieldName = "EquipmentReference";
            this.colEquipmentReference17.Name = "colEquipmentReference17";
            this.colEquipmentReference17.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference17.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference17.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference17.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference17.Visible = true;
            this.colEquipmentReference17.VisibleIndex = 0;
            this.colEquipmentReference17.Width = 138;
            // 
            // colVID
            // 
            this.colVID.FieldName = "VID";
            this.colVID.Name = "colVID";
            this.colVID.OptionsColumn.AllowEdit = false;
            this.colVID.OptionsColumn.AllowFocus = false;
            this.colVID.OptionsColumn.ReadOnly = true;
            this.colVID.OptionsColumn.ShowInCustomizationForm = false;
            this.colVID.OptionsColumn.ShowInExpressionEditor = false;
            this.colVID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colEquipmentID17
            // 
            this.colEquipmentID17.FieldName = "EquipmentID";
            this.colEquipmentID17.Name = "colEquipmentID17";
            this.colEquipmentID17.OptionsColumn.AllowEdit = false;
            this.colEquipmentID17.OptionsColumn.AllowFocus = false;
            this.colEquipmentID17.OptionsColumn.ReadOnly = true;
            this.colEquipmentID17.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID17.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID17.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID17.Width = 76;
            // 
            // colSpeed
            // 
            this.colSpeed.FieldName = "Speed";
            this.colSpeed.Name = "colSpeed";
            this.colSpeed.OptionsColumn.AllowEdit = false;
            this.colSpeed.OptionsColumn.AllowFocus = false;
            this.colSpeed.OptionsColumn.ReadOnly = true;
            this.colSpeed.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colSpeedMPH1
            // 
            this.colSpeedMPH1.FieldName = "SpeedMPH";
            this.colSpeedMPH1.Name = "colSpeedMPH1";
            this.colSpeedMPH1.OptionsColumn.AllowEdit = false;
            this.colSpeedMPH1.OptionsColumn.AllowFocus = false;
            this.colSpeedMPH1.OptionsColumn.ReadOnly = true;
            this.colSpeedMPH1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSpeedMPH1.Visible = true;
            this.colSpeedMPH1.VisibleIndex = 1;
            this.colSpeedMPH1.Width = 87;
            // 
            // colTravelTime
            // 
            this.colTravelTime.FieldName = "TravelTime";
            this.colTravelTime.Name = "colTravelTime";
            this.colTravelTime.OptionsColumn.AllowEdit = false;
            this.colTravelTime.OptionsColumn.AllowFocus = false;
            this.colTravelTime.OptionsColumn.ReadOnly = true;
            this.colTravelTime.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTravelTime.Visible = true;
            this.colTravelTime.VisibleIndex = 2;
            // 
            // colRoadTypeID
            // 
            this.colRoadTypeID.FieldName = "RoadTypeID";
            this.colRoadTypeID.Name = "colRoadTypeID";
            this.colRoadTypeID.OptionsColumn.AllowEdit = false;
            this.colRoadTypeID.OptionsColumn.AllowFocus = false;
            this.colRoadTypeID.OptionsColumn.ReadOnly = true;
            this.colRoadTypeID.OptionsColumn.ShowInCustomizationForm = false;
            this.colRoadTypeID.OptionsColumn.ShowInExpressionEditor = false;
            this.colRoadTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRoadTypeID.Width = 78;
            // 
            // colRoadType
            // 
            this.colRoadType.FieldName = "RoadType";
            this.colRoadType.Name = "colRoadType";
            this.colRoadType.OptionsColumn.AllowEdit = false;
            this.colRoadType.OptionsColumn.AllowFocus = false;
            this.colRoadType.OptionsColumn.ReadOnly = true;
            this.colRoadType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRoadType.Visible = true;
            this.colRoadType.VisibleIndex = 3;
            this.colRoadType.Width = 121;
            // 
            // colPlaceName1
            // 
            this.colPlaceName1.FieldName = "PlaceName";
            this.colPlaceName1.Name = "colPlaceName1";
            this.colPlaceName1.OptionsColumn.AllowEdit = false;
            this.colPlaceName1.OptionsColumn.AllowFocus = false;
            this.colPlaceName1.OptionsColumn.ReadOnly = true;
            this.colPlaceName1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPlaceName1.Visible = true;
            this.colPlaceName1.VisibleIndex = 4;
            this.colPlaceName1.Width = 174;
            // 
            // colDistrict1
            // 
            this.colDistrict1.FieldName = "District";
            this.colDistrict1.Name = "colDistrict1";
            this.colDistrict1.OptionsColumn.AllowEdit = false;
            this.colDistrict1.OptionsColumn.AllowFocus = false;
            this.colDistrict1.OptionsColumn.ReadOnly = true;
            this.colDistrict1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDistrict1.Visible = true;
            this.colDistrict1.VisibleIndex = 5;
            this.colDistrict1.Width = 118;
            // 
            // colReported
            // 
            this.colReported.ColumnEdit = this.ceReported;
            this.colReported.FieldName = "Reported";
            this.colReported.Name = "colReported";
            this.colReported.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colReported.Visible = true;
            this.colReported.VisibleIndex = 6;
            this.colReported.Width = 64;
            // 
            // ceReported
            // 
            this.ceReported.AutoHeight = false;
            this.ceReported.Caption = "Check";
            this.ceReported.Name = "ceReported";
            // 
            // colMode17
            // 
            this.colMode17.FieldName = "Mode";
            this.colMode17.Name = "colMode17";
            this.colMode17.OptionsColumn.AllowEdit = false;
            this.colMode17.OptionsColumn.AllowFocus = false;
            this.colMode17.OptionsColumn.ReadOnly = true;
            this.colMode17.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode17.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode17.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID17
            // 
            this.colRecordID17.FieldName = "RecordID";
            this.colRecordID17.Name = "colRecordID17";
            this.colRecordID17.OptionsColumn.AllowEdit = false;
            this.colRecordID17.OptionsColumn.AllowFocus = false;
            this.colRecordID17.OptionsColumn.ReadOnly = true;
            this.colRecordID17.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID17.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID17.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // txtShortDescription
            // 
            this.txtShortDescription.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11008PlantItemBindingSource, "ShortDescription", true));
            this.txtShortDescription.Location = new System.Drawing.Point(191, 467);
            this.txtShortDescription.MenuManager = this.barManager1;
            this.txtShortDescription.Name = "txtShortDescription";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtShortDescription, true);
            this.txtShortDescription.Size = new System.Drawing.Size(451, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtShortDescription, optionsSpelling4);
            this.txtShortDescription.StyleController = this.equipmentDataLayoutControl;
            this.txtShortDescription.TabIndex = 135;
            this.txtShortDescription.Tag = "Plant Short Description";
            this.txtShortDescription.Validating += new System.ComponentModel.CancelEventHandler(this.txtShortDescription_Validating);
            // 
            // lueDepreciationSetting
            // 
            this.lueDepreciationSetting.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11066DepreciationSettingsItemBindingSource, "DepreciationSettingID", true));
            this.lueDepreciationSetting.Location = new System.Drawing.Point(448, 314);
            this.lueDepreciationSetting.MenuManager = this.barManager1;
            this.lueDepreciationSetting.Name = "lueDepreciationSetting";
            this.lueDepreciationSetting.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueDepreciationSetting.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("SettingName", "Setting Name", 150, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("OccurrenceFrequency", "Occurrence Frequency", 150, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("OccursUnitDescriptor", "Occurs Unit Descriptor", 150, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaximumOccurrence", "Maximum Occurrence", 150, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DayOfMonthApplyDepreciation", "Day Of Month To Apply Depreciation", 170, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("SetAsDefault", "Set As Default", 79, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("LastChanged", "Last Changed", 76, DevExpress.Utils.FormatType.DateTime, "dd/MM/yyyy", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueDepreciationSetting.Properties.DataSource = this.spAS11066DepreciationSettingsItemBindingSource;
            this.lueDepreciationSetting.Properties.DisplayMember = "SettingName";
            this.lueDepreciationSetting.Properties.NullText = "";
            this.lueDepreciationSetting.Properties.NullValuePrompt = "-- Please Select Depreciation Setting --";
            this.lueDepreciationSetting.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueDepreciationSetting.Properties.PopupWidth = 900;
            this.lueDepreciationSetting.Properties.ValueMember = "DepreciationSettingID";
            this.lueDepreciationSetting.Size = new System.Drawing.Size(54, 20);
            this.lueDepreciationSetting.StyleController = this.equipmentDataLayoutControl;
            this.lueDepreciationSetting.TabIndex = 134;
            this.lueDepreciationSetting.EditValueChanged += new System.EventHandler(this.lueDepreciationSetting_EditValueChanged);
            // 
            // spAS11066DepreciationSettingsItemBindingSource
            // 
            this.spAS11066DepreciationSettingsItemBindingSource.DataMember = "sp_AS_11066_Depreciation_Settings_Item";
            this.spAS11066DepreciationSettingsItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // lueVehicleCategoryID
            // 
            this.lueVehicleCategoryID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11005VehicleItemBindingSource, "VehicleCategoryID", true));
            this.lueVehicleCategoryID.Location = new System.Drawing.Point(203, 523);
            this.lueVehicleCategoryID.MenuManager = this.barManager1;
            this.lueVehicleCategoryID.Name = "lueVehicleCategoryID";
            this.lueVehicleCategoryID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueVehicleCategoryID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Vehicle  Category", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueVehicleCategoryID.Properties.DataSource = this.spAS11000PLVehicleCategoryBindingSource;
            this.lueVehicleCategoryID.Properties.DisplayMember = "Value";
            this.lueVehicleCategoryID.Properties.NullText = "";
            this.lueVehicleCategoryID.Properties.NullValuePrompt = "-- Please Select Vehicle Category --";
            this.lueVehicleCategoryID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueVehicleCategoryID.Properties.ValueMember = "PickListID";
            this.lueVehicleCategoryID.Size = new System.Drawing.Size(624, 20);
            this.lueVehicleCategoryID.StyleController = this.equipmentDataLayoutControl;
            this.lueVehicleCategoryID.TabIndex = 133;
            this.lueVehicleCategoryID.Tag = "Vehicle Category";
            this.lueVehicleCategoryID.EditValueChanged += new System.EventHandler(this.lueVehicleCategoryID_EditValueChanged);
            this.lueVehicleCategoryID.Validating += new System.ComponentModel.CancelEventHandler(this.lueVehicleCategoryID_Validating);
            // 
            // spAS11005VehicleItemBindingSource
            // 
            this.spAS11005VehicleItemBindingSource.DataMember = "sp_AS_11005_Vehicle_Item";
            this.spAS11005VehicleItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spAS11000PLVehicleCategoryBindingSource
            // 
            this.spAS11000PLVehicleCategoryBindingSource.DataMember = "sp_AS_11000_PL_Vehicle_Category";
            this.spAS11000PLVehicleCategoryBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // deliveryDateDateEdit
            // 
            this.deliveryDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11002EquipmentItemBindingSource, "DeliveryDate", true));
            this.deliveryDateDateEdit.EditValue = null;
            this.deliveryDateDateEdit.Location = new System.Drawing.Point(448, 175);
            this.deliveryDateDateEdit.MenuManager = this.barManager1;
            this.deliveryDateDateEdit.Name = "deliveryDateDateEdit";
            this.deliveryDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deliveryDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deliveryDateDateEdit.Size = new System.Drawing.Size(54, 20);
            this.deliveryDateDateEdit.StyleController = this.equipmentDataLayoutControl;
            this.deliveryDateDateEdit.TabIndex = 131;
            this.deliveryDateDateEdit.Tag = "Delivery Date";
            this.deliveryDateDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.deliveryDateDateEdit_Validating);
            // 
            // spnDayOfMonthApplyDepreciation
            // 
            this.spnDayOfMonthApplyDepreciation.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11002EquipmentItemBindingSource, "DayOfMonthApplyDepreciation", true));
            this.spnDayOfMonthApplyDepreciation.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnDayOfMonthApplyDepreciation.Location = new System.Drawing.Point(448, 338);
            this.spnDayOfMonthApplyDepreciation.MenuManager = this.barManager1;
            this.spnDayOfMonthApplyDepreciation.Name = "spnDayOfMonthApplyDepreciation";
            this.spnDayOfMonthApplyDepreciation.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnDayOfMonthApplyDepreciation.Properties.MaxLength = 31;
            this.spnDayOfMonthApplyDepreciation.Properties.MaxValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spnDayOfMonthApplyDepreciation.Size = new System.Drawing.Size(54, 20);
            this.spnDayOfMonthApplyDepreciation.StyleController = this.equipmentDataLayoutControl;
            this.spnDayOfMonthApplyDepreciation.TabIndex = 130;
            // 
            // spnMaximumOccurrence
            // 
            this.spnMaximumOccurrence.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11002EquipmentItemBindingSource, "MaximumOccurrence", true));
            this.spnMaximumOccurrence.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnMaximumOccurrence.Location = new System.Drawing.Point(448, 386);
            this.spnMaximumOccurrence.MenuManager = this.barManager1;
            this.spnMaximumOccurrence.Name = "spnMaximumOccurrence";
            this.spnMaximumOccurrence.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnMaximumOccurrence.Properties.MaxLength = 1000;
            this.spnMaximumOccurrence.Size = new System.Drawing.Size(54, 20);
            this.spnMaximumOccurrence.StyleController = this.equipmentDataLayoutControl;
            this.spnMaximumOccurrence.TabIndex = 129;
            // 
            // lueOccursUnitDescriptor
            // 
            this.lueOccursUnitDescriptor.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11002EquipmentItemBindingSource, "OccursUnitDescriptor", true));
            this.lueOccursUnitDescriptor.Location = new System.Drawing.Point(448, 362);
            this.lueOccursUnitDescriptor.MenuManager = this.barManager1;
            this.lueOccursUnitDescriptor.Name = "lueOccursUnitDescriptor";
            this.lueOccursUnitDescriptor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueOccursUnitDescriptor.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Unit Descriptor", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueOccursUnitDescriptor.Properties.DataSource = this.spAS11000PLUnitDescriptorBindingSource;
            this.lueOccursUnitDescriptor.Properties.DisplayMember = "Value";
            this.lueOccursUnitDescriptor.Properties.NullText = "";
            this.lueOccursUnitDescriptor.Properties.NullValuePrompt = "-- Please Select Unit ---";
            this.lueOccursUnitDescriptor.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueOccursUnitDescriptor.Properties.ValueMember = "PickListID";
            this.lueOccursUnitDescriptor.Size = new System.Drawing.Size(54, 20);
            this.lueOccursUnitDescriptor.StyleController = this.equipmentDataLayoutControl;
            this.lueOccursUnitDescriptor.TabIndex = 128;
            // 
            // spAS11000PLUnitDescriptorBindingSource
            // 
            this.spAS11000PLUnitDescriptorBindingSource.DataMember = "sp_AS_11000_PL_Unit_Descriptor";
            this.spAS11000PLUnitDescriptorBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // ceArchive
            // 
            this.ceArchive.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11002EquipmentItemBindingSource, "Archive", true));
            this.ceArchive.Location = new System.Drawing.Point(281, 223);
            this.ceArchive.MenuManager = this.barManager1;
            this.ceArchive.Name = "ceArchive";
            this.ceArchive.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.ceArchive.Properties.Caption = "(Tick to Archive)";
            this.ceArchive.Properties.ValueChecked = ((long)(1));
            this.ceArchive.Properties.ValueUnchecked = 0;
            this.ceArchive.Size = new System.Drawing.Size(221, 19);
            this.ceArchive.StyleController = this.equipmentDataLayoutControl;
            this.ceArchive.TabIndex = 33;
            this.ceArchive.CheckedChanged += new System.EventHandler(this.ceArchive_CheckedChanged);
            // 
            // equipmentDataNavigator
            // 
            this.equipmentDataNavigator.Buttons.Append.Visible = false;
            this.equipmentDataNavigator.Buttons.CancelEdit.Visible = false;
            this.equipmentDataNavigator.Buttons.EndEdit.Visible = false;
            this.equipmentDataNavigator.Buttons.Remove.Visible = false;
            this.equipmentDataNavigator.DataSource = this.spAS11002EquipmentItemBindingSource;
            this.equipmentDataNavigator.Location = new System.Drawing.Point(276, 12);
            this.equipmentDataNavigator.Name = "equipmentDataNavigator";
            this.equipmentDataNavigator.Size = new System.Drawing.Size(177, 19);
            this.equipmentDataNavigator.StyleController = this.equipmentDataLayoutControl;
            this.equipmentDataNavigator.TabIndex = 120;
            this.equipmentDataNavigator.Text = "equipmentDataNavigator";
            this.equipmentDataNavigator.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.equipmentDataNavigator.TextStringFormat = "Equipment {0} of {1}";
            // 
            // spnQuantity
            // 
            this.spnQuantity.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp_AS_11029_Office_ItemBindingSource, "Quantity", true));
            this.spnQuantity.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spnQuantity.Location = new System.Drawing.Point(839, 467);
            this.spnQuantity.MenuManager = this.barManager1;
            this.spnQuantity.Name = "spnQuantity";
            this.spnQuantity.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnQuantity.Properties.MaxValue = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.spnQuantity.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spnQuantity.Size = new System.Drawing.Size(482, 20);
            this.spnQuantity.StyleController = this.equipmentDataLayoutControl;
            this.spnQuantity.TabIndex = 119;
            // 
            // lueSoftwareProduct_ID
            // 
            this.lueSoftwareProduct_ID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11035SoftwareItemBindingSource, "ProductID", true));
            this.lueSoftwareProduct_ID.Location = new System.Drawing.Point(191, 467);
            this.lueSoftwareProduct_ID.MenuManager = this.barManager1;
            this.lueSoftwareProduct_ID.Name = "lueSoftwareProduct_ID";
            this.lueSoftwareProduct_ID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueSoftwareProduct_ID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Product", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueSoftwareProduct_ID.Properties.DataSource = this.spAS11000PLSoftwareProductBindingSource;
            this.lueSoftwareProduct_ID.Properties.DisplayMember = "Value";
            this.lueSoftwareProduct_ID.Properties.NullText = "";
            this.lueSoftwareProduct_ID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueSoftwareProduct_ID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueSoftwareProduct_ID.Properties.ValueMember = "PickListID";
            this.lueSoftwareProduct_ID.Size = new System.Drawing.Size(703, 20);
            this.lueSoftwareProduct_ID.StyleController = this.equipmentDataLayoutControl;
            this.lueSoftwareProduct_ID.TabIndex = 96;
            this.lueSoftwareProduct_ID.Validating += new System.ComponentModel.CancelEventHandler(this.product_IDLookUpEdit_Validating);
            // 
            // spAS11035SoftwareItemBindingSource
            // 
            this.spAS11035SoftwareItemBindingSource.DataMember = "sp_AS_11035_Software_Item";
            this.spAS11035SoftwareItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spAS11000PLSoftwareProductBindingSource
            // 
            this.spAS11000PLSoftwareProductBindingSource.DataMember = "sp_AS_11000_PL_Software_Product";
            this.spAS11000PLSoftwareProductBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // lueSoftwareVersion_ID
            // 
            this.lueSoftwareVersion_ID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11035SoftwareItemBindingSource, "VersionID", true));
            this.lueSoftwareVersion_ID.Location = new System.Drawing.Point(191, 491);
            this.lueSoftwareVersion_ID.MenuManager = this.barManager1;
            this.lueSoftwareVersion_ID.Name = "lueSoftwareVersion_ID";
            this.lueSoftwareVersion_ID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueSoftwareVersion_ID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Version", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueSoftwareVersion_ID.Properties.DataSource = this.spAS11000PLSoftwareVersionBindingSource;
            this.lueSoftwareVersion_ID.Properties.DisplayMember = "Value";
            this.lueSoftwareVersion_ID.Properties.NullText = "";
            this.lueSoftwareVersion_ID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueSoftwareVersion_ID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueSoftwareVersion_ID.Properties.ValueMember = "PickListID";
            this.lueSoftwareVersion_ID.Size = new System.Drawing.Size(703, 20);
            this.lueSoftwareVersion_ID.StyleController = this.equipmentDataLayoutControl;
            this.lueSoftwareVersion_ID.TabIndex = 98;
            this.lueSoftwareVersion_ID.Validating += new System.ComponentModel.CancelEventHandler(this.version_IDLookUpEdit_Validating);
            // 
            // spAS11000PLSoftwareVersionBindingSource
            // 
            this.spAS11000PLSoftwareVersionBindingSource.DataMember = "sp_AS_11000_PL_Software_Version";
            this.spAS11000PLSoftwareVersionBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // deSoftwareYear
            // 
            this.deSoftwareYear.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11035SoftwareItemBindingSource, "Year", true));
            this.deSoftwareYear.EditValue = null;
            this.deSoftwareYear.Location = new System.Drawing.Point(1065, 491);
            this.deSoftwareYear.MenuManager = this.barManager1;
            this.deSoftwareYear.Name = "deSoftwareYear";
            this.deSoftwareYear.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deSoftwareYear.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deSoftwareYear.Size = new System.Drawing.Size(256, 20);
            this.deSoftwareYear.StyleController = this.equipmentDataLayoutControl;
            this.deSoftwareYear.TabIndex = 100;
            // 
            // txtSoftwareLicence
            // 
            this.txtSoftwareLicence.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11035SoftwareItemBindingSource, "Licence", true));
            this.txtSoftwareLicence.Location = new System.Drawing.Point(191, 515);
            this.txtSoftwareLicence.MenuManager = this.barManager1;
            this.txtSoftwareLicence.Name = "txtSoftwareLicence";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtSoftwareLicence, true);
            this.txtSoftwareLicence.Size = new System.Drawing.Size(703, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtSoftwareLicence, optionsSpelling5);
            this.txtSoftwareLicence.StyleController = this.equipmentDataLayoutControl;
            this.txtSoftwareLicence.TabIndex = 102;
            this.txtSoftwareLicence.Validating += new System.ComponentModel.CancelEventHandler(this.licenceTextEdit_Validating);
            // 
            // lueSoftwareParent_Program
            // 
            this.lueSoftwareParent_Program.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11035SoftwareItemBindingSource, "ParentProgram", true));
            this.lueSoftwareParent_Program.Location = new System.Drawing.Point(1065, 467);
            this.lueSoftwareParent_Program.MenuManager = this.barManager1;
            this.lueSoftwareParent_Program.Name = "lueSoftwareParent_Program";
            this.lueSoftwareParent_Program.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueSoftwareParent_Program.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Product", "Product", 47, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Version", "Version", 45, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Year", "Year", 32, DevExpress.Utils.FormatType.DateTime, "dd/MM/yyyy", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Acquisation_Method", "Acquisation_Method", 107, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Quantity_Limit", "Quantity_Limit", 79, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far)});
            this.lueSoftwareParent_Program.Properties.DataSource = this.spAS11035SoftwareItemBindingSource;
            this.lueSoftwareParent_Program.Properties.DisplayMember = "Product";
            this.lueSoftwareParent_Program.Properties.NullText = "";
            this.lueSoftwareParent_Program.Properties.NullValuePrompt = "-- Please Select --";
            this.lueSoftwareParent_Program.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueSoftwareParent_Program.Properties.ValueMember = "EquipmentID";
            this.lueSoftwareParent_Program.Size = new System.Drawing.Size(256, 20);
            this.lueSoftwareParent_Program.StyleController = this.equipmentDataLayoutControl;
            this.lueSoftwareParent_Program.TabIndex = 104;
            this.lueSoftwareParent_Program.Validating += new System.ComponentModel.CancelEventHandler(this.parent_ProgramLookUpEdit_Validating);
            // 
            // lueSoftwareAcquisation_Method_ID
            // 
            this.lueSoftwareAcquisation_Method_ID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11035SoftwareItemBindingSource, "AcquisationMethodID", true));
            this.lueSoftwareAcquisation_Method_ID.Location = new System.Drawing.Point(1065, 515);
            this.lueSoftwareAcquisation_Method_ID.MenuManager = this.barManager1;
            this.lueSoftwareAcquisation_Method_ID.Name = "lueSoftwareAcquisation_Method_ID";
            this.lueSoftwareAcquisation_Method_ID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueSoftwareAcquisation_Method_ID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Acquisation Method", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueSoftwareAcquisation_Method_ID.Properties.DataSource = this.spAS11000PLSoftwareAcquisationMethodBindingSource;
            this.lueSoftwareAcquisation_Method_ID.Properties.DisplayMember = "Value";
            this.lueSoftwareAcquisation_Method_ID.Properties.NullText = "";
            this.lueSoftwareAcquisation_Method_ID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueSoftwareAcquisation_Method_ID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueSoftwareAcquisation_Method_ID.Properties.ValueMember = "PickListID";
            this.lueSoftwareAcquisation_Method_ID.Size = new System.Drawing.Size(256, 20);
            this.lueSoftwareAcquisation_Method_ID.StyleController = this.equipmentDataLayoutControl;
            this.lueSoftwareAcquisation_Method_ID.TabIndex = 106;
            this.lueSoftwareAcquisation_Method_ID.Validating += new System.ComponentModel.CancelEventHandler(this.acquisation_Method_IDLookUpEdit_Validating);
            // 
            // spAS11000PLSoftwareAcquisationMethodBindingSource
            // 
            this.spAS11000PLSoftwareAcquisationMethodBindingSource.DataMember = "sp_AS_11000_PL_Software_Acquisation_Method";
            this.spAS11000PLSoftwareAcquisationMethodBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spnSoftwareQuantity_Limit
            // 
            this.spnSoftwareQuantity_Limit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11035SoftwareItemBindingSource, "QuantityLimit", true));
            this.spnSoftwareQuantity_Limit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnSoftwareQuantity_Limit.Location = new System.Drawing.Point(191, 539);
            this.spnSoftwareQuantity_Limit.MenuManager = this.barManager1;
            this.spnSoftwareQuantity_Limit.Name = "spnSoftwareQuantity_Limit";
            this.spnSoftwareQuantity_Limit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnSoftwareQuantity_Limit.Properties.MaxValue = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.spnSoftwareQuantity_Limit.Size = new System.Drawing.Size(703, 20);
            this.spnSoftwareQuantity_Limit.StyleController = this.equipmentDataLayoutControl;
            this.spnSoftwareQuantity_Limit.TabIndex = 108;
            // 
            // txtSoftwareLicence_Key
            // 
            this.txtSoftwareLicence_Key.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11035SoftwareItemBindingSource, "LicenceKey", true));
            this.txtSoftwareLicence_Key.Location = new System.Drawing.Point(1065, 539);
            this.txtSoftwareLicence_Key.MenuManager = this.barManager1;
            this.txtSoftwareLicence_Key.Name = "txtSoftwareLicence_Key";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtSoftwareLicence_Key, true);
            this.txtSoftwareLicence_Key.Size = new System.Drawing.Size(256, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtSoftwareLicence_Key, optionsSpelling6);
            this.txtSoftwareLicence_Key.StyleController = this.equipmentDataLayoutControl;
            this.txtSoftwareLicence_Key.TabIndex = 110;
            this.txtSoftwareLicence_Key.Validating += new System.ComponentModel.CancelEventHandler(this.licence_KeyTextEdit_Validating);
            // 
            // lueSoftwareLicence_Type_ID
            // 
            this.lueSoftwareLicence_Type_ID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11035SoftwareItemBindingSource, "LicenceTypeID", true));
            this.lueSoftwareLicence_Type_ID.Location = new System.Drawing.Point(191, 563);
            this.lueSoftwareLicence_Type_ID.MenuManager = this.barManager1;
            this.lueSoftwareLicence_Type_ID.Name = "lueSoftwareLicence_Type_ID";
            this.lueSoftwareLicence_Type_ID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueSoftwareLicence_Type_ID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Licence Type", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueSoftwareLicence_Type_ID.Properties.DataSource = this.spAS11000PLLicenceTypeBindingSource;
            this.lueSoftwareLicence_Type_ID.Properties.DisplayMember = "Value";
            this.lueSoftwareLicence_Type_ID.Properties.NullText = "";
            this.lueSoftwareLicence_Type_ID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueSoftwareLicence_Type_ID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueSoftwareLicence_Type_ID.Properties.ValueMember = "PickListID";
            this.lueSoftwareLicence_Type_ID.Size = new System.Drawing.Size(703, 20);
            this.lueSoftwareLicence_Type_ID.StyleController = this.equipmentDataLayoutControl;
            this.lueSoftwareLicence_Type_ID.TabIndex = 112;
            this.lueSoftwareLicence_Type_ID.Validating += new System.ComponentModel.CancelEventHandler(this.licence_Type_IDLookUpEdit_Validating);
            // 
            // spAS11000PLLicenceTypeBindingSource
            // 
            this.spAS11000PLLicenceTypeBindingSource.DataMember = "sp_AS_11000_PL_Licence Type";
            this.spAS11000PLLicenceTypeBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // deSoftwareValid_Until
            // 
            this.deSoftwareValid_Until.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11035SoftwareItemBindingSource, "ValidUntil", true));
            this.deSoftwareValid_Until.EditValue = null;
            this.deSoftwareValid_Until.Location = new System.Drawing.Point(1065, 563);
            this.deSoftwareValid_Until.MenuManager = this.barManager1;
            this.deSoftwareValid_Until.Name = "deSoftwareValid_Until";
            this.deSoftwareValid_Until.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deSoftwareValid_Until.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deSoftwareValid_Until.Size = new System.Drawing.Size(256, 20);
            this.deSoftwareValid_Until.StyleController = this.equipmentDataLayoutControl;
            this.deSoftwareValid_Until.TabIndex = 114;
            this.deSoftwareValid_Until.Validating += new System.ComponentModel.CancelEventHandler(this.valid_UntilDateEdit_Validating);
            // 
            // lueHardware_Type_ID
            // 
            this.lueHardware_Type_ID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11032HardwareItemBindingSource, "HardwareTypeID", true));
            this.lueHardware_Type_ID.Location = new System.Drawing.Point(191, 467);
            this.lueHardware_Type_ID.MenuManager = this.barManager1;
            this.lueHardware_Type_ID.Name = "lueHardware_Type_ID";
            this.lueHardware_Type_ID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueHardware_Type_ID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Hardware Type", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueHardware_Type_ID.Properties.DataSource = this.spAS11000PLHardwareTypeBindingSource;
            this.lueHardware_Type_ID.Properties.DisplayMember = "Value";
            this.lueHardware_Type_ID.Properties.NullText = "";
            this.lueHardware_Type_ID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueHardware_Type_ID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueHardware_Type_ID.Properties.ValueMember = "PickListID";
            this.lueHardware_Type_ID.Size = new System.Drawing.Size(476, 20);
            this.lueHardware_Type_ID.StyleController = this.equipmentDataLayoutControl;
            this.lueHardware_Type_ID.TabIndex = 89;
            this.lueHardware_Type_ID.Validating += new System.ComponentModel.CancelEventHandler(this.hardware_Type_IDLookUpEdit_Validating);
            // 
            // spAS11000PLHardwareTypeBindingSource
            // 
            this.spAS11000PLHardwareTypeBindingSource.DataMember = "sp_AS_11000_PL_Hardware_Type";
            this.spAS11000PLHardwareTypeBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // lueHardwareSpecification_ID
            // 
            this.lueHardwareSpecification_ID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11032HardwareItemBindingSource, "SpecificationID", true));
            this.lueHardwareSpecification_ID.Location = new System.Drawing.Point(838, 467);
            this.lueHardwareSpecification_ID.MenuManager = this.barManager1;
            this.lueHardwareSpecification_ID.Name = "lueHardwareSpecification_ID";
            this.lueHardwareSpecification_ID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueHardwareSpecification_ID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Specification", "Specification", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueHardwareSpecification_ID.Properties.DataSource = this.spAS11141HardwareSpecificationsItemBindingSource;
            this.lueHardwareSpecification_ID.Properties.DisplayMember = "Specification";
            this.lueHardwareSpecification_ID.Properties.NullText = "";
            this.lueHardwareSpecification_ID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueHardwareSpecification_ID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueHardwareSpecification_ID.Properties.ValueMember = "SpecificationID";
            this.lueHardwareSpecification_ID.Size = new System.Drawing.Size(483, 20);
            this.lueHardwareSpecification_ID.StyleController = this.equipmentDataLayoutControl;
            this.lueHardwareSpecification_ID.TabIndex = 91;
            this.lueHardwareSpecification_ID.Validating += new System.ComponentModel.CancelEventHandler(this.specification_IDLookUpEdit_Validating);
            // 
            // spAS11141HardwareSpecificationsItemBindingSource
            // 
            this.spAS11141HardwareSpecificationsItemBindingSource.DataMember = "sp_AS_11141_Hardware_Specifications_Item";
            this.spAS11141HardwareSpecificationsItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // txtHardwareDefault_Password
            // 
            this.txtHardwareDefault_Password.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11032HardwareItemBindingSource, "DefaultPassword", true));
            this.txtHardwareDefault_Password.Location = new System.Drawing.Point(191, 515);
            this.txtHardwareDefault_Password.MenuManager = this.barManager1;
            this.txtHardwareDefault_Password.Name = "txtHardwareDefault_Password";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtHardwareDefault_Password, true);
            this.txtHardwareDefault_Password.Size = new System.Drawing.Size(1130, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtHardwareDefault_Password, optionsSpelling7);
            this.txtHardwareDefault_Password.StyleController = this.equipmentDataLayoutControl;
            this.txtHardwareDefault_Password.TabIndex = 93;
            // 
            // lueGadget_Type_ID
            // 
            this.lueGadget_Type_ID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11026GadgetItemBindingSource, "GadgetTypeID", true));
            this.lueGadget_Type_ID.Location = new System.Drawing.Point(203, 523);
            this.lueGadget_Type_ID.MenuManager = this.barManager1;
            this.lueGadget_Type_ID.Name = "lueGadget_Type_ID";
            this.lueGadget_Type_ID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueGadget_Type_ID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Gadget Type", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueGadget_Type_ID.Properties.DataSource = this.spAS11000PLGadgetTypeBindingSource;
            this.lueGadget_Type_ID.Properties.DisplayMember = "Value";
            this.lueGadget_Type_ID.Properties.NullText = "";
            this.lueGadget_Type_ID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueGadget_Type_ID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueGadget_Type_ID.Properties.ValueMember = "PickListID";
            this.lueGadget_Type_ID.Size = new System.Drawing.Size(471, 20);
            this.lueGadget_Type_ID.StyleController = this.equipmentDataLayoutControl;
            this.lueGadget_Type_ID.TabIndex = 82;
            this.lueGadget_Type_ID.Validating += new System.ComponentModel.CancelEventHandler(this.gadget_Type_IDLookUpEdit_Validating);
            // 
            // spAS11000PLGadgetTypeBindingSource
            // 
            this.spAS11000PLGadgetTypeBindingSource.DataMember = "sp_AS_11000_PL_Gadget_Type";
            this.spAS11000PLGadgetTypeBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // lueGadgetCasing_ID
            // 
            this.lueGadgetCasing_ID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11026GadgetItemBindingSource, "CasingID", true));
            this.lueGadgetCasing_ID.Location = new System.Drawing.Point(845, 499);
            this.lueGadgetCasing_ID.MenuManager = this.barManager1;
            this.lueGadgetCasing_ID.Name = "lueGadgetCasing_ID";
            this.lueGadgetCasing_ID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueGadgetCasing_ID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Casing", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueGadgetCasing_ID.Properties.DataSource = this.spAS11000PLCasingBindingSource;
            this.lueGadgetCasing_ID.Properties.DisplayMember = "Value";
            this.lueGadgetCasing_ID.Properties.NullText = "";
            this.lueGadgetCasing_ID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueGadgetCasing_ID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueGadgetCasing_ID.Properties.ValueMember = "PickListID";
            this.lueGadgetCasing_ID.Size = new System.Drawing.Size(464, 20);
            this.lueGadgetCasing_ID.StyleController = this.equipmentDataLayoutControl;
            this.lueGadgetCasing_ID.TabIndex = 86;
            this.lueGadgetCasing_ID.Validating += new System.ComponentModel.CancelEventHandler(this.casing_IDLookUpEdit_Validating);
            // 
            // spAS11000PLCasingBindingSource
            // 
            this.spAS11000PLCasingBindingSource.DataMember = "sp_AS_11000_PL_Casing";
            this.spAS11000PLCasingBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // luePlantCategory_ID
            // 
            this.luePlantCategory_ID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11008PlantItemBindingSource, "PlantCategoryID", true));
            this.luePlantCategory_ID.Location = new System.Drawing.Point(813, 467);
            this.luePlantCategory_ID.MenuManager = this.barManager1;
            this.luePlantCategory_ID.Name = "luePlantCategory_ID";
            this.luePlantCategory_ID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luePlantCategory_ID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Plant and Machinery Category", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.luePlantCategory_ID.Properties.DataSource = this.spAS11000PLPlantCategoryBindingSource;
            this.luePlantCategory_ID.Properties.DisplayMember = "Value";
            this.luePlantCategory_ID.Properties.NullText = "";
            this.luePlantCategory_ID.Properties.NullValuePrompt = "-- Please Select --";
            this.luePlantCategory_ID.Properties.NullValuePromptShowForEmptyValue = true;
            this.luePlantCategory_ID.Properties.ValueMember = "PickListID";
            this.luePlantCategory_ID.Size = new System.Drawing.Size(508, 20);
            this.luePlantCategory_ID.StyleController = this.equipmentDataLayoutControl;
            this.luePlantCategory_ID.TabIndex = 73;
            this.luePlantCategory_ID.Validating += new System.ComponentModel.CancelEventHandler(this.plant_Category_IDLookUpEdit_Validating);
            // 
            // spAS11000PLPlantCategoryBindingSource
            // 
            this.spAS11000PLPlantCategoryBindingSource.DataMember = "sp_AS_11000_PL_Plant_Category";
            this.spAS11000PLPlantCategoryBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spnPlantEngine_Size
            // 
            this.spnPlantEngine_Size.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11008PlantItemBindingSource, "EngineSize", true));
            this.spnPlantEngine_Size.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnPlantEngine_Size.Location = new System.Drawing.Point(813, 491);
            this.spnPlantEngine_Size.MenuManager = this.barManager1;
            this.spnPlantEngine_Size.Name = "spnPlantEngine_Size";
            this.spnPlantEngine_Size.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnPlantEngine_Size.Properties.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.spnPlantEngine_Size.Properties.MaxValue = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.spnPlantEngine_Size.Size = new System.Drawing.Size(508, 20);
            this.spnPlantEngine_Size.StyleController = this.equipmentDataLayoutControl;
            this.spnPlantEngine_Size.TabIndex = 75;
            // 
            // luePlantFuel_Type_ID
            // 
            this.luePlantFuel_Type_ID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11008PlantItemBindingSource, "FuelTypeID", true));
            this.luePlantFuel_Type_ID.Location = new System.Drawing.Point(191, 491);
            this.luePlantFuel_Type_ID.MenuManager = this.barManager1;
            this.luePlantFuel_Type_ID.Name = "luePlantFuel_Type_ID";
            this.luePlantFuel_Type_ID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luePlantFuel_Type_ID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Fuel Type", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.luePlantFuel_Type_ID.Properties.DataSource = this.spAS11000PLFuelBindingSource;
            this.luePlantFuel_Type_ID.Properties.DisplayMember = "Value";
            this.luePlantFuel_Type_ID.Properties.NullText = "";
            this.luePlantFuel_Type_ID.Properties.NullValuePrompt = "-- Please Select --";
            this.luePlantFuel_Type_ID.Properties.NullValuePromptShowForEmptyValue = true;
            this.luePlantFuel_Type_ID.Properties.ValueMember = "PickListID";
            this.luePlantFuel_Type_ID.Size = new System.Drawing.Size(451, 20);
            this.luePlantFuel_Type_ID.StyleController = this.equipmentDataLayoutControl;
            this.luePlantFuel_Type_ID.TabIndex = 77;
            this.luePlantFuel_Type_ID.Validating += new System.ComponentModel.CancelEventHandler(this.PlantFuel_Type_IDLookUpEdit_Validating);
            // 
            // spAS11000PLFuelBindingSource
            // 
            this.spAS11000PLFuelBindingSource.DataMember = "sp_AS_11000_PL_Fuel";
            this.spAS11000PLFuelBindingSource.DataSource = this.dataSet_AS_DataEntry;
            this.spAS11000PLFuelBindingSource.Sort = "intOrder";
            // 
            // txtRegistration_Plate
            // 
            this.txtRegistration_Plate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11005VehicleItemBindingSource, "RegistrationPlate", true));
            this.txtRegistration_Plate.Location = new System.Drawing.Point(203, 499);
            this.txtRegistration_Plate.MenuManager = this.barManager1;
            this.txtRegistration_Plate.Name = "txtRegistration_Plate";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtRegistration_Plate, true);
            this.txtRegistration_Plate.Size = new System.Drawing.Size(624, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtRegistration_Plate, optionsSpelling8);
            this.txtRegistration_Plate.StyleController = this.equipmentDataLayoutControl;
            this.txtRegistration_Plate.TabIndex = 36;
            this.txtRegistration_Plate.Tag = "Registration Plate";
            this.txtRegistration_Plate.Validating += new System.ComponentModel.CancelEventHandler(this.registration_PlateTextEdit_Validating);
            // 
            // txtLog_Book_Number
            // 
            this.txtLog_Book_Number.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11005VehicleItemBindingSource, "LogBookNumber", true));
            this.txtLog_Book_Number.Location = new System.Drawing.Point(203, 547);
            this.txtLog_Book_Number.MenuManager = this.barManager1;
            this.txtLog_Book_Number.Name = "txtLog_Book_Number";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtLog_Book_Number, true);
            this.txtLog_Book_Number.Size = new System.Drawing.Size(624, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtLog_Book_Number, optionsSpelling9);
            this.txtLog_Book_Number.StyleController = this.equipmentDataLayoutControl;
            this.txtLog_Book_Number.TabIndex = 38;
            this.txtLog_Book_Number.Validating += new System.ComponentModel.CancelEventHandler(this.log_Book_NumberTextEdit_Validating);
            // 
            // txtModel_Specifications
            // 
            this.txtModel_Specifications.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11005VehicleItemBindingSource, "ModelSpecifications", true));
            this.txtModel_Specifications.Location = new System.Drawing.Point(203, 615);
            this.txtModel_Specifications.MenuManager = this.barManager1;
            this.txtModel_Specifications.Name = "txtModel_Specifications";
            this.txtModel_Specifications.Properties.NullText = "None";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtModel_Specifications, true);
            this.txtModel_Specifications.Size = new System.Drawing.Size(458, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtModel_Specifications, optionsSpelling10);
            this.txtModel_Specifications.StyleController = this.equipmentDataLayoutControl;
            this.txtModel_Specifications.TabIndex = 42;
            // 
            // spnVehicleEngine_Size
            // 
            this.spnVehicleEngine_Size.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11005VehicleItemBindingSource, "EngineSize", true));
            this.spnVehicleEngine_Size.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnVehicleEngine_Size.Location = new System.Drawing.Point(1022, 499);
            this.spnVehicleEngine_Size.MenuManager = this.barManager1;
            this.spnVehicleEngine_Size.Name = "spnVehicleEngine_Size";
            this.spnVehicleEngine_Size.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnVehicleEngine_Size.Properties.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.spnVehicleEngine_Size.Properties.MaxValue = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.spnVehicleEngine_Size.Size = new System.Drawing.Size(287, 20);
            this.spnVehicleEngine_Size.StyleController = this.equipmentDataLayoutControl;
            this.spnVehicleEngine_Size.TabIndex = 44;
            this.spnVehicleEngine_Size.Validating += new System.ComponentModel.CancelEventHandler(this.engine_SizeSpinEdit_Validating);
            // 
            // lueVehicleFuel_Type_ID
            // 
            this.lueVehicleFuel_Type_ID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11005VehicleItemBindingSource, "FuelTypeID", true));
            this.lueVehicleFuel_Type_ID.Location = new System.Drawing.Point(1022, 523);
            this.lueVehicleFuel_Type_ID.MenuManager = this.barManager1;
            this.lueVehicleFuel_Type_ID.Name = "lueVehicleFuel_Type_ID";
            this.lueVehicleFuel_Type_ID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueVehicleFuel_Type_ID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Fuel Type", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("intOrder", "int Order", 53, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.lueVehicleFuel_Type_ID.Properties.DataSource = this.spAS11000PLFuelBindingSource;
            this.lueVehicleFuel_Type_ID.Properties.DisplayMember = "Value";
            this.lueVehicleFuel_Type_ID.Properties.NullText = "";
            this.lueVehicleFuel_Type_ID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueVehicleFuel_Type_ID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueVehicleFuel_Type_ID.Properties.ValueMember = "PickListID";
            this.lueVehicleFuel_Type_ID.Size = new System.Drawing.Size(287, 20);
            this.lueVehicleFuel_Type_ID.StyleController = this.equipmentDataLayoutControl;
            this.lueVehicleFuel_Type_ID.TabIndex = 46;
            this.lueVehicleFuel_Type_ID.Validating += new System.ComponentModel.CancelEventHandler(this.fuel_Type_IDLookUpEdit_Validating);
            // 
            // spnEmissions
            // 
            this.spnEmissions.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11005VehicleItemBindingSource, "Emissions", true));
            this.spnEmissions.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnEmissions.Location = new System.Drawing.Point(1022, 547);
            this.spnEmissions.MenuManager = this.barManager1;
            this.spnEmissions.Name = "spnEmissions";
            this.spnEmissions.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnEmissions.Properties.Increment = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.spnEmissions.Properties.MaxValue = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.spnEmissions.Size = new System.Drawing.Size(287, 20);
            this.spnEmissions.StyleController = this.equipmentDataLayoutControl;
            this.spnEmissions.TabIndex = 48;
            this.spnEmissions.Validating += new System.ComponentModel.CancelEventHandler(this.emissionsSpinEdit_Validating);
            // 
            // spnTotal_Tyre_Number
            // 
            this.spnTotal_Tyre_Number.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11005VehicleItemBindingSource, "TotalTyreNumber", true));
            this.spnTotal_Tyre_Number.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnTotal_Tyre_Number.Location = new System.Drawing.Point(203, 639);
            this.spnTotal_Tyre_Number.MenuManager = this.barManager1;
            this.spnTotal_Tyre_Number.Name = "spnTotal_Tyre_Number";
            this.spnTotal_Tyre_Number.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnTotal_Tyre_Number.Properties.MaxValue = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.spnTotal_Tyre_Number.Size = new System.Drawing.Size(458, 20);
            this.spnTotal_Tyre_Number.StyleController = this.equipmentDataLayoutControl;
            this.spnTotal_Tyre_Number.TabIndex = 50;
            this.spnTotal_Tyre_Number.Validating += new System.ComponentModel.CancelEventHandler(this.total_Tyre_NumberSpinEdit_Validating);
            // 
            // lueTowbar_Type_ID
            // 
            this.lueTowbar_Type_ID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11005VehicleItemBindingSource, "TowbarTypeID", true));
            this.lueTowbar_Type_ID.Location = new System.Drawing.Point(203, 663);
            this.lueTowbar_Type_ID.MenuManager = this.barManager1;
            this.lueTowbar_Type_ID.Name = "lueTowbar_Type_ID";
            this.lueTowbar_Type_ID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueTowbar_Type_ID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Towbar Type", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueTowbar_Type_ID.Properties.DataSource = this.spAS11000PLVehicleTowbarTypeBindingSource;
            this.lueTowbar_Type_ID.Properties.DisplayMember = "Value";
            this.lueTowbar_Type_ID.Properties.NullText = "";
            this.lueTowbar_Type_ID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueTowbar_Type_ID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueTowbar_Type_ID.Properties.ValueMember = "PickListID";
            this.lueTowbar_Type_ID.Size = new System.Drawing.Size(458, 20);
            this.lueTowbar_Type_ID.StyleController = this.equipmentDataLayoutControl;
            this.lueTowbar_Type_ID.TabIndex = 52;
            this.lueTowbar_Type_ID.Validating += new System.ComponentModel.CancelEventHandler(this.towbar_Type_IDLookUpEdit_Validating);
            // 
            // spAS11000PLVehicleTowbarTypeBindingSource
            // 
            this.spAS11000PLVehicleTowbarTypeBindingSource.DataMember = "sp_AS_11000_PL_Vehicle_Towbar_Type";
            this.spAS11000PLVehicleTowbarTypeBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // cpeColour
            // 
            this.cpeColour.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11005VehicleItemBindingSource, "Colour", true));
            this.cpeColour.EditValue = System.Drawing.Color.Empty;
            this.cpeColour.Location = new System.Drawing.Point(832, 615);
            this.cpeColour.MenuManager = this.barManager1;
            this.cpeColour.Name = "cpeColour";
            this.cpeColour.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cpeColour.Properties.ColorDialogOptions.AllowTransparency = false;
            this.cpeColour.Properties.ShowColorDialog = false;
            this.cpeColour.Properties.ShowCustomColors = false;
            this.cpeColour.Properties.ShowSystemColors = false;
            this.cpeColour.Size = new System.Drawing.Size(477, 20);
            this.cpeColour.StyleController = this.equipmentDataLayoutControl;
            this.cpeColour.TabIndex = 54;
            this.cpeColour.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.cpeColour_EditValueChanging);
            this.cpeColour.Validating += new System.ComponentModel.CancelEventHandler(this.colourColorPickEdit_Validating);
            // 
            // deRegistration_Date
            // 
            this.deRegistration_Date.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11005VehicleItemBindingSource, "RegistrationDate", true));
            this.deRegistration_Date.EditValue = null;
            this.deRegistration_Date.Location = new System.Drawing.Point(832, 639);
            this.deRegistration_Date.MenuManager = this.barManager1;
            this.deRegistration_Date.Name = "deRegistration_Date";
            this.deRegistration_Date.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deRegistration_Date.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deRegistration_Date.Size = new System.Drawing.Size(477, 20);
            this.deRegistration_Date.StyleController = this.equipmentDataLayoutControl;
            this.deRegistration_Date.TabIndex = 56;
            this.deRegistration_Date.EditValueChanged += new System.EventHandler(this.deRegistration_Date_EditValueChanged);
            this.deRegistration_Date.Validating += new System.ComponentModel.CancelEventHandler(this.registration_DateDateEdit_Validating);
            // 
            // deMOT_Due_Date
            // 
            this.deMOT_Due_Date.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11005VehicleItemBindingSource, "MOTDueDate", true));
            this.deMOT_Due_Date.EditValue = null;
            this.deMOT_Due_Date.Location = new System.Drawing.Point(203, 731);
            this.deMOT_Due_Date.MenuManager = this.barManager1;
            this.deMOT_Due_Date.Name = "deMOT_Due_Date";
            this.deMOT_Due_Date.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deMOT_Due_Date.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deMOT_Due_Date.Size = new System.Drawing.Size(635, 20);
            this.deMOT_Due_Date.StyleController = this.equipmentDataLayoutControl;
            this.deMOT_Due_Date.TabIndex = 60;
            this.deMOT_Due_Date.Validating += new System.ComponentModel.CancelEventHandler(this.mOT_Due_DateDateEdit_Validating);
            // 
            // spnCurrentMileage
            // 
            this.spnCurrentMileage.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11005VehicleItemBindingSource, "CurrentMileage", true));
            this.spnCurrentMileage.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnCurrentMileage.Location = new System.Drawing.Point(203, 755);
            this.spnCurrentMileage.MenuManager = this.barManager1;
            this.spnCurrentMileage.Name = "spnCurrentMileage";
            this.spnCurrentMileage.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnCurrentMileage.Properties.MaxValue = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.spnCurrentMileage.Size = new System.Drawing.Size(635, 20);
            this.spnCurrentMileage.StyleController = this.equipmentDataLayoutControl;
            this.spnCurrentMileage.TabIndex = 62;
            this.spnCurrentMileage.Validating += new System.ComponentModel.CancelEventHandler(this.currentMileageSpinEdit_Validating);
            // 
            // spnService_Due_Mileage
            // 
            this.spnService_Due_Mileage.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11005VehicleItemBindingSource, "ServiceDueMileage", true));
            this.spnService_Due_Mileage.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnService_Due_Mileage.Location = new System.Drawing.Point(203, 779);
            this.spnService_Due_Mileage.MenuManager = this.barManager1;
            this.spnService_Due_Mileage.Name = "spnService_Due_Mileage";
            this.spnService_Due_Mileage.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnService_Due_Mileage.Properties.MaxValue = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.spnService_Due_Mileage.Size = new System.Drawing.Size(635, 20);
            this.spnService_Due_Mileage.StyleController = this.equipmentDataLayoutControl;
            this.spnService_Due_Mileage.TabIndex = 64;
            this.spnService_Due_Mileage.Validating += new System.ComponentModel.CancelEventHandler(this.service_Due_MileageSpinEdit_Validating);
            // 
            // txtRadioCode
            // 
            this.txtRadioCode.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11005VehicleItemBindingSource, "RadioCode", true));
            this.txtRadioCode.Location = new System.Drawing.Point(1033, 731);
            this.txtRadioCode.MenuManager = this.barManager1;
            this.txtRadioCode.Name = "txtRadioCode";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtRadioCode, true);
            this.txtRadioCode.Size = new System.Drawing.Size(276, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtRadioCode, optionsSpelling11);
            this.txtRadioCode.StyleController = this.equipmentDataLayoutControl;
            this.txtRadioCode.TabIndex = 66;
            // 
            // txtKeyCode
            // 
            this.txtKeyCode.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11005VehicleItemBindingSource, "KeyCode", true));
            this.txtKeyCode.Location = new System.Drawing.Point(1033, 755);
            this.txtKeyCode.MenuManager = this.barManager1;
            this.txtKeyCode.Name = "txtKeyCode";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtKeyCode, true);
            this.txtKeyCode.Size = new System.Drawing.Size(276, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtKeyCode, optionsSpelling12);
            this.txtKeyCode.StyleController = this.equipmentDataLayoutControl;
            this.txtKeyCode.TabIndex = 68;
            // 
            // txtElectronicCode
            // 
            this.txtElectronicCode.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11005VehicleItemBindingSource, "ElectronicCode", true));
            this.txtElectronicCode.Location = new System.Drawing.Point(1033, 779);
            this.txtElectronicCode.MenuManager = this.barManager1;
            this.txtElectronicCode.Name = "txtElectronicCode";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtElectronicCode, true);
            this.txtElectronicCode.Size = new System.Drawing.Size(276, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtElectronicCode, optionsSpelling13);
            this.txtElectronicCode.StyleController = this.equipmentDataLayoutControl;
            this.txtElectronicCode.TabIndex = 70;
            // 
            // txtEquipmentReference
            // 
            this.txtEquipmentReference.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11002EquipmentItemBindingSource, "EquipmentReference", true));
            this.txtEquipmentReference.Location = new System.Drawing.Point(203, 103);
            this.txtEquipmentReference.MenuManager = this.barManager1;
            this.txtEquipmentReference.Name = "txtEquipmentReference";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtEquipmentReference, true);
            this.txtEquipmentReference.Size = new System.Drawing.Size(50, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtEquipmentReference, optionsSpelling14);
            this.txtEquipmentReference.StyleController = this.equipmentDataLayoutControl;
            this.txtEquipmentReference.TabIndex = 7;
            // 
            // txtManufacturer_ID
            // 
            this.txtManufacturer_ID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11002EquipmentItemBindingSource, "ManufacturerID", true));
            this.txtManufacturer_ID.Location = new System.Drawing.Point(709, 71);
            this.txtManufacturer_ID.MenuManager = this.barManager1;
            this.txtManufacturer_ID.Name = "txtManufacturer_ID";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtManufacturer_ID, true);
            this.txtManufacturer_ID.Size = new System.Drawing.Size(612, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtManufacturer_ID, optionsSpelling15);
            this.txtManufacturer_ID.StyleController = this.equipmentDataLayoutControl;
            this.txtManufacturer_ID.TabIndex = 11;
            this.txtManufacturer_ID.Tag = "Manufacturer Identity";
            this.txtManufacturer_ID.Validating += new System.ComponentModel.CancelEventHandler(this.Manufacturer_IDTextEdit_Validating);
            // 
            // lueMake
            // 
            this.lueMake.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11002EquipmentItemBindingSource, "Make", true));
            this.lueMake.Location = new System.Drawing.Point(709, 95);
            this.lueMake.MenuManager = this.barManager1;
            this.lueMake.Name = "lueMake";
            this.lueMake.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("lueMake.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject25, serializableAppearanceObject26, serializableAppearanceObject27, serializableAppearanceObject28, "Add Make", "Add", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("lueMake.Properties.Buttons1"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject29, serializableAppearanceObject30, serializableAppearanceObject31, serializableAppearanceObject32, "Refresh Make List", "Refresh", null, true)});
            this.lueMake.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Make", 63, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueMake.Properties.DataSource = this.spAS11014MakeListBindingSource;
            this.lueMake.Properties.DisplayMember = "Description";
            this.lueMake.Properties.NullText = "";
            this.lueMake.Properties.NullValuePrompt = "-- Please Select a Make --";
            this.lueMake.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueMake.Properties.ValueMember = "Make";
            this.lueMake.Size = new System.Drawing.Size(612, 22);
            this.lueMake.StyleController = this.equipmentDataLayoutControl;
            this.lueMake.TabIndex = 13;
            this.lueMake.Tag = "Make";
            this.lueMake.ButtonPressed += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.makeLookUpEdit_ButtonPressed);
            this.lueMake.EditValueChanged += new System.EventHandler(this.lueMake_EditValueChanged);
            this.lueMake.Validating += new System.ComponentModel.CancelEventHandler(this.lueMake_Validating);
            // 
            // spAS11014MakeListBindingSource
            // 
            this.spAS11014MakeListBindingSource.DataMember = "sp_AS_11014_Make_List";
            this.spAS11014MakeListBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // lueModel
            // 
            this.lueModel.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11002EquipmentItemBindingSource, "Model", true));
            this.lueModel.Location = new System.Drawing.Point(709, 121);
            this.lueModel.MenuManager = this.barManager1;
            this.lueModel.Name = "lueModel";
            this.lueModel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("lueModel.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Add a Model", "Add", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("lueModel.Properties.Buttons1"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Refresh Models", "Refresh", null, true)});
            this.lueModel.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Model", 63, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueModel.Properties.DataSource = this.spAS11017ModelListBindingSource;
            this.lueModel.Properties.DisplayMember = "Description";
            this.lueModel.Properties.NullText = "";
            this.lueModel.Properties.NullValuePrompt = "-- Please Select a Model --";
            this.lueModel.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueModel.Properties.ValueMember = "Model";
            this.lueModel.Size = new System.Drawing.Size(612, 22);
            this.lueModel.StyleController = this.equipmentDataLayoutControl;
            this.lueModel.TabIndex = 15;
            this.lueModel.Tag = "Model";
            this.lueModel.ButtonPressed += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.modelLookUpEdit_ButtonPressed);
            this.lueModel.Validating += new System.ComponentModel.CancelEventHandler(this.lueModel_Validating);
            // 
            // spAS11017ModelListBindingSource
            // 
            this.spAS11017ModelListBindingSource.DataMember = "sp_AS_11017_Model_List";
            this.spAS11017ModelListBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // lueSupplier
            // 
            this.lueSupplier.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11002EquipmentItemBindingSource, "Supplier", true));
            this.lueSupplier.Location = new System.Drawing.Point(709, 147);
            this.lueSupplier.MenuManager = this.barManager1;
            this.lueSupplier.Name = "lueSupplier";
            this.lueSupplier.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("lueSupplier.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Add Supplier", "Add", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("lueSupplier.Properties.Buttons1"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Refresh Supplier List", "Refresh", null, true)});
            this.lueSupplier.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("SupplierReference", "Supplier Reference", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 150, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("AccountNumber", "Account Number", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("SupplierType", "Supplier Type", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueSupplier.Properties.DataSource = this.spAS11020SupplierListBindingSource;
            this.lueSupplier.Properties.DisplayMember = "SupplierReference";
            this.lueSupplier.Properties.NullText = "";
            this.lueSupplier.Properties.NullValuePrompt = "-- Please Select a Supplier --";
            this.lueSupplier.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueSupplier.Properties.PopupWidth = 400;
            this.lueSupplier.Properties.ValueMember = "SupplierID";
            this.lueSupplier.Size = new System.Drawing.Size(612, 22);
            this.lueSupplier.StyleController = this.equipmentDataLayoutControl;
            this.lueSupplier.TabIndex = 17;
            this.lueSupplier.Tag = "Supplier";
            this.lueSupplier.ButtonPressed += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.supplierLookUpEdit_ButtonPressed);
            this.lueSupplier.EditValueChanged += new System.EventHandler(this.lueSupplier_EditValueChanged);
            this.lueSupplier.Validating += new System.ComponentModel.CancelEventHandler(this.lueSupplier_Validating);
            // 
            // spAS11020SupplierListBindingSource
            // 
            this.spAS11020SupplierListBindingSource.DataMember = "sp_AS_11020_Supplier_List";
            this.spAS11020SupplierListBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // lueGc_Marked
            // 
            this.lueGc_Marked.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11002EquipmentItemBindingSource, "GCMarked", true));
            this.lueGc_Marked.Location = new System.Drawing.Point(448, 103);
            this.lueGc_Marked.MenuManager = this.barManager1;
            this.lueGc_Marked.Name = "lueGc_Marked";
            this.lueGc_Marked.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueGc_Marked.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Ground Control Marked", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueGc_Marked.Properties.DataSource = this.spAS11000PLEquipmentGCMarkedBindingSource;
            this.lueGc_Marked.Properties.DisplayMember = "Value";
            this.lueGc_Marked.Properties.NullText = "";
            this.lueGc_Marked.Properties.NullValuePrompt = "-- Please Select --";
            this.lueGc_Marked.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueGc_Marked.Properties.ValueMember = "PickListID";
            this.lueGc_Marked.Size = new System.Drawing.Size(54, 20);
            this.lueGc_Marked.StyleController = this.equipmentDataLayoutControl;
            this.lueGc_Marked.TabIndex = 19;
            this.lueGc_Marked.Validating += new System.ComponentModel.CancelEventHandler(this.lueGC_Marked_Validating);
            // 
            // spAS11000PLEquipmentGCMarkedBindingSource
            // 
            this.spAS11000PLEquipmentGCMarkedBindingSource.DataMember = "sp_AS_11000_PL_Equipment_GC_Marked";
            this.spAS11000PLEquipmentGCMarkedBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // lueOwnership_Status
            // 
            this.lueOwnership_Status.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11002EquipmentItemBindingSource, "OwnershipStatus", true));
            this.lueOwnership_Status.Location = new System.Drawing.Point(448, 127);
            this.lueOwnership_Status.MenuManager = this.barManager1;
            this.lueOwnership_Status.Name = "lueOwnership_Status";
            this.lueOwnership_Status.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueOwnership_Status.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Ownership Status", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueOwnership_Status.Properties.DataSource = this.spAS11000PLEquipmentOwnershipStatusBindingSource;
            this.lueOwnership_Status.Properties.DisplayMember = "Value";
            this.lueOwnership_Status.Properties.NullText = "";
            this.lueOwnership_Status.Properties.NullValuePrompt = "-- Please Select --";
            this.lueOwnership_Status.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueOwnership_Status.Properties.ValueMember = "PickListID";
            this.lueOwnership_Status.Size = new System.Drawing.Size(54, 20);
            this.lueOwnership_Status.StyleController = this.equipmentDataLayoutControl;
            this.lueOwnership_Status.TabIndex = 21;
            this.lueOwnership_Status.EditValueChanged += new System.EventHandler(this.lueOwnership_Status_EditValueChanged);
            this.lueOwnership_Status.Validating += new System.ComponentModel.CancelEventHandler(this.lueOwnership_Status_Validating);
            // 
            // spAS11000PLEquipmentOwnershipStatusBindingSource
            // 
            this.spAS11000PLEquipmentOwnershipStatusBindingSource.DataMember = "sp_AS_11000_PL_Equipment_Ownership_Status";
            this.spAS11000PLEquipmentOwnershipStatusBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // lueAvailability
            // 
            this.lueAvailability.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11002EquipmentItemBindingSource, "Availability", true));
            this.lueAvailability.Location = new System.Drawing.Point(448, 151);
            this.lueAvailability.MenuManager = this.barManager1;
            this.lueAvailability.Name = "lueAvailability";
            this.lueAvailability.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueAvailability.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Availability", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueAvailability.Properties.DataSource = this.spAS11000PLEquipmentAvailabilityBindingSource;
            this.lueAvailability.Properties.DisplayMember = "Value";
            this.lueAvailability.Properties.NullText = "";
            this.lueAvailability.Properties.NullValuePrompt = "-- Please Select --";
            this.lueAvailability.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueAvailability.Properties.ValueMember = "PickListID";
            this.lueAvailability.Size = new System.Drawing.Size(54, 20);
            this.lueAvailability.StyleController = this.equipmentDataLayoutControl;
            this.lueAvailability.TabIndex = 23;
            this.lueAvailability.Validating += new System.ComponentModel.CancelEventHandler(this.lueAvailability_Validating);
            // 
            // spAS11000PLEquipmentAvailabilityBindingSource
            // 
            this.spAS11000PLEquipmentAvailabilityBindingSource.DataMember = "sp_AS_11000_PL_Equipment_Availability";
            this.spAS11000PLEquipmentAvailabilityBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // deDepreciation_Start_Date
            // 
            this.deDepreciation_Start_Date.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11002EquipmentItemBindingSource, "DepreciationStartDate", true));
            this.deDepreciation_Start_Date.EditValue = null;
            this.deDepreciation_Start_Date.Location = new System.Drawing.Point(448, 290);
            this.deDepreciation_Start_Date.MenuManager = this.barManager1;
            this.deDepreciation_Start_Date.Name = "deDepreciation_Start_Date";
            this.deDepreciation_Start_Date.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deDepreciation_Start_Date.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deDepreciation_Start_Date.Size = new System.Drawing.Size(54, 20);
            this.deDepreciation_Start_Date.StyleController = this.equipmentDataLayoutControl;
            this.deDepreciation_Start_Date.TabIndex = 25;
            // 
            // txtEx_Asset_ID
            // 
            this.txtEx_Asset_ID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11002EquipmentItemBindingSource, "ExchequerID", true));
            this.txtEx_Asset_ID.Location = new System.Drawing.Point(203, 285);
            this.txtEx_Asset_ID.MenuManager = this.barManager1;
            this.txtEx_Asset_ID.Name = "txtEx_Asset_ID";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtEx_Asset_ID, true);
            this.txtEx_Asset_ID.Size = new System.Drawing.Size(50, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtEx_Asset_ID, optionsSpelling16);
            this.txtEx_Asset_ID.StyleController = this.equipmentDataLayoutControl;
            this.txtEx_Asset_ID.TabIndex = 27;
            // 
            // txtSage_Asset_Ref
            // 
            this.txtSage_Asset_Ref.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11002EquipmentItemBindingSource, "SageReference", true));
            this.txtSage_Asset_Ref.Location = new System.Drawing.Point(203, 309);
            this.txtSage_Asset_Ref.MenuManager = this.barManager1;
            this.txtSage_Asset_Ref.Name = "txtSage_Asset_Ref";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtSage_Asset_Ref, true);
            this.txtSage_Asset_Ref.Size = new System.Drawing.Size(50, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtSage_Asset_Ref, optionsSpelling17);
            this.txtSage_Asset_Ref.StyleController = this.equipmentDataLayoutControl;
            this.txtSage_Asset_Ref.TabIndex = 29;
            // 
            // meNotes
            // 
            this.meNotes.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11002EquipmentItemBindingSource, "Notes", true));
            this.meNotes.Location = new System.Drawing.Point(542, 281);
            this.meNotes.MenuManager = this.barManager1;
            this.meNotes.Name = "meNotes";
            this.scSpellChecker.SetShowSpellCheckMenu(this.meNotes, true);
            this.meNotes.Size = new System.Drawing.Size(779, 126);
            this.scSpellChecker.SetSpellCheckerOptions(this.meNotes, optionsSpelling18);
            this.meNotes.StyleController = this.equipmentDataLayoutControl;
            this.meNotes.TabIndex = 31;
            // 
            // memDescription
            // 
            this.memDescription.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp_AS_11029_Office_ItemBindingSource, "Description", true));
            this.memDescription.Location = new System.Drawing.Point(36, 524);
            this.memDescription.MenuManager = this.barManager1;
            this.memDescription.Name = "memDescription";
            this.scSpellChecker.SetShowSpellCheckMenu(this.memDescription, true);
            this.memDescription.Size = new System.Drawing.Size(1273, 285);
            this.scSpellChecker.SetSpellCheckerOptions(this.memDescription, optionsSpelling19);
            this.memDescription.StyleController = this.equipmentDataLayoutControl;
            this.memDescription.TabIndex = 117;
            this.memDescription.Tag = "Office Equipment Description";
            this.memDescription.Validating += new System.ComponentModel.CancelEventHandler(this.memDescription_Validating);
            // 
            // soldDateDateEdit
            // 
            this.soldDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11002EquipmentItemBindingSource, "SoldDate", true));
            this.soldDateDateEdit.EditValue = null;
            this.soldDateDateEdit.Location = new System.Drawing.Point(448, 199);
            this.soldDateDateEdit.Name = "soldDateDateEdit";
            this.soldDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.soldDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.soldDateDateEdit.Size = new System.Drawing.Size(54, 20);
            this.soldDateDateEdit.StyleController = this.equipmentDataLayoutControl;
            this.soldDateDateEdit.TabIndex = 131;
            this.soldDateDateEdit.Tag = "Sold Date";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem8,
            this.tabbedControlGroup1,
            this.emptySpaceItem9,
            this.linkedChildDataGroup,
            this.equipmentLayoutControlGroup,
            this.manufacturerLayoutControlGroup,
            this.emptySpaceItem13,
            this.layoutControlItem55,
            this.emptySpaceItem7,
            this.remarksLayoutControlGroup});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1345, 1130);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(518, 411);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(807, 11);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 422);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.vehicleLayoutControlGroup;
            this.tabbedControlGroup1.SelectedTabPageIndex = 1;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(1325, 403);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.NoDetailsLayoutControlGroup,
            this.vehicleLayoutControlGroup,
            this.plantLayoutControlGroup,
            this.gadgetLayoutControlGroup,
            this.hardwareLayoutControlGroup,
            this.softwareLayoutControlGroup,
            this.officeLayoutControlGroup});
            // 
            // vehicleLayoutControlGroup
            // 
            this.vehicleLayoutControlGroup.CustomizationFormText = "Vehicles";
            this.vehicleLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.vehRegistrationLayoutControlGroup,
            this.vehSpecificationLayoutControlGroup,
            this.vehMaintenanceLayoutControlGroup,
            this.vehCodesLayoutControlGroup,
            this.emptySpaceItem3,
            this.vehPerfomanceLayoutControlGroup});
            this.vehicleLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.vehicleLayoutControlGroup.Name = "vehicleLayoutControlGroup";
            this.vehicleLayoutControlGroup.Size = new System.Drawing.Size(1301, 358);
            this.vehicleLayoutControlGroup.Text = "Vehicle Detail";
            this.vehicleLayoutControlGroup.Shown += new System.EventHandler(this.childLayoutControlGroup_Shown);
            // 
            // vehRegistrationLayoutControlGroup
            // 
            this.vehRegistrationLayoutControlGroup.CustomizationFormText = "Registration Details";
            this.vehRegistrationLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem54,
            this.layoutControlItem53,
            this.layoutControlItem31});
            this.vehRegistrationLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.vehRegistrationLayoutControlGroup.Name = "vehRegistrationLayoutControlGroup";
            this.vehRegistrationLayoutControlGroup.Size = new System.Drawing.Size(819, 116);
            this.vehRegistrationLayoutControlGroup.Text = "Registration Details";
            // 
            // layoutControlItem54
            // 
            this.layoutControlItem54.Control = this.txtRegistration_Plate;
            this.layoutControlItem54.CustomizationFormText = "Registration Plate:";
            this.layoutControlItem54.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem54.Name = "layoutControlItem54";
            this.layoutControlItem54.Size = new System.Drawing.Size(795, 24);
            this.layoutControlItem54.Text = "Registration Plate:";
            this.layoutControlItem54.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem53
            // 
            this.layoutControlItem53.Control = this.txtLog_Book_Number;
            this.layoutControlItem53.CustomizationFormText = "Log Book Number:";
            this.layoutControlItem53.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem53.Name = "layoutControlItem53";
            this.layoutControlItem53.Size = new System.Drawing.Size(795, 24);
            this.layoutControlItem53.Text = "Log Book Number:";
            this.layoutControlItem53.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.Control = this.lueVehicleCategoryID;
            this.layoutControlItem31.CustomizationFormText = "Vehicle Category:";
            this.layoutControlItem31.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(795, 24);
            this.layoutControlItem31.Text = "Vehicle Category:";
            this.layoutControlItem31.TextSize = new System.Drawing.Size(164, 13);
            // 
            // vehSpecificationLayoutControlGroup
            // 
            this.vehSpecificationLayoutControlGroup.CustomizationFormText = "Vehicle Specification";
            this.vehSpecificationLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem49,
            this.layoutControlItem39,
            this.layoutControlItem34,
            this.layoutControlItem36,
            this.layoutControlItem38,
            this.emptySpaceItem6});
            this.vehSpecificationLayoutControlGroup.Location = new System.Drawing.Point(0, 116);
            this.vehSpecificationLayoutControlGroup.Name = "vehSpecificationLayoutControlGroup";
            this.vehSpecificationLayoutControlGroup.Size = new System.Drawing.Size(1301, 116);
            this.vehSpecificationLayoutControlGroup.Text = "Vehicle Specification";
            // 
            // layoutControlItem49
            // 
            this.layoutControlItem49.Control = this.txtModel_Specifications;
            this.layoutControlItem49.CustomizationFormText = "Model Specifications:";
            this.layoutControlItem49.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem49.Name = "layoutControlItem49";
            this.layoutControlItem49.Size = new System.Drawing.Size(629, 24);
            this.layoutControlItem49.Text = "Model Specifications:";
            this.layoutControlItem49.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem39
            // 
            this.layoutControlItem39.Control = this.spnTotal_Tyre_Number;
            this.layoutControlItem39.CustomizationFormText = "Total Tyre Number:";
            this.layoutControlItem39.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem39.Name = "layoutControlItem39";
            this.layoutControlItem39.Size = new System.Drawing.Size(629, 24);
            this.layoutControlItem39.Text = "Total Tyre Number:";
            this.layoutControlItem39.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.Control = this.lueTowbar_Type_ID;
            this.layoutControlItem34.CustomizationFormText = "Towbar Type ID:";
            this.layoutControlItem34.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem34.Name = "layoutControlItem34";
            this.layoutControlItem34.Size = new System.Drawing.Size(629, 24);
            this.layoutControlItem34.Text = "Towbar Type:";
            this.layoutControlItem34.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.Control = this.cpeColour;
            this.layoutControlItem36.CustomizationFormText = "Colour:";
            this.layoutControlItem36.Location = new System.Drawing.Point(629, 0);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(648, 24);
            this.layoutControlItem36.Text = "Colour:";
            this.layoutControlItem36.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.Control = this.deRegistration_Date;
            this.layoutControlItem38.CustomizationFormText = "Registration Date:";
            this.layoutControlItem38.Location = new System.Drawing.Point(629, 24);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.Size = new System.Drawing.Size(648, 24);
            this.layoutControlItem38.Text = "Registration Date:";
            this.layoutControlItem38.TextSize = new System.Drawing.Size(164, 13);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(629, 48);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(648, 24);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // vehMaintenanceLayoutControlGroup
            // 
            this.vehMaintenanceLayoutControlGroup.CustomizationFormText = "Maintenance Details";
            this.vehMaintenanceLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem42,
            this.layoutControlItem44,
            this.layoutControlItem46});
            this.vehMaintenanceLayoutControlGroup.Location = new System.Drawing.Point(0, 232);
            this.vehMaintenanceLayoutControlGroup.Name = "vehMaintenanceLayoutControlGroup";
            this.vehMaintenanceLayoutControlGroup.Size = new System.Drawing.Size(830, 116);
            this.vehMaintenanceLayoutControlGroup.Text = "Maintenance Details";
            // 
            // layoutControlItem42
            // 
            this.layoutControlItem42.Control = this.deMOT_Due_Date;
            this.layoutControlItem42.CustomizationFormText = "MOT Due Date:";
            this.layoutControlItem42.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem42.Name = "layoutControlItem42";
            this.layoutControlItem42.Size = new System.Drawing.Size(806, 24);
            this.layoutControlItem42.Text = "MOT Due Date:";
            this.layoutControlItem42.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem44
            // 
            this.layoutControlItem44.Control = this.spnCurrentMileage;
            this.layoutControlItem44.CustomizationFormText = "Current Mileage:";
            this.layoutControlItem44.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem44.Name = "layoutControlItem44";
            this.layoutControlItem44.Size = new System.Drawing.Size(806, 24);
            this.layoutControlItem44.Text = "Current Mileage:";
            this.layoutControlItem44.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem46
            // 
            this.layoutControlItem46.Control = this.spnService_Due_Mileage;
            this.layoutControlItem46.CustomizationFormText = "Service Due Mileage:";
            this.layoutControlItem46.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem46.Name = "layoutControlItem46";
            this.layoutControlItem46.Size = new System.Drawing.Size(806, 24);
            this.layoutControlItem46.Text = "Service Due Mileage:";
            this.layoutControlItem46.TextSize = new System.Drawing.Size(164, 13);
            // 
            // vehCodesLayoutControlGroup
            // 
            this.vehCodesLayoutControlGroup.CustomizationFormText = "Vehicle Codes";
            this.vehCodesLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem48,
            this.layoutControlItem50,
            this.layoutControlItem52});
            this.vehCodesLayoutControlGroup.Location = new System.Drawing.Point(830, 232);
            this.vehCodesLayoutControlGroup.Name = "vehCodesLayoutControlGroup";
            this.vehCodesLayoutControlGroup.Size = new System.Drawing.Size(471, 116);
            this.vehCodesLayoutControlGroup.Text = "Vehicle Codes";
            // 
            // layoutControlItem48
            // 
            this.layoutControlItem48.Control = this.txtRadioCode;
            this.layoutControlItem48.CustomizationFormText = "Radio Code:";
            this.layoutControlItem48.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem48.Name = "layoutControlItem48";
            this.layoutControlItem48.Size = new System.Drawing.Size(447, 24);
            this.layoutControlItem48.Text = "Radio Code:";
            this.layoutControlItem48.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem50
            // 
            this.layoutControlItem50.Control = this.txtKeyCode;
            this.layoutControlItem50.CustomizationFormText = "Key Code:";
            this.layoutControlItem50.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem50.Name = "layoutControlItem50";
            this.layoutControlItem50.Size = new System.Drawing.Size(447, 24);
            this.layoutControlItem50.Text = "Key Code:";
            this.layoutControlItem50.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem52
            // 
            this.layoutControlItem52.Control = this.txtElectronicCode;
            this.layoutControlItem52.CustomizationFormText = "Electronic Code:";
            this.layoutControlItem52.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem52.Name = "layoutControlItem52";
            this.layoutControlItem52.Size = new System.Drawing.Size(447, 24);
            this.layoutControlItem52.Text = "Electronic Code:";
            this.layoutControlItem52.TextSize = new System.Drawing.Size(164, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 348);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(1301, 10);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // vehPerfomanceLayoutControlGroup
            // 
            this.vehPerfomanceLayoutControlGroup.CustomizationFormText = "Perfomance Details";
            this.vehPerfomanceLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem47,
            this.layoutControlItem45,
            this.layoutControlItem43});
            this.vehPerfomanceLayoutControlGroup.Location = new System.Drawing.Point(819, 0);
            this.vehPerfomanceLayoutControlGroup.Name = "vehPerfomanceLayoutControlGroup";
            this.vehPerfomanceLayoutControlGroup.Size = new System.Drawing.Size(482, 116);
            this.vehPerfomanceLayoutControlGroup.Text = "Perfomance Details";
            // 
            // layoutControlItem47
            // 
            this.layoutControlItem47.Control = this.spnVehicleEngine_Size;
            this.layoutControlItem47.CustomizationFormText = "Engine Size:";
            this.layoutControlItem47.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem47.Name = "layoutControlItem47";
            this.layoutControlItem47.Size = new System.Drawing.Size(458, 24);
            this.layoutControlItem47.Text = "Engine Size:";
            this.layoutControlItem47.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem45
            // 
            this.layoutControlItem45.Control = this.lueVehicleFuel_Type_ID;
            this.layoutControlItem45.CustomizationFormText = "Fuel Type ID:";
            this.layoutControlItem45.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem45.Name = "layoutControlItem45";
            this.layoutControlItem45.Size = new System.Drawing.Size(458, 24);
            this.layoutControlItem45.Text = "Fuel Type:";
            this.layoutControlItem45.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem43
            // 
            this.layoutControlItem43.Control = this.spnEmissions;
            this.layoutControlItem43.CustomizationFormText = "Emissions:";
            this.layoutControlItem43.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem43.Name = "layoutControlItem43";
            this.layoutControlItem43.Size = new System.Drawing.Size(458, 24);
            this.layoutControlItem43.Text = "Emissions:";
            this.layoutControlItem43.TextSize = new System.Drawing.Size(164, 13);
            // 
            // NoDetailsLayoutControlGroup
            // 
            this.NoDetailsLayoutControlGroup.CustomizationFormText = "No Details";
            this.NoDetailsLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem4});
            this.NoDetailsLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.NoDetailsLayoutControlGroup.Name = "NoDetailsLayoutControlGroup";
            this.NoDetailsLayoutControlGroup.Size = new System.Drawing.Size(1301, 358);
            this.NoDetailsLayoutControlGroup.Text = "No Details";
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(1301, 358);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // plantLayoutControlGroup
            // 
            this.plantLayoutControlGroup.CustomizationFormText = "Plant and Machinery";
            this.plantLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem33,
            this.layoutControlItem32,
            this.layoutControlItem51,
            this.layoutControlItem29,
            this.layoutControlItem64,
            this.layoutControlItem67,
            this.layoutControlItem57});
            this.plantLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.plantLayoutControlGroup.Name = "plantLayoutControlGroup";
            this.plantLayoutControlGroup.Size = new System.Drawing.Size(1301, 358);
            this.plantLayoutControlGroup.Text = "Plant and Machinery Detail";
            this.plantLayoutControlGroup.Shown += new System.EventHandler(this.childLayoutControlGroup_Shown);
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.Control = this.luePlantCategory_ID;
            this.layoutControlItem33.CustomizationFormText = "Plant Category:";
            this.layoutControlItem33.Location = new System.Drawing.Point(622, 0);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(679, 24);
            this.layoutControlItem33.Text = "Plant Category:";
            this.layoutControlItem33.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.Control = this.spnPlantEngine_Size;
            this.layoutControlItem32.CustomizationFormText = "Engine Size:";
            this.layoutControlItem32.Location = new System.Drawing.Point(622, 24);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(679, 24);
            this.layoutControlItem32.Text = "Engine Size:";
            this.layoutControlItem32.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem51
            // 
            this.layoutControlItem51.Control = this.txtShortDescription;
            this.layoutControlItem51.CustomizationFormText = "Short Description:";
            this.layoutControlItem51.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem51.Name = "layoutControlItem51";
            this.layoutControlItem51.Size = new System.Drawing.Size(622, 24);
            this.layoutControlItem51.Text = "Short Description:";
            this.layoutControlItem51.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.Control = this.luePlantFuel_Type_ID;
            this.layoutControlItem29.CustomizationFormText = "Fuel Type ID:";
            this.layoutControlItem29.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(622, 24);
            this.layoutControlItem29.Text = "Fuel Type:";
            this.layoutControlItem29.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem64
            // 
            this.layoutControlItem64.Control = this.deLOLERDate;
            this.layoutControlItem64.CustomizationFormText = "LOLERDate:";
            this.layoutControlItem64.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem64.Name = "layoutControlItem64";
            this.layoutControlItem64.Size = new System.Drawing.Size(622, 24);
            this.layoutControlItem64.Text = "LOLERDate:";
            this.layoutControlItem64.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem67
            // 
            this.layoutControlItem67.Control = this.dePUWERDate;
            this.layoutControlItem67.CustomizationFormText = "PUWERDate:";
            this.layoutControlItem67.Location = new System.Drawing.Point(622, 48);
            this.layoutControlItem67.Name = "layoutControlItem67";
            this.layoutControlItem67.Size = new System.Drawing.Size(679, 310);
            this.layoutControlItem67.Text = "PUWERDate:";
            this.layoutControlItem67.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem57
            // 
            this.layoutControlItem57.Control = this.txtPlantRegistrationPlate;
            this.layoutControlItem57.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem57.Name = "layoutControlItem57";
            this.layoutControlItem57.Size = new System.Drawing.Size(622, 286);
            this.layoutControlItem57.Text = "Registration Plate:";
            this.layoutControlItem57.TextSize = new System.Drawing.Size(164, 13);
            // 
            // gadgetLayoutControlGroup
            // 
            this.gadgetLayoutControlGroup.CustomizationFormText = "Gadgets";
            this.gadgetLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3});
            this.gadgetLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.gadgetLayoutControlGroup.Name = "gadgetLayoutControlGroup";
            this.gadgetLayoutControlGroup.Size = new System.Drawing.Size(1301, 358);
            this.gadgetLayoutControlGroup.Text = "Gadgets Detail";
            this.gadgetLayoutControlGroup.Shown += new System.EventHandler(this.childLayoutControlGroup_Shown);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Gadget Details";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem25,
            this.layoutControlItem35,
            this.layoutControlItem63});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(1301, 358);
            this.layoutControlGroup3.Text = "Gadget Details";
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.lueGadget_Type_ID;
            this.layoutControlItem25.CustomizationFormText = "Gadget Type ID:";
            this.layoutControlItem25.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(642, 290);
            this.layoutControlItem25.Text = "Gadget Type:";
            this.layoutControlItem25.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.Control = this.lueGadgetCasing_ID;
            this.layoutControlItem35.CustomizationFormText = "Casing :";
            this.layoutControlItem35.Location = new System.Drawing.Point(642, 0);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Size = new System.Drawing.Size(635, 314);
            this.layoutControlItem35.Text = "Casing :";
            this.layoutControlItem35.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem63
            // 
            this.layoutControlItem63.Control = this.txtIMEI;
            this.layoutControlItem63.CustomizationFormText = "IMEI:";
            this.layoutControlItem63.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem63.Name = "layoutControlItem63";
            this.layoutControlItem63.Size = new System.Drawing.Size(642, 24);
            this.layoutControlItem63.Text = "IMEI:";
            this.layoutControlItem63.TextSize = new System.Drawing.Size(164, 13);
            // 
            // hardwareLayoutControlGroup
            // 
            this.hardwareLayoutControlGroup.CustomizationFormText = "Hardware";
            this.hardwareLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem28,
            this.layoutControlItem41,
            this.layoutControlItem37,
            this.layoutControlItem65,
            this.layoutControlItem66});
            this.hardwareLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.hardwareLayoutControlGroup.Name = "hardwareLayoutControlGroup";
            this.hardwareLayoutControlGroup.Size = new System.Drawing.Size(1301, 358);
            this.hardwareLayoutControlGroup.Text = "Hardware Detail";
            this.hardwareLayoutControlGroup.Shown += new System.EventHandler(this.childLayoutControlGroup_Shown);
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.Control = this.lueHardware_Type_ID;
            this.layoutControlItem28.CustomizationFormText = "Hardware Type ID:";
            this.layoutControlItem28.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(647, 24);
            this.layoutControlItem28.Text = "Hardware Type:";
            this.layoutControlItem28.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem41
            // 
            this.layoutControlItem41.Control = this.txtHardwareDefault_Password;
            this.layoutControlItem41.CustomizationFormText = "Default Password:";
            this.layoutControlItem41.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem41.Name = "layoutControlItem41";
            this.layoutControlItem41.Size = new System.Drawing.Size(1301, 310);
            this.layoutControlItem41.Text = "Default Password:";
            this.layoutControlItem41.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.Control = this.lueHardwareSpecification_ID;
            this.layoutControlItem37.CustomizationFormText = "Specification ID:";
            this.layoutControlItem37.Location = new System.Drawing.Point(647, 0);
            this.layoutControlItem37.Name = "layoutControlItem37";
            this.layoutControlItem37.Size = new System.Drawing.Size(654, 24);
            this.layoutControlItem37.Text = "Specification:";
            this.layoutControlItem37.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem65
            // 
            this.layoutControlItem65.Control = this.txtSerialNumber;
            this.layoutControlItem65.CustomizationFormText = "Serial Number:";
            this.layoutControlItem65.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem65.Name = "layoutControlItem65";
            this.layoutControlItem65.Size = new System.Drawing.Size(648, 24);
            this.layoutControlItem65.Text = "Serial Number:";
            this.layoutControlItem65.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem66
            // 
            this.layoutControlItem66.Control = this.txtServiceTag;
            this.layoutControlItem66.CustomizationFormText = "Service Tag:";
            this.layoutControlItem66.Location = new System.Drawing.Point(648, 24);
            this.layoutControlItem66.Name = "layoutControlItem66";
            this.layoutControlItem66.Size = new System.Drawing.Size(653, 24);
            this.layoutControlItem66.Text = "Service Tag:";
            this.layoutControlItem66.TextSize = new System.Drawing.Size(164, 13);
            // 
            // softwareLayoutControlGroup
            // 
            this.softwareLayoutControlGroup.CustomizationFormText = "Software";
            this.softwareLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem23,
            this.layoutControlItem21,
            this.layoutControlItem17,
            this.layoutControlItem11,
            this.layoutControlItem7,
            this.layoutControlItem15,
            this.layoutControlItem19,
            this.layoutControlItem13,
            this.layoutControlItem9,
            this.layoutControlItem5});
            this.softwareLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.softwareLayoutControlGroup.Name = "softwareLayoutControlGroup";
            this.softwareLayoutControlGroup.Size = new System.Drawing.Size(1301, 358);
            this.softwareLayoutControlGroup.Text = "Software Detail";
            this.softwareLayoutControlGroup.Shown += new System.EventHandler(this.childLayoutControlGroup_Shown);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.lueSoftwareProduct_ID;
            this.layoutControlItem23.CustomizationFormText = "Product:";
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(874, 24);
            this.layoutControlItem23.Text = "Product:";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.lueSoftwareVersion_ID;
            this.layoutControlItem21.CustomizationFormText = "Version:";
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(874, 24);
            this.layoutControlItem21.Text = "Version:";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.txtSoftwareLicence;
            this.layoutControlItem17.CustomizationFormText = "Licence:";
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(874, 24);
            this.layoutControlItem17.Text = "Licence:";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.spnSoftwareQuantity_Limit;
            this.layoutControlItem11.CustomizationFormText = "Quantity Limit:";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(874, 24);
            this.layoutControlItem11.Text = "Quantity Limit:";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.lueSoftwareLicence_Type_ID;
            this.layoutControlItem7.CustomizationFormText = "Licence Type ID:";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(874, 262);
            this.layoutControlItem7.Text = "Licence Type:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.lueSoftwareParent_Program;
            this.layoutControlItem15.CustomizationFormText = "Parent Program:";
            this.layoutControlItem15.Location = new System.Drawing.Point(874, 0);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(427, 24);
            this.layoutControlItem15.Text = "Parent Program:";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.deSoftwareYear;
            this.layoutControlItem19.CustomizationFormText = "Year:";
            this.layoutControlItem19.Location = new System.Drawing.Point(874, 24);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(427, 24);
            this.layoutControlItem19.Text = "Year:";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.lueSoftwareAcquisation_Method_ID;
            this.layoutControlItem13.CustomizationFormText = "Acquisation Method:";
            this.layoutControlItem13.Location = new System.Drawing.Point(874, 48);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(427, 24);
            this.layoutControlItem13.Text = "Acquisation Method:";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.txtSoftwareLicence_Key;
            this.layoutControlItem9.CustomizationFormText = "Licence Key:";
            this.layoutControlItem9.Location = new System.Drawing.Point(874, 72);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(427, 24);
            this.layoutControlItem9.Text = "Licence Key:";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.deSoftwareValid_Until;
            this.layoutControlItem5.CustomizationFormText = "Valid Until:";
            this.layoutControlItem5.Location = new System.Drawing.Point(874, 96);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(427, 262);
            this.layoutControlItem5.Text = "Valid Until:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(164, 13);
            // 
            // officeLayoutControlGroup
            // 
            this.officeLayoutControlGroup.CustomizationFormText = "Office Equipment";
            this.officeLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.tabbedControlGroup2,
            this.layoutControlItem62});
            this.officeLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.officeLayoutControlGroup.Name = "officeLayoutControlGroup";
            this.officeLayoutControlGroup.Size = new System.Drawing.Size(1301, 358);
            this.officeLayoutControlGroup.Text = "Office Equipment Detail";
            this.officeLayoutControlGroup.Shown += new System.EventHandler(this.childLayoutControlGroup_Shown);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.spnQuantity;
            this.layoutControlItem2.CustomizationFormText = "Quantity:";
            this.layoutControlItem2.Location = new System.Drawing.Point(648, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(653, 24);
            this.layoutControlItem2.Text = "Quantity:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(164, 13);
            // 
            // tabbedControlGroup2
            // 
            this.tabbedControlGroup2.CustomizationFormText = "tabbedControlGroup2";
            this.tabbedControlGroup2.Location = new System.Drawing.Point(0, 24);
            this.tabbedControlGroup2.Name = "tabbedControlGroup2";
            this.tabbedControlGroup2.SelectedTabPage = this.layoutControlGroup2;
            this.tabbedControlGroup2.SelectedTabPageIndex = 0;
            this.tabbedControlGroup2.Size = new System.Drawing.Size(1301, 334);
            this.tabbedControlGroup2.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "Item Description";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1277, 289);
            this.layoutControlGroup2.Text = "Item Description";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.memDescription;
            this.layoutControlItem3.CustomizationFormText = "Description:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(1277, 289);
            this.layoutControlItem3.Text = "Description:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem62
            // 
            this.layoutControlItem62.Control = this.lueOfficeCategoryID;
            this.layoutControlItem62.CustomizationFormText = "Office Category :";
            this.layoutControlItem62.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem62.Name = "layoutControlItem62";
            this.layoutControlItem62.Size = new System.Drawing.Size(648, 24);
            this.layoutControlItem62.Text = "Office Category :";
            this.layoutControlItem62.TextSize = new System.Drawing.Size(164, 13);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.CustomizationFormText = "emptySpaceItem9";
            this.emptySpaceItem9.Location = new System.Drawing.Point(0, 1100);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(1325, 10);
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // linkedChildDataGroup
            // 
            this.linkedChildDataGroup.CustomizationFormText = "linkedChildDataGroup";
            this.linkedChildDataGroup.Location = new System.Drawing.Point(0, 825);
            this.linkedChildDataGroup.Name = "linkedChildDataGroup";
            this.linkedChildDataGroup.SelectedTabPage = this.layoutControlGroup4;
            this.linkedChildDataGroup.SelectedTabPageIndex = 0;
            this.linkedChildDataGroup.Size = new System.Drawing.Size(1325, 275);
            this.linkedChildDataGroup.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup4});
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Linked Child Data";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem27,
            this.emptySpaceItem2});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(1301, 230);
            this.layoutControlGroup4.Text = "Linked Child Data";
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.Control = this.equipmentChildTabControl;
            this.layoutControlItem27.CustomizationFormText = "layoutControlItem27";
            this.layoutControlItem27.Location = new System.Drawing.Point(2, 0);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(1299, 230);
            this.layoutControlItem27.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem27.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(10, 230);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(1, 230);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(2, 230);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // equipmentLayoutControlGroup
            // 
            this.equipmentLayoutControlGroup.CustomizationFormText = "General Equipment Details";
            this.equipmentLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.equipRefLayoutControlGroup,
            this.depreciationLayoutControlGroup,
            this.GCLayoutControlGroup,
            this.financeLayoutControlGroup});
            this.equipmentLayoutControlGroup.Location = new System.Drawing.Point(0, 23);
            this.equipmentLayoutControlGroup.Name = "equipmentLayoutControlGroup";
            this.equipmentLayoutControlGroup.Size = new System.Drawing.Size(518, 399);
            this.equipmentLayoutControlGroup.Text = "General Equipment Details";
            // 
            // equipRefLayoutControlGroup
            // 
            this.equipRefLayoutControlGroup.CaptionImage = ((System.Drawing.Image)(resources.GetObject("equipRefLayoutControlGroup.CaptionImage")));
            this.equipRefLayoutControlGroup.CustomizationFormText = "Equpment Reference";
            this.equipRefLayoutControlGroup.ExpandButtonVisible = true;
            this.equipRefLayoutControlGroup.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.equipRefLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem6,
            this.emptySpaceItem5});
            this.equipRefLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.equipRefLayoutControlGroup.Name = "equipRefLayoutControlGroup";
            this.equipRefLayoutControlGroup.Size = new System.Drawing.Size(245, 182);
            this.equipRefLayoutControlGroup.Text = "Equpment Reference";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtEquipmentReference;
            this.layoutControlItem4.CustomizationFormText = "Ground Control Reference:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(221, 24);
            this.layoutControlItem4.Text = "Ground Control Reference:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.lueEquipment_Category;
            this.layoutControlItem6.CustomizationFormText = "Equipment Category:";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(221, 24);
            this.layoutControlItem6.Text = "Equipment Category:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(164, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 48);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(221, 86);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // depreciationLayoutControlGroup
            // 
            this.depreciationLayoutControlGroup.CustomizationFormText = "Depreciation Details";
            this.depreciationLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem22,
            this.layoutControlItem40,
            this.layoutControlItem56,
            this.layoutControlItem61,
            this.layoutControlItem60});
            this.depreciationLayoutControlGroup.Location = new System.Drawing.Point(245, 191);
            this.depreciationLayoutControlGroup.Name = "depreciationLayoutControlGroup";
            this.depreciationLayoutControlGroup.Size = new System.Drawing.Size(249, 164);
            this.depreciationLayoutControlGroup.Text = "Depreciation Details";
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.deDepreciation_Start_Date;
            this.layoutControlItem22.CustomizationFormText = "Depreciation Start Date:";
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(225, 24);
            this.layoutControlItem22.Text = "Depreciation Start Date:";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem40
            // 
            this.layoutControlItem40.Control = this.lueOccursUnitDescriptor;
            this.layoutControlItem40.CustomizationFormText = "Occurs Unit Descriptor:";
            this.layoutControlItem40.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem40.Name = "layoutControlItem40";
            this.layoutControlItem40.Size = new System.Drawing.Size(225, 24);
            this.layoutControlItem40.Text = "Occurs Unit Descriptor:";
            this.layoutControlItem40.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem56
            // 
            this.layoutControlItem56.Control = this.spnMaximumOccurrence;
            this.layoutControlItem56.CustomizationFormText = "Maximum Occurrence:";
            this.layoutControlItem56.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem56.Name = "layoutControlItem56";
            this.layoutControlItem56.Size = new System.Drawing.Size(225, 24);
            this.layoutControlItem56.Text = "Maximum Occurrence:";
            this.layoutControlItem56.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem61
            // 
            this.layoutControlItem61.Control = this.spnDayOfMonthApplyDepreciation;
            this.layoutControlItem61.CustomizationFormText = "Day Of Month Apply Depreciation:";
            this.layoutControlItem61.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem61.Name = "layoutControlItem61";
            this.layoutControlItem61.Size = new System.Drawing.Size(225, 24);
            this.layoutControlItem61.Text = "Day Of Month Apply Depreciation:";
            this.layoutControlItem61.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem60
            // 
            this.layoutControlItem60.Control = this.lueDepreciationSetting;
            this.layoutControlItem60.CustomizationFormText = "Depreciation Setting:";
            this.layoutControlItem60.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem60.Name = "layoutControlItem60";
            this.layoutControlItem60.Size = new System.Drawing.Size(225, 24);
            this.layoutControlItem60.Text = "Depreciation Setting:";
            this.layoutControlItem60.TextSize = new System.Drawing.Size(164, 13);
            // 
            // GCLayoutControlGroup
            // 
            this.GCLayoutControlGroup.CaptionImage = ((System.Drawing.Image)(resources.GetObject("GCLayoutControlGroup.CaptionImage")));
            this.GCLayoutControlGroup.CustomizationFormText = "Ground Control Details";
            this.GCLayoutControlGroup.ExpandButtonVisible = true;
            this.GCLayoutControlGroup.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.GCLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem16,
            this.layoutControlItem18,
            this.layoutControlItem20,
            this.layoutControlItem30,
            this.layoutControlItem59,
            this.layoutControlItem8});
            this.GCLayoutControlGroup.Location = new System.Drawing.Point(245, 0);
            this.GCLayoutControlGroup.Name = "GCLayoutControlGroup";
            this.GCLayoutControlGroup.Size = new System.Drawing.Size(249, 191);
            this.GCLayoutControlGroup.Text = "Ground Control Details";
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.lueGc_Marked;
            this.layoutControlItem16.CustomizationFormText = "GC Marked:";
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(225, 24);
            this.layoutControlItem16.Text = "GC Marked:";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.lueOwnership_Status;
            this.layoutControlItem18.CustomizationFormText = "Ownership Status:";
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(225, 24);
            this.layoutControlItem18.Text = "Ownership Status:";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.lueAvailability;
            this.layoutControlItem20.CustomizationFormText = "Availability:";
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(225, 24);
            this.layoutControlItem20.Text = "Availability:";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.Control = this.deliveryDateDateEdit;
            this.layoutControlItem30.CustomizationFormText = "Delivery Date:";
            this.layoutControlItem30.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(225, 24);
            this.layoutControlItem30.Text = "Delivery Date:";
            this.layoutControlItem30.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem59
            // 
            this.layoutControlItem59.Control = this.ceArchive;
            this.layoutControlItem59.CustomizationFormText = "Archive:";
            this.layoutControlItem59.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem59.Name = "layoutControlItem59";
            this.layoutControlItem59.Size = new System.Drawing.Size(225, 23);
            this.layoutControlItem59.Text = "Archive:";
            this.layoutControlItem59.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem59.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.soldDateDateEdit;
            this.layoutControlItem8.CustomizationFormText = "Sold Date:";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(225, 24);
            this.layoutControlItem8.Text = "Sold Date:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(164, 13);
            // 
            // financeLayoutControlGroup
            // 
            this.financeLayoutControlGroup.CaptionImage = ((System.Drawing.Image)(resources.GetObject("financeLayoutControlGroup.CaptionImage")));
            this.financeLayoutControlGroup.CustomizationFormText = "Finance Details";
            this.financeLayoutControlGroup.ExpandButtonVisible = true;
            this.financeLayoutControlGroup.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.financeLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem24,
            this.layoutControlItem26,
            this.emptySpaceItem10,
            this.layoutControlItem68});
            this.financeLayoutControlGroup.Location = new System.Drawing.Point(0, 182);
            this.financeLayoutControlGroup.Name = "financeLayoutControlGroup";
            this.financeLayoutControlGroup.Size = new System.Drawing.Size(245, 173);
            this.financeLayoutControlGroup.Text = "Finance Details";
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.txtEx_Asset_ID;
            this.layoutControlItem24.CustomizationFormText = "Ex Asset ID:";
            this.layoutControlItem24.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(221, 24);
            this.layoutControlItem24.Text = "Exchequer ID:";
            this.layoutControlItem24.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.txtSage_Asset_Ref;
            this.layoutControlItem26.CustomizationFormText = "Sage Asset Ref:";
            this.layoutControlItem26.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(221, 24);
            this.layoutControlItem26.Text = "Sage Reference:";
            this.layoutControlItem26.TextSize = new System.Drawing.Size(164, 13);
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.CustomizationFormText = "emptySpaceItem10";
            this.emptySpaceItem10.Location = new System.Drawing.Point(0, 72);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(221, 53);
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem68
            // 
            this.layoutControlItem68.Control = this.lueExchequerCategoryID;
            this.layoutControlItem68.CustomizationFormText = "Exchequer Category :";
            this.layoutControlItem68.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem68.Name = "layoutControlItem68";
            this.layoutControlItem68.Size = new System.Drawing.Size(221, 24);
            this.layoutControlItem68.Text = "Exchequer Category :";
            this.layoutControlItem68.TextSize = new System.Drawing.Size(164, 13);
            // 
            // manufacturerLayoutControlGroup
            // 
            this.manufacturerLayoutControlGroup.CaptionImage = ((System.Drawing.Image)(resources.GetObject("manufacturerLayoutControlGroup.CaptionImage")));
            this.manufacturerLayoutControlGroup.CustomizationFormText = "Manufacturer Details";
            this.manufacturerLayoutControlGroup.ExpandButtonVisible = true;
            this.manufacturerLayoutControlGroup.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.manufacturerLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lblManufacturerID,
            this.layoutControlItem10,
            this.layoutControlItem12,
            this.layoutControlItem14,
            this.emptySpaceItem11});
            this.manufacturerLayoutControlGroup.Location = new System.Drawing.Point(518, 23);
            this.manufacturerLayoutControlGroup.Name = "manufacturerLayoutControlGroup";
            this.manufacturerLayoutControlGroup.Size = new System.Drawing.Size(807, 210);
            this.manufacturerLayoutControlGroup.Text = "Manufacturer Details";
            // 
            // lblManufacturerID
            // 
            this.lblManufacturerID.Control = this.txtManufacturer_ID;
            this.lblManufacturerID.CustomizationFormText = "Manufacturer ID:";
            this.lblManufacturerID.Location = new System.Drawing.Point(0, 0);
            this.lblManufacturerID.Name = "lblManufacturerID";
            this.lblManufacturerID.Size = new System.Drawing.Size(783, 24);
            this.lblManufacturerID.Text = "Manufacturer ID:";
            this.lblManufacturerID.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.lueMake;
            this.layoutControlItem10.CustomizationFormText = "Make:";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(783, 26);
            this.layoutControlItem10.Text = "Make:";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.lueModel;
            this.layoutControlItem12.CustomizationFormText = "Model:";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 50);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(783, 26);
            this.layoutControlItem12.Text = "Model:";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.lueSupplier;
            this.layoutControlItem14.CustomizationFormText = "Supplier:";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 76);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(783, 26);
            this.layoutControlItem14.Text = "Supplier:";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(164, 13);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.CustomizationFormText = "emptySpaceItem11";
            this.emptySpaceItem11.Location = new System.Drawing.Point(0, 102);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(783, 60);
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.CustomizationFormText = "emptySpaceItem13";
            this.emptySpaceItem13.Location = new System.Drawing.Point(445, 0);
            this.emptySpaceItem13.Name = "emptySpaceItem13";
            this.emptySpaceItem13.Size = new System.Drawing.Size(880, 23);
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem55
            // 
            this.layoutControlItem55.Control = this.equipmentDataNavigator;
            this.layoutControlItem55.CustomizationFormText = "layoutControlItem55";
            this.layoutControlItem55.Location = new System.Drawing.Point(264, 0);
            this.layoutControlItem55.Name = "layoutControlItem55";
            this.layoutControlItem55.Size = new System.Drawing.Size(181, 23);
            this.layoutControlItem55.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem55.TextVisible = false;
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(264, 23);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // remarksLayoutControlGroup
            // 
            this.remarksLayoutControlGroup.CaptionImage = ((System.Drawing.Image)(resources.GetObject("remarksLayoutControlGroup.CaptionImage")));
            this.remarksLayoutControlGroup.CustomizationFormText = "Remarks";
            this.remarksLayoutControlGroup.ExpandButtonVisible = true;
            this.remarksLayoutControlGroup.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.remarksLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.remarksLayoutControlGroup.Location = new System.Drawing.Point(518, 233);
            this.remarksLayoutControlGroup.Name = "remarksLayoutControlGroup";
            this.remarksLayoutControlGroup.Size = new System.Drawing.Size(807, 178);
            this.remarksLayoutControlGroup.Text = "Remarks";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.meNotes;
            this.layoutControlItem1.CustomizationFormText = "Notes:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(783, 130);
            this.layoutControlItem1.Text = "Notes:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // popupContainerControl1
            // 
            this.popupContainerControl1.Controls.Add(this.gridControl1);
            this.popupContainerControl1.Controls.Add(this.btnPopUp1Ok);
            this.popupContainerControl1.Location = new System.Drawing.Point(283, 12);
            this.popupContainerControl1.Name = "popupContainerControl1";
            this.popupContainerControl1.Size = new System.Drawing.Size(351, 302);
            this.popupContainerControl1.TabIndex = 5;
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.DataSource = this.spAS11000FleetManagerDummyScreenBindingSource;
            this.gridControl1.Location = new System.Drawing.Point(0, 3);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(344, 269);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // spAS11000FleetManagerDummyScreenBindingSource
            // 
            this.spAS11000FleetManagerDummyScreenBindingSource.DataMember = "sp_AS_11000_FleetManagerDummyScreen";
            this.spAS11000FleetManagerDummyScreenBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription,
            this.colID});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colID, DevExpress.Data.ColumnSortOrder.Descending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Linked Data";
            this.colDescription.FieldName = "Description";
            this.colDescription.MinWidth = 50;
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 490;
            // 
            // colID
            // 
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            // 
            // btnPopUp1Ok
            // 
            this.btnPopUp1Ok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPopUp1Ok.Location = new System.Drawing.Point(4, 276);
            this.btnPopUp1Ok.Name = "btnPopUp1Ok";
            this.btnPopUp1Ok.Size = new System.Drawing.Size(75, 23);
            this.btnPopUp1Ok.TabIndex = 0;
            this.btnPopUp1Ok.Text = "Ok";
            this.btnPopUp1Ok.Click += new System.EventHandler(this.btnPopUp1Ok_Click);
            // 
            // layoutControlItem58
            // 
            this.layoutControlItem58.Control = this.lueAvailability;
            this.layoutControlItem58.CustomizationFormText = "Availability:";
            this.layoutControlItem58.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem58.Name = "layoutControlItem20";
            this.layoutControlItem58.Size = new System.Drawing.Size(494, 24);
            this.layoutControlItem58.Text = "Availability:";
            this.layoutControlItem58.TextSize = new System.Drawing.Size(149, 13);
            // 
            // sp_AS_11005_Vehicle_ItemTableAdapter
            // 
            this.sp_AS_11005_Vehicle_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11008_Plant_ItemTableAdapter
            // 
            this.sp_AS_11008_Plant_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11035_Software_ItemTableAdapter
            // 
            this.sp_AS_11035_Software_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11032_Hardware_ItemTableAdapter
            // 
            this.sp_AS_11032_Hardware_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11011_Equipment_Category_ListTableAdapter
            // 
            this.sp_AS_11011_Equipment_Category_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Equipment_GC_MarkedTableAdapter
            // 
            this.sp_AS_11000_PL_Equipment_GC_MarkedTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Equipment_Ownership_StatusTableAdapter
            // 
            this.sp_AS_11000_PL_Equipment_Ownership_StatusTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Equipment_AvailabilityTableAdapter
            // 
            this.sp_AS_11000_PL_Equipment_AvailabilityTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11014_Make_ListTableAdapter
            // 
            this.sp_AS_11014_Make_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11017_Model_ListTableAdapter
            // 
            this.sp_AS_11017_Model_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11020_Supplier_ListTableAdapter
            // 
            this.sp_AS_11020_Supplier_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11023_Tracker_ListTableAdapter
            // 
            this.sp_AS_11023_Tracker_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Vehicle_Towbar_TypeTableAdapter
            // 
            this.sp_AS_11000_PL_Vehicle_Towbar_TypeTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Plant_CategoryTableAdapter
            // 
            this.sp_AS_11000_PL_Plant_CategoryTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_FuelTableAdapter
            // 
            this.sp_AS_11000_PL_FuelTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Gadget_TypeTableAdapter
            // 
            this.sp_AS_11000_PL_Gadget_TypeTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_CasingTableAdapter
            // 
            this.sp_AS_11000_PL_CasingTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Hardware_TypeTableAdapter
            // 
            this.sp_AS_11000_PL_Hardware_TypeTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Software_ProductTableAdapter
            // 
            this.sp_AS_11000_PL_Software_ProductTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Software_VersionTableAdapter
            // 
            this.sp_AS_11000_PL_Software_VersionTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Licence_TypeTableAdapter
            // 
            this.sp_AS_11000_PL_Licence_TypeTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Software_Acquisation_MethodTableAdapter
            // 
            this.sp_AS_11000_PL_Software_Acquisation_MethodTableAdapter.ClearBeforeFill = true;
            // 
            // spAS11000DuplicateMakeModelSearchBindingSource
            // 
            this.spAS11000DuplicateMakeModelSearchBindingSource.DataMember = "sp_AS_11000_Duplicate_Make_Model_Search";
            this.spAS11000DuplicateMakeModelSearchBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11000_Duplicate_Make_Model_SearchTableAdapter
            // 
            this.sp_AS_11000_Duplicate_Make_Model_SearchTableAdapter.ClearBeforeFill = true;
            // 
            // spAS11000DuplicateSearchBindingSource
            // 
            this.spAS11000DuplicateSearchBindingSource.DataMember = "sp_AS_11000_Duplicate_Search";
            this.spAS11000DuplicateSearchBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11000_Duplicate_SearchTableAdapter
            // 
            this.sp_AS_11000_Duplicate_SearchTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11066_Depreciation_Settings_ItemTableAdapter
            // 
            this.sp_AS_11066_Depreciation_Settings_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Unit_DescriptorTableAdapter
            // 
            this.sp_AS_11000_PL_Unit_DescriptorTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11029_Office_ItemTableAdapter
            // 
            this.sp_AS_11029_Office_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Office_CategoryTableAdapter
            // 
            this.sp_AS_11000_PL_Office_CategoryTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Vehicle_CategoryTableAdapter
            // 
            this.sp_AS_11000_PL_Vehicle_CategoryTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11026_Gadget_ItemTableAdapter
            // 
            this.sp_AS_11026_Gadget_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // bar2
            // 
            this.bar2.BarName = "Filter";
            this.bar2.DockCol = 1;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItem1)});
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.Text = "Filter";
            // 
            // barEditItem1
            // 
            this.barEditItem1.Caption = "Linked Data :";
            this.barEditItem1.Edit = this.repositoryItemPopupContainerEdit1;
            this.barEditItem1.EditValue = "No Linked Data Filter";
            this.barEditItem1.EditWidth = 353;
            this.barEditItem1.Id = 35;
            this.barEditItem1.Name = "barEditItem1";
            this.barEditItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // repositoryItemPopupContainerEdit1
            // 
            this.repositoryItemPopupContainerEdit1.AutoHeight = false;
            this.repositoryItemPopupContainerEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit1.Name = "repositoryItemPopupContainerEdit1";
            this.repositoryItemPopupContainerEdit1.PopupControl = this.popupContainerControl1;
            this.repositoryItemPopupContainerEdit1.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEdit1.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEdit1_QueryResultValue);
            // 
            // sp_AS_11000_FleetManagerDummyScreenTableAdapter
            // 
            this.sp_AS_11000_FleetManagerDummyScreenTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11044_Keeper_Allocation_ItemTableAdapter
            // 
            this.sp_AS_11044_Keeper_Allocation_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11041_Transaction_ItemTableAdapter
            // 
            this.sp_AS_11041_Transaction_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11050_Depreciation_ItemTableAdapter
            // 
            this.sp_AS_11050_Depreciation_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11075_Cover_ItemTableAdapter
            // 
            this.sp_AS_11075_Cover_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11078_Work_Detail_ItemTableAdapter
            // 
            this.sp_AS_11078_Work_Detail_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11081_Incident_ItemTableAdapter
            // 
            this.sp_AS_11081_Incident_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11084_Purpose_ItemTableAdapter
            // 
            this.sp_AS_11084_Purpose_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11089_Fuel_Card_ItemTableAdapter
            // 
            this.sp_AS_11089_Fuel_Card_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11105_Speeding_ItemTableAdapter
            // 
            this.sp_AS_11105_Speeding_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11002_Equipment_ItemTableAdapter
            // 
            this.sp_AS_11002_Equipment_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp_AS_11044_Keeper_Allocation_ListTableAdapter
            // 
            this.sp_AS_11044_Keeper_Allocation_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Incident_StatusTableAdapter
            // 
            this.sp_AS_11000_PL_Incident_StatusTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Incident_TypeTableAdapter
            // 
            this.sp_AS_11000_PL_Incident_TypeTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Incident_Repair_DueTableAdapter
            // 
            this.sp_AS_11000_PL_Incident_Repair_DueTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_SeverityTableAdapter
            // 
            this.sp_AS_11000_PL_SeverityTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Legal_ActionTableAdapter
            // 
            this.sp_AS_11000_PL_Legal_ActionTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Keeper_At_FaultTableAdapter
            // 
            this.sp_AS_11000_PL_Keeper_At_FaultTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11096_Road_Tax_ItemTableAdapter
            // 
            this.sp_AS_11096_Road_Tax_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11141_Hardware_Specifications_ItemTableAdapter
            // 
            this.sp_AS_11141_Hardware_Specifications_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Exchequer_CategoryTableAdapter
            // 
            this.sp_AS_11000_PL_Exchequer_CategoryTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11054_Service_Data_ItemTableAdapter
            // 
            this.sp_AS_11054_Service_Data_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // bar3
            // 
            this.bar3.BarName = "Status Bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barStaticItem2, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem3),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem4),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barStaticItem5, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItem2
            // 
            this.barStaticItem2.Caption = "Form Mode: Editing";
            this.barStaticItem2.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItem2.Glyph")));
            this.barStaticItem2.Id = 29;
            this.barStaticItem2.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barStaticItem2.LargeGlyph")));
            this.barStaticItem2.Name = "barStaticItem2";
            toolTipTitleItem10.Text = "Form Mode - Information";
            toolTipItem10.LeftIndent = 6;
            toolTipItem10.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add";
            superToolTip10.Items.Add(toolTipTitleItem10);
            superToolTip10.Items.Add(toolTipItem10);
            this.barStaticItem2.SuperTip = superToolTip10;
            this.barStaticItem2.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem3
            // 
            this.barStaticItem3.Caption = "Records Loaded: 1";
            this.barStaticItem3.Id = 31;
            this.barStaticItem3.Name = "barStaticItem3";
            this.barStaticItem3.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem4
            // 
            this.barStaticItem4.Caption = "Changes Pending";
            this.barStaticItem4.Id = 32;
            this.barStaticItem4.Name = "barStaticItem4";
            this.barStaticItem4.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItem4.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItem5
            // 
            this.barStaticItem5.Caption = "Information:";
            this.barStaticItem5.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItem5.Glyph")));
            this.barStaticItem5.Id = 33;
            this.barStaticItem5.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barStaticItem5.LargeGlyph")));
            this.barStaticItem5.Name = "barStaticItem5";
            this.barStaticItem5.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bar4
            // 
            this.bar4.BarName = "bar1";
            this.bar4.DockCol = 0;
            this.bar4.DockRow = 0;
            this.bar4.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar4.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar4.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2)});
            this.bar4.Text = "Screen Functions";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Save";
            this.barButtonItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.Glyph")));
            this.barButtonItem1.Id = 27;
            this.barButtonItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.LargeGlyph")));
            this.barButtonItem1.Name = "barButtonItem1";
            toolTipTitleItem11.Appearance.Options.UseImage = true;
            toolTipTitleItem11.Text = "Save Button - Information";
            toolTipItem11.LeftIndent = 6;
            toolTipItem11.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip11.Items.Add(toolTipTitleItem11);
            superToolTip11.Items.Add(toolTipItem11);
            this.barButtonItem1.SuperTip = superToolTip11;
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Cancel";
            this.barButtonItem2.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem2.Glyph")));
            this.barButtonItem2.Id = 28;
            this.barButtonItem2.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem2.LargeGlyph")));
            this.barButtonItem2.Name = "barButtonItem2";
            toolTipTitleItem12.Appearance.Options.UseImage = true;
            toolTipTitleItem12.Text = "Cancel Button - Information";
            toolTipItem12.LeftIndent = 6;
            toolTipItem12.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip12.Items.Add(toolTipTitleItem12);
            superToolTip12.Items.Add(toolTipItem12);
            this.barButtonItem2.SuperTip = superToolTip12;
            // 
            // bar5
            // 
            this.bar5.BarName = "Filter";
            this.bar5.DockCol = 1;
            this.bar5.DockRow = 0;
            this.bar5.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar5.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItem2)});
            this.bar5.OptionsBar.DrawDragBorder = false;
            this.bar5.Text = "Filter";
            // 
            // barEditItem2
            // 
            this.barEditItem2.Caption = "Linked Data :";
            this.barEditItem2.Edit = this.repositoryItemPopupContainerEdit2;
            this.barEditItem2.EditValue = "No Linked Data Filter";
            this.barEditItem2.EditWidth = 353;
            this.barEditItem2.Id = 35;
            this.barEditItem2.Name = "barEditItem2";
            this.barEditItem2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // repositoryItemPopupContainerEdit2
            // 
            this.repositoryItemPopupContainerEdit2.AutoHeight = false;
            this.repositoryItemPopupContainerEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit2.Name = "repositoryItemPopupContainerEdit2";
            this.repositoryItemPopupContainerEdit2.PopupControl = this.popupContainerControl2;
            this.repositoryItemPopupContainerEdit2.ShowPopupCloseButton = false;
            // 
            // popupContainerControl2
            // 
            this.popupContainerControl2.Controls.Add(this.gridControl2);
            this.popupContainerControl2.Controls.Add(this.simpleButton1);
            this.popupContainerControl2.Location = new System.Drawing.Point(283, 12);
            this.popupContainerControl2.Name = "popupContainerControl2";
            this.popupContainerControl2.Size = new System.Drawing.Size(351, 302);
            this.popupContainerControl2.TabIndex = 5;
            // 
            // gridControl2
            // 
            this.gridControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl2.Location = new System.Drawing.Point(0, 3);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(344, 269);
            this.gridControl2.TabIndex = 1;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn2, DevExpress.Data.ColumnSortOrder.Descending)});
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Linked Data";
            this.gridColumn1.FieldName = "Description";
            this.gridColumn1.MinWidth = 50;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 490;
            // 
            // gridColumn2
            // 
            this.gridColumn2.FieldName = "ID";
            this.gridColumn2.Name = "gridColumn2";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButton1.Location = new System.Drawing.Point(4, 276);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "Ok";
            // 
            // barStaticItem6
            // 
            this.barStaticItem6.Id = -1;
            this.barStaticItem6.Name = "barStaticItem6";
            this.barStaticItem6.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // frm_AS_Equipment_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1362, 740);
            this.Controls.Add(this.equipmentDataLayoutControl);
            this.Controls.Add(this.popupContainerControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AS_Equipment_Edit";
            this.Text = "Manage Equipment";
            this.Activated += new System.EventHandler(this.frm_AS_Equipment_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_AS_Equipment_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_AS_Equipment_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.popupContainerControl1, 0);
            this.Controls.SetChildIndex(this.equipmentDataLayoutControl, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProviderEquipment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueEquipment_Category.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11002EquipmentItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11011EquipmentCategoryListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentDataLayoutControl)).EndInit();
            this.equipmentDataLayoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtPlantRegistrationPlate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11008PlantItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueExchequerCategoryID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLExchequerCategoryBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dePUWERDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dePUWERDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deLOLERDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deLOLERDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceTag.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11032HardwareItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSerialNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueOfficeCategoryID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11029_Office_ItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLOfficeCategoryBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIMEI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11026GadgetItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentChildTabControl)).EndInit();
            this.equipmentChildTabControl.ResumeLayout(false);
            this.roadTaxTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.roadTaxGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11096RoadTaxItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.roadTaxGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit4)).EndInit();
            this.keeperTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.keeperGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11044KeeperAllocationItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.keeperGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueKeeperNotes)).EndInit();
            this.transactionTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.transactionGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11041TransactionItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.transactionsGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceOnExchequer)).EndInit();
            this.purposeTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.purposeGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11084PurposeItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.purposeGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit17)).EndInit();
            this.fuelCardTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fuelCardGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11089FuelCardItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fuelCardGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit19)).EndInit();
            this.trackerTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.trackerGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11023TrackerListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackerGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit10)).EndInit();
            this.billingTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.billingGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11050DepreciationItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.billingGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            this.depreciationTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.depreciationGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.depreciationGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEditDep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            this.coverTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.coverGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11075CoverItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.coverGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit1)).EndInit();
            this.serviceDatalTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.serviceDataGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11054ServiceDataItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.serviceDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit12)).EndInit();
            this.workDetailTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.workDetailGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11078WorkDetailItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.workDetailGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit14)).EndInit();
            this.incidentTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.incidentGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11081IncidentItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.incidentGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.incidentNotesMemoEdit)).EndInit();
            this.speedingTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.speedingGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11105SpeedingItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speedingGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceReported)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShortDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueDepreciationSetting.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11066DepreciationSettingsItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueVehicleCategoryID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11005VehicleItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLVehicleCategoryBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deliveryDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deliveryDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnDayOfMonthApplyDepreciation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnMaximumOccurrence.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueOccursUnitDescriptor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLUnitDescriptorBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceArchive.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnQuantity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSoftwareProduct_ID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11035SoftwareItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLSoftwareProductBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSoftwareVersion_ID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLSoftwareVersionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deSoftwareYear.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deSoftwareYear.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoftwareLicence.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSoftwareParent_Program.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSoftwareAcquisation_Method_ID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLSoftwareAcquisationMethodBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnSoftwareQuantity_Limit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoftwareLicence_Key.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSoftwareLicence_Type_ID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLLicenceTypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deSoftwareValid_Until.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deSoftwareValid_Until.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueHardware_Type_ID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLHardwareTypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueHardwareSpecification_ID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11141HardwareSpecificationsItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHardwareDefault_Password.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueGadget_Type_ID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLGadgetTypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueGadgetCasing_ID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLCasingBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luePlantCategory_ID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLPlantCategoryBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnPlantEngine_Size.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luePlantFuel_Type_ID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLFuelBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegistration_Plate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLog_Book_Number.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtModel_Specifications.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnVehicleEngine_Size.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueVehicleFuel_Type_ID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnEmissions.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnTotal_Tyre_Number.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTowbar_Type_ID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLVehicleTowbarTypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cpeColour.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deRegistration_Date.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deRegistration_Date.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deMOT_Due_Date.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deMOT_Due_Date.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnCurrentMileage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnService_Due_Mileage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRadioCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKeyCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtElectronicCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentReference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtManufacturer_ID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueMake.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11014MakeListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueModel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11017ModelListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSupplier.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11020SupplierListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueGc_Marked.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLEquipmentGCMarkedBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueOwnership_Status.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLEquipmentOwnershipStatusBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueAvailability.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLEquipmentAvailabilityBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDepreciation_Start_Date.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDepreciation_Start_Date.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEx_Asset_ID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSage_Asset_Ref.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meNotes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.soldDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.soldDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehicleLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehRegistrationLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehSpecificationLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehMaintenanceLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehCodesLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehPerfomanceLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoDetailsLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem67)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gadgetLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hardwareLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem66)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.softwareLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.officeLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkedChildDataGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipRefLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.depreciationLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GCLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.financeLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem68)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.manufacturerLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblManufacturerID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.remarksLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl1)).EndInit();
            this.popupContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000FleetManagerDummyScreenBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000DuplicateMakeModelSearchBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000DuplicateSearchBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl2)).EndInit();
            this.popupContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private DataSet_AS_DataEntryTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraBars.Bar barStatus;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.BarStaticItem bsiFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProviderEquipment;
        private DevExpress.XtraDataLayout.DataLayoutControl equipmentDataLayoutControl;
        private DevExpress.XtraEditors.TextEdit txtEquipmentReference;
        private DevExpress.XtraEditors.LookUpEdit lueEquipment_Category;
        private DevExpress.XtraEditors.TextEdit txtManufacturer_ID;
        private DevExpress.XtraEditors.LookUpEdit lueMake;
        private DevExpress.XtraEditors.LookUpEdit lueModel;
        private DevExpress.XtraEditors.LookUpEdit lueSupplier;
        private DevExpress.XtraEditors.LookUpEdit lueGc_Marked;
        private DevExpress.XtraEditors.LookUpEdit lueOwnership_Status;
        private DevExpress.XtraEditors.LookUpEdit lueAvailability;
        private DevExpress.XtraEditors.DateEdit deDepreciation_Start_Date;
        private DevExpress.XtraEditors.TextEdit txtEx_Asset_ID;
        private DevExpress.XtraEditors.TextEdit txtSage_Asset_Ref;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.LookUpEdit luePlantCategory_ID;
        private DevExpress.XtraEditors.SpinEdit spnPlantEngine_Size;
        private DevExpress.XtraEditors.LookUpEdit luePlantFuel_Type_ID;
        private DevExpress.XtraEditors.TextEdit txtRegistration_Plate;
        private DevExpress.XtraEditors.TextEdit txtLog_Book_Number;
        private DevExpress.XtraEditors.TextEdit txtModel_Specifications;
        private DevExpress.XtraEditors.SpinEdit spnVehicleEngine_Size;
        private DevExpress.XtraEditors.LookUpEdit lueVehicleFuel_Type_ID;
        private DevExpress.XtraEditors.SpinEdit spnEmissions;
        private DevExpress.XtraEditors.SpinEdit spnTotal_Tyre_Number;
        private DevExpress.XtraEditors.LookUpEdit lueTowbar_Type_ID;
        private DevExpress.XtraEditors.ColorPickEdit cpeColour;
        private DevExpress.XtraEditors.DateEdit deRegistration_Date;
        private DevExpress.XtraEditors.DateEdit deMOT_Due_Date;
        private DevExpress.XtraEditors.SpinEdit spnCurrentMileage;
        private DevExpress.XtraEditors.SpinEdit spnService_Due_Mileage;
        private DevExpress.XtraEditors.TextEdit txtRadioCode;
        private DevExpress.XtraEditors.TextEdit txtKeyCode;
        private DevExpress.XtraEditors.TextEdit txtElectronicCode;
        private DevExpress.XtraEditors.LookUpEdit lueGadget_Type_ID;
        private DevExpress.XtraEditors.LookUpEdit lueGadgetCasing_ID;
        private DevExpress.XtraEditors.LookUpEdit lueHardware_Type_ID;
        private DevExpress.XtraEditors.LookUpEdit lueHardwareSpecification_ID;
        private DevExpress.XtraEditors.TextEdit txtHardwareDefault_Password;
        private DevExpress.XtraEditors.LookUpEdit lueSoftwareProduct_ID;
        private DevExpress.XtraEditors.LookUpEdit lueSoftwareVersion_ID;
        private DevExpress.XtraEditors.DateEdit deSoftwareYear;
        private DevExpress.XtraEditors.TextEdit txtSoftwareLicence;
        private DevExpress.XtraEditors.LookUpEdit lueSoftwareParent_Program;
        private DevExpress.XtraEditors.LookUpEdit lueSoftwareAcquisation_Method_ID;
        private DevExpress.XtraEditors.SpinEdit spnSoftwareQuantity_Limit;
        private DevExpress.XtraEditors.TextEdit txtSoftwareLicence_Key;
        private DevExpress.XtraEditors.LookUpEdit lueSoftwareLicence_Type_ID;
        private DevExpress.XtraEditors.DateEdit deSoftwareValid_Until;
        private DevExpress.XtraEditors.SpinEdit spnQuantity;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup plantLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlGroup gadgetLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private DevExpress.XtraLayout.LayoutControlGroup hardwareLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem41;
        private DevExpress.XtraLayout.LayoutControlGroup softwareLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlGroup officeLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.DataNavigator equipmentDataNavigator;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem55;
        private DevExpress.XtraLayout.LayoutControlGroup NoDetailsLayoutControlGroup;
        private DevExpress.XtraEditors.CheckEdit ceArchive;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem58;
        private DevExpress.XtraEditors.MemoEdit meNotes;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraEditors.SpinEdit spnDayOfMonthApplyDepreciation;
        private DevExpress.XtraEditors.SpinEdit spnMaximumOccurrence;
        private DevExpress.XtraEditors.LookUpEdit lueOccursUnitDescriptor;
        private DevExpress.XtraEditors.DateEdit deliveryDateDateEdit;
        private System.Windows.Forms.BindingSource spAS11035SoftwareItemBindingSource;
        private System.Windows.Forms.BindingSource spAS11032HardwareItemBindingSource;
        private System.Windows.Forms.BindingSource spAS11008PlantItemBindingSource;
        private System.Windows.Forms.BindingSource spAS11005VehicleItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11005_Vehicle_ItemTableAdapter sp_AS_11005_Vehicle_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11008_Plant_ItemTableAdapter sp_AS_11008_Plant_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11035_Software_ItemTableAdapter sp_AS_11035_Software_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11032_Hardware_ItemTableAdapter sp_AS_11032_Hardware_ItemTableAdapter;
        private DevExpress.XtraEditors.MemoEdit memDescription;
        private System.Windows.Forms.BindingSource spAS11000PLHardwareTypeBindingSource;
        private System.Windows.Forms.BindingSource spAS11000PLGadgetTypeBindingSource;
        private System.Windows.Forms.BindingSource spAS11000PLCasingBindingSource;
        private System.Windows.Forms.BindingSource spAS11000PLPlantCategoryBindingSource;
        private System.Windows.Forms.BindingSource spAS11000PLFuelBindingSource;
        private System.Windows.Forms.BindingSource spAS11023TrackerListBindingSource;
        private System.Windows.Forms.BindingSource spAS11000PLVehicleTowbarTypeBindingSource;
        private System.Windows.Forms.BindingSource spAS11011EquipmentCategoryListBindingSource;
        private System.Windows.Forms.BindingSource spAS11014MakeListBindingSource;
        private System.Windows.Forms.BindingSource spAS11017ModelListBindingSource;
        private System.Windows.Forms.BindingSource spAS11020SupplierListBindingSource;
        private System.Windows.Forms.BindingSource spAS11000PLEquipmentGCMarkedBindingSource;
        private System.Windows.Forms.BindingSource spAS11000PLEquipmentOwnershipStatusBindingSource;
        private System.Windows.Forms.BindingSource spAS11000PLEquipmentAvailabilityBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11011_Equipment_Category_ListTableAdapter sp_AS_11011_Equipment_Category_ListTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Equipment_GC_MarkedTableAdapter sp_AS_11000_PL_Equipment_GC_MarkedTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Equipment_Ownership_StatusTableAdapter sp_AS_11000_PL_Equipment_Ownership_StatusTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Equipment_AvailabilityTableAdapter sp_AS_11000_PL_Equipment_AvailabilityTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11014_Make_ListTableAdapter sp_AS_11014_Make_ListTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11017_Model_ListTableAdapter sp_AS_11017_Model_ListTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11020_Supplier_ListTableAdapter sp_AS_11020_Supplier_ListTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11023_Tracker_ListTableAdapter sp_AS_11023_Tracker_ListTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Vehicle_Towbar_TypeTableAdapter sp_AS_11000_PL_Vehicle_Towbar_TypeTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Plant_CategoryTableAdapter sp_AS_11000_PL_Plant_CategoryTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_FuelTableAdapter sp_AS_11000_PL_FuelTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Gadget_TypeTableAdapter sp_AS_11000_PL_Gadget_TypeTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_CasingTableAdapter sp_AS_11000_PL_CasingTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Hardware_TypeTableAdapter sp_AS_11000_PL_Hardware_TypeTableAdapter;
        private System.Windows.Forms.BindingSource spAS11000PLSoftwareProductBindingSource;
        private System.Windows.Forms.BindingSource spAS11000PLSoftwareVersionBindingSource;
        private System.Windows.Forms.BindingSource spAS11000PLSoftwareAcquisationMethodBindingSource;
        private System.Windows.Forms.BindingSource spAS11000PLLicenceTypeBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Software_ProductTableAdapter sp_AS_11000_PL_Software_ProductTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Software_VersionTableAdapter sp_AS_11000_PL_Software_VersionTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Licence_TypeTableAdapter sp_AS_11000_PL_Licence_TypeTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Software_Acquisation_MethodTableAdapter sp_AS_11000_PL_Software_Acquisation_MethodTableAdapter;
        private System.Windows.Forms.BindingSource spAS11000DuplicateMakeModelSearchBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_Duplicate_Make_Model_SearchTableAdapter sp_AS_11000_Duplicate_Make_Model_SearchTableAdapter;
        private System.Windows.Forms.BindingSource spAS11000DuplicateSearchBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_Duplicate_SearchTableAdapter sp_AS_11000_Duplicate_SearchTableAdapter;
        private System.Windows.Forms.BindingSource spAS11066DepreciationSettingsItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11066_Depreciation_Settings_ItemTableAdapter sp_AS_11066_Depreciation_Settings_ItemTableAdapter;
        private System.Windows.Forms.BindingSource spAS11000PLUnitDescriptorBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Unit_DescriptorTableAdapter sp_AS_11000_PL_Unit_DescriptorTableAdapter;
        private System.Windows.Forms.BindingSource sp_AS_11029_Office_ItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11029_Office_ItemTableAdapter sp_AS_11029_Office_ItemTableAdapter;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private System.Windows.Forms.BindingSource spAS11000PLOfficeCategoryBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Office_CategoryTableAdapter sp_AS_11000_PL_Office_CategoryTableAdapter;
        private DevExpress.XtraEditors.LookUpEdit lueVehicleCategoryID;
        private System.Windows.Forms.BindingSource spAS11000PLVehicleCategoryBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Vehicle_CategoryTableAdapter sp_AS_11000_PL_Vehicle_CategoryTableAdapter;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private System.Windows.Forms.BindingSource spAS11026GadgetItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11026_Gadget_ItemTableAdapter sp_AS_11026_Gadget_ItemTableAdapter;
        private DevExpress.XtraEditors.LookUpEdit lueDepreciationSetting;
        private DevExpress.XtraEditors.TextEdit txtShortDescription;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem51;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit1;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControl1;
        private DevExpress.XtraEditors.SimpleButton btnPopUp1Ok;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource spAS11000FleetManagerDummyScreenBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_FleetManagerDummyScreenTableAdapter sp_AS_11000_FleetManagerDummyScreenTableAdapter;
        private DevExpress.XtraTab.XtraTabControl equipmentChildTabControl;
        private DevExpress.XtraTab.XtraTabPage roadTaxTabPage;
        private DevExpress.XtraGrid.GridControl roadTaxGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView roadTaxGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colRoadTaxID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference12;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID14;
        private DevExpress.XtraGrid.Columns.GridColumn colTaxExpiryDate;
        private DevExpress.XtraGrid.Columns.GridColumn colAmount;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit moneyTextEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn colTaxPeriodID;
        private DevExpress.XtraGrid.Columns.GridColumn colTaxPeriod;
        private DevExpress.XtraGrid.Columns.GridColumn colMode13;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID13;
        private DevExpress.XtraTab.XtraTabPage trackerTabPage;
        private DevExpress.XtraGrid.GridControl trackerGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView trackerGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colTrackerInformationID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference6;
        private DevExpress.XtraGrid.Columns.GridColumn colRegistration;
        private DevExpress.XtraGrid.Columns.GridColumn colTrackerVID;
        private DevExpress.XtraGrid.Columns.GridColumn colTrackerReference;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID8;
        private DevExpress.XtraGrid.Columns.GridColumn colMake1;
        private DevExpress.XtraGrid.Columns.GridColumn colModel1;
        private DevExpress.XtraGrid.Columns.GridColumn colOdometerMetres;
        private DevExpress.XtraGrid.Columns.GridColumn colOdometerMiles;
        private DevExpress.XtraGrid.Columns.GridColumn colReason;
        private DevExpress.XtraGrid.Columns.GridColumn colMPG;
        private DevExpress.XtraGrid.Columns.GridColumn colLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colLatestStartJourney;
        private DevExpress.XtraGrid.Columns.GridColumn colLatestEndJourney;
        private DevExpress.XtraGrid.Columns.GridColumn colLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeedKPH;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeedMPH;
        private DevExpress.XtraGrid.Columns.GridColumn colAverageSpeed;
        private DevExpress.XtraGrid.Columns.GridColumn colPlaceName;
        private DevExpress.XtraGrid.Columns.GridColumn colRoadName;
        private DevExpress.XtraGrid.Columns.GridColumn colPostSect;
        private DevExpress.XtraGrid.Columns.GridColumn colGeofenceName;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrict;
        private DevExpress.XtraGrid.Columns.GridColumn colLastUpdate;
        private DevExpress.XtraGrid.Columns.GridColumn colDriverName;
        private DevExpress.XtraGrid.Columns.GridColumn colDriverMobile;
        private DevExpress.XtraGrid.Columns.GridColumn colDriverEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colMode4;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID4;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit10;
        private DevExpress.XtraTab.XtraTabPage keeperTabPage;
        private DevExpress.XtraGrid.GridControl keeperGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView keeperGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperAllocationID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference9;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperType;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperID;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeper;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID11;
        private DevExpress.XtraGrid.Columns.GridColumn colAllocationStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colAllocationStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colAllocationDate;
        private DevExpress.XtraGrid.Columns.GridColumn colAllocationEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colNotes1;
        private DevExpress.XtraGrid.Columns.GridColumn colMode7;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID7;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit lueKeeperNotes;
        private DevExpress.XtraTab.XtraTabPage transactionTabPage;
        private DevExpress.XtraGrid.GridControl transactionGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView transactionsGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colTransactionID1;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference10;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID12;
        private DevExpress.XtraGrid.Columns.GridColumn colTransactionTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colTransactionType;
        private DevExpress.XtraGrid.Columns.GridColumn colTransactionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTransactionOrderNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPurchaseInvoice;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colCompletedOnExchequer;
        private DevExpress.XtraGrid.Columns.GridColumn colNetTransactionPrice;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit moneyTextEdit;
        private DevExpress.XtraGrid.Columns.GridColumn colVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colMode8;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID8;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit ceOnExchequer;
        private DevExpress.XtraTab.XtraTabPage billingTabPage;
        private DevExpress.XtraGrid.GridControl billingGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView billingGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colDepreciationID1;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID1;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference;
        private DevExpress.XtraGrid.Columns.GridColumn colNarrative1;
        private DevExpress.XtraGrid.Columns.GridColumn colBalanceSheetCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colProfitLossCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingCentreCodeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartmentCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCentreCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colDepreciationAmount1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit moneyTextEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colPreviousValue1;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentValue1;
        private DevExpress.XtraGrid.Columns.GridColumn colPeriodNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colPeriodStartDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colPeriodEndDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colAllowEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colMode9;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID9;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraTab.XtraTabPage depreciationTabPage;
        private DevExpress.XtraGrid.GridControl depreciationGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView depreciationGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colDepreciationID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference1;
        private DevExpress.XtraGrid.Columns.GridColumn colNarrative;
        private DevExpress.XtraGrid.Columns.GridColumn colBalanceSheetCode;
        private DevExpress.XtraGrid.Columns.GridColumn colProfitLossCode;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingCentreCodeID;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyCode;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartmentCode;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCentreCode;
        private DevExpress.XtraGrid.Columns.GridColumn colDepreciationAmount;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit moneyTextEditDep;
        private DevExpress.XtraGrid.Columns.GridColumn colPreviousValue;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentValue;
        private DevExpress.XtraGrid.Columns.GridColumn colPeriodNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPeriodStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colPeriodEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colAllowEdit;
        private DevExpress.XtraGrid.Columns.GridColumn colMode;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private DevExpress.XtraTab.XtraTabPage coverTabPage;
        private DevExpress.XtraGrid.GridControl coverGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView coverGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colCoverID;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit9;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference16;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID3;
        private DevExpress.XtraGrid.Columns.GridColumn colCoverType;
        private DevExpress.XtraGrid.Columns.GridColumn colCoverTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colSupplierID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSupplierReference1;
        private DevExpress.XtraGrid.Columns.GridColumn colPolicy;
        private DevExpress.XtraGrid.Columns.GridColumn colAnnualCoverCost;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit moneyTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colRenewalDate;
        private DevExpress.XtraGrid.Columns.GridColumn colNotes3;
        private DevExpress.XtraGrid.Columns.GridColumn colExcess;
        private DevExpress.XtraGrid.Columns.GridColumn colMode11;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID11;
        private DevExpress.XtraTab.XtraTabPage serviceDatalTabPage;
        private DevExpress.XtraTab.XtraTabPage workDetailTabPage;
        private DevExpress.XtraGrid.GridControl workDetailGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView workDetailGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkDetailID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID2;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference11;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkType;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateRaised;
        private DevExpress.XtraGrid.Columns.GridColumn colSupplierID;
        private DevExpress.XtraGrid.Columns.GridColumn colSupplierReference;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn colTransactionID;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentMileage;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkCompletionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceIntervalID;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceType;
        private DevExpress.XtraGrid.Columns.GridColumn colNotes2;
        private DevExpress.XtraGrid.Columns.GridColumn colMode10;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID10;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit13;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit14;
        private DevExpress.XtraTab.XtraTabPage incidentTabPage;
        private DevExpress.XtraGrid.GridControl incidentGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView incidentGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID7;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference5;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentType;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentReference;
        private DevExpress.XtraGrid.Columns.GridColumn colDateHappened;
        private DevExpress.XtraGrid.Columns.GridColumn colLocation;
        private DevExpress.XtraGrid.Columns.GridColumn colRepairDueID;
        private DevExpress.XtraGrid.Columns.GridColumn colRepairDue;
        private DevExpress.XtraGrid.Columns.GridColumn colSeverityID;
        private DevExpress.XtraGrid.Columns.GridColumn colSeverity;
        private DevExpress.XtraGrid.Columns.GridColumn colWitness;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperAtFaultID;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperAtFault;
        private DevExpress.XtraGrid.Columns.GridColumn colLegalActionID;
        private DevExpress.XtraGrid.Columns.GridColumn colLegalAction;
        private DevExpress.XtraGrid.Columns.GridColumn colFollowUpDate;
        private DevExpress.XtraGrid.Columns.GridColumn colCost;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit moneyTextEdit5;
        private DevExpress.XtraGrid.Columns.GridColumn colNotes4;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit incidentNotesMemoEdit;
        private DevExpress.XtraGrid.Columns.GridColumn colMode3;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID3;
        private DevExpress.XtraTab.XtraTabPage purposeTabPage;
        private DevExpress.XtraGrid.GridControl purposeGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView purposeGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentPurposeID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID15;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference13;
        private DevExpress.XtraGrid.Columns.GridColumn colPurpose;
        private DevExpress.XtraGrid.Columns.GridColumn colPurposeID;
        private DevExpress.XtraGrid.Columns.GridColumn colMode14;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID14;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit16;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit17;
        private DevExpress.XtraTab.XtraTabPage fuelCardTabPage;
        private DevExpress.XtraGrid.GridControl fuelCardGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView fuelCardGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelCardID;
        private DevExpress.XtraGrid.Columns.GridColumn colCardNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colRegistrationMark;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference14;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID16;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeper1;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperAllocationID1;
        private DevExpress.XtraGrid.Columns.GridColumn colExpiryDate;
        private DevExpress.XtraGrid.Columns.GridColumn colSupplierReference2;
        private DevExpress.XtraGrid.Columns.GridColumn colSupplierID2;
        private DevExpress.XtraGrid.Columns.GridColumn colCardStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colCardStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colNotes5;
        private DevExpress.XtraGrid.Columns.GridColumn colMode15;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID15;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit18;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit19;
        private DevExpress.XtraTab.XtraTabPage speedingTabPage;
        private DevExpress.XtraGrid.GridControl speedingGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView speedingGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeedingID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference17;
        private DevExpress.XtraGrid.Columns.GridColumn colVID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID17;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeed;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeedMPH1;
        private DevExpress.XtraGrid.Columns.GridColumn colTravelTime;
        private DevExpress.XtraGrid.Columns.GridColumn colRoadTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colRoadType;
        private DevExpress.XtraGrid.Columns.GridColumn colPlaceName1;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrict1;
        private DevExpress.XtraGrid.Columns.GridColumn colReported;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit ceReported;
        private DevExpress.XtraGrid.Columns.GridColumn colMode17;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID17;
        private System.Windows.Forms.BindingSource spAS11044KeeperAllocationItemBindingSource;
        private System.Windows.Forms.BindingSource spAS11041TransactionItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11044_Keeper_Allocation_ItemTableAdapter sp_AS_11044_Keeper_Allocation_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11041_Transaction_ItemTableAdapter sp_AS_11041_Transaction_ItemTableAdapter;
        private System.Windows.Forms.BindingSource spAS11050DepreciationItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11050_Depreciation_ItemTableAdapter sp_AS_11050_Depreciation_ItemTableAdapter;
        private System.Windows.Forms.BindingSource spAS11075CoverItemBindingSource;
        private System.Windows.Forms.BindingSource spAS11078WorkDetailItemBindingSource;
        private System.Windows.Forms.BindingSource spAS11081IncidentItemBindingSource;
        private System.Windows.Forms.BindingSource spAS11084PurposeItemBindingSource;
        private System.Windows.Forms.BindingSource spAS11089FuelCardItemBindingSource;
        private System.Windows.Forms.BindingSource spAS11105SpeedingItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11075_Cover_ItemTableAdapter sp_AS_11075_Cover_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11078_Work_Detail_ItemTableAdapter sp_AS_11078_Work_Detail_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11081_Incident_ItemTableAdapter sp_AS_11081_Incident_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11084_Purpose_ItemTableAdapter sp_AS_11084_Purpose_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11089_Fuel_Card_ItemTableAdapter sp_AS_11089_Fuel_Card_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11105_Speeding_ItemTableAdapter sp_AS_11105_Speeding_ItemTableAdapter;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.TabbedControlGroup linkedChildDataGroup;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private System.Windows.Forms.BindingSource spAS11002EquipmentItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11002_Equipment_ItemTableAdapter sp_AS_11002_Equipment_ItemTableAdapter;
        private DevExpress.XtraLayout.LayoutControlGroup equipmentLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlGroup equipRefLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlGroup remarksLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup depreciationLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem40;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem56;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem61;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem60;
        private DevExpress.XtraLayout.LayoutControlGroup GCLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem59;
        private DevExpress.XtraLayout.LayoutControlGroup financeLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraLayout.LayoutControlGroup manufacturerLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlItem lblManufacturerID;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DataSet_AT dataSet_AT;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11044_Keeper_Allocation_ListTableAdapter sp_AS_11044_Keeper_Allocation_ListTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Incident_StatusTableAdapter sp_AS_11000_PL_Incident_StatusTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Incident_TypeTableAdapter sp_AS_11000_PL_Incident_TypeTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Incident_Repair_DueTableAdapter sp_AS_11000_PL_Incident_Repair_DueTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_SeverityTableAdapter sp_AS_11000_PL_SeverityTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Legal_ActionTableAdapter sp_AS_11000_PL_Legal_ActionTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Keeper_At_FaultTableAdapter sp_AS_11000_PL_Keeper_At_FaultTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private System.Windows.Forms.BindingSource spAS11096RoadTaxItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11096_Road_Tax_ItemTableAdapter sp_AS_11096_Road_Tax_ItemTableAdapter;
        private DevExpress.XtraEditors.TextEdit txtIMEI;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem63;
        private DevExpress.XtraEditors.LookUpEdit lueOfficeCategoryID;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem62;
        private DevExpress.XtraEditors.TextEdit txtServiceTag;
        private DevExpress.XtraEditors.TextEdit txtSerialNumber;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem65;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem66;
        private System.Windows.Forms.BindingSource spAS11141HardwareSpecificationsItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11141_Hardware_Specifications_ItemTableAdapter sp_AS_11141_Hardware_Specifications_ItemTableAdapter;
        private DevExpress.XtraEditors.DateEdit dePUWERDate;
        private DevExpress.XtraEditors.DateEdit deLOLERDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem64;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem67;
        private DevExpress.XtraEditors.LookUpEdit lueExchequerCategoryID;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem68;
        private System.Windows.Forms.BindingSource spAS11000PLExchequerCategoryBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Exchequer_CategoryTableAdapter sp_AS_11000_PL_Exchequer_CategoryTableAdapter;
        private DevExpress.XtraLayout.LayoutControlGroup vehicleLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlGroup vehRegistrationLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem54;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem53;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraLayout.LayoutControlGroup vehSpecificationLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem49;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem39;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.LayoutControlGroup vehMaintenanceLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem42;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem44;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem46;
        private DevExpress.XtraLayout.LayoutControlGroup vehCodesLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem48;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem50;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem52;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlGroup vehPerfomanceLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem47;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem45;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem43;
        private DevExpress.XtraGrid.GridControl serviceDataGridControl;
        private System.Windows.Forms.BindingSource spAS11054ServiceDataItemBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView serviceDataGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceDataID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID13;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference15;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceType1;
        private DevExpress.XtraGrid.Columns.GridColumn colDistanceUnitsID;
        private DevExpress.XtraGrid.Columns.GridColumn colDistanceUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colDistanceFrequency;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeUnitsID;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeFrequency;
        private DevExpress.XtraGrid.Columns.GridColumn colWarrantyPeriod;
        private DevExpress.XtraGrid.Columns.GridColumn colWarrantyEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colWarrantyEndDistance;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceDataNotes;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceDueDistance;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceDueDate;
        private DevExpress.XtraGrid.Columns.GridColumn colAlertWithinXDistance;
        private DevExpress.XtraGrid.Columns.GridColumn colAlertWithinXTime;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkType1;
        private DevExpress.XtraGrid.Columns.GridColumn colIntervalScheduleNotes;
        private DevExpress.XtraGrid.Columns.GridColumn colIsCurrentInterval;
        private DevExpress.XtraGrid.Columns.GridColumn colMode12;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID12;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit11;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit12;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11054_Service_Data_ItemTableAdapter sp_AS_11054_Service_Data_ItemTableAdapter;
        private DevExpress.XtraEditors.TextEdit txtPlantRegistrationPlate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem57;
        private DevExpress.XtraEditors.DateEdit soldDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItem2;
        private DevExpress.XtraBars.BarStaticItem barStaticItem3;
        private DevExpress.XtraBars.BarStaticItem barStaticItem4;
        private DevExpress.XtraBars.BarStaticItem barStaticItem5;
        private DevExpress.XtraBars.Bar bar4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.Bar bar5;
        private DevExpress.XtraBars.BarEditItem barEditItem2;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit2;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControl2;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraBars.BarStaticItem barStaticItem6;
    }
}

﻿namespace WoodPlan5
{
    partial class frm_AS_Depreciation_Setting_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AS_Depreciation_Setting_Manager));
            this.imageCollection1 = new DevExpress.Utils.ImageCollection();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.depSettingGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11066DepreciationSettingsItemBindingSource = new System.Windows.Forms.BindingSource();
            this.depSettingGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDepreciationSettingID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOccurrenceFrequency = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOccursUnitDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOccursUnitDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaximumOccurrence = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDayOfMonthApplyDepreciation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSetAsDefault = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ceSetAsDefault = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colLastChanged = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSettingName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sp_AS_11000_PL_Supplier_TypeTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Supplier_TypeTableAdapter();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiCancel = new DevExpress.XtraBars.BarButtonItem();
            this.dxErrorProviderDepSetting = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource();
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.sp_AS_11066_Depreciation_Settings_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11066_Depreciation_Settings_ItemTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.depSettingGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11066DepreciationSettingsItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.depSettingGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceSetAsDefault)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProviderDepSetting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.Manager = null;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1372, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 476);
            this.barDockControlBottom.Size = new System.Drawing.Size(1372, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 476);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1372, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 476);
            // 
            // bbiSave
            // 
            this.bbiSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSave.Glyph")));
            this.bbiSave.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiSave.LargeGlyph")));
            // 
            // pmEditContextMenu
            // 
            this.pmEditContextMenu.Manager = null;
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiCancel});
            this.barManager1.MaxItemId = 31;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertGalleryImage("add_16x16.png", "images/actions/add_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/add_16x16.png"), 0);
            this.imageCollection1.Images.SetKeyName(0, "add_16x16.png");
            this.imageCollection1.InsertGalleryImage("edit_16x16.png", "images/edit/edit_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/edit_16x16.png"), 1);
            this.imageCollection1.Images.SetKeyName(1, "edit_16x16.png");
            this.imageCollection1.InsertGalleryImage("delete_16x16.png", "images/edit/delete_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/delete_16x16.png"), 2);
            this.imageCollection1.Images.SetKeyName(2, "delete_16x16.png");
            this.imageCollection1.InsertGalleryImage("preview_16x16.png", "images/print/preview_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/print/preview_16x16.png"), 3);
            this.imageCollection1.Images.SetKeyName(3, "preview_16x16.png");
            this.imageCollection1.InsertGalleryImage("apply_16x16.png", "images/actions/apply_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/apply_16x16.png"), 4);
            this.imageCollection1.Images.SetKeyName(4, "apply_16x16.png");
            this.imageCollection1.InsertGalleryImage("save_16x16.png", "images/save/save_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/save/save_16x16.png"), 5);
            this.imageCollection1.Images.SetKeyName(5, "save_16x16.png");
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1675, 521, 250, 350);
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1372, 476);
            this.dataLayoutControl1.TabIndex = 4;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1372, 476);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "DataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // depSettingGridControl
            // 
            this.depSettingGridControl.DataSource = this.spAS11066DepreciationSettingsItemBindingSource;
            this.depSettingGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.depSettingGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.depSettingGridControl.EmbeddedNavigator.Buttons.Append.ImageIndex = 0;
            this.depSettingGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.depSettingGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.depSettingGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.depSettingGridControl.EmbeddedNavigator.Buttons.Edit.ImageIndex = 1;
            this.depSettingGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.depSettingGridControl.EmbeddedNavigator.Buttons.EndEdit.ImageIndex = 4;
            this.depSettingGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.depSettingGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.depSettingGridControl.EmbeddedNavigator.Buttons.Remove.ImageIndex = 2;
            this.depSettingGridControl.EmbeddedNavigator.Buttons.Remove.Tag = "";
            this.depSettingGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.depSettingGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "", "delete")});
            this.depSettingGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.depSettingGridControl_EmbeddedNavigator_ButtonClick);
            this.depSettingGridControl.Location = new System.Drawing.Point(0, 0);
            this.depSettingGridControl.MainView = this.depSettingGridView;
            this.depSettingGridControl.Name = "depSettingGridControl";
            this.depSettingGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.ceSetAsDefault});
            this.depSettingGridControl.Size = new System.Drawing.Size(1372, 476);
            this.depSettingGridControl.TabIndex = 5;
            this.depSettingGridControl.UseEmbeddedNavigator = true;
            this.depSettingGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.depSettingGridView});
            // 
            // spAS11066DepreciationSettingsItemBindingSource
            // 
            this.spAS11066DepreciationSettingsItemBindingSource.DataMember = "sp_AS_11066_Depreciation_Settings_Item";
            this.spAS11066DepreciationSettingsItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // depSettingGridView
            // 
            this.depSettingGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDepreciationSettingID,
            this.colOccurrenceFrequency,
            this.colOccursUnitDescriptorID,
            this.colOccursUnitDescriptor,
            this.colMaximumOccurrence,
            this.colDayOfMonthApplyDepreciation,
            this.colSetAsDefault,
            this.colLastChanged,
            this.colMode,
            this.colRecordID,
            this.colSettingName});
            this.depSettingGridView.GridControl = this.depSettingGridControl;
            this.depSettingGridView.Name = "depSettingGridView";
            this.depSettingGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.depSettingGridView.OptionsFind.AlwaysVisible = true;
            this.depSettingGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.depSettingGridView.OptionsLayout.StoreAppearance = true;
            this.depSettingGridView.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.depSettingGridView.OptionsView.AllowHtmlDrawHeaders = true;
            this.depSettingGridView.OptionsView.ColumnAutoWidth = false;
            this.depSettingGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.depSettingGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.depSettingGridView_PopupMenuShowing);
            this.depSettingGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.depSettingGridView_SelectionChanged);
            this.depSettingGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.depSettingGridView_CustomDrawEmptyForeground);
            this.depSettingGridView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.depSettingGridView_FocusedRowChanged);
            this.depSettingGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.depSettingGridView_CustomFilterDialog);
            this.depSettingGridView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.depSettingGridView_MouseDown);
            this.depSettingGridView.DoubleClick += new System.EventHandler(this.depSettingGridView_DoubleClick);
            // 
            // colDepreciationSettingID
            // 
            this.colDepreciationSettingID.FieldName = "DepreciationSettingID";
            this.colDepreciationSettingID.Name = "colDepreciationSettingID";
            this.colDepreciationSettingID.OptionsColumn.AllowEdit = false;
            this.colDepreciationSettingID.OptionsColumn.AllowFocus = false;
            this.colDepreciationSettingID.OptionsColumn.ReadOnly = true;
            this.colDepreciationSettingID.OptionsColumn.ShowInCustomizationForm = false;
            this.colDepreciationSettingID.OptionsColumn.ShowInExpressionEditor = false;
            this.colDepreciationSettingID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDepreciationSettingID.Width = 132;
            // 
            // colOccurrenceFrequency
            // 
            this.colOccurrenceFrequency.FieldName = "OccurrenceFrequency";
            this.colOccurrenceFrequency.Name = "colOccurrenceFrequency";
            this.colOccurrenceFrequency.OptionsColumn.AllowEdit = false;
            this.colOccurrenceFrequency.OptionsColumn.AllowFocus = false;
            this.colOccurrenceFrequency.OptionsColumn.ReadOnly = true;
            this.colOccurrenceFrequency.OptionsColumn.ShowInCustomizationForm = false;
            this.colOccurrenceFrequency.OptionsColumn.ShowInExpressionEditor = false;
            this.colOccurrenceFrequency.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colOccurrenceFrequency.Width = 130;
            // 
            // colOccursUnitDescriptorID
            // 
            this.colOccursUnitDescriptorID.FieldName = "OccursUnitDescriptorID";
            this.colOccursUnitDescriptorID.Name = "colOccursUnitDescriptorID";
            this.colOccursUnitDescriptorID.OptionsColumn.AllowEdit = false;
            this.colOccursUnitDescriptorID.OptionsColumn.AllowFocus = false;
            this.colOccursUnitDescriptorID.OptionsColumn.ReadOnly = true;
            this.colOccursUnitDescriptorID.OptionsColumn.ShowInCustomizationForm = false;
            this.colOccursUnitDescriptorID.OptionsColumn.ShowInExpressionEditor = false;
            this.colOccursUnitDescriptorID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colOccursUnitDescriptorID.Width = 142;
            // 
            // colOccursUnitDescriptor
            // 
            this.colOccursUnitDescriptor.FieldName = "OccursUnitDescriptor";
            this.colOccursUnitDescriptor.Name = "colOccursUnitDescriptor";
            this.colOccursUnitDescriptor.OptionsColumn.AllowEdit = false;
            this.colOccursUnitDescriptor.OptionsColumn.AllowFocus = false;
            this.colOccursUnitDescriptor.OptionsColumn.ReadOnly = true;
            this.colOccursUnitDescriptor.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colOccursUnitDescriptor.Visible = true;
            this.colOccursUnitDescriptor.VisibleIndex = 3;
            this.colOccursUnitDescriptor.Width = 193;
            // 
            // colMaximumOccurrence
            // 
            this.colMaximumOccurrence.FieldName = "MaximumOccurrence";
            this.colMaximumOccurrence.Name = "colMaximumOccurrence";
            this.colMaximumOccurrence.OptionsColumn.AllowEdit = false;
            this.colMaximumOccurrence.OptionsColumn.AllowFocus = false;
            this.colMaximumOccurrence.OptionsColumn.ReadOnly = true;
            this.colMaximumOccurrence.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colMaximumOccurrence.Visible = true;
            this.colMaximumOccurrence.VisibleIndex = 2;
            this.colMaximumOccurrence.Width = 185;
            // 
            // colDayOfMonthApplyDepreciation
            // 
            this.colDayOfMonthApplyDepreciation.FieldName = "DayOfMonthApplyDepreciation";
            this.colDayOfMonthApplyDepreciation.Name = "colDayOfMonthApplyDepreciation";
            this.colDayOfMonthApplyDepreciation.OptionsColumn.AllowEdit = false;
            this.colDayOfMonthApplyDepreciation.OptionsColumn.AllowFocus = false;
            this.colDayOfMonthApplyDepreciation.OptionsColumn.ReadOnly = true;
            this.colDayOfMonthApplyDepreciation.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDayOfMonthApplyDepreciation.Visible = true;
            this.colDayOfMonthApplyDepreciation.VisibleIndex = 1;
            this.colDayOfMonthApplyDepreciation.Width = 219;
            // 
            // colSetAsDefault
            // 
            this.colSetAsDefault.ColumnEdit = this.ceSetAsDefault;
            this.colSetAsDefault.FieldName = "SetAsDefault";
            this.colSetAsDefault.Name = "colSetAsDefault";
            this.colSetAsDefault.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSetAsDefault.Visible = true;
            this.colSetAsDefault.VisibleIndex = 4;
            this.colSetAsDefault.Width = 103;
            // 
            // ceSetAsDefault
            // 
            this.ceSetAsDefault.AutoHeight = false;
            this.ceSetAsDefault.Caption = "Set As Default";
            this.ceSetAsDefault.Name = "ceSetAsDefault";
            this.ceSetAsDefault.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.ceSetAsDefault_EditValueChanging);
            // 
            // colLastChanged
            // 
            this.colLastChanged.FieldName = "LastChanged";
            this.colLastChanged.Name = "colLastChanged";
            this.colLastChanged.OptionsColumn.AllowEdit = false;
            this.colLastChanged.OptionsColumn.AllowFocus = false;
            this.colLastChanged.OptionsColumn.ReadOnly = true;
            this.colLastChanged.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLastChanged.Visible = true;
            this.colLastChanged.VisibleIndex = 5;
            this.colLastChanged.Width = 230;
            // 
            // colMode
            // 
            this.colMode.FieldName = "Mode";
            this.colMode.Name = "colMode";
            this.colMode.OptionsColumn.AllowEdit = false;
            this.colMode.OptionsColumn.AllowFocus = false;
            this.colMode.OptionsColumn.ReadOnly = true;
            this.colMode.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID
            // 
            this.colRecordID.FieldName = "RecordID";
            this.colRecordID.Name = "colRecordID";
            this.colRecordID.OptionsColumn.AllowEdit = false;
            this.colRecordID.OptionsColumn.AllowFocus = false;
            this.colRecordID.OptionsColumn.ReadOnly = true;
            this.colRecordID.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colSettingName
            // 
            this.colSettingName.FieldName = "SettingName";
            this.colSettingName.Name = "colSettingName";
            this.colSettingName.OptionsColumn.AllowEdit = false;
            this.colSettingName.OptionsColumn.AllowFocus = false;
            this.colSettingName.OptionsColumn.ReadOnly = true;
            this.colSettingName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSettingName.Visible = true;
            this.colSettingName.VisibleIndex = 0;
            this.colSettingName.Width = 382;
            // 
            // sp_AS_11000_PL_Supplier_TypeTableAdapter
            // 
            this.sp_AS_11000_PL_Supplier_TypeTableAdapter.ClearBeforeFill = true;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.OptionsBar.MultiLine = true;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 2";
            // 
            // bbiCancel
            // 
            this.bbiCancel.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.bbiCancel.Caption = "Cancel and Close";
            this.bbiCancel.DropDownEnabled = false;
            this.bbiCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiCancel.Glyph")));
            this.bbiCancel.Id = 30;
            this.bbiCancel.Name = "bbiCancel";
            this.bbiCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCancel_ItemClick);
            // 
            // dxErrorProviderDepSetting
            // 
            this.dxErrorProviderDepSetting.ContainerControl = this;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.dataSet_AT;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11066_Depreciation_Settings_ItemTableAdapter
            // 
            this.sp_AS_11066_Depreciation_Settings_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // frm_AS_Depreciation_Setting_Manager
            // 
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1372, 476);
            this.Controls.Add(this.depSettingGridControl);
            this.Controls.Add(this.dataLayoutControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AS_Depreciation_Setting_Manager";
            this.Text = "Depreciation Setting Manager";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.frm_AS_Depreciation_Setting_Manager_Activated);
            this.Load += new System.EventHandler(this.frm_AS_Depreciation_Setting_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            this.Controls.SetChildIndex(this.depSettingGridControl, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.depSettingGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11066DepreciationSettingsItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.depSettingGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceSetAsDefault)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProviderDepSetting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraGrid.GridControl depSettingGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView depSettingGridView;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Supplier_TypeTableAdapter sp_AS_11000_PL_Supplier_TypeTableAdapter;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiCancel;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProviderDepSetting;
        private DataSet_AT dataSet_AT;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit ceSetAsDefault;
        private DevExpress.XtraGrid.Columns.GridColumn colDepreciationSettingID;
        private DevExpress.XtraGrid.Columns.GridColumn colOccurrenceFrequency;
        private DevExpress.XtraGrid.Columns.GridColumn colOccursUnitDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colOccursUnitDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colMaximumOccurrence;
        private DevExpress.XtraGrid.Columns.GridColumn colDayOfMonthApplyDepreciation;
        private DevExpress.XtraGrid.Columns.GridColumn colSetAsDefault;
        private DevExpress.XtraGrid.Columns.GridColumn colLastChanged;
        private DevExpress.XtraGrid.Columns.GridColumn colMode;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colSettingName;
        private System.Windows.Forms.BindingSource spAS11066DepreciationSettingsItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11066_Depreciation_Settings_ItemTableAdapter sp_AS_11066_Depreciation_Settings_ItemTableAdapter;
    }
}

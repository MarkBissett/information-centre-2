﻿namespace WoodPlan5
{
    partial class frm_AS_Email_Schedule_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AS_Email_Schedule_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.tableAdapterManager = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager();
            this.barStatus = new DevExpress.XtraBars.Bar();
            this.bsiFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.formDataLayoutControl = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.dataNavigator = new DevExpress.XtraEditors.DataNavigator();
            this.deEmailDate = new DevExpress.XtraEditors.DateEdit();
            this.spAS11157EmailScheduleItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lueTaskType = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLExtra1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lueTaskStatusID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLExtra2BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ceIsCurrentTask = new DevExpress.XtraEditors.CheckEdit();
            this.formLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForEmailDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTaskType = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIsCurrentTask = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTaskStatusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp_AS_11157_Email_Schedule_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11157_Email_Schedule_ItemTableAdapter();
            this.sp_AS_11000_PL_Extra1TableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Extra1TableAdapter();
            this.sp_AS_11000_PL_Extra2TableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Extra2TableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.formDataLayoutControl)).BeginInit();
            this.formDataLayoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deEmailDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEmailDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11157EmailScheduleItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTaskType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLExtra1BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTaskStatusID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLExtra2BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceIsCurrentTask.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.formLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmailDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTaskType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsCurrentTask)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTaskStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(797, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 214);
            this.barDockControlBottom.Size = new System.Drawing.Size(797, 30);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 188);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(797, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 188);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barStatus,
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.bsiFormMode,
            this.barStaticItem1,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemChangesPending,
            this.barStaticItemInformation});
            this.barManager1.MaxItemId = 35;
            this.barManager1.StatusBar = this.barStatus;
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "dataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.sp_AS_11002_Equipment_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11005_Vehicle_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11008_Plant_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11011_Equipment_Category_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11014_Make_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11017_Model_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11020_Supplier_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Tracker_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Verilocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11026_Gadget_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11029_Office_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11032_Hardware_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11035_Software_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11041_Transaction_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11047_Equipment_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11050_Depreciation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Interval_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11057_Company_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11060_Department_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11063_Cost_Centre_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11066_Depreciation_Settings_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11075_Cover_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11078_Work_Detail_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11081_Incident_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11084_Purpose_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11087_Equipment_Category_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11089_Fuel_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11093_Initial_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11096_Road_Tax_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11099_Improvement_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11102_Rental_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11105_Speeding_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11108_Rental_Details_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11112_Keeper_Email_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11117_Sim_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11120_Additional_FollowUps_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11123_Sim_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_AlertTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11129_Software_Assign_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11133_Picklist_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_AS_11141_Hardware_Specifications_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11157_Email_Schedule_ItemTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // barStatus
            // 
            this.barStatus.BarName = "Status Bar";
            this.barStatus.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.barStatus.DockCol = 0;
            this.barStatus.DockRow = 0;
            this.barStatus.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.barStatus.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiFormMode, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barStaticItemInformation, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barStatus.OptionsBar.AllowQuickCustomization = false;
            this.barStatus.OptionsBar.DrawDragBorder = false;
            this.barStatus.OptionsBar.UseWholeRow = true;
            this.barStatus.Text = "Status bar";
            // 
            // bsiFormMode
            // 
            this.bsiFormMode.Caption = "Form Mode: Editing";
            this.bsiFormMode.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.Glyph")));
            this.bsiFormMode.Id = 29;
            this.bsiFormMode.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.LargeGlyph")));
            this.bsiFormMode.Name = "bsiFormMode";
            toolTipTitleItem1.Text = "Form Mode - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bsiFormMode.SuperTip = superToolTip1;
            this.bsiFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 31;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 32;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.Glyph")));
            this.barStaticItemInformation.Id = 33;
            this.barStaticItemInformation.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.LargeGlyph")));
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 27;
            this.bbiFormSave.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.LargeGlyph")));
            this.bbiFormSave.Name = "bbiFormSave";
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Text = "Save Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormSave.SuperTip = superToolTip2;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.Glyph")));
            this.bbiFormCancel.Id = 28;
            this.bbiFormCancel.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.LargeGlyph")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Text = "Cancel Button - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiFormCancel.SuperTip = superToolTip3;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "barStaticItem1";
            this.barStaticItem1.Id = 30;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // formDataLayoutControl
            // 
            this.formDataLayoutControl.Controls.Add(this.dataNavigator);
            this.formDataLayoutControl.Controls.Add(this.deEmailDate);
            this.formDataLayoutControl.Controls.Add(this.lueTaskType);
            this.formDataLayoutControl.Controls.Add(this.lueTaskStatusID);
            this.formDataLayoutControl.Controls.Add(this.ceIsCurrentTask);
            this.formDataLayoutControl.DataSource = this.spAS11157EmailScheduleItemBindingSource;
            this.formDataLayoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.formDataLayoutControl.Location = new System.Drawing.Point(0, 26);
            this.formDataLayoutControl.Name = "formDataLayoutControl";
            this.formDataLayoutControl.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1458, 494, 250, 350);
            this.formDataLayoutControl.Root = this.formLayoutControlGroup;
            this.formDataLayoutControl.Size = new System.Drawing.Size(797, 188);
            this.formDataLayoutControl.TabIndex = 4;
            this.formDataLayoutControl.Text = "dataLayoutControl1";
            // 
            // dataNavigator
            // 
            this.dataNavigator.Buttons.Append.Visible = false;
            this.dataNavigator.Buttons.CancelEdit.Visible = false;
            this.dataNavigator.Buttons.EndEdit.Visible = false;
            this.dataNavigator.Buttons.Remove.Visible = false;
            this.dataNavigator.Location = new System.Drawing.Point(187, 12);
            this.dataNavigator.Name = "dataNavigator";
            this.dataNavigator.Size = new System.Drawing.Size(241, 19);
            this.dataNavigator.StyleController = this.formDataLayoutControl;
            this.dataNavigator.TabIndex = 121;
            this.dataNavigator.Text = "emailScheduleDataNavigator";
            this.dataNavigator.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator.TextStringFormat = "Task {0} of {1}";
            // 
            // deEmailDate
            // 
            this.deEmailDate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11157EmailScheduleItemBindingSource, "EmailDate", true));
            this.deEmailDate.EditValue = null;
            this.deEmailDate.Location = new System.Drawing.Point(96, 35);
            this.deEmailDate.MenuManager = this.barManager1;
            this.deEmailDate.Name = "deEmailDate";
            this.deEmailDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deEmailDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deEmailDate.Size = new System.Drawing.Size(300, 20);
            this.deEmailDate.StyleController = this.formDataLayoutControl;
            this.deEmailDate.TabIndex = 122;
            // 
            // spAS11157EmailScheduleItemBindingSource
            // 
            this.spAS11157EmailScheduleItemBindingSource.DataMember = "sp_AS_11157_Email_Schedule_Item";
            this.spAS11157EmailScheduleItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // lueTaskType
            // 
            this.lueTaskType.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11157EmailScheduleItemBindingSource, "TaskType", true));
            this.lueTaskType.Location = new System.Drawing.Point(96, 59);
            this.lueTaskType.MenuManager = this.barManager1;
            this.lueTaskType.Name = "lueTaskType";
            this.lueTaskType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueTaskType.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Task Type", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueTaskType.Properties.DataSource = this.spAS11000PLExtra1BindingSource;
            this.lueTaskType.Properties.DisplayMember = "Value";
            this.lueTaskType.Properties.NullText = "";
            this.lueTaskType.Properties.NullValuePrompt = "-- Please Select --";
            this.lueTaskType.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueTaskType.Properties.ValueMember = "PickListID";
            this.lueTaskType.Size = new System.Drawing.Size(300, 20);
            this.lueTaskType.StyleController = this.formDataLayoutControl;
            this.lueTaskType.TabIndex = 123;
            // 
            // spAS11000PLExtra1BindingSource
            // 
            this.spAS11000PLExtra1BindingSource.DataMember = "sp_AS_11000_PL_Extra1";
            this.spAS11000PLExtra1BindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // lueTaskStatusID
            // 
            this.lueTaskStatusID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11157EmailScheduleItemBindingSource, "TaskStatusID", true));
            this.lueTaskStatusID.Location = new System.Drawing.Point(484, 59);
            this.lueTaskStatusID.MenuManager = this.barManager1;
            this.lueTaskStatusID.Name = "lueTaskStatusID";
            this.lueTaskStatusID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueTaskStatusID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Task Status", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueTaskStatusID.Properties.DataSource = this.spAS11000PLExtra2BindingSource;
            this.lueTaskStatusID.Properties.DisplayMember = "Value";
            this.lueTaskStatusID.Properties.NullText = "";
            this.lueTaskStatusID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueTaskStatusID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueTaskStatusID.Properties.ValueMember = "PickListID";
            this.lueTaskStatusID.Size = new System.Drawing.Size(301, 20);
            this.lueTaskStatusID.StyleController = this.formDataLayoutControl;
            this.lueTaskStatusID.TabIndex = 124;
            // 
            // spAS11000PLExtra2BindingSource
            // 
            this.spAS11000PLExtra2BindingSource.DataMember = "sp_AS_11000_PL_Extra2";
            this.spAS11000PLExtra2BindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // ceIsCurrentTask
            // 
            this.ceIsCurrentTask.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11157EmailScheduleItemBindingSource, "IsCurrentTask", true));
            this.ceIsCurrentTask.Location = new System.Drawing.Point(484, 35);
            this.ceIsCurrentTask.MenuManager = this.barManager1;
            this.ceIsCurrentTask.Name = "ceIsCurrentTask";
            this.ceIsCurrentTask.Properties.Caption = "";
            this.ceIsCurrentTask.Size = new System.Drawing.Size(301, 19);
            this.ceIsCurrentTask.StyleController = this.formDataLayoutControl;
            this.ceIsCurrentTask.TabIndex = 125;
            this.ceIsCurrentTask.Validating += new System.ComponentModel.CancelEventHandler(this.ceIsCurrentTask_Validating);
            // 
            // formLayoutControlGroup
            // 
            this.formLayoutControlGroup.CustomizationFormText = "depSettingsLayoutControlGroup";
            this.formLayoutControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.formLayoutControlGroup.GroupBordersVisible = false;
            this.formLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlGroup1,
            this.emptySpaceItem1,
            this.emptySpaceItem2});
            this.formLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.formLayoutControlGroup.Name = "formLayoutControlGroup";
            this.formLayoutControlGroup.Size = new System.Drawing.Size(797, 188);
            this.formLayoutControlGroup.Text = "depSettingsLayoutControlGroup";
            this.formLayoutControlGroup.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(175, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(245, 23);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AllowDrawBackground = false;
            this.layoutControlGroup1.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForEmailDate,
            this.ItemForTaskType,
            this.ItemForIsCurrentTask,
            this.ItemForTaskStatusID});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 23);
            this.layoutControlGroup1.Name = "autoGeneratedGroup0";
            this.layoutControlGroup1.Size = new System.Drawing.Size(777, 145);
            this.layoutControlGroup1.Text = "autoGeneratedGroup0";
            // 
            // ItemForEmailDate
            // 
            this.ItemForEmailDate.Control = this.deEmailDate;
            this.ItemForEmailDate.CustomizationFormText = "Email Date :";
            this.ItemForEmailDate.Location = new System.Drawing.Point(0, 0);
            this.ItemForEmailDate.Name = "ItemForEmailDate";
            this.ItemForEmailDate.Size = new System.Drawing.Size(388, 24);
            this.ItemForEmailDate.Text = "Email Date :";
            this.ItemForEmailDate.TextSize = new System.Drawing.Size(81, 13);
            // 
            // ItemForTaskType
            // 
            this.ItemForTaskType.Control = this.lueTaskType;
            this.ItemForTaskType.CustomizationFormText = "Task Type :";
            this.ItemForTaskType.Location = new System.Drawing.Point(0, 24);
            this.ItemForTaskType.Name = "ItemForTaskType";
            this.ItemForTaskType.Size = new System.Drawing.Size(388, 121);
            this.ItemForTaskType.Text = "Task Type :";
            this.ItemForTaskType.TextSize = new System.Drawing.Size(81, 13);
            // 
            // ItemForIsCurrentTask
            // 
            this.ItemForIsCurrentTask.Control = this.ceIsCurrentTask;
            this.ItemForIsCurrentTask.CustomizationFormText = "Is Current Task :";
            this.ItemForIsCurrentTask.Location = new System.Drawing.Point(388, 0);
            this.ItemForIsCurrentTask.Name = "ItemForIsCurrentTask";
            this.ItemForIsCurrentTask.Size = new System.Drawing.Size(389, 24);
            this.ItemForIsCurrentTask.Text = "Is Current Task :";
            this.ItemForIsCurrentTask.TextSize = new System.Drawing.Size(81, 13);
            // 
            // ItemForTaskStatusID
            // 
            this.ItemForTaskStatusID.Control = this.lueTaskStatusID;
            this.ItemForTaskStatusID.CustomizationFormText = "Task Status :";
            this.ItemForTaskStatusID.Location = new System.Drawing.Point(388, 24);
            this.ItemForTaskStatusID.Name = "ItemForTaskStatusID";
            this.ItemForTaskStatusID.Size = new System.Drawing.Size(389, 121);
            this.ItemForTaskStatusID.Text = "Task Status :";
            this.ItemForTaskStatusID.TextSize = new System.Drawing.Size(81, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(175, 23);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(420, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(357, 23);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp_AS_11157_Email_Schedule_ItemTableAdapter
            // 
            this.sp_AS_11157_Email_Schedule_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Extra1TableAdapter
            // 
            this.sp_AS_11000_PL_Extra1TableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Extra2TableAdapter
            // 
            this.sp_AS_11000_PL_Extra2TableAdapter.ClearBeforeFill = true;
            // 
            // frm_AS_Email_Schedule_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(797, 244);
            this.Controls.Add(this.formDataLayoutControl);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AS_Email_Schedule_Edit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Email Schedule Edit";
            this.Activated += new System.EventHandler(this.frm_AS_Email_Schedule_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_AS_Email_Schedule_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_AS_Email_Schedule_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.formDataLayoutControl, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.formDataLayoutControl)).EndInit();
            this.formDataLayoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.deEmailDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEmailDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11157EmailScheduleItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTaskType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLExtra1BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTaskStatusID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLExtra2BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceIsCurrentTask.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.formLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmailDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTaskType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsCurrentTask)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTaskStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private DataSet_AS_DataEntryTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraBars.Bar barStatus;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.BarStaticItem bsiFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private DevExpress.XtraDataLayout.DataLayoutControl formDataLayoutControl;
        private DevExpress.XtraLayout.LayoutControlGroup formLayoutControlGroup;
        private DevExpress.XtraEditors.DataNavigator dataNavigator;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.DateEdit deEmailDate;
        private System.Windows.Forms.BindingSource spAS11157EmailScheduleItemBindingSource;
        private DevExpress.XtraEditors.LookUpEdit lueTaskType;
        private System.Windows.Forms.BindingSource spAS11000PLExtra1BindingSource;
        private DevExpress.XtraEditors.LookUpEdit lueTaskStatusID;
        private System.Windows.Forms.BindingSource spAS11000PLExtra2BindingSource;
        private DevExpress.XtraEditors.CheckEdit ceIsCurrentTask;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmailDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTaskType;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTaskStatusID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsCurrentTask;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11157_Email_Schedule_ItemTableAdapter sp_AS_11157_Email_Schedule_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Extra1TableAdapter sp_AS_11000_PL_Extra1TableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Extra2TableAdapter sp_AS_11000_PL_Extra2TableAdapter;


    }
}

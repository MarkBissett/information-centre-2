﻿namespace WoodPlan5.Forms.Assets.KAMReportWizard.ViewModels
{
    class Step1PageViewModel : IWizardPageViewModel
    {
        public bool IsComplete
        {
            //get { return !string.IsNullOrEmpty(Path) && System.IO.Directory.Exists(Path); }
            get
            {
                return blnSelected;
            }
        }
        public bool blnSelected
        {
            get;
            set;
        }
        public bool CanReturn { get { return true; } }

        public bool pageIsValid
        {
            get { return true; }
        }
    }
}
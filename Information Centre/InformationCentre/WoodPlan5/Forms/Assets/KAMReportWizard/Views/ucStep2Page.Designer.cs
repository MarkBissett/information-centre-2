﻿namespace WoodPlan5.Forms.Assets.KAMReportWizard.Views
{
    partial class ucStep2Page
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucStep2Page));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnNext = new DevExpress.XtraEditors.SimpleButton();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.lueAddStepCategory = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11011EquipmentCategoryListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.lueAddStepMake = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11014MakeListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lueEditStepModel = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11017ModelListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lueEditStepMake = new DevExpress.XtraEditors.LookUpEdit();
            this.txtEditStepModelDescription = new DevExpress.XtraEditors.TextEdit();
            this.txtEditStepMakeDescription = new DevExpress.XtraEditors.TextEdit();
            this.txtAddStepModelDescription = new DevExpress.XtraEditors.TextEdit();
            this.txtAddStepMakeDescription = new DevExpress.XtraEditors.TextEdit();
            this.btnRefresh = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lblAction = new DevExpress.XtraLayout.SimpleLabelItem();
            this.lblProceed = new DevExpress.XtraLayout.SimpleLabelItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcEditViewMake = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcEditMake = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcEditViewModel = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcEditModel = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciCategory = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcAddMake = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcAddViewMake = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcAddModel = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp_AS_11014_Make_ListTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11014_Make_ListTableAdapter();
            this.sp_AS_11011_Equipment_Category_ListTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11011_Equipment_Category_ListTableAdapter();
            this.sp_AS_11017_Model_ListTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11017_Model_ListTableAdapter();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.spAS11000DuplicateMakeModelSearchBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11000_Duplicate_Make_Model_SearchTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_Duplicate_Make_Model_SearchTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueAddStepCategory.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11011EquipmentCategoryListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueAddStepMake.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11014MakeListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueEditStepModel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11017ModelListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueEditStepMake.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEditStepModelDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEditStepMakeDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddStepModelDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddStepMakeDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAction)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblProceed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcEditViewMake)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcEditMake)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcEditViewModel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcEditModel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcAddMake)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcAddViewMake)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcAddModel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000DuplicateMakeModelSearchBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowCustomization = false;
            this.layoutControl1.Controls.Add(this.btnNext);
            this.layoutControl1.Controls.Add(this.lueAddStepCategory);
            this.layoutControl1.Controls.Add(this.lueAddStepMake);
            this.layoutControl1.Controls.Add(this.lueEditStepModel);
            this.layoutControl1.Controls.Add(this.lueEditStepMake);
            this.layoutControl1.Controls.Add(this.txtEditStepModelDescription);
            this.layoutControl1.Controls.Add(this.txtEditStepMakeDescription);
            this.layoutControl1.Controls.Add(this.txtAddStepModelDescription);
            this.layoutControl1.Controls.Add(this.txtAddStepMakeDescription);
            this.layoutControl1.Controls.Add(this.btnRefresh);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(0, 300, 450, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(860, 355);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btnNext
            // 
            this.btnNext.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnNext.Appearance.Options.UseFont = true;
            this.btnNext.ImageIndex = 7;
            this.btnNext.ImageList = this.imageCollection1;
            this.btnNext.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnNext.Location = new System.Drawing.Point(702, 286);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(100, 24);
            this.btnNext.StyleController = this.layoutControl1;
            this.btnNext.TabIndex = 15;
            this.btnNext.Text = "Next";
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertGalleryImage("info_16x16.png", "images/support/info_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/support/info_16x16.png"), 0);
            this.imageCollection1.Images.SetKeyName(0, "info_16x16.png");
            this.imageCollection1.InsertGalleryImage("add_16x16.png", "images/actions/add_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/add_16x16.png"), 1);
            this.imageCollection1.Images.SetKeyName(1, "add_16x16.png");
            this.imageCollection1.InsertGalleryImage("edit_16x16.png", "images/edit/edit_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/edit_16x16.png"), 2);
            this.imageCollection1.Images.SetKeyName(2, "edit_16x16.png");
            this.imageCollection1.InsertGalleryImage("delete_16x16.png", "images/edit/delete_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/delete_16x16.png"), 3);
            this.imageCollection1.Images.SetKeyName(3, "delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(4, "Ok_16x16.png");
            this.imageCollection1.InsertGalleryImage("refresh_16x16.png", "images/actions/refresh_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/refresh_16x16.png"), 5);
            this.imageCollection1.Images.SetKeyName(5, "refresh_16x16.png");
            this.imageCollection1.InsertGalleryImage("forward_16x16.png", "images/navigation/forward_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/navigation/forward_16x16.png"), 6);
            this.imageCollection1.Images.SetKeyName(6, "forward_16x16.png");
            this.imageCollection1.InsertGalleryImage("next_16x16.png", "images/arrows/next_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/arrows/next_16x16.png"), 7);
            this.imageCollection1.Images.SetKeyName(7, "next_16x16.png");
            // 
            // lueAddStepCategory
            // 
            this.lueAddStepCategory.Location = new System.Drawing.Point(360, 112);
            this.lueAddStepCategory.Name = "lueAddStepCategory";
            this.lueAddStepCategory.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueAddStepCategory.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Equipment Category", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ProfitLossCode", "Profit Loss Code", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("BalanceSheetCode", "Balance Sheet Code", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DisposalsGL", "Disposals GL", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueAddStepCategory.Properties.DataSource = this.spAS11011EquipmentCategoryListBindingSource;
            this.lueAddStepCategory.Properties.DisplayMember = "Description";
            this.lueAddStepCategory.Properties.NullText = "";
            this.lueAddStepCategory.Properties.NullValuePrompt = "-- Please Select Category --";
            this.lueAddStepCategory.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueAddStepCategory.Properties.PopupWidth = 700;
            this.lueAddStepCategory.Properties.ValueMember = "EquipmentCategory";
            this.lueAddStepCategory.Size = new System.Drawing.Size(338, 20);
            this.lueAddStepCategory.StyleController = this.layoutControl1;
            this.lueAddStepCategory.TabIndex = 14;
            // 
            // spAS11011EquipmentCategoryListBindingSource
            // 
            this.spAS11011EquipmentCategoryListBindingSource.DataMember = "sp_AS_11011_Equipment_Category_List";
            this.spAS11011EquipmentCategoryListBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "DataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // lueAddStepMake
            // 
            this.lueAddStepMake.Location = new System.Drawing.Point(360, 160);
            this.lueAddStepMake.Name = "lueAddStepMake";
            this.lueAddStepMake.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueAddStepMake.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CategoryDescription", "Category", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Make", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueAddStepMake.Properties.DataSource = this.spAS11014MakeListBindingSource;
            this.lueAddStepMake.Properties.DisplayMember = "Description";
            this.lueAddStepMake.Properties.NullText = "";
            this.lueAddStepMake.Properties.NullValuePrompt = "-- Please Select Make --";
            this.lueAddStepMake.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueAddStepMake.Properties.PopupWidth = 700;
            this.lueAddStepMake.Properties.ValueMember = "Make";
            this.lueAddStepMake.Size = new System.Drawing.Size(338, 20);
            this.lueAddStepMake.StyleController = this.layoutControl1;
            this.lueAddStepMake.TabIndex = 13;
            // 
            // spAS11014MakeListBindingSource
            // 
            this.spAS11014MakeListBindingSource.DataMember = "sp_AS_11014_Make_List";
            this.spAS11014MakeListBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // lueEditStepModel
            // 
            this.lueEditStepModel.Location = new System.Drawing.Point(360, 256);
            this.lueEditStepModel.Name = "lueEditStepModel";
            this.lueEditStepModel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueEditStepModel.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Make_Description", "Make", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Model", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueEditStepModel.Properties.DataSource = this.spAS11017ModelListBindingSource;
            this.lueEditStepModel.Properties.DisplayMember = "Description";
            this.lueEditStepModel.Properties.NullText = "";
            this.lueEditStepModel.Properties.NullValuePrompt = "-- Please Select Model --";
            this.lueEditStepModel.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueEditStepModel.Properties.PopupWidth = 700;
            this.lueEditStepModel.Properties.ValueMember = "Model";
            this.lueEditStepModel.Size = new System.Drawing.Size(338, 20);
            this.lueEditStepModel.StyleController = this.layoutControl1;
            this.lueEditStepModel.TabIndex = 12;
            // 
            // spAS11017ModelListBindingSource
            // 
            this.spAS11017ModelListBindingSource.DataMember = "sp_AS_11017_Model_List";
            this.spAS11017ModelListBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // lueEditStepMake
            // 
            this.lueEditStepMake.Location = new System.Drawing.Point(360, 208);
            this.lueEditStepMake.Name = "lueEditStepMake";
            this.lueEditStepMake.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueEditStepMake.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CategoryDescription", "Category", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Make", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueEditStepMake.Properties.DataSource = this.spAS11014MakeListBindingSource;
            this.lueEditStepMake.Properties.DisplayMember = "Description";
            this.lueEditStepMake.Properties.NullText = "";
            this.lueEditStepMake.Properties.NullValuePrompt = "-- Please Select Make --";
            this.lueEditStepMake.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueEditStepMake.Properties.PopupWidth = 700;
            this.lueEditStepMake.Properties.ValueMember = "Make";
            this.lueEditStepMake.Size = new System.Drawing.Size(338, 20);
            this.lueEditStepMake.StyleController = this.layoutControl1;
            this.lueEditStepMake.TabIndex = 11;
            // 
            // txtEditStepModelDescription
            // 
            this.txtEditStepModelDescription.Location = new System.Drawing.Point(360, 280);
            this.txtEditStepModelDescription.Name = "txtEditStepModelDescription";
            this.txtEditStepModelDescription.Size = new System.Drawing.Size(338, 20);
            this.txtEditStepModelDescription.StyleController = this.layoutControl1;
            this.txtEditStepModelDescription.TabIndex = 9;
            // 
            // txtEditStepMakeDescription
            // 
            this.txtEditStepMakeDescription.Location = new System.Drawing.Point(360, 232);
            this.txtEditStepMakeDescription.Name = "txtEditStepMakeDescription";
            this.txtEditStepMakeDescription.Size = new System.Drawing.Size(338, 20);
            this.txtEditStepMakeDescription.StyleController = this.layoutControl1;
            this.txtEditStepMakeDescription.TabIndex = 8;
            // 
            // txtAddStepModelDescription
            // 
            this.txtAddStepModelDescription.Location = new System.Drawing.Point(360, 184);
            this.txtAddStepModelDescription.Name = "txtAddStepModelDescription";
            this.txtAddStepModelDescription.Size = new System.Drawing.Size(338, 20);
            this.txtAddStepModelDescription.StyleController = this.layoutControl1;
            this.txtAddStepModelDescription.TabIndex = 7;
            // 
            // txtAddStepMakeDescription
            // 
            this.txtAddStepMakeDescription.Location = new System.Drawing.Point(360, 136);
            this.txtAddStepMakeDescription.Name = "txtAddStepMakeDescription";
            this.txtAddStepMakeDescription.Size = new System.Drawing.Size(338, 20);
            this.txtAddStepMakeDescription.StyleController = this.layoutControl1;
            this.txtAddStepMakeDescription.TabIndex = 6;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnRefresh.Appearance.Options.UseFont = true;
            this.btnRefresh.ImageIndex = 5;
            this.btnRefresh.ImageList = this.imageCollection1;
            this.btnRefresh.Location = new System.Drawing.Point(42, 36);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(111, 22);
            this.btnRefresh.StyleController = this.layoutControl1;
            this.btnRefresh.TabIndex = 5;
            this.btnRefresh.Text = "Refresh List";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lblAction,
            this.lblProceed,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.emptySpaceItem5,
            this.layoutControlItem2,
            this.lcEditViewMake,
            this.lcEditMake,
            this.lcEditViewModel,
            this.lcEditModel,
            this.lciCategory,
            this.lcAddMake,
            this.lcAddViewMake,
            this.lcAddModel,
            this.emptySpaceItem3,
            this.emptySpaceItem4,
            this.layoutControlItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(40, 40, 0, 40);
            this.layoutControlGroup1.Size = new System.Drawing.Size(844, 365);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // lblAction
            // 
            this.lblAction.AllowHotTrack = false;
            this.lblAction.AppearanceItemCaption.Font = new System.Drawing.Font("Segoe UI Semibold", 15.75F, System.Drawing.FontStyle.Bold);
            this.lblAction.AppearanceItemCaption.Options.UseFont = true;
            this.lblAction.CustomizationFormText = "Options:";
            this.lblAction.Location = new System.Drawing.Point(0, 0);
            this.lblAction.Name = "lblAction";
            this.lblAction.Size = new System.Drawing.Size(764, 34);
            this.lblAction.Text = "Add Make:";
            this.lblAction.TextSize = new System.Drawing.Size(200, 30);
            // 
            // lblProceed
            // 
            this.lblProceed.AllowHotTrack = false;
            this.lblProceed.CustomizationFormText = "Select the category and make to continue";
            this.lblProceed.Location = new System.Drawing.Point(0, 312);
            this.lblProceed.Name = "lblProceed";
            this.lblProceed.Padding = new DevExpress.XtraLayout.Utils.Padding(80, 0, 0, 0);
            this.lblProceed.Size = new System.Drawing.Size(764, 13);
            this.lblProceed.Text = "Select the category and make to continue";
            this.lblProceed.TextSize = new System.Drawing.Size(200, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(115, 34);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(649, 26);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(660, 60);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(104, 224);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 60);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(115, 252);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.btnRefresh;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 34);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(115, 26);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // lcEditViewMake
            // 
            this.lcEditViewMake.Control = this.lueEditStepMake;
            this.lcEditViewMake.CustomizationFormText = "Select Make To Edit :";
            this.lcEditViewMake.Location = new System.Drawing.Point(115, 206);
            this.lcEditViewMake.Name = "lcEditViewMake";
            this.lcEditViewMake.Size = new System.Drawing.Size(545, 24);
            this.lcEditViewMake.Text = "Select Make To Edit :";
            this.lcEditViewMake.TextSize = new System.Drawing.Size(200, 13);
            this.lcEditViewMake.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // lcEditMake
            // 
            this.lcEditMake.Control = this.txtEditStepMakeDescription;
            this.lcEditMake.CustomizationFormText = "Enter New Make Description :";
            this.lcEditMake.Location = new System.Drawing.Point(115, 230);
            this.lcEditMake.Name = "lcEditMake";
            this.lcEditMake.Size = new System.Drawing.Size(545, 24);
            this.lcEditMake.Text = "Enter New Make Description :";
            this.lcEditMake.TextSize = new System.Drawing.Size(200, 13);
            this.lcEditMake.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // lcEditViewModel
            // 
            this.lcEditViewModel.Control = this.lueEditStepModel;
            this.lcEditViewModel.CustomizationFormText = "Select Model To Edit :";
            this.lcEditViewModel.Location = new System.Drawing.Point(115, 254);
            this.lcEditViewModel.Name = "lcEditViewModel";
            this.lcEditViewModel.Size = new System.Drawing.Size(545, 24);
            this.lcEditViewModel.Text = "Select Model To Edit :";
            this.lcEditViewModel.TextSize = new System.Drawing.Size(200, 13);
            this.lcEditViewModel.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // lcEditModel
            // 
            this.lcEditModel.Control = this.txtEditStepModelDescription;
            this.lcEditModel.CustomizationFormText = "Enter New Model Description :";
            this.lcEditModel.Location = new System.Drawing.Point(115, 278);
            this.lcEditModel.Name = "lcEditModel";
            this.lcEditModel.Size = new System.Drawing.Size(545, 24);
            this.lcEditModel.Text = "Enter New Model Description :";
            this.lcEditModel.TextSize = new System.Drawing.Size(200, 13);
            this.lcEditModel.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // lciCategory
            // 
            this.lciCategory.Control = this.lueAddStepCategory;
            this.lciCategory.CustomizationFormText = "Select Category :";
            this.lciCategory.Location = new System.Drawing.Point(115, 110);
            this.lciCategory.MinSize = new System.Drawing.Size(545, 24);
            this.lciCategory.Name = "lciCategory";
            this.lciCategory.Size = new System.Drawing.Size(545, 24);
            this.lciCategory.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciCategory.Text = "Select Category :";
            this.lciCategory.TextSize = new System.Drawing.Size(200, 13);
            // 
            // lcAddMake
            // 
            this.lcAddMake.Control = this.txtAddStepMakeDescription;
            this.lcAddMake.CustomizationFormText = "Make Description :";
            this.lcAddMake.Location = new System.Drawing.Point(115, 134);
            this.lcAddMake.Name = "lcAddMake";
            this.lcAddMake.Size = new System.Drawing.Size(545, 24);
            this.lcAddMake.Text = "Make Description :";
            this.lcAddMake.TextSize = new System.Drawing.Size(200, 13);
            this.lcAddMake.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // lcAddViewMake
            // 
            this.lcAddViewMake.Control = this.lueAddStepMake;
            this.lcAddViewMake.CustomizationFormText = "Select Make :";
            this.lcAddViewMake.Location = new System.Drawing.Point(115, 158);
            this.lcAddViewMake.Name = "lcAddViewMake";
            this.lcAddViewMake.Size = new System.Drawing.Size(545, 24);
            this.lcAddViewMake.Text = "Select Make :";
            this.lcAddViewMake.TextSize = new System.Drawing.Size(200, 13);
            // 
            // lcAddModel
            // 
            this.lcAddModel.Control = this.txtAddStepModelDescription;
            this.lcAddModel.CustomizationFormText = "Model Description :";
            this.lcAddModel.Location = new System.Drawing.Point(115, 182);
            this.lcAddModel.Name = "lcAddModel";
            this.lcAddModel.Size = new System.Drawing.Size(545, 24);
            this.lcAddModel.Text = "Model Description :";
            this.lcAddModel.TextSize = new System.Drawing.Size(200, 13);
            this.lcAddModel.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(115, 60);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(545, 50);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(545, 50);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(545, 50);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(115, 302);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(545, 10);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.btnNext;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(660, 284);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(104, 28);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // sp_AS_11014_Make_ListTableAdapter
            // 
            this.sp_AS_11014_Make_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11011_Equipment_Category_ListTableAdapter
            // 
            this.sp_AS_11011_Equipment_Category_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11017_Model_ListTableAdapter
            // 
            this.sp_AS_11017_Model_ListTableAdapter.ClearBeforeFill = true;
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // spAS11000DuplicateMakeModelSearchBindingSource
            // 
            this.spAS11000DuplicateMakeModelSearchBindingSource.DataMember = "sp_AS_11000_Duplicate_Make_Model_Search";
            this.spAS11000DuplicateMakeModelSearchBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11000_Duplicate_Make_Model_SearchTableAdapter
            // 
            this.sp_AS_11000_Duplicate_Make_Model_SearchTableAdapter.ClearBeforeFill = true;
            // 
            // ucStep2Page
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Appearance.Options.UseBackColor = true;
            this.Appearance.Options.UseForeColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.LookAndFeel.SkinName = "Office 2013";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "ucStep2Page";
            this.Size = new System.Drawing.Size(860, 355);
            this.Enter += new System.EventHandler(this.ucStep2Page_Enter);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueAddStepCategory.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11011EquipmentCategoryListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueAddStepMake.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11014MakeListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueEditStepModel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11017ModelListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueEditStepMake.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEditStepModelDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEditStepMakeDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddStepModelDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddStepMakeDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAction)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblProceed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcEditViewMake)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcEditMake)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcEditViewModel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcEditModel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcAddMake)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcAddViewMake)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcAddModel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000DuplicateMakeModelSearchBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.SimpleLabelItem lblAction;
        private DevExpress.XtraLayout.SimpleLabelItem lblProceed;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.LookUpEdit lueEditStepModel;
        private DevExpress.XtraEditors.LookUpEdit lueEditStepMake;
        private DevExpress.XtraEditors.TextEdit txtEditStepModelDescription;
        private DevExpress.XtraEditors.TextEdit txtEditStepMakeDescription;
        private DevExpress.XtraEditors.TextEdit txtAddStepModelDescription;
        private DevExpress.XtraEditors.TextEdit txtAddStepMakeDescription;
        private DevExpress.XtraEditors.SimpleButton btnRefresh;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem lcAddMake;
        private DevExpress.XtraLayout.LayoutControlItem lcAddModel;
        private DevExpress.XtraLayout.LayoutControlItem lcEditMake;
        private DevExpress.XtraLayout.LayoutControlItem lcEditModel;
        private DevExpress.XtraLayout.LayoutControlItem lcEditViewMake;
        private DevExpress.XtraLayout.LayoutControlItem lcEditViewModel;
        private DevExpress.XtraEditors.LookUpEdit lueAddStepMake;
        private DevExpress.XtraLayout.LayoutControlItem lcAddViewMake;
        private System.Windows.Forms.BindingSource spAS11014MakeListBindingSource;
        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private System.Windows.Forms.BindingSource spAS11011EquipmentCategoryListBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11014_Make_ListTableAdapter sp_AS_11014_Make_ListTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11011_Equipment_Category_ListTableAdapter sp_AS_11011_Equipment_Category_ListTableAdapter;
        private System.Windows.Forms.BindingSource spAS11017ModelListBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11017_Model_ListTableAdapter sp_AS_11017_Model_ListTableAdapter;
        private DevExpress.XtraEditors.LookUpEdit lueAddStepCategory;
        private DevExpress.XtraLayout.LayoutControlItem lciCategory;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.SimpleButton btnNext;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private System.Windows.Forms.BindingSource spAS11000DuplicateMakeModelSearchBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_Duplicate_Make_Model_SearchTableAdapter sp_AS_11000_Duplicate_Make_Model_SearchTableAdapter;
    }
}

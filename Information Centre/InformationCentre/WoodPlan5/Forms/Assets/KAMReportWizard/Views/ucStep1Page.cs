﻿using System.Windows.Forms;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors;

namespace WoodPlan5.Forms.Assets.KAMReportWizard.Views
{
    public partial class ucStep1Page : Views.BaseWizardPage
    {
        public ucStep1Page()
        {
            InitializeComponent();
        }

        private void radioGroupActionOption_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if ((((RadioGroup)sender).SelectedIndex) != -1 )
            {
                ((ViewModels.Step1PageViewModel)PageViewModel).blnSelected = true;
                WizardViewModel.PageCompleted();
            }
            else 
            {                
                ((ViewModels.Step1PageViewModel)PageViewModel).blnSelected = false;
            }
        }

        private void ucStep1Page_Validated(object sender, System.EventArgs e)
        {

            //if (radioGroupActionOption.SelectedIndex != -1)
            //{
            //    switch (radioGroupActionOption.SelectedIndex)
            //    {
            //        case 0://add make
            //            ucStep2Page.strSelectedAction = "addmake";                       
            //            break;
            //        case 1://add model
            //            ucStep2Page.strSelectedAction = "addmodel";
            //            break;
            //        case 2://edit make
            //            ucStep2Page.strSelectedAction = "editmake";
            //            break;
            //        case 3://edit make
            //            ucStep2Page.strSelectedAction = "editmodel";
            //            break;
            //    }
            //}
            //else
            //{
            //    ucStep2Page.strSelectedAction = "";
            //}
        }

        private void btnNext_Click(object sender, System.EventArgs e)
        {
            if (validatePage())
            {
                ((ViewModels.Step1PageViewModel)PageViewModel).blnSelected = true;
             //   updateValues();
                WizardViewModel.PageCompleted();

            }
            else
            {
                ((ViewModels.Step1PageViewModel)PageViewModel).blnSelected = false;
            }
        }

        private bool validatePage()
        {
            bool validEntry;
            bool valid = false;
            dxErrorProvider1.ClearErrors();
         //   if (lueClientID.sele)
            return valid;
        }
        
        
    }
}
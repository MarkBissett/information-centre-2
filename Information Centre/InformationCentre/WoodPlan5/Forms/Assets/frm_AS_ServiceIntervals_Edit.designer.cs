﻿namespace WoodPlan5
{
    partial class frm_AS_ServiceIntervals_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AS_ServiceIntervals_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.tableAdapterManager = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager();
            this.barStatus = new DevExpress.XtraBars.Bar();
            this.bsiFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.editFormLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.editFormDataNavigator = new DevExpress.XtraEditors.DataNavigator();
            this.spAS11054ServiceIntervalsItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.editFormDataLayoutControlGroup = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.txtEquipmentReference = new DevExpress.XtraEditors.TextEdit();
            this.lueServiceTypeID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLServiceTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.deServiceDueDate = new DevExpress.XtraEditors.DateEdit();
            this.lueUnitTypeID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLUnitTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spnUnitsFrom = new DevExpress.XtraEditors.SpinEdit();
            this.spnUnitsTo = new DevExpress.XtraEditors.SpinEdit();
            this.ceServiceDone = new DevExpress.XtraEditors.CheckEdit();
            this.ceRequireReminder = new DevExpress.XtraEditors.CheckEdit();
            this.lueAlertUnitsID = new DevExpress.XtraEditors.LookUpEdit();
            this.spnAlertWithinXUnits = new DevExpress.XtraEditors.SpinEdit();
            this.ceHasAlternativeAlert = new DevExpress.XtraEditors.CheckEdit();
            this.lueLinkedServiceInterval = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11054ServiceIntervalsListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ServiceDescriptionMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForServiceDone = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAlertUnitsID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLinkedServiceInterval = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForServiceTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForServiceDueDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUnitsTo = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAlertWithinXUnits = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUnitsFrom = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUnitTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForHasAlternativeAlert = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRequireReminder = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForServiceDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForEquipmentReference = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp_AS_11000_PL_Service_TypeTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Service_TypeTableAdapter();
            this.sp_AS_11054_Service_Intervals_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11054_Service_Intervals_ItemTableAdapter();
            this.sp_AS_11000_PL_Unit_TypeTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Unit_TypeTableAdapter();
            this.sp_AS_11054_Service_Intervals_ListTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11054_Service_Intervals_ListTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11054ServiceIntervalsItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).BeginInit();
            this.editFormDataLayoutControlGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentReference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueServiceTypeID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLServiceTypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deServiceDueDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deServiceDueDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueUnitTypeID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLUnitTypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnUnitsFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnUnitsTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceServiceDone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceRequireReminder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueAlertUnitsID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnAlertWithinXUnits.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceHasAlternativeAlert.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueLinkedServiceInterval.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11054ServiceIntervalsListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ServiceDescriptionMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForServiceDone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAlertUnitsID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedServiceInterval)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForServiceTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForServiceDueDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUnitsTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAlertWithinXUnits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUnitsFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUnitTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHasAlternativeAlert)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRequireReminder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForServiceDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipmentReference)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1285, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 483);
            this.barDockControlBottom.Size = new System.Drawing.Size(1285, 30);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 457);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1285, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 457);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barStatus,
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.bsiFormMode,
            this.barStaticItem1,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemChangesPending,
            this.barStaticItemInformation});
            this.barManager1.MaxItemId = 35;
            this.barManager1.StatusBar = this.barStatus;
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "dataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.sp_AS_11002_Equipment_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11005_Vehicle_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11008_Plant_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11011_Equipment_Category_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11014_Make_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11017_Model_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11020_Supplier_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Tracker_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Verilocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11026_Gadget_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11029_Office_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11032_Hardware_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11035_Software_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11041_Transaction_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11047_Equipment_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11050_Depreciation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Data_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Interval_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11057_Company_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11060_Department_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11063_Cost_Centre_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11066_Depreciation_Settings_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11075_Cover_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11078_Work_Detail_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11081_Incident_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11084_Purpose_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11087_Equipment_Category_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11089_Fuel_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11093_Initial_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11096_Road_Tax_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11099_Improvement_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11102_Rental_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11105_Speeding_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11108_Rental_Details_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11112_Keeper_Email_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11117_Sim_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11120_Additional_FollowUps_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11123_Sim_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_AlertTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11129_Software_Assign_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11133_Picklist_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_AS_11141_Hardware_Specifications_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11157_Email_Schedule_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11163_Service_ScheduleTableAdapter = null;
            this.tableAdapterManager.sp_AS_11166_P11D_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11169_NetBookValue_ItemTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // barStatus
            // 
            this.barStatus.BarName = "Status Bar";
            this.barStatus.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.barStatus.DockCol = 0;
            this.barStatus.DockRow = 0;
            this.barStatus.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.barStatus.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiFormMode, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barStaticItemInformation, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barStatus.OptionsBar.AllowQuickCustomization = false;
            this.barStatus.OptionsBar.DrawDragBorder = false;
            this.barStatus.OptionsBar.UseWholeRow = true;
            this.barStatus.Text = "Status bar";
            // 
            // bsiFormMode
            // 
            this.bsiFormMode.Caption = "Form Mode: Editing";
            this.bsiFormMode.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.Glyph")));
            this.bsiFormMode.Id = 29;
            this.bsiFormMode.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.LargeGlyph")));
            this.bsiFormMode.Name = "bsiFormMode";
            toolTipTitleItem1.Text = "Form Mode - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bsiFormMode.SuperTip = superToolTip1;
            this.bsiFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 31;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 32;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.Glyph")));
            this.barStaticItemInformation.Id = 33;
            this.barStaticItemInformation.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.LargeGlyph")));
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 27;
            this.bbiFormSave.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.LargeGlyph")));
            this.bbiFormSave.Name = "bbiFormSave";
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Text = "Save Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormSave.SuperTip = superToolTip2;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.Glyph")));
            this.bbiFormCancel.Id = 28;
            this.bbiFormCancel.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.LargeGlyph")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Text = "Cancel Button - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiFormCancel.SuperTip = superToolTip3;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "barStaticItem1";
            this.barStaticItem1.Id = 30;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // editFormLayoutControlGroup
            // 
            this.editFormLayoutControlGroup.CustomizationFormText = "Main Layout Control Group";
            this.editFormLayoutControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.editFormLayoutControlGroup.GroupBordersVisible = false;
            this.editFormLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlGroup1,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.ItemForEquipmentReference});
            this.editFormLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.editFormLayoutControlGroup.Name = "Root";
            this.editFormLayoutControlGroup.Size = new System.Drawing.Size(1285, 457);
            this.editFormLayoutControlGroup.Text = "Root";
            this.editFormLayoutControlGroup.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.editFormDataNavigator;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(173, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(382, 23);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // editFormDataNavigator
            // 
            this.editFormDataNavigator.Buttons.Append.Visible = false;
            this.editFormDataNavigator.Buttons.CancelEdit.Visible = false;
            this.editFormDataNavigator.Buttons.EndEdit.Visible = false;
            this.editFormDataNavigator.Buttons.Remove.Visible = false;
            this.editFormDataNavigator.DataSource = this.spAS11054ServiceIntervalsItemBindingSource;
            this.editFormDataNavigator.Location = new System.Drawing.Point(185, 12);
            this.editFormDataNavigator.Name = "editFormDataNavigator";
            this.editFormDataNavigator.Size = new System.Drawing.Size(378, 19);
            this.editFormDataNavigator.StyleController = this.editFormDataLayoutControlGroup;
            this.editFormDataNavigator.TabIndex = 122;
            this.editFormDataNavigator.Text = "depreciationDataNavigator";
            this.editFormDataNavigator.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.editFormDataNavigator.TextStringFormat = "Service Interval Record {0} of {1}";
            this.editFormDataNavigator.PositionChanged += new System.EventHandler(this.editFormDataNavigator_PositionChanged);
            // 
            // spAS11054ServiceIntervalsItemBindingSource
            // 
            this.spAS11054ServiceIntervalsItemBindingSource.DataMember = "sp_AS_11054_Service_Intervals_Item";
            this.spAS11054ServiceIntervalsItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // editFormDataLayoutControlGroup
            // 
            this.editFormDataLayoutControlGroup.Controls.Add(this.editFormDataNavigator);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtEquipmentReference);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueServiceTypeID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.deServiceDueDate);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueUnitTypeID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.spnUnitsFrom);
            this.editFormDataLayoutControlGroup.Controls.Add(this.spnUnitsTo);
            this.editFormDataLayoutControlGroup.Controls.Add(this.ceServiceDone);
            this.editFormDataLayoutControlGroup.Controls.Add(this.ceRequireReminder);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueAlertUnitsID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.spnAlertWithinXUnits);
            this.editFormDataLayoutControlGroup.Controls.Add(this.ceHasAlternativeAlert);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueLinkedServiceInterval);
            this.editFormDataLayoutControlGroup.Controls.Add(this.ServiceDescriptionMemoEdit);
            this.editFormDataLayoutControlGroup.DataSource = this.spAS11054ServiceIntervalsItemBindingSource;
            this.editFormDataLayoutControlGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editFormDataLayoutControlGroup.Location = new System.Drawing.Point(0, 26);
            this.editFormDataLayoutControlGroup.Name = "editFormDataLayoutControlGroup";
            this.editFormDataLayoutControlGroup.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(2338, 246, 250, 350);
            this.editFormDataLayoutControlGroup.Root = this.editFormLayoutControlGroup;
            this.editFormDataLayoutControlGroup.Size = new System.Drawing.Size(1285, 457);
            this.editFormDataLayoutControlGroup.TabIndex = 4;
            this.editFormDataLayoutControlGroup.Text = "dataLayoutControl1";
            // 
            // txtEquipmentReference
            // 
            this.txtEquipmentReference.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11054ServiceIntervalsItemBindingSource, "EquipmentReference", true));
            this.txtEquipmentReference.Location = new System.Drawing.Point(131, 35);
            this.txtEquipmentReference.MenuManager = this.barManager1;
            this.txtEquipmentReference.Name = "txtEquipmentReference";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtEquipmentReference, true);
            this.txtEquipmentReference.Size = new System.Drawing.Size(1142, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtEquipmentReference, optionsSpelling1);
            this.txtEquipmentReference.StyleController = this.editFormDataLayoutControlGroup;
            this.txtEquipmentReference.TabIndex = 123;
            // 
            // lueServiceTypeID
            // 
            this.lueServiceTypeID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11054ServiceIntervalsItemBindingSource, "ServiceTypeID", true));
            this.lueServiceTypeID.Location = new System.Drawing.Point(131, 59);
            this.lueServiceTypeID.MenuManager = this.barManager1;
            this.lueServiceTypeID.Name = "lueServiceTypeID";
            this.lueServiceTypeID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueServiceTypeID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Service Type", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueServiceTypeID.Properties.DataSource = this.spAS11000PLServiceTypeBindingSource;
            this.lueServiceTypeID.Properties.DisplayMember = "Value";
            this.lueServiceTypeID.Properties.NullText = "";
            this.lueServiceTypeID.Properties.NullValuePrompt = "--Please Select Service Type --";
            this.lueServiceTypeID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueServiceTypeID.Properties.ValueMember = "PickListID";
            this.lueServiceTypeID.Size = new System.Drawing.Size(509, 20);
            this.lueServiceTypeID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueServiceTypeID.TabIndex = 124;
            this.lueServiceTypeID.Tag = "Service Type";
            this.lueServiceTypeID.EditValueChanged += new System.EventHandler(this.lueServiceTypeID_EditValueChanged);
            this.lueServiceTypeID.Validating += new System.ComponentModel.CancelEventHandler(this.lookupEdit_Validating);
            // 
            // spAS11000PLServiceTypeBindingSource
            // 
            this.spAS11000PLServiceTypeBindingSource.DataMember = "sp_AS_11000_PL_Service_Type";
            this.spAS11000PLServiceTypeBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // deServiceDueDate
            // 
            this.deServiceDueDate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11054ServiceIntervalsItemBindingSource, "ServiceDueDate", true));
            this.deServiceDueDate.EditValue = null;
            this.deServiceDueDate.Location = new System.Drawing.Point(763, 59);
            this.deServiceDueDate.MenuManager = this.barManager1;
            this.deServiceDueDate.Name = "deServiceDueDate";
            this.deServiceDueDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deServiceDueDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deServiceDueDate.Size = new System.Drawing.Size(510, 20);
            this.deServiceDueDate.StyleController = this.editFormDataLayoutControlGroup;
            this.deServiceDueDate.TabIndex = 125;
            this.deServiceDueDate.Tag = "Service Due Date";
            this.deServiceDueDate.Validating += new System.ComponentModel.CancelEventHandler(this.deServiceDueDate_Validating);
            // 
            // lueUnitTypeID
            // 
            this.lueUnitTypeID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11054ServiceIntervalsItemBindingSource, "UnitTypeID", true));
            this.lueUnitTypeID.Location = new System.Drawing.Point(763, 107);
            this.lueUnitTypeID.MenuManager = this.barManager1;
            this.lueUnitTypeID.Name = "lueUnitTypeID";
            this.lueUnitTypeID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueUnitTypeID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Units", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueUnitTypeID.Properties.DataSource = this.spAS11000PLUnitTypeBindingSource;
            this.lueUnitTypeID.Properties.DisplayMember = "Value";
            this.lueUnitTypeID.Properties.NullText = "";
            this.lueUnitTypeID.Properties.NullValuePrompt = "--Please Select Unit Description --";
            this.lueUnitTypeID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueUnitTypeID.Properties.ValueMember = "PickListID";
            this.lueUnitTypeID.Size = new System.Drawing.Size(510, 20);
            this.lueUnitTypeID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueUnitTypeID.TabIndex = 126;
            this.lueUnitTypeID.Tag = "Unit Type";
            this.lueUnitTypeID.EditValueChanged += new System.EventHandler(this.lueUnitTypeID_EditValueChanged);
            this.lueUnitTypeID.Validating += new System.ComponentModel.CancelEventHandler(this.lookupEdit_Validating);
            // 
            // spAS11000PLUnitTypeBindingSource
            // 
            this.spAS11000PLUnitTypeBindingSource.DataMember = "sp_AS_11000_PL_Unit_Type";
            this.spAS11000PLUnitTypeBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spnUnitsFrom
            // 
            this.spnUnitsFrom.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11054ServiceIntervalsItemBindingSource, "UnitsFrom", true));
            this.spnUnitsFrom.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnUnitsFrom.Location = new System.Drawing.Point(131, 131);
            this.spnUnitsFrom.MenuManager = this.barManager1;
            this.spnUnitsFrom.Name = "spnUnitsFrom";
            this.spnUnitsFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnUnitsFrom.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spnUnitsFrom.Properties.MaxValue = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.spnUnitsFrom.Size = new System.Drawing.Size(507, 20);
            this.spnUnitsFrom.StyleController = this.editFormDataLayoutControlGroup;
            this.spnUnitsFrom.TabIndex = 127;
            this.spnUnitsFrom.Validating += new System.ComponentModel.CancelEventHandler(this.spinEdit_Validating);
            // 
            // spnUnitsTo
            // 
            this.spnUnitsTo.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11054ServiceIntervalsItemBindingSource, "UnitsTo", true));
            this.spnUnitsTo.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnUnitsTo.Location = new System.Drawing.Point(761, 131);
            this.spnUnitsTo.MenuManager = this.barManager1;
            this.spnUnitsTo.Name = "spnUnitsTo";
            this.spnUnitsTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnUnitsTo.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spnUnitsTo.Properties.MaxValue = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.spnUnitsTo.Size = new System.Drawing.Size(512, 20);
            this.spnUnitsTo.StyleController = this.editFormDataLayoutControlGroup;
            this.spnUnitsTo.TabIndex = 128;
            this.spnUnitsTo.Validating += new System.ComponentModel.CancelEventHandler(this.spinEdit_Validating);
            // 
            // ceServiceDone
            // 
            this.ceServiceDone.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11054ServiceIntervalsItemBindingSource, "ServiceDone", true));
            this.ceServiceDone.Location = new System.Drawing.Point(131, 155);
            this.ceServiceDone.MenuManager = this.barManager1;
            this.ceServiceDone.Name = "ceServiceDone";
            this.ceServiceDone.Properties.Caption = "";
            this.ceServiceDone.Properties.Tag = "Service Done";
            this.ceServiceDone.Size = new System.Drawing.Size(1142, 19);
            this.ceServiceDone.StyleController = this.editFormDataLayoutControlGroup;
            this.ceServiceDone.TabIndex = 129;
            // 
            // ceRequireReminder
            // 
            this.ceRequireReminder.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11054ServiceIntervalsItemBindingSource, "RequireReminder", true));
            this.ceRequireReminder.Location = new System.Drawing.Point(131, 107);
            this.ceRequireReminder.MenuManager = this.barManager1;
            this.ceRequireReminder.Name = "ceRequireReminder";
            this.ceRequireReminder.Properties.Caption = "";
            this.ceRequireReminder.Size = new System.Drawing.Size(509, 19);
            this.ceRequireReminder.StyleController = this.editFormDataLayoutControlGroup;
            this.ceRequireReminder.TabIndex = 130;
            // 
            // lueAlertUnitsID
            // 
            this.lueAlertUnitsID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11054ServiceIntervalsItemBindingSource, "AlertUnitsID", true));
            this.lueAlertUnitsID.Location = new System.Drawing.Point(131, 178);
            this.lueAlertUnitsID.MenuManager = this.barManager1;
            this.lueAlertUnitsID.Name = "lueAlertUnitsID";
            this.lueAlertUnitsID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueAlertUnitsID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Units", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueAlertUnitsID.Properties.DataSource = this.spAS11000PLUnitTypeBindingSource;
            this.lueAlertUnitsID.Properties.DisplayMember = "Value";
            this.lueAlertUnitsID.Properties.NullText = "";
            this.lueAlertUnitsID.Properties.NullValuePrompt = "--Please Select Alert Unit Description--";
            this.lueAlertUnitsID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueAlertUnitsID.Properties.ValueMember = "PickListID";
            this.lueAlertUnitsID.Size = new System.Drawing.Size(509, 20);
            this.lueAlertUnitsID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueAlertUnitsID.TabIndex = 131;
            this.lueAlertUnitsID.Tag = "Alert Units";
            this.lueAlertUnitsID.EditValueChanged += new System.EventHandler(this.lueUnitTypeID_EditValueChanged);
            this.lueAlertUnitsID.Validating += new System.ComponentModel.CancelEventHandler(this.lookupEdit_Validating);
            // 
            // spnAlertWithinXUnits
            // 
            this.spnAlertWithinXUnits.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11054ServiceIntervalsItemBindingSource, "AlertWithinXUnits", true));
            this.spnAlertWithinXUnits.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnAlertWithinXUnits.Location = new System.Drawing.Point(763, 178);
            this.spnAlertWithinXUnits.MenuManager = this.barManager1;
            this.spnAlertWithinXUnits.Name = "spnAlertWithinXUnits";
            this.spnAlertWithinXUnits.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnAlertWithinXUnits.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spnAlertWithinXUnits.Properties.MaxValue = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.spnAlertWithinXUnits.Size = new System.Drawing.Size(510, 20);
            this.spnAlertWithinXUnits.StyleController = this.editFormDataLayoutControlGroup;
            this.spnAlertWithinXUnits.TabIndex = 132;
            this.spnAlertWithinXUnits.Validating += new System.ComponentModel.CancelEventHandler(this.spinEdit_Validating);
            // 
            // ceHasAlternativeAlert
            // 
            this.ceHasAlternativeAlert.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11054ServiceIntervalsItemBindingSource, "HasAlternativeAlert", true));
            this.ceHasAlternativeAlert.Location = new System.Drawing.Point(131, 83);
            this.ceHasAlternativeAlert.MenuManager = this.barManager1;
            this.ceHasAlternativeAlert.Name = "ceHasAlternativeAlert";
            this.ceHasAlternativeAlert.Properties.Caption = "";
            this.ceHasAlternativeAlert.Size = new System.Drawing.Size(509, 19);
            this.ceHasAlternativeAlert.StyleController = this.editFormDataLayoutControlGroup;
            this.ceHasAlternativeAlert.TabIndex = 133;
            this.ceHasAlternativeAlert.CheckStateChanged += new System.EventHandler(this.ceHasAlternativeAlert_CheckStateChanged);
            // 
            // lueLinkedServiceInterval
            // 
            this.lueLinkedServiceInterval.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11054ServiceIntervalsItemBindingSource, "LinkedServiceInterval", true));
            this.lueLinkedServiceInterval.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.lueLinkedServiceInterval.Location = new System.Drawing.Point(763, 83);
            this.lueLinkedServiceInterval.MenuManager = this.barManager1;
            this.lueLinkedServiceInterval.Name = "lueLinkedServiceInterval";
            this.lueLinkedServiceInterval.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueLinkedServiceInterval.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("EquipmentReference", "Equipment Reference", 113, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ServiceType", "Service Type", 72, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ServiceDueDate", "Service Due Date", 93, DevExpress.Utils.FormatType.DateTime, "dd/MM/yyyy", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("UnitType", "Unit Type", 56, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("UnitsFrom", "Units From", 61, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("UnitsTo", "Units To", 49, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ServiceDone", "Service Done", 73, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("AlertUnits", "Alert Units", 60, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("AlertWithinXUnits", "Alert Within XUnits", 99, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far)});
            this.lueLinkedServiceInterval.Properties.DataSource = this.spAS11054ServiceIntervalsListBindingSource;
            this.lueLinkedServiceInterval.Properties.DisplayMember = "ServiceType";
            this.lueLinkedServiceInterval.Properties.NullText = "";
            this.lueLinkedServiceInterval.Properties.NullValuePrompt = "-- Please Select Linked Service --";
            this.lueLinkedServiceInterval.Properties.PopupWidth = 800;
            this.lueLinkedServiceInterval.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.lueLinkedServiceInterval.Properties.ValueMember = "ServiceIntervalID";
            this.lueLinkedServiceInterval.Size = new System.Drawing.Size(510, 20);
            this.lueLinkedServiceInterval.StyleController = this.editFormDataLayoutControlGroup;
            this.lueLinkedServiceInterval.TabIndex = 134;
            this.lueLinkedServiceInterval.Tag = "Linked Service Interval";
            this.lueLinkedServiceInterval.Validating += new System.ComponentModel.CancelEventHandler(this.lookupEdit_Validating);
            // 
            // spAS11054ServiceIntervalsListBindingSource
            // 
            this.spAS11054ServiceIntervalsListBindingSource.DataMember = "sp_AS_11054_Service_Intervals_List";
            this.spAS11054ServiceIntervalsListBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // ServiceDescriptionMemoEdit
            // 
            this.ServiceDescriptionMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11054ServiceIntervalsItemBindingSource, "ServiceDescription", true));
            this.ServiceDescriptionMemoEdit.Location = new System.Drawing.Point(24, 235);
            this.ServiceDescriptionMemoEdit.MenuManager = this.barManager1;
            this.ServiceDescriptionMemoEdit.Name = "ServiceDescriptionMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.ServiceDescriptionMemoEdit, true);
            this.ServiceDescriptionMemoEdit.Size = new System.Drawing.Size(1237, 198);
            this.scSpellChecker.SetSpellCheckerOptions(this.ServiceDescriptionMemoEdit, optionsSpelling2);
            this.ServiceDescriptionMemoEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.ServiceDescriptionMemoEdit.TabIndex = 135;
            this.ServiceDescriptionMemoEdit.UseOptimizedRendering = true;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AllowDrawBackground = false;
            this.layoutControlGroup1.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForServiceDone,
            this.ItemForAlertUnitsID,
            this.ItemForLinkedServiceInterval,
            this.ItemForServiceTypeID,
            this.ItemForServiceDueDate,
            this.ItemForUnitsTo,
            this.ItemForAlertWithinXUnits,
            this.ItemForUnitsFrom,
            this.ItemForUnitTypeID,
            this.ItemForHasAlternativeAlert,
            this.ItemForRequireReminder,
            this.tabbedControlGroup1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 47);
            this.layoutControlGroup1.Name = "autoGeneratedGroup0";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1265, 390);
            this.layoutControlGroup1.Text = "autoGeneratedGroup0";
            // 
            // ItemForServiceDone
            // 
            this.ItemForServiceDone.Control = this.ceServiceDone;
            this.ItemForServiceDone.CustomizationFormText = "Service Done :";
            this.ItemForServiceDone.Location = new System.Drawing.Point(0, 96);
            this.ItemForServiceDone.Name = "ItemForServiceDone";
            this.ItemForServiceDone.Size = new System.Drawing.Size(1265, 23);
            this.ItemForServiceDone.Text = "Service Done :";
            this.ItemForServiceDone.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForAlertUnitsID
            // 
            this.ItemForAlertUnitsID.Control = this.lueAlertUnitsID;
            this.ItemForAlertUnitsID.CustomizationFormText = "Alert Units :";
            this.ItemForAlertUnitsID.Location = new System.Drawing.Point(0, 119);
            this.ItemForAlertUnitsID.Name = "ItemForAlertUnitsID";
            this.ItemForAlertUnitsID.Size = new System.Drawing.Size(632, 24);
            this.ItemForAlertUnitsID.Text = "Alert Units :";
            this.ItemForAlertUnitsID.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForLinkedServiceInterval
            // 
            this.ItemForLinkedServiceInterval.Control = this.lueLinkedServiceInterval;
            this.ItemForLinkedServiceInterval.CustomizationFormText = "Linked Service Interval :";
            this.ItemForLinkedServiceInterval.Location = new System.Drawing.Point(632, 24);
            this.ItemForLinkedServiceInterval.Name = "ItemForLinkedServiceInterval";
            this.ItemForLinkedServiceInterval.Size = new System.Drawing.Size(633, 24);
            this.ItemForLinkedServiceInterval.Text = "Linked Service Interval :";
            this.ItemForLinkedServiceInterval.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForServiceTypeID
            // 
            this.ItemForServiceTypeID.Control = this.lueServiceTypeID;
            this.ItemForServiceTypeID.CustomizationFormText = "Service Type :";
            this.ItemForServiceTypeID.Location = new System.Drawing.Point(0, 0);
            this.ItemForServiceTypeID.Name = "ItemForServiceTypeID";
            this.ItemForServiceTypeID.Size = new System.Drawing.Size(632, 24);
            this.ItemForServiceTypeID.Text = "Service Type :";
            this.ItemForServiceTypeID.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForServiceDueDate
            // 
            this.ItemForServiceDueDate.Control = this.deServiceDueDate;
            this.ItemForServiceDueDate.CustomizationFormText = "Service Due Date :";
            this.ItemForServiceDueDate.Location = new System.Drawing.Point(632, 0);
            this.ItemForServiceDueDate.Name = "ItemForServiceDueDate";
            this.ItemForServiceDueDate.Size = new System.Drawing.Size(633, 24);
            this.ItemForServiceDueDate.Text = "Service Due Date :";
            this.ItemForServiceDueDate.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForUnitsTo
            // 
            this.ItemForUnitsTo.Control = this.spnUnitsTo;
            this.ItemForUnitsTo.CustomizationFormText = "Units To :";
            this.ItemForUnitsTo.Location = new System.Drawing.Point(630, 72);
            this.ItemForUnitsTo.Name = "ItemForUnitsTo";
            this.ItemForUnitsTo.Size = new System.Drawing.Size(635, 24);
            this.ItemForUnitsTo.Text = "Units To :";
            this.ItemForUnitsTo.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForAlertWithinXUnits
            // 
            this.ItemForAlertWithinXUnits.Control = this.spnAlertWithinXUnits;
            this.ItemForAlertWithinXUnits.CustomizationFormText = "Alert Within :";
            this.ItemForAlertWithinXUnits.Location = new System.Drawing.Point(632, 119);
            this.ItemForAlertWithinXUnits.Name = "ItemForAlertWithinXUnits";
            this.ItemForAlertWithinXUnits.Size = new System.Drawing.Size(633, 24);
            this.ItemForAlertWithinXUnits.Text = "Alert Within :";
            this.ItemForAlertWithinXUnits.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForUnitsFrom
            // 
            this.ItemForUnitsFrom.Control = this.spnUnitsFrom;
            this.ItemForUnitsFrom.CustomizationFormText = "Units From :";
            this.ItemForUnitsFrom.Location = new System.Drawing.Point(0, 72);
            this.ItemForUnitsFrom.Name = "ItemForUnitsFrom";
            this.ItemForUnitsFrom.Size = new System.Drawing.Size(630, 24);
            this.ItemForUnitsFrom.Text = "Units From :";
            this.ItemForUnitsFrom.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForUnitTypeID
            // 
            this.ItemForUnitTypeID.Control = this.lueUnitTypeID;
            this.ItemForUnitTypeID.CustomizationFormText = "Unit Type :";
            this.ItemForUnitTypeID.Location = new System.Drawing.Point(632, 48);
            this.ItemForUnitTypeID.Name = "ItemForUnitTypeID";
            this.ItemForUnitTypeID.Size = new System.Drawing.Size(633, 24);
            this.ItemForUnitTypeID.Text = "Unit Type :";
            this.ItemForUnitTypeID.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForHasAlternativeAlert
            // 
            this.ItemForHasAlternativeAlert.Control = this.ceHasAlternativeAlert;
            this.ItemForHasAlternativeAlert.CustomizationFormText = "Has Alternative Alert :";
            this.ItemForHasAlternativeAlert.Location = new System.Drawing.Point(0, 24);
            this.ItemForHasAlternativeAlert.Name = "ItemForHasAlternativeAlert";
            this.ItemForHasAlternativeAlert.Size = new System.Drawing.Size(632, 24);
            this.ItemForHasAlternativeAlert.Text = "Has Alternative Alert :";
            this.ItemForHasAlternativeAlert.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForRequireReminder
            // 
            this.ItemForRequireReminder.Control = this.ceRequireReminder;
            this.ItemForRequireReminder.CustomizationFormText = "Require Reminder :";
            this.ItemForRequireReminder.Location = new System.Drawing.Point(0, 48);
            this.ItemForRequireReminder.Name = "ItemForRequireReminder";
            this.ItemForRequireReminder.Size = new System.Drawing.Size(632, 24);
            this.ItemForRequireReminder.Text = "Require Reminder :";
            this.ItemForRequireReminder.TextSize = new System.Drawing.Size(116, 13);
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 143);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup2;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(1265, 247);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.tabbedControlGroup1.Text = "tabbedControlGroup1";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "Service Description";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForServiceDescription});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1241, 202);
            this.layoutControlGroup2.Text = "Service Description";
            // 
            // ItemForServiceDescription
            // 
            this.ItemForServiceDescription.Control = this.ServiceDescriptionMemoEdit;
            this.ItemForServiceDescription.CustomizationFormText = "Service Description";
            this.ItemForServiceDescription.Location = new System.Drawing.Point(0, 0);
            this.ItemForServiceDescription.Name = "ItemForServiceDescription";
            this.ItemForServiceDescription.Size = new System.Drawing.Size(1241, 202);
            this.ItemForServiceDescription.Text = "Service Description";
            this.ItemForServiceDescription.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForServiceDescription.TextToControlDistance = 0;
            this.ItemForServiceDescription.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(173, 23);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(555, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(710, 23);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForEquipmentReference
            // 
            this.ItemForEquipmentReference.Control = this.txtEquipmentReference;
            this.ItemForEquipmentReference.CustomizationFormText = "Equipment Reference :";
            this.ItemForEquipmentReference.Location = new System.Drawing.Point(0, 23);
            this.ItemForEquipmentReference.Name = "ItemForEquipmentReference";
            this.ItemForEquipmentReference.Size = new System.Drawing.Size(1265, 24);
            this.ItemForEquipmentReference.Text = "Equipment Reference :";
            this.ItemForEquipmentReference.TextSize = new System.Drawing.Size(116, 13);
            // 
            // sp_AS_11000_PL_Service_TypeTableAdapter
            // 
            this.sp_AS_11000_PL_Service_TypeTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11054_Service_Intervals_ItemTableAdapter
            // 
            this.sp_AS_11054_Service_Intervals_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Unit_TypeTableAdapter
            // 
            this.sp_AS_11000_PL_Unit_TypeTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11054_Service_Intervals_ListTableAdapter
            // 
            this.sp_AS_11054_Service_Intervals_ListTableAdapter.ClearBeforeFill = true;
            // 
            // frm_AS_ServiceIntervals_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1285, 513);
            this.Controls.Add(this.editFormDataLayoutControlGroup);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AS_ServiceIntervals_Edit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Manage Service Intervals";
            this.Activated += new System.EventHandler(this.frm_AS_ServiceIntervals_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_AS_ServiceIntervals_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_AS_ServiceIntervals_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.editFormDataLayoutControlGroup, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11054ServiceIntervalsItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).EndInit();
            this.editFormDataLayoutControlGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentReference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueServiceTypeID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLServiceTypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deServiceDueDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deServiceDueDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueUnitTypeID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLUnitTypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnUnitsFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnUnitsTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceServiceDone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceRequireReminder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueAlertUnitsID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnAlertWithinXUnits.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceHasAlternativeAlert.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueLinkedServiceInterval.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11054ServiceIntervalsListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ServiceDescriptionMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForServiceDone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAlertUnitsID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedServiceInterval)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForServiceTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForServiceDueDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUnitsTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAlertWithinXUnits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUnitsFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUnitTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHasAlternativeAlert)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRequireReminder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForServiceDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipmentReference)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private DataSet_AS_DataEntryTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraBars.Bar barStatus;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.BarStaticItem bsiFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private DevExpress.XtraDataLayout.DataLayoutControl editFormDataLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlGroup editFormLayoutControlGroup;
        private DevExpress.XtraEditors.DataNavigator editFormDataNavigator;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private System.Windows.Forms.BindingSource spAS11000PLServiceTypeBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Service_TypeTableAdapter sp_AS_11000_PL_Service_TypeTableAdapter;
        private DevExpress.XtraEditors.TextEdit txtEquipmentReference;
        private System.Windows.Forms.BindingSource spAS11054ServiceIntervalsItemBindingSource;
        private DevExpress.XtraEditors.LookUpEdit lueServiceTypeID;
        private DevExpress.XtraEditors.DateEdit deServiceDueDate;
        private DevExpress.XtraEditors.LookUpEdit lueUnitTypeID;
        private DevExpress.XtraEditors.SpinEdit spnUnitsFrom;
        private DevExpress.XtraEditors.SpinEdit spnUnitsTo;
        private DevExpress.XtraEditors.CheckEdit ceServiceDone;
        private DevExpress.XtraEditors.CheckEdit ceRequireReminder;
        private DevExpress.XtraEditors.LookUpEdit lueAlertUnitsID;
        private DevExpress.XtraEditors.SpinEdit spnAlertWithinXUnits;
        private DevExpress.XtraEditors.CheckEdit ceHasAlternativeAlert;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEquipmentReference;
        private DevExpress.XtraLayout.LayoutControlItem ItemForServiceTypeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForServiceDueDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUnitTypeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUnitsFrom;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUnitsTo;
        private DevExpress.XtraLayout.LayoutControlItem ItemForServiceDone;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRequireReminder;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAlertUnitsID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAlertWithinXUnits;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHasAlternativeAlert;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedServiceInterval;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11054_Service_Intervals_ItemTableAdapter sp_AS_11054_Service_Intervals_ItemTableAdapter;
        private System.Windows.Forms.BindingSource spAS11000PLUnitTypeBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Unit_TypeTableAdapter sp_AS_11000_PL_Unit_TypeTableAdapter;
        private DevExpress.XtraEditors.LookUpEdit lueLinkedServiceInterval;
        private System.Windows.Forms.BindingSource spAS11054ServiceIntervalsListBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11054_Service_Intervals_ListTableAdapter sp_AS_11054_Service_Intervals_ListTableAdapter;
        private DevExpress.XtraEditors.MemoEdit ServiceDescriptionMemoEdit;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForServiceDescription;


    }
}

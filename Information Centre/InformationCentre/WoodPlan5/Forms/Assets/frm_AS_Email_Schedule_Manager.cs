﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;
using WoodPlan5.Properties;
using BaseObjects;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Xml;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
using System.Data.SqlClient;

namespace WoodPlan5
{
    public partial class frm_AS_Email_Schedule_Manager : BaseObjects.frmBase
    {
        public frm_AS_Email_Schedule_Manager()
        {
            InitializeComponent();
        }

        #region Instance Variables

        private bool isRunning = false;

        // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewStateEmailSchedule; 
        private Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        public int numOfSelectedRows = 0;
        private int i_int_FocusedGrid = 0;

        GridHitInfo downHitInfo = null;
        private uint _PropertyName;
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        public int intLinkedToRecordID = 0;
        private bool bool_FormLoading = true;
        public string strPassedInRecordDs = ""; 
        public bool emailScheduleChanged = false;
        public enum FormMode { add, edit, view, delete, block_add, block_edit };
        public enum SentenceCase { Upper, Lower, Title, AsIs }

        private string strMessage1, strMessage2;

        private bool iBool_AllowDelete = false;
        private bool iBool_AllowAdd = false;
        private bool iBool_AllowEdit = false;
        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        private string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        private string i_str_AddedRecordIDs2 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //               
        private string i_str_AddedRecordIDs3 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        private bool internalRowFocusing;

        #endregion

        #region Events

        private void emailScheduleGridView_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void emailScheduleGridView_MouseDown(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }


        private void emailScheduleGridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void emailScheduleGridView_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void emailScheduleGridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "emailScheduleGridView":
                    message = "No Schedule Details Available";
                    break;                
                default:
                    message = "No Details Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void emailScheduleGridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);
            if (isRunning) return;
            GridView view = sender as GridView;
            numOfSelectedRows = view.SelectedRowsCount;
            
            int tempNumOfSelectedRows;
            if (numOfSelectedRows > 0)
            {
                tempNumOfSelectedRows = numOfSelectedRows;
            }
            else
            {
                tempNumOfSelectedRows = 1;
            }
            int[] emailScheduleIDs = new int[tempNumOfSelectedRows];

            int[] intRowHandles = view.GetSelectedRows();
            if (view.SelectedRowsCount > 0)
            {
                int countRows = 0;

                foreach (int intRowHandle in intRowHandles)
                {
                    DataRow dr = view.GetDataRow(intRowHandle);
                    switch (i_int_FocusedGrid)
                    {
                        case 0://Email schedule
                            emailScheduleIDs[countRows] = (((DataSet_AS_DataEntry.sp_AS_11157_Email_Schedule_ItemRow)(dr)).EmailScheduleID);
                            countRows += 1;
                            break;                     
                    }
                }               
                strRecordIDs = stringRecords(emailScheduleIDs);               
            }
            else
            {
                strRecordIDs = "";
            }
            SetMenuStatus();  
        }

        private void frm_AS_Email_Schedule_Manager_Load(object sender, EventArgs e)
        {
            this.FormID = 1104;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            LoadConnectionStrings(this.GlobalSettings.ConnectionString);
            LoadAdapters();
            // Get Form Permissions //            
            ProcessPermissionsForForm();
            RefreshGridViewStateEmailSchedule = new RefreshGridState(emailScheduleGridView, "EmailScheduleID");
            //PostOpen(null);
            PopulatePickList();
        }

        private void PopulatePickList()
        {           
          //  sp_AS_11000_PL_Unit_DescriptorTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Unit_Descriptor, 25);
        }

        private void emailScheduleGridControl_EmbeddedNavigator_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = emailScheduleGridView;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                  
                    break;
                default:
                    break;
            }
        }

     
        private void frm_AS_Email_Schedule_Manager_Activated(object sender, EventArgs e)
        {
            frmActivated();
        }

        public void frmActivated()
        {
            if (UpdateRefreshStatus > 0 || !string.IsNullOrEmpty(i_str_AddedRecordIDs1))
            {
                Load_Data();
            }
            SetMenuStatus();
        }

        #endregion

        #region Method

        private string stringRecords(int[] IDs)
        {
            string strIDs = "";
            foreach (int rec in IDs)
            {
                strIDs += Convert.ToString(rec) + ',';
            }
            return strIDs;
        }
              
        public void UpdateFormRefreshStatus(int status, string strNewIDs1, string strNewIDs2, string strNewIDs3)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
            if (strNewIDs2 != "") i_str_AddedRecordIDs2 = strNewIDs2;
            if (strNewIDs3 != "") i_str_AddedRecordIDs3 = strNewIDs3;
        }
        private void Load_Data()
        {
            if (UpdateRefreshStatus > 0)
                UpdateRefreshStatus = 0;
            emailScheduleGridControl.BeginUpdate();
            sp_AS_11157_Email_Schedule_ItemTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11157_Email_Schedule_Item, "", strFormMode);
            this.RefreshGridViewStateEmailSchedule.LoadViewInfo();  // Reload any expanded groups and selected rows //
            emailScheduleGridControl.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)emailScheduleGridControl.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["EmailScheduleID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }
        }

        private void getCurrentGridControl(out GridControl gridControl, out string strMessage1, out string strMessage2)
        {
            switch (i_int_FocusedGrid)
            {
                case 0://Email schedule
                    gridControl = emailScheduleGridControl;
                    strMessage1 = "1 Email Schedule record";
                    strMessage2 = " Email Schedule records";
                    break;               
                default:
                    gridControl = emailScheduleGridControl;
                    strMessage1 = "1 Email Schedule record";
                    strMessage2 = " Email Schedule records";
                    break;
            }
        }


        private void storeGridViewState()
        {
            // Store Grid View State //
            this.RefreshGridViewStateEmailSchedule.SaveViewInfo();
        }

        private void Delete_Record()
        {
            if (!iBool_AllowDelete)
                return;
            
            int[] intRowHandles;
            int intCount = 0;
            GridControl gridControl = null;
            GridView view = null;
            string strMessage = "";
            
            switch (i_int_FocusedGrid)
            {
                case 0:     // Email Schedule
                    if (!iBool_AllowDelete)
                        return;
                    break;
                default:
                    if (!iBool_AllowDelete)
                        return;
                    break;
            }
            getCurrentGridControl(out gridControl, out strMessage1, out strMessage2);

            view = (GridView)gridControl.MainView;
            view.PostEditor();
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;

            
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more records to delete.", "No Records To Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Checks passed so delete selected record(s) //
            strMessage = "You have " + (intCount == 1 ? strMessage1 : Convert.ToString(intRowHandles.Length) + strMessage2) +
            " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this record" : "these records") +
            " will no longer be available for selection and any related records will also be deleted!";
            if (XtraMessageBox.Show(strMessage, "Permanently Delete Record(s)", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                frmProgress fProgress = new frmProgress(20);
                fProgress.UpdateCaption("Deleting...");
                fProgress.Show();
                Application.DoEvents();

                if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                this.RefreshGridViewStateEmailSchedule.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                this.RefreshGridViewStateEmailSchedule.SaveViewInfo();  // Store Grid View State //
                try
                {
                    switch (i_int_FocusedGrid)
                    {
                        case 0:     // Email Schedule record
                            sp_AS_11157_Email_Schedule_ItemTableAdapter.Delete("delete", strRecordIDs);
                            break;
                    }
                }
                catch (SqlException ex)
                {
                    XtraMessageBox.Show(ex.Message, "Error deleting", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(ex.Message, "Error deleting", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                Load_Data();

                if (fProgress != null)
                {
                    fProgress.UpdateProgress(20); // Update Progress Bar //
                    fProgress.Close();
                    fProgress = null;
                }
                if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void Edit_Record()
        {
            if (!iBool_AllowEdit)
                return;
            XtraMessageBox.Show("Add/edit is restricted on this manager screen. Please use the Keeper manager to schedule Bulk Email.", "Add/Edit Email Schedule Record(s) Restriction", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            //OpenEditForm(FormMode.edit, "frm_AS_Email_Schedule_Manager");
        }

        private void Add_Record()
        {
            if (!iBool_AllowAdd)
                return;
            XtraMessageBox.Show("Add/edit is restricted on this manager screen. Please use the Keeper manager to schedule Bulk Email.", "Add/Edit Email Schedule Record(s) Restriction", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            //OpenEditForm(FormMode.add, "frm_AS_Email_Schedule_Manager");
        }

        private void OpenEditForm(FormMode mode, string frmCaller)
        {
            if (mode == FormMode.block_add || mode == FormMode.block_edit)
            {
                XtraMessageBox.Show("block add/edit is restricted for this manager screen", "Edit Email Schedule Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            try
            {
                GridView view = (GridView)emailScheduleGridControl.MainView; ;
                frmProgress fProgress = null;
                System.Reflection.MethodInfo method = null;
                string strRecordsToLoad = "";
                DateTime dtDateToLoad = getCurrentDate();
               
                int intCount = 0;

                int[] intRowHandles = view.GetSelectedRows();

                intCount = intRowHandles.Length;
                if (mode == FormMode.edit || mode == FormMode.block_edit)
                {
                    if (intCount != 1)
                    {
                        XtraMessageBox.Show("Select one task to edit before proceeding.", "Edit Email Schedule Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    foreach (int intRowHandle in intRowHandles)
                    {
                        DataRow dr = view.GetDataRow(intRowHandle);
                        strRecordsToLoad += (((DataSet_AS_DataEntry.sp_AS_11157_Email_Schedule_ItemRow)(dr)).EmailScheduleID).ToString() + ',';
                        dtDateToLoad = ((DataSet_AS_DataEntry.sp_AS_11157_Email_Schedule_ItemRow)(dr)).EmailDate;
                    }
                }
               
                storeGridViewState();

                frm_AS_Email_Schedule_Edit dChildForm = new frm_AS_Email_Schedule_Edit();
                dChildForm.GlobalSettings = this.GlobalSettings;
                dChildForm.strRecordIDs = strRecordsToLoad;
                dChildForm.strFormMode = mode.ToString();
                dChildForm.dtPassedEmailDate = dtDateToLoad;
                dChildForm.formMode = (frm_AS_Email_Schedule_Edit.FormMode)mode;
                dChildForm.strCaller = frmCaller;
                dChildForm.intRecordCount = intCount;
                dChildForm.FormPermissions = this.FormPermissions;
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager5 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                dChildForm.splashScreenManager = splashScreenManager5;
                dChildForm.splashScreenManager.ShowWaitForm();
                dChildForm.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to load the form.", "Error Loading Form", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    private void CheckChangedTable(DataTable dt)
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                switch (dt.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }

            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                emailScheduleChanged = true;
            }
        }
     
        private string SaveChangesExtracted(out bool shouldReturn)
        {
            shouldReturn = false;

            if (dxErrorProvider.HasErrors)
            {
                //string strErrors = GetInvalidDataEntryValues(dxErrorProviderSupplier, equipmentDataLayoutControl);
                //DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors +
                //"Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                shouldReturn = true;
                return "Error";
            }

            return String.Empty;
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            if (strFormMode.ToLower() == "add")
            {
                // add
            }
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //         
            EndEdit();
            this.Validate();
            bool shouldReturn;
            string result = SaveChangesExtracted(out shouldReturn);
            if (shouldReturn)
                return result;

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            EndEdit();

            CheckChangedTable(this.dataSet_AS_DataEntry.sp_AS_11157_Email_Schedule_Item);//Supplier Check
            try
            {
                // Insert and Update queries defined in Table Adapter //
                if (emailScheduleChanged)
                {
                    this.sp_AS_11157_Email_Schedule_ItemTableAdapter.Update(dataSet_AS_DataEntry);
                    emailScheduleChanged = false;
                }
               
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            //// If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            //string strNewIDs = "";
            ////if (this.strFormMode.ToLower() == "add")
            ////{
            //DataRowView currentRow = (DataRowView)sp_AS_11002_Equipment_ItemBindingSource.Current;
            //if (currentRow != null) strNewIDs = Convert.ToInt32(currentRow["EquipmentID"]) + ";";

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private void EndEdit()
        {
            spAS11157EmailScheduleItemBindingSource.EndEdit();
        }

        private string CheckForPendingSave()
        {
            EndEdit();

            string strMessage = "";
            string strMessageEmailSchedule = CheckTablePendingSave(this.dataSet_AS_DataEntry.sp_AS_11157_Email_Schedule_Item, " on the Depreciation Setting Form ");//Dep Setting Check

            if (strMessageEmailSchedule != "")
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (strMessageEmailSchedule != "") strMessage += strMessageEmailSchedule;
            }
            return strMessage;
        }

        private string CheckTablePendingSave(DataTable dt, string formArea)
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                switch (dt.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";

            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)" + formArea + "\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)" + formArea + "\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)" + formArea + "\n";
            }
            return strMessage;
        }
                
        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;

            bbiSave.Enabled = false;
            bbiCancel.Enabled = true;
            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = null;
            getCurrentView(out view);

            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();

            toggleMenuButtons(alItems, intRowHandles);

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) 
                frmParent.PermissionsHandler(alItems);

            // Set enabled status of emailScheduleView navigator custom buttons //
            setCustomNavigatorAccess(emailScheduleGridControl, view);
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                }
            }
        }

        private void setCustomNavigatorAccess(GridControl xGridControl, GridView view)
        {
            view = (GridView)xGridControl.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (iBool_AllowAdd)
            {
                xGridControl.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                xGridControl.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEdit)
            {
                xGridControl.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                xGridControl.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                xGridControl.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                xGridControl.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }
        }

        private void toggleMenuButtons(ArrayList alItems, int[] intRowHandles)
        {
            switch (i_int_FocusedGrid)
            {
                case 0://Dep setting

                    if (iBool_AllowAdd)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;
                        bbiBlockAdd.Enabled = false;
                    }

                    if (iBool_AllowEdit && intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                        if (intRowHandles.Length >= 2)
                        {
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                    }
                    if (iBool_AllowDelete && intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    break;
            }
        }
        
        private void getCurrentView(out GridView view)
        {
            switch (i_int_FocusedGrid)
            {
                case 0://Dep  setting
                    view = (GridView)emailScheduleGridControl.MainView;
                    break;
                default:
                    view = (GridView)emailScheduleGridControl.MainView;
                    break;
            }

        }

        private void LoadConnectionStrings(string ConnString)
        {
            strConnectionString = ConnString;
            sp_AS_11157_Email_Schedule_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
        }

        private void LoadAdapters()
        {
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, FormID, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);
            sp_AS_11157_Email_Schedule_ItemTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11157_Email_Schedule_Item, "", strFormMode);
        }

        public override void PostOpen(object objParameter)
        {
            //Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            //LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            //this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            //if (fProgress != null)
            //{
            //    fProgress.UpdateProgress(10); // Update Progress Bar //
            //    fProgress.Close();
            //    fProgress = null;
            //}
            //bool_FormLoading = false;

            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
            if (strPassedInRecordDs == "")  // Not opened in drill-down mode so load last saved screen settings for current user //
            {
                Application.DoEvents();  // Allow Form time to repaint itself //
                //LoadLastSavedUserScreenSettings();
            }
            bool_FormLoading = false;
        }

        #endregion

        private void bbiCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void emailScheduleGridView_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void emailScheduleGridView_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
        }

        private void ceIsCurrentTask_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (!ce.Checked)
            {
                e.Cancel = false;
                string strRecordSelected = "";
                DataRowView currentRow = (DataRowView)spAS11157EmailScheduleItemBindingSource.Current;
                if (currentRow != null)
                {
                   // currentRow["SetAsDefault"] = true;
                    strRecordSelected = currentRow["EmailScheduleID"].ToString();
                }


                this.sp_AS_11157_Email_Schedule_ItemTableAdapter.Update("edit", strRecordSelected, Convert.ToInt32(currentRow["EmailScheduleID"]), Convert.ToDateTime(currentRow["EmailDate"]), Convert.ToInt32(currentRow["TaskType"]), Convert.ToInt32(currentRow["TaskStatus"]), true);
                UpdateRefreshStatus = 1;
                i_str_AddedRecordIDs1 = strRecordSelected;

                frmActivated();
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void emailScheduleGridView_RowStyle(object sender, RowStyleEventArgs e)
        {

        }

        private void emailScheduleGridView_ShowingEditor(object sender, CancelEventArgs e)
        {
            GridView view = sender as GridView;
            if (view.FocusedColumn.FieldName != "IsCurrentTask")
                return;
            DateTime dtEmailDate = getCurrentDate();
            string cellValue = view.GetFocusedRowCellValue(view.Columns["EmailDate"]).ToString();
            if (DateTime.TryParse(cellValue, out dtEmailDate))
            {
                if (dtEmailDate < getCurrentDate())
                {
                    e.Cancel = true;
                }
                else
                {
                    e.Cancel = false;
                }
            }
            else 
            {
                e.Cancel = true;
            }
            

        }
        private DateTime getCurrentDate()
        {
            DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter GetDate = new DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter();
            GetDate.ChangeConnectionString(strConnectionString);
            DateTime d = DateTime.Parse(GetDate.sp_AS_11156_Get_Server_Time().ToString());
            return d;
        }

        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Load_Data();
        }

        private void bbiExcelExport_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ExportGrid("Excel");
        }

        private void bbiExportOldExcel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            ExportGrid("ExcelOld");
        }

        private void ExportGrid(string strFormat)
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog();

            folderDlg.ShowNewFolderButton = true;

            DialogResult result = folderDlg.ShowDialog();

            Environment.SpecialFolder root = folderDlg.RootFolder;
            string FileName;
            GridControl Grid_To_Export = emailScheduleGridControl;

            if (result == DialogResult.OK)
            {
                try
                {
                    if (strFormat == "Excel")
                    {
                        FileName = folderDlg.SelectedPath + "\\" + Grid_To_Export.Tag + "_" + getCurrentDate().ToString("yyyy-MM-dd_HH_mm_ss") + ".xlsx";
                        Grid_To_Export.ExportToXlsx((FileName));
                    }
                    else if (strFormat == "ExcelOld")
                    {
                        FileName = folderDlg.SelectedPath + "\\" + Grid_To_Export.Tag + "_" + getCurrentDate().ToString("yyyy-MM-dd_HH_mm_ss") + ".xls";
                        Grid_To_Export.ExportToXls((FileName));
                    }

                }
                catch (Exception)
                {
                    XtraMessageBox.Show("Please contact ICT to upgrade or install a compatible  and enable the feature", "Compatibility Issue", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

        }


    }
}

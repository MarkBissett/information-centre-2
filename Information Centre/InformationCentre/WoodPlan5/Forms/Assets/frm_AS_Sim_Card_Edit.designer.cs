﻿namespace WoodPlan5
{
    partial class frm_AS_Sim_Card_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AS_Sim_Card_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.tableAdapterManager = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager();
            this.barStatus = new DevExpress.XtraBars.Bar();
            this.bsiFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.editFormLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.editFormDataNavigator = new DevExpress.XtraEditors.DataNavigator();
            this.spAS11117SimCardItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.editFormDataLayoutControlGroup = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.lueHasBeenSwappedID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLHasBeenSwappedBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.txtSimNumber = new DevExpress.XtraEditors.TextEdit();
            this.txtProviderCustomerReference = new DevExpress.XtraEditors.TextEdit();
            this.txtMobileNumber = new DevExpress.XtraEditors.TextEdit();
            this.lueSimStatusID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLSimCardStatusBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.deConnectionDate = new DevExpress.XtraEditors.DateEdit();
            this.deDisconnectionDate = new DevExpress.XtraEditors.DateEdit();
            this.memNotes = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSimNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForProviderCustomerReference = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMobileNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSimStatusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForConnectionDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForNotes = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDisconnectionDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp_AS_11117_Sim_Card_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11117_Sim_Card_ItemTableAdapter();
            this.sp_AS_11000_PL_Sim_Card_StatusTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Sim_Card_StatusTableAdapter();
            this.sp_AS_11000_PL_Has_Been_SwappedTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Has_Been_SwappedTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11117SimCardItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).BeginInit();
            this.editFormDataLayoutControlGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lueHasBeenSwappedID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLHasBeenSwappedBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSimNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProviderCustomerReference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMobileNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSimStatusID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLSimCardStatusBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deConnectionDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deConnectionDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDisconnectionDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDisconnectionDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memNotes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSimNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForProviderCustomerReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMobileNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSimStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForConnectionDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNotes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDisconnectionDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(834, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 491);
            this.barDockControlBottom.Size = new System.Drawing.Size(834, 30);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 465);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(834, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 465);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barStatus,
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.bsiFormMode,
            this.barStaticItem1,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemChangesPending,
            this.barStaticItemInformation});
            this.barManager1.MaxItemId = 35;
            this.barManager1.StatusBar = this.barStatus;
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "dataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.sp_AS_11002_Equipment_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11005_Vehicle_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11008_Plant_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11011_Equipment_Category_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11014_Make_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11017_Model_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11020_Supplier_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Tracker_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Verilocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11026_Gadget_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11029_Office_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11032_Hardware_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11035_Software_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11041_Transaction_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11047_Equipment_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11050_Depreciation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Interval_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11057_Company_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11060_Department_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11063_Cost_Centre_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11066_Depreciation_Settings_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11075_Cover_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11078_Work_Detail_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11081_Incident_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11084_Purpose_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11087_Equipment_Category_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11089_Fuel_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11093_Initial_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11096_Road_Tax_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11099_Improvement_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11102_Rental_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11105_Speeding_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11108_Rental_Details_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11112_Keeper_Email_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11117_Sim_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11120_Additional_FollowUps_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11123_Sim_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_AlertTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11129_Software_Assign_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11133_Picklist_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_AS_11141_Hardware_Specifications_ItemTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // barStatus
            // 
            this.barStatus.BarName = "Status Bar";
            this.barStatus.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.barStatus.DockCol = 0;
            this.barStatus.DockRow = 0;
            this.barStatus.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.barStatus.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiFormMode, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barStaticItemInformation, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barStatus.OptionsBar.AllowQuickCustomization = false;
            this.barStatus.OptionsBar.DrawDragBorder = false;
            this.barStatus.OptionsBar.UseWholeRow = true;
            this.barStatus.Text = "Status bar";
            // 
            // bsiFormMode
            // 
            this.bsiFormMode.Caption = "Form Mode: Editing";
            this.bsiFormMode.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.Glyph")));
            this.bsiFormMode.Id = 29;
            this.bsiFormMode.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.LargeGlyph")));
            this.bsiFormMode.Name = "bsiFormMode";
            toolTipTitleItem1.Text = "Form Mode - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bsiFormMode.SuperTip = superToolTip1;
            this.bsiFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 31;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 32;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.Glyph")));
            this.barStaticItemInformation.Id = 33;
            this.barStaticItemInformation.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.LargeGlyph")));
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 27;
            this.bbiFormSave.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.LargeGlyph")));
            this.bbiFormSave.Name = "bbiFormSave";
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Text = "Save Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormSave.SuperTip = superToolTip2;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.Glyph")));
            this.bbiFormCancel.Id = 28;
            this.bbiFormCancel.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.LargeGlyph")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Text = "Cancel Button - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiFormCancel.SuperTip = superToolTip3;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "barStaticItem1";
            this.barStaticItem1.Id = 30;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // editFormLayoutControlGroup
            // 
            this.editFormLayoutControlGroup.CustomizationFormText = "Main Layout Control Group";
            this.editFormLayoutControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.editFormLayoutControlGroup.GroupBordersVisible = false;
            this.editFormLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlGroup1,
            this.emptySpaceItem1,
            this.emptySpaceItem2});
            this.editFormLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.editFormLayoutControlGroup.Name = "Root";
            this.editFormLayoutControlGroup.Size = new System.Drawing.Size(834, 465);
            this.editFormLayoutControlGroup.Text = "Root";
            this.editFormLayoutControlGroup.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.editFormDataNavigator;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(161, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(262, 23);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // editFormDataNavigator
            // 
            this.editFormDataNavigator.Buttons.Append.Visible = false;
            this.editFormDataNavigator.Buttons.CancelEdit.Visible = false;
            this.editFormDataNavigator.Buttons.EndEdit.Visible = false;
            this.editFormDataNavigator.Buttons.Remove.Visible = false;
            this.editFormDataNavigator.DataSource = this.spAS11117SimCardItemBindingSource;
            this.editFormDataNavigator.Location = new System.Drawing.Point(173, 12);
            this.editFormDataNavigator.Name = "editFormDataNavigator";
            this.editFormDataNavigator.Size = new System.Drawing.Size(258, 19);
            this.editFormDataNavigator.StyleController = this.editFormDataLayoutControlGroup;
            this.editFormDataNavigator.TabIndex = 122;
            this.editFormDataNavigator.Text = "depreciationDataNavigator";
            this.editFormDataNavigator.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            // 
            // spAS11117SimCardItemBindingSource
            // 
            this.spAS11117SimCardItemBindingSource.DataMember = "sp_AS_11117_Sim_Card_Item";
            this.spAS11117SimCardItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // editFormDataLayoutControlGroup
            // 
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueHasBeenSwappedID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.editFormDataNavigator);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtSimNumber);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtProviderCustomerReference);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtMobileNumber);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueSimStatusID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.deConnectionDate);
            this.editFormDataLayoutControlGroup.Controls.Add(this.deDisconnectionDate);
            this.editFormDataLayoutControlGroup.Controls.Add(this.memNotes);
            this.editFormDataLayoutControlGroup.DataSource = this.spAS11117SimCardItemBindingSource;
            this.editFormDataLayoutControlGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editFormDataLayoutControlGroup.Location = new System.Drawing.Point(0, 26);
            this.editFormDataLayoutControlGroup.Name = "editFormDataLayoutControlGroup";
            this.editFormDataLayoutControlGroup.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1935, 238, 250, 350);
            this.editFormDataLayoutControlGroup.Root = this.editFormLayoutControlGroup;
            this.editFormDataLayoutControlGroup.Size = new System.Drawing.Size(834, 465);
            this.editFormDataLayoutControlGroup.TabIndex = 4;
            this.editFormDataLayoutControlGroup.Text = "dataLayoutControl1";
            // 
            // lueHasBeenSwappedID
            // 
            this.lueHasBeenSwappedID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11117SimCardItemBindingSource, "HasBeenSwappedID", true));
            this.lueHasBeenSwappedID.Location = new System.Drawing.Point(566, 107);
            this.lueHasBeenSwappedID.MenuManager = this.barManager1;
            this.lueHasBeenSwappedID.Name = "lueHasBeenSwappedID";
            this.lueHasBeenSwappedID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueHasBeenSwappedID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Has Been Swapped", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueHasBeenSwappedID.Properties.DataSource = this.spAS11000PLHasBeenSwappedBindingSource;
            this.lueHasBeenSwappedID.Properties.DisplayMember = "Value";
            this.lueHasBeenSwappedID.Properties.NullText = "";
            this.lueHasBeenSwappedID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueHasBeenSwappedID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueHasBeenSwappedID.Properties.ValueMember = "PickListID";
            this.lueHasBeenSwappedID.Size = new System.Drawing.Size(256, 20);
            this.lueHasBeenSwappedID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueHasBeenSwappedID.TabIndex = 131;
            this.lueHasBeenSwappedID.Validating += new System.ComponentModel.CancelEventHandler(this.lookupEdit_Validating);
            // 
            // spAS11000PLHasBeenSwappedBindingSource
            // 
            this.spAS11000PLHasBeenSwappedBindingSource.DataMember = "sp_AS_11000_PL_Has_Been_Swapped";
            this.spAS11000PLHasBeenSwappedBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // txtSimNumber
            // 
            this.txtSimNumber.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11117SimCardItemBindingSource, "SimNumber", true));
            this.txtSimNumber.Location = new System.Drawing.Point(164, 35);
            this.txtSimNumber.MenuManager = this.barManager1;
            this.txtSimNumber.Name = "txtSimNumber";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtSimNumber, true);
            this.txtSimNumber.Size = new System.Drawing.Size(658, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtSimNumber, optionsSpelling1);
            this.txtSimNumber.StyleController = this.editFormDataLayoutControlGroup;
            this.txtSimNumber.TabIndex = 123;
            this.txtSimNumber.Validating += new System.ComponentModel.CancelEventHandler(this.textEdit_Validating);
            // 
            // txtProviderCustomerReference
            // 
            this.txtProviderCustomerReference.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11117SimCardItemBindingSource, "ProviderCustomerReference", true));
            this.txtProviderCustomerReference.Location = new System.Drawing.Point(164, 59);
            this.txtProviderCustomerReference.MenuManager = this.barManager1;
            this.txtProviderCustomerReference.Name = "txtProviderCustomerReference";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtProviderCustomerReference, true);
            this.txtProviderCustomerReference.Size = new System.Drawing.Size(658, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtProviderCustomerReference, optionsSpelling2);
            this.txtProviderCustomerReference.StyleController = this.editFormDataLayoutControlGroup;
            this.txtProviderCustomerReference.TabIndex = 124;
            this.txtProviderCustomerReference.Validating += new System.ComponentModel.CancelEventHandler(this.textEdit_Validating);
            // 
            // txtMobileNumber
            // 
            this.txtMobileNumber.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11117SimCardItemBindingSource, "MobileNumber", true));
            this.txtMobileNumber.Location = new System.Drawing.Point(164, 83);
            this.txtMobileNumber.MenuManager = this.barManager1;
            this.txtMobileNumber.Name = "txtMobileNumber";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtMobileNumber, true);
            this.txtMobileNumber.Size = new System.Drawing.Size(658, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtMobileNumber, optionsSpelling3);
            this.txtMobileNumber.StyleController = this.editFormDataLayoutControlGroup;
            this.txtMobileNumber.TabIndex = 125;
            this.txtMobileNumber.Validating += new System.ComponentModel.CancelEventHandler(this.textEdit_Validating);
            // 
            // lueSimStatusID
            // 
            this.lueSimStatusID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11117SimCardItemBindingSource, "SimStatusID", true));
            this.lueSimStatusID.Location = new System.Drawing.Point(164, 107);
            this.lueSimStatusID.MenuManager = this.barManager1;
            this.lueSimStatusID.Name = "lueSimStatusID";
            this.lueSimStatusID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueSimStatusID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Sim Card Status", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueSimStatusID.Properties.DataSource = this.spAS11000PLSimCardStatusBindingSource;
            this.lueSimStatusID.Properties.DisplayMember = "Value";
            this.lueSimStatusID.Properties.NullText = "";
            this.lueSimStatusID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueSimStatusID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueSimStatusID.Properties.ValueMember = "PickListID";
            this.lueSimStatusID.Size = new System.Drawing.Size(246, 20);
            this.lueSimStatusID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueSimStatusID.TabIndex = 126;
            this.lueSimStatusID.Tag = "Sim Status";
            this.lueSimStatusID.EditValueChanged += new System.EventHandler(this.lueSimStatusID_EditValueChanged);
            this.lueSimStatusID.Validating += new System.ComponentModel.CancelEventHandler(this.lookupEdit_Validating);
            // 
            // spAS11000PLSimCardStatusBindingSource
            // 
            this.spAS11000PLSimCardStatusBindingSource.DataMember = "sp_AS_11000_PL_Sim_Card_Status";
            this.spAS11000PLSimCardStatusBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // deConnectionDate
            // 
            this.deConnectionDate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11117SimCardItemBindingSource, "ConnectionDate", true));
            this.deConnectionDate.EditValue = null;
            this.deConnectionDate.Location = new System.Drawing.Point(164, 131);
            this.deConnectionDate.MenuManager = this.barManager1;
            this.deConnectionDate.Name = "deConnectionDate";
            this.deConnectionDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deConnectionDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deConnectionDate.Size = new System.Drawing.Size(246, 20);
            this.deConnectionDate.StyleController = this.editFormDataLayoutControlGroup;
            this.deConnectionDate.TabIndex = 127;
            this.deConnectionDate.Tag = "Connection Date";
            this.deConnectionDate.Validating += new System.ComponentModel.CancelEventHandler(this.dateEdit_Validating);
            // 
            // deDisconnectionDate
            // 
            this.deDisconnectionDate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11117SimCardItemBindingSource, "DisconnectionDate", true));
            this.deDisconnectionDate.EditValue = null;
            this.deDisconnectionDate.Location = new System.Drawing.Point(566, 131);
            this.deDisconnectionDate.MenuManager = this.barManager1;
            this.deDisconnectionDate.Name = "deDisconnectionDate";
            this.deDisconnectionDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deDisconnectionDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deDisconnectionDate.Size = new System.Drawing.Size(256, 20);
            this.deDisconnectionDate.StyleController = this.editFormDataLayoutControlGroup;
            this.deDisconnectionDate.TabIndex = 128;
            this.deDisconnectionDate.Tag = "Disconnection Date";
            this.deDisconnectionDate.Validating += new System.ComponentModel.CancelEventHandler(this.dateEdit_Validating);
            // 
            // memNotes
            // 
            this.memNotes.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11117SimCardItemBindingSource, "Notes", true));
            this.memNotes.Location = new System.Drawing.Point(24, 191);
            this.memNotes.MenuManager = this.barManager1;
            this.memNotes.Name = "memNotes";
            this.scSpellChecker.SetShowSpellCheckMenu(this.memNotes, true);
            this.memNotes.Size = new System.Drawing.Size(786, 250);
            this.scSpellChecker.SetSpellCheckerOptions(this.memNotes, optionsSpelling4);
            this.memNotes.StyleController = this.editFormDataLayoutControlGroup;
            this.memNotes.TabIndex = 130;
            this.memNotes.UseOptimizedRendering = true;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AllowDrawBackground = false;
            this.layoutControlGroup1.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSimNumber,
            this.ItemForProviderCustomerReference,
            this.ItemForMobileNumber,
            this.ItemForSimStatusID,
            this.ItemForConnectionDate,
            this.tabbedControlGroup1,
            this.ItemForDisconnectionDate,
            this.layoutControlItem3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 23);
            this.layoutControlGroup1.Name = "autoGeneratedGroup0";
            this.layoutControlGroup1.Size = new System.Drawing.Size(814, 422);
            this.layoutControlGroup1.Text = "autoGeneratedGroup0";
            // 
            // ItemForSimNumber
            // 
            this.ItemForSimNumber.Control = this.txtSimNumber;
            this.ItemForSimNumber.CustomizationFormText = "Sim Number :";
            this.ItemForSimNumber.Location = new System.Drawing.Point(0, 0);
            this.ItemForSimNumber.Name = "ItemForSimNumber";
            this.ItemForSimNumber.Size = new System.Drawing.Size(814, 24);
            this.ItemForSimNumber.Text = "Sim Number :";
            this.ItemForSimNumber.TextSize = new System.Drawing.Size(149, 13);
            // 
            // ItemForProviderCustomerReference
            // 
            this.ItemForProviderCustomerReference.Control = this.txtProviderCustomerReference;
            this.ItemForProviderCustomerReference.CustomizationFormText = "Provider Customer Reference :";
            this.ItemForProviderCustomerReference.Location = new System.Drawing.Point(0, 24);
            this.ItemForProviderCustomerReference.Name = "ItemForProviderCustomerReference";
            this.ItemForProviderCustomerReference.Size = new System.Drawing.Size(814, 24);
            this.ItemForProviderCustomerReference.Text = "Provider Customer Reference :";
            this.ItemForProviderCustomerReference.TextSize = new System.Drawing.Size(149, 13);
            // 
            // ItemForMobileNumber
            // 
            this.ItemForMobileNumber.Control = this.txtMobileNumber;
            this.ItemForMobileNumber.CustomizationFormText = "Mobile Number :";
            this.ItemForMobileNumber.Location = new System.Drawing.Point(0, 48);
            this.ItemForMobileNumber.Name = "ItemForMobileNumber";
            this.ItemForMobileNumber.Size = new System.Drawing.Size(814, 24);
            this.ItemForMobileNumber.Text = "Mobile Number :";
            this.ItemForMobileNumber.TextSize = new System.Drawing.Size(149, 13);
            // 
            // ItemForSimStatusID
            // 
            this.ItemForSimStatusID.Control = this.lueSimStatusID;
            this.ItemForSimStatusID.CustomizationFormText = "Sim Status :";
            this.ItemForSimStatusID.Location = new System.Drawing.Point(0, 72);
            this.ItemForSimStatusID.Name = "ItemForSimStatusID";
            this.ItemForSimStatusID.Size = new System.Drawing.Size(402, 24);
            this.ItemForSimStatusID.Text = "Sim Status :";
            this.ItemForSimStatusID.TextSize = new System.Drawing.Size(149, 13);
            // 
            // ItemForConnectionDate
            // 
            this.ItemForConnectionDate.Control = this.deConnectionDate;
            this.ItemForConnectionDate.CustomizationFormText = "Connection Date :";
            this.ItemForConnectionDate.Location = new System.Drawing.Point(0, 96);
            this.ItemForConnectionDate.Name = "ItemForConnectionDate";
            this.ItemForConnectionDate.Size = new System.Drawing.Size(402, 24);
            this.ItemForConnectionDate.Text = "Connection Date :";
            this.ItemForConnectionDate.TextSize = new System.Drawing.Size(149, 13);
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 120);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup2;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(814, 302);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.tabbedControlGroup1.Text = "tabbedControlGroup1";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup2.CaptionImage")));
            this.layoutControlGroup2.CustomizationFormText = "Notes";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForNotes});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(790, 254);
            this.layoutControlGroup2.Text = "Notes";
            // 
            // ItemForNotes
            // 
            this.ItemForNotes.Control = this.memNotes;
            this.ItemForNotes.CustomizationFormText = "Notes";
            this.ItemForNotes.Location = new System.Drawing.Point(0, 0);
            this.ItemForNotes.Name = "ItemForNotes";
            this.ItemForNotes.Size = new System.Drawing.Size(790, 254);
            this.ItemForNotes.Text = "Notes";
            this.ItemForNotes.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForNotes.TextToControlDistance = 0;
            this.ItemForNotes.TextVisible = false;
            // 
            // ItemForDisconnectionDate
            // 
            this.ItemForDisconnectionDate.Control = this.deDisconnectionDate;
            this.ItemForDisconnectionDate.CustomizationFormText = "Disconnection Date :";
            this.ItemForDisconnectionDate.Location = new System.Drawing.Point(402, 96);
            this.ItemForDisconnectionDate.Name = "ItemForDisconnectionDate";
            this.ItemForDisconnectionDate.Size = new System.Drawing.Size(412, 24);
            this.ItemForDisconnectionDate.Text = "Disconnection Date :";
            this.ItemForDisconnectionDate.TextSize = new System.Drawing.Size(149, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.lueHasBeenSwappedID;
            this.layoutControlItem3.CustomizationFormText = "Has Been Swapped :";
            this.layoutControlItem3.Location = new System.Drawing.Point(402, 72);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(412, 24);
            this.layoutControlItem3.Text = "Has Been Swapped :";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(149, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(161, 23);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(423, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(391, 23);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp_AS_11117_Sim_Card_ItemTableAdapter
            // 
            this.sp_AS_11117_Sim_Card_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Sim_Card_StatusTableAdapter
            // 
            this.sp_AS_11000_PL_Sim_Card_StatusTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Has_Been_SwappedTableAdapter
            // 
            this.sp_AS_11000_PL_Has_Been_SwappedTableAdapter.ClearBeforeFill = true;
            // 
            // frm_AS_Sim_Card_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(834, 521);
            this.Controls.Add(this.editFormDataLayoutControlGroup);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AS_Sim_Card_Edit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Manage Sim Card Details";
            this.Activated += new System.EventHandler(this.frm_AS_Sim_Card_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_AS_Sim_Card_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_AS_Sim_Card_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.editFormDataLayoutControlGroup, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11117SimCardItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).EndInit();
            this.editFormDataLayoutControlGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lueHasBeenSwappedID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLHasBeenSwappedBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSimNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProviderCustomerReference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMobileNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSimStatusID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLSimCardStatusBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deConnectionDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deConnectionDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDisconnectionDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDisconnectionDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memNotes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSimNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForProviderCustomerReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMobileNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSimStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForConnectionDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNotes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDisconnectionDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private DataSet_AS_DataEntryTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraBars.Bar barStatus;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.BarStaticItem bsiFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private DevExpress.XtraDataLayout.DataLayoutControl editFormDataLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlGroup editFormLayoutControlGroup;
        private DevExpress.XtraEditors.DataNavigator editFormDataNavigator;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private System.Windows.Forms.BindingSource spAS11117SimCardItemBindingSource;
        private DevExpress.XtraEditors.TextEdit txtSimNumber;
        private DevExpress.XtraEditors.TextEdit txtProviderCustomerReference;
        private DevExpress.XtraEditors.TextEdit txtMobileNumber;
        private DevExpress.XtraEditors.LookUpEdit lueSimStatusID;
        private DevExpress.XtraEditors.DateEdit deConnectionDate;
        private DevExpress.XtraEditors.DateEdit deDisconnectionDate;
        private DevExpress.XtraEditors.MemoEdit memNotes;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSimNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForProviderCustomerReference;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMobileNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSimStatusID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForConnectionDate;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNotes;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDisconnectionDate;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11117_Sim_Card_ItemTableAdapter sp_AS_11117_Sim_Card_ItemTableAdapter;
        private System.Windows.Forms.BindingSource spAS11000PLSimCardStatusBindingSource;
        private System.Windows.Forms.BindingSource spAS11000PLHasBeenSwappedBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Sim_Card_StatusTableAdapter sp_AS_11000_PL_Sim_Card_StatusTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Has_Been_SwappedTableAdapter sp_AS_11000_PL_Has_Been_SwappedTableAdapter;
        private DevExpress.XtraEditors.LookUpEdit lueHasBeenSwappedID;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;


    }
}

﻿namespace WoodPlan5
{
    partial class frm_AS_Cover_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AS_Cover_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.tableAdapterManager = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager();
            this.barStatus = new DevExpress.XtraBars.Bar();
            this.bsiFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.editFormLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.editFormDataNavigator = new DevExpress.XtraEditors.DataNavigator();
            this.spAS11075CoverItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.editFormDataLayoutControlGroup = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.txtEquipmentReference = new DevExpress.XtraEditors.TextEdit();
            this.lueCoverTypeID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLCoverTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lueSupplierID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11020SupplierListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.txtPolicy = new DevExpress.XtraEditors.TextEdit();
            this.AnnualCoverCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.deRenewalDate = new DevExpress.XtraEditors.DateEdit();
            this.NotesMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.spnExcess = new DevExpress.XtraEditors.SpinEdit();
            this.deStartDate = new DevExpress.XtraEditors.DateEdit();
            this.deEndDate = new DevExpress.XtraEditors.DateEdit();
            this.lueCoverStatusID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLCoverStatusBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForGroundControlReference = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCoverTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPolicy = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAnnualCoverCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRenewalDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForExcess = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSupplierID = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForNotes = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStartDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEndDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCoverStatusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp_AS_11075_Cover_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11075_Cover_ItemTableAdapter();
            this.sp_AS_11020_Supplier_ListTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11020_Supplier_ListTableAdapter();
            this.sp_AS_11000_PL_Cover_TypeTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Cover_TypeTableAdapter();
            this.sp_AS_11000_PL_Cover_StatusTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Cover_StatusTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11075CoverItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).BeginInit();
            this.editFormDataLayoutControlGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentReference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueCoverTypeID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLCoverTypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSupplierID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11020SupplierListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPolicy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AnnualCoverCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deRenewalDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deRenewalDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnExcess.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStartDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStartDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueCoverStatusID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLCoverStatusBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGroundControlReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCoverTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPolicy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAnnualCoverCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRenewalDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSupplierID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNotes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCoverStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1335, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 653);
            this.barDockControlBottom.Size = new System.Drawing.Size(1335, 30);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 627);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1335, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 627);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barStatus,
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.bsiFormMode,
            this.barStaticItem1,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemChangesPending,
            this.barStaticItemInformation});
            this.barManager1.MaxItemId = 35;
            this.barManager1.StatusBar = this.barStatus;
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "dataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.sp_AS_11002_Equipment_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11005_Vehicle_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11008_Plant_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11011_Equipment_Category_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11014_Make_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11017_Model_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11020_Supplier_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Tracker_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Verilocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11026_Gadget_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11029_Office_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11032_Hardware_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11035_Software_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11041_Transaction_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11047_Equipment_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11050_Depreciation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Data_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Interval_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11057_Company_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11060_Department_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11063_Cost_Centre_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11066_Depreciation_Settings_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11075_Cover_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11078_Work_Detail_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11081_Incident_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11084_Purpose_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11087_Equipment_Category_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11089_Fuel_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11093_Initial_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11096_Road_Tax_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11099_Improvement_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11102_Rental_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11105_Speeding_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11108_Rental_Details_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11112_Keeper_Email_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11117_Sim_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11120_Additional_FollowUps_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11123_Sim_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_AlertTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11129_Software_Assign_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11133_Picklist_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_AS_11141_Hardware_Specifications_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11157_Email_Schedule_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11163_Service_ScheduleTableAdapter = null;
            this.tableAdapterManager.sp_AS_11166_P11D_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11169_NetBookValue_ItemTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // barStatus
            // 
            this.barStatus.BarName = "Status Bar";
            this.barStatus.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.barStatus.DockCol = 0;
            this.barStatus.DockRow = 0;
            this.barStatus.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.barStatus.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiFormMode, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barStaticItemInformation, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barStatus.OptionsBar.AllowQuickCustomization = false;
            this.barStatus.OptionsBar.DrawDragBorder = false;
            this.barStatus.OptionsBar.UseWholeRow = true;
            this.barStatus.Text = "Status bar";
            // 
            // bsiFormMode
            // 
            this.bsiFormMode.Caption = "Form Mode: Editing";
            this.bsiFormMode.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.Glyph")));
            this.bsiFormMode.Id = 29;
            this.bsiFormMode.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.LargeGlyph")));
            this.bsiFormMode.Name = "bsiFormMode";
            toolTipTitleItem1.Text = "Form Mode - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bsiFormMode.SuperTip = superToolTip1;
            this.bsiFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 31;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 32;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.Glyph")));
            this.barStaticItemInformation.Id = 33;
            this.barStaticItemInformation.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.LargeGlyph")));
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 27;
            this.bbiFormSave.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.LargeGlyph")));
            this.bbiFormSave.Name = "bbiFormSave";
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Text = "Save Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormSave.SuperTip = superToolTip2;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.Glyph")));
            this.bbiFormCancel.Id = 28;
            this.bbiFormCancel.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.LargeGlyph")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Text = "Cancel Button - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiFormCancel.SuperTip = superToolTip3;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "barStaticItem1";
            this.barStaticItem1.Id = 30;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // editFormLayoutControlGroup
            // 
            this.editFormLayoutControlGroup.CustomizationFormText = "Main Layout Control Group";
            this.editFormLayoutControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.editFormLayoutControlGroup.GroupBordersVisible = false;
            this.editFormLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlGroup1,
            this.emptySpaceItem1,
            this.emptySpaceItem2});
            this.editFormLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.editFormLayoutControlGroup.Name = "Root";
            this.editFormLayoutControlGroup.Size = new System.Drawing.Size(1335, 627);
            this.editFormLayoutControlGroup.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.editFormDataNavigator;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(184, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(263, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // editFormDataNavigator
            // 
            this.editFormDataNavigator.Buttons.Append.Visible = false;
            this.editFormDataNavigator.Buttons.CancelEdit.Visible = false;
            this.editFormDataNavigator.Buttons.EndEdit.Visible = false;
            this.editFormDataNavigator.Buttons.Remove.Visible = false;
            this.editFormDataNavigator.DataSource = this.spAS11075CoverItemBindingSource;
            this.editFormDataNavigator.Location = new System.Drawing.Point(196, 12);
            this.editFormDataNavigator.Name = "editFormDataNavigator";
            this.editFormDataNavigator.Size = new System.Drawing.Size(259, 19);
            this.editFormDataNavigator.StyleController = this.editFormDataLayoutControlGroup;
            this.editFormDataNavigator.TabIndex = 122;
            this.editFormDataNavigator.Text = "depreciationDataNavigator";
            this.editFormDataNavigator.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            // 
            // spAS11075CoverItemBindingSource
            // 
            this.spAS11075CoverItemBindingSource.DataMember = "sp_AS_11075_Cover_Item";
            this.spAS11075CoverItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // editFormDataLayoutControlGroup
            // 
            this.editFormDataLayoutControlGroup.Controls.Add(this.editFormDataNavigator);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtEquipmentReference);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueCoverTypeID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueSupplierID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtPolicy);
            this.editFormDataLayoutControlGroup.Controls.Add(this.AnnualCoverCostSpinEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.deRenewalDate);
            this.editFormDataLayoutControlGroup.Controls.Add(this.NotesMemoEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.spnExcess);
            this.editFormDataLayoutControlGroup.Controls.Add(this.deStartDate);
            this.editFormDataLayoutControlGroup.Controls.Add(this.deEndDate);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueCoverStatusID);
            this.editFormDataLayoutControlGroup.DataSource = this.spAS11075CoverItemBindingSource;
            this.editFormDataLayoutControlGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editFormDataLayoutControlGroup.Location = new System.Drawing.Point(0, 26);
            this.editFormDataLayoutControlGroup.Name = "editFormDataLayoutControlGroup";
            this.editFormDataLayoutControlGroup.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(2244, 194, 250, 350);
            this.editFormDataLayoutControlGroup.Root = this.editFormLayoutControlGroup;
            this.editFormDataLayoutControlGroup.Size = new System.Drawing.Size(1335, 627);
            this.editFormDataLayoutControlGroup.TabIndex = 4;
            this.editFormDataLayoutControlGroup.Text = "dataLayoutControl1";
            // 
            // txtEquipmentReference
            // 
            this.txtEquipmentReference.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11075CoverItemBindingSource, "EquipmentReference", true));
            this.txtEquipmentReference.Location = new System.Drawing.Point(125, 35);
            this.txtEquipmentReference.MenuManager = this.barManager1;
            this.txtEquipmentReference.Name = "txtEquipmentReference";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtEquipmentReference, true);
            this.txtEquipmentReference.Size = new System.Drawing.Size(540, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtEquipmentReference, optionsSpelling1);
            this.txtEquipmentReference.StyleController = this.editFormDataLayoutControlGroup;
            this.txtEquipmentReference.TabIndex = 123;
            // 
            // lueCoverTypeID
            // 
            this.lueCoverTypeID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11075CoverItemBindingSource, "CoverTypeID", true));
            this.lueCoverTypeID.Location = new System.Drawing.Point(782, 59);
            this.lueCoverTypeID.MenuManager = this.barManager1;
            this.lueCoverTypeID.Name = "lueCoverTypeID";
            this.lueCoverTypeID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueCoverTypeID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Cover Type", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueCoverTypeID.Properties.DataSource = this.spAS11000PLCoverTypeBindingSource;
            this.lueCoverTypeID.Properties.DisplayMember = "Value";
            this.lueCoverTypeID.Properties.NullText = "";
            this.lueCoverTypeID.Properties.NullValuePrompt = "-- Please Select Cover Type --";
            this.lueCoverTypeID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueCoverTypeID.Properties.ValueMember = "PickListID";
            this.lueCoverTypeID.Size = new System.Drawing.Size(541, 20);
            this.lueCoverTypeID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueCoverTypeID.TabIndex = 124;
            this.lueCoverTypeID.Tag = "Cover Type";
            this.lueCoverTypeID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // spAS11000PLCoverTypeBindingSource
            // 
            this.spAS11000PLCoverTypeBindingSource.DataMember = "sp_AS_11000_PL_Cover_Type";
            this.spAS11000PLCoverTypeBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // lueSupplierID
            // 
            this.lueSupplierID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11075CoverItemBindingSource, "SupplierID", true));
            this.lueSupplierID.Location = new System.Drawing.Point(125, 59);
            this.lueSupplierID.MenuManager = this.barManager1;
            this.lueSupplierID.Name = "lueSupplierID";
            this.lueSupplierID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueSupplierID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("SupplierReference", "Supplier Reference", 90, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueSupplierID.Properties.DataSource = this.spAS11020SupplierListBindingSource;
            this.lueSupplierID.Properties.DisplayMember = "SupplierReference";
            this.lueSupplierID.Properties.NullText = "";
            this.lueSupplierID.Properties.NullValuePrompt = "-- Please Select Supplier --";
            this.lueSupplierID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueSupplierID.Properties.ValueMember = "SupplierID";
            this.lueSupplierID.Size = new System.Drawing.Size(540, 20);
            this.lueSupplierID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueSupplierID.TabIndex = 125;
            this.lueSupplierID.Tag = "Supplier";
            this.lueSupplierID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // spAS11020SupplierListBindingSource
            // 
            this.spAS11020SupplierListBindingSource.DataMember = "sp_AS_11020_Supplier_List";
            this.spAS11020SupplierListBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // txtPolicy
            // 
            this.txtPolicy.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11075CoverItemBindingSource, "Policy", true));
            this.txtPolicy.Location = new System.Drawing.Point(125, 83);
            this.txtPolicy.MenuManager = this.barManager1;
            this.txtPolicy.Name = "txtPolicy";
            this.txtPolicy.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtPolicy, true);
            this.txtPolicy.Size = new System.Drawing.Size(540, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtPolicy, optionsSpelling2);
            this.txtPolicy.StyleController = this.editFormDataLayoutControlGroup;
            this.txtPolicy.TabIndex = 126;
            this.txtPolicy.Tag = "Policy";
            this.txtPolicy.EditValueChanged += new System.EventHandler(this.txtPolicy_EditValueChanged);
            this.txtPolicy.Validating += new System.ComponentModel.CancelEventHandler(this.txtPolicy_Validating);
            // 
            // AnnualCoverCostSpinEdit
            // 
            this.AnnualCoverCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11075CoverItemBindingSource, "AnnualCoverCost", true));
            this.AnnualCoverCostSpinEdit.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.AnnualCoverCostSpinEdit.Location = new System.Drawing.Point(125, 107);
            this.AnnualCoverCostSpinEdit.MenuManager = this.barManager1;
            this.AnnualCoverCostSpinEdit.Name = "AnnualCoverCostSpinEdit";
            this.AnnualCoverCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AnnualCoverCostSpinEdit.Properties.DisplayFormat.FormatString = "c2";
            this.AnnualCoverCostSpinEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.AnnualCoverCostSpinEdit.Properties.MaxValue = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.AnnualCoverCostSpinEdit.Size = new System.Drawing.Size(540, 20);
            this.AnnualCoverCostSpinEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.AnnualCoverCostSpinEdit.TabIndex = 127;
            this.AnnualCoverCostSpinEdit.Tag = "Annual Cover Cost";
            this.AnnualCoverCostSpinEdit.Validating += new System.ComponentModel.CancelEventHandler(this.spinEdit_Validating);
            // 
            // deRenewalDate
            // 
            this.deRenewalDate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11075CoverItemBindingSource, "RenewalDate", true));
            this.deRenewalDate.EditValue = null;
            this.deRenewalDate.Location = new System.Drawing.Point(782, 107);
            this.deRenewalDate.MenuManager = this.barManager1;
            this.deRenewalDate.Name = "deRenewalDate";
            this.deRenewalDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deRenewalDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deRenewalDate.Size = new System.Drawing.Size(541, 20);
            this.deRenewalDate.StyleController = this.editFormDataLayoutControlGroup;
            this.deRenewalDate.TabIndex = 128;
            this.deRenewalDate.Tag = "Renewal Date";
            this.deRenewalDate.Validating += new System.ComponentModel.CancelEventHandler(this.dateEdit_Validating);
            // 
            // NotesMemoEdit
            // 
            this.NotesMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11075CoverItemBindingSource, "Notes", true));
            this.NotesMemoEdit.Location = new System.Drawing.Point(24, 191);
            this.NotesMemoEdit.MenuManager = this.barManager1;
            this.NotesMemoEdit.Name = "NotesMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.NotesMemoEdit, true);
            this.NotesMemoEdit.Size = new System.Drawing.Size(1287, 412);
            this.scSpellChecker.SetSpellCheckerOptions(this.NotesMemoEdit, optionsSpelling3);
            this.NotesMemoEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.NotesMemoEdit.TabIndex = 129;
            this.NotesMemoEdit.Tag = "Notes";
            // 
            // spnExcess
            // 
            this.spnExcess.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11075CoverItemBindingSource, "Excess", true));
            this.spnExcess.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spnExcess.Location = new System.Drawing.Point(782, 83);
            this.spnExcess.MenuManager = this.barManager1;
            this.spnExcess.Name = "spnExcess";
            this.spnExcess.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnExcess.Properties.DisplayFormat.FormatString = "c2";
            this.spnExcess.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.spnExcess.Properties.MaxValue = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.spnExcess.Size = new System.Drawing.Size(541, 20);
            this.spnExcess.StyleController = this.editFormDataLayoutControlGroup;
            this.spnExcess.TabIndex = 130;
            this.spnExcess.Tag = "Excess";
            this.spnExcess.Validating += new System.ComponentModel.CancelEventHandler(this.spinEdit_Validating);
            // 
            // deStartDate
            // 
            this.deStartDate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11075CoverItemBindingSource, "StartDate", true));
            this.deStartDate.EditValue = null;
            this.deStartDate.Location = new System.Drawing.Point(125, 131);
            this.deStartDate.MenuManager = this.barManager1;
            this.deStartDate.Name = "deStartDate";
            this.deStartDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deStartDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deStartDate.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.deStartDate.Properties.NullValuePrompt = "-- Please Enter Start Date --";
            this.deStartDate.Properties.NullValuePromptShowForEmptyValue = true;
            this.deStartDate.Size = new System.Drawing.Size(540, 20);
            this.deStartDate.StyleController = this.editFormDataLayoutControlGroup;
            this.deStartDate.TabIndex = 132;
            this.deStartDate.Tag = "Start Date";
            this.deStartDate.Validating += new System.ComponentModel.CancelEventHandler(this.dateEdit_Validating);
            // 
            // deEndDate
            // 
            this.deEndDate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11075CoverItemBindingSource, "EndDate", true));
            this.deEndDate.EditValue = null;
            this.deEndDate.Location = new System.Drawing.Point(782, 131);
            this.deEndDate.MenuManager = this.barManager1;
            this.deEndDate.Name = "deEndDate";
            this.deEndDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deEndDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deEndDate.Properties.NullDate = new System.DateTime(3000, 1, 1, 0, 0, 0, 0);
            this.deEndDate.Size = new System.Drawing.Size(541, 20);
            this.deEndDate.StyleController = this.editFormDataLayoutControlGroup;
            this.deEndDate.TabIndex = 133;
            this.deEndDate.Tag = "End Date";
            this.deEndDate.Validating += new System.ComponentModel.CancelEventHandler(this.dateEdit_Validating);
            // 
            // lueCoverStatusID
            // 
            this.lueCoverStatusID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11075CoverItemBindingSource, "CoverStatusID", true));
            this.lueCoverStatusID.Location = new System.Drawing.Point(782, 35);
            this.lueCoverStatusID.MenuManager = this.barManager1;
            this.lueCoverStatusID.Name = "lueCoverStatusID";
            this.lueCoverStatusID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueCoverStatusID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Cover Status", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("intOrder", "int Order", 53, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.lueCoverStatusID.Properties.DataSource = this.spAS11000PLCoverStatusBindingSource;
            this.lueCoverStatusID.Properties.DisplayMember = "Value";
            this.lueCoverStatusID.Properties.NullText = "";
            this.lueCoverStatusID.Properties.NullValuePrompt = "-- Please Select Cover Status --";
            this.lueCoverStatusID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueCoverStatusID.Properties.ValueMember = "PickListID";
            this.lueCoverStatusID.Size = new System.Drawing.Size(541, 20);
            this.lueCoverStatusID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueCoverStatusID.TabIndex = 134;
            this.lueCoverStatusID.Tag = "Cover Status";
            this.lueCoverStatusID.EditValueChanged += new System.EventHandler(this.lueCoverStatusID_EditValueChanged);
            this.lueCoverStatusID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // spAS11000PLCoverStatusBindingSource
            // 
            this.spAS11000PLCoverStatusBindingSource.DataMember = "sp_AS_11000_PL_Cover_Status";
            this.spAS11000PLCoverStatusBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AllowDrawBackground = false;
            this.layoutControlGroup1.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForGroundControlReference,
            this.ItemForCoverTypeID,
            this.ItemForPolicy,
            this.ItemForAnnualCoverCost,
            this.ItemForRenewalDate,
            this.ItemForExcess,
            this.ItemForSupplierID,
            this.tabbedControlGroup1,
            this.ItemForStartDate,
            this.ItemForEndDate,
            this.ItemForCoverStatusID});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 23);
            this.layoutControlGroup1.Name = "autoGeneratedGroup0";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1315, 584);
            // 
            // ItemForGroundControlReference
            // 
            this.ItemForGroundControlReference.Control = this.txtEquipmentReference;
            this.ItemForGroundControlReference.CustomizationFormText = "Equipment Reference :";
            this.ItemForGroundControlReference.Location = new System.Drawing.Point(0, 0);
            this.ItemForGroundControlReference.Name = "ItemForGroundControlReference";
            this.ItemForGroundControlReference.Size = new System.Drawing.Size(657, 24);
            this.ItemForGroundControlReference.Text = "Equipment Reference :";
            this.ItemForGroundControlReference.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForCoverTypeID
            // 
            this.ItemForCoverTypeID.Control = this.lueCoverTypeID;
            this.ItemForCoverTypeID.CustomizationFormText = "Cover Type :";
            this.ItemForCoverTypeID.Location = new System.Drawing.Point(657, 24);
            this.ItemForCoverTypeID.Name = "ItemForCoverTypeID";
            this.ItemForCoverTypeID.Size = new System.Drawing.Size(658, 24);
            this.ItemForCoverTypeID.Text = "Cover Type :";
            this.ItemForCoverTypeID.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForPolicy
            // 
            this.ItemForPolicy.Control = this.txtPolicy;
            this.ItemForPolicy.CustomizationFormText = "Policy :";
            this.ItemForPolicy.Location = new System.Drawing.Point(0, 48);
            this.ItemForPolicy.Name = "ItemForPolicy";
            this.ItemForPolicy.Size = new System.Drawing.Size(657, 24);
            this.ItemForPolicy.Text = "Policy :";
            this.ItemForPolicy.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForAnnualCoverCost
            // 
            this.ItemForAnnualCoverCost.Control = this.AnnualCoverCostSpinEdit;
            this.ItemForAnnualCoverCost.CustomizationFormText = "Annual Cover Cost :";
            this.ItemForAnnualCoverCost.Location = new System.Drawing.Point(0, 72);
            this.ItemForAnnualCoverCost.Name = "ItemForAnnualCoverCost";
            this.ItemForAnnualCoverCost.Size = new System.Drawing.Size(657, 24);
            this.ItemForAnnualCoverCost.Text = "Annual Cover Cost :";
            this.ItemForAnnualCoverCost.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForRenewalDate
            // 
            this.ItemForRenewalDate.Control = this.deRenewalDate;
            this.ItemForRenewalDate.CustomizationFormText = "Renewal Date :";
            this.ItemForRenewalDate.Location = new System.Drawing.Point(657, 72);
            this.ItemForRenewalDate.Name = "ItemForRenewalDate";
            this.ItemForRenewalDate.Size = new System.Drawing.Size(658, 24);
            this.ItemForRenewalDate.Text = "Renewal Date :";
            this.ItemForRenewalDate.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForExcess
            // 
            this.ItemForExcess.Control = this.spnExcess;
            this.ItemForExcess.CustomizationFormText = "Excess :";
            this.ItemForExcess.Location = new System.Drawing.Point(657, 48);
            this.ItemForExcess.Name = "ItemForExcess";
            this.ItemForExcess.Size = new System.Drawing.Size(658, 24);
            this.ItemForExcess.Text = "Excess :";
            this.ItemForExcess.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForSupplierID
            // 
            this.ItemForSupplierID.Control = this.lueSupplierID;
            this.ItemForSupplierID.CustomizationFormText = "Supplier :";
            this.ItemForSupplierID.Location = new System.Drawing.Point(0, 24);
            this.ItemForSupplierID.Name = "ItemForSupplierID";
            this.ItemForSupplierID.Size = new System.Drawing.Size(657, 24);
            this.ItemForSupplierID.Text = "Supplier :";
            this.ItemForSupplierID.TextSize = new System.Drawing.Size(110, 13);
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 120);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup2;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(1315, 464);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup2.CaptionImage")));
            this.layoutControlGroup2.CustomizationFormText = "Notes";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForNotes});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1291, 416);
            this.layoutControlGroup2.Text = "Notes";
            // 
            // ItemForNotes
            // 
            this.ItemForNotes.Control = this.NotesMemoEdit;
            this.ItemForNotes.CustomizationFormText = "Notes";
            this.ItemForNotes.Location = new System.Drawing.Point(0, 0);
            this.ItemForNotes.Name = "ItemForNotes";
            this.ItemForNotes.Size = new System.Drawing.Size(1291, 416);
            this.ItemForNotes.Text = "Notes";
            this.ItemForNotes.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForNotes.TextVisible = false;
            // 
            // ItemForStartDate
            // 
            this.ItemForStartDate.Control = this.deStartDate;
            this.ItemForStartDate.CustomizationFormText = "Start Date :";
            this.ItemForStartDate.Location = new System.Drawing.Point(0, 96);
            this.ItemForStartDate.Name = "ItemForStartDate";
            this.ItemForStartDate.Size = new System.Drawing.Size(657, 24);
            this.ItemForStartDate.Text = "Start Date :";
            this.ItemForStartDate.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForEndDate
            // 
            this.ItemForEndDate.Control = this.deEndDate;
            this.ItemForEndDate.CustomizationFormText = "End Date :";
            this.ItemForEndDate.Location = new System.Drawing.Point(657, 96);
            this.ItemForEndDate.Name = "ItemForEndDate";
            this.ItemForEndDate.Size = new System.Drawing.Size(658, 24);
            this.ItemForEndDate.Text = "End Date :";
            this.ItemForEndDate.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForCoverStatusID
            // 
            this.ItemForCoverStatusID.Control = this.lueCoverStatusID;
            this.ItemForCoverStatusID.CustomizationFormText = "Cover Status :";
            this.ItemForCoverStatusID.Location = new System.Drawing.Point(657, 0);
            this.ItemForCoverStatusID.Name = "ItemForCoverStatusID";
            this.ItemForCoverStatusID.Size = new System.Drawing.Size(658, 24);
            this.ItemForCoverStatusID.Text = "Cover Status :";
            this.ItemForCoverStatusID.TextSize = new System.Drawing.Size(110, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(184, 23);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(447, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(868, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp_AS_11075_Cover_ItemTableAdapter
            // 
            this.sp_AS_11075_Cover_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11020_Supplier_ListTableAdapter
            // 
            this.sp_AS_11020_Supplier_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Cover_TypeTableAdapter
            // 
            this.sp_AS_11000_PL_Cover_TypeTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Cover_StatusTableAdapter
            // 
            this.sp_AS_11000_PL_Cover_StatusTableAdapter.ClearBeforeFill = true;
            // 
            // frm_AS_Cover_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1335, 683);
            this.Controls.Add(this.editFormDataLayoutControlGroup);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AS_Cover_Edit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Manage Breakdown and Insurance Cover";
            this.Activated += new System.EventHandler(this.frm_AS_Cover_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_AS_Cover_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_AS_Cover_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.editFormDataLayoutControlGroup, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11075CoverItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).EndInit();
            this.editFormDataLayoutControlGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentReference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueCoverTypeID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLCoverTypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSupplierID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11020SupplierListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPolicy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AnnualCoverCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deRenewalDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deRenewalDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnExcess.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStartDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStartDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueCoverStatusID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLCoverStatusBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGroundControlReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCoverTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPolicy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAnnualCoverCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRenewalDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSupplierID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNotes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCoverStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private DataSet_AS_DataEntryTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraBars.Bar barStatus;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.BarStaticItem bsiFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private DevExpress.XtraDataLayout.DataLayoutControl editFormDataLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlGroup editFormLayoutControlGroup;
        private DevExpress.XtraEditors.DataNavigator editFormDataNavigator;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.TextEdit txtEquipmentReference;
        private System.Windows.Forms.BindingSource spAS11075CoverItemBindingSource;
        private DevExpress.XtraEditors.LookUpEdit lueCoverTypeID;
        private DevExpress.XtraEditors.LookUpEdit lueSupplierID;
        private System.Windows.Forms.BindingSource spAS11020SupplierListBindingSource;
        private DevExpress.XtraEditors.TextEdit txtPolicy;
        private DevExpress.XtraEditors.SpinEdit AnnualCoverCostSpinEdit;
        private DevExpress.XtraEditors.DateEdit deRenewalDate;
        private DevExpress.XtraEditors.MemoEdit NotesMemoEdit;
        private DevExpress.XtraEditors.SpinEdit spnExcess;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGroundControlReference;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCoverTypeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPolicy;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAnnualCoverCost;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRenewalDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExcess;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSupplierID;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11075_Cover_ItemTableAdapter sp_AS_11075_Cover_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11020_Supplier_ListTableAdapter sp_AS_11020_Supplier_ListTableAdapter;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private System.Windows.Forms.BindingSource spAS11000PLCoverTypeBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Cover_TypeTableAdapter sp_AS_11000_PL_Cover_TypeTableAdapter;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNotes;
        private DevExpress.XtraEditors.DateEdit deStartDate;
        private DevExpress.XtraEditors.DateEdit deEndDate;
        private DevExpress.XtraEditors.LookUpEdit lueCoverStatusID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStartDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEndDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCoverStatusID;
        private System.Windows.Forms.BindingSource spAS11000PLCoverStatusBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Cover_StatusTableAdapter sp_AS_11000_PL_Cover_StatusTableAdapter;


    }
}

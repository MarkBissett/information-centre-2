﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;
using WoodPlan5.Properties;
using BaseObjects;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Xml;
using DevExpress.XtraEditors.Repository;
using DevExpress.Utils;
using System.Reflection;
using DevExpress.XtraSplashScreen;

namespace WoodPlan5
{
    public partial class frm_AS_Equipment_Edit: BaseObjects.frmBase
    {
        public frm_AS_Equipment_Edit()
        {
            InitializeComponent();
        }

        private void frm_AS_Equipment_Edit_Load(object sender, EventArgs e)
        {
            txtEquipmentReference.Properties.ReadOnly = true;

            this.FormID = 110101;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;
            sp_AS_11000_FleetManagerDummyScreenTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11000_FleetManagerDummyScreenTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_FleetManagerDummyScreen,strCaller);
            gridControl1.ForceInitialize();
            // Add record selection checkboxes to popup grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;
            // Populate Main Dataset //
            sp_AS_11002_Equipment_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            // Populate Child dataSet
            sp_AS_11005_Vehicle_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11008_Plant_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11026_Gadget_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11029_Office_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11032_Hardware_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11035_Software_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11000_Duplicate_SearchTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11014_Make_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11017_Model_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11020_Supplier_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11066_Depreciation_Settings_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11096_Road_Tax_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11141_Hardware_Specifications_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            CreateNewInstancesGridStates();
            equipmentLayoutControlGroup.BeginUpdate();
            switch (strFormMode.ToLower())
            {
                case "add":
                    //lueEquipment_Category.Properties.ReadOnly = false;
                    addNewEquipmentRow();
                    break;
                case "blockadd":
                    this.Close();
                    break;
                case "blockedit":
                    MessageBox.Show("this section is not yet ready","Under Construction", MessageBoxButtons.OK,MessageBoxIcon.Information);
                    this.Close();
                    Get_ChildData();
                        loadEquipment();
                        equipmentDataNavigator.Enabled = false;
                        block_EditDisableVehicle();
                        DataRow drNewRow = this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item.NewRow();
                        drNewRow["Mode"] = "blockedit";
                        drNewRow["RecordID"] = strRecordIDs;
                        drNewRow["EquipmentID"] = "";
                        drNewRow["EquipmentReference"] = "";
                        drNewRow["EquipmentCategory"] = "";
                        drNewRow["ManufacturerID"] = "";
                        drNewRow["Make"] = "";
                        drNewRow["Model"] = "";
                        drNewRow["DepreciationStartDate"] = "";
                        drNewRow["ExchequerID"] = "";
                        drNewRow["SageReference"] = "";
                        this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item.Rows.Add(drNewRow);
                        this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
                                           
                    break;
                case "edit":
                case "view":
                    lueEquipment_Category.Properties.ReadOnly = true;
                    try
                    {
                      loadEquipment();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                    break;
            }
            if (intExchequerCategoryMVG != 0)
            {
                spAS11000PLExchequerCategoryBindingSource.Filter = String.Format("PickListID in ({0})", intExchequerCategoryMVG);
            }
            equipmentLayoutControlGroup.EndUpdate();
            if (strFormMode == "view")  // Disable all controls //
            {
                equipmentDataLayoutControl.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }

           spnMaximumOccurrence.Properties.ReadOnly = true;
           spnDayOfMonthApplyDepreciation.Properties.ReadOnly = true;                                                                                               
           lueOccursUnitDescriptor.Properties.ReadOnly = true;
            //Populate picklist Data
            PopulateEquipmentPickList();
            Get_ChildData();
            
            ProcessPermissionsForForm();
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
        }

        private void CreateNewInstancesGridStates()
        {

            RefreshGridViewStateRoadTax = new RefreshGridState(roadTaxGridView, "RoadTaxID");

            RefreshGridViewStateTracker = new RefreshGridState(trackerGridView, "TrackerInformationID");

            RefreshGridViewStateTransactions = new RefreshGridState(transactionsGridView, "TransactionID");

            RefreshGridViewStateKeeper = new RefreshGridState(keeperGridView, "KeeperAllocationID");

            RefreshGridViewStateBilling = new RefreshGridState(billingGridView, "EquipmentBillingID");

            RefreshGridViewStateDepreciation = new RefreshGridState(depreciationGridView, "DepreciationID");

            RefreshGridViewStateCover = new RefreshGridState(coverGridView, "CoverID");

            RefreshGridViewStateServiceInterval = new RefreshGridState(serviceDataGridView, "ServiceDataID");

            RefreshGridViewStateWork = new RefreshGridState(workDetailGridView, "WorkDetailID");

            RefreshGridViewStateIncident = new RefreshGridState(incidentGridView, "IncidentID");

            RefreshGridViewStatePurpose = new RefreshGridState(purposeGridView, "EquipmentPurposeID");

            RefreshGridViewStateFuelCard = new RefreshGridState(fuelCardGridView, "fuelCardID");

            RefreshGridViewStateSpeeding = new RefreshGridState(speedingGridView, "SpeedingID");
        }

        #region Instance Variables

        public int intExchequerCategoryMVG = 0;
        private string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        private string i_str_AddedRecordIDs2 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        private string i_str_AddedRecordIDs3 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //        
        private string strMessage1, strMessage2;
        private int iDistinctSelectedType = 1;
        public int numOfSelectedRows = 1;
        public string strRecordsToLoad = "";
        private string gcRef = "";
        int i_int_FocusedGrid = 0;
        GridHitInfo downHitInfo = null;
        private Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        private bool isRunning = false;
        BaseObjects.GridCheckMarksSelection selection1;
        string shownTabs = "";
        private uint _PropertyName;
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        public int intLinkedToRecordID = 0;
        public string strLinkedToRecordDesc = "";
        bool bool_FormLoading = true;
        
        private string tempMake = "";
        private string tempModel = "";
        private string tempManufacturer = "";
        private string tempRegistration = "";

        public bool equipmentChanged = false;
        public bool vehicleChanged = false;
        public bool plantChanged = false;
        public bool gadgetChanged = false;
        public bool hardwareChanged = false;
        public bool softwareChanged = false;
        public bool officeChanged = false;

        private bool P11dManagerPromptRequired = false;

        public string strEquipmentIDs = "";
        public enum FormMode { add, edit, view, delete, blockadd, blockedit };
        public FormMode formMode;
        public enum EquipmentType { None, Vehicle = 1, Plant = 2, Gadget = 3, Hardware = 4, Software = 5, Office = 6 }
        public enum SentenceCase { Upper , Lower, Title, AsIs }
        public EquipmentType passedEquipType;
        
        private bool iBool_AllowAddRoadTax = false;
        private bool iBool_AllowEditRoadTax = false;
        private bool iBool_AllowDeleteRoadTax = false;

        private bool iBool_AllowAddSpeeding = false;
        private bool iBool_AllowEditSpeeding = false;
        private bool iBool_AllowDeleteSpeeding = false;

        private bool iBool_AllowAddTracker = false;
        private bool iBool_AllowEditTracker = false;
        private bool iBool_AllowDeleteTracker = false;

        private bool iBool_AllowAddBilling = false;
        private bool iBool_AllowEditBilling = false;
        private bool iBool_AllowDeleteBilling = false;

        private bool iBool_AllowAddKeeper = false;
        private bool iBool_AllowEditKeeper = false;
        private bool iBool_AllowDeleteKeeper = false;

        private bool iBool_AllowAddReminders = false;
        private bool iBool_AllowEditReminders = false;
        private bool iBool_AllowDeleteReminders = false;

        private bool iBool_AllowAddTransactions = false;
        private bool iBool_AllowEditTransactions = false;
        private bool iBool_AllowDeleteTransactions = false;
        
        internal bool iBool_AllowAddDepreciation = false;
        internal bool iBool_AllowEditDepreciation = false;
        internal bool iBool_AllowDeleteDepreciation = false;

        private bool iBool_AllowAddCover = false;
        private bool iBool_AllowEditCover = false;
        private bool iBool_AllowDeleteCover = false;

        private bool iBool_AllowAddServiceData = false;
        private bool iBool_AllowEditServiceData = false;
        private bool iBool_AllowDeleteServiceData = false;

        private bool iBool_AllowAddWorkDetail = false;
        private bool iBool_AllowEditWorkDetail = false;
        private bool iBool_AllowDeleteWorkDetail = false;

        private bool iBool_AllowAddIncident = false;
        private bool iBool_AllowEditIncident = false;
        private bool iBool_AllowDeleteIncident = false;

        private bool iBool_AllowAddPurpose = false;
        private bool iBool_AllowEditPurpose = false;
        private bool iBool_AllowDeletePurpose = false;

        private bool iBool_AllowAddFuelCard = false;
        private bool iBool_AllowEditFuelCard = false;
        private bool iBool_AllowDeleteFuelCard = false;

        // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewStateRoadTax;
        public RefreshGridState RefreshGridViewStateTracker;
        public RefreshGridState RefreshGridViewStateKeeper;
        public RefreshGridState RefreshGridViewStateTransactions;
        public RefreshGridState RefreshGridViewStateBilling;
        public RefreshGridState RefreshGridViewStateDepreciation;
        public RefreshGridState RefreshGridViewStateCover;
        public RefreshGridState RefreshGridViewStateServiceInterval;
        public RefreshGridState RefreshGridViewStateWork;
        public RefreshGridState RefreshGridViewStateIncident;
        public RefreshGridState RefreshGridViewStatePurpose;
        public RefreshGridState RefreshGridViewStateFuelCard;
        public RefreshGridState RefreshGridViewStateSpeeding;        
        #endregion

        #region Compulsory Implementation 
        
        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode.ToLower())
            {
                case "add":
                    try
                    {
                        bsiFormMode.Caption = "Form Mode: Adding";
                        bsiFormMode.ImageIndex = 0;  // Add //
                        bsiFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                      //  barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        if (passedEquipType != EquipmentType.None)
                        {
                            lueEquipment_Category.Enabled = false;                            
                        }

                        depreciationLayoutControlGroup.Enabled = iBool_AllowAddDepreciation;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        bsiFormMode.Caption = "Form Mode: Block Adding";
                        bsiFormMode.ImageIndex = 0;  // Add //
                        bsiFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    try
                    {
                        bsiFormMode.Caption = "Form Mode: Editing";
                        //barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        bsiFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        ////barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        txtEquipmentReference.Focus();
                        depreciationLayoutControlGroup.Enabled = iBool_AllowEditDepreciation;
                        manufacturerIDDescription(lueEquipment_Category.EditValue.ToString());
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                    ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        bsiFormMode.Caption = "Form Mode: Block Editing";
                        //barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        bsiFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        //barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        block_EditDisableEquipment();             
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }

                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        bsiFormMode.Caption = "Form Mode: Viewing";
                        bsiFormMode.ImageIndex = 4;  // View //
                        bsiFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        //barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();
            SetEditorButtons();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            if (strFormMode == "view")  // Disable all controls //
            {
                equipmentDataLayoutControl.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
        
            bbiSave.Enabled = false;
            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }

        #endregion

        #region Form_Events

       
        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) 
                this.Close();
        }

        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private void frm_AS_Equipment_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strFormMode == "view") return;
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProviderEquipment.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }

        private void frm_AS_Equipment_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }

        }
        
        #endregion

        #region Form_Functions

        private void saveGridViewState()
        {
            // Store Grid View State //
            this.RefreshGridViewStateRoadTax.SaveViewInfo();
            this.RefreshGridViewStateTracker.SaveViewInfo();
            this.RefreshGridViewStateBilling.SaveViewInfo();
            this.RefreshGridViewStateKeeper.SaveViewInfo();
            this.RefreshGridViewStateTransactions.SaveViewInfo();
            this.RefreshGridViewStateDepreciation.SaveViewInfo();
            this.RefreshGridViewStateCover.SaveViewInfo();
            this.RefreshGridViewStateServiceInterval.SaveViewInfo();
            this.RefreshGridViewStateWork.SaveViewInfo();
            this.RefreshGridViewStateIncident.SaveViewInfo();
            this.RefreshGridViewStatePurpose.SaveViewInfo();
            this.RefreshGridViewStateFuelCard.SaveViewInfo();
            this.RefreshGridViewStateSpeeding.SaveViewInfo();
            //this.RefreshGridViewStateReminder.SaveViewInfo();

        }

        private void loadGridViewState()
        {
            // Store Grid View State //
            this.RefreshGridViewStateRoadTax.LoadViewInfo();
            this.RefreshGridViewStateTracker.LoadViewInfo();
            this.RefreshGridViewStateBilling.LoadViewInfo();
            this.RefreshGridViewStateKeeper.LoadViewInfo();
            this.RefreshGridViewStateTransactions.LoadViewInfo();
            this.RefreshGridViewStateDepreciation.LoadViewInfo();
            this.RefreshGridViewStateCover.LoadViewInfo();
            this.RefreshGridViewStateServiceInterval.LoadViewInfo();
            this.RefreshGridViewStateWork.LoadViewInfo();
            this.RefreshGridViewStateIncident.LoadViewInfo();
            this.RefreshGridViewStatePurpose.LoadViewInfo();
            this.RefreshGridViewStateFuelCard.LoadViewInfo();
            this.RefreshGridViewStateSpeeding.LoadViewInfo();
            //this.RefreshGridViewStateReminder.LoadViewInfo();        
        }
        
        private void OpenEditForm(FormMode mode, string frmCaller, GridControl gridControl, EquipmentType equipmentType)
        {
            if (strFormMode != "edit")
            {
                if (strFormMode != "blockedit")
                {
                    XtraMessageBox.Show("Please complete and save main form records before making detailed form changes.", "Add/Edit Restricted", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                else
                {
                    XtraMessageBox.Show("Making changes to this section is rectricted during block editting.", "Add/Edit Restricted", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                return;
            }
            switch (equipmentType)
            {
                case EquipmentType.None:
                case EquipmentType.Vehicle:
                case EquipmentType.Plant:
                    break;
                case EquipmentType.Gadget:
                case EquipmentType.Hardware:
                case EquipmentType.Software:
                case EquipmentType.Office:
                    XtraMessageBox.Show("Manipulation of this equipment type is restricted on the " + this.Text, "Restricted Access Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
            }
            bool cancelChildLoad = false;
            GridView view = null;
            frmProgress fProgress = null;
            MethodInfo method = null;
            strRecordsToLoad = "";
            int[] intRowHandles;
            int intCount = 0;
            saveGridViewState();
            gcRef = "";
            
            switch (i_int_FocusedGrid)
            {
                

                #region Billing

                case 7:     // Billing

                    if (mode == FormMode.blockadd || mode == FormMode.add)
                    {
                        XtraMessageBox.Show("Adding is restricted in this section, add billing records in the depreciation data.", "Add/Block Add Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    if (numOfSelectedRows != 1)
                    {
                        XtraMessageBox.Show("Select one equipment record to edit/block edit the equipment's billing records.", "Edit/Block Edit Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    view = (GridView)gridControl.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (mode == FormMode.edit || mode == FormMode.blockedit)
                    {
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Billing Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            DataRow dr = view.GetDataRow(intRowHandle);
                            strRecordsToLoad += (((DataSet_AS_DataEntry.sp_AS_11050_Depreciation_ItemRow)(dr)).DepreciationID).ToString() + ',';
                        }
                    }
                    if (mode == FormMode.add || mode == FormMode.blockadd)
                    {
                        if (numOfSelectedRows < 1)
                        {
                            XtraMessageBox.Show("Please select an equipment to allocate billing(s) before proceeding.", "Add Billing Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                    }
                    cancelChildLoad = false;
                    gcRef = "";

                    if (cancelChildLoad)
                    {
                        return;
                    }

                    frm_AS_Billings_Edit bChildForm = new frm_AS_Billings_Edit();
                    bChildForm.MdiParent = this.MdiParent;
                    bChildForm.GlobalSettings = this.GlobalSettings;
                    bChildForm.strRecordIDs = strRecordsToLoad;
                    bChildForm.strEquipmentIDs = strRecordIDs;
                    bChildForm.formMode = (frm_AS_Billings_Edit.FormMode)mode;
                    bChildForm.strFormMode = (mode.ToString()).ToLower();
                    bChildForm.strCaller = frmCaller;
                    bChildForm.strGCReference = gcRef;
                    bChildForm.intRecordCount = intCount;
                    bChildForm.FormPermissions = this.FormPermissions;

                    SplashScreenManager splashScreenManager2 = new SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    bChildForm.splashScreenManager = splashScreenManager2;
                    bChildForm.splashScreenManager.ShowWaitForm();
                    bChildForm.Show();
                    break;
                #endregion

                #region Keeper

                case 8:     // Keeper
                    view = (GridView)gridControl.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;

                    if (mode == FormMode.edit)
                    {
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Keeper Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            DataRow dr = view.GetDataRow(intRowHandle);

                            strRecordsToLoad += (((DataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_ItemRow)(dr)).KeeperAllocationID).ToString() + ',';
                        }
                    }

                    if (mode == FormMode.blockedit)
                    {
                        XtraMessageBox.Show("Blockedit is restricted.", "Blockedit Keeper Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }

                    if (mode == FormMode.add || mode == FormMode.blockadd)
                    {
                        if (strRecordIDs == "")
                        {
                            XtraMessageBox.Show("Please select an equipment to allocate keepers before proceeding.", "Add Keeper Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                    }
                    cancelChildLoad = false;
                    gcRef = "";
                    if (mode == FormMode.add || mode == FormMode.blockadd)
                    {
                        if (numOfSelectedRows == 0)
                        {
                            XtraMessageBox.Show("Select one equipment record or one type of equipment to add keeper allocations.", "Add Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        if (numOfSelectedRows > 1)//block add option
                        {
                            string strTypes = "You have selected multiple records, do you want to block add the keepers";
                            switch (XtraMessageBox.Show(strTypes, "Block Add Option", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                            {
                                case DialogResult.No:
                                    // cancel operation //
                                    XtraMessageBox.Show("Please select the one record to proceed", "Block Add Option Cancelled", MessageBoxButtons.OK);
                                    cancelChildLoad = true;
                                    break;
                                case DialogResult.Yes:
                                    // change to block add //
                                    mode = FormMode.blockadd;
                                    break;
                            }

                        }
                        if (cancelChildLoad)
                        {
                            return;
                        }

                        gcRef =  ((DataSet_AS_DataEntry.sp_AS_11002_Equipment_ItemRow)((DataRowView)(spAS11002EquipmentItemBindingSource.CurrencyManager.Current)).Row).EquipmentReference;
                    }

                    if (mode == FormMode.blockadd)
                    {
                        gcRef = "";
                    }
                    if (mode == FormMode.add)
                    {
                        if (numOfSelectedRows != 1)
                        {
                            XtraMessageBox.Show("Select one equipment record to add keepers, otherwise right click for block adding keepers.", "Add Keeper Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        gcRef = ((DataSet_AS_DataEntry.sp_AS_11002_Equipment_ItemRow)((DataRowView)(spAS11002EquipmentItemBindingSource.CurrencyManager.Current)).Row).EquipmentReference;
                    }
                       
                  
                    frm_AS_Keeper_Edit kChildForm = new frm_AS_Keeper_Edit();
                    kChildForm.MdiParent = this.MdiParent;
                    kChildForm.GlobalSettings = this.GlobalSettings;
                    kChildForm.strRecordIDs = strRecordsToLoad;
                    kChildForm.strEquipmentIDs = strRecordIDs;
                    kChildForm.formMode = (frm_AS_Keeper_Edit.FormMode)mode;
                    kChildForm.strFormMode = (mode.ToString()).ToLower();
                    kChildForm.strCaller = frmCaller;
                    kChildForm.strGCReference = gcRef;
                    if (mode == FormMode.blockadd)
                    {
                        kChildForm.intRecordCount = numOfSelectedRows;
                    }
                    else
                    {
                        kChildForm.intRecordCount = intCount;
                    }
                    kChildForm.FormPermissions = this.FormPermissions;
                    SplashScreenManager splashScreenManager3 = new SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    kChildForm.splashScreenManager = splashScreenManager3;
                    kChildForm.splashScreenManager.ShowWaitForm();
                    kChildForm.Show();
                    break;

                #endregion

                #region Transactions
                        
                case 9:     // Transactions
                   XtraMessageBox.Show("Please complete editting the main form and save before accessing making changes to the transactions", "Add/Edit Restricted");
                            return;
                #endregion

                #region Reminders

                case 10:     // Reminders

                    break;
                #endregion

                #region Depreciation

                case 11:     // Depreciation
                    break;
                #endregion

                #region Incidents
                case 18: // Speeding data                
                case 12:     // Incidents 
                    view = (GridView)gridControl.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;

                    if (mode == FormMode.edit || mode == FormMode.blockedit)
                    {
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Incident Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            DataRow dr = view.GetDataRow(intRowHandle);

                            strRecordsToLoad += (((DataSet_AS_DataEntry.sp_AS_11081_Incident_ItemRow)(dr)).IncidentID).ToString() + ',';
                        }
                    }


                    cancelChildLoad = false;
                    gcRef = "";
                    if (mode == FormMode.add || mode == FormMode.blockadd)
                    {
                        if (numOfSelectedRows == 0)
                        {
                            XtraMessageBox.Show("Select one equipment record or one type of equipment to add incidents.", "Add Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        if (numOfSelectedRows > 1)//block add option
                        {
                            string strTypes = "You have selected multiple records, do you want to block add the incidents";
                            switch (XtraMessageBox.Show(strTypes, "Block Add Option", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                            {
                                case DialogResult.No:
                                    // cancel operation //
                                    XtraMessageBox.Show("Please select the one record to proceed", "Block Add Option Cancelled", MessageBoxButtons.OK);
                                    cancelChildLoad = true;
                                    break;
                                case DialogResult.Yes:
                                    // change to block add //
                                    mode = FormMode.blockadd;
                                    break;
                            }

                        }
                        if (cancelChildLoad)
                        {
                            return;
                        }

                        gcRef = ((DataSet_AS_DataEntry.sp_AS_11002_Equipment_ItemRow)((DataRowView)(spAS11002EquipmentItemBindingSource.CurrencyManager.Current)).Row).EquipmentReference;
                    }

                    if (mode == FormMode.blockadd)
                    {
                        gcRef = "";
                    }
                    if (mode == FormMode.add)
                    {
                        if (numOfSelectedRows != 1)
                        {
                            XtraMessageBox.Show("Select one equipment record to add incidents, otherwise right click for block adding incidents.", "Add Incident Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        gcRef = ((DataSet_AS_DataEntry.sp_AS_11002_Equipment_ItemRow)((DataRowView)(spAS11002EquipmentItemBindingSource.CurrencyManager.Current)).Row).EquipmentReference;
                    }

                    frm_AS_Incident_Edit iChildForm = new frm_AS_Incident_Edit();
                    iChildForm.MdiParent = this.MdiParent;
                    iChildForm.GlobalSettings = this.GlobalSettings;
                    iChildForm.strRecordIDs = strRecordsToLoad;
                    iChildForm.strEquipmentIDs = strRecordIDs;
                    iChildForm.formMode = (frm_AS_Incident_Edit.FormMode)mode;
                    iChildForm.strFormMode = (mode.ToString()).ToLower();
                    iChildForm.strCaller = frmCaller;
                    iChildForm.strGCReference = gcRef;
                    if (mode == FormMode.blockadd)
                    {
                        iChildForm.intRecordCount = numOfSelectedRows;
                    }
                    else
                    {
                        iChildForm.intRecordCount = intCount;
                    }
                    iChildForm.FormPermissions = this.FormPermissions;
                    SplashScreenManager splashScreenManager5 = new SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    iChildForm.splashScreenManager = splashScreenManager5;
                    iChildForm.splashScreenManager.ShowWaitForm();
                    iChildForm.Show();
                    break;
                #endregion

                #region Service Data
                case 13:     // Service Data
                     view = (GridView)gridControl.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;

                    if (mode == FormMode.blockadd || mode == FormMode.blockedit)
                    {
                        XtraMessageBox.Show("blockadd/blockedit is restricted in this section.", "Block Add/Edit Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    if (mode == FormMode.edit || mode == FormMode.blockedit)
                    {
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Service Interval Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            DataRow dr = view.GetDataRow(intRowHandle);
                            strRecordsToLoad += (((DataSet_AS_DataEntry.sp_AS_11054_Service_Data_ItemRow)(dr)).ServiceDataID).ToString() + ',';
                        }
                    }


                    cancelChildLoad = false;
                    gcRef = "";
                    if (mode == FormMode.add || mode == FormMode.blockadd)
                    {
                        if (numOfSelectedRows == 0 || iDistinctSelectedType == 0)
                        {
                            XtraMessageBox.Show("Select one equipment record or one type of equipment to add service interval(s).", "Add Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        if (numOfSelectedRows > 1)//block add option
                        {
                            string strTypes = "You have selected multiple records, do you want to block add the Service Intervals";
                            switch (XtraMessageBox.Show(strTypes, "Block Add Option", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                            {
                                case DialogResult.No:
                                    // cancel operation //
                                    XtraMessageBox.Show("Please select the one record to proceed", "Block Add Option Cancelled", MessageBoxButtons.OK);
                                    cancelChildLoad = true;
                                    break;
                                case DialogResult.Yes:
                                    // change to block add //
                                    mode = FormMode.blockadd;
                                    break;
                            }

                        }
                        if (cancelChildLoad)
                        {
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            DataRow dr = view.GetDataRow(intRowHandle);

                            strRecordsToLoad += (((DataSet_AS_DataEntry.sp_AS_11054_Service_Data_ItemRow)(dr)).ServiceDataID).ToString() + ',';
                        }
                    }

                    if (mode == FormMode.blockadd)
                    {
                        gcRef = "";
                    }
                    if (mode == FormMode.add)
                    {
                        if (numOfSelectedRows != 1)
                        {
                            XtraMessageBox.Show("Select one equipment record to add service intervals, otherwise right click for block adding service intervals.", "Add Service Interval Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        gcRef = ((DataSet_AS_DataEntry.sp_AS_11002_Equipment_ItemRow)((DataRowView)(spAS11002EquipmentItemBindingSource.CurrencyManager.Current)).Row).EquipmentReference;
                                             
                    }

                    frm_AS_Service_Data_Edit sIChildForm = new frm_AS_Service_Data_Edit();
                    sIChildForm.MdiParent = this.MdiParent;
                    sIChildForm.GlobalSettings = this.GlobalSettings;
                    sIChildForm.strRecordIDs = strRecordsToLoad;
                    sIChildForm.strEquipmentIDs = strRecordIDs;
                    sIChildForm.formMode = (frm_AS_Service_Data_Edit.FormMode)mode;
                    sIChildForm.strFormMode = (mode.ToString()).ToLower();
                    sIChildForm.strCaller = frmCaller;
                    sIChildForm.strGCReference = gcRef;
                    if (mode == FormMode.blockadd)
                    {
                        sIChildForm.intRecordCount = numOfSelectedRows;
                    }
                    else
                    {
                        sIChildForm.intRecordCount = intCount;
                    }
                    sIChildForm.FormPermissions = this.FormPermissions;
                    SplashScreenManager splashScreenManager6 = new SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    sIChildForm.splashScreenManager = splashScreenManager6;
                    sIChildForm.splashScreenManager.ShowWaitForm();
                    sIChildForm.Show();
                    break;
                #endregion

                #region Cover

                case 14:     // Cover
                    view = (GridView)gridControl.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;

                    if (mode == FormMode.edit)
                    {
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Cover Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            DataRow dr = view.GetDataRow(intRowHandle);

                            strRecordsToLoad += (((DataSet_AS_DataEntry.sp_AS_11075_Cover_ItemRow)(dr)).CoverID).ToString() + ',';
                        }
                    }

                    if (mode == FormMode.blockedit)
                    {
                        XtraMessageBox.Show("Blockedit is restricted.", "Blockedit Cover Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }

                    cancelChildLoad = false;
                    gcRef = "";
                    if (mode == FormMode.add || mode == FormMode.blockadd)
                    {
                        if (numOfSelectedRows == 0 || iDistinctSelectedType == 0)
                        {
                            XtraMessageBox.Show("Select one equipment record or one type of equipment to add cover details.", "Add Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        if (numOfSelectedRows > 1)//block add option
                        {
                            string strTypes = "You have selected multiple records, do you want to block add the cover details";
                            switch (XtraMessageBox.Show(strTypes, "Block Add Option", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                            {
                                case DialogResult.No:
                                    // cancel operation //
                                    XtraMessageBox.Show("Please select the one record to proceed", "Block Add Option Cancelled", MessageBoxButtons.OK);
                                    cancelChildLoad = true;
                                    break;
                                case DialogResult.Yes:
                                    // change to block add //
                                    mode = FormMode.blockadd;
                                    break;
                            }
                        }
                        if (cancelChildLoad)
                        {
                            return;
                        }


                        gcRef = getEquipmentReference();
                    }

                    if (mode == FormMode.blockadd)
                    {
                        gcRef = "";
                    }
                    if (mode == FormMode.add)
                    {
                        if (numOfSelectedRows != 1)
                        {
                            XtraMessageBox.Show("Select one equipment record to add cover details, otherwise right click for block adding cover details.", "Add Cover Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }


                        gcRef = getEquipmentReference();

                    }

                    frm_AS_Cover_Edit cChildForm = new frm_AS_Cover_Edit();
                    cChildForm.MdiParent = this.MdiParent;
                    cChildForm.GlobalSettings = this.GlobalSettings;
                    cChildForm.strRecordIDs = strRecordsToLoad;
                    cChildForm.strEquipmentIDs = strRecordIDs;
                    cChildForm.formMode = (frm_AS_Cover_Edit.FormMode)mode;
                    cChildForm.strFormMode = (mode.ToString()).ToLower();
                    cChildForm.strCaller = frmCaller;
                    cChildForm.strGCReference = gcRef;
                    if (mode == FormMode.blockadd)
                    {
                        cChildForm.intRecordCount = numOfSelectedRows;
                    }
                    else
                    {
                        cChildForm.intRecordCount = intCount;
                    }
                    cChildForm.FormPermissions = this.FormPermissions;
                    SplashScreenManager splashScreenManager7 = new SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    cChildForm.splashScreenManager = splashScreenManager7;
                    cChildForm.splashScreenManager.ShowWaitForm();
                    cChildForm.Show();
                    break;
                #endregion

                #region Work Detail

                case 15:     // Work Detail
                    view = (GridView)gridControl.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;

                    if (mode == FormMode.edit)
                    {
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Work Order Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            DataRow dr = view.GetDataRow(intRowHandle);

                            strRecordsToLoad += (((DataSet_AS_DataEntry.sp_AS_11078_Work_Detail_ItemRow)(dr)).WorkDetailID).ToString() + ',';
                        }
                    }

                    if (mode == FormMode.blockedit)
                    {
                        XtraMessageBox.Show("Blockedit is restricted.", "Blockedit Work Order Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }

                    cancelChildLoad = false;
                    gcRef = "";
                    if (mode == FormMode.add || mode == FormMode.blockadd)
                    {
                        if (numOfSelectedRows == 0)
                        {
                            XtraMessageBox.Show("Select one equipment record or one type of equipment to add Work Orders.", "Add Work Order Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        if (numOfSelectedRows > 1)//block add option
                        {
                            string strTypes = "You have selected multiple records, do you want to block add the Work Orders";
                            switch (XtraMessageBox.Show(strTypes, "Block Add Option", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                            {
                                case DialogResult.No:
                                    // cancel operation //
                                    XtraMessageBox.Show("Please select the one record to proceed", "Block Add Option Cancelled", MessageBoxButtons.OK);
                                    cancelChildLoad = true;
                                    break;
                                case DialogResult.Yes:
                                    // change to block add //
                                    mode = FormMode.blockadd;
                                    break;
                            }

                        }
                        if (cancelChildLoad)
                        {
                            return;
                        }


                        gcRef = getEquipmentReference();
                    }

                    if (mode == FormMode.blockadd)
                    {
                        gcRef = "";
                    }
                    if (mode == FormMode.add)
                    {
                        if (numOfSelectedRows != 1)
                        {
                            XtraMessageBox.Show("Select one equipment record to add Work Orders, otherwise right click for block adding Work Orders.", "Add Work Order Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }


                        gcRef = getEquipmentReference();

                    }

                    frm_AS_WorkDetail_Edit wChildForm = new frm_AS_WorkDetail_Edit();
                    wChildForm.MdiParent = this.MdiParent;
                    wChildForm.GlobalSettings = this.GlobalSettings;
                    wChildForm.strRecordIDs = strRecordsToLoad;
                    wChildForm.strEquipmentIDs = strRecordIDs;
                    wChildForm.formMode = (frm_AS_WorkDetail_Edit.FormMode)mode;
                    wChildForm.strFormMode = (mode.ToString()).ToLower();
                    wChildForm.strCaller = frmCaller;
                    wChildForm.strGCReference = gcRef;
                    if (mode == FormMode.blockadd)
                    {
                        wChildForm.intRecordCount = numOfSelectedRows;
                    }
                    else
                    {
                        wChildForm.intRecordCount = intCount;
                    }
                    wChildForm.FormPermissions = this.FormPermissions;
                    SplashScreenManager splashScreenManager8 = new SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    wChildForm.splashScreenManager = splashScreenManager8;
                    wChildForm.splashScreenManager.ShowWaitForm();
                    wChildForm.Show();
                    break;

                #endregion

                #region Purpose

                case 16:     // Purpose
                    view = (GridView)gridControl.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;

                    if (mode == FormMode.edit || mode == FormMode.blockedit)
                    {
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Equipment Purpose Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            DataRow dr = view.GetDataRow(intRowHandle);

                            strRecordsToLoad += (((DataSet_AS_DataEntry.sp_AS_11084_Purpose_ItemRow)(dr)).EquipmentPurposeID).ToString() + ',';
                        }
                    }

                    //if (mode == FormMode.blockedit)
                    //{
                    //    XtraMessageBox.Show("Blockedit is restricted.", "Blockedit Equipment Purpose Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    //    return;
                    //}

                    cancelChildLoad = false;
                    gcRef = "";
                    if (mode == FormMode.add || mode == FormMode.blockadd)
                    {
                        if (numOfSelectedRows == 0)
                        {
                            XtraMessageBox.Show("Select one equipment record or one type of equipment to add Equipment Purpose.", "Add Equipment Purpose Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        if (numOfSelectedRows > 1)//block add option
                        {
                            string strTypes = "You have selected multiple records, do you want to block add the equipment purpose?";
                            switch (XtraMessageBox.Show(strTypes, "Block Add Option", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1))
                            {
                                case DialogResult.No:
                                    // cancel operation //
                                    XtraMessageBox.Show("Please select the one record to proceed", "Block Add Option Cancelled", MessageBoxButtons.OK);
                                    cancelChildLoad = true;
                                    break;
                                case DialogResult.Yes:
                                    // change to block add //
                                    mode = FormMode.blockadd;
                                    break;
                            }

                        }
                        if (cancelChildLoad)
                        {
                            return;
                        }


                        gcRef = getEquipmentReference();
                    }

                    if (mode == FormMode.blockadd)
                    {
                        gcRef = "";
                    }
                    if (mode == FormMode.add)
                    {
                        if (numOfSelectedRows != 1)
                        {
                            XtraMessageBox.Show("Select one equipment record to add Equipment Purpose records, otherwise right click for block adding Equipment Purpose records.", "Add Equipment Purpose Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }


                        gcRef = getEquipmentReference();
                    }

                    frm_AS_Purpose_Edit pChildForm = new frm_AS_Purpose_Edit();
                    pChildForm.MdiParent = this.MdiParent;
                    pChildForm.GlobalSettings = this.GlobalSettings;
                    pChildForm.strRecordIDs = strRecordsToLoad;
                    pChildForm.strEquipmentIDs = strRecordIDs;
                    pChildForm.formMode = (frm_AS_Purpose_Edit.FormMode)mode;
                    pChildForm.strFormMode = (mode.ToString()).ToLower();
                    pChildForm.strCaller = frmCaller;
                    pChildForm.strGCReference = gcRef;
                    if (mode == FormMode.blockadd)
                    {
                        pChildForm.intRecordCount = numOfSelectedRows;
                    }
                    else
                    {
                        pChildForm.intRecordCount = intCount;
                    }
                    pChildForm.FormPermissions = this.FormPermissions;
                    SplashScreenManager splashScreenManager9 = new SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    pChildForm.splashScreenManager = splashScreenManager9;
                    pChildForm.splashScreenManager.ShowWaitForm();
                    pChildForm.Show();
                    break;
                #endregion

                #region Fuel Card

                case 17:     // Fuel Card
                    view = (GridView)gridControl.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;

                    if (mode == FormMode.edit)
                    {
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Fuel Card Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            DataRow dr = view.GetDataRow(intRowHandle);

                            strRecordsToLoad += (((DataSet_AS_DataEntry.sp_AS_11089_Fuel_Card_ItemRow)(dr)).FuelCardID).ToString() + ',';
                        }
                    }

                    if (mode == FormMode.blockedit)
                    {
                        XtraMessageBox.Show("Blockedit is restricted.", "Blockedit Fuel Card Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }

                    if (mode == FormMode.blockadd)
                    {
                        XtraMessageBox.Show("Blockadd is restricted.", "Blockadd Fuel Card Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    if (mode == FormMode.add)
                    {
                        if (strRecordIDs == "")
                        {
                            XtraMessageBox.Show("Please select an equipment to allocate Fuel Cards before proceeding.", "Add Fuel Card Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                    }
                    cancelChildLoad = false;
                    gcRef = "";
                    if (mode == FormMode.add)
                    {
                        if (numOfSelectedRows == 0)
                        {
                            XtraMessageBox.Show("Select one equipment record or one type of equipment to add Fuel Card allocations.", "Add Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        if (numOfSelectedRows > 1)//block add option
                        {
                            string strTypes = "You have selected multiple records, please select one equipment to add the Fuel Cards";
                            XtraMessageBox.Show(strTypes, "Block Add Restricted", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            cancelChildLoad = true;
                        }
                        if (cancelChildLoad)
                        {
                            return;
                        }

                        int equipType = 0;
                        string equipID = "";
                        gcRef = getEquipmentReference();

                        equipType = ((DataSet_AS_DataEntry.sp_AS_11002_Equipment_ItemRow)((DataRowView)(spAS11002EquipmentItemBindingSource.CurrencyManager.Current)).Row).EquipmentCategory;
                        equipID = ((DataSet_AS_DataEntry.sp_AS_11002_Equipment_ItemRow)((DataRowView)(spAS11002EquipmentItemBindingSource.CurrencyManager.Current)).Row).EquipmentID.ToString();
                        if (equipType != 1 && equipType != 2)
                        {
                            XtraMessageBox.Show("Fuel Card allocations are restricted to Plant and Vehicles.", "Add Record Restiction", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        sp_AS_11044_Keeper_Allocation_ListTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_List, equipID, "view");
                        if (this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item.Count < 1)
                        {
                            XtraMessageBox.Show(("Please add Keepers to Equipment Reference " + gcRef + " before you proceed to add Fuel Cards"), "Add Record Restiction", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        
                    }

                    if (mode == FormMode.blockadd)
                    {
                        gcRef = "";
                    }
                    if (mode == FormMode.add)
                    {
                        if (numOfSelectedRows != 1)
                        {
                            XtraMessageBox.Show("Select one equipment record to add Fuel Cards, otherwise right click for block adding Fuel Cards.", "Add Fuel Card Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        gcRef = getEquipmentReference();
                    }

                    frm_AS_FuelCard_Edit fcChildForm = new frm_AS_FuelCard_Edit();
                    fcChildForm.MdiParent = this.MdiParent;
                    fcChildForm.GlobalSettings = this.GlobalSettings;
                    fcChildForm.strRecordIDs = strRecordsToLoad;
                    fcChildForm.strEquipmentIDs = strRecordIDs;
                    fcChildForm.formMode = (frm_AS_FuelCard_Edit.FormMode)mode;
                    fcChildForm.strFormMode = (mode.ToString()).ToLower();
                    fcChildForm.strCaller = frmCaller;
                    fcChildForm.strGCReference = gcRef;
                    if (mode == FormMode.blockadd)
                    {
                        fcChildForm.intRecordCount = numOfSelectedRows;
                    }
                    else
                    {
                        fcChildForm.intRecordCount = intCount;
                    }
                    fcChildForm.FormPermissions = this.FormPermissions;
                    SplashScreenManager splashScreenManager10 = new SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    fcChildForm.splashScreenManager = splashScreenManager10;
                    fcChildForm.splashScreenManager.ShowWaitForm();
                    fcChildForm.Show();
                    break;
                #endregion

                #region Road Tax

                case 19:     //  Road Tax
                    view = (GridView)gridControl.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;

                    if (mode == FormMode.edit || mode == FormMode.blockedit)
                    {
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Road Tax Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            DataRow dr = view.GetDataRow(intRowHandle);

                            strRecordsToLoad += (((DataSet_AS_DataEntry.sp_AS_11096_Road_Tax_ItemRow)(dr)).RoadTaxID).ToString() + ',';
                        }
                    }


                    //if (mode == FormMode.blockadd)
                    //{
                    //    XtraMessageBox.Show("Blockadd is restricted.", "Blockadd Road Tax Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    //    return;
                    //}
                    if (mode == FormMode.add)
                    {
                        if (strRecordIDs == "")
                        {
                            XtraMessageBox.Show("Please select an equipment to allocate Road Tax before proceeding.", "Add Road Tax Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                    }
                    cancelChildLoad = false;
                    gcRef = "";
                    if (mode == FormMode.add)
                    {
                        if (numOfSelectedRows == 0)
                        {
                            XtraMessageBox.Show("Select one equipment record or one type of equipment to add Road Tax.", "Add Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        if (numOfSelectedRows > 1)//block add option
                        {
                            string strTypes = "You have selected multiple records, do you want to block add the road tax ?";
                            switch (XtraMessageBox.Show(strTypes, "Block Add Option", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1))
                            {
                                case DialogResult.No:
                                    // cancel operation //
                                    XtraMessageBox.Show("Please select the one record to proceed", "Block Add Option Cancelled", MessageBoxButtons.OK);
                                    cancelChildLoad = true;
                                    break;
                                case DialogResult.Yes:
                                    // change to block add //
                                    mode = FormMode.blockadd;
                                    break;
                            }

                        }
                        if (cancelChildLoad)
                        {
                            return;
                        }

                        //int equipType = 0;
                        //string equipID = "";
                        gcRef = getEquipmentReference();

                    //    int[] intEquipRowhandles = equipmentGridView.GetSelectedRows();
                    //    foreach (int intRowHandle in intEquipRowhandles)
                    //    {
                    //        DataRow dr = equipmentGridView.GetDataRow(intRowHandle);
                    //        gcRef = (((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentReference).ToString();

                    //        //restrict  addition to plant and vehicle

                    //        equipType = Convert.ToInt32(((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentCategoryID);
                    //        if (equipType != 1 && equipType != 2)
                    //        {
                    //            XtraMessageBox.Show("Road Tax allocations are restricted to Plant and Vehicles.", "Add Record Restiction", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    //            return;
                    //        }


                    //        equipID = (((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentID).ToString();
                           
                    //    }
                    }

                    if (mode == FormMode.blockadd)
                    {
                        gcRef = "";
                    }
                    if (mode == FormMode.add)
                    {
                        if (numOfSelectedRows != 1)
                        {
                            XtraMessageBox.Show("Select one equipment record to add Road Tax, otherwise right click for block adding Road Tax.", "Add Road Tax Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        gcRef = getEquipmentReference();
                    }

                    frm_AS_Road_Tax_Edit rtChildForm = new frm_AS_Road_Tax_Edit();
                    rtChildForm.MdiParent = this.MdiParent;
                    rtChildForm.GlobalSettings = this.GlobalSettings;
                    rtChildForm.strRecordIDs = strRecordsToLoad;
                    rtChildForm.strEquipmentIDs = strRecordIDs;
                    rtChildForm.formMode = (frm_AS_Road_Tax_Edit.FormMode)mode;
                    rtChildForm.strFormMode = (mode.ToString()).ToLower();
                    rtChildForm.strCaller = frmCaller;
                    rtChildForm.strGCReference = gcRef;
                    if (mode == FormMode.blockadd)
                    {
                        rtChildForm.intRecordCount = numOfSelectedRows;
                    }
                    else
                    {
                        rtChildForm.intRecordCount = intCount;
                    }
                    rtChildForm.FormPermissions = this.FormPermissions;
                    SplashScreenManager splashScreenManager11 = new SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    rtChildForm.splashScreenManager = splashScreenManager11;
                    rtChildForm.splashScreenManager.ShowWaitForm();
                    rtChildForm.Show();
                    break;
                #endregion

                #region Tracker Details

                case 4:     //  Tracker
                    if (((DataSet_AS_DataEntry.sp_AS_11002_Equipment_ItemRow)((DataRowView)(spAS11002EquipmentItemBindingSource.CurrencyManager.Current)).Row).EquipmentCategory != 1
                        || ((DataSet_AS_DataEntry.sp_AS_11002_Equipment_ItemRow)((DataRowView)(spAS11002EquipmentItemBindingSource.CurrencyManager.Current)).Row).EquipmentCategory != 2)
                    {
                        XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Verilocation Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                    }
                    view = (GridView)gridControl.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;

                    if (mode == FormMode.edit)
                    {
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Verilocation Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            DataRow dr = view.GetDataRow(intRowHandle);

                            strRecordsToLoad += (((DataSet_AS_DataEntry.sp_AS_11023_Tracker_ListRow)(dr)).TrackerInformationID).ToString() + ',';
                        }
                    }

                    if (mode == FormMode.blockedit)
                    {
                        XtraMessageBox.Show("Blockedit is restricted.", "Blockedit Verilocation Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }

                    if (mode == FormMode.blockadd)
                    {
                        XtraMessageBox.Show("Blockadd is restricted.", "Blockadd Verilocation Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    if (mode == FormMode.add)
                    {
                        if (strRecordIDs == "")
                        {
                            XtraMessageBox.Show("Please select an equipment to allocate Tracker before proceeding.", "Add Tracker(Verilocation) Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                    }
                    cancelChildLoad = false;
                    gcRef = "";
                    if (mode == FormMode.add)
                    {
                        if (numOfSelectedRows == 0)
                        {
                            XtraMessageBox.Show("Select one equipment record or one type of equipment to add Tracker.", "Add Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        if (numOfSelectedRows > 1)//block add option
                        {
                            string strTypes = "You have selected multiple records, block add is restricted";
                            XtraMessageBox.Show(strTypes, "Block Add Restricted", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            cancelChildLoad = true;
                        }
                        if (cancelChildLoad)
                        {
                            return;
                        }

                        int equipType = 0;
                        string equipID = "";
                        
                        gcRef = getEquipmentReference();
                        equipType = ((DataSet_AS_DataEntry.sp_AS_11002_Equipment_ItemRow)((DataRowView)(spAS11002EquipmentItemBindingSource.CurrencyManager.Current)).Row).EquipmentCategory;
                            if (equipType != 1 && equipType != 2)
                            {
                                XtraMessageBox.Show("Verilocation Tracker allocations are restricted to Plant and Vehicles.", "Add Record Restiction", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                return;
                            }

                         equipID = (((DataSet_AS_DataEntry.sp_AS_11002_Equipment_ItemRow)((DataRowView)(spAS11002EquipmentItemBindingSource.CurrencyManager.Current)).Row).EquipmentID).ToString();
                    }

                    if (mode == FormMode.blockadd)
                    {
                        gcRef = "";
                    }
                    if (mode == FormMode.add)
                    {
                        if (numOfSelectedRows != 1)
                        {
                            XtraMessageBox.Show("Select one equipment record to add Verilocation tracker.", "Add Verilocation Tracker Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        getEquipmentReference();
                    }

                    //frm_AS_Tracker_Edit trChildForm = new frm_AS_Tracker_Edit();
                    //trChildForm.MdiParent = this.MdiParent;
                    //trChildForm.GlobalSettings = this.GlobalSettings;
                    //trChildForm.strRecordIDs = strRecordsToLoad;
                    //trChildForm.strEquipmentIDs = strRecordIDs;
                    //trChildForm.formMode = (frm_AS_Tracker_Edit.FormMode)mode;
                    //trChildForm.strFormMode = (mode.ToString()).ToLower();
                    //trChildForm.strCaller = frmCaller;
                    //trChildForm.strGCReference = gcRef;
                    //if (mode == FormMode.blockadd)
                    //{
                    //    trChildForm.intRecordCount = numOfSelectedRows;
                    //}
                    //else
                    //{
                    //    trChildForm.intRecordCount = intCount;
                    //}
                    //trChildForm.FormPermissions = this.FormPermissions;
                    //SplashScreenManager splashScreenManager12 = new SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    //trChildForm.splashScreenManager = splashScreenManager12;
                    //trChildForm.splashScreenManager.ShowWaitForm();
                    //trChildForm.Show();
                    break;
                #endregion

                default:
                    MessageBox.Show("Failed to load edit form", "Error Loading Edit form", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
            }
        }

        private string getEquipmentReference()
        {
            return ((DataSet_AS_DataEntry.sp_AS_11002_Equipment_ItemRow)((DataRowView)(spAS11002EquipmentItemBindingSource.CurrencyManager.Current)).Row).EquipmentReference;
        }
        
        private void ProcessPermissionsForForm()
        {
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, FormID, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 19:  // RoadTax //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddRoadTax = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditRoadTax = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteRoadTax = true;
                        }
                        break;
                    case 4:  // Tracker //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddTracker = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditTracker = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteTracker = true;
                        }
                        break;
                   
                    case 7:  // Billings //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddBilling = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditBilling = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteBilling = true;
                        }
                        break;
                    case 8:  // Keeper //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddKeeper = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditKeeper = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteKeeper = true;
                        }
                        break;
                    case 9:  // Transactions //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddTransactions = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditTransactions = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteTransactions = true;
                        }
                        break;
                    case 10:  // Reminders //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddReminders = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditReminders = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteReminders = true;
                        }
                        break;
                    case 11:  // Depreciation //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddDepreciation = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditDepreciation = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteDepreciation = true;
                        }
                        break;
                    case 12:  // Incident //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddIncident = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditIncident = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteIncident = true;
                        }
                        break;
                    case 13:  // ServiceData //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddServiceData = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditServiceData = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteServiceData = true;
                        }
                        break;
                    case 14:  // Cover //13:  // Servicedata //12:  // Incident // 11:  // Depreciation //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddCover = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditCover = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteCover = true;
                        }
                        break;
                    case 15:  // Work //14:  // Cover //13:  // Servicedata //12:  // Incident // 11:  // Depreciation //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddWorkDetail = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditWorkDetail = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteWorkDetail = true;
                        }
                        break;
                    case 16:  // Purpose //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddPurpose = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditPurpose = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeletePurpose = true;
                        }
                        break;
                    case 17:  // FuelCard //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddFuelCard = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditFuelCard = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteFuelCard = true;
                        }
                        break;
                    case 18:  // Speeding //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddSpeeding = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditSpeeding = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteSpeeding = true;
                        }
                        break;
                }
            }
        }
        
        internal void manufacturerIDDescription(string  manufacturerIDEquipType)
        {
            switch (manufacturerIDEquipType)
            {
                case "1":
                    lblManufacturerID.Text = "VIN Number:";
                    break;
                case "3":
                case "4":
                case "5":
                    this.lblManufacturerID.Text = "ICT Tag:";
                    break;
                case "6":
                    this.lblManufacturerID.Text = "Unique Item Description:";
                    break;               
                default:
                    this.lblManufacturerID.Text = "Manufacturer ID:";
                    break;
            }
        }

        private void block_EditDisableEquipment()
        {
            txtEquipmentReference.Properties.ReadOnly = true;
            lueEquipment_Category.Properties.ReadOnly = true;
            txtManufacturer_ID.Properties.ReadOnly = true;
            lueMake.Properties.ReadOnly = true;
            lueModel.Properties.ReadOnly = true;
            lueSupplier.Properties.ReadOnly = true;
            lueOwnership_Status.Properties.ReadOnly = true;
            lueAvailability.Properties.ReadOnly = true;
            lueGc_Marked.Properties.ReadOnly = true;
            ceArchive.Properties.ReadOnly = true;
            meNotes.Properties.ReadOnly = true;
            txtEx_Asset_ID.Properties.ReadOnly = true;
            txtSage_Asset_Ref.Properties.ReadOnly = true;
            deDepreciation_Start_Date.Properties.ReadOnly = true;
        }

        private void block_EditDisableVehicle()
        {
            txtRegistration_Plate.Properties.ReadOnly = true;
            txtLog_Book_Number.Properties.ReadOnly = true;
            txtRadioCode.Properties.ReadOnly = true;
            txtKeyCode.Properties.ReadOnly = true;
            txtElectronicCode.Properties.ReadOnly = true;
            txtModel_Specifications.Properties.ReadOnly = true;
            spnCurrentMileage.Properties.ReadOnly = true;
        }

         private void block_EditDisableGadget()
        {
            //txtIMEI.Properties.ReadOnly = true;
            
            //txtIMEI.EditValue = null;
            lueGadget_Type_ID.EditValue = null;
            lueGadgetCasing_ID.EditValue = null;
        }

        private void block_EditDisableHardware()
        {
            lueHardware_Type_ID.EditValue = null;
            lueHardwareSpecification_ID.EditValue = null;
            txtHardwareDefault_Password.EditValue = null;
        }

        private void block_EditDisableSoftware()
        {
            txtSoftwareLicence.Properties.ReadOnly = true;

            txtSoftwareLicence.EditValue = null;
            txtSoftwareLicence_Key.EditValue = null;
            lueSoftwareAcquisation_Method_ID.EditValue = null;
            lueSoftwareLicence_Type_ID.EditValue = null;
            lueSoftwareParent_Program.EditValue = null;
            lueSoftwareProduct_ID.EditValue = null;
            lueSoftwareVersion_ID.EditValue = null;
            spnSoftwareQuantity_Limit.EditValue = null;
            deSoftwareYear.EditValue = null;
            deSoftwareValid_Until.EditValue = null;
        }

        private void block_EditDisableOffice()
        {
            lueOfficeCategoryID.EditValue = null;
        }

        private void ClearErrors(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is LookUpEdit)
                    dxErrorProviderEquipment.SetError(item2, "");
                if (item2 is TextEdit)
                    dxErrorProviderEquipment.SetError(item2, "");
                if (item2 is DateEdit)
                    dxErrorProviderEquipment.SetError(item2, "");
                if (item2 is ColorPickEdit)
                    dxErrorProviderEquipment.SetError(item2, "");
                if (item2 is SpinEdit)
                    dxErrorProviderEquipment.SetError(item2, "");
                if (item2 is ContainerControl)
                    this.ClearErrors(item.Controls);
                if (item2 is DevExpress.XtraDataLayout.DataLayoutControl)
                    this.ClearErrors(item.Controls);

            }
        }

        internal void RefreshModel()
        {
            sp_AS_11017_Model_ListTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11017_Model_List, "", "view"); //Model
            if (lueMake.EditValue == null ? false : true)
            {
                this.spAS11017ModelListBindingSource.Filter = "Make = " + lueMake.EditValue;

                if (this.spAS11017ModelListBindingSource.Count == 0)
                {
                    lueModel.Properties.NullValuePrompt = "--No Models Available For Selected Make, Please Add--";
                    lueModel.Properties.AllowFocused = false;
                }
                else
                {
                    lueModel.Properties.NullValuePrompt = "--Please Select a Model--";
                    lueModel.Properties.AllowFocused = true;
                }
            }
            else
            {
                this.spAS11017ModelListBindingSource.Filter = "Make = 0";
                lueModel.Properties.NullValuePrompt = "--No Models Available, Please Select Make Before Proceeding--";
            }

            
        }

        internal void RefreshMake()
        {
            sp_AS_11014_Make_ListTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11014_Make_List, "", "view"); //Make
            if (lueEquipment_Category.EditValue == null ? false : true)
            {
                this.spAS11014MakeListBindingSource.Filter = "EquipmentCategory = " + lueEquipment_Category.EditValue;

                if (this.spAS11014MakeListBindingSource.Count == 0)
                {
                    lueMake.Properties.NullValuePrompt = "--No Makes Available For Selected Category, Please Add--";
                    lueMake.Properties.AllowFocused = false;
                }
                else
                {
                    lueMake.Properties.NullValuePrompt = "--Please Select a Make--";
                    lueMake.Properties.AllowFocused = true;
                }
            }
            else
            {
                this.spAS11014MakeListBindingSource.Filter = "EquipmentCategory = 0";
                lueMake.Properties.NullValuePrompt = "--No Makes Available, Please Select Category Before Proceeding--";
            }
            
        }

        private void RefreshSupplier()
        {
            sp_AS_11020_Supplier_ListTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11020_Supplier_List, "", "view"); //Supplier
            if (this.spAS11020SupplierListBindingSource.Count == 0)
            {
                lueSupplier.Properties.NullValuePrompt = "--No Suppliers Available, Please Add Suppliers--";
                lueSupplier.Properties.AllowFocused = false;
            }
            else
            {
                lueSupplier.Properties.NullValuePrompt = "--Please Select a Supplier--";
                lueSupplier.Properties.AllowFocused = true;
                //Filter suppliers based on caller
                string strSupplierFilter = "";
                switch (strCaller)
                {
                    case "frm_AS_Fleet_Manager":
                        strSupplierFilter = "[VehicleSupplier] = True OR [PlantSupplier] = True";
                        break;
                    case "frm_AS_Office_Manager":
                        strSupplierFilter = "[OfficeSupplier] = True";
                        break;
                    case "frm_AS_Equipment_Manager":
                        strSupplierFilter = "";
                        break;
                    case "frm_AS_Gadget_Manager":
                        strSupplierFilter = "[GadgetSupplier] = True";
                        break;
                    case "frm_AS_Hardware_Manager":
                        strSupplierFilter = "[HardwareSupplier] = True";
                        break;
                    case "frm_AS_Software_Manager":
                        strSupplierFilter = "[SoftwareSupplier] = True";
                        break;
                    default:
                        strSupplierFilter = "";
                        break;
                }
                spAS11020SupplierListBindingSource.Filter = strSupplierFilter;
            }
        }

        private void Open_Child_Forms(string formName, FormMode formMode, string frmCaller)
        {
            System.Reflection.MethodInfo method = null; 
            switch (formName)
            {
                case "frm_Makes_Model_Wizard":             
                    frm_Makes_Model_Wizard wChildForm = new frm_Makes_Model_Wizard();
                    wChildForm.GlobalSettings = this.GlobalSettings;
                    wChildForm.strRecordIDs = "";
                    wChildForm.strFormMode = "add";
                    wChildForm.strCaller = "frmMain2";
                    //mChildForm.FormPermissions = this.FormPermissions;
                    DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    wChildForm.splashScreenManager = splashScreenManager1;
                    wChildForm.splashScreenManager.ShowWaitForm();
                    wChildForm.ShowDialog();
                    break;
                //case "frm_AS_Makes_Models_Wizard":
                //    frm_AS_Makes_Models_Wizard mChildForm = new frm_AS_Makes_Models_Wizard();
           
                //    mChildForm.GlobalSettings = this.GlobalSettings;
                //    mChildForm.strRecordIDs = "";
                //    mChildForm.strFormMode = formMode.ToString();
                //    mChildForm.strCaller = frmCaller;
                //    mChildForm.intRecordCount = 0;
                //    mChildForm.FormPermissions = this.FormPermissions;
                //    DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                //    mChildForm.splashScreenManager = splashScreenManager1;
                //    mChildForm.splashScreenManager.ShowWaitForm();
                    
                  //  mChildForm.ShowDialog();
                  ////  method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                  //  //if (method != null) 
                  //  //    method.Invoke(mChildForm, new object[] { null });
                  //  break;
                case "frm_AS_Supplier_Edit":
                    frm_AS_Supplier_Edit sChildForm = new frm_AS_Supplier_Edit();
                    sChildForm.GlobalSettings = this.GlobalSettings;
                    sChildForm.MaximumSize = new System.Drawing.Size(1065, 586);
                    sChildForm.MinimumSize = new System.Drawing.Size(1065, 586);
                    sChildForm.strRecordIDs = "";
                    sChildForm.strFormMode = formMode.ToString();
                    sChildForm.strCaller = frmCaller;
                    sChildForm.intRecordCount = 0;
                    sChildForm.FormPermissions = this.FormPermissions;
                    DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager2 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    sChildForm.splashScreenManager = splashScreenManager2;
                    sChildForm.splashScreenManager.ShowWaitForm();
                    sChildForm.ShowDialog();
                    //method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    //if (method != null) method.Invoke(sChildForm, new object[] { null });
                    break;
                default:
                    MessageBox.Show("Failed to load the form.", "Error Loading Form", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;

            }
        }

        private DateTime getCurrentDate()
        {
            DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter GetDate = new DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter();
            GetDate.ChangeConnectionString(strConnectionString);
            GetDate.sp_AS_11138_Get_Server_Date();
            DateTime d = DateTime.Parse(GetDate.sp_AS_11138_Get_Server_Date().ToString());
            return d;
        }

        private void addNewEquipmentRow()
        {
            try
            {
                int intExchequerCategory = 0;

                if (intExchequerCategoryMVG != 0)
                {
                    intExchequerCategory = intExchequerCategoryMVG;
                }
                else
                {
                    sp_AS_11000_PL_Exchequer_CategoryTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Exchequer_Category, 54); //Exchequer Category
                    DataRow[] drExCats = this.dataSet_AS_DataEntry.sp_AS_11000_PL_Exchequer_Category.Select("[value] like 'none'");
                    if (drExCats.Length > 0)
                    {
                        foreach (DataRow drExCat in drExCats)
                        {
                            intExchequerCategory = ((DataSet_AS_DataEntry.sp_AS_11000_PL_Exchequer_CategoryRow)(drExCat)).PickListID;
                        }
                    }
                }                

                DataRow drNewRow;
                drNewRow = this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item.NewRow();
                drNewRow["Mode"] = "add";
                drNewRow["RecordID"] = "-1";
                if ((int)passedEquipType == 0)
                {
                    drNewRow["EquipmentCategory"] = "1";
                }
                else
                {
                    drNewRow["EquipmentCategory"] = (int)passedEquipType;
                }
                drNewRow["EquipmentReference"] = "";
                drNewRow["ManufacturerID"] = DBNull.Value;
                drNewRow["Make"] = DBNull.Value;
                drNewRow["Model"] = DBNull.Value;
                drNewRow["ExchequerID"] = "";
                drNewRow["ExchequerCategoryID"] = intExchequerCategory;
                drNewRow["Archive"] = false;
                sp_AS_11066_Depreciation_Settings_ItemTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_Item, "","");
                string depFilter = "";
                switch ((int)passedEquipType)
                {
                    case 1:
                    case 2:
                    case 4:
                    case 6:
                        depFilter = "SettingName = 'Default Setting'";
                        break;
                    case 3:
                        depFilter = "SettingName = 'Gadget Setting'";
                        break;
                    case 5:
                        depFilter = "SettingName = 'No Depreciation Required'";
                        break;
                    default:
                        depFilter = "SettingName = 'Default Setting'";
                        break;

                }
                
                DataRow[] rows = dataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_Item.Select(depFilter);
                if (rows.Length > 0)
                //if (this.dataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_Item.Count > 0)
                {
                    //DataRow row = this.dataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_Item.Rows[0];
                    DataRow row = rows[0];
                    if ((((DataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_ItemRow)(row)).MaximumOccurrence).ToString() != "")
                    {
                        drNewRow["MaximumOccurrence"] =  ((DataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_ItemRow)(row)).MaximumOccurrence;
                    }
                    else
                    {
                        drNewRow["MaximumOccurrence"] = 48;
                    }
                    if ((((DataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_ItemRow)(row)).DayOfMonthApplyDepreciation).ToString() != "")
                    {
                        drNewRow["DayOfMonthApplyDepreciation"] = ((DataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_ItemRow)(row)).DayOfMonthApplyDepreciation;
                    }
                    else
                    {
                        drNewRow["DayOfMonthApplyDepreciation"] = 1;
                    }

                    if ((((DataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_ItemRow)(row)).OccursUnitDescriptorID).ToString() != "")
                    {
                        drNewRow["OccursUnitDescriptor"] = ((DataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_ItemRow)(row)).OccursUnitDescriptorID;
                    }
                    else
                    {
                        drNewRow["OccursUnitDescriptor"] = 64;
                    }

                }
                else
                {
                    drNewRow["MaximumOccurrence"] = 48;
                    drNewRow["DayOfMonthApplyDepreciation"] = 1;
                    drNewRow["OccursUnitDescriptor"] = 64;
                }
                this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item.Rows.Add(drNewRow);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void RefreshChildBindingSource()
        {
            spAS11005VehicleItemBindingSource.CancelEdit();
            spAS11008PlantItemBindingSource.CancelEdit();
            spAS11026GadgetItemBindingSource.CancelEdit();
            spAS11032HardwareItemBindingSource.CancelEdit();
            spAS11035SoftwareItemBindingSource.CancelEdit();
            spAS11035SoftwareItemBindingSource.CancelEdit();
            sp_AS_11029_Office_ItemBindingSource.CancelEdit();
        }

        private void loadEquipment()
        {
            //load Equipment
            equipmentLayoutControlGroup.BeginUpdate();
            sp_AS_11002_Equipment_ItemTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item, strRecordIDs, strFormMode);
            equipmentLayoutControlGroup.EndUpdate();
          
            //load vehicle
            vehicleLayoutControlGroup.BeginUpdate();
            sp_AS_11005_Vehicle_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11005_Vehicle_Item, strRecordIDs, strFormMode);
            vehicleLayoutControlGroup.EndUpdate();

            //load plant and machinery
            plantLayoutControlGroup.BeginUpdate();
            sp_AS_11008_Plant_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11008_Plant_Item, strRecordIDs, strFormMode);
            plantLayoutControlGroup.EndUpdate();

            //load gadget
            gadgetLayoutControlGroup.BeginUpdate();
            sp_AS_11026_Gadget_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11026_Gadget_Item, strRecordIDs, strFormMode);
            gadgetLayoutControlGroup.EndUpdate();
           
            //load software
            softwareLayoutControlGroup.BeginUpdate();
            sp_AS_11035_Software_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11035_Software_Item, strRecordIDs, strFormMode);
            softwareLayoutControlGroup.EndUpdate();

            //load office
            officeLayoutControlGroup.BeginUpdate();
            sp_AS_11029_Office_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11029_Office_Item, strRecordIDs, strFormMode);
            officeLayoutControlGroup.EndUpdate();

            //load hardware
            hardwareLayoutControlGroup.BeginUpdate();
            sp_AS_11032_Hardware_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11032_Hardware_Item, strRecordIDs, strFormMode);
            hardwareLayoutControlGroup.EndUpdate();

        }
        
        private void spAS11002EquipmentItemBindingSource_CurrentChanged(object sender, EventArgs e)
        {
            
            LoadLinkedRecords((((DataRowView)(spAS11002EquipmentItemBindingSource.CurrencyManager.Current)).Row).ItemArray[0].ToString());
            Get_ChildData();
        }

        private void LoadLinkedRecords(string strLinkedRecordIDs)
        {
            if (strFormMode == "edit")
            {
                string[] t = splitStrRecords(PopupContainerEdit1_Get_Selected());
                ArrayList tabs = new ArrayList();
                foreach (string h in t)
                {
                    tabs.Add(Regex.Replace(h, @"\s+", ""));
                }
                if (tabs.Contains("NoLinkedDataFilter"))
                {
                    sp_AS_11041_Transaction_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11041_Transaction_Item, strLinkedRecordIDs, "view");
                    sp_AS_11044_Keeper_Allocation_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item, strLinkedRecordIDs, "view");
                    sp_AS_11050_Depreciation_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11050_Depreciation_Item, strLinkedRecordIDs, "view");
                    sp_AS_11078_Work_Detail_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11078_Work_Detail_Item, strLinkedRecordIDs, "view");
                    sp_AS_11075_Cover_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11075_Cover_Item, strLinkedRecordIDs, "view");
                    //sp_AS_11054_Service_Data_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11054_Service_Data_Item, strLinkedRecordIDs, "view");
                    sp_AS_11081_Incident_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11081_Incident_Item, strLinkedRecordIDs, "view");
                    sp_AS_11084_Purpose_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11084_Purpose_Item, strLinkedRecordIDs, "view");
                    sp_AS_11089_Fuel_Card_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11089_Fuel_Card_Item, strLinkedRecordIDs, "view");
                    sp_AS_11023_Tracker_ListTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11023_Tracker_List, strLinkedRecordIDs, "view");
                    sp_AS_11105_Speeding_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11105_Speeding_Item, strLinkedRecordIDs, "view");
                    //sp_AS_11096_Road_Tax_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11096_Road_Tax_Item, strLinkedRecordIDs, "view");
                    loadGridViewState();

                    //this.RefreshGridViewStateRoadTax.LoadViewInfo();
                    //this.RefreshGridViewStateSoftware.LoadViewInfo();
                    //this.RefreshGridViewStateTracker.LoadViewInfo();
                    //this.RefreshGridViewStateOffice.LoadViewInfo();
                    //this.RefreshGridViewStateBilling.LoadViewInfo();
                    //this.RefreshGridViewStateKeeper.LoadViewInfo();
                    //this.RefreshGridViewStateTransactions.LoadViewInfo();
                    //this.RefreshGridViewStateDepreciation.LoadViewInfo();
                    //this.RefreshGridViewStateCover.LoadViewInfo();
                    //this.RefreshGridViewStateServiceInterval.LoadViewInfo();
                    //this.RefreshGridViewStateWork.LoadViewInfo();
                    //this.RefreshGridViewStateIncident.LoadViewInfo();
                    //this.RefreshGridViewStatePurpose.LoadViewInfo();
                    //this.RefreshGridViewStateFuelCard.LoadViewInfo();
                    //this.RefreshGridViewStateSpeeding.LoadViewInfo();
                }
                else
                {
                    if (tabs.Contains("RoadTax"))
                    {
                        //sp_AS_11096_Road_Tax_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11096_Road_Tax_Item, strLinkedRecordIDs, "view");
                        //this.RefreshGridViewStateRoadTax.LoadViewInfo();
                    }                  

                    if (tabs.Contains("Verilocation"))
                    {
                        sp_AS_11023_Tracker_ListTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11023_Tracker_List, strLinkedRecordIDs, "view");
                    }                   

                    if (tabs.Contains("Keepers"))
                    {
                        sp_AS_11044_Keeper_Allocation_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item, strLinkedRecordIDs, "view");
                        this.RefreshGridViewStateKeeper.LoadViewInfo();
                    }

                    if (tabs.Contains("Transactions"))
                    {
                        sp_AS_11041_Transaction_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11041_Transaction_Item, strLinkedRecordIDs, "view");
                    }

                    if (tabs.Contains("DepreciationData"))
                    {
                        sp_AS_11050_Depreciation_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11050_Depreciation_Item, strLinkedRecordIDs, "view");
                    }

                    if (tabs.Contains("BillingDetails"))
                    {
                        sp_AS_11050_Depreciation_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11050_Depreciation_Item, strLinkedRecordIDs, "view");
                    }

                    if (tabs.Contains("PolicyandInsuranceCoverDetails"))
                    {
                        sp_AS_11075_Cover_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11075_Cover_Item, strLinkedRecordIDs, "view");
                    }

                    if (tabs.Contains("ServiceIntervalInformation"))
                    {
                        sp_AS_11054_Service_Data_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11054_Service_Data_Item, strLinkedRecordIDs, "view");
                    }

                    if (tabs.Contains("WorkOrderDetails"))
                    {
                        sp_AS_11078_Work_Detail_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11078_Work_Detail_Item, strLinkedRecordIDs, "view");
                    }

                    if (tabs.Contains("IncidentDetails"))
                    {
                        sp_AS_11081_Incident_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11081_Incident_Item, strLinkedRecordIDs, "view");
                    }

                    if (tabs.Contains("AssetPurpose"))
                    {
                        sp_AS_11084_Purpose_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11084_Purpose_Item, strLinkedRecordIDs, "view");
                    }

                    if (tabs.Contains("FuelCard"))
                    {
                        sp_AS_11089_Fuel_Card_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11089_Fuel_Card_Item, strLinkedRecordIDs, "view");
                    }

                    if (tabs.Contains("SpeedingData"))
                    {
                        sp_AS_11105_Speeding_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11105_Speeding_Item, strLinkedRecordIDs, "view");
                    }
                }

              
            }
        }

        private void PopulateEquipmentPickList()
        {
            sp_AS_11011_Equipment_Category_ListTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11011_Equipment_Category_List, "", "view"); //Equipment Category

            sp_AS_11000_PL_Exchequer_CategoryTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Exchequer_Category, 54); //Exchequer Category

            sp_AS_11000_PL_Equipment_GC_MarkedTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Equipment_GC_Marked, 3); //GC Marked
            sp_AS_11000_PL_Equipment_Ownership_StatusTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Equipment_Ownership_Status, 1); //Ownership Status
            sp_AS_11000_PL_Equipment_AvailabilityTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Equipment_Availability, 2); //Availability
            sp_AS_11000_PL_FuelTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Fuel, 5); //Fuel
            sp_AS_11000_PL_Plant_CategoryTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Plant_Category, 12); //Purpose
            sp_AS_11000_PL_Vehicle_Towbar_TypeTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Vehicle_Towbar_Type, 10); //Tow Bar Type
            sp_AS_11000_PL_Vehicle_CategoryTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Vehicle_Category, 33); //Vehicle Category

            sp_AS_11000_PL_Gadget_TypeTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Gadget_Type, 13); //Gadget_Type
            sp_AS_11000_PL_CasingTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Casing, 14); //Casing
            sp_AS_11000_PL_Hardware_TypeTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Hardware_Type, 15); //Hardware_Type
            sp_AS_11000_PL_Software_ProductTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Software_Product, 19); //Software_Product
            sp_AS_11000_PL_Software_VersionTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Software_Version, 20); //Software_Version

            sp_AS_11000_PL_Office_CategoryTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Office_Category, 27); //Office Category

            sp_AS_11000_PL_Software_Acquisation_MethodTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Software_Acquisation_Method, 21); //Software_Acquisation_Method

            sp_AS_11000_PL_Licence_TypeTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Licence_Type, 22); //Licence_Type

            sp_AS_11014_Make_ListTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11014_Make_List, "", "view"); //Make

            sp_AS_11017_Model_ListTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11017_Model_List, "", "view"); //Model

            sp_AS_11020_Supplier_ListTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11020_Supplier_List, "", "view"); //Supplier

            sp_AS_11000_PL_Unit_DescriptorTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Unit_Descriptor, 25); //Unit descriptor

            sp_AS_11066_Depreciation_Settings_ItemTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_Item, "", ""); //Depreciation setiing

            sp_AS_11141_Hardware_Specifications_ItemTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11141_Hardware_Specifications_Item, "", "");//Hardware Specifications
            //Filter types based on caller
            string strCategoryFilter = "";
            switch (strCaller)
            {
                case "frm_AS_Fleet_Manager":
                    strCategoryFilter = "[EquipmentCategory] = '1' OR [EquipmentCategory] = '2'";
                    break;
                case "frm_AS_Office_Manager":
                    strCategoryFilter = "[EquipmentCategory] = '6'";
                    break;
                case "frm_AS_Equipment_Manager":
                    strCategoryFilter = "";
                    break;
                case "frm_AS_Gadget_Manager":
                    strCategoryFilter = "[EquipmentCategory] = '3'";
                    break;
                case "frm_AS_Hardware_Manager":
                    strCategoryFilter = "[EquipmentCategory] = '4'";
                    break;
                case "frm_AS_Software_Manager":
                    strCategoryFilter = "[EquipmentCategory] = '5'";
                    break;
                default:
                    strCategoryFilter = "";
                    break;
            }
            spAS11011EquipmentCategoryListBindingSource.Filter = strCategoryFilter;
            //Filter suppliers based on caller
            string strSupplierFilter = "";
            switch (strCaller)
            {
                case "frm_AS_Fleet_Manager":
                    strSupplierFilter = "[VehicleSupplier] = True OR [PlantSupplier] = True";
                    break;
                case "frm_AS_Office_Manager":
                    strSupplierFilter = "[OfficeSupplier] = True";
                    break;
                case "frm_AS_Equipment_Manager":
                    strSupplierFilter = "";
                    break;
                case "frm_AS_Gadget_Manager":
                    strSupplierFilter = "[GadgetSupplier] = True";
                    break;
                case "frm_AS_Hardware_Manager":
                    strSupplierFilter = "[HardwareSupplier] = True";
                    break;
                case "frm_AS_Software_Manager":
                    strSupplierFilter = "[SoftwareSupplier] = True";
                    break;
                default:
                    strSupplierFilter = "";
                    break;
            }
            spAS11020SupplierListBindingSource.Filter = strSupplierFilter;
            //sp_AS_11023_Tracker_ListTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11023_Tracker_List, "", "view"); //Tracker Reference  

        }

        private string SaveChangesExtracted(out bool shouldReturn)
        {
            shouldReturn = false;

            if (dxErrorProviderEquipment.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProviderEquipment, equipmentDataLayoutControl);
                XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors +
                "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                shouldReturn = true;
                return "Error";
            }

            return String.Empty;
        }
        
        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //         
            EndEdit();

            this.Validate();
            ValidateChildren();
            bool shouldReturn;
            string result = SaveChangesExtracted(out shouldReturn);
            if (shouldReturn)
                return result;

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            EndEdit();

            CheckChangedTable(this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item);//Equipment Check
            CheckChangedTable(this.dataSet_AS_DataEntry.sp_AS_11005_Vehicle_Item);//Vehicle Check
            CheckChangedTable(this.dataSet_AS_DataEntry.sp_AS_11008_Plant_Item);//Plant Check
            CheckChangedTable(this.dataSet_AS_DataEntry.sp_AS_11026_Gadget_Item);//Gadget Check
            CheckChangedTable(this.dataSet_AS_DataEntry.sp_AS_11035_Software_Item);//Software Check
            CheckChangedTable(this.dataSet_AS_DataEntry.sp_AS_11032_Hardware_Item);//Hardware Check
            CheckChangedTable(this.dataSet_AS_DataEntry.sp_AS_11029_Office_Item);//Office Check

            try
            {
                // Insert and Update queries defined in Table Adapter //
                if (equipmentChanged)
                    {
                      this.sp_AS_11002_Equipment_ItemTableAdapter.Update(dataSet_AS_DataEntry);
                        equipmentChanged = false;
                    }
                if (vehicleChanged) //&& (strCaller == "frm_AS_Fleet_Manager" || strCaller == "frm_AS_Equipment_Manager"))
                    {
                        
                        if (strFormMode.ToLower() == "add")
                        {
                            //DataColumn equipID = this.dataSet_AS_DataEntry.sp_AS_11005_Vehicle_Item.EquipmentIDColumn;
                            //equipID.ReadOnly = false;
                            this.dataSet_AS_DataEntry.sp_AS_11005_Vehicle_Item.Rows[0]["EquipmentID"] = this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item[0].EquipmentID;                            
                            this.sp_AS_11005_Vehicle_ItemTableAdapter.Update(dataSet_AS_DataEntry);                        
                        }
                        else 
                        {
                            this.sp_AS_11005_Vehicle_ItemTableAdapter.Update(dataSet_AS_DataEntry);
                        }
                        vehicleChanged = false;
                    }

                if (plantChanged)
                    {
                        
                        if (strFormMode.ToLower() == "add")
                        {
                            this.dataSet_AS_DataEntry.sp_AS_11008_Plant_Item[0].EquipmentID = this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item[0].EquipmentID;
                            this.sp_AS_11008_Plant_ItemTableAdapter.Update(dataSet_AS_DataEntry);
                        }
                        else
                        {
                            this.sp_AS_11008_Plant_ItemTableAdapter.Update(dataSet_AS_DataEntry);
                        }
                        plantChanged = false;
                    }
                if (gadgetChanged)
                    {
                        if (strFormMode.ToLower() == "add")
                        {
                            this.dataSet_AS_DataEntry.sp_AS_11026_Gadget_Item[0].EquipmentID = this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item[0].EquipmentID;
                            this.sp_AS_11026_Gadget_ItemTableAdapter.Update(dataSet_AS_DataEntry);
                        }
                        else
                        {
                            this.sp_AS_11026_Gadget_ItemTableAdapter.Update(dataSet_AS_DataEntry);
                        }
                        
                        gadgetChanged = false;
                    }
                if (hardwareChanged)
                    {
                        if (strFormMode.ToLower() == "add")
                        {
                            this.dataSet_AS_DataEntry.sp_AS_11032_Hardware_Item[0].EquipmentID = this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item[0].EquipmentID;
                            this.sp_AS_11032_Hardware_ItemTableAdapter.Update(dataSet_AS_DataEntry);
                        }
                        else
                        {
                            this.sp_AS_11032_Hardware_ItemTableAdapter.Update(dataSet_AS_DataEntry);
                        }                        
                        hardwareChanged = false;
                    }
                if (softwareChanged)
                    {
                        if (strFormMode.ToLower() == "add")
                        {
                            this.dataSet_AS_DataEntry.sp_AS_11035_Software_Item[0].EquipmentID = this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item[0].EquipmentID;
                            this.sp_AS_11035_Software_ItemTableAdapter.Update(dataSet_AS_DataEntry);
                        }
                        else
                        {
                            this.sp_AS_11035_Software_ItemTableAdapter.Update(dataSet_AS_DataEntry);
                        }
                        
                        softwareChanged = false;
                    }
                if (officeChanged)
                    {
                        if (strFormMode.ToLower() == "add")
                        {
                            this.dataSet_AS_DataEntry.sp_AS_11029_Office_Item[0].EquipmentID = this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item[0].EquipmentID;
                            this.sp_AS_11029_Office_ItemTableAdapter.Update(dataSet_AS_DataEntry);
                        }
                        else
                        {
                            this.sp_AS_11029_Office_ItemTableAdapter.Update(dataSet_AS_DataEntry);
                        }
                        
                        officeChanged = false;
                    }
            }
            catch (Exception )//ex)
            {
                //fProgress.Close();
                //fProgress.Dispose();
                //var dialogTypeName = "System.Windows.Forms.PropertyGridInternal.GridErrorDlg";
                //var dialogType = typeof(Form).Assembly.GetType(dialogTypeName);
                //var dialog = (Form)Activator.CreateInstance(dialogType, new PropertyGrid());
                //dialog.Text = "Error Saving Changes";
                //dialogType.GetProperty("Details").SetValue(dialog, ex.Message, null);
                //dialogType.GetProperty("Message").SetValue(dialog, String.Format("An error occurred while saving the record changes [{0}]!\n\nTry saving again - if the problem persists, contact Technical Support.", ex.Message), null);
                //var msgResult = dialog.ShowDialog();
                ////XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                //return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }
            string strNewID = "";
            string Category = "";
            DataRowView currentRow = (DataRowView)spAS11002EquipmentItemBindingSource.Current;
            if (currentRow != null)
            {
                strNewID = this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item[0].EquipmentID + ";";
                Category = this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item[0].EquipmentCategory.ToString();
            }
            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //     
            UpdateManagerScreen(strNewID, Category);
            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();

            if (P11dManagerPromptRequired)
            {
                int intCategory = 0;
                int.TryParse(Category, out intCategory);

                if ( intCategory == 1 || intCategory == 7)
                {
                    switch (XtraMessageBox.Show("Would you like to view the P11D Manager to email changes?", "Open P11D", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1))
                    {
                        case DialogResult.No:
                            // cancel operation //
                            break;
                        case DialogResult.Yes:
                            OpenP11DManagerViaMain();
                            break;
                    }
                }            
            }
            P11dManagerPromptRequired = false;

            return "";  // No problems //
        }

        private void OpenP11DManagerViaMain()
        {
            // Open Notification Manager via main //
            if (Application.OpenForms["frmMain2"] != null)
            {
                (Application.OpenForms["frmMain2"] as frmMain2).Open_Screen(1121);
            }

            if (Application.OpenForms["frm_AS_P11D_Manager"] != null)
            {
                (Application.OpenForms["frm_AS_P11D_Manager"] as frm_AS_P11D_Manager).LoadAdapters();
            }
        }

        private void UpdateManagerScreen(string strNewID, string Category)
        {
            switch (strCaller)
            {
                case "frm_AS_Equipment_Manager":
                    if (formMode == FormMode.add)
                    {
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Equipment_Manager).UpdateFormRefreshStatus(1, strNewID, strNewID, Category);
                            (Application.OpenForms[strCaller] as frm_AS_Equipment_Manager).frmActivated();
                        }
                    }
                    else if (formMode == FormMode.edit || formMode == FormMode.blockedit)
                    {
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Equipment_Manager).UpdateFormRefreshStatus(1, strEquipmentIDs, strRecordIDs, Category);
                            (Application.OpenForms[strCaller] as frm_AS_Equipment_Manager).frmActivated();
                        }
                    }
                    else
                    {
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Equipment_Manager).UpdateFormRefreshStatus(1, strRecordIDs, "", Category);
                            (Application.OpenForms[strCaller] as frm_AS_Equipment_Manager).frmActivated();
                        }
                    }
                    break;
                case "frm_AS_Office_Manager":
                    if (formMode == FormMode.add)
                    {
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Office_Manager).UpdateFormRefreshStatus(1, strNewID, strNewID, Category);
                            (Application.OpenForms[strCaller] as frm_AS_Office_Manager).frmActivated();
                        }
                    }
                    else if (formMode == FormMode.edit || formMode == FormMode.blockedit)
                    {
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Office_Manager).UpdateFormRefreshStatus(1, strEquipmentIDs, strRecordIDs, Category);
                            (Application.OpenForms[strCaller] as frm_AS_Office_Manager).frmActivated();
                        }
                    }
                    else
                    {
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Office_Manager).UpdateFormRefreshStatus(1, strRecordIDs, "", Category);
                            (Application.OpenForms[strCaller] as frm_AS_Office_Manager).frmActivated();
                        }
                    }
                    break;
                case "frm_AS_Fleet_Manager":
                    if (formMode == FormMode.add)
                    {
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Fleet_Manager).UpdateFormRefreshStatus(1, strNewID, strNewID, Category);
                            (Application.OpenForms[strCaller] as frm_AS_Fleet_Manager).frmActivated();
                        }
                    }
                    else if (formMode == FormMode.edit || formMode == FormMode.blockedit)
                    {
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Fleet_Manager).UpdateFormRefreshStatus(1, strEquipmentIDs, strRecordIDs, Category);
                            (Application.OpenForms[strCaller] as frm_AS_Fleet_Manager).frmActivated();
                        }
                    }
                    else
                    {
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Fleet_Manager).UpdateFormRefreshStatus(1, strRecordIDs, "", Category);
                            (Application.OpenForms[strCaller] as frm_AS_Fleet_Manager).frmActivated();
                        }
                    }
                    break;
                
                case "frm_AS_Gadget_Manager":
                    if (formMode == FormMode.add)
                    {
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Gadget_Manager).UpdateFormRefreshStatus(1, strNewID, strNewID, Category);
                            (Application.OpenForms[strCaller] as frm_AS_Gadget_Manager).frmActivated();
                        }
                    }
                    else if (formMode == FormMode.edit || formMode == FormMode.blockedit)
                    {
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Gadget_Manager).UpdateFormRefreshStatus(1, strEquipmentIDs, strRecordIDs, Category);
                            (Application.OpenForms[strCaller] as frm_AS_Gadget_Manager).frmActivated();
                        }
                    }
                    else
                    {
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Gadget_Manager).UpdateFormRefreshStatus(1, strRecordIDs, "", Category);
                            (Application.OpenForms[strCaller] as frm_AS_Gadget_Manager).frmActivated();
                        }
                    }
                    break;
                case "frm_AS_Hardware_Manager":
                    if (formMode == FormMode.add)
                    {
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Hardware_Manager).UpdateFormRefreshStatus(1, strNewID, strNewID, Category);
                            (Application.OpenForms[strCaller] as frm_AS_Hardware_Manager).frmActivated();
                        }
                    }
                    else if (formMode == FormMode.edit || formMode == FormMode.blockedit)
                    {
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Hardware_Manager).UpdateFormRefreshStatus(1, strEquipmentIDs, strRecordIDs, Category);
                            (Application.OpenForms[strCaller] as frm_AS_Hardware_Manager).frmActivated();
                        }
                    }
                    else
                    {
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Hardware_Manager).UpdateFormRefreshStatus(1, strRecordIDs, "", Category);
                            (Application.OpenForms[strCaller] as frm_AS_Hardware_Manager).frmActivated();
                        }
                    }
                    break;
                case "frm_AS_Software_Manager":
                    if (formMode == FormMode.add)
                    {
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Software_Manager).UpdateFormRefreshStatus(1, strNewID, strNewID, Category);
                            (Application.OpenForms[strCaller] as frm_AS_Software_Manager).frmActivated();
                        }
                    }
                    else if (formMode == FormMode.edit || formMode == FormMode.blockedit)
                    {
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Software_Manager).UpdateFormRefreshStatus(1, strEquipmentIDs, strRecordIDs, Category);
                            (Application.OpenForms[strCaller] as frm_AS_Software_Manager).frmActivated();
                        }
                    }
                    else
                    {
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Software_Manager).UpdateFormRefreshStatus(1, strRecordIDs, "", Category);
                            (Application.OpenForms[strCaller] as frm_AS_Software_Manager).frmActivated();
                        }
                    }
                    break;
            }

        }

        private string[] splitStrRecords(string t)
        {
            char[] delimiters = new char[] { ',', ';' };
            string[] parts = t.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            return parts;
        }

        //private string[] splitLinkedRecords(string t)
        //{
        //    char[] delimiters = new char[] { ';', ',' };
        //    string[] parts = t.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
        //    return parts;
        //}

        private void Attach_EditValueChanged_To_Children(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
                if (item2 is DevExpress.XtraDataLayout.DataLayoutControl) this.Attach_EditValueChanged_To_Children(item.Controls);

            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void SetEditorButtons()
        {
            // Get Form Permissions //
        }

        private Boolean SetChangesPendingLabel()
        {
            EndEdit();

            DataSet dsChanges = this.dataSet_AS_DataEntry.GetChanges();
            if (dsChanges != null) 
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "block_add" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;

                if (strFormMode.ToLower() == "add")
                {
                    bbiFormSave.Enabled = true;
                }
                return false;
            }

           
        }

        private void EndEdit()
        {
            spAS11002EquipmentItemBindingSource.EndEdit();
            spAS11005VehicleItemBindingSource.EndEdit();
            spAS11008PlantItemBindingSource.EndEdit();
            spAS11026GadgetItemBindingSource.EndEdit();
            sp_AS_11029_Office_ItemBindingSource.EndEdit();
            spAS11032HardwareItemBindingSource.EndEdit();
            spAS11035SoftwareItemBindingSource.EndEdit();
        }

        private string CheckForPendingSave()
        {
            EndEdit();

            string strMessage = "";
            string strMessageEquip = CheckTablePendingSave(this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item, " on the General Equipment Tab Page ");//Equipment Check
            string strMessageVehicle = CheckTablePendingSave(this.dataSet_AS_DataEntry.sp_AS_11005_Vehicle_Item, " on the Vehicle Tab Page ");//Vehicle Check
            string strMessagePlant = CheckTablePendingSave(this.dataSet_AS_DataEntry.sp_AS_11008_Plant_Item, " on the Plant Tab Page ");//Plant Check
            string strMessageGadget = CheckTablePendingSave(this.dataSet_AS_DataEntry.sp_AS_11026_Gadget_Item, " on the Gadget Tab Page ");//Gadget Check
            string strMessageSoftware = CheckTablePendingSave(this.dataSet_AS_DataEntry.sp_AS_11035_Software_Item, " on the Software Tab Page ");//Software Check
            string strMessageHardware = CheckTablePendingSave(this.dataSet_AS_DataEntry.sp_AS_11032_Hardware_Item, " on the Hardware Tab Page ");//Hardware Check
            string strMessageOffice = CheckTablePendingSave(this.dataSet_AS_DataEntry.sp_AS_11029_Office_Item, " on the Office Tab Page ");//Office Check

            if (strMessageEquip != "" || strMessageVehicle != "" || strMessagePlant != "" || strMessageGadget != "" || strMessageSoftware != "" || strMessageHardware != "" || strMessageOffice != "")
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (strMessageEquip != "") strMessage += strMessageEquip;
                if (strMessageVehicle != "") strMessage += strMessageVehicle;
                if (strMessagePlant != "") strMessage += strMessagePlant;
                if (strMessageGadget != "") strMessage += strMessageGadget;
                if (strMessageSoftware != "") strMessage += strMessageSoftware;
                if (strMessageHardware != "") strMessage += strMessageHardware;
                if (strMessageOffice != "") strMessage += strMessageOffice;
            }
            return strMessage;
        }

        private string CheckTablePendingSave(DataTable dt, string formArea)
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;

  
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                switch (dt.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";

            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)" + formArea + "\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)" + formArea + "\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)" + formArea + "\n";
            }
            return strMessage;
        }

        private void CheckChangedTable(DataTable dt)
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                switch (dt.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }

            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                IdentifyChangedBindingSource(dt);
            }
        }
        
        private void IdentifyChangedBindingSource(DataTable dt)
        {

            switch (dt.TableName)
            {
                case "sp_AS_11002_Equipment_Item"://Equipment
                    equipmentChanged = true;
                    break;
                case "sp_AS_11005_Vehicle_Item"://Vehicle
                    vehicleChanged = true;
                    break;
                case "sp_AS_11008_Plant_Item": //Plant
                    plantChanged = true;
                    break;
                case "sp_AS_11026_Gadget_Item": //Gadget
                    gadgetChanged = true;
                    break;
                case "sp_AS_11035_Software_Item": //Software
                    softwareChanged = true;
                    break;
                case "sp_AS_11032_Hardware_Item": //Hardware
                    hardwareChanged = true;
                    break;
                case "sp_AS_11029_Office_Item": //Office
                    officeChanged = true;
                    break;

            }
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            ConfigureFormAccordingToMode();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
            bool_FormLoading = false;

        }

        #region ValidateMethod

        private bool validateDateEdit(DateEdit dateEdit)
        {
            bool valid = true;
            if (bool_FormLoading)
                return valid;
            string ErrorText = "Please Enter a Valid Date.";
            if (dateEdit.Tag != null)
            {
                ErrorText = "Please Enter a valid " + dateEdit.Tag.ToString() + ".";
            }
            //Check if date is after 

         
            return valid;
        }

        private bool validateLookupEdit(LookUpEdit lookupEdit, EquipmentType equipmentType)
        {

            bool valid = true;
            if (bool_FormLoading)
                return valid;

            if (equipmentType != EquipmentType.None)
            {
                if ((int)equipmentType != Convert.ToInt32(lueEquipment_Category.EditValue))
                {
                    dxErrorProviderEquipment.SetError(lookupEdit, "");
                    return valid;
                }
            }

            string ErrorText = "Please Select Value.";

            if ( lookupEdit.Tag == null ? false : true)
            {
                ErrorText = "Please Select " + lookupEdit.Tag + ".";
            }
            if ((lookupEdit.EditValue == null) || this.strFormMode.ToLower() != "blockedit" && string.IsNullOrEmpty(lookupEdit.EditValue.ToString()))
            {
                dxErrorProviderEquipment.SetError(lookupEdit, ErrorText);
                valid = false;  // Show stop icon as field is invalid //
                return valid;
            }
            else
            {
                dxErrorProviderEquipment.SetError(lookupEdit, "");
                return valid;
            }
        }

        private bool validateMemEdit(MemoEdit txtBox, SentenceCase SentenceCase, EquipmentType equipmentType)
        {
            if (bool_FormLoading)
                return true;

            if (equipmentType != EquipmentType.None)
            {
                if ((int)equipmentType != Convert.ToInt32(lueEquipment_Category.EditValue))
                {
                    dxErrorProviderEquipment.SetError(txtBox, "");
                    return true;
                }
            }
            bool valid = false;
            if (txtBox.Text != "")
            {
                switch (SentenceCase)
                {
                    case SentenceCase.Upper:
                        txtBox.Text = txtBox.Text.ToUpper();
                        break;
                    case SentenceCase.Lower:
                        txtBox.Text = txtBox.Text.ToLower();
                        break;
                    case SentenceCase.Title:
                        txtBox.Text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(txtBox.Text);
                        break;
                }
            }

            string originalText = txtBox.Text;
            //check empty field
            if (originalText == "")
            {
                dxErrorProviderEquipment.SetError(txtBox, "Please enter a valid text.");
                return valid;
            }
            else
            {
                valid = true;
            }

            if (valid)
            {
                dxErrorProviderEquipment.SetError(txtBox, "");
            }
            return valid;
        }

        private bool validateTextBox(TextEdit txtBox, SentenceCase SentenceCase, EquipmentType equipmentType)
        {
            if (bool_FormLoading)
                return true;

            if (equipmentType != EquipmentType.None)
            {
                if ((int)equipmentType != Convert.ToInt32(lueEquipment_Category.EditValue))
                {
                    dxErrorProviderEquipment.SetError(txtBox, "");
                    return true;
                }
            }
            bool valid = false;
            if (txtBox.Text != "")
            {
                switch (SentenceCase)
                {
                    case SentenceCase.Upper:
                        txtBox.Text = txtBox.Text.ToUpper();
                        break;
                    case SentenceCase.Lower:
                        txtBox.Text = txtBox.Text.ToLower();
                        break;
                    case SentenceCase.Title:
                        txtBox.Text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(txtBox.Text);
                        break;
                }
            }

            if (equipmentType == EquipmentType.Plant && txtBox.Text == "N/A")
            {
                dxErrorProviderEquipment.SetError(txtBox, "");
                return true;
            }
            string originalText = txtBox.Text;
            string parsedText = "";
            switch (txtBox.Name)
            {
                case "txtServiceTag":
                case "txtShortDescription":
                    parsedText = Regex.Replace(txtBox.Text, "[^a-z_A-Z0-9 ]", "");
                    break;
                default:
                    parsedText = Regex.Replace(txtBox.Text, "[^a-z_A-Z0-9]", "");
                    break;
            }

            //check non alphanumeric characters
            if (originalText != parsedText || (txtBox.Text == "" && txtBox.Name != "txtPlantRegistrationPlate"))
            {
                dxErrorProviderEquipment.SetError(txtBox, "Please enter a valid text, avoid entering non alphanumeric characters, invalid characters have been removed");
                //txtBox.Focus();
                return valid;
            }
            
            //check registration plate length
            if ((txtBox.Name == "txtRegistration_Plate" || txtBox.Name == "txtPlantRegistrationPlate") && parsedText.Length > 7)
            {
                dxErrorProviderEquipment.SetError(txtBox, "Supplied registration exceeds 7 characters");
                return valid;
            }
            
            //check duplication last
            if (txtBox.Name == "txtRegistration_Plate" || txtBox.Name == "txtPlantRegistrationPlate" || txtBox.Name == "txtManufacturer_ID" || txtBox.Name == "txtIMEI")
            {
                string searchMode, searchValue;

                switch (txtBox.Name)
                {
                    case "txtRegistration_Plate":
                        searchMode = "vehicle_plate";
                        searchValue = txtBox.Text;

                        valid = checkDuplicate(searchValue, searchMode, EquipmentType.Vehicle);
                        if (!valid)
                        {
                            dxErrorProviderEquipment.SetError(txtBox, "Duplicate Registration Plate already exists");
                        }
                        break;
                    case "txtPlantRegistrationPlate":
                        searchMode = "plant_plate";
                        searchValue = txtBox.Text;

                        valid = checkDuplicate(searchValue, searchMode, EquipmentType.Plant);
                        if (!valid)
                        {
                            dxErrorProviderEquipment.SetError(txtBox, "Duplicate Registration Plate already exists");
                        }
                        break;
                    case "txtIMEI":
                        searchMode = "imei";
                        searchValue = txtBox.Text;

                        valid = checkDuplicate(searchValue, searchMode, EquipmentType.Gadget);
                        if (!valid)
                        {
                            dxErrorProviderEquipment.SetError(txtBox, "Duplicate IMEI already exists");
                        }
                        break;
                    case "txtManufacturer_ID":
                        if (lueEquipment_Category.EditValue != null)
                        {
                            searchMode = "manufacturer_id";
                            searchValue = txtBox.Text;

                            valid = checkDuplicate(searchValue, searchMode, EquipmentType.None);
                            if (!valid)
                            {
                                dxErrorProviderEquipment.SetError(txtBox, "Duplicate Manufacturer Identity already exists");
                            }
                        }
                        else
                        {
                            valid = false;
                            dxErrorProviderEquipment.SetError(txtBox, "Cannot validate this field before a Equipment Category value is selected.");
                        }
                        break;
                }
            }
            else
            {
                valid = true;
            }
            if (valid)
            {
                dxErrorProviderEquipment.SetError(txtBox, "");
            }
            return valid;
        }

        private bool validateSpinEdit(SpinEdit spnEdit, EquipmentType equipmentType)
        {
            bool valid = true;

            if (bool_FormLoading)
                return valid;

            if (equipmentType != EquipmentType.None)
            {
                if ((int)equipmentType != Convert.ToInt32(lueEquipment_Category.EditValue))
                {
                    dxErrorProviderEquipment.SetError(spnEdit, "");
                    return valid;
                }
            }


            if (spnEdit.EditValue == null || spnEdit.EditValue.ToString() == "")
            {
                dxErrorProviderEquipment.SetError(spnEdit, "Please enter a valid " + spnEdit.Tag + " value");
                //spnEdit.Focus();
                return valid = false;  // Show stop icon as field is invalid //
            }
            else
            {
                dxErrorProviderEquipment.SetError(spnEdit, "");
                return valid;
            }

        }

        #endregion

        private bool checkDuplicate(string searchValue, string searchMode,EquipmentType equipmentType )
        {
            bool dup = true;

            switch (strFormMode.ToLower())
            {
                case "add":
                    sp_AS_11000_Duplicate_SearchTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_Duplicate_Search, searchValue, Convert.ToInt32(lueEquipment_Category.EditValue), searchMode);

                    switch (spAS11000DuplicateSearchBindingSource.Count)
                    {
                        case 0:
                            dup = true;
                            break;
                        case 1:
                            dup = false;
                            break;
                        default:
                            dup = true;
                            break;
                    }
                        break;
                case "edit":
                        string whereClause ="";

                        switch (searchMode.ToLower())
                        {

                            case "vehicle_plate":
                                whereClause = "[RegistrationPlate] ='" + searchValue + "'";
                                break;
                            case "imei":
                                whereClause = "[IMEI] ='" + searchValue + "'";
                                break;
                            case "manufacturer_id":
                                whereClause = "[ManufacturerID] ='" + searchValue + "'";
                                break;
                        }
                        switch (equipmentType)
                        {
                            case EquipmentType.None://Equipment
                                switch (Convert.ToInt32(lueEquipment_Category.EditValue))
                                {
                                    case 1://Vehicles
                                        dup = returnDuplicateStatus(whereClause, this.spAS11002EquipmentItemBindingSource, this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item, EquipmentType.Vehicle);
                                        break;
                                    case 2://Plant
                                        dup = returnDuplicateStatus(whereClause, this.spAS11002EquipmentItemBindingSource, this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item, EquipmentType.Plant);
                                        break;
                                    case 3://Gadget
                                        dup = returnDuplicateStatus(whereClause, this.spAS11002EquipmentItemBindingSource, this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item, EquipmentType.Gadget);
                                        break;
                                    case 4://Hardware
                                        dup = returnDuplicateStatus(whereClause, this.spAS11002EquipmentItemBindingSource, this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item, EquipmentType.Hardware);
                                        break;
                                    case 5://Software
                                        dup = returnDuplicateStatus(whereClause, this.spAS11002EquipmentItemBindingSource, this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item, EquipmentType.Software);
                                        break;
                                    case 6://Office
                                        dup = returnDuplicateStatus(whereClause, this.spAS11002EquipmentItemBindingSource, this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item, EquipmentType.Office);
                                        break;
                                }

                                break;
                            case EquipmentType.Vehicle:
                                dup = returnDuplicateStatus(whereClause, this.spAS11005VehicleItemBindingSource, this.dataSet_AS_DataEntry.sp_AS_11005_Vehicle_Item, EquipmentType.None);
                                break;
                            case EquipmentType.Plant:
                                dup = returnDuplicateStatus(whereClause, this.spAS11008PlantItemBindingSource, this.dataSet_AS_DataEntry.sp_AS_11008_Plant_Item, EquipmentType.None);
                                break;
                            case EquipmentType.Gadget:
                                dup = returnDuplicateStatus(whereClause, this.spAS11026GadgetItemBindingSource, this.dataSet_AS_DataEntry.sp_AS_11026_Gadget_Item, EquipmentType.None);
                                break;
                            case EquipmentType.Hardware:
                                dup = returnDuplicateStatus(whereClause, this.spAS11032HardwareItemBindingSource, this.dataSet_AS_DataEntry.sp_AS_11032_Hardware_Item, EquipmentType.None);
                                break;
                            case EquipmentType.Software:
                                dup = returnDuplicateStatus(whereClause, this.spAS11035SoftwareItemBindingSource, this.dataSet_AS_DataEntry.sp_AS_11035_Software_Item, EquipmentType.None);
                                break;
                            case EquipmentType.Office:
                                dup = returnDuplicateStatus(whereClause, this.sp_AS_11029_Office_ItemBindingSource, this.dataSet_AS_DataEntry.sp_AS_11029_Office_Item, EquipmentType.None);
                                break;
                        }
                        break;
                default:

                        break;
            }
          
           
       

            return dup;

        }
        
        private bool returnDuplicateStatus(string whereClause, BindingSource bs, DataTable dt, EquipmentType equipmentType)
        {

            if (dt.Select(whereClause).GetLength(0) > 1)
            {
                bs.ResetCurrentItem();
                return false;
            }
            else
            {
                return true;
            }
        }

        private void checkPreliminaryFields()
        {
            MessageBox.Show("Please select equipment category before changing this field", "Preliminary fields to be completed", MessageBoxButtons.OK, MessageBoxIcon.Error);
            dxErrorProviderEquipment.SetError(lueEquipment_Category, "Please Select Category.");
            this.lueEquipment_Category.Focus();
        }

        #endregion

        #region Data Navigator

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion
              
        #region EquipmentValidator

        private void lueExchequerCategoryID_Validating(object sender, CancelEventArgs e)
        {
            if (validateLookupEdit((LookUpEdit)sender, EquipmentType.None))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void deliveryDateDateEdit_Validating(object sender, CancelEventArgs e)
        {

        }

        private void lueAvailability_Validating(object sender, CancelEventArgs e)
        {          

            if (validateLookupEdit((LookUpEdit)sender, EquipmentType.None))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void lueOwnership_Status_Validating(object sender, CancelEventArgs e)
        {        
            if (validateLookupEdit((LookUpEdit)sender, EquipmentType.None))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void lueGC_Marked_Validating(object sender, CancelEventArgs e)
        {          
            if (validateLookupEdit((LookUpEdit)sender, EquipmentType.None))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void lueSupplier_Validating(object sender, CancelEventArgs e)
        {
            if (validateLookupEdit((LookUpEdit)sender, EquipmentType.None))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void lueModel_Validating(object sender, CancelEventArgs e)
        {         
            if (validateLookupEdit((LookUpEdit)sender, EquipmentType.None))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void lueMake_Validating(object sender, CancelEventArgs e)
        {
            if (validateLookupEdit((LookUpEdit)sender, EquipmentType.None))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void lueEquipment_Category_Validating(object sender, CancelEventArgs e)
        {
            if (validateLookupEdit((LookUpEdit)sender, EquipmentType.None))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void Manufacturer_IDTextEdit_Validating(object sender, CancelEventArgs e)
        {
            if (validateTextBox((TextEdit)sender, SentenceCase.Upper, EquipmentType.None))
            {
                e.Cancel = false;
            }
            else
            {
                e.Cancel = true;
            }

        }

        private void lueMake_EditValueChanged(object sender, EventArgs e)
        {

            if (lueMake.EditValue != null)
            {
                if (lueMake.EditValue.ToString() != "")
                {
                    RefreshModel();
                    try
                    {
                        switch (strFormMode.ToLower())
                        {
                            case "add":

                                // ChildVisibility(0, (int)passedEquipType);

                                //CategoryEditValueChangedCount+=1;
                                this.lueModel.EditValue = null;
                                break;

                            case "block_add":

                                break;

                            case "edit":
                            //DataRowView currRow = (DataRowView)spAS11002EquipmentItemBindingSource.Current;
                            //ChildVisibility(Convert.ToInt32(currRow["EquipmentID"]), Convert.ToInt32(currRow["EquipmentCategory"]));
                            //break;
                            case "view":
                                DataRowView currRow = (DataRowView)spAS11002EquipmentItemBindingSource.Current;
                                ChildVisibility(Convert.ToInt32(currRow["EquipmentID"]), Convert.ToInt32(currRow["EquipmentCategory"]));
                                if (currRow.Row.RowState == DataRowState.Modified)
                                {
                                    this.lueModel.EditValue = null;
                                }
                                break;
                            case "blockedit":
     
                                break;
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                }
            }
            else
            {
                this.lueModel.EditValue = null;
            }
        }

        private void lueEquipment_Category_EditValueChanged(object sender, EventArgs e)
        {
            if (bool_FormLoading && strFormMode.ToLower() != "add")
                return;

            if (lueEquipment_Category.EditValue != null)
             
            {
                if (lueEquipment_Category.EditValue.ToString() != "")
                {
                    //RefreshChildBindingSource();
                    RejectChildChanges();
                    ClearErrors(this.Controls);
                    RefreshMake();

                    manufacturerIDDescription(lueEquipment_Category.EditValue.ToString());
                                        
                    try
                    {
                        switch (strFormMode.ToLower())
                        {
                            case "add":
                                 if (bool_FormLoading)
                                {
                                   
                                    ChildVisibility(0, (passedEquipType == EquipmentType.None ? 1 : (int)passedEquipType));
                                }
                                else
                                {
                                    RefreshChildBindingSource();
                                    ChildVisibility(0, Convert.ToInt32(lueEquipment_Category.EditValue));
                                }
                                
                                this.lueMake.EditValue = null;
                                break;

                            case "block_add":

                                break;

                            case "edit":
                            case "view":
                                DataRowView currRow = (DataRowView)spAS11002EquipmentItemBindingSource.Current;
                                ChildVisibility(Convert.ToInt32(currRow["EquipmentID"]), Convert.ToInt32(lueEquipment_Category.EditValue));
                                if (currRow.Row.RowState == DataRowState.Modified)
                                {
                                    this.lueMake.EditValue = null;
                                }
                                break;
                            case "blockedit":
                                break;
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                }
            }
           
        }

        private void lueSupplier_EditValueChanged(object sender, EventArgs e)
        {
            RefreshSupplier();
        }

        #endregion

        #region Child Objects

        #region EditorEvents

        private void lueDepreciationSetting_EditValueChanged(object sender, EventArgs e)
        {
            if (bool_FormLoading)
            {
                return;
            }
            if (lueDepreciationSetting.EditValue != null && lueDepreciationSetting.EditValue.ToString() != "")
            {
                //change linked values
               DataRow[] rows = this.dataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_Item.Select("DepreciationSettingID  = " + lueDepreciationSetting.EditValue.ToString());

               if (rows.Length > 0)
               //if (this.dataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_Item.Count > 0)
               {
                   //DataRow row = this.dataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_Item.Rows[0];
                   DataRow row = rows[0];
                   if ((((DataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_ItemRow)(row)).MaximumOccurrence).ToString() != "")
                   {
                       spnMaximumOccurrence.EditValue = ((DataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_ItemRow)(row)).MaximumOccurrence;
                   }
                   else
                   {
                       spnMaximumOccurrence.EditValue = 48;
                   }
                   if ((((DataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_ItemRow)(row)).DayOfMonthApplyDepreciation).ToString() != "")
                   {
                       spnDayOfMonthApplyDepreciation.EditValue = ((DataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_ItemRow)(row)).DayOfMonthApplyDepreciation;
                   }
                   else
                   {
                       spnDayOfMonthApplyDepreciation.EditValue = 1;
                   }

                   if ((((DataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_ItemRow)(row)).OccursUnitDescriptorID).ToString() != "")
                   {
                       lueOccursUnitDescriptor.EditValue = ((DataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_ItemRow)(row)).OccursUnitDescriptorID;
                   }
                   else
                   {
                       lueOccursUnitDescriptor.EditValue = 64;
                   }

               }
               else
               {
                   spnMaximumOccurrence.EditValue = 48;
                   spnDayOfMonthApplyDepreciation.EditValue = 1;
                   lueOccursUnitDescriptor.EditValue = 64;
               }

            }
            
                    
        }

        private void makeLookUpEdit_ButtonPressed(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("Add".Equals(e.Button.Tag)) // Add Make Button 
                {
                    Open_Child_Forms("frm_Makes_Model_Wizard", FormMode.view, "frm_Makes_Model_Wizard");  
                }
                else if ("Refresh".Equals(e.Button.Tag)) //Refresh Make List
                {                    
                    RefreshMake();
                }
            }
        }

        private void modelLookUpEdit_ButtonPressed(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("Add".Equals(e.Button.Tag)) // Add Make Button 
                {
                    Open_Child_Forms("frm_Makes_Model_Wizard", FormMode.edit, "frm_Makes_Model_Wizard");
                }
                else if ("Refresh".Equals(e.Button.Tag)) //Refresh Make List
                {
                    RefreshModel();
                }
            }
        }

        private void supplierLookUpEdit_ButtonPressed(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("Add".Equals(e.Button.Tag)) // Add Make Button 
                {
                    Open_Child_Forms("frm_AS_Supplier_Edit", FormMode.add, "frm_AS_Equipment_Edit");
                }
                else if ("Refresh".Equals(e.Button.Tag)) //Refresh Make List
                {
                    RefreshSupplier();
                }
            }
        }

        #endregion
        
        #region Common Child Events & Methods

        #region CommonChildEvents

        private void childLayoutControlGroup_Shown(object sender, EventArgs e)
        {
            NoDetailsLayoutControlGroup.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
        }

        #endregion

        #region CommonChildMethods

        private void HideAllChildPages(string HeaderText, string BackgroundText)
        {
            if (HeaderText == "")
                HeaderText = "No Details";
            if (BackgroundText == "")
                BackgroundText = "No Details Available";

            NoDetailsLayoutControlGroup.Text = HeaderText;
            NoDetailsLayoutControlGroup.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            vehicleLayoutControlGroup.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            plantLayoutControlGroup.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            gadgetLayoutControlGroup.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            hardwareLayoutControlGroup.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            softwareLayoutControlGroup.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            officeLayoutControlGroup.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
        }

        private void ChildVisibility( int IDs, int typesOfEquipment)
        {
            string strChildRecordIDs = IDs.ToString();
            HideAllChildPages("", "");
        
            switch (typesOfEquipment)
            {

                case 1://vehicles
                    vehicleLayoutControlGroup.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    //navigate to record
                    if (strFormMode.ToLower() != "add")
                    {
                       spAS11005VehicleItemBindingSource.Position = spAS11005VehicleItemBindingSource.Find("EquipmentID", strChildRecordIDs);
                    }
                    if (strFormMode.ToLower() == "add")
                    {
                        BeginAddNewChild(typesOfEquipment);
                    }
                    break;
                case 2://plant
                    plantLayoutControlGroup.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    //navigate to record
                    if (strFormMode.ToLower() != "add")
                    {
                        spAS11008PlantItemBindingSource.Position = spAS11008PlantItemBindingSource.Find("EquipmentID", strChildRecordIDs);
                    }
                    if (strFormMode.ToLower() == "add")
                    {
                        BeginAddNewChild(typesOfEquipment);
                    }
                    break;
                case 3://gadget
                    gadgetLayoutControlGroup.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    //navigate to record
                    if (strFormMode.ToLower() != "add")
                    {
                        spAS11026GadgetItemBindingSource.Position = spAS11026GadgetItemBindingSource.Find("EquipmentID", strChildRecordIDs);
                    }
                    if (strFormMode.ToLower() == "add")
                    {
                        BeginAddNewChild(typesOfEquipment);
                    }
                    break;                
                case 4://hardware
                    hardwareLayoutControlGroup.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    //navigate to record
                    if (strFormMode.ToLower() != "add")
                    {
                        spAS11032HardwareItemBindingSource.Position = spAS11032HardwareItemBindingSource.Find("EquipmentID", strChildRecordIDs);
                    }

                    if (strFormMode.ToLower() == "add")
                    {
                        BeginAddNewChild(typesOfEquipment);
                    }
                    break;
                case 5://software
                    softwareLayoutControlGroup.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    //navigate to record
                    if (strFormMode.ToLower() != "add")
                    {
                        spAS11035SoftwareItemBindingSource.Position = spAS11035SoftwareItemBindingSource.Find("EquipmentID", strChildRecordIDs);
                    }

                    if (strFormMode.ToLower() == "add")
                    {
                        BeginAddNewChild(typesOfEquipment);
                    }
                    break;
                case 6://office
                    officeLayoutControlGroup.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    //navigate to record
                    if (strFormMode.ToLower() != "add")
                    {
                        sp_AS_11029_Office_ItemBindingSource.Position = sp_AS_11029_Office_ItemBindingSource.Find("EquipmentID", strChildRecordIDs);
                    }

                    if (strFormMode.ToLower() == "add")
                    {
                        BeginAddNewChild(typesOfEquipment);
                    }
                    break;
                default:
                    if (strFormMode.ToLower() == "add")
                    {                       
                       HideAllChildPages("Please Select Equipment Type", "Awaiting Equipment section completion...");                     
                    }
                  
                    break;
            }

        }

        private void BeginAddNewChild(int EquipmentType)
        {
            RejectChildChanges();
            DataRow drNewRow;
            switch (EquipmentType)
            {
                case 1://Vehicles
                    drNewRow = this.dataSet_AS_DataEntry.sp_AS_11005_Vehicle_Item.NewRow();
                    drNewRow["Mode"] = "add";
                    drNewRow["EquipmentID"] = "-1";
                    drNewRow["TotalTyreNumber"] = "5";
                    drNewRow["RegistrationDate"] =  getCurrentDate();
                                      
                    if (spAS11005VehicleItemBindingSource.Find("EquipmentID", "-1") < 0)
                    {
                        this.dataSet_AS_DataEntry.sp_AS_11005_Vehicle_Item.Rows.Add(drNewRow);
                    }
                    break;
                case 2://Plant
                    drNewRow = this.dataSet_AS_DataEntry.sp_AS_11008_Plant_Item.NewRow();
                    drNewRow["Mode"] = "add";
                    drNewRow["EquipmentID"] = "-1";
                    if (spAS11008PlantItemBindingSource.Find("EquipmentID", "-1") < 0)
                    {
                        this.dataSet_AS_DataEntry.sp_AS_11008_Plant_Item.Rows.Add(drNewRow);
                    }
                    break;
                case 3://Gadget
                    drNewRow = this.dataSet_AS_DataEntry.sp_AS_11026_Gadget_Item.NewRow();
                    drNewRow["Mode"] = "add";
                    drNewRow["EquipmentID"] = "-1";
                    if (spAS11026GadgetItemBindingSource.Find("EquipmentID", "-1") < 0)
                    {
                        this.dataSet_AS_DataEntry.sp_AS_11026_Gadget_Item.Rows.Add(drNewRow);
                    }
                    break;
                case 4://Hardware
                    drNewRow = this.dataSet_AS_DataEntry.sp_AS_11032_Hardware_Item.NewRow();
                    drNewRow["Mode"] = "add";
                    drNewRow["EquipmentID"] = "-1";
                    if (spAS11032HardwareItemBindingSource.Find("EquipmentID", "-1") < 0)
                    {
                        this.dataSet_AS_DataEntry.sp_AS_11032_Hardware_Item.Rows.Add(drNewRow);
                    }
                    break;
                case 5://Software
                    drNewRow = this.dataSet_AS_DataEntry.sp_AS_11035_Software_Item.NewRow();
                    drNewRow["Mode"] = "add";
                    drNewRow["EquipmentID"] = "-1";
                    if (spAS11035SoftwareItemBindingSource.Find("EquipmentID", "-1") < 0)
                    {
                        this.dataSet_AS_DataEntry.sp_AS_11035_Software_Item.Rows.Add(drNewRow);
                    }
                    break;
                case 6://Office
                    drNewRow = this.dataSet_AS_DataEntry.sp_AS_11029_Office_Item.NewRow();
                    drNewRow["Mode"] = "add";
                    drNewRow["EquipmentID"] = "-1";
                    drNewRow["Quantity"] = "1";
                    if (sp_AS_11029_Office_ItemBindingSource.Find("EquipmentID", "-1") < 0)
                    {
                        this.dataSet_AS_DataEntry.sp_AS_11029_Office_Item.Rows.Add(drNewRow);
                    }
                    break;
            }
        }


        private void RejectChildChanges()
        {
            this.dataSet_AS_DataEntry.sp_AS_11005_Vehicle_Item.RejectChanges();
            this.dataSet_AS_DataEntry.sp_AS_11008_Plant_Item.RejectChanges();
            this.dataSet_AS_DataEntry.sp_AS_11026_Gadget_Item.RejectChanges();
            this.dataSet_AS_DataEntry.sp_AS_11029_Office_Item.RejectChanges();
            this.dataSet_AS_DataEntry.sp_AS_11032_Hardware_Item.RejectChanges();
            this.dataSet_AS_DataEntry.sp_AS_11035_Software_Item.RejectChanges();
        }


        #endregion

        #region ChildValidation

        #region VehicleValidation

        private void lueVehicleCategoryID_EditValueChanged(object sender, EventArgs e)
        {
            if (bool_FormLoading)
                return;

            calculateMOTDueDate();
        }

        private void lueVehicleCategoryID_Validating(object sender, CancelEventArgs e)
        {
            if (validateLookupEdit((LookUpEdit)sender, EquipmentType.Vehicle))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void registration_PlateTextEdit_Validating(object sender, CancelEventArgs e)
        {
            if (validateTextBox((TextEdit)sender, SentenceCase.Upper, EquipmentType.Vehicle))
            {
                e.Cancel = false;
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void log_Book_NumberTextEdit_Validating(object sender, CancelEventArgs e)
        {
            if (validateTextBox((TextEdit)sender, SentenceCase.Upper, EquipmentType.Vehicle))
            {
                e.Cancel = false;
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void tracker_Info_IDLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            if (validateLookupEdit((LookUpEdit)sender, EquipmentType.Vehicle))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void engine_SizeSpinEdit_Validating(object sender, CancelEventArgs e)
        {
            if (validateSpinEdit((SpinEdit)sender,EquipmentType.Vehicle))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void fuel_Type_IDLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            if (validateLookupEdit((LookUpEdit)sender, EquipmentType.Vehicle))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void emissionsSpinEdit_Validating(object sender, CancelEventArgs e)
        {
            if (validateSpinEdit((SpinEdit)sender, EquipmentType.Vehicle))
            {
                e.Cancel = false;// Remove stop icon as fie ld is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void total_Tyre_NumberSpinEdit_Validating(object sender, CancelEventArgs e)
        {
            if (validateSpinEdit((SpinEdit)sender, EquipmentType.Vehicle))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void towbar_Type_IDLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            if (validateLookupEdit((LookUpEdit)sender, EquipmentType.Vehicle))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void colourColorPickEdit_Validating(object sender, CancelEventArgs e)
        {

        }

        private void registration_DateDateEdit_Validating(object sender, CancelEventArgs e)
        {

        }

        private void deRegistration_Date_EditValueChanged(object sender, EventArgs e)
        {
            if (bool_FormLoading)
                return;

            calculateMOTDueDate();
        }


        private void mOT_Due_DateDateEdit_Validating(object sender, CancelEventArgs e)
        {

        }

        private void calculateMOTDueDate()
        {
            if (formMode == FormMode.add || deMOT_Due_Date.Properties.GetDisplayText(deMOT_Due_Date.EditValue) == "")
            {
                if (lueVehicleCategoryID.Properties.GetDisplayText(lueVehicleCategoryID.EditValue) == "Car")
                {
                    //deMOT_Due_Date.EditValue = getCurrentDate().AddYears(3);
                    deMOT_Due_Date.EditValue = deRegistration_Date.DateTime.AddYears(3);
                }
                else if (lueVehicleCategoryID.Properties.GetDisplayText(lueVehicleCategoryID.EditValue) == "Commercial")
                {
                    //deMOT_Due_Date.EditValue = getCurrentDate().AddYears(1);
                    deMOT_Due_Date.EditValue = deRegistration_Date.DateTime.AddYears(1);
                }
            }

        }


        private void currentMileageSpinEdit_Validating(object sender, CancelEventArgs e)
        {
            if (validateSpinEdit((SpinEdit)sender, EquipmentType.Vehicle))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void service_Due_MileageSpinEdit_Validating(object sender, CancelEventArgs e)
        {
            if (validateSpinEdit((SpinEdit)sender, EquipmentType.Vehicle))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        #endregion

        #region PlantValidation

        private void plant_Category_IDLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            if (validateLookupEdit((LookUpEdit)sender, EquipmentType.Plant))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void PlantFuel_Type_IDLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            if (validateLookupEdit((LookUpEdit)sender, EquipmentType.Plant))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void txtShortDescription_Validating(object sender, CancelEventArgs e)
        {
            if (validateTextBox((TextEdit)sender, SentenceCase.Title, EquipmentType.Plant))
            {
                e.Cancel = false;
            }
            else
            {
                e.Cancel = true;
            }
        }
        
        private void txtPlantRegistrationPlate_Validating(object sender, CancelEventArgs e)
        {
            if (validateTextBox((TextEdit)sender, SentenceCase.Upper, EquipmentType.Plant))
            {
                e.Cancel = false;
            }
            else
            {
                e.Cancel = true;
            }
        }

        #endregion

        #region GadgetValidation
        private void txtIMEI_Validating(object sender, CancelEventArgs e)
        {
            if (validateTextBox((TextEdit)sender, SentenceCase.Upper, EquipmentType.Gadget))
            {
                e.Cancel = false;
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void gadget_Type_IDLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            if (validateLookupEdit((LookUpEdit)sender, EquipmentType.Gadget))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void iMEITextEdit_Validating(object sender, CancelEventArgs e)
        {
            if (validateTextBox((TextEdit)sender, SentenceCase.Upper, EquipmentType.Gadget))
            {
                e.Cancel = false;
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void casing_IDLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            if (validateLookupEdit((LookUpEdit)sender, EquipmentType.Gadget))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        #endregion

        #region HardwareValidation

        private void hardware_Type_IDLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            if (validateLookupEdit((LookUpEdit)sender, EquipmentType.Hardware))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void specification_IDLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            if (validateLookupEdit((LookUpEdit)sender, EquipmentType.Hardware))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }
        #endregion

        #region SoftwareValidation

        private void product_IDLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            if (validateLookupEdit((LookUpEdit)sender, EquipmentType.Software))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void version_IDLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            if (validateLookupEdit((LookUpEdit)sender, EquipmentType.Software))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void licenceTextEdit_Validating(object sender, CancelEventArgs e)
        {
            if (validateTextBox((TextEdit)sender, SentenceCase.Upper, EquipmentType.Software))
            {
                e.Cancel = false;
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void licence_Type_IDLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            if (validateLookupEdit((LookUpEdit)sender, EquipmentType.Software))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void parent_ProgramLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            if (validateLookupEdit((LookUpEdit)sender, EquipmentType.Software))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void acquisation_Method_IDLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            if (validateLookupEdit((LookUpEdit)sender, EquipmentType.Software))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void licence_KeyTextEdit_Validating(object sender, CancelEventArgs e)
        {
            if (validateTextBox((TextEdit)sender, SentenceCase.Upper, EquipmentType.Software))
            {
                e.Cancel = false;
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void valid_UntilDateEdit_Validating(object sender, CancelEventArgs e)
        {

        }

        #endregion

        #region OfficeValidation

        private void memDescription_Validating(object sender, CancelEventArgs e)
        {
            if (validateMemEdit((MemoEdit)sender,SentenceCase.AsIs, EquipmentType.Office))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void lueOfficeCategoryID_Validating(object sender, CancelEventArgs e)
        {
            if (validateLookupEdit((LookUpEdit)sender, EquipmentType.Office))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }


        #endregion

        private void ceArchive_CheckedChanged(object sender, EventArgs e)
        {

        }

        #endregion
        
        #region linkedChildMethodsAnd Events

        private void btnPopUp1Ok_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }
        
        private void repositoryItemPopupContainerEdit1_QueryResultValue(object sender, DevExpress.XtraEditors.Controls.QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit1_Get_Selected();

            linkedchildVisibility();
            
        }

        private string PopupContainerEdit1_Get_Selected()
        {
            shownTabs = "";
            GridView view = (GridView)gridControl1.MainView;
            if (view.DataRowCount <= 0)
            {
               
                return "No Linked Data Filter";

            }
            else if (selection1.SelectedCount <= 0)
            {
                return "No Linked Data Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        
                        if (intCount == 0)
                        {
                            shownTabs = Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        else if (intCount >= 1)
                        {
                            shownTabs += ", " + Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        intCount++;
                    }
                }
            }

            return (string.IsNullOrEmpty(shownTabs) ? "No Linked Data Filter" : shownTabs);
        }
        
        private void linkedchildVisibility()
        {
            string[] t = splitStrRecords(PopupContainerEdit1_Get_Selected());
            ArrayList tabs  = new ArrayList();
            foreach (string h in t)
            {
                tabs.Add(Regex.Replace(h, @"\s+", ""));
            }
            if (tabs.Contains("NoLinkedDataFilter"))
            {
                equipmentChildTabControl.TabPages.Add(roadTaxTabPage);
                equipmentChildTabControl.TabPages.Add(trackerTabPage);
                equipmentChildTabControl.TabPages.Add(keeperTabPage);
                equipmentChildTabControl.TabPages.Add(transactionTabPage);
                equipmentChildTabControl.TabPages.Add(depreciationTabPage);
                equipmentChildTabControl.TabPages.Add(billingTabPage);
                equipmentChildTabControl.TabPages.Add(coverTabPage);
                equipmentChildTabControl.TabPages.Add(serviceDatalTabPage);
                equipmentChildTabControl.TabPages.Add(workDetailTabPage);
                equipmentChildTabControl.TabPages.Add(incidentTabPage);
                equipmentChildTabControl.TabPages.Add(purposeTabPage);
                equipmentChildTabControl.TabPages.Add(fuelCardTabPage);
                equipmentChildTabControl.TabPages.Add(speedingTabPage);
            }
            else
            {
                if (tabs.Contains("RoadTax"))
                {
                    equipmentChildTabControl.TabPages.Add(roadTaxTabPage);
                }
                else
                {
                    equipmentChildTabControl.TabPages.Remove(roadTaxTabPage);
                }

                if (tabs.Contains("Verilocation"))
                {
                    equipmentChildTabControl.TabPages.Add(trackerTabPage);
                }
                else
                {
                    equipmentChildTabControl.TabPages.Remove(trackerTabPage);
                }

                if (tabs.Contains("Keepers"))
                {
                    equipmentChildTabControl.TabPages.Add(keeperTabPage);
                }
                else
                {
                    equipmentChildTabControl.TabPages.Remove(keeperTabPage);
                }

                if (tabs.Contains("Transactions"))
                {
                    equipmentChildTabControl.TabPages.Add(transactionTabPage);
                }
                else
                {
                    equipmentChildTabControl.TabPages.Remove(transactionTabPage);
                }

                if (tabs.Contains("DepreciationData"))
                {
                    equipmentChildTabControl.TabPages.Add(depreciationTabPage);
                }
                else
                {
                    equipmentChildTabControl.TabPages.Remove(depreciationTabPage);
                }

                if (tabs.Contains("BillingDetails"))
                {
                    equipmentChildTabControl.TabPages.Add(billingTabPage);
                }
                else
                {
                    equipmentChildTabControl.TabPages.Remove(billingTabPage);
                }

                if (tabs.Contains("PolicyandInsuranceCoverDetails"))
                {
                    equipmentChildTabControl.TabPages.Add(coverTabPage);
                }
                else
                {
                    equipmentChildTabControl.TabPages.Remove(coverTabPage);
                }

                if (tabs.Contains("ServiceIntervalInformation"))
                {
                    equipmentChildTabControl.TabPages.Add(serviceDatalTabPage);
                }
                else
                {
                    equipmentChildTabControl.TabPages.Remove(serviceDatalTabPage);
                }

                if (tabs.Contains("WorkOrderDetails"))
                {
                    equipmentChildTabControl.TabPages.Add(workDetailTabPage);
                }
                else
                {
                    equipmentChildTabControl.TabPages.Remove(workDetailTabPage);
                }

                if (tabs.Contains("IncidentDetails"))
                {
                    equipmentChildTabControl.TabPages.Add(incidentTabPage);
                }
                else
                {
                    equipmentChildTabControl.TabPages.Remove(incidentTabPage);
                }

                if (tabs.Contains("AssetPurpose"))
                {
                    equipmentChildTabControl.TabPages.Add(purposeTabPage);
                }
                else
                {
                    equipmentChildTabControl.TabPages.Remove(purposeTabPage);
                }

                if (tabs.Contains("FuelCard"))
                {
                    equipmentChildTabControl.TabPages.Add(fuelCardTabPage);
                }
                else
                {
                    equipmentChildTabControl.TabPages.Remove(fuelCardTabPage);
                }

                if (tabs.Contains("SpeedingData"))
                {
                    equipmentChildTabControl.TabPages.Add(speedingTabPage);
                }
                else
                {
                    equipmentChildTabControl.TabPages.Remove(speedingTabPage);
                }
            }
          
        }

        private void Get_ChildData()
        {
            switch (strFormMode.ToLower())
            {
                case "add":
                    if (passedEquipType != EquipmentType.None)
                    {
                        switch (passedEquipType)
                        {
                            case EquipmentType.Vehicle:
                                lueEquipment_Category.EditValue = (int)EquipmentType.Vehicle;
                                break;
                            case EquipmentType.Plant:
                                lueEquipment_Category.EditValue = (int)EquipmentType.Plant;
                                break;
                            case EquipmentType.Gadget:
                                lueEquipment_Category.EditValue = (int)EquipmentType.Gadget;
                                break;
                            case EquipmentType.Hardware:
                                lueEquipment_Category.EditValue = (int)EquipmentType.Hardware;
                                break;
                            case EquipmentType.Software:
                                lueEquipment_Category.EditValue = (int)EquipmentType.Software;
                                break;
                            case EquipmentType.Office:
                                lueEquipment_Category.EditValue = (int)EquipmentType.Office;
                                break;
                        }
                    }
                    else
                    {
                        lueEquipment_Category.EditValue = (int)EquipmentType.Vehicle; ;//default add vehicle
                    }
                    break;
                case "blockedit":
                    if (passedEquipType != EquipmentType.None)
                    {

                        ChildVisibility(0, (int)passedEquipType);
                        switch (passedEquipType)
                        {
                            case EquipmentType.Vehicle:
                                lueEquipment_Category.EditValue = (int)EquipmentType.Vehicle;
                                //block_EditDisableVehicle();

                                DataRow drNewRow;
                                drNewRow = this.dataSet_AS_DataEntry.sp_AS_11005_Vehicle_Item.NewRow();
                                drNewRow["strMode"] = "blockedit";
                                drNewRow["strRecordIDs"] = strRecordIDs;
                                drNewRow["EquipmentID"] = "";
                                drNewRow["RegistrationPlate"] = "";
                                drNewRow["Log_Book_Number"] = "";
                                drNewRow["Tracker_Info_ID"] = "";
                                drNewRow["Model_Specifications"] = "";
                                drNewRow["Engine_Size"] = "";
                                drNewRow["Fuel_Type_ID"] = "";
                                drNewRow["Emissions"] = "";
                                drNewRow["Total_Tyre_Number"] = "";
                                drNewRow["Towbar_Type_ID"] = "";
                                drNewRow["Towbar_Type"] = "";
                                drNewRow["Colour"] = "";
                                drNewRow["Registration_Date"] = "";
                                drNewRow["Purpose Purpose_ID"] = "";
                                drNewRow["Purpose"] = "";
                                drNewRow["MOT_Due_Date"] = "";
                                drNewRow["CurrentMileage"] = "";
                                drNewRow["Service_Due_Mileage"] = "";
                                drNewRow["RadioCode"] = "";
                                drNewRow["KeyCode"] = "";
                                drNewRow["ElectronicCode"] = "";
                                this.dataSet_AS_DataEntry.sp_AS_11005_Vehicle_Item.Rows.Add(drNewRow);
                                this.dataSet_AS_DataEntry.sp_AS_11005_Vehicle_Item.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
                                break;
                            case EquipmentType.Plant:
                                lueEquipment_Category.EditValue = (int)EquipmentType.Plant;
                                break;
                            case EquipmentType.Gadget:
                                lueEquipment_Category.EditValue = (int)EquipmentType.Gadget;
                                block_EditDisableGadget();
                                break;
                            case EquipmentType.Hardware:
                                lueEquipment_Category.EditValue = (int)EquipmentType.Hardware;
                                block_EditDisableHardware();
                                break;
                            case EquipmentType.Software:
                                lueEquipment_Category.EditValue = (int)EquipmentType.Software;
                                block_EditDisableSoftware();
                                break;
                            case EquipmentType.Office:
                                lueEquipment_Category.EditValue = (int)EquipmentType.Office;
                                block_EditDisableOffice();
                                break;
                        }
                    }
                    else
                    {
                        lueEquipment_Category.EditValue = (int)EquipmentType.Vehicle; ;//default add vehicle
                        block_EditDisableVehicle();
                    }
                    break;
                case "edit":
                case "view":
                    try
                    {
                        DataRowView currRow = (DataRowView)spAS11002EquipmentItemBindingSource.Current;
                        ChildVisibility(Convert.ToInt32(currRow["EquipmentID"]), Convert.ToInt32(currRow["EquipmentCategory"]));
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                    break;
                default:
                    break;
            }
        }


        #region childGridView


        public void UpdateFormRefreshStatus(int status, string strNewIDs1, string strNewIDs2, string strNewIDs3)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
            if (strNewIDs2 != "") i_str_AddedRecordIDs2 = strNewIDs2;
            if (strNewIDs3 != "") i_str_AddedRecordIDs3 = strNewIDs3;
        }

        internal void frmActivated()
        {
            if (UpdateRefreshStatus > 0)
                UpdateRefreshStatus = 0;
            LoadLinkedRecords((((DataRowView)(spAS11002EquipmentItemBindingSource.CurrencyManager.Current)).Row).ItemArray[0].ToString());
            //this.RefreshGridViewStateEquipment.LoadViewInfo();  // Reload any expanded groups and selected rows //
            loadGridViewState();

            //// Highlight any recently added Equipment new rows //
            //if (i_str_AddedRecordIDs1 != "")
            //{
            //    string[] strArray = splitStrRecords(i_str_AddedRecordIDs1);//i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            //    int intID = 0;
            //    int intRowHandle = 0;
            //    GridView view = (GridView)equipmentGridControl.MainView;
            //    view.ClearSelection(); // Clear any current selection so just the new record is selected //
            //    foreach (string strElement in strArray)
            //    {
            //        intID = Convert.ToInt32(strElement);
            //        intRowHandle = view.LocateByValue(0, view.Columns["EquipmentID"], intID);
            //        if (intRowHandle != GridControl.InvalidRowHandle)
            //        {
            //            view.MakeRowVisible(intRowHandle, false);
            //            view.SelectRow(intRowHandle);
            //        }
            //    }
            //    i_str_AddedRecordIDs1 = "";
            //}
            if (i_str_AddedRecordIDs2 != "")
            {
                highlightChildrow();
            }
        }

        private void highlightChildrow()
        {
            GridView view = null;
            string viewID = "";
            switch (i_int_FocusedGrid)
            {
              
                case 19://Road Tax
                    // Highlight any recently added Road Tax new rows //
                    view = (GridView)roadTaxGridControl.MainView;
                    viewID = "RoadTaxID";
                    break;
                case 4://Tracker
                    // Highlight any recently added Tracker new rows //
                    view = (GridView)trackerGridControl.MainView;
                    viewID = "TrackerInformationID";
                    break;
               
                case 7://billing
                    // Highlight any recently added billing new rows //
                    view = (GridView)billingGridControl.MainView;
                    viewID = "DepreciationID";
                    break;
                case 8://keeper
                    // Highlight any recently added keepers new rows //
                    view = (GridView)keeperGridControl.MainView;
                    viewID = "KeeperAllocationID";
                    break;
                case 9://transactions
                    // Highlight any recently added transaction new rows //
                    view = (GridView)transactionGridControl.MainView;
                    viewID = "TransactionID";
                    break;
                case 11://depreciation
                    // Highlight any recently added depreciation new rows //
                    view = (GridView)depreciationGridControl.MainView;
                    viewID = "DepreciationID";
                    break;
                case 12://Incident
                    // Highlight any recently added Incident new rows //
                    view = (GridView)incidentGridControl.MainView;
                    viewID = "IncidentID";
                    break;
                case 13://Service Interval
                    // Highlight any recently added ServiceInterval new rows //
                    view = (GridView)serviceDataGridControl.MainView;
                    viewID = "ServiceDataID";
                    break;
                case 14://Cover
                    // Highlight any recently added Cover new rows //
                    view = (GridView)coverGridControl.MainView;
                    viewID = "CoverID";
                    break;
                case 15://work
                    // Highlight any recently added workService new rows //
                    view = (GridView)workDetailGridControl.MainView;
                    viewID = "WorkDetailID";
                    break;
                case 16://Purpose
                    // Highlight any recently added Purpose new rows //
                    view = (GridView)purposeGridControl.MainView;
                    viewID = "EquipmentPurposeID";
                    break;
                case 17://Fuel Cards
                    // Highlight any recently added Fuel Cards new rows //
                    view = (GridView)fuelCardGridControl.MainView;
                    viewID = "FuelCardID";
                    break;
                case 18://Speeding
                    // Highlight any recently added Speeding new rows //
                    view = (GridView)speedingGridControl.MainView;
                    viewID = "SpeedingID";
                    break;
            }
            
            if (i_str_AddedRecordIDs2 != "")
            {
                string[] strArray = splitStrRecords(i_str_AddedRecordIDs2);
                int intID = 0;
                int intRowHandle = 0;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns[viewID], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.MakeRowVisible(intRowHandle, false);
                        view.SelectRow(intRowHandle);
                    }
                }
                i_str_AddedRecordIDs2 = "";
            }

        }
        

        private void childGridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {                
                case "roadTaxGridView":
                    message = "No Road Tax Available";
                    break;
                case "trackerGridView":
                    message = "No Verilocation Tracker Information Available";
                    break;               
                case "keeperGridView":
                     message = "No Keeper Details Available";
                    break;
                case "transactionsGridView":
                     message = "No Transactions Details Available";
                    break;
                case "billingGridView":
                     message = "No Billings Information Available";
                    break;
                case "depreciationGridView":
                     message = "No Depreciation Data Available";
                    break;
                case "incidentGridView":
                     message = "No Incident Data Available";
                    break;
                case "serviceDataGridView":
                     message = "No Service Interval Data Available";
                    break;
                case "coverGridView":
                     message = "No Cover Data Available";
                    break;
                case "workDetailGridView":
                     message = "No Work Order Detail Data Available";
                    break;
                case "purposeGridView":
                     message = "No Equipment Purpose Data Available";
                    break;
                case "fuelCardGridView":
                     message = "No Fuel Card Data Available";
                    break;
                case "speedingGridView":
                     message = "No Speeding Data Available";
                    break;
                default:
                     message = "No Details Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);

        }

        private void childGridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning)
                return;
            isRunning = true;
            GridView view = sender as GridView;
            childGridViewSelectionChanged(view, e);
        }


        private void commonGridView_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                iBoolDontFireGridGotFocusOnDoubleClick = true;
                if (i_int_FocusedGrid != 18)
                {
                    Edit_Record();
                }
            }
        }

        private void commonGridView_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            string GridName = ((GridView)sender).Name;
            switch (GridName)
            {
                case "roadTaxGridView":
                    i_int_FocusedGrid = 19;
                    break;

                case "trackerGridView":
                    i_int_FocusedGrid = 4;
                    break;

                case "softwareGridView":
                    i_int_FocusedGrid = 5;
                    break;

                case "officeGridView":
                    i_int_FocusedGrid = 6;
                    break;

                case "billingGridView":
                    i_int_FocusedGrid = 7;
                    break;

                case "keeperGridView":
                    i_int_FocusedGrid = 8;
                    break;

                case "transactionsGridView":
                    i_int_FocusedGrid = 9;
                    break;

                //case "reminderGridView":
                //    i_int_FocusedGrid = 10;
                //    break;
                case "depreciationGridView":
                    i_int_FocusedGrid = 11;
                    break;
                case "incidentGridView":
                    i_int_FocusedGrid = 12;
                    break;
                case "serviceDataGridView":
                    i_int_FocusedGrid = 13;
                    break;
                case "coverGridView":
                    i_int_FocusedGrid = 14;
                    break;
                case "workDetailGridView":
                    i_int_FocusedGrid = 15;
                    break;
                case "purposeGridView":
                    i_int_FocusedGrid = 16;
                    break;
                case "fuelCardGridView":
                    i_int_FocusedGrid = 17;
                    break;
                case "speedingGridView":
                    i_int_FocusedGrid = 18;
                    break;
            }

            SetMenuStatus();
        }

        private void commonGridControl_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("insert".Equals(e.Button.Tag))
                    {
                        Insert_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    break;
                default:
                    break;
            }
        }
        private void commonGridView_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void customFilterDraw(GridView view, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(view, e);
        }

        private void childGridViewSelectionChanged(GridView view, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(view, e, ref isRunning);
            SetMenuStatus();

            if (i_int_FocusedGrid == 12)//Incident
            {
                GridView childView = incidentGridView;
                int numOfSelectedIncidentInfoRows = childView.SelectedRowsCount;
                int tempNumOfSelectedRows;
                if (numOfSelectedIncidentInfoRows > 0)
                {
                    tempNumOfSelectedRows = numOfSelectedIncidentInfoRows;
                }
                else
                {
                    tempNumOfSelectedRows = 1;
                }

                int[] IDs = new int[tempNumOfSelectedRows];
                int[] intRowHandles = childView.GetSelectedRows();

                if (numOfSelectedIncidentInfoRows > 0)
                {
                    int countRows = 0;

                    foreach (int intRowHandle in intRowHandles)
                    {
                        DataRow dr = childView.GetDataRow(intRowHandle);
                        IDs[countRows] = (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11081_Incident_ItemRow)(dr)).IncidentID);
                        countRows += 1;
                    }
                }
                else
                {
                    IDs[0] = 0;
                }
                //load linked incident losses records
                // sp_AS_11081_Incident_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11081_Incident_Item, stringRecords(IDs), "view");
            }
        }


        #region CRUD

        #region CRUD Methods

        private void Add_Record()
        {

            switch (i_int_FocusedGrid)
            {                
                case 19:     // Road Tax //
                    if (!iBool_AllowAddRoadTax)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Equipment_Edit", roadTaxGridControl, EquipmentType.None);
                    break;
                case 4:     // Tracker //
                    if (!iBool_AllowAddTracker)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Equipment_Edit", trackerGridControl, EquipmentType.None);
                    break;                
                case 7:     // Billings //
                    if (!iBool_AllowAddBilling)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Equipment_Edit", billingGridControl, EquipmentType.None);
                    break;
                case 8:     // Keeper //
                    if (!iBool_AllowAddKeeper)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Equipment_Edit", keeperGridControl, EquipmentType.None);
                    break;
                case 9:     // Transaction //
                    if (!iBool_AllowAddTransactions)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Equipment_Edit", transactionGridControl, EquipmentType.None);
                    break;
                //case 10:     // reminders //
                //    if (!iBool_AllowAddReminders)
                //        return;
                //    OpenEditForm(FormMode.add, "frm_AS_Equipment_Edit", officeGridControl, EquipmentType.None);
                //    break;
                case 11:     // depreciation //
                    if (!iBool_AllowAddDepreciation)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Equipment_Edit", depreciationGridControl, EquipmentType.None);
                    break;
                case 12:     // incident //
                    if (!iBool_AllowAddIncident)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Equipment_Edit", incidentGridControl, EquipmentType.None);
                    break;
                case 13:     // ServiceInterval //
                    if (!iBool_AllowAddServiceData)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Equipment_Edit", serviceDataGridControl, EquipmentType.None);
                    break;
                case 14:     // Cover //
                    if (!iBool_AllowAddCover)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Equipment_Edit", coverGridControl, EquipmentType.None);
                    break;
                case 15:     // work //
                    if (!iBool_AllowAddWorkDetail)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Equipment_Edit", workDetailGridControl, EquipmentType.None);
                    break;
                case 16:     // Purpose //
                    if (!iBool_AllowAddPurpose)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Equipment_Edit", purposeGridControl, EquipmentType.None);
                    break;
                case 17:     // Fuel Cards //
                    if (!iBool_AllowAddFuelCard)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Equipment_Edit", fuelCardGridControl, EquipmentType.None);
                    break;
                case 18:     // Speeding //
                    if (!iBool_AllowAddSpeeding)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Equipment_Edit", speedingGridControl, EquipmentType.None);
                    break;
                default:
                    break;
            }
        }

        private void Insert_Record()
        {
            switch (i_int_FocusedGrid)
            {
                case 18:
                    if (!iBool_AllowAddIncident)
                        return;

                    string autoInsertIncident = "Do you want to auto add selected speeding record(s) into the incident register?";
                    switch (XtraMessageBox.Show(autoInsertIncident, "Auto Insert Option", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1))
                    {
                        case DialogResult.No:
                            // cancel operation //
                            XtraMessageBox.Show("You can manually enter the incident in the Incident Form", "Opening Incident Form", MessageBoxButtons.OK);
                            OpenEditForm(FormMode.add, "frm_AS_Equipment_Edit", incidentGridControl, EquipmentType.None);
                            break;
                        case DialogResult.Yes:
                            DateTime dateHappened;
                            string district, notes;
                            int equipID;

                            this.sp_AS_11000_PL_Incident_TypeTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11000_PL_Incident_Type, 31);
                            this.sp_AS_11000_PL_Incident_StatusTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11000_PL_Incident_Status, 30);
                            this.sp_AS_11000_PL_Legal_ActionTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11000_PL_Legal_Action, 38);
                            this.sp_AS_11000_PL_SeverityTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11000_PL_Severity, 36);
                            this.sp_AS_11000_PL_Incident_Repair_DueTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11000_PL_Incident_Repair_Due, 32);
                            this.sp_AS_11000_PL_Keeper_At_FaultTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11000_PL_Keeper_At_Fault, 37);
                            // insert record into incident table //
                            // DataRow[] speedingDR = this.dataSet_AS_DataEntry.sp_AS_11105_Speeding_Item.Select("SpeedingID  = '");

                            //int equipID = ((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11105_Over_Speeding_ItemRow)(speedingDR[0])).EquipmentID;

                            int incStatusID = Convert.ToInt32((this.dataSet_AS_DataEntry.sp_AS_11000_PL_Incident_Status.Select("Value = 'Open'"))[0]["PickListID"]);
                            int incTypeID = Convert.ToInt32((this.dataSet_AS_DataEntry.sp_AS_11000_PL_Incident_Type.Select("Value = 'Speeding'"))[0]["PickListID"]);
                            int incLegalActionID = Convert.ToInt32((this.dataSet_AS_DataEntry.sp_AS_11000_PL_Legal_Action.Select("Value = 'None'"))[0]["PickListID"]);
                            int incSeverityID = Convert.ToInt32((this.dataSet_AS_DataEntry.sp_AS_11000_PL_Severity.Select("Value = 'High'"))[0]["PickListID"]);
                            int incKeeper_At_FaultID = Convert.ToInt32((this.dataSet_AS_DataEntry.sp_AS_11000_PL_Keeper_At_Fault.Select("Value = 'No'"))[0]["PickListID"]);
                            int incRepairDueID = Convert.ToInt32((this.dataSet_AS_DataEntry.sp_AS_11000_PL_Incident_Repair_Due.Select("Value = 'None'"))[0]["PickListID"]);
                            //-->insert record
                            GridView view = null;

                            int[] intRowHandles;
                            int intCount = 0;
                            int rowInsertCount = 0;
                            view = (GridView)speedingGridControl.MainView;
                            view.PostEditor();
                            intRowHandles = view.GetSelectedRows();
                            intCount = intRowHandles.Length;
                            string strUpdatedEquipID = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                DataRow dr = view.GetDataRow(intRowHandle);
                                district = ((DataSet_AS_DataEntry.sp_AS_11105_Speeding_ItemRow)(dr)).District;
                                dateHappened = ((DataSet_AS_DataEntry.sp_AS_11105_Speeding_ItemRow)(dr)).TravelTime;
                                equipID = ((DataSet_AS_DataEntry.sp_AS_11105_Speeding_ItemRow)(dr)).EquipmentID;
                                strUpdatedEquipID += equipID + ";";
                                notes = "Speed of " + (((DataSet_AS_DataEntry.sp_AS_11105_Speeding_ItemRow)(dr)).SpeedMPH).ToString() + " MPH recorded on Road Type "
                                    + (((DataSet_AS_DataEntry.sp_AS_11105_Speeding_ItemRow)(dr)).RoadType).ToString() + " at location "
                                    + (((DataSet_AS_DataEntry.sp_AS_11105_Speeding_ItemRow)(dr)).PlaceName).ToString() + " in the "
                                    + (((DataSet_AS_DataEntry.sp_AS_11105_Speeding_ItemRow)(dr)).District).ToString() + " area.";
                                this.sp_AS_11081_Incident_ItemTableAdapter.Insert("add", "", equipID, 0, incStatusID, incTypeID, "VERILOCATION SPEEDING", dateHappened, district, incRepairDueID, incSeverityID, "Verilocation", incKeeper_At_FaultID, incLegalActionID, dateHappened.AddDays(7), 0, notes);

                                rowInsertCount++;
                            }
                                                        
                            XtraMessageBox.Show(rowInsertCount + " Speeding incidents added.", "Auto Update Complete", MessageBoxButtons.OK);


                            break;
                    }
                    break;
            }
        }

        private void Block_Add()
        {
            switch (i_int_FocusedGrid)
            {
                case 0:     // Equipment //     
                case 1:     // vehicle //
                case 2:     // plant //
                // case 3:     // gadget //
                //case 4:     // hardware //
                case 5:     // software //
                case 6:     // office //                       
                    XtraMessageBox.Show("Block adding is restricted.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    break;
                case 7:     // Billings //
                    if (!iBool_AllowAddBilling)
                        return;
                    OpenEditForm(FormMode.blockadd, "frm_AS_Equipment_Edit", billingGridControl, EquipmentType.None);
                    break;
                case 8:     // Keeper //
                    if (!iBool_AllowAddKeeper)
                        return;
                    OpenEditForm(FormMode.blockadd, "frm_AS_Equipment_Edit", keeperGridControl, EquipmentType.None);
                    break;
                case 9:     // Transaction //
                    if (!iBool_AllowAddTransactions)
                        return;
                    OpenEditForm(FormMode.blockadd, "frm_AS_Equipment_Edit", transactionGridControl, EquipmentType.None);
                    break;
                case 11:     // Depreciation //
                    if (!iBool_AllowAddDepreciation)
                        return;
                    OpenEditForm(FormMode.blockadd, "frm_AS_Equipment_Edit", depreciationGridControl, EquipmentType.None);
                    break;
                case 12:     // Incident //
                    if (!iBool_AllowAddIncident)
                        return;
                    OpenEditForm(FormMode.blockadd, "frm_AS_Equipment_Edit", incidentGridControl, EquipmentType.None);
                    break;
                case 13:     // serviceData //
                    if (!iBool_AllowAddServiceData)
                        return;
                    OpenEditForm(FormMode.blockadd, "frm_AS_Equipment_Edit", serviceDataGridControl, EquipmentType.None);
                    break;
                case 14:     // Cover //
                    if (!iBool_AllowAddCover)
                        return;
                    OpenEditForm(FormMode.blockadd, "frm_AS_Equipment_Edit", coverGridControl, EquipmentType.None);
                    break;
                case 15:     // Work //
                    if (!iBool_AllowAddWorkDetail)
                        return;
                    OpenEditForm(FormMode.blockadd, "frm_AS_Equipment_Edit", workDetailGridControl, EquipmentType.None);
                    break;
                case 16:     // Purpose //
                    if (!iBool_AllowAddPurpose)
                        return;
                    OpenEditForm(FormMode.blockadd, "frm_AS_Equipment_Edit", purposeGridControl, EquipmentType.None);
                    break;
                case 17:     // Fuel Cards //
                    if (!iBool_AllowAddFuelCard)
                        return;
                    OpenEditForm(FormMode.blockadd, "frm_AS_Equipment_Edit", fuelCardGridControl, EquipmentType.None);
                    break;
                case 19:     // Road Tax //
                    if (!iBool_AllowAddRoadTax)
                        return;
                    OpenEditForm(FormMode.blockadd, "frm_AS_Equipment_Edit", roadTaxGridControl, EquipmentType.None);
                    break;
                case 4:     // Tracker Information //
                    if (!iBool_AllowAddTracker)
                        return;
                    OpenEditForm(FormMode.blockadd, "frm_AS_Equipment_Edit", trackerGridControl, EquipmentType.None);
                    break;
                case 18:     // speeding //
                    if (!iBool_AllowAddSpeeding)
                        return;
                    OpenEditForm(FormMode.blockadd, "frm_AS_Equipment_Edit", speedingGridControl, EquipmentType.None);
                    break;
                default:
                    break;
            }
        }

        private void Block_Edit()
        {
            switch (i_int_FocusedGrid)
            {
                case 19:     // Road Tax //
                    if (!iBool_AllowEditRoadTax)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Equipment_Edit", roadTaxGridControl, EquipmentType.None);
                    break;
                case 4:     // Tracker //
                    if (!iBool_AllowEditTracker)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Equipment_Edit", trackerGridControl, EquipmentType.None);
                    break;
               
                case 7:     // Billings //
                    if (!iBool_AllowEditBilling)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Equipment_Edit", billingGridControl, EquipmentType.None);
                    break;
                case 8:     // Keeper //
                    if (!iBool_AllowEditKeeper)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Equipment_Edit", keeperGridControl, EquipmentType.None);
                    break;
                case 9:     // Transaction //
                    if (!iBool_AllowEditTransactions)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Equipment_Edit", transactionGridControl, EquipmentType.None);
                    break;
                //case 10:     // Reminders //
                //    if (!iBool_AllowEditReminders)
                //        return;
                //    OpenEditForm(FormMode.blockedit, "frm_AS_Equipment_Edit", officeGridControl, EquipmentType.Office);
                //    break;
                case 11:     // Depreciation //
                    if (!iBool_AllowEditDepreciation)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Equipment_Edit", depreciationGridControl, EquipmentType.None);
                    break;
                case 12:     // Incident //
                    if (!iBool_AllowEditIncident)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Equipment_Edit", incidentGridControl, EquipmentType.None);
                    break;
                case 13:     // serviceData //
                    if (!iBool_AllowEditServiceData)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Equipment_Edit", serviceDataGridControl, EquipmentType.None);
                    break;
                case 14:     // Cover //
                    if (!iBool_AllowEditCover)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Equipment_Edit", coverGridControl, EquipmentType.None);
                    break;
                case 15:     // Work //
                    if (!iBool_AllowEditWorkDetail)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Equipment_Edit", workDetailGridControl, EquipmentType.None);
                    break;
                case 16:     // Purpose //
                    if (!iBool_AllowEditPurpose)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Equipment_Edit", purposeGridControl, EquipmentType.None);
                    break;
                case 17:     // Fuel Cards //
                    if (!iBool_AllowEditFuelCard)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Equipment_Edit", fuelCardGridControl, EquipmentType.None);
                    break;
                case 18:     // Speeding //
                    if (!iBool_AllowEditSpeeding)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Equipment_Edit", speedingGridControl, EquipmentType.None);
                    break;
                default:
                    break;
            }
        }

        private void Edit_Record()
        {
            switch (i_int_FocusedGrid)
            {
                case 19:     // Road Tax //
                    if (!iBool_AllowEditRoadTax)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Equipment_Edit", roadTaxGridControl, EquipmentType.None);
                    break;
                case 4:     // Tracker //
                    if (!iBool_AllowEditTracker)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Equipment_Edit", trackerGridControl, EquipmentType.None);
                    break;
               
                case 7:     // Billings //
                    if (!iBool_AllowEditBilling)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Equipment_Edit", billingGridControl, EquipmentType.None);
                    break;
                case 8:     // Keeper //
                    if (!iBool_AllowEditKeeper)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Equipment_Edit", keeperGridControl, EquipmentType.None);
                    break;
                case 9:     // Transaction //
                    if (!iBool_AllowEditTransactions)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Equipment_Edit", transactionGridControl, EquipmentType.None);
                    break;
                //case 10:     // Reminders //
                //    if (!iBool_AllowEditReminders)
                //        return;
                //    OpenEditForm(FormMode.edit, "frm_AS_Equipment_Edit", officeGridControl, EquipmentType.Office);
                //    break;
                case 11:     // Depreciation //
                    if (!iBool_AllowEditDepreciation)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Equipment_Edit", depreciationGridControl, EquipmentType.None);
                    break;
                case 12:     // Incident //
                    if (!iBool_AllowEditIncident)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Equipment_Edit", incidentGridControl, EquipmentType.None);
                    break;
                case 13:     // serviceData //
                    if (!iBool_AllowEditServiceData)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Equipment_Edit", serviceDataGridControl, EquipmentType.None);
                    break;
                case 14:     // Cover //
                    if (!iBool_AllowEditCover)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Equipment_Edit", coverGridControl, EquipmentType.None);
                    break;
                case 15:     // Work //
                    if (!iBool_AllowEditWorkDetail)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Equipment_Edit", workDetailGridControl, EquipmentType.None);
                    break;
                case 16:     // Purpose //
                    if (!iBool_AllowEditPurpose)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Equipment_Edit", purposeGridControl, EquipmentType.None);
                    break;
                case 17:     // Fuel Cards //
                    if (!iBool_AllowEditFuelCard)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Equipment_Edit", fuelCardGridControl, EquipmentType.None);
                    break;
                case 18:     // speeding //
                    if (!iBool_AllowEditSpeeding)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Equipment_Edit", speedingGridControl, EquipmentType.None);
                    break;
                default:
                    break;
            }

        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridControl gridControl = null;
            GridView view = null;
            string strMessage = "";


            switch (i_int_FocusedGrid)
            {
                case 19:     // road tax
                    if (!iBool_AllowDeleteRoadTax)
                        return;
                    view = (GridView)roadTaxGridControl.MainView;
                    break;
                case 4:     // Tracker 
                    if (!iBool_AllowDeleteTracker)
                        return;
                    view = (GridView)trackerGridControl.MainView;
                    break;
               
                case 7:     // Billing
                    if (!iBool_AllowDeleteBilling)
                        return;
                    view = (GridView)billingGridControl.MainView;
                    break;
                case 8:     // Keeper
                    if (!iBool_AllowDeleteKeeper)
                        return;
                    view = (GridView)keeperGridControl.MainView;
                    break;
                case 9:     // Transaction
                    if (!iBool_AllowDeleteTransactions)
                        return;
                    view = (GridView)transactionGridControl.MainView;
                    break;
                case 11:     // Depreciation
                    if (!iBool_AllowDeleteDepreciation)
                        return;
                    view = (GridView)depreciationGridControl.MainView;
                    break;
                case 12:     // Incident
                    if (!iBool_AllowDeleteIncident)
                        return;
                    view = (GridView)incidentGridControl.MainView;
                    break;
                case 13:     // serviceData
                    if (!iBool_AllowDeleteServiceData)
                        return;
                    view = (GridView)serviceDataGridControl.MainView;
                    break;
                case 14:     // Cover
                    if (!iBool_AllowDeleteCover)
                        return;
                    view = (GridView)coverGridControl.MainView;
                    break;
                case 15:     // Work
                    if (!iBool_AllowDeleteWorkDetail)
                        return;
                    view = (GridView)workDetailGridControl.MainView;
                    break;
                case 16:     // Purpose
                    if (!iBool_AllowDeletePurpose)
                        return;
                    view = (GridView)purposeGridControl.MainView;
                    break;
                case 17:     // Fuel Cards
                    if (!iBool_AllowDeleteFuelCard)
                        return;
                    view = (GridView)fuelCardGridControl.MainView;
                    break;
                case 18:     // Speeding
                    if (!iBool_AllowDeleteSpeeding)
                        return;
                    view = (GridView)speedingGridControl.MainView;
                    break;
                
            }
            getCurrentGridControl( out strMessage1, out strMessage2);

            
            view.PostEditor();
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;

            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more Equipment records to delete.", "No Equipment Records To Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Checks passed so delete selected record(s) //
            strMessage = "You have " + (intCount == 1 ? strMessage1 : Convert.ToString(intRowHandles.Length) + strMessage2) +
            " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this record" : "these records") +
            " will no longer be available for selection and any related records will also be deleted!";
            if (XtraMessageBox.Show(strMessage, "Permanently Delete Record(s)", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                frmProgress fProgress = new frmProgress(20);
                fProgress.UpdateCaption("Deleting...");
                fProgress.Show();
                Application.DoEvents();

                string strRecordsToLoad = "";
                string strChildLinkedRecordsToLoad = "";
                foreach (int intRowHandle in intRowHandles)
                {
                    DataRow dr = view.GetDataRow(intRowHandle);
                    switch (i_int_FocusedGrid)
                    {
                        case 0:     // Equipment
                        case 1:     // Vehicle
                        case 2:     // Plant 
                        //case 3:     // Gadget 
                        //case 4:     // Hardware 
                        case 5:     // Software 
                        case 6:     // Office
                            strRecordsToLoad += (((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentID).ToString() + ',';
                            break;
                        case 19:     // Road Tax
                            strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11096_Road_Tax_ItemRow)(dr)).RoadTaxID).ToString() + ',';
                            break;
                        case 4:     // tracker
                            strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11023_Tracker_ListRow)(dr)).TrackerInformationID).ToString() + ',';
                            break;
                        case 7:     // Billing
                            strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11047_Equipment_Billing_ItemRow)(dr)).DepreciationID).ToString() + ',';
                            break;
                        case 8:     // Keeper
                            strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_ItemRow)(dr)).KeeperAllocationID).ToString() + ',';
                            break;
                        case 9:     // Transaction
                            strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11041_Transaction_ItemRow)(dr)).TransactionID).ToString() + ',';
                            break;
                        case 11:     // Depreciation
                            strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11050_Depreciation_ItemRow)(dr)).DepreciationID).ToString() + ',';
                            break;
                        case 12:     // Incident
                            strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11081_Incident_ItemRow)(dr)).IncidentID).ToString() + ',';
                            break;
                        case 13:     // ServiceData
                            //check if there is linked data 
                            DataRow[] drs = dataSet_AS_DataEntry.sp_AS_11054_Service_Data_Item.Select("ServiceDataID = " + (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11054_Service_Data_ItemRow)(dr)).ServiceDataID).ToString());

                            if (drs.Length >= 2)
                            {
                                // delete service interval schedule
                                strChildLinkedRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11054_Service_Data_ItemRow)(dr)).ServiceIntervalScheduleID).ToString() + ',';
                            }

                            strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11054_Service_Data_ItemRow)(dr)).ServiceDataID).ToString() + ',';
                            break;
                        case 14:     // Cover
                            strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11075_Cover_ItemRow)(dr)).CoverID).ToString() + ',';
                            break;
                        case 15:     // WorkDetail
                            strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11078_Work_Detail_ItemRow)(dr)).WorkDetailID).ToString() + ',';
                            break;
                        case 16:     // Purpose
                            strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11084_Purpose_ItemRow)(dr)).EquipmentPurposeID).ToString() + ',';
                            break;
                        case 17:     // FuelCard
                            strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11089_Fuel_Card_ItemRow)(dr)).FuelCardID).ToString() + ',';
                            break;
                        case 18:     // speeding
                            strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11105_Speeding_ItemRow)(dr)).SpeedingID).ToString() + ',';
                            break;
                        default:
                            strRecordsToLoad = "";
                            break;
                    }
                }
                if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

               
                if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                switch (i_int_FocusedGrid)
                {
                    case 0:     // Equipment
                    case 1:     // Vehicle
                    case 2:     // Plant 
                    //case 3:     // Gadget 
                    // case 4:     // Hardware 
                    case 5:     // Software 
                    case 6:     // Office
                        sp_AS_11002_Equipment_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                        break;
                    case 19:     // Road Tax
                        sp_AS_11096_Road_Tax_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                        break;
                    case 4:     // Tracker
                        sp_AS_11023_Tracker_ListTableAdapter.Delete("delete", strRecordsToLoad);
                        break;
                    case 7:     // Billing
                        //sp_AS_11047_Equipment_Billing_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                        break;
                    case 8:     // Keeper
                        sp_AS_11044_Keeper_Allocation_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                        break;
                    case 9:     // Transaction
                        sp_AS_11041_Transaction_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                        break;
                    case 11:     // Depreciation
                        sp_AS_11050_Depreciation_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                        break;
                    case 12:     // Incident
                        sp_AS_11081_Incident_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                        break;                   
                    case 13:     // Service Data
                        if (strChildLinkedRecordsToLoad != "")
                        {
                            sp_AS_11054_Service_Data_ItemTableAdapter.Delete("del_child", strChildLinkedRecordsToLoad);
                            UpdateFormRefreshStatus(1, strRecordIDs, "", "");
                        }
                        else if (strRecordsToLoad != "")
                        {
                            sp_AS_11054_Service_Data_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                            UpdateFormRefreshStatus(1, strRecordIDs, "", "");
                        }
                        break;
                    case 14:     // Cover
                        sp_AS_11075_Cover_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                        break;
                    case 15:     // WorkDetail
                        sp_AS_11078_Work_Detail_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                        break;
                    case 16:     // Purpose
                        sp_AS_11084_Purpose_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                        break;
                    case 17:     // Fuel Card
                        sp_AS_11089_Fuel_Card_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                        break;
                    case 18:     // Speeding
                        sp_AS_11105_Speeding_ItemTableAdapter.Delete("delete", strRecordsToLoad);           //UpdateFormRefreshStatus(1, strRecordIDs, "", "");
                        break;
                }
                if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                LoadLinkedRecords((((DataRowView)(spAS11002EquipmentItemBindingSource.CurrencyManager.Current)).Row).ItemArray[0].ToString());

                if (fProgress != null)
                {
                    fProgress.UpdateProgress(20); // Update Progress Bar //
                    fProgress.Close();
                    fProgress = null;
                }
                if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void getCurrentGridControl(out string strMessage1, out string strMessage2)
        {
            switch (i_int_FocusedGrid)
            {
                case 19://road tax
                    strMessage1 = "1 Road Tax record";
                    strMessage2 = " Road Tax records";

                    break;
                case 4://Tracker
                    strMessage1 = "1 Verilocation Tracker record";
                    strMessage2 = " Verilocation Tracker records";

                    break;
                case 7://billing 
                    strMessage1 = "1 Billing record";
                    strMessage2 = " Billing records";

                    break;
                case 8://keeper
                    strMessage1 = "1 Keeper record";
                    strMessage2 = " Keeper records";

                    break;
                case 9://transaction
                    strMessage1 = "1 Transaction record";
                    strMessage2 = " Transaction records";

                    break;
                case 11://depreciation
                    strMessage1 = "1 Depreciation record";
                    strMessage2 = " Depreciation records";

                    break;
                case 12://Incident
                    strMessage1 = "1 Incident record";
                    strMessage2 = " Incident records";

                    break;
                case 13://Service Interval

                    strMessage1 = "1 Service Interval record";
                    strMessage2 = " Service Interval records";

                    break;
                case 14://Cover
                    strMessage1 = "1 Cover record";
                    strMessage2 = " Cover records";

                    break;
                case 15://WorkDetail

                    strMessage1 = "1 Work Order Detail record";
                    strMessage2 = " Work Order Detail records";

                    break;
                case 16://Purpose

                    strMessage1 = "1 Purpose record";
                    strMessage2 = " Purpose records";

                    break;
                case 17://Fuel Cards

                    strMessage1 = "1 Fuel Card record";
                    strMessage2 = " Fuel Card records";

                    break;
                case 18://Speeding

                    strMessage1 = "1 Speeding record";
                    strMessage2 = " Speeding records";

                    break;
                default:
                    strMessage1 = "1 record";
                    strMessage2 = " records";
                    break;

            }
        }


        private void commonView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            // This event replace the default functionality code of the FilterEditor (Column editor type is bound to column's RepositoryItem from grid - this uses a combo box instead to present the available values). //
            // Date Fields are ignored [default DatePicker used]. //
            GridView view = (GridView)sender;
            customFilterDraw(view, e);
        }

        private void equipmentView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion

        #region CRUD Events

        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnBlockAddEvent(object sender, EventArgs e)
        {
            Block_Add();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void View_Record()
        {
            switch (i_int_FocusedGrid)
            {

                case 19:     // Road Tax //
                    if (!iBool_AllowEditRoadTax)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Equipment_Edit", roadTaxGridControl, EquipmentType.None);
                    break;
                case 4:     // Tracker //
                    if (!iBool_AllowEditTracker)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Equipment_Edit", trackerGridControl, EquipmentType.None);
                    break;

                case 7:     // Billings //
                    if (!iBool_AllowEditBilling)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Equipment_Edit", billingGridControl, EquipmentType.None);
                    break;
                case 8:     // Keeper //
                    if (!iBool_AllowEditKeeper)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Equipment_Edit", keeperGridControl, EquipmentType.None);
                    break;
                case 9:     // Transaction //
                    if (!iBool_AllowEditTransactions)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Equipment_Edit", transactionGridControl, EquipmentType.None);
                    break;
                //case 10:     // Reminders //
                //    if (!iBool_AllowEditReminders)
                //        return;
                //    OpenEditForm(FormMode.view, "frm_AS_Equipment_Edit", officeGridControl, EquipmentType.Office);
                //    break;
                case 11:     // Depreciation //
                    if (!iBool_AllowEditDepreciation)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Equipment_Edit", depreciationGridControl, EquipmentType.None);
                    break;
                case 12:     // Incident //
                    if (!iBool_AllowEditIncident)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Equipment_Edit", incidentGridControl, EquipmentType.None);
                    break;
                case 13:     // ServiceInterval //
                    if (!iBool_AllowEditServiceData)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Equipment_Edit", serviceDataGridControl, EquipmentType.None);
                    break;
                case 14:     // Cover //
                    if (!iBool_AllowEditCover)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Equipment_Edit", coverGridControl, EquipmentType.None);
                    break;
                case 15:     // Work //
                    if (!iBool_AllowEditWorkDetail)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Equipment_Edit", workDetailGridControl, EquipmentType.None);
                    break;
                case 16:     // Purpose //
                    if (!iBool_AllowEditPurpose)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Equipment_Edit", purposeGridControl, EquipmentType.None);
                    break;
                case 17:     // Fuel Cards //
                    if (!iBool_AllowEditFuelCard)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Equipment_Edit", fuelCardGridControl, EquipmentType.None);
                    break;
                case 18:     // speeding //
                    if (!iBool_AllowEditSpeeding)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Equipment_Edit", speedingGridControl, EquipmentType.None);
                    break;
                default:
                    break;
            }
        }

        #endregion


        private void lueOwnership_Status_EditValueChanged(object sender, EventArgs e)
        {
            P11dManagerPromptRequired = false;

            if (bool_FormLoading)
                return;

            if (lueOwnership_Status.Text != "Sold")
                return;

            if (this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item == null)
                return;

            if (this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item.Count == 0)
                return;

            DataRow[] ActiveKeepers = this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item.Select("AllocationStatus = 'Active'");

            if (ActiveKeepers.Length == 0)
                return;

            XtraMessageBox.Show("Ownership status of Sold will end the current Active keeper allocation.",
                                "Equipment Edit",MessageBoxButtons.OK,MessageBoxIcon.Information);

            P11dManagerPromptRequired = true;
        }

        private void cpeColour_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            if (bool_FormLoading)
                return;

            e.NewValue = Regex.Replace(e.NewValue.ToString().Remove(0, 7), "]", "");

        }



        #endregion

        #endregion



        #endregion

        #endregion

        #endregion











    }
}


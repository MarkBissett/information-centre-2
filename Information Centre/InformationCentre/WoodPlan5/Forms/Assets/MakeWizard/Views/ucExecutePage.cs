﻿using DevExpress.XtraEditors;
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace WoodPlan5.Forms.Assets.MakeWizard.Views
{
    public partial class ucExecutePage : Views.BaseWizardPage
    {
        public ucExecutePage()
        {
            InitializeComponent();
        }

        #region Declaration

        private bool blnOkToFinish;

        #endregion

        void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            for (int i = 0; i <= 10; i++)
            {
                switch (i)
                {
                    case 0:
                        SaveChanges();
                        break;
                }
                System.Threading.Thread.Sleep(50);
                ((BackgroundWorker)sender).ReportProgress(i);
            }
        }

        void bgWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressEdit.PerformStep();
        }

        void startButton_Click(object sender, EventArgs e)
        {
            if (!bgWorker.IsBusy)
            {
                btnCommit.Enabled = false;
                bgWorker.RunWorkerAsync();
            }
            
        }

        void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!e.Cancelled && blnOkToFinish)
                ((ViewModels.ExecutePageViewModel)PageViewModel).IsComplete = true;
            WizardViewModel.PageCompleted();
        }

        private void ErrorCompletingWizard(string errorMessage)
        {
            if (errorMessage == "")
            {
                XtraMessageBox.Show("An error occurred whilst trying to complete the wizard. No changes have been saved.", "Error Completing the Wizard", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                XtraMessageBox.Show(errorMessage, "Error Completing the Wizard", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void SaveChanges()
        {
            blnOkToFinish = false;
            switch (ucStep2Page.strSelectedAction)
            {
                case "addmake"://add make
                    if (ucStep2Page.intAddCategory != 0 && ucStep2Page.strAddMake != "")//Adding Make
                    {
                        try
                        {
                            sp_AS_11014_Make_ListTableAdapter.Insert("add", "", 0, ucStep2Page.strAddMake, ucStep2Page.intAddCategory);//Add new make
                            blnOkToFinish = true;                          
                        }
                        catch (Exception ex)
                        {
                            ErrorCompletingWizard(ex.Message);
                        }
                    }
                    else
                    {
                        ErrorCompletingWizard("An error occurred whilst trying to complete the wizard - Invalid value supplied. No changes have been made. Please correct before proceeding.");
                    }
                    break;
                case "addmodel"://add model
                    if (ucStep2Page.intAddMake != 0 && ucStep2Page.strAddModel != "")//Adding Model
                    {
                        try
                        {
                            sp_AS_11017_Model_ListTableAdapter.Insert("add", "", 0, ucStep2Page.strAddModel, ucStep2Page.intAddMake);//Add new make
                            blnOkToFinish = true;  
                        }
                        catch (Exception ex)
                        {
                            ErrorCompletingWizard(ex.Message);
                        }
                    }
                    else
                    {
                        ErrorCompletingWizard("An error occurred whilst trying to complete the wizard - Invalid value supplied. No changes have been made. Please correct before proceeding.");
                    }
                    break;
                case "editmake"://edit make
                    if (ucStep2Page.intEditMake != 0 && ucStep2Page.strEditMake != "")//Edit Make
                    {
                        try
                        {
                            sp_AS_11014_Make_ListTableAdapter.Update("edit", ucStep2Page.intEditMake.ToString(), ucStep2Page.intEditMake, ucStep2Page.strEditMake,0);//edit Make
                            blnOkToFinish = true;  
                        }
                        catch (Exception ex)
                        {
                            ErrorCompletingWizard(ex.Message);
                        }
                    }
                    else
                    {
                        ErrorCompletingWizard("An error occurred whilst trying to complete the wizard - Invalid value supplied. No changes have been made. Please correct before proceeding.");
                    }
                    break;
                case "editmodel"://edit model
                    if (ucStep2Page.intEditModel != 0 && ucStep2Page.strEditModel != "")//Edit Make
                    {
                        try
                        {
                            sp_AS_11017_Model_ListTableAdapter.Update("edit", ucStep2Page.intEditModel.ToString(), ucStep2Page.intEditModel, ucStep2Page.strEditModel, 0);//edit Make
                            blnOkToFinish = true;  
                        }
                        catch (Exception ex)
                        {
                            ErrorCompletingWizard(ex.Message);
                        }
                    }
                    else
                    {
                        ErrorCompletingWizard("An error occurred whilst trying to complete the wizard - Invalid value supplied. No changes have been made. Please correct before proceeding.");
                    }
                    break;                    
                default:
                    ErrorCompletingWizard("");
                    break;
            }
        }

        private void ucExecutePage_Enter(object sender, EventArgs e)
        {
            switch (ucStep2Page.strSelectedAction)
            {
                case "addmake"://add make
                    if (ucStep2Page.intAddCategory != 0 && ucStep2Page.strAddMake != "" && ucStep2Page.strNewCategory !="")//Adding Make
                    {
                        lblText1.Text = "New make : ";
                        lblText2.Text = "Make Category : ";
                        lblChange1.Text = ucStep2Page.strAddMake;
                        lblChange2.Text = ucStep2Page.strNewCategory;
                    }
                    else
                    {
                        resetLabels();   
                    }
                    break;
                case "addmodel"://add model
                    if (ucStep2Page.intAddMake != 0 && ucStep2Page.strAddModel != "" && ucStep2Page.strNewMake != "")//Adding Model
                    {
                        lblText1.Text = "New model : ";
                        lblText2.Text = "Model make : ";
                        lblChange1.Text = ucStep2Page.strAddModel;
                        lblChange2.Text = ucStep2Page.strNewMake;
                    }
                    else
                    {
                        resetLabels();   
                    }
                    break;
                case "editmake"://edit make
                    if (ucStep2Page.intEditMake != 0 && ucStep2Page.strEditMake != "" && ucStep2Page.strOldMake !="")//Edit Make
                    {
                        lblText1.Text = "Old make : ";
                        lblText2.Text = "New make : ";
                        lblChange1.Text = ucStep2Page.strOldMake;
                        lblChange2.Text = ucStep2Page.strEditMake;
                    }
                    else
                    {
                        resetLabels();   
                    }
                    break;
                case "editmodel"://edit model
                    if (ucStep2Page.intEditModel != 0 && ucStep2Page.strEditModel != "" && ucStep2Page.strOldModel != "")//Edit Make
                    {
                        lblText1.Text = "Old model : ";
                        lblText2.Text = "New model : ";
                        lblChange1.Text = ucStep2Page.strOldModel;
                        lblChange2.Text = ucStep2Page.strEditModel;
                    }
                    else
                    {
                        resetLabels();  
                    }
                    break;
                default:
                    resetLabels();                  
                    break;
            }
        }

        private void resetLabels()
        {
            lblText1.Text = "";
            lblText2.Text = "";
            lblChange1.Text = "";
            lblChange2.Text = "";
        }

    }
}
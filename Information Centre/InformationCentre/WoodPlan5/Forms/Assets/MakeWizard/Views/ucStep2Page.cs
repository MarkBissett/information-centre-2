﻿using System.Windows.Forms;
using DevExpress.XtraEditors.Controls;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Data;
using System;

namespace WoodPlan5.Forms.Assets.MakeWizard.Views
{
    public partial class ucStep2Page : Views.BaseWizardPage
    {
        public ucStep2Page()
        {
            InitializeComponent();
            LoadAdapters();
        }

        #region Declaration

        public static string strSelectedAction;
        
        public static string strAddMake;
        public static int intAddCategory;
        public static string strNewCategory;
         

        public static string strAddModel;
        public static int intAddMake;
        public static string strNewMake;

        public static string strEditMake;
        public static string strOldMake;
        public static int intEditMake;

        public static string strEditModel;
        public static string strOldModel;
        public static int intEditModel;

        public enum wizardMode { Add_Make, Add_Model, Edit_Make, Edit_Model }

        #endregion

        #region Methods

        private void updateValues()
        {
            resetValues();

            switch (strSelectedAction)
            {
                case "addmake"://Add Make
                    strAddMake = txtAddStepMakeDescription.Text;
                    intAddCategory =Convert.ToInt32(lueAddStepCategory.EditValue);
                    strNewCategory = lueAddStepCategory.Properties.GetDisplayText(lueAddStepCategory.EditValue);
                    break;
                case "addmodel": //Add Model 
                    strAddModel = txtAddStepModelDescription.Text;
                    intAddMake = Convert.ToInt32(lueAddStepMake.EditValue);
                    strNewMake = lueAddStepMake.Properties.GetDisplayText(lueAddStepMake.EditValue);
                    break;
                case "editmake"://Edit Make 
                    strEditMake = txtEditStepMakeDescription.Text;
                    intEditMake = Convert.ToInt32(lueEditStepMake.EditValue);
                    strOldMake = lueEditStepMake.Properties.GetDisplayText(lueEditStepMake.EditValue);
                    break;
                case "editmodel"://Edit Model 
                    strEditModel = txtEditStepModelDescription.Text;
                    intEditModel = Convert.ToInt32(lueEditStepModel.EditValue);
                    strOldModel = lueEditStepModel.Properties.GetDisplayText(lueEditStepModel.EditValue);
                    break;
            }
        }

        private void resetValues()
        {
            strAddMake = "";
            intAddCategory = 0;

            strAddModel = "";
            intAddMake = 0;

            strEditMake = "";
            intEditMake = 0;

            strEditModel = "";
            intEditModel = 0;

            strOldMake = "";
            strOldModel = "";
            strNewCategory = "";
            strNewMake = "";
        }

        private bool validatePage()
        {
            bool validTextBox;
            bool valid = false;
            dxErrorProvider1.ClearErrors();
            switch (strSelectedAction)
            {
                case "addmake"://add make
                    validTextBox = validateTxtDescription(txtAddStepMakeDescription);
                    if (lueAddStepCategory.Properties.GetDisplayText(lueAddStepCategory.EditValue) != "" && validTextBox)
                    {
                        valid = true;
                    }
                    else if (lueAddStepCategory.Properties.GetDisplayText(lueAddStepCategory.EditValue) == "")
                    {
                        dxErrorProvider1.SetError(lueAddStepCategory, "Please select a category before proceeding.");
                        valid = false;
                    }
                    else if (validTextBox == false)
                    {
                        if (txtAddStepMakeDescription.ErrorText == "")
                        {
                            dxErrorProvider1.SetError(txtAddStepMakeDescription, "Please make sure the new Make is Unique and valid text has been entered.");
                        }
                        valid = false;
                    }
                    break;
                case "addmodel"://add model

                    validTextBox = validateTxtDescription(txtAddStepModelDescription);
                    if (lueAddStepMake.Properties.GetDisplayText(lueAddStepMake.EditValue) != "" && validTextBox)
                    {
                        valid = true;
                    }
                    else if (lueAddStepMake.Properties.GetDisplayText(lueAddStepMake.EditValue) == "")
                    {
                        dxErrorProvider1.SetError(lueAddStepMake, "Please select a Make before proceeding.");
                        valid = false;
                    }
                    else if (validTextBox == false)
                    {
                        if (txtAddStepModelDescription.ErrorText == "")
                        {
                            dxErrorProvider1.SetError(txtAddStepModelDescription, "Please make sure the new Model is Unique and valid text has been entered.");
                        }
                        valid = false;
                    }
                    break;
                case "editmake"://edit make
                    validTextBox = validateTxtDescription(txtEditStepMakeDescription);
                    if (lueEditStepMake.Properties.GetDisplayText(lueEditStepMake.EditValue) != "" && validTextBox)
                    {
                        valid = true;
                    }
                    else if (lueEditStepMake.Properties.GetDisplayText(lueEditStepMake.EditValue) == "")
                    {
                        dxErrorProvider1.SetError(lueEditStepMake, "Please select value to edit before proceeding.");
                        valid = false;
                    }
                    else if (validTextBox == false)
                    {
                        if (txtEditStepMakeDescription.ErrorText == "")
                        {
                            dxErrorProvider1.SetError(txtEditStepMakeDescription, "Please make sure the Make is Unique and valid text has been entered.");
                        }
                        valid = false;
                    }
                    break;
                case "editmodel"://edit make
                    validTextBox = validateTxtDescription(txtEditStepModelDescription);
                    if (lueEditStepModel.Properties.GetDisplayText(lueEditStepModel.EditValue) != "" && validTextBox)
                    {
                        valid = true;
                    }
                    else if (lueEditStepModel.Properties.GetDisplayText(lueEditStepModel.EditValue) == "")
                    {
                        dxErrorProvider1.SetError(lueEditStepModel, "Please select a value to edit before proceeding.");
                        valid = false;
                    }
                    else if (validTextBox == false)
                    {
                        if (txtEditStepModelDescription.ErrorText == "")
                        {
                            dxErrorProvider1.SetError(txtEditStepModelDescription, "Please make sure the Model is Unique and valid text has been entered.");
                        }
                        valid = false;
                    }
                    break;
               
            }
            return valid;
        }
        
        private bool checkDuplicate(DevExpress.XtraEditors.TextEdit txtBox, int Make_Model_ID, wizardMode mode, int parentGroup)
        {
            bool result = true;
            string errorText = "";
            string Make_Or_Model = "";

            if (mode == wizardMode.Add_Make || mode == wizardMode.Edit_Make)
            {
                errorText = "Make already exists.";
                Make_Or_Model = "make";
            }
            else if (mode == wizardMode.Add_Model || mode == wizardMode.Edit_Model)
            {
                errorText = "Model already exists.";
                Make_Or_Model = "model";
            }
            sp_AS_11000_Duplicate_Make_Model_SearchTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_Duplicate_Make_Model_Search, txtBox.Text, Make_Model_ID, Make_Or_Model, parentGroup);

            DataRow row = this.dataSet_AS_DataEntry.sp_AS_11000_Duplicate_Make_Model_Search.Rows[0];

            if (Convert.ToInt32(row["SearchCount"]) > 0)
            {
                dxErrorProvider1.SetError(txtBox, errorText);
                txtBox.Focus();
                return false;
            }
            return result;

        }

        private bool validateTxtDescription(DevExpress.XtraEditors.TextEdit txtBox)
        {
            bool valid = false;
            if (txtBox.Text != "")
                txtBox.Text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(txtBox.Text);

            string originalText = txtBox.Text;
            string parsedText = Regex.Replace(txtBox.Text, "[^a-z_A-Z0-9] ", "");

            if (originalText != parsedText || txtBox.Text == "")
            {
                txtBox.Text = parsedText;
                dxErrorProvider1.SetError(txtBox, "Please enter a valid text, avoid entering non alphanumeric characters.");
                txtBox.Focus();
            }
            else
            {
                switch (strSelectedAction)
                {
                    case "addmake"://Add Make - Check if Make exists
                        valid = checkDuplicate(txtBox, -1, wizardMode.Add_Make, Convert.ToInt32(lueAddStepCategory.EditValue.ToString()));
                        break;
                    case "addmodel": //Add Model - Check if Model exists
                        valid = checkDuplicate(txtBox, -1, wizardMode.Add_Model, Convert.ToInt32(lueAddStepMake.EditValue.ToString()));
                        break;
                    case "editmake"://Edit Make - Check if Make already exists
                        valid = checkDuplicate(txtBox, Convert.ToInt32(lueEditStepMake.EditValue.ToString()), wizardMode.Edit_Make, -1);
                        break;
                    case "editmodel"://Edit Model - Check if Model exists
                        valid = checkDuplicate(txtBox, Convert.ToInt32(lueEditStepModel.EditValue.ToString()), wizardMode.Edit_Model, -1);
                        break;
                }
            }
            return valid;
        }

        private void LoadAdapters()
        {
            sp_AS_11017_Model_ListTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11017_Model_List, "", "view");

            sp_AS_11014_Make_ListTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11014_Make_List, "", "view");

            sp_AS_11011_Equipment_Category_ListTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11011_Equipment_Category_List, "", "");
        }

        public void hideAllControls()
        {
            lcAddMake.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            lcAddModel.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            lcAddViewMake.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            lciCategory.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            lcEditMake.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            lcEditModel.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            lcEditViewMake.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            lcEditViewModel.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
        }

        #endregion

        #region "Events"

        private void btnNext_Click(object sender, System.EventArgs e)
        {
            if (validatePage())
            {
                ((ViewModels.Step2PageViewModel)PageViewModel).blnValidEntry = true;
                updateValues();
                WizardViewModel.PageCompleted();
                
            }
            else
            {
                ((ViewModels.Step2PageViewModel)PageViewModel).blnValidEntry = false;
            }
        }

        private void btnRefresh_Click(object sender, System.EventArgs e)
        {
            LoadAdapters();
        }

        private void ucStep2Page_Enter(object sender, System.EventArgs e)
        {
             hideAllControls();
            switch (strSelectedAction)
            {
                case "addmake"://add make
                    lciCategory.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    lciCategory.Location = new System.Drawing.Point(115, 110);
                    lcAddMake.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    lcAddMake.Location = new System.Drawing.Point(115, 134);
                    lblProceed.Text = "Select the category and enter the make to continue.";
                    lblAction.Text = "Adding Make";
                    break;
                case "addmodel"://add model
                    lcAddViewMake.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    lcAddViewMake.Location = new System.Drawing.Point(115, 110);
                    lcAddModel.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    lcAddModel.Location = new System.Drawing.Point(115, 134);
                    lblProceed.Text = "Select the make and enter the model to continue.";
                    lblAction.Text = "Adding Model";
                    break;
                case "editmake"://edit make
                    lcEditViewMake.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    lcEditViewMake.Location = new System.Drawing.Point(115, 110);
                    lcEditMake.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    lcEditMake.Location = new System.Drawing.Point(115, 134);                                                                                           
                    lblProceed.Text = "Select the make to edit and enter the new make description to continue.";
                    lblAction.Text = "Amending Make";
                    break;
                case "editmodel"://edit make
                    lcEditModel.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    lcEditModel.Location = new System.Drawing.Point(115, 110);
                    lcEditViewModel.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    lcEditViewModel.Location = new System.Drawing.Point(115, 134);    
                    lblProceed.Text = "Select the model to edit and enter the new model description to continue.";
                    lblAction.Text = "Amending Model";
                    break;
            }
        }

        #endregion
        
    }
}
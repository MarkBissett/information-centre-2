﻿namespace WoodPlan5.Forms.Assets.MakeWizard.ViewModels
{
    class Step2PageViewModel : IWizardPageViewModel
    {
        public bool IsComplete
        {
            //get { return !string.IsNullOrEmpty(Path) && System.IO.Directory.Exists(Path); }
            get
            {
                return blnValidEntry;
            }
        }
        public bool blnValidEntry
        {
            get;
            set;
        }
        public bool CanReturn { get { return true; } }


        public bool pageIsValid
        {
            get { return true; }
        }
    }
}
﻿namespace WoodPlan5.Forms.Assets.MakeWizard.ViewModels
{
    class Step1PageViewModel : IWizardPageViewModel
    {
        public bool IsComplete
        {
            get
            {
                return blnSelected;
            }
        }
        public bool blnSelected
        {
            get;
            set;
        }

        public bool CanReturn { get { return true; } }

        public bool pageIsValid
        {
            get { return true; }
        }

    }
}
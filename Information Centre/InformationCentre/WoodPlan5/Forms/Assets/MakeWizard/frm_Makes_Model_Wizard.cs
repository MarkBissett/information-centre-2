﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using BaseObjects;
using WoodPlan5.Forms.Assets.MakeWizard;
using WoodPlan5.Forms.Assets.MakeWizard.Views;
using DevExpress.XtraBars.Docking2010.Views.WindowsUI;
using System.Drawing;
using WoodPlan5.Forms.Assets;
namespace WoodPlan5
{
    public partial class frm_Makes_Model_Wizard : BaseObjects.frmBase
    {

        IWizardViewModel wizardViewModel;
        public frm_Makes_Model_Wizard()
        {
            InitializeComponent();

            wizardViewModel = new WoodPlan5.Forms.Assets.MakeWizard.ViewModels.WizardViewModel(
             new IWizardPageViewModel[]{
                    new WoodPlan5.Forms.Assets.MakeWizard.ViewModels.StartPageViewModel(),
                    new WoodPlan5.Forms.Assets.MakeWizard.ViewModels.Step1PageViewModel(),
                    new WoodPlan5.Forms.Assets.MakeWizard.ViewModels.Step2PageViewModel(),
                    new WoodPlan5.Forms.Assets.MakeWizard.ViewModels.ExecutePageViewModel(),
                    new WoodPlan5.Forms.Assets.MakeWizard.ViewModels.FinishPageViewModel() 
                },
             windowsUIView1, this);

            windowsUIView1.AddDocument(new ucStartPage() { Text = "Start" });
            windowsUIView1.AddDocument(new ucStep1Page() { Text = "Step 1" });
            windowsUIView1.AddDocument(new ucStep2Page() { Text = "Step 2" });
            windowsUIView1.AddDocument(new ucExecutePage() { Text = "Execute" });
            windowsUIView1.AddDocument(new ucFinishPage() { Text = "Finish" });

            foreach (Document document in windowsUIView1.Documents)
                pageGroup.Items.Add(document);
        }

        void windowsUIView1_NavigationBarsShowing(object sender, NavigationBarsCancelEventArgs e)
        {
            e.Cancel = true;
        }
        void windowsUIView1_NavigatedTo(object sender, NavigationEventArgs e)
        {
                e.Parameter = wizardViewModel;

                switch (((DevExpress.XtraBars.Docking2010.Views.BaseDocument)(e.Document)).Caption)
                {
                    case "Start":
                        this.windowsUIView1.Caption = "Makes & Models Wizard";
                        break;
                    case "Step 1":
                        this.windowsUIView1.Caption = "Step 1 of 3";
                        break;
                    case "Step 2":
                        this.windowsUIView1.Caption = "Step 2 of 3";
                        break;
                    case "Execute":
                        this.windowsUIView1.Caption = "Confirm Changes";
                        break;
                    case "Finish":
                        this.windowsUIView1.Caption = "";
                        break;
                }
          
        }
        
        void windowsUIView1_QueryDocumentActions(object sender, QueryDocumentActionsEventArgs e)
        {
            e.DocumentActions.Add(new DocumentAction((document) => wizardViewModel.CanPrev(),(document) => wizardViewModel.Prev()) { Caption = "Back", Image = imageList1.Images[0] });
            e.DocumentActions.Add(new DocumentAction((document) => wizardViewModel.CanNext(),(document) => wizardViewModel.Next()) { Caption = "Next", Image = imageList1.Images[1] });
            e.DocumentActions.Add(new DocumentAction((document) => wizardViewModel.CanClose(),(document) => wizardViewModel.Close(true)) { Caption = "Exit", Image = imageList1.Images[2] });
        }


        private void frm_Makes_Model_Wizard_Load(object sender, EventArgs e)
        {
            this.FormID = 1301;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            LoadConnectionStrings(this.GlobalSettings.ConnectionString);
            LoadAdapters();

            // Get Form Permissions //            
            ProcessPermissionsForForm();
            PostOpen(null);
        }
        #region Instance Variables

        private string strConnectionString = "";
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool bool_FormLoading = true;


        private bool iBool_AllowDelete = false;
        private bool iBool_AllowAdd = false;
        private bool iBool_AllowEdit = false;
        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        #endregion

        #region Events



        public void frmActivated()
        {
            if (UpdateRefreshStatus > 0)// || !string.IsNullOrEmpty(i_str_AddedRecordIDs1))
            {
                Load_Data();
            }
            SetMenuStatus();
        }

        #endregion

        #region Method

              
 
        private void Load_Data()
        {
            if (UpdateRefreshStatus > 0)
                UpdateRefreshStatus = 0;
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;

            bbiSave.Enabled = false;
            bbiCancel.Enabled = true;
            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = null;
            //getCurrentView(out view);

            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();


            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) 
                frmParent.PermissionsHandler(alItems);

            // Set enabled status of equipmentView navigator custom buttons //
            setCustomNavigatorAccess(notificationGridControl, view);
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                }
            }
        }

        private void setCustomNavigatorAccess(GridControl xGridControl, GridView view)
        {
            view = (GridView)xGridControl.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (iBool_AllowAdd)
            {
                xGridControl.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                xGridControl.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEdit)
            {
                xGridControl.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                xGridControl.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                xGridControl.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                xGridControl.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }
        }

        public static bool validatePage()
        {

            return true;
        }

        private void LoadConnectionStrings(string ConnString)
        {
            strConnectionString = ConnString;
            //sp_AS_11126_Notification_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
        }

        private void LoadAdapters()
        {
            this.sp00039GetFormPermissionsForUserTableAdapter.Fill(this.dataSet_AT.sp00039GetFormPermissionsForUser, FormID, GlobalSettings.UserID,0, GlobalSettings.ViewedPeriodID);
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            ConfigureFormAccordingToMode();
            _KeepWaitFormOpen = false;
            if (splashScreenManager.IsSplashFormVisible)
            {
                //if (strCaller != "")
                //{
                splashScreenManager.CloseWaitForm();
                //}
            }
            bool_FormLoading = false;
        }

        private void ConfigureFormAccordingToMode()
        {
     
            //Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            //Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            //if (strFormMode == "view")  // Disable all controls //
            //{
            //    // equipmentDataLayoutControl.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            //}
        }


        #endregion

      

       



    }
}
